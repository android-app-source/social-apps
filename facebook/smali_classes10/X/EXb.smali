.class public final LX/EXb;
.super LX/EX1;
.source ""

# interfaces
.implements LX/EXX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EX1",
        "<",
        "LX/EXb;",
        ">;",
        "LX/EXX;"
    }
.end annotation


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXb;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EXb;


# instance fields
.field public bitField0_:I

.field public ccGenericServices_:Z

.field public goPackage_:Ljava/lang/Object;

.field public javaGenerateEqualsAndHash_:Z

.field public javaGenericServices_:Z

.field public javaMultipleFiles_:Z

.field public javaOuterClassname_:Ljava/lang/Object;

.field public javaPackage_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public optimizeFor_:LX/EXa;

.field public pyGenericServices_:Z

.field public uninterpretedOption_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2134564
    new-instance v0, LX/EXW;

    invoke-direct {v0}, LX/EXW;-><init>()V

    sput-object v0, LX/EXb;->a:LX/EWZ;

    .line 2134565
    new-instance v0, LX/EXb;

    invoke-direct {v0}, LX/EXb;-><init>()V

    .line 2134566
    sput-object v0, LX/EXb;->c:LX/EXb;

    invoke-direct {v0}, LX/EXb;->N()V

    .line 2134567
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2134568
    invoke-direct {p0}, LX/EX1;-><init>()V

    .line 2134569
    iput-byte v0, p0, LX/EXb;->memoizedIsInitialized:B

    .line 2134570
    iput v0, p0, LX/EXb;->memoizedSerializedSize:I

    .line 2134571
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2134572
    iput-object v0, p0, LX/EXb;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/16 v6, 0x200

    .line 2134573
    invoke-direct {p0}, LX/EX1;-><init>()V

    .line 2134574
    iput-byte v1, p0, LX/EXb;->memoizedIsInitialized:B

    .line 2134575
    iput v1, p0, LX/EXb;->memoizedSerializedSize:I

    .line 2134576
    invoke-direct {p0}, LX/EXb;->N()V

    .line 2134577
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v3

    move v1, v0

    .line 2134578
    :cond_0
    :goto_0
    if-nez v1, :cond_4

    .line 2134579
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v4

    .line 2134580
    sparse-switch v4, :sswitch_data_0

    .line 2134581
    invoke-virtual {p0, p1, v3, p2, v4}, LX/EX1;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 2134582
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 2134583
    goto :goto_0

    .line 2134584
    :sswitch_1
    iget v4, p0, LX/EXb;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/EXb;->bitField0_:I

    .line 2134585
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v4

    iput-object v4, p0, LX/EXb;->javaPackage_:Ljava/lang/Object;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2134586
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 2134587
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2134588
    move-object v0, v0

    .line 2134589
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2134590
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit16 v1, v1, 0x200

    if-ne v1, v6, :cond_1

    .line 2134591
    iget-object v1, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    .line 2134592
    :cond_1
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EXb;->unknownFields:LX/EZQ;

    .line 2134593
    invoke-virtual {p0}, LX/EX1;->E()V

    throw v0

    .line 2134594
    :sswitch_2
    :try_start_2
    iget v4, p0, LX/EXb;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, LX/EXb;->bitField0_:I

    .line 2134595
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v4

    iput-object v4, p0, LX/EXb;->javaOuterClassname_:Ljava/lang/Object;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2134596
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 2134597
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2134598
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2134599
    move-object v0, v2

    .line 2134600
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2134601
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, LX/EWd;->m()I

    move-result v4

    .line 2134602
    invoke-static {v4}, LX/EXa;->valueOf(I)LX/EXa;

    move-result-object v5

    .line 2134603
    if-nez v5, :cond_2

    .line 2134604
    const/16 v5, 0x9

    invoke-virtual {v3, v5, v4}, LX/EZM;->a(II)LX/EZM;

    goto :goto_0

    .line 2134605
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 2134606
    :cond_2
    iget v4, p0, LX/EXb;->bitField0_:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, LX/EXb;->bitField0_:I

    .line 2134607
    iput-object v5, p0, LX/EXb;->optimizeFor_:LX/EXa;

    goto :goto_0

    .line 2134608
    :sswitch_4
    iget v4, p0, LX/EXb;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, LX/EXb;->bitField0_:I

    .line 2134609
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXb;->javaMultipleFiles_:Z

    goto/16 :goto_0

    .line 2134610
    :sswitch_5
    iget v4, p0, LX/EXb;->bitField0_:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, LX/EXb;->bitField0_:I

    .line 2134611
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v4

    iput-object v4, p0, LX/EXb;->goPackage_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 2134612
    :sswitch_6
    iget v4, p0, LX/EXb;->bitField0_:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, LX/EXb;->bitField0_:I

    .line 2134613
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXb;->ccGenericServices_:Z

    goto/16 :goto_0

    .line 2134614
    :sswitch_7
    iget v4, p0, LX/EXb;->bitField0_:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, LX/EXb;->bitField0_:I

    .line 2134615
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXb;->javaGenericServices_:Z

    goto/16 :goto_0

    .line 2134616
    :sswitch_8
    iget v4, p0, LX/EXb;->bitField0_:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, LX/EXb;->bitField0_:I

    .line 2134617
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXb;->pyGenericServices_:Z

    goto/16 :goto_0

    .line 2134618
    :sswitch_9
    iget v4, p0, LX/EXb;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, LX/EXb;->bitField0_:I

    .line 2134619
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXb;->javaGenerateEqualsAndHash_:Z

    goto/16 :goto_0

    .line 2134620
    :sswitch_a
    and-int/lit16 v4, v0, 0x200

    if-eq v4, v6, :cond_3

    .line 2134621
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    .line 2134622
    or-int/lit16 v0, v0, 0x200

    .line 2134623
    :cond_3
    iget-object v4, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    sget-object v5, LX/EYB;->a:LX/EWZ;

    invoke-virtual {p1, v5, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 2134624
    :cond_4
    and-int/lit16 v0, v0, 0x200

    if-ne v0, v6, :cond_5

    .line 2134625
    iget-object v0, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    .line 2134626
    :cond_5
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXb;->unknownFields:LX/EZQ;

    .line 2134627
    invoke-virtual {p0}, LX/EX1;->E()V

    .line 2134628
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x42 -> :sswitch_2
        0x48 -> :sswitch_3
        0x50 -> :sswitch_4
        0x5a -> :sswitch_5
        0x80 -> :sswitch_6
        0x88 -> :sswitch_7
        0x90 -> :sswitch_8
        0xa0 -> :sswitch_9
        0x1f3a -> :sswitch_a
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWy",
            "<",
            "LX/EXb;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 2134629
    invoke-direct {p0, p1}, LX/EX1;-><init>(LX/EWy;)V

    .line 2134630
    iput-byte v0, p0, LX/EXb;->memoizedIsInitialized:B

    .line 2134631
    iput v0, p0, LX/EXb;->memoizedSerializedSize:I

    .line 2134632
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXb;->unknownFields:LX/EZQ;

    .line 2134633
    return-void
.end method

.method private J()LX/EWc;
    .locals 2

    .prologue
    .line 2134634
    iget-object v0, p0, LX/EXb;->javaPackage_:Ljava/lang/Object;

    .line 2134635
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2134636
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2134637
    iput-object v0, p0, LX/EXb;->javaPackage_:Ljava/lang/Object;

    .line 2134638
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private K()LX/EWc;
    .locals 2

    .prologue
    .line 2134639
    iget-object v0, p0, LX/EXb;->javaOuterClassname_:Ljava/lang/Object;

    .line 2134640
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2134641
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2134642
    iput-object v0, p0, LX/EXb;->javaOuterClassname_:Ljava/lang/Object;

    .line 2134643
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private L()LX/EWc;
    .locals 2

    .prologue
    .line 2134646
    iget-object v0, p0, LX/EXb;->goPackage_:Ljava/lang/Object;

    .line 2134647
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2134648
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2134649
    iput-object v0, p0, LX/EXb;->goPackage_:Ljava/lang/Object;

    .line 2134650
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private N()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2134651
    const-string v0, ""

    iput-object v0, p0, LX/EXb;->javaPackage_:Ljava/lang/Object;

    .line 2134652
    const-string v0, ""

    iput-object v0, p0, LX/EXb;->javaOuterClassname_:Ljava/lang/Object;

    .line 2134653
    iput-boolean v1, p0, LX/EXb;->javaMultipleFiles_:Z

    .line 2134654
    iput-boolean v1, p0, LX/EXb;->javaGenerateEqualsAndHash_:Z

    .line 2134655
    sget-object v0, LX/EXa;->SPEED:LX/EXa;

    iput-object v0, p0, LX/EXb;->optimizeFor_:LX/EXa;

    .line 2134656
    const-string v0, ""

    iput-object v0, p0, LX/EXb;->goPackage_:Ljava/lang/Object;

    .line 2134657
    iput-boolean v1, p0, LX/EXb;->ccGenericServices_:Z

    .line 2134658
    iput-boolean v1, p0, LX/EXb;->javaGenericServices_:Z

    .line 2134659
    iput-boolean v1, p0, LX/EXb;->pyGenericServices_:Z

    .line 2134660
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    .line 2134661
    return-void
.end method

.method public static a(LX/EXb;)LX/EXY;
    .locals 1

    .prologue
    .line 2134662
    invoke-static {}, LX/EXY;->x()LX/EXY;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EXY;->a(LX/EXb;)LX/EXY;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final D()LX/EXY;
    .locals 1

    .prologue
    .line 2134563
    invoke-static {p0}, LX/EXb;->a(LX/EXb;)LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2134644
    new-instance v0, LX/EXY;

    invoke-direct {v0, p1}, LX/EXY;-><init>(LX/EYd;)V

    .line 2134645
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/16 v3, 0x8

    const/4 v1, 0x1

    .line 2134489
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2134490
    invoke-virtual {p0}, LX/EX1;->G()LX/EYf;

    move-result-object v2

    .line 2134491
    iget v0, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2134492
    invoke-direct {p0}, LX/EXb;->J()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2134493
    :cond_0
    iget v0, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2134494
    invoke-direct {p0}, LX/EXb;->K()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2134495
    :cond_1
    iget v0, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v4, :cond_2

    .line 2134496
    const/16 v0, 0x9

    iget-object v1, p0, LX/EXb;->optimizeFor_:LX/EXa;

    invoke-virtual {v1}, LX/EXa;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/EWf;->d(II)V

    .line 2134497
    :cond_2
    iget v0, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 2134498
    const/16 v0, 0xa

    iget-boolean v1, p0, LX/EXb;->javaMultipleFiles_:Z

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(IZ)V

    .line 2134499
    :cond_3
    iget v0, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 2134500
    const/16 v0, 0xb

    invoke-direct {p0}, LX/EXb;->L()LX/EWc;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2134501
    :cond_4
    iget v0, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 2134502
    iget-boolean v0, p0, LX/EXb;->ccGenericServices_:Z

    invoke-virtual {p1, v4, v0}, LX/EWf;->a(IZ)V

    .line 2134503
    :cond_5
    iget v0, p0, LX/EXb;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 2134504
    const/16 v0, 0x11

    iget-boolean v1, p0, LX/EXb;->javaGenericServices_:Z

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(IZ)V

    .line 2134505
    :cond_6
    iget v0, p0, LX/EXb;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_7

    .line 2134506
    const/16 v0, 0x12

    iget-boolean v1, p0, LX/EXb;->pyGenericServices_:Z

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(IZ)V

    .line 2134507
    :cond_7
    iget v0, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v3, :cond_8

    .line 2134508
    const/16 v0, 0x14

    iget-boolean v1, p0, LX/EXb;->javaGenerateEqualsAndHash_:Z

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(IZ)V

    .line 2134509
    :cond_8
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 2134510
    const/16 v3, 0x3e7

    iget-object v0, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2134511
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2134512
    :cond_9
    const/high16 v0, 0x20000000

    invoke-virtual {v2, v0, p1}, LX/EYf;->a(ILX/EWf;)V

    .line 2134513
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2134514
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2134516
    iget-byte v0, p0, LX/EXb;->memoizedIsInitialized:B

    .line 2134517
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2134518
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 2134519
    :goto_1
    iget-object v3, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move v3, v3

    .line 2134520
    if-ge v0, v3, :cond_3

    .line 2134521
    iget-object v3, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EYB;

    move-object v3, v3

    .line 2134522
    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2134523
    iput-byte v1, p0, LX/EXb;->memoizedIsInitialized:B

    goto :goto_0

    .line 2134524
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2134525
    :cond_3
    invoke-virtual {p0}, LX/EX1;->F()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2134526
    iput-byte v1, p0, LX/EXb;->memoizedIsInitialized:B

    goto :goto_0

    .line 2134527
    :cond_4
    iput-byte v2, p0, LX/EXb;->memoizedIsInitialized:B

    move v1, v2

    .line 2134528
    goto :goto_0
.end method

.method public final b()I
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2134529
    iget v0, p0, LX/EXb;->memoizedSerializedSize:I

    .line 2134530
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2134531
    :goto_0
    return v0

    .line 2134532
    :cond_0
    iget v0, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 2134533
    invoke-direct {p0}, LX/EXb;->J()LX/EWc;

    move-result-object v0

    invoke-static {v3, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2134534
    :goto_1
    iget v2, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2134535
    invoke-direct {p0}, LX/EXb;->K()LX/EWc;

    move-result-object v2

    invoke-static {v4, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2134536
    :cond_1
    iget v2, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v5, :cond_2

    .line 2134537
    const/16 v2, 0x9

    iget-object v3, p0, LX/EXb;->optimizeFor_:LX/EXa;

    invoke-virtual {v3}, LX/EXa;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, LX/EWf;->h(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2134538
    :cond_2
    iget v2, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 2134539
    const/16 v2, 0xa

    iget-boolean v3, p0, LX/EXb;->javaMultipleFiles_:Z

    invoke-static {v2, v3}, LX/EWf;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2134540
    :cond_3
    iget v2, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_4

    .line 2134541
    const/16 v2, 0xb

    invoke-direct {p0}, LX/EXb;->L()LX/EWc;

    move-result-object v3

    invoke-static {v2, v3}, LX/EWf;->c(ILX/EWc;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2134542
    :cond_4
    iget v2, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_5

    .line 2134543
    iget-boolean v2, p0, LX/EXb;->ccGenericServices_:Z

    invoke-static {v5, v2}, LX/EWf;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2134544
    :cond_5
    iget v2, p0, LX/EXb;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_6

    .line 2134545
    const/16 v2, 0x11

    iget-boolean v3, p0, LX/EXb;->javaGenericServices_:Z

    invoke-static {v2, v3}, LX/EWf;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2134546
    :cond_6
    iget v2, p0, LX/EXb;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_7

    .line 2134547
    const/16 v2, 0x12

    iget-boolean v3, p0, LX/EXb;->pyGenericServices_:Z

    invoke-static {v2, v3}, LX/EWf;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2134548
    :cond_7
    iget v2, p0, LX/EXb;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_8

    .line 2134549
    const/16 v2, 0x14

    iget-boolean v3, p0, LX/EXb;->javaGenerateEqualsAndHash_:Z

    invoke-static {v2, v3}, LX/EWf;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    move v2, v0

    .line 2134550
    :goto_2
    iget-object v0, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 2134551
    const/16 v3, 0x3e7

    iget-object v0, p0, LX/EXb;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2134552
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2134553
    :cond_9
    invoke-virtual {p0}, LX/EX1;->H()I

    move-result v0

    add-int/2addr v0, v2

    .line 2134554
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2134555
    iput v0, p0, LX/EXb;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2134556
    iget-object v0, p0, LX/EXb;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2134557
    sget-object v0, LX/EYC;->t:LX/EYn;

    const-class v1, LX/EXb;

    const-class v2, LX/EXY;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2134515
    sget-object v0, LX/EXb;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2134558
    invoke-virtual {p0}, LX/EXb;->D()LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2134559
    invoke-static {}, LX/EXY;->x()LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2134560
    invoke-virtual {p0}, LX/EXb;->D()LX/EXY;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2134561
    sget-object v0, LX/EXb;->c:LX/EXb;

    return-object v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2134562
    invoke-super {p0}, LX/EX1;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
