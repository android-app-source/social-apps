.class public LX/ETT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Ha;


# instance fields
.field public final a:LX/157;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/14v;

.field public final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Sh;

.field public final e:LX/3Hf;


# direct methods
.method public constructor <init>(LX/14v;LX/0Sh;LX/3Hf;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2125108
    new-instance v0, LX/ETS;

    invoke-direct {v0, p0}, LX/ETS;-><init>(LX/ETT;)V

    iput-object v0, p0, LX/ETT;->a:LX/157;

    .line 2125109
    iput-object p1, p0, LX/ETT;->b:LX/14v;

    .line 2125110
    iput-object p2, p0, LX/ETT;->d:LX/0Sh;

    .line 2125111
    iput-object p3, p0, LX/ETT;->e:LX/3Hf;

    .line 2125112
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/ETT;->c:Ljava/util/HashMap;

    .line 2125113
    return-void
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 2

    .prologue
    .line 2125103
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2125104
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2125105
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    .line 2125106
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 0

    .prologue
    .line 2125076
    invoke-virtual {p0, p1}, LX/ETT;->c(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    .line 2125077
    return-void
.end method

.method public final a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;)V
    .locals 0

    .prologue
    .line 2125102
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2125096
    iget-object v0, p0, LX/ETT;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2125097
    if-nez v0, :cond_1

    .line 2125098
    :cond_0
    :goto_0
    return-void

    .line 2125099
    :cond_1
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2125100
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2125101
    iget-object v1, p0, LX/ETT;->e:LX/3Hf;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/3Hf;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/6TW;)Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method

.method public final b(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 0

    .prologue
    .line 2125094
    invoke-virtual {p0, p1}, LX/ETT;->d(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V

    .line 2125095
    return-void
.end method

.method public final b(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;)V
    .locals 0

    .prologue
    .line 2125093
    return-void
.end method

.method public final c(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 2

    .prologue
    .line 2125085
    iget-object v0, p0, LX/ETT;->c:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2125086
    if-nez v0, :cond_1

    .line 2125087
    :cond_0
    :goto_0
    return-void

    .line 2125088
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    .line 2125089
    invoke-static {v1}, LX/Ac6;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2125090
    invoke-static {v0}, LX/ETT;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    .line 2125091
    invoke-static {v1}, LX/Ac6;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2125092
    iget-object v1, p0, LX/ETT;->e:LX/3Hf;

    invoke-virtual {v1, v0, p1}, LX/3Hf;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/6TW;)Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method

.method public final d(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 3

    .prologue
    .line 2125079
    iget-object v0, p0, LX/ETT;->c:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2125080
    if-nez v0, :cond_1

    .line 2125081
    :cond_0
    :goto_0
    return-void

    .line 2125082
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    .line 2125083
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v1, v2, :cond_0

    .line 2125084
    iget-object v1, p0, LX/ETT;->e:LX/3Hf;

    invoke-virtual {v1, v0, p1}, LX/3Hf;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/6TW;)Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method

.method public final s_(I)V
    .locals 0

    .prologue
    .line 2125078
    return-void
.end method
