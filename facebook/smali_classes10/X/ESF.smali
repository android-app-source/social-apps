.class public final LX/ESF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;)V
    .locals 0

    .prologue
    .line 2122693
    iput-object p1, p0, LX/ESF;->a:Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Z
    .locals 6

    .prologue
    .line 2122694
    iget-object v0, p0, LX/ESF;->a:Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2122695
    iget-wide v4, v0, LX/74w;->a:J

    move-wide v0, v4

    .line 2122696
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 2122697
    iget-object v0, p0, LX/ESF;->a:Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2122698
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2122699
    const-string v2, "onResultPhotoObjectParam"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2122700
    iget-object v0, p0, LX/ESF;->a:Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->setResult(ILandroid/content/Intent;)V

    .line 2122701
    iget-object v0, p0, LX/ESF;->a:Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;

    invoke-virtual {v0}, Lcom/facebook/vault/ui/VaultFullScreenGalleryActivity;->finish()V

    .line 2122702
    return-void
.end method
