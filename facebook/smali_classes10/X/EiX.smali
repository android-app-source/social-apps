.class public final LX/EiX;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/growth/model/Contactpoint;

.field public final synthetic b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/fragment/ConfContactpointFragment;Lcom/facebook/growth/model/Contactpoint;)V
    .locals 0

    .prologue
    .line 2159787
    iput-object p1, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iput-object p2, p0, LX/EiX;->a:Lcom/facebook/growth/model/Contactpoint;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 6

    .prologue
    .line 2159788
    iget-object v0, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v1, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159789
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v2

    .line 2159790
    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    iget-object v2, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    invoke-virtual {v2}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->n()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v2

    .line 2159791
    iget-object v3, v0, LX/2U9;->a:LX/0Zb;

    sget-object v4, LX/Eiw;->CHANGE_CONTACTPOINT_FAILURE:LX/Eiw;

    invoke-virtual {v4}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2159792
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2159793
    const-string v4, "confirmation"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159794
    const-string v4, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159795
    const-string v4, "new_contactpoint_type"

    invoke-virtual {v2}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159796
    const-string v4, "error_code"

    invoke-static {p1}, LX/2U9;->a(Lcom/facebook/fbservice/service/ServiceException;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2159797
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2159798
    :cond_0
    iget-object v0, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2159799
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2159800
    iget-object v0, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v1, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159801
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v2

    .line 2159802
    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    iget-object v2, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    invoke-virtual {v2}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->n()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v2

    .line 2159803
    iget-object v3, v0, LX/2U9;->a:LX/0Zb;

    sget-object v4, LX/Eiw;->CHANGE_CONTACTPOINT_SUCCESS:LX/Eiw;

    invoke-virtual {v4}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v4

    const/4 p1, 0x1

    invoke-interface {v3, v4, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2159804
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2159805
    const-string v4, "confirmation"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159806
    const-string v4, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, v4, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159807
    const-string v4, "new_contactpoint_type"

    invoke-virtual {v2}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, v4, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159808
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2159809
    :cond_0
    iget-object v0, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159810
    iget-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->b:Z

    move v0, v1

    .line 2159811
    if-eqz v0, :cond_1

    .line 2159812
    iget-object v0, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->d:LX/EjB;

    invoke-virtual {v0}, LX/EjB;->a()V

    .line 2159813
    :cond_1
    iget-object v0, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    iget-object v1, p0, LX/EiX;->a:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0, v1}, Lcom/facebook/confirmation/model/AccountConfirmationData;->a(Lcom/facebook/growth/model/Contactpoint;)V

    .line 2159814
    iget-object v0, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    const/4 v1, 0x1

    .line 2159815
    iput-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->f:Z

    .line 2159816
    iget-object v0, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U8;

    iget-object v1, p0, LX/EiX;->a:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0, v1}, LX/2U8;->a(Lcom/facebook/growth/model/Contactpoint;)Z

    .line 2159817
    iget-object v0, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    iget-object v1, p0, LX/EiX;->b:Lcom/facebook/confirmation/fragment/ConfContactpointFragment;

    invoke-virtual {v1}, Lcom/facebook/confirmation/fragment/ConfContactpointFragment;->o()LX/EiG;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(LX/EiG;)V

    .line 2159818
    return-void
.end method
