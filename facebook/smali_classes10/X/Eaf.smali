.class public LX/Eaf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Eae;

.field public final b:LX/Eas;


# direct methods
.method public constructor <init>(LX/Eae;LX/Eas;)V
    .locals 0

    .prologue
    .line 2142100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142101
    iput-object p1, p0, LX/Eaf;->a:LX/Eae;

    .line 2142102
    iput-object p2, p0, LX/Eaf;->b:LX/Eas;

    .line 2142103
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4

    .prologue
    .line 2142104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142105
    :try_start_0
    sget-object v0, LX/Ebp;->a:LX/EWZ;

    invoke-virtual {v0, p1}, LX/EWZ;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebp;

    move-object v0, v0

    .line 2142106
    new-instance v1, LX/Eae;

    .line 2142107
    iget-object v2, v0, LX/Ebp;->publicKey_:LX/EWc;

    move-object v2, v2

    .line 2142108
    invoke-virtual {v2}, LX/EWc;->d()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/Eae;-><init>([BI)V

    iput-object v1, p0, LX/Eaf;->a:LX/Eae;

    .line 2142109
    iget-object v1, v0, LX/Ebp;->privateKey_:LX/EWc;

    move-object v0, v1

    .line 2142110
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    invoke-static {v0}, LX/Ear;->a([B)LX/Eas;

    move-result-object v0

    iput-object v0, p0, LX/Eaf;->b:LX/Eas;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0

    .line 2142111
    return-void

    .line 2142112
    :catch_0
    move-exception v0

    .line 2142113
    new-instance v1, LX/Eag;

    invoke-direct {v1, v0}, LX/Eag;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final c()[B
    .locals 2

    .prologue
    .line 2142097
    invoke-static {}, LX/Ebo;->u()LX/Ebo;

    move-result-object v0

    iget-object v1, p0, LX/Eaf;->a:LX/Eae;

    invoke-virtual {v1}, LX/Eae;->b()[B

    move-result-object v1

    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ebo;->a(LX/EWc;)LX/Ebo;

    move-result-object v0

    iget-object v1, p0, LX/Eaf;->b:LX/Eas;

    .line 2142098
    iget-object p0, v1, LX/Eas;->a:[B

    move-object v1, p0

    .line 2142099
    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ebo;->b(LX/EWc;)LX/Ebo;

    move-result-object v0

    invoke-virtual {v0}, LX/Ebo;->l()LX/Ebp;

    move-result-object v0

    invoke-virtual {v0}, LX/EWX;->jZ_()[B

    move-result-object v0

    return-object v0
.end method
