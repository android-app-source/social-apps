.class public final LX/E3s;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic c:LX/3U7;

.field public final synthetic d:LX/E2b;

.field public final synthetic e:LX/E2c;

.field public final synthetic f:LX/0us;

.field public final synthetic g:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;LX/0Px;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/3U7;LX/E2b;LX/E2c;LX/0us;)V
    .locals 0

    .prologue
    .line 2075549
    iput-object p1, p0, LX/E3s;->g:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;

    iput-object p2, p0, LX/E3s;->a:LX/0Px;

    iput-object p3, p0, LX/E3s;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p4, p0, LX/E3s;->c:LX/3U7;

    iput-object p5, p0, LX/E3s;->d:LX/E2b;

    iput-object p6, p0, LX/E3s;->e:LX/E2c;

    iput-object p7, p0, LX/E3s;->f:LX/0us;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2075506
    iget-object v1, p0, LX/E3s;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    move v4, v0

    move v3, v0

    :goto_0
    if-ge v4, v5, :cond_1

    iget-object v0, p0, LX/E3s;->a:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2075507
    new-instance v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v1, p0, LX/E3s;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075508
    iget-object v7, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v7

    .line 2075509
    iget-object v7, p0, LX/E3s;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075510
    iget-object v8, v7, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v8

    .line 2075511
    invoke-direct {v6, v0, v1, v7}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2075512
    iget-object v1, p0, LX/E3s;->g:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->g:LX/1vo;

    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v7

    invoke-virtual {v1, v7}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v1

    .line 2075513
    invoke-interface {v1, v6}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2075514
    instance-of v7, v1, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    if-eqz v7, :cond_0

    .line 2075515
    check-cast v1, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->b(LX/9uc;)LX/1Cz;

    move-result-object v3

    iget-object v0, p0, LX/E3s;->c:LX/3U7;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v7, p0, LX/E3s;->g:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;

    iget-object v7, v7, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->d:LX/1Qx;

    invoke-static {v1, v3, v0, v7}, LX/6Vo;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v0

    invoke-virtual {p1, v0, v6}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    move v0, v2

    .line 2075516
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v3, v0

    goto :goto_0

    .line 2075517
    :cond_0
    instance-of v7, v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v7, :cond_3

    .line 2075518
    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-static {v0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->b(LX/9uc;)LX/1Cz;

    move-result-object v3

    iget-object v0, p0, LX/E3s;->c:LX/3U7;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v7, p0, LX/E3s;->g:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;

    iget-object v7, v7, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->d:LX/1Qx;

    invoke-static {v1, v3, v0, v7}, LX/6Vo;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v0

    invoke-virtual {p1, v0, v6}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    move v0, v2

    goto :goto_1

    .line 2075519
    :cond_1
    if-nez v3, :cond_2

    .line 2075520
    iget-object v0, p0, LX/E3s;->g:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;

    iget-object v1, p0, LX/E3s;->c:LX/3U7;

    .line 2075521
    move-object v2, v1

    check-cast v2, LX/2kp;

    invoke-interface {v2}, LX/2kp;->t()LX/2jY;

    move-result-object v2

    .line 2075522
    iget-object v3, v2, LX/2jY;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2075523
    const-string v3, "VIDEO_HOME"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2075524
    check-cast v1, LX/1Ps;

    invoke-interface {v1}, LX/1Ps;->i()Ljava/lang/Object;

    move-result-object v2

    .line 2075525
    instance-of v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v3, :cond_2

    .line 2075526
    check-cast v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075527
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v3

    .line 2075528
    invoke-interface {v2}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v2

    .line 2075529
    if-eqz v2, :cond_2

    .line 2075530
    iget-object v3, v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->k:LX/03V;

    const-string v4, "ReactionPaginatedHScrollUnitComponentPartDefinition.noPages"

    const-string v5, "No needed pages for section: %s"

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2075531
    :cond_2
    return-void

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public final c(I)V
    .locals 5

    .prologue
    .line 2075532
    iget-object v0, p0, LX/E3s;->c:LX/3U7;

    check-cast v0, LX/1Pr;

    iget-object v1, p0, LX/E3s;->d:LX/E2b;

    iget-object v2, p0, LX/E3s;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2c;

    .line 2075533
    iput p1, v0, LX/E2c;->d:I

    .line 2075534
    iget-object v0, p0, LX/E3s;->c:LX/3U7;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    iget-object v1, p0, LX/E3s;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075535
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2075536
    iget-object v2, p0, LX/E3s;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075537
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2075538
    invoke-virtual {v0, v1, v2}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2075539
    iget-object v0, p0, LX/E3s;->c:LX/3U7;

    check-cast v0, LX/2kp;

    invoke-interface {v0}, LX/2kp;->t()LX/2jY;

    move-result-object v0

    .line 2075540
    iget-object v1, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2075541
    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    if-ne v0, v1, :cond_0

    .line 2075542
    iget-object v0, p0, LX/E3s;->c:LX/3U7;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v1

    iget-object v0, p0, LX/E3s;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075543
    iget-object v2, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2075544
    iget-object v0, p0, LX/E3s;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075545
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2075546
    iget-object v0, p0, LX/E3s;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    invoke-interface {v0}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, p1, v0}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 2075547
    :cond_0
    iget-object v0, p0, LX/E3s;->g:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;

    iget-object v1, p0, LX/E3s;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p0, LX/E3s;->c:LX/3U7;

    iget-object v3, p0, LX/E3s;->e:LX/E2c;

    iget-object v4, p0, LX/E3s;->f:LX/0us;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;->a$redex0(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/3U7;LX/E2c;LX/0us;)V

    .line 2075548
    return-void
.end method
