.class public LX/DDw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1976131
    return-void
.end method

.method public static a(LX/0QB;)LX/DDw;
    .locals 3

    .prologue
    .line 1976132
    const-class v1, LX/DDw;

    monitor-enter v1

    .line 1976133
    :try_start_0
    sget-object v0, LX/DDw;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976134
    sput-object v2, LX/DDw;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976135
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976136
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1976137
    new-instance v0, LX/DDw;

    invoke-direct {v0}, LX/DDw;-><init>()V

    .line 1976138
    move-object v0, v0

    .line 1976139
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976140
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DDw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976141
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976142
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 3

    .prologue
    .line 1976143
    const-class v0, LX/DDh;

    sget-object v1, LX/0ja;->a:LX/3AL;

    sget-object v2, LX/0ja;->e:LX/3AM;

    invoke-virtual {p1, v0, v1, v2}, LX/0ja;->a(Ljava/lang/Class;LX/3AL;LX/3AM;)V

    .line 1976144
    return-void
.end method
