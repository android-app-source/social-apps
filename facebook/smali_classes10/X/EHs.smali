.class public final LX/EHs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/EI9;


# direct methods
.method public constructor <init>(LX/EI9;ZLandroid/view/View;)V
    .locals 0

    .prologue
    .line 2100452
    iput-object p1, p0, LX/EHs;->c:LX/EI9;

    iput-boolean p2, p0, LX/EHs;->a:Z

    iput-object p3, p0, LX/EHs;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2100453
    iget-boolean v0, p0, LX/EHs;->a:Z

    if-nez v0, :cond_0

    .line 2100454
    iget-object v0, p0, LX/EHs;->b:Landroid/view/View;

    iget-object v1, p0, LX/EHs;->c:LX/EI9;

    iget-object v1, v1, LX/EI9;->H:Landroid/view/View;

    if-ne v0, v1, :cond_1

    .line 2100455
    iget-object v0, p0, LX/EHs;->c:LX/EI9;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/EI9;->setButtonsHolderVisibility(LX/EI9;Z)V

    .line 2100456
    :cond_0
    :goto_0
    return-void

    .line 2100457
    :cond_1
    iget-object v0, p0, LX/EHs;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2100458
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2100459
    iget-boolean v0, p0, LX/EHs;->a:Z

    if-eqz v0, :cond_0

    .line 2100460
    iget-object v0, p0, LX/EHs;->b:Landroid/view/View;

    iget-object v1, p0, LX/EHs;->c:LX/EI9;

    iget-object v1, v1, LX/EI9;->H:Landroid/view/View;

    if-ne v0, v1, :cond_1

    .line 2100461
    iget-object v0, p0, LX/EHs;->c:LX/EI9;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/EI9;->setButtonsHolderVisibility(LX/EI9;Z)V

    .line 2100462
    :cond_0
    :goto_0
    return-void

    .line 2100463
    :cond_1
    iget-object v0, p0, LX/EHs;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
