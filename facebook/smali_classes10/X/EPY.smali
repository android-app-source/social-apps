.class public final LX/EPY;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/CxV;

.field public final synthetic d:LX/CzL;

.field public final synthetic e:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Ljava/lang/String;

.field public final synthetic h:LX/EPa;


# direct methods
.method public constructor <init>(LX/EPa;Ljava/lang/String;Ljava/lang/String;LX/CxV;LX/CzL;Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2117065
    iput-object p1, p0, LX/EPY;->h:LX/EPa;

    iput-object p2, p0, LX/EPY;->a:Ljava/lang/String;

    iput-object p3, p0, LX/EPY;->b:Ljava/lang/String;

    iput-object p4, p0, LX/EPY;->c:LX/CxV;

    iput-object p5, p0, LX/EPY;->d:LX/CzL;

    iput-object p6, p0, LX/EPY;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    iput-object p7, p0, LX/EPY;->f:Ljava/lang/String;

    iput-object p8, p0, LX/EPY;->g:Ljava/lang/String;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    .line 2117066
    iget-object v0, p0, LX/EPY;->h:LX/EPa;

    iget-object v0, v0, LX/EPa;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nD;

    iget-object v1, p0, LX/EPY;->a:Ljava/lang/String;

    iget-object v2, p0, LX/EPY;->b:Ljava/lang/String;

    iget-object v3, p0, LX/EPY;->c:LX/CxV;

    invoke-interface {v3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v3

    invoke-interface {v3}, LX/8ef;->m()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/8ci;->p:LX/8ci;

    iget-object v5, p0, LX/EPY;->c:LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v5

    .line 2117067
    iget-object v6, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v5, v6

    .line 2117068
    iget-object v6, p0, LX/EPY;->c:LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/1nD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v1

    .line 2117069
    iget-object v0, p0, LX/EPY;->h:LX/EPa;

    iget-object v0, v0, LX/EPa;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2117070
    iget-object v0, p0, LX/EPY;->h:LX/EPa;

    iget-object v0, v0, LX/EPa;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v1, p0, LX/EPY;->c:LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->CLICK:LX/8ch;

    iget-object v3, p0, LX/EPY;->c:LX/CxV;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/EPY;->d:LX/CzL;

    invoke-interface {v3, v4}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, LX/EPY;->d:LX/CzL;

    iget-object v5, p0, LX/EPY;->c:LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v7

    iget-object v5, p0, LX/EPY;->c:LX/CxV;

    check-cast v5, LX/CxP;

    iget-object v6, p0, LX/EPY;->d:LX/CzL;

    invoke-interface {v5, v6}, LX/CxP;->b(LX/CzL;)I

    move-result v8

    iget-object v9, p0, LX/EPY;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    iget-object v5, p0, LX/EPY;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->SHOWING:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-ne v5, v6, :cond_0

    iget-object v5, p0, LX/EPY;->f:Ljava/lang/String;

    :goto_0
    iget-object v6, p0, LX/EPY;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->SHOWING:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-ne v6, v10, :cond_1

    iget-object v6, p0, LX/EPY;->g:Ljava/lang/String;

    :goto_1
    invoke-static {v7, v8, v9, v5, v6}, LX/CvY;->c(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2117071
    return-void

    .line 2117072
    :cond_0
    iget-object v5, p0, LX/EPY;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v6, p0, LX/EPY;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2117073
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2117074
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2117075
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2117076
    return-void
.end method
