.class public final enum LX/D8y;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D8y;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D8y;

.field public static final enum APPEAR:LX/D8y;

.field public static final enum HOOK_SHOT_BOTTOM:LX/D8y;

.field public static final enum HOOK_SHOT_TOP:LX/D8y;

.field public static final enum SLIDE_IN_BOTTOM:LX/D8y;

.field public static final enum ZOOM:LX/D8y;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1969578
    new-instance v0, LX/D8y;

    const-string v1, "HOOK_SHOT_TOP"

    invoke-direct {v0, v1, v2}, LX/D8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8y;->HOOK_SHOT_TOP:LX/D8y;

    .line 1969579
    new-instance v0, LX/D8y;

    const-string v1, "HOOK_SHOT_BOTTOM"

    invoke-direct {v0, v1, v3}, LX/D8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8y;->HOOK_SHOT_BOTTOM:LX/D8y;

    .line 1969580
    new-instance v0, LX/D8y;

    const-string v1, "ZOOM"

    invoke-direct {v0, v1, v4}, LX/D8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8y;->ZOOM:LX/D8y;

    .line 1969581
    new-instance v0, LX/D8y;

    const-string v1, "APPEAR"

    invoke-direct {v0, v1, v5}, LX/D8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8y;->APPEAR:LX/D8y;

    .line 1969582
    new-instance v0, LX/D8y;

    const-string v1, "SLIDE_IN_BOTTOM"

    invoke-direct {v0, v1, v6}, LX/D8y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D8y;->SLIDE_IN_BOTTOM:LX/D8y;

    .line 1969583
    const/4 v0, 0x5

    new-array v0, v0, [LX/D8y;

    sget-object v1, LX/D8y;->HOOK_SHOT_TOP:LX/D8y;

    aput-object v1, v0, v2

    sget-object v1, LX/D8y;->HOOK_SHOT_BOTTOM:LX/D8y;

    aput-object v1, v0, v3

    sget-object v1, LX/D8y;->ZOOM:LX/D8y;

    aput-object v1, v0, v4

    sget-object v1, LX/D8y;->APPEAR:LX/D8y;

    aput-object v1, v0, v5

    sget-object v1, LX/D8y;->SLIDE_IN_BOTTOM:LX/D8y;

    aput-object v1, v0, v6

    sput-object v0, LX/D8y;->$VALUES:[LX/D8y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1969575
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D8y;
    .locals 1

    .prologue
    .line 1969577
    const-class v0, LX/D8y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D8y;

    return-object v0
.end method

.method public static values()[LX/D8y;
    .locals 1

    .prologue
    .line 1969576
    sget-object v0, LX/D8y;->$VALUES:[LX/D8y;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D8y;

    return-object v0
.end method
