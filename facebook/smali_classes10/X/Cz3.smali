.class public LX/Cz3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cyv;


# instance fields
.field private final a:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

.field private final b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V
    .locals 2

    .prologue
    .line 1954118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954119
    iput-object p1, p0, LX/Cz3;->a:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    .line 1954120
    invoke-static {p1}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    iput-object v0, p0, LX/Cz3;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1954121
    iget-object v0, p0, LX/Cz3;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Unknown result type."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1954122
    return-void

    .line 1954123
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 2

    .prologue
    .line 1954116
    iget-object v0, p0, LX/Cz3;->a:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cz3;->a:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    goto :goto_0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1954117
    iget-object v0, p0, LX/Cz3;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;
    .locals 1

    .prologue
    .line 1954124
    iget-object v0, p0, LX/Cz3;->a:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1954105
    if-ne p1, p0, :cond_1

    .line 1954106
    :cond_0
    :goto_0
    return v0

    .line 1954107
    :cond_1
    instance-of v2, p1, LX/Cz3;

    if-nez v2, :cond_2

    move v0, v1

    .line 1954108
    goto :goto_0

    .line 1954109
    :cond_2
    check-cast p1, LX/Cz3;

    .line 1954110
    iget-object v2, p0, LX/Cz3;->a:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    invoke-virtual {p1}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Cz3;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {p1}, LX/Cz3;->c()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1954111
    iget-object v0, p0, LX/Cz3;->a:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 1954112
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/Cz3;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1954113
    return v0

    .line 1954114
    :cond_0
    iget-object v0, p0, LX/Cz3;->a:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1954115
    :cond_1
    iget-object v1, p0, LX/Cz3;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->hashCode()I

    move-result v1

    goto :goto_1
.end method
