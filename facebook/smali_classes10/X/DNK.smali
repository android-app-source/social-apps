.class public final LX/DNK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/44w",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedHomeStories;",
        "LX/0ta;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DNR;


# direct methods
.method public constructor <init>(LX/DNR;)V
    .locals 0

    .prologue
    .line 1991335
    iput-object p1, p0, LX/DNK;->a:LX/DNR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1991336
    iget-object v0, p0, LX/DNK;->a:LX/DNR;

    iget-boolean v0, v0, LX/DNR;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNK;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->b:LX/2lS;

    if-eqz v0, :cond_0

    .line 1991337
    iget-object v0, p0, LX/DNK;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->b:LX/2lS;

    .line 1991338
    sget-object v1, LX/2lT;->CACHED_STORIES:LX/2lT;

    invoke-static {v0, v1}, LX/2lS;->a(LX/2lS;LX/2lT;)V

    .line 1991339
    :cond_0
    iget-object v0, p0, LX/DNK;->a:LX/DNR;

    invoke-static {v0}, LX/DNR;->p(LX/DNR;)V

    .line 1991340
    iget-object v0, p0, LX/DNK;->a:LX/DNR;

    iget-boolean v0, v0, LX/DNR;->w:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 1991341
    :goto_0
    iget-object v1, p0, LX/DNK;->a:LX/DNR;

    iget-object v1, v1, LX/DNR;->r:LX/DNT;

    iget-object v2, p0, LX/DNK;->a:LX/DNR;

    iget-object v2, v2, LX/DNR;->c:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->s()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/DNK;->a:LX/DNR;

    iget-boolean v3, v3, LX/DNR;->w:Z

    .line 1991342
    new-instance v5, LX/0rT;

    invoke-direct {v5}, LX/0rT;-><init>()V

    if-eqz v3, :cond_2

    iget v4, v1, LX/DNT;->f:I

    .line 1991343
    :goto_1
    iput v4, v5, LX/0rT;->c:I

    .line 1991344
    move-object v4, v5

    .line 1991345
    iget-object v5, v1, LX/DNT;->g:Lcom/facebook/api/feedtype/FeedType;

    .line 1991346
    iput-object v5, v4, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 1991347
    move-object v4, v4

    .line 1991348
    iput-object v0, v4, LX/0rT;->a:LX/0rS;

    .line 1991349
    move-object v4, v4

    .line 1991350
    iget-object v5, v1, LX/DNT;->h:Lcom/facebook/api/feed/FeedFetchContext;

    .line 1991351
    iput-object v5, v4, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 1991352
    move-object v4, v4

    .line 1991353
    iput-object v2, v4, LX/0rT;->f:Ljava/lang/String;

    .line 1991354
    move-object v4, v4

    .line 1991355
    iget-object v5, v1, LX/DNT;->d:LX/1BY;

    invoke-virtual {v5}, LX/1BY;->a()LX/0Px;

    move-result-object v5

    .line 1991356
    iput-object v5, v4, LX/0rT;->n:LX/0Px;

    .line 1991357
    move-object v4, v4

    .line 1991358
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1991359
    const-string v6, "fetchFeedParams"

    invoke-virtual {v4}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1991360
    iget-object v4, v1, LX/DNT;->b:LX/0aG;

    const-string v6, "feed_fetch_news_feed_after"

    const p0, -0x5c4712a9

    invoke-static {v4, v6, v5, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->startTryNotToUseMainThread()LX/1ML;

    move-result-object v4

    .line 1991361
    sget-object v5, LX/DNT;->a:LX/DNS;

    iget-object v6, v1, LX/DNT;->c:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 1991362
    return-object v0

    .line 1991363
    :cond_1
    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    goto :goto_0

    .line 1991364
    :cond_2
    iget v4, v1, LX/DNT;->e:I

    goto :goto_1
.end method
