.class public final LX/DIH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/goodfriends/audience/AudienceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V
    .locals 0

    .prologue
    .line 1983145
    iput-object p1, p0, LX/DIH;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x334ca2ad    # -9.403868E7f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1983146
    iget-object v1, p0, LX/DIH;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v1, v1, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    const/4 v5, 0x0

    .line 1983147
    iget-object v3, v1, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->i:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 1983148
    iget-object v3, v1, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 1983149
    iget-object v3, v1, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->l:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result p0

    move v4, v5

    :goto_0
    if-ge v4, p0, :cond_1

    iget-object v3, v1, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->l:LX/0Px;

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodfriends/data/FriendData;

    .line 1983150
    invoke-virtual {v3}, Lcom/facebook/goodfriends/data/FriendData;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1983151
    iget-object p1, v1, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->j:Ljava/util/Set;

    invoke-interface {p1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1983152
    :cond_0
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1983153
    :cond_1
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1983154
    invoke-static {v1, v5}, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->h(Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;I)V

    .line 1983155
    const v1, 0x50b4bca1

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
