.class public LX/Dvd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Dvd;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dvr;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2059049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059050
    iput-object p1, p0, LX/Dvd;->a:LX/0Ot;

    .line 2059051
    return-void
.end method

.method public static a(LX/0QB;)LX/Dvd;
    .locals 4

    .prologue
    .line 2059052
    sget-object v0, LX/Dvd;->b:LX/Dvd;

    if-nez v0, :cond_1

    .line 2059053
    const-class v1, LX/Dvd;

    monitor-enter v1

    .line 2059054
    :try_start_0
    sget-object v0, LX/Dvd;->b:LX/Dvd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2059055
    if-eqz v2, :cond_0

    .line 2059056
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2059057
    new-instance v3, LX/Dvd;

    const/16 p0, 0x2e99

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Dvd;-><init>(LX/0Ot;)V

    .line 2059058
    move-object v0, v3

    .line 2059059
    sput-object v0, LX/Dvd;->b:LX/Dvd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2059060
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2059061
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2059062
    :cond_1
    sget-object v0, LX/Dvd;->b:LX/Dvd;

    return-object v0

    .line 2059063
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2059064
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/Dvm;LX/Dvf;LX/0Px;Z)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Dvm;",
            "LX/Dvf;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;",
            ">;Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2059065
    invoke-interface {p2}, LX/Dvf;->a()LX/0Px;

    move-result-object v6

    .line 2059066
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2059067
    const/4 v3, 0x0

    .line 2059068
    if-eqz p4, :cond_0

    .line 2059069
    iget-object v0, p0, LX/Dvd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2059070
    if-nez p1, :cond_a

    .line 2059071
    :cond_0
    :goto_0
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v6, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/Dvo;

    if-eqz v0, :cond_1

    .line 2059072
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dvo;

    .line 2059073
    invoke-virtual {v0, p1, p3}, LX/Dvo;->a(LX/Dvm;LX/0Px;)LX/0Px;

    move-result-object v0

    .line 2059074
    :goto_1
    return-object v0

    .line 2059075
    :cond_1
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    move v4, v0

    :goto_2
    if-ge v4, v8, :cond_7

    invoke-virtual {p3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    .line 2059076
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v5, v0

    :goto_3
    if-ge v5, v9, :cond_6

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dvi;

    .line 2059077
    instance-of v1, v0, LX/Dvk;

    if-eqz v1, :cond_9

    move-object v1, v0

    .line 2059078
    check-cast v1, LX/Dvk;

    .line 2059079
    invoke-virtual {v1, v2}, LX/Dvk;->a(Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 2059080
    if-eqz v3, :cond_9

    invoke-virtual {v3, p1}, LX/Dvn;->a(LX/Dvm;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2059081
    invoke-virtual {v3, p1}, LX/Dvn;->b(LX/Dvm;)LX/0Px;

    move-result-object v0

    .line 2059082
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2059083
    invoke-virtual {v7, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    :cond_2
    move-object v0, v3

    .line 2059084
    :cond_3
    :goto_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v3, v0

    goto :goto_3

    .line 2059085
    :cond_4
    instance-of v3, v0, LX/Dvn;

    if-eqz v3, :cond_5

    check-cast v0, LX/Dvn;

    .line 2059086
    :goto_5
    invoke-virtual {v1, p1, v2}, LX/Dvk;->a(LX/Dvm;Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;)LX/0Px;

    move-result-object v1

    .line 2059087
    if-eqz v1, :cond_3

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2059088
    invoke-virtual {v7, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_4

    .line 2059089
    :cond_5
    const/4 v0, 0x0

    goto :goto_5

    .line 2059090
    :cond_6
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 2059091
    :cond_7
    if-eqz v3, :cond_8

    invoke-virtual {v3, p1}, LX/Dvn;->a(LX/Dvm;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p1, LX/Dvm;->e:Z

    if-nez v0, :cond_8

    .line 2059092
    const/4 v0, 0x1

    invoke-virtual {v3, p1, v0}, LX/Dvn;->a(LX/Dvm;Z)LX/0Px;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2059093
    :cond_8
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_1

    :cond_9
    move-object v0, v3

    goto :goto_4

    .line 2059094
    :cond_a
    sget-object v0, LX/Dvr;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2059095
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2059096
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/16 :goto_0
.end method
