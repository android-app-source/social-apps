.class public LX/DcH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dc9;


# instance fields
.field private final a:LX/Dc5;


# direct methods
.method public constructor <init>(LX/Dc5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2018134
    iput-object p1, p0, LX/DcH;->a:LX/Dc5;

    .line 2018135
    return-void
.end method


# virtual methods
.method public final a()LX/DcG;
    .locals 1

    .prologue
    .line 2018136
    sget-object v0, LX/DcG;->NONE:LX/DcG;

    return-object v0
.end method

.method public final a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V
    .locals 2
    .param p4    # Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018137
    iget-object v0, p0, LX/DcH;->a:LX/Dc5;

    const-string v1, "none"

    invoke-virtual {v0, p1, p3, v1}, LX/Dc5;->a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2018138
    return-void
.end method

.method public final a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 2018139
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 2018140
    const/4 v0, 0x0

    return v0
.end method

.method public final a(LX/15i;I)Z
    .locals 3
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "isVisibleMenu"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2018141
    if-nez p2, :cond_1

    .line 2018142
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1, p2, v0}, LX/15i;->h(II)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1, p2, v1}, LX/15i;->h(II)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p1, p2, v2}, LX/15i;->h(II)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Z
    .locals 1
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "shouldShowInManagementScreen"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2018143
    const/4 v0, 0x1

    return v0
.end method
