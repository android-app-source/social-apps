.class public final LX/Dqu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Dqy;


# direct methods
.method public constructor <init>(LX/Dqy;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2048758
    iput-object p1, p0, LX/Dqu;->b:LX/Dqy;

    iput-object p2, p0, LX/Dqu;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    .line 2048759
    check-cast p1, LX/4ok;

    .line 2048760
    iget-object v0, p0, LX/Dqu;->b:LX/Dqy;

    .line 2048761
    new-instance v1, LX/4HN;

    invoke-direct {v1}, LX/4HN;-><init>()V

    invoke-virtual {p1}, LX/4ok;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 2048762
    const-string v3, "category_key"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2048763
    move-object v1, v1

    .line 2048764
    invoke-virtual {p1}, LX/4ok;->isChecked()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2048765
    const-string v3, "status"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2048766
    move-object v1, v1

    .line 2048767
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2048768
    const-string v3, "client_mutation_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2048769
    move-object v1, v1

    .line 2048770
    new-instance v2, LX/BDd;

    invoke-direct {v2}, LX/BDd;-><init>()V

    move-object v2, v2

    .line 2048771
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2048772
    iget-object v1, v0, LX/Dqy;->c:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2048773
    iget-object v2, v0, LX/Dqy;->b:LX/1Ck;

    invoke-virtual {p1}, LX/4ok;->getKey()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/Dqv;

    invoke-direct {v4, v0, p1}, LX/Dqv;-><init>(LX/Dqy;LX/4ok;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2048774
    iget-object v0, p0, LX/Dqu;->b:LX/Dqy;

    iget-object v0, v0, LX/Dqy;->e:LX/33a;

    iget-object v1, p0, LX/Dqu;->a:Ljava/lang/String;

    invoke-virtual {p1}, LX/4ok;->isChecked()Z

    move-result v2

    .line 2048775
    const-string v3, "push_settings_updated"

    invoke-static {v3}, LX/33a;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "category_key"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "preference_status"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2048776
    iget-object v4, v0, LX/33a;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2048777
    const/4 v0, 0x1

    return v0
.end method
