.class public final LX/ECE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2SC;


# direct methods
.method public constructor <init>(LX/2SC;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2089524
    iput-object p1, p0, LX/ECE;->b:LX/2SC;

    iput-object p2, p0, LX/ECE;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2089525
    const/4 v0, 0x0

    .line 2089526
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_0

    .line 2089527
    :goto_0
    return-object v0

    .line 2089528
    :cond_0
    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 2089529
    :try_start_1
    iget-object v1, p0, LX/ECE;->b:LX/2SC;

    iget-object v2, p0, LX/ECE;->a:Ljava/lang/String;

    .line 2089530
    if-nez v2, :cond_4

    .line 2089531
    const/4 v4, 0x0

    .line 2089532
    :goto_1
    move-object v1, v4

    .line 2089533
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2089534
    :try_start_2
    invoke-static {v3, v2}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2089535
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 2089536
    if-eqz v3, :cond_1

    .line 2089537
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 2089538
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    :goto_2
    if-eqz v1, :cond_2

    .line 2089539
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 2089540
    :cond_2
    if-eqz v2, :cond_3

    .line 2089541
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_3
    throw v0

    .line 2089542
    :catchall_1
    move-exception v1

    move-object v2, v3

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    .line 2089543
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "fb_voicemail_asset_"

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2089544
    new-instance v4, Ljava/io/File;

    iget-object p1, v1, LX/2SC;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p1

    invoke-direct {v4, p1, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_1
.end method
