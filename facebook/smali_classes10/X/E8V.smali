.class public final LX/E8V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;)V
    .locals 0

    .prologue
    .line 2082780
    iput-object p1, p0, LX/E8V;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 5

    .prologue
    .line 2082781
    iget-object v0, p0, LX/E8V;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    .line 2082782
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, v1

    .line 2082783
    if-nez v0, :cond_1

    .line 2082784
    :cond_0
    :goto_0
    return-void

    .line 2082785
    :cond_1
    iget-object v0, p0, LX/E8V;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    invoke-virtual {v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->O()V

    .line 2082786
    iget-object v0, p0, LX/E8V;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v1, p0, LX/E8V;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-object v1, v1, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->j:LX/E8t;

    invoke-virtual {v1}, LX/E8t;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2082787
    iget-object v1, p0, LX/E8V;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-object v1, v1, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/E8V;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-object v1, v1, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 2082788
    iget-object v1, p0, LX/E8V;->a:Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-object v1, v1, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n:Landroid/view/ViewGroup;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    const/16 v4, 0x50

    invoke-direct {v2, v3, v0, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
