.class public final LX/Eqq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Equ;


# direct methods
.method public constructor <init>(LX/Equ;)V
    .locals 0

    .prologue
    .line 2172140
    iput-object p1, p0, LX/Eqq;->a:LX/Equ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2172141
    iget-object v0, p0, LX/Eqq;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->A:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    iget-object v1, p0, LX/Eqq;->a:LX/Equ;

    iget-object v1, v1, LX/Equ;->s:LX/3Oq;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2172142
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 2172143
    move-object v0, v0

    .line 2172144
    sget-object v1, LX/2RS;->NAME:LX/2RS;

    .line 2172145
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 2172146
    move-object v0, v0

    .line 2172147
    iget-object v1, p0, LX/Eqq;->a:LX/Equ;

    iget-object v1, v1, LX/Equ;->r:LX/2RC;

    sget-object v2, LX/2RV;->CONTACT:LX/2RV;

    invoke-virtual {v1, v0, v2}, LX/2RC;->a(LX/2RR;LX/2RV;)Landroid/database/Cursor;

    move-result-object v0

    .line 2172148
    iget-object v1, p0, LX/Eqq;->a:LX/Equ;

    iget-object v1, v1, LX/Equ;->w:LX/Era;

    invoke-virtual {v1, v0}, LX/Era;->a(Landroid/database/Cursor;)LX/ErZ;

    move-result-object v1

    .line 2172149
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    .line 2172150
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v1}, LX/2TZ;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2172151
    invoke-virtual {v1}, LX/ErZ;->b()Landroid/util/Pair;

    move-result-object v0

    .line 2172152
    if-eqz v0, :cond_0

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v3, :cond_0

    .line 2172153
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2172154
    new-instance v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    .line 2172155
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2172156
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2TZ;->close()V

    throw v0

    :cond_1
    invoke-virtual {v1}, LX/2TZ;->close()V

    .line 2172157
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
