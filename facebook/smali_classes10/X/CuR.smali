.class public final LX/CuR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/67q;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/Cu2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Cu2;)V
    .locals 1

    .prologue
    .line 1946646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1946647
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/CuR;->a:Ljava/lang/ref/WeakReference;

    .line 1946648
    return-void
.end method


# virtual methods
.method public final a(LX/68u;)V
    .locals 8

    .prologue
    .line 1946649
    iget-object v0, p0, LX/CuR;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cu2;

    .line 1946650
    if-eqz v0, :cond_0

    .line 1946651
    iget v1, p1, LX/68u;->C:F

    move v1, v1

    .line 1946652
    iget v2, v0, LX/Cu2;->f:I

    int-to-double v2, v2

    float-to-double v4, v1

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    .line 1946653
    iget-object v3, v0, LX/Cu2;->c:Landroid/widget/ImageView;

    int-to-float v2, v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 1946654
    :cond_0
    return-void
.end method
