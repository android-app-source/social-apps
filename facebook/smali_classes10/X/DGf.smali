.class public LX/DGf;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DGf",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980308
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1980309
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DGf;->b:LX/0Zi;

    .line 1980310
    iput-object p1, p0, LX/DGf;->a:LX/0Ot;

    .line 1980311
    return-void
.end method

.method public static a(LX/0QB;)LX/DGf;
    .locals 4

    .prologue
    .line 1980312
    const-class v1, LX/DGf;

    monitor-enter v1

    .line 1980313
    :try_start_0
    sget-object v0, LX/DGf;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980314
    sput-object v2, LX/DGf;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980315
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980316
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980317
    new-instance v3, LX/DGf;

    const/16 p0, 0x21b0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DGf;-><init>(LX/0Ot;)V

    .line 1980318
    move-object v0, v3

    .line 1980319
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980320
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DGf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980321
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980322
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1980323
    check-cast p2, LX/DGe;

    .line 1980324
    iget-object v0, p0, LX/DGf;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;

    iget-object v1, p2, LX/DGe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/DGe;->b:LX/1Pk;

    iget-object v3, p2, LX/DGe;->c:LX/1dl;

    .line 1980325
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x2

    .line 1980326
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x1

    const p0, 0x7f0b08fe

    invoke-interface {v5, v6, p0}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v5

    move-object v5, v5

    .line 1980327
    iget-object v6, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1980328
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStorySet;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1980329
    iget-object v6, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1980330
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStorySet;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v6, 0x1

    :goto_0
    move v6, v6

    .line 1980331
    if-nez v6, :cond_0

    .line 1980332
    const/4 v6, 0x0

    .line 1980333
    :goto_1
    move-object v6, v6

    .line 1980334
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    const/4 p2, 0x1

    .line 1980335
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    .line 1980336
    iget-object v6, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1980337
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStorySet;->I()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const p0, 0x7f0b0050

    invoke-virtual {v6, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const p0, 0x1010036

    invoke-virtual {v6, p0}, LX/1ne;->o(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v6

    const p0, 0x7f0a044e

    invoke-virtual {v6, p0}, LX/1ne;->v(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/1ne;->d(Z)LX/1ne;

    move-result-object v6

    move-object v6, v6

    .line 1980338
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 1980339
    invoke-interface {v2}, LX/1Pk;->e()LX/1SX;

    move-result-object v5

    .line 1980340
    iget-object v6, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;->d:LX/1vb;

    invoke-virtual {v6, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v5

    move-object v5, v5

    .line 1980341
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1980342
    return-object v0

    .line 1980343
    :cond_0
    iget-object v6, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1980344
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStorySet;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1980345
    iget-object p0, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;->c:LX/1nu;

    invoke-virtual {p0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v6

    const p0, 0x7f0208fd

    invoke-virtual {v6, p0}, LX/1nw;->h(I)LX/1nw;

    move-result-object v6

    sget-object p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, p0}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    sget-object p0, LX/1Up;->a:LX/1Up;

    invoke-virtual {v6, p0}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const p0, 0x7f0b10be

    invoke-interface {v6, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const p0, 0x7f0b10be

    invoke-interface {v6, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const/4 p0, 0x5

    const p2, 0x7f0b0901

    invoke-interface {v6, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    goto/16 :goto_1

    :cond_1
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1980346
    invoke-static {}, LX/1dS;->b()V

    .line 1980347
    const/4 v0, 0x0

    return-object v0
.end method
