.class public LX/Doz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DoS;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field private final a:LX/Dox;

.field public final b:LX/Ecl;

.field private final c:LX/Ecm;

.field public final d:LX/Ecn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042945
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Doz;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Dox;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042895
    new-instance v0, LX/Ecl;

    invoke-direct {v0}, LX/Ecl;-><init>()V

    iput-object v0, p0, LX/Doz;->b:LX/Ecl;

    .line 2042896
    new-instance v0, LX/Ecm;

    invoke-direct {v0}, LX/Ecm;-><init>()V

    iput-object v0, p0, LX/Doz;->c:LX/Ecm;

    .line 2042897
    new-instance v0, LX/Ecn;

    invoke-direct {v0}, LX/Ecn;-><init>()V

    iput-object v0, p0, LX/Doz;->d:LX/Ecn;

    .line 2042898
    iput-object p1, p0, LX/Doz;->a:LX/Dox;

    .line 2042899
    return-void
.end method

.method public static a(LX/0QB;)LX/Doz;
    .locals 8

    .prologue
    .line 2042913
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2042914
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2042915
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2042916
    if-nez v1, :cond_0

    .line 2042917
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2042918
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2042919
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2042920
    sget-object v1, LX/Doz;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2042921
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2042922
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2042923
    :cond_1
    if-nez v1, :cond_4

    .line 2042924
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2042925
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2042926
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2042927
    new-instance v7, LX/Doz;

    .line 2042928
    new-instance p0, LX/Dox;

    invoke-static {v0}, LX/Dp2;->a(LX/0QB;)LX/Dp2;

    move-result-object v1

    check-cast v1, LX/Dp2;

    invoke-direct {p0, v1}, LX/Dox;-><init>(LX/Dp2;)V

    .line 2042929
    move-object v1, p0

    .line 2042930
    check-cast v1, LX/Dox;

    invoke-direct {v7, v1}, LX/Doz;-><init>(LX/Dox;)V

    .line 2042931
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2042932
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2042933
    if-nez v1, :cond_2

    .line 2042934
    sget-object v0, LX/Doz;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doz;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2042935
    :goto_1
    if-eqz v0, :cond_3

    .line 2042936
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042937
    :goto_3
    check-cast v0, LX/Doz;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2042938
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2042939
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2042940
    :catchall_1
    move-exception v0

    .line 2042941
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042942
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2042943
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2042944
    :cond_2
    :try_start_8
    sget-object v0, LX/Doz;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doz;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()LX/Eaf;
    .locals 1

    .prologue
    .line 2042912
    iget-object v0, p0, LX/Doz;->a:LX/Dox;

    invoke-virtual {v0}, LX/Dox;->a()LX/Eaf;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/Ebg;
    .locals 1

    .prologue
    .line 2042911
    iget-object v0, p0, LX/Doz;->b:LX/Ecl;

    invoke-virtual {v0, p1}, LX/Ecl;->a(I)LX/Ebg;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Eap;)LX/Ebh;
    .locals 1

    .prologue
    .line 2042910
    iget-object v0, p0, LX/Doz;->c:LX/Ecm;

    invoke-virtual {v0, p1}, LX/Ecm;->a(LX/Eap;)LX/Ebh;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Eap;LX/Eae;)V
    .locals 1

    .prologue
    .line 2042908
    iget-object v0, p0, LX/Doz;->a:LX/Dox;

    invoke-virtual {v0, p1, p2}, LX/Dox;->a(LX/Eap;LX/Eae;)V

    .line 2042909
    return-void
.end method

.method public final a(LX/Eap;LX/Ebh;)V
    .locals 1

    .prologue
    .line 2042906
    iget-object v0, p0, LX/Doz;->c:LX/Ecm;

    invoke-virtual {v0, p1, p2}, LX/Ecm;->a(LX/Eap;LX/Ebh;)V

    .line 2042907
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2042905
    iget-object v0, p0, LX/Doz;->a:LX/Dox;

    invoke-virtual {v0}, LX/Dox;->b()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2042903
    iget-object v0, p0, LX/Doz;->b:LX/Ecl;

    invoke-virtual {v0, p1}, LX/Ecl;->b(I)V

    .line 2042904
    return-void
.end method

.method public final b(LX/Eap;)Z
    .locals 1

    .prologue
    .line 2042902
    iget-object v0, p0, LX/Doz;->c:LX/Ecm;

    invoke-virtual {v0, p1}, LX/Ecm;->b(LX/Eap;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/Eap;LX/Eae;)Z
    .locals 1

    .prologue
    .line 2042901
    iget-object v0, p0, LX/Doz;->a:LX/Dox;

    invoke-virtual {v0, p1, p2}, LX/Dox;->b(LX/Eap;LX/Eae;)Z

    move-result v0

    return v0
.end method

.method public final c(I)LX/Ebk;
    .locals 1

    .prologue
    .line 2042900
    iget-object v0, p0, LX/Doz;->d:LX/Ecn;

    invoke-virtual {v0, p1}, LX/Ecn;->c(I)LX/Ebk;

    move-result-object v0

    return-object v0
.end method
