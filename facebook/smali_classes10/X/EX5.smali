.class public final LX/EX5;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EX4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EX5;",
        ">;",
        "LX/EX4;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:Ljava/lang/Object;

.field public c:I

.field public d:LX/EXA;

.field public e:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/EXA;",
            "LX/EX9;",
            "LX/EX8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2132362
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2132363
    const-string v0, ""

    iput-object v0, p0, LX/EX5;->b:Ljava/lang/Object;

    .line 2132364
    sget-object v0, LX/EXA;->c:LX/EXA;

    move-object v0, v0

    .line 2132365
    iput-object v0, p0, LX/EX5;->d:LX/EXA;

    .line 2132366
    invoke-direct {p0}, LX/EX5;->m()V

    .line 2132367
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2132368
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2132369
    const-string v0, ""

    iput-object v0, p0, LX/EX5;->b:Ljava/lang/Object;

    .line 2132370
    sget-object v0, LX/EXA;->c:LX/EXA;

    move-object v0, v0

    .line 2132371
    iput-object v0, p0, LX/EX5;->d:LX/EXA;

    .line 2132372
    invoke-direct {p0}, LX/EX5;->m()V

    .line 2132373
    return-void
.end method

.method private d(LX/EWY;)LX/EX5;
    .locals 1

    .prologue
    .line 2132374
    instance-of v0, p1, LX/EX6;

    if-eqz v0, :cond_0

    .line 2132375
    check-cast p1, LX/EX6;

    invoke-virtual {p0, p1}, LX/EX5;->a(LX/EX6;)LX/EX5;

    move-result-object p0

    .line 2132376
    :goto_0
    return-object p0

    .line 2132377
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EX5;
    .locals 4

    .prologue
    .line 2132378
    const/4 v2, 0x0

    .line 2132379
    :try_start_0
    sget-object v0, LX/EX6;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EX6;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2132380
    if-eqz v0, :cond_0

    .line 2132381
    invoke-virtual {p0, v0}, LX/EX5;->a(LX/EX6;)LX/EX5;

    .line 2132382
    :cond_0
    return-object p0

    .line 2132383
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2132384
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2132385
    check-cast v0, LX/EX6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2132386
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2132387
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2132388
    invoke-virtual {p0, v1}, LX/EX5;->a(LX/EX6;)LX/EX5;

    :cond_1
    throw v0

    .line 2132389
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 2132390
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2132391
    iget-object v0, p0, LX/EX5;->e:LX/EZ7;

    if-nez v0, :cond_0

    .line 2132392
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/EX5;->d:LX/EXA;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2132393
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2132394
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/EX5;->e:LX/EZ7;

    .line 2132395
    const/4 v0, 0x0

    iput-object v0, p0, LX/EX5;->d:LX/EXA;

    .line 2132396
    :cond_0
    return-void
.end method

.method public static n()LX/EX5;
    .locals 1

    .prologue
    .line 2132397
    new-instance v0, LX/EX5;

    invoke-direct {v0}, LX/EX5;-><init>()V

    return-object v0
.end method

.method private u()LX/EX5;
    .locals 2

    .prologue
    .line 2132398
    invoke-static {}, LX/EX5;->n()LX/EX5;

    move-result-object v0

    invoke-direct {p0}, LX/EX5;->y()LX/EX6;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EX5;->a(LX/EX6;)LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method private x()LX/EX6;
    .locals 2

    .prologue
    .line 2132399
    invoke-direct {p0}, LX/EX5;->y()LX/EX6;

    move-result-object v0

    .line 2132400
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2132401
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2132402
    :cond_0
    return-object v0
.end method

.method private y()LX/EX6;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2132403
    new-instance v2, LX/EX6;

    invoke-direct {v2, p0}, LX/EX6;-><init>(LX/EWj;)V

    .line 2132404
    iget v3, p0, LX/EX5;->a:I

    .line 2132405
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 2132406
    :goto_0
    iget-object v1, p0, LX/EX5;->b:Ljava/lang/Object;

    .line 2132407
    iput-object v1, v2, LX/EX6;->name_:Ljava/lang/Object;

    .line 2132408
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2132409
    or-int/lit8 v0, v0, 0x2

    .line 2132410
    :cond_0
    iget v1, p0, LX/EX5;->c:I

    .line 2132411
    iput v1, v2, LX/EX6;->number_:I

    .line 2132412
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 2132413
    or-int/lit8 v0, v0, 0x4

    move v1, v0

    .line 2132414
    :goto_1
    iget-object v0, p0, LX/EX5;->e:LX/EZ7;

    if-nez v0, :cond_1

    .line 2132415
    iget-object v0, p0, LX/EX5;->d:LX/EXA;

    .line 2132416
    iput-object v0, v2, LX/EX6;->options_:LX/EXA;

    .line 2132417
    :goto_2
    iput v1, v2, LX/EX6;->bitField0_:I

    .line 2132418
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2132419
    return-object v2

    .line 2132420
    :cond_1
    iget-object v0, p0, LX/EX5;->e:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EXA;

    .line 2132421
    iput-object v0, v2, LX/EX6;->options_:LX/EXA;

    .line 2132422
    goto :goto_2

    :cond_2
    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2132423
    invoke-direct {p0, p1}, LX/EX5;->d(LX/EWY;)LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2132424
    invoke-direct {p0, p1, p2}, LX/EX5;->d(LX/EWd;LX/EYZ;)LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EX6;)LX/EX5;
    .locals 3

    .prologue
    .line 2132425
    sget-object v0, LX/EX6;->c:LX/EX6;

    move-object v0, v0

    .line 2132426
    if-ne p1, v0, :cond_0

    .line 2132427
    :goto_0
    return-object p0

    .line 2132428
    :cond_0
    const/4 v0, 0x1

    .line 2132429
    iget v1, p1, LX/EX6;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_1
    move v0, v0

    .line 2132430
    if-eqz v0, :cond_1

    .line 2132431
    iget v0, p0, LX/EX5;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EX5;->a:I

    .line 2132432
    iget-object v0, p1, LX/EX6;->name_:Ljava/lang/Object;

    iput-object v0, p0, LX/EX5;->b:Ljava/lang/Object;

    .line 2132433
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2132434
    :cond_1
    iget v0, p1, LX/EX6;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2132435
    if-eqz v0, :cond_2

    .line 2132436
    iget v0, p1, LX/EX6;->number_:I

    move v0, v0

    .line 2132437
    iget v1, p0, LX/EX5;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, LX/EX5;->a:I

    .line 2132438
    iput v0, p0, LX/EX5;->c:I

    .line 2132439
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2132440
    :cond_2
    invoke-virtual {p1}, LX/EX6;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2132441
    iget-object v0, p1, LX/EX6;->options_:LX/EXA;

    move-object v0, v0

    .line 2132442
    iget-object v1, p0, LX/EX5;->e:LX/EZ7;

    if-nez v1, :cond_7

    .line 2132443
    iget v1, p0, LX/EX5;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_6

    iget-object v1, p0, LX/EX5;->d:LX/EXA;

    .line 2132444
    sget-object v2, LX/EXA;->c:LX/EXA;

    move-object v2, v2

    .line 2132445
    if-eq v1, v2, :cond_6

    .line 2132446
    iget-object v1, p0, LX/EX5;->d:LX/EXA;

    invoke-static {v1}, LX/EXA;->a(LX/EXA;)LX/EX9;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/EX9;->a(LX/EXA;)LX/EX9;

    move-result-object v1

    invoke-virtual {v1}, LX/EX9;->l()LX/EXA;

    move-result-object v1

    iput-object v1, p0, LX/EX5;->d:LX/EXA;

    .line 2132447
    :goto_3
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2132448
    :goto_4
    iget v1, p0, LX/EX5;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, LX/EX5;->a:I

    .line 2132449
    :cond_3
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 2132450
    :cond_6
    iput-object v0, p0, LX/EX5;->d:LX/EXA;

    goto :goto_3

    .line 2132451
    :cond_7
    iget-object v1, p0, LX/EX5;->e:LX/EZ7;

    invoke-virtual {v1, v0}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto :goto_4
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2132353
    iget v0, p0, LX/EX5;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2132354
    if-eqz v0, :cond_0

    .line 2132355
    iget-object v0, p0, LX/EX5;->e:LX/EZ7;

    if-nez v0, :cond_2

    .line 2132356
    iget-object v0, p0, LX/EX5;->d:LX/EXA;

    .line 2132357
    :goto_1
    move-object v0, v0

    .line 2132358
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2132359
    const/4 v0, 0x0

    .line 2132360
    :goto_2
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/EX5;->e:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->c()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EXA;

    goto :goto_1
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2132361
    invoke-direct {p0, p1, p2}, LX/EX5;->d(LX/EWd;LX/EYZ;)LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2132342
    invoke-direct {p0}, LX/EX5;->u()LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2132339
    invoke-direct {p0, p1, p2}, LX/EX5;->d(LX/EWd;LX/EYZ;)LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2132340
    invoke-direct {p0}, LX/EX5;->u()LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2132341
    invoke-direct {p0, p1}, LX/EX5;->d(LX/EWY;)LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2132352
    invoke-direct {p0}, LX/EX5;->u()LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2132343
    sget-object v0, LX/EYC;->n:LX/EYn;

    const-class v1, LX/EX6;

    const-class v2, LX/EX5;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2132344
    sget-object v0, LX/EYC;->m:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2132345
    invoke-direct {p0}, LX/EX5;->u()LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2132346
    invoke-direct {p0}, LX/EX5;->y()LX/EX6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2132347
    invoke-direct {p0}, LX/EX5;->x()LX/EX6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2132348
    invoke-direct {p0}, LX/EX5;->y()LX/EX6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2132349
    invoke-direct {p0}, LX/EX5;->x()LX/EX6;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2132350
    sget-object v0, LX/EX6;->c:LX/EX6;

    move-object v0, v0

    .line 2132351
    return-object v0
.end method
