.class public final LX/EfK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/EfL;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/EfL;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2154133
    iput-object p1, p0, LX/EfK;->a:LX/EfL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2154134
    iput-object p2, p0, LX/EfK;->b:Ljava/lang/String;

    .line 2154135
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/audience/model/ReplyThread;)V
    .locals 9

    .prologue
    .line 2154136
    if-eqz p1, :cond_0

    .line 2154137
    iget-object v0, p0, LX/EfK;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->setLoading(Z)V

    .line 2154138
    :cond_0
    iget-object v0, p0, LX/EfK;->a:LX/EfL;

    iget-boolean v0, v0, LX/EfL;->G:Z

    if-eqz v0, :cond_1

    .line 2154139
    :goto_0
    return-void

    .line 2154140
    :cond_1
    iget-object v0, p0, LX/EfK;->a:LX/EfL;

    const/4 v1, 0x0

    .line 2154141
    iget-object v2, v0, LX/EfL;->H:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2154142
    iget-boolean v3, v2, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    move v2, v3

    .line 2154143
    if-eqz v2, :cond_2

    if-eqz p1, :cond_2

    iget-object v2, v0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    if-eqz v2, :cond_5

    .line 2154144
    :cond_2
    :goto_1
    move v0, v1

    .line 2154145
    if-eqz v0, :cond_3

    .line 2154146
    iget-object v0, p0, LX/EfK;->a:LX/EfL;

    const/4 v4, 0x1

    .line 2154147
    iput-boolean v4, v0, LX/EfL;->G:Z

    .line 2154148
    new-instance v2, LX/31Y;

    invoke-virtual {v0}, LX/EfL;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v2, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2154149
    iget-object v1, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v1, v1

    .line 2154150
    iget-object v3, v1, Lcom/facebook/audience/model/Reply;->k:Landroid/net/Uri;

    move-object v1, v3

    .line 2154151
    if-eqz v1, :cond_7

    const v1, 0x7f082aa5

    .line 2154152
    :goto_2
    iget-object v3, v0, LX/EfL;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2154153
    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2154154
    iget-object p0, p1, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object p0, p0

    .line 2154155
    invoke-virtual {p0}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/7hK;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v3, v4

    invoke-static {v1, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2154156
    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2154157
    const v1, 0x7f082aa6

    new-instance v3, LX/EfI;

    invoke-direct {v3, v0}, LX/EfI;-><init>(LX/EfL;)V

    invoke-virtual {v2, v1, v3}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2154158
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2154159
    goto :goto_0

    .line 2154160
    :cond_3
    iget-object v0, p0, LX/EfK;->a:LX/EfL;

    .line 2154161
    iget-object v1, v0, LX/EfL;->H:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2154162
    iget-boolean v2, v1, Lcom/facebook/audience/model/ReplyThreadConfig;->a:Z

    move v1, v2

    .line 2154163
    if-eqz v1, :cond_4

    iget-object v1, v0, LX/EfL;->m:LX/0fO;

    invoke-virtual {v1}, LX/0fO;->p()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2154164
    iget-boolean v1, p1, Lcom/facebook/audience/model/ReplyThread;->c:Z

    move v1, v1

    .line 2154165
    if-eqz v1, :cond_8

    .line 2154166
    iget-object v1, v0, LX/EfL;->H:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2154167
    new-instance v2, LX/7gu;

    invoke-direct {v2, v1}, LX/7gu;-><init>(Lcom/facebook/audience/model/ReplyThreadConfig;)V

    move-object v1, v2

    .line 2154168
    iget-boolean v2, p1, Lcom/facebook/audience/model/ReplyThread;->b:Z

    move v2, v2

    .line 2154169
    iput-boolean v2, v1, LX/7gu;->b:Z

    .line 2154170
    move-object v1, v1

    .line 2154171
    invoke-virtual {v1}, LX/7gu;->a()Lcom/facebook/audience/model/ReplyThreadConfig;

    move-result-object v1

    iput-object v1, v0, LX/EfL;->H:Lcom/facebook/audience/model/ReplyThreadConfig;

    .line 2154172
    :cond_4
    :goto_3
    iget-object v0, p0, LX/EfK;->a:LX/EfL;

    .line 2154173
    iput-object p1, v0, LX/EfL;->z:Lcom/facebook/audience/model/ReplyThread;

    .line 2154174
    iget-object v2, v0, LX/EfL;->t:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    .line 2154175
    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v3, v3

    .line 2154176
    new-instance v4, LX/EfJ;

    invoke-direct {v4, v3}, LX/EfJ;-><init>(Lcom/facebook/audience/model/Reply;)V

    move-object v3, v4

    .line 2154177
    invoke-virtual {v2, v3}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->a(LX/AKy;)V

    .line 2154178
    const/4 v5, 0x0

    .line 2154179
    iget-object v2, v0, LX/EfL;->x:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2154180
    iget-object v2, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v2, v2

    .line 2154181
    iget-boolean v3, v2, Lcom/facebook/audience/model/Reply;->c:Z

    move v2, v3

    .line 2154182
    if-eqz v2, :cond_a

    .line 2154183
    iget-object v2, p1, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v2, v2

    .line 2154184
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2154185
    iget-object v2, v0, LX/EfL;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0827ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2154186
    :goto_4
    iget-object v3, v0, LX/EfL;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2154187
    iget-object v2, v0, LX/EfL;->q:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    .line 2154188
    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v3, v3

    .line 2154189
    iget-object v4, v3, Lcom/facebook/audience/model/Reply;->e:Ljava/lang/String;

    move-object v3, v4

    .line 2154190
    iget-object v4, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v4, v4

    .line 2154191
    iget-wide v7, v4, Lcom/facebook/audience/model/Reply;->i:J

    move-wide v4, v7

    .line 2154192
    iget-object v6, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v6, v6

    .line 2154193
    iget-object v7, v6, Lcom/facebook/audience/model/Reply;->f:Landroid/net/Uri;

    move-object v6, v7

    .line 2154194
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->a(Ljava/lang/String;JLandroid/net/Uri;)V

    .line 2154195
    iget-object v2, v0, LX/EfL;->e:Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;

    .line 2154196
    iput-object p1, v2, Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;->d:Lcom/facebook/audience/model/ReplyThread;

    .line 2154197
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2154198
    invoke-virtual {v0}, LX/EfL;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0827aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2154199
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2154200
    iget-object v5, p1, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v5, v5

    .line 2154201
    invoke-virtual {v5}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/7hK;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2154202
    iget-object v3, v0, LX/EfL;->v:Lcom/facebook/audience/ui/SnacksReplyFooterBar;

    invoke-virtual {v3, v2}, Lcom/facebook/audience/ui/SnacksReplyFooterBar;->setReplyEditTextHint(Ljava/lang/String;)V

    .line 2154203
    iget-object v2, v0, LX/EfL;->s:LX/1P1;

    iget-object v3, v0, LX/EfL;->e:Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, LX/1OR;->e(I)V

    .line 2154204
    iget-boolean v2, v0, LX/EfL;->D:Z

    if-eqz v2, :cond_9

    .line 2154205
    invoke-static {v0}, LX/EfL;->f(LX/EfL;)V

    .line 2154206
    :goto_5
    goto/16 :goto_0

    .line 2154207
    :cond_5
    iget-object v2, v0, LX/EfL;->m:LX/0fO;

    invoke-virtual {v2}, LX/0fO;->p()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2154208
    iget-boolean v2, p1, Lcom/facebook/audience/model/ReplyThread;->c:Z

    move v2, v2

    .line 2154209
    if-eqz v2, :cond_2

    .line 2154210
    iget-boolean v2, p1, Lcom/facebook/audience/model/ReplyThread;->b:Z

    move v2, v2

    .line 2154211
    if-nez v2, :cond_2

    const/4 v1, 0x1

    goto/16 :goto_1

    .line 2154212
    :cond_6
    iget-boolean v1, p1, Lcom/facebook/audience/model/ReplyThread;->c:Z

    move v1, v1

    .line 2154213
    goto/16 :goto_1

    .line 2154214
    :cond_7
    const v1, 0x7f082aa4

    goto/16 :goto_2

    .line 2154215
    :cond_8
    iget-object v1, v0, LX/EfL;->i:LX/AFJ;

    .line 2154216
    iget-object v2, p1, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v2, v2

    .line 2154217
    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2154218
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/AFJ;->a(Lcom/facebook/audience/model/AudienceControlData;LX/0Px;)J

    goto/16 :goto_3

    .line 2154219
    :cond_9
    iget-object v2, v0, LX/EfL;->j:Lcom/facebook/audience/util/PrefetchUtils;

    .line 2154220
    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v3, v3

    .line 2154221
    iget-object v4, v3, Lcom/facebook/audience/model/Reply;->b:Landroid/net/Uri;

    move-object v3, v4

    .line 2154222
    invoke-virtual {v2, v3}, Lcom/facebook/audience/util/PrefetchUtils;->a(Landroid/net/Uri;)V

    .line 2154223
    iget-object v2, v0, LX/EfL;->j:Lcom/facebook/audience/util/PrefetchUtils;

    .line 2154224
    iget-object v3, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v3, v3

    .line 2154225
    iget-object v4, v3, Lcom/facebook/audience/model/Reply;->k:Landroid/net/Uri;

    move-object v3, v4

    .line 2154226
    invoke-virtual {v2, v3}, Lcom/facebook/audience/util/PrefetchUtils;->b(Landroid/net/Uri;)V

    goto :goto_5

    .line 2154227
    :cond_a
    iget-object v2, v0, LX/EfL;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0827ac

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2154228
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 2154229
    iget-object v4, p1, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v4, v4

    .line 2154230
    invoke-virtual {v4}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/7hK;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4
.end method
