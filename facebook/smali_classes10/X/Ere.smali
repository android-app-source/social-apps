.class public LX/Ere;
.super LX/3Tf;
.source ""


# instance fields
.field private c:LX/8RE;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/invite/EventInviteeToken;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/621",
            "<",
            "Lcom/facebook/events/invite/EventInviteeToken;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2173239
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 2173240
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ere;->d:Ljava/util/List;

    .line 2173241
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ere;->e:Ljava/util/List;

    .line 2173242
    new-instance v0, LX/Erc;

    invoke-direct {v0}, LX/Erc;-><init>()V

    iput-object v0, p0, LX/Ere;->c:LX/8RE;

    .line 2173243
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2173238
    sget-object v0, LX/Erd;->HEADER:LX/Erd;

    invoke-virtual {v0}, LX/Erd;->ordinal()I

    move-result v0

    return v0
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2173232
    if-nez p4, :cond_0

    .line 2173233
    iget-object v0, p0, LX/Ere;->c:LX/8RE;

    invoke-interface {v0, p5}, LX/8RE;->b(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 2173234
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/Ere;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 2173235
    if-eqz v0, :cond_1

    .line 2173236
    iget-object v1, p0, LX/Ere;->c:LX/8RE;

    iget-object v2, p0, LX/Ere;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-interface {v1, p4, v0, v2}, LX/8RE;->b(Landroid/view/View;LX/8QL;Z)V

    .line 2173237
    :cond_1
    return-object p4
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2173231
    new-instance v0, Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2173228
    iget-object v0, p0, LX/Ere;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 2173229
    iget-object v0, p0, LX/Ere;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2173230
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2173227
    iget-object v0, p0, LX/Ere;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 2173226
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2173225
    iget-object v0, p0, LX/Ere;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 2173224
    iget-object v0, p0, LX/Ere;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(II)I
    .locals 1

    .prologue
    .line 2173222
    sget-object v0, LX/Erd;->SUBTITLED_ITEM:LX/Erd;

    invoke-virtual {v0}, LX/Erd;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2173223
    invoke-static {}, LX/Erd;->values()[LX/Erd;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
