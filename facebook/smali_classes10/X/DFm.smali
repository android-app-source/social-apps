.class public LX/DFm;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Z

.field public c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1978762
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DFm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1978763
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1978751
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1978752
    const-class v0, LX/DFm;

    invoke-static {v0, p0}, LX/DFm;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1978753
    invoke-virtual {p0}, LX/DFm;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/DFm;->c:Landroid/content/res/Resources;

    .line 1978754
    const v0, 0x7f03063f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1978755
    const v0, 0x7f0d1142

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    .line 1978756
    const v1, 0x7f08186d

    .line 1978757
    iget-object v2, p0, LX/DFm;->a:LX/23P;

    iget-object p1, p0, LX/DFm;->c:Landroid/content/res/Resources;

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {v2, p1, p2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    move-object v1, v2

    .line 1978758
    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 1978759
    iget-object v1, p0, LX/DFm;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setTextColor(I)V

    .line 1978760
    const v1, 0x7f020b08

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setBackgroundResource(I)V

    .line 1978761
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/DFm;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object p0

    check-cast p0, LX/23P;

    iput-object p0, p1, LX/DFm;->a:LX/23P;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1978750
    iget-boolean v0, p0, LX/DFm;->b:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x31bbcf44

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978746
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1978747
    const/4 v1, 0x1

    .line 1978748
    iput-boolean v1, p0, LX/DFm;->b:Z

    .line 1978749
    const/16 v1, 0x2d

    const v2, 0xf750944

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x530ab067

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978742
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1978743
    const/4 v1, 0x0

    .line 1978744
    iput-boolean v1, p0, LX/DFm;->b:Z

    .line 1978745
    const/16 v1, 0x2d

    const v2, -0x688e00e0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
