.class public LX/E56;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E54;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E56;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E57;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2077874
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E56;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E57;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077904
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2077905
    iput-object p1, p0, LX/E56;->b:LX/0Ot;

    .line 2077906
    return-void
.end method

.method public static a(LX/0QB;)LX/E56;
    .locals 4

    .prologue
    .line 2077891
    sget-object v0, LX/E56;->c:LX/E56;

    if-nez v0, :cond_1

    .line 2077892
    const-class v1, LX/E56;

    monitor-enter v1

    .line 2077893
    :try_start_0
    sget-object v0, LX/E56;->c:LX/E56;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077894
    if-eqz v2, :cond_0

    .line 2077895
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077896
    new-instance v3, LX/E56;

    const/16 p0, 0x311d

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E56;-><init>(LX/0Ot;)V

    .line 2077897
    move-object v0, v3

    .line 2077898
    sput-object v0, LX/E56;->c:LX/E56;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077899
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077900
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077901
    :cond_1
    sget-object v0, LX/E56;->c:LX/E56;

    return-object v0

    .line 2077902
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077903
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2077907
    const v0, 0x4ce23ab4

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 2077885
    check-cast p2, LX/E55;

    .line 2077886
    iget-object v0, p0, LX/E56;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/E55;->a:LX/1X1;

    .line 2077887
    invoke-static {p1, v0}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object p0

    .line 2077888
    const p2, 0x4ce23ab4

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 2077889
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 2077890
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2077875
    invoke-static {}, LX/1dS;->b()V

    .line 2077876
    iget v0, p1, LX/1dQ;->b:I

    .line 2077877
    packed-switch v0, :pswitch_data_0

    .line 2077878
    :goto_0
    return-object v2

    .line 2077879
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2077880
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2077881
    check-cast v1, LX/E55;

    .line 2077882
    iget-object p1, p0, LX/E56;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/E57;

    iget-object p2, v1, LX/E55;->b:Landroid/view/View$OnClickListener;

    .line 2077883
    invoke-interface {p2, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2077884
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4ce23ab4
        :pswitch_0
    .end packed-switch
.end method
