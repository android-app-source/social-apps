.class public final LX/ExS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Landroid/util/SparseArray",
        "<",
        "LX/Euu;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V
    .locals 0

    .prologue
    .line 2184741
    iput-object p1, p0, LX/ExS;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2184742
    iget-object v0, p0, LX/ExS;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->f:LX/Eui;

    sget-object v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2184743
    const-string v5, "You must provide a caller context"

    invoke-static {v1, v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2184744
    new-instance v5, LX/Ev0;

    invoke-direct {v5}, LX/Ev0;-><init>()V

    move-object v5, v5

    .line 2184745
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    .line 2184746
    sget-object v6, LX/0zS;->a:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    const-wide/16 v7, 0x258

    invoke-virtual {v6, v7, v8}, LX/0zO;->a(J)LX/0zO;

    move-result-object v6

    const-string v7, "FC_CONTEXTUAL_PYMK_ATTRIBUTES"

    invoke-static {v7}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v7

    .line 2184747
    iput-object v7, v6, LX/0zO;->d:Ljava/util/Set;

    .line 2184748
    move-object v6, v6

    .line 2184749
    iput-object v1, v6, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2184750
    move-object v2, v5

    .line 2184751
    iget-object v3, v0, LX/Eui;->c:LX/2dl;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/2dl;->a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2184752
    new-instance v3, LX/Euh;

    invoke-direct {v3, v0}, LX/Euh;-><init>(LX/Eui;)V

    iget-object v4, v0, LX/Eui;->b:LX/0TD;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2184753
    return-object v0
.end method
