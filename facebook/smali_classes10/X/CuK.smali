.class public LX/CuK;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/facebook/richdocument/view/widget/media/plugins/AudioPlugin$AudioPluginClickListener;"
    }
.end annotation


# instance fields
.field public a:LX/Ctq;


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 2

    .prologue
    .line 1946482
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1946483
    invoke-interface {p1}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_0

    .line 1946484
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expecting RichVideoPlayer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1946485
    :cond_0
    return-void
.end method

.method public static a(LX/CuK;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1946486
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-object v1, v1

    .line 1946487
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/CuK;->a:LX/Ctq;

    if-nez v2, :cond_1

    .line 1946488
    :cond_0
    :goto_0
    return-void

    .line 1946489
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1946490
    iget-object v2, p0, LX/CuK;->a:LX/Ctq;

    invoke-virtual {v2, v0}, LX/Ctq;->a(Z)V

    .line 1946491
    :goto_1
    if-eqz p1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->k()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1946492
    :goto_2
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1946493
    iget-object v3, p0, LX/CuK;->a:LX/Ctq;

    if-nez v0, :cond_6

    const/4 v2, 0x1

    :goto_3
    const/4 v5, 0x1

    const/4 p1, 0x0

    .line 1946494
    iget-boolean v0, v3, LX/Ctq;->n:Z

    if-nez v2, :cond_7

    move v4, v5

    :goto_4
    if-ne v0, v4, :cond_8

    .line 1946495
    :goto_5
    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1946496
    iget-object v2, p0, LX/CuK;->a:LX/Ctq;

    invoke-virtual {v2}, LX/Ctq;->g()V

    .line 1946497
    :cond_2
    goto :goto_0

    .line 1946498
    :cond_3
    iget-object v2, p0, LX/CuK;->a:LX/Ctq;

    invoke-virtual {v2}, LX/Ctq;->h()V

    goto :goto_1

    .line 1946499
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->k()Z

    move-result v0

    goto :goto_2

    .line 1946500
    :cond_6
    const/4 v2, 0x0

    goto :goto_3

    :cond_7
    move v4, p1

    .line 1946501
    goto :goto_4

    .line 1946502
    :cond_8
    iget-object v0, v3, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    if-eqz v2, :cond_9

    const v4, 0x7f0200dd

    :goto_6
    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbImageButton;->setImageResource(I)V

    .line 1946503
    if-nez v2, :cond_a

    move v4, v5

    :goto_7
    iput-boolean v4, v3, LX/Ctq;->n:Z

    .line 1946504
    iput-boolean p1, v3, LX/Ctq;->e:Z

    goto :goto_5

    .line 1946505
    :cond_9
    const v4, 0x7f0200e5

    goto :goto_6

    :cond_a
    move v4, p1

    .line 1946506
    goto :goto_7
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1946507
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/CuK;->a(LX/CuK;Z)V

    .line 1946508
    return-void
.end method

.method public final b(LX/CrS;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1946509
    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v1

    .line 1946510
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1946511
    if-nez v0, :cond_1

    .line 1946512
    :cond_0
    :goto_0
    return-void

    .line 1946513
    :cond_1
    iget-object v4, p0, LX/CuK;->a:LX/Ctq;

    if-eqz v4, :cond_0

    .line 1946514
    iget-object v4, v1, LX/Cqw;->f:LX/Cqt;

    move-object v4, v4

    .line 1946515
    sget-object p1, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    if-eq v4, p1, :cond_2

    sget-object p1, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    if-ne v4, p1, :cond_7

    :cond_2
    const/4 p1, 0x1

    :goto_1
    move v4, p1

    .line 1946516
    if-nez v4, :cond_3

    sget-object v4, LX/Cqw;->b:LX/Cqw;

    if-ne v1, v4, :cond_5

    :cond_3
    move v1, v3

    .line 1946517
    :goto_2
    iget-object v4, p0, LX/CuK;->a:LX/Ctq;

    .line 1946518
    iput-boolean v1, v4, LX/Ctq;->d:Z

    .line 1946519
    invoke-static {p0, v2}, LX/CuK;->a(LX/CuK;Z)V

    .line 1946520
    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1946521
    :cond_4
    iget-object v0, p0, LX/CuK;->a:LX/Ctq;

    invoke-virtual {v0}, LX/Ctq;->h()V

    goto :goto_0

    :cond_5
    move v1, v2

    .line 1946522
    goto :goto_2

    .line 1946523
    :cond_6
    iget-object v0, p0, LX/CuK;->a:LX/Ctq;

    invoke-virtual {v0, v3}, LX/Ctq;->a(Z)V

    goto :goto_0

    :cond_7
    const/4 p1, 0x0

    goto :goto_1
.end method
