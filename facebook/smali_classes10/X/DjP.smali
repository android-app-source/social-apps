.class public final LX/DjP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DjQ;


# direct methods
.method public constructor <init>(LX/DjQ;)V
    .locals 0

    .prologue
    .line 2033230
    iput-object p1, p0, LX/DjP;->a:LX/DjQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2033231
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2033232
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2033233
    if-eqz p1, :cond_0

    .line 2033234
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2033235
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    .line 2033236
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2033237
    :goto_3
    return-object v0

    .line 2033238
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2033239
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2033240
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2033241
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2033242
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2033243
    const-class v5, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {v4, v0, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    .line 2033244
    :cond_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2033245
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {v4, v0, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2033246
    if-nez v0, :cond_6

    move v0, v1

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    .line 2033247
    :cond_7
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2033248
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {v4, v0, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v5, -0x758acc48

    invoke-static {v4, v0, v1, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2033249
    if-eqz v0, :cond_9

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2033250
    :goto_4
    invoke-interface {v0}, LX/3Sb;->b()LX/2sN;

    move-result-object v0

    :cond_8
    :goto_5
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2033251
    const-class v5, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    invoke-virtual {v4, v1, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 2033252
    const-class v5, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;

    invoke-virtual {v4, v1, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 2033253
    :cond_9
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    .line 2033254
    :cond_a
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_3
.end method
