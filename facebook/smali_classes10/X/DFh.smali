.class public LX/DFh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/2eq;


# direct methods
.method public constructor <init>(LX/2eq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1978655
    iput-object p1, p0, LX/DFh;->a:LX/2eq;

    .line 1978656
    return-void
.end method

.method public static a(LX/0QB;)LX/DFh;
    .locals 4

    .prologue
    .line 1978657
    const-class v1, LX/DFh;

    monitor-enter v1

    .line 1978658
    :try_start_0
    sget-object v0, LX/DFh;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978659
    sput-object v2, LX/DFh;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978660
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978661
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978662
    new-instance p0, LX/DFh;

    invoke-static {v0}, LX/2eq;->a(LX/0QB;)LX/2eq;

    move-result-object v3

    check-cast v3, LX/2eq;

    invoke-direct {p0, v3}, LX/DFh;-><init>(LX/2eq;)V

    .line 1978663
    move-object v0, p0

    .line 1978664
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978665
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978666
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978667
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
