.class public LX/CsV;
.super Landroid/webkit/WebView;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Csq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1942851
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 1942852
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/CsV;->a:Ljava/util/List;

    .line 1942853
    new-instance v0, LX/Cst;

    const/4 p1, 0x0

    invoke-direct {v0, p0, p1}, LX/Cst;-><init>(LX/CsV;Landroid/webkit/WebViewClient;)V

    invoke-virtual {p0, v0}, LX/CsV;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1942854
    return-void
.end method


# virtual methods
.method public final a(LX/Csq;)V
    .locals 1

    .prologue
    .line 1942855
    iget-object v0, p0, LX/CsV;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1942856
    iget-object v0, p0, LX/CsV;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1942857
    :cond_0
    return-void
.end method

.method public final b(LX/Csq;)V
    .locals 1

    .prologue
    .line 1942858
    iget-object v0, p0, LX/CsV;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1942859
    return-void
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 1942860
    iget-object v0, p0, LX/CsV;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1942861
    invoke-super {p0}, Landroid/webkit/WebView;->destroy()V

    .line 1942862
    return-void
.end method

.method public setWebViewClient(Landroid/webkit/WebViewClient;)V
    .locals 1

    .prologue
    .line 1942863
    new-instance v0, LX/Cst;

    invoke-direct {v0, p0, p1}, LX/Cst;-><init>(LX/CsV;Landroid/webkit/WebViewClient;)V

    invoke-super {p0, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1942864
    return-void
.end method
