.class public final enum LX/DcG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DcG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DcG;

.field public static final enum LINK_MENU:LX/DcG;

.field public static final enum NONE:LX/DcG;

.field public static final enum PHOTO_MENU:LX/DcG;

.field public static final enum STRUCTURED_MENU:LX/DcG;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2018125
    new-instance v0, LX/DcG;

    const-string v1, "PHOTO_MENU"

    invoke-direct {v0, v1, v2}, LX/DcG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DcG;->PHOTO_MENU:LX/DcG;

    .line 2018126
    new-instance v0, LX/DcG;

    const-string v1, "STRUCTURED_MENU"

    invoke-direct {v0, v1, v3}, LX/DcG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DcG;->STRUCTURED_MENU:LX/DcG;

    .line 2018127
    new-instance v0, LX/DcG;

    const-string v1, "LINK_MENU"

    invoke-direct {v0, v1, v4}, LX/DcG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DcG;->LINK_MENU:LX/DcG;

    .line 2018128
    new-instance v0, LX/DcG;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, LX/DcG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DcG;->NONE:LX/DcG;

    .line 2018129
    const/4 v0, 0x4

    new-array v0, v0, [LX/DcG;

    sget-object v1, LX/DcG;->PHOTO_MENU:LX/DcG;

    aput-object v1, v0, v2

    sget-object v1, LX/DcG;->STRUCTURED_MENU:LX/DcG;

    aput-object v1, v0, v3

    sget-object v1, LX/DcG;->LINK_MENU:LX/DcG;

    aput-object v1, v0, v4

    sget-object v1, LX/DcG;->NONE:LX/DcG;

    aput-object v1, v0, v5

    sput-object v0, LX/DcG;->$VALUES:[LX/DcG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2018130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DcG;
    .locals 1

    .prologue
    .line 2018131
    const-class v0, LX/DcG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DcG;

    return-object v0
.end method

.method public static values()[LX/DcG;
    .locals 1

    .prologue
    .line 2018132
    sget-object v0, LX/DcG;->$VALUES:[LX/DcG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DcG;

    return-object v0
.end method
