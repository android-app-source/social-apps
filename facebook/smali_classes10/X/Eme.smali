.class public abstract LX/Eme;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2166058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166059
    const/4 v0, 0x0

    iput-object v0, p0, LX/Eme;->a:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 2166057
    iget-object v0, p0, LX/Eme;->a:Ljava/lang/Object;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 2166051
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166052
    iget-object v0, p0, LX/Eme;->a:Ljava/lang/Object;

    if-eq v0, p1, :cond_1

    .line 2166053
    iget-object v0, p0, LX/Eme;->a:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Eme;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0}, LX/Eme;->b(Ljava/lang/Object;)V

    .line 2166054
    :cond_0
    iput-object p1, p0, LX/Eme;->a:Ljava/lang/Object;

    .line 2166055
    invoke-virtual {p0}, LX/Eme;->b()V

    .line 2166056
    :cond_1
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 2166050
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 2166046
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166047
    iget-object v0, p0, LX/Eme;->a:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    .line 2166048
    const/4 v0, 0x0

    iput-object v0, p0, LX/Eme;->a:Ljava/lang/Object;

    .line 2166049
    :cond_0
    return-void
.end method
