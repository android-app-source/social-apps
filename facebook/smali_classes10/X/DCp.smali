.class public final LX/DCp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3mN;


# direct methods
.method public constructor <init>(LX/3mN;)V
    .locals 0

    .prologue
    .line 1974592
    iput-object p1, p0, LX/DCp;->a:LX/3mN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1974593
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1974594
    if-nez p1, :cond_0

    .line 1974595
    const/4 v0, 0x0

    .line 1974596
    :goto_0
    return-object v0

    .line 1974597
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1974598
    check-cast v0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel;

    .line 1974599
    if-nez v0, :cond_1

    .line 1974600
    const/4 p0, 0x0

    .line 1974601
    :goto_1
    move-object v0, p0

    .line 1974602
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->F()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v0

    goto :goto_0

    .line 1974603
    :cond_1
    new-instance p0, LX/4XR;

    invoke-direct {p0}, LX/4XR;-><init>()V

    .line 1974604
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p1

    .line 1974605
    iput-object p1, p0, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1974606
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel;->b()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel;

    move-result-object p1

    invoke-static {p1}, LX/81l;->a(Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel;)Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object p1

    .line 1974607
    iput-object p1, p0, LX/4XR;->G:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    .line 1974608
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel;->c()I

    move-result p1

    .line 1974609
    iput p1, p0, LX/4XR;->ff:I

    .line 1974610
    invoke-virtual {p0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    goto :goto_1
.end method
