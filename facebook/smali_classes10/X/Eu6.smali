.class public LX/Eu6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field private static e:LX/0Xm;


# instance fields
.field public d:LX/23P;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2179040
    const v0, 0x7f0201fe

    sput v0, LX/Eu6;->a:I

    .line 2179041
    const v0, 0x7f0a09da

    sput v0, LX/Eu6;->b:I

    .line 2179042
    const v0, 0x7f0b004e

    sput v0, LX/Eu6;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2179043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179044
    return-void
.end method

.method public static a(LX/0QB;)LX/Eu6;
    .locals 4

    .prologue
    .line 2179045
    const-class v1, LX/Eu6;

    monitor-enter v1

    .line 2179046
    :try_start_0
    sget-object v0, LX/Eu6;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2179047
    sput-object v2, LX/Eu6;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2179048
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2179049
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2179050
    new-instance p0, LX/Eu6;

    invoke-direct {p0}, LX/Eu6;-><init>()V

    .line 2179051
    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    .line 2179052
    iput-object v3, p0, LX/Eu6;->d:LX/23P;

    .line 2179053
    move-object v0, p0

    .line 2179054
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2179055
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Eu6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2179056
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2179057
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/1dc;III)LX/1Dg;
    .locals 6
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p5    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;III)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2179058
    new-instance v0, LX/Eu5;

    move-object v1, p0

    move-object v2, p1

    move v3, p6

    move v4, p8

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/Eu5;-><init>(LX/Eu6;LX/1De;IILjava/lang/CharSequence;)V

    .line 2179059
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p4}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b08a9

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x7

    const v3, 0x7f0b08a8

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    if-nez p5, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/8yw;->c(LX/1De;)LX/8yu;

    move-result-object v2

    invoke-static {p1}, LX/Eu9;->c(LX/1De;)LX/Eu7;

    move-result-object v3

    invoke-virtual {v3, p6}, LX/Eu7;->h(I)LX/Eu7;

    move-result-object v3

    invoke-virtual {v3, p8}, LX/Eu7;->i(I)LX/Eu7;

    move-result-object v3

    iget-object v4, p0, LX/Eu6;->d:LX/23P;

    const/4 v5, 0x0

    invoke-virtual {v4, p2, v5}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Eu7;->a(Ljava/lang/CharSequence;)LX/Eu7;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8yu;->a(LX/1X5;)LX/8yu;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/8yu;->a(LX/0QR;)LX/8yu;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/8yu;->h(I)LX/8yu;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Di;->a(Z)LX/1Di;

    move-result-object v1

    const/4 v3, 0x2

    const v4, 0x7f0b08a3

    invoke-interface {v1, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    goto :goto_0
.end method
