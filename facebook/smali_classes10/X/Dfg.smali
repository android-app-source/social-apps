.class public final LX/Dfg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:LX/Dfh;


# direct methods
.method public constructor <init>(LX/Dfh;)V
    .locals 0

    .prologue
    .line 2027644
    iput-object p1, p0, LX/Dfg;->a:LX/Dfh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 2027645
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 2027646
    iget-object v1, p0, LX/Dfg;->a:LX/Dfh;

    iget-object v1, v1, LX/Dfh;->a:Landroid/view/View;

    iget-object v2, p0, LX/Dfg;->a:LX/Dfh;

    iget-object v2, v2, LX/Dfh;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v0, v3, v0

    mul-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 2027647
    return-void
.end method
