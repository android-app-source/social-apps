.class public LX/EP1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/11S;


# direct methods
.method public constructor <init>(LX/11S;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2115690
    iput-object p1, p0, LX/EP1;->a:LX/11S;

    .line 2115691
    return-void
.end method

.method public static a(LX/0QB;)LX/EP1;
    .locals 4

    .prologue
    .line 2115692
    const-class v1, LX/EP1;

    monitor-enter v1

    .line 2115693
    :try_start_0
    sget-object v0, LX/EP1;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115694
    sput-object v2, LX/EP1;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115695
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115696
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115697
    new-instance p0, LX/EP1;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v3

    check-cast v3, LX/11S;

    invoke-direct {p0, v3}, LX/EP1;-><init>(LX/11S;)V

    .line 2115698
    move-object v0, p0

    .line 2115699
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115700
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EP1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115701
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115702
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
