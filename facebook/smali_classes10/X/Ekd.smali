.class public LX/Ekd;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final f:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LX/Ekc;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/database/sqlite/SQLiteDatabase;

.field public final b:LX/1Se;

.field public final c:LX/Ekf;

.field public final d:LX/14r;

.field public final e:LX/14r;

.field private final g:LX/Eka;

.field public final h:LX/EkZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2163642
    new-instance v0, LX/EkX;

    invoke-direct {v0}, LX/EkX;-><init>()V

    sput-object v0, LX/Ekd;->f:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(LX/1Se;LX/Ekf;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2163643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163644
    new-instance v0, LX/14r;

    invoke-direct {v0, v1}, LX/14r;-><init>(I)V

    iput-object v0, p0, LX/Ekd;->d:LX/14r;

    .line 2163645
    new-instance v0, LX/14r;

    invoke-direct {v0, v1}, LX/14r;-><init>(I)V

    iput-object v0, p0, LX/Ekd;->e:LX/14r;

    .line 2163646
    new-instance v0, LX/Eka;

    invoke-direct {v0}, LX/Eka;-><init>()V

    iput-object v0, p0, LX/Ekd;->g:LX/Eka;

    .line 2163647
    new-instance v0, LX/EkZ;

    invoke-direct {v0, p0}, LX/EkZ;-><init>(LX/Ekd;)V

    iput-object v0, p0, LX/Ekd;->h:LX/EkZ;

    .line 2163648
    iput-object p1, p0, LX/Ekd;->b:LX/1Se;

    .line 2163649
    iput-object p2, p0, LX/Ekd;->c:LX/Ekf;

    .line 2163650
    iget-object v0, p0, LX/Ekd;->b:LX/1Se;

    invoke-interface {v0}, LX/1Se;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, LX/Ekd;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 2163651
    return-void
.end method

.method private static c(LX/Ekd;)LX/Ekb;
    .locals 3

    .prologue
    .line 2163652
    sget-object v0, LX/Ekd;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ekc;

    iget-object v1, p0, LX/Ekd;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 2163653
    iget-object v2, v0, LX/Ekc;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ekb;

    .line 2163654
    if-nez v2, :cond_0

    .line 2163655
    new-instance v2, LX/Ekb;

    invoke-direct {v2}, LX/Ekb;-><init>()V

    .line 2163656
    iget-object p0, v0, LX/Ekc;->a:Ljava/util/WeakHashMap;

    invoke-virtual {p0, v1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2163657
    :cond_0
    move-object v0, v2

    .line 2163658
    return-object v0
.end method


# virtual methods
.method public final a(LX/AU6;)LX/EkY;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/AU2;",
            ">(",
            "LX/AU6",
            "<TT;>;)",
            "Lcom/facebook/crudolib/dbinsert/Inserter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2163659
    iget-object v0, p0, LX/Ekd;->g:LX/Eka;

    invoke-interface {p1}, LX/AU4;->b()Ljava/lang/Object;

    move-result-object v1

    .line 2163660
    iget-object v2, v0, LX/Eka;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2163661
    iget-object v2, v0, LX/Eka;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2163662
    :cond_0
    new-instance v0, LX/EkY;

    invoke-direct {v0, p0, p1}, LX/EkY;-><init>(LX/Ekd;LX/AU6;)V

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 2163663
    iget-object v0, p0, LX/Ekd;->d:LX/14r;

    iget v0, v0, LX/14r;->a:I

    iget-object v1, p0, LX/Ekd;->e:LX/14r;

    iget v1, v1, LX/14r;->a:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 2163664
    :goto_0
    if-eqz v0, :cond_0

    .line 2163665
    iget-object v1, p0, LX/Ekd;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2163666
    :cond_0
    iget-object v1, p0, LX/Ekd;->a:Landroid/database/sqlite/SQLiteDatabase;

    const v2, 0x690a79d7

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2163667
    invoke-static {p0}, LX/Ekd;->c(LX/Ekd;)LX/Ekb;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Ekb;->a(Z)I

    .line 2163668
    return-void

    .line 2163669
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2163670
    iget-object v0, p0, LX/Ekd;->a:Landroid/database/sqlite/SQLiteDatabase;

    const v1, -0x2c627f21

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2163671
    invoke-static {p0}, LX/Ekd;->c(LX/Ekd;)LX/Ekb;

    move-result-object v0

    iget-object v1, p0, LX/Ekd;->g:LX/Eka;

    .line 2163672
    iget-object p0, v0, LX/Ekb;->a:Ljava/util/ArrayList;

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2163673
    iget p0, v0, LX/Ekb;->b:I

    add-int/lit8 p0, p0, 0x1

    iput p0, v0, LX/Ekb;->b:I

    .line 2163674
    return-void
.end method
