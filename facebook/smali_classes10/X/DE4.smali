.class public LX/DE4;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DE5;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DE4",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DE5;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976331
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1976332
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DE4;->b:LX/0Zi;

    .line 1976333
    iput-object p1, p0, LX/DE4;->a:LX/0Ot;

    .line 1976334
    return-void
.end method

.method public static a(LX/0QB;)LX/DE4;
    .locals 4

    .prologue
    .line 1976319
    const-class v1, LX/DE4;

    monitor-enter v1

    .line 1976320
    :try_start_0
    sget-object v0, LX/DE4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976321
    sput-object v2, LX/DE4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976322
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976323
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976324
    new-instance v3, LX/DE4;

    const/16 p0, 0x1fb1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DE4;-><init>(LX/0Ot;)V

    .line 1976325
    move-object v0, v3

    .line 1976326
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976327
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DE4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976328
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976329
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1976330
    const v0, 0x7bdb9661

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1976289
    check-cast p2, LX/DE3;

    .line 1976290
    iget-object v0, p0, LX/DE4;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DE5;

    iget-object v1, p2, LX/DE3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/DE3;->b:LX/1Po;

    .line 1976291
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 p0, 0x1

    .line 1976292
    const-string v5, ""

    .line 1976293
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1976294
    check-cast v4, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1976295
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1976296
    check-cast v4, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1976297
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1976298
    check-cast v4, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 1976299
    :goto_0
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a015d

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0050

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/16 v5, 0xa

    invoke-interface {v4, p0, v5}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x0

    const/16 p0, 0x8

    invoke-interface {v4, v5, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 1976300
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/DE5;->a:LX/DEA;

    const/4 v5, 0x0

    .line 1976301
    new-instance p0, LX/DE9;

    invoke-direct {p0, v4}, LX/DE9;-><init>(LX/DEA;)V

    .line 1976302
    iget-object p2, v4, LX/DEA;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/DE8;

    .line 1976303
    if-nez p2, :cond_0

    .line 1976304
    new-instance p2, LX/DE8;

    invoke-direct {p2, v4}, LX/DE8;-><init>(LX/DEA;)V

    .line 1976305
    :cond_0
    invoke-static {p2, p1, v5, v5, p0}, LX/DE8;->a$redex0(LX/DE8;LX/1De;IILX/DE9;)V

    .line 1976306
    move-object p0, p2

    .line 1976307
    move-object v5, p0

    .line 1976308
    move-object v4, v5

    .line 1976309
    iget-object v5, v4, LX/DE8;->a:LX/DE9;

    iput-object v2, v5, LX/DE9;->a:LX/1Po;

    .line 1976310
    iget-object v5, v4, LX/DE8;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1976311
    move-object v4, v4

    .line 1976312
    iget-object v5, v4, LX/DE8;->a:LX/DE9;

    iput-object v1, v5, LX/DE9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1976313
    iget-object v5, v4, LX/DE8;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1976314
    move-object v4, v4

    .line 1976315
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    iget-boolean v3, v0, LX/DE5;->d:Z

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1976316
    return-object v0

    :cond_1
    invoke-static {p1}, LX/3mm;->c(LX/1De;)LX/3mp;

    move-result-object v3

    const v5, 0x7f081866

    invoke-virtual {v3, v5}, LX/3mp;->h(I)LX/3mp;

    move-result-object v3

    .line 1976317
    const v5, 0x7bdb9661

    const/4 p0, 0x0

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1976318
    invoke-virtual {v3, v5}, LX/3mp;->a(LX/1dQ;)LX/3mp;

    move-result-object v3

    goto :goto_1

    :cond_2
    move-object v4, v5

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1976278
    invoke-static {}, LX/1dS;->b()V

    .line 1976279
    iget v0, p1, LX/1dQ;->b:I

    .line 1976280
    packed-switch v0, :pswitch_data_0

    .line 1976281
    :goto_0
    return-object v2

    .line 1976282
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1976283
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 1976284
    iget-object p1, p0, LX/DE4;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DE5;

    .line 1976285
    iget-object p2, p1, LX/DE5;->c:LX/17Y;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->P:Ljava/lang/String;

    invoke-interface {p2, p0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    .line 1976286
    const-string p0, "extra_navigation_source"

    const-string v1, "gysc"

    invoke-virtual {p2, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1976287
    iget-object p0, p1, LX/DE5;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p0, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1976288
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7bdb9661
        :pswitch_0
    .end packed-switch
.end method
