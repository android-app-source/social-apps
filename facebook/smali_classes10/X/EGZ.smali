.class public final enum LX/EGZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EGZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EGZ;

.field public static final enum END_CALL:LX/EGZ;

.field public static final enum END_CALL_WITH_RETRY:LX/EGZ;

.field public static final enum NO_END_CALL:LX/EGZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2097429
    new-instance v0, LX/EGZ;

    const-string v1, "NO_END_CALL"

    invoke-direct {v0, v1, v2}, LX/EGZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGZ;->NO_END_CALL:LX/EGZ;

    .line 2097430
    new-instance v0, LX/EGZ;

    const-string v1, "END_CALL"

    invoke-direct {v0, v1, v3}, LX/EGZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGZ;->END_CALL:LX/EGZ;

    .line 2097431
    new-instance v0, LX/EGZ;

    const-string v1, "END_CALL_WITH_RETRY"

    invoke-direct {v0, v1, v4}, LX/EGZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGZ;->END_CALL_WITH_RETRY:LX/EGZ;

    .line 2097432
    const/4 v0, 0x3

    new-array v0, v0, [LX/EGZ;

    sget-object v1, LX/EGZ;->NO_END_CALL:LX/EGZ;

    aput-object v1, v0, v2

    sget-object v1, LX/EGZ;->END_CALL:LX/EGZ;

    aput-object v1, v0, v3

    sget-object v1, LX/EGZ;->END_CALL_WITH_RETRY:LX/EGZ;

    aput-object v1, v0, v4

    sput-object v0, LX/EGZ;->$VALUES:[LX/EGZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2097433
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EGZ;
    .locals 1

    .prologue
    .line 2097434
    const-class v0, LX/EGZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EGZ;

    return-object v0
.end method

.method public static values()[LX/EGZ;
    .locals 1

    .prologue
    .line 2097435
    sget-object v0, LX/EGZ;->$VALUES:[LX/EGZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EGZ;

    return-object v0
.end method
