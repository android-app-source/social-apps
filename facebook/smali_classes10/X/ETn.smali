.class public LX/ETn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ETd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/ETd",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;


# instance fields
.field private final c:LX/ETQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2125369
    sget-object v0, LX/ESz;->SEEN:LX/ESz;

    invoke-virtual {v0}, LX/ESz;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ETn;->a:Ljava/lang/String;

    .line 2125370
    sget-object v0, LX/ESz;->LIVE:LX/ESz;

    invoke-virtual {v0}, LX/ESz;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ETn;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/ETQ;)V
    .locals 0
    .param p1    # LX/ETQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2125367
    iput-object p1, p0, LX/ETn;->c:LX/ETQ;

    .line 2125368
    return-void
.end method

.method private static a(LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2125349
    move v0, v1

    :goto_0
    invoke-virtual {p0}, LX/ETQ;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2125350
    invoke-virtual {p0, v0}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v2

    .line 2125351
    invoke-static {v2, p1}, LX/ETn;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2125352
    iget-object v1, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2125353
    invoke-interface {v1}, LX/9uc;->cN()Ljava/lang/String;

    move-result-object v3

    sget-object p1, LX/ETn;->b:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1}, LX/9uc;->cN()Ljava/lang/String;

    move-result-object v3

    sget-object p1, LX/ETn;->a:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2125354
    :cond_0
    :goto_1
    move-object v1, v2

    .line 2125355
    invoke-virtual {p0, v0, v1}, LX/ETQ;->b(ILcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v1

    .line 2125356
    :cond_1
    :goto_2
    return v1

    .line 2125357
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2125358
    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    invoke-static {v2, p1}, LX/ETn;->a(LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2125359
    const/4 v1, 0x1

    goto :goto_2

    .line 2125360
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2125361
    :cond_4
    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;->a(LX/9uc;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/9vT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;)LX/9vT;

    move-result-object v1

    sget-object v3, LX/ETn;->a:Ljava/lang/String;

    .line 2125362
    iput-object v3, v1, LX/9vT;->cH:Ljava/lang/String;

    .line 2125363
    move-object v1, v1

    .line 2125364
    invoke-virtual {v1}, LX/9vT;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v1

    .line 2125365
    invoke-virtual {v2, v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a(LX/9uc;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v2

    goto :goto_1
.end method

.method private a(LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ESz;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2125330
    move v0, v1

    :goto_0
    invoke-virtual {p1}, LX/ETQ;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2125331
    invoke-virtual {p1, v0}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v2

    .line 2125332
    invoke-static {v2, p2}, LX/ETn;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2125333
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2125334
    invoke-interface {v3}, LX/9uc;->cN()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, LX/ESz;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2125335
    :cond_0
    :goto_1
    return v1

    .line 2125336
    :cond_1
    iget-object v1, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2125337
    invoke-interface {v1}, LX/9uc;->cN()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, LX/ESz;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2125338
    :goto_2
    move-object v1, v2

    .line 2125339
    invoke-virtual {p1, v0, v1}, LX/ETQ;->b(ILcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v1

    goto :goto_1

    .line 2125340
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2125341
    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    invoke-direct {p0, v2, p2, p3}, LX/ETn;->a(LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ESz;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2125342
    const/4 v1, 0x1

    goto :goto_1

    .line 2125343
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2125344
    :cond_4
    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;->a(LX/9uc;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/9vT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;)LX/9vT;

    move-result-object v1

    invoke-virtual {p3}, LX/ESz;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2125345
    iput-object v3, v1, LX/9vT;->cH:Ljava/lang/String;

    .line 2125346
    move-object v1, v1

    .line 2125347
    invoke-virtual {v1}, LX/9vT;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v1

    .line 2125348
    invoke-virtual {v2, v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a(LX/9uc;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v2

    goto :goto_2
.end method

.method private static a(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2125319
    iget-object v1, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2125320
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 2125321
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2125322
    invoke-interface {v2}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2125323
    :cond_0
    :goto_0
    return v0

    .line 2125324
    :cond_1
    invoke-static {p0}, LX/ESx;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/ESx;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2125325
    iget-object v1, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2125326
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v1

    .line 2125327
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2125328
    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2125329
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 2

    .prologue
    .line 2125314
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2125315
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 2125316
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v0, v1, :cond_0

    .line 2125317
    const/4 v0, 0x0

    .line 2125318
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/ETn;->c:LX/ETQ;

    invoke-static {v0, p1}, LX/ETn;->a(LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ESz;)Z
    .locals 2

    .prologue
    .line 2125309
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2125310
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 2125311
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_CREATOR:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v0, v1, :cond_0

    .line 2125312
    const/4 v0, 0x0

    .line 2125313
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/ETn;->c:LX/ETQ;

    invoke-direct {p0, v0, p1, p2}, LX/ETn;->a(LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ESz;)Z

    move-result v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/ESz;)Z
    .locals 1

    .prologue
    .line 2125307
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {p0, p1, p2}, LX/ETn;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ESz;)Z

    move-result v0

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2125308
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {p0, p1}, LX/ETn;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    return v0
.end method
