.class public final LX/Ee7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/Ee8;


# direct methods
.method public constructor <init>(LX/Ee8;)V
    .locals 0

    .prologue
    .line 2152014
    iput-object p1, p0, LX/Ee7;->a:LX/Ee8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, -0x4bd11721

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2152015
    iget-object v1, p0, LX/Ee7;->a:LX/Ee8;

    iget-object v2, p0, LX/Ee7;->a:LX/Ee8;

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 2152016
    :try_start_0
    const-string v3, "com.android.internal.os.PowerProfile"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 2152017
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v5, v7

    invoke-virtual {v3, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object p1, v2, LX/Ee8;->e:Landroid/content/Context;

    aput-object p1, v7, v9

    invoke-virtual {v5, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    .line 2152018
    const-string v5, "sPowerMap"

    invoke-virtual {v3, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 2152019
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2152020
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    .line 2152021
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 2152022
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 2152023
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    .line 2152024
    instance-of v7, v5, [Ljava/lang/Double;

    if-eqz v7, :cond_1

    .line 2152025
    new-instance p2, Lorg/json/JSONArray;

    invoke-direct {p2}, Lorg/json/JSONArray;-><init>()V

    .line 2152026
    check-cast v5, [Ljava/lang/Double;

    check-cast v5, [Ljava/lang/Double;

    move v7, v8

    .line 2152027
    :goto_1
    array-length p3, v5

    if-ge v7, p3, :cond_0

    .line 2152028
    aget-object p3, v5, v7

    invoke-virtual {p2, p3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2152029
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 2152030
    :cond_0
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v9, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2152031
    :catch_0
    move-exception v3

    .line 2152032
    iget-object v5, v2, LX/Ee8;->f:LX/03V;

    sget-object v7, LX/Ee8;->a:Ljava/lang/String;

    const-string v8, "Unable to read power profile."

    invoke-virtual {v5, v7, v8, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v6

    .line 2152033
    :goto_2
    move-object v2, v3

    .line 2152034
    if-nez v2, :cond_3

    .line 2152035
    :goto_3
    iget-object v1, p0, LX/Ee7;->a:LX/Ee8;

    iget-object v1, v1, LX/Ee8;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/Ee8;->b:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2152036
    iget-object v1, p0, LX/Ee7;->a:LX/Ee8;

    iget-object v1, v1, LX/Ee8;->i:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2152037
    const/16 v1, 0x27

    const v2, -0x76117b24

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2152038
    :cond_1
    :try_start_1
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v9, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 2152039
    :cond_2
    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_2

    .line 2152040
    :cond_3
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "android_power_profile"

    invoke-direct {v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2152041
    const-string v5, "power_profile"

    invoke-virtual {v3, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2152042
    iget-object v5, v1, LX/Ee8;->c:LX/0Zb;

    invoke-interface {v5, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_3
.end method
