.class public final LX/Dlc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;

.field public final synthetic b:LX/Dld;


# direct methods
.method public constructor <init>(LX/Dld;Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;)V
    .locals 0

    .prologue
    .line 2038364
    iput-object p1, p0, LX/Dlc;->b:LX/Dld;

    iput-object p2, p0, LX/Dlc;->a:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x6fd5b63d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2038365
    iget-object v1, p0, LX/Dlc;->b:LX/Dld;

    iget-object v1, v1, LX/Dld;->m:LX/Dlg;

    iget-object v1, v1, LX/Dlg;->h:LX/Dip;

    iget-object v2, p0, LX/Dlc;->a:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2038366
    iget-object v4, v1, LX/Dip;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->i:LX/Dih;

    iget-object v5, v1, LX/Dip;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    iget-object v5, v5, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->b:Ljava/lang/String;

    iget-object v6, v1, LX/Dip;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    iget-object v6, v6, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->u:Ljava/lang/String;

    .line 2038367
    iget-object v7, v4, LX/Dih;->a:LX/0Zb;

    const-string p0, "appointment_calendar_tap_appointment"

    invoke-static {p0, v5}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "request_id"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "referrer"

    invoke-virtual {p0, p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v7, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2038368
    invoke-static {v2}, LX/DkQ;->f(Ljava/lang/String;)LX/DkQ;

    move-result-object v5

    .line 2038369
    iget-object v4, v1, LX/Dip;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v4, v1, LX/Dip;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->e:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/auth/viewercontext/ViewerContext;

    const/4 v7, 0x0

    invoke-static {v6, v5, v4, v7}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2038370
    const-string v5, "referrer"

    const-string v6, "calendar"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2038371
    iget-object v5, v1, LX/Dip;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    iget-object v5, v5, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->c:Lcom/facebook/content/SecureContextHelper;

    const/4 v6, 0x1

    iget-object v7, v1, LX/Dip;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    invoke-interface {v5, v4, v6, v7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2038372
    const v1, 0x157a284d

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
