.class public final LX/Cpd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1938442
    iput-object p1, p0, LX/Cpd;->c:Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    iput-object p2, p0, LX/Cpd;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Cpd;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5fd4baa5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1938443
    iget-object v1, p0, LX/Cpd;->c:Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    iget-object v2, p0, LX/Cpd;->a:Ljava/lang/String;

    iget-object v3, p0, LX/Cpd;->b:Ljava/lang/String;

    .line 1938444
    new-instance v6, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v6, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1938445
    if-nez v2, :cond_4

    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v6, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1938446
    const-string v5, "extra_instant_articles_id"

    invoke-virtual {v6, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938447
    const-string v5, "extra_instant_articles_canonical_url"

    invoke-virtual {v6, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938448
    const-string v5, "com.android.browser.headers"

    invoke-static {}, LX/Cs3;->a()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {v6, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1938449
    const-string v5, "extra_instant_articles_referrer"

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->z:Ljava/lang/String;

    invoke-virtual {v6, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938450
    const-string v5, "extra_parent_article_click_source"

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->g:LX/Chi;

    .line 1938451
    iget-object p1, p0, LX/Chi;->j:Ljava/lang/String;

    move-object p0, p1

    .line 1938452
    invoke-virtual {v6, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938453
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->e:LX/ClD;

    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/ClD;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1938454
    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1938455
    const-string p0, "click_source_document_chaining_id"

    invoke-virtual {v6, p0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938456
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->e:LX/ClD;

    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/ClD;->a(Landroid/content/Context;)I

    move-result v5

    .line 1938457
    const/4 p0, -0x1

    if-eq v5, p0, :cond_0

    .line 1938458
    const-string p0, "click_source_document_depth"

    invoke-virtual {v6, p0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1938459
    :cond_0
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1938460
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->i:LX/Cig;

    new-instance p0, LX/Cin;

    invoke-direct {p0}, LX/Cin;-><init>()V

    invoke-virtual {v5, p0}, LX/0b4;->a(LX/0b7;)V

    .line 1938461
    :cond_1
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v5, v6, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1938462
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1938463
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1938464
    const-string v5, "article_ID"

    invoke-interface {v6, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938465
    :cond_2
    const-string v5, "ia_source"

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->z:Ljava/lang/String;

    invoke-interface {v6, v5, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938466
    const-string v5, "position"

    iget p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->x:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v6, v5, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938467
    const-string p0, "is_instant_article"

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x1

    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v6, p0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938468
    const-string v5, "click_source"

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->z:Ljava/lang/String;

    invoke-interface {v6, v5, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938469
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->C:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 1938470
    const-string v5, "block_id"

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->C:Ljava/lang/String;

    invoke-interface {v6, v5, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938471
    :cond_3
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->c:LX/Ckw;

    invoke-virtual {v5, v2, v6}, LX/Ckw;->b(Ljava/lang/String;Ljava/util/Map;)V

    .line 1938472
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->c:LX/Ckw;

    iget-object v6, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->z:Ljava/lang/String;

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->C:Ljava/lang/String;

    invoke-virtual {v5, v2, v6, p0}, LX/Ckw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1938473
    const v1, -0x5b543719

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1938474
    :cond_4
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_0

    .line 1938475
    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method
