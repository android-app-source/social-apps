.class public final LX/DcB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DcG;

.field public final synthetic b:LX/Dc9;

.field public final synthetic c:Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

.field public final synthetic d:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;LX/DcG;LX/Dc9;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V
    .locals 0

    .prologue
    .line 2017979
    iput-object p1, p0, LX/DcB;->d:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    iput-object p2, p0, LX/DcB;->a:LX/DcG;

    iput-object p3, p0, LX/DcB;->b:LX/Dc9;

    iput-object p4, p0, LX/DcB;->c:Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x175f588a

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017980
    iget-object v1, p0, LX/DcB;->d:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    iget-object v1, v1, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->c:LX/DbM;

    iget-object v2, p0, LX/DcB;->d:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    iget-object v2, v2, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->i:Ljava/lang/String;

    iget-object v3, p0, LX/DcB;->a:LX/DcG;

    invoke-virtual {v3}, LX/DcG;->name()Ljava/lang/String;

    move-result-object v3

    .line 2017981
    const-string v4, "page_menu_management"

    const-string v5, "menu_management_row_tap"

    invoke-static {v4, v5, v2}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2017982
    const-string v5, "menu_type"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2017983
    iget-object v5, v1, LX/DbM;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2017984
    iget-object v1, p0, LX/DcB;->b:LX/Dc9;

    iget-object v2, p0, LX/DcB;->d:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    iget-object v3, p0, LX/DcB;->d:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    iget-object v3, v3, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->j:Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v4, p0, LX/DcB;->d:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    iget-object v4, v4, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->i:Ljava/lang/String;

    iget-object v5, p0, LX/DcB;->c:Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    invoke-interface {v1, v2, v3, v4, v5}, LX/Dc8;->a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V

    .line 2017985
    const v1, -0x256440f1

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
