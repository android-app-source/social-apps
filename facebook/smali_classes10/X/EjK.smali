.class public LX/EjK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2161245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2161246
    iput-boolean v4, p0, LX/EjK;->e:Z

    .line 2161247
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, LX/EjK;->a:J

    .line 2161248
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EjK;->b:Ljava/lang/String;

    .line 2161249
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2161250
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2161251
    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/EjK;->c:Ljava/lang/String;

    .line 2161252
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2161253
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2161254
    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    :goto_1
    iput v0, p0, LX/EjK;->d:I

    .line 2161255
    iput-boolean v4, p0, LX/EjK;->e:Z

    .line 2161256
    return-void

    .line 2161257
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2161258
    goto :goto_1
.end method

.method public constructor <init>(Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2161259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2161260
    iput-boolean v4, p0, LX/EjK;->e:Z

    .line 2161261
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, LX/EjK;->a:J

    .line 2161262
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EjK;->b:Ljava/lang/String;

    .line 2161263
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2161264
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2161265
    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/EjK;->c:Ljava/lang/String;

    .line 2161266
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2161267
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2161268
    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    :goto_1
    iput v0, p0, LX/EjK;->d:I

    .line 2161269
    iput-boolean v4, p0, LX/EjK;->e:Z

    .line 2161270
    return-void

    .line 2161271
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2161272
    goto :goto_1
.end method
