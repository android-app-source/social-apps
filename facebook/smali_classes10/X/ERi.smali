.class public LX/ERi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EQY;


# instance fields
.field private final a:LX/ES8;

.field private b:LX/2TK;


# direct methods
.method public constructor <init>(LX/ES8;LX/2TK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2121337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121338
    iput-object p1, p0, LX/ERi;->a:LX/ES8;

    .line 2121339
    iput-object p2, p0, LX/ERi;->b:LX/2TK;

    .line 2121340
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    .line 2121341
    iget-object v0, p0, LX/ERi;->a:LX/ES8;

    invoke-virtual {v0, v8, v9}, LX/ES8;->b(J)Ljava/util/Map;

    move-result-object v3

    .line 2121342
    if-nez v3, :cond_1

    .line 2121343
    :cond_0
    return-void

    .line 2121344
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2121345
    instance-of v1, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->j()J

    move-result-wide v6

    cmp-long v1, v6, v8

    if-lez v1, :cond_2

    .line 2121346
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->j()J

    move-result-wide v6

    invoke-static {v1, v6, v7}, LX/75E;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    .line 2121347
    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2121348
    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2121349
    cmp-long v1, v6, v8

    if-lez v1, :cond_2

    .line 2121350
    iput-wide v6, v0, Lcom/facebook/ipc/media/MediaItem;->e:J

    .line 2121351
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2121352
    iget-object v0, p0, LX/ERi;->b:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->b()Z

    move-result v0

    return v0
.end method
