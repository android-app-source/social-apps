.class public final LX/EbT;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EbS;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EbT;",
        ">;",
        "LX/EbS;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:LX/EWc;

.field private c:I

.field private d:I

.field private e:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2143854
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2143855
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbT;->b:LX/EWc;

    .line 2143856
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbT;->e:LX/EWc;

    .line 2143857
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2143858
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2143859
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbT;->b:LX/EWc;

    .line 2143860
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbT;->e:LX/EWc;

    .line 2143861
    return-void
.end method

.method private d(LX/EWY;)LX/EbT;
    .locals 1

    .prologue
    .line 2143862
    instance-of v0, p1, LX/EbU;

    if-eqz v0, :cond_0

    .line 2143863
    check-cast p1, LX/EbU;

    invoke-virtual {p0, p1}, LX/EbT;->a(LX/EbU;)LX/EbT;

    move-result-object p0

    .line 2143864
    :goto_0
    return-object p0

    .line 2143865
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EbT;
    .locals 4

    .prologue
    .line 2143866
    const/4 v2, 0x0

    .line 2143867
    :try_start_0
    sget-object v0, LX/EbU;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EbU;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2143868
    if-eqz v0, :cond_0

    .line 2143869
    invoke-virtual {p0, v0}, LX/EbT;->a(LX/EbU;)LX/EbT;

    .line 2143870
    :cond_0
    return-object p0

    .line 2143871
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2143872
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2143873
    check-cast v0, LX/EbU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2143874
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2143875
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2143876
    invoke-virtual {p0, v1}, LX/EbT;->a(LX/EbU;)LX/EbT;

    :cond_1
    throw v0

    .line 2143877
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static u()LX/EbT;
    .locals 1

    .prologue
    .line 2143878
    new-instance v0, LX/EbT;

    invoke-direct {v0}, LX/EbT;-><init>()V

    return-object v0
.end method

.method private w()LX/EbT;
    .locals 2

    .prologue
    .line 2143879
    invoke-static {}, LX/EbT;->u()LX/EbT;

    move-result-object v0

    invoke-direct {p0}, LX/EbT;->y()LX/EbU;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EbT;->a(LX/EbU;)LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/EbU;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2143880
    new-instance v2, LX/EbU;

    invoke-direct {v2, p0}, LX/EbU;-><init>(LX/EWj;)V

    .line 2143881
    iget v3, p0, LX/EbT;->a:I

    .line 2143882
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 2143883
    :goto_0
    iget-object v1, p0, LX/EbT;->b:LX/EWc;

    .line 2143884
    iput-object v1, v2, LX/EbU;->ratchetKey_:LX/EWc;

    .line 2143885
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2143886
    or-int/lit8 v0, v0, 0x2

    .line 2143887
    :cond_0
    iget v1, p0, LX/EbT;->c:I

    .line 2143888
    iput v1, v2, LX/EbU;->counter_:I

    .line 2143889
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2143890
    or-int/lit8 v0, v0, 0x4

    .line 2143891
    :cond_1
    iget v1, p0, LX/EbT;->d:I

    .line 2143892
    iput v1, v2, LX/EbU;->previousCounter_:I

    .line 2143893
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 2143894
    or-int/lit8 v0, v0, 0x8

    .line 2143895
    :cond_2
    iget-object v1, p0, LX/EbT;->e:LX/EWc;

    .line 2143896
    iput-object v1, v2, LX/EbU;->ciphertext_:LX/EWc;

    .line 2143897
    iput v0, v2, LX/EbU;->bitField0_:I

    .line 2143898
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2143899
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2143933
    invoke-direct {p0, p1}, LX/EbT;->d(LX/EWY;)LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2143900
    invoke-direct {p0, p1, p2}, LX/EbT;->d(LX/EWd;LX/EYZ;)LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/EbT;
    .locals 1

    .prologue
    .line 2143901
    iget v0, p0, LX/EbT;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EbT;->a:I

    .line 2143902
    iput p1, p0, LX/EbT;->c:I

    .line 2143903
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143904
    return-object p0
.end method

.method public final a(LX/EWc;)LX/EbT;
    .locals 1

    .prologue
    .line 2143905
    if-nez p1, :cond_0

    .line 2143906
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2143907
    :cond_0
    iget v0, p0, LX/EbT;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EbT;->a:I

    .line 2143908
    iput-object p1, p0, LX/EbT;->b:LX/EWc;

    .line 2143909
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143910
    return-object p0
.end method

.method public final a(LX/EbU;)LX/EbT;
    .locals 2

    .prologue
    .line 2143911
    sget-object v0, LX/EbU;->c:LX/EbU;

    move-object v0, v0

    .line 2143912
    if-ne p1, v0, :cond_0

    .line 2143913
    :goto_0
    return-object p0

    .line 2143914
    :cond_0
    invoke-virtual {p1}, LX/EbU;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2143915
    iget-object v0, p1, LX/EbU;->ratchetKey_:LX/EWc;

    move-object v0, v0

    .line 2143916
    invoke-virtual {p0, v0}, LX/EbT;->a(LX/EWc;)LX/EbT;

    .line 2143917
    :cond_1
    invoke-virtual {p1}, LX/EbU;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2143918
    iget v0, p1, LX/EbU;->counter_:I

    move v0, v0

    .line 2143919
    invoke-virtual {p0, v0}, LX/EbT;->a(I)LX/EbT;

    .line 2143920
    :cond_2
    iget v0, p1, LX/EbU;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2143921
    if-eqz v0, :cond_3

    .line 2143922
    iget v0, p1, LX/EbU;->previousCounter_:I

    move v0, v0

    .line 2143923
    invoke-virtual {p0, v0}, LX/EbT;->b(I)LX/EbT;

    .line 2143924
    :cond_3
    invoke-virtual {p1}, LX/EbU;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2143925
    iget-object v0, p1, LX/EbU;->ciphertext_:LX/EWc;

    move-object v0, v0

    .line 2143926
    invoke-virtual {p0, v0}, LX/EbT;->b(LX/EWc;)LX/EbT;

    .line 2143927
    :cond_4
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2143928
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2143853
    invoke-direct {p0, p1, p2}, LX/EbT;->d(LX/EWd;LX/EYZ;)LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2143829
    invoke-direct {p0}, LX/EbT;->w()LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)LX/EbT;
    .locals 1

    .prologue
    .line 2143929
    iget v0, p0, LX/EbT;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EbT;->a:I

    .line 2143930
    iput p1, p0, LX/EbT;->d:I

    .line 2143931
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143932
    return-object p0
.end method

.method public final b(LX/EWc;)LX/EbT;
    .locals 1

    .prologue
    .line 2143830
    if-nez p1, :cond_0

    .line 2143831
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2143832
    :cond_0
    iget v0, p0, LX/EbT;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EbT;->a:I

    .line 2143833
    iput-object p1, p0, LX/EbT;->e:LX/EWc;

    .line 2143834
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143835
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2143836
    invoke-direct {p0, p1, p2}, LX/EbT;->d(LX/EWd;LX/EYZ;)LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2143837
    invoke-direct {p0}, LX/EbT;->w()LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2143838
    invoke-direct {p0, p1}, LX/EbT;->d(LX/EWY;)LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2143839
    invoke-direct {p0}, LX/EbT;->w()LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2143840
    sget-object v0, LX/EbV;->b:LX/EYn;

    const-class v1, LX/EbU;

    const-class v2, LX/EbT;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2143841
    sget-object v0, LX/EbV;->a:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2143842
    invoke-direct {p0}, LX/EbT;->w()LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2143843
    invoke-direct {p0}, LX/EbT;->y()LX/EbU;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2143844
    invoke-virtual {p0}, LX/EbT;->l()LX/EbU;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2143845
    invoke-direct {p0}, LX/EbT;->y()LX/EbU;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2143846
    invoke-virtual {p0}, LX/EbT;->l()LX/EbU;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EbU;
    .locals 2

    .prologue
    .line 2143847
    invoke-direct {p0}, LX/EbT;->y()LX/EbU;

    move-result-object v0

    .line 2143848
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2143849
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2143850
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2143851
    sget-object v0, LX/EbU;->c:LX/EbU;

    move-object v0, v0

    .line 2143852
    return-object v0
.end method
