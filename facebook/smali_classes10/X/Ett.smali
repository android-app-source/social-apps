.class public LX/Ett;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/Eu4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2178771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2178772
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2178773
    iput-object v0, p0, LX/Ett;->b:LX/0Ot;

    .line 2178774
    return-void
.end method

.method public static a(LX/0QB;)LX/Ett;
    .locals 5

    .prologue
    .line 2178775
    const-class v1, LX/Ett;

    monitor-enter v1

    .line 2178776
    :try_start_0
    sget-object v0, LX/Ett;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2178777
    sput-object v2, LX/Ett;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2178778
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2178779
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2178780
    new-instance v4, LX/Ett;

    invoke-direct {v4}, LX/Ett;-><init>()V

    .line 2178781
    invoke-static {v0}, LX/Eu4;->a(LX/0QB;)LX/Eu4;

    move-result-object v3

    check-cast v3, LX/Eu4;

    const/16 p0, 0xa79

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2178782
    iput-object v3, v4, LX/Ett;->a:LX/Eu4;

    iput-object p0, v4, LX/Ett;->b:LX/0Ot;

    .line 2178783
    move-object v0, v4

    .line 2178784
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2178785
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ett;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2178786
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2178787
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
