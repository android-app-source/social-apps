.class public final LX/EYF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EYE;


# instance fields
.field private final a:I

.field private b:LX/EWr;

.field private final c:Ljava/lang/String;

.field public final d:LX/EYQ;

.field private final e:LX/EYF;

.field private final f:[LX/EYF;

.field private final g:[LX/EYL;

.field private final h:[LX/EYP;

.field private final i:[LX/EYP;


# direct methods
.method public constructor <init>(LX/EWr;LX/EYQ;LX/EYF;I)V
    .locals 8

    .prologue
    .line 2136790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2136791
    iput p4, p0, LX/EYF;->a:I

    .line 2136792
    iput-object p1, p0, LX/EYF;->b:LX/EWr;

    .line 2136793
    invoke-virtual {p1}, LX/EWr;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3, v0}, LX/EYT;->b(LX/EYQ;LX/EYF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EYF;->c:Ljava/lang/String;

    .line 2136794
    iput-object p2, p0, LX/EYF;->d:LX/EYQ;

    .line 2136795
    iput-object p3, p0, LX/EYF;->e:LX/EYF;

    .line 2136796
    invoke-virtual {p1}, LX/EWr;->n()I

    move-result v0

    new-array v0, v0, [LX/EYF;

    iput-object v0, p0, LX/EYF;->f:[LX/EYF;

    .line 2136797
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, LX/EWr;->n()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2136798
    iget-object v1, p0, LX/EYF;->f:[LX/EYF;

    new-instance v2, LX/EYF;

    invoke-virtual {p1, v0}, LX/EWr;->c(I)LX/EWr;

    move-result-object v3

    invoke-direct {v2, v3, p2, p0, v0}, LX/EYF;-><init>(LX/EWr;LX/EYQ;LX/EYF;I)V

    aput-object v2, v1, v0

    .line 2136799
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2136800
    :cond_0
    invoke-virtual {p1}, LX/EWr;->o()I

    move-result v0

    new-array v0, v0, [LX/EYL;

    iput-object v0, p0, LX/EYF;->g:[LX/EYL;

    .line 2136801
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {p1}, LX/EWr;->o()I

    move-result v0

    if-ge v4, v0, :cond_1

    .line 2136802
    iget-object v6, p0, LX/EYF;->g:[LX/EYL;

    new-instance v0, LX/EYL;

    invoke-virtual {p1, v4}, LX/EWr;->d(I)LX/EWv;

    move-result-object v1

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v4}, LX/EYL;-><init>(LX/EWv;LX/EYQ;LX/EYF;I)V

    aput-object v0, v6, v4

    .line 2136803
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2136804
    :cond_1
    invoke-virtual {p1}, LX/EWr;->l()I

    move-result v0

    new-array v0, v0, [LX/EYP;

    iput-object v0, p0, LX/EYF;->h:[LX/EYP;

    .line 2136805
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {p1}, LX/EWr;->l()I

    move-result v0

    if-ge v4, v0, :cond_2

    .line 2136806
    iget-object v7, p0, LX/EYF;->h:[LX/EYP;

    new-instance v0, LX/EYP;

    invoke-virtual {p1, v4}, LX/EWr;->a(I)LX/EXL;

    move-result-object v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, LX/EYP;-><init>(LX/EXL;LX/EYQ;LX/EYF;IZB)V

    aput-object v0, v7, v4

    .line 2136807
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2136808
    :cond_2
    invoke-virtual {p1}, LX/EWr;->m()I

    move-result v0

    new-array v0, v0, [LX/EYP;

    iput-object v0, p0, LX/EYF;->i:[LX/EYP;

    .line 2136809
    const/4 v4, 0x0

    :goto_3
    invoke-virtual {p1}, LX/EWr;->m()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 2136810
    iget-object v7, p0, LX/EYF;->i:[LX/EYP;

    new-instance v0, LX/EYP;

    invoke-virtual {p1, v4}, LX/EWr;->b(I)LX/EXL;

    move-result-object v1

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, LX/EYP;-><init>(LX/EXL;LX/EYQ;LX/EYF;IZB)V

    aput-object v0, v7, v4

    .line 2136811
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 2136812
    :cond_3
    iget-object v0, p2, LX/EYQ;->h:LX/EYJ;

    invoke-virtual {v0, p0}, LX/EYJ;->a(LX/EYE;)V

    .line 2136813
    return-void
.end method

.method public static a$redex0(LX/EYF;LX/EWr;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2136823
    iput-object p1, p0, LX/EYF;->b:LX/EWr;

    move v0, v1

    .line 2136824
    :goto_0
    iget-object v2, p0, LX/EYF;->f:[LX/EYF;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2136825
    iget-object v2, p0, LX/EYF;->f:[LX/EYF;

    aget-object v2, v2, v0

    invoke-virtual {p1, v0}, LX/EWr;->c(I)LX/EWr;

    move-result-object v3

    invoke-static {v2, v3}, LX/EYF;->a$redex0(LX/EYF;LX/EWr;)V

    .line 2136826
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 2136827
    :goto_1
    iget-object v2, p0, LX/EYF;->g:[LX/EYL;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2136828
    iget-object v2, p0, LX/EYF;->g:[LX/EYL;

    aget-object v2, v2, v0

    invoke-virtual {p1, v0}, LX/EWr;->d(I)LX/EWv;

    move-result-object v3

    invoke-static {v2, v3}, LX/EYL;->a$redex0(LX/EYL;LX/EWv;)V

    .line 2136829
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 2136830
    :goto_2
    iget-object v2, p0, LX/EYF;->h:[LX/EYP;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 2136831
    iget-object v2, p0, LX/EYF;->h:[LX/EYP;

    aget-object v2, v2, v0

    invoke-virtual {p1, v0}, LX/EWr;->a(I)LX/EXL;

    move-result-object v3

    .line 2136832
    iput-object v3, v2, LX/EYP;->c:LX/EXL;

    .line 2136833
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2136834
    :cond_2
    :goto_3
    iget-object v0, p0, LX/EYF;->i:[LX/EYP;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 2136835
    iget-object v0, p0, LX/EYF;->i:[LX/EYP;

    aget-object v0, v0, v1

    invoke-virtual {p1, v1}, LX/EWr;->b(I)LX/EXL;

    move-result-object v2

    .line 2136836
    iput-object v2, v0, LX/EYP;->c:LX/EXL;

    .line 2136837
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2136838
    :cond_3
    return-void
.end method

.method public static j(LX/EYF;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2136839
    iget-object v2, p0, LX/EYF;->f:[LX/EYF;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 2136840
    invoke-static {v4}, LX/EYF;->j(LX/EYF;)V

    .line 2136841
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2136842
    :cond_0
    iget-object v2, p0, LX/EYF;->h:[LX/EYP;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2136843
    invoke-static {v4}, LX/EYP;->x(LX/EYP;)V

    .line 2136844
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2136845
    :cond_1
    iget-object v1, p0, LX/EYF;->i:[LX/EYP;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2136846
    invoke-static {v3}, LX/EYP;->x(LX/EYP;)V

    .line 2136847
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2136848
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2136858
    iget-object v0, p0, LX/EYF;->b:LX/EWr;

    invoke-virtual {v0}, LX/EWr;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Z
    .locals 3

    .prologue
    .line 2136849
    iget-object v0, p0, LX/EYF;->b:LX/EWr;

    .line 2136850
    iget-object v1, v0, LX/EWr;->extensionRange_:Ljava/util/List;

    move-object v0, v1

    .line 2136851
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWq;

    .line 2136852
    iget v2, v0, LX/EWq;->start_:I

    move v2, v2

    .line 2136853
    if-gt v2, p1, :cond_0

    .line 2136854
    iget v2, v0, LX/EWq;->end_:I

    move v0, v2

    .line 2136855
    if-ge p1, v0, :cond_0

    .line 2136856
    const/4 v0, 0x1

    .line 2136857
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2136821
    iget-object v0, p0, LX/EYF;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/EYQ;
    .locals 1

    .prologue
    .line 2136822
    iget-object v0, p0, LX/EYF;->d:LX/EYQ;

    return-object v0
.end method

.method public final d()LX/EXf;
    .locals 1

    .prologue
    .line 2136818
    iget-object v0, p0, LX/EYF;->b:LX/EWr;

    .line 2136819
    iget-object p0, v0, LX/EWr;->options_:LX/EXf;

    move-object v0, p0

    .line 2136820
    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/EYP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2136817
    iget-object v0, p0, LX/EYF;->h:[LX/EYP;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/EYF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2136816
    iget-object v0, p0, LX/EYF;->f:[LX/EYF;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/EYL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2136815
    iget-object v0, p0, LX/EYF;->g:[LX/EYL;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/EWY;
    .locals 1

    .prologue
    .line 2136814
    iget-object v0, p0, LX/EYF;->b:LX/EWr;

    return-object v0
.end method
