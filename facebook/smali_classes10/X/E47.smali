.class public final LX/E47;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic c:LX/2km;

.field public final synthetic d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)V
    .locals 0

    .prologue
    .line 2075981
    iput-object p1, p0, LX/E47;->d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;

    iput-object p2, p0, LX/E47;->a:Ljava/lang/String;

    iput-object p3, p0, LX/E47;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p4, p0, LX/E47;->c:LX/2km;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x31be59fe

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2075982
    iget-object v1, p0, LX/E47;->d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->h:LX/E1i;

    iget-object v2, p0, LX/E47;->a:Ljava/lang/String;

    iget-object v4, p0, LX/E47;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075983
    invoke-static {v4}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->h(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->c()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 2075984
    move-object v3, v5

    .line 2075985
    iget-object v5, p0, LX/E47;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075986
    invoke-static {v5}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionReviewUnitComponentPartDefinition;->h(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel;->e()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;

    move-result-object p1

    .line 2075987
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionStoryAttachmentStoryFragmentModel$FeedbackModel;->a()Ljava/lang/String;

    move-result-object p1

    :goto_0
    move-object p1, p1

    .line 2075988
    move-object v4, p1

    .line 2075989
    sget-object v5, LX/Cfc;->LOCAL_CONTENT_REVIEW_UNIT_TAP:LX/Cfc;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v1

    .line 2075990
    iget-object v2, p0, LX/E47;->c:LX/2km;

    iget-object v3, p0, LX/E47;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075991
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v4

    .line 2075992
    iget-object v4, p0, LX/E47;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075993
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2075994
    invoke-interface {v2, v3, v4, v1}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2075995
    const v1, 0x34408e62

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method
