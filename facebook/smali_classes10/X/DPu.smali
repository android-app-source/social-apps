.class public final LX/DPu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DPv;


# direct methods
.method public constructor <init>(LX/DPv;)V
    .locals 0

    .prologue
    .line 1993932
    iput-object p1, p0, LX/DPu;->a:LX/DPv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5de09699

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1993933
    iget-object v1, p0, LX/DPu;->a:LX/DPv;

    iget-object v1, v1, LX/DPv;->d:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v1, v1, Lcom/facebook/groups/info/GroupInfoAdapter;->k:LX/DPo;

    iget-object v2, p0, LX/DPu;->a:LX/DPv;

    iget-object v2, v2, LX/DPv;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1993934
    invoke-static {v2}, LX/5QR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1993935
    iget-object v5, v1, LX/DPo;->d:LX/17W;

    invoke-virtual {v5, v3, p0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1993936
    new-instance p1, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {p1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, LX/DPo;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ComponentName;

    invoke-virtual {p1, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v5

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v5, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v5

    .line 1993937
    iget-object p0, v1, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v5, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1993938
    :cond_0
    const v1, 0x371c475f

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
