.class public final LX/EWh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2130838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/EYQ;)LX/EYa;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2130839
    sput-object p1, LX/EYC;->O:LX/EYQ;

    .line 2130840
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130841
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130842
    sput-object v0, LX/EYC;->a:LX/EYF;

    .line 2130843
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->a:LX/EYF;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "File"

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130844
    sput-object v0, LX/EYC;->b:LX/EYn;

    .line 2130845
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130846
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130847
    sput-object v0, LX/EYC;->c:LX/EYF;

    .line 2130848
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->c:LX/EYF;

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Package"

    aput-object v3, v2, v6

    const-string v3, "Dependency"

    aput-object v3, v2, v7

    const-string v3, "PublicDependency"

    aput-object v3, v2, v8

    const-string v3, "WeakDependency"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "MessageType"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "EnumType"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "Service"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "Extension"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "Options"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "SourceCodeInfo"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130849
    sput-object v0, LX/EYC;->d:LX/EYn;

    .line 2130850
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130851
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130852
    sput-object v0, LX/EYC;->e:LX/EYF;

    .line 2130853
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->e:LX/EYF;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Field"

    aput-object v3, v2, v6

    const-string v3, "Extension"

    aput-object v3, v2, v7

    const-string v3, "NestedType"

    aput-object v3, v2, v8

    const-string v3, "EnumType"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "ExtensionRange"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "Options"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130854
    sput-object v0, LX/EYC;->f:LX/EYn;

    .line 2130855
    sget-object v0, LX/EYC;->e:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130856
    sput-object v0, LX/EYC;->g:LX/EYF;

    .line 2130857
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->g:LX/EYF;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "Start"

    aput-object v3, v2, v5

    const-string v3, "End"

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130858
    sput-object v0, LX/EYC;->h:LX/EYn;

    .line 2130859
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130860
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130861
    sput-object v0, LX/EYC;->i:LX/EYF;

    .line 2130862
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->i:LX/EYF;

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Number"

    aput-object v3, v2, v6

    const-string v3, "Label"

    aput-object v3, v2, v7

    const-string v3, "Type"

    aput-object v3, v2, v8

    const-string v3, "TypeName"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "Extendee"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "DefaultValue"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "Options"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130863
    sput-object v0, LX/EYC;->j:LX/EYn;

    .line 2130864
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130865
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130866
    sput-object v0, LX/EYC;->k:LX/EYF;

    .line 2130867
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->k:LX/EYF;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Value"

    aput-object v3, v2, v6

    const-string v3, "Options"

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130868
    sput-object v0, LX/EYC;->l:LX/EYn;

    .line 2130869
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130870
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130871
    sput-object v0, LX/EYC;->m:LX/EYF;

    .line 2130872
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->m:LX/EYF;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Number"

    aput-object v3, v2, v6

    const-string v3, "Options"

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130873
    sput-object v0, LX/EYC;->n:LX/EYn;

    .line 2130874
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130875
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130876
    sput-object v0, LX/EYC;->o:LX/EYF;

    .line 2130877
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->o:LX/EYF;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "Method"

    aput-object v3, v2, v6

    const-string v3, "Options"

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130878
    sput-object v0, LX/EYC;->p:LX/EYn;

    .line 2130879
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130880
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130881
    sput-object v0, LX/EYC;->q:LX/EYF;

    .line 2130882
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->q:LX/EYF;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "InputType"

    aput-object v3, v2, v6

    const-string v3, "OutputType"

    aput-object v3, v2, v7

    const-string v3, "Options"

    aput-object v3, v2, v8

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130883
    sput-object v0, LX/EYC;->r:LX/EYn;

    .line 2130884
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130885
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130886
    sput-object v0, LX/EYC;->s:LX/EYF;

    .line 2130887
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->s:LX/EYF;

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "JavaPackage"

    aput-object v3, v2, v5

    const-string v3, "JavaOuterClassname"

    aput-object v3, v2, v6

    const-string v3, "JavaMultipleFiles"

    aput-object v3, v2, v7

    const-string v3, "JavaGenerateEqualsAndHash"

    aput-object v3, v2, v8

    const-string v3, "OptimizeFor"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "GoPackage"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "CcGenericServices"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "JavaGenericServices"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "PyGenericServices"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "UninterpretedOption"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130888
    sput-object v0, LX/EYC;->t:LX/EYn;

    .line 2130889
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130890
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130891
    sput-object v0, LX/EYC;->u:LX/EYF;

    .line 2130892
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->u:LX/EYF;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "MessageSetWireFormat"

    aput-object v3, v2, v5

    const-string v3, "NoStandardDescriptorAccessor"

    aput-object v3, v2, v6

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130893
    sput-object v0, LX/EYC;->v:LX/EYn;

    .line 2130894
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130895
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130896
    sput-object v0, LX/EYC;->w:LX/EYF;

    .line 2130897
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->w:LX/EYF;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Ctype"

    aput-object v3, v2, v5

    const-string v3, "Packed"

    aput-object v3, v2, v6

    const-string v3, "Lazy"

    aput-object v3, v2, v7

    const-string v3, "Deprecated"

    aput-object v3, v2, v8

    const-string v3, "ExperimentalMapKey"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "Weak"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "UninterpretedOption"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130898
    sput-object v0, LX/EYC;->x:LX/EYn;

    .line 2130899
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130900
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xb

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130901
    sput-object v0, LX/EYC;->y:LX/EYF;

    .line 2130902
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->y:LX/EYF;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "AllowAlias"

    aput-object v3, v2, v5

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130903
    sput-object v0, LX/EYC;->z:LX/EYn;

    .line 2130904
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130905
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xc

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130906
    sput-object v0, LX/EYC;->A:LX/EYF;

    .line 2130907
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->A:LX/EYF;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130908
    sput-object v0, LX/EYC;->B:LX/EYn;

    .line 2130909
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130910
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130911
    sput-object v0, LX/EYC;->C:LX/EYF;

    .line 2130912
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->C:LX/EYF;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130913
    sput-object v0, LX/EYC;->D:LX/EYn;

    .line 2130914
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130915
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xe

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130916
    sput-object v0, LX/EYC;->E:LX/EYF;

    .line 2130917
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->E:LX/EYF;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "UninterpretedOption"

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130918
    sput-object v0, LX/EYC;->F:LX/EYn;

    .line 2130919
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130920
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0xf

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130921
    sput-object v0, LX/EYC;->G:LX/EYF;

    .line 2130922
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->G:LX/EYF;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Name"

    aput-object v3, v2, v5

    const-string v3, "IdentifierValue"

    aput-object v3, v2, v6

    const-string v3, "PositiveIntValue"

    aput-object v3, v2, v7

    const-string v3, "NegativeIntValue"

    aput-object v3, v2, v8

    const-string v3, "DoubleValue"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "StringValue"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "AggregateValue"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130923
    sput-object v0, LX/EYC;->H:LX/EYn;

    .line 2130924
    sget-object v0, LX/EYC;->G:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130925
    sput-object v0, LX/EYC;->I:LX/EYF;

    .line 2130926
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->I:LX/EYF;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "NamePart"

    aput-object v3, v2, v5

    const-string v3, "IsExtension"

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130927
    sput-object v0, LX/EYC;->J:LX/EYn;

    .line 2130928
    sget-object v0, LX/EYC;->O:LX/EYQ;

    move-object v0, v0

    .line 2130929
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x10

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130930
    sput-object v0, LX/EYC;->K:LX/EYF;

    .line 2130931
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->K:LX/EYF;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "Location"

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130932
    sput-object v0, LX/EYC;->L:LX/EYn;

    .line 2130933
    sget-object v0, LX/EYC;->K:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2130934
    sput-object v0, LX/EYC;->M:LX/EYF;

    .line 2130935
    new-instance v0, LX/EYn;

    sget-object v1, LX/EYC;->M:LX/EYF;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "Path"

    aput-object v3, v2, v5

    const-string v3, "Span"

    aput-object v3, v2, v6

    const-string v3, "LeadingComments"

    aput-object v3, v2, v7

    const-string v3, "TrailingComments"

    aput-object v3, v2, v8

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2130936
    sput-object v0, LX/EYC;->N:LX/EYn;

    .line 2130937
    const/4 v0, 0x0

    return-object v0
.end method
