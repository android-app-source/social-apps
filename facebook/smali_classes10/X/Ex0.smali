.class public final LX/Ex0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V
    .locals 0

    .prologue
    .line 2183924
    iput-object p1, p0, LX/Ex0;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2183925
    iget-object v0, p0, LX/Ex0;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    .line 2183926
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    invoke-virtual {v1, p3}, LX/Exj;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Eus;

    .line 2183927
    sget-object v2, LX/0ax;->bE:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, LX/Eus;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2183928
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2183929
    const-string v4, "timeline_friend_request_ref"

    sget-object v5, LX/5P2;->FRIENDS_CENTER:LX/5P2;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2183930
    new-instance v4, LX/5vg;

    invoke-direct {v4}, LX/5vg;-><init>()V

    invoke-virtual {v1}, LX/Eus;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 2183931
    iput-object v5, v4, LX/5vg;->d:Ljava/lang/String;

    .line 2183932
    move-object v4, v4

    .line 2183933
    new-instance v5, LX/4aM;

    invoke-direct {v5}, LX/4aM;-><init>()V

    invoke-virtual {v1}, LX/Eus;->d()Ljava/lang/String;

    move-result-object v6

    .line 2183934
    iput-object v6, v5, LX/4aM;->b:Ljava/lang/String;

    .line 2183935
    move-object v5, v5

    .line 2183936
    invoke-virtual {v5}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    .line 2183937
    iput-object v5, v4, LX/5vg;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2183938
    move-object v4, v4

    .line 2183939
    invoke-virtual {v1}, LX/Eus;->b()Ljava/lang/String;

    move-result-object v5

    .line 2183940
    iput-object v5, v4, LX/5vg;->e:Ljava/lang/String;

    .line 2183941
    move-object v4, v4

    .line 2183942
    invoke-virtual {v1}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2183943
    iput-object v1, v4, LX/5vg;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2183944
    move-object v1, v4

    .line 2183945
    invoke-virtual {v1}, LX/5vg;->a()Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;

    move-result-object v1

    invoke-static {v3, v1}, LX/5ve;->a(Landroid/os/Bundle;LX/36O;)V

    .line 2183946
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v4, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2183947
    return-void
.end method
