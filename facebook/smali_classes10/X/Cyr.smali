.class public LX/Cyr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cyq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Cyq",
        "<",
        "Lcom/facebook/search/results/protocol/elections/SearchElectionQueryModels$SearchElectionQueryModel;",
        "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionsModuleInterfaces$SearchResultsElectionsNode;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Cyr;


# instance fields
.field private final a:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0tX;

.field public final c:LX/2Sc;

.field public d:LX/Cwy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Cwy",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionsModuleInterfaces$SearchResultsElectionsNode;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/Cx0;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;LX/2Sc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1953818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1953819
    iput-object p1, p0, LX/Cyr;->a:LX/1Ck;

    .line 1953820
    iput-object p2, p0, LX/Cyr;->b:LX/0tX;

    .line 1953821
    iput-object p3, p0, LX/Cyr;->c:LX/2Sc;

    .line 1953822
    return-void
.end method

.method public static a(LX/0QB;)LX/Cyr;
    .locals 6

    .prologue
    .line 1953805
    sget-object v0, LX/Cyr;->f:LX/Cyr;

    if-nez v0, :cond_1

    .line 1953806
    const-class v1, LX/Cyr;

    monitor-enter v1

    .line 1953807
    :try_start_0
    sget-object v0, LX/Cyr;->f:LX/Cyr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1953808
    if-eqz v2, :cond_0

    .line 1953809
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1953810
    new-instance p0, LX/Cyr;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v5

    check-cast v5, LX/2Sc;

    invoke-direct {p0, v3, v4, v5}, LX/Cyr;-><init>(LX/1Ck;LX/0tX;LX/2Sc;)V

    .line 1953811
    move-object v0, p0

    .line 1953812
    sput-object v0, LX/Cyr;->f:LX/Cyr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1953813
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1953814
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1953815
    :cond_1
    sget-object v0, LX/Cyr;->f:LX/Cyr;

    return-object v0

    .line 1953816
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1953817
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b$redex0(LX/Cyr;)V
    .locals 1

    .prologue
    .line 1953827
    iget-object v0, p0, LX/Cyr;->e:LX/Cx0;

    if-eqz v0, :cond_0

    .line 1953828
    iget-object v0, p0, LX/Cyr;->e:LX/Cx0;

    invoke-interface {v0}, LX/Cx0;->a()V

    .line 1953829
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0gW;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchElectionQueryModels$SearchElectionQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1953830
    iget-object v0, p0, LX/Cyr;->a:LX/1Ck;

    const-string v1, "search_result_elections_key"

    .line 1953831
    invoke-static {p1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 1953832
    iget-object v3, p0, LX/Cyr;->b:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    move-object v2, v2

    .line 1953833
    new-instance v3, LX/Cyp;

    invoke-direct {v3, p0}, LX/Cyp;-><init>(LX/Cyr;)V

    move-object v3, v3

    .line 1953834
    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1953835
    return-void
.end method

.method public final a(LX/Cwy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cwy",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionsModuleInterfaces$SearchResultsElectionsNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1953825
    iput-object p1, p0, LX/Cyr;->d:LX/Cwy;

    .line 1953826
    return-void
.end method

.method public final a(LX/Cx0;)V
    .locals 0

    .prologue
    .line 1953823
    iput-object p1, p0, LX/Cyr;->e:LX/Cx0;

    .line 1953824
    return-void
.end method
