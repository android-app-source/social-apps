.class public final LX/DnF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dlr;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;)V
    .locals 0

    .prologue
    .line 2040102
    iput-object p1, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;)V
    .locals 13

    .prologue
    .line 2040103
    iget-object v0, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2040104
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2040105
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-ne p2, v1, :cond_1

    .line 2040106
    iget-object v1, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->j:LX/Dih;

    iget-object v2, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->n:Ljava/lang/String;

    .line 2040107
    iget-object v3, v1, LX/Dih;->a:LX/0Zb;

    const-string v4, "admin_tapped_add_notes"

    invoke-static {v4, v0}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p2, "referrer"

    invoke-virtual {v4, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2040108
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->EDIT_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2040109
    iget-object v1, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->m:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2040110
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2040111
    invoke-virtual {p1, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->setViewState(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;)V

    .line 2040112
    :cond_0
    :goto_0
    return-void

    .line 2040113
    :cond_1
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->EDIT_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-ne p2, v1, :cond_3

    .line 2040114
    iget-object v1, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->j:LX/Dih;

    iget-object v2, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->n:Ljava/lang/String;

    .line 2040115
    iget-object v3, v1, LX/Dih;->a:LX/0Zb;

    const-string v4, "admin_edit_notes_saved"

    invoke-static {v4, v0}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p2, "referrer"

    invoke-virtual {v4, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2040116
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->SAVING_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2040117
    iget-object v2, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->m:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2040118
    iput-object v1, v2, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2040119
    invoke-virtual {p1, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->setViewState(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;)V

    .line 2040120
    iget-object v1, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v2, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->h:LX/Djv;

    iget-object v3, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->k:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    iget-object v4, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->m:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2040121
    iget-object v5, v4, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    move-object v5, v5

    .line 2040122
    sget-object v6, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->SAVING_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-eq v5, v6, :cond_4

    .line 2040123
    :cond_2
    :goto_1
    goto :goto_0

    .line 2040124
    :cond_3
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->SAVING_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-eq p2, v1, :cond_0

    .line 2040125
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->VIEW_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    if-ne p2, v1, :cond_0

    .line 2040126
    iget-object v1, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->j:LX/Dih;

    iget-object v2, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->n:Ljava/lang/String;

    .line 2040127
    iget-object v3, v1, LX/Dih;->a:LX/0Zb;

    const-string v4, "admin_tapped_edit_notes"

    invoke-static {v4, v0}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "referrer"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2040128
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->EDIT_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2040129
    iget-object v1, p0, LX/DnF;->a:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->m:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2040130
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2040131
    invoke-virtual {p1, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView;->setViewState(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;)V

    goto :goto_0

    .line 2040132
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->k()Ljava/lang/String;

    move-result-object v10

    .line 2040133
    iget-object v5, v4, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;

    move-object v7, v5

    .line 2040134
    iget-object v5, v4, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->c:Ljava/lang/String;

    move-object v8, v5

    .line 2040135
    if-nez v7, :cond_5

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2040136
    sget-object v5, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2040137
    iput-object v5, v4, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2040138
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_1

    .line 2040139
    :cond_5
    if-nez v7, :cond_6

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    .line 2040140
    iget-object v5, v1, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->h:LX/Djv;

    const-string v6, "ADD"

    new-instance v7, LX/4Ji;

    invoke-direct {v7}, LX/4Ji;-><init>()V

    invoke-virtual {v7, v8}, LX/4Ji;->a(Ljava/lang/String;)LX/4Ji;

    move-result-object v8

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x0

    new-instance v12, LX/Dn1;

    invoke-direct {v12, v1, v4}, LX/Dn1;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;)V

    move-object v7, v0

    invoke-virtual/range {v5 .. v12}, LX/Djv;->a(Ljava/lang/String;Ljava/lang/String;LX/4Ji;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Dju;)V

    goto :goto_1

    .line 2040141
    :cond_6
    if-eqz v7, :cond_7

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2040142
    iget-object v5, v1, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->h:LX/Djv;

    const-string v6, "DELETE"

    const/4 v8, 0x0

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2040143
    iget-object v11, v7, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;->a:Ljava/lang/String;

    move-object v11, v11

    .line 2040144
    new-instance v12, LX/Dn2;

    invoke-direct {v12, v1, v4}, LX/Dn2;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;)V

    move-object v7, v0

    invoke-virtual/range {v5 .. v12}, LX/Djv;->a(Ljava/lang/String;Ljava/lang/String;LX/4Ji;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Dju;)V

    goto/16 :goto_1

    .line 2040145
    :cond_7
    if-eqz v7, :cond_2

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2040146
    iget-object v5, v7, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2040147
    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2040148
    sget-object v5, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->VIEW_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2040149
    iput-object v5, v4, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2040150
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    goto/16 :goto_1

    .line 2040151
    :cond_8
    const-string v6, "UPDATE"

    new-instance v5, LX/4Ji;

    invoke-direct {v5}, LX/4Ji;-><init>()V

    invoke-virtual {v5, v8}, LX/4Ji;->a(Ljava/lang/String;)LX/4Ji;

    move-result-object v8

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2040152
    iget-object v5, v7, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;->a:Ljava/lang/String;

    move-object v11, v5

    .line 2040153
    new-instance v12, LX/Dn3;

    invoke-direct {v12, v1, v4}, LX/Dn3;-><init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;)V

    move-object v5, v2

    move-object v7, v0

    invoke-virtual/range {v5 .. v12}, LX/Djv;->a(Ljava/lang/String;Ljava/lang/String;LX/4Ji;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Dju;)V

    goto/16 :goto_1
.end method
