.class public final LX/Ewx;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V
    .locals 0

    .prologue
    .line 2183902
    iput-object p1, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2183903
    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2183904
    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    .line 2183905
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2183906
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v2, 0x7f080039

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance p1, LX/Ewt;

    invoke-direct {p1, v0}, LX/Ewt;-><init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    invoke-virtual {v1, v2, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2183907
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->k:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2183908
    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    const/4 v1, 0x0

    .line 2183909
    iput-object v1, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->p:Ljava/lang/String;

    .line 2183910
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 2183858
    check-cast p1, LX/0Px;

    const/4 v2, 0x0

    .line 2183859
    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->c:LX/Eun;

    if-eqz v0, :cond_0

    .line 2183860
    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->c:LX/Eun;

    invoke-virtual {v0}, LX/Eun;->a()V

    .line 2183861
    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    const/4 v1, 0x1

    .line 2183862
    iput-boolean v1, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->d:Z

    .line 2183863
    :cond_0
    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eub;

    invoke-virtual {v0}, LX/Eub;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2183864
    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    invoke-virtual {v0, v2}, LX/Exj;->a(Z)V

    .line 2183865
    :cond_1
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2183866
    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-static {v0}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->c(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    .line 2183867
    :goto_0
    return-void

    .line 2183868
    :cond_2
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2183869
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_6

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2183870
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2183871
    iget-object v1, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->n:Ljava/util/Map;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v1, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2183872
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    .line 2183873
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->n()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    sget-object v10, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2183874
    new-instance v10, LX/Euq;

    invoke-direct {v10}, LX/Euq;-><init>()V

    .line 2183875
    iput-wide v6, v10, LX/Euq;->a:J

    .line 2183876
    move-object v10, v10

    .line 2183877
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2183878
    :goto_2
    iput-object v1, v10, LX/Euq;->c:Ljava/lang/String;

    .line 2183879
    move-object v1, v10

    .line 2183880
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v10

    .line 2183881
    iput-object v10, v1, LX/Euq;->d:Ljava/lang/String;

    .line 2183882
    move-object v10, v1

    .line 2183883
    if-eqz v8, :cond_5

    invoke-virtual {v9, v8, v2}, LX/15i;->j(II)I

    move-result v1

    .line 2183884
    :goto_3
    iput v1, v10, LX/Euq;->e:I

    .line 2183885
    move-object v1, v10

    .line 2183886
    sget-object v8, LX/2h7;->FRIENDS_CENTER_SEARCH:LX/2h7;

    .line 2183887
    iput-object v8, v1, LX/Euq;->f:LX/2h7;

    .line 2183888
    move-object v1, v1

    .line 2183889
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 2183890
    iput-object v0, v1, LX/Euq;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2183891
    move-object v0, v1

    .line 2183892
    invoke-virtual {v0}, LX/Euq;->b()LX/Eus;

    move-result-object v0

    .line 2183893
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2183894
    iget-object v1, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->n:Ljava/util/Map;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v1, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2183895
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2183896
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2183897
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_3

    .line 2183898
    :cond_6
    iget-object v0, p0, LX/Ewx;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2183899
    iget-object v2, v0, LX/Exw;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2183900
    const v2, 0x18b30b88

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2183901
    goto/16 :goto_0
.end method
