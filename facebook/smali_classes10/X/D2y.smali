.class public LX/D2y;
.super LX/1wH;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/D2z;


# direct methods
.method public constructor <init>(LX/D2z;)V
    .locals 0

    .prologue
    .line 1959441
    iput-object p1, p0, LX/D2y;->b:LX/D2z;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1959393
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1959394
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1959395
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1959396
    invoke-virtual {p0, p2}, LX/1wH;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1959397
    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    iget-object v1, v1, LX/D2z;->j:LX/0Ot;

    invoke-virtual {p0, p1, p2, p3, v1}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/0Ot;)V

    .line 1959398
    :cond_0
    invoke-virtual {p0, v0}, LX/1wH;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1959399
    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    invoke-virtual {v1}, LX/D2z;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, p2, p3, v1}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;)V

    .line 1959400
    :cond_1
    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1959401
    invoke-virtual {p0, p1, p2, p3}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1959402
    :cond_2
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    move v1, v3

    .line 1959403
    if-eqz v1, :cond_3

    .line 1959404
    const v1, 0x7f08106d

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1959405
    new-instance v3, LX/D2u;

    invoke-direct {v3, p0, v0, v2}, LX/D2u;-><init>(LX/D2y;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1959406
    iget-object v3, p0, LX/D2y;->b:LX/D2z;

    .line 1959407
    const v4, 0x7f0207fd

    move v4, v4

    .line 1959408
    invoke-virtual {v3, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1959409
    :cond_3
    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    invoke-virtual {v1, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1959410
    const v1, 0x7f08106b

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1959411
    new-instance v3, LX/D2v;

    invoke-direct {v3, p0, v0, v2}, LX/D2v;-><init>(LX/D2y;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1959412
    iget-object v3, p0, LX/D2y;->b:LX/D2z;

    .line 1959413
    const v4, 0x7f020952

    move v4, v4

    .line 1959414
    invoke-virtual {v3, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1959415
    :cond_4
    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    .line 1959416
    invoke-virtual {v1, v0}, LX/1SX;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    move v1, v3

    .line 1959417
    if-eqz v1, :cond_5

    .line 1959418
    const v1, 0x7f081068

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1959419
    new-instance v3, LX/D2w;

    invoke-direct {v3, p0, v0, v2}, LX/D2w;-><init>(LX/D2y;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1959420
    iget-object v3, p0, LX/D2y;->b:LX/D2z;

    iget-object v3, v3, LX/D2z;->d:LX/1e4;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v3

    .line 1959421
    iget-object v4, p0, LX/D2y;->b:LX/D2z;

    .line 1959422
    invoke-virtual {v4, v1, v3, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1959423
    :cond_5
    invoke-virtual {p0, p2}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1959424
    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    .line 1959425
    invoke-virtual {v1, p1, p2, p3}, LX/1SX;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1959426
    :cond_6
    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1959427
    const v1, 0x7f081578

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1959428
    new-instance v3, LX/D2x;

    invoke-direct {v3, p0, v0, p3}, LX/D2x;-><init>(LX/D2y;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1959429
    iget-object v3, p0, LX/D2y;->b:LX/D2z;

    .line 1959430
    const v4, 0x7f020a0b

    move v4, v4

    .line 1959431
    invoke-virtual {v3, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1959432
    :cond_7
    invoke-virtual {p0, p1, p2}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1959433
    invoke-virtual {p0, p2}, LX/1wH;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1959434
    invoke-virtual {p0, p1, p2, v2}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 1959435
    :cond_8
    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    iget-object v1, v1, LX/D2z;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1e0;

    invoke-virtual {v1, v0}, LX/1e0;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1959436
    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    iget-object v1, v1, LX/D2z;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1e0;

    invoke-virtual {v1, p1, v0, v2}, LX/1e0;->a(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    .line 1959437
    :cond_9
    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    iget-object v1, v1, LX/D2z;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1e0;

    invoke-virtual {v1, v0}, LX/1e0;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1959438
    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    iget-object v1, v1, LX/D2z;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1e0;

    invoke-virtual {v1, p1, v0, v2}, LX/1e0;->b(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    .line 1959439
    :cond_a
    invoke-super {p0, p1, p2, p3}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1959440
    return-void
.end method

.method public a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1959386
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1959387
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1959388
    invoke-super {p0, p1}, LX/1wH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    .line 1959389
    invoke-virtual {v1, v0}, LX/1SX;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 1959390
    if-nez v1, :cond_0

    .line 1959391
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 1959392
    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, LX/1wH;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/D2y;->b:LX/D2z;

    invoke-virtual {v1, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
