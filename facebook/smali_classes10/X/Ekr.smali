.class public LX/Ekr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/El3;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Z

.field private final d:LX/Eky;


# direct methods
.method public constructor <init>(LX/Ekq;)V
    .locals 2

    .prologue
    .line 2163806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163807
    iget-object v0, p1, LX/Ekq;->a:LX/El3;

    if-nez v0, :cond_0

    .line 2163808
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Missing engine"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163809
    :cond_0
    iget-object v0, p1, LX/Ekq;->a:LX/El3;

    iput-object v0, p0, LX/Ekr;->a:LX/El3;

    .line 2163810
    iget-object v0, p1, LX/Ekq;->b:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_1

    .line 2163811
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p1, LX/Ekq;->b:Ljava/util/concurrent/ExecutorService;

    .line 2163812
    :cond_1
    iget-object v0, p1, LX/Ekq;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, LX/Ekr;->b:Ljava/util/concurrent/ExecutorService;

    .line 2163813
    iget-boolean v0, p1, LX/Ekq;->c:Z

    iput-boolean v0, p0, LX/Ekr;->c:Z

    .line 2163814
    new-instance v0, LX/Eky;

    invoke-direct {v0, p0}, LX/Eky;-><init>(LX/Ekr;)V

    iput-object v0, p0, LX/Ekr;->d:LX/Eky;

    .line 2163815
    return-void
.end method


# virtual methods
.method public final a(LX/Ekj;)LX/Ekk;
    .locals 3

    .prologue
    .line 2163816
    if-nez p1, :cond_0

    .line 2163817
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "appRequest required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163818
    :cond_0
    iget-object v0, p0, LX/Ekr;->d:LX/Eky;

    .line 2163819
    iget-object v1, v0, LX/Eky;->a:LX/Ekr;

    .line 2163820
    iget-object v2, v1, LX/Ekr;->b:Ljava/util/concurrent/ExecutorService;

    move-object v1, v2

    .line 2163821
    new-instance v2, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;

    invoke-direct {v2, v0, p1}, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;-><init>(LX/Eky;LX/Ekj;)V

    const p0, -0x75624fa9

    invoke-static {v1, v2, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2163822
    iget-object v0, p1, LX/Ekj;->e:LX/Ekk;

    move-object v0, v0

    .line 2163823
    return-object v0
.end method
