.class public final LX/EOs;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;

.field private final b:LX/CxV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

.field private final d:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
            "TE;Z)V"
        }
    .end annotation

    .prologue
    .line 2115401
    iput-object p1, p0, LX/EOs;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;

    invoke-direct {p0}, LX/2h0;-><init>()V

    .line 2115402
    iput-object p4, p0, LX/EOs;->b:LX/CxV;

    .line 2115403
    iput-object p2, p0, LX/EOs;->c:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    .line 2115404
    iput-object p3, p0, LX/EOs;->d:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    .line 2115405
    if-eqz p5, :cond_0

    .line 2115406
    const v0, 0x7f081cf6

    iput v0, p0, LX/EOs;->e:I

    .line 2115407
    const v0, 0x7f081cf8

    iput v0, p0, LX/EOs;->f:I

    .line 2115408
    :goto_0
    return-void

    .line 2115409
    :cond_0
    const v0, 0x7f082287

    iput v0, p0, LX/EOs;->e:I

    .line 2115410
    const v0, 0x7f082288

    iput v0, p0, LX/EOs;->f:I

    goto :goto_0
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2115411
    iget-object v0, p0, LX/EOs;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->f:LX/0kL;

    new-instance v1, LX/27k;

    iget v2, p0, LX/EOs;->f:I

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2115412
    iget-object v1, p0, LX/EOs;->d:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    iget-object v2, p0, LX/EOs;->c:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    iget-object v3, p0, LX/EOs;->b:LX/CxV;

    .line 2115413
    invoke-static {v1, v2, v3}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;)V

    .line 2115414
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2115415
    iget-object v0, p0, LX/EOs;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->f:LX/0kL;

    new-instance v1, LX/27k;

    iget v2, p0, LX/EOs;->e:I

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2115416
    return-void
.end method
