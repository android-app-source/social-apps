.class public final enum LX/DiO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DiO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DiO;

.field public static final enum CREATE_EVENT:LX/DiO;

.field public static final enum LIVE_LOCATION:LX/DiO;

.field public static final enum LOCATION:LX/DiO;

.field public static final enum NUX:LX/DiO;

.field public static final enum ORDER_FOOD:LX/DiO;

.field public static final enum PAYMENT:LX/DiO;

.field public static final enum POLL:LX/DiO;

.field public static final enum SCHEDULE_CALL:LX/DiO;

.field public static final enum STICKER:LX/DiO;

.field public static final enum TEXT:LX/DiO;

.field public static final enum TRANSPORTATION:LX/DiO;


# instance fields
.field private mId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 2032103
    new-instance v0, LX/DiO;

    const-string v1, "NUX"

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->NUX:LX/DiO;

    .line 2032104
    new-instance v0, LX/DiO;

    const-string v1, "CREATE_EVENT"

    invoke-direct {v0, v1, v4, v4}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->CREATE_EVENT:LX/DiO;

    .line 2032105
    new-instance v0, LX/DiO;

    const-string v1, "LOCATION"

    invoke-direct {v0, v1, v5, v5}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->LOCATION:LX/DiO;

    .line 2032106
    new-instance v0, LX/DiO;

    const-string v1, "PAYMENT"

    invoke-direct {v0, v1, v6, v6}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->PAYMENT:LX/DiO;

    .line 2032107
    new-instance v0, LX/DiO;

    const-string v1, "POLL"

    invoke-direct {v0, v1, v7, v7}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->POLL:LX/DiO;

    .line 2032108
    new-instance v0, LX/DiO;

    const-string v1, "STICKER"

    invoke-direct {v0, v1, v8, v8}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->STICKER:LX/DiO;

    .line 2032109
    new-instance v0, LX/DiO;

    const-string v1, "TEXT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->TEXT:LX/DiO;

    .line 2032110
    new-instance v0, LX/DiO;

    const-string v1, "TRANSPORTATION"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->TRANSPORTATION:LX/DiO;

    .line 2032111
    new-instance v0, LX/DiO;

    const-string v1, "ORDER_FOOD"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->ORDER_FOOD:LX/DiO;

    .line 2032112
    new-instance v0, LX/DiO;

    const-string v1, "SCHEDULE_CALL"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->SCHEDULE_CALL:LX/DiO;

    .line 2032113
    new-instance v0, LX/DiO;

    const-string v1, "LIVE_LOCATION"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/DiO;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiO;->LIVE_LOCATION:LX/DiO;

    .line 2032114
    const/16 v0, 0xb

    new-array v0, v0, [LX/DiO;

    const/4 v1, 0x0

    sget-object v2, LX/DiO;->NUX:LX/DiO;

    aput-object v2, v0, v1

    sget-object v1, LX/DiO;->CREATE_EVENT:LX/DiO;

    aput-object v1, v0, v4

    sget-object v1, LX/DiO;->LOCATION:LX/DiO;

    aput-object v1, v0, v5

    sget-object v1, LX/DiO;->PAYMENT:LX/DiO;

    aput-object v1, v0, v6

    sget-object v1, LX/DiO;->POLL:LX/DiO;

    aput-object v1, v0, v7

    sget-object v1, LX/DiO;->STICKER:LX/DiO;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, LX/DiO;->TEXT:LX/DiO;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DiO;->TRANSPORTATION:LX/DiO;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/DiO;->ORDER_FOOD:LX/DiO;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/DiO;->SCHEDULE_CALL:LX/DiO;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/DiO;->LIVE_LOCATION:LX/DiO;

    aput-object v2, v0, v1

    sput-object v0, LX/DiO;->$VALUES:[LX/DiO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2032100
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2032101
    iput p3, p0, LX/DiO;->mId:I

    .line 2032102
    return-void
.end method

.method public static fromId(I)LX/DiO;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2032095
    invoke-static {}, LX/DiO;->values()[LX/DiO;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2032096
    invoke-virtual {v0}, LX/DiO;->getId()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 2032097
    :goto_1
    return-object v0

    .line 2032098
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2032099
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/DiO;
    .locals 1

    .prologue
    .line 2032094
    const-class v0, LX/DiO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DiO;

    return-object v0
.end method

.method public static values()[LX/DiO;
    .locals 1

    .prologue
    .line 2032093
    sget-object v0, LX/DiO;->$VALUES:[LX/DiO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DiO;

    return-object v0
.end method


# virtual methods
.method public final getId()I
    .locals 1

    .prologue
    .line 2032092
    iget v0, p0, LX/DiO;->mId:I

    return v0
.end method
