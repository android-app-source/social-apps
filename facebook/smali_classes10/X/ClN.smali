.class public final LX/ClN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Landroid/text/SpannableStringBuilder;

.field public final synthetic c:LX/ClO;


# direct methods
.method public constructor <init>(LX/ClO;Landroid/content/Context;Landroid/text/SpannableStringBuilder;)V
    .locals 0

    .prologue
    .line 1932406
    iput-object p1, p0, LX/ClN;->c:LX/ClO;

    iput-object p2, p0, LX/ClN;->a:Landroid/content/Context;

    iput-object p3, p0, LX/ClN;->b:Landroid/text/SpannableStringBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 1932407
    iget-object v0, p0, LX/ClN;->a:Landroid/content/Context;

    iget-object v1, p0, LX/ClN;->b:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1932408
    iget-object v0, p0, LX/ClN;->a:Landroid/content/Context;

    const-string v1, "Copied to clipboard"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1932409
    const/4 v0, 0x1

    return v0
.end method
