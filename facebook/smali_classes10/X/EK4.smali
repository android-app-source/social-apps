.class public LX/EK4;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2105848
    const/4 v0, 0x0

    const v1, 0x7f030381

    invoke-direct {p0, p1, v0, v1}, LX/EK4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2105849
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2105850
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2105851
    invoke-virtual {p0, p3}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2105852
    const v0, 0x7f0d08f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/EK4;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2105853
    const v0, 0x7f0d08f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/EK4;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2105854
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;Z)V
    .locals 1

    .prologue
    .line 2105855
    iget-object v0, p0, LX/EK4;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;Z)V

    .line 2105856
    return-void
.end method
