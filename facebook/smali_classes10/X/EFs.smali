.class public LX/EFs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/view/WindowManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup$LayoutParams;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/Window;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/EGW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ml;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2096564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096565
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/EFs;->e:Ljava/util/Map;

    .line 2096566
    invoke-virtual {p1}, LX/1Ml;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/0dd;->d:LX/0Tn;

    invoke-interface {p2, v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    iput-boolean v0, p0, LX/EFs;->d:Z

    .line 2096567
    return-void

    .line 2096568
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/EFs;
    .locals 3

    .prologue
    .line 2096559
    new-instance v2, LX/EFs;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v0

    check-cast v0, LX/1Ml;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/EFs;-><init>(LX/1Ml;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2096560
    invoke-static {p0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2096561
    iput-object v0, v2, LX/EFs;->a:Landroid/view/WindowManager;

    .line 2096562
    move-object v0, v2

    .line 2096563
    return-object v0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 2096551
    iget-boolean v0, p0, LX/EFs;->d:Z

    move v0, v0

    .line 2096552
    if-eqz v0, :cond_1

    .line 2096553
    iget-object v0, p0, LX/EFs;->a:Landroid/view/WindowManager;

    invoke-interface {v0, p1, p2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2096554
    :cond_0
    :goto_0
    return-void

    .line 2096555
    :cond_1
    iget-object v0, p0, LX/EFs;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 2096556
    iget-object v0, p0, LX/EFs;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Window;

    .line 2096557
    if-eqz v0, :cond_0

    .line 2096558
    invoke-virtual {v0, p1, p2}, Landroid/view/Window;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public static c(LX/EFs;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2096543
    iget-boolean v0, p0, LX/EFs;->b:Z

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2096544
    iget-boolean v0, p0, LX/EFs;->d:Z

    if-eqz v0, :cond_1

    .line 2096545
    iget-object v0, p0, LX/EFs;->a:Landroid/view/WindowManager;

    invoke-interface {v0, p1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 2096546
    :cond_0
    :goto_0
    return-void

    .line 2096547
    :cond_1
    iget-object v0, p0, LX/EFs;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 2096548
    invoke-static {p1}, LX/EFs;->d(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 2096549
    if-eqz v0, :cond_0

    .line 2096550
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private static d(Landroid/view/View;)Landroid/view/ViewGroup;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2096538
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2096539
    if-eqz v0, :cond_0

    .line 2096540
    instance-of v1, p0, Landroid/view/ViewGroup;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2096541
    check-cast v0, Landroid/view/ViewGroup;

    .line 2096542
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2096468
    iget-boolean v0, p0, LX/EFs;->b:Z

    if-nez v0, :cond_1

    .line 2096469
    :cond_0
    :goto_0
    return-void

    .line 2096470
    :cond_1
    iget-object v0, p0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    .line 2096471
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096472
    iget-boolean v1, p0, LX/EFs;->d:Z

    move v1, v1

    .line 2096473
    if-eqz v1, :cond_2

    .line 2096474
    iget-object v1, p0, LX/EFs;->a:Landroid/view/WindowManager;

    invoke-interface {v1, p1, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2096475
    :cond_2
    invoke-static {p1}, LX/EFs;->d(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    .line 2096476
    if-eqz v1, :cond_0

    .line 2096477
    invoke-virtual {v1, p1, v0}, Landroid/view/ViewGroup;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 2096530
    iget-object v0, p0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    .line 2096531
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096532
    iget-boolean v1, p0, LX/EFs;->d:Z

    move v1, v1

    .line 2096533
    if-eqz v1, :cond_0

    .line 2096534
    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 2096535
    :goto_0
    invoke-direct {p0, p1}, LX/EFs;->f(Landroid/view/View;)V

    .line 2096536
    return-void

    .line 2096537
    :cond_0
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0
.end method

.method public final a(Landroid/view/View;II)V
    .locals 3

    .prologue
    .line 2096512
    iget-object v0, p0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    .line 2096513
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096514
    iget-boolean v1, p0, LX/EFs;->d:Z

    move v1, v1

    .line 2096515
    if-eqz v1, :cond_0

    move-object v1, v0

    .line 2096516
    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    iput p2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2096517
    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    iput p3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2096518
    :goto_0
    invoke-direct {p0, p1}, LX/EFs;->f(Landroid/view/View;)V

    .line 2096519
    return-void

    .line 2096520
    :cond_0
    iget-object v1, p0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    .line 2096521
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096522
    iget-boolean v2, p0, LX/EFs;->d:Z

    move v2, v2

    .line 2096523
    if-eqz v2, :cond_2

    .line 2096524
    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 2096525
    :goto_1
    move v1, v1

    .line 2096526
    and-int/lit8 v1, v1, 0x3

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    move-object v1, v0

    .line 2096527
    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    iput p2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2096528
    :goto_2
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput p3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 2096529
    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    iput p2, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto :goto_2

    :cond_2
    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_1
.end method

.method public final a(Landroid/view/View;IZ)V
    .locals 4

    .prologue
    .line 2096501
    iget-boolean v0, p0, LX/EFs;->d:Z

    move v0, v0

    .line 2096502
    if-nez v0, :cond_1

    .line 2096503
    :cond_0
    :goto_0
    return-void

    .line 2096504
    :cond_1
    iget-object v0, p0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 2096505
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096506
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 2096507
    if-eqz p3, :cond_2

    .line 2096508
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/2addr v2, p2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 2096509
    :goto_1
    if-eq v1, p2, :cond_0

    .line 2096510
    invoke-direct {p0, p1}, LX/EFs;->f(Landroid/view/View;)V

    goto :goto_0

    .line 2096511
    :cond_2
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_1
.end method

.method public final a(Landroid/view/View;LX/EFr;)V
    .locals 2

    .prologue
    .line 2096488
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096489
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096490
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Tracked view shouldn\'t be attached to anything"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2096491
    iget-boolean v0, p0, LX/EFs;->d:Z

    move v0, v0

    .line 2096492
    if-eqz v0, :cond_2

    .line 2096493
    invoke-interface {p2}, LX/EFr;->a()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 2096494
    :goto_1
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096495
    iget-object v1, p0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096496
    iget-boolean v1, p0, LX/EFs;->b:Z

    if-eqz v1, :cond_0

    .line 2096497
    invoke-direct {p0, p1, v0}, LX/EFs;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2096498
    :cond_0
    return-void

    .line 2096499
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2096500
    :cond_2
    invoke-interface {p2}, LX/EFr;->b()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2096483
    iget-boolean v0, p0, LX/EFs;->b:Z

    if-eqz v0, :cond_1

    .line 2096484
    :cond_0
    return-void

    .line 2096485
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EFs;->b:Z

    .line 2096486
    iget-object v0, p0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2096487
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p0, v1, v0}, LX/EFs;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2096478
    iget-boolean v0, p0, LX/EFs;->b:Z

    if-nez v0, :cond_0

    .line 2096479
    :goto_0
    return-void

    .line 2096480
    :cond_0
    iget-object v0, p0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2096481
    invoke-static {p0, v0}, LX/EFs;->c(LX/EFs;Landroid/view/View;)V

    goto :goto_1

    .line 2096482
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EFs;->b:Z

    goto :goto_0
.end method
