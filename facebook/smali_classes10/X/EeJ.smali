.class public LX/EeJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/growth/profile/SetProfilePhotoParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/EeJ;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2152368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152369
    return-void
.end method

.method public static a(LX/0QB;)LX/EeJ;
    .locals 3

    .prologue
    .line 2152370
    sget-object v0, LX/EeJ;->a:LX/EeJ;

    if-nez v0, :cond_1

    .line 2152371
    const-class v1, LX/EeJ;

    monitor-enter v1

    .line 2152372
    :try_start_0
    sget-object v0, LX/EeJ;->a:LX/EeJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2152373
    if-eqz v2, :cond_0

    .line 2152374
    :try_start_1
    new-instance v0, LX/EeJ;

    invoke-direct {v0}, LX/EeJ;-><init>()V

    .line 2152375
    move-object v0, v0

    .line 2152376
    sput-object v0, LX/EeJ;->a:LX/EeJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2152377
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2152378
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2152379
    :cond_1
    sget-object v0, LX/EeJ;->a:LX/EeJ;

    return-object v0

    .line 2152380
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2152381
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 2152382
    check-cast p1, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;

    .line 2152383
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2152384
    iget-object v0, p1, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2152385
    if-eqz v0, :cond_0

    .line 2152386
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "profile_pic_source"

    .line 2152387
    iget-object v3, p1, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2152388
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152389
    :cond_0
    iget-object v0, p1, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2152390
    if-eqz v0, :cond_1

    .line 2152391
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "profile_pic_method"

    .line 2152392
    iget-object v3, p1, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2152393
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152394
    :cond_1
    const-string v2, "%s/picture"

    .line 2152395
    iget-wide v8, p1, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->a:J

    move-wide v4, v8

    .line 2152396
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    const-string v0, "me"

    :goto_0
    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2152397
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "set_profile_photo"

    .line 2152398
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2152399
    move-object v2, v2

    .line 2152400
    const-string v3, "POST"

    .line 2152401
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2152402
    move-object v2, v2

    .line 2152403
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2152404
    move-object v0, v2

    .line 2152405
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2152406
    move-object v0, v0

    .line 2152407
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2152408
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2152409
    move-object v0, v0

    .line 2152410
    iget-object v1, p1, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2152411
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2152412
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2152413
    new-instance v1, LX/4ct;

    const-string v3, "image/jpeg"

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 2152414
    new-instance v2, LX/4cQ;

    const-string v3, "source"

    invoke-direct {v2, v3, v1}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 2152415
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2152416
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 2152417
    :cond_2
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2152418
    :cond_3
    iget-wide v8, p1, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->a:J

    move-wide v4, v8

    .line 2152419
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2152420
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2152421
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
