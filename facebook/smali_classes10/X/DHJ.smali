.class public final LX/DHJ;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DHK;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:Z

.field public f:I

.field public final synthetic g:LX/DHK;


# direct methods
.method public constructor <init>(LX/DHK;)V
    .locals 1

    .prologue
    .line 1981700
    iput-object p1, p0, LX/DHJ;->g:LX/DHK;

    .line 1981701
    move-object v0, p1

    .line 1981702
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1981703
    const/4 v0, 0x0

    iput v0, p0, LX/DHJ;->c:I

    .line 1981704
    const/4 v0, 0x1

    iput v0, p0, LX/DHJ;->d:I

    .line 1981705
    sget v0, LX/DHL;->a:I

    iput v0, p0, LX/DHJ;->f:I

    .line 1981706
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1981707
    const-string v0, "SingleCreatorSetTitleAndSubtitleComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1981708
    if-ne p0, p1, :cond_1

    .line 1981709
    :cond_0
    :goto_0
    return v0

    .line 1981710
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1981711
    goto :goto_0

    .line 1981712
    :cond_3
    check-cast p1, LX/DHJ;

    .line 1981713
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1981714
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1981715
    if-eq v2, v3, :cond_0

    .line 1981716
    iget-object v2, p0, LX/DHJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DHJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/DHJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1981717
    goto :goto_0

    .line 1981718
    :cond_5
    iget-object v2, p1, LX/DHJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1981719
    :cond_6
    iget-object v2, p0, LX/DHJ;->b:LX/1Pn;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DHJ;->b:LX/1Pn;

    iget-object v3, p1, LX/DHJ;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1981720
    goto :goto_0

    .line 1981721
    :cond_8
    iget-object v2, p1, LX/DHJ;->b:LX/1Pn;

    if-nez v2, :cond_7

    .line 1981722
    :cond_9
    iget v2, p0, LX/DHJ;->c:I

    iget v3, p1, LX/DHJ;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1981723
    goto :goto_0

    .line 1981724
    :cond_a
    iget v2, p0, LX/DHJ;->d:I

    iget v3, p1, LX/DHJ;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1981725
    goto :goto_0

    .line 1981726
    :cond_b
    iget-boolean v2, p0, LX/DHJ;->e:Z

    iget-boolean v3, p1, LX/DHJ;->e:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1981727
    goto :goto_0

    .line 1981728
    :cond_c
    iget v2, p0, LX/DHJ;->f:I

    iget v3, p1, LX/DHJ;->f:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1981729
    goto :goto_0
.end method
