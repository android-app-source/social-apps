.class public LX/Dpp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final duration_ms:Ljava/lang/Integer;

.field public final height:Ljava/lang/Integer;

.field public final rotation:Ljava/lang/Integer;

.field public final width:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2047434
    new-instance v0, LX/1sv;

    const-string v1, "VideoMetadata"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Dpp;->b:LX/1sv;

    .line 2047435
    new-instance v0, LX/1sw;

    const-string v1, "width"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpp;->c:LX/1sw;

    .line 2047436
    new-instance v0, LX/1sw;

    const-string v1, "height"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpp;->d:LX/1sw;

    .line 2047437
    new-instance v0, LX/1sw;

    const-string v1, "duration_ms"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpp;->e:LX/1sw;

    .line 2047438
    new-instance v0, LX/1sw;

    const-string v1, "rotation"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpp;->f:LX/1sw;

    .line 2047439
    const/4 v0, 0x1

    sput-boolean v0, LX/Dpp;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 2047300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047301
    iput-object p1, p0, LX/Dpp;->width:Ljava/lang/Integer;

    .line 2047302
    iput-object p2, p0, LX/Dpp;->height:Ljava/lang/Integer;

    .line 2047303
    iput-object p3, p0, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    .line 2047304
    iput-object p4, p0, LX/Dpp;->rotation:Ljava/lang/Integer;

    .line 2047305
    return-void
.end method

.method public static b(LX/1su;)LX/Dpp;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/16 v6, 0x8

    .line 2047414
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    move-object v2, v0

    move-object v3, v0

    .line 2047415
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v4

    .line 2047416
    iget-byte v5, v4, LX/1sw;->b:B

    if-eqz v5, :cond_4

    .line 2047417
    iget-short v5, v4, LX/1sw;->c:S

    packed-switch v5, :pswitch_data_0

    .line 2047418
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047419
    :pswitch_0
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v6, :cond_0

    .line 2047420
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 2047421
    :cond_0
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047422
    :pswitch_1
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v6, :cond_1

    .line 2047423
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 2047424
    :cond_1
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047425
    :pswitch_2
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v6, :cond_2

    .line 2047426
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 2047427
    :cond_2
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047428
    :pswitch_3
    iget-byte v5, v4, LX/1sw;->b:B

    if-ne v5, v6, :cond_3

    .line 2047429
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2047430
    :cond_3
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047431
    :cond_4
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2047432
    new-instance v4, LX/Dpp;

    invoke-direct {v4, v3, v2, v1, v0}, LX/Dpp;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2047433
    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2047365
    if-eqz p2, :cond_6

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2047366
    :goto_0
    if-eqz p2, :cond_7

    const-string v0, "\n"

    move-object v3, v0

    .line 2047367
    :goto_1
    if-eqz p2, :cond_8

    const-string v0, " "

    .line 2047368
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "VideoMetadata"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2047369
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047370
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047371
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047372
    const/4 v1, 0x1

    .line 2047373
    iget-object v6, p0, LX/Dpp;->width:Ljava/lang/Integer;

    if-eqz v6, :cond_0

    .line 2047374
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047375
    const-string v1, "width"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047376
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047377
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047378
    iget-object v1, p0, LX/Dpp;->width:Ljava/lang/Integer;

    if-nez v1, :cond_9

    .line 2047379
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 2047380
    :cond_0
    iget-object v6, p0, LX/Dpp;->height:Ljava/lang/Integer;

    if-eqz v6, :cond_2

    .line 2047381
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047382
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047383
    const-string v1, "height"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047384
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047385
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047386
    iget-object v1, p0, LX/Dpp;->height:Ljava/lang/Integer;

    if-nez v1, :cond_a

    .line 2047387
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 2047388
    :cond_2
    iget-object v6, p0, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    if-eqz v6, :cond_d

    .line 2047389
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047390
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047391
    const-string v1, "duration_ms"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047392
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047393
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047394
    iget-object v1, p0, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 2047395
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047396
    :goto_5
    iget-object v1, p0, LX/Dpp;->rotation:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 2047397
    if-nez v2, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047398
    :cond_4
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047399
    const-string v1, "rotation"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047400
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047401
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047402
    iget-object v0, p0, LX/Dpp;->rotation:Ljava/lang/Integer;

    if-nez v0, :cond_c

    .line 2047403
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047404
    :cond_5
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047405
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047406
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2047407
    :cond_6
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2047408
    :cond_7
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2047409
    :cond_8
    const-string v0, ""

    goto/16 :goto_2

    .line 2047410
    :cond_9
    iget-object v1, p0, LX/Dpp;->width:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2047411
    :cond_a
    iget-object v1, p0, LX/Dpp;->height:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2047412
    :cond_b
    iget-object v1, p0, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2047413
    :cond_c
    iget-object v0, p0, LX/Dpp;->rotation:Ljava/lang/Integer;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_d
    move v2, v1

    goto/16 :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2047345
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2047346
    iget-object v0, p0, LX/Dpp;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2047347
    iget-object v0, p0, LX/Dpp;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2047348
    sget-object v0, LX/Dpp;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2047349
    iget-object v0, p0, LX/Dpp;->width:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2047350
    :cond_0
    iget-object v0, p0, LX/Dpp;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2047351
    iget-object v0, p0, LX/Dpp;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2047352
    sget-object v0, LX/Dpp;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2047353
    iget-object v0, p0, LX/Dpp;->height:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2047354
    :cond_1
    iget-object v0, p0, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2047355
    iget-object v0, p0, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2047356
    sget-object v0, LX/Dpp;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2047357
    iget-object v0, p0, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2047358
    :cond_2
    iget-object v0, p0, LX/Dpp;->rotation:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2047359
    iget-object v0, p0, LX/Dpp;->rotation:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2047360
    sget-object v0, LX/Dpp;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2047361
    iget-object v0, p0, LX/Dpp;->rotation:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2047362
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2047363
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2047364
    return-void
.end method

.method public final a(LX/Dpp;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2047314
    if-nez p1, :cond_1

    .line 2047315
    :cond_0
    :goto_0
    return v2

    .line 2047316
    :cond_1
    iget-object v0, p0, LX/Dpp;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2047317
    :goto_1
    iget-object v3, p1, LX/Dpp;->width:Ljava/lang/Integer;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2047318
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 2047319
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2047320
    iget-object v0, p0, LX/Dpp;->width:Ljava/lang/Integer;

    iget-object v3, p1, LX/Dpp;->width:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2047321
    :cond_3
    iget-object v0, p0, LX/Dpp;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    move v0, v1

    .line 2047322
    :goto_3
    iget-object v3, p1, LX/Dpp;->height:Ljava/lang/Integer;

    if-eqz v3, :cond_d

    move v3, v1

    .line 2047323
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2047324
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2047325
    iget-object v0, p0, LX/Dpp;->height:Ljava/lang/Integer;

    iget-object v3, p1, LX/Dpp;->height:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2047326
    :cond_5
    iget-object v0, p0, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    move v0, v1

    .line 2047327
    :goto_5
    iget-object v3, p1, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    if-eqz v3, :cond_f

    move v3, v1

    .line 2047328
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2047329
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2047330
    iget-object v0, p0, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    iget-object v3, p1, LX/Dpp;->duration_ms:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2047331
    :cond_7
    iget-object v0, p0, LX/Dpp;->rotation:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    move v0, v1

    .line 2047332
    :goto_7
    iget-object v3, p1, LX/Dpp;->rotation:Ljava/lang/Integer;

    if-eqz v3, :cond_11

    move v3, v1

    .line 2047333
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2047334
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2047335
    iget-object v0, p0, LX/Dpp;->rotation:Ljava/lang/Integer;

    iget-object v3, p1, LX/Dpp;->rotation:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_9
    move v2, v1

    .line 2047336
    goto :goto_0

    :cond_a
    move v0, v2

    .line 2047337
    goto :goto_1

    :cond_b
    move v3, v2

    .line 2047338
    goto :goto_2

    :cond_c
    move v0, v2

    .line 2047339
    goto :goto_3

    :cond_d
    move v3, v2

    .line 2047340
    goto :goto_4

    :cond_e
    move v0, v2

    .line 2047341
    goto :goto_5

    :cond_f
    move v3, v2

    .line 2047342
    goto :goto_6

    :cond_10
    move v0, v2

    .line 2047343
    goto :goto_7

    :cond_11
    move v3, v2

    .line 2047344
    goto :goto_8
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2047310
    if-nez p1, :cond_1

    .line 2047311
    :cond_0
    :goto_0
    return v0

    .line 2047312
    :cond_1
    instance-of v1, p1, LX/Dpp;

    if-eqz v1, :cond_0

    .line 2047313
    check-cast p1, LX/Dpp;

    invoke-virtual {p0, p1}, LX/Dpp;->a(LX/Dpp;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2047309
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2047306
    sget-boolean v0, LX/Dpp;->a:Z

    .line 2047307
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Dpp;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2047308
    return-object v0
.end method
