.class public abstract LX/EWp;
.super LX/EWY;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static b:Z = false


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2131431
    const/4 v0, 0x0

    sput-boolean v0, LX/EWp;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2131432
    invoke-direct {p0}, LX/EWY;-><init>()V

    .line 2131433
    return-void
.end method

.method public constructor <init>(B)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)V"
        }
    .end annotation

    .prologue
    .line 2131434
    invoke-direct {p0}, LX/EWY;-><init>()V

    .line 2131435
    return-void
.end method

.method public static varargs b(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2131436
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 2131437
    :catch_0
    move-exception v0

    .line 2131438
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t use Java reflection to implement protocol message reflection."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2131439
    :catch_1
    move-exception v0

    .line 2131440
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 2131441
    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_0

    .line 2131442
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 2131443
    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    .line 2131444
    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 2131445
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected exception thrown by generated accessor method."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static varargs b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 4

    .prologue
    .line 2131446
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2131447
    :catch_0
    move-exception v0

    .line 2131448
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Generated message class \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" missing method \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\"."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static j(LX/EWp;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131449
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 2131450
    invoke-virtual {p0}, LX/EWp;->h()LX/EYn;

    move-result-object v0

    iget-object v0, v0, LX/EYn;->a:LX/EYF;

    .line 2131451
    invoke-virtual {v0}, LX/EYF;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2131452
    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2131453
    invoke-virtual {p0, v0}, LX/EWp;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2131454
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2131455
    invoke-virtual {v2, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2131456
    :cond_1
    invoke-virtual {p0, v0}, LX/EWp;->a(LX/EYP;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2131457
    invoke-virtual {p0, v0}, LX/EWp;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2131458
    :cond_2
    return-object v2
.end method


# virtual methods
.method public E()V
    .locals 0

    .prologue
    .line 2131410
    return-void
.end method

.method public abstract a(LX/EYd;)LX/EWU;
.end method

.method public a()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2131418
    invoke-virtual {p0}, LX/EWp;->e()LX/EYF;

    move-result-object v0

    invoke-virtual {v0}, LX/EYF;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2131419
    invoke-virtual {v0}, LX/EYP;->k()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2131420
    invoke-virtual {p0, v0}, LX/EWp;->a(LX/EYP;)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 2131421
    :goto_0
    return v0

    .line 2131422
    :cond_1
    invoke-virtual {v0}, LX/EYP;->f()LX/EYN;

    move-result-object v3

    sget-object v4, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v3, v4, :cond_0

    .line 2131423
    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2131424
    invoke-virtual {p0, v0}, LX/EWp;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2131425
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    .line 2131426
    invoke-interface {v0}, LX/EWQ;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2131427
    goto :goto_0

    .line 2131428
    :cond_3
    invoke-virtual {p0, v0}, LX/EWp;->a(LX/EYP;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v0}, LX/EWp;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    invoke-interface {v0}, LX/EWQ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2131429
    goto :goto_0

    .line 2131430
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(LX/EWd;LX/EZM;LX/EYZ;I)Z
    .locals 1

    .prologue
    .line 2131459
    invoke-virtual {p2, p4, p1}, LX/EZM;->a(ILX/EWd;)Z

    move-result v0

    return v0
.end method

.method public a(LX/EYP;)Z
    .locals 1

    .prologue
    .line 2131417
    invoke-virtual {p0}, LX/EWp;->h()LX/EYn;

    move-result-object v0

    invoke-static {v0, p1}, LX/EYn;->a$redex0(LX/EYn;LX/EYP;)LX/EYg;

    move-result-object v0

    invoke-interface {v0, p0}, LX/EYg;->b(LX/EWp;)Z

    move-result v0

    return v0
.end method

.method public b(LX/EYP;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2131416
    invoke-virtual {p0}, LX/EWp;->h()LX/EYn;

    move-result-object v0

    invoke-static {v0, p1}, LX/EYn;->a$redex0(LX/EYn;LX/EYP;)LX/EYg;

    move-result-object v0

    invoke-interface {v0, p0}, LX/EYg;->a(LX/EWp;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2131415
    invoke-virtual {p0}, LX/EWp;->h()LX/EYn;

    move-result-object v0

    iget-object v0, v0, LX/EYn;->a:LX/EYF;

    return-object v0
.end method

.method public g()LX/EZQ;
    .locals 2

    .prologue
    .line 2131414
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract h()LX/EYn;
.end method

.method public i()LX/EWZ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<+",
            "Lcom/google/protobuf/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131413
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public kb_()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131412
    invoke-static {p0}, LX/EWp;->j(LX/EWp;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2131411
    new-instance v0, LX/EYo;

    invoke-direct {v0, p0}, LX/EYo;-><init>(LX/EWW;)V

    return-object v0
.end method
