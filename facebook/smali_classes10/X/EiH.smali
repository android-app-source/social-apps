.class public LX/EiH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/confirmation/model/AccountConfirmationData;

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/EiG;",
            "LX/EiI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/model/AccountConfirmationData;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2159241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2159242
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LX/EiG;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LX/EiH;->b:Ljava/util/Map;

    .line 2159243
    iput-object p1, p0, LX/EiH;->a:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159244
    iget-object v0, p0, LX/EiH;->b:Ljava/util/Map;

    sget-object v1, LX/EiG;->EMAIL_ACQUIRED:LX/EiG;

    new-instance v2, LX/EiI;

    const-class p1, Lcom/facebook/confirmation/fragment/ConfEmailCodeInputFragment;

    invoke-direct {v2, p1}, LX/EiI;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v2}, LX/EiI;->b()LX/EiI;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2159245
    iget-object v0, p0, LX/EiH;->b:Ljava/util/Map;

    sget-object v1, LX/EiG;->PHONE_ACQUIRED:LX/EiG;

    new-instance v2, LX/EiI;

    const-class p1, Lcom/facebook/confirmation/fragment/ConfPhoneCodeInputFragment;

    invoke-direct {v2, p1}, LX/EiI;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v2}, LX/EiI;->b()LX/EiI;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2159246
    iget-object v0, p0, LX/EiH;->b:Ljava/util/Map;

    sget-object v1, LX/EiG;->UPDATE_EMAIL:LX/EiG;

    new-instance v2, LX/EiI;

    const-class p1, Lcom/facebook/confirmation/fragment/ConfEmailFragment;

    invoke-direct {v2, p1}, LX/EiI;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v2}, LX/EiI;->a()LX/EiI;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2159247
    iget-object v0, p0, LX/EiH;->b:Ljava/util/Map;

    sget-object v1, LX/EiG;->UPDATE_PHONE:LX/EiG;

    new-instance v2, LX/EiI;

    const-class p1, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;

    invoke-direct {v2, p1}, LX/EiI;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v2}, LX/EiI;->a()LX/EiI;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2159248
    iget-object v0, p0, LX/EiH;->b:Ljava/util/Map;

    sget-object v1, LX/EiG;->PHONE_SWITCH_TO_EMAIL:LX/EiG;

    new-instance v2, LX/EiI;

    const-class p1, Lcom/facebook/confirmation/fragment/ConfEmailFragment;

    invoke-direct {v2, p1}, LX/EiI;-><init>(Ljava/lang/Class;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2159249
    iget-object v0, p0, LX/EiH;->b:Ljava/util/Map;

    sget-object v1, LX/EiG;->EMAIL_SWITCH_TO_PHONE:LX/EiG;

    new-instance v2, LX/EiI;

    const-class p1, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;

    invoke-direct {v2, p1}, LX/EiI;-><init>(Ljava/lang/Class;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2159250
    return-void
.end method
