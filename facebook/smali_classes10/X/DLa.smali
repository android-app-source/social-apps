.class public final enum LX/DLa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DLa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DLa;

.field public static final enum FETCH_GROUP_DOCS_AND_FILES:LX/DLa;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1988378
    new-instance v0, LX/DLa;

    const-string v1, "FETCH_GROUP_DOCS_AND_FILES"

    invoke-direct {v0, v1, v2}, LX/DLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DLa;->FETCH_GROUP_DOCS_AND_FILES:LX/DLa;

    .line 1988379
    const/4 v0, 0x1

    new-array v0, v0, [LX/DLa;

    sget-object v1, LX/DLa;->FETCH_GROUP_DOCS_AND_FILES:LX/DLa;

    aput-object v1, v0, v2

    sput-object v0, LX/DLa;->$VALUES:[LX/DLa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1988380
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DLa;
    .locals 1

    .prologue
    .line 1988381
    const-class v0, LX/DLa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DLa;

    return-object v0
.end method

.method public static values()[LX/DLa;
    .locals 1

    .prologue
    .line 1988382
    sget-object v0, LX/DLa;->$VALUES:[LX/DLa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DLa;

    return-object v0
.end method
