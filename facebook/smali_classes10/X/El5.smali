.class public abstract LX/El5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/El0;


# instance fields
.field private volatile a:Lorg/apache/http/HttpMessage;

.field private volatile b:[Lorg/apache/http/Header;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2164018
    iget-object v0, p0, LX/El5;->b:[Lorg/apache/http/Header;

    aget-object v0, v0, p1

    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2164019
    iget-object v0, p0, LX/El5;->a:Lorg/apache/http/HttpMessage;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpMessage;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 2164020
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lorg/apache/http/HttpMessage;)V
    .locals 1

    .prologue
    .line 2164021
    iput-object p1, p0, LX/El5;->a:Lorg/apache/http/HttpMessage;

    .line 2164022
    invoke-interface {p1}, Lorg/apache/http/HttpMessage;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    iput-object v0, p0, LX/El5;->b:[Lorg/apache/http/Header;

    .line 2164023
    return-void
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2164024
    iget-object v0, p0, LX/El5;->b:[Lorg/apache/http/Header;

    aget-object v0, v0, p1

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2164025
    iget-object v0, p0, LX/El5;->b:[Lorg/apache/http/Header;

    array-length v0, v0

    return v0
.end method
