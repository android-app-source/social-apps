.class public final synthetic LX/D6z;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I

.field public static final synthetic d:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1966678
    invoke-static {}, LX/2qV;->values()[LX/2qV;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/D6z;->d:[I

    :try_start_0
    sget-object v0, LX/D6z;->d:[I

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_10

    :goto_0
    :try_start_1
    sget-object v0, LX/D6z;->d:[I

    sget-object v1, LX/2qV;->ATTEMPT_TO_PAUSE:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_f

    :goto_1
    :try_start_2
    sget-object v0, LX/D6z;->d:[I

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_e

    :goto_2
    :try_start_3
    sget-object v0, LX/D6z;->d:[I

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_d

    .line 1966679
    :goto_3
    invoke-static {}, LX/2oN;->values()[LX/2oN;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/D6z;->c:[I

    :try_start_4
    sget-object v0, LX/D6z;->c:[I

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_c

    :goto_4
    :try_start_5
    sget-object v0, LX/D6z;->c:[I

    sget-object v1, LX/2oN;->TRANSITION:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_b

    :goto_5
    :try_start_6
    sget-object v0, LX/D6z;->c:[I

    sget-object v1, LX/2oN;->WAIT_FOR_ADS:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_a

    :goto_6
    :try_start_7
    sget-object v0, LX/D6z;->c:[I

    sget-object v1, LX/2oN;->VOD_NO_VIDEO_AD:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_9

    :goto_7
    :try_start_8
    sget-object v0, LX/D6z;->c:[I

    sget-object v1, LX/2oN;->STATIC_COUNTDOWN:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :goto_8
    :try_start_9
    sget-object v0, LX/D6z;->c:[I

    sget-object v1, LX/2oN;->VIDEO_AD:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_7

    .line 1966680
    :goto_9
    invoke-static {}, LX/3H0;->values()[LX/3H0;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/D6z;->b:[I

    :try_start_a
    sget-object v0, LX/D6z;->b:[I

    sget-object v1, LX/3H0;->LIVE:LX/3H0;

    invoke-virtual {v1}, LX/3H0;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_6

    :goto_a
    :try_start_b
    sget-object v0, LX/D6z;->b:[I

    sget-object v1, LX/3H0;->NONLIVE:LX/3H0;

    invoke-virtual {v1}, LX/3H0;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_5

    :goto_b
    :try_start_c
    sget-object v0, LX/D6z;->b:[I

    sget-object v1, LX/3H0;->NONLIVE_POST_ROLL:LX/3H0;

    invoke-virtual {v1}, LX/3H0;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_4

    :goto_c
    :try_start_d
    sget-object v0, LX/D6z;->b:[I

    sget-object v1, LX/3H0;->VOD:LX/3H0;

    invoke-virtual {v1}, LX/3H0;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_3

    .line 1966681
    :goto_d
    invoke-static {}, LX/04G;->values()[LX/04G;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/D6z;->a:[I

    :try_start_e
    sget-object v0, LX/D6z;->a:[I

    sget-object v1, LX/04G;->INLINE_PLAYER:LX/04G;

    invoke-virtual {v1}, LX/04G;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_2

    :goto_e
    :try_start_f
    sget-object v0, LX/D6z;->a:[I

    sget-object v1, LX/04G;->CHANNEL_PLAYER:LX/04G;

    invoke-virtual {v1}, LX/04G;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_1

    :goto_f
    :try_start_10
    sget-object v0, LX/D6z;->a:[I

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-virtual {v1}, LX/04G;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_0

    :goto_10
    return-void

    :catch_0
    goto :goto_10

    :catch_1
    goto :goto_f

    :catch_2
    goto :goto_e

    :catch_3
    goto :goto_d

    :catch_4
    goto :goto_c

    :catch_5
    goto :goto_b

    :catch_6
    goto :goto_a

    :catch_7
    goto :goto_9

    :catch_8
    goto :goto_8

    :catch_9
    goto :goto_7

    :catch_a
    goto/16 :goto_6

    :catch_b
    goto/16 :goto_5

    :catch_c
    goto/16 :goto_4

    :catch_d
    goto/16 :goto_3

    :catch_e
    goto/16 :goto_2

    :catch_f
    goto/16 :goto_1

    :catch_10
    goto/16 :goto_0
.end method
