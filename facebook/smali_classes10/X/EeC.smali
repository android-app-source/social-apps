.class public final LX/EeC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2152101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2152102
    new-instance v0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;

    invoke-direct {v0, p1}, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2152103
    new-array v0, p1, [Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;

    return-object v0
.end method
