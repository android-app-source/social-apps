.class public LX/Ebh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/Ebj;

.field public b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/Ebj;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2144231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144232
    new-instance v0, LX/Ebj;

    invoke-direct {v0}, LX/Ebj;-><init>()V

    iput-object v0, p0, LX/Ebh;->a:LX/Ebj;

    .line 2144233
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Ebh;->b:Ljava/util/LinkedList;

    .line 2144234
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ebh;->c:Z

    .line 2144235
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ebh;->c:Z

    .line 2144236
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2144259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144260
    new-instance v0, LX/Ebj;

    invoke-direct {v0}, LX/Ebj;-><init>()V

    iput-object v0, p0, LX/Ebh;->a:LX/Ebj;

    .line 2144261
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Ebh;->b:Ljava/util/LinkedList;

    .line 2144262
    iput-boolean v3, p0, LX/Ebh;->c:Z

    .line 2144263
    sget-object v0, LX/Ebx;->a:LX/EWZ;

    invoke-virtual {v0, p1}, LX/EWZ;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebx;

    move-object v0, v0

    .line 2144264
    new-instance v1, LX/Ebj;

    .line 2144265
    iget-object v2, v0, LX/Ebx;->currentSession_:LX/Ecf;

    move-object v2, v2

    .line 2144266
    invoke-direct {v1, v2}, LX/Ebj;-><init>(LX/Ecf;)V

    iput-object v1, p0, LX/Ebh;->a:LX/Ebj;

    .line 2144267
    iput-boolean v3, p0, LX/Ebh;->c:Z

    .line 2144268
    iget-object v1, v0, LX/Ebx;->previousSessions_:Ljava/util/List;

    move-object v0, v1

    .line 2144269
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ecf;

    .line 2144270
    iget-object v2, p0, LX/Ebh;->b:Ljava/util/LinkedList;

    new-instance v3, LX/Ebj;

    invoke-direct {v3, v0}, LX/Ebj;-><init>(LX/Ecf;)V

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2144271
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/Ebj;)V
    .locals 2

    .prologue
    .line 2144254
    iget-object v0, p0, LX/Ebh;->b:Ljava/util/LinkedList;

    iget-object v1, p0, LX/Ebh;->a:LX/Ebj;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 2144255
    iput-object p1, p0, LX/Ebh;->a:LX/Ebj;

    .line 2144256
    iget-object v0, p0, LX/Ebh;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x28

    if-le v0, v1, :cond_0

    .line 2144257
    iget-object v0, p0, LX/Ebh;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 2144258
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2144252
    new-instance v0, LX/Ebj;

    invoke-direct {v0}, LX/Ebj;-><init>()V

    invoke-virtual {p0, v0}, LX/Ebh;->a(LX/Ebj;)V

    .line 2144253
    return-void
.end method

.method public final e()[B
    .locals 4

    .prologue
    .line 2144237
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 2144238
    iget-object v0, p0, LX/Ebh;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebj;

    .line 2144239
    iget-object v3, v0, LX/Ebj;->a:LX/Ecf;

    move-object v0, v3

    .line 2144240
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2144241
    :cond_0
    invoke-static {}, LX/Ebw;->u()LX/Ebw;

    move-result-object v0

    iget-object v2, p0, LX/Ebh;->a:LX/Ebj;

    .line 2144242
    iget-object v3, v2, LX/Ebj;->a:LX/Ecf;

    move-object v2, v3

    .line 2144243
    invoke-virtual {v0, v2}, LX/Ebw;->a(LX/Ecf;)LX/Ebw;

    move-result-object v0

    .line 2144244
    iget-object v2, v0, LX/Ebw;->e:LX/EZ2;

    if-nez v2, :cond_1

    .line 2144245
    invoke-static {v0}, LX/Ebw;->A(LX/Ebw;)V

    .line 2144246
    iget-object v2, v0, LX/Ebw;->d:Ljava/util/List;

    invoke-static {v1, v2}, LX/EWS;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 2144247
    invoke-virtual {v0}, LX/EWj;->t()V

    .line 2144248
    :goto_1
    move-object v0, v0

    .line 2144249
    invoke-virtual {v0}, LX/Ebw;->l()LX/Ebx;

    move-result-object v0

    .line 2144250
    invoke-virtual {v0}, LX/EWX;->jZ_()[B

    move-result-object v0

    return-object v0

    .line 2144251
    :cond_1
    iget-object v2, v0, LX/Ebw;->e:LX/EZ2;

    invoke-virtual {v2, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_1
.end method
