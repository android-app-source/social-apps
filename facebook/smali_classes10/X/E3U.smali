.class public final LX/E3U;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/E3V;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View$OnClickListener;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View$OnClickListener;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2074605
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2074606
    iput-object p1, p0, LX/E3U;->a:Landroid/content/Context;

    .line 2074607
    iput-object p2, p0, LX/E3U;->b:Ljava/util/List;

    .line 2074608
    iput-object p3, p0, LX/E3U;->c:Ljava/util/List;

    .line 2074609
    return-void
.end method

.method private d()I
    .locals 2

    .prologue
    .line 2074610
    iget-object v0, p0, LX/E3U;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1649

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2074611
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03114d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2074612
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {p0}, LX/E3U;->d()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2074613
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {p0}, LX/E3U;->d()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2074614
    const v1, 0x7f0d28e3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2074615
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2074616
    new-instance v1, LX/E3V;

    invoke-direct {v1, v0}, LX/E3V;-><init>(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2074617
    check-cast p1, LX/E3V;

    .line 2074618
    iget-object v0, p0, LX/E3U;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p0, LX/E3U;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View$OnClickListener;

    .line 2074619
    iget-object p0, p1, LX/E3V;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object p2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionFacepileHScrollUnitComponentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2074620
    iget-object p0, p1, LX/E3V;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2074621
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2074622
    iget-object v0, p0, LX/E3U;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
