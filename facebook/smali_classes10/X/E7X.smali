.class public abstract LX/E7X;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "LX/E7V;",
        ">",
        "LX/1OM",
        "<TVH;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1U8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2081746
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2081747
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E7X;->a:Ljava/util/List;

    .line 2081748
    return-void
.end method


# virtual methods
.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 2081749
    check-cast p1, LX/E7V;

    .line 2081750
    iget-object v0, p0, LX/E7X;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    invoke-virtual {p1, v0}, LX/E7V;->a(LX/1U8;)V

    .line 2081751
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/1U8;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2081752
    iget-object v0, p0, LX/E7X;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2081753
    iget-object v1, p0, LX/E7X;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2081754
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/1OM;->c(II)V

    .line 2081755
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2081756
    iget-object v0, p0, LX/E7X;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
