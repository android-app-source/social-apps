.class public LX/DCI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0bH;

.field public final b:LX/967;

.field private c:LX/DCH;

.field private d:LX/DCG;

.field public e:LX/0qq;

.field public f:LX/0g4;

.field public final g:LX/1L9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1L9",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0bH;LX/967;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1973893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1973894
    new-instance v0, LX/DCF;

    invoke-direct {v0, p0}, LX/DCF;-><init>(LX/DCI;)V

    iput-object v0, p0, LX/DCI;->g:LX/1L9;

    .line 1973895
    iput-object p1, p0, LX/DCI;->a:LX/0bH;

    .line 1973896
    iput-object p2, p0, LX/DCI;->b:LX/967;

    .line 1973897
    return-void
.end method

.method public static a(LX/0QB;)LX/DCI;
    .locals 3

    .prologue
    .line 1973910
    new-instance v2, LX/DCI;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v0

    check-cast v0, LX/0bH;

    invoke-static {p0}, LX/967;->b(LX/0QB;)LX/967;

    move-result-object v1

    check-cast v1, LX/967;

    invoke-direct {v2, v0, v1}, LX/DCI;-><init>(LX/0bH;LX/967;)V

    .line 1973911
    move-object v0, v2

    .line 1973912
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1973905
    iget-object v0, p0, LX/DCI;->d:LX/DCG;

    if-eqz v0, :cond_0

    .line 1973906
    iget-object v0, p0, LX/DCI;->a:LX/0bH;

    iget-object v1, p0, LX/DCI;->d:LX/DCG;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1973907
    :cond_0
    iget-object v0, p0, LX/DCI;->c:LX/DCH;

    if-eqz v0, :cond_1

    .line 1973908
    iget-object v0, p0, LX/DCI;->a:LX/0bH;

    iget-object v1, p0, LX/DCI;->c:LX/DCH;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1973909
    :cond_1
    return-void
.end method

.method public final a(LX/0qq;LX/0g4;)V
    .locals 2
    .param p2    # LX/0g4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1973898
    new-instance v0, LX/DCG;

    invoke-direct {v0, p0}, LX/DCG;-><init>(LX/DCI;)V

    iput-object v0, p0, LX/DCI;->d:LX/DCG;

    .line 1973899
    new-instance v0, LX/DCH;

    invoke-direct {v0, p0}, LX/DCH;-><init>(LX/DCI;)V

    iput-object v0, p0, LX/DCI;->c:LX/DCH;

    .line 1973900
    iget-object v0, p0, LX/DCI;->a:LX/0bH;

    iget-object v1, p0, LX/DCI;->d:LX/DCG;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1973901
    iget-object v0, p0, LX/DCI;->a:LX/0bH;

    iget-object v1, p0, LX/DCI;->c:LX/DCH;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1973902
    iput-object p1, p0, LX/DCI;->e:LX/0qq;

    .line 1973903
    iput-object p2, p0, LX/DCI;->f:LX/0g4;

    .line 1973904
    return-void
.end method
