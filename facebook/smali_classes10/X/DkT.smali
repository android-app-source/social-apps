.class public final LX/DkT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/Dka;


# direct methods
.method public constructor <init>(LX/Dka;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2034265
    iput-object p1, p0, LX/DkT;->e:LX/Dka;

    iput-object p2, p0, LX/DkT;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DkT;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DkT;->c:Ljava/lang/String;

    iput-object p5, p0, LX/DkT;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2034266
    iget-object v0, p0, LX/DkT;->e:LX/Dka;

    iget-object v1, p0, LX/DkT;->a:Ljava/lang/String;

    iget-object v2, p0, LX/DkT;->b:Ljava/lang/String;

    iget-object v3, p0, LX/DkT;->c:Ljava/lang/String;

    iget-object v4, p0, LX/DkT;->d:Ljava/lang/String;

    .line 2034267
    invoke-static {}, LX/DlU;->a()LX/DlT;

    move-result-object v6

    .line 2034268
    if-nez v4, :cond_0

    new-instance v5, LX/4HK;

    invoke-direct {v5}, LX/4HK;-><init>()V

    invoke-virtual {v5, v1}, LX/4HK;->a(Ljava/lang/String;)LX/4HK;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/4HK;->b(Ljava/lang/String;)LX/4HK;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/4HK;->c(Ljava/lang/String;)LX/4HK;

    move-result-object v5

    const-string p0, "DECLINE"

    invoke-virtual {v5, p0}, LX/4HK;->d(Ljava/lang/String;)LX/4HK;

    move-result-object v5

    .line 2034269
    :goto_0
    const-string p0, "input"

    invoke-virtual {v6, p0, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2034270
    iget-object v5, v0, LX/Dka;->a:LX/0tX;

    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 2034271
    return-object v0

    .line 2034272
    :cond_0
    new-instance v5, LX/4HK;

    invoke-direct {v5}, LX/4HK;-><init>()V

    invoke-virtual {v5, v1}, LX/4HK;->a(Ljava/lang/String;)LX/4HK;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/4HK;->b(Ljava/lang/String;)LX/4HK;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/4HK;->c(Ljava/lang/String;)LX/4HK;

    move-result-object v5

    const-string p0, "DECLINE"

    invoke-virtual {v5, p0}, LX/4HK;->d(Ljava/lang/String;)LX/4HK;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/4HK;->f(Ljava/lang/String;)LX/4HK;

    move-result-object v5

    goto :goto_0
.end method
