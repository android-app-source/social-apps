.class public LX/DbB;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DbB;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2016925
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2016926
    const-string v0, "2g_empathy/{%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "enable"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2016927
    const-string v0, "2g_empathy/{%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "disable"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/http/prefs/delaybasedqp/DelayBasedHttpQPActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2016928
    return-void
.end method

.method public static a(LX/0QB;)LX/DbB;
    .locals 3

    .prologue
    .line 2016929
    sget-object v0, LX/DbB;->a:LX/DbB;

    if-nez v0, :cond_1

    .line 2016930
    const-class v1, LX/DbB;

    monitor-enter v1

    .line 2016931
    :try_start_0
    sget-object v0, LX/DbB;->a:LX/DbB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2016932
    if-eqz v2, :cond_0

    .line 2016933
    :try_start_1
    new-instance v0, LX/DbB;

    invoke-direct {v0}, LX/DbB;-><init>()V

    .line 2016934
    move-object v0, v0

    .line 2016935
    sput-object v0, LX/DbB;->a:LX/DbB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2016936
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2016937
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2016938
    :cond_1
    sget-object v0, LX/DbB;->a:LX/DbB;

    return-object v0

    .line 2016939
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2016940
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
