.class public LX/D6U;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0w9;

.field private final b:LX/0tX;

.field private final c:LX/0wp;

.field private final d:LX/0tQ;


# direct methods
.method public constructor <init>(LX/0tQ;LX/0w9;LX/0tX;LX/0wp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1965747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1965748
    iput-object p1, p0, LX/D6U;->d:LX/0tQ;

    .line 1965749
    iput-object p2, p0, LX/D6U;->a:LX/0w9;

    .line 1965750
    iput-object p3, p0, LX/D6U;->b:LX/0tX;

    .line 1965751
    iput-object p4, p0, LX/D6U;->c:LX/0wp;

    .line 1965752
    return-void
.end method

.method public static a(LX/D6U;LX/0gW;)LX/0gW;
    .locals 2

    .prologue
    .line 1965763
    invoke-static {p1}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 1965764
    iget-object v0, p0, LX/D6U;->a:LX/0w9;

    invoke-virtual {v0, p1}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 1965765
    const-string v0, "caller"

    const-string v1, "CHANNEL_VIEW_FROM_NEWSFEED"

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1965766
    const-string v0, "enable_hd"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1965767
    const-string v0, "enable_download"

    iget-object v1, p0, LX/D6U;->d:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1965768
    const/4 v0, 0x1

    move v0, v0

    .line 1965769
    if-eqz v0, :cond_0

    .line 1965770
    const-string v0, "scrubbing"

    const-string v1, "MPEG_DASH"

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1965771
    :cond_0
    return-object p1
.end method

.method public static a(LX/D6U;LX/0gW;Z)LX/1Zp;
    .locals 4

    .prologue
    .line 1965755
    if-eqz p2, :cond_1

    sget-object v0, LX/0zS;->c:LX/0zS;

    .line 1965756
    :goto_0
    invoke-static {p1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1965757
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1965758
    move-object v0, v0

    .line 1965759
    if-nez p2, :cond_0

    .line 1965760
    const-wide/16 v2, 0x708

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    .line 1965761
    :cond_0
    iget-object v1, p0, LX/D6U;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0

    .line 1965762
    :cond_1
    sget-object v0, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/D6U;
    .locals 5

    .prologue
    .line 1965753
    new-instance v4, LX/D6U;

    invoke-static {p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v0

    check-cast v0, LX/0tQ;

    invoke-static {p0}, LX/0w9;->a(LX/0QB;)LX/0w9;

    move-result-object v1

    check-cast v1, LX/0w9;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v3

    check-cast v3, LX/0wp;

    invoke-direct {v4, v0, v1, v2, v3}, LX/D6U;-><init>(LX/0tQ;LX/0w9;LX/0tX;LX/0wp;)V

    .line 1965754
    return-object v4
.end method
