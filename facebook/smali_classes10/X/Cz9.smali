.class public final LX/Cz9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "LX/Cz3;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CzA;

.field private b:I

.field private c:LX/Cz3;


# direct methods
.method public constructor <init>(LX/CzA;)V
    .locals 1

    .prologue
    .line 1954191
    iput-object p1, p0, LX/Cz9;->a:LX/CzA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954192
    const/4 v0, -0x1

    iput v0, p0, LX/Cz9;->b:I

    .line 1954193
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cz9;->c:LX/Cz3;

    .line 1954194
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/Cz9;->a(I)V

    .line 1954195
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 1954183
    :goto_0
    iget-object v0, p0, LX/Cz9;->a:LX/CzA;

    iget-object v0, v0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 1954184
    iget-object v0, p0, LX/Cz9;->a:LX/CzA;

    iget-object v0, v0, LX/CzA;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyv;

    .line 1954185
    instance-of v1, v0, LX/Cz3;

    if-eqz v1, :cond_0

    .line 1954186
    iput p1, p0, LX/Cz9;->b:I

    .line 1954187
    check-cast v0, LX/Cz3;

    iput-object v0, p0, LX/Cz9;->c:LX/Cz3;

    .line 1954188
    :goto_1
    return-void

    .line 1954189
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1954190
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cz9;->c:LX/Cz3;

    goto :goto_1
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 1954182
    iget-object v0, p0, LX/Cz9;->c:LX/Cz3;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1954178
    iget-object v0, p0, LX/Cz9;->c:LX/Cz3;

    .line 1954179
    iget v1, p0, LX/Cz9;->b:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, LX/Cz9;->a(I)V

    .line 1954180
    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 1954181
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Removing is not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
