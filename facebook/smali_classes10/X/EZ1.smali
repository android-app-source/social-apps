.class public final LX/EZ1;
.super Ljava/util/AbstractList;
.source ""

# interfaces
.implements Ljava/util/List;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MType:",
        "LX/EWp;",
        "BType:",
        "LX/EWj;",
        "IType::",
        "LX/EWT;",
        ">",
        "Ljava/util/AbstractList",
        "<TIType;>;",
        "Ljava/util/List",
        "<TIType;>;"
    }
.end annotation


# instance fields
.field public a:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation
.end field


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2138355
    iget v0, p0, Ljava/util/AbstractList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EZ1;->modCount:I

    .line 2138356
    return-void
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2138357
    iget-object v0, p0, LX/EZ1;->a:LX/EZ2;

    .line 2138358
    iget-object p0, v0, LX/EZ2;->d:Ljava/util/List;

    if-nez p0, :cond_0

    .line 2138359
    iget-object p0, v0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EWT;

    .line 2138360
    :goto_0
    move-object v0, p0

    .line 2138361
    return-object v0

    .line 2138362
    :cond_0
    iget-object p0, v0, LX/EZ2;->d:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EZ7;

    .line 2138363
    if-nez p0, :cond_1

    .line 2138364
    iget-object p0, v0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EWT;

    goto :goto_0

    .line 2138365
    :cond_1
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    if-eqz v0, :cond_2

    .line 2138366
    iget-object v0, p0, LX/EZ7;->b:LX/EWj;

    .line 2138367
    :goto_1
    move-object p0, v0

    .line 2138368
    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/EZ7;->c:LX/EWp;

    goto :goto_1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2138369
    iget-object v0, p0, LX/EZ1;->a:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->c()I

    move-result v0

    return v0
.end method
