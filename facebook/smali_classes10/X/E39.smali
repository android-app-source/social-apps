.class public LX/E39;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2073652
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E39;->a:Ljava/util/List;

    .line 2073653
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2073649
    iget-object v0, p0, LX/E39;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2073650
    iget-object v0, p0, LX/E39;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
