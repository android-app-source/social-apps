.class public LX/E8O;
.super LX/E8N;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "LX/1a1;",
        ">",
        "LX/E8N",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionStoryAttachmentFragment;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/Cft;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Cft",
            "<TVH;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionStoryAttachmentFragment;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/E6V;

.field private final d:Lcom/facebook/reaction/ReactionUtil;

.field private final e:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/Cft;Ljava/lang/String;LX/E6V;Ljava/lang/String;Ljava/lang/String;LX/0Sh;LX/2j3;Lcom/facebook/reaction/ReactionUtil;)V
    .locals 1
    .param p1    # LX/Cft;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/E6V;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cft",
            "<TVH;>;",
            "Ljava/lang/String;",
            "LX/E6V;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/2j3;",
            "Lcom/facebook/reaction/ReactionUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2082391
    invoke-direct {p0, p6, p7, p4, p5}, LX/E8N;-><init>(LX/0Sh;LX/2j3;Ljava/lang/String;Ljava/lang/String;)V

    .line 2082392
    iput-object p1, p0, LX/E8O;->a:LX/Cft;

    .line 2082393
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/E8O;->b:Ljava/util/List;

    .line 2082394
    iput-object p3, p0, LX/E8O;->c:LX/E6V;

    .line 2082395
    iput-object p8, p0, LX/E8O;->d:Lcom/facebook/reaction/ReactionUtil;

    .line 2082396
    iput-object p5, p0, LX/E8O;->e:Ljava/lang/String;

    .line 2082397
    iput-object p2, p0, LX/E8O;->f:Ljava/lang/String;

    .line 2082398
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0us;
    .locals 1

    .prologue
    .line 2082399
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;

    .line 2082400
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0Ve;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2082401
    iget-object v0, p0, LX/E8O;->d:Lcom/facebook/reaction/ReactionUtil;

    const/16 v3, 0xa

    iget-object v4, p0, LX/E8O;->f:Ljava/lang/String;

    iget-object v5, p0, LX/E8O;->e:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    .line 2082402
    invoke-static {}, LX/9qR;->c()LX/9qN;

    move-result-object p0

    const-string p1, "reaction_story_id"

    invoke-virtual {p0, p1, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p0

    const-string p1, "reaction_after_cursor"

    invoke-virtual {p0, p1, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p0

    const-string p1, "reaction_result_count"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p0

    check-cast p0, LX/9qN;

    .line 2082403
    invoke-virtual {v0, p0, v5}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 2082404
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    .line 2082405
    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 2082406
    iput-object v5, p0, LX/0zO;->z:Ljava/lang/String;

    .line 2082407
    :cond_0
    iget-object p1, v0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    .line 2082408
    iget-object p1, v0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    invoke-virtual {p1, v4, p0, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2082409
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2082410
    check-cast p1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;

    .line 2082411
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;

    .line 2082412
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionMoreAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v0

    .line 2082413
    if-eqz v0, :cond_0

    iget-object v4, p0, LX/E8O;->a:LX/Cft;

    invoke-virtual {v4, v0}, LX/Cft;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2082414
    iget-object v4, p0, LX/E8O;->b:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2082415
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2082416
    :cond_1
    iget-object v0, p0, LX/E8O;->c:LX/E6V;

    invoke-interface {v0}, LX/E6V;->d()V

    .line 2082417
    return-void
.end method
