.class public LX/Dud;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2057532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/p2p/model/PaymentCard;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2057527
    instance-of v0, p1, Lcom/facebook/payments/p2p/model/PartialPaymentCard;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/payments/p2p/model/PaymentCard;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2057528
    iget-object v1, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    iget-object v1, v1, LX/DtH;->cardType:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2057529
    const-string v2, "%s %s \u2022%s"

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->j:LX/DtH;

    iget-object v1, v1, LX/DtH;->cardType:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->f()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, v3, v1, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2057530
    :goto_1
    move-object v0, v1

    .line 2057531
    goto :goto_0

    :cond_1
    invoke-virtual {p1, v0}, Lcom/facebook/payments/p2p/model/PaymentCard;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
