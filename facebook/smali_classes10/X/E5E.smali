.class public LX/E5E;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E5E;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E5F;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E5E",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E5F;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078079
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078080
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/E5E;->b:LX/0Zi;

    .line 2078081
    iput-object p1, p0, LX/E5E;->a:LX/0Ot;

    .line 2078082
    return-void
.end method

.method public static a(LX/0QB;)LX/E5E;
    .locals 4

    .prologue
    .line 2078083
    sget-object v0, LX/E5E;->c:LX/E5E;

    if-nez v0, :cond_1

    .line 2078084
    const-class v1, LX/E5E;

    monitor-enter v1

    .line 2078085
    :try_start_0
    sget-object v0, LX/E5E;->c:LX/E5E;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078086
    if-eqz v2, :cond_0

    .line 2078087
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078088
    new-instance v3, LX/E5E;

    const/16 p0, 0x3121

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E5E;-><init>(LX/0Ot;)V

    .line 2078089
    move-object v0, v3

    .line 2078090
    sput-object v0, LX/E5E;->c:LX/E5E;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078091
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078092
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078093
    :cond_1
    sget-object v0, LX/E5E;->c:LX/E5E;

    return-object v0

    .line 2078094
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 16

    .prologue
    .line 2078096
    check-cast p2, LX/E5D;

    .line 2078097
    move-object/from16 v0, p0

    iget-object v2, v0, LX/E5E;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E5F;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/E5D;->a:LX/0Px;

    move-object/from16 v0, p2

    iget-object v5, v0, LX/E5D;->b:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v6, v0, LX/E5D;->c:LX/0Px;

    move-object/from16 v0, p2

    iget-object v7, v0, LX/E5D;->d:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-wide v8, v0, LX/E5D;->e:D

    move-object/from16 v0, p2

    iget-wide v10, v0, LX/E5D;->f:D

    move-object/from16 v0, p2

    iget-object v12, v0, LX/E5D;->g:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v13, v0, LX/E5D;->h:LX/2km;

    move-object/from16 v0, p2

    iget-object v14, v0, LX/E5D;->i:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v15, v0, LX/E5D;->j:Ljava/lang/String;

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v15}, LX/E5F;->a(LX/1De;LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;DDLjava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;)LX/1Dg;

    move-result-object v2

    .line 2078098
    return-object v2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2078099
    invoke-static {}, LX/1dS;->b()V

    .line 2078100
    const/4 v0, 0x0

    return-object v0
.end method
