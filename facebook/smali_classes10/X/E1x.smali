.class public final LX/E1x;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E1y;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

.field public d:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

.field public e:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

.field public final synthetic f:LX/E1y;


# direct methods
.method public constructor <init>(LX/E1y;)V
    .locals 1

    .prologue
    .line 2071954
    iput-object p1, p0, LX/E1x;->f:LX/E1y;

    .line 2071955
    move-object v0, p1

    .line 2071956
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2071957
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2071958
    const-string v0, "ReactionCoreComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2071959
    if-ne p0, p1, :cond_1

    .line 2071960
    :cond_0
    :goto_0
    return v0

    .line 2071961
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2071962
    goto :goto_0

    .line 2071963
    :cond_3
    check-cast p1, LX/E1x;

    .line 2071964
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2071965
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2071966
    if-eq v2, v3, :cond_0

    .line 2071967
    iget-object v2, p0, LX/E1x;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E1x;->a:LX/1X1;

    iget-object v3, p1, LX/E1x;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2071968
    goto :goto_0

    .line 2071969
    :cond_5
    iget-object v2, p1, LX/E1x;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 2071970
    :cond_6
    iget-object v2, p0, LX/E1x;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E1x;->b:Ljava/lang/String;

    iget-object v3, p1, LX/E1x;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2071971
    goto :goto_0

    .line 2071972
    :cond_8
    iget-object v2, p1, LX/E1x;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2071973
    :cond_9
    iget-object v2, p0, LX/E1x;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E1x;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    iget-object v3, p1, LX/E1x;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2071974
    goto :goto_0

    .line 2071975
    :cond_b
    iget-object v2, p1, LX/E1x;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    if-nez v2, :cond_a

    .line 2071976
    :cond_c
    iget-object v2, p0, LX/E1x;->d:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/E1x;->d:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    iget-object v3, p1, LX/E1x;->d:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2071977
    goto :goto_0

    .line 2071978
    :cond_e
    iget-object v2, p1, LX/E1x;->d:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    if-nez v2, :cond_d

    .line 2071979
    :cond_f
    iget-object v2, p0, LX/E1x;->e:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/E1x;->e:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    iget-object v3, p1, LX/E1x;->e:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2071980
    goto :goto_0

    .line 2071981
    :cond_10
    iget-object v2, p1, LX/E1x;->e:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2071982
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/E1x;

    .line 2071983
    iget-object v1, v0, LX/E1x;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/E1x;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/E1x;->a:LX/1X1;

    .line 2071984
    return-object v0

    .line 2071985
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
