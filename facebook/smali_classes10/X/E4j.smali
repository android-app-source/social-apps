.class public LX/E4j;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kn;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E4j;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E4j",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077129
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2077130
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/E4j;->b:LX/0Zi;

    .line 2077131
    iput-object p1, p0, LX/E4j;->a:LX/0Ot;

    .line 2077132
    return-void
.end method

.method public static a(LX/0QB;)LX/E4j;
    .locals 4

    .prologue
    .line 2077133
    sget-object v0, LX/E4j;->c:LX/E4j;

    if-nez v0, :cond_1

    .line 2077134
    const-class v1, LX/E4j;

    monitor-enter v1

    .line 2077135
    :try_start_0
    sget-object v0, LX/E4j;->c:LX/E4j;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077136
    if-eqz v2, :cond_0

    .line 2077137
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077138
    new-instance v3, LX/E4j;

    const/16 p0, 0x3111

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E4j;-><init>(LX/0Ot;)V

    .line 2077139
    move-object v0, v3

    .line 2077140
    sput-object v0, LX/E4j;->c:LX/E4j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077141
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077142
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077143
    :cond_1
    sget-object v0, LX/E4j;->c:LX/E4j;

    return-object v0

    .line 2077144
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077145
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 6

    .prologue
    .line 2077146
    check-cast p2, LX/E4i;

    .line 2077147
    iget-object v0, p0, LX/E4j;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;

    iget-object v2, p2, LX/E4i;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p2, LX/E4i;->e:Ljava/lang/String;

    iget-object v4, p2, LX/E4i;->f:LX/2km;

    iget-object v5, p2, LX/E4i;->g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->onClick(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2077148
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2077149
    const v0, -0x37e16413

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2077150
    check-cast p2, LX/E4i;

    .line 2077151
    iget-object v0, p0, LX/E4j;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;

    iget-object v1, p2, LX/E4i;->a:Ljava/lang/String;

    iget-object v2, p2, LX/E4i;->b:Ljava/lang/String;

    iget-object v3, p2, LX/E4i;->c:Ljava/lang/String;

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 p0, 0x2

    .line 2077152
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v5

    const v6, 0x7f0a083f

    invoke-virtual {v5, v6}, LX/1nh;->i(I)LX/1nh;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->d(Z)LX/1Dh;

    move-result-object v4

    .line 2077153
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x6

    const v7, 0x7f0b1f7c

    invoke-interface {v5, v6, v7}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x3

    const v7, 0x7f0b1f7c

    invoke-interface {v5, v6, v7}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v5

    const/16 v6, 0x8

    const v7, 0x7f0b163a

    invoke-interface {v5, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const v6, 0x7f021153

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    .line 2077154
    const v6, -0x37e16413

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 2077155
    invoke-interface {v5, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    .line 2077156
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v6

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v7

    const p0, 0x7f0a010a

    invoke-virtual {v7, p0}, LX/1nh;->i(I)LX/1nh;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->c:LX/1Ad;

    invoke-virtual {v7, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v7

    sget-object p0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, p0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v7

    invoke-virtual {v7}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1636

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1636

    invoke-interface {v6, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const/16 v7, 0x8

    const p0, 0x7f0b163a

    invoke-interface {v6, v7, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    const/4 v7, 0x2

    const p0, 0x7f0b163d

    invoke-interface {v6, v7, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    move-object v6, v6

    .line 2077157
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2077158
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const p0, 0x7f0b0050

    invoke-virtual {v7, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v7, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const p0, 0x7f0b004e

    invoke-virtual {v7, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const p0, -0x777778

    invoke-virtual {v7, p0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2077159
    iget-object v6, v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionAdinterfacesObjectiveBlockComponentSpec;->d:LX/1vg;

    invoke-virtual {v6, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v6

    const v7, 0x7f0a00e6

    invoke-virtual {v6, v7}, LX/2xv;->j(I)LX/2xv;

    move-result-object v6

    const v7, 0x7f0207ed

    invoke-virtual {v6, v7}, LX/2xv;->h(I)LX/2xv;

    move-result-object v6

    invoke-virtual {v6}, LX/1n6;->b()LX/1dc;

    move-result-object v6

    .line 2077160
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1633

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1633

    invoke-interface {v6, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const/16 v7, 0x8

    const p0, 0x7f0b163c

    invoke-interface {v6, v7, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    move-object v6, v6

    .line 2077161
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2077162
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2077163
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2077164
    invoke-static {}, LX/1dS;->b()V

    .line 2077165
    iget v0, p1, LX/1dQ;->b:I

    .line 2077166
    packed-switch v0, :pswitch_data_0

    .line 2077167
    :goto_0
    return-object v2

    .line 2077168
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2077169
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/E4j;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x37e16413
        :pswitch_0
    .end packed-switch
.end method
