.class public LX/CvE;
.super LX/16T;
.source ""

# interfaces
.implements LX/16E;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public static g:Z

.field private static h:LX/0hs;

.field private static volatile i:LX/CvE;


# instance fields
.field public final b:LX/0tQ;

.field private final c:LX/19w;

.field public final d:LX/0yI;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7WS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1947870
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/CvE;->a:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(LX/0tQ;LX/19w;LX/0yI;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tQ;",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            "LX/0yI;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/7WS;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1947923
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1947924
    iput-object p1, p0, LX/CvE;->b:LX/0tQ;

    .line 1947925
    iput-object p2, p0, LX/CvE;->c:LX/19w;

    .line 1947926
    iput-object p3, p0, LX/CvE;->d:LX/0yI;

    .line 1947927
    iput-object p4, p0, LX/CvE;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1947928
    iput-object p5, p0, LX/CvE;->f:LX/0Ot;

    .line 1947929
    return-void
.end method

.method public static a(LX/0QB;)LX/CvE;
    .locals 9

    .prologue
    .line 1947910
    sget-object v0, LX/CvE;->i:LX/CvE;

    if-nez v0, :cond_1

    .line 1947911
    const-class v1, LX/CvE;

    monitor-enter v1

    .line 1947912
    :try_start_0
    sget-object v0, LX/CvE;->i:LX/CvE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1947913
    if-eqz v2, :cond_0

    .line 1947914
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1947915
    new-instance v3, LX/CvE;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v4

    check-cast v4, LX/0tQ;

    invoke-static {v0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v5

    check-cast v5, LX/19w;

    invoke-static {v0}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v6

    check-cast v6, LX/0yI;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v8, 0x390f

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/CvE;-><init>(LX/0tQ;LX/19w;LX/0yI;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;)V

    .line 1947916
    move-object v0, v3

    .line 1947917
    sput-object v0, LX/CvE;->i:LX/CvE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1947918
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1947919
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1947920
    :cond_1
    sget-object v0, LX/CvE;->i:LX/CvE;

    return-object v0

    .line 1947921
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1947922
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1947905
    iget-object v0, p0, LX/CvE;->b:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, LX/CvA;->d()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, LX/CvE;->h:LX/0hs;

    if-eqz v0, :cond_0

    sget-object v0, LX/CvE;->h:LX/0hs;

    .line 1947906
    iget-boolean p0, v0, LX/0ht;->r:Z

    move v0, p0

    .line 1947907
    if-nez v0, :cond_1

    :cond_0
    sget-boolean v0, LX/CvE;->g:Z

    if-nez v0, :cond_1

    sget-object v0, LX/CvE;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1947908
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 1947909
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1947873
    instance-of v0, p2, Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1947874
    :cond_0
    :goto_0
    return-void

    .line 1947875
    :cond_1
    check-cast p2, Ljava/lang/String;

    .line 1947876
    iget-object v0, p0, LX/CvE;->c:LX/19w;

    invoke-virtual {v0, p2}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v0

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    .line 1947877
    sget-object v1, LX/CvE;->h:LX/0hs;

    if-eqz v1, :cond_2

    sget-object v1, LX/CvE;->h:LX/0hs;

    .line 1947878
    iget-boolean v2, v1, LX/0ht;->r:Z

    move v1, v2

    .line 1947879
    if-nez v1, :cond_0

    :cond_2
    sget-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-ne v0, v1, :cond_0

    sget-object v0, LX/CvE;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, LX/CvE;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/Cuz;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LX/CvA;->d()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, LX/CvE;->g:Z

    if-nez v0, :cond_0

    .line 1947880
    const/4 v0, 0x1

    sput-boolean v0, LX/CvE;->g:Z

    .line 1947881
    sget-object v0, LX/CvE;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1947882
    new-instance v1, LX/0hs;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1947883
    sput-object v1, LX/CvE;->h:LX/0hs;

    const/4 v2, -0x1

    .line 1947884
    iput v2, v1, LX/0hs;->t:I

    .line 1947885
    sget-object v1, LX/CvE;->h:LX/0hs;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1947886
    iget-object v3, p0, LX/CvE;->d:LX/0yI;

    sget-object p1, LX/0yY;->TIMEBASED_OFFLINE_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v3, p1}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, LX/CvE;->d:LX/0yI;

    sget-object p1, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v3, p1}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1947887
    iget-object v3, p0, LX/CvE;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p1, LX/0df;->P:LX/0Tn;

    const-string p2, ""

    invoke-interface {v3, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1947888
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_3

    .line 1947889
    :goto_1
    move-object v2, v3

    .line 1947890
    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1947891
    sget-object v1, LX/CvE;->h:LX/0hs;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1947892
    iget-object v3, p0, LX/CvE;->d:LX/0yI;

    sget-object p1, LX/0yY;->TIMEBASED_OFFLINE_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v3, p1}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, LX/CvE;->d:LX/0yI;

    sget-object p1, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v3, p1}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1947893
    iget-object v3, p0, LX/CvE;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p1, LX/0df;->Q:LX/0Tn;

    const-string p2, ""

    invoke-interface {v3, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1947894
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_5

    .line 1947895
    :goto_2
    move-object v2, v3

    .line 1947896
    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1947897
    sget-object v1, LX/CvE;->h:LX/0hs;

    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1947898
    :cond_3
    iget-object v3, p0, LX/CvE;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7WS;

    invoke-virtual {v3}, LX/7WS;->a()V

    .line 1947899
    :cond_4
    sget-object v3, LX/CvD;->a:[I

    iget-object p1, p0, LX/CvE;->b:LX/0tQ;

    invoke-virtual {p1}, LX/0tQ;->m()LX/2qY;

    move-result-object p1

    invoke-virtual {p1}, LX/2qY;->ordinal()I

    move-result p1

    aget v3, v3, p1

    packed-switch v3, :pswitch_data_0

    .line 1947900
    const v3, 0x7f080d45

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_1

    .line 1947901
    :pswitch_0
    const v3, 0x7f080d47

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_1

    .line 1947902
    :pswitch_1
    const v3, 0x7f080d46

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_1

    .line 1947903
    :cond_5
    iget-object v3, p0, LX/CvE;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7WS;

    invoke-virtual {v3}, LX/7WS;->a()V

    .line 1947904
    :cond_6
    const v3, 0x7f080d48

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1947872
    const-string v0, "4372"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1947871
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_STALLING:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
