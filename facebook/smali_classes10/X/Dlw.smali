.class public abstract LX/Dlw;
.super LX/1a1;
.source ""


# instance fields
.field private final l:Z

.field public final m:Landroid/widget/ImageView;

.field public final n:Landroid/widget/TextView;

.field public final o:Landroid/widget/TextView;

.field private final p:LX/0wM;


# direct methods
.method public constructor <init>(Landroid/view/View;ZLX/0wM;)V
    .locals 1

    .prologue
    .line 2038782
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2038783
    const v0, 0x7f0d05a6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Dlw;->m:Landroid/widget/ImageView;

    .line 2038784
    const v0, 0x7f0d05a7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Dlw;->n:Landroid/widget/TextView;

    .line 2038785
    const v0, 0x7f0d05a8

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Dlw;->o:Landroid/widget/TextView;

    .line 2038786
    iput-boolean p2, p0, LX/Dlw;->l:Z

    .line 2038787
    iput-object p3, p0, LX/Dlw;->p:LX/0wM;

    .line 2038788
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2038789
    iget-boolean v0, p0, LX/Dlw;->l:Z

    if-eqz v0, :cond_1

    .line 2038790
    iget-object v0, p0, LX/Dlw;->m:Landroid/widget/ImageView;

    iget-object v1, p0, LX/Dlw;->p:LX/0wM;

    iget-object v2, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2038791
    :goto_0
    iget-boolean v0, p0, LX/Dlw;->l:Z

    if-eqz v0, :cond_0

    .line 2038792
    iget-object v0, p0, LX/Dlw;->n:Landroid/widget/TextView;

    iget-object v1, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2038793
    iget-object v0, p0, LX/Dlw;->o:Landroid/widget/TextView;

    iget-object v1, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2038794
    :cond_0
    iget-object v0, p0, LX/Dlw;->n:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2038795
    if-eqz p3, :cond_2

    .line 2038796
    iget-object v0, p0, LX/Dlw;->o:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2038797
    iget-object v0, p0, LX/Dlw;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2038798
    :goto_1
    return-void

    .line 2038799
    :cond_1
    iget-object v0, p0, LX/Dlw;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2038800
    :cond_2
    iget-object v0, p0, LX/Dlw;->o:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public abstract a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
.end method
