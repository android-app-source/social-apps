.class public final LX/EwT;
.super LX/2hO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 0

    .prologue
    .line 2183206
    iput-object p1, p0, LX/EwT;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-direct {p0}, LX/2hO;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 12

    .prologue
    .line 2183207
    check-cast p1, LX/2iB;

    .line 2183208
    if-nez p1, :cond_1

    .line 2183209
    :cond_0
    :goto_0
    return-void

    .line 2183210
    :cond_1
    iget-object v0, p0, LX/EwT;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    if-eqz v0, :cond_2

    .line 2183211
    iget-object v0, p0, LX/EwT;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    new-instance v1, LX/EwS;

    invoke-direct {v1, p0, p1}, LX/EwS;-><init>(LX/EwT;LX/2iB;)V

    iget-wide v2, p1, LX/2iB;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2kW;->a(LX/0Rl;Ljava/lang/String;)V

    goto :goto_0

    .line 2183212
    :cond_2
    iget-object v0, p0, LX/EwT;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    iget-wide v2, p1, LX/2iB;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/83X;

    .line 2183213
    if-eqz v0, :cond_0

    instance-of v0, v0, LX/Ewo;

    if-eqz v0, :cond_0

    .line 2183214
    iget-object v0, p0, LX/EwT;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    iget-wide v2, p1, LX/2iB;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2183215
    iget-object v0, p0, LX/EwT;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-wide v2, p1, LX/2iB;->a:J

    .line 2183216
    const/4 v7, 0x0

    move v8, v7

    :goto_1
    iget-object v7, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v8, v7, :cond_5

    .line 2183217
    iget-object v7, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/Ewo;

    invoke-virtual {v7}, LX/Eus;->a()J

    move-result-wide v9

    cmp-long v7, v9, v2

    if-nez v7, :cond_4

    .line 2183218
    :goto_2
    move v4, v8

    .line 2183219
    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 2183220
    iget-object v5, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2183221
    invoke-static {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183222
    iget-object v4, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    invoke-virtual {v4}, LX/Eui;->a()Z

    move-result v4

    if-nez v4, :cond_3

    .line 2183223
    invoke-static {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183224
    :cond_3
    goto :goto_0

    .line 2183225
    :cond_4
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_1

    .line 2183226
    :cond_5
    const/4 v8, -0x1

    goto :goto_2
.end method
