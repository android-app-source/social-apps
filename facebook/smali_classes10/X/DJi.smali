.class public final LX/DJi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

.field public final synthetic b:LX/DJk;


# direct methods
.method public constructor <init>(LX/DJk;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)V
    .locals 0

    .prologue
    .line 1985753
    iput-object p1, p0, LX/DJi;->b:LX/DJk;

    iput-object p2, p0, LX/DJi;->a:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    .line 1985754
    iget-object v0, p0, LX/DJi;->b:LX/DJk;

    iget-object v0, v0, LX/DJk;->f:LX/DJl;

    iget-object v0, v0, LX/DJl;->e:LX/B0k;

    iget-object v1, p0, LX/DJi;->b:LX/DJk;

    iget-object v1, v1, LX/DJk;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/DJi;->b:LX/DJk;

    iget-object v2, v2, LX/DJk;->c:LX/21D;

    invoke-virtual {v0, v1, v2}, LX/B0k;->b(Ljava/lang/String;LX/21D;)V

    .line 1985755
    iget-object v0, p0, LX/DJi;->b:LX/DJk;

    iget-object v0, v0, LX/DJk;->f:LX/DJl;

    iget-object v1, p0, LX/DJi;->b:LX/DJk;

    iget-object v1, v1, LX/DJk;->d:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    iget-object v2, p0, LX/DJi;->b:LX/DJk;

    iget-object v2, v2, LX/DJk;->b:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    iget-object v3, p0, LX/DJi;->b:LX/DJk;

    iget-object v3, v3, LX/DJk;->c:LX/21D;

    iget-object v4, p0, LX/DJi;->b:LX/DJk;

    iget-object v4, v4, LX/DJk;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v5, p0, LX/DJi;->a:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1985756
    iget-object v6, v0, LX/DJl;->c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    const-string v7, "launchSellComposerForConversion"

    invoke-interface {v6, v3, v7, v4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(LX/21D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    sget-object v7, LX/2rt;->SELL:LX/2rt;

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    new-instance v7, LX/89K;

    invoke-direct {v7}, LX/89K;-><init>()V

    invoke-static {}, LX/88g;->c()LX/88g;

    move-result-object v7

    invoke-static {v7}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setCommerceInfo(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    .line 1985757
    iget-object v7, v0, LX/DJl;->a:LX/1Kf;

    const/4 p1, 0x0

    const/16 p2, 0x6de

    invoke-interface {v7, p1, v6, p2, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1985758
    iget-object v0, p0, LX/DJi;->b:LX/DJk;

    iget-object v0, v0, LX/DJk;->e:LX/DJr;

    if-eqz v0, :cond_0

    .line 1985759
    iget-object v0, p0, LX/DJi;->b:LX/DJk;

    iget-object v0, v0, LX/DJk;->e:LX/DJr;

    invoke-interface {v0}, LX/DJr;->a()V

    .line 1985760
    :cond_0
    return-void
.end method
