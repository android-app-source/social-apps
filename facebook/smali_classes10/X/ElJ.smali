.class public final enum LX/ElJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ElJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ElJ;

.field public static final enum API_EC_DOMAIN:LX/ElJ;

.field public static final enum GRAPHQL_KERROR_DOMAIN:LX/ElJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2164239
    new-instance v0, LX/ElJ;

    const-string v1, "API_EC_DOMAIN"

    invoke-direct {v0, v1, v2}, LX/ElJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ElJ;->API_EC_DOMAIN:LX/ElJ;

    .line 2164240
    new-instance v0, LX/ElJ;

    const-string v1, "GRAPHQL_KERROR_DOMAIN"

    invoke-direct {v0, v1, v3}, LX/ElJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ElJ;->GRAPHQL_KERROR_DOMAIN:LX/ElJ;

    .line 2164241
    const/4 v0, 0x2

    new-array v0, v0, [LX/ElJ;

    sget-object v1, LX/ElJ;->API_EC_DOMAIN:LX/ElJ;

    aput-object v1, v0, v2

    sget-object v1, LX/ElJ;->GRAPHQL_KERROR_DOMAIN:LX/ElJ;

    aput-object v1, v0, v3

    sput-object v0, LX/ElJ;->$VALUES:[LX/ElJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2164242
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ElJ;
    .locals 1

    .prologue
    .line 2164243
    const-class v0, LX/ElJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ElJ;

    return-object v0
.end method

.method public static values()[LX/ElJ;
    .locals 1

    .prologue
    .line 2164244
    sget-object v0, LX/ElJ;->$VALUES:[LX/ElJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ElJ;

    return-object v0
.end method
