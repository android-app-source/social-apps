.class public LX/EAi;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/EAi;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2085845
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2085846
    sget-object v0, LX/0ax;->eI:Ljava/lang/String;

    const-string v1, "{review_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->USER_REVIEWS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2085847
    return-void
.end method

.method public static a(LX/0QB;)LX/EAi;
    .locals 3

    .prologue
    .line 2085848
    sget-object v0, LX/EAi;->a:LX/EAi;

    if-nez v0, :cond_1

    .line 2085849
    const-class v1, LX/EAi;

    monitor-enter v1

    .line 2085850
    :try_start_0
    sget-object v0, LX/EAi;->a:LX/EAi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2085851
    if-eqz v2, :cond_0

    .line 2085852
    :try_start_1
    new-instance v0, LX/EAi;

    invoke-direct {v0}, LX/EAi;-><init>()V

    .line 2085853
    move-object v0, v0

    .line 2085854
    sput-object v0, LX/EAi;->a:LX/EAi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2085855
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2085856
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2085857
    :cond_1
    sget-object v0, LX/EAi;->a:LX/EAi;

    return-object v0

    .line 2085858
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2085859
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
