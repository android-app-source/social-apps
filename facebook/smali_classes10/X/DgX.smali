.class public final LX/DgX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DgW;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V
    .locals 0

    .prologue
    .line 2029504
    iput-object p1, p0, LX/DgX;->a:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2029505
    iget-object v0, p0, LX/DgX;->a:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    invoke-static {v0}, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->e(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V

    .line 2029506
    return-void
.end method

.method public final a(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 2029507
    iget-object v0, p0, LX/DgX;->a:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    .line 2029508
    iput-object p1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->h:Landroid/location/Location;

    .line 2029509
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->f:LX/Dgb;

    sget-object p0, LX/Dgb;->UNSET:LX/Dgb;

    if-ne v1, p0, :cond_0

    .line 2029510
    invoke-static {v0}, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->e(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V

    .line 2029511
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 2

    .prologue
    .line 2029512
    iget-object v0, p0, LX/DgX;->a:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    .line 2029513
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->f:LX/Dgb;

    sget-object p0, LX/Dgb;->UNSET:LX/Dgb;

    if-ne v1, p0, :cond_0

    .line 2029514
    :goto_0
    return-void

    .line 2029515
    :cond_0
    invoke-static {v0, p1}, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->c(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;Lcom/facebook/android/maps/model/LatLng;)V

    goto :goto_0
.end method
