.class public LX/EKT;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EKU;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EKT",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EKU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106810
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2106811
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/EKT;->b:LX/0Zi;

    .line 2106812
    iput-object p1, p0, LX/EKT;->a:LX/0Ot;

    .line 2106813
    return-void
.end method

.method public static a(LX/0QB;)LX/EKT;
    .locals 4

    .prologue
    .line 2106799
    const-class v1, LX/EKT;

    monitor-enter v1

    .line 2106800
    :try_start_0
    sget-object v0, LX/EKT;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106801
    sput-object v2, LX/EKT;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106802
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106803
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106804
    new-instance v3, LX/EKT;

    const/16 p0, 0x33cc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EKT;-><init>(LX/0Ot;)V

    .line 2106805
    move-object v0, v3

    .line 2106806
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106807
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106808
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106809
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2106740
    check-cast p2, LX/EKS;

    .line 2106741
    iget-object v0, p0, LX/EKT;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EKU;

    iget-object v1, p2, LX/EKS;->a:LX/CzL;

    iget-object v2, p2, LX/EKS;->b:LX/Cwx;

    const/4 p2, 0x0

    .line 2106742
    iget-object v3, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 2106743
    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aR()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v3

    .line 2106744
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082295

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->fw_()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, p2

    const/4 v7, 0x1

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->e()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2106745
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x4

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    .line 2106746
    new-instance v7, LX/EKy;

    invoke-direct {v7}, LX/EKy;-><init>()V

    .line 2106747
    sget-object p0, LX/EKz;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EKx;

    .line 2106748
    if-nez p0, :cond_0

    .line 2106749
    new-instance p0, LX/EKx;

    invoke-direct {p0}, LX/EKx;-><init>()V

    .line 2106750
    :cond_0
    invoke-static {p0, p1, v6, v6, v7}, LX/EKx;->a$redex0(LX/EKx;LX/1De;IILX/EKy;)V

    .line 2106751
    move-object v7, p0

    .line 2106752
    move-object v6, v7

    .line 2106753
    move-object v6, v6

    .line 2106754
    iget-object v7, v6, LX/EKx;->a:LX/EKy;

    iput-object v4, v7, LX/EKy;->a:Ljava/lang/CharSequence;

    .line 2106755
    iget-object v7, v6, LX/EKx;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v7, p0}, Ljava/util/BitSet;->set(I)V

    .line 2106756
    move-object v4, v6

    .line 2106757
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 2106758
    const v6, -0x26aae2d3

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 2106759
    invoke-interface {v4, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/EKU;->b:LX/EKv;

    const/4 v6, 0x0

    .line 2106760
    new-instance v7, LX/EKu;

    invoke-direct {v7, v5}, LX/EKu;-><init>(LX/EKv;)V

    .line 2106761
    sget-object p0, LX/EKv;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EKt;

    .line 2106762
    if-nez p0, :cond_1

    .line 2106763
    new-instance p0, LX/EKt;

    invoke-direct {p0}, LX/EKt;-><init>()V

    .line 2106764
    :cond_1
    invoke-static {p0, p1, v6, v6, v7}, LX/EKt;->a$redex0(LX/EKt;LX/1De;IILX/EKu;)V

    .line 2106765
    move-object v7, p0

    .line 2106766
    move-object v6, v7

    .line 2106767
    move-object v5, v6

    .line 2106768
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->a()LX/0Px;

    move-result-object v3

    .line 2106769
    iget-object v6, v5, LX/EKt;->a:LX/EKu;

    iput-object v3, v6, LX/EKu;->a:LX/0Px;

    .line 2106770
    iget-object v6, v5, LX/EKt;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2106771
    move-object v3, v5

    .line 2106772
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/EKU;->c:LX/EKf;

    const/4 v5, 0x0

    .line 2106773
    new-instance v6, LX/EKe;

    invoke-direct {v6, v4}, LX/EKe;-><init>(LX/EKf;)V

    .line 2106774
    iget-object v7, v4, LX/EKf;->b:LX/0Zi;

    invoke-virtual {v7}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/EKd;

    .line 2106775
    if-nez v7, :cond_2

    .line 2106776
    new-instance v7, LX/EKd;

    invoke-direct {v7, v4}, LX/EKd;-><init>(LX/EKf;)V

    .line 2106777
    :cond_2
    invoke-static {v7, p1, v5, v5, v6}, LX/EKd;->a$redex0(LX/EKd;LX/1De;IILX/EKe;)V

    .line 2106778
    move-object v6, v7

    .line 2106779
    move-object v5, v6

    .line 2106780
    move-object v4, v5

    .line 2106781
    iget-object v5, v4, LX/EKd;->a:LX/EKe;

    iput-object v1, v5, LX/EKe;->a:LX/CzL;

    .line 2106782
    iget-object v5, v4, LX/EKd;->e:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2106783
    move-object v4, v4

    .line 2106784
    check-cast v2, LX/CxP;

    .line 2106785
    iget-object v5, v4, LX/EKd;->a:LX/EKe;

    iput-object v2, v5, LX/EKe;->b:LX/CxP;

    .line 2106786
    iget-object v5, v4, LX/EKd;->e:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2106787
    move-object v4, v4

    .line 2106788
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/EKU;->d:LX/EKj;

    const/4 v5, 0x0

    .line 2106789
    new-instance v6, LX/EKi;

    invoke-direct {v6, v4}, LX/EKi;-><init>(LX/EKj;)V

    .line 2106790
    sget-object v7, LX/EKj;->a:LX/0Zi;

    invoke-virtual {v7}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/EKh;

    .line 2106791
    if-nez v7, :cond_3

    .line 2106792
    new-instance v7, LX/EKh;

    invoke-direct {v7}, LX/EKh;-><init>()V

    .line 2106793
    :cond_3
    invoke-static {v7, p1, v5, v5, v6}, LX/EKh;->a$redex0(LX/EKh;LX/1De;IILX/EKi;)V

    .line 2106794
    move-object v6, v7

    .line 2106795
    move-object v5, v6

    .line 2106796
    move-object v4, v5

    .line 2106797
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2106798
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2106727
    invoke-static {}, LX/1dS;->b()V

    .line 2106728
    iget v0, p1, LX/1dQ;->b:I

    .line 2106729
    packed-switch v0, :pswitch_data_0

    .line 2106730
    :goto_0
    return-object v1

    .line 2106731
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2106732
    check-cast v0, LX/EKS;

    .line 2106733
    iget-object v2, p0, LX/EKT;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v2, v0, LX/EKS;->a:LX/CzL;

    iget-object v3, v0, LX/EKS;->b:LX/Cwx;

    .line 2106734
    iget-object p1, v2, LX/CzL;->a:Ljava/lang/Object;

    move-object p1, p1

    .line 2106735
    check-cast p1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aR()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object p1

    .line 2106736
    new-instance p2, LX/A32;

    invoke-direct {p2}, LX/A32;-><init>()V

    .line 2106737
    const-string p0, "topic_id"

    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p0, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2106738
    sget-object p0, LX/D0P;->ELECTIONS:LX/D0P;

    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->d()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v3, p0, p1, v2, p2}, LX/Cwx;->a(LX/D0P;Ljava/lang/String;LX/CzL;LX/0gW;)V

    .line 2106739
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x26aae2d3
        :pswitch_0
    .end packed-switch
.end method
