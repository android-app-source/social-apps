.class public final LX/DQ8;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

.field public final synthetic c:Lcom/facebook/groups/info/GroupInfoAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;ZLcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V
    .locals 0

    .prologue
    .line 1994061
    iput-object p1, p0, LX/DQ8;->c:Lcom/facebook/groups/info/GroupInfoAdapter;

    iput-boolean p3, p0, LX/DQ8;->a:Z

    iput-object p4, p0, LX/DQ8;->b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1994062
    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    .line 1994063
    iget-boolean v0, p0, LX/DQ8;->a:Z

    if-eqz v0, :cond_0

    const v0, 0x7f08304d

    .line 1994064
    :goto_0
    iget-object v1, p0, LX/DQ8;->c:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v1, v1, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1994065
    iget-object v0, p0, LX/DQ8;->c:Lcom/facebook/groups/info/GroupInfoAdapter;

    sget-object v1, LX/DQO;->REPORT_GROUP:LX/DQO;

    iget-object v2, p0, LX/DQ8;->b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v0, v1, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a$redex0(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1994066
    return-void

    .line 1994067
    :cond_0
    const v0, 0x7f08304b

    goto :goto_0
.end method
