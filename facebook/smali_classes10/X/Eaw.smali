.class public LX/Eaw;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Object;


# instance fields
.field private final b:LX/2PE;

.field private final c:LX/Eay;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2142784
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Eaw;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2PE;LX/Eay;)V
    .locals 0

    .prologue
    .line 2142780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142781
    iput-object p1, p0, LX/Eaw;->b:LX/2PE;

    .line 2142782
    iput-object p2, p0, LX/Eaw;->c:LX/Eay;

    .line 2142783
    return-void
.end method

.method private static a(LX/Eb2;I)LX/Eb0;
    .locals 5

    .prologue
    .line 2142715
    invoke-virtual {p0}, LX/Eb2;->b()LX/Eaz;

    move-result-object v0

    .line 2142716
    iget v1, v0, LX/Eaz;->c:I

    move v1, v1

    .line 2142717
    if-le v1, p1, :cond_3

    .line 2142718
    invoke-virtual {p0, p1}, LX/Eb2;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2142719
    const/4 v1, 0x0

    .line 2142720
    new-instance v2, Ljava/util/LinkedList;

    iget-object v0, p0, LX/Eb2;->a:LX/EcH;

    .line 2142721
    iget-object v3, v0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    move-object v0, v3

    .line 2142722
    invoke-direct {v2, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 2142723
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 2142724
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2142725
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcC;

    .line 2142726
    iget v4, v0, LX/EcC;->iteration_:I

    move v4, v4

    .line 2142727
    if-ne v4, p1, :cond_0

    .line 2142728
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 2142729
    :goto_0
    iget-object v3, p0, LX/Eb2;->a:LX/EcH;

    invoke-virtual {v3}, LX/EcH;->r()LX/Ec4;

    move-result-object v3

    .line 2142730
    iget-object v4, v3, LX/Ec4;->h:LX/EZ2;

    if-nez v4, :cond_8

    .line 2142731
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    iput-object v4, v3, LX/Ec4;->g:Ljava/util/List;

    .line 2142732
    iget v4, v3, LX/Ec4;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, v3, LX/Ec4;->a:I

    .line 2142733
    invoke-virtual {v3}, LX/EWj;->t()V

    .line 2142734
    :goto_1
    move-object v3, v3

    .line 2142735
    iget-object v4, v3, LX/Ec4;->h:LX/EZ2;

    if-nez v4, :cond_9

    .line 2142736
    invoke-static {v3}, LX/Ec4;->D(LX/Ec4;)V

    .line 2142737
    iget-object v4, v3, LX/Ec4;->g:Ljava/util/List;

    invoke-static {v2, v4}, LX/EWS;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 2142738
    invoke-virtual {v3}, LX/EWj;->t()V

    .line 2142739
    :goto_2
    move-object v2, v3

    .line 2142740
    invoke-virtual {v2}, LX/Ec4;->l()LX/EcH;

    move-result-object v2

    iput-object v2, p0, LX/Eb2;->a:LX/EcH;

    .line 2142741
    if-eqz v0, :cond_1

    .line 2142742
    new-instance v1, LX/Eb0;

    .line 2142743
    iget v2, v0, LX/EcC;->iteration_:I

    move v2, v2

    .line 2142744
    iget-object v3, v0, LX/EcC;->seed_:LX/EWc;

    move-object v0, v3

    .line 2142745
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/Eb0;-><init>(I[B)V

    .line 2142746
    :cond_1
    move-object v0, v1

    .line 2142747
    :goto_3
    return-object v0

    .line 2142748
    :cond_2
    new-instance v1, LX/Ead;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received message with old counter: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2142749
    iget v3, v0, LX/Eaz;->c:I

    move v0, v3

    .line 2142750
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " , "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Ead;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2142751
    :cond_3
    iget v1, v0, LX/Eaz;->c:I

    move v1, v1

    .line 2142752
    sub-int v1, p1, v1

    const/16 v2, 0x7d0

    if-le v1, v2, :cond_4

    .line 2142753
    new-instance v0, LX/Eai;

    const-string v1, "Over 2000 messages into the future!"

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2142754
    :cond_4
    :goto_4
    iget v1, v0, LX/Eaz;->c:I

    move v1, v1

    .line 2142755
    if-ge v1, p1, :cond_6

    .line 2142756
    invoke-virtual {v0}, LX/Eaz;->b()LX/Eb0;

    move-result-object v1

    .line 2142757
    invoke-static {}, LX/EcB;->u()LX/EcB;

    move-result-object v2

    .line 2142758
    iget v3, v1, LX/Eb0;->a:I

    move v3, v3

    .line 2142759
    invoke-virtual {v2, v3}, LX/EcB;->a(I)LX/EcB;

    move-result-object v2

    .line 2142760
    iget-object v3, v1, LX/Eb0;->d:[B

    move-object v3, v3

    .line 2142761
    invoke-static {v3}, LX/EWc;->a([B)LX/EWc;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/EcB;->a(LX/EWc;)LX/EcB;

    move-result-object v2

    invoke-virtual {v2}, LX/EcB;->l()LX/EcC;

    move-result-object v2

    .line 2142762
    iget-object v3, p0, LX/Eb2;->a:LX/EcH;

    invoke-virtual {v3}, LX/EcH;->r()LX/Ec4;

    move-result-object v3

    .line 2142763
    invoke-virtual {v3, v2}, LX/Ec4;->a(LX/EcC;)LX/Ec4;

    .line 2142764
    iget-object v2, v3, LX/Ec4;->h:LX/EZ2;

    if-nez v2, :cond_a

    .line 2142765
    iget-object v2, v3, LX/Ec4;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2142766
    :goto_5
    move v2, v2

    .line 2142767
    const/16 v4, 0x7d0

    if-le v2, v4, :cond_5

    .line 2142768
    const/4 v2, 0x0

    .line 2142769
    iget-object v4, v3, LX/Ec4;->h:LX/EZ2;

    if-nez v4, :cond_b

    .line 2142770
    invoke-static {v3}, LX/Ec4;->D(LX/Ec4;)V

    .line 2142771
    iget-object v4, v3, LX/Ec4;->g:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2142772
    invoke-virtual {v3}, LX/EWj;->t()V

    .line 2142773
    :cond_5
    :goto_6
    invoke-virtual {v3}, LX/Ec4;->l()LX/EcH;

    move-result-object v2

    iput-object v2, p0, LX/Eb2;->a:LX/EcH;

    .line 2142774
    invoke-virtual {v0}, LX/Eaz;->c()LX/Eaz;

    move-result-object v0

    goto :goto_4

    .line 2142775
    :cond_6
    invoke-virtual {v0}, LX/Eaz;->c()LX/Eaz;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/Eb2;->a(LX/Eaz;)V

    .line 2142776
    invoke-virtual {v0}, LX/Eaz;->b()LX/Eb0;

    move-result-object v0

    goto/16 :goto_3

    :cond_7
    move-object v0, v1

    goto/16 :goto_0

    .line 2142777
    :cond_8
    iget-object v4, v3, LX/Ec4;->h:LX/EZ2;

    invoke-virtual {v4}, LX/EZ2;->e()V

    goto/16 :goto_1

    .line 2142778
    :cond_9
    iget-object v4, v3, LX/Ec4;->h:LX/EZ2;

    invoke-virtual {v4, v2}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto/16 :goto_2

    :cond_a
    iget-object v2, v3, LX/Ec4;->h:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_5

    .line 2142779
    :cond_b
    iget-object v4, v3, LX/Ec4;->h:LX/EZ2;

    invoke-virtual {v4, v2}, LX/EZ2;->d(I)V

    goto :goto_6
.end method

.method public static a(LX/Eaw;[BLX/Eac;)[B
    .locals 5

    .prologue
    .line 2142695
    sget-object v1, LX/Eaw;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2142696
    :try_start_0
    iget-object v0, p0, LX/Eaw;->b:LX/2PE;

    iget-object v2, p0, LX/Eaw;->c:LX/Eay;

    invoke-virtual {v0, v2}, LX/2PE;->a(LX/Eay;)LX/Eb1;

    move-result-object v0

    .line 2142697
    invoke-virtual {v0}, LX/Eb1;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2142698
    new-instance v0, LX/Eal;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No sender key for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Eaw;->c:LX/Eay;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LX/Eal;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/Eah; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2142699
    :catch_0
    move-exception v0

    .line 2142700
    :goto_0
    :try_start_1
    new-instance v2, LX/Eai;

    invoke-direct {v2, v0}, LX/Eai;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 2142701
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2142702
    :cond_0
    :try_start_2
    new-instance v2, LX/EbC;

    invoke-direct {v2, p1}, LX/EbC;-><init>([B)V

    .line 2142703
    iget v3, v2, LX/EbC;->b:I

    move v3, v3

    .line 2142704
    invoke-virtual {v0, v3}, LX/Eb1;->a(I)LX/Eb2;

    move-result-object v3

    .line 2142705
    invoke-virtual {v3}, LX/Eb2;->c()LX/Eat;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/EbC;->a(LX/Eat;)V

    .line 2142706
    iget v4, v2, LX/EbC;->c:I

    move v4, v4

    .line 2142707
    invoke-static {v3, v4}, LX/Eaw;->a(LX/Eb2;I)LX/Eb0;

    move-result-object v3

    .line 2142708
    iget-object v4, v3, LX/Eb0;->b:[B

    move-object v4, v4

    .line 2142709
    iget-object p1, v3, LX/Eb0;->c:[B

    move-object v3, p1

    .line 2142710
    iget-object p1, v2, LX/EbC;->d:[B

    move-object v2, p1

    .line 2142711
    invoke-static {v4, v3, v2}, LX/Eaw;->a([B[B[B)[B

    move-result-object v2

    .line 2142712
    iget-object v3, p0, LX/Eaw;->b:LX/2PE;

    iget-object v4, p0, LX/Eaw;->c:LX/Eay;

    invoke-virtual {v3, v4, v0}, LX/2PE;->a(LX/Eay;LX/Eb1;)V
    :try_end_2
    .catch LX/Eag; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/Eah; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2142713
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v2

    .line 2142714
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private static a([B[B[B)[B
    .locals 5

    .prologue
    .line 2142685
    :try_start_0
    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v0, p0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 2142686
    const-string v1, "AES/CBC/PKCS5Padding"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 2142687
    const/4 v2, 0x2

    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "AES"

    invoke-direct {v3, p1, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, v2, v3, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 2142688
    invoke-virtual {v1, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 2142689
    :catch_0
    move-exception v0

    .line 2142690
    :goto_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 2142691
    :catch_1
    move-exception v0

    .line 2142692
    :goto_1
    new-instance v1, LX/Eai;

    invoke-direct {v1, v0}, LX/Eai;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2142693
    :catch_2
    move-exception v0

    goto :goto_1

    .line 2142694
    :catch_3
    move-exception v0

    goto :goto_0

    :catch_4
    move-exception v0

    goto :goto_0

    :catch_5
    move-exception v0

    goto :goto_0
.end method

.method private static b([B[B[B)[B
    .locals 5

    .prologue
    .line 2142662
    :try_start_0
    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v0, p0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 2142663
    const-string v1, "AES/CBC/PKCS5Padding"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 2142664
    const/4 v2, 0x1

    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "AES"

    invoke-direct {v3, p1, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, v2, v3, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 2142665
    invoke-virtual {v1, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v0

    return-object v0

    .line 2142666
    :catch_0
    move-exception v0

    .line 2142667
    :goto_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 2142668
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0

    :catch_4
    move-exception v0

    goto :goto_0

    :catch_5
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a([B)[B
    .locals 8

    .prologue
    .line 2142669
    sget-object v1, LX/Eaw;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2142670
    :try_start_0
    iget-object v0, p0, LX/Eaw;->b:LX/2PE;

    iget-object v2, p0, LX/Eaw;->c:LX/Eay;

    invoke-virtual {v0, v2}, LX/2PE;->a(LX/Eay;)LX/Eb1;

    move-result-object v0

    .line 2142671
    invoke-virtual {v0}, LX/Eb1;->b()LX/Eb2;

    move-result-object v2

    .line 2142672
    invoke-virtual {v2}, LX/Eb2;->b()LX/Eaz;

    move-result-object v3

    invoke-virtual {v3}, LX/Eaz;->b()LX/Eb0;

    move-result-object v3

    .line 2142673
    iget-object v4, v3, LX/Eb0;->b:[B

    move-object v4, v4

    .line 2142674
    iget-object v5, v3, LX/Eb0;->c:[B

    move-object v5, v5

    .line 2142675
    invoke-static {v4, v5, p1}, LX/Eaw;->b([B[B[B)[B

    move-result-object v4

    .line 2142676
    new-instance v5, LX/EbC;

    invoke-virtual {v2}, LX/Eb2;->a()I

    move-result v6

    .line 2142677
    iget v7, v3, LX/Eb0;->a:I

    move v3, v7

    .line 2142678
    invoke-virtual {v2}, LX/Eb2;->d()LX/Eas;

    move-result-object v7

    invoke-direct {v5, v6, v3, v4, v7}, LX/EbC;-><init>(II[BLX/Eas;)V

    .line 2142679
    invoke-virtual {v2}, LX/Eb2;->b()LX/Eaz;

    move-result-object v3

    invoke-virtual {v3}, LX/Eaz;->c()LX/Eaz;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Eb2;->a(LX/Eaz;)V

    .line 2142680
    iget-object v2, p0, LX/Eaw;->b:LX/2PE;

    iget-object v3, p0, LX/Eaw;->c:LX/Eay;

    invoke-virtual {v2, v3, v0}, LX/2PE;->a(LX/Eay;LX/Eb1;)V

    .line 2142681
    invoke-virtual {v5}, LX/EbC;->a()[B
    :try_end_0
    .catch LX/Eah; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1

    return-object v0

    .line 2142682
    :catch_0
    move-exception v0

    .line 2142683
    new-instance v2, LX/Eal;

    invoke-direct {v2, v0}, LX/Eal;-><init>(Ljava/lang/Exception;)V

    throw v2

    .line 2142684
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
