.class public final LX/Es7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:LX/Es8;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Es8;)V
    .locals 0

    .prologue
    .line 2174496
    iput-object p1, p0, LX/Es7;->a:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2174497
    iput-object p2, p0, LX/Es7;->b:Ljava/lang/String;

    .line 2174498
    iput-object p3, p0, LX/Es7;->c:Ljava/lang/String;

    .line 2174499
    iput-object p4, p0, LX/Es7;->d:Ljava/lang/String;

    .line 2174500
    iput-object p5, p0, LX/Es7;->e:Ljava/lang/String;

    .line 2174501
    iput-object p6, p0, LX/Es7;->f:LX/Es8;

    .line 2174502
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const v0, -0x64de0c4c

    invoke-static {v8, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2174503
    iget-object v1, p0, LX/Es7;->a:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->f:LX/1Cn;

    iget-object v2, p0, LX/Es7;->e:Ljava/lang/String;

    const-string v3, "promotion"

    invoke-virtual {v1, v2, v3}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2174504
    new-instance v1, LX/89I;

    iget-object v2, p0, LX/Es7;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->USER:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v2, p0, LX/Es7;->c:Ljava/lang/String;

    .line 2174505
    iput-object v2, v1, LX/89I;->d:Ljava/lang/String;

    .line 2174506
    move-object v1, v1

    .line 2174507
    iget-object v2, p0, LX/Es7;->d:Ljava/lang/String;

    .line 2174508
    iput-object v2, v1, LX/89I;->c:Ljava/lang/String;

    .line 2174509
    move-object v1, v1

    .line 2174510
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    .line 2174511
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082a17

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/Es7;->d:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2174512
    sget-object v3, LX/21D;->NEWSFEED:LX/21D;

    const-string v4, "goodwillMessageAndPostPartDefinition"

    invoke-static {v3, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    iget-object v4, p0, LX/Es7;->a:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->g:LX/1Nq;

    invoke-static {v2}, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v3

    sget-object v4, LX/21D;->NEWSFEED:LX/21D;

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v3

    const-string v4, "goodwillBirthdayPromo"

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const-string v2, "top_friend_birthday_promotion"

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    .line 2174513
    iget-object v2, p0, LX/Es7;->f:LX/Es8;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/Es8;->d:Ljava/lang/String;

    .line 2174514
    iget-object v2, p0, LX/Es7;->a:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->e:LX/1Kf;

    iget-object v3, p0, LX/Es7;->f:LX/Es8;

    iget-object v3, v3, LX/Es8;->d:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v2, v3, v1, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2174515
    const v1, -0x39629d46

    invoke-static {v8, v8, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
