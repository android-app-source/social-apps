.class public final LX/EYz;
.super Ljava/util/AbstractList;
.source ""

# interfaces
.implements Ljava/util/List;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MType:",
        "LX/EWp;",
        "BType:",
        "LX/EWj;",
        "IType::",
        "LX/EWT;",
        ">",
        "Ljava/util/AbstractList",
        "<TBType;>;",
        "Ljava/util/List",
        "<TBType;>;"
    }
.end annotation


# instance fields
.field public a:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation
.end field


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2138335
    iget v0, p0, Ljava/util/AbstractList;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EYz;->modCount:I

    .line 2138336
    return-void
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2138337
    iget-object v0, p0, LX/EYz;->a:LX/EZ2;

    .line 2138338
    iget-object v1, v0, LX/EZ2;->d:Ljava/util/List;

    if-nez v1, :cond_0

    .line 2138339
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, v0, LX/EZ2;->d:Ljava/util/List;

    .line 2138340
    const/4 v1, 0x0

    :goto_0
    iget-object v2, v0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2138341
    iget-object v2, v0, LX/EZ2;->d:Ljava/util/List;

    const/4 p0, 0x0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2138342
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2138343
    :cond_0
    iget-object v1, v0, LX/EZ2;->d:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EZ7;

    .line 2138344
    if-nez v1, :cond_1

    .line 2138345
    iget-object v1, v0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EWp;

    .line 2138346
    new-instance v2, LX/EZ7;

    iget-boolean p0, v0, LX/EZ2;->e:Z

    invoke-direct {v2, v1, v0, p0}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    .line 2138347
    iget-object v1, v0, LX/EZ2;->d:Ljava/util/List;

    invoke-interface {v1, p1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-object v1, v2

    .line 2138348
    :cond_1
    invoke-virtual {v1}, LX/EZ7;->e()LX/EWj;

    move-result-object v1

    move-object v0, v1

    .line 2138349
    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2138350
    iget-object v0, p0, LX/EYz;->a:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->c()I

    move-result v0

    return v0
.end method
