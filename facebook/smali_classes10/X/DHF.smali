.class public LX/DHF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DHH;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DHF",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DHH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981553
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1981554
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DHF;->b:LX/0Zi;

    .line 1981555
    iput-object p1, p0, LX/DHF;->a:LX/0Ot;

    .line 1981556
    return-void
.end method

.method public static a(LX/0QB;)LX/DHF;
    .locals 4

    .prologue
    .line 1981557
    const-class v1, LX/DHF;

    monitor-enter v1

    .line 1981558
    :try_start_0
    sget-object v0, LX/DHF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981559
    sput-object v2, LX/DHF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981560
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981561
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981562
    new-instance v3, LX/DHF;

    const/16 p0, 0x21ca

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DHF;-><init>(LX/0Ot;)V

    .line 1981563
    move-object v0, v3

    .line 1981564
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981565
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DHF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981566
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981567
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1981568
    const v0, -0x5f9ad2c8

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1981569
    check-cast p2, LX/DHE;

    .line 1981570
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1981571
    iget-object v0, p0, LX/DHF;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DHH;

    iget-object v2, p2, LX/DHE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/DHE;->b:LX/1Po;

    const/4 v5, 0x2

    const/4 p0, 0x0

    const/4 v7, 0x1

    .line 1981572
    invoke-static {v0, v2, v3}, LX/DHH;->a(LX/DHH;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/3Qx;

    move-result-object v4

    .line 1981573
    if-nez v4, :cond_0

    .line 1981574
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    .line 1981575
    :goto_0
    move-object v2, v4

    .line 1981576
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1981577
    check-cast v0, LX/3Qx;

    iput-object v0, p2, LX/DHE;->c:LX/3Qx;

    .line 1981578
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1981579
    return-object v2

    .line 1981580
    :cond_0
    iput-object v4, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1981581
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    const v6, 0x7f0b0063

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0f28

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a84

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    .line 1981582
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    const v6, 0x7f081862

    invoke-virtual {v5, v6}, LX/1ne;->h(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a0443

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    .line 1981583
    const v6, -0x5f9ad2c8

    const/4 v0, 0x0

    invoke-static {p1, v6, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 1981584
    invoke-interface {v5, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    .line 1981585
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x4

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    const/4 v6, -0x1

    invoke-interface {v5, v6}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1981586
    invoke-static {}, LX/1dS;->b()V

    .line 1981587
    iget v0, p1, LX/1dQ;->b:I

    .line 1981588
    packed-switch v0, :pswitch_data_0

    .line 1981589
    :goto_0
    return-object v2

    .line 1981590
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1981591
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1981592
    check-cast v1, LX/DHE;

    .line 1981593
    iget-object v3, p0, LX/DHF;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DHH;

    iget-object v4, v1, LX/DHE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, v1, LX/DHE;->c:LX/3Qx;

    .line 1981594
    if-eqz v5, :cond_0

    .line 1981595
    invoke-virtual {v5, v0}, LX/3Qx;->onClick(Landroid/view/View;)V

    .line 1981596
    iget-object p2, v3, LX/DHH;->d:LX/6W7;

    .line 1981597
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1981598
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 1981599
    const-string p0, "single_creator_set_see_all_click"

    invoke-static {p0}, LX/6W7;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v1, "story_set_id"

    invoke-static {p1}, LX/6W7;->c(Lcom/facebook/graphql/model/GraphQLStorySet;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v1, "creator_id"

    invoke-static {p1}, LX/6W7;->d(Lcom/facebook/graphql/model/GraphQLStorySet;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 1981600
    invoke-static {p2, p0}, LX/6W7;->a(LX/6W7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1981601
    iget-object p2, v3, LX/DHH;->e:LX/2yK;

    sget-object p0, LX/0ig;->A:LX/0ih;

    .line 1981602
    iget-object p1, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1981603
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStorySet;

    const/4 v1, 0x2

    invoke-virtual {p2, p0, p1, v1}, LX/2yK;->a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;I)V

    .line 1981604
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x5f9ad2c8
        :pswitch_0
    .end packed-switch
.end method
