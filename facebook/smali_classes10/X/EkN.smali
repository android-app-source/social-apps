.class public abstract LX/EkN;
.super LX/1OM;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "LX/1a1;",
        ">",
        "LX/1OM",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/database/Cursor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Landroid/database/DataSetObserver;

.field public d:Z

.field public e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2163350
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EkN;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 2163351
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4
    .param p2    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2163352
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2163353
    iput-object p1, p0, LX/EkN;->a:Landroid/content/Context;

    .line 2163354
    iput-object p2, p0, LX/EkN;->b:Landroid/database/Cursor;

    .line 2163355
    if-eqz p2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/EkN;->d:Z

    .line 2163356
    iget-boolean v0, p0, LX/EkN;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    const-string v3, "_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    :goto_1
    iput v0, p0, LX/EkN;->e:I

    .line 2163357
    new-instance v0, LX/EkP;

    invoke-direct {v0, p0}, LX/EkP;-><init>(LX/EkN;)V

    iput-object v0, p0, LX/EkN;->c:Landroid/database/DataSetObserver;

    .line 2163358
    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 2163359
    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    iget-object v2, p0, LX/EkN;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2163360
    :cond_0
    invoke-virtual {p0, v1}, LX/1OM;->a(Z)V

    .line 2163361
    return-void

    :cond_1
    move v0, v2

    .line 2163362
    goto :goto_0

    .line 2163363
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2163364
    iget-boolean v0, p0, LX/EkN;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2163365
    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    iget v1, p0, LX/EkN;->e:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 2163366
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public a(LX/1a1;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;I)V"
        }
    .end annotation

    .prologue
    .line 2163367
    iget-boolean v0, p0, LX/EkN;->d:Z

    if-nez v0, :cond_0

    .line 2163368
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this should only be called when the cursor is valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163369
    :cond_0
    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2163370
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "couldn\'t move cursor to position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163371
    :cond_1
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 2163372
    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    if-ne p1, v0, :cond_1

    .line 2163373
    const/4 v0, 0x0

    .line 2163374
    :goto_0
    move-object v0, v0

    .line 2163375
    if-eqz v0, :cond_0

    .line 2163376
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2163377
    :cond_0
    return-void

    .line 2163378
    :cond_1
    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    .line 2163379
    if-eqz v0, :cond_2

    iget-object v1, p0, LX/EkN;->c:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_2

    .line 2163380
    iget-object v1, p0, LX/EkN;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2163381
    :cond_2
    iput-object p1, p0, LX/EkN;->b:Landroid/database/Cursor;

    .line 2163382
    iget-object v1, p0, LX/EkN;->b:Landroid/database/Cursor;

    if-eqz v1, :cond_4

    .line 2163383
    iget-object v1, p0, LX/EkN;->c:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_3

    .line 2163384
    iget-object v1, p0, LX/EkN;->b:Landroid/database/Cursor;

    iget-object v2, p0, LX/EkN;->c:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2163385
    :cond_3
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, LX/EkN;->e:I

    .line 2163386
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/EkN;->d:Z

    .line 2163387
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0

    .line 2163388
    :cond_4
    const/4 v1, -0x1

    iput v1, p0, LX/EkN;->e:I

    .line 2163389
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EkN;->d:Z

    .line 2163390
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2163391
    const/4 v0, 0x1

    invoke-super {p0, v0}, LX/1OM;->a(Z)V

    .line 2163392
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2163393
    iget-boolean v0, p0, LX/EkN;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 2163394
    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 2163395
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
