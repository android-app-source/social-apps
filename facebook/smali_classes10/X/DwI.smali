.class public LX/DwI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2060277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2060278
    iput-object p1, p0, LX/DwI;->a:LX/0ad;

    .line 2060279
    return-void
.end method

.method public static b(LX/0QB;)LX/DwI;
    .locals 2

    .prologue
    .line 2060280
    new-instance v1, LX/DwI;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/DwI;-><init>(LX/0ad;)V

    .line 2060281
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z
    .locals 3
    .param p2    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2060282
    invoke-static {p2}, LX/DwG;->c(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, LX/DwG;->b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, LX/DwG;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->SHARED:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/DwI;->a:LX/0ad;

    sget-short v2, LX/1EB;->as:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
