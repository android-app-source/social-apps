.class public LX/DMJ;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/2ht;


# instance fields
.field public final a:LX/17W;

.field private final b:LX/6RZ;

.field private final c:Landroid/content/res/Resources;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/EventTimeframe;
    .end annotation
.end field

.field public final g:LX/DMP;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/38v;LX/DMP;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/6RZ;Landroid/content/res/Resources;LX/17W;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/EventTimeframe;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/DMP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/38v;",
            "LX/DMP;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/6RZ;",
            "Landroid/content/res/Resources;",
            "LX/17W;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1989716
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1989717
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/DMJ;->h:Ljava/util/List;

    .line 1989718
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1989719
    iput-object v0, p0, LX/DMJ;->i:LX/0Px;

    .line 1989720
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1989721
    iput-object v0, p0, LX/DMJ;->j:LX/0Px;

    .line 1989722
    iput-object p1, p0, LX/DMJ;->f:Ljava/lang/String;

    .line 1989723
    iput-object p3, p0, LX/DMJ;->g:LX/DMP;

    .line 1989724
    iput-object p4, p0, LX/DMJ;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1989725
    iput-object p5, p0, LX/DMJ;->e:LX/0Or;

    .line 1989726
    iput-object p6, p0, LX/DMJ;->b:LX/6RZ;

    .line 1989727
    iput-object p7, p0, LX/DMJ;->c:Landroid/content/res/Resources;

    .line 1989728
    iput-object p8, p0, LX/DMJ;->a:LX/17W;

    .line 1989729
    invoke-direct {p0}, LX/DMJ;->b()V

    .line 1989730
    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;JLX/6Rc;LX/6Rc;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1989779
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v0

    .line 1989780
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 1989781
    iget-object v1, p0, LX/DMJ;->b:LX/6RZ;

    invoke-virtual {v1, v2, v3, p2, p3}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v1

    .line 1989782
    sget-object v4, LX/DMH;->a:[I

    invoke-virtual {v1}, LX/6RY;->ordinal()I

    move-result v1

    aget v1, v4, v1

    packed-switch v1, :pswitch_data_0

    .line 1989783
    :cond_0
    invoke-virtual {p5, v2, v3}, LX/6Rc;->c(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1989784
    iget-object v1, p0, LX/DMJ;->b:LX/6RZ;

    invoke-virtual {v1, v0}, LX/6RZ;->f(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1989785
    :goto_0
    return-object v0

    .line 1989786
    :pswitch_0
    iget-object v0, p0, LX/DMJ;->b:LX/6RZ;

    .line 1989787
    iget-object v1, v0, LX/6RZ;->o:Ljava/lang/String;

    move-object v0, v1

    .line 1989788
    goto :goto_0

    .line 1989789
    :pswitch_1
    iget-object v0, p0, LX/DMJ;->b:LX/6RZ;

    .line 1989790
    iget-object v1, v0, LX/6RZ;->p:Ljava/lang/String;

    move-object v0, v1

    .line 1989791
    goto :goto_0

    .line 1989792
    :pswitch_2
    iget-object v0, p0, LX/DMJ;->b:LX/6RZ;

    .line 1989793
    iget-object v1, v0, LX/6RZ;->q:Ljava/lang/String;

    move-object v0, v1

    .line 1989794
    goto :goto_0

    .line 1989795
    :pswitch_3
    iget-object v1, p0, LX/DMJ;->b:LX/6RZ;

    invoke-virtual {v1, v0}, LX/6RZ;->e(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1989796
    :pswitch_4
    iget-object v0, p0, LX/DMJ;->c:Landroid/content/res/Resources;

    const v1, 0x7f0830c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1989797
    :pswitch_5
    invoke-virtual {p4, v2, v3}, LX/6Rc;->c(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1989798
    iget-object v1, p0, LX/DMJ;->b:LX/6RZ;

    invoke-virtual {v1, v0}, LX/6RZ;->h(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1989799
    :cond_1
    iget-object v1, p0, LX/DMJ;->b:LX/6RZ;

    invoke-virtual {v1, v0}, LX/6RZ;->g(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private b()V
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 1989755
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1989756
    iput-object v0, p0, LX/DMJ;->i:LX/0Px;

    .line 1989757
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1989758
    iget-object v0, p0, LX/DMJ;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1989759
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1989760
    invoke-static {v2, v3}, LX/6Rc;->a(J)LX/6Rc;

    move-result-object v4

    .line 1989761
    invoke-static {v2, v3}, LX/6Rc;->b(J)LX/6Rc;

    move-result-object v5

    .line 1989762
    iget-object v0, p0, LX/DMJ;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1989763
    iget-object v0, p0, LX/DMJ;->j:LX/0Px;

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/model/Event;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/DMJ;->a(Lcom/facebook/events/model/Event;JLX/6Rc;LX/6Rc;)Ljava/lang/String;

    move-result-object v7

    .line 1989764
    new-instance v0, LX/DMD;

    sget-object v1, LX/DMO;->b:LX/DML;

    invoke-direct {v0, p0, v1, v7}, LX/DMD;-><init>(LX/DMJ;LX/DML;Ljava/lang/String;)V

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1989765
    iget-object v0, p0, LX/DMJ;->h:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1989766
    iget-object v0, p0, LX/DMJ;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v10

    move v8, v6

    :goto_0
    if-ge v8, v10, :cond_0

    iget-object v0, p0, LX/DMJ;->j:LX/0Px;

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/model/Event;

    move-object v0, p0

    .line 1989767
    invoke-direct/range {v0 .. v5}, LX/DMJ;->a(Lcom/facebook/events/model/Event;JLX/6Rc;LX/6Rc;)Ljava/lang/String;

    move-result-object v6

    .line 1989768
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1989769
    new-instance v0, LX/DME;

    sget-object v7, LX/DMO;->b:LX/DML;

    invoke-direct {v0, p0, v7, v6}, LX/DME;-><init>(LX/DMJ;LX/DML;Ljava/lang/String;)V

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1989770
    iget-object v0, p0, LX/DMJ;->h:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v6

    .line 1989771
    :goto_1
    new-instance v7, LX/DMG;

    sget-object v11, LX/DMO;->a:LX/DML;

    invoke-direct {v7, p0, v11, v1}, LX/DMG;-><init>(LX/DMJ;LX/DML;Lcom/facebook/events/model/Event;)V

    invoke-virtual {v9, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1989772
    iget-object v1, p0, LX/DMJ;->h:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1989773
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move-object v7, v0

    goto :goto_0

    .line 1989774
    :cond_0
    iget-boolean v0, p0, LX/DMJ;->k:Z

    move v0, v0

    .line 1989775
    if-eqz v0, :cond_1

    .line 1989776
    new-instance v0, LX/DaP;

    sget-object v1, LX/DMO;->c:LX/DML;

    invoke-direct {v0, v1}, LX/DaP;-><init>(LX/DML;)V

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1989777
    :cond_1
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/DMJ;->i:LX/0Px;

    .line 1989778
    return-void

    :cond_2
    move-object v0, v7

    goto :goto_1
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1989754
    sget-object v0, LX/DMO;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-interface {v0, p2}, LX/DML;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1989751
    check-cast p2, LX/DMB;

    .line 1989752
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 1989753
    return-void
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1989745
    if-nez p2, :cond_1

    .line 1989746
    new-instance v0, LX/DM9;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DM9;-><init>(Landroid/content/Context;)V

    .line 1989747
    :goto_0
    check-cast v0, LX/DM9;

    .line 1989748
    iget-object v1, p0, LX/DMJ;->j:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/DMJ;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 1989749
    iget-object v1, p0, LX/DMJ;->h:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/DM9;->a(Ljava/lang/String;)V

    .line 1989750
    :cond_0
    return-object v0

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1989744
    const/4 v0, 0x0

    return v0
.end method

.method public final e(I)I
    .locals 3

    .prologue
    .line 1989743
    iget-object v0, p0, LX/DMJ;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b2047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget-object v1, p0, LX/DMJ;->c:Landroid/content/res/Resources;

    const v2, 0x7f0b11e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final f(I)Z
    .locals 1

    .prologue
    .line 1989742
    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1989741
    iget-object v0, p0, LX/DMJ;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1989740
    iget-object v0, p0, LX/DMJ;->i:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1989739
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1989738
    sget-object v1, LX/DMO;->d:LX/0Px;

    iget-object v0, p0, LX/DMJ;->i:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1989737
    sget-object v0, LX/DMO;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 1989734
    invoke-direct {p0}, LX/DMJ;->b()V

    .line 1989735
    invoke-super {p0}, LX/1Cv;->notifyDataSetChanged()V

    .line 1989736
    return-void
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 1989731
    if-nez p1, :cond_0

    .line 1989732
    sget-object v0, LX/DMI;->EVENT_TIME_BUCKET_HEADER:LX/DMI;

    invoke-virtual {v0}, LX/DMI;->ordinal()I

    move-result v0

    .line 1989733
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
