.class public final LX/EeN;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/EeS;


# direct methods
.method public constructor <init>(LX/EeS;)V
    .locals 0

    .prologue
    .line 2152459
    iput-object p1, p0, LX/EeN;->a:LX/EeS;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x1fe282ce

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2152460
    const-string v0, "extra_download_id"

    invoke-virtual {p2, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2152461
    iget-object v4, p0, LX/EeN;->a:LX/EeS;

    monitor-enter v4

    .line 2152462
    cmp-long v0, v2, v6

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, LX/EeN;->a:LX/EeS;

    iget-object v0, v0, LX/EeS;->o:LX/EeX;

    iget-wide v6, v0, LX/EeX;->downloadId:J

    cmp-long v0, v2, v6

    if-nez v0, :cond_1

    .line 2152463
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_0

    .line 2152464
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Completing download in "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152465
    :cond_0
    iget-object v0, p0, LX/EeN;->a:LX/EeS;

    iget-object v2, p0, LX/EeN;->a:LX/EeS;

    iget-object v2, v2, LX/EeS;->k:LX/Eek;

    const-wide/16 v6, 0x0

    invoke-static {v0, v2, v6, v7}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V

    .line 2152466
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152467
    const v0, -0x3dea3f66

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    return-void

    .line 2152468
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const v2, 0x3edd117c

    invoke-static {p2, v2, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    throw v0
.end method
