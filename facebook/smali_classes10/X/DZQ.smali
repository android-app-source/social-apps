.class public final LX/DZQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DZW;


# direct methods
.method public constructor <init>(LX/DZW;)V
    .locals 0

    .prologue
    .line 2013354
    iput-object p1, p0, LX/DZQ;->a:LX/DZW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0xa60dcb8

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2013331
    check-cast p1, LX/Daz;

    .line 2013332
    iget-object v1, p1, LX/Daz;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2013333
    iget-object v2, p0, LX/DZQ;->a:LX/DZW;

    iget-object v2, v2, LX/DZW;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2013334
    iget-object v2, p0, LX/DZQ;->a:LX/DZW;

    .line 2013335
    iput-object v1, v2, LX/DZW;->i:Ljava/lang/String;

    .line 2013336
    iget-object v1, p0, LX/DZQ;->a:LX/DZW;

    invoke-static {v1}, LX/DZW;->c(LX/DZW;)V

    .line 2013337
    iget-object v1, p0, LX/DZQ;->a:LX/DZW;

    iget-object v1, v1, LX/DZW;->b:LX/DZH;

    iget-object v2, p0, LX/DZQ;->a:LX/DZW;

    iget-object v2, v2, LX/DZW;->o:Ljava/lang/String;

    iget-object v3, p0, LX/DZQ;->a:LX/DZW;

    iget-object v3, v3, LX/DZW;->n:LX/DQn;

    invoke-interface {v3}, LX/DQn;->kx_()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v3

    iget-object v4, p0, LX/DZQ;->a:LX/DZW;

    iget-object v4, v4, LX/DZW;->i:Ljava/lang/String;

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v4

    .line 2013338
    iget-object v6, v1, LX/DZH;->h:LX/DQQ;

    if-eqz v6, :cond_0

    .line 2013339
    iget-object v6, v1, LX/DZH;->h:LX/DQQ;

    invoke-interface {v6, v4}, LX/DQQ;->a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V

    .line 2013340
    :cond_0
    new-instance v6, LX/4GE;

    invoke-direct {v6}, LX/4GE;-><init>()V

    .line 2013341
    const-string p0, "group_id"

    invoke-virtual {v6, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013342
    move-object p0, v6

    .line 2013343
    sget-object v6, LX/DZH;->b:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2013344
    const-string p1, "setting"

    invoke-virtual {p0, p1, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013345
    move-object v6, p0

    .line 2013346
    iget-object p0, v1, LX/DZH;->g:Ljava/lang/String;

    .line 2013347
    const-string p1, "actor_id"

    invoke-virtual {v6, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013348
    move-object v6, v6

    .line 2013349
    new-instance p0, LX/DZj;

    invoke-direct {p0}, LX/DZj;-><init>()V

    move-object p0, p0

    .line 2013350
    const-string p1, "input"

    invoke-virtual {p0, p1, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/DZj;

    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    .line 2013351
    iget-object p0, v1, LX/DZH;->f:LX/0tX;

    invoke-virtual {p0, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2013352
    new-instance p0, LX/DZF;

    invoke-direct {p0, v1, v3}, LX/DZF;-><init>(LX/DZH;Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V

    iget-object p1, v1, LX/DZH;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2013353
    :cond_1
    const v1, -0x7b0718d6

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
