.class public final LX/Eji;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2162451
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 2162452
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2162453
    :goto_0
    return v1

    .line 2162454
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_6

    .line 2162455
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2162456
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2162457
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 2162458
    const-string v8, "do_base_hashes_match"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2162459
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 2162460
    :cond_1
    const-string v8, "server_contact_hashes"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2162461
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2162462
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_2

    .line 2162463
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_2

    .line 2162464
    const/4 v8, 0x0

    .line 2162465
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_d

    .line 2162466
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2162467
    :goto_3
    move v7, v8

    .line 2162468
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2162469
    :cond_2
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 2162470
    goto :goto_1

    .line 2162471
    :cond_3
    const-string v8, "session_id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2162472
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2162473
    :cond_4
    const-string v8, "setting"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2162474
    invoke-static {p0, p1}, LX/Ejh;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2162475
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2162476
    :cond_6
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2162477
    if-eqz v0, :cond_7

    .line 2162478
    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 2162479
    :cond_7
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 2162480
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2162481
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2162482
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1

    .line 2162483
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2162484
    :cond_a
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_c

    .line 2162485
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2162486
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2162487
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_a

    if-eqz v10, :cond_a

    .line 2162488
    const-string v11, "hash"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 2162489
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 2162490
    :cond_b
    const-string v11, "record_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2162491
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 2162492
    :cond_c
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2162493
    invoke-virtual {p1, v8, v9}, LX/186;->b(II)V

    .line 2162494
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 2162495
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_d
    move v7, v8

    move v9, v8

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2162384
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2162385
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2162386
    if-eqz v0, :cond_0

    .line 2162387
    const-string v1, "do_base_hashes_match"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162388
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2162389
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2162390
    if-eqz v0, :cond_4

    .line 2162391
    const-string v1, "server_contact_hashes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162392
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2162393
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 2162394
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 2162395
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2162396
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2162397
    if-eqz v3, :cond_1

    .line 2162398
    const-string p3, "hash"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162399
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2162400
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2162401
    if-eqz v3, :cond_2

    .line 2162402
    const-string p3, "record_id"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162403
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2162404
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2162405
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2162406
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2162407
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2162408
    if-eqz v0, :cond_5

    .line 2162409
    const-string v1, "session_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162410
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2162411
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2162412
    if-eqz v0, :cond_e

    .line 2162413
    const-string v1, "setting"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162414
    const/4 p1, 0x1

    const/4 v3, 0x0

    .line 2162415
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2162416
    invoke-virtual {p0, v0, v3, v3}, LX/15i;->a(III)I

    move-result v1

    .line 2162417
    if-eqz v1, :cond_6

    .line 2162418
    const-string v2, "batch_size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162419
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2162420
    :cond_6
    invoke-virtual {p0, v0, p1}, LX/15i;->g(II)I

    move-result v1

    .line 2162421
    if-eqz v1, :cond_7

    .line 2162422
    const-string v1, "field_setting"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162423
    invoke-virtual {p0, v0, p1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2162424
    :cond_7
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 2162425
    if-eqz v1, :cond_8

    .line 2162426
    const-string v2, "max_concurrent_batches"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162427
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2162428
    :cond_8
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 2162429
    if-eqz v1, :cond_9

    .line 2162430
    const-string v2, "max_num_contacts"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162431
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2162432
    :cond_9
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 2162433
    if-eqz v1, :cond_a

    .line 2162434
    const-string v2, "max_num_emails_in_contact"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162435
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2162436
    :cond_a
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 2162437
    if-eqz v1, :cond_b

    .line 2162438
    const-string v2, "max_num_phones_in_contact"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162439
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2162440
    :cond_b
    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 2162441
    if-eqz v1, :cond_c

    .line 2162442
    const-string v2, "max_num_retries"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162443
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2162444
    :cond_c
    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1, v3}, LX/15i;->a(III)I

    move-result v1

    .line 2162445
    if-eqz v1, :cond_d

    .line 2162446
    const-string v2, "upload_interval"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162447
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2162448
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2162449
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2162450
    return-void
.end method
