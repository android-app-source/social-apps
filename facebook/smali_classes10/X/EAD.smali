.class public final LX/EAD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EA0;

.field public final synthetic b:LX/EAH;


# direct methods
.method public constructor <init>(LX/EAH;LX/EA0;)V
    .locals 0

    .prologue
    .line 2085350
    iput-object p1, p0, LX/EAD;->b:LX/EAH;

    iput-object p2, p0, LX/EAD;->a:LX/EA0;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2085351
    iget-object v0, p0, LX/EAD;->a:LX/EA0;

    .line 2085352
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/EA0;->k:Z

    .line 2085353
    iget-object v1, v0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v1}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->n()V

    .line 2085354
    iget-object v1, v0, LX/EA0;->g:LX/79D;

    iget-object v2, v0, LX/EA0;->t:Ljava/lang/String;

    .line 2085355
    const-string p0, "reviews_feed_header_load_failure"

    const-string p1, "reviews_feed"

    invoke-static {v1, p0, p1, v2}, LX/79D;->b(LX/79D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2085356
    iget-object v1, v0, LX/EA0;->i:LX/0kL;

    new-instance v2, LX/27k;

    const p0, 0x7f0814f8

    invoke-direct {v2, p0}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2085357
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2085358
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2085359
    iget-object v1, p0, LX/EAD;->a:LX/EA0;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/EA0;->a(Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;Z)V

    .line 2085360
    return-void

    .line 2085361
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2085362
    check-cast v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

    goto :goto_0
.end method
