.class public LX/Exw;
.super LX/Exj;
.source ""


# instance fields
.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Eus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/Exs;LX/0Ot;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Exs;",
            "LX/0Ot",
            "<",
            "LX/Exz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2185630
    invoke-direct {p0, p1, p2}, LX/Exj;-><init>(LX/Exs;LX/0Ot;)V

    .line 2185631
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Exw;->c:Ljava/util/List;

    .line 2185632
    return-void
.end method

.method public static b(LX/0QB;)LX/Exw;
    .locals 3

    .prologue
    .line 2185633
    new-instance v1, LX/Exw;

    invoke-static {p0}, LX/Exs;->b(LX/0QB;)LX/Exs;

    move-result-object v0

    check-cast v0, LX/Exs;

    const/16 v2, 0x2246

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/Exw;-><init>(LX/Exs;LX/0Ot;)V

    .line 2185634
    return-object v1
.end method


# virtual methods
.method public final a(J)I
    .locals 7

    .prologue
    .line 2185635
    iget-object v0, p0, LX/Exw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 2185636
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2185637
    iget-object v0, p0, LX/Exw;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eus;

    invoke-virtual {v0}, LX/Eus;->a()J

    move-result-wide v4

    cmp-long v0, v4, p1

    if-nez v0, :cond_0

    move v0, v1

    .line 2185638
    :goto_1
    return v0

    .line 2185639
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2185640
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final a(I)LX/Eus;
    .locals 1

    .prologue
    .line 2185641
    iget-boolean v0, p0, LX/Exj;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Exw;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    .line 2185642
    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 2185643
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2185644
    :cond_1
    iget-object v0, p0, LX/Exw;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eus;

    goto :goto_1
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/Eus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2185645
    iget-object v0, p0, LX/Exw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2185646
    iget-object v0, p0, LX/Exw;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2185647
    const v0, -0x5a1bd786

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2185648
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2185649
    iget-boolean v0, p0, LX/Exj;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Exw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2185650
    :goto_0
    iget-object v1, p0, LX/Exw;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 2185651
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
