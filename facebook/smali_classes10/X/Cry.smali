.class public LX/Cry;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/GmZ;

.field public final b:Landroid/animation/ValueAnimator;

.field private c:Landroid/animation/ValueAnimator;

.field public d:F

.field public e:F

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>(LX/GmZ;J)V
    .locals 2

    .prologue
    .line 1941583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941584
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/Cry;->b:Landroid/animation/ValueAnimator;

    .line 1941585
    iput-object p1, p0, LX/Cry;->a:LX/GmZ;

    .line 1941586
    iget-object v0, p0, LX/Cry;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p2, p3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1941587
    iget-object v0, p0, LX/Cry;->b:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1941588
    iget-object v0, p0, LX/Cry;->b:Landroid/animation/ValueAnimator;

    new-instance v1, LX/Cru;

    invoke-direct {v1, p0}, LX/Cru;-><init>(LX/Cry;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1941589
    iget-object v0, p0, LX/Cry;->b:Landroid/animation/ValueAnimator;

    new-instance v1, LX/Crv;

    invoke-direct {v1, p0}, LX/Crv;-><init>(LX/Cry;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1941590
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f333333    # 0.7f
    .end array-data
.end method

.method public static c(LX/Cry;)V
    .locals 3

    .prologue
    .line 1941602
    iget-object v0, p0, LX/Cry;->a:LX/GmZ;

    iget v1, p0, LX/Cry;->d:F

    iget v2, p0, LX/Cry;->e:F

    add-float/2addr v1, v2

    .line 1941603
    iget-object v2, v0, LX/GmZ;->ac:LX/Csi;

    if-eqz v2, :cond_0

    .line 1941604
    iget-object v2, v0, LX/GmZ;->ac:LX/Csi;

    invoke-interface {v2, v1}, LX/Csi;->a(F)V

    .line 1941605
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-nez v2, :cond_1

    .line 1941606
    iget-object v2, v0, LX/GmZ;->ac:LX/Csi;

    invoke-interface {v2}, LX/Csi;->b()V

    .line 1941607
    :cond_0
    :goto_0
    return-void

    .line 1941608
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-nez v2, :cond_0

    .line 1941609
    iget-object v2, v0, LX/GmZ;->ac:LX/Csi;

    invoke-interface {v2}, LX/Csi;->e()V

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1941591
    iget-boolean v0, p0, LX/Cry;->f:Z

    if-eqz v0, :cond_0

    .line 1941592
    iput-boolean v3, p0, LX/Cry;->g:Z

    .line 1941593
    iget-object v0, p0, LX/Cry;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1941594
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v1, v0, v4

    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, LX/Cry;->d:F

    sub-float/2addr v1, v2

    aput v1, v0, v3

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/Cry;->c:Landroid/animation/ValueAnimator;

    .line 1941595
    iget-object v0, p0, LX/Cry;->c:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1941596
    iget-object v0, p0, LX/Cry;->c:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1941597
    iget-object v0, p0, LX/Cry;->c:Landroid/animation/ValueAnimator;

    new-instance v1, LX/Crw;

    invoke-direct {v1, p0}, LX/Crw;-><init>(LX/Cry;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1941598
    iget-object v0, p0, LX/Cry;->c:Landroid/animation/ValueAnimator;

    new-instance v1, LX/Crx;

    invoke-direct {v1, p0}, LX/Crx;-><init>(LX/Cry;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1941599
    iget-object v0, p0, LX/Cry;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1941600
    iput-boolean v4, p0, LX/Cry;->f:Z

    .line 1941601
    :cond_0
    return-void
.end method
