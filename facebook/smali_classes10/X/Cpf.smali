.class public LX/Cpf;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;
.implements LX/20T;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Co7;",
        ">;",
        "Lcom/facebook/richdocument/view/block/ShareBlockView;",
        "LX/20T;"
    }
.end annotation


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:F

.field private final d:F

.field private final e:D

.field private final f:D

.field public g:LX/215;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Landroid/widget/FrameLayout;

.field private i:Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const v3, 0x3f4ccccd    # 0.8f

    const-wide/high16 v6, 0x402e000000000000L    # 15.0

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    .line 1938595
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1938596
    iput v3, p0, LX/Cpf;->c:F

    .line 1938597
    iput v8, p0, LX/Cpf;->d:F

    .line 1938598
    iput-wide v6, p0, LX/Cpf;->e:D

    .line 1938599
    iput-wide v4, p0, LX/Cpf;->f:D

    .line 1938600
    const-class v0, LX/Cpf;

    invoke-static {v0, p0}, LX/Cpf;->a(Ljava/lang/Class;LX/02k;)V

    .line 1938601
    iget-object v0, p0, LX/Cpf;->b:LX/Ck0;

    const/4 v1, 0x0

    const v2, 0x7f0d0176

    invoke-virtual {v0, p1, v1, v2}, LX/Ck0;->a(Landroid/view/View;II)V

    .line 1938602
    const v0, 0x7f0d2a4c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Cpf;->h:Landroid/widget/FrameLayout;

    .line 1938603
    const v0, 0x7f0d16d3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;

    iput-object v0, p0, LX/Cpf;->i:Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;

    .line 1938604
    iget-object v0, p0, LX/Cpf;->h:Landroid/widget/FrameLayout;

    new-instance v1, LX/Cpe;

    invoke-direct {v1, p0}, LX/Cpe;-><init>(LX/Cpf;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1938605
    iget-object v0, p0, LX/Cpf;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    iput-object v0, p0, LX/Cpf;->g:LX/215;

    .line 1938606
    iget-object v0, p0, LX/Cpf;->g:LX/215;

    .line 1938607
    iput v3, v0, LX/215;->b:F

    .line 1938608
    iget-object v0, p0, LX/Cpf;->g:LX/215;

    .line 1938609
    iput v8, v0, LX/215;->c:F

    .line 1938610
    iget-object v0, p0, LX/Cpf;->g:LX/215;

    invoke-static {v6, v7, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/215;->a(LX/0wT;)V

    .line 1938611
    iget-object v0, p0, LX/Cpf;->g:LX/215;

    invoke-virtual {v0, p0}, LX/215;->a(LX/20T;)V

    .line 1938612
    iget-object v0, p0, LX/Cpf;->i:Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0619

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v0, v1}, LX/CoV;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 1938613
    iget-object v0, p0, LX/Cpf;->b:LX/Ck0;

    invoke-virtual {v0, p1}, LX/Ck0;->a(Landroid/view/View;)V

    .line 1938614
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Cpf;

    const/16 p0, 0x13a4

    invoke-static {v1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v1}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v1

    check-cast v1, LX/Ck0;

    iput-object p0, p1, LX/Cpf;->a:LX/0Or;

    iput-object v1, p1, LX/Cpf;->b:LX/Ck0;

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 1938615
    iget-object v0, p0, LX/Cpf;->i:Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setScaleX(F)V

    .line 1938616
    iget-object v0, p0, LX/Cpf;->i:Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setScaleY(F)V

    .line 1938617
    return-void
.end method

.method public final isPressed()Z
    .locals 1

    .prologue
    .line 1938618
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isPressed()Z

    move-result v0

    return v0
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 1938619
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    move-result v0

    return v0
.end method
