.class public LX/Dus;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/Long;

.field public final e:LX/Dup;

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/9br;

.field private h:I


# direct methods
.method public constructor <init>(Ljava/lang/Long;LX/Dup;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .param p1    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Dup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "LX/Dup;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2057699
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2057700
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Dus;->f:Ljava/util/ArrayList;

    .line 2057701
    const/4 v0, 0x0

    iput v0, p0, LX/Dus;->h:I

    .line 2057702
    iput-object p1, p0, LX/Dus;->d:Ljava/lang/Long;

    .line 2057703
    iput-object p2, p0, LX/Dus;->e:LX/Dup;

    .line 2057704
    iput-object p3, p0, LX/Dus;->b:LX/0Ot;

    .line 2057705
    iput-object p4, p0, LX/Dus;->c:LX/0Ot;

    .line 2057706
    iput-object p5, p0, LX/Dus;->a:LX/0Ot;

    .line 2057707
    invoke-direct {p0}, LX/Dus;->d()V

    .line 2057708
    return-void
.end method

.method private d()V
    .locals 9

    .prologue
    .line 2057709
    new-instance v1, LX/9bs;

    iget-object v0, p0, LX/Dus;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v0

    iget-object v0, p0, LX/Dus;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v3, 0x7f0b0aba

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    iget-object v0, p0, LX/Dus;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v4, 0x7f0b0abb

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-direct {v1, v2, v3, v0}, LX/9bs;-><init>(FFF)V

    const/high16 v5, 0x42c80000    # 100.0f

    .line 2057710
    iget v0, v1, LX/9bs;->a:F

    iget v2, v1, LX/9bs;->c:F

    add-float/2addr v0, v2

    iget v2, v1, LX/9bs;->c:F

    iget v3, v1, LX/9bs;->b:F

    add-float/2addr v2, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    .line 2057711
    iget v2, v1, LX/9bs;->a:F

    .line 2057712
    const/high16 v3, 0x42c80000    # 100.0f

    iget v4, v1, LX/9bs;->c:F

    add-float/2addr v4, v2

    int-to-float v6, v0

    iget v7, v1, LX/9bs;->b:F

    iget v8, v1, LX/9bs;->c:F

    add-float/2addr v7, v8

    mul-float/2addr v6, v7

    sub-float/2addr v4, v6

    mul-float/2addr v3, v4

    int-to-float v4, v0

    iget v6, v1, LX/9bs;->b:F

    iget v7, v1, LX/9bs;->c:F

    add-float/2addr v6, v7

    mul-float/2addr v4, v6

    iget v6, v1, LX/9bs;->c:F

    sub-float/2addr v4, v6

    div-float/2addr v3, v4

    move v2, v3

    .line 2057713
    iget v3, v1, LX/9bs;->b:F

    add-float v4, v2, v5

    mul-float/2addr v3, v4

    div-float/2addr v3, v5

    float-to-int v3, v3

    .line 2057714
    iget v4, v1, LX/9bs;->c:F

    add-float/2addr v2, v5

    mul-float/2addr v2, v4

    div-float/2addr v2, v5

    float-to-int v2, v2

    .line 2057715
    new-instance v4, LX/9br;

    invoke-direct {v4, v0, v3, v2}, LX/9br;-><init>(III)V

    move-object v0, v4

    .line 2057716
    iput-object v0, p0, LX/Dus;->g:LX/9br;

    .line 2057717
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2057718
    iget-object v0, p0, LX/Dus;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2057719
    :goto_0
    return-void

    .line 2057720
    :cond_0
    iget-object v0, p0, LX/Dus;->g:LX/9br;

    .line 2057721
    iget v1, v0, LX/9br;->b:I

    move v0, v1

    .line 2057722
    iget-object v1, p0, LX/Dus;->e:LX/Dup;

    .line 2057723
    sget-object v4, LX/Dur;->a:[I

    invoke-virtual {v1}, LX/Dup;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2057724
    const/4 v4, 0x0

    const-string v5, "Unhandled case"

    invoke-static {v4, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2057725
    const/4 v4, 0x0

    :goto_1
    move-object v1, v4

    .line 2057726
    iget-object v0, p0, LX/Dus;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2057727
    new-instance v2, LX/Duq;

    invoke-direct {v2, p0}, LX/Duq;-><init>(LX/Dus;)V

    .line 2057728
    iget-object v0, p0, LX/Dus;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    .line 2057729
    sget-object v3, LX/Dur;->a:[I

    iget-object v4, p0, LX/Dus;->e:LX/Dup;

    invoke-virtual {v4}, LX/Dup;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 2057730
    const/4 v3, 0x0

    const-string v4, "Unhandled case"

    invoke-static {v3, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2057731
    const/4 v3, 0x0

    :goto_2
    move-object v3, v3

    .line 2057732
    invoke-virtual {v0, v3, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2057733
    :pswitch_0
    invoke-static {}, LX/5hT;->a()LX/5hR;

    move-result-object v4

    .line 2057734
    const-string v5, "node_id"

    iget-object v6, p0, LX/Dus;->d:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "image_width"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "image_height"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "media_type"

    sget-object v7, LX/0wF;->IMAGEWEBP:LX/0wF;

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2057735
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    goto :goto_1

    .line 2057736
    :pswitch_1
    new-instance v4, LX/5hL;

    invoke-direct {v4}, LX/5hL;-><init>()V

    move-object v4, v4

    .line 2057737
    const-string v5, "video_list_id"

    iget-object v6, p0, LX/Dus;->d:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "image_width"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "image_height"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "media_type"

    sget-object v7, LX/0wF;->IMAGEWEBP:LX/0wF;

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2057738
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    goto/16 :goto_1

    .line 2057739
    :pswitch_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tasks-executeVideoUploadedQuery"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/Dus;->d:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 2057740
    :pswitch_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tasks-executeVideosInVideoListQuery"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/Dus;->d:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final getCount()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2057741
    iget-object v1, p0, LX/Dus;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2057742
    :cond_0
    :goto_0
    return v0

    .line 2057743
    :cond_1
    iget-object v1, p0, LX/Dus;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Dus;->g:LX/9br;

    if-eqz v1, :cond_0

    .line 2057744
    iget v0, p0, LX/Dus;->h:I

    if-eqz v0, :cond_2

    iget v1, p0, LX/Dus;->h:I

    iget-object v0, p0, LX/Dus;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v0, :cond_3

    .line 2057745
    :cond_2
    iget-object v0, p0, LX/Dus;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, LX/Dus;->h:I

    .line 2057746
    invoke-direct {p0}, LX/Dus;->d()V

    .line 2057747
    :cond_3
    iget-object v0, p0, LX/Dus;->g:LX/9br;

    .line 2057748
    iget v1, v0, LX/9br;->a:I

    move v0, v1

    .line 2057749
    iget-object v1, p0, LX/Dus;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    rem-int/2addr v1, v0

    if-nez v1, :cond_4

    .line 2057750
    iget-object v1, p0, LX/Dus;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    div-int v0, v1, v0

    goto :goto_0

    .line 2057751
    :cond_4
    iget-object v1, p0, LX/Dus;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    div-int v0, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2057752
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2057753
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    .line 2057754
    iget-object v0, p0, LX/Dus;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2057755
    const/4 v0, 0x0

    .line 2057756
    :goto_0
    return-object v0

    .line 2057757
    :cond_0
    if-eqz p2, :cond_2

    .line 2057758
    check-cast p2, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;

    move-object v0, p2

    .line 2057759
    :goto_1
    iget-object v1, p0, LX/Dus;->g:LX/9br;

    .line 2057760
    iget v2, v1, LX/9br;->a:I

    move v1, v2

    .line 2057761
    iget-object v2, p0, LX/Dus;->g:LX/9br;

    .line 2057762
    iget v3, v2, LX/9br;->b:I

    move v2, v3

    .line 2057763
    iget-object v3, p0, LX/Dus;->g:LX/9br;

    .line 2057764
    iget v4, v3, LX/9br;->c:I

    move v4, v4

    .line 2057765
    int-to-double v2, v2

    int-to-double v4, v4

    .line 2057766
    iget v6, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->e:I

    if-ne v6, v1, :cond_8

    iget-wide v6, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->c:D

    cmpl-double v6, v6, v2

    if-nez v6, :cond_8

    iget-wide v6, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->d:D

    cmpl-double v6, v6, v4

    if-nez v6, :cond_8

    invoke-virtual {v0}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->getChildCount()I

    move-result v6

    if-lez v6, :cond_8

    .line 2057767
    :cond_1
    mul-int v2, p1, v1

    .line 2057768
    add-int/2addr v1, v2

    iget-object v3, p0, LX/Dus;->f:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 2057769
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 2057770
    :goto_2
    if-ge v1, v3, :cond_3

    .line 2057771
    iget-object v2, p0, LX/Dus;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2057772
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2057773
    :cond_2
    new-instance v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 2057774
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    const/16 v7, 0x8

    const/4 v4, 0x0

    .line 2057775
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->getChildCount()I

    move-result v3

    if-gt v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    move v5, v4

    .line 2057776
    :goto_4
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ge v5, v2, :cond_6

    .line 2057777
    iget-object v2, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->g:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2057778
    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2057779
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->S()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    if-nez v6, :cond_5

    .line 2057780
    invoke-virtual {v2, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2057781
    :goto_5
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_4

    :cond_4
    move v2, v4

    .line 2057782
    goto :goto_3

    .line 2057783
    :cond_5
    iget v6, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->b:I

    invoke-virtual {v2, v6, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setTag(ILjava/lang/Object;)V

    .line 2057784
    invoke-virtual {v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2057785
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->S()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v6, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_5

    .line 2057786
    :cond_6
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v3, v2

    :goto_6
    iget v2, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->e:I

    if-ge v3, v2, :cond_7

    .line 2057787
    iget-object v2, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2057788
    iget-object v2, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget v4, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->b:I

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setTag(ILjava/lang/Object;)V

    .line 2057789
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 2057790
    :cond_7
    goto/16 :goto_0

    .line 2057791
    :cond_8
    iput v1, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->e:I

    .line 2057792
    iput-wide v2, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->c:D

    .line 2057793
    iput-wide v4, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->d:D

    .line 2057794
    invoke-virtual {v0}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->removeAllViews()V

    .line 2057795
    iget-object v6, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->g:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 2057796
    invoke-virtual {v0}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 2057797
    new-instance v7, LX/1Uo;

    invoke-direct {v7, v6}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v8, LX/1Up;->e:LX/1Up;

    invoke-virtual {v7, v8}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v7

    const v8, 0x7f021a30

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v7

    .line 2057798
    const/4 v6, 0x0

    :goto_7
    if-ge v6, v1, :cond_1

    .line 2057799
    new-instance v8, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v7}, LX/1Uo;->u()LX/1af;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    .line 2057800
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setTag(Ljava/lang/Object;)V

    .line 2057801
    iget-object v9, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v9}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2057802
    invoke-virtual {v0, v8}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->addView(Landroid/view/View;)V

    .line 2057803
    iget-object v9, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->g:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2057804
    add-int/lit8 v6, v6, 0x1

    goto :goto_7
.end method
