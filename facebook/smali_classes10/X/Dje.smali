.class public final LX/Dje;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dix;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;)V
    .locals 0

    .prologue
    .line 2033405
    iput-object p1, p0, LX/Dje;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2033406
    iget-object v0, p0, LX/Dje;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->d:LX/Dih;

    iget-object v1, p0, LX/Dje;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->o:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v1}, LX/DnS;->g(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Dje;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->o:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v2}, LX/DnS;->h(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v2

    .line 2033407
    iget-object v3, v0, LX/Dih;->a:LX/0Zb;

    const-string v4, "profservices_booking_consumer_cancel_appointment"

    invoke-static {v4, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "request_id"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2033408
    iget-object v0, p0, LX/Dje;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/ThreadAppointmentRequestDetailFragment;->l:LX/DkN;

    new-instance v1, LX/Djd;

    invoke-direct {v1, p0}, LX/Djd;-><init>(LX/Dje;)V

    .line 2033409
    iget-object v2, v0, LX/DkN;->b:LX/1Ck;

    const-string v3, "user_cancel_appointment"

    new-instance v4, LX/DkE;

    invoke-direct {v4, v0}, LX/DkE;-><init>(LX/DkN;)V

    new-instance v5, LX/Dk1;

    invoke-direct {v5, v0, v1}, LX/Dk1;-><init>(LX/DkN;LX/DjG;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2033410
    return-void
.end method
