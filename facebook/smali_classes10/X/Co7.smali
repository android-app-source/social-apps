.class public LX/Co7;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/ShareBlockView;",
        "Lcom/facebook/richdocument/model/data/ShareBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CoV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Cpf;)V
    .locals 3

    .prologue
    .line 1934997
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934998
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Co7;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v2

    check-cast v2, LX/Chi;

    invoke-static {v0}, LX/CoV;->a(LX/0QB;)LX/CoV;

    move-result-object p1

    check-cast p1, LX/CoV;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v0

    check-cast v0, LX/Ckw;

    iput-object v2, p0, LX/Co7;->d:LX/Chi;

    iput-object p1, p0, LX/Co7;->e:LX/CoV;

    iput-object v0, p0, LX/Co7;->f:LX/Ckw;

    .line 1934999
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 3

    .prologue
    .line 1935000
    check-cast p1, LX/Cmc;

    .line 1935001
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935002
    check-cast v0, LX/Cpf;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1935003
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1935004
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935005
    check-cast v0, LX/Cpf;

    new-instance v2, LX/Co6;

    invoke-direct {v2, p0, v1, p1}, LX/Co6;-><init>(LX/Co7;Ljava/lang/String;LX/Cmc;)V

    .line 1935006
    iget-object v1, v0, LX/Cpf;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1935007
    return-void
.end method
