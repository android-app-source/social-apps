.class public final enum LX/Cm2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cm2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cm2;

.field public static final enum BOTTOM:LX/Cm2;

.field public static final enum INLINE:LX/Cm2;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1933153
    new-instance v0, LX/Cm2;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v2}, LX/Cm2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cm2;->BOTTOM:LX/Cm2;

    .line 1933154
    new-instance v0, LX/Cm2;

    const-string v1, "INLINE"

    invoke-direct {v0, v1, v3}, LX/Cm2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cm2;->INLINE:LX/Cm2;

    .line 1933155
    const/4 v0, 0x2

    new-array v0, v0, [LX/Cm2;

    sget-object v1, LX/Cm2;->BOTTOM:LX/Cm2;

    aput-object v1, v0, v2

    sget-object v1, LX/Cm2;->INLINE:LX/Cm2;

    aput-object v1, v0, v3

    sput-object v0, LX/Cm2;->$VALUES:[LX/Cm2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1933156
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cm2;
    .locals 1

    .prologue
    .line 1933157
    const-class v0, LX/Cm2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cm2;

    return-object v0
.end method

.method public static values()[LX/Cm2;
    .locals 1

    .prologue
    .line 1933158
    sget-object v0, LX/Cm2;->$VALUES:[LX/Cm2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cm2;

    return-object v0
.end method
