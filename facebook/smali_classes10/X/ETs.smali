.class public LX/ETs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/video/videohome/environment/HasShownVideosContainer;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2125532
    const-class v0, LX/ETs;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ETs;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2125534
    return-void
.end method

.method public static a(LX/0QB;)LX/ETs;
    .locals 1

    .prologue
    .line 2125535
    new-instance v0, LX/ETs;

    invoke-direct {v0}, LX/ETs;-><init>()V

    .line 2125536
    move-object v0, v0

    .line 2125537
    return-object v0
.end method


# virtual methods
.method public final a(LX/ETy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 2125538
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2125539
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/ETs;->b:Ljava/lang/ref/WeakReference;

    .line 2125540
    return-void
.end method
