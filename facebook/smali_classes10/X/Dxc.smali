.class public LX/Dxc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

.field private final b:LX/BQc;

.field private c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;LX/BQc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2063033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2063034
    iput-object p1, p0, LX/Dxc;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2063035
    iput-object p2, p0, LX/Dxc;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2063036
    iput-object p3, p0, LX/Dxc;->a:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    .line 2063037
    iput-object p4, p0, LX/Dxc;->b:LX/BQc;

    .line 2063038
    return-void
.end method

.method public static b(LX/0QB;)LX/Dxc;
    .locals 5

    .prologue
    .line 2063039
    new-instance v4, LX/Dxc;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {p0}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->b(LX/0QB;)Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    invoke-static {p0}, LX/BQc;->b(LX/0QB;)LX/BQc;

    move-result-object v3

    check-cast v3, LX/BQc;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Dxc;-><init>(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;LX/BQc;)V

    .line 2063040
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)Z
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 2063041
    if-eq p3, v1, :cond_1

    .line 2063042
    :cond_0
    :goto_0
    return v0

    .line 2063043
    :cond_1
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    .line 2063044
    :sswitch_0
    if-eqz p4, :cond_0

    sget-object v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2063045
    iget-object v0, p0, LX/Dxc;->b:LX/BQc;

    sget-object v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    const-string v4, "extra_profile_pic_expiration"

    invoke-virtual {p4, v4, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v4, "staging_ground_photo_caption"

    invoke-virtual {p4, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "profile_photo_method_extra"

    invoke-virtual {p4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/Dxc;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual/range {v0 .. v6}, LX/BQc;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    move v0, v8

    .line 2063046
    goto :goto_0

    .line 2063047
    :sswitch_1
    if-eqz p4, :cond_0

    const-string v1, "extra_media_items"

    invoke-virtual {p4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2063048
    const-string v1, "extra_media_items"

    invoke-virtual {p4, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2063049
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v8, :cond_0

    .line 2063050
    iget-object v1, p0, LX/Dxc;->a:Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v4

    iget-object v0, p0, LX/Dxc;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2063051
    iget-object v5, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v5

    .line 2063052
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    move-object v5, p1

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Landroid/app/Activity;J)V

    move v0, v8

    .line 2063053
    goto :goto_0

    .line 2063054
    :sswitch_2
    invoke-virtual {p1, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2063055
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    move v0, v8

    .line 2063056
    goto :goto_0

    .line 2063057
    :sswitch_3
    invoke-virtual {p1, v1, p4}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2063058
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    move v0, v8

    .line 2063059
    goto :goto_0

    .line 2063060
    :sswitch_4
    invoke-virtual {p1, v1, p4}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2063061
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    move v0, v8

    .line 2063062
    goto :goto_0

    .line 2063063
    :sswitch_5
    iget-object v0, p0, LX/Dxc;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p4}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move v0, v8

    .line 2063064
    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_1
        0x6dc -> :sswitch_5
        0x7d0 -> :sswitch_0
        0x7d1 -> :sswitch_1
        0x26b9 -> :sswitch_4
        0x26ba -> :sswitch_2
        0x26bb -> :sswitch_3
        0x26bc -> :sswitch_2
    .end sparse-switch
.end method
