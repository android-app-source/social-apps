.class public LX/DH6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/DH6;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0bH;

.field private final c:LX/1yo;

.field public final d:LX/6W7;

.field public final e:LX/2yK;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0bH;LX/1yo;LX/6W7;LX/2yK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1981275
    iput-object p1, p0, LX/DH6;->a:Landroid/content/Context;

    .line 1981276
    iput-object p2, p0, LX/DH6;->b:LX/0bH;

    .line 1981277
    iput-object p3, p0, LX/DH6;->c:LX/1yo;

    .line 1981278
    iput-object p4, p0, LX/DH6;->d:LX/6W7;

    .line 1981279
    iput-object p5, p0, LX/DH6;->e:LX/2yK;

    .line 1981280
    return-void
.end method

.method public static a(LX/0QB;)LX/DH6;
    .locals 9

    .prologue
    .line 1981323
    sget-object v0, LX/DH6;->f:LX/DH6;

    if-nez v0, :cond_1

    .line 1981324
    const-class v1, LX/DH6;

    monitor-enter v1

    .line 1981325
    :try_start_0
    sget-object v0, LX/DH6;->f:LX/DH6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1981326
    if-eqz v2, :cond_0

    .line 1981327
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1981328
    new-instance v3, LX/DH6;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {v0}, LX/1yo;->b(LX/0QB;)LX/1yo;

    move-result-object v6

    check-cast v6, LX/1yo;

    invoke-static {v0}, LX/6W7;->a(LX/0QB;)LX/6W7;

    move-result-object v7

    check-cast v7, LX/6W7;

    invoke-static {v0}, LX/2yK;->b(LX/0QB;)LX/2yK;

    move-result-object v8

    check-cast v8, LX/2yK;

    invoke-direct/range {v3 .. v8}, LX/DH6;-><init>(Landroid/content/Context;LX/0bH;LX/1yo;LX/6W7;LX/2yK;)V

    .line 1981329
    move-object v0, v3

    .line 1981330
    sput-object v0, LX/DH6;->f:LX/DH6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981331
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1981332
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1981333
    :cond_1
    sget-object v0, LX/DH6;->f:LX/DH6;

    return-object v0

    .line 1981334
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1981335
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/D4s;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;)",
            "LX/D4s;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1981281
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1981282
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1981283
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1981284
    if-nez v1, :cond_1

    .line 1981285
    :cond_0
    :goto_0
    return-object v2

    .line 1981286
    :cond_1
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1981287
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1981288
    invoke-static {v1}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v4

    .line 1981289
    if-eqz v4, :cond_0

    .line 1981290
    new-instance v5, LX/D4r;

    invoke-direct {v5}, LX/D4r;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v6

    .line 1981291
    iput-object v6, v5, LX/D4r;->a:Ljava/lang/String;

    .line 1981292
    move-object v5, v5

    .line 1981293
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    .line 1981294
    iput-object v6, v5, LX/D4r;->c:Ljava/lang/String;

    .line 1981295
    move-object v5, v5

    .line 1981296
    iput-object v4, v5, LX/D4r;->f:Ljava/lang/String;

    .line 1981297
    move-object v4, v5

    .line 1981298
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 1981299
    const-string v0, "SingleCreatorChannelFeedLauncherParamUtil"

    const-string v3, "Unsupported Actor Type: %d"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v7

    invoke-static {v0, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1981300
    :sswitch_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1981301
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1981302
    iput-object v0, v4, LX/D4r;->d:Ljava/lang/String;

    .line 1981303
    :cond_2
    :goto_1
    invoke-virtual {v4}, LX/D4r;->a()LX/D4s;

    move-result-object v2

    goto :goto_0

    .line 1981304
    :sswitch_1
    invoke-static {v3}, LX/1yo;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 1981305
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1981306
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->w()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1981307
    :cond_3
    iput-object v2, v4, LX/D4r;->d:Ljava/lang/String;

    .line 1981308
    move-object v2, v4

    .line 1981309
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v3

    .line 1981310
    iput-boolean v3, v2, LX/D4r;->g:Z

    .line 1981311
    move-object v2, v2

    .line 1981312
    iput-boolean v8, v2, LX/D4r;->i:Z

    .line 1981313
    move-object v2, v2

    .line 1981314
    iget-object v3, p0, LX/DH6;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f081a47

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1981315
    iput-object v3, v2, LX/D4r;->k:Ljava/lang/String;

    .line 1981316
    move-object v2, v2

    .line 1981317
    iget-object v3, p0, LX/DH6;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f081a46

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1981318
    iput-object v3, v2, LX/D4r;->l:Ljava/lang/String;

    .line 1981319
    move-object v2, v2

    .line 1981320
    new-instance v3, LX/DH5;

    invoke-direct {v3, p0, p1, v1, v0}, LX/DH5;-><init>(LX/DH6;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1981321
    iput-object v3, v2, LX/D4r;->m:LX/BUv;

    .line 1981322
    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
    .end sparse-switch
.end method
