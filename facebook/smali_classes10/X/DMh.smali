.class public final LX/DMh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DLO;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/events/GroupEventsTabFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/events/GroupEventsTabFragment;)V
    .locals 0

    .prologue
    .line 1990115
    iput-object p1, p0, LX/DMh;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1990116
    iget-object v0, p0, LX/DMh;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    iget-object v0, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->b:Landroid/content/res/Resources;

    const v1, 0x7f020970

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1990117
    iget-object v0, p0, LX/DMh;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    iget-object v0, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->b:Landroid/content/res/Resources;

    const v1, 0x7f081b53

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 9

    .prologue
    .line 1990118
    iget-object v0, p0, LX/DMh;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    .line 1990119
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/groups/events/GroupEventsTabFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->GROUP_PERMALINK_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    iget-object v5, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->n:Ljava/lang/String;

    iget-object v6, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->j:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    invoke-virtual {v6}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->j()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->j:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    invoke-virtual {v7}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v7

    iget-object v8, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->j:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    invoke-virtual {v8}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->k()Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    move-result-object v8

    if-nez v8, :cond_0

    const/4 v8, 0x0

    :goto_0
    invoke-static/range {v1 .. v8}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1990120
    iget-object v2, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->c:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0xd7

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    invoke-interface {v2, v1, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1990121
    return-void

    .line 1990122
    :cond_0
    iget-object v8, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->j:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    invoke-virtual {v8}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->k()Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;->a()Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1990123
    iget-object v0, p0, LX/DMh;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    iget-object v0, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->j:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DMh;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    iget-object v0, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->j:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1990124
    const/4 v0, 0x1

    .line 1990125
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
