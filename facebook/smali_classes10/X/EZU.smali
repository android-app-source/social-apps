.class public final LX/EZU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2139504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 2139505
    const/16 v0, -0xc

    if-gt p0, v0, :cond_0

    const/16 v0, -0x41

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    shl-int/lit8 v0, p1, 0x8

    xor-int/2addr v0, p0

    goto :goto_0
.end method

.method public static a(III)I
    .locals 2

    .prologue
    const/16 v1, -0x41

    .line 2139506
    const/16 v0, -0xc

    if-gt p0, v0, :cond_0

    if-gt p1, v1, :cond_0

    if-le p2, v1, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    shl-int/lit8 v0, p1, 0x8

    xor-int/2addr v0, p0

    shl-int/lit8 v1, p2, 0x10

    xor-int/2addr v0, v1

    goto :goto_0
.end method

.method public static b([BII)I
    .locals 8

    .prologue
    .line 2139507
    :goto_0
    if-ge p1, p2, :cond_0

    aget-byte v0, p0, p1

    if-ltz v0, :cond_0

    .line 2139508
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2139509
    :cond_0
    if-lt p1, p2, :cond_1

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_1
    const/16 v7, -0x20

    const/16 v6, -0x60

    const/4 v1, -0x1

    const/16 v5, -0x41

    .line 2139510
    :goto_2
    if-lt p1, p2, :cond_3

    .line 2139511
    const/4 v0, 0x0

    .line 2139512
    :cond_2
    :goto_3
    move v0, v0

    .line 2139513
    goto :goto_1

    .line 2139514
    :cond_3
    add-int/lit8 v2, p1, 0x1

    aget-byte v0, p0, p1

    if-gez v0, :cond_e

    .line 2139515
    if-ge v0, v7, :cond_5

    .line 2139516
    if-ge v2, p2, :cond_2

    .line 2139517
    const/16 v3, -0x3e

    if-lt v0, v3, :cond_4

    add-int/lit8 v0, v2, 0x1

    aget-byte v2, p0, v2

    if-le v2, v5, :cond_d

    :cond_4
    move v0, v1

    .line 2139518
    goto :goto_3

    .line 2139519
    :cond_5
    const/16 v3, -0x10

    if-ge v0, v3, :cond_a

    .line 2139520
    add-int/lit8 v3, p2, -0x1

    if-lt v2, v3, :cond_6

    .line 2139521
    invoke-static {p0, v2, p2}, LX/EZU;->d([BII)I

    move-result v0

    goto :goto_3

    .line 2139522
    :cond_6
    add-int/lit8 v3, v2, 0x1

    aget-byte v2, p0, v2

    if-gt v2, v5, :cond_9

    if-ne v0, v7, :cond_7

    if-lt v2, v6, :cond_9

    :cond_7
    const/16 v4, -0x13

    if-ne v0, v4, :cond_8

    if-ge v2, v6, :cond_9

    :cond_8
    add-int/lit8 v0, v3, 0x1

    aget-byte v2, p0, v3

    if-le v2, v5, :cond_d

    :cond_9
    move v0, v1

    .line 2139523
    goto :goto_3

    .line 2139524
    :cond_a
    add-int/lit8 v3, p2, -0x2

    if-lt v2, v3, :cond_b

    .line 2139525
    invoke-static {p0, v2, p2}, LX/EZU;->d([BII)I

    move-result v0

    goto :goto_3

    .line 2139526
    :cond_b
    add-int/lit8 v3, v2, 0x1

    aget-byte v2, p0, v2

    if-gt v2, v5, :cond_c

    shl-int/lit8 v0, v0, 0x1c

    add-int/lit8 v2, v2, 0x70

    add-int/2addr v0, v2

    shr-int/lit8 v0, v0, 0x1e

    if-nez v0, :cond_c

    add-int/lit8 v2, v3, 0x1

    aget-byte v0, p0, v3

    if-gt v0, v5, :cond_c

    add-int/lit8 v0, v2, 0x1

    aget-byte v2, p0, v2

    if-le v2, v5, :cond_d

    :cond_c
    move v0, v1

    .line 2139527
    goto :goto_3

    :cond_d
    move p1, v0

    .line 2139528
    goto :goto_2

    :cond_e
    move p1, v2

    goto :goto_2
.end method

.method public static d([BII)I
    .locals 3

    .prologue
    .line 2139529
    add-int/lit8 v0, p1, -0x1

    aget-byte v0, p0, v0

    .line 2139530
    sub-int v1, p2, p1

    packed-switch v1, :pswitch_data_0

    .line 2139531
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2139532
    :pswitch_0
    const/16 v1, -0xc

    if-le v0, v1, :cond_0

    const/4 v0, -0x1

    :cond_0
    move v0, v0

    .line 2139533
    :goto_0
    return v0

    .line 2139534
    :pswitch_1
    aget-byte v1, p0, p1

    invoke-static {v0, v1}, LX/EZU;->a(II)I

    move-result v0

    goto :goto_0

    .line 2139535
    :pswitch_2
    aget-byte v1, p0, p1

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    invoke-static {v0, v1, v2}, LX/EZU;->a(III)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
