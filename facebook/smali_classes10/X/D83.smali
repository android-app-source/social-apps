.class public final LX/D83;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/D85;

.field private final b:Landroid/graphics/Rect;

.field private c:I

.field public d:F

.field private e:I

.field private f:Z

.field private g:Landroid/view/View;

.field public h:Landroid/content/Context;

.field public i:LX/D7g;

.field public j:I


# direct methods
.method public constructor <init>(LX/D85;Landroid/content/Context;LX/D7g;I)V
    .locals 2

    .prologue
    .line 1967777
    iput-object p1, p0, LX/D83;->a:LX/D85;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1967778
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/D83;->b:Landroid/graphics/Rect;

    .line 1967779
    iput-object p2, p0, LX/D83;->h:Landroid/content/Context;

    .line 1967780
    iput-object p3, p0, LX/D83;->i:LX/D7g;

    .line 1967781
    iput p4, p0, LX/D83;->j:I

    .line 1967782
    iget-object v0, p0, LX/D83;->h:Landroid/content/Context;

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1967783
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1967784
    :cond_0
    :goto_0
    return-void

    .line 1967785
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D83;->g:Landroid/view/View;

    .line 1967786
    iget-object v0, p0, LX/D83;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1967787
    iget-object v0, p0, LX/D83;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/D83;->e:I

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1967788
    iget-boolean v0, p0, LX/D83;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D83;->h:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 1967789
    iget-object v0, p0, LX/D83;->h:Landroid/content/Context;

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1967790
    if-eqz v0, :cond_0

    .line 1967791
    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1967792
    const/4 v0, 0x1

    .line 1967793
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onGlobalLayout()V
    .locals 5

    .prologue
    .line 1967794
    iget-object v0, p0, LX/D83;->g:Landroid/view/View;

    iget-object v1, p0, LX/D83;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1967795
    iget-object v0, p0, LX/D83;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1967796
    iget v1, p0, LX/D83;->c:I

    if-eqz v1, :cond_1

    .line 1967797
    iget v1, p0, LX/D83;->c:I

    iget v2, p0, LX/D83;->e:I

    add-int/2addr v2, v0

    if-le v1, v2, :cond_2

    .line 1967798
    iget-object v1, p0, LX/D83;->a:LX/D85;

    invoke-virtual {v1}, LX/D85;->e()LX/D7g;

    move-result-object v1

    invoke-interface {v1}, LX/D7g;->b()F

    move-result v1

    iput v1, p0, LX/D83;->d:F

    .line 1967799
    iget-object v1, p0, LX/D83;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    .line 1967800
    iget-object v2, p0, LX/D83;->h:Landroid/content/Context;

    const-class v3, Landroid/app/Activity;

    invoke-static {v2, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    .line 1967801
    if-nez v2, :cond_4

    .line 1967802
    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/D83;->f:Z

    .line 1967803
    :cond_1
    :goto_1
    iput v0, p0, LX/D83;->c:I

    .line 1967804
    return-void

    .line 1967805
    :cond_2
    iget v1, p0, LX/D83;->c:I

    iget v2, p0, LX/D83;->e:I

    add-int/2addr v1, v2

    if-ge v1, v0, :cond_1

    .line 1967806
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/D83;->f:Z

    .line 1967807
    iget-object v1, p0, LX/D83;->a:LX/D85;

    iget-object v1, v1, LX/D85;->e:LX/D7g;

    invoke-interface {v1}, LX/D7g;->b()F

    move-result v1

    .line 1967808
    iget v2, p0, LX/D83;->d:F

    iget v3, p0, LX/D83;->j:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_5

    iget v2, p0, LX/D83;->j:I

    int-to-float v2, v2

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_5

    .line 1967809
    iget-object v1, p0, LX/D83;->i:LX/D7g;

    iget v2, p0, LX/D83;->j:I

    int-to-float v2, v2

    invoke-interface {v1, v2}, LX/D7g;->a(F)V

    .line 1967810
    :cond_3
    :goto_2
    goto :goto_1

    .line 1967811
    :cond_4
    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    .line 1967812
    if-eqz v2, :cond_0

    .line 1967813
    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 1967814
    invoke-virtual {v2, v3}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1967815
    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    add-int/2addr v2, v3

    .line 1967816
    sub-int/2addr v2, v1

    .line 1967817
    if-ltz v2, :cond_0

    .line 1967818
    iget-object v3, p0, LX/D83;->i:LX/D7g;

    invoke-interface {v3}, LX/D7g;->b()F

    move-result v3

    float-to-int v3, v3

    sub-int v2, v3, v2

    .line 1967819
    iget-object v3, p0, LX/D83;->i:LX/D7g;

    int-to-float v2, v2

    invoke-interface {v3, v2}, LX/D7g;->a(F)V

    .line 1967820
    iget-object v2, p0, LX/D83;->a:LX/D85;

    invoke-static {v2}, LX/D85;->h(LX/D85;)V

    goto :goto_0

    .line 1967821
    :cond_5
    iget v2, p0, LX/D83;->d:F

    iget v3, p0, LX/D83;->j:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    iget v2, p0, LX/D83;->j:I

    int-to-float v2, v2

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_3

    .line 1967822
    iget-object v2, p0, LX/D83;->i:LX/D7g;

    iget v3, p0, LX/D83;->j:I

    int-to-float v3, v3

    invoke-interface {v2, v3}, LX/D7g;->a(F)V

    .line 1967823
    iget-object v2, p0, LX/D83;->i:LX/D7g;

    iget v3, p0, LX/D83;->j:I

    int-to-float v3, v3

    sub-float v1, v3, v1

    float-to-int v1, v1

    invoke-interface {v2, v1}, LX/D7g;->b(I)V

    .line 1967824
    iget-object v1, p0, LX/D83;->a:LX/D85;

    invoke-static {v1}, LX/D85;->h(LX/D85;)V

    goto :goto_2
.end method
