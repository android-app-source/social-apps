.class public final LX/DHI;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/DHK;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/DHJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DHK",
            "<TE;>.SingleCreatorSetTitleAndSubtitleComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/DHK;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/DHK;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1981696
    iput-object p1, p0, LX/DHI;->b:LX/DHK;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1981697
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/DHI;->c:[Ljava/lang/String;

    .line 1981698
    iput v3, p0, LX/DHI;->d:I

    .line 1981699
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/DHI;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/DHI;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/DHI;LX/1De;IILX/DHJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/DHK",
            "<TE;>.SingleCreatorSetTitleAndSubtitleComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1981692
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1981693
    iput-object p4, p0, LX/DHI;->a:LX/DHJ;

    .line 1981694
    iget-object v0, p0, LX/DHI;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1981695
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1981688
    invoke-super {p0}, LX/1X5;->a()V

    .line 1981689
    const/4 v0, 0x0

    iput-object v0, p0, LX/DHI;->a:LX/DHJ;

    .line 1981690
    iget-object v0, p0, LX/DHI;->b:LX/DHK;

    iget-object v0, v0, LX/DHK;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1981691
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/DHK;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1981678
    iget-object v1, p0, LX/DHI;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/DHI;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/DHI;->d:I

    if-ge v1, v2, :cond_2

    .line 1981679
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1981680
    :goto_0
    iget v2, p0, LX/DHI;->d:I

    if-ge v0, v2, :cond_1

    .line 1981681
    iget-object v2, p0, LX/DHI;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1981682
    iget-object v2, p0, LX/DHI;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1981683
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1981684
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1981685
    :cond_2
    iget-object v0, p0, LX/DHI;->a:LX/DHJ;

    .line 1981686
    invoke-virtual {p0}, LX/DHI;->a()V

    .line 1981687
    return-object v0
.end method
