.class public LX/CsX;
.super LX/CsW;
.source ""

# interfaces
.implements Landroid/webkit/WebView$PictureListener;


# static fields
.field public static final g:LX/0wT;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final h:LX/CsU;

.field public i:J

.field public j:F

.field private k:F

.field public l:I

.field public m:I

.field public n:I

.field public o:LX/0wd;

.field public p:I

.field private q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1942949
    const-wide v0, 0x4071800000000000L    # 280.0

    const-wide/high16 v2, 0x4043000000000000L    # 38.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/CsX;->g:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    .line 1942934
    invoke-direct {p0, p1}, LX/CsW;-><init>(Landroid/content/Context;)V

    .line 1942935
    new-instance v0, LX/CsU;

    invoke-direct {v0, p0}, LX/CsU;-><init>(LX/CsX;)V

    iput-object v0, p0, LX/CsX;->h:LX/CsU;

    .line 1942936
    const/4 v6, 0x0

    .line 1942937
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    move-object v4, p0

    check-cast v4, LX/CsX;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v2}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v8

    check-cast v8, LX/Ckw;

    invoke-static {v2}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v9

    check-cast v9, LX/0wW;

    invoke-static {v2}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    iput-object v5, v4, LX/CsX;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v7, v4, LX/CsX;->b:LX/0So;

    iput-object v8, v4, LX/CsX;->c:LX/Ckw;

    iput-object v9, v4, LX/CsX;->d:LX/0wW;

    iput-object v2, v4, LX/CsX;->e:LX/0Uh;

    .line 1942938
    iget-object v2, p0, LX/CsX;->d:LX/0wW;

    invoke-virtual {v2}, LX/0wW;->a()LX/0wd;

    move-result-object v2

    sget-object v3, LX/CsX;->g:LX/0wT;

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v2

    const-wide v4, 0x4072c00000000000L    # 300.0

    invoke-virtual {v2, v4, v5}, LX/0wd;->c(D)LX/0wd;

    move-result-object v2

    const/4 v3, 0x1

    .line 1942939
    iput-boolean v3, v2, LX/0wd;->c:Z

    .line 1942940
    move-object v2, v2

    .line 1942941
    invoke-virtual {v2}, LX/0wd;->j()LX/0wd;

    move-result-object v2

    iput-object v2, p0, LX/CsX;->o:LX/0wd;

    .line 1942942
    iget-object v2, p0, LX/CsX;->o:LX/0wd;

    iget-object v3, p0, LX/CsX;->h:LX/CsU;

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1942943
    new-instance v2, LX/CsT;

    invoke-direct {v2, p0}, LX/CsT;-><init>(LX/CsX;)V

    invoke-virtual {p0, v2}, LX/CsX;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1942944
    invoke-super {p0, p0}, LX/CsW;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    .line 1942945
    iget-object v2, p0, LX/CsX;->e:LX/0Uh;

    const/16 v3, 0xc5

    invoke-virtual {v2, v3, v6}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 1942946
    if-eqz v2, :cond_0

    .line 1942947
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/CsX;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1942948
    :cond_0
    return-void
.end method


# virtual methods
.method public getFallbackAspectRatio()F
    .locals 1

    .prologue
    .line 1942933
    iget v0, p0, LX/CsX;->j:F

    return v0
.end method

.method public getWebViewHorizontalScrollRange()I
    .locals 1

    .prologue
    .line 1942932
    invoke-virtual {p0}, LX/CsX;->computeHorizontalScrollRange()I

    move-result v0

    return v0
.end method

.method public getWebViewVerticalScrollRange()I
    .locals 1

    .prologue
    .line 1942931
    invoke-virtual {p0}, LX/CsX;->computeVerticalScrollRange()I

    move-result v0

    return v0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1942891
    invoke-super {p0, p1, p2}, LX/CsW;->onMeasure(II)V

    .line 1942892
    iget v0, p0, LX/CsX;->k:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    iget v0, p0, LX/CsX;->l:I

    if-nez v0, :cond_0

    iget v0, p0, LX/CsX;->m:I

    if-nez v0, :cond_0

    .line 1942893
    invoke-virtual {p0}, LX/CsX;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/CsX;->k:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/CsX;->n:I

    .line 1942894
    iget v0, p0, LX/CsX;->n:I

    int-to-float v0, v0

    const v1, 0x3f666666    # 0.9f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/CsX;->l:I

    .line 1942895
    iget v0, p0, LX/CsX;->n:I

    int-to-float v0, v0

    const v1, 0x3f8ccccd    # 1.1f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/CsX;->m:I

    .line 1942896
    :cond_0
    invoke-virtual {p0}, LX/CsX;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1942897
    const/4 v1, -0x2

    if-eq v0, v1, :cond_2

    if-lez v0, :cond_2

    .line 1942898
    :cond_1
    :goto_0
    return-void

    .line 1942899
    :cond_2
    iget v0, p0, LX/CsX;->p:I

    if-lez v0, :cond_3

    .line 1942900
    invoke-virtual {p0}, LX/CsX;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, LX/CsX;->p:I

    invoke-virtual {p0, v0, v1}, LX/CsX;->setMeasuredDimension(II)V

    goto :goto_0

    .line 1942901
    :cond_3
    iget v0, p0, LX/CsX;->n:I

    if-lez v0, :cond_4

    .line 1942902
    invoke-virtual {p0}, LX/CsX;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, LX/CsX;->n:I

    invoke-virtual {p0, v0, v1}, LX/CsX;->setMeasuredDimension(II)V

    goto :goto_0

    .line 1942903
    :cond_4
    iget v0, p0, LX/CsX;->j:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    .line 1942904
    invoke-virtual {p0}, LX/CsX;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/CsX;->j:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 1942905
    invoke-virtual {p0}, LX/CsX;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, LX/CsX;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public final onNewPicture(Landroid/webkit/WebView;Landroid/graphics/Picture;)V
    .locals 7

    .prologue
    .line 1942922
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1942923
    iget-object v2, p0, LX/CsX;->h:LX/CsU;

    .line 1942924
    iget v3, v2, LX/CsU;->b:I

    if-eq v0, v3, :cond_0

    .line 1942925
    iput v0, v2, LX/CsU;->b:I

    .line 1942926
    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 1942927
    iget-object v4, v2, LX/CsU;->a:LX/CsX;

    invoke-virtual {v4, v3}, LX/CsX;->getLocationOnScreen([I)V

    .line 1942928
    iget-object v3, v2, LX/CsU;->a:LX/CsX;

    iget-object v3, v3, LX/CsX;->o:LX/0wd;

    iget-object v4, v2, LX/CsU;->a:LX/CsX;

    invoke-virtual {v4}, LX/CsX;->getMeasuredHeight()I

    move-result v4

    int-to-double v5, v4

    invoke-virtual {v3, v5, v6}, LX/0wd;->a(D)LX/0wd;

    .line 1942929
    iget-object v3, v2, LX/CsU;->a:LX/CsX;

    iget-object v3, v3, LX/CsX;->o:LX/0wd;

    iget v4, v2, LX/CsU;->b:I

    int-to-double v5, v4

    invoke-virtual {v3, v5, v6}, LX/0wd;->b(D)LX/0wd;

    .line 1942930
    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x2

    const v0, -0x6f089fb2

    invoke-static {v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1942919
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 1942920
    iget-object v1, p0, LX/CsX;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/CsX;->i:J

    .line 1942921
    :cond_0
    invoke-super {p0, p1}, LX/CsW;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x556e6e70

    invoke-static {v4, v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setFallbackAspectRatio(F)V
    .locals 1

    .prologue
    .line 1942915
    iput p1, p0, LX/CsX;->j:F

    .line 1942916
    iget-object v0, p0, LX/CsX;->h:LX/CsU;

    invoke-virtual {v0}, LX/CsU;->a()V

    .line 1942917
    invoke-virtual {p0}, LX/CsX;->invalidate()V

    .line 1942918
    return-void
.end method

.method public setHeightRangeAspectRatio(F)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1942910
    iput p1, p0, LX/CsX;->k:F

    .line 1942911
    iput v0, p0, LX/CsX;->l:I

    .line 1942912
    iput v0, p0, LX/CsX;->m:I

    .line 1942913
    invoke-virtual {p0}, LX/CsX;->requestLayout()V

    .line 1942914
    return-void
.end method

.method public setPictureListener(Landroid/webkit/WebView$PictureListener;)V
    .locals 2

    .prologue
    .line 1942909
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "setPictureListener not supported by IAWebView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final stopLoading()V
    .locals 1

    .prologue
    .line 1942906
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CsX;->q:Z

    .line 1942907
    invoke-super {p0}, LX/CsW;->stopLoading()V

    .line 1942908
    return-void
.end method
