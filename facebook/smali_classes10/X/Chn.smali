.class public final LX/Chn;
.super LX/Chm;
.source ""


# instance fields
.field public final synthetic a:LX/Chr;


# direct methods
.method public constructor <init>(LX/Chr;)V
    .locals 0

    .prologue
    .line 1928554
    iput-object p1, p0, LX/Chn;->a:LX/Chr;

    invoke-direct {p0}, LX/Chm;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 1928555
    check-cast p1, LX/CiX;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1928556
    iget-object v0, p1, LX/CiX;->b:LX/Cqw;

    move-object v0, v0

    .line 1928557
    iget-object v1, v0, LX/Cqw;->e:LX/Cqu;

    move-object v1, v1

    .line 1928558
    iget-object v2, v0, LX/Cqw;->f:LX/Cqt;

    move-object v2, v2

    .line 1928559
    iget-object v3, p0, LX/Chn;->a:LX/Chr;

    iget-boolean v3, v3, LX/Chr;->e:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/Chn;->a:LX/Chr;

    iget-object v3, v3, LX/Chr;->f:LX/Cqw;

    .line 1928560
    iget-object v6, v3, LX/Cqw;->e:LX/Cqu;

    move-object v3, v6

    .line 1928561
    if-ne v1, v3, :cond_0

    iget-object v1, p0, LX/Chn;->a:LX/Chr;

    iget-object v1, v1, LX/Chr;->f:LX/Cqw;

    .line 1928562
    iget-object v3, v1, LX/Cqw;->f:LX/Cqt;

    move-object v1, v3

    .line 1928563
    if-eq v2, v1, :cond_2

    .line 1928564
    :cond_0
    iget-object v1, p0, LX/Chn;->a:LX/Chr;

    .line 1928565
    iput-boolean v5, v1, LX/Chr;->e:Z

    .line 1928566
    iget-object v1, p0, LX/Chn;->a:LX/Chr;

    .line 1928567
    iput-object v0, v1, LX/Chr;->f:LX/Cqw;

    .line 1928568
    sget-object v2, LX/Cqw;->b:LX/Cqw;

    invoke-virtual {v0, v2}, LX/Cqw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, LX/Cqw;->c:LX/Cqw;

    invoke-virtual {v0, v2}, LX/Cqw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, LX/Cqw;->d:LX/Cqw;

    invoke-virtual {v0, v2}, LX/Cqw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_1
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1928569
    move v0, v2

    .line 1928570
    if-eqz v0, :cond_4

    .line 1928571
    iget-object v0, p0, LX/Chn;->a:LX/Chr;

    .line 1928572
    iput-boolean v5, v0, LX/Chr;->g:Z

    .line 1928573
    iget-object v0, p0, LX/Chn;->a:LX/Chr;

    .line 1928574
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Chr;->g:Z

    .line 1928575
    iget-object v1, v0, LX/Chr;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Chq;

    .line 1928576
    invoke-interface {v1, v0}, LX/Chq;->a(LX/Chr;)V

    goto :goto_1

    .line 1928577
    :cond_2
    :goto_2
    iget-boolean v0, p1, LX/CiX;->d:Z

    move v0, v0

    .line 1928578
    if-eqz v0, :cond_3

    .line 1928579
    iget-object v0, p0, LX/Chn;->a:LX/Chr;

    .line 1928580
    iput-boolean v4, v0, LX/Chr;->e:Z

    .line 1928581
    :cond_3
    return-void

    .line 1928582
    :cond_4
    iget-object v0, p0, LX/Chn;->a:LX/Chr;

    .line 1928583
    iput-boolean v4, v0, LX/Chr;->g:Z

    .line 1928584
    iget-object v0, p0, LX/Chn;->a:LX/Chr;

    const/4 v1, 0x0

    .line 1928585
    iput-boolean v1, v0, LX/Chr;->g:Z

    .line 1928586
    iput-boolean v1, v0, LX/Chr;->h:Z

    .line 1928587
    iget-object v1, v0, LX/Chr;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Chq;

    .line 1928588
    invoke-interface {v1, v0}, LX/Chq;->b(LX/Chr;)V

    goto :goto_3

    .line 1928589
    :cond_5
    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    goto :goto_0
.end method
