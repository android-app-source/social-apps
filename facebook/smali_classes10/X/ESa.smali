.class public final LX/ESa;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V
    .locals 0

    .prologue
    .line 2123162
    iput-object p1, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x40e6d559

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2123163
    const-string v1, "vault.table_refreshed_key"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2123164
    iget-object v1, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    invoke-virtual {v1}, LX/ESY;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2123165
    iget-object v1, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/ESY;->cancel(Z)Z

    .line 2123166
    :cond_0
    iget-object v1, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    new-instance v2, LX/ESY;

    iget-object v3, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {v2, v3, v4}, LX/ESY;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;Z)V

    .line 2123167
    iput-object v2, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    .line 2123168
    iget-object v1, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e:LX/0Sh;

    iget-object v2, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v2, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 2123169
    :cond_1
    :goto_0
    const v1, -0x3480f1ad    # -1.6715347E7f

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    return-void

    .line 2123170
    :cond_2
    const-string v1, "vault.sync_start"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "vault.sync_end"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2123171
    :cond_3
    iget-object v1, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v1}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->a()V

    goto :goto_0

    .line 2123172
    :cond_4
    const-string v1, "vault.row_upload_key"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2123173
    iget-object v1, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    const/4 p1, 0x0

    .line 2123174
    const-string v2, "vault.row_upload_key"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2123175
    const-string v3, "vault.upload_percentage"

    invoke-virtual {p2, v3, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 2123176
    if-eqz v2, :cond_5

    .line 2123177
    iget-object v4, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->j:LX/2TN;

    invoke-virtual {v4, v2}, LX/2TN;->d(Ljava/lang/String;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    .line 2123178
    if-nez v4, :cond_7

    .line 2123179
    :cond_5
    :goto_1
    goto :goto_0

    .line 2123180
    :cond_6
    const-string v1, "vault.status_change_key"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2123181
    iget-object v1, p0, LX/ESa;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->A:Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;

    invoke-virtual {v1}, Lcom/facebook/vault/ui/VaultSyncScreenErrorBar;->a()V

    goto :goto_0

    .line 2123182
    :cond_7
    const-string v5, "vault.upload_completed"

    invoke-virtual {p2, v5, p1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 2123183
    if-eqz v5, :cond_9

    .line 2123184
    iget-object v3, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "file://"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v3, v4}, LX/ES9;->b(Landroid/net/Uri;)V

    .line 2123185
    :goto_2
    iget-object v3, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->r:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2123186
    iget-object v2, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    if-eqz v2, :cond_8

    iget-object v2, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    invoke-virtual {v2}, LX/ESY;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2123187
    iget-object v2, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/ESY;->cancel(Z)Z

    .line 2123188
    :cond_8
    new-instance v2, LX/ESY;

    invoke-direct {v2, v1}, LX/ESY;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V

    iput-object v2, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    .line 2123189
    iget-object v2, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e:LX/0Sh;

    iget-object v3, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    new-array v4, p1, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v4}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    goto :goto_1

    .line 2123190
    :cond_9
    iget-object v5, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string p0, "file://"

    invoke-direct {v6, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v5, v4, v3}, LX/ES9;->a(Landroid/net/Uri;I)V

    goto :goto_2
.end method
