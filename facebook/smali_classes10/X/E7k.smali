.class public LX/E7k;
.super LX/Cfm;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/E6l;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/E6l;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/rows/attachments/ReactionEventBlocksAttachmentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081944
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->EVENT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    invoke-direct {p0, v0}, LX/Cfm;-><init>(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V

    .line 2081945
    iput-object p1, p0, LX/E7k;->a:LX/0Or;

    .line 2081946
    iput-object p2, p0, LX/E7k;->b:LX/0Ot;

    .line 2081947
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<",
            "Lcom/facebook/reaction/common/ReactionAttachmentNode;",
            "+",
            "LX/1PW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2081948
    iget-object v0, p0, LX/E7k;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    return-object v0
.end method

.method public final c()LX/Cfk;
    .locals 1

    .prologue
    .line 2081949
    iget-object v0, p0, LX/E7k;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cfk;

    return-object v0
.end method
