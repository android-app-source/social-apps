.class public LX/DxJ;
.super LX/Dvb;
.source ""


# instance fields
.field public final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DvH;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/Dcc;

.field private final v:Z

.field private final w:Z

.field public x:Z

.field public y:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/Dce;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/DwK;LX/Dw8;LX/Dcc;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/Dvf;)V
    .locals 10
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p9    # LX/Dcc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/Dvf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dux;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DvH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvd;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/lang/String;",
            "LX/DwK;",
            "LX/Dw8;",
            "LX/Dcc;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "LX/Dvf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2062725
    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p12

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, LX/Dvb;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/Dvf;LX/0Ot;Ljava/lang/String;LX/DwK;LX/Dw8;)V

    .line 2062726
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/DxJ;->x:Z

    .line 2062727
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/DxJ;->y:LX/0am;

    .line 2062728
    iput-object p2, p0, LX/DxJ;->t:LX/0Ot;

    .line 2062729
    move-object/from16 v0, p9

    iput-object v0, p0, LX/DxJ;->u:LX/Dcc;

    .line 2062730
    invoke-virtual/range {p10 .. p10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/DxJ;->v:Z

    .line 2062731
    invoke-virtual/range {p11 .. p11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/DxJ;->w:Z

    .line 2062732
    return-void
.end method


# virtual methods
.method public final a(LX/Dce;)V
    .locals 1

    .prologue
    .line 2062723
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/DxJ;->y:LX/0am;

    .line 2062724
    return-void
.end method

.method public final d()V
    .locals 8

    .prologue
    .line 2062711
    iget-boolean v1, p0, LX/Dvb;->m:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/Dvb;->p:Z

    if-nez v1, :cond_1

    .line 2062712
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DxJ;->x:Z

    .line 2062713
    return-void

    .line 2062714
    :cond_1
    iget-object v1, p0, LX/Dvb;->h:LX/Dv0;

    if-nez v1, :cond_2

    .line 2062715
    iget-object v2, p0, LX/Dvb;->j:Ljava/lang/String;

    iget-object v3, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    const-string v4, "LoadScreenImagesUploads"

    iget-boolean v5, p0, LX/Dvb;->o:Z

    iget-boolean v6, p0, LX/Dvb;->p:Z

    iget-boolean v7, p0, LX/Dvb;->r:Z

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, LX/Dvb;->a(Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Ljava/lang/String;ZZZ)V

    .line 2062716
    :cond_2
    iget-boolean v1, p0, LX/Dvb;->o:Z

    if-eqz v1, :cond_3

    .line 2062717
    iget-object v1, p0, LX/Dvb;->q:LX/DwK;

    const-string v2, "LoadImageURLs"

    invoke-virtual {v1, v2}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2062718
    :cond_3
    iget-object v1, p0, LX/Dvb;->h:LX/Dv0;

    .line 2062719
    iget-object v2, v1, LX/Dv0;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2062720
    iget-boolean v2, p0, LX/Dvb;->m:Z

    if-nez v2, :cond_4

    .line 2062721
    const/4 v1, 0x0

    move-object v2, v1

    .line 2062722
    :goto_1
    iget-object v1, p0, LX/Dvb;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/Dvb;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/DxH;

    invoke-direct {v4, p0, v2}, LX/DxH;-><init>(LX/DxJ;Ljava/lang/String;)V

    new-instance v2, LX/DxI;

    invoke-direct {v2, p0}, LX/DxI;-><init>(LX/DxJ;)V

    invoke-virtual {v1, v3, v4, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0

    :cond_4
    move-object v2, v1

    goto :goto_1
.end method

.method public final e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2062710
    const-string v0, "fetchUploadedMediaSet_%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/DvW;
    .locals 1

    .prologue
    .line 2062709
    sget-object v0, LX/DvW;->UPLOADED_MEDIA_SET:LX/DvW;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2062733
    iget-boolean v0, p0, LX/DxJ;->v:Z

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 2062708
    iget-boolean v0, p0, LX/DxJ;->w:Z

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 2062707
    iget-boolean v0, p0, LX/Dvb;->m:Z

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 2062703
    invoke-super {p0}, LX/Dvb;->notifyDataSetChanged()V

    .line 2062704
    invoke-virtual {p0}, LX/DxJ;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DxJ;->y:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2062705
    iget-object v0, p0, LX/DxJ;->y:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dce;

    invoke-interface {v0}, LX/Dce;->a()V

    .line 2062706
    :cond_0
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 2062699
    invoke-virtual {p0}, LX/Dvb;->j()V

    .line 2062700
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DxJ;->m:Z

    .line 2062701
    invoke-virtual {p0}, LX/Dvb;->d()V

    .line 2062702
    return-void
.end method
