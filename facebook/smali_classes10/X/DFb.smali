.class public final LX/DFb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DFc;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/DFc;


# direct methods
.method public constructor <init>(LX/DFc;)V
    .locals 1

    .prologue
    .line 1978509
    iput-object p1, p0, LX/DFb;->d:LX/DFc;

    .line 1978510
    move-object v0, p1

    .line 1978511
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1978512
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1978513
    const-string v0, "PersonYouMayKnowProfilePictureComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1978514
    if-ne p0, p1, :cond_1

    .line 1978515
    :cond_0
    :goto_0
    return v0

    .line 1978516
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1978517
    goto :goto_0

    .line 1978518
    :cond_3
    check-cast p1, LX/DFb;

    .line 1978519
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1978520
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1978521
    if-eq v2, v3, :cond_0

    .line 1978522
    iget-object v2, p0, LX/DFb;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DFb;->a:Ljava/lang/String;

    iget-object v3, p1, LX/DFb;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1978523
    goto :goto_0

    .line 1978524
    :cond_5
    iget-object v2, p1, LX/DFb;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1978525
    :cond_6
    iget-object v2, p0, LX/DFb;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DFb;->b:Ljava/lang/String;

    iget-object v3, p1, LX/DFb;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1978526
    goto :goto_0

    .line 1978527
    :cond_8
    iget-object v2, p1, LX/DFb;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1978528
    :cond_9
    iget-object v2, p0, LX/DFb;->c:LX/1Pp;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/DFb;->c:LX/1Pp;

    iget-object v3, p1, LX/DFb;->c:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1978529
    goto :goto_0

    .line 1978530
    :cond_a
    iget-object v2, p1, LX/DFb;->c:LX/1Pp;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
