.class public LX/Cuh;
.super LX/2oy;
.source ""


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Z

.field public c:LX/Cub;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1947093
    invoke-direct {p0, p1}, LX/2oy;-><init>(Landroid/content/Context;)V

    .line 1947094
    const-class v0, LX/Cuh;

    invoke-static {v0, p0}, LX/Cuh;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1947095
    iget-object v0, p0, LX/Cuh;->a:LX/0Uh;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Cuh;->b:Z

    .line 1947096
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Cug;

    invoke-direct {v1, p0}, LX/Cug;-><init>(LX/Cuh;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1947097
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cuh;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object p0, p1, LX/Cuh;->a:LX/0Uh;

    return-void
.end method


# virtual methods
.method public final a(LX/Cuj;)Z
    .locals 1

    .prologue
    .line 1947090
    iget-object v0, p0, LX/Cuh;->c:LX/Cub;

    if-eqz v0, :cond_0

    .line 1947091
    iget-object v0, p0, LX/Cuh;->c:LX/Cub;

    invoke-virtual {v0, p1}, LX/Cub;->a(LX/Cuj;)Z

    move-result v0

    .line 1947092
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
