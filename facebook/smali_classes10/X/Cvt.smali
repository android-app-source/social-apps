.class public LX/Cvt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cvs;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Cvt;


# instance fields
.field private final a:LX/11i;

.field private final b:LX/0id;

.field private final c:LX/Cvy;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Qe;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2Sg;


# direct methods
.method public constructor <init>(LX/11i;LX/0id;LX/Cvy;LX/0Or;LX/2Sg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11i;",
            "LX/0id;",
            "LX/Cvy;",
            "LX/0Or",
            "<",
            "LX/3Qe;",
            ">;",
            "LX/2Sg;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1949746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949747
    iput-object p1, p0, LX/Cvt;->a:LX/11i;

    .line 1949748
    iput-object p2, p0, LX/Cvt;->b:LX/0id;

    .line 1949749
    iput-object p3, p0, LX/Cvt;->c:LX/Cvy;

    .line 1949750
    iput-object p4, p0, LX/Cvt;->d:LX/0Or;

    .line 1949751
    iput-object p5, p0, LX/Cvt;->e:LX/2Sg;

    .line 1949752
    return-void
.end method

.method public static a(LX/0QB;)LX/Cvt;
    .locals 9

    .prologue
    .line 1949733
    sget-object v0, LX/Cvt;->f:LX/Cvt;

    if-nez v0, :cond_1

    .line 1949734
    const-class v1, LX/Cvt;

    monitor-enter v1

    .line 1949735
    :try_start_0
    sget-object v0, LX/Cvt;->f:LX/Cvt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1949736
    if-eqz v2, :cond_0

    .line 1949737
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1949738
    new-instance v3, LX/Cvt;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v4

    check-cast v4, LX/11i;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v5

    check-cast v5, LX/0id;

    invoke-static {v0}, LX/Cvy;->a(LX/0QB;)LX/Cvy;

    move-result-object v6

    check-cast v6, LX/Cvy;

    const/16 v7, 0x1138

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/2Sg;->a(LX/0QB;)LX/2Sg;

    move-result-object v8

    check-cast v8, LX/2Sg;

    invoke-direct/range {v3 .. v8}, LX/Cvt;-><init>(LX/11i;LX/0id;LX/Cvy;LX/0Or;LX/2Sg;)V

    .line 1949739
    move-object v0, v3

    .line 1949740
    sput-object v0, LX/Cvt;->f:LX/Cvt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1949741
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1949742
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1949743
    :cond_1
    sget-object v0, LX/Cvt;->f:LX/Cvt;

    return-object v0

    .line 1949744
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1949745
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1949722
    iget-object v0, p0, LX/Cvt;->b:LX/0id;

    const-string v1, "on_search_icon_click"

    invoke-virtual {v0, v1}, LX/0id;->a(Ljava/lang/String;)V

    .line 1949723
    iget-object v0, p0, LX/Cvt;->c:LX/Cvy;

    .line 1949724
    invoke-static {v0}, LX/Cvy;->h(LX/Cvy;)V

    .line 1949725
    const-string v1, "entered_search_via_button"

    invoke-static {v0, v1}, LX/Cvy;->a(LX/Cvy;Ljava/lang/String;)V

    .line 1949726
    iget-object v0, p0, LX/Cvt;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qe;

    invoke-virtual {v0}, LX/3Qe;->a()V

    .line 1949727
    iget-object v0, p0, LX/Cvt;->e:LX/2Sg;

    .line 1949728
    sget-object v1, LX/2Si;->COLD_START:LX/2Si;

    invoke-static {v0, v1}, LX/2Sg;->b(LX/2Sg;LX/2Si;)LX/11o;

    move-result-object v1

    .line 1949729
    sget-object v2, LX/Cvr;->SEARCH_ICON_CLICKED:LX/Cvr;

    invoke-static {v1, v2}, LX/2Sg;->a(LX/11o;LX/Cvr;)V

    .line 1949730
    const-string v2, "pre_fetch"

    invoke-interface {v1, v2}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1949731
    const-string v2, "pre_fetch"

    const p0, 0x2f0e52e0

    invoke-static {v1, v2, p0}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949732
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1949717
    iget-object v0, p0, LX/Cvt;->e:LX/2Sg;

    .line 1949718
    invoke-static {v0}, LX/2Sg;->i(LX/2Sg;)LX/11o;

    move-result-object v1

    .line 1949719
    if-eqz v1, :cond_0

    .line 1949720
    const-string v2, "activity_transition"

    const p0, -0x6944eed2

    invoke-static {v1, v2, p0}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949721
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1949711
    iget-object v0, p0, LX/Cvt;->e:LX/2Sg;

    .line 1949712
    invoke-static {v0}, LX/2Sg;->i(LX/2Sg;)LX/11o;

    move-result-object v1

    .line 1949713
    if-eqz v1, :cond_0

    .line 1949714
    const-string v2, "activity_transition"

    const p0, -0x6b5f2757

    invoke-static {v1, v2, p0}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949715
    const-string v2, "fragment_creation"

    const p0, -0x1c56d527

    invoke-static {v1, v2, p0}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949716
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1949705
    iget-object v0, p0, LX/Cvt;->e:LX/2Sg;

    .line 1949706
    invoke-static {v0}, LX/2Sg;->i(LX/2Sg;)LX/11o;

    move-result-object v1

    .line 1949707
    if-eqz v1, :cond_0

    .line 1949708
    const-string v2, "fragment_creation"

    const p0, 0x2d6b18c5

    invoke-static {v1, v2, p0}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949709
    const-string v2, "create_view_hierarchy"

    const p0, -0x53e2d0f3

    invoke-static {v1, v2, p0}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949710
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1949699
    iget-object v0, p0, LX/Cvt;->e:LX/2Sg;

    .line 1949700
    invoke-static {v0}, LX/2Sg;->i(LX/2Sg;)LX/11o;

    move-result-object v1

    .line 1949701
    if-eqz v1, :cond_0

    .line 1949702
    const-string v2, "create_view_hierarchy"

    const p0, 0x1ad4a808

    invoke-static {v1, v2, p0}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949703
    const-string v2, "after_view_creation"

    const p0, 0x6a7128ff

    invoke-static {v1, v2, p0}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949704
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 1949687
    iget-object v0, p0, LX/Cvt;->e:LX/2Sg;

    .line 1949688
    invoke-static {v0}, LX/2Sg;->i(LX/2Sg;)LX/11o;

    move-result-object v1

    .line 1949689
    if-eqz v1, :cond_0

    const-string v2, "after_view_creation"

    invoke-interface {v1, v2}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1949690
    const-string v2, "after_view_creation"

    const v3, -0x256f6e40

    invoke-static {v1, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949691
    const-string v2, "on_resume"

    const v3, -0x6ded2526

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1949692
    :cond_0
    iget-object v0, p0, LX/Cvt;->c:LX/Cvy;

    .line 1949693
    iget-object v1, v0, LX/Cvy;->a:LX/11i;

    sget-object v2, LX/Cvx;->b:LX/Cvw;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 1949694
    if-nez v1, :cond_1

    .line 1949695
    invoke-static {v0}, LX/Cvy;->h(LX/Cvy;)V

    .line 1949696
    :cond_1
    const-string v1, "search_fragment_resumed"

    invoke-static {v0, v1}, LX/Cvy;->a(LX/Cvy;Ljava/lang/String;)V

    .line 1949697
    iget-object v0, p0, LX/Cvt;->b:LX/0id;

    const-string v1, "SearchTypeahead"

    invoke-virtual {v0, v1}, LX/0id;->b(Ljava/lang/String;)V

    .line 1949698
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1949684
    iget-object v0, p0, LX/Cvt;->c:LX/Cvy;

    .line 1949685
    const-string p0, "search_fragment_paused"

    invoke-static {v0, p0}, LX/Cvy;->a(LX/Cvy;Ljava/lang/String;)V

    .line 1949686
    return-void
.end method
