.class public final LX/DgP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DgO;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V
    .locals 0

    .prologue
    .line 2029363
    iput-object p1, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 2

    .prologue
    .line 2029357
    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->s:LX/DgO;

    if-eqz v0, :cond_0

    .line 2029358
    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->s:LX/DgO;

    invoke-interface {v0, p1}, LX/DgO;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 2029359
    :cond_0
    iget-object v1, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->s:LX/DgO;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->c(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;Z)V

    .line 2029360
    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2029361
    return-void

    .line 2029362
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/location/sending/NearbyPlace;)V
    .locals 2

    .prologue
    .line 2029370
    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->s:LX/DgO;

    if-eqz v0, :cond_0

    .line 2029371
    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->s:LX/DgO;

    invoke-interface {v0, p1}, LX/DgO;->a(Lcom/facebook/messaging/location/sending/NearbyPlace;)V

    .line 2029372
    :cond_0
    iget-object v1, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->s:LX/DgO;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->c(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;Z)V

    .line 2029373
    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2029374
    return-void

    .line 2029375
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 2

    .prologue
    .line 2029364
    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->s:LX/DgO;

    if-eqz v0, :cond_0

    .line 2029365
    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->s:LX/DgO;

    invoke-interface {v0, p1}, LX/DgO;->b(Lcom/facebook/android/maps/model/LatLng;)V

    .line 2029366
    :cond_0
    iget-object v1, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->s:LX/DgO;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->c(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;Z)V

    .line 2029367
    iget-object v0, p0, LX/DgP;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2029368
    return-void

    .line 2029369
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
