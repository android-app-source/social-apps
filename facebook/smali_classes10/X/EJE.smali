.class public final LX/EJE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EJF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public final synthetic c:LX/EJF;


# direct methods
.method public constructor <init>(LX/EJF;)V
    .locals 1

    .prologue
    .line 2103652
    iput-object p1, p0, LX/EJE;->c:LX/EJF;

    .line 2103653
    move-object v0, p1

    .line 2103654
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2103655
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2103656
    const-string v0, "SearchResultsAwarenessSerpSuccessComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2103657
    if-ne p0, p1, :cond_1

    .line 2103658
    :cond_0
    :goto_0
    return v0

    .line 2103659
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2103660
    goto :goto_0

    .line 2103661
    :cond_3
    check-cast p1, LX/EJE;

    .line 2103662
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2103663
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2103664
    if-eq v2, v3, :cond_0

    .line 2103665
    iget-object v2, p0, LX/EJE;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EJE;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/EJE;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2103666
    goto :goto_0

    .line 2103667
    :cond_5
    iget-object v2, p1, LX/EJE;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 2103668
    :cond_6
    iget-object v2, p0, LX/EJE;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/EJE;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/EJE;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2103669
    goto :goto_0

    .line 2103670
    :cond_7
    iget-object v2, p1, LX/EJE;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
