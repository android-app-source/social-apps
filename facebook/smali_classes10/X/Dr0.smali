.class public LX/Dr0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Dr0;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048825
    iput-object p1, p0, LX/Dr0;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2048826
    return-void
.end method

.method public static a(LX/0QB;)LX/Dr0;
    .locals 4

    .prologue
    .line 2048827
    sget-object v0, LX/Dr0;->b:LX/Dr0;

    if-nez v0, :cond_1

    .line 2048828
    const-class v1, LX/Dr0;

    monitor-enter v1

    .line 2048829
    :try_start_0
    sget-object v0, LX/Dr0;->b:LX/Dr0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2048830
    if-eqz v2, :cond_0

    .line 2048831
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2048832
    new-instance p0, LX/Dr0;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/Dr0;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2048833
    move-object v0, p0

    .line 2048834
    sput-object v0, LX/Dr0;->b:LX/Dr0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2048835
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2048836
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2048837
    :cond_1
    sget-object v0, LX/Dr0;->b:LX/Dr0;

    return-object v0

    .line 2048838
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2048839
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 4

    .prologue
    .line 2048840
    iget-object v0, p0, LX/Dr0;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 2048841
    iget-object v1, p0, LX/Dr0;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0hM;->j:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2048842
    sget-object v1, LX/0hM;->k:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->l:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->m:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->n:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->p:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->o:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->q:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->r:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->s:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->t:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->u:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->v:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->w:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->x:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->y:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->z:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0db;->I:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0db;->J:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 2048843
    :cond_0
    sget-object v1, LX/0hM;->j:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2048844
    return-void
.end method
