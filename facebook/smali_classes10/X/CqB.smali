.class public LX/CqB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/richdocument/view/carousel/PageableFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/carousel/PageableFragment;)V
    .locals 0

    .prologue
    .line 1939464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1939465
    iput-object p1, p0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    .line 1939466
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1939467
    if-ne p0, p1, :cond_1

    .line 1939468
    :cond_0
    :goto_0
    return v0

    .line 1939469
    :cond_1
    instance-of v2, p1, LX/CqB;

    if-nez v2, :cond_2

    move v0, v1

    .line 1939470
    goto :goto_0

    .line 1939471
    :cond_2
    check-cast p1, LX/CqB;

    .line 1939472
    iget-object v2, p0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    iget-object v3, p1, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v2, v3}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1939473
    iget-object v2, p0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p1, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    iget-object v2, p0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p1, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    move v0, v1

    .line 1939474
    goto :goto_0

    .line 1939475
    :cond_5
    iget-object v2, p0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->l()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_6

    .line 1939476
    iget-object v2, p1, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v2}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->l()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1939477
    :cond_6
    iget-object v0, p0, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->l()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LX/CqB;->a:Lcom/facebook/richdocument/view/carousel/PageableFragment;

    invoke-virtual {v1}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
