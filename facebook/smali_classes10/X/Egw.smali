.class public final LX/Egw;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Egy;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

.field public final synthetic b:LX/Egy;


# direct methods
.method public constructor <init>(LX/Egy;)V
    .locals 1

    .prologue
    .line 2157430
    iput-object p1, p0, LX/Egw;->b:LX/Egy;

    .line 2157431
    move-object v0, p1

    .line 2157432
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2157433
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2157434
    const-string v0, "BookmarkItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2157435
    if-ne p0, p1, :cond_1

    .line 2157436
    :cond_0
    :goto_0
    return v0

    .line 2157437
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2157438
    goto :goto_0

    .line 2157439
    :cond_3
    check-cast p1, LX/Egw;

    .line 2157440
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2157441
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2157442
    if-eq v2, v3, :cond_0

    .line 2157443
    iget-object v2, p0, LX/Egw;->a:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Egw;->a:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    iget-object v3, p1, LX/Egw;->a:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2157444
    goto :goto_0

    .line 2157445
    :cond_4
    iget-object v2, p1, LX/Egw;->a:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
