.class public LX/Ecm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DoQ;


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Eap;",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2147967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2147968
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Ecm;->a:Ljava/util/Map;

    .line 2147969
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/Eap;)LX/Ebh;
    .locals 2

    .prologue
    .line 2147974
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/Ecm;->b(LX/Eap;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2147975
    new-instance v1, LX/Ebh;

    iget-object v0, p0, LX/Ecm;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v1, v0}, LX/Ebh;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    .line 2147976
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, LX/Ebh;

    invoke-direct {v0}, LX/Ebh;-><init>()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2147977
    :catch_0
    move-exception v0

    .line 2147978
    :try_start_2
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2147979
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/Eap;LX/Ebh;)V
    .locals 2

    .prologue
    .line 2147971
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Ecm;->a:Ljava/util/Map;

    invoke-virtual {p2}, LX/Ebh;->e()[B

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2147972
    monitor-exit p0

    return-void

    .line 2147973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/Eap;)Z
    .locals 1

    .prologue
    .line 2147970
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Ecm;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
