.class public final LX/ECr;
.super LX/1a1;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

.field public m:Landroid/view/View;

.field private n:Landroid/view/View;

.field public o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field public t:Z

.field public u:LX/EGF;

.field public v:I


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2090432
    iput-object p1, p0, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    .line 2090433
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2090434
    iput-object p2, p0, LX/ECr;->m:Landroid/view/View;

    .line 2090435
    const v0, 0x7f0d2aba

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/ECr;->n:Landroid/view/View;

    .line 2090436
    const v0, 0x7f0d2abc

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/ECr;->p:Landroid/view/View;

    .line 2090437
    const v0, 0x7f0d2abb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/ECr;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2090438
    const v0, 0x7f0d2ab9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/ECr;->q:Landroid/view/View;

    .line 2090439
    const v0, 0x7f0d05b4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/ECr;->r:Landroid/view/View;

    .line 2090440
    const v0, 0x7f0d2abd

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/ECr;->s:Landroid/view/View;

    .line 2090441
    return-void
.end method

.method public static x(LX/ECr;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2090442
    iget-object v0, p0, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget-object v0, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    iget v3, p0, LX/ECr;->v:I

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGF;

    .line 2090443
    iget-object v3, p0, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget-object v3, v3, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->h:LX/ECw;

    invoke-virtual {v3, v0}, LX/ECw;->a(LX/EGF;)LX/ECv;

    move-result-object v3

    .line 2090444
    iget-boolean v0, p0, LX/ECr;->t:Z

    if-eqz v0, :cond_0

    .line 2090445
    iget-object v0, p0, LX/ECr;->n:Landroid/view/View;

    const v4, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 2090446
    iget-object v0, p0, LX/ECr;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2090447
    :goto_0
    iget-object v0, p0, LX/ECr;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2090448
    iget-object v4, p0, LX/ECr;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v0, LX/ECv;->COMPLETED:LX/ECv;

    if-ne v3, v0, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v4, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 2090449
    iget-object v4, p0, LX/ECr;->s:Landroid/view/View;

    sget-object v0, LX/ECv;->IN_PROGRESS:LX/ECv;

    if-ne v3, v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2090450
    iget-object v0, p0, LX/ECr;->r:Landroid/view/View;

    sget-object v4, LX/ECv;->NOT_STARTED:LX/ECv;

    if-ne v3, v4, :cond_4

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2090451
    return-void

    .line 2090452
    :cond_0
    iget-object v4, p0, LX/ECr;->n:Landroid/view/View;

    sget-object v0, LX/ECv;->NOT_STARTED:LX/ECv;

    if-ne v3, v0, :cond_1

    const v0, 0x3e4ccccd    # 0.2f

    :goto_4
    invoke-virtual {v4, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2090453
    iget-object v0, p0, LX/ECr;->q:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2090454
    :cond_1
    const v0, 0x3e99999a    # 0.3f

    goto :goto_4

    .line 2090455
    :cond_2
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2090456
    goto :goto_2

    :cond_4
    move v1, v2

    .line 2090457
    goto :goto_3
.end method
