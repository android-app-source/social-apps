.class public LX/CrO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/CrO;


# instance fields
.field private final a:LX/8bZ;


# direct methods
.method public constructor <init>(LX/8bZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1940863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1940864
    iput-object p1, p0, LX/CrO;->a:LX/8bZ;

    .line 1940865
    return-void
.end method

.method public static a(LX/0QB;)LX/CrO;
    .locals 4

    .prologue
    .line 1940866
    sget-object v0, LX/CrO;->b:LX/CrO;

    if-nez v0, :cond_1

    .line 1940867
    const-class v1, LX/CrO;

    monitor-enter v1

    .line 1940868
    :try_start_0
    sget-object v0, LX/CrO;->b:LX/CrO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1940869
    if-eqz v2, :cond_0

    .line 1940870
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1940871
    new-instance p0, LX/CrO;

    invoke-static {v0}, LX/8bZ;->b(LX/0QB;)LX/8bZ;

    move-result-object v3

    check-cast v3, LX/8bZ;

    invoke-direct {p0, v3}, LX/CrO;-><init>(LX/8bZ;)V

    .line 1940872
    move-object v0, p0

    .line 1940873
    sput-object v0, LX/CrO;->b:LX/CrO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1940874
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1940875
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1940876
    :cond_1
    sget-object v0, LX/CrO;->b:LX/CrO;

    return-object v0

    .line 1940877
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1940878
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public a(LX/CrN;Landroid/content/Context;LX/Ctg;Z)LX/CqX;
    .locals 3

    .prologue
    .line 1940879
    new-instance v1, LX/CrK;

    invoke-direct {v1, p2}, LX/CrK;-><init>(Landroid/content/Context;)V

    .line 1940880
    if-eqz p4, :cond_2

    .line 1940881
    iget-object v0, p0, LX/CrO;->a:LX/8bZ;

    const/4 v2, 0x0

    .line 1940882
    invoke-virtual {v0}, LX/8bZ;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/8bZ;->d:LX/0Uh;

    const/16 p2, 0xbd

    invoke-virtual {p0, p2, v2}, LX/0Uh;->a(IZ)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v0, v2

    .line 1940883
    if-eqz v0, :cond_1

    .line 1940884
    sget-object v0, LX/CrM;->a:[I

    invoke-virtual {p1}, LX/CrN;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1940885
    new-instance v0, LX/Cr2;

    invoke-direct {v0, p3, v1}, LX/Cr2;-><init>(LX/Ctg;LX/CrK;)V

    .line 1940886
    :goto_0
    return-object v0

    .line 1940887
    :pswitch_0
    new-instance v0, LX/Cr2;

    invoke-direct {v0, p3, v1}, LX/Cr2;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940888
    :pswitch_1
    new-instance v0, LX/Cr1;

    invoke-direct {v0, p3, v1}, LX/Cr1;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940889
    :pswitch_2
    new-instance v0, LX/CrC;

    invoke-direct {v0, p3, v1}, LX/CrC;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940890
    :pswitch_3
    new-instance v0, LX/Cr4;

    invoke-direct {v0, p3, v1}, LX/Cr4;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940891
    :pswitch_4
    new-instance v0, LX/CrE;

    invoke-direct {v0, p3, v1}, LX/CrE;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940892
    :pswitch_5
    new-instance v0, LX/Cqr;

    invoke-direct {v0, p3, v1}, LX/Cqr;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940893
    :cond_1
    sget-object v0, LX/CrM;->a:[I

    invoke-virtual {p1}, LX/CrN;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    .line 1940894
    new-instance v0, LX/Cqy;

    invoke-direct {v0, p3, v1}, LX/Cqy;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940895
    :pswitch_6
    new-instance v0, LX/Cqy;

    invoke-direct {v0, p3, v1}, LX/Cqy;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940896
    :pswitch_7
    new-instance v0, LX/Cr0;

    invoke-direct {v0, p3, v1}, LX/Cr0;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940897
    :pswitch_8
    new-instance v0, LX/CrB;

    invoke-direct {v0, p3, v1}, LX/CrB;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940898
    :pswitch_9
    new-instance v0, LX/Cr3;

    invoke-direct {v0, p3, v1}, LX/Cr3;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940899
    :pswitch_a
    new-instance v0, LX/CrD;

    invoke-direct {v0, p3, v1}, LX/CrD;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940900
    :pswitch_b
    new-instance v0, LX/Cqq;

    invoke-direct {v0, p3, v1}, LX/Cqq;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940901
    :cond_2
    sget-object v0, LX/CrM;->a:[I

    invoke-virtual {p1}, LX/CrN;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_2

    .line 1940902
    new-instance v0, LX/Cqx;

    invoke-direct {v0, p3, v1}, LX/Cqx;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940903
    :pswitch_c
    new-instance v0, LX/Cqx;

    invoke-direct {v0, p3, v1}, LX/Cqx;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940904
    :pswitch_d
    new-instance v0, LX/Cqz;

    invoke-direct {v0, p3, v1}, LX/Cqz;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940905
    :pswitch_e
    new-instance v0, LX/CrG;

    invoke-direct {v0, p3, v1}, LX/CrG;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 1940906
    :pswitch_f
    new-instance v0, LX/CrI;

    invoke-direct {v0, p3, v1}, LX/CrI;-><init>(LX/Ctg;LX/CrK;)V

    goto/16 :goto_0

    .line 1940907
    :pswitch_10
    new-instance v0, LX/CrH;

    invoke-direct {v0, p3, v1}, LX/CrH;-><init>(LX/Ctg;LX/CrK;)V

    goto/16 :goto_0

    .line 1940908
    :pswitch_11
    new-instance v0, LX/Cr6;

    invoke-direct {v0, p3, v1}, LX/Cr6;-><init>(LX/Ctg;LX/CrK;)V

    goto/16 :goto_0

    .line 1940909
    :pswitch_12
    new-instance v0, LX/Cr9;

    invoke-direct {v0, p3, v1}, LX/Cr9;-><init>(LX/Ctg;LX/CrK;)V

    goto/16 :goto_0

    .line 1940910
    :pswitch_13
    new-instance v0, LX/Cqp;

    invoke-direct {v0, p3, v1}, LX/Cqp;-><init>(LX/Ctg;LX/CrK;)V

    goto/16 :goto_0

    .line 1940911
    :pswitch_14
    new-instance v0, LX/Cql;

    invoke-direct {v0, p3, v1}, LX/Cql;-><init>(LX/Ctg;LX/CrK;)V

    goto/16 :goto_0

    .line 1940912
    :pswitch_15
    new-instance v0, LX/Cqm;

    invoke-direct {v0, p3, v1}, LX/Cqm;-><init>(LX/Ctg;LX/CrK;)V

    goto/16 :goto_0

    .line 1940913
    :pswitch_16
    new-instance v0, LX/Cr3;

    invoke-direct {v0, p3, v1}, LX/Cr3;-><init>(LX/Ctg;LX/CrK;)V

    goto/16 :goto_0

    .line 1940914
    :pswitch_17
    new-instance v0, LX/CrD;

    invoke-direct {v0, p3, v1}, LX/CrD;-><init>(LX/Ctg;LX/CrK;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
        :pswitch_12
        :pswitch_16
        :pswitch_17
        :pswitch_13
        :pswitch_13
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method
