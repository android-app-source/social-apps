.class public LX/ETl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ETa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/ETa",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/ETQ;

.field private final b:LX/EVb;


# direct methods
.method public constructor <init>(LX/ETQ;LX/EVb;)V
    .locals 0
    .param p1    # LX/ETQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2125297
    iput-object p1, p0, LX/ETl;->a:LX/ETQ;

    .line 2125298
    iput-object p2, p0, LX/ETl;->b:LX/EVb;

    .line 2125299
    return-void
.end method

.method private a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125300
    iget-object v0, p0, LX/ETl;->a:LX/ETQ;

    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/ETQ;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125301
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/ETQ;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2125302
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETQ;II)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125281
    :goto_0
    if-ltz p3, :cond_5

    invoke-virtual {p2}, LX/ETQ;->size()I

    move-result v0

    if-ge p3, v0, :cond_5

    .line 2125282
    invoke-virtual {p2, p3}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    .line 2125283
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2125284
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    .line 2125285
    const/4 v0, 0x1

    if-ne p4, v0, :cond_2

    const/4 v0, 0x0

    .line 2125286
    :goto_1
    invoke-virtual {v1, p1}, LX/ETQ;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2125287
    invoke-virtual {v1, p1}, LX/ETQ;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)I

    move-result v0

    add-int/2addr v0, p4

    .line 2125288
    :cond_0
    invoke-direct {p0, p1, v1, v0, p4}, LX/ETl;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETQ;II)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    .line 2125289
    if-eqz v0, :cond_4

    .line 2125290
    :cond_1
    :goto_2
    return-object v0

    .line 2125291
    :cond_2
    invoke-virtual {v1}, LX/ETQ;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 2125292
    :cond_3
    iget-object v1, p0, LX/ETl;->b:LX/EVb;

    invoke-static {v0, v1}, LX/EV8;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EVb;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2125293
    :cond_4
    add-int/2addr p3, p4

    .line 2125294
    goto :goto_0

    .line 2125295
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETZ;)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125261
    iget-object v0, p0, LX/ETl;->a:LX/ETQ;

    invoke-virtual {v0, p1}, LX/ETQ;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)I

    move-result v2

    .line 2125262
    sget-object v0, LX/ETZ;->NEXT:LX/ETZ;

    if-ne p2, v0, :cond_4

    const/4 v0, 0x1

    move v1, v0

    .line 2125263
    :goto_0
    add-int v0, v2, v1

    .line 2125264
    if-ltz v2, :cond_0

    iget-object v3, p0, LX/ETl;->a:LX/ETQ;

    invoke-virtual {v3}, LX/ETQ;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le v2, v3, :cond_6

    .line 2125265
    :cond_0
    invoke-direct {p0, p1}, LX/ETl;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    .line 2125266
    if-nez v0, :cond_3

    .line 2125267
    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2125268
    iget-object v4, p0, LX/ETl;->a:LX/ETQ;

    invoke-virtual {v4, v2}, LX/ETQ;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125269
    invoke-virtual {v4}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2125270
    invoke-virtual {v4}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v4

    invoke-virtual {v4}, LX/ETQ;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125271
    invoke-virtual {v4}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p2

    invoke-static {v3, p2}, LX/0sa;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 2125272
    :goto_1
    move-object p1, v4

    .line 2125273
    if-eqz p1, :cond_3

    .line 2125274
    invoke-direct {p0, p1}, LX/ETl;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    .line 2125275
    :cond_3
    if-nez v0, :cond_5

    .line 2125276
    const/4 v0, 0x0

    .line 2125277
    :goto_2
    return-object v0

    .line 2125278
    :cond_4
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    .line 2125279
    :cond_5
    iget-object v2, p0, LX/ETl;->a:LX/ETQ;

    invoke-virtual {v2, v0}, LX/ETQ;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)I

    move-result v0

    .line 2125280
    :cond_6
    iget-object v2, p0, LX/ETl;->a:LX/ETQ;

    invoke-direct {p0, p1, v2, v0, v1}, LX/ETl;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETQ;II)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    goto :goto_2

    :cond_7
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/ETZ;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2125260
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {p0, p1, p2}, LX/ETl;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETZ;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    return-object v0
.end method
