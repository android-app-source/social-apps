.class public final LX/EYS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EYE;


# instance fields
.field private final a:I

.field public b:LX/EXr;

.field private final c:Ljava/lang/String;

.field private final d:LX/EYQ;

.field public e:[LX/EYR;


# direct methods
.method public constructor <init>(LX/EXr;LX/EYQ;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2137382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137383
    iput p3, p0, LX/EYS;->a:I

    .line 2137384
    iput-object p1, p0, LX/EYS;->b:LX/EXr;

    .line 2137385
    const/4 v0, 0x0

    invoke-virtual {p1}, LX/EXr;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v0, v1}, LX/EYT;->b(LX/EYQ;LX/EYF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EYS;->c:Ljava/lang/String;

    .line 2137386
    iput-object p2, p0, LX/EYS;->d:LX/EYQ;

    .line 2137387
    invoke-virtual {p1}, LX/EXr;->l()I

    move-result v0

    new-array v0, v0, [LX/EYR;

    iput-object v0, p0, LX/EYS;->e:[LX/EYR;

    move v4, v5

    .line 2137388
    :goto_0
    invoke-virtual {p1}, LX/EXr;->l()I

    move-result v0

    if-ge v4, v0, :cond_0

    .line 2137389
    iget-object v6, p0, LX/EYS;->e:[LX/EYR;

    new-instance v0, LX/EYR;

    invoke-virtual {p1, v4}, LX/EXr;->a(I)LX/EXj;

    move-result-object v1

    move-object v2, p2

    move-object v3, p0

    invoke-direct/range {v0 .. v4}, LX/EYR;-><init>(LX/EXj;LX/EYQ;LX/EYS;I)V

    aput-object v0, v6, v4

    .line 2137390
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2137391
    :cond_0
    iget-object v0, p2, LX/EYQ;->h:LX/EYJ;

    invoke-virtual {v0, p0}, LX/EYJ;->a(LX/EYE;)V

    .line 2137392
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137393
    iget-object v0, p0, LX/EYS;->b:LX/EXr;

    invoke-virtual {v0}, LX/EXr;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137394
    iget-object v0, p0, LX/EYS;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/EYQ;
    .locals 1

    .prologue
    .line 2137395
    iget-object v0, p0, LX/EYS;->d:LX/EYQ;

    return-object v0
.end method

.method public final h()LX/EWY;
    .locals 1

    .prologue
    .line 2137396
    iget-object v0, p0, LX/EYS;->b:LX/EXr;

    return-object v0
.end method
