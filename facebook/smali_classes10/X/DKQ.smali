.class public LX/DKQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/groups/create/protocol/CreateGroupParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1986922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1986923
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1986924
    check-cast p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;

    .line 1986925
    const-string v0, "/me/groups"

    .line 1986926
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1986927
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "name"

    .line 1986928
    iget-object v4, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1986929
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v2

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "privacy"

    .line 1986930
    iget-object v5, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1986931
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986932
    iget-object v2, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1986933
    if-eqz v2, :cond_0

    .line 1986934
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "description"

    .line 1986935
    iget-object v4, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1986936
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986937
    :cond_0
    iget-object v2, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1986938
    if-eqz v2, :cond_1

    .line 1986939
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "group_icon_id"

    .line 1986940
    iget-object v4, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1986941
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986942
    :cond_1
    iget-object v2, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1986943
    if-eqz v2, :cond_2

    .line 1986944
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "ref"

    .line 1986945
    iget-object v4, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->e:Ljava/lang/String;

    move-object v4, v4

    .line 1986946
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986947
    :cond_2
    iget-object v2, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1986948
    if-eqz v2, :cond_3

    .line 1986949
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "suggestion_category"

    .line 1986950
    iget-object v4, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->f:Ljava/lang/String;

    move-object v4, v4

    .line 1986951
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986952
    :cond_3
    iget-object v2, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1986953
    if-eqz v2, :cond_4

    .line 1986954
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "suggestion_identifier"

    .line 1986955
    iget-object v4, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->g:Ljava/lang/String;

    move-object v4, v4

    .line 1986956
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986957
    :cond_4
    iget-object v2, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->i:Ljava/lang/String;

    move-object v2, v2

    .line 1986958
    if-eqz v2, :cond_5

    .line 1986959
    iget-object v2, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->i:Ljava/lang/String;

    move-object v2, v2

    .line 1986960
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    .line 1986961
    const-string v0, "/%s/groups"

    .line 1986962
    iget-object v2, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->i:Ljava/lang/String;

    move-object v2, v2

    .line 1986963
    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1986964
    :cond_5
    iget-object v2, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1986965
    if-eqz v2, :cond_6

    .line 1986966
    iget-object v2, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1986967
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1986968
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "group_type"

    .line 1986969
    iget-object v4, p1, Lcom/facebook/groups/create/protocol/CreateGroupParams;->h:Ljava/lang/String;

    move-object v4, v4

    .line 1986970
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986971
    :cond_6
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "create_group"

    .line 1986972
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1986973
    move-object v2, v2

    .line 1986974
    const-string v3, "POST"

    .line 1986975
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1986976
    move-object v2, v2

    .line 1986977
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 1986978
    move-object v0, v2

    .line 1986979
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1986980
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1986981
    move-object v0, v0

    .line 1986982
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1986983
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1986984
    move-object v0, v0

    .line 1986985
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1986986
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1986987
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 1986988
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1986989
    :cond_0
    new-instance v0, Lorg/apache/http/HttpException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Group creation failed,"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
