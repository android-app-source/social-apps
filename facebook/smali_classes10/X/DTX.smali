.class public final LX/DTX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/DTZ;


# direct methods
.method public constructor <init>(LX/DTZ;Ljava/lang/String;ZLandroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2000437
    iput-object p1, p0, LX/DTX;->e:LX/DTZ;

    iput-object p2, p0, LX/DTX;->a:Ljava/lang/String;

    iput-boolean p3, p0, LX/DTX;->b:Z

    iput-object p4, p0, LX/DTX;->c:Landroid/content/Context;

    iput-object p5, p0, LX/DTX;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    .line 2000438
    new-instance v1, LX/DTW;

    invoke-direct {v1, p0}, LX/DTW;-><init>(LX/DTX;)V

    .line 2000439
    iget-boolean v0, p0, LX/DTX;->b:Z

    if-nez v0, :cond_0

    .line 2000440
    iget-object v0, p0, LX/DTX;->e:LX/DTZ;

    iget-object v2, p0, LX/DTX;->c:Landroid/content/Context;

    iget-object v3, p0, LX/DTX;->d:Ljava/lang/String;

    const v4, 0x7f082fb0

    const v5, 0x7f082fad

    const v6, 0x7f082fae

    .line 2000441
    new-instance v7, LX/0ju;

    invoke-direct {v7, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2000442
    iget-object v8, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000443
    iget-object v8, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p0, 0x7f082f93

    invoke-virtual {v8, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance p0, LX/DTG;

    invoke-direct {p0, v0}, LX/DTG;-><init>(LX/DTZ;)V

    invoke-virtual {v7, v8, p0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000444
    iget-object v8, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000445
    iget-object v8, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v3, p0, p1

    invoke-virtual {v8, v6, p0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000446
    invoke-virtual {v7}, LX/0ju;->a()LX/2EJ;

    move-result-object v7

    invoke-virtual {v7}, LX/2EJ;->show()V

    .line 2000447
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2000448
    :cond_0
    iget-object v0, p0, LX/DTX;->e:LX/DTZ;

    iget-object v2, p0, LX/DTX;->c:Landroid/content/Context;

    const v3, 0x7f082fb0

    const v4, 0x7f082fad

    const v5, 0x7f082faf

    .line 2000449
    new-instance v6, LX/0ju;

    invoke-direct {v6, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2000450
    iget-object v7, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000451
    iget-object v7, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const v8, 0x7f082f93

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, LX/DTF;

    invoke-direct {v8, v0}, LX/DTF;-><init>(LX/DTZ;)V

    invoke-virtual {v6, v7, v8}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000452
    iget-object v7, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000453
    iget-object v7, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000454
    invoke-virtual {v6}, LX/0ju;->a()LX/2EJ;

    move-result-object v6

    invoke-virtual {v6}, LX/2EJ;->show()V

    .line 2000455
    goto :goto_0
.end method
