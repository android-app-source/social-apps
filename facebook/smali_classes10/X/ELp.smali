.class public final LX/ELp;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ELq;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CxP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<+",
            "LX/8d0;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/ELq;


# direct methods
.method public constructor <init>(LX/ELq;)V
    .locals 1

    .prologue
    .line 2109808
    iput-object p1, p0, LX/ELp;->c:LX/ELq;

    .line 2109809
    move-object v0, p1

    .line 2109810
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2109811
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2109812
    const-string v0, "SearchResultsRelatedPagesHScrollCardComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2109813
    if-ne p0, p1, :cond_1

    .line 2109814
    :cond_0
    :goto_0
    return v0

    .line 2109815
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2109816
    goto :goto_0

    .line 2109817
    :cond_3
    check-cast p1, LX/ELp;

    .line 2109818
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2109819
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2109820
    if-eq v2, v3, :cond_0

    .line 2109821
    iget-object v2, p0, LX/ELp;->a:LX/CxP;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ELp;->a:LX/CxP;

    iget-object v3, p1, LX/ELp;->a:LX/CxP;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2109822
    goto :goto_0

    .line 2109823
    :cond_5
    iget-object v2, p1, LX/ELp;->a:LX/CxP;

    if-nez v2, :cond_4

    .line 2109824
    :cond_6
    iget-object v2, p0, LX/ELp;->b:LX/CzL;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/ELp;->b:LX/CzL;

    iget-object v3, p1, LX/ELp;->b:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2109825
    goto :goto_0

    .line 2109826
    :cond_7
    iget-object v2, p1, LX/ELp;->b:LX/CzL;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
