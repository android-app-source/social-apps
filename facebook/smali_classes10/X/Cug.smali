.class public final LX/Cug;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Cuh;


# direct methods
.method public constructor <init>(LX/Cuh;)V
    .locals 0

    .prologue
    .line 1947078
    iput-object p1, p0, LX/Cug;->a:LX/Cuh;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1947079
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1947080
    check-cast p1, LX/2ou;

    .line 1947081
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    .line 1947082
    sget-object v1, LX/Cuf;->a:[I

    invoke-virtual {v0}, LX/2qV;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1947083
    :goto_0
    return-void

    .line 1947084
    :pswitch_0
    iget-object v0, p0, LX/Cug;->a:LX/Cuh;

    iget-boolean v0, v0, LX/Cuh;->b:Z

    if-eqz v0, :cond_0

    .line 1947085
    iget-object v0, p0, LX/Cug;->a:LX/Cuh;

    sget-object v1, LX/Cuj;->ATTEMPT_TO_PLAY:LX/Cuj;

    invoke-virtual {v0, v1}, LX/Cuh;->a(LX/Cuj;)Z

    goto :goto_0

    .line 1947086
    :cond_0
    iget-object v0, p0, LX/Cug;->a:LX/Cuh;

    sget-object v1, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    invoke-virtual {v0, v1}, LX/Cuh;->a(LX/Cuj;)Z

    goto :goto_0

    .line 1947087
    :pswitch_1
    iget-object v0, p0, LX/Cug;->a:LX/Cuh;

    sget-object v1, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    invoke-virtual {v0, v1}, LX/Cuh;->a(LX/Cuj;)Z

    goto :goto_0

    .line 1947088
    :pswitch_2
    iget-object v0, p0, LX/Cug;->a:LX/Cuh;

    sget-object v1, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v0, v1}, LX/Cuh;->a(LX/Cuj;)Z

    goto :goto_0

    .line 1947089
    :pswitch_3
    iget-object v0, p0, LX/Cug;->a:LX/Cuh;

    sget-object v1, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v0, v1}, LX/Cuh;->a(LX/Cuj;)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
