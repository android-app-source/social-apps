.class public final LX/Ebs;
.super LX/EWj;
.source ""

# interfaces
.implements LX/Ebr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/Ebs;",
        ">;",
        "LX/Ebr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:LX/EWc;

.field private d:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2144727
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2144728
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebs;->c:LX/EWc;

    .line 2144729
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebs;->d:LX/EWc;

    .line 2144730
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2144731
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2144732
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebs;->c:LX/EWc;

    .line 2144733
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebs;->d:LX/EWc;

    .line 2144734
    return-void
.end method

.method private d(LX/EWY;)LX/Ebs;
    .locals 1

    .prologue
    .line 2144735
    instance-of v0, p1, LX/Ebt;

    if-eqz v0, :cond_0

    .line 2144736
    check-cast p1, LX/Ebt;

    invoke-virtual {p0, p1}, LX/Ebs;->a(LX/Ebt;)LX/Ebs;

    move-result-object p0

    .line 2144737
    :goto_0
    return-object p0

    .line 2144738
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/Ebs;
    .locals 4

    .prologue
    .line 2144739
    const/4 v2, 0x0

    .line 2144740
    :try_start_0
    sget-object v0, LX/Ebt;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebt;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2144741
    if-eqz v0, :cond_0

    .line 2144742
    invoke-virtual {p0, v0}, LX/Ebs;->a(LX/Ebt;)LX/Ebs;

    .line 2144743
    :cond_0
    return-object p0

    .line 2144744
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2144745
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2144746
    check-cast v0, LX/Ebt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2144747
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2144748
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2144749
    invoke-virtual {p0, v1}, LX/Ebs;->a(LX/Ebt;)LX/Ebs;

    :cond_1
    throw v0

    .line 2144750
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static u()LX/Ebs;
    .locals 1

    .prologue
    .line 2144751
    new-instance v0, LX/Ebs;

    invoke-direct {v0}, LX/Ebs;-><init>()V

    return-object v0
.end method

.method private w()LX/Ebs;
    .locals 2

    .prologue
    .line 2144752
    invoke-static {}, LX/Ebs;->u()LX/Ebs;

    move-result-object v0

    invoke-direct {p0}, LX/Ebs;->y()LX/Ebt;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ebs;->a(LX/Ebt;)LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/Ebt;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2144753
    new-instance v2, LX/Ebt;

    invoke-direct {v2, p0}, LX/Ebt;-><init>(LX/EWj;)V

    .line 2144754
    iget v3, p0, LX/Ebs;->a:I

    .line 2144755
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 2144756
    :goto_0
    iget v1, p0, LX/Ebs;->b:I

    .line 2144757
    iput v1, v2, LX/Ebt;->id_:I

    .line 2144758
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2144759
    or-int/lit8 v0, v0, 0x2

    .line 2144760
    :cond_0
    iget-object v1, p0, LX/Ebs;->c:LX/EWc;

    .line 2144761
    iput-object v1, v2, LX/Ebt;->publicKey_:LX/EWc;

    .line 2144762
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 2144763
    or-int/lit8 v0, v0, 0x4

    .line 2144764
    :cond_1
    iget-object v1, p0, LX/Ebs;->d:LX/EWc;

    .line 2144765
    iput-object v1, v2, LX/Ebt;->privateKey_:LX/EWc;

    .line 2144766
    iput v0, v2, LX/Ebt;->bitField0_:I

    .line 2144767
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2144768
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2144769
    invoke-direct {p0, p1}, LX/Ebs;->d(LX/EWY;)LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2144770
    invoke-direct {p0, p1, p2}, LX/Ebs;->d(LX/EWd;LX/EYZ;)LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/Ebs;
    .locals 1

    .prologue
    .line 2144771
    iget v0, p0, LX/Ebs;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ebs;->a:I

    .line 2144772
    iput p1, p0, LX/Ebs;->b:I

    .line 2144773
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2144774
    return-object p0
.end method

.method public final a(LX/EWc;)LX/Ebs;
    .locals 1

    .prologue
    .line 2144775
    if-nez p1, :cond_0

    .line 2144776
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2144777
    :cond_0
    iget v0, p0, LX/Ebs;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/Ebs;->a:I

    .line 2144778
    iput-object p1, p0, LX/Ebs;->c:LX/EWc;

    .line 2144779
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2144780
    return-object p0
.end method

.method public final a(LX/Ebt;)LX/Ebs;
    .locals 2

    .prologue
    .line 2144781
    sget-object v0, LX/Ebt;->c:LX/Ebt;

    move-object v0, v0

    .line 2144782
    if-ne p1, v0, :cond_0

    .line 2144783
    :goto_0
    return-object p0

    .line 2144784
    :cond_0
    const/4 v0, 0x1

    .line 2144785
    iget v1, p1, LX/Ebt;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_1
    move v0, v0

    .line 2144786
    if-eqz v0, :cond_1

    .line 2144787
    iget v0, p1, LX/Ebt;->id_:I

    move v0, v0

    .line 2144788
    invoke-virtual {p0, v0}, LX/Ebs;->a(I)LX/Ebs;

    .line 2144789
    :cond_1
    iget v0, p1, LX/Ebt;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2144790
    if-eqz v0, :cond_2

    .line 2144791
    iget-object v0, p1, LX/Ebt;->publicKey_:LX/EWc;

    move-object v0, v0

    .line 2144792
    invoke-virtual {p0, v0}, LX/Ebs;->a(LX/EWc;)LX/Ebs;

    .line 2144793
    :cond_2
    iget v0, p1, LX/Ebt;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2144794
    if-eqz v0, :cond_3

    .line 2144795
    iget-object v0, p1, LX/Ebt;->privateKey_:LX/EWc;

    move-object v0, v0

    .line 2144796
    invoke-virtual {p0, v0}, LX/Ebs;->b(LX/EWc;)LX/Ebs;

    .line 2144797
    :cond_3
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2144798
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2144799
    invoke-direct {p0, p1, p2}, LX/Ebs;->d(LX/EWd;LX/EYZ;)LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2144726
    invoke-direct {p0}, LX/Ebs;->w()LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/EWc;)LX/Ebs;
    .locals 1

    .prologue
    .line 2144720
    if-nez p1, :cond_0

    .line 2144721
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2144722
    :cond_0
    iget v0, p0, LX/Ebs;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/Ebs;->a:I

    .line 2144723
    iput-object p1, p0, LX/Ebs;->d:LX/EWc;

    .line 2144724
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2144725
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2144710
    invoke-direct {p0, p1, p2}, LX/Ebs;->d(LX/EWd;LX/EYZ;)LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2144709
    invoke-direct {p0}, LX/Ebs;->w()LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2144708
    invoke-direct {p0, p1}, LX/Ebs;->d(LX/EWY;)LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2144707
    invoke-direct {p0}, LX/Ebs;->w()LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2144706
    sget-object v0, LX/Eck;->p:LX/EYn;

    const-class v1, LX/Ebt;

    const-class v2, LX/Ebs;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2144703
    sget-object v0, LX/Eck;->o:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2144711
    invoke-direct {p0}, LX/Ebs;->w()LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2144712
    invoke-direct {p0}, LX/Ebs;->y()LX/Ebt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2144713
    invoke-virtual {p0}, LX/Ebs;->l()LX/Ebt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2144714
    invoke-direct {p0}, LX/Ebs;->y()LX/Ebt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2144715
    invoke-virtual {p0}, LX/Ebs;->l()LX/Ebt;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/Ebt;
    .locals 2

    .prologue
    .line 2144716
    invoke-direct {p0}, LX/Ebs;->y()LX/Ebt;

    move-result-object v0

    .line 2144717
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2144718
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2144719
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2144704
    sget-object v0, LX/Ebt;->c:LX/Ebt;

    move-object v0, v0

    .line 2144705
    return-object v0
.end method
