.class public LX/DXJ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DXH;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DXK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2010014
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DXJ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DXK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2010015
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2010016
    iput-object p1, p0, LX/DXJ;->b:LX/0Ot;

    .line 2010017
    return-void
.end method

.method public static a(LX/0QB;)LX/DXJ;
    .locals 4

    .prologue
    .line 2010018
    const-class v1, LX/DXJ;

    monitor-enter v1

    .line 2010019
    :try_start_0
    sget-object v0, LX/DXJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2010020
    sput-object v2, LX/DXJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2010021
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2010022
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2010023
    new-instance v3, LX/DXJ;

    const/16 p0, 0x247f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DXJ;-><init>(LX/0Ot;)V

    .line 2010024
    move-object v0, v3

    .line 2010025
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2010026
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DXJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2010027
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2010028
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2010029
    check-cast p2, LX/DXI;

    .line 2010030
    iget-object v0, p0, LX/DXJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DXK;

    iget-object v1, p2, LX/DXI;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    .line 2010031
    if-nez v1, :cond_0

    .line 2010032
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/BdR;->c(LX/1De;)LX/BdP;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 2010033
    :goto_0
    move-object v0, v2

    .line 2010034
    return-object v0

    .line 2010035
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;->n()Z

    move-result v3

    .line 2010036
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x1

    invoke-interface {v2, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    if-eqz v3, :cond_4

    const v2, 0x7f083091

    :goto_1
    invoke-virtual {v5, v2}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    const v5, 0x7f0b0050

    invoke-virtual {v2, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v5, 0x7f0a00f9

    invoke-virtual {v2, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    iget-object v4, v0, LX/DXK;->a:LX/DXd;

    const/4 v5, 0x0

    .line 2010037
    new-instance p0, LX/DXb;

    invoke-direct {p0, v4}, LX/DXb;-><init>(LX/DXd;)V

    .line 2010038
    sget-object p2, LX/DXd;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/DXa;

    .line 2010039
    if-nez p2, :cond_1

    .line 2010040
    new-instance p2, LX/DXa;

    invoke-direct {p2}, LX/DXa;-><init>()V

    .line 2010041
    :cond_1
    invoke-static {p2, p1, v5, v5, p0}, LX/DXa;->a$redex0(LX/DXa;LX/1De;IILX/DXb;)V

    .line 2010042
    move-object p0, p2

    .line 2010043
    move-object v5, p0

    .line 2010044
    move-object v4, v5

    .line 2010045
    iget-object v5, v4, LX/DXa;->a:LX/DXb;

    iput-object v1, v5, LX/DXb;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    .line 2010046
    iget-object v5, v4, LX/DXa;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2010047
    move-object v4, v4

    .line 2010048
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    if-eqz v3, :cond_5

    const v2, 0x7f083094

    :goto_2
    invoke-virtual {v5, v2}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    const v5, 0x7f0b004e

    invoke-virtual {v2, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v5, 0x7f0a00f9

    invoke-virtual {v2, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2010049
    if-nez v3, :cond_2

    .line 2010050
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f08002c

    invoke-virtual {v4, v5}, LX/1ne;->h(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00dc

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2010051
    :cond_2
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f08309c

    invoke-virtual {v4, v5}, LX/1ne;->h(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0050

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00f9

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2010052
    if-eqz v3, :cond_6

    .line 2010053
    iget-object v3, v0, LX/DXK;->c:LX/DXY;

    const/4 v4, 0x0

    .line 2010054
    new-instance v5, LX/DXX;

    invoke-direct {v5, v3}, LX/DXX;-><init>(LX/DXY;)V

    .line 2010055
    sget-object p0, LX/DXY;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DXW;

    .line 2010056
    if-nez p0, :cond_3

    .line 2010057
    new-instance p0, LX/DXW;

    invoke-direct {p0}, LX/DXW;-><init>()V

    .line 2010058
    :cond_3
    invoke-static {p0, p1, v4, v4, v5}, LX/DXW;->a$redex0(LX/DXW;LX/1De;IILX/DXX;)V

    .line 2010059
    move-object v5, p0

    .line 2010060
    move-object v4, v5

    .line 2010061
    move-object v3, v4

    .line 2010062
    iget-object v4, v3, LX/DXW;->a:LX/DXX;

    iput-object v1, v4, LX/DXX;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    .line 2010063
    iget-object v4, v3, LX/DXW;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2010064
    move-object v3, v3

    .line 2010065
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2010066
    :goto_3
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_0

    .line 2010067
    :cond_4
    const v2, 0x7f083090

    goto/16 :goto_1

    :cond_5
    const v2, 0x7f083093

    goto/16 :goto_2

    .line 2010068
    :cond_6
    iget-object v3, v0, LX/DXK;->b:LX/DXU;

    const/4 v4, 0x0

    .line 2010069
    new-instance v5, LX/DXT;

    invoke-direct {v5, v3}, LX/DXT;-><init>(LX/DXU;)V

    .line 2010070
    sget-object p0, LX/DXU;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DXS;

    .line 2010071
    if-nez p0, :cond_7

    .line 2010072
    new-instance p0, LX/DXS;

    invoke-direct {p0}, LX/DXS;-><init>()V

    .line 2010073
    :cond_7
    invoke-static {p0, p1, v4, v4, v5}, LX/DXS;->a$redex0(LX/DXS;LX/1De;IILX/DXT;)V

    .line 2010074
    move-object v5, p0

    .line 2010075
    move-object v4, v5

    .line 2010076
    move-object v3, v4

    .line 2010077
    iget-object v4, v3, LX/DXS;->a:LX/DXT;

    iput-object v1, v4, LX/DXT;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    .line 2010078
    iget-object v4, v3, LX/DXS;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2010079
    move-object v3, v3

    .line 2010080
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2010081
    invoke-static {}, LX/1dS;->b()V

    .line 2010082
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/DXH;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2010083
    new-instance v1, LX/DXI;

    invoke-direct {v1, p0}, LX/DXI;-><init>(LX/DXJ;)V

    .line 2010084
    sget-object v2, LX/DXJ;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DXH;

    .line 2010085
    if-nez v2, :cond_0

    .line 2010086
    new-instance v2, LX/DXH;

    invoke-direct {v2}, LX/DXH;-><init>()V

    .line 2010087
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/DXH;->a$redex0(LX/DXH;LX/1De;IILX/DXI;)V

    .line 2010088
    move-object v1, v2

    .line 2010089
    move-object v0, v1

    .line 2010090
    return-object v0
.end method
