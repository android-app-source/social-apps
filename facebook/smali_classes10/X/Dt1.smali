.class public final LX/Dt1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLInterfaces$DefaultTimeRangeFields;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dt3;


# direct methods
.method public constructor <init>(LX/Dt3;)V
    .locals 0

    .prologue
    .line 2051027
    iput-object p1, p0, LX/Dt1;->a:LX/Dt3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 6

    .prologue
    .line 2051021
    check-cast p1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    check-cast p2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 2051022
    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 2051023
    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 2051024
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2051025
    goto :goto_0

    .line 2051026
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
