.class public final enum LX/EYO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EYO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EYO;

.field public static final enum BOOL:LX/EYO;

.field public static final enum BYTES:LX/EYO;

.field public static final enum DOUBLE:LX/EYO;

.field public static final enum ENUM:LX/EYO;

.field public static final enum FIXED32:LX/EYO;

.field public static final enum FIXED64:LX/EYO;

.field public static final enum FLOAT:LX/EYO;

.field public static final enum GROUP:LX/EYO;

.field public static final enum INT32:LX/EYO;

.field public static final enum INT64:LX/EYO;

.field public static final enum MESSAGE:LX/EYO;

.field public static final enum SFIXED32:LX/EYO;

.field public static final enum SFIXED64:LX/EYO;

.field public static final enum SINT32:LX/EYO;

.field public static final enum SINT64:LX/EYO;

.field public static final enum STRING:LX/EYO;

.field public static final enum UINT32:LX/EYO;

.field public static final enum UINT64:LX/EYO;


# instance fields
.field private javaType:LX/EYN;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2137063
    new-instance v0, LX/EYO;

    const-string v1, "DOUBLE"

    sget-object v2, LX/EYN;->DOUBLE:LX/EYN;

    invoke-direct {v0, v1, v4, v2}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->DOUBLE:LX/EYO;

    .line 2137064
    new-instance v0, LX/EYO;

    const-string v1, "FLOAT"

    sget-object v2, LX/EYN;->FLOAT:LX/EYN;

    invoke-direct {v0, v1, v5, v2}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->FLOAT:LX/EYO;

    .line 2137065
    new-instance v0, LX/EYO;

    const-string v1, "INT64"

    sget-object v2, LX/EYN;->LONG:LX/EYN;

    invoke-direct {v0, v1, v6, v2}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->INT64:LX/EYO;

    .line 2137066
    new-instance v0, LX/EYO;

    const-string v1, "UINT64"

    sget-object v2, LX/EYN;->LONG:LX/EYN;

    invoke-direct {v0, v1, v7, v2}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->UINT64:LX/EYO;

    .line 2137067
    new-instance v0, LX/EYO;

    const-string v1, "INT32"

    sget-object v2, LX/EYN;->INT:LX/EYN;

    invoke-direct {v0, v1, v8, v2}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->INT32:LX/EYO;

    .line 2137068
    new-instance v0, LX/EYO;

    const-string v1, "FIXED64"

    const/4 v2, 0x5

    sget-object v3, LX/EYN;->LONG:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->FIXED64:LX/EYO;

    .line 2137069
    new-instance v0, LX/EYO;

    const-string v1, "FIXED32"

    const/4 v2, 0x6

    sget-object v3, LX/EYN;->INT:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->FIXED32:LX/EYO;

    .line 2137070
    new-instance v0, LX/EYO;

    const-string v1, "BOOL"

    const/4 v2, 0x7

    sget-object v3, LX/EYN;->BOOLEAN:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->BOOL:LX/EYO;

    .line 2137071
    new-instance v0, LX/EYO;

    const-string v1, "STRING"

    const/16 v2, 0x8

    sget-object v3, LX/EYN;->STRING:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->STRING:LX/EYO;

    .line 2137072
    new-instance v0, LX/EYO;

    const-string v1, "GROUP"

    const/16 v2, 0x9

    sget-object v3, LX/EYN;->MESSAGE:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->GROUP:LX/EYO;

    .line 2137073
    new-instance v0, LX/EYO;

    const-string v1, "MESSAGE"

    const/16 v2, 0xa

    sget-object v3, LX/EYN;->MESSAGE:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->MESSAGE:LX/EYO;

    .line 2137074
    new-instance v0, LX/EYO;

    const-string v1, "BYTES"

    const/16 v2, 0xb

    sget-object v3, LX/EYN;->BYTE_STRING:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->BYTES:LX/EYO;

    .line 2137075
    new-instance v0, LX/EYO;

    const-string v1, "UINT32"

    const/16 v2, 0xc

    sget-object v3, LX/EYN;->INT:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->UINT32:LX/EYO;

    .line 2137076
    new-instance v0, LX/EYO;

    const-string v1, "ENUM"

    const/16 v2, 0xd

    sget-object v3, LX/EYN;->ENUM:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->ENUM:LX/EYO;

    .line 2137077
    new-instance v0, LX/EYO;

    const-string v1, "SFIXED32"

    const/16 v2, 0xe

    sget-object v3, LX/EYN;->INT:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->SFIXED32:LX/EYO;

    .line 2137078
    new-instance v0, LX/EYO;

    const-string v1, "SFIXED64"

    const/16 v2, 0xf

    sget-object v3, LX/EYN;->LONG:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->SFIXED64:LX/EYO;

    .line 2137079
    new-instance v0, LX/EYO;

    const-string v1, "SINT32"

    const/16 v2, 0x10

    sget-object v3, LX/EYN;->INT:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->SINT32:LX/EYO;

    .line 2137080
    new-instance v0, LX/EYO;

    const-string v1, "SINT64"

    const/16 v2, 0x11

    sget-object v3, LX/EYN;->LONG:LX/EYN;

    invoke-direct {v0, v1, v2, v3}, LX/EYO;-><init>(Ljava/lang/String;ILX/EYN;)V

    sput-object v0, LX/EYO;->SINT64:LX/EYO;

    .line 2137081
    const/16 v0, 0x12

    new-array v0, v0, [LX/EYO;

    sget-object v1, LX/EYO;->DOUBLE:LX/EYO;

    aput-object v1, v0, v4

    sget-object v1, LX/EYO;->FLOAT:LX/EYO;

    aput-object v1, v0, v5

    sget-object v1, LX/EYO;->INT64:LX/EYO;

    aput-object v1, v0, v6

    sget-object v1, LX/EYO;->UINT64:LX/EYO;

    aput-object v1, v0, v7

    sget-object v1, LX/EYO;->INT32:LX/EYO;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/EYO;->FIXED64:LX/EYO;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EYO;->FIXED32:LX/EYO;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EYO;->BOOL:LX/EYO;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EYO;->STRING:LX/EYO;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/EYO;->GROUP:LX/EYO;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/EYO;->MESSAGE:LX/EYO;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/EYO;->BYTES:LX/EYO;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/EYO;->UINT32:LX/EYO;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/EYO;->ENUM:LX/EYO;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/EYO;->SFIXED32:LX/EYO;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/EYO;->SFIXED64:LX/EYO;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/EYO;->SINT32:LX/EYO;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/EYO;->SINT64:LX/EYO;

    aput-object v2, v0, v1

    sput-object v0, LX/EYO;->$VALUES:[LX/EYO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/EYN;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYN;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2137060
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2137061
    iput-object p3, p0, LX/EYO;->javaType:LX/EYN;

    .line 2137062
    return-void
.end method

.method public static valueOf(LX/EXK;)LX/EYO;
    .locals 2

    .prologue
    .line 2137059
    invoke-static {}, LX/EYO;->values()[LX/EYO;

    move-result-object v0

    invoke-virtual {p0}, LX/EXK;->getNumber()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/EYO;
    .locals 1

    .prologue
    .line 2137082
    const-class v0, LX/EYO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EYO;

    return-object v0
.end method

.method public static values()[LX/EYO;
    .locals 1

    .prologue
    .line 2137058
    sget-object v0, LX/EYO;->$VALUES:[LX/EYO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EYO;

    return-object v0
.end method


# virtual methods
.method public final getJavaType()LX/EYN;
    .locals 1

    .prologue
    .line 2137057
    iget-object v0, p0, LX/EYO;->javaType:LX/EYN;

    return-object v0
.end method

.method public final toProto()LX/EXK;
    .locals 1

    .prologue
    .line 2137056
    invoke-virtual {p0}, LX/EYO;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, LX/EXK;->valueOf(I)LX/EXK;

    move-result-object v0

    return-object v0
.end method
