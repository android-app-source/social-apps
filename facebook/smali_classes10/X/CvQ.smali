.class public LX/CvQ;
.super LX/1Cd;
.source ""


# instance fields
.field public final a:LX/0SG;

.field private final b:LX/CvY;

.field private final c:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field private final d:LX/CzE;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CvU;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/Cvl;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1BX;

.field public final i:Z

.field public final j:I


# direct methods
.method public constructor <init>(LX/0SG;LX/CvY;LX/1BX;LX/0ad;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;)V
    .locals 2
    .param p5    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1948288
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 1948289
    iput-object p1, p0, LX/CvQ;->a:LX/0SG;

    .line 1948290
    iput-object p2, p0, LX/CvQ;->b:LX/CvY;

    .line 1948291
    iput-object p5, p0, LX/CvQ;->c:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1948292
    iput-object p3, p0, LX/CvQ;->h:LX/1BX;

    .line 1948293
    iput-object p6, p0, LX/CvQ;->d:LX/CzE;

    .line 1948294
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CvQ;->e:Ljava/util/List;

    .line 1948295
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CvQ;->f:Ljava/util/Map;

    .line 1948296
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CvQ;->g:Ljava/util/Map;

    .line 1948297
    sget-short v0, LX/100;->bu:S

    invoke-interface {p4, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CvQ;->i:Z

    .line 1948298
    sget v0, LX/100;->bt:I

    invoke-interface {p4, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/CvQ;->j:I

    .line 1948299
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1948281
    iget-object v0, p0, LX/CvQ;->e:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1948282
    iget-object v1, p0, LX/CvQ;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1948283
    iget-object v1, p0, LX/CvQ;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1948284
    iget-object v1, p0, LX/CvQ;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1948285
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1948286
    iget-object v1, p0, LX/CvQ;->b:LX/CvY;

    iget-object v2, p0, LX/CvQ;->c:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v1, v2, v0}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;)V

    .line 1948287
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;JJ)V
    .locals 12

    .prologue
    .line 1948259
    const-wide/16 v0, 0xa

    cmp-long v0, p4, v0

    if-gez v0, :cond_1

    .line 1948260
    :cond_0
    :goto_0
    return-void

    .line 1948261
    :cond_1
    const/4 v7, 0x0

    .line 1948262
    const/4 v1, 0x0

    .line 1948263
    const/4 v9, 0x0

    .line 1948264
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_3

    .line 1948265
    iget-object v2, p0, LX/CvQ;->d:LX/CzE;

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v0}, LX/CzE;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v7

    .line 1948266
    iget-object v2, p0, LX/CvQ;->d:LX/CzE;

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v0}, LX/CzE;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v9

    move-object v8, v1

    .line 1948267
    :goto_1
    const/4 v1, 0x0

    .line 1948268
    instance-of v0, p1, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 1948269
    check-cast v0, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->b()Ljava/lang/String;

    move-result-object v1

    .line 1948270
    :cond_2
    :goto_2
    iget-object v0, p0, LX/CvQ;->d:LX/CzE;

    invoke-virtual {v0, p1}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v0

    invoke-static {p1}, LX/CvQ;->e(Lcom/facebook/graphql/model/FeedUnit;)LX/0P1;

    move-result-object v10

    move-wide v2, p2

    move-wide/from16 v4, p4

    move-object v6, p1

    invoke-static/range {v0 .. v10}, LX/CvU;->a(ILjava/lang/String;JJLcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;LX/0P1;)LX/CvU;

    move-result-object v0

    .line 1948271
    iget-object v1, p0, LX/CvQ;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1948272
    iget-object v0, p0, LX/CvQ;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_0

    .line 1948273
    invoke-direct {p0}, LX/CvQ;->a()V

    goto :goto_0

    .line 1948274
    :cond_3
    instance-of v0, p1, LX/Cz6;

    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1948275
    check-cast v0, LX/Cz6;

    invoke-interface {v0}, LX/Cz6;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v7

    .line 1948276
    instance-of v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 1948277
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->t()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    move-object v1, v0

    :cond_4
    move-object v0, p1

    .line 1948278
    check-cast v0, LX/Cz6;

    invoke-interface {v0}, LX/Cz6;->m()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v9, v0

    move-object v8, v1

    goto :goto_1

    .line 1948279
    :cond_5
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    .line 1948280
    iget-object v1, p0, LX/CvQ;->d:LX/CzE;

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/CzE;->c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_6
    move-object v8, v1

    goto :goto_1
.end method

.method public static a$redex0(LX/CvQ;Lcom/facebook/graphql/model/FeedUnit;Z)V
    .locals 9

    .prologue
    .line 1948243
    invoke-static {p0, p1}, LX/CvQ;->b(LX/CvQ;Lcom/facebook/graphql/model/FeedUnit;)LX/Cvl;

    move-result-object v6

    .line 1948244
    if-eqz p2, :cond_1

    .line 1948245
    iget-boolean v0, v6, LX/Cvl;->c:Z

    move v0, v0

    .line 1948246
    if-nez v0, :cond_1

    .line 1948247
    iget-object v0, p0, LX/CvQ;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 1948248
    iput-wide v0, v6, LX/Cvl;->a:J

    .line 1948249
    :cond_0
    :goto_0
    iput-boolean p2, v6, LX/Cvl;->c:Z

    .line 1948250
    return-void

    .line 1948251
    :cond_1
    if-nez p2, :cond_0

    .line 1948252
    iget-boolean v0, v6, LX/Cvl;->c:Z

    move v0, v0

    .line 1948253
    if-eqz v0, :cond_0

    .line 1948254
    iget-wide v7, v6, LX/Cvl;->a:J

    move-wide v2, v7

    .line 1948255
    iget-object v0, p0, LX/CvQ;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 1948256
    iget-wide v7, v6, LX/Cvl;->a:J

    move-wide v4, v7

    .line 1948257
    sub-long v4, v0, v4

    move-object v0, p0

    move-object v1, p1

    .line 1948258
    invoke-direct/range {v0 .. v5}, LX/CvQ;->a(Lcom/facebook/graphql/model/FeedUnit;JJ)V

    goto :goto_0
.end method

.method public static b(LX/CvQ;Lcom/facebook/graphql/model/FeedUnit;)LX/Cvl;
    .locals 2

    .prologue
    .line 1948238
    iget-object v0, p0, LX/CvQ;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvl;

    .line 1948239
    if-nez v0, :cond_0

    .line 1948240
    new-instance v0, LX/Cvl;

    invoke-direct {v0}, LX/Cvl;-><init>()V

    .line 1948241
    iget-object v1, p0, LX/CvQ;->g:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948242
    :cond_0
    return-object v0
.end method

.method public static d(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 1948300
    instance-of v0, p0, LX/Cz6;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Lcom/facebook/graphql/model/FeedUnit;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1948230
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 1948231
    instance-of v1, p0, Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v1, :cond_0

    .line 1948232
    check-cast p0, Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 1948233
    iget-object v1, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    move-object v1, v1

    .line 1948234
    invoke-virtual {v1}, LX/Cz3;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1948235
    :cond_0
    :goto_0
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    .line 1948236
    :sswitch_0
    const-string v2, "media_combined_module_number_of_annotated_results"

    invoke-virtual {v1}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v1}, LX/Cvf;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1948237
    :sswitch_1
    const-string v2, "thumbnail_decorations"

    invoke-virtual {v1}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v1}, LX/Cvh;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5852ec13 -> :sswitch_0
        -0x5389cb28 -> :sswitch_1
        -0x30cf2d19 -> :sswitch_1
        0x4ed245b -> :sswitch_1
        0xd5fee8a -> :sswitch_1
        0x735086f1 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/0g8;Ljava/lang/Object;I)V
    .locals 1

    .prologue
    .line 1948228
    new-instance v0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;-><init>(LX/CvQ;LX/0g8;Ljava/lang/Object;I)V

    invoke-interface {p1, v0}, LX/0g8;->a(Ljava/lang/Runnable;)V

    .line 1948229
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1948224
    invoke-static {p1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1948225
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/CvQ;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/CvQ;->i:Z

    if-eqz v1, :cond_1

    .line 1948226
    :cond_0
    :goto_0
    return-void

    .line 1948227
    :cond_1
    iget-object v1, p0, LX/CvQ;->f:Ljava/util/Map;

    iget-object v2, p0, LX/CvQ;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(LX/0g8;)V
    .locals 0

    .prologue
    .line 1948222
    invoke-direct {p0}, LX/CvQ;->a()V

    .line 1948223
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1948212
    invoke-static {p1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 1948213
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/CvQ;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1948214
    :cond_0
    :goto_0
    return-void

    .line 1948215
    :cond_1
    iget-boolean v0, p0, LX/CvQ;->i:Z

    if-eqz v0, :cond_2

    .line 1948216
    const/4 v0, 0x0

    invoke-static {p0, v1, v0}, LX/CvQ;->a$redex0(LX/CvQ;Lcom/facebook/graphql/model/FeedUnit;Z)V

    goto :goto_0

    .line 1948217
    :cond_2
    iget-object v0, p0, LX/CvQ;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1948218
    if-eqz v0, :cond_0

    .line 1948219
    iget-object v2, p0, LX/CvQ;->f:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948220
    iget-object v2, p0, LX/CvQ;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v2, v4

    .line 1948221
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/CvQ;->a(Lcom/facebook/graphql/model/FeedUnit;JJ)V

    goto :goto_0
.end method
