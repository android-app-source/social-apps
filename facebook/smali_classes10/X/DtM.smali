.class public final enum LX/DtM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DtM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DtM;

.field public static final enum MESSENGER_COMMERCE:LX/DtM;

.field public static final enum NMOR_TRANSFER:LX/DtM;

.field public static final enum PEER_TO_PEER_SINGLE_SIDED_INCENTIVE:LX/DtM;

.field public static final enum PEER_TO_PEER_TRANSFER:LX/DtM;

.field public static final enum UNKNOWN:LX/DtM;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2051932
    new-instance v0, LX/DtM;

    const-string v1, "MESSENGER_COMMERCE"

    const-string v2, "MessengerCommercePayment"

    invoke-direct {v0, v1, v3, v2}, LX/DtM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DtM;->MESSENGER_COMMERCE:LX/DtM;

    .line 2051933
    new-instance v0, LX/DtM;

    const-string v1, "NMOR_TRANSFER"

    const-string v2, "NmorPayment"

    invoke-direct {v0, v1, v4, v2}, LX/DtM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DtM;->NMOR_TRANSFER:LX/DtM;

    .line 2051934
    new-instance v0, LX/DtM;

    const-string v1, "PEER_TO_PEER_SINGLE_SIDED_INCENTIVE"

    const-string v2, "PeerToPeerSingleSidedIncentivePayment"

    invoke-direct {v0, v1, v5, v2}, LX/DtM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DtM;->PEER_TO_PEER_SINGLE_SIDED_INCENTIVE:LX/DtM;

    .line 2051935
    new-instance v0, LX/DtM;

    const-string v1, "PEER_TO_PEER_TRANSFER"

    const-string v2, "PeerToPeerTransfer"

    invoke-direct {v0, v1, v6, v2}, LX/DtM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DtM;->PEER_TO_PEER_TRANSFER:LX/DtM;

    .line 2051936
    new-instance v0, LX/DtM;

    const-string v1, "UNKNOWN"

    const-string v2, "Unknown"

    invoke-direct {v0, v1, v7, v2}, LX/DtM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DtM;->UNKNOWN:LX/DtM;

    .line 2051937
    const/4 v0, 0x5

    new-array v0, v0, [LX/DtM;

    sget-object v1, LX/DtM;->MESSENGER_COMMERCE:LX/DtM;

    aput-object v1, v0, v3

    sget-object v1, LX/DtM;->NMOR_TRANSFER:LX/DtM;

    aput-object v1, v0, v4

    sget-object v1, LX/DtM;->PEER_TO_PEER_SINGLE_SIDED_INCENTIVE:LX/DtM;

    aput-object v1, v0, v5

    sget-object v1, LX/DtM;->PEER_TO_PEER_TRANSFER:LX/DtM;

    aput-object v1, v0, v6

    sget-object v1, LX/DtM;->UNKNOWN:LX/DtM;

    aput-object v1, v0, v7

    sput-object v0, LX/DtM;->$VALUES:[LX/DtM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2051938
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2051939
    iput-object p3, p0, LX/DtM;->mValue:Ljava/lang/String;

    .line 2051940
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/DtM;
    .locals 5

    .prologue
    .line 2051941
    invoke-static {}, LX/DtM;->values()[LX/DtM;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2051942
    iget-object v4, v0, LX/DtM;->mValue:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2051943
    :goto_1
    return-object v0

    .line 2051944
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2051945
    :cond_1
    sget-object v0, LX/DtM;->UNKNOWN:LX/DtM;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/DtM;
    .locals 1

    .prologue
    .line 2051946
    const-class v0, LX/DtM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DtM;

    return-object v0
.end method

.method public static values()[LX/DtM;
    .locals 1

    .prologue
    .line 2051947
    sget-object v0, LX/DtM;->$VALUES:[LX/DtM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DtM;

    return-object v0
.end method


# virtual methods
.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2051948
    iget-object v0, p0, LX/DtM;->mValue:Ljava/lang/String;

    return-object v0
.end method
