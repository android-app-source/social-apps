.class public final LX/D6m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Zp;

.field public final synthetic b:LX/D6p;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:I

.field public final synthetic e:LX/3H0;

.field public final synthetic f:LX/D6o;

.field public final synthetic g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;LX/1Zp;LX/D6p;Ljava/lang/String;ILX/3H0;LX/D6o;)V
    .locals 0

    .prologue
    .line 1966129
    iput-object p1, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iput-object p2, p0, LX/D6m;->a:LX/1Zp;

    iput-object p3, p0, LX/D6m;->b:LX/D6p;

    iput-object p4, p0, LX/D6m;->c:Ljava/lang/String;

    iput p5, p0, LX/D6m;->d:I

    iput-object p6, p0, LX/D6m;->e:LX/3H0;

    iput-object p7, p0, LX/D6m;->f:LX/D6o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(ZZ)V
    .locals 6

    .prologue
    .line 1966130
    iget-object v0, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->o:LX/3H4;

    iget-object v1, p0, LX/D6m;->c:Ljava/lang/String;

    iget-object v3, p0, LX/D6m;->e:LX/3H0;

    iget-object v2, p0, LX/D6m;->f:LX/D6o;

    iget-object v2, v2, LX/D6o;->a:LX/D6v;

    iget v5, v2, LX/D6v;->g:I

    move v2, p1

    move v4, p2

    .line 1966131
    if-eqz v2, :cond_0

    const-string p0, "commercial_break_video_fetch_success"

    .line 1966132
    :goto_0
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {p1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "commercial_break"

    .line 1966133
    iput-object p0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1966134
    move-object p0, p1

    .line 1966135
    const-string p1, "host_video_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "instream_video_ad_type"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "is_sponsored"

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "is_empty"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "ad_break_index"

    invoke-virtual {p0, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 1966136
    iget-object p1, v0, LX/3H4;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1966137
    return-void

    .line 1966138
    :cond_0
    const-string p0, "commercial_break_video_fetch_failure"

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1966139
    iget-object v0, p0, LX/D6m;->b:LX/D6p;

    sget-object v1, LX/D6r;->FAILED:LX/D6r;

    iput-object v1, v0, LX/D6p;->b:LX/D6r;

    .line 1966140
    invoke-direct {p0, v2, v2}, LX/D6m;->a(ZZ)V

    .line 1966141
    iget-object v0, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    const-string v1, "Failed to fetch video ad"

    invoke-static {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->j(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)V

    .line 1966142
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1966143
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1966144
    if-eqz p1, :cond_0

    .line 1966145
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1966146
    instance-of v0, v0, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D6m;->a:LX/1Zp;

    invoke-virtual {v0}, LX/1Zp;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1966147
    :cond_0
    iget-object v0, p0, LX/D6m;->b:LX/D6p;

    sget-object v2, LX/D6r;->FAILED:LX/D6r;

    iput-object v2, v0, LX/D6p;->b:LX/D6r;

    .line 1966148
    invoke-direct {p0, v1, v1}, LX/D6m;->a(ZZ)V

    .line 1966149
    iget-object v0, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    const-string v1, "Video ad fetcher returned no result"

    invoke-static {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->j(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)V

    .line 1966150
    :cond_1
    :goto_0
    return-void

    .line 1966151
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1966152
    check-cast v0, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;->a()Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;

    move-result-object v0

    .line 1966153
    if-nez v0, :cond_3

    .line 1966154
    iget-object v0, p0, LX/D6m;->b:LX/D6p;

    sget-object v2, LX/D6r;->FAILED:LX/D6r;

    iput-object v2, v0, LX/D6p;->b:LX/D6r;

    .line 1966155
    invoke-direct {p0, v1, v1}, LX/D6m;->a(ZZ)V

    .line 1966156
    iget-object v0, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    const-string v1, "Video ad fetcher returns no data model"

    invoke-static {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->j(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)V

    goto :goto_0

    .line 1966157
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel$InstreamVideoAdsModel;->a()LX/0Px;

    move-result-object v0

    .line 1966158
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1966159
    iget-object v0, p0, LX/D6m;->b:LX/D6p;

    sget-object v2, LX/D6r;->FAILED:LX/D6r;

    iput-object v2, v0, LX/D6p;->b:LX/D6r;

    .line 1966160
    invoke-direct {p0, v1, v1}, LX/D6m;->a(ZZ)V

    .line 1966161
    iget-object v0, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    const-string v1, "Video ad fetcher returns no ad"

    invoke-static {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->j(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)V

    goto :goto_0

    .line 1966162
    :cond_4
    iget-object v3, p0, LX/D6m;->b:LX/D6p;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, v3, LX/D6p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1966163
    iget-object v0, p0, LX/D6m;->b:LX/D6p;

    iget-object v0, v0, LX/D6p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_6

    .line 1966164
    iget-object v0, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    const-string v3, "Video ad fetcher returned empty ad"

    invoke-static {v0, v3}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->j(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)V

    .line 1966165
    :goto_1
    iget-object v0, p0, LX/D6m;->b:LX/D6p;

    sget-object v3, LX/D6r;->SUCCESS:LX/D6r;

    iput-object v3, v0, LX/D6p;->b:LX/D6r;

    .line 1966166
    iget-object v0, p0, LX/D6m;->b:LX/D6p;

    iget-object v0, v0, LX/D6p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    invoke-direct {p0, v1, v0}, LX/D6m;->a(ZZ)V

    .line 1966167
    iget-object v0, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->l:Ljava/util/Map;

    iget-object v1, p0, LX/D6m;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1966168
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Gv;

    .line 1966169
    :goto_3
    if-eqz v0, :cond_1

    .line 1966170
    iget v1, p0, LX/D6m;->d:I

    .line 1966171
    iget-object v2, v0, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    if-eqz v2, :cond_5

    iget-object v2, v0, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    iget v2, v2, LX/D6v;->g:I

    if-ne v2, v1, :cond_5

    .line 1966172
    iget-object v2, v0, LX/3Gv;->a:Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    iget-object v2, v2, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;->D:LX/D6v;

    .line 1966173
    iget-object v0, v2, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->WAIT_FOR_ADS:LX/2oN;

    if-ne v0, v1, :cond_5

    .line 1966174
    iget-object v0, v2, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/D6u;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1966175
    iget-object v0, v2, LX/D6v;->m:LX/D6u;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/D6u;->sendEmptyMessage(I)Z

    .line 1966176
    :cond_5
    goto/16 :goto_0

    .line 1966177
    :cond_6
    iget-object v3, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v0, p0, LX/D6m;->b:LX/D6p;

    iget-object v0, v0, LX/D6p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1966178
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1966179
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v4, p0, LX/D6m;->c:Ljava/lang/String;

    iget v5, p0, LX/D6m;->d:I

    .line 1966180
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v6

    .line 1966181
    if-nez v6, :cond_a

    .line 1966182
    :cond_7
    :goto_4
    iget-object v3, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v0, p0, LX/D6m;->b:LX/D6p;

    iget-object v0, v0, LX/D6p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1966183
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1966184
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1966185
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 1966186
    sget-object v5, LX/1Li;->COMMERCIAL_BREAK:LX/1Li;

    .line 1966187
    iget-object v6, v3, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->p:LX/1JD;

    sget-object v7, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v5, v7}, LX/1JD;->a(LX/1Li;Lcom/facebook/common/callercontext/CallerContext;)LX/1ly;

    move-result-object v5

    .line 1966188
    invoke-virtual {v5, v4}, LX/1ly;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/Void;

    .line 1966189
    invoke-virtual {v5}, LX/1ly;->a()V

    .line 1966190
    iget-object v0, p0, LX/D6m;->g:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    const-string v3, "Succeeded to fetch video ad"

    invoke-static {v0, v3}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->j(Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 1966191
    goto :goto_2

    .line 1966192
    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    .line 1966193
    :cond_a
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    .line 1966194
    if-eqz v6, :cond_7

    .line 1966195
    iget-object v7, v3, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->t:LX/04J;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v8

    sget-object p1, LX/0JE;->HOST_VIDEO_ID:LX/0JE;

    iget-object p1, p1, LX/0JE;->value:Ljava/lang/String;

    invoke-virtual {v7, v8, p1, v4}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1966196
    iget-object v7, v3, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->t:LX/04J;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    sget-object v8, LX/0JE;->INSTREAM_VIDEO_AD_BREAK_INDEX:LX/0JE;

    iget-object v8, v8, LX/0JE;->value:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v7, v6, v8, p1}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_4
.end method
