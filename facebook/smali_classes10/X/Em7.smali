.class public LX/Em7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/49e;


# instance fields
.field private a:LX/0Zb;

.field private b:LX/0SG;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2165092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2165093
    iput-object p1, p0, LX/Em7;->a:LX/0Zb;

    .line 2165094
    iput-object p2, p0, LX/Em7;->b:LX/0SG;

    .line 2165095
    return-void
.end method


# virtual methods
.method public final a(LX/0dI;LX/0dI;LX/49d;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2165096
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "phoneid_update"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2165097
    const-string v1, "old_id"

    .line 2165098
    iget-object v2, p1, LX/0dI;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2165099
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "old_ts"

    .line 2165100
    iget-wide v6, p1, LX/0dI;->b:J

    move-wide v4, v6

    .line 2165101
    invoke-virtual {v1, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "new_id"

    .line 2165102
    iget-object v3, p2, LX/0dI;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2165103
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "new_ts"

    .line 2165104
    iget-wide v6, p2, LX/0dI;->b:J

    move-wide v4, v6

    .line 2165105
    invoke-virtual {v1, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "type"

    invoke-virtual {p3}, LX/49d;->type()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2165106
    if-eqz p4, :cond_0

    .line 2165107
    const-string v1, "src_pkg"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2165108
    :cond_0
    iget-object v1, p0, LX/Em7;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2165109
    iput-wide v2, v0, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e:J

    .line 2165110
    iget-object v1, p0, LX/Em7;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2165111
    return-void
.end method
