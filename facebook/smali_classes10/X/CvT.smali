.class public LX/CvT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0gT;


# instance fields
.field private a:LX/CwW;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/CvS;",
            ">;"
        }
    .end annotation
.end field

.field private e:I


# direct methods
.method public constructor <init>(LX/CwW;LX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwW;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1948314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1948315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CvT;->d:Ljava/util/Map;

    .line 1948316
    iput-object p2, p0, LX/CvT;->b:LX/0Px;

    .line 1948317
    iput-object p3, p0, LX/CvT;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1948318
    iput-object p1, p0, LX/CvT;->a:LX/CwW;

    .line 1948319
    iput p4, p0, LX/CvT;->e:I

    .line 1948320
    return-void
.end method


# virtual methods
.method public final serialize(LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1948321
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1948322
    const-string v0, "count"

    iget v1, p0, LX/CvT;->e:I

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(Ljava/lang/String;I)V

    .line 1948323
    const-string v0, "request"

    iget-object v1, p0, LX/CvT;->a:LX/CwW;

    invoke-virtual {v1}, LX/CwW;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1948324
    const-string v0, "role"

    iget-object v1, p0, LX/CvT;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1948325
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1948326
    const/4 v1, 0x1

    .line 1948327
    iget-object v0, p0, LX/CvT;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    iget-object v0, p0, LX/CvT;->b:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1948328
    if-eqz v1, :cond_0

    move v1, v2

    .line 1948329
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1948330
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1948331
    :cond_0
    const/16 v6, 0x2c

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1948332
    :cond_1
    const-string v0, "style"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1948333
    const-string v0, "units"

    invoke-virtual {p1, v0}, LX/0nX;->f(Ljava/lang/String;)V

    .line 1948334
    iget-object v0, p0, LX/CvT;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 1948335
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1948336
    const-string v3, "feedUnit"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1948337
    const-string v0, "views"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1948338
    invoke-virtual {p1}, LX/0nX;->g()V

    goto :goto_2

    .line 1948339
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1948340
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1948341
    return-void
.end method

.method public final serializeWithType(LX/0nX;LX/0my;LX/4qz;)V
    .locals 2

    .prologue
    .line 1948342
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Serialization infrastructure does not support type serialization."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
