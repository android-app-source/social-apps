.class public final LX/El4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ekz;


# instance fields
.field public a:Ljava/lang/String;

.field public final b:Lorg/apache/http/message/HeaderGroup;

.field public c:Ljava/lang/String;

.field public d:LX/El1;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2164011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2164012
    new-instance v0, Lorg/apache/http/message/HeaderGroup;

    invoke-direct {v0}, Lorg/apache/http/message/HeaderGroup;-><init>()V

    iput-object v0, p0, LX/El4;->b:Lorg/apache/http/message/HeaderGroup;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/Ekz;
    .locals 0

    .prologue
    .line 2164013
    iput-object p1, p0, LX/El4;->a:Ljava/lang/String;

    .line 2164014
    return-object p0
.end method

.method public final a(Ljava/lang/String;LX/El1;)LX/Ekz;
    .locals 0
    .param p2    # LX/El1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2164008
    iput-object p1, p0, LX/El4;->c:Ljava/lang/String;

    .line 2164009
    iput-object p2, p0, LX/El4;->d:LX/El1;

    .line 2164010
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/Ekz;
    .locals 2

    .prologue
    .line 2164015
    iget-object v0, p0, LX/El4;->b:Lorg/apache/http/message/HeaderGroup;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    invoke-direct {v1, p1, p2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 2164016
    return-object p0
.end method

.method public final a()LX/El6;
    .locals 2

    .prologue
    .line 2164007
    new-instance v0, LX/El6;

    invoke-direct {v0, p0}, LX/El6;-><init>(LX/El4;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/Ekz;
    .locals 0

    .prologue
    .line 2164003
    iput-object p1, p0, LX/El4;->e:Ljava/lang/String;

    .line 2164004
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)LX/Ekz;
    .locals 2

    .prologue
    .line 2164005
    iget-object v0, p0, LX/El4;->b:Lorg/apache/http/message/HeaderGroup;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    invoke-direct {v1, p1, p2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/message/HeaderGroup;->updateHeader(Lorg/apache/http/Header;)V

    .line 2164006
    return-object p0
.end method
