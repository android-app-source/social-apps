.class public LX/EZ2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EYd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MType:",
        "LX/EWp;",
        "BType:",
        "LX/EWj;",
        "IType::",
        "LX/EWT;",
        ">",
        "Ljava/lang/Object;",
        "LX/EYd;"
    }
.end annotation


# instance fields
.field private a:LX/EYd;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TMType;>;"
        }
    .end annotation
.end field

.field private c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EZ7",
            "<TMType;TBType;TIType;>;>;"
        }
    .end annotation
.end field

.field public e:Z

.field private f:LX/EZ0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ0",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation
.end field

.field private g:LX/EYz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EYz",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation
.end field

.field private h:LX/EZ1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ1",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;ZLX/EYd;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TMType;>;Z",
            "LX/EYd;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2138473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2138474
    iput-object p1, p0, LX/EZ2;->b:Ljava/util/List;

    .line 2138475
    iput-boolean p2, p0, LX/EZ2;->c:Z

    .line 2138476
    iput-object p3, p0, LX/EZ2;->a:LX/EYd;

    .line 2138477
    iput-boolean p4, p0, LX/EZ2;->e:Z

    .line 2138478
    return-void
.end method

.method private a(IZ)LX/EWp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)TMType;"
        }
    .end annotation

    .prologue
    .line 2138466
    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2138467
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWp;

    .line 2138468
    :goto_0
    return-object v0

    .line 2138469
    :cond_0
    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZ7;

    .line 2138470
    if-nez v0, :cond_1

    .line 2138471
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWp;

    goto :goto_0

    .line 2138472
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, LX/EZ7;->c()LX/EWp;

    move-result-object v0

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2138462
    iget-boolean v0, p0, LX/EZ2;->c:Z

    if-nez v0, :cond_0

    .line 2138463
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    .line 2138464
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EZ2;->c:Z

    .line 2138465
    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 2138458
    iget-boolean v0, p0, LX/EZ2;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EZ2;->a:LX/EYd;

    if-eqz v0, :cond_0

    .line 2138459
    iget-object v0, p0, LX/EZ2;->a:LX/EYd;

    invoke-interface {v0}, LX/EYd;->a()V

    .line 2138460
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EZ2;->e:Z

    .line 2138461
    :cond_0
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 2138451
    iget-object v0, p0, LX/EZ2;->f:LX/EZ0;

    if-eqz v0, :cond_0

    .line 2138452
    iget-object v0, p0, LX/EZ2;->f:LX/EZ0;

    invoke-virtual {v0}, LX/EZ0;->a()V

    .line 2138453
    :cond_0
    iget-object v0, p0, LX/EZ2;->g:LX/EYz;

    if-eqz v0, :cond_1

    .line 2138454
    iget-object v0, p0, LX/EZ2;->g:LX/EYz;

    invoke-virtual {v0}, LX/EYz;->a()V

    .line 2138455
    :cond_1
    iget-object v0, p0, LX/EZ2;->h:LX/EZ1;

    if-eqz v0, :cond_2

    .line 2138456
    iget-object v0, p0, LX/EZ2;->h:LX/EZ1;

    invoke-virtual {v0}, LX/EZ1;->a()V

    .line 2138457
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(I)LX/EWp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TMType;"
        }
    .end annotation

    .prologue
    .line 2138450
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EZ2;->a(IZ)LX/EWp;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILX/EWp;)LX/EZ2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITMType;)",
            "LX/EZ2",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation

    .prologue
    .line 2138439
    if-nez p2, :cond_0

    .line 2138440
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2138441
    :cond_0
    invoke-direct {p0}, LX/EZ2;->g()V

    .line 2138442
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2138443
    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2138444
    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZ7;

    .line 2138445
    if-eqz v0, :cond_1

    .line 2138446
    invoke-virtual {v0}, LX/EZ7;->b()V

    .line 2138447
    :cond_1
    invoke-direct {p0}, LX/EZ2;->i()V

    .line 2138448
    invoke-direct {p0}, LX/EZ2;->j()V

    .line 2138449
    return-object p0
.end method

.method public final a(LX/EWp;)LX/EZ2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMType;)",
            "LX/EZ2",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation

    .prologue
    .line 2138430
    if-nez p1, :cond_0

    .line 2138431
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2138432
    :cond_0
    invoke-direct {p0}, LX/EZ2;->g()V

    .line 2138433
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2138434
    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2138435
    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2138436
    :cond_1
    invoke-direct {p0}, LX/EZ2;->i()V

    .line 2138437
    invoke-direct {p0}, LX/EZ2;->j()V

    .line 2138438
    return-object p0
.end method

.method public final a(Ljava/lang/Iterable;)LX/EZ2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TMType;>;)",
            "LX/EZ2",
            "<TMType;TBType;TIType;>;"
        }
    .end annotation

    .prologue
    .line 2138370
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWp;

    .line 2138371
    if-nez v0, :cond_0

    .line 2138372
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2138373
    :cond_1
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 2138374
    check-cast v0, Ljava/util/Collection;

    .line 2138375
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 2138376
    :goto_0
    return-object p0

    .line 2138377
    :cond_2
    invoke-direct {p0}, LX/EZ2;->g()V

    .line 2138378
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWp;

    .line 2138379
    invoke-virtual {p0, v0}, LX/EZ2;->a(LX/EWp;)LX/EZ2;

    goto :goto_1

    .line 2138380
    :cond_3
    invoke-direct {p0}, LX/EZ2;->g()V

    .line 2138381
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWp;

    .line 2138382
    invoke-virtual {p0, v0}, LX/EZ2;->a(LX/EWp;)LX/EZ2;

    goto :goto_2

    .line 2138383
    :cond_4
    invoke-direct {p0}, LX/EZ2;->i()V

    .line 2138384
    invoke-direct {p0}, LX/EZ2;->j()V

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 2138428
    invoke-direct {p0}, LX/EZ2;->i()V

    .line 2138429
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2138426
    const/4 v0, 0x0

    iput-object v0, p0, LX/EZ2;->a:LX/EYd;

    .line 2138427
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2138425
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 2138416
    invoke-direct {p0}, LX/EZ2;->g()V

    .line 2138417
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2138418
    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2138419
    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZ7;

    .line 2138420
    if-eqz v0, :cond_0

    .line 2138421
    invoke-virtual {v0}, LX/EZ7;->b()V

    .line 2138422
    :cond_0
    invoke-direct {p0}, LX/EZ2;->i()V

    .line 2138423
    invoke-direct {p0}, LX/EZ2;->j()V

    .line 2138424
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2138415
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2138405
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    .line 2138406
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EZ2;->c:Z

    .line 2138407
    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2138408
    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EZ7;

    .line 2138409
    if-eqz v0, :cond_0

    .line 2138410
    invoke-virtual {v0}, LX/EZ7;->b()V

    goto :goto_0

    .line 2138411
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    .line 2138412
    :cond_2
    invoke-direct {p0}, LX/EZ2;->i()V

    .line 2138413
    invoke-direct {p0}, LX/EZ2;->j()V

    .line 2138414
    return-void
.end method

.method public final f()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TMType;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2138385
    iput-boolean v4, p0, LX/EZ2;->e:Z

    .line 2138386
    iget-boolean v0, p0, LX/EZ2;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EZ2;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2138387
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    .line 2138388
    :goto_0
    return-object v0

    .line 2138389
    :cond_0
    iget-boolean v0, p0, LX/EZ2;->c:Z

    if-nez v0, :cond_2

    move v2, v3

    .line 2138390
    :goto_1
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 2138391
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    .line 2138392
    iget-object v1, p0, LX/EZ2;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EZ7;

    .line 2138393
    if-eqz v1, :cond_1

    .line 2138394
    invoke-virtual {v1}, LX/EZ7;->d()LX/EWp;

    move-result-object v1

    if-eq v1, v0, :cond_1

    move v0, v3

    .line 2138395
    :goto_2
    if-eqz v0, :cond_2

    .line 2138396
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    goto :goto_0

    .line 2138397
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2138398
    :cond_2
    invoke-direct {p0}, LX/EZ2;->g()V

    move v0, v3

    .line 2138399
    :goto_3
    iget-object v1, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 2138400
    iget-object v1, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-direct {p0, v0, v4}, LX/EZ2;->a(IZ)LX/EWp;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2138401
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2138402
    :cond_3
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    .line 2138403
    iput-boolean v3, p0, LX/EZ2;->c:Z

    .line 2138404
    iget-object v0, p0, LX/EZ2;->b:Ljava/util/List;

    goto :goto_0

    :cond_4
    move v0, v4

    goto :goto_2
.end method
