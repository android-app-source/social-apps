.class public LX/DBb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0jU;

.field public final b:LX/0lC;


# direct methods
.method public constructor <init>(LX/0jU;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1973063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1973064
    iput-object p1, p0, LX/DBb;->a:LX/0jU;

    .line 1973065
    iput-object p2, p0, LX/DBb;->b:LX/0lC;

    .line 1973066
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 1973067
    const-string v0, "serialized"

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1973068
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move-object v1, v4

    .line 1973069
    :goto_0
    move-object v0, v1

    .line 1973070
    if-eqz v0, :cond_1

    .line 1973071
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1973072
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https://our.intern.facebook.com/intern/feedtools/serialized_to_entstory.php?zombie_story="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1973073
    :goto_1
    return-object v0

    .line 1973074
    :catch_0
    const-string v1, "Failed to encode into UTF-8: %s"

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1973075
    :cond_1
    const-string v0, "Failed to get value for key \"%s\": %s"

    const-string v1, "serialized"

    invoke-static {v0, v1, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1973076
    :cond_2
    sget-object v1, LX/6VJ;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v1, v4

    .line 1973077
    goto :goto_0

    .line 1973078
    :cond_3
    const-string v1, "[\\{\\}\"]"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1973079
    const/16 v2, 0x2c

    invoke-static {v2}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    move v2, v3

    .line 1973080
    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_5

    .line 1973081
    const/16 v1, 0x3a

    invoke-static {v1}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v6

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v6, v1}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    .line 1973082
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v7, 0x2

    if-ne v1, v7, :cond_4

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1973083
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0

    .line 1973084
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_5
    move-object v1, v4

    .line 1973085
    goto/16 :goto_0
.end method
