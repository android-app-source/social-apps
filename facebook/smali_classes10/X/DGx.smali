.class public final LX/DGx;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$DefaultSavableObjectExtraFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1Pv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    .line 1981044
    iput-object p1, p0, LX/DGx;->a:Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    invoke-direct {p0}, LX/2h1;-><init>()V

    .line 1981045
    iput-object p2, p0, LX/DGx;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981046
    iput-object p3, p0, LX/DGx;->c:LX/1Pv;

    .line 1981047
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1981048
    iget-object v0, p0, LX/DGx;->a:Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->l:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->a:Ljava/lang/String;

    const-string v2, "Failed to save / unsave link"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1981049
    iget-object v0, p0, LX/DGx;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981050
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1981051
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0sa;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f08185b

    .line 1981052
    :goto_0
    iget-object v1, p0, LX/DGx;->a:Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    iget-object v1, v1, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->m:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1981053
    return-void

    .line 1981054
    :cond_0
    const v0, 0x7f08185a

    goto :goto_0
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1981055
    return-void
.end method
