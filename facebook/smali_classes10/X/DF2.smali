.class public LX/DF2;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DF0;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DF3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1977625
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DF2;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DF3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1977622
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1977623
    iput-object p1, p0, LX/DF2;->b:LX/0Ot;

    .line 1977624
    return-void
.end method

.method public static a(LX/0QB;)LX/DF2;
    .locals 4

    .prologue
    .line 1977588
    const-class v1, LX/DF2;

    monitor-enter v1

    .line 1977589
    :try_start_0
    sget-object v0, LX/DF2;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1977590
    sput-object v2, LX/DF2;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1977591
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977592
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1977593
    new-instance v3, LX/DF2;

    const/16 p0, 0x20d9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DF2;-><init>(LX/0Ot;)V

    .line 1977594
    move-object v0, v3

    .line 1977595
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1977596
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DF2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1977597
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1977598
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1977607
    iget-object v0, p0, LX/DF2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DF3;

    const/4 v3, 0x2

    .line 1977608
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b0f68

    invoke-interface {v1, v2}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b0f67

    invoke-interface {v1, v2}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f020a3d

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    const/16 v2, 0x8

    invoke-interface {v1, v2, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0f69

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    .line 1977609
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    const v4, 0x7f020b82

    invoke-virtual {v3, v4}, LX/1o5;->h(I)LX/1o5;

    move-result-object v3

    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/16 v4, 0x68

    invoke-interface {v3, v4}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 1977610
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 p2, 0x1

    .line 1977611
    iget-object v3, v0, LX/DF3;->d:Landroid/content/Context;

    const v4, 0x7f08186e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v3, v0, LX/DF3;->g:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    invoke-virtual {v3}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1977612
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x6

    const/16 p0, 0xc

    invoke-interface {v4, v5, p0}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x20

    invoke-interface {v4, p2, v5}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    const/16 p0, 0x14

    invoke-interface {v4, v5, p0}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0a010c

    invoke-virtual {v3, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b0052

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    iget-object v5, v0, LX/DF3;->b:Landroid/graphics/Typeface;

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v3

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    move-object v3, v3

    .line 1977613
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 1977614
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x6

    const/16 v5, 0x8

    invoke-interface {v3, v4, v5}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f08186c

    invoke-virtual {v4, v5}, LX/1ne;->h(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a010e

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0050

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    iget-object v5, v0, LX/DF3;->c:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    move-object v3, v3

    .line 1977615
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const/4 v5, 0x2

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 p0, 0x1

    .line 1977616
    iget-object v2, v0, LX/DF3;->a:LX/23P;

    const v3, 0x7f08186d

    invoke-virtual {p1, v3}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1977617
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0f6a

    invoke-interface {v3, v4}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0a00d5

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    .line 1977618
    const v4, 0x4915b7a9

    const/4 v0, 0x0

    invoke-static {p1, v4, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1977619
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x6

    const/16 v5, 0xc

    invoke-interface {v3, v4, v5}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v4, 0x7f0a00d5

    invoke-virtual {v2, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const v4, 0x7f0b004e

    invoke-virtual {v2, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    sget-object v4, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v2, v4}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v2

    const/16 v4, 0x28

    invoke-interface {v2, v4}, LX/1Di;->r(I)LX/1Di;

    move-result-object v2

    const v4, 0x7f020b08

    invoke-interface {v2, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    move-object v2, v2

    .line 1977620
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1977621
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1977599
    invoke-static {}, LX/1dS;->b()V

    .line 1977600
    iget v0, p1, LX/1dQ;->b:I

    .line 1977601
    packed-switch v0, :pswitch_data_0

    .line 1977602
    :goto_0
    return-object v2

    .line 1977603
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1977604
    iget-object p1, p0, LX/DF2;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DF3;

    .line 1977605
    iget-object p2, p1, LX/DF3;->h:LX/2e1;

    iget-object p0, p1, LX/DF3;->f:LX/17Y;

    iget-object v0, p1, LX/DF3;->d:Landroid/content/Context;

    iget-object v1, p1, LX/DF3;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-static {p2, p0, v0, v1}, Lcom/facebook/feedplugins/pymk/rows/CCUPromoCardPartDefinition;->a(LX/2e1;LX/17Y;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 1977606
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4915b7a9
        :pswitch_0
    .end packed-switch
.end method
