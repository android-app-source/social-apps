.class public LX/DN3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/DN3;


# instance fields
.field private final a:LX/0Uh;

.field public final b:LX/0ad;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0ad;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1990849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1990850
    iput-object p1, p0, LX/DN3;->a:LX/0Uh;

    .line 1990851
    iput-object p2, p0, LX/DN3;->b:LX/0ad;

    .line 1990852
    iput-object p3, p0, LX/DN3;->c:LX/0Or;

    .line 1990853
    return-void
.end method

.method public static a(LX/0QB;)LX/DN3;
    .locals 6

    .prologue
    .line 1990836
    sget-object v0, LX/DN3;->d:LX/DN3;

    if-nez v0, :cond_1

    .line 1990837
    const-class v1, LX/DN3;

    monitor-enter v1

    .line 1990838
    :try_start_0
    sget-object v0, LX/DN3;->d:LX/DN3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1990839
    if-eqz v2, :cond_0

    .line 1990840
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1990841
    new-instance v5, LX/DN3;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const/16 p0, 0x148e

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/DN3;-><init>(LX/0Uh;LX/0ad;LX/0Or;)V

    .line 1990842
    move-object v0, v5

    .line 1990843
    sput-object v0, LX/DN3;->d:LX/DN3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1990844
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1990845
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1990846
    :cond_1
    sget-object v0, LX/DN3;->d:LX/DN3;

    return-object v0

    .line 1990847
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1990848
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1990835
    iget-object v0, p0, LX/DN3;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DN3;->a:LX/0Uh;

    const/16 v2, 0x3c4

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DN3;->b:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/DN2;->c:S

    invoke-interface {v0, v2, v3, v1}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1990834
    iget-object v0, p0, LX/DN3;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1990833
    invoke-virtual {p0}, LX/DN3;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/DN3;->b:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/DN2;->b:S

    invoke-interface {v1, v2, v3, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
