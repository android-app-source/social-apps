.class public LX/DwK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/DwK;


# instance fields
.field public final a:LX/11i;


# direct methods
.method public constructor <init>(LX/11i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2060300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2060301
    iput-object p1, p0, LX/DwK;->a:LX/11i;

    .line 2060302
    return-void
.end method

.method public static a(LX/0QB;)LX/DwK;
    .locals 4

    .prologue
    .line 2060311
    sget-object v0, LX/DwK;->b:LX/DwK;

    if-nez v0, :cond_1

    .line 2060312
    const-class v1, LX/DwK;

    monitor-enter v1

    .line 2060313
    :try_start_0
    sget-object v0, LX/DwK;->b:LX/DwK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2060314
    if-eqz v2, :cond_0

    .line 2060315
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2060316
    new-instance p0, LX/DwK;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-direct {p0, v3}, LX/DwK;-><init>(LX/11i;)V

    .line 2060317
    move-object v0, p0

    .line 2060318
    sput-object v0, LX/DwK;->b:LX/DwK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2060319
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2060320
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2060321
    :cond_1
    sget-object v0, LX/DwK;->b:LX/DwK;

    return-object v0

    .line 2060322
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2060323
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2060324
    iget-object v0, p0, LX/DwK;->a:LX/11i;

    sget-object v1, LX/DwM;->a:LX/DwL;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2060325
    if-eqz v0, :cond_0

    .line 2060326
    const v1, 0x1583f93e

    invoke-static {v0, p1, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2060327
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2060307
    iget-object v0, p0, LX/DwK;->a:LX/11i;

    sget-object v1, LX/DwM;->a:LX/DwL;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2060308
    if-eqz v0, :cond_0

    .line 2060309
    const/4 v1, 0x0

    invoke-static {p2, p3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    const v3, 0x7999852

    invoke-static {v0, p1, v1, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2060310
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2060303
    iget-object v0, p0, LX/DwK;->a:LX/11i;

    sget-object v1, LX/DwM;->a:LX/DwL;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2060304
    if-eqz v0, :cond_0

    .line 2060305
    const v1, 0x1a8686bf

    invoke-static {v0, p1, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2060306
    :cond_0
    return-void
.end method
