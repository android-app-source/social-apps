.class public final enum LX/Dlk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dlk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dlk;

.field public static final enum PAST:LX/Dlk;

.field public static final enum UPCOMING:LX/Dlk;


# instance fields
.field public final tabTextString:I

.field public final tabTextViewId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2038555
    new-instance v0, LX/Dlk;

    const-string v1, "UPCOMING"

    const v2, 0x7f0d0597

    const v3, 0x7f082bc6

    invoke-direct {v0, v1, v4, v2, v3}, LX/Dlk;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/Dlk;->UPCOMING:LX/Dlk;

    .line 2038556
    new-instance v0, LX/Dlk;

    const-string v1, "PAST"

    const v2, 0x7f0d0598

    const v3, 0x7f082bc7

    invoke-direct {v0, v1, v5, v2, v3}, LX/Dlk;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/Dlk;->PAST:LX/Dlk;

    .line 2038557
    const/4 v0, 0x2

    new-array v0, v0, [LX/Dlk;

    sget-object v1, LX/Dlk;->UPCOMING:LX/Dlk;

    aput-object v1, v0, v4

    sget-object v1, LX/Dlk;->PAST:LX/Dlk;

    aput-object v1, v0, v5

    sput-object v0, LX/Dlk;->$VALUES:[LX/Dlk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2038558
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2038559
    iput p3, p0, LX/Dlk;->tabTextViewId:I

    .line 2038560
    iput p4, p0, LX/Dlk;->tabTextString:I

    .line 2038561
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dlk;
    .locals 1

    .prologue
    .line 2038562
    const-class v0, LX/Dlk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dlk;

    return-object v0
.end method

.method public static values()[LX/Dlk;
    .locals 1

    .prologue
    .line 2038563
    sget-object v0, LX/Dlk;->$VALUES:[LX/Dlk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dlk;

    return-object v0
.end method
