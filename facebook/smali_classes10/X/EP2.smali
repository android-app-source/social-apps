.class public final LX/EP2;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

.field public final synthetic b:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;)V
    .locals 0

    .prologue
    .line 2115707
    iput-object p1, p0, LX/EP2;->b:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;

    iput-object p2, p0, LX/EP2;->a:Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2115704
    iget-object v0, p0, LX/EP2;->a:Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

    .line 2115705
    iget-object p0, v0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-object v0, p0

    .line 2115706
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;->a()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final a(LX/2eI;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2115708
    iget-object v0, p0, LX/EP2;->b:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;

    new-instance v1, LX/EP3;

    iget-object v2, p0, LX/EP2;->a:Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

    .line 2115709
    iget v3, v2, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->b:I

    move v2, v3

    .line 2115710
    invoke-direct {v1, v2}, LX/EP3;-><init>(I)V

    invoke-virtual {p1, v0, v1}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2115711
    iget-object v0, p0, LX/EP2;->a:Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

    .line 2115712
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-object v0, v1

    .line 2115713
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;->a()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;->a()LX/0Px;

    move-result-object v2

    .line 2115714
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsEdge;

    .line 2115715
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItem;

    move-result-object v0

    .line 2115716
    iget-object v4, p0, LX/EP2;->b:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;

    iget-object v4, v4, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;

    new-instance v5, LX/EP3;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItem;->a()I

    move-result v7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItem;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-direct {v5, v6, v7, v0}, LX/EP3;-><init>(Ljava/lang/String;ILcom/facebook/graphql/model/GraphQLImage;)V

    invoke-virtual {p1, v4, v5}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2115717
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2115718
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 2115703
    return-void
.end method
