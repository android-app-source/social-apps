.class public final enum LX/ClI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ClI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ClI;

.field public static final enum LOAD_FINISH:LX/ClI;

.field public static final enum LOAD_START:LX/ClI;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1932277
    new-instance v0, LX/ClI;

    const-string v1, "LOAD_START"

    invoke-direct {v0, v1, v2}, LX/ClI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClI;->LOAD_START:LX/ClI;

    .line 1932278
    new-instance v0, LX/ClI;

    const-string v1, "LOAD_FINISH"

    invoke-direct {v0, v1, v3}, LX/ClI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClI;->LOAD_FINISH:LX/ClI;

    .line 1932279
    const/4 v0, 0x2

    new-array v0, v0, [LX/ClI;

    sget-object v1, LX/ClI;->LOAD_START:LX/ClI;

    aput-object v1, v0, v2

    sget-object v1, LX/ClI;->LOAD_FINISH:LX/ClI;

    aput-object v1, v0, v3

    sput-object v0, LX/ClI;->$VALUES:[LX/ClI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1932276
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ClI;
    .locals 1

    .prologue
    .line 1932274
    const-class v0, LX/ClI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ClI;

    return-object v0
.end method

.method public static values()[LX/ClI;
    .locals 1

    .prologue
    .line 1932275
    sget-object v0, LX/ClI;->$VALUES:[LX/ClI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ClI;

    return-object v0
.end method
