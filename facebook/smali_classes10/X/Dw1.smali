.class public final LX/Dw1;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2059532
    iput-object p1, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iput-object p2, p0, LX/Dw1;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 5
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2059539
    const/16 v4, 0xc

    .line 2059540
    iget-object v0, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->q:Z

    if-eqz v0, :cond_0

    .line 2059541
    iget-object v0, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c:LX/DwK;

    const-string v1, "LoadImageThumbnail"

    const-string v2, "ExtraImageThumbnailType"

    iget-object v3, p0, LX/Dw1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/DwK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2059542
    :cond_0
    iget-object v0, p0, LX/Dw1;->a:Ljava/lang/String;

    const-string v1, "LoadLandscapeImageThumbnail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Dw1;->a:Ljava/lang/String;

    const-string v1, "LoadPortraitImageThumbnail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2059543
    :cond_1
    const/4 v0, 0x6

    .line 2059544
    :goto_0
    iget-object v1, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-object v1, v1, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->d:LX/DwJ;

    iget-object v2, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-object v2, v2, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->p:Ljava/lang/String;

    .line 2059545
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2059546
    :cond_2
    :goto_1
    iget-object v1, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-object v1, v1, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->d:LX/DwJ;

    iget-object v2, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-object v2, v2, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->p:Ljava/lang/String;

    const/4 v3, -0x1

    .line 2059547
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_a

    .line 2059548
    :cond_3
    :goto_2
    move v1, v3

    .line 2059549
    if-lt v1, v4, :cond_4

    sub-int v0, v1, v0

    if-ge v0, v4, :cond_4

    .line 2059550
    iget-object v0, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->q:Z

    if-eqz v0, :cond_4

    .line 2059551
    iget-object v0, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c:LX/DwK;

    const-string v1, "LoadScreenImages"

    const-string v2, "ExtraLoadScreenImagesSource"

    iget-object v3, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-object v3, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/DwK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2059552
    iget-object v0, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c:LX/DwK;

    .line 2059553
    iget-object v1, v0, LX/DwK;->a:LX/11i;

    sget-object v2, LX/DwM;->a:LX/DwL;

    invoke-interface {v1, v2}, LX/11i;->b(LX/0Pq;)V

    .line 2059554
    :cond_4
    return-void

    .line 2059555
    :cond_5
    iget-object v0, p0, LX/Dw1;->a:Ljava/lang/String;

    const-string v1, "LoadSquareImageThumbnail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2059556
    const/4 v0, 0x4

    goto :goto_0

    .line 2059557
    :cond_6
    const/4 v0, 0x1

    goto :goto_0

    .line 2059558
    :cond_7
    const-string v3, "LoadScreenImagesPhotosOf"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2059559
    iget v3, v1, LX/DwJ;->a:I

    add-int/2addr v3, v0

    iput v3, v1, LX/DwJ;->a:I

    .line 2059560
    :cond_8
    const-string v3, "LoadScreenImagesUploads"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2059561
    iget v3, v1, LX/DwJ;->b:I

    add-int/2addr v3, v0

    iput v3, v1, LX/DwJ;->b:I

    .line 2059562
    :cond_9
    const-string v3, "LoadScreenImagesAlbum"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2059563
    iget v3, v1, LX/DwJ;->c:I

    add-int/2addr v3, v0

    iput v3, v1, LX/DwJ;->c:I

    goto :goto_1

    .line 2059564
    :cond_a
    const-string p1, "LoadScreenImagesPhotosOf"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_b

    .line 2059565
    iget v3, v1, LX/DwJ;->a:I

    goto :goto_2

    .line 2059566
    :cond_b
    const-string p1, "LoadScreenImagesUploads"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_c

    .line 2059567
    iget v3, v1, LX/DwJ;->b:I

    goto :goto_2

    .line 2059568
    :cond_c
    const-string p1, "LoadScreenImagesAlbum"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2059569
    iget v3, v1, LX/DwJ;->c:I

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2059533
    iget-object v0, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-boolean v0, v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->q:Z

    if-eqz v0, :cond_0

    .line 2059534
    iget-object v0, p0, LX/Dw1;->b:Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c:LX/DwK;

    const-string v1, "LoadImageThumbnail"

    iget-object v2, p0, LX/Dw1;->a:Ljava/lang/String;

    .line 2059535
    iget-object v3, v0, LX/DwK;->a:LX/11i;

    sget-object p0, LX/DwM;->a:LX/DwL;

    invoke-interface {v3, p0}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v3

    .line 2059536
    if-eqz v3, :cond_0

    .line 2059537
    const/4 p0, 0x0

    const-string p1, "ExtraImageThumbnailType"

    invoke-static {p1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object p1

    const p2, -0x1053e0d9

    invoke-static {v3, v1, p0, p1, p2}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2059538
    :cond_0
    return-void
.end method
