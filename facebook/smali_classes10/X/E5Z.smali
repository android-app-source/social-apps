.class public LX/E5Z;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E5a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078615
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5Z;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E5a;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078616
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078617
    iput-object p1, p0, LX/E5Z;->b:LX/0Ot;

    .line 2078618
    return-void
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 8

    .prologue
    .line 2078619
    check-cast p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;

    .line 2078620
    iget-object v0, p0, LX/E5Z;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E5a;

    iget-object v2, p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->b:LX/1U8;

    iget-object v3, p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->d:LX/2km;

    iget-object v4, p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->e:LX/1Pn;

    iget-object v5, p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->f:[J

    check-cast v5, [J

    iget-object v6, p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->g:Ljava/lang/String;

    iget-object v7, p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->h:Ljava/lang/String;

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, LX/E5a;->onClick(Landroid/view/View;LX/1U8;LX/2km;LX/1Pn;[JLjava/lang/String;Ljava/lang/String;)V

    .line 2078621
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2078622
    const v0, -0x1f9f723f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2078623
    check-cast p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;

    .line 2078624
    iget-object v0, p0, LX/E5Z;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E5a;

    iget-object v1, p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v2, p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->b:LX/1U8;

    iget-boolean v3, p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/binders/ReactionPhotoComponent$ReactionPhotoComponentImpl;->c:Z

    .line 2078625
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2078626
    invoke-interface {v2}, LX/1U8;->e()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->c()I

    move-result v5

    int-to-float v5, v5

    invoke-interface {v2}, LX/1U8;->e()LX/1Fb;

    move-result-object p0

    invoke-interface {p0}, LX/1Fb;->a()I

    move-result p0

    int-to-float p0, p0

    div-float/2addr v5, p0

    const/high16 p0, 0x3fc00000    # 1.5f

    invoke-static {v5, p0}, Ljava/lang/Math;->min(FF)F

    move-result p0

    .line 2078627
    if-eqz v3, :cond_0

    .line 2078628
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    move v5, v4

    .line 2078629
    :goto_0
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/1up;->c(F)LX/1up;

    move-result-object v4

    sget-object p0, LX/1Up;->g:LX/1Up;

    invoke-virtual {v4, p0}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object p0

    iget-object v4, v0, LX/E5a;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-interface {v2}, LX/1U8;->e()LX/1Fb;

    move-result-object p2

    invoke-interface {p2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {v4, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 2078630
    const p0, -0x1f9f723f

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2078631
    invoke-interface {v4, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Di;->g(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2078632
    return-object v0

    .line 2078633
    :cond_0
    invoke-interface {v2}, LX/1U8;->e()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->c()I

    move-result v5

    const p2, 0x7f0b1642

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v5, v4

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2078634
    invoke-static {}, LX/1dS;->b()V

    .line 2078635
    iget v0, p1, LX/1dQ;->b:I

    .line 2078636
    packed-switch v0, :pswitch_data_0

    .line 2078637
    :goto_0
    return-object v2

    .line 2078638
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2078639
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/E5Z;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1f9f723f
        :pswitch_0
    .end packed-switch
.end method
