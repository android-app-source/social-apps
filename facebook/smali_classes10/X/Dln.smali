.class public LX/Dln;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0wM;

.field public final c:Landroid/view/View;

.field public final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0wM;Landroid/view/View;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0
    .param p3    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/drawee/fbpipeline/FbDraweeView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/widget/TextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/widget/TextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2038670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2038671
    iput-object p1, p0, LX/Dln;->a:Landroid/content/Context;

    .line 2038672
    iput-object p2, p0, LX/Dln;->b:LX/0wM;

    .line 2038673
    iput-object p3, p0, LX/Dln;->c:Landroid/view/View;

    .line 2038674
    iput-object p4, p0, LX/Dln;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2038675
    iput-object p5, p0, LX/Dln;->e:Landroid/widget/TextView;

    .line 2038676
    iput-object p6, p0, LX/Dln;->f:Landroid/widget/TextView;

    .line 2038677
    return-void
.end method
