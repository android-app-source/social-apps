.class public LX/EJ9;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EJ7;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2103325
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EJ9;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103326
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2103327
    iput-object p1, p0, LX/EJ9;->b:LX/0Ot;

    .line 2103328
    return-void
.end method

.method public static a(LX/0QB;)LX/EJ9;
    .locals 4

    .prologue
    .line 2103329
    const-class v1, LX/EJ9;

    monitor-enter v1

    .line 2103330
    :try_start_0
    sget-object v0, LX/EJ9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103331
    sput-object v2, LX/EJ9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103332
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103333
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103334
    new-instance v3, LX/EJ9;

    const/16 p0, 0x3382

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EJ9;-><init>(LX/0Ot;)V

    .line 2103335
    move-object v0, v3

    .line 2103336
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103337
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EJ9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103338
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103339
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2103340
    check-cast p2, LX/EJ8;

    .line 2103341
    iget-object v0, p0, LX/EJ9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;

    iget-object v1, p2, LX/EJ8;->a:LX/CzL;

    .line 2103342
    iget-object v2, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 2103343
    check-cast v2, LX/8cw;

    .line 2103344
    invoke-static {v2}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;->a(LX/8cw;)Landroid/net/Uri;

    move-result-object v3

    .line 2103345
    if-nez v3, :cond_0

    .line 2103346
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 2103347
    :goto_0
    move-object v0, v2

    .line 2103348
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x4

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p0

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p2

    iget-object v2, v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2, v3}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v2

    sget-object v3, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    sget-object v3, LX/1Up;->g:LX/1Up;

    invoke-virtual {v2, v3}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b171e

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-interface {p0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2103349
    invoke-static {}, LX/1dS;->b()V

    .line 2103350
    const/4 v0, 0x0

    return-object v0
.end method
