.class public LX/DFo;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1978806
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DFo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1978807
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1978804
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/DFo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V

    .line 1978805
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V
    .locals 3

    .prologue
    .line 1978795
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1978796
    const v0, 0x7f0310b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1978797
    const v0, 0x7f0d27ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1978798
    const v1, 0x7f0310b2

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1978799
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1978800
    const v0, 0x7f0d27af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1978801
    const v1, 0x7f081868

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1978802
    invoke-virtual {p0}, LX/DFo;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081867

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1978803
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1978786
    iget-boolean v0, p0, LX/DFo;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x33949402

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978791
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1978792
    const/4 v1, 0x1

    .line 1978793
    iput-boolean v1, p0, LX/DFo;->a:Z

    .line 1978794
    const/16 v1, 0x2d

    const v2, -0x6bf533d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0xae52fb3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978787
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1978788
    const/4 v1, 0x0

    .line 1978789
    iput-boolean v1, p0, LX/DFo;->a:Z

    .line 1978790
    const/16 v1, 0x2d

    const v2, 0x7373e261

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
