.class public final LX/EWd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[B

.field public b:I

.field private c:I

.field public d:I

.field private final e:Ljava/io/InputStream;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2130490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2130491
    const v0, 0x7fffffff

    iput v0, p0, LX/EWd;->h:I

    .line 2130492
    const/16 v0, 0x40

    iput v0, p0, LX/EWd;->j:I

    .line 2130493
    const/high16 v0, 0x4000000

    iput v0, p0, LX/EWd;->k:I

    .line 2130494
    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, LX/EWd;->a:[B

    .line 2130495
    iput v1, p0, LX/EWd;->b:I

    .line 2130496
    iput v1, p0, LX/EWd;->d:I

    .line 2130497
    iput v1, p0, LX/EWd;->g:I

    .line 2130498
    iput-object p1, p0, LX/EWd;->e:Ljava/io/InputStream;

    .line 2130499
    return-void
.end method

.method private constructor <init>([BII)V
    .locals 1

    .prologue
    .line 2130500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2130501
    const v0, 0x7fffffff

    iput v0, p0, LX/EWd;->h:I

    .line 2130502
    const/16 v0, 0x40

    iput v0, p0, LX/EWd;->j:I

    .line 2130503
    const/high16 v0, 0x4000000

    iput v0, p0, LX/EWd;->k:I

    .line 2130504
    iput-object p1, p0, LX/EWd;->a:[B

    .line 2130505
    add-int v0, p2, p3

    iput v0, p0, LX/EWd;->b:I

    .line 2130506
    iput p2, p0, LX/EWd;->d:I

    .line 2130507
    neg-int v0, p2

    iput v0, p0, LX/EWd;->g:I

    .line 2130508
    const/4 v0, 0x0

    iput-object v0, p0, LX/EWd;->e:Ljava/io/InputStream;

    .line 2130509
    return-void
.end method

.method public static a([BII)LX/EWd;
    .locals 2

    .prologue
    .line 2130510
    new-instance v0, LX/EWd;

    invoke-direct {v0, p0, p1, p2}, LX/EWd;-><init>([BII)V

    .line 2130511
    :try_start_0
    invoke-virtual {v0, p2}, LX/EWd;->c(I)I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0

    .line 2130512
    return-object v0

    .line 2130513
    :catch_0
    move-exception v0

    .line 2130514
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(LX/EWd;Z)Z
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 2130515
    iget v0, p0, LX/EWd;->d:I

    iget v3, p0, LX/EWd;->b:I

    if-ge v0, v3, :cond_0

    .line 2130516
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "refillBuffer() called when buffer wasn\'t empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2130517
    :cond_0
    iget v0, p0, LX/EWd;->g:I

    iget v3, p0, LX/EWd;->b:I

    add-int/2addr v0, v3

    iget v3, p0, LX/EWd;->h:I

    if-ne v0, v3, :cond_2

    .line 2130518
    if-eqz p1, :cond_1

    .line 2130519
    invoke-static {}, LX/EYr;->b()LX/EYr;

    move-result-object v0

    throw v0

    :cond_1
    move v0, v2

    .line 2130520
    :goto_0
    return v0

    .line 2130521
    :cond_2
    iget v0, p0, LX/EWd;->g:I

    iget v3, p0, LX/EWd;->b:I

    add-int/2addr v0, v3

    iput v0, p0, LX/EWd;->g:I

    .line 2130522
    iput v2, p0, LX/EWd;->d:I

    .line 2130523
    iget-object v0, p0, LX/EWd;->e:Ljava/io/InputStream;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iput v0, p0, LX/EWd;->b:I

    .line 2130524
    iget v0, p0, LX/EWd;->b:I

    if-eqz v0, :cond_3

    iget v0, p0, LX/EWd;->b:I

    if-ge v0, v1, :cond_5

    .line 2130525
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InputStream#read(byte[]) returned invalid result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/EWd;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nThe InputStream implementation is buggy."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2130526
    :cond_4
    iget-object v0, p0, LX/EWd;->e:Ljava/io/InputStream;

    iget-object v3, p0, LX/EWd;->a:[B

    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v0

    goto :goto_1

    .line 2130527
    :cond_5
    iget v0, p0, LX/EWd;->b:I

    if-ne v0, v1, :cond_7

    .line 2130528
    iput v2, p0, LX/EWd;->b:I

    .line 2130529
    if-eqz p1, :cond_6

    .line 2130530
    invoke-static {}, LX/EYr;->b()LX/EYr;

    move-result-object v0

    throw v0

    :cond_6
    move v0, v2

    .line 2130531
    goto :goto_0

    .line 2130532
    :cond_7
    invoke-direct {p0}, LX/EWd;->x()V

    .line 2130533
    iget v0, p0, LX/EWd;->g:I

    iget v1, p0, LX/EWd;->b:I

    add-int/2addr v0, v1

    iget v1, p0, LX/EWd;->c:I

    add-int/2addr v0, v1

    .line 2130534
    iget v1, p0, LX/EWd;->k:I

    if-gt v0, v1, :cond_8

    if-gez v0, :cond_9

    .line 2130535
    :cond_8
    new-instance v0, LX/EYr;

    const-string v1, "Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit."

    invoke-direct {v0, v1}, LX/EYr;-><init>(Ljava/lang/String;)V

    move-object v0, v0

    .line 2130536
    throw v0

    .line 2130537
    :cond_9
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static f(LX/EWd;I)[B
    .locals 11

    .prologue
    const/16 v10, 0x1000

    const/4 v5, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 2130538
    if-gez p1, :cond_0

    .line 2130539
    invoke-static {}, LX/EYr;->c()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130540
    :cond_0
    iget v0, p0, LX/EWd;->g:I

    iget v2, p0, LX/EWd;->d:I

    add-int/2addr v0, v2

    add-int/2addr v0, p1

    iget v2, p0, LX/EWd;->h:I

    if-le v0, v2, :cond_1

    .line 2130541
    iget v0, p0, LX/EWd;->h:I

    iget v1, p0, LX/EWd;->g:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/EWd;->d:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, LX/EWd;->g(I)V

    .line 2130542
    invoke-static {}, LX/EYr;->b()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130543
    :cond_1
    iget v0, p0, LX/EWd;->b:I

    iget v2, p0, LX/EWd;->d:I

    sub-int/2addr v0, v2

    if-gt p1, v0, :cond_2

    .line 2130544
    new-array v0, p1, [B

    .line 2130545
    iget-object v2, p0, LX/EWd;->a:[B

    iget v3, p0, LX/EWd;->d:I

    invoke-static {v2, v3, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2130546
    iget v1, p0, LX/EWd;->d:I

    add-int/2addr v1, p1

    iput v1, p0, LX/EWd;->d:I

    .line 2130547
    :goto_0
    return-object v0

    .line 2130548
    :cond_2
    if-ge p1, v10, :cond_4

    .line 2130549
    new-array v2, p1, [B

    .line 2130550
    iget v0, p0, LX/EWd;->b:I

    iget v3, p0, LX/EWd;->d:I

    sub-int/2addr v0, v3

    .line 2130551
    iget-object v3, p0, LX/EWd;->a:[B

    iget v4, p0, LX/EWd;->d:I

    invoke-static {v3, v4, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2130552
    iget v3, p0, LX/EWd;->b:I

    iput v3, p0, LX/EWd;->d:I

    .line 2130553
    invoke-static {p0, v5}, LX/EWd;->a(LX/EWd;Z)Z

    .line 2130554
    :goto_1
    sub-int v3, p1, v0

    iget v4, p0, LX/EWd;->b:I

    if-le v3, v4, :cond_3

    .line 2130555
    iget-object v3, p0, LX/EWd;->a:[B

    iget v4, p0, LX/EWd;->b:I

    invoke-static {v3, v1, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2130556
    iget v3, p0, LX/EWd;->b:I

    add-int/2addr v0, v3

    .line 2130557
    iget v3, p0, LX/EWd;->b:I

    iput v3, p0, LX/EWd;->d:I

    .line 2130558
    invoke-static {p0, v5}, LX/EWd;->a(LX/EWd;Z)Z

    goto :goto_1

    .line 2130559
    :cond_3
    iget-object v3, p0, LX/EWd;->a:[B

    sub-int v4, p1, v0

    invoke-static {v3, v1, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2130560
    sub-int v0, p1, v0

    iput v0, p0, LX/EWd;->d:I

    move-object v0, v2

    .line 2130561
    goto :goto_0

    .line 2130562
    :cond_4
    iget v5, p0, LX/EWd;->d:I

    .line 2130563
    iget v6, p0, LX/EWd;->b:I

    .line 2130564
    iget v0, p0, LX/EWd;->g:I

    iget v2, p0, LX/EWd;->b:I

    add-int/2addr v0, v2

    iput v0, p0, LX/EWd;->g:I

    .line 2130565
    iput v1, p0, LX/EWd;->d:I

    .line 2130566
    iput v1, p0, LX/EWd;->b:I

    .line 2130567
    sub-int v0, v6, v5

    sub-int v0, p1, v0

    .line 2130568
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v4, v0

    .line 2130569
    :goto_2
    if-lez v4, :cond_8

    .line 2130570
    invoke-static {v4, v10}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-array v8, v0, [B

    move v0, v1

    .line 2130571
    :goto_3
    array-length v2, v8

    if-ge v0, v2, :cond_7

    .line 2130572
    iget-object v2, p0, LX/EWd;->e:Ljava/io/InputStream;

    if-nez v2, :cond_5

    move v2, v3

    .line 2130573
    :goto_4
    if-ne v2, v3, :cond_6

    .line 2130574
    invoke-static {}, LX/EYr;->b()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130575
    :cond_5
    iget-object v2, p0, LX/EWd;->e:Ljava/io/InputStream;

    array-length v9, v8

    sub-int/2addr v9, v0

    invoke-virtual {v2, v8, v0, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    goto :goto_4

    .line 2130576
    :cond_6
    iget v9, p0, LX/EWd;->g:I

    add-int/2addr v9, v2

    iput v9, p0, LX/EWd;->g:I

    .line 2130577
    add-int/2addr v0, v2

    .line 2130578
    goto :goto_3

    .line 2130579
    :cond_7
    array-length v0, v8

    sub-int v0, v4, v0

    .line 2130580
    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v4, v0

    .line 2130581
    goto :goto_2

    .line 2130582
    :cond_8
    new-array v3, p1, [B

    .line 2130583
    sub-int v0, v6, v5

    .line 2130584
    iget-object v2, p0, LX/EWd;->a:[B

    invoke-static {v2, v5, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2130585
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 2130586
    array-length v5, v0

    invoke-static {v0, v1, v3, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2130587
    array-length v0, v0

    add-int/2addr v0, v2

    move v2, v0

    .line 2130588
    goto :goto_5

    :cond_9
    move-object v0, v3

    .line 2130589
    goto/16 :goto_0
.end method

.method private g(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2130590
    if-gez p1, :cond_0

    .line 2130591
    invoke-static {}, LX/EYr;->c()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130592
    :cond_0
    iget v0, p0, LX/EWd;->g:I

    iget v1, p0, LX/EWd;->d:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iget v1, p0, LX/EWd;->h:I

    if-le v0, v1, :cond_1

    .line 2130593
    iget v0, p0, LX/EWd;->h:I

    iget v1, p0, LX/EWd;->g:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/EWd;->d:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, LX/EWd;->g(I)V

    .line 2130594
    invoke-static {}, LX/EYr;->b()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130595
    :cond_1
    iget v0, p0, LX/EWd;->b:I

    iget v1, p0, LX/EWd;->d:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_2

    .line 2130596
    iget v0, p0, LX/EWd;->d:I

    add-int/2addr v0, p1

    iput v0, p0, LX/EWd;->d:I

    .line 2130597
    :goto_0
    return-void

    .line 2130598
    :cond_2
    iget v0, p0, LX/EWd;->b:I

    iget v1, p0, LX/EWd;->d:I

    sub-int/2addr v0, v1

    .line 2130599
    iget v1, p0, LX/EWd;->b:I

    iput v1, p0, LX/EWd;->d:I

    .line 2130600
    invoke-static {p0, v3}, LX/EWd;->a(LX/EWd;Z)Z

    .line 2130601
    :goto_1
    sub-int v1, p1, v0

    iget v2, p0, LX/EWd;->b:I

    if-le v1, v2, :cond_3

    .line 2130602
    iget v1, p0, LX/EWd;->b:I

    add-int/2addr v0, v1

    .line 2130603
    iget v1, p0, LX/EWd;->b:I

    iput v1, p0, LX/EWd;->d:I

    .line 2130604
    invoke-static {p0, v3}, LX/EWd;->a(LX/EWd;Z)Z

    goto :goto_1

    .line 2130605
    :cond_3
    sub-int v0, p1, v0

    iput v0, p0, LX/EWd;->d:I

    goto :goto_0
.end method

.method public static u(LX/EWd;)J
    .locals 6

    .prologue
    .line 2130651
    const/4 v2, 0x0

    .line 2130652
    const-wide/16 v0, 0x0

    .line 2130653
    :goto_0
    const/16 v3, 0x40

    if-ge v2, v3, :cond_1

    .line 2130654
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v3

    .line 2130655
    and-int/lit8 v4, v3, 0x7f

    int-to-long v4, v4

    shl-long/2addr v4, v2

    or-long/2addr v0, v4

    .line 2130656
    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_0

    .line 2130657
    return-wide v0

    .line 2130658
    :cond_0
    add-int/lit8 v2, v2, 0x7

    .line 2130659
    goto :goto_0

    .line 2130660
    :cond_1
    invoke-static {}, LX/EYr;->d()LX/EYr;

    move-result-object v0

    throw v0
.end method

.method public static v(LX/EWd;)I
    .locals 4

    .prologue
    .line 2130468
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v0

    .line 2130469
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v1

    .line 2130470
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v2

    .line 2130471
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v3

    .line 2130472
    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    and-int/lit16 v1, v2, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    and-int/lit16 v1, v3, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public static w(LX/EWd;)J
    .locals 14

    .prologue
    const-wide/16 v12, 0xff

    .line 2130606
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v0

    .line 2130607
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v1

    .line 2130608
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v2

    .line 2130609
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v3

    .line 2130610
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v4

    .line 2130611
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v5

    .line 2130612
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v6

    .line 2130613
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v7

    .line 2130614
    int-to-long v8, v0

    and-long/2addr v8, v12

    int-to-long v0, v1

    and-long/2addr v0, v12

    const/16 v10, 0x8

    shl-long/2addr v0, v10

    or-long/2addr v0, v8

    int-to-long v8, v2

    and-long/2addr v8, v12

    const/16 v2, 0x10

    shl-long/2addr v8, v2

    or-long/2addr v0, v8

    int-to-long v2, v3

    and-long/2addr v2, v12

    const/16 v8, 0x18

    shl-long/2addr v2, v8

    or-long/2addr v0, v2

    int-to-long v2, v4

    and-long/2addr v2, v12

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v5

    and-long/2addr v2, v12

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v6

    and-long/2addr v2, v12

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v7

    and-long/2addr v2, v12

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private x()V
    .locals 2

    .prologue
    .line 2130615
    iget v0, p0, LX/EWd;->b:I

    iget v1, p0, LX/EWd;->c:I

    add-int/2addr v0, v1

    iput v0, p0, LX/EWd;->b:I

    .line 2130616
    iget v0, p0, LX/EWd;->g:I

    iget v1, p0, LX/EWd;->b:I

    add-int/2addr v0, v1

    .line 2130617
    iget v1, p0, LX/EWd;->h:I

    if-le v0, v1, :cond_0

    .line 2130618
    iget v1, p0, LX/EWd;->h:I

    sub-int/2addr v0, v1

    iput v0, p0, LX/EWd;->c:I

    .line 2130619
    iget v0, p0, LX/EWd;->b:I

    iget v1, p0, LX/EWd;->c:I

    sub-int/2addr v0, v1

    iput v0, p0, LX/EWd;->b:I

    .line 2130620
    :goto_0
    return-void

    .line 2130621
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, LX/EWd;->c:I

    goto :goto_0
.end method

.method private z()B
    .locals 3

    .prologue
    .line 2130622
    iget v0, p0, LX/EWd;->d:I

    iget v1, p0, LX/EWd;->b:I

    if-ne v0, v1, :cond_0

    .line 2130623
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/EWd;->a(LX/EWd;Z)Z

    .line 2130624
    :cond_0
    iget-object v0, p0, LX/EWd;->a:[B

    iget v1, p0, LX/EWd;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/EWd;->d:I

    aget-byte v0, v0, v1

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2130625
    const/4 v1, 0x0

    .line 2130626
    iget v2, p0, LX/EWd;->d:I

    iget v3, p0, LX/EWd;->b:I

    if-ne v2, v3, :cond_0

    invoke-static {p0, v1}, LX/EWd;->a(LX/EWd;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v1, v1

    .line 2130627
    if-eqz v1, :cond_1

    .line 2130628
    iput v0, p0, LX/EWd;->f:I

    .line 2130629
    :goto_0
    return v0

    .line 2130630
    :cond_1
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v0

    iput v0, p0, LX/EWd;->f:I

    .line 2130631
    iget v0, p0, LX/EWd;->f:I

    .line 2130632
    ushr-int/lit8 v1, v0, 0x3

    move v0, v1

    .line 2130633
    if-nez v0, :cond_2

    .line 2130634
    new-instance v0, LX/EYr;

    const-string v1, "Protocol message contained an invalid tag (zero)."

    invoke-direct {v0, v1}, LX/EYr;-><init>(Ljava/lang/String;)V

    move-object v0, v0

    .line 2130635
    throw v0

    .line 2130636
    :cond_2
    iget v0, p0, LX/EWd;->f:I

    goto :goto_0
.end method

.method public final a(LX/EWZ;LX/EYZ;)LX/EWW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/EWW;",
            ">(",
            "Lcom/google/protobuf/Parser",
            "<TT;>;",
            "LX/EYZ;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 2130637
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v0

    .line 2130638
    iget v1, p0, LX/EWd;->i:I

    iget v2, p0, LX/EWd;->j:I

    if-lt v1, v2, :cond_0

    .line 2130639
    invoke-static {}, LX/EYr;->h()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130640
    :cond_0
    invoke-virtual {p0, v0}, LX/EWd;->c(I)I

    move-result v1

    .line 2130641
    iget v0, p0, LX/EWd;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EWd;->i:I

    .line 2130642
    invoke-virtual {p1, p0, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    .line 2130643
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, LX/EWd;->a(I)V

    .line 2130644
    iget v2, p0, LX/EWd;->i:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/EWd;->i:I

    .line 2130645
    invoke-virtual {p0, v1}, LX/EWd;->d(I)V

    .line 2130646
    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2130647
    iget v0, p0, LX/EWd;->f:I

    if-eq v0, p1, :cond_0

    .line 2130648
    new-instance v0, LX/EYr;

    const-string p0, "Protocol message end-group tag did not match expected tag."

    invoke-direct {v0, p0}, LX/EYr;-><init>(Ljava/lang/String;)V

    move-object v0, v0

    .line 2130649
    throw v0

    .line 2130650
    :cond_0
    return-void
.end method

.method public final a(ILX/EWR;LX/EYZ;)V
    .locals 2

    .prologue
    .line 2130473
    iget v0, p0, LX/EWd;->i:I

    iget v1, p0, LX/EWd;->j:I

    if-lt v0, v1, :cond_0

    .line 2130474
    invoke-static {}, LX/EYr;->h()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130475
    :cond_0
    iget v0, p0, LX/EWd;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EWd;->i:I

    .line 2130476
    invoke-interface {p2, p0, p3}, LX/EWR;->c(LX/EWd;LX/EYZ;)LX/EWR;

    .line 2130477
    const/4 v0, 0x4

    invoke-static {p1, v0}, LX/EZb;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, LX/EWd;->a(I)V

    .line 2130478
    iget v0, p0, LX/EWd;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/EWd;->i:I

    .line 2130479
    return-void
.end method

.method public final a(LX/EWR;LX/EYZ;)V
    .locals 3

    .prologue
    .line 2130480
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v0

    .line 2130481
    iget v1, p0, LX/EWd;->i:I

    iget v2, p0, LX/EWd;->j:I

    if-lt v1, v2, :cond_0

    .line 2130482
    invoke-static {}, LX/EYr;->h()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130483
    :cond_0
    invoke-virtual {p0, v0}, LX/EWd;->c(I)I

    move-result v0

    .line 2130484
    iget v1, p0, LX/EWd;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/EWd;->i:I

    .line 2130485
    invoke-interface {p1, p0, p2}, LX/EWR;->c(LX/EWd;LX/EYZ;)LX/EWR;

    .line 2130486
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LX/EWd;->a(I)V

    .line 2130487
    iget v1, p0, LX/EWd;->i:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/EWd;->i:I

    .line 2130488
    invoke-virtual {p0, v0}, LX/EWd;->d(I)V

    .line 2130489
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 2130402
    invoke-static {p0}, LX/EWd;->w(LX/EWd;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final b(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2130403
    and-int/lit8 v1, p1, 0x7

    move v1, v1

    .line 2130404
    packed-switch v1, :pswitch_data_0

    .line 2130405
    invoke-static {}, LX/EYr;->g()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130406
    :pswitch_0
    invoke-virtual {p0}, LX/EWd;->f()I

    .line 2130407
    :goto_0
    return v0

    .line 2130408
    :pswitch_1
    invoke-static {p0}, LX/EWd;->w(LX/EWd;)J

    goto :goto_0

    .line 2130409
    :pswitch_2
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v1

    invoke-direct {p0, v1}, LX/EWd;->g(I)V

    goto :goto_0

    .line 2130410
    :cond_0
    :pswitch_3
    invoke-virtual {p0}, LX/EWd;->a()I

    move-result v1

    .line 2130411
    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, LX/EWd;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2130412
    :cond_1
    ushr-int/lit8 v1, p1, 0x3

    move v1, v1

    .line 2130413
    const/4 v2, 0x4

    invoke-static {v1, v2}, LX/EZb;->a(II)I

    move-result v1

    invoke-virtual {p0, v1}, LX/EWd;->a(I)V

    goto :goto_0

    .line 2130414
    :pswitch_4
    const/4 v0, 0x0

    goto :goto_0

    .line 2130415
    :pswitch_5
    invoke-static {p0}, LX/EWd;->v(LX/EWd;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 2130416
    if-gez p1, :cond_0

    .line 2130417
    invoke-static {}, LX/EYr;->c()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130418
    :cond_0
    iget v0, p0, LX/EWd;->g:I

    iget v1, p0, LX/EWd;->d:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    .line 2130419
    iget v1, p0, LX/EWd;->h:I

    .line 2130420
    if-le v0, v1, :cond_1

    .line 2130421
    invoke-static {}, LX/EYr;->b()LX/EYr;

    move-result-object v0

    throw v0

    .line 2130422
    :cond_1
    iput v0, p0, LX/EWd;->h:I

    .line 2130423
    invoke-direct {p0}, LX/EWd;->x()V

    .line 2130424
    return v1
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 2130425
    invoke-static {p0}, LX/EWd;->u(LX/EWd;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 2130426
    iput p1, p0, LX/EWd;->h:I

    .line 2130427
    invoke-direct {p0}, LX/EWd;->x()V

    .line 2130428
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 2130429
    invoke-static {p0}, LX/EWd;->u(LX/EWd;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2130430
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v0

    return v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 2130431
    invoke-static {p0}, LX/EWd;->w(LX/EWd;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 2130432
    invoke-static {p0}, LX/EWd;->v(LX/EWd;)I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 2130433
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/EWc;
    .locals 3

    .prologue
    .line 2130434
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v1

    .line 2130435
    if-nez v1, :cond_0

    .line 2130436
    sget-object v0, LX/EWc;->a:LX/EWc;

    .line 2130437
    :goto_0
    return-object v0

    .line 2130438
    :cond_0
    iget v0, p0, LX/EWd;->b:I

    iget v2, p0, LX/EWd;->d:I

    sub-int/2addr v0, v2

    if-gt v1, v0, :cond_1

    if-lez v1, :cond_1

    .line 2130439
    iget-object v0, p0, LX/EWd;->a:[B

    iget v2, p0, LX/EWd;->d:I

    invoke-static {v0, v2, v1}, LX/EWc;->a([BII)LX/EWc;

    move-result-object v0

    .line 2130440
    iget v2, p0, LX/EWd;->d:I

    add-int/2addr v1, v2

    iput v1, p0, LX/EWd;->d:I

    goto :goto_0

    .line 2130441
    :cond_1
    invoke-static {p0, v1}, LX/EWd;->f(LX/EWd;I)[B

    move-result-object v0

    invoke-static {v0}, LX/EWc;->a([B)LX/EWc;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 2130442
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v0

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2130443
    invoke-virtual {p0}, LX/EWd;->r()I

    move-result v0

    return v0
.end method

.method public final r()I
    .locals 3

    .prologue
    .line 2130444
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v0

    .line 2130445
    if-ltz v0, :cond_1

    .line 2130446
    :cond_0
    :goto_0
    return v0

    .line 2130447
    :cond_1
    and-int/lit8 v0, v0, 0x7f

    .line 2130448
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v1

    if-ltz v1, :cond_2

    .line 2130449
    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    goto :goto_0

    .line 2130450
    :cond_2
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    .line 2130451
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v1

    if-ltz v1, :cond_3

    .line 2130452
    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    goto :goto_0

    .line 2130453
    :cond_3
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    .line 2130454
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v1

    if-ltz v1, :cond_4

    .line 2130455
    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    goto :goto_0

    .line 2130456
    :cond_4
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    .line 2130457
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v1

    shl-int/lit8 v2, v1, 0x1c

    or-int/2addr v0, v2

    .line 2130458
    if-gez v1, :cond_0

    .line 2130459
    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_5

    .line 2130460
    invoke-direct {p0}, LX/EWd;->z()B

    move-result v2

    if-gez v2, :cond_0

    .line 2130461
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2130462
    :cond_5
    invoke-static {}, LX/EYr;->d()LX/EYr;

    move-result-object v0

    throw v0
.end method

.method public final s()I
    .locals 2

    .prologue
    .line 2130463
    iget v0, p0, LX/EWd;->h:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 2130464
    const/4 v0, -0x1

    .line 2130465
    :goto_0
    return v0

    .line 2130466
    :cond_0
    iget v0, p0, LX/EWd;->g:I

    iget v1, p0, LX/EWd;->d:I

    add-int/2addr v0, v1

    .line 2130467
    iget v1, p0, LX/EWd;->h:I

    sub-int v0, v1, v0

    goto :goto_0
.end method
