.class public LX/D4G;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static m:LX/0Xm;


# instance fields
.field public final a:LX/D4D;

.field public final b:LX/D4F;

.field public final c:LX/D4E;

.field private final d:LX/0hB;

.field private final e:LX/1AY;

.field public final f:LX/7zh;

.field public final g:LX/D4B;

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/D5z;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

.field public j:LX/D5P;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/D5z;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0hB;LX/1AY;Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1961933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1961934
    new-instance v0, LX/D4D;

    invoke-direct {v0, p0}, LX/D4D;-><init>(LX/D4G;)V

    iput-object v0, p0, LX/D4G;->a:LX/D4D;

    .line 1961935
    new-instance v0, LX/D4F;

    invoke-direct {v0, p0}, LX/D4F;-><init>(LX/D4G;)V

    iput-object v0, p0, LX/D4G;->b:LX/D4F;

    .line 1961936
    new-instance v0, LX/D4E;

    invoke-direct {v0, p0}, LX/D4E;-><init>(LX/D4G;)V

    iput-object v0, p0, LX/D4G;->c:LX/D4E;

    .line 1961937
    iput-object p1, p0, LX/D4G;->d:LX/0hB;

    .line 1961938
    iput-object p2, p0, LX/D4G;->e:LX/1AY;

    .line 1961939
    iput-object p3, p0, LX/D4G;->i:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    .line 1961940
    new-instance v0, LX/D4A;

    invoke-direct {v0, p0}, LX/D4A;-><init>(LX/D4G;)V

    iput-object v0, p0, LX/D4G;->f:LX/7zh;

    .line 1961941
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/D4G;->h:Ljava/util/Set;

    .line 1961942
    new-instance v0, LX/D4B;

    invoke-direct {v0, p0}, LX/D4B;-><init>(LX/D4G;)V

    iput-object v0, p0, LX/D4G;->g:LX/D4B;

    .line 1961943
    return-void
.end method

.method private static a(LX/D4G;LX/D5z;Z)I
    .locals 3

    .prologue
    .line 1961944
    if-nez p1, :cond_1

    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1961945
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D5z;

    invoke-virtual {v0}, LX/D5z;->getBottom()I

    move-result v0

    .line 1961946
    :goto_0
    return v0

    .line 1961947
    :cond_0
    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D5z;

    invoke-virtual {v0}, LX/D5z;->getBottom()I

    move-result v0

    neg-int v0, v0

    goto :goto_0

    .line 1961948
    :cond_1
    iget-object v0, p0, LX/D4G;->d:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 1961949
    invoke-virtual {p1}, LX/D5z;->getBottom()I

    move-result v1

    invoke-virtual {p1}, LX/D5z;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 1961950
    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/D4G;
    .locals 6

    .prologue
    .line 1961951
    const-class v1, LX/D4G;

    monitor-enter v1

    .line 1961952
    :try_start_0
    sget-object v0, LX/D4G;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1961953
    sput-object v2, LX/D4G;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1961954
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1961955
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1961956
    new-instance p0, LX/D4G;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-static {v0}, LX/1AY;->b(LX/0QB;)LX/1AY;

    move-result-object v4

    check-cast v4, LX/1AY;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-direct {p0, v3, v4, v5}, LX/D4G;-><init>(LX/0hB;LX/1AY;Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;)V

    .line 1961957
    move-object v0, p0

    .line 1961958
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1961959
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D4G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1961960
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1961961
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static c(LX/D4G;Z)LX/D5z;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1961962
    iget-object v0, p0, LX/D4G;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1961963
    :cond_0
    return-object v1

    .line 1961964
    :cond_1
    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1961965
    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D5z;

    .line 1961966
    if-eqz v0, :cond_0

    .line 1961967
    invoke-virtual {v0}, LX/D5z;->getTop()I

    move-result v3

    .line 1961968
    const v0, 0x7fffffff

    .line 1961969
    iget-object v2, p0, LX/D4G;->h:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    .line 1961970
    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1961971
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D5z;

    .line 1961972
    if-eqz v0, :cond_2

    iget-object v5, p0, LX/D4G;->e:LX/1AY;

    invoke-virtual {v5, v0}, LX/1AY;->b(Landroid/view/View;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1961973
    invoke-virtual {v0}, LX/D5z;->getTop()I

    move-result v5

    .line 1961974
    if-eqz p1, :cond_3

    if-le v5, v3, :cond_3

    sub-int v6, v5, v3

    if-ge v6, v2, :cond_3

    .line 1961975
    sub-int v1, v5, v3

    move v2, v1

    move-object v1, v0

    .line 1961976
    goto :goto_0

    .line 1961977
    :cond_3
    if-nez p1, :cond_4

    if-ge v5, v3, :cond_4

    sub-int v6, v3, v5

    if-ge v6, v2, :cond_4

    .line 1961978
    sub-int v2, v3, v5

    move v1, v2

    :goto_1
    move v2, v1

    move-object v1, v0

    .line 1961979
    goto :goto_0

    :cond_4
    move-object v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public static c(LX/D4G;LX/1Aa;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Aa",
            "<",
            "LX/D5z;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1961980
    iget-object v0, p0, LX/D4G;->f:LX/7zh;

    invoke-virtual {p1, v0}, LX/1Aa;->b(LX/7zh;)V

    .line 1961981
    iget-object v0, p0, LX/D4G;->g:LX/D4B;

    .line 1961982
    iget-object p0, p1, LX/1Aa;->q:Ljava/util/List;

    invoke-interface {p0, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1961983
    return-void
.end method

.method public static d$redex0(LX/D4G;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1961984
    iget-object v0, p0, LX/D4G;->l:LX/0QK;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1961985
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, LX/D4G;->l:LX/0QK;

    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D5z;

    .line 1961986
    iget-object p0, v0, LX/D5z;->r:Ljava/lang/String;

    move-object v0, p0

    .line 1961987
    invoke-interface {v2, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1961988
    iget-object v0, p0, LX/D4G;->j:LX/D5P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1961989
    monitor-enter p0

    .line 1961990
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, v0}, LX/D4G;->c(LX/D4G;Z)LX/D5z;

    move-result-object v0

    .line 1961991
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/D4G;->a(LX/D4G;LX/D5z;Z)I

    move-result v0

    .line 1961992
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1961993
    iget-object v1, p0, LX/D4G;->j:LX/D5P;

    invoke-virtual {v1, v0, p1}, LX/D5P;->a(IZ)V

    .line 1961994
    :cond_0
    return-void

    .line 1961995
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1961996
    iget-object v0, p0, LX/D4G;->j:LX/D5P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1961997
    monitor-enter p0

    .line 1961998
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0}, LX/D4G;->c(LX/D4G;Z)LX/D5z;

    move-result-object v0

    .line 1961999
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/D4G;->a(LX/D4G;LX/D5z;Z)I

    move-result v0

    .line 1962000
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1962001
    iget-object v1, p0, LX/D4G;->j:LX/D5P;

    invoke-virtual {v1, v0, p1}, LX/D5P;->a(IZ)V

    .line 1962002
    :cond_0
    return-void

    .line 1962003
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
