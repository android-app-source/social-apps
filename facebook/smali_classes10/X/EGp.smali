.class public final enum LX/EGp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EGp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EGp;

.field public static final enum IN_CALL:LX/EGp;

.field public static final enum NOTIFY_START:LX/EGp;

.field public static final enum RUNG_START:LX/EGp;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2098444
    new-instance v0, LX/EGp;

    const-string v1, "NOTIFY_START"

    invoke-direct {v0, v1, v2}, LX/EGp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGp;->NOTIFY_START:LX/EGp;

    .line 2098445
    new-instance v0, LX/EGp;

    const-string v1, "RUNG_START"

    invoke-direct {v0, v1, v3}, LX/EGp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGp;->RUNG_START:LX/EGp;

    .line 2098446
    new-instance v0, LX/EGp;

    const-string v1, "IN_CALL"

    invoke-direct {v0, v1, v4}, LX/EGp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGp;->IN_CALL:LX/EGp;

    .line 2098447
    const/4 v0, 0x3

    new-array v0, v0, [LX/EGp;

    sget-object v1, LX/EGp;->NOTIFY_START:LX/EGp;

    aput-object v1, v0, v2

    sget-object v1, LX/EGp;->RUNG_START:LX/EGp;

    aput-object v1, v0, v3

    sget-object v1, LX/EGp;->IN_CALL:LX/EGp;

    aput-object v1, v0, v4

    sput-object v0, LX/EGp;->$VALUES:[LX/EGp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2098448
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EGp;
    .locals 1

    .prologue
    .line 2098449
    const-class v0, LX/EGp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EGp;

    return-object v0
.end method

.method public static values()[LX/EGp;
    .locals 1

    .prologue
    .line 2098450
    sget-object v0, LX/EGp;->$VALUES:[LX/EGp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EGp;

    return-object v0
.end method
