.class public LX/DHC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pn;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/1xT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xT",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/DHK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DHK",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:LX/1yk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1yk",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final d:LX/1ym;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ym",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/6W7;

.field public final f:LX/14w;

.field private final g:LX/2yK;


# direct methods
.method public constructor <init>(LX/1xT;LX/DHK;LX/1yk;LX/1ym;LX/6W7;LX/14w;LX/2yK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1981498
    iput-object p1, p0, LX/DHC;->a:LX/1xT;

    .line 1981499
    iput-object p2, p0, LX/DHC;->b:LX/DHK;

    .line 1981500
    iput-object p3, p0, LX/DHC;->c:LX/1yk;

    .line 1981501
    iput-object p4, p0, LX/DHC;->d:LX/1ym;

    .line 1981502
    iput-object p5, p0, LX/DHC;->e:LX/6W7;

    .line 1981503
    iput-object p6, p0, LX/DHC;->f:LX/14w;

    .line 1981504
    iput-object p7, p0, LX/DHC;->g:LX/2yK;

    .line 1981505
    return-void
.end method

.method public static a(LX/0QB;)LX/DHC;
    .locals 11

    .prologue
    .line 1981481
    const-class v1, LX/DHC;

    monitor-enter v1

    .line 1981482
    :try_start_0
    sget-object v0, LX/DHC;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981483
    sput-object v2, LX/DHC;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981484
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981485
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981486
    new-instance v3, LX/DHC;

    invoke-static {v0}, LX/1xT;->a(LX/0QB;)LX/1xT;

    move-result-object v4

    check-cast v4, LX/1xT;

    invoke-static {v0}, LX/DHK;->a(LX/0QB;)LX/DHK;

    move-result-object v5

    check-cast v5, LX/DHK;

    invoke-static {v0}, LX/1yk;->a(LX/0QB;)LX/1yk;

    move-result-object v6

    check-cast v6, LX/1yk;

    invoke-static {v0}, LX/1ym;->a(LX/0QB;)LX/1ym;

    move-result-object v7

    check-cast v7, LX/1ym;

    invoke-static {v0}, LX/6W7;->a(LX/0QB;)LX/6W7;

    move-result-object v8

    check-cast v8, LX/6W7;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v9

    check-cast v9, LX/14w;

    invoke-static {v0}, LX/2yK;->b(LX/0QB;)LX/2yK;

    move-result-object v10

    check-cast v10, LX/2yK;

    invoke-direct/range {v3 .. v10}, LX/DHC;-><init>(LX/1xT;LX/DHK;LX/1yk;LX/1ym;LX/6W7;LX/14w;LX/2yK;)V

    .line 1981487
    move-object v0, v3

    .line 1981488
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981489
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DHC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981490
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/DHC;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1981492
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    .line 1981493
    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_0

    .line 1981494
    iget-object v2, p0, LX/DHC;->e:LX/6W7;

    move-object v0, v1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    const-string v3, "newsfeed"

    invoke-virtual {v2, v0, v3}, LX/6W7;->a(Lcom/facebook/graphql/model/GraphQLStorySet;Ljava/lang/String;)V

    .line 1981495
    iget-object v0, p0, LX/DHC;->g:LX/2yK;

    sget-object v2, LX/0ig;->A:LX/0ih;

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    const/16 v3, 0x9

    invoke-virtual {v0, v2, v1, v3}, LX/2yK;->a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;I)V

    .line 1981496
    :cond_0
    return-void
.end method
