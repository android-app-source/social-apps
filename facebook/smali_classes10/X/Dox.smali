.class public LX/Dox;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DoO;


# instance fields
.field private final a:LX/Dp2;


# direct methods
.method public constructor <init>(LX/Dp2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042847
    iput-object p1, p0, LX/Dox;->a:LX/Dp2;

    .line 2042848
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/Eaf;
    .locals 1

    .prologue
    .line 2042849
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Dox;->a:LX/Dp2;

    invoke-virtual {v0}, LX/Dp2;->a()LX/Eaf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/Eap;LX/Eae;)V
    .locals 0

    .prologue
    .line 2042850
    return-void
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 2042851
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Dox;->a:LX/Dp2;

    invoke-virtual {v0}, LX/Dp2;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(LX/Eap;LX/Eae;)Z
    .locals 1

    .prologue
    .line 2042852
    const/4 v0, 0x1

    return v0
.end method
