.class public LX/Co5;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/RelatedArticleBlockView;",
        "Lcom/facebook/richdocument/model/data/RelatedArticleBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;)V
    .locals 0

    .prologue
    .line 1934897
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934898
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;LX/Clb;)LX/Clf;
    .locals 1

    .prologue
    .line 1934899
    new-instance v0, LX/Cle;

    invoke-direct {v0, p0}, LX/Cle;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, LX/Cle;->a(LX/Clb;)LX/Cle;

    move-result-object v0

    .line 1934900
    iput-object p1, v0, LX/Cle;->d:Ljava/lang/CharSequence;

    .line 1934901
    move-object v0, v0

    .line 1934902
    invoke-virtual {v0}, LX/Cle;->a()LX/Clf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 12

    .prologue
    .line 1934903
    check-cast p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;

    .line 1934904
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934905
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1934906
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934907
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    .line 1934908
    iget-object v1, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1934909
    iget-object v2, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1934910
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1934911
    iget-object v4, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->f:Ljava/lang/String;

    move-object v4, v4

    .line 1934912
    sget-object v5, LX/Clb;->RELATED_ARTICLES:LX/Clb;

    invoke-static {v3, v4, v5}, LX/Co5;->a(Landroid/content/Context;Ljava/lang/String;LX/Clb;)LX/Clf;

    move-result-object v3

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 1934913
    iget-object v5, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->i:Ljava/lang/String;

    move-object v5, v5

    .line 1934914
    sget-object v6, LX/Clb;->KICKER:LX/Clb;

    invoke-static {v4, v5, v6}, LX/Co5;->a(Landroid/content/Context;Ljava/lang/String;LX/Clb;)LX/Clf;

    move-result-object v4

    .line 1934915
    iget-boolean v5, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->k:Z

    move v5, v5

    .line 1934916
    iget-object v6, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->m:Ljava/lang/String;

    move-object v6, v6

    .line 1934917
    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 1934918
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1934919
    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v7

    new-instance v8, LX/CpY;

    invoke-direct {v8, v0, v1, v6}, LX/CpY;-><init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1934920
    :cond_0
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934921
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1934922
    invoke-virtual {v7, v3}, LX/CtG;->setText(LX/Clf;)V

    .line 1934923
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934924
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1934925
    new-instance v8, LX/CpZ;

    invoke-direct {v8, v0, v1, v6}, LX/CpZ;-><init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, LX/CtG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1934926
    if-eqz v4, :cond_1

    .line 1934927
    iget-object v7, v4, LX/Clf;->a:Ljava/lang/CharSequence;

    move-object v7, v7

    .line 1934928
    invoke-static {v7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1934929
    :cond_1
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v7, v11}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1934930
    :goto_0
    if-eqz v5, :cond_6

    .line 1934931
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v7, v10}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1934932
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934933
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1934934
    new-instance v8, LX/Cpb;

    invoke-direct {v8, v0, v1, v6}, LX/Cpb;-><init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, LX/CtG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1934935
    :goto_1
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v8, 0x3ff47ae1    # 1.91f

    invoke-virtual {v7, v8}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1934936
    iget-object v8, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v2, :cond_7

    const/4 v7, 0x0

    :goto_2
    sget-object v9, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v8, v7, v9}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1934937
    if-eqz v6, :cond_8

    .line 1934938
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->e:LX/0Uh;

    const/16 v8, 0x43a

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, LX/0Uh;->a(IZ)Z

    move-result v7

    move v7, v7

    .line 1934939
    if-eqz v7, :cond_8

    .line 1934940
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->o:Landroid/view/View;

    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    .line 1934941
    :goto_3
    iget-object v0, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->n:LX/Cm2;

    move-object v1, v0

    .line 1934942
    sget-object v0, LX/Cm2;->BOTTOM:LX/Cm2;

    if-ne v1, v0, :cond_4

    .line 1934943
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934944
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    const-string v2, "footer_related_article"

    .line 1934945
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->q:Ljava/lang/String;

    .line 1934946
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934947
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0622

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1934948
    iput v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->p:I

    .line 1934949
    :cond_2
    :goto_4
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934950
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v2

    .line 1934951
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->s:Ljava/lang/String;

    .line 1934952
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934953
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v2

    .line 1934954
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->t:Ljava/lang/String;

    .line 1934955
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934956
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    .line 1934957
    iget v2, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->q:I

    move v2, v2

    .line 1934958
    iput v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->u:I

    .line 1934959
    iget v0, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->l:I

    move v2, v0

    .line 1934960
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934961
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    .line 1934962
    iput v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->r:I

    .line 1934963
    sget-object v0, LX/Cm2;->BOTTOM:LX/Cm2;

    if-ne v1, v0, :cond_3

    const/4 v0, 0x1

    if-ne v2, v0, :cond_3

    .line 1934964
    iget-object v0, p0, LX/CnT;->a:LX/Chv;

    new-instance v1, LX/CiE;

    invoke-direct {v1}, LX/CiE;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1934965
    :cond_3
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934966
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->lw_()LX/Cml;

    move-result-object v1

    invoke-interface {v0, v1}, LX/CnG;->a(LX/Cml;)V

    .line 1934967
    return-void

    .line 1934968
    :cond_4
    sget-object v0, LX/Cm2;->INLINE:LX/Cm2;

    if-ne v1, v0, :cond_2

    .line 1934969
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934970
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    const-string v2, "inline_related_article"

    .line 1934971
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->q:Ljava/lang/String;

    .line 1934972
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934973
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1934974
    iput v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->p:I

    .line 1934975
    goto :goto_4

    .line 1934976
    :cond_5
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v7, v10}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1934977
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934978
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1934979
    invoke-virtual {v7, v4}, LX/CtG;->setText(LX/Clf;)V

    .line 1934980
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934981
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1934982
    new-instance v8, LX/Cpa;

    invoke-direct {v8, v0, v1, v6}, LX/Cpa;-><init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, LX/CtG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1934983
    :cond_6
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v7, v11}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 1934984
    :cond_7
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    goto/16 :goto_2

    .line 1934985
    :cond_8
    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->o:Landroid/view/View;

    invoke-virtual {v7, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3
.end method
