.class public final LX/DOf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:LX/DOl;


# direct methods
.method public constructor <init>(LX/DOl;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1992619
    iput-object p1, p0, LX/DOf;->d:LX/DOl;

    iput-object p2, p0, LX/DOf;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/DOf;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DOf;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 1992620
    iget-object v0, p0, LX/DOf;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/DOl;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1992621
    iget-object v1, p0, LX/DOf;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/DOf;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1992622
    :cond_0
    iget-object v1, p0, LX/DOf;->d:LX/DOl;

    iget-object v1, v1, LX/DOl;->c:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f081055

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1992623
    iget-object v1, p0, LX/DOf;->d:LX/DOl;

    iget-object v1, v1, LX/DOl;->i:LX/03V;

    sget-object v2, LX/DOl;->l:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Group feed story "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/DOf;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "does not have an actor id in group"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "in method deletePostAndBlockUser"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992624
    :goto_0
    return-void

    .line 1992625
    :cond_1
    iget-object v1, p0, LX/DOf;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    .line 1992626
    iget-object v2, p0, LX/DOf;->d:LX/DOl;

    iget-object v3, p0, LX/DOf;->b:Ljava/lang/String;

    .line 1992627
    new-instance v4, LX/4Fg;

    invoke-direct {v4}, LX/4Fg;-><init>()V

    iget-object p1, v2, LX/DOl;->g:Ljava/lang/String;

    invoke-virtual {v4, p1}, LX/4Fg;->a(Ljava/lang/String;)LX/4Fg;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/4Fg;->b(Ljava/lang/String;)LX/4Fg;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/4Fg;->d(Ljava/lang/String;)LX/4Fg;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/4Fg;->c(Ljava/lang/String;)LX/4Fg;

    move-result-object v4

    .line 1992628
    invoke-static {}, LX/DV9;->d()LX/DV4;

    move-result-object p1

    .line 1992629
    const-string p2, "input"

    invoke-virtual {p1, p2, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1992630
    move-object v0, p1

    .line 1992631
    iget-object v1, p0, LX/DOf;->d:LX/DOl;

    iget-object v2, p0, LX/DOf;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1992632
    iget-object v3, v1, LX/DOl;->f:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1992633
    new-instance v4, LX/DOg;

    invoke-direct {v4, v1, v2}, LX/DOg;-><init>(LX/DOl;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iget-object p0, v1, LX/DOl;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1992634
    goto :goto_0
.end method
