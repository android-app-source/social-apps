.class public LX/EaX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2141579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/EZh;[B[BJ[B)I
    .locals 13

    .prologue
    .line 2141580
    const/16 v2, 0x20

    new-array v2, v2, [B

    .line 2141581
    const/16 v3, 0x20

    new-array v3, v3, [B

    .line 2141582
    const/16 v4, 0x20

    new-array v4, v4, [B

    .line 2141583
    const/16 v5, 0x40

    new-array v5, v5, [B

    .line 2141584
    const/16 v6, 0x20

    new-array v6, v6, [B

    .line 2141585
    new-instance v7, LX/EaI;

    invoke-direct {v7}, LX/EaI;-><init>()V

    .line 2141586
    new-instance v8, LX/EaF;

    invoke-direct {v8}, LX/EaF;-><init>()V

    .line 2141587
    const-wide/16 v10, 0x40

    cmp-long v9, p3, v10

    if-gez v9, :cond_0

    const/4 v2, -0x1

    .line 2141588
    :goto_0
    return v2

    .line 2141589
    :cond_0
    const/16 v9, 0x3f

    aget-byte v9, p2, v9

    and-int/lit16 v9, v9, 0xe0

    if-eqz v9, :cond_1

    const/4 v2, -0x1

    goto :goto_0

    .line 2141590
    :cond_1
    move-object/from16 v0, p5

    invoke-static {v7, v0}, LX/Ea9;->a(LX/EaI;[B)I

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, -0x1

    goto :goto_0

    .line 2141591
    :cond_2
    const/16 v9, 0x40

    new-array v9, v9, [B

    .line 2141592
    const-wide/16 v10, 0x20

    move-object/from16 v0, p5

    invoke-virtual {p0, v9, v0, v10, v11}, LX/EZh;->a([B[BJ)V

    .line 2141593
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x20

    move-object/from16 v0, p5

    invoke-static {v0, v9, v2, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2141594
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x20

    invoke-static {p2, v9, v3, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2141595
    const/16 v9, 0x20

    const/4 v10, 0x0

    const/16 v11, 0x20

    invoke-static {p2, v9, v4, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2141596
    const/4 v9, 0x0

    const/4 v10, 0x0

    move-wide/from16 v0, p3

    long-to-int v11, v0

    invoke-static {p2, v9, p1, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2141597
    const/4 v9, 0x0

    const/16 v10, 0x20

    const/16 v11, 0x20

    invoke-static {v2, v9, p1, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2141598
    move-wide/from16 v0, p3

    invoke-virtual {p0, v5, p1, v0, v1}, LX/EZh;->a([B[BJ)V

    .line 2141599
    invoke-static {v5}, LX/EaZ;->a([B)V

    .line 2141600
    invoke-static {v8, v5, v7, v4}, LX/Ea8;->a(LX/EaF;[BLX/EaI;[B)V

    .line 2141601
    invoke-static {v6, v8}, LX/EaW;->a([BLX/EaF;)V

    .line 2141602
    invoke-static {v6, v3}, LX/EZm;->a([B[B)I

    move-result v2

    if-nez v2, :cond_3

    .line 2141603
    const/16 v2, 0x40

    const/4 v3, 0x0

    const-wide/16 v4, 0x40

    sub-long v4, p3, v4

    long-to-int v4, v4

    invoke-static {p1, v2, p1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2141604
    const/4 v2, 0x0

    goto :goto_0

    .line 2141605
    :cond_3
    const/4 v2, -0x1

    goto :goto_0
.end method
