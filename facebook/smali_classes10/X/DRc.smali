.class public LX/DRc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DRb;


# instance fields
.field public a:Lcom/facebook/groups/learning/GroupsLearningTabBar;

.field public b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

.field public c:Landroid/view/ViewStub;

.field private d:Landroid/content/Context;

.field private e:LX/0zG;

.field private f:I

.field public g:I

.field public h:LX/DRb;

.field public i:LX/DRa;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0zG;Landroid/view/ViewStub;I)V
    .locals 1

    .prologue
    .line 1998398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1998399
    sget-object v0, LX/DRa;->LEARNING_UNITS:LX/DRa;

    iput-object v0, p0, LX/DRc;->i:LX/DRa;

    .line 1998400
    iput-object p1, p0, LX/DRc;->d:Landroid/content/Context;

    .line 1998401
    iput-object p2, p0, LX/DRc;->e:LX/0zG;

    .line 1998402
    iput-object p3, p0, LX/DRc;->c:Landroid/view/ViewStub;

    .line 1998403
    iput p4, p0, LX/DRc;->f:I

    .line 1998404
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1998405
    iget-object v0, p0, LX/DRc;->e:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1998406
    instance-of v0, v0, LX/63T;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DRc;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0413

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget v1, p0, LX/DRc;->f:I

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/DRa;)V
    .locals 1

    .prologue
    .line 1998407
    iput-object p1, p0, LX/DRc;->i:LX/DRa;

    .line 1998408
    iget-object v0, p0, LX/DRc;->a:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    if-eqz v0, :cond_0

    .line 1998409
    iget-object v0, p0, LX/DRc;->a:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    invoke-virtual {v0, p1}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->setSelectedTab(LX/DRa;)V

    .line 1998410
    :cond_0
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    if-eqz v0, :cond_1

    .line 1998411
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    invoke-virtual {v0, p1}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->setSelectedTab(LX/DRa;)V

    .line 1998412
    :cond_1
    iget-object v0, p0, LX/DRc;->h:LX/DRb;

    if-eqz v0, :cond_2

    .line 1998413
    iget-object v0, p0, LX/DRc;->h:LX/DRb;

    invoke-interface {v0, p1}, LX/DRb;->a(LX/DRa;)V

    .line 1998414
    :cond_2
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1998415
    iget-object v0, p0, LX/DRc;->a:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    if-nez v0, :cond_1

    .line 1998416
    :cond_0
    :goto_0
    return-void

    .line 1998417
    :cond_1
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    invoke-virtual {v0}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1998418
    if-nez v0, :cond_2

    .line 1998419
    iget-object v0, p0, LX/DRc;->a:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    invoke-virtual {v0}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->getTop()I

    move-result v0

    invoke-virtual {p0}, LX/DRc;->a()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 1998420
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    if-eqz v0, :cond_4

    .line 1998421
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    .line 1998422
    :goto_2
    move-object v0, v0

    .line 1998423
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->setVisibility(I)V

    goto :goto_0

    .line 1998424
    :cond_2
    iget-object v0, p0, LX/DRc;->a:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    invoke-virtual {v0}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->getTop()I

    move-result v0

    invoke-virtual {p0}, LX/DRc;->a()I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    if-eqz v0, :cond_0

    .line 1998425
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1998426
    :cond_4
    iget-object v0, p0, LX/DRc;->c:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/learning/GroupsLearningTabBar;

    iput-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    .line 1998427
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->setVisibility(I)V

    .line 1998428
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    invoke-virtual {p0}, LX/DRc;->a()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->setTranslationY(F)V

    .line 1998429
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    iget-object v1, p0, LX/DRc;->i:LX/DRa;

    invoke-virtual {v0, v1}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->setSelectedTab(LX/DRa;)V

    .line 1998430
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    .line 1998431
    iput-object p0, v0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->a:LX/DRb;

    .line 1998432
    iget-object v0, p0, LX/DRc;->b:Lcom/facebook/groups/learning/GroupsLearningTabBar;

    goto :goto_2
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1998433
    iget-object v0, p0, LX/DRc;->i:LX/DRa;

    sget-object v1, LX/DRa;->LEARNING_UNITS:LX/DRa;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
