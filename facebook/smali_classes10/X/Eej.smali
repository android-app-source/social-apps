.class public LX/Eej;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EeO;


# instance fields
.field private final a:Landroid/app/DownloadManager;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/DownloadManager;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/DownloadManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2153227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2153228
    iput-object p1, p0, LX/Eej;->a:Landroid/app/DownloadManager;

    .line 2153229
    iput-object p2, p0, LX/Eej;->b:LX/0Or;

    .line 2153230
    iput-object p3, p0, LX/Eej;->c:LX/0Or;

    .line 2153231
    return-void
.end method


# virtual methods
.method public final a(LX/EeX;)LX/EeY;
    .locals 6

    .prologue
    const/4 v4, 0x2

    .line 2153232
    invoke-virtual {p1}, LX/EeX;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2153233
    iget-object v0, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v0, v0, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadUri:Ljava/lang/String;

    .line 2153234
    :goto_0
    move-object v0, v0

    .line 2153235
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2153236
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_4

    .line 2153237
    new-instance v1, LX/EeZ;

    invoke-direct {v1, v0}, LX/EeZ;-><init>(Landroid/net/Uri;)V

    .line 2153238
    :goto_1
    move-object v1, v1

    .line 2153239
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v2, ".facebook.com"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2153240
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "OAuth "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/Eej;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/EeZ;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 2153241
    :cond_0
    const-string v2, "User-Agent"

    iget-object v0, p0, LX/Eej;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/EeZ;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 2153242
    const-string v0, "X-Compute-Etag"

    const-string v2, "true"

    invoke-virtual {v1, v0, v2}, LX/EeZ;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 2153243
    iget-boolean v0, p1, LX/EeX;->isBackgroundMode:Z

    if-eqz v0, :cond_2

    .line 2153244
    invoke-virtual {v1, v4}, LX/EeZ;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 2153245
    :goto_2
    iget-boolean v0, p1, LX/EeX;->isWifiOnly:Z

    if-eqz v0, :cond_1

    .line 2153246
    invoke-virtual {v1, v4}, LX/EeZ;->setAllowedNetworkTypes(I)Landroid/app/DownloadManager$Request;

    .line 2153247
    :cond_1
    iget-object v0, p0, LX/Eej;->a:Landroid/app/DownloadManager;

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    .line 2153248
    new-instance v2, LX/EeW;

    invoke-direct {v2, p1}, LX/EeW;-><init>(LX/EeX;)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2153249
    iput-object v3, v2, LX/EeW;->e:Ljava/lang/Integer;

    .line 2153250
    move-object v2, v2

    .line 2153251
    iput-wide v0, v2, LX/EeW;->f:J

    .line 2153252
    move-object v0, v2

    .line 2153253
    invoke-virtual {v0}, LX/EeW;->a()LX/EeX;

    move-result-object v0

    .line 2153254
    new-instance v1, LX/EeY;

    invoke-direct {v1, v0}, LX/EeY;-><init>(LX/EeX;)V

    return-object v1

    .line 2153255
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/EeZ;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 2153256
    const-string v0, "Downloading update"

    invoke-virtual {v1, v0}, LX/EeZ;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 2153257
    iget-object v0, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v0, v0, Lcom/facebook/appupdate/ReleaseInfo;->appName:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/EeZ;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    goto :goto_2

    :cond_3
    iget-object v0, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v0, v0, Lcom/facebook/appupdate/ReleaseInfo;->downloadUri:Ljava/lang/String;

    goto/16 :goto_0

    .line 2153258
    :cond_4
    sget-object v1, LX/EeZ;->b:Ljava/lang/reflect/Field;

    if-nez v1, :cond_5

    .line 2153259
    invoke-static {}, LX/EeZ;->a()V

    .line 2153260
    :cond_5
    sget-object v1, LX/EeZ;->b:Ljava/lang/reflect/Field;

    if-eqz v1, :cond_6

    .line 2153261
    new-instance v1, LX/EeZ;

    sget-object v2, LX/EeZ;->a:Landroid/net/Uri;

    invoke-direct {v1, v2, v0}, LX/EeZ;-><init>(Landroid/net/Uri;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 2153262
    :cond_6
    new-instance v1, LX/EeZ;

    invoke-direct {v1, v0}, LX/EeZ;-><init>(Landroid/net/Uri;)V

    goto/16 :goto_1
.end method
