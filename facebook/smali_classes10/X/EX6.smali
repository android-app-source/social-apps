.class public final LX/EX6;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EX4;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EX6;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EX6;


# instance fields
.field public bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public name_:Ljava/lang/Object;

.field public number_:I

.field public options_:LX/EXA;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2132500
    new-instance v0, LX/EX3;

    invoke-direct {v0}, LX/EX3;-><init>()V

    sput-object v0, LX/EX6;->a:LX/EWZ;

    .line 2132501
    new-instance v0, LX/EX6;

    invoke-direct {v0}, LX/EX6;-><init>()V

    .line 2132502
    sput-object v0, LX/EX6;->c:LX/EX6;

    invoke-direct {v0}, LX/EX6;->r()V

    .line 2132503
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2132504
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2132505
    iput-byte v0, p0, LX/EX6;->memoizedIsInitialized:B

    .line 2132506
    iput v0, p0, LX/EX6;->memoizedSerializedSize:I

    .line 2132507
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2132508
    iput-object v0, p0, LX/EX6;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x1

    .line 2132509
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2132510
    iput-byte v0, p0, LX/EX6;->memoizedIsInitialized:B

    .line 2132511
    iput v0, p0, LX/EX6;->memoizedSerializedSize:I

    .line 2132512
    invoke-direct {p0}, LX/EX6;->r()V

    .line 2132513
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v4

    .line 2132514
    const/4 v0, 0x0

    move v2, v0

    .line 2132515
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 2132516
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2132517
    sparse-switch v0, :sswitch_data_0

    .line 2132518
    invoke-virtual {p0, p1, v4, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 2132519
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 2132520
    goto :goto_0

    .line 2132521
    :sswitch_1
    iget v0, p0, LX/EX6;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EX6;->bitField0_:I

    .line 2132522
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EX6;->name_:Ljava/lang/Object;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2132523
    :catch_0
    move-exception v0

    .line 2132524
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2132525
    move-object v0, v0

    .line 2132526
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2132527
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EX6;->unknownFields:LX/EZQ;

    .line 2132528
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2132529
    :sswitch_2
    :try_start_2
    iget v0, p0, LX/EX6;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EX6;->bitField0_:I

    .line 2132530
    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v0

    iput v0, p0, LX/EX6;->number_:I
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2132531
    :catch_1
    move-exception v0

    .line 2132532
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2132533
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2132534
    move-object v0, v1

    .line 2132535
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2132536
    :sswitch_3
    const/4 v0, 0x0

    .line 2132537
    :try_start_4
    iget v1, p0, LX/EX6;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v5, 0x4

    if-ne v1, v5, :cond_3

    .line 2132538
    iget-object v0, p0, LX/EX6;->options_:LX/EXA;

    invoke-virtual {v0}, LX/EXA;->j()LX/EX9;

    move-result-object v0

    move-object v1, v0

    .line 2132539
    :goto_1
    sget-object v0, LX/EXA;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EXA;

    iput-object v0, p0, LX/EX6;->options_:LX/EXA;

    .line 2132540
    if-eqz v1, :cond_1

    .line 2132541
    iget-object v0, p0, LX/EX6;->options_:LX/EXA;

    invoke-virtual {v1, v0}, LX/EX9;->a(LX/EXA;)LX/EX9;

    .line 2132542
    invoke-virtual {v1}, LX/EX9;->l()LX/EXA;

    move-result-object v0

    iput-object v0, p0, LX/EX6;->options_:LX/EXA;

    .line 2132543
    :cond_1
    iget v0, p0, LX/EX6;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EX6;->bitField0_:I
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2132544
    :cond_2
    invoke-virtual {v4}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EX6;->unknownFields:LX/EZQ;

    .line 2132545
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2132546
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2132547
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2132548
    iput-byte v1, p0, LX/EX6;->memoizedIsInitialized:B

    .line 2132549
    iput v1, p0, LX/EX6;->memoizedSerializedSize:I

    .line 2132550
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EX6;->unknownFields:LX/EZQ;

    .line 2132551
    return-void
.end method

.method private static b(LX/EX6;)LX/EX5;
    .locals 1

    .prologue
    .line 2132552
    invoke-static {}, LX/EX5;->n()LX/EX5;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EX5;->a(LX/EX6;)LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method private q()LX/EWc;
    .locals 2

    .prologue
    .line 2132553
    iget-object v0, p0, LX/EX6;->name_:Ljava/lang/Object;

    .line 2132554
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2132555
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2132556
    iput-object v0, p0, LX/EX6;->name_:Ljava/lang/Object;

    .line 2132557
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private r()V
    .locals 1

    .prologue
    .line 2132558
    const-string v0, ""

    iput-object v0, p0, LX/EX6;->name_:Ljava/lang/Object;

    .line 2132559
    const/4 v0, 0x0

    iput v0, p0, LX/EX6;->number_:I

    .line 2132560
    sget-object v0, LX/EXA;->c:LX/EXA;

    move-object v0, v0

    .line 2132561
    iput-object v0, p0, LX/EX6;->options_:LX/EXA;

    .line 2132562
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2132563
    new-instance v0, LX/EX5;

    invoke-direct {v0, p1}, LX/EX5;-><init>(LX/EYd;)V

    .line 2132564
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2132481
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2132482
    iget v0, p0, LX/EX6;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2132483
    invoke-direct {p0}, LX/EX6;->q()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2132484
    :cond_0
    iget v0, p0, LX/EX6;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2132485
    iget v0, p0, LX/EX6;->number_:I

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(II)V

    .line 2132486
    :cond_1
    iget v0, p0, LX/EX6;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 2132487
    const/4 v0, 0x3

    iget-object v1, p0, LX/EX6;->options_:LX/EXA;

    invoke-virtual {p1, v0, v1}, LX/EWf;->b(ILX/EWW;)V

    .line 2132488
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2132489
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2132490
    iget-byte v2, p0, LX/EX6;->memoizedIsInitialized:B

    .line 2132491
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    .line 2132492
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 2132493
    goto :goto_0

    .line 2132494
    :cond_1
    invoke-virtual {p0}, LX/EX6;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2132495
    iget-object v2, p0, LX/EX6;->options_:LX/EXA;

    move-object v2, v2

    .line 2132496
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2132497
    iput-byte v1, p0, LX/EX6;->memoizedIsInitialized:B

    move v0, v1

    .line 2132498
    goto :goto_0

    .line 2132499
    :cond_2
    iput-byte v0, p0, LX/EX6;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2132469
    iget v0, p0, LX/EX6;->memoizedSerializedSize:I

    .line 2132470
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2132471
    :goto_0
    return v0

    .line 2132472
    :cond_0
    const/4 v0, 0x0

    .line 2132473
    iget v1, p0, LX/EX6;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2132474
    invoke-direct {p0}, LX/EX6;->q()LX/EWc;

    move-result-object v0

    invoke-static {v2, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2132475
    :cond_1
    iget v1, p0, LX/EX6;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2132476
    iget v1, p0, LX/EX6;->number_:I

    invoke-static {v3, v1}, LX/EWf;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2132477
    :cond_2
    iget v1, p0, LX/EX6;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 2132478
    const/4 v1, 0x3

    iget-object v2, p0, LX/EX6;->options_:LX/EXA;

    invoke-static {v1, v2}, LX/EWf;->e(ILX/EWW;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2132479
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2132480
    iput v0, p0, LX/EX6;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2132468
    iget-object v0, p0, LX/EX6;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2132467
    sget-object v0, LX/EYC;->n:LX/EYn;

    const-class v1, LX/EX6;

    const-class v2, LX/EX5;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EX6;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2132466
    sget-object v0, LX/EX6;->a:LX/EWZ;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2132457
    iget-object v0, p0, LX/EX6;->name_:Ljava/lang/Object;

    .line 2132458
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2132459
    check-cast v0, Ljava/lang/String;

    .line 2132460
    :goto_0
    return-object v0

    .line 2132461
    :cond_0
    check-cast v0, LX/EWc;

    .line 2132462
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2132463
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2132464
    iput-object v1, p0, LX/EX6;->name_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2132465
    goto :goto_0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 2132456
    iget v0, p0, LX/EX6;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2132455
    invoke-static {p0}, LX/EX6;->b(LX/EX6;)LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2132454
    invoke-static {}, LX/EX5;->n()LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2132453
    invoke-static {p0}, LX/EX6;->b(LX/EX6;)LX/EX5;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2132452
    sget-object v0, LX/EX6;->c:LX/EX6;

    return-object v0
.end method
