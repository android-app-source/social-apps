.class public LX/Enb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EnY;


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2167281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2167282
    iget-object v0, p0, LX/Enb;->a:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Called getTitleHeight before initializing"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2167283
    iget-object v0, p0, LX/Enb;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    return v0

    .line 2167284
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 2167285
    const/high16 v0, 0x40c00000    # 6.0f

    mul-float/2addr v0, p1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2167286
    iget-object v1, p0, LX/Enb;->a:Lcom/facebook/resources/ui/FbTextView;

    neg-float v0, v0

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 2167287
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2167288
    const v0, 0x7f0d0d92

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Enb;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2167289
    if-eqz p2, :cond_0

    .line 2167290
    invoke-virtual {p0, p2}, LX/Enb;->a(Ljava/lang/String;)V

    .line 2167291
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2167292
    iget-object v0, p0, LX/Enb;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2167293
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2167294
    const/4 v0, 0x0

    return v0
.end method
