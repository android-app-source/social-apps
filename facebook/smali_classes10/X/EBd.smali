.class public final LX/EBd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/39A;


# instance fields
.field public final synthetic a:Lcom/facebook/rtc/helpers/RtcCallStartParams;

.field public final synthetic b:Lcom/facebook/rtc/activities/RtcZeroRatingActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/activities/RtcZeroRatingActivity;Lcom/facebook/rtc/helpers/RtcCallStartParams;)V
    .locals 0

    .prologue
    .line 2087710
    iput-object p1, p0, LX/EBd;->b:Lcom/facebook/rtc/activities/RtcZeroRatingActivity;

    iput-object p2, p0, LX/EBd;->a:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2087711
    iget-object v0, p0, LX/EBd;->b:Lcom/facebook/rtc/activities/RtcZeroRatingActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->q:LX/2S7;

    iget-object v1, p0, LX/EBd;->a:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-wide v2, v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/EBd;->a:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-object v2, v2, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    iget-object v3, p0, LX/EBd;->a:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-boolean v3, v3, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    sget-object v4, LX/79O;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 2087712
    iget-object v0, p0, LX/EBd;->b:Lcom/facebook/rtc/activities/RtcZeroRatingActivity;

    invoke-virtual {v0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2087713
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2087714
    iget-object v0, p0, LX/EBd;->b:Lcom/facebook/rtc/activities/RtcZeroRatingActivity;

    iget-object v0, v0, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->p:LX/3A0;

    iget-object v1, p0, LX/EBd;->a:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/3A0;->a(Lcom/facebook/rtc/helpers/RtcCallStartParams;Z)V

    .line 2087715
    iget-object v0, p0, LX/EBd;->b:Lcom/facebook/rtc/activities/RtcZeroRatingActivity;

    invoke-virtual {v0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2087716
    return-void
.end method
