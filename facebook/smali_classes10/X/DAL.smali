.class public LX/DAL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "LX/0Px",
        "<",
        "Lcom/facebook/contactlogs/data/ContactLogMetadata;",
        ">;>;",
        "LX/DAK;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/DAE;


# direct methods
.method public constructor <init>(LX/DAE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1971070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1971071
    iput-object p1, p0, LX/DAL;->a:LX/DAE;

    .line 1971072
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1971073
    check-cast p1, LX/0P1;

    .line 1971074
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "MobileUploadContactLog"

    .line 1971075
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1971076
    move-object v0, v0

    .line 1971077
    const-string v1, "POST"

    .line 1971078
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1971079
    move-object v0, v0

    .line 1971080
    const-string v1, "method/friendFinder.mobilecalllogsync"

    .line 1971081
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1971082
    move-object v0, v0

    .line 1971083
    iget-object v1, p0, LX/DAL;->a:LX/DAE;

    .line 1971084
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1971085
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "format"

    const-string p0, "json"

    invoke-direct {v2, v4, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1971086
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string p0, "call_logs"

    const-string v2, "call_logs"

    invoke-virtual {p1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    invoke-static {v1, v2}, LX/DAE;->a(LX/DAE;LX/0Px;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, p0, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1971087
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string p0, "mms_logs"

    const-string v2, "mms_logs"

    invoke-virtual {p1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    invoke-static {v1, v2}, LX/DAE;->a(LX/DAE;LX/0Px;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, p0, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1971088
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string p0, "sms_logs"

    const-string v2, "sms_logs"

    invoke-virtual {p1, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    invoke-static {v1, v2}, LX/DAE;->a(LX/DAE;LX/0Px;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, p0, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1971089
    move-object v1, v3

    .line 1971090
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1971091
    move-object v0, v0

    .line 1971092
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1971093
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1971094
    move-object v0, v0

    .line 1971095
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1971096
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1971097
    const-string v1, "server_status"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1971098
    sget-object v1, LX/DAK;->UNKNOWN:LX/DAK;

    invoke-virtual {v1}, LX/DAK;->ordinal()I

    move-result v1

    invoke-static {v0, v1}, LX/16N;->a(LX/0lF;I)I

    move-result v0

    .line 1971099
    invoke-static {}, LX/DAK;->values()[LX/DAK;

    move-result-object v1

    aget-object v0, v1, v0

    return-object v0
.end method
