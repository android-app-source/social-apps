.class public final LX/Dx4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dx6;


# direct methods
.method public constructor <init>(LX/Dx6;)V
    .locals 0

    .prologue
    .line 2062338
    iput-object p1, p0, LX/Dx4;->a:LX/Dx6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2062339
    const/4 v3, 0x0

    .line 2062340
    iget-object v0, p0, LX/Dx4;->a:LX/Dx6;

    iget-boolean v0, v0, LX/Dvb;->m:Z

    if-nez v0, :cond_2

    move-object v4, v3

    .line 2062341
    :goto_0
    iget-object v0, p0, LX/Dx4;->a:LX/Dx6;

    iget-object v0, v0, LX/Dx6;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DvH;

    iget-object v1, p0, LX/Dx4;->a:LX/Dx6;

    iget-object v1, v1, LX/Dvb;->j:Ljava/lang/String;

    iget-object v2, p0, LX/Dx4;->a:LX/Dx6;

    iget-object v2, v2, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget-object v5, p0, LX/Dx4;->a:LX/Dx6;

    .line 2062342
    invoke-virtual {v5}, LX/Dvb;->b()Z

    move-result v6

    move v5, v6

    .line 2062343
    if-eqz v5, :cond_3

    const/16 v5, 0x1e

    :goto_1
    iget-object v6, p0, LX/Dx4;->a:LX/Dx6;

    .line 2062344
    invoke-virtual {v6}, LX/Dvb;->i()Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    move-result-object p0

    move-object v6, p0

    .line 2062345
    new-instance v7, LX/8IC;

    invoke-direct {v7}, LX/8IC;-><init>()V

    move-object v8, v7

    .line 2062346
    const-string v7, "node_id"

    invoke-virtual {v8, v7, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2062347
    const-string v7, "count"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2062348
    const-string v9, "automatic_photo_captioning_enabled"

    iget-object v7, v0, LX/DvH;->g:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0sX;

    invoke-virtual {v7}, LX/0sX;->a()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v9, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2062349
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 2062350
    const-string v7, "before"

    invoke-virtual {v8, v7, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2062351
    :cond_0
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2062352
    const-string v7, "after"

    invoke-virtual {v8, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2062353
    :cond_1
    iget-object v7, v0, LX/DvH;->f:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/DvI;

    .line 2062354
    iget-object v9, v7, LX/DvI;->c:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0se;

    invoke-virtual {v9, v8}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 2062355
    const-string v9, "image_thumbnail_width"

    sget v10, LX/DvI;->d:I

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "image_large_thumbnail_width"

    sget v11, LX/DvI;->e:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "image_portrait_width"

    sget v11, LX/DvI;->f:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "image_portrait_height"

    sget v11, LX/DvI;->g:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "image_landscape_width"

    sget v11, LX/DvI;->h:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "image_landscape_height"

    sget v11, LX/DvI;->i:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "large_portrait_height"

    sget v11, LX/DvI;->j:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v10

    const-string v11, "media_type"

    iget-object v9, v7, LX/DvI;->b:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0rq;

    invoke-virtual {v9}, LX/0rq;->a()LX/0wF;

    move-result-object v9

    invoke-virtual {v10, v11, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2062356
    iget-object v7, v0, LX/DvH;->h:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0ad;

    sget-short v9, LX/5lr;->d:S

    const/4 v10, 0x0

    invoke-interface {v7, v9, v10}, LX/0ad;->a(SZ)Z

    move-result v9

    .line 2062357
    iget-object v7, v0, LX/DvH;->h:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0ad;

    sget v10, LX/5lr;->e:I

    const v11, 0x15180

    invoke-interface {v7, v10, v11}, LX/0ad;->a(II)I

    move-result v10

    .line 2062358
    iget-object v7, v0, LX/DvH;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v11

    if-eqz v9, :cond_5

    sget-object v8, LX/0zS;->a:LX/0zS;

    :goto_2
    invoke-virtual {v11, v8}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v8

    int-to-long v9, v10

    invoke-virtual {v8, v9, v10}, LX/0zO;->a(J)LX/0zO;

    move-result-object v8

    sget-object v9, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v8, v9}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v9

    .line 2062359
    iget-object v7, v0, LX/DvH;->c:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0QK;

    iget-object v8, v0, LX/DvH;->a:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {v9, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 2062360
    iget-object v7, v0, LX/DvH;->e:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/DvF;

    iget-object v8, v0, LX/DvH;->a:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {v9, v7, v6, v8}, LX/DvG;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/DvF;Ljava/lang/Object;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 2062361
    iget-object v7, v0, LX/DvH;->d:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/DvF;

    new-instance v10, LX/DvK;

    sget-object v8, LX/DvW;->TAGGED_MEDIA_SET:LX/DvW;

    invoke-direct {v10, v2, v8}, LX/DvK;-><init>(Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;)V

    iget-object v8, v0, LX/DvH;->a:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {v9, v7, v10, v8}, LX/DvG;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/DvF;Ljava/lang/Object;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 2062362
    move-object v0, v7

    .line 2062363
    return-object v0

    .line 2062364
    :cond_2
    iget-object v0, p0, LX/Dx4;->a:LX/Dx6;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    if-eqz v0, :cond_4

    .line 2062365
    iget-object v0, p0, LX/Dx4;->a:LX/Dx6;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    .line 2062366
    iget-object v1, v0, LX/Dv0;->d:Ljava/lang/String;

    move-object v4, v1

    .line 2062367
    goto/16 :goto_0

    .line 2062368
    :cond_3
    const/16 v5, 0xc

    goto/16 :goto_1

    :cond_4
    move-object v4, v3

    goto/16 :goto_0

    .line 2062369
    :cond_5
    sget-object v8, LX/0zS;->c:LX/0zS;

    goto :goto_2
.end method
