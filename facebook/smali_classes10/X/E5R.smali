.class public final LX/E5R;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/E5T;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/E5S;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2078397
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2078398
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "message"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/E5R;->b:[Ljava/lang/String;

    .line 2078399
    iput v3, p0, LX/E5R;->c:I

    .line 2078400
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/E5R;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/E5R;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/E5R;LX/1De;IILX/E5S;)V
    .locals 1

    .prologue
    .line 2078374
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2078375
    iput-object p4, p0, LX/E5R;->a:LX/E5S;

    .line 2078376
    iget-object v0, p0, LX/E5R;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2078377
    return-void
.end method


# virtual methods
.method public final a(LX/174;)LX/E5R;
    .locals 1

    .prologue
    .line 2078378
    iget-object v0, p0, LX/E5R;->a:LX/E5S;

    iput-object p1, v0, LX/E5S;->c:LX/174;

    .line 2078379
    return-object p0
.end method

.method public final a(Landroid/text/SpannableStringBuilder;)LX/E5R;
    .locals 2

    .prologue
    .line 2078380
    iget-object v0, p0, LX/E5R;->a:LX/E5S;

    iput-object p1, v0, LX/E5S;->a:Landroid/text/SpannableStringBuilder;

    .line 2078381
    iget-object v0, p0, LX/E5R;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2078382
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2078383
    invoke-super {p0}, LX/1X5;->a()V

    .line 2078384
    const/4 v0, 0x0

    iput-object v0, p0, LX/E5R;->a:LX/E5S;

    .line 2078385
    sget-object v0, LX/E5T;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2078386
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/E5T;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2078387
    iget-object v1, p0, LX/E5R;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/E5R;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/E5R;->c:I

    if-ge v1, v2, :cond_2

    .line 2078388
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2078389
    :goto_0
    iget v2, p0, LX/E5R;->c:I

    if-ge v0, v2, :cond_1

    .line 2078390
    iget-object v2, p0, LX/E5R;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2078391
    iget-object v2, p0, LX/E5R;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2078392
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2078393
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2078394
    :cond_2
    iget-object v0, p0, LX/E5R;->a:LX/E5S;

    .line 2078395
    invoke-virtual {p0}, LX/E5R;->a()V

    .line 2078396
    return-object v0
.end method
