.class public final enum LX/CiA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CiA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CiA;

.field public static final enum REGISTER:LX/CiA;

.field public static final enum UNREGISTER:LX/CiA;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1928724
    new-instance v0, LX/CiA;

    const-string v1, "REGISTER"

    invoke-direct {v0, v1, v2}, LX/CiA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiA;->REGISTER:LX/CiA;

    .line 1928725
    new-instance v0, LX/CiA;

    const-string v1, "UNREGISTER"

    invoke-direct {v0, v1, v3}, LX/CiA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiA;->UNREGISTER:LX/CiA;

    .line 1928726
    const/4 v0, 0x2

    new-array v0, v0, [LX/CiA;

    sget-object v1, LX/CiA;->REGISTER:LX/CiA;

    aput-object v1, v0, v2

    sget-object v1, LX/CiA;->UNREGISTER:LX/CiA;

    aput-object v1, v0, v3

    sput-object v0, LX/CiA;->$VALUES:[LX/CiA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1928721
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CiA;
    .locals 1

    .prologue
    .line 1928723
    const-class v0, LX/CiA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CiA;

    return-object v0
.end method

.method public static values()[LX/CiA;
    .locals 1

    .prologue
    .line 1928722
    sget-object v0, LX/CiA;->$VALUES:[LX/CiA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CiA;

    return-object v0
.end method
