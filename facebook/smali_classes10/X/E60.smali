.class public final LX/E60;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E67;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;LX/E67;LX/1Pn;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 2079391
    iput-object p1, p0, LX/E60;->d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;

    iput-object p2, p0, LX/E60;->a:LX/E67;

    iput-object p3, p0, LX/E60;->b:LX/1Pn;

    iput-object p4, p0, LX/E60;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const v0, 0x76ce2c9f

    invoke-static {v6, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2079392
    iget-object v0, p0, LX/E60;->d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition$1$1;

    invoke-direct {v2, p0}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionHideRichNotifActionPartDefinition$1$1;-><init>(LX/E60;)V

    const-wide/16 v4, 0x7d0

    const v3, 0x537247e8

    invoke-static {v0, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2079393
    iget-object v0, p0, LX/E60;->b:LX/1Pn;

    check-cast v0, LX/1Pq;

    new-array v2, v7, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    iget-object v4, p0, LX/E60;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2079394
    iget-object v0, p0, LX/E60;->b:LX/1Pn;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    iget-object v2, p0, LX/E60;->a:LX/E67;

    iget-object v2, v2, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079395
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2079396
    iget-object v3, p0, LX/E60;->a:LX/E67;

    iget-object v3, v3, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079397
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2079398
    iget-object v4, p0, LX/E60;->a:LX/E67;

    iget-object v4, v4, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079399
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v5

    .line 2079400
    invoke-interface {v4}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/Cfc;->UNHIGHLIGHT_RICH_NOTIFICATION_TAP:LX/Cfc;

    invoke-virtual {v0, v2, v3, v4, v5}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    .line 2079401
    const v0, -0x4ca27708

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
