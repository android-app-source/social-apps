.class public LX/DcF;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DcF;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018121
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2018122
    sget-object v0, LX/0ax;->eB:Ljava/lang/String;

    const-string v1, "{com.facebook.katana.profile.id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->PAGE_MENU_MANAGEMENT_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2018123
    sget-object v0, LX/0ax;->eC:Ljava/lang/String;

    const-string v1, "{com.facebook.katana.profile.id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->PAGE_MENU_MANAGEMENT_LINK_MENU_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2018124
    return-void
.end method

.method public static a(LX/0QB;)LX/DcF;
    .locals 3

    .prologue
    .line 2018109
    sget-object v0, LX/DcF;->a:LX/DcF;

    if-nez v0, :cond_1

    .line 2018110
    const-class v1, LX/DcF;

    monitor-enter v1

    .line 2018111
    :try_start_0
    sget-object v0, LX/DcF;->a:LX/DcF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2018112
    if-eqz v2, :cond_0

    .line 2018113
    :try_start_1
    new-instance v0, LX/DcF;

    invoke-direct {v0}, LX/DcF;-><init>()V

    .line 2018114
    move-object v0, v0

    .line 2018115
    sput-object v0, LX/DcF;->a:LX/DcF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2018116
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2018117
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2018118
    :cond_1
    sget-object v0, LX/DcF;->a:LX/DcF;

    return-object v0

    .line 2018119
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2018120
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
