.class public final LX/DK0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1986234
    iput-object p1, p0, LX/DK0;->b:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    iput-object p2, p0, LX/DK0;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1986235
    iget-object v0, p0, LX/DK0;->b:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    iget-object v0, v0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->c:LX/03V;

    const-string v1, "CheckedPostToMarketplaceTextView"

    const-string v2, "Couldn\'t complete FetchMarketplaceCrossPostShowSettingsQuery"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1986236
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1986237
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1986238
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1986239
    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchMarketplaceCrossPostShowSettingsGraphQLModels$FetchMarketplaceCrossPostShowSettingsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2

    .line 1986240
    if-nez v0, :cond_1

    .line 1986241
    :cond_0
    return-void

    .line 1986242
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1986243
    :cond_1
    const v2, -0x245fec8f

    invoke-static {v1, v0, v4, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1986244
    invoke-virtual {v2, v1, v4}, LX/15i;->g(II)I

    move-result v3

    if-eqz v3, :cond_2

    .line 1986245
    invoke-virtual {v2, v1, v4}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v2, v1, v4}, LX/15i;->g(II)I

    move-result v1

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1986246
    const/4 v3, 0x2

    invoke-virtual {v2, v1, v3}, LX/15i;->h(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1986247
    iget-object v1, p0, LX/DK0;->b:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    iget-object v2, p0, LX/DK0;->a:Ljava/lang/String;

    .line 1986248
    invoke-static {v1, v2}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a$redex0(Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;Ljava/lang/String;)V

    .line 1986249
    goto :goto_1

    .line 1986250
    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 1986251
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
