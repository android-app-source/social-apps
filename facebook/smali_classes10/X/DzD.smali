.class public final LX/DzD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DzG;


# direct methods
.method public constructor <init>(LX/DzG;)V
    .locals 0

    .prologue
    .line 2066508
    iput-object p1, p0, LX/DzD;->a:LX/DzG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x2

    const v0, 0x2e6a14a3

    invoke-static {v2, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2066509
    iget-object v0, p0, LX/DzD;->a:LX/DzG;

    iget-object v0, v0, LX/DzG;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2066510
    if-nez v0, :cond_0

    .line 2066511
    const v0, -0x14a6e998

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2066512
    :goto_0
    return-void

    .line 2066513
    :cond_0
    iget-object v2, p0, LX/DzD;->a:LX/DzG;

    const-string v3, "niem_permission_settings_click"

    invoke-static {v2, v3}, LX/DzG;->a$redex0(LX/DzG;Ljava/lang/String;)V

    .line 2066514
    iget-object v2, p0, LX/DzD;->a:LX/DzG;

    iget-object v2, v2, LX/DzG;->h:LX/0i5;

    if-nez v2, :cond_1

    .line 2066515
    iget-object v2, p0, LX/DzD;->a:LX/DzG;

    iget-object v3, p0, LX/DzD;->a:LX/DzG;

    iget-object v3, v3, LX/DzG;->g:LX/0i4;

    invoke-virtual {v3, v0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v3

    .line 2066516
    iput-object v3, v2, LX/DzG;->h:LX/0i5;

    .line 2066517
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2066518
    iget-object v2, p0, LX/DzD;->a:LX/DzG;

    const v3, 0x7f081764

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2066519
    iput-object v3, v2, LX/DzG;->j:Ljava/lang/String;

    .line 2066520
    iget-object v2, p0, LX/DzD;->a:LX/DzG;

    const v3, 0x7f081765

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/DzD;->a:LX/DzG;

    iget-object v6, v6, LX/DzG;->u:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2066521
    iput-object v0, v2, LX/DzG;->k:Ljava/lang/String;

    .line 2066522
    :cond_1
    iget-object v0, p0, LX/DzD;->a:LX/DzG;

    iget-object v0, v0, LX/DzG;->h:LX/0i5;

    sget-object v2, LX/DzG;->f:[Ljava/lang/String;

    iget-object v3, p0, LX/DzD;->a:LX/DzG;

    iget-object v3, v3, LX/DzG;->j:Ljava/lang/String;

    iget-object v4, p0, LX/DzD;->a:LX/DzG;

    iget-object v4, v4, LX/DzG;->k:Ljava/lang/String;

    iget-object v5, p0, LX/DzD;->a:LX/DzG;

    invoke-virtual {v0, v2, v3, v4, v5}, LX/0i5;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Zx;)V

    .line 2066523
    const v0, 0x23b59195

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
