.class public LX/E1z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/E1z;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2072038
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;)I
    .locals 1

    .prologue
    .line 2072039
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    if-ne p0, v0, :cond_0

    .line 2072040
    const v0, 0x7f0b1689

    .line 2072041
    :goto_0
    return v0

    .line 2072042
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    if-ne p0, v0, :cond_1

    .line 2072043
    const v0, 0x7f0b168a

    goto :goto_0

    .line 2072044
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    if-ne p0, v0, :cond_2

    .line 2072045
    const v0, 0x7f0b168b

    goto :goto_0

    .line 2072046
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    if-ne p0, v0, :cond_3

    .line 2072047
    const v0, 0x7f0b168c

    goto :goto_0

    .line 2072048
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    if-ne p0, v0, :cond_4

    .line 2072049
    const v0, 0x7f0b168d

    goto :goto_0

    .line 2072050
    :cond_4
    const v0, 0x7f0b1688

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/E1z;
    .locals 3

    .prologue
    .line 2072051
    sget-object v0, LX/E1z;->a:LX/E1z;

    if-nez v0, :cond_1

    .line 2072052
    const-class v1, LX/E1z;

    monitor-enter v1

    .line 2072053
    :try_start_0
    sget-object v0, LX/E1z;->a:LX/E1z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072054
    if-eqz v2, :cond_0

    .line 2072055
    :try_start_1
    new-instance v0, LX/E1z;

    invoke-direct {v0}, LX/E1z;-><init>()V

    .line 2072056
    move-object v0, v0

    .line 2072057
    sput-object v0, LX/E1z;->a:LX/E1z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072058
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072059
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072060
    :cond_1
    sget-object v0, LX/E1z;->a:LX/E1z;

    return-object v0

    .line 2072061
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072062
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/res/Resources;Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;)Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 2072063
    const v0, 0x7f0b0033

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v6, v0, -0x1

    .line 2072064
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    const v1, 0x7f0215c3

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v5

    :goto_0
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v5

    :goto_1
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v5

    :goto_2
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->a()Z

    move-result v7

    if-eqz v7, :cond_3

    :goto_3
    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    return-object v0

    :cond_0
    move v2, v6

    goto :goto_0

    :cond_1
    move v3, v6

    goto :goto_1

    :cond_2
    move v4, v6

    goto :goto_2

    :cond_3
    move v5, v6

    goto :goto_3
.end method
