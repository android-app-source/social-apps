.class public final LX/E3k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9uc;

.field public final synthetic b:LX/2km;

.field public final synthetic c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;LX/9uc;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2075250
    iput-object p1, p0, LX/E3k;->d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;

    iput-object p2, p0, LX/E3k;->a:LX/9uc;

    iput-object p3, p0, LX/E3k;->b:LX/2km;

    iput-object p4, p0, LX/E3k;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x4fd50e6e

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2075251
    iget-object v0, p0, LX/E3k;->a:LX/9uc;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v0

    .line 2075252
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel$FeedbackModel;

    move-result-object v2

    .line 2075253
    iget-object v3, p0, LX/E3k;->d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;

    iget-object v3, v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionImageStoryBlockUnitComponentPartDefinition;->a:LX/E1i;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel$FeedbackModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-object v2, LX/Cfc;->STORY_TAP:LX/Cfc;

    invoke-virtual {v3, v4, v5, v0, v2}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    .line 2075254
    iget-object v2, p0, LX/E3k;->b:LX/2km;

    iget-object v3, p0, LX/E3k;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075255
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v4

    .line 2075256
    iget-object v4, p0, LX/E3k;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075257
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2075258
    invoke-interface {v2, v3, v4, v0}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2075259
    const v0, 0x672131fe

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2075260
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
