.class public final LX/E6F;
.super Landroid/text/style/ImageSpan;
.source ""


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;I)V
    .locals 0

    .prologue
    .line 2079829
    invoke-direct {p0, p1}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 2079830
    iput p2, p0, LX/E6F;->a:I

    .line 2079831
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 3

    .prologue
    .line 2079832
    invoke-virtual {p0}, LX/E6F;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2079833
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2079834
    invoke-virtual {p9}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    .line 2079835
    iget v2, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int v1, v2, v1

    .line 2079836
    iget v2, p0, LX/E6F;->a:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 2079837
    int-to-float v1, v1

    invoke-virtual {p1, p5, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2079838
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2079839
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2079840
    return-void
.end method
