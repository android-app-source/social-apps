.class public final LX/EP9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Cxe;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic c:LX/EPA;


# direct methods
.method public constructor <init>(LX/EPA;LX/Cxe;Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 0

    .prologue
    .line 2115996
    iput-object p1, p0, LX/EP9;->c:LX/EPA;

    iput-object p2, p0, LX/EP9;->a:LX/Cxe;

    iput-object p3, p0, LX/EP9;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, -0x2266c70d

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2115997
    iget-object v0, p0, LX/EP9;->a:LX/Cxe;

    iget-object v1, p0, LX/EP9;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v0, v1}, LX/Cxe;->c(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 2115998
    iget-object v0, p0, LX/EP9;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2115999
    iget-object v0, p0, LX/EP9;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v4

    .line 2116000
    iget-object v0, p0, LX/EP9;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2116001
    iget-object v0, p0, LX/EP9;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->k()Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticleVersion;->j()Ljava/lang/String;

    move-result-object v5

    .line 2116002
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EP9;->c:LX/EPA;

    iget-object v0, v0, LX/EPA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EJ5;

    iget-object v1, p0, LX/EP9;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/EP9;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->cV()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/EP9;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, LX/EP9;->a:LX/Cxe;

    check-cast v6, LX/CxV;

    invoke-virtual/range {v0 .. v6}, LX/EJ5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CxV;)V

    .line 2116003
    const v0, 0x53d0eaac

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move-object v4, v5

    goto :goto_0
.end method
