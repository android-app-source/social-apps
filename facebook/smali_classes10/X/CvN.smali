.class public LX/CvN;
.super LX/1Cd;
.source ""


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/CvY;

.field private final c:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field private final d:LX/CzB;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CvU;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzB;)V
    .locals 1
    .param p3    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/CzB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1948134
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 1948135
    iput-object p1, p0, LX/CvN;->a:LX/0SG;

    .line 1948136
    iput-object p2, p0, LX/CvN;->b:LX/CvY;

    .line 1948137
    iput-object p3, p0, LX/CvN;->c:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1948138
    iput-object p4, p0, LX/CvN;->d:LX/CzB;

    .line 1948139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CvN;->e:Ljava/util/List;

    .line 1948140
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CvN;->f:Ljava/util/Map;

    .line 1948141
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1948142
    iget-object v0, p0, LX/CvN;->e:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1948143
    iget-object v1, p0, LX/CvN;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1948144
    iget-object v1, p0, LX/CvN;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1948145
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1948146
    iget-object v1, p0, LX/CvN;->b:LX/CvY;

    iget-object v2, p0, LX/CvN;->c:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v1, v2, v0}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;)V

    .line 1948147
    :cond_0
    return-void
.end method

.method private static c(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1948148
    instance-of v0, p0, LX/1Rk;

    if-eqz v0, :cond_0

    .line 1948149
    check-cast p0, LX/1Rk;

    .line 1948150
    iget-object v0, p0, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 1948151
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 1948152
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    if-eqz v1, :cond_0

    .line 1948153
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 1948154
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1948155
    invoke-static {p1}, LX/CvN;->c(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    move-result-object v0

    .line 1948156
    if-nez v0, :cond_0

    .line 1948157
    :goto_0
    return-void

    .line 1948158
    :cond_0
    iget-object v1, p0, LX/CvN;->f:Ljava/util/Map;

    iget-object v2, p0, LX/CvN;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(LX/0g8;)V
    .locals 0

    .prologue
    .line 1948159
    invoke-direct {p0}, LX/CvN;->a()V

    .line 1948160
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 14

    .prologue
    const/4 v9, 0x0

    .line 1948161
    invoke-static {p1}, LX/CvN;->c(Ljava/lang/Object;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    move-result-object v10

    .line 1948162
    if-nez v10, :cond_1

    .line 1948163
    :cond_0
    :goto_0
    return-void

    .line 1948164
    :cond_1
    iget-object v0, p0, LX/CvN;->f:Ljava/util/Map;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1948165
    if-eqz v0, :cond_0

    .line 1948166
    iget-object v1, p0, LX/CvN;->f:Ljava/util/Map;

    invoke-interface {v1, v10}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948167
    iget-object v1, p0, LX/CvN;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v6, v2, v4

    .line 1948168
    const-wide/16 v2, 0xa

    cmp-long v1, v6, v2

    if-ltz v1, :cond_0

    .line 1948169
    new-instance v1, LX/CvU;

    iget-object v2, p0, LX/CvN;->d:LX/CzB;

    invoke-virtual {v2, v10}, LX/CzB;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;)I

    move-result v2

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    .line 1948170
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v13, v0

    .line 1948171
    move-object v10, v9

    move-object v11, v9

    invoke-direct/range {v1 .. v13}, LX/CvU;-><init>(ILjava/lang/String;JJLX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0P1;)V

    .line 1948172
    iget-object v0, p0, LX/CvN;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1948173
    iget-object v0, p0, LX/CvN;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_0

    .line 1948174
    invoke-direct {p0}, LX/CvN;->a()V

    goto :goto_0
.end method
