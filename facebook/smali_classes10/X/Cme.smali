.class public LX/Cme;
.super LX/Cm9;
.source ""

# interfaces
.implements LX/Cm3;


# instance fields
.field private final a:LX/Clo;

.field private final b:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field private final c:Z


# direct methods
.method public constructor <init>(LX/Cmd;)V
    .locals 1

    .prologue
    .line 1933498
    invoke-direct {p0, p1}, LX/Cm9;-><init>(LX/Cm8;)V

    .line 1933499
    iget-object v0, p1, LX/Cmd;->a:LX/Clo;

    iput-object v0, p0, LX/Cme;->a:LX/Clo;

    .line 1933500
    iget-object v0, p1, LX/Cmd;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, LX/Cme;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1933501
    iget-boolean v0, p1, LX/Cmd;->c:Z

    iput-boolean v0, p0, LX/Cme;->c:Z

    .line 1933502
    return-void
.end method


# virtual methods
.method public final a()LX/Clo;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933503
    iget-object v0, p0, LX/Cme;->a:LX/Clo;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 1933504
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->SLIDESHOW:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1933505
    iget-boolean v0, p0, LX/Cme;->c:Z

    return v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933506
    iget-object v0, p0, LX/Cme;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method
