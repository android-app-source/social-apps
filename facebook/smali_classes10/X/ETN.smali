.class public LX/ETN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7zb;
.implements LX/7zg;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile q:LX/ETN;


# instance fields
.field public final b:LX/19w;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:Landroid/content/Context;

.field public final e:LX/ETL;

.field public final f:LX/16U;

.field public final g:LX/ETM;

.field public h:LX/1Aa;

.field private i:LX/2xj;

.field private j:LX/0oB;

.field public k:Lcom/facebook/video/videohome/data/VideoHomeItem;

.field public l:Z

.field public m:Z

.field public n:Z

.field public final o:Ljava/lang/Runnable;

.field public final p:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2124829
    const-class v0, LX/ETN;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ETN;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/16U;LX/19w;Ljava/util/concurrent/Executor;LX/2xj;)V
    .locals 2
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2124746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2124747
    new-instance v0, LX/ETL;

    invoke-direct {v0, p0}, LX/ETL;-><init>(LX/ETN;)V

    iput-object v0, p0, LX/ETN;->e:LX/ETL;

    .line 2124748
    new-instance v0, LX/ETM;

    invoke-direct {v0, p0}, LX/ETM;-><init>(LX/ETN;)V

    iput-object v0, p0, LX/ETN;->g:LX/ETM;

    .line 2124749
    new-instance v0, Lcom/facebook/video/videohome/data/VideoHomeDownloadedSectionHelper$1;

    invoke-direct {v0, p0}, Lcom/facebook/video/videohome/data/VideoHomeDownloadedSectionHelper$1;-><init>(LX/ETN;)V

    iput-object v0, p0, LX/ETN;->o:Ljava/lang/Runnable;

    .line 2124750
    new-instance v0, Lcom/facebook/video/videohome/data/VideoHomeDownloadedSectionHelper$2;

    invoke-direct {v0, p0}, Lcom/facebook/video/videohome/data/VideoHomeDownloadedSectionHelper$2;-><init>(LX/ETN;)V

    iput-object v0, p0, LX/ETN;->p:Ljava/lang/Runnable;

    .line 2124751
    iput-object p1, p0, LX/ETN;->d:Landroid/content/Context;

    .line 2124752
    iput-object p2, p0, LX/ETN;->f:LX/16U;

    .line 2124753
    iput-object p3, p0, LX/ETN;->b:LX/19w;

    .line 2124754
    iput-object p4, p0, LX/ETN;->c:Ljava/util/concurrent/Executor;

    .line 2124755
    iput-object p5, p0, LX/ETN;->i:LX/2xj;

    .line 2124756
    new-instance v0, LX/ETI;

    invoke-direct {v0, p0}, LX/ETI;-><init>(LX/ETN;)V

    iput-object v0, p0, LX/ETN;->j:LX/0oB;

    .line 2124757
    iget-object v0, p0, LX/ETN;->i:LX/2xj;

    iget-object v1, p0, LX/ETN;->j:LX/0oB;

    invoke-virtual {v0, v1}, LX/2xj;->a(LX/0oB;)V

    .line 2124758
    return-void
.end method

.method public static a(LX/0QB;)LX/ETN;
    .locals 9

    .prologue
    .line 2124816
    sget-object v0, LX/ETN;->q:LX/ETN;

    if-nez v0, :cond_1

    .line 2124817
    const-class v1, LX/ETN;

    monitor-enter v1

    .line 2124818
    :try_start_0
    sget-object v0, LX/ETN;->q:LX/ETN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2124819
    if-eqz v2, :cond_0

    .line 2124820
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2124821
    new-instance v3, LX/ETN;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/16U;->a(LX/0QB;)LX/16U;

    move-result-object v5

    check-cast v5, LX/16U;

    invoke-static {v0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v6

    check-cast v6, LX/19w;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v8

    check-cast v8, LX/2xj;

    invoke-direct/range {v3 .. v8}, LX/ETN;-><init>(Landroid/content/Context;LX/16U;LX/19w;Ljava/util/concurrent/Executor;LX/2xj;)V

    .line 2124822
    move-object v0, v3

    .line 2124823
    sput-object v0, LX/ETN;->q:LX/ETN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2124824
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2124825
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2124826
    :cond_1
    sget-object v0, LX/ETN;->q:LX/ETN;

    return-object v0

    .line 2124827
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2124828
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;"
        }
    .end annotation

    .prologue
    .line 2124795
    new-instance v1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    new-instance v0, LX/9vz;

    invoke-direct {v0}, LX/9vz;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_EM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2124796
    iput-object v2, v0, LX/9vz;->J:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2124797
    move-object v2, v0

    .line 2124798
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2124799
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v0

    .line 2124800
    iput-object v0, v2, LX/9vz;->K:Ljava/lang/String;

    .line 2124801
    move-object v2, v2

    .line 2124802
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2124803
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2124804
    iput-object v0, v2, LX/9vz;->ap:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2124805
    move-object v0, v2

    .line 2124806
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->NONE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 2124807
    iput-object v2, v0, LX/9vz;->cg:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 2124808
    move-object v0, v0

    .line 2124809
    new-instance v2, LX/5t9;

    invoke-direct {v2}, LX/5t9;-><init>()V

    invoke-static {p0}, LX/15V;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v3

    .line 2124810
    iput-object v3, v2, LX/5t9;->b:Ljava/lang/String;

    .line 2124811
    move-object v2, v2

    .line 2124812
    invoke-virtual {v2}, LX/5t9;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel;

    move-result-object v2

    .line 2124813
    iput-object v2, v0, LX/9vz;->ci:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionTextWithEntitiesWithImagesModel;

    .line 2124814
    move-object v0, v0

    .line 2124815
    invoke-virtual {v0}, LX/9vz;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v0

    const-string v2, "download-section-id"

    const-string v3, "download-type-token"

    invoke-direct {v1, v0, v2, v3}, Lcom/facebook/video/videohome/data/VideoHomeItem;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2124793
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/ETN;->n:Z

    .line 2124794
    return-void
.end method

.method public final a(LX/2oV;)V
    .locals 1

    .prologue
    .line 2124790
    instance-of v0, p1, LX/7zc;

    if-eqz v0, :cond_0

    .line 2124791
    check-cast p1, LX/7zc;

    invoke-virtual {p1, p0}, LX/7zc;->a(LX/7zb;)V

    .line 2124792
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/Set;LX/2oO;)V
    .locals 0
    .param p2    # LX/2oO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/046;",
            ">;",
            "LX/2oO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2124789
    return-void
.end method

.method public final a(ZLX/ETO;)V
    .locals 9

    .prologue
    .line 2124771
    iget-boolean v0, p0, LX/ETN;->l:Z

    if-eqz v0, :cond_1

    .line 2124772
    if-eqz p2, :cond_0

    .line 2124773
    invoke-interface {p2}, LX/ETO;->a()V

    .line 2124774
    :cond_0
    :goto_0
    return-void

    .line 2124775
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/ETN;->l:Z

    .line 2124776
    iget-object v1, p0, LX/ETN;->b:LX/19w;

    if-eqz p1, :cond_3

    const/4 v0, 0x0

    :goto_1
    const/4 v2, 0x5

    .line 2124777
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2124778
    new-instance v6, Ljava/util/ArrayList;

    iget-object v3, v1, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2124779
    new-instance v3, LX/7JZ;

    invoke-direct {v3, v1}, LX/7JZ;-><init>(LX/19w;)V

    invoke-static {v6, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move v4, v0

    .line 2124780
    :goto_2
    add-int v3, v0, v2

    if-ge v4, v3, :cond_2

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_2

    .line 2124781
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7Jg;

    .line 2124782
    iget-object v7, v3, LX/7Jg;->l:LX/1A0;

    sget-object v8, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne v7, v8, :cond_2

    .line 2124783
    iget-object v3, v3, LX/7Jg;->a:Ljava/lang/String;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2124784
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 2124785
    :cond_2
    move-object v3, v5

    .line 2124786
    invoke-virtual {v1, v3}, LX/19w;->a(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2124787
    new-instance v1, LX/ETJ;

    invoke-direct {v1, p0, p1, p2}, LX/ETJ;-><init>(LX/ETN;ZLX/ETO;)V

    iget-object v2, p0, LX/ETN;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2124788
    :cond_3
    iget-object v0, p0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    invoke-virtual {v0}, LX/ETQ;->size()I

    move-result v0

    goto :goto_1
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 2124766
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    invoke-virtual {v0}, LX/ETQ;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124767
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2124768
    iget-object v1, p0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/ETQ;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2124769
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2124770
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2124762
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ETN;->n:Z

    .line 2124763
    iget-boolean v0, p0, LX/ETN;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ETN;->e:LX/ETL;

    invoke-virtual {v0}, LX/ETL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2124764
    iget-object v0, p0, LX/ETN;->c:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/ETN;->o:Ljava/lang/Runnable;

    const v2, -0x1ecb1612

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2124765
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2124759
    iget-object v0, p0, LX/ETN;->e:LX/ETL;

    .line 2124760
    iget-object v1, v0, LX/ETL;->b:LX/ETK;

    move-object v0, v1

    .line 2124761
    sget-object v1, LX/ETK;->EMPTY:LX/ETK;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
