.class public final LX/CsM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1942473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;
    .locals 2

    .prologue
    .line 1942474
    new-instance v0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;

    invoke-direct {v0, p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1942475
    invoke-static {p1}, LX/CsM;->a(Landroid/os/Parcel;)Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1942476
    new-array v0, p1, [Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;

    move-object v0, v0

    .line 1942477
    return-object v0
.end method
