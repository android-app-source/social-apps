.class public final enum LX/Cmr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cmr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cmr;

.field public static final enum CENTER:LX/Cmr;

.field public static final enum LEFT:LX/Cmr;

.field public static final enum RIGHT:LX/Cmr;


# instance fields
.field private final mGravity:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1933610
    new-instance v0, LX/Cmr;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v4, v6}, LX/Cmr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Cmr;->LEFT:LX/Cmr;

    .line 1933611
    new-instance v0, LX/Cmr;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v3, v3}, LX/Cmr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Cmr;->CENTER:LX/Cmr;

    .line 1933612
    new-instance v0, LX/Cmr;

    const-string v1, "RIGHT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v5, v2}, LX/Cmr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Cmr;->RIGHT:LX/Cmr;

    .line 1933613
    new-array v0, v6, [LX/Cmr;

    sget-object v1, LX/Cmr;->LEFT:LX/Cmr;

    aput-object v1, v0, v4

    sget-object v1, LX/Cmr;->CENTER:LX/Cmr;

    aput-object v1, v0, v3

    sget-object v1, LX/Cmr;->RIGHT:LX/Cmr;

    aput-object v1, v0, v5

    sput-object v0, LX/Cmr;->$VALUES:[LX/Cmr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1933607
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1933608
    iput p3, p0, LX/Cmr;->mGravity:I

    .line 1933609
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cmr;
    .locals 1

    .prologue
    .line 1933614
    const-class v0, LX/Cmr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cmr;

    return-object v0
.end method

.method public static values()[LX/Cmr;
    .locals 1

    .prologue
    .line 1933606
    sget-object v0, LX/Cmr;->$VALUES:[LX/Cmr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cmr;

    return-object v0
.end method


# virtual methods
.method public final getGravity()I
    .locals 1

    .prologue
    .line 1933605
    iget v0, p0, LX/Cmr;->mGravity:I

    return v0
.end method
