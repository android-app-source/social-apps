.class public final LX/E77;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BeT;


# instance fields
.field public final synthetic a:LX/E78;


# direct methods
.method public constructor <init>(LX/E78;)V
    .locals 0

    .prologue
    .line 2081107
    iput-object p1, p0, LX/E77;->a:LX/E78;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2081108
    iget-object v0, p0, LX/E77;->a:LX/E78;

    .line 2081109
    iget-object p1, v0, LX/Cfk;->j:LX/Cgb;

    move-object p1, p1

    .line 2081110
    move-object v0, p1

    .line 2081111
    if-eqz v0, :cond_0

    .line 2081112
    iget-object v0, p0, LX/E77;->a:LX/E78;

    .line 2081113
    iget-object p1, v0, LX/Cfk;->j:LX/Cgb;

    move-object p1, p1

    .line 2081114
    move-object v0, p1

    .line 2081115
    invoke-interface {v0}, LX/Cgb;->b()V

    .line 2081116
    iget-object v0, p0, LX/E77;->a:LX/E78;

    .line 2081117
    iget-object p0, v0, LX/Cfk;->j:LX/Cgb;

    move-object p0, p0

    .line 2081118
    move-object v0, p0

    .line 2081119
    invoke-interface {v0}, LX/Cgb;->kC_()V

    .line 2081120
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2081121
    iget-object v0, p0, LX/E77;->a:LX/E78;

    .line 2081122
    iget-object v1, v0, LX/Cfk;->j:LX/Cgb;

    move-object v1, v1

    .line 2081123
    move-object v0, v1

    .line 2081124
    if-eqz v0, :cond_0

    .line 2081125
    iget-object v0, p0, LX/E77;->a:LX/E78;

    .line 2081126
    iget-object v1, v0, LX/Cfk;->j:LX/Cgb;

    move-object v1, v1

    .line 2081127
    move-object v0, v1

    .line 2081128
    invoke-interface {v0}, LX/Cgb;->b()V

    .line 2081129
    iget-object v0, p0, LX/E77;->a:LX/E78;

    .line 2081130
    iget-object v1, v0, LX/Cfk;->j:LX/Cgb;

    move-object v1, v1

    .line 2081131
    move-object v0, v1

    .line 2081132
    invoke-interface {v0}, LX/Cgb;->kC_()V

    .line 2081133
    :cond_0
    iget-object v0, p0, LX/E77;->a:LX/E78;

    iget-object v1, p0, LX/E77;->a:LX/E78;

    iget-object v1, v1, LX/E78;->b:Ljava/lang/String;

    iget-object v2, p0, LX/E77;->a:LX/E78;

    iget-object v2, v2, LX/E78;->c:Ljava/lang/String;

    new-instance v3, LX/Cfl;

    const/4 v4, 0x0

    sget-object v5, LX/Cfc;->QUESTION_ANSWER_TAP:LX/Cfc;

    invoke-direct {v3, v4, v5}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;)V

    .line 2081134
    invoke-virtual {v0, v1, v2, v3}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2081135
    return-void
.end method

.method public final kD_()V
    .locals 6

    .prologue
    .line 2081136
    iget-object v0, p0, LX/E77;->a:LX/E78;

    .line 2081137
    iget-object v1, v0, LX/Cfk;->j:LX/Cgb;

    move-object v1, v1

    .line 2081138
    move-object v0, v1

    .line 2081139
    if-eqz v0, :cond_0

    .line 2081140
    iget-object v0, p0, LX/E77;->a:LX/E78;

    .line 2081141
    iget-object v1, v0, LX/Cfk;->j:LX/Cgb;

    move-object v1, v1

    .line 2081142
    move-object v0, v1

    .line 2081143
    invoke-interface {v0}, LX/Cgb;->b()V

    .line 2081144
    iget-object v0, p0, LX/E77;->a:LX/E78;

    .line 2081145
    iget-object v1, v0, LX/Cfk;->j:LX/Cgb;

    move-object v1, v1

    .line 2081146
    move-object v0, v1

    .line 2081147
    invoke-interface {v0}, LX/Cgb;->kC_()V

    .line 2081148
    :cond_0
    iget-object v0, p0, LX/E77;->a:LX/E78;

    iget-object v1, p0, LX/E77;->a:LX/E78;

    iget-object v1, v1, LX/E78;->b:Ljava/lang/String;

    iget-object v2, p0, LX/E77;->a:LX/E78;

    iget-object v2, v2, LX/E78;->c:Ljava/lang/String;

    new-instance v3, LX/Cfl;

    const/4 v4, 0x0

    sget-object v5, LX/Cfc;->QUESTION_SKIP_TAP:LX/Cfc;

    invoke-direct {v3, v4, v5}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;)V

    .line 2081149
    invoke-virtual {v0, v1, v2, v3}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2081150
    return-void
.end method
