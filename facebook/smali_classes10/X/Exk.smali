.class public final LX/Exk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/83X;

.field public final synthetic b:LX/Exs;


# direct methods
.method public constructor <init>(LX/Exs;LX/83X;)V
    .locals 0

    .prologue
    .line 2185287
    iput-object p1, p0, LX/Exk;->b:LX/Exs;

    iput-object p2, p0, LX/Exk;->a:LX/83X;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x23a0bdb7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2185288
    iget-object v1, p0, LX/Exk;->b:LX/Exs;

    iget-object v2, p0, LX/Exk;->a:LX/83X;

    .line 2185289
    instance-of v4, v2, LX/Eus;

    if-eqz v4, :cond_0

    .line 2185290
    iget-object v4, v1, LX/Exs;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2hd;

    iget-object v6, v1, LX/Exs;->s:Ljava/lang/String;

    move-object v4, v2

    check-cast v4, LX/Eus;

    .line 2185291
    iget-object v7, v4, LX/Eus;->h:Ljava/lang/String;

    move-object v7, v7

    .line 2185292
    invoke-interface {v2}, LX/2lr;->a()J

    move-result-wide v8

    invoke-interface {v2}, LX/83W;->g()LX/2h7;

    move-result-object v4

    iget-object v10, v4, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    invoke-virtual/range {v5 .. v10}, LX/2hd;->b(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 2185293
    :cond_0
    iget-object v4, v1, LX/Exs;->o:LX/Ey4;

    iget-object v5, v1, LX/Exs;->u:Ljava/lang/String;

    iget-boolean v6, v1, LX/Exs;->t:Z

    .line 2185294
    const-string v7, "start"

    invoke-static {v4, v7, v5, v6}, LX/Ey4;->a(LX/Ey4;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2185295
    invoke-interface {v2}, LX/83X;->c()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v11

    .line 2185296
    invoke-interface {v2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v9

    .line 2185297
    invoke-static {v1, v9}, LX/Exs;->a(LX/Exs;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185298
    sget-object v4, LX/Exs;->b:Ljava/util/Map;

    invoke-interface {v4, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-interface {v2, v4}, LX/2np;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185299
    iget-object v4, v1, LX/Exs;->r:LX/Ew6;

    if-eqz v4, :cond_1

    .line 2185300
    iget-object v4, v1, LX/Exs;->r:LX/Ew6;

    invoke-interface {v2}, LX/2lr;->a()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, LX/Ew6;->a(J)V

    .line 2185301
    :cond_1
    iget-object v4, v1, LX/Exs;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2iT;

    invoke-interface {v2}, LX/2lr;->a()J

    move-result-wide v6

    invoke-interface {v2}, LX/83W;->g()LX/2h7;

    move-result-object v8

    new-instance v10, LX/Exp;

    invoke-direct {v10, v1, v2, v9, v11}, LX/Exp;-><init>(LX/Exs;LX/83X;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    invoke-virtual/range {v5 .. v10}, LX/2iT;->a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)V

    .line 2185302
    const v1, 0xbc37681

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
