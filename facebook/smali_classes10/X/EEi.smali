.class public final LX/EEi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/EEp;


# direct methods
.method public constructor <init>(LX/EEp;)V
    .locals 0

    .prologue
    .line 2094150
    iput-object p1, p0, LX/EEi;->a:LX/EEp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1d32f2d6

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2094151
    iget-object v0, p0, LX/EEi;->a:LX/EEp;

    iget-object v0, v0, LX/EEp;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2094152
    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/EDx;->ae:Ljava/util/Map;

    if-nez v3, :cond_1

    .line 2094153
    :cond_0
    :goto_0
    const v0, 0x2256a612

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2094154
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    iget-object v3, v0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2094155
    iget-object v3, v0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EGE;

    .line 2094156
    invoke-virtual {v3}, LX/EGE;->a()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 2094157
    iget-object v3, v3, LX/EGE;->b:Ljava/lang/String;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2094158
    :cond_3
    iget-object p0, v0, LX/EDx;->ad:LX/EFw;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v4, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {p0, v3}, LX/EFw;->b([Ljava/lang/String;)V

    goto :goto_0
.end method
