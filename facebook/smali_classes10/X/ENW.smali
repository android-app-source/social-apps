.class public LX/ENW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1vg;

.field private final d:LX/CvY;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/0Ot;LX/1vg;LX/CvY;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/1vg;",
            "LX/CvY;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2112768
    iput-object p1, p0, LX/ENW;->a:Landroid/app/Activity;

    .line 2112769
    iput-object p2, p0, LX/ENW;->b:LX/0Ot;

    .line 2112770
    iput-object p3, p0, LX/ENW;->c:LX/1vg;

    .line 2112771
    iput-object p4, p0, LX/ENW;->d:LX/CvY;

    .line 2112772
    return-void
.end method

.method public static a(LX/0QB;)LX/ENW;
    .locals 7

    .prologue
    .line 2112786
    const-class v1, LX/ENW;

    monitor-enter v1

    .line 2112787
    :try_start_0
    sget-object v0, LX/ENW;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112788
    sput-object v2, LX/ENW;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112789
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112790
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112791
    new-instance v6, LX/ENW;

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    const/16 v4, 0x3be

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v5

    check-cast v5, LX/CvY;

    invoke-direct {v6, v3, p0, v4, v5}, LX/ENW;-><init>(Landroid/app/Activity;LX/0Ot;LX/1vg;LX/CvY;)V

    .line 2112792
    move-object v0, v6

    .line 2112793
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112794
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ENW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112795
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112796
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/CzL;LX/1Pg;)V
    .locals 11
    .param p1    # LX/CzL;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<",
            "LX/8d6;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2112773
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2112774
    check-cast v0, LX/8d6;

    .line 2112775
    invoke-interface {v0}, LX/8d6;->bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v1

    if-eqz v1, :cond_0

    move v1, v6

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2112776
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LX/8d6;->bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->j()Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    .line 2112777
    :goto_1
    iget-object v0, p0, LX/ENW;->d:LX/CvY;

    move-object v1, p2

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->SHARE:LX/8ch;

    move-object v3, p2

    check-cast v3, LX/CxP;

    invoke-interface {v3, p1}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    move-object v4, p2

    check-cast v4, LX/CxV;

    invoke-interface {v4}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v4

    check-cast p2, LX/CxP;

    invoke-interface {p2, p1}, LX/CxP;->b(LX/CzL;)I

    move-result v5

    .line 2112778
    iget-object v9, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v9, v9

    .line 2112779
    invoke-static {v9}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v9

    invoke-static {v4, v5, v9, v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2112780
    sget-object v0, LX/21D;->SEARCH:LX/21D;

    const-string v1, "searchShareButton"

    invoke-static {v8}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v2

    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2112781
    invoke-virtual {v0, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEditTagEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "search_ufi"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    .line 2112782
    iget-object v0, p0, LX/ENW;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/16 v2, 0x6dc

    iget-object v3, p0, LX/ENW;->a:Landroid/app/Activity;

    invoke-interface {v0, v10, v1, v2, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2112783
    return-void

    :cond_0
    move v1, v7

    .line 2112784
    goto :goto_0

    .line 2112785
    :cond_1
    invoke-interface {v0}, LX/8d6;->bi()Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    goto :goto_1
.end method
