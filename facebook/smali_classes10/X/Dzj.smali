.class public final LX/Dzj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 0

    .prologue
    .line 2067137
    iput-object p1, p0, LX/Dzj;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 2067138
    iget-object v0, p0, LX/Dzj;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-static {v0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->q(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067139
    iget-object v0, p0, LX/Dzj;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->g:LX/1Ck;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/Dzj;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->h:LX/0TD;

    new-instance v3, LX/Dzh;

    invoke-direct {v3, p0}, LX/Dzh;-><init>(LX/Dzj;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/Dzi;

    invoke-direct {v3, p0}, LX/Dzi;-><init>(LX/Dzj;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2067140
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2067136
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2067135
    return-void
.end method
