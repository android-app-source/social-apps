.class public LX/DRd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/DRd;


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1998434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1998435
    iput-object p1, p0, LX/DRd;->a:LX/0Uh;

    .line 1998436
    return-void
.end method

.method public static a(LX/0QB;)LX/DRd;
    .locals 4

    .prologue
    .line 1998437
    sget-object v0, LX/DRd;->b:LX/DRd;

    if-nez v0, :cond_1

    .line 1998438
    const-class v1, LX/DRd;

    monitor-enter v1

    .line 1998439
    :try_start_0
    sget-object v0, LX/DRd;->b:LX/DRd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1998440
    if-eqz v2, :cond_0

    .line 1998441
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1998442
    new-instance p0, LX/DRd;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/DRd;-><init>(LX/0Uh;)V

    .line 1998443
    move-object v0, p0

    .line 1998444
    sput-object v0, LX/DRd;->b:LX/DRd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1998445
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1998446
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1998447
    :cond_1
    sget-object v0, LX/DRd;->b:LX/DRd;

    return-object v0

    .line 1998448
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1998449
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1998450
    if-eqz p0, :cond_1

    .line 1998451
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1998452
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1998453
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->C()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1998454
    invoke-virtual {v3, v0, v2}, LX/15i;->j(II)I

    move-result v0

    if-lez v0, :cond_2

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1998455
    iget-object v1, p0, LX/DRd;->a:LX/0Uh;

    const/16 v2, 0x470

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1998456
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->SCHOOL_CLASS:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-virtual {p1, v0}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1998457
    :cond_0
    return v0
.end method
