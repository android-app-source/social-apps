.class public final LX/EZ5;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field public final synthetic a:LX/EZ6;

.field private b:LX/EZ3;

.field private c:LX/EYy;

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(LX/EZ6;)V
    .locals 0

    .prologue
    .line 2138566
    iput-object p1, p0, LX/EZ5;->a:LX/EZ6;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 2138567
    invoke-direct {p0}, LX/EZ5;->a()V

    .line 2138568
    return-void
.end method

.method private a([BII)I
    .locals 5

    .prologue
    .line 2138515
    move v1, p3

    move v0, p2

    .line 2138516
    :goto_0
    if-lez v1, :cond_2

    .line 2138517
    invoke-direct {p0}, LX/EZ5;->b()V

    .line 2138518
    iget-object v2, p0, LX/EZ5;->c:LX/EYy;

    if-nez v2, :cond_0

    .line 2138519
    if-ne v1, p3, :cond_2

    .line 2138520
    const/4 v0, -0x1

    .line 2138521
    :goto_1
    return v0

    .line 2138522
    :cond_0
    iget v2, p0, LX/EZ5;->d:I

    iget v3, p0, LX/EZ5;->e:I

    sub-int/2addr v2, v3

    .line 2138523
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 2138524
    if-eqz p1, :cond_1

    .line 2138525
    iget-object v3, p0, LX/EZ5;->c:LX/EYy;

    iget v4, p0, LX/EZ5;->e:I

    invoke-virtual {v3, p1, v4, v0, v2}, LX/EWc;->a([BIII)V

    .line 2138526
    add-int/2addr v0, v2

    .line 2138527
    :cond_1
    iget v3, p0, LX/EZ5;->e:I

    add-int/2addr v3, v2

    iput v3, p0, LX/EZ5;->e:I

    .line 2138528
    sub-int/2addr v1, v2

    .line 2138529
    goto :goto_0

    .line 2138530
    :cond_2
    sub-int v0, p3, v1

    goto :goto_1
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2138560
    new-instance v0, LX/EZ3;

    iget-object v1, p0, LX/EZ5;->a:LX/EZ6;

    invoke-direct {v0, v1}, LX/EZ3;-><init>(LX/EWc;)V

    iput-object v0, p0, LX/EZ5;->b:LX/EZ3;

    .line 2138561
    iget-object v0, p0, LX/EZ5;->b:LX/EZ3;

    invoke-virtual {v0}, LX/EZ3;->a()LX/EYy;

    move-result-object v0

    iput-object v0, p0, LX/EZ5;->c:LX/EYy;

    .line 2138562
    iget-object v0, p0, LX/EZ5;->c:LX/EYy;

    invoke-virtual {v0}, LX/EYy;->b()I

    move-result v0

    iput v0, p0, LX/EZ5;->d:I

    .line 2138563
    iput v2, p0, LX/EZ5;->e:I

    .line 2138564
    iput v2, p0, LX/EZ5;->f:I

    .line 2138565
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2138551
    iget-object v0, p0, LX/EZ5;->c:LX/EYy;

    if-eqz v0, :cond_0

    iget v0, p0, LX/EZ5;->e:I

    iget v1, p0, LX/EZ5;->d:I

    if-ne v0, v1, :cond_0

    .line 2138552
    iget v0, p0, LX/EZ5;->f:I

    iget v1, p0, LX/EZ5;->d:I

    add-int/2addr v0, v1

    iput v0, p0, LX/EZ5;->f:I

    .line 2138553
    iput v2, p0, LX/EZ5;->e:I

    .line 2138554
    iget-object v0, p0, LX/EZ5;->b:LX/EZ3;

    invoke-virtual {v0}, LX/EZ3;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2138555
    iget-object v0, p0, LX/EZ5;->b:LX/EZ3;

    invoke-virtual {v0}, LX/EZ3;->a()LX/EYy;

    move-result-object v0

    iput-object v0, p0, LX/EZ5;->c:LX/EYy;

    .line 2138556
    iget-object v0, p0, LX/EZ5;->c:LX/EYy;

    invoke-virtual {v0}, LX/EYy;->b()I

    move-result v0

    iput v0, p0, LX/EZ5;->d:I

    .line 2138557
    :cond_0
    :goto_0
    return-void

    .line 2138558
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/EZ5;->c:LX/EYy;

    .line 2138559
    iput v2, p0, LX/EZ5;->d:I

    goto :goto_0
.end method


# virtual methods
.method public final available()I
    .locals 2

    .prologue
    .line 2138549
    iget v0, p0, LX/EZ5;->f:I

    iget v1, p0, LX/EZ5;->e:I

    add-int/2addr v0, v1

    .line 2138550
    iget-object v1, p0, LX/EZ5;->a:LX/EZ6;

    invoke-virtual {v1}, LX/EZ6;->b()I

    move-result v1

    sub-int v0, v1, v0

    return v0
.end method

.method public final mark(I)V
    .locals 2

    .prologue
    .line 2138569
    iget v0, p0, LX/EZ5;->f:I

    iget v1, p0, LX/EZ5;->e:I

    add-int/2addr v0, v1

    iput v0, p0, LX/EZ5;->g:I

    .line 2138570
    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 2138548
    const/4 v0, 0x1

    return v0
.end method

.method public final read()I
    .locals 3

    .prologue
    .line 2138544
    invoke-direct {p0}, LX/EZ5;->b()V

    .line 2138545
    iget-object v0, p0, LX/EZ5;->c:LX/EYy;

    if-nez v0, :cond_0

    .line 2138546
    const/4 v0, -0x1

    .line 2138547
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/EZ5;->c:LX/EYy;

    iget v1, p0, LX/EZ5;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/EZ5;->e:I

    invoke-virtual {v0, v1}, LX/EYy;->a(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 2138539
    if-nez p1, :cond_0

    .line 2138540
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2138541
    :cond_0
    if-ltz p2, :cond_1

    if-ltz p3, :cond_1

    array-length v0, p1

    sub-int/2addr v0, p2

    if-le p3, v0, :cond_2

    .line 2138542
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 2138543
    :cond_2
    invoke-direct {p0, p1, p2, p3}, LX/EZ5;->a([BII)I

    move-result v0

    return v0
.end method

.method public final declared-synchronized reset()V
    .locals 3

    .prologue
    .line 2138535
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/EZ5;->a()V

    .line 2138536
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget v2, p0, LX/EZ5;->g:I

    invoke-direct {p0, v0, v1, v2}, LX/EZ5;->a([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2138537
    monitor-exit p0

    return-void

    .line 2138538
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final skip(J)J
    .locals 5

    .prologue
    const-wide/32 v0, 0x7fffffff

    .line 2138531
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    .line 2138532
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 2138533
    :cond_0
    cmp-long v2, p1, v0

    if-lez v2, :cond_1

    move-wide p1, v0

    .line 2138534
    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    long-to-int v2, p1

    invoke-direct {p0, v0, v1, v2}, LX/EZ5;->a([BII)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method
