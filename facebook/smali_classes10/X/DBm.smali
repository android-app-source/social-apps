.class public LX/DBm;
.super Landroid/view/View;
.source ""


# instance fields
.field public final A:Ljava/text/SimpleDateFormat;

.field private final B:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final C:Ljava/lang/String;

.field private D:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public E:Lcom/facebook/graphql/model/FeedUnit;

.field public F:Lcom/facebook/graphql/model/FeedUnit;

.field public G:Lcom/facebook/graphql/model/FeedUnit;

.field public H:Lcom/facebook/graphql/model/FeedUnit;

.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Z

.field public s:[F

.field private t:I

.field public final u:Landroid/graphics/Paint;

.field private final v:I

.field private final w:I

.field private final x:I

.field private final y:I

.field private final z:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1973341
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1973342
    iput v1, p0, LX/DBm;->a:I

    .line 1973343
    iput v1, p0, LX/DBm;->b:I

    .line 1973344
    iput v1, p0, LX/DBm;->c:I

    .line 1973345
    iput v1, p0, LX/DBm;->d:I

    .line 1973346
    iput v1, p0, LX/DBm;->e:I

    .line 1973347
    const-string v0, "INITIAL STRING"

    iput-object v0, p0, LX/DBm;->f:Ljava/lang/String;

    .line 1973348
    const-string v0, ""

    iput-object v0, p0, LX/DBm;->g:Ljava/lang/String;

    .line 1973349
    iput v1, p0, LX/DBm;->o:I

    .line 1973350
    iput-boolean v1, p0, LX/DBm;->p:Z

    .line 1973351
    iput-boolean v1, p0, LX/DBm;->q:Z

    .line 1973352
    iput-boolean v1, p0, LX/DBm;->r:Z

    .line 1973353
    iput v1, p0, LX/DBm;->t:I

    .line 1973354
    const/16 v0, 0x2bc

    iput v0, p0, LX/DBm;->v:I

    .line 1973355
    const v0, -0xff0100

    iput v0, p0, LX/DBm;->w:I

    .line 1973356
    const/4 v0, -0x1

    iput v0, p0, LX/DBm;->x:I

    .line 1973357
    const/16 v0, -0x100

    iput v0, p0, LX/DBm;->y:I

    .line 1973358
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/DBm;->z:[I

    .line 1973359
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd HH:mm"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, LX/DBm;->A:Ljava/text/SimpleDateFormat;

    .line 1973360
    const-string v0, "Green = Ads, Yellow = Gaps, Black->light by ranking times: repeats after 7 dif times"

    iput-object v0, p0, LX/DBm;->C:Ljava/lang/String;

    .line 1973361
    iput-object p2, p0, LX/DBm;->B:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1973362
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/DBm;->u:Landroid/graphics/Paint;

    .line 1973363
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/DBm;->m:Ljava/util/Map;

    .line 1973364
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/DBm;->n:Ljava/util/Map;

    .line 1973365
    return-void

    nop

    :array_0
    .array-data 4
        -0x1000000
        -0xd9d9da
        -0xb2b2b3
        -0x8c8c8d
        -0x666667
        -0x3f3f40
        -0x1a1a1b
    .end array-data
.end method

.method private a(ILandroid/graphics/Canvas;)I
    .locals 7

    .prologue
    const/16 v3, 0x1e

    .line 1973366
    const/4 v2, 0x0

    const-string v4, "DB"

    iget-boolean v0, p0, LX/DBm;->p:Z

    invoke-static {v0}, LX/DBm;->a(Z)I

    move-result v6

    move-object v0, p0

    move v1, p1

    move-object v5, p2

    invoke-static/range {v0 .. v6}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    .line 1973367
    const/16 v2, 0x3c

    const-string v4, "NN"

    iget-boolean v0, p0, LX/DBm;->q:Z

    invoke-static {v0}, LX/DBm;->a(Z)I

    move-result v6

    move-object v0, p0

    move v1, p1

    move-object v5, p2

    invoke-static/range {v0 .. v6}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    .line 1973368
    const/16 v2, 0x78

    const-string v4, "NM"

    iget-boolean v0, p0, LX/DBm;->r:Z

    invoke-static {v0}, LX/DBm;->a(Z)I

    move-result v6

    move-object v0, p0

    move v1, p1

    move-object v5, p2

    invoke-static/range {v0 .. v6}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    move-result-object v0

    .line 1973369
    const/4 v1, 0x3

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    sub-float v0, v1, v0

    float-to-int v0, v0

    return v0
.end method

.method private a(Landroid/graphics/Canvas;IILjava/util/List;ZZZZZLjava/lang/String;)I
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "II",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;ZZZZZ",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 1973395
    const/16 v12, 0x44

    .line 1973396
    const/4 v9, 0x0

    .line 1973397
    const/4 v3, 0x0

    .line 1973398
    const/4 v10, 0x1

    .line 1973399
    const/4 v11, 0x1

    .line 1973400
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v2, "GAP Index: "

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1973401
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v2, "AD Index: "

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1973402
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v2, "Prepared Index: "

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1973403
    add-int/lit8 v20, p3, 0x4

    .line 1973404
    const/4 v13, 0x0

    .line 1973405
    const/4 v2, 0x0

    move v15, v2

    move/from16 v16, v3

    .line 1973406
    :goto_0
    if-eqz p4, :cond_c

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    if-ge v15, v2, :cond_c

    const/16 v2, 0x2bc

    if-ge v9, v2, :cond_c

    .line 1973407
    move-object/from16 v0, p4

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, LX/7zl;

    .line 1973408
    invoke-virtual {v8}, LX/7zl;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1973409
    move-object/from16 v0, p0

    iget-object v2, v0, LX/DBm;->u:Landroid/graphics/Paint;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1973410
    add-int v2, p2, v9

    int-to-float v3, v2

    move/from16 v0, v20

    int-to-float v4, v0

    add-int v2, p2, v9

    add-int/lit8 v2, v2, 0x4

    int-to-float v5, v2

    add-int/lit8 v2, v20, 0x3c

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, LX/DBm;->u:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1973411
    add-int/lit8 v2, v9, 0x4

    move v9, v2

    .line 1973412
    :cond_0
    invoke-virtual {v8}, LX/7zl;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    if-eqz p5, :cond_7

    .line 1973413
    if-nez v10, :cond_3

    .line 1973414
    const-string v2, ", "

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973415
    :goto_1
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1973416
    move-object/from16 v0, p0

    iget-object v3, v0, LX/DBm;->u:Landroid/graphics/Paint;

    invoke-virtual {v8}, LX/7zl;->c()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, -0x1

    :goto_2
    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1973417
    add-int v2, p2, v9

    int-to-float v3, v2

    move/from16 v0, v20

    int-to-float v4, v0

    add-int v2, p2, v9

    add-int/lit8 v2, v2, 0x10

    int-to-float v5, v2

    add-int/lit8 v2, v20, 0x3c

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, LX/DBm;->u:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1973418
    move-object/from16 v0, p0

    iget-object v2, v0, LX/DBm;->n:Ljava/util/Map;

    invoke-virtual {v8}, LX/7zl;->i()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1973419
    move-object/from16 v0, p0

    iget-object v2, v0, LX/DBm;->n:Ljava/util/Map;

    invoke-virtual {v8}, LX/7zl;->i()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1973420
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1973421
    const/high16 v3, 0x42b40000    # 90.0f

    add-int v4, p2, v9

    int-to-float v4, v4

    add-int/lit8 v5, v20, 0x4

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1973422
    move-object/from16 v0, p0

    iget-object v3, v0, LX/DBm;->u:Landroid/graphics/Paint;

    const/high16 v4, 0x41800000    # 16.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1973423
    move-object/from16 v0, p0

    iget-object v3, v0, LX/DBm;->u:Landroid/graphics/Paint;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1973424
    if-eqz p6, :cond_1

    const-string v3, "%s %d %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    invoke-virtual {v8}, LX/7zl;->d()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x2

    add-int/lit8 v5, v13, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1973425
    :cond_1
    add-int v3, p2, v9

    int-to-float v3, v3

    add-int/lit8 v4, v20, 0x4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/DBm;->u:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1973426
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 1973427
    move-object/from16 v0, p0

    iget-object v2, v0, LX/DBm;->u:Landroid/graphics/Paint;

    const v3, -0xff0100

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1973428
    add-int/lit8 v3, v9, 0x10

    .line 1973429
    const/4 v2, 0x0

    move v13, v2

    move v14, v10

    move v10, v3

    .line 1973430
    :goto_4
    if-eqz p9, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, LX/DBm;->D:Ljava/util/Set;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, LX/DBm;->D:Ljava/util/Set;

    invoke-virtual {v8}, LX/7zl;->h()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1973431
    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1973432
    const-string v2, ", "

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973433
    :cond_2
    add-int/lit8 v2, v16, 0x1

    .line 1973434
    invoke-virtual {v8}, LX/7zl;->e()Z

    move-result v3

    if-eqz v3, :cond_11

    if-eqz p7, :cond_11

    .line 1973435
    add-int/lit8 v9, v2, 0x1

    .line 1973436
    if-nez v11, :cond_a

    .line 1973437
    const-string v2, ", "

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v8, v11

    .line 1973438
    :goto_5
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1973439
    move-object/from16 v0, p0

    iget-object v2, v0, LX/DBm;->u:Landroid/graphics/Paint;

    const/16 v3, -0x100

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1973440
    add-int v2, p2, v10

    int-to-float v3, v2

    move/from16 v0, v20

    int-to-float v4, v0

    add-int v2, p2, v10

    add-int/lit8 v2, v2, 0x6

    int-to-float v5, v2

    add-int/lit8 v2, v20, 0x3c

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, LX/DBm;->u:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1973441
    add-int/lit8 v2, v10, 0x6

    move v11, v8

    move v3, v2

    move v2, v9

    .line 1973442
    :goto_6
    add-int/lit8 v4, v15, 0x1

    move v15, v4

    move v10, v14

    move/from16 v16, v2

    move v9, v3

    goto/16 :goto_0

    .line 1973443
    :cond_3
    if-nez v10, :cond_4

    const/4 v2, 0x1

    :goto_7
    move v10, v2

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_7

    .line 1973444
    :cond_5
    const v2, -0xff0100

    goto/16 :goto_2

    .line 1973445
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v3, v0, LX/DBm;->t:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, LX/DBm;->t:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1973446
    move-object/from16 v0, p0

    iget-object v3, v0, LX/DBm;->n:Ljava/util/Map;

    invoke-virtual {v8}, LX/7zl;->i()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 1973447
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, LX/DBm;->m:Ljava/util/Map;

    invoke-virtual {v8}, LX/7zl;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1973448
    move-object/from16 v0, p0

    iget-object v2, v0, LX/DBm;->m:Ljava/util/Map;

    invoke-virtual {v8}, LX/7zl;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, LX/DBm;->o:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1973449
    move-object/from16 v0, p0

    iget v2, v0, LX/DBm;->o:I

    move-object/from16 v0, p0

    iget-object v3, v0, LX/DBm;->z:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_9

    .line 1973450
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/DBm;->o:I

    .line 1973451
    :cond_8
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, LX/DBm;->u:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/DBm;->z:[I

    move-object/from16 v0, p0

    iget-object v2, v0, LX/DBm;->m:Ljava/util/Map;

    invoke-virtual {v8}, LX/7zl;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget v2, v4, v2

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1973452
    add-int v2, p2, v9

    int-to-float v3, v2

    move/from16 v0, v20

    int-to-float v4, v0

    add-int v2, p2, v9

    add-int/lit8 v2, v2, 0x4

    int-to-float v5, v2

    add-int/lit8 v2, v20, 0x3c

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, LX/DBm;->u:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1973453
    add-int/lit8 v3, v9, 0x4

    .line 1973454
    add-int/lit8 v2, v13, 0x1

    move v13, v2

    move v14, v10

    move v10, v3

    goto/16 :goto_4

    .line 1973455
    :cond_9
    move-object/from16 v0, p0

    iget v2, v0, LX/DBm;->o:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, LX/DBm;->o:I

    goto :goto_8

    .line 1973456
    :cond_a
    if-nez v11, :cond_b

    const/4 v2, 0x1

    :goto_9
    move v8, v2

    goto/16 :goto_5

    :cond_b
    const/4 v2, 0x0

    goto :goto_9

    .line 1973457
    :cond_c
    if-eqz p5, :cond_10

    if-eqz p8, :cond_10

    .line 1973458
    add-int/lit8 v2, v20, 0x3c

    add-int/lit8 v3, v2, 0x4

    const/4 v4, 0x0

    const/16 v5, 0x1e

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const v8, -0xff0100

    move-object/from16 v2, p0

    move-object/from16 v7, p1

    invoke-static/range {v2 .. v8}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    move-result-object v2

    .line 1973459
    const/high16 v3, 0x42880000    # 68.0f

    const/4 v4, 0x3

    aget v4, v2, v4

    const/4 v5, 0x1

    aget v2, v2, v5

    sub-float v2, v4, v2

    add-float/2addr v2, v3

    float-to-int v9, v2

    .line 1973460
    :goto_a
    if-eqz p7, :cond_d

    if-eqz p8, :cond_d

    .line 1973461
    add-int/lit8 v2, v20, 0x3c

    add-int/lit8 v3, v2, 0x22

    const/4 v4, 0x0

    const/16 v5, 0x1e

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v8, -0x100

    move-object/from16 v2, p0

    move-object/from16 v7, p1

    invoke-static/range {v2 .. v8}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    move-result-object v2

    .line 1973462
    int-to-float v3, v9

    const/4 v4, 0x3

    aget v4, v2, v4

    const/4 v5, 0x1

    aget v2, v2, v5

    sub-float v2, v4, v2

    add-float/2addr v2, v3

    float-to-int v9, v2

    .line 1973463
    :cond_d
    if-eqz p9, :cond_e

    .line 1973464
    add-int/lit8 v2, v20, 0x3c

    add-int/lit8 v3, v2, 0x4

    const/4 v4, 0x0

    const/16 v5, 0x1e

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v8, -0x100

    move-object/from16 v2, p0

    move-object/from16 v7, p1

    invoke-static/range {v2 .. v8}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    move-result-object v2

    .line 1973465
    int-to-float v3, v9

    const/4 v4, 0x3

    aget v4, v2, v4

    const/4 v5, 0x1

    aget v2, v2, v5

    sub-float v2, v4, v2

    add-float/2addr v2, v3

    float-to-int v2, v2

    move v9, v2

    .line 1973466
    :cond_e
    if-eqz p10, :cond_f

    .line 1973467
    add-int/lit8 v2, v20, 0x4

    add-int/lit8 v3, v2, 0xf

    const/16 v4, 0x2c0

    const/16 v5, 0x1e

    const v8, -0xff0100

    move-object/from16 v2, p0

    move-object/from16 v6, p10

    move-object/from16 v7, p1

    invoke-static/range {v2 .. v8}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    .line 1973468
    :cond_f
    return v9

    :cond_10
    move v9, v12

    goto :goto_a

    :cond_11
    move v3, v10

    goto/16 :goto_6
.end method

.method private static a(Z)I
    .locals 1

    .prologue
    .line 1973370
    if-nez p0, :cond_0

    .line 1973371
    const v0, -0xff0100

    .line 1973372
    :goto_0
    return v0

    :cond_0
    const v0, -0xff01

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 1973373
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Story: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1973374
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 1973375
    invoke-static {p1}, LX/DBm;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973376
    invoke-static {p1}, LX/DBm;->c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973377
    const-string v3, " - FetchedAt: "

    .line 1973378
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v2

    .line 1973379
    if-eqz v2, :cond_1

    .line 1973380
    const/4 v4, 0x3

    invoke-virtual {v2, v4}, LX/15i;->b(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1973381
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1973382
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 1973383
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1973384
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/DBm;->A:Ljava/text/SimpleDateFormat;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1973385
    :goto_0
    move-object v1, v2

    .line 1973386
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973387
    invoke-static {p1}, LX/DBm;->e(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973388
    invoke-static {p1}, LX/0x1;->b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v1

    .line 1973389
    const-string p1, "1"

    invoke-static {v1, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    move v1, p1

    .line 1973390
    if-eqz v1, :cond_2

    .line 1973391
    const-string v1, " : S "

    .line 1973392
    :goto_1
    move-object v1, v1

    .line 1973393
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973394
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v2, v3

    goto :goto_0

    :cond_2
    const-string v1, " : U "

    goto :goto_1
.end method

.method private static a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F
    .locals 9

    .prologue
    .line 1973324
    iget-object v0, p0, LX/DBm;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1973325
    iget-object v0, p0, LX/DBm;->u:Landroid/graphics/Paint;

    int-to-float v1, p3

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p5

    move-object v5, p4

    .line 1973326
    const/4 v6, 0x4

    new-array v6, v6, [F

    .line 1973327
    const/4 v7, 0x0

    invoke-virtual {v4}, Landroid/graphics/Canvas;->getWidth()I

    move-result v8

    int-to-float v8, v8

    iget-object p6, v0, LX/DBm;->u:Landroid/graphics/Paint;

    invoke-virtual {p6, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result p6

    sub-float/2addr v8, p6

    int-to-float p6, v2

    sub-float/2addr v8, p6

    aput v8, v6, v7

    .line 1973328
    const/4 v7, 0x1

    int-to-float v8, v1

    aput v8, v6, v7

    .line 1973329
    const/4 v7, 0x2

    invoke-virtual {v4}, Landroid/graphics/Canvas;->getWidth()I

    move-result v8

    sub-int/2addr v8, v2

    int-to-float v8, v8

    aput v8, v6, v7

    .line 1973330
    const/4 v7, 0x3

    add-int v8, v1, v3

    add-int/lit8 v8, v8, 0x5

    int-to-float v8, v8

    aput v8, v6, v7

    .line 1973331
    move-object v6, v6

    .line 1973332
    const/4 v0, 0x0

    aget v1, v6, v0

    const/4 v0, 0x1

    aget v2, v6, v0

    const/4 v0, 0x2

    aget v3, v6, v0

    const/4 v0, 0x3

    aget v4, v6, v0

    iget-object v5, p0, LX/DBm;->u:Landroid/graphics/Paint;

    move-object v0, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1973333
    iget-object v0, p0, LX/DBm;->u:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1973334
    invoke-virtual {p5}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, LX/DBm;->u:Landroid/graphics/Paint;

    invoke-virtual {v1, p4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    sub-float/2addr v0, v1

    int-to-float v1, p2

    sub-float/2addr v0, v1

    add-int v1, p1, p3

    int-to-float v1, v1

    iget-object v2, p0, LX/DBm;->u:Landroid/graphics/Paint;

    invoke-virtual {p5, p4, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1973335
    return-object v6
.end method

.method private static b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1973336
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1973337
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    .line 1973338
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1973339
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1973340
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x12

    .line 1973316
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1973317
    invoke-static {p0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1973318
    if-eqz v0, :cond_1

    .line 1973319
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1973320
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v2, :cond_0

    .line 1973321
    :goto_0
    return-object v0

    .line 1973322
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1973323
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private static e(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1973310
    invoke-static {p0}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v0

    .line 1973311
    iget-boolean p0, v0, LX/0x2;->t:Z

    move v0, p0

    .line 1973312
    move v0, v0

    .line 1973313
    if-eqz v0, :cond_0

    .line 1973314
    const-string v0, " C"

    .line 1973315
    :goto_0
    return-object v0

    :cond_0
    const-string v0, " N"

    goto :goto_0
.end method

.method private static g(LX/DBm;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1973275
    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_1
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1973276
    if-eqz v0, :cond_3

    .line 1973277
    :cond_2
    :goto_1
    return-void

    .line 1973278
    :cond_3
    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1973279
    if-eqz v0, :cond_4

    .line 1973280
    iput-object p1, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    goto :goto_1

    .line 1973281
    :cond_4
    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 1973282
    if-eqz v0, :cond_5

    .line 1973283
    iput-object p1, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    goto :goto_1

    .line 1973284
    :cond_5
    iget-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    if-nez v0, :cond_d

    iget-object v0, p0, LX/DBm;->H:Lcom/facebook/graphql/model/FeedUnit;

    if-nez v0, :cond_d

    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 1973285
    if-eqz v0, :cond_6

    .line 1973286
    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973287
    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973288
    iput-object p1, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    goto :goto_1

    .line 1973289
    :cond_6
    iget-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_e

    iget-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 1973290
    if-eqz v0, :cond_7

    .line 1973291
    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973292
    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973293
    iput-object p1, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    goto :goto_1

    .line 1973294
    :cond_7
    iget-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_f

    iget-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    :goto_6
    move v0, v0

    .line 1973295
    if-eqz v0, :cond_8

    .line 1973296
    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->H:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973297
    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973298
    iget-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973299
    iput-object v1, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    goto :goto_1

    .line 1973300
    :cond_8
    iget-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    if-nez v0, :cond_10

    iget-object v0, p0, LX/DBm;->H:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_10

    iget-object v0, p0, LX/DBm;->H:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_7
    move v0, v0

    .line 1973301
    if-eqz v0, :cond_9

    .line 1973302
    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973303
    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973304
    iput-object v1, p0, LX/DBm;->H:Lcom/facebook/graphql/model/FeedUnit;

    goto/16 :goto_1

    .line 1973305
    :cond_9
    iget-object v0, p0, LX/DBm;->G:Lcom/facebook/graphql/model/FeedUnit;

    if-nez v0, :cond_11

    iget-object v0, p0, LX/DBm;->H:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_11

    iget-object v0, p0, LX/DBm;->H:Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    const/4 v0, 0x1

    :goto_8
    move v0, v0

    .line 1973306
    if-eqz v0, :cond_2

    .line 1973307
    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->H:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973308
    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    .line 1973309
    iput-object p1, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    goto/16 :goto_1

    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_e
    const/4 v0, 0x0

    goto :goto_5

    :cond_f
    const/4 v0, 0x0

    goto :goto_6

    :cond_10
    const/4 v0, 0x0

    goto :goto_7

    :cond_11
    const/4 v0, 0x0

    goto :goto_8
.end method


# virtual methods
.method public final a(IIIIILjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZLjava/util/Set;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/7zl;",
            ">;ZZZ",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1973256
    iput p1, p0, LX/DBm;->a:I

    .line 1973257
    iput p2, p0, LX/DBm;->b:I

    .line 1973258
    iput p3, p0, LX/DBm;->c:I

    .line 1973259
    iput p4, p0, LX/DBm;->d:I

    .line 1973260
    iput p5, p0, LX/DBm;->e:I

    .line 1973261
    iput-object p6, p0, LX/DBm;->f:Ljava/lang/String;

    .line 1973262
    iput-object p7, p0, LX/DBm;->g:Ljava/lang/String;

    .line 1973263
    iput-object p8, p0, LX/DBm;->h:Ljava/util/List;

    .line 1973264
    iput-object p9, p0, LX/DBm;->i:Ljava/util/List;

    .line 1973265
    iput-object p10, p0, LX/DBm;->j:Ljava/util/List;

    .line 1973266
    iput-object p11, p0, LX/DBm;->k:Ljava/util/List;

    .line 1973267
    iput-object p12, p0, LX/DBm;->l:Ljava/util/List;

    .line 1973268
    iput-boolean p13, p0, LX/DBm;->p:Z

    .line 1973269
    iput-boolean p14, p0, LX/DBm;->q:Z

    .line 1973270
    move/from16 v0, p15

    iput-boolean v0, p0, LX/DBm;->r:Z

    .line 1973271
    move-object/from16 v0, p16

    iput-object v0, p0, LX/DBm;->D:Ljava/util/Set;

    .line 1973272
    move-object/from16 v0, p17

    invoke-static {p0, v0}, LX/DBm;->g(LX/DBm;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1973273
    invoke-virtual {p0}, LX/DBm;->invalidate()V

    .line 1973274
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    .line 1973210
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1973211
    const/4 v3, 0x0

    .line 1973212
    iget-object v0, p0, LX/DBm;->B:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eJ;->j:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1973213
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1973214
    const-string v1, "ui p:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/DBm;->d:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973215
    const-string v1, " s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/DBm;->e:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973216
    const-string v1, "  col s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/DBm;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973217
    const-string v1, " f:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/DBm;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973218
    const-string v1, " a:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/DBm;->c:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973219
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1973220
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/DBm;->a(ILandroid/graphics/Canvas;)I

    move-result v0

    add-int/lit8 v1, v0, 0x0

    .line 1973221
    const/4 v2, 0x0

    const/16 v3, 0x1e

    const v6, -0xff0100

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    move-result-object v0

    .line 1973222
    int-to-float v1, v1

    const/4 v2, 0x3

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    sub-float v0, v2, v0

    add-float/2addr v0, v1

    float-to-int v1, v0

    .line 1973223
    const/4 v2, 0x0

    const/16 v3, 0x1e

    iget-object v4, p0, LX/DBm;->g:Ljava/lang/String;

    const v6, -0xff0100

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    move-result-object v0

    .line 1973224
    int-to-float v1, v1

    const/4 v2, 0x3

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    sub-float v0, v2, v0

    add-float/2addr v0, v1

    float-to-int v1, v0

    .line 1973225
    const/4 v2, 0x0

    const/16 v3, 0x1e

    iget-object v4, p0, LX/DBm;->f:Ljava/lang/String;

    const v6, -0xff0100

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    move-result-object v0

    .line 1973226
    int-to-float v1, v1

    const/4 v2, 0x3

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    sub-float v0, v2, v0

    add-float/2addr v0, v1

    float-to-int v3, v0

    .line 1973227
    :cond_0
    iget-object v0, p0, LX/DBm;->B:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eJ;->k:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1973228
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    add-int/lit16 v2, v0, -0x2bc

    iget-object v4, p0, LX/DBm;->h:Ljava/util/List;

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const-string v10, "Server"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, LX/DBm;->a(Landroid/graphics/Canvas;IILjava/util/List;ZZZZZLjava/lang/String;)I

    move-result v0

    add-int/2addr v3, v0

    .line 1973229
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    add-int/lit16 v2, v0, -0x2bc

    iget-object v4, p0, LX/DBm;->i:Ljava/util/List;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const-string v10, "Sorted"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, LX/DBm;->a(Landroid/graphics/Canvas;IILjava/util/List;ZZZZZLjava/lang/String;)I

    move-result v0

    add-int/2addr v3, v0

    .line 1973230
    iget-object v0, p0, LX/DBm;->j:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1973231
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    add-int/lit16 v2, v0, -0x2bc

    iget-object v4, p0, LX/DBm;->j:Ljava/util/List;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const-string v10, "Sorted Ads"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, LX/DBm;->a(Landroid/graphics/Canvas;IILjava/util/List;ZZZZZLjava/lang/String;)I

    move-result v0

    add-int/2addr v3, v0

    .line 1973232
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    add-int/lit16 v2, v0, -0x2bc

    iget-object v4, p0, LX/DBm;->l:Ljava/util/List;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "Pushed Down Ads"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, LX/DBm;->a(Landroid/graphics/Canvas;IILjava/util/List;ZZZZZLjava/lang/String;)I

    move-result v0

    add-int/2addr v3, v0

    .line 1973233
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    add-int/lit16 v2, v0, -0x2bc

    iget-object v4, p0, LX/DBm;->k:Ljava/util/List;

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const-string v10, "UI"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, LX/DBm;->a(Landroid/graphics/Canvas;IILjava/util/List;ZZZZZLjava/lang/String;)I

    move-result v0

    add-int/2addr v3, v0

    .line 1973234
    :cond_2
    iget-object v0, p0, LX/DBm;->B:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0eJ;->l:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1973235
    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_3

    .line 1973236
    add-int/lit8 v1, v3, 0x5

    .line 1973237
    iget-object v0, p0, LX/DBm;->E:Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {p0, v0}, LX/DBm;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v4

    .line 1973238
    const/4 v2, 0x0

    const/16 v3, 0x1e

    const v6, -0xff0100

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    move-result-object v0

    .line 1973239
    int-to-float v1, v1

    const/4 v2, 0x3

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    sub-float v0, v2, v0

    add-float/2addr v0, v1

    float-to-int v3, v0

    .line 1973240
    :cond_3
    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_4

    .line 1973241
    add-int/lit8 v1, v3, 0x5

    .line 1973242
    iget-object v0, p0, LX/DBm;->F:Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {p0, v0}, LX/DBm;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v4

    .line 1973243
    const/4 v2, 0x0

    const/16 v3, 0x1e

    const v6, -0xff0100

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    move-result-object v0

    .line 1973244
    int-to-float v1, v1

    const/4 v2, 0x3

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    sub-float v0, v2, v0

    add-float/2addr v0, v1

    float-to-int v3, v0

    .line 1973245
    :cond_4
    iget-object v0, p0, LX/DBm;->k:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/DBm;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/DBm;->d:I

    if-le v0, v1, :cond_6

    iget v0, p0, LX/DBm;->d:I

    if-ltz v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1973246
    if-eqz v0, :cond_5

    .line 1973247
    iget-object v0, p0, LX/DBm;->k:Ljava/util/List;

    iget v1, p0, LX/DBm;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zl;

    .line 1973248
    iget-boolean v1, v0, LX/7zl;->c:Z

    move v0, v1

    .line 1973249
    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1973250
    if-eqz v0, :cond_5

    .line 1973251
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Score: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/DBm;->k:Ljava/util/List;

    iget v2, p0, LX/DBm;->d:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zl;

    .line 1973252
    iget-wide v11, v0, LX/7zl;->e:D

    move-wide v4, v11

    .line 1973253
    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1973254
    add-int/lit8 v1, v3, 0x5

    const/4 v2, 0x0

    const/16 v3, 0x1e

    const v6, -0xff01

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, LX/DBm;->a(LX/DBm;IIILjava/lang/String;Landroid/graphics/Canvas;I)[F

    move-result-object v0

    iput-object v0, p0, LX/DBm;->s:[F

    .line 1973255
    :cond_5
    return-void

    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x2

    const v1, 0x5e5eea1c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1973184
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1973185
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1973186
    iget-object v7, p0, LX/DBm;->s:[F

    if-eqz v7, :cond_0

    iget-object v7, p0, LX/DBm;->s:[F

    array-length v7, v7

    const/4 p1, 0x4

    if-ge v7, p1, :cond_4

    :cond_0
    move v5, v6

    .line 1973187
    :cond_1
    :goto_0
    move v2, v5

    .line 1973188
    if-eqz v2, :cond_3

    .line 1973189
    iget v5, p0, LX/DBm;->d:I

    iget-object v6, p0, LX/DBm;->k:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_2

    iget v5, p0, LX/DBm;->d:I

    if-ltz v5, :cond_2

    .line 1973190
    invoke-virtual {p0}, LX/DBm;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v5, p0, LX/DBm;->k:Ljava/util/List;

    iget v7, p0, LX/DBm;->d:I

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7zl;

    .line 1973191
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1973192
    const-string v9, "client_seen_state : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1973193
    iget v12, v5, LX/7zl;->g:I

    int-to-double v12, v12

    move-wide v10, v12

    .line 1973194
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973195
    const-string v9, "story_ranking_time : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1973196
    iget-wide v12, v5, LX/7zl;->b:J

    move-wide v10, v12

    .line 1973197
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973198
    const-string v9, "image_cache_state : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1973199
    iget v12, v5, LX/7zl;->h:I

    int-to-double v12, v12

    move-wide v10, v12

    .line 1973200
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1973201
    const-string v9, "weight_final : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1973202
    iget-wide v12, v5, LX/7zl;->f:D

    move-wide v10, v12

    .line 1973203
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 1973204
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v5, v8

    .line 1973205
    const/4 v7, 0x0

    invoke-static {v6, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 1973206
    :cond_2
    const v2, -0x592d9113

    invoke-static {v4, v4, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1973207
    :goto_1
    return v0

    :cond_3
    const/4 v0, 0x0

    const v2, -0x3f762c30

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_1

    .line 1973208
    :cond_4
    iget-object v7, p0, LX/DBm;->s:[F

    aget v7, v7, v6

    cmpl-float v7, v2, v7

    if-lez v7, :cond_5

    iget-object v7, p0, LX/DBm;->s:[F

    const/4 p1, 0x2

    aget v7, v7, p1

    cmpg-float v7, v2, v7

    if-gez v7, :cond_5

    iget-object v7, p0, LX/DBm;->s:[F

    aget v7, v7, v5

    cmpl-float v7, v3, v7

    if-lez v7, :cond_5

    iget-object v7, p0, LX/DBm;->s:[F

    const/4 p1, 0x3

    aget v7, v7, p1

    const/high16 p1, 0x41f00000    # 30.0f

    add-float/2addr v7, p1

    cmpg-float v7, v3, v7

    if-ltz v7, :cond_1

    :cond_5
    move v5, v6

    .line 1973209
    goto/16 :goto_0
.end method
