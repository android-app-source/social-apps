.class public LX/DTp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/DTp;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2000795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2000796
    iput-object p1, p0, LX/DTp;->a:LX/0Or;

    .line 2000797
    return-void
.end method

.method public static a(LX/0QB;)LX/DTp;
    .locals 4

    .prologue
    .line 2000798
    sget-object v0, LX/DTp;->b:LX/DTp;

    if-nez v0, :cond_1

    .line 2000799
    const-class v1, LX/DTp;

    monitor-enter v1

    .line 2000800
    :try_start_0
    sget-object v0, LX/DTp;->b:LX/DTp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2000801
    if-eqz v2, :cond_0

    .line 2000802
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2000803
    new-instance v3, LX/DTp;

    const/16 p0, 0xc

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DTp;-><init>(LX/0Or;)V

    .line 2000804
    move-object v0, v3

    .line 2000805
    sput-object v0, LX/DTp;->b:LX/DTp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2000806
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2000807
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2000808
    :cond_1
    sget-object v0, LX/DTp;->b:LX/DTp;

    return-object v0

    .line 2000809
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2000810
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/DTp;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2000811
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/DTp;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2000812
    invoke-static {p0}, LX/DTp;->a(LX/DTp;)Landroid/content/Intent;

    move-result-object v0

    .line 2000813
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2000814
    const-string v1, "group_admin_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2000815
    const-string v1, "is_viewer_joined"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2000816
    const-string v1, "group_visibility"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2000817
    const-string v1, "group_url"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2000818
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUP_MEMBERSHIP_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2000819
    return-object v0
.end method
