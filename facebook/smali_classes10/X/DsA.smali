.class public final LX/DsA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)V
    .locals 0

    .prologue
    .line 2050107
    iput-object p1, p0, LX/DsA;->a:Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x72e5fd1a

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2050108
    iget-object v1, p0, LX/DsA;->a:Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;

    const/4 p1, 0x2

    .line 2050109
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.RINGTONE_PICKER"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2050110
    const-string v4, "android.intent.extra.ringtone.TITLE"

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f081178

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2050111
    const-string v4, "android.intent.extra.ringtone.SHOW_SILENT"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2050112
    const-string v4, "android.intent.extra.ringtone.SHOW_DEFAULT"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2050113
    const-string v4, "android.intent.extra.ringtone.EXISTING_URI"

    invoke-static {v1}, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->m(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2050114
    const-string v4, "android.intent.extra.ringtone.TYPE"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2050115
    const-string v4, "android.intent.extra.ringtone.DEFAULT_URI"

    invoke-static {p1}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2050116
    iget-object v4, v1, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x698

    invoke-interface {v4, v3, v5, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2050117
    const v1, -0x4c524889

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
