.class public final LX/D7K;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1966895
    const-class v1, Lcom/facebook/video/commercialbreak/protocol/FetchInstreamVideoAdsModels$InstreamVideoAdsQueryModel;

    const v0, -0x46067001

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "InstreamVideoAdsQuery"

    const-string v6, "2c319712abad91a47a46dabc6185cdae"

    const-string v7, "viewer"

    const-string v8, "10155261855071729"

    const/4 v9, 0x0

    .line 1966896
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1966897
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1966898
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1966899
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1966900
    sparse-switch v0, :sswitch_data_0

    .line 1966901
    :goto_0
    return-object p1

    .line 1966902
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1966903
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1966904
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1966905
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1966906
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1966907
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1966908
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1966909
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1966910
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1966911
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1966912
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1966913
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1966914
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1966915
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1966916
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1966917
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1966918
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1966919
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1966920
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1966921
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1966922
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1966923
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1966924
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1966925
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1966926
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1966927
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 1966928
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 1966929
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 1966930
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x76bbb26c -> :sswitch_2
        -0x6a24640d -> :sswitch_19
        -0x680de62a -> :sswitch_e
        -0x6326fdb3 -> :sswitch_d
        -0x626f1062 -> :sswitch_0
        -0x5fc5bdbc -> :sswitch_7
        -0x57984ae8 -> :sswitch_17
        -0x513764de -> :sswitch_1a
        -0x4496acc9 -> :sswitch_f
        -0x41a91745 -> :sswitch_15
        -0x41143822 -> :sswitch_9
        -0x3c54de38 -> :sswitch_12
        -0x3b85b241 -> :sswitch_1c
        -0x25a646c8 -> :sswitch_a
        -0x21572b1f -> :sswitch_3
        -0x1d6ce0bf -> :sswitch_14
        -0x1b87b280 -> :sswitch_c
        -0x15db59af -> :sswitch_1b
        -0x12efdeb3 -> :sswitch_10
        -0xe6b8bc8 -> :sswitch_5
        -0x3e446ed -> :sswitch_b
        -0x12603b3 -> :sswitch_18
        0xa1fa812 -> :sswitch_8
        0x194ae016 -> :sswitch_1
        0x214100e0 -> :sswitch_11
        0x291d8de0 -> :sswitch_16
        0x41490ad4 -> :sswitch_6
        0x6f2b3e31 -> :sswitch_4
        0x73a026b5 -> :sswitch_13
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1966931
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1966932
    :goto_1
    return v0

    .line 1966933
    :sswitch_0
    const-string v2, "9"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "10"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "18"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "20"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "24"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "26"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "27"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "28"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v2, "23"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    .line 1966934
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1966935
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1966936
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1966937
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1966938
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1966939
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1966940
    :pswitch_6
    const-string v0, "mobile"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1966941
    :pswitch_7
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 1966942
    :pswitch_8
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x39 -> :sswitch_0
        0x61f -> :sswitch_1
        0x627 -> :sswitch_2
        0x63e -> :sswitch_3
        0x641 -> :sswitch_8
        0x642 -> :sswitch_4
        0x644 -> :sswitch_5
        0x645 -> :sswitch_6
        0x646 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
