.class public LX/EGF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/EGF;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/AMT;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/EGG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/EGG;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2096911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096912
    iput-object p1, p0, LX/EGF;->b:Ljava/lang/String;

    .line 2096913
    iput-object p2, p0, LX/EGF;->c:Ljava/lang/String;

    .line 2096914
    iput-object p3, p0, LX/EGF;->d:Ljava/lang/String;

    .line 2096915
    iput-object p4, p0, LX/EGF;->e:Ljava/lang/String;

    .line 2096916
    iput-object p5, p0, LX/EGF;->f:LX/0Px;

    .line 2096917
    invoke-direct {p0}, LX/EGF;->a()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/EGF;->a:LX/0Px;

    .line 2096918
    return-void
.end method

.method private a()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/AMT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2096919
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2096920
    iget-object v0, p0, LX/EGF;->f:LX/0Px;

    if-eqz v0, :cond_0

    .line 2096921
    iget-object v0, p0, LX/EGF;->c:Ljava/lang/String;

    iget-object v1, p0, LX/EGF;->b:Ljava/lang/String;

    const-string v3, ".msqrd"

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/EGF;->d:Ljava/lang/String;

    const/4 v4, 0x1

    iget-object v5, p0, LX/EGF;->b:Ljava/lang/String;

    invoke-static {v0, v1, v3, v4, v5}, LX/AMT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)LX/AMT;

    move-result-object v0

    move-object v0, v0

    .line 2096922
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2096923
    iget-object v3, p0, LX/EGF;->f:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGG;

    .line 2096924
    iget-object v5, v0, LX/EGG;->a:Ljava/lang/String;

    iget-object p0, v0, LX/EGG;->b:Ljava/lang/String;

    invoke-static {v5, p0}, LX/AMT;->a(Ljava/lang/String;Ljava/lang/String;)LX/AMT;

    move-result-object v5

    move-object v0, v5

    .line 2096925
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2096926
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2096927
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel;)LX/EGF;
    .locals 11
    .param p0    # Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2096928
    if-nez p0, :cond_1

    .line 2096929
    :cond_0
    :goto_0
    return-object v0

    .line 2096930
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel;->a()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v2

    .line 2096931
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel;->j()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$ThumbnailImageModel;

    move-result-object v4

    .line 2096932
    if-eqz v2, :cond_0

    if-eqz v4, :cond_0

    .line 2096933
    invoke-virtual {v2}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->j()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    move-result-object v3

    .line 2096934
    if-eqz v3, :cond_0

    .line 2096935
    invoke-virtual {v3}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->l()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    move-result-object v5

    .line 2096936
    if-eqz v5, :cond_0

    .line 2096937
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2096938
    invoke-virtual {v3}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->j()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    .line 2096939
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v8, :cond_2

    invoke-virtual {v7, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;

    .line 2096940
    new-instance v9, LX/EGG;

    invoke-virtual {v0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v10, v0}, LX/EGG;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2096941
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2096942
    :cond_2
    new-instance v0, LX/EGF;

    invoke-virtual {v2}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$ThumbnailImageModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/EGF;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    goto :goto_0
.end method


# virtual methods
.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2096943
    check-cast p1, LX/EGF;

    .line 2096944
    iget-object v0, p1, LX/EGF;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGF;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2096945
    :cond_0
    const/4 v0, -0x1

    .line 2096946
    :goto_0
    return v0

    .line 2096947
    :cond_1
    iget-object v0, p1, LX/EGF;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/EGF;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2096948
    const/4 v0, 0x0

    goto :goto_0

    .line 2096949
    :cond_2
    iget-object v0, p0, LX/EGF;->b:Ljava/lang/String;

    iget-object v1, p1, LX/EGF;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2096950
    instance-of v0, p1, LX/EGF;

    if-nez v0, :cond_0

    .line 2096951
    const/4 v0, 0x0

    .line 2096952
    :goto_0
    return v0

    .line 2096953
    :cond_0
    if-ne p1, p0, :cond_1

    .line 2096954
    const/4 v0, 0x1

    goto :goto_0

    .line 2096955
    :cond_1
    check-cast p1, LX/EGF;

    .line 2096956
    iget-object v0, p0, LX/EGF;->b:Ljava/lang/String;

    iget-object v1, p1, LX/EGF;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2096957
    iget-object v0, p0, LX/EGF;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EGF;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 2096958
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/EGF;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EGF;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 2096959
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/EGF;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EGF;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 2096960
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/EGF;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/EGF;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 2096961
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/EGF;->f:LX/0Px;

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/EGF;->f:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 2096962
    return v0

    :cond_1
    move v0, v1

    .line 2096963
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2096964
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2096965
    goto :goto_2

    :cond_4
    move v0, v1

    .line 2096966
    goto :goto_3
.end method
