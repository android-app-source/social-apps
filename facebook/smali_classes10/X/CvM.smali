.class public LX/CvM;
.super LX/1Cd;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final h:Ljava/lang/Class;

.field private static volatile i:LX/CvM;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "LX/6VT;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/CvL;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public e:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Cve;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6VU;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1948131
    const-class v0, LX/CvM;

    sput-object v0, LX/CvM;->h:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1948079
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 1948080
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CvM;->a:Ljava/util/Map;

    .line 1948081
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CvM;->b:Ljava/util/Map;

    .line 1948082
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CvM;->c:Ljava/util/Map;

    .line 1948083
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1948084
    iput-object v0, p0, LX/CvM;->g:LX/0Ot;

    .line 1948085
    return-void
.end method

.method public static a(LX/0QB;)LX/CvM;
    .locals 6

    .prologue
    .line 1948116
    sget-object v0, LX/CvM;->i:LX/CvM;

    if-nez v0, :cond_1

    .line 1948117
    const-class v1, LX/CvM;

    monitor-enter v1

    .line 1948118
    :try_start_0
    sget-object v0, LX/CvM;->i:LX/CvM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1948119
    if-eqz v2, :cond_0

    .line 1948120
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1948121
    new-instance v5, LX/CvM;

    invoke-direct {v5}, LX/CvM;-><init>()V

    .line 1948122
    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/Cve;->a(LX/0QB;)LX/Cve;

    move-result-object v4

    check-cast v4, LX/Cve;

    const/16 p0, 0x1cb8

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1948123
    iput-object v3, v5, LX/CvM;->e:LX/0So;

    iput-object v4, v5, LX/CvM;->f:LX/Cve;

    iput-object p0, v5, LX/CvM;->g:LX/0Ot;

    .line 1948124
    move-object v0, v5

    .line 1948125
    sput-object v0, LX/CvM;->i:LX/CvM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1948126
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1948127
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1948128
    :cond_1
    sget-object v0, LX/CvM;->i:LX/CvM;

    return-object v0

    .line 1948129
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1948130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1948132
    iget-object v0, p0, LX/CvM;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1948133
    return-void
.end method

.method public final b(LX/0g8;)V
    .locals 6

    .prologue
    .line 1948095
    invoke-static {}, LX/1fG;->a()V

    .line 1948096
    iget-object v0, p0, LX/CvM;->b:Ljava/util/Map;

    sget-object v1, LX/CvL;->EXIT:LX/CvL;

    iget-object v2, p0, LX/CvM;->e:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948097
    iget-object v0, p0, LX/CvM;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 1948098
    iget-object v0, p0, LX/CvM;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1948099
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1948100
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1948101
    iget-object v1, p0, LX/CvM;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6VU;

    invoke-virtual {v1, v0}, LX/1BL;->c(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1fG;

    .line 1948102
    sget-boolean v5, LX/1fG;->i:Z

    if-nez v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    const-string p1, "Attempting to get image load times while they are being tracked"

    invoke-static {v5, p1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1948103
    iget-object v5, v1, LX/1fG;->f:Ljava/util/ArrayList;

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    move-object v1, v5

    .line 1948104
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1948105
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1948106
    iget-object v1, p0, LX/CvM;->a:Ljava/util/Map;

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948107
    iget-object v1, p0, LX/CvM;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    goto :goto_0

    .line 1948108
    :cond_2
    iget-object v0, p0, LX/CvM;->f:LX/Cve;

    iget-object v1, p0, LX/CvM;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iget-object v2, p0, LX/CvM;->a:Ljava/util/Map;

    iget-object v3, p0, LX/CvM;->b:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3}, LX/Cve;->a(LX/CwB;Ljava/util/Map;Ljava/util/Map;)V

    .line 1948109
    iget-object v0, p0, LX/CvM;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VU;

    invoke-virtual {v0}, LX/6VU;->a()V

    .line 1948110
    iget-object v0, p0, LX/CvM;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1948111
    iget-object v1, p0, LX/CvM;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6VU;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/1BL;->b(Ljava/lang/String;)V

    goto :goto_3

    .line 1948112
    :cond_3
    iget-object v0, p0, LX/CvM;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1948113
    iget-object v0, p0, LX/CvM;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1948114
    return-void

    .line 1948115
    :cond_4
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1948090
    iget-object v0, p0, LX/CvM;->b:Ljava/util/Map;

    sget-object v1, LX/CvL;->ENTER:LX/CvL;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1948091
    invoke-static {}, LX/1fG;->b()V

    .line 1948092
    iget-object v0, p0, LX/CvM;->b:Ljava/util/Map;

    sget-object v1, LX/CvL;->ENTER:LX/CvL;

    iget-object v2, p0, LX/CvM;->e:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948093
    iget-object v0, p0, LX/CvM;->f:LX/Cve;

    iget-object v1, p0, LX/CvM;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v0, v1}, LX/Cve;->b(LX/CwB;)V

    .line 1948094
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1948086
    iget-object v0, p0, LX/CvM;->b:Ljava/util/Map;

    sget-object v1, LX/CvL;->FIRST_SCROLL:LX/CvL;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1948087
    invoke-static {}, LX/1fG;->a()V

    .line 1948088
    iget-object v0, p0, LX/CvM;->b:Ljava/util/Map;

    sget-object v1, LX/CvL;->FIRST_SCROLL:LX/CvL;

    iget-object v2, p0, LX/CvM;->e:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948089
    :cond_0
    return-void
.end method
