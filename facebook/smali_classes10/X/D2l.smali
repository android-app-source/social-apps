.class public LX/D2l;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/D2k;",
        ">;",
        "Lcom/facebook/timeline/profilevideo/view/ProfileImageViewProvider;"
    }
.end annotation


# instance fields
.field public a:Landroid/widget/ImageView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Landroid/graphics/Bitmap;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1959242
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1959243
    return-void
.end method

.method public static d(LX/D2l;)V
    .locals 2

    .prologue
    .line 1959244
    iget-object v0, p0, LX/D2l;->a:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D2l;->b:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 1959245
    :cond_0
    :goto_0
    return-void

    .line 1959246
    :cond_1
    iget-object v0, p0, LX/D2l;->a:Landroid/widget/ImageView;

    iget-object v1, p0, LX/D2l;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1959247
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03132e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1959248
    new-instance v1, LX/D2k;

    invoke-direct {v1, v0}, LX/D2k;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 1959249
    check-cast p1, LX/D2k;

    .line 1959250
    const/4 v0, 0x1

    if-ne v0, p2, :cond_0

    .line 1959251
    iget-object v0, p1, LX/D2k;->l:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1959252
    iget-object v0, p1, LX/D2k;->l:Landroid/widget/ImageView;

    iput-object v0, p0, LX/D2l;->a:Landroid/widget/ImageView;

    .line 1959253
    invoke-static {p0}, LX/D2l;->d(LX/D2l;)V

    .line 1959254
    :goto_0
    return-void

    .line 1959255
    :cond_0
    iget-object v0, p1, LX/D2k;->l:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1959256
    const/4 v0, 0x3

    return v0
.end method
