.class public final LX/Cuc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Cub;

.field public final synthetic b:LX/Cuw;

.field public final synthetic c:Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;LX/Cub;LX/Cuw;)V
    .locals 0

    .prologue
    .line 1946936
    iput-object p1, p0, LX/Cuc;->c:Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;

    iput-object p2, p0, LX/Cuc;->a:LX/Cub;

    iput-object p3, p0, LX/Cuc;->b:LX/Cuw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x4053d077

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1946937
    iget-object v1, p0, LX/Cuc;->a:LX/Cub;

    invoke-virtual {v1}, LX/Cub;->a()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v1

    .line 1946938
    iget-object v2, p0, LX/Cuc;->c:Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;

    .line 1946939
    iget-boolean p1, v2, LX/Cud;->e:Z

    move v2, p1

    .line 1946940
    if-eqz v2, :cond_1

    iget-object v2, p0, LX/Cuc;->b:LX/Cuw;

    invoke-virtual {v2}, LX/Cuw;->c()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/Cuc;->b:LX/Cuw;

    invoke-virtual {v2}, LX/Cuw;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1946941
    :cond_0
    iget-object v1, p0, LX/Cuc;->c:Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;

    invoke-virtual {v1}, LX/Cud;->a()V

    .line 1946942
    :goto_0
    const v1, 0x6a91c8b2    # 8.8121E25f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1946943
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1946944
    iget-object v1, p0, LX/Cuc;->b:LX/Cuw;

    invoke-virtual {v1}, LX/Cuw;->f()V

    .line 1946945
    iget-object v1, p0, LX/Cuc;->a:LX/Cub;

    sget-object v2, LX/Crd;->CONTROLLER_PAUSE:LX/Crd;

    invoke-virtual {v1, v2}, LX/Cts;->a(LX/Crd;)Z

    goto :goto_0

    .line 1946946
    :cond_2
    iget-object v1, p0, LX/Cuc;->b:LX/Cuw;

    invoke-virtual {v1}, LX/Cuw;->g()V

    .line 1946947
    iget-object v1, p0, LX/Cuc;->a:LX/Cub;

    sget-object v2, LX/Crd;->CLICK_MEDIA:LX/Crd;

    invoke-virtual {v1, v2}, LX/Cts;->a(LX/Crd;)Z

    goto :goto_0
.end method
