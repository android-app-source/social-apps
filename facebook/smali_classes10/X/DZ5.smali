.class public LX/DZ5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0zG;


# direct methods
.method public constructor <init>(LX/0zG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2013124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2013125
    iput-object p1, p0, LX/DZ5;->a:LX/0zG;

    .line 2013126
    return-void
.end method

.method public static b(LX/0QB;)LX/DZ5;
    .locals 2

    .prologue
    .line 2013127
    new-instance v1, LX/DZ5;

    invoke-static {p0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v0

    check-cast v0, LX/0zG;

    invoke-direct {v1, v0}, LX/DZ5;-><init>(LX/0zG;)V

    .line 2013128
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p4    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2013129
    const-class v0, LX/1ZF;

    invoke-virtual {p1, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2013130
    if-eqz v0, :cond_0

    if-eqz p5, :cond_0

    .line 2013131
    invoke-interface {v0, p2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2013132
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2013133
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 2013134
    iput-object p3, v1, LX/108;->g:Ljava/lang/String;

    .line 2013135
    move-object v1, v1

    .line 2013136
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2013137
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2013138
    new-instance v1, LX/DZ4;

    invoke-direct {v1, p0, p5}, LX/DZ4;-><init>(LX/DZ5;Landroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2013139
    :cond_0
    iget-object v0, p0, LX/DZ5;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2013140
    if-eqz v0, :cond_1

    if-eqz p4, :cond_1

    .line 2013141
    invoke-interface {v0, p4}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2013142
    :cond_1
    return-void
.end method
