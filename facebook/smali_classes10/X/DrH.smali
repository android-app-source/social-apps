.class public LX/DrH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2049142
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "notifications_bucket_settings/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/DrH;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2049132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LX/0Tn;
    .locals 2

    .prologue
    .line 2049141
    sget-object v0, LX/DrH;->a:LX/0Tn;

    const-string v1, "total_buckets/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static a(I)LX/0Tn;
    .locals 2

    .prologue
    .line 2049140
    invoke-static {p0}, LX/DrH;->j(I)LX/0Tn;

    move-result-object v0

    const-string v1, "bucket_type/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static b(I)LX/0Tn;
    .locals 2

    .prologue
    .line 2049139
    invoke-static {p0}, LX/DrH;->j(I)LX/0Tn;

    move-result-object v0

    const-string v1, "title_text/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static c(I)LX/0Tn;
    .locals 2

    .prologue
    .line 2049138
    invoke-static {p0}, LX/DrH;->j(I)LX/0Tn;

    move-result-object v0

    const-string v1, "icon_uri/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static d(I)LX/0Tn;
    .locals 2

    .prologue
    .line 2049143
    invoke-static {p0}, LX/DrH;->j(I)LX/0Tn;

    move-result-object v0

    const-string v1, "icon_width/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static e(I)LX/0Tn;
    .locals 2

    .prologue
    .line 2049137
    invoke-static {p0}, LX/DrH;->j(I)LX/0Tn;

    move-result-object v0

    const-string v1, "icon_height/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static f(I)LX/0Tn;
    .locals 2

    .prologue
    .line 2049136
    invoke-static {p0}, LX/DrH;->j(I)LX/0Tn;

    move-result-object v0

    const-string v1, "seen_filter/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static g(I)LX/0Tn;
    .locals 2

    .prologue
    .line 2049135
    invoke-static {p0}, LX/DrH;->j(I)LX/0Tn;

    move-result-object v0

    const-string v1, "max_count/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static h(I)LX/0Tn;
    .locals 2

    .prologue
    .line 2049134
    invoke-static {p0}, LX/DrH;->j(I)LX/0Tn;

    move-result-object v0

    const-string v1, "max_impression_count/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static i(I)LX/0Tn;
    .locals 2

    .prologue
    .line 2049133
    invoke-static {p0}, LX/DrH;->j(I)LX/0Tn;

    move-result-object v0

    const-string v1, "min_to_expire/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method private static j(I)LX/0Tn;
    .locals 3

    .prologue
    .line 2049131
    sget-object v0, LX/DrH;->a:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
