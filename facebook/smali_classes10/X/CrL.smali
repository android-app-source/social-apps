.class public LX/CrL;
.super LX/0xh;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:LX/CrK;

.field public final c:LX/CqX;

.field public d:LX/CrS;

.field public e:LX/CrS;

.field public f:LX/CrS;


# direct methods
.method public constructor <init>(LX/CrK;LX/CqX;)V
    .locals 2

    .prologue
    .line 1940803
    invoke-direct {p0}, LX/0xh;-><init>()V

    .line 1940804
    const-class v0, LX/CrL;

    invoke-virtual {p1}, LX/CrK;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/CrL;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 1940805
    iput-object p1, p0, LX/CrL;->b:LX/CrK;

    .line 1940806
    iput-object p2, p0, LX/CrL;->c:LX/CqX;

    .line 1940807
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CrL;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object p0

    check-cast p0, LX/Chv;

    iput-object p0, p1, LX/CrL;->a:LX/Chv;

    return-void
.end method

.method private e(LX/0wd;)V
    .locals 2

    .prologue
    .line 1940808
    iget-object v0, p0, LX/CrL;->b:LX/CrK;

    .line 1940809
    iget-object v1, v0, LX/CrK;->c:LX/0wd;

    if-ne p1, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1940810
    if-nez v0, :cond_0

    .line 1940811
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Caller Spring is not its TransitionSpring"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1940812
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 6

    .prologue
    .line 1940813
    invoke-super {p0, p1}, LX/0xh;->a(LX/0wd;)V

    .line 1940814
    invoke-direct {p0, p1}, LX/CrL;->e(LX/0wd;)V

    .line 1940815
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v1, v0

    .line 1940816
    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_1

    .line 1940817
    iget-object v0, p0, LX/CrL;->b:LX/CrK;

    .line 1940818
    iget-object v2, v0, LX/CrK;->e:LX/Cqv;

    move-object v0, v2

    .line 1940819
    iget-object v2, p0, LX/CrL;->e:LX/CrS;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/CrL;->e:LX/CrS;

    invoke-interface {v2}, LX/CrS;->a()LX/Cqv;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1940820
    if-eqz v0, :cond_3

    .line 1940821
    :cond_1
    iget-object v0, p0, LX/CrL;->b:LX/CrK;

    .line 1940822
    iget-object v1, v0, LX/CrK;->e:LX/Cqv;

    move-object v0, v1

    .line 1940823
    iget-object v1, p0, LX/CrL;->c:LX/CqX;

    invoke-virtual {v1}, LX/CqX;->g()LX/CrS;

    move-result-object v1

    iput-object v1, p0, LX/CrL;->d:LX/CrS;

    .line 1940824
    iget-object v1, p0, LX/CrL;->c:LX/CqX;

    invoke-virtual {v1, v0}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v1

    iput-object v1, p0, LX/CrL;->e:LX/CrS;

    .line 1940825
    const/4 v1, 0x0

    iput-object v1, p0, LX/CrL;->f:LX/CrS;

    .line 1940826
    :cond_2
    :goto_1
    return-void

    .line 1940827
    :cond_3
    iget-object v0, p0, LX/CrL;->d:LX/CrS;

    iget-object v2, p0, LX/CrL;->e:LX/CrS;

    invoke-interface {v0, v2, v1}, LX/CrS;->a(LX/CrS;F)LX/CrS;

    move-result-object v0

    .line 1940828
    iget-object v2, p0, LX/CrL;->f:LX/CrS;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1940829
    iget-object v2, p0, LX/CrL;->c:LX/CqX;

    invoke-virtual {v2, v0}, LX/CqX;->a(LX/CrS;)V

    .line 1940830
    iput-object v0, p0, LX/CrL;->f:LX/CrS;

    .line 1940831
    iget-object v2, p0, LX/CrL;->a:LX/Chv;

    new-instance v3, LX/CiX;

    iget-object v0, p0, LX/CrL;->c:LX/CqX;

    .line 1940832
    iget-object v4, v0, LX/CqX;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 1940833
    iget-object v0, p0, LX/CrL;->e:LX/CrS;

    invoke-interface {v0}, LX/CrS;->a()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v0, v1, v5}, LX/CiX;-><init>(Ljava/lang/Object;LX/Cqw;FZ)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 6

    .prologue
    .line 1940834
    invoke-super {p0, p1}, LX/0xh;->b(LX/0wd;)V

    .line 1940835
    invoke-direct {p0, p1}, LX/CrL;->e(LX/0wd;)V

    .line 1940836
    iget-object v0, p0, LX/CrL;->e:LX/CrS;

    if-eqz v0, :cond_0

    .line 1940837
    iget-object v0, p0, LX/CrL;->c:LX/CqX;

    iget-object v1, p0, LX/CrL;->e:LX/CrS;

    invoke-virtual {v0, v1}, LX/CqX;->a(LX/CrS;)V

    .line 1940838
    iget-object v1, p0, LX/CrL;->a:LX/Chv;

    new-instance v2, LX/CiX;

    iget-object v0, p0, LX/CrL;->c:LX/CqX;

    .line 1940839
    iget-object v3, v0, LX/CqX;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 1940840
    iget-object v0, p0, LX/CrL;->e:LX/CrS;

    invoke-interface {v0}, LX/CrS;->a()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v4

    double-to-float v4, v4

    const/4 v5, 0x1

    invoke-direct {v2, v3, v0, v4, v5}, LX/CiX;-><init>(Ljava/lang/Object;LX/Cqw;FZ)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1940841
    const/4 v0, 0x0

    iput-object v0, p0, LX/CrL;->e:LX/CrS;

    .line 1940842
    :cond_0
    return-void
.end method
