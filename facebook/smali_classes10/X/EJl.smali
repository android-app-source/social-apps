.class public final LX/EJl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;LX/CzL;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2105120
    iput-object p1, p0, LX/EJl;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    iput-object p2, p0, LX/EJl;->a:LX/CzL;

    iput-object p3, p0, LX/EJl;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 17

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const v3, 0x480b7a64

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 2105121
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EJl;->a:LX/CzL;

    invoke-static {v1}, LX/EJf;->a(LX/CzL;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 2105122
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v1, v2, :cond_1

    .line 2105123
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EJl;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    iget-object v2, v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->c:LX/7j6;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EJl;->a:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/A2T;

    invoke-interface {v1}, LX/A2T;->dW_()Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/7iP;->GLOBAL_SEARCH:LX/7iP;

    invoke-virtual {v2, v1, v3}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 2105124
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EJl;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    iget-object v12, v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->f:LX/CvY;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EJl;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v13

    sget-object v14, LX/8ch;->CLICK:LX/8ch;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EJl;->a:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->d()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, LX/EJl;->a:LX/CzL;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EJl;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->d(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;)LX/CvY;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EJl;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EJl;->a:LX/CzL;

    invoke-virtual {v2}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/EJl;->a:LX/CzL;

    invoke-virtual {v3}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ae()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/EJl;->b:LX/1Pn;

    check-cast v4, LX/CxP;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/EJl;->a:LX/CzL;

    invoke-virtual {v5}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v5

    invoke-interface {v4, v5}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/EJl;->a:LX/CzL;

    invoke-virtual {v5}, LX/CzL;->d()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/EJl;->a:LX/CzL;

    invoke-virtual {v6}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v6

    invoke-static {v6}, LX/8eM;->j(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/EJl;->a:LX/CzL;

    invoke-virtual {v7}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v7

    sget-object v8, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/EJl;->a:LX/CzL;

    invoke-virtual {v9}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/A2T;

    invoke-interface {v9}, LX/A2T;->dW_()Ljava/lang/String;

    move-result-object v9

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v10

    invoke-static/range {v1 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v1, v12

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    move-object/from16 v5, v16

    invoke-virtual/range {v1 .. v6}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2105125
    const v1, -0x5be8e32f

    invoke-static {v1, v11}, LX/02F;->a(II)V

    return-void

    .line 2105126
    :cond_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v1, v2, :cond_2

    .line 2105127
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EJl;->a:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/A2T;

    invoke-interface {v1}, LX/A2T;->aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v1

    .line 2105128
    new-instance v2, LX/89k;

    invoke-direct {v2}, LX/89k;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/89k;->a(Ljava/lang/String;)LX/89k;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/89k;->b(Ljava/lang/String;)LX/89k;

    move-result-object v1

    invoke-virtual {v1}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v1

    .line 2105129
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EJl;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->d:LX/0hy;

    invoke-interface {v2, v1}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v1

    .line 2105130
    if-eqz v1, :cond_0

    .line 2105131
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EJl;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/EJl;->b:LX/1Pn;

    invoke-interface {v3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2105132
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unsupported product item role"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const v2, 0x5d03193d

    invoke-static {v2, v11}, LX/02F;->a(II)V

    throw v1
.end method
