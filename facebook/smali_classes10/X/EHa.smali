.class public LX/EHa;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/63a;


# instance fields
.field private a:Lcom/facebook/rtc/views/RtcPulsingCircleView;

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2099809
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2099810
    const/4 v0, 0x0

    iput v0, p0, LX/EHa;->c:I

    .line 2099811
    invoke-direct {p0}, LX/EHa;->a()V

    .line 2099812
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2099833
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2099834
    iput v2, p0, LX/EHa;->c:I

    .line 2099835
    if-eqz p2, :cond_0

    .line 2099836
    sget-object v0, LX/03r;->RtcPulsingCircleVideoButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2099837
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, LX/EHa;->c:I

    .line 2099838
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2099839
    :cond_0
    invoke-direct {p0}, LX/EHa;->a()V

    .line 2099840
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2099825
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2099826
    iput v2, p0, LX/EHa;->c:I

    .line 2099827
    if-eqz p2, :cond_0

    .line 2099828
    sget-object v0, LX/03r;->RtcPulsingCircleVideoButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2099829
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, LX/EHa;->c:I

    .line 2099830
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2099831
    :cond_0
    invoke-direct {p0}, LX/EHa;->a()V

    .line 2099832
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2099819
    const v0, 0x7f031240

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2099820
    const v0, 0x7f0d2adc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcPulsingCircleView;

    iput-object v0, p0, LX/EHa;->a:Lcom/facebook/rtc/views/RtcPulsingCircleView;

    .line 2099821
    const v0, 0x7f0d2adb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/EHa;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2099822
    iget-object v0, p0, LX/EHa;->a:Lcom/facebook/rtc/views/RtcPulsingCircleView;

    iget v1, p0, LX/EHa;->c:I

    .line 2099823
    iput v1, v0, Lcom/facebook/rtc/views/RtcPulsingCircleView;->k:I

    .line 2099824
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 2099817
    invoke-super {p0, p2, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2099818
    return-void
.end method

.method public setButtonTintColor(I)V
    .locals 1

    .prologue
    .line 2099813
    iget-object v0, p0, LX/EHa;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2099814
    iget-object v0, p0, LX/EHa;->a:Lcom/facebook/rtc/views/RtcPulsingCircleView;

    .line 2099815
    iput p1, v0, Lcom/facebook/rtc/views/RtcPulsingCircleView;->j:I

    .line 2099816
    return-void
.end method
