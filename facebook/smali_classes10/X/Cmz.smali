.class public LX/Cmz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/Cml;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/richdocument/model/style/BlockStyler",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/Cms;

.field private final b:LX/Cmj;

.field private final c:LX/Cmq;

.field private final d:LX/Cmk;


# direct methods
.method public constructor <init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;)V
    .locals 0

    .prologue
    .line 1933666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933667
    iput-object p1, p0, LX/Cmz;->a:LX/Cms;

    .line 1933668
    iput-object p2, p0, LX/Cmz;->b:LX/Cmj;

    .line 1933669
    iput-object p3, p0, LX/Cmz;->c:LX/Cmq;

    .line 1933670
    iput-object p4, p0, LX/Cmz;->d:LX/Cmk;

    .line 1933671
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;LX/Cml;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 1933672
    iget-object v0, p0, LX/Cmz;->a:LX/Cms;

    if-eqz v0, :cond_0

    .line 1933673
    iget-object v0, p0, LX/Cmz;->a:LX/Cms;

    invoke-interface {v0, p1, p2}, LX/Cms;->a(Landroid/view/View;LX/Cml;)V

    .line 1933674
    :cond_0
    iget-object v0, p0, LX/Cmz;->b:LX/Cmj;

    if-eqz v0, :cond_1

    .line 1933675
    iget-object v0, p0, LX/Cmz;->b:LX/Cmj;

    invoke-interface {v0, p1, p2}, LX/Cmj;->a(Landroid/view/View;LX/Cml;)V

    .line 1933676
    :cond_1
    iget-object v0, p0, LX/Cmz;->c:LX/Cmq;

    if-eqz v0, :cond_2

    .line 1933677
    iget-object v0, p0, LX/Cmz;->c:LX/Cmq;

    invoke-interface {v0, p1, p2}, LX/Cmq;->a(Landroid/view/View;LX/Cml;)V

    .line 1933678
    :cond_2
    iget-object v0, p0, LX/Cmz;->d:LX/Cmk;

    if-eqz v0, :cond_3

    .line 1933679
    iget-object v0, p0, LX/Cmz;->d:LX/Cmk;

    invoke-interface {v0, p1, p2}, LX/Cmk;->a(Landroid/view/View;LX/Cml;)V

    .line 1933680
    :cond_3
    return-void
.end method
