.class public LX/DQX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1g8;


# direct methods
.method public constructor <init>(LX/0Or;LX/1g8;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1g8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1994683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1994684
    iput-object p1, p0, LX/DQX;->a:LX/0Or;

    .line 1994685
    iput-object p2, p0, LX/DQX;->b:LX/1g8;

    .line 1994686
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 1994687
    iget-object v0, p0, LX/DQX;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1994688
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1994689
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1994690
    const-string v2, "source"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1994691
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1994692
    const-string v3, "source"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1994693
    const-string v0, "groupId"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1994694
    iget-object v0, p0, LX/DQX;->b:LX/1g8;

    .line 1994695
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "admin_panel_member_requests_view"

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "member_requests"

    .line 1994696
    iput-object p0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1994697
    move-object v3, v3

    .line 1994698
    const-string p0, "group_id"

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1994699
    iget-object p0, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1994700
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    const-string v1, "/groups_pending_member_queue"

    .line 1994701
    iput-object v1, v0, LX/98r;->a:Ljava/lang/String;

    .line 1994702
    move-object v0, v0

    .line 1994703
    iput-object v2, v0, LX/98r;->f:Landroid/os/Bundle;

    .line 1994704
    move-object v0, v0

    .line 1994705
    const-string v1, "FBGroupsPendingMemberQueueRoute"

    .line 1994706
    iput-object v1, v0, LX/98r;->b:Ljava/lang/String;

    .line 1994707
    move-object v0, v0

    .line 1994708
    const-string v1, "member_requests"

    .line 1994709
    iput-object v1, v0, LX/98r;->g:Ljava/lang/String;

    .line 1994710
    move-object v0, v0

    .line 1994711
    const v1, 0x7f08305f

    .line 1994712
    iput v1, v0, LX/98r;->d:I

    .line 1994713
    move-object v0, v0

    .line 1994714
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 1994715
    invoke-static {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    .line 1994716
    :goto_0
    return-object v0

    .line 1994717
    :cond_0
    new-instance v0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    invoke-direct {v0}, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;-><init>()V

    .line 1994718
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method
