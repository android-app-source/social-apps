.class public LX/Eei;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1wq;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2153219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2153220
    iput-object p1, p0, LX/Eei;->a:Landroid/content/Context;

    .line 2153221
    return-void
.end method

.method private static a([Landroid/content/pm/Signature;[Landroid/content/pm/Signature;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2153208
    if-eqz p0, :cond_0

    array-length v2, p0

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_1

    .line 2153209
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Apps should have valid signatures."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2153210
    :cond_1
    array-length v2, p0

    array-length v3, p1

    if-eq v2, v3, :cond_3

    .line 2153211
    :cond_2
    :goto_0
    return v0

    .line 2153212
    :cond_3
    array-length v4, p0

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_5

    aget-object v5, p0, v3

    .line 2153213
    array-length v6, p1

    move v2, v0

    :goto_2
    if-ge v2, v6, :cond_6

    aget-object v7, p1, v2

    .line 2153214
    invoke-virtual {v7}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    move v2, v1

    .line 2153215
    :goto_3
    if-eqz v2, :cond_2

    .line 2153216
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2153217
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 2153218
    goto :goto_0

    :cond_6
    move v2, v0

    goto :goto_3
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)[Landroid/content/pm/Signature;
    .locals 2

    .prologue
    .line 2153222
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2153223
    const/16 v1, 0x40

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 2153224
    if-nez v0, :cond_0

    .line 2153225
    const/4 v0, 0x0

    .line 2153226
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)[Landroid/content/pm/Signature;
    .locals 2

    .prologue
    .line 2153204
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2153205
    const/16 v1, 0x40

    :try_start_0
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2153206
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    :goto_0
    return-object v0

    .line 2153207
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/EeX;)Z
    .locals 3

    .prologue
    .line 2153190
    iget-object v0, p0, LX/Eei;->a:Landroid/content/Context;

    .line 2153191
    iget-object v1, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v1, v1, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    .line 2153192
    invoke-static {v0, v1}, LX/Eei;->b(Landroid/content/Context;Ljava/lang/String;)[Landroid/content/pm/Signature;

    move-result-object v1

    move-object v0, v1

    .line 2153193
    if-nez v0, :cond_0

    .line 2153194
    const/4 v0, 0x1

    .line 2153195
    :goto_0
    return v0

    .line 2153196
    :cond_0
    iget-object v1, p1, LX/EeX;->localFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 2153197
    iget-object v2, p0, LX/Eei;->a:Landroid/content/Context;

    invoke-static {v2, v1}, LX/Eei;->a(Landroid/content/Context;Ljava/lang/String;)[Landroid/content/pm/Signature;

    move-result-object v1

    .line 2153198
    invoke-static {v0, v1}, LX/Eei;->a([Landroid/content/pm/Signature;[Landroid/content/pm/Signature;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2153199
    iget-object v0, p0, LX/Eei;->a:Landroid/content/Context;

    invoke-static {v0, p1}, LX/Eei;->b(Landroid/content/Context;Ljava/lang/String;)[Landroid/content/pm/Signature;

    move-result-object v0

    .line 2153200
    if-nez v0, :cond_0

    .line 2153201
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Installed package not found"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2153202
    :cond_0
    iget-object v1, p0, LX/Eei;->a:Landroid/content/Context;

    invoke-static {v1, p2}, LX/Eei;->a(Landroid/content/Context;Ljava/lang/String;)[Landroid/content/pm/Signature;

    move-result-object v1

    .line 2153203
    invoke-static {v0, v1}, LX/Eei;->a([Landroid/content/pm/Signature;[Landroid/content/pm/Signature;)Z

    move-result v0

    return v0
.end method
