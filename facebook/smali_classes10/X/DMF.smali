.class public final LX/DMF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DMG;


# direct methods
.method public constructor <init>(LX/DMG;)V
    .locals 0

    .prologue
    .line 1989694
    iput-object p1, p0, LX/DMF;->a:LX/DMG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x46cebece

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1989695
    iget-object v1, p0, LX/DMF;->a:LX/DMG;

    iget-object v1, v1, LX/DMG;->b:LX/DMJ;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/DMF;->a:LX/DMG;

    iget-object v3, v3, LX/DMG;->a:Lcom/facebook/events/model/Event;

    .line 1989696
    iget-object p0, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, p0

    .line 1989697
    invoke-static {v3}, LX/5QR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1989698
    iget-object v5, v1, LX/DMJ;->a:LX/17W;

    invoke-virtual {v5, v2, p0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1989699
    new-instance p1, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {p1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, LX/DMJ;->e:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ComponentName;

    invoke-virtual {p1, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v5

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v5, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v5

    .line 1989700
    iget-object p0, v1, LX/DMJ;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v5, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1989701
    :cond_0
    const v1, 0x68efd85

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
