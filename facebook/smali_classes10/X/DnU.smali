.class public final enum LX/DnU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DnU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DnU;

.field public static final enum FBUI:LX/DnU;

.field public static final enum FIG:LX/DnU;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2040444
    new-instance v0, LX/DnU;

    const-string v1, "FBUI"

    invoke-direct {v0, v1, v2}, LX/DnU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DnU;->FBUI:LX/DnU;

    .line 2040445
    new-instance v0, LX/DnU;

    const-string v1, "FIG"

    invoke-direct {v0, v1, v3}, LX/DnU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DnU;->FIG:LX/DnU;

    .line 2040446
    const/4 v0, 0x2

    new-array v0, v0, [LX/DnU;

    sget-object v1, LX/DnU;->FBUI:LX/DnU;

    aput-object v1, v0, v2

    sget-object v1, LX/DnU;->FIG:LX/DnU;

    aput-object v1, v0, v3

    sput-object v0, LX/DnU;->$VALUES:[LX/DnU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2040443
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DnU;
    .locals 1

    .prologue
    .line 2040447
    const-class v0, LX/DnU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DnU;

    return-object v0
.end method

.method public static values()[LX/DnU;
    .locals 1

    .prologue
    .line 2040442
    sget-object v0, LX/DnU;->$VALUES:[LX/DnU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DnU;

    return-object v0
.end method
