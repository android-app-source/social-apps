.class public final LX/E9J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;)V
    .locals 0

    .prologue
    .line 2084044
    iput-object p1, p0, LX/E9J;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2084045
    iget-object v0, p0, LX/E9J;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iget-object v1, v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->b:LX/1vi;

    new-instance v2, LX/CgR;

    iget-object v0, p0, LX/E9J;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iget-boolean v0, v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->o:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, LX/E9J;->a:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iget-object v3, v3, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->i:LX/2ja;

    invoke-virtual {v3}, LX/2ja;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/CgR;-><init>(ZLjava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2084046
    return-void

    .line 2084047
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2084048
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2084049
    invoke-direct {p0}, LX/E9J;->a()V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2084050
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2084051
    return-void
.end method
