.class public LX/ELr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/ELB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/ELB",
            "<TE;",
            "LX/8d0;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/EME;

.field public final c:LX/ELu;


# direct methods
.method public constructor <init>(LX/ELB;LX/EME;LX/ELu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2109892
    iput-object p1, p0, LX/ELr;->a:LX/ELB;

    .line 2109893
    iput-object p2, p0, LX/ELr;->b:LX/EME;

    .line 2109894
    iput-object p3, p0, LX/ELr;->c:LX/ELu;

    .line 2109895
    return-void
.end method

.method public static a(LX/0QB;)LX/ELr;
    .locals 6

    .prologue
    .line 2109896
    const-class v1, LX/ELr;

    monitor-enter v1

    .line 2109897
    :try_start_0
    sget-object v0, LX/ELr;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109898
    sput-object v2, LX/ELr;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109899
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109900
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109901
    new-instance p0, LX/ELr;

    invoke-static {v0}, LX/ELB;->a(LX/0QB;)LX/ELB;

    move-result-object v3

    check-cast v3, LX/ELB;

    invoke-static {v0}, LX/EME;->a(LX/0QB;)LX/EME;

    move-result-object v4

    check-cast v4, LX/EME;

    invoke-static {v0}, LX/ELu;->a(LX/0QB;)LX/ELu;

    move-result-object v5

    check-cast v5, LX/ELu;

    invoke-direct {p0, v3, v4, v5}, LX/ELr;-><init>(LX/ELB;LX/EME;LX/ELu;)V

    .line 2109902
    move-object v0, p0

    .line 2109903
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109904
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ELr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109905
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109906
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
