.class public LX/Ess;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:LX/Esf;

.field public final b:Landroid/content/res/Resources;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/C51;

.field public final e:LX/1Kf;

.field public final f:LX/03V;

.field public final g:LX/1Nq;

.field public final h:LX/0Uh;

.field public final i:LX/Es5;

.field public final j:LX/1Cn;


# direct methods
.method public constructor <init>(LX/Esf;Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Kf;LX/03V;LX/1Nq;LX/0Uh;LX/Es5;LX/1Cn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2176600
    iput-object p1, p0, LX/Ess;->a:LX/Esf;

    .line 2176601
    iput-object p2, p0, LX/Ess;->b:Landroid/content/res/Resources;

    .line 2176602
    iput-object p3, p0, LX/Ess;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2176603
    iput-object p4, p0, LX/Ess;->d:LX/C51;

    .line 2176604
    iput-object p5, p0, LX/Ess;->e:LX/1Kf;

    .line 2176605
    iput-object p6, p0, LX/Ess;->f:LX/03V;

    .line 2176606
    iput-object p7, p0, LX/Ess;->g:LX/1Nq;

    .line 2176607
    iput-object p8, p0, LX/Ess;->h:LX/0Uh;

    .line 2176608
    iput-object p9, p0, LX/Ess;->i:LX/Es5;

    .line 2176609
    iput-object p10, p0, LX/Ess;->j:LX/1Cn;

    .line 2176610
    return-void
.end method

.method public static a(LX/0QB;)LX/Ess;
    .locals 14

    .prologue
    .line 2176611
    const-class v1, LX/Ess;

    monitor-enter v1

    .line 2176612
    :try_start_0
    sget-object v0, LX/Ess;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176613
    sput-object v2, LX/Ess;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176614
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176615
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176616
    new-instance v3, LX/Ess;

    invoke-static {v0}, LX/Esf;->a(LX/0QB;)LX/Esf;

    move-result-object v4

    check-cast v4, LX/Esf;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    const-class v7, LX/C51;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/C51;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v8

    check-cast v8, LX/1Kf;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v10

    check-cast v10, LX/1Nq;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {v0}, LX/Es5;->a(LX/0QB;)LX/Es5;

    move-result-object v12

    check-cast v12, LX/Es5;

    invoke-static {v0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v13

    check-cast v13, LX/1Cn;

    invoke-direct/range {v3 .. v13}, LX/Ess;-><init>(LX/Esf;Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Kf;LX/03V;LX/1Nq;LX/0Uh;LX/Es5;LX/1Cn;)V

    .line 2176617
    move-object v0, v3

    .line 2176618
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176619
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ess;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176620
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
