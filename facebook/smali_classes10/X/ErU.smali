.class public LX/ErU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/ErU;


# instance fields
.field private final a:LX/0Yj;

.field private final b:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2173040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2173041
    iput-object p1, p0, LX/ErU;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2173042
    new-instance v0, LX/0Yj;

    const v1, 0x60014

    const-string v2, "EventInviteTTI"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "event_invite"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    .line 2173043
    iput-boolean v4, v0, LX/0Yj;->n:Z

    .line 2173044
    move-object v0, v0

    .line 2173045
    iput-object v0, p0, LX/ErU;->a:LX/0Yj;

    .line 2173046
    return-void
.end method

.method public static a(LX/0QB;)LX/ErU;
    .locals 4

    .prologue
    .line 2173027
    sget-object v0, LX/ErU;->c:LX/ErU;

    if-nez v0, :cond_1

    .line 2173028
    const-class v1, LX/ErU;

    monitor-enter v1

    .line 2173029
    :try_start_0
    sget-object v0, LX/ErU;->c:LX/ErU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2173030
    if-eqz v2, :cond_0

    .line 2173031
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2173032
    new-instance p0, LX/ErU;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v3

    check-cast v3, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v3}, LX/ErU;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 2173033
    move-object v0, p0

    .line 2173034
    sput-object v0, LX/ErU;->c:LX/ErU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173035
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2173036
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2173037
    :cond_1
    sget-object v0, LX/ErU;->c:LX/ErU;

    return-object v0

    .line 2173038
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2173039
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2173047
    iget-object v0, p0, LX/ErU;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/ErU;->a:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 2173048
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2173025
    iget-object v0, p0, LX/ErU;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/ErU;->a:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    .line 2173026
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2173023
    iget-object v0, p0, LX/ErU;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/ErU;->a:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    .line 2173024
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2173021
    iget-object v0, p0, LX/ErU;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v1, p0, LX/ErU;->a:LX/0Yj;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 2173022
    return-void
.end method
