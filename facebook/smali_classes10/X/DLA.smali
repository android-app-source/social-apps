.class public final LX/DLA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DLJ;


# direct methods
.method public constructor <init>(LX/DLJ;)V
    .locals 0

    .prologue
    .line 1987960
    iput-object p1, p0, LX/DLA;->a:LX/DLJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1b1b7d1a

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1987961
    iget-object v1, p0, LX/DLA;->a:LX/DLJ;

    .line 1987962
    iget-object v3, v1, LX/DLJ;->n:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3Af;

    iput-object v3, v1, LX/DLJ;->D:LX/3Af;

    .line 1987963
    new-instance v3, LX/7TY;

    iget-object p0, v1, LX/DLJ;->D:LX/3Af;

    invoke-virtual {p0}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v3, p0}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 1987964
    iget-object p0, v1, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-virtual {p0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    .line 1987965
    :goto_0
    const p0, 0x7f0830b1

    invoke-virtual {v3, p0}, LX/34c;->e(I)LX/3Ai;

    move-result-object p0

    .line 1987966
    const p1, 0x7f020945

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1987967
    new-instance p1, LX/DLE;

    invoke-direct {p1, v1}, LX/DLE;-><init>(LX/DLJ;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1987968
    iget-object p0, v1, LX/DLJ;->D:LX/3Af;

    invoke-virtual {p0, v3}, LX/3Af;->a(LX/1OM;)V

    .line 1987969
    iget-object v3, v1, LX/DLJ;->D:LX/3Af;

    invoke-virtual {v3}, LX/3Af;->show()V

    .line 1987970
    const v1, 0x5801add0

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1987971
    :cond_0
    const p0, 0x7f0830ae

    invoke-virtual {v3, p0}, LX/34c;->e(I)LX/3Ai;

    move-result-object p0

    .line 1987972
    const p1, 0x7f020751

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1987973
    new-instance p1, LX/DLF;

    invoke-direct {p1, v1}, LX/DLF;-><init>(LX/DLJ;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method
