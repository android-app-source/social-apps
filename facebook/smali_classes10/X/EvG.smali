.class public final LX/EvG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2181092
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2181093
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2181094
    :goto_0
    return v1

    .line 2181095
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2181096
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2181097
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2181098
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2181099
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2181100
    const-string v6, "current_city"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2181101
    invoke-static {p0, p1}, LX/Ev8;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2181102
    :cond_2
    const-string v6, "education_experiences"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2181103
    invoke-static {p0, p1}, LX/EvB;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2181104
    :cond_3
    const-string v6, "hometown"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2181105
    invoke-static {p0, p1}, LX/EvC;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2181106
    :cond_4
    const-string v6, "work_experiences"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2181107
    invoke-static {p0, p1}, LX/EvF;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2181108
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2181109
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2181110
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2181111
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2181112
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2181113
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2181114
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2181115
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2181116
    if-eqz v0, :cond_0

    .line 2181117
    const-string v1, "current_city"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2181118
    invoke-static {p0, v0, p2}, LX/Ev8;->a(LX/15i;ILX/0nX;)V

    .line 2181119
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2181120
    if-eqz v0, :cond_1

    .line 2181121
    const-string v1, "education_experiences"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2181122
    invoke-static {p0, v0, p2, p3}, LX/EvB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2181123
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2181124
    if-eqz v0, :cond_2

    .line 2181125
    const-string v1, "hometown"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2181126
    invoke-static {p0, v0, p2}, LX/EvC;->a(LX/15i;ILX/0nX;)V

    .line 2181127
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2181128
    if-eqz v0, :cond_3

    .line 2181129
    const-string v1, "work_experiences"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2181130
    invoke-static {p0, v0, p2, p3}, LX/EvF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2181131
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2181132
    return-void
.end method
