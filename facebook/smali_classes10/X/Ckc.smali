.class public final enum LX/Ckc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ckc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ckc;

.field public static final enum CENTER:LX/Ckc;

.field public static final enum CENTER_IN:LX/Ckc;

.field public static final enum LEFT:LX/Ckc;

.field public static final enum LEFT_FLUSH:LX/Ckc;

.field public static final enum LEFT_OF:LX/Ckc;

.field public static final enum RIGHT:LX/Ckc;

.field public static final enum RIGHT_OF:LX/Ckc;

.field private static final sTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Ckc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1931166
    new-instance v0, LX/Ckc;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, LX/Ckc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckc;->LEFT:LX/Ckc;

    .line 1931167
    new-instance v0, LX/Ckc;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, LX/Ckc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckc;->RIGHT:LX/Ckc;

    .line 1931168
    new-instance v0, LX/Ckc;

    const-string v1, "LEFT_FLUSH"

    invoke-direct {v0, v1, v5}, LX/Ckc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckc;->LEFT_FLUSH:LX/Ckc;

    .line 1931169
    new-instance v0, LX/Ckc;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v6}, LX/Ckc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckc;->CENTER:LX/Ckc;

    .line 1931170
    new-instance v0, LX/Ckc;

    const-string v1, "LEFT_OF"

    invoke-direct {v0, v1, v7}, LX/Ckc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckc;->LEFT_OF:LX/Ckc;

    .line 1931171
    new-instance v0, LX/Ckc;

    const-string v1, "RIGHT_OF"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Ckc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckc;->RIGHT_OF:LX/Ckc;

    .line 1931172
    new-instance v0, LX/Ckc;

    const-string v1, "CENTER_IN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Ckc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckc;->CENTER_IN:LX/Ckc;

    .line 1931173
    const/4 v0, 0x7

    new-array v0, v0, [LX/Ckc;

    sget-object v1, LX/Ckc;->LEFT:LX/Ckc;

    aput-object v1, v0, v3

    sget-object v1, LX/Ckc;->RIGHT:LX/Ckc;

    aput-object v1, v0, v4

    sget-object v1, LX/Ckc;->LEFT_FLUSH:LX/Ckc;

    aput-object v1, v0, v5

    sget-object v1, LX/Ckc;->CENTER:LX/Ckc;

    aput-object v1, v0, v6

    sget-object v1, LX/Ckc;->LEFT_OF:LX/Ckc;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Ckc;->RIGHT_OF:LX/Ckc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Ckc;->CENTER_IN:LX/Ckc;

    aput-object v2, v0, v1

    sput-object v0, LX/Ckc;->$VALUES:[LX/Ckc;

    .line 1931174
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1931175
    sput-object v0, LX/Ckc;->sTypeMap:Ljava/util/Map;

    const-string v1, "left"

    sget-object v2, LX/Ckc;->LEFT:LX/Ckc;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931176
    sget-object v0, LX/Ckc;->sTypeMap:Ljava/util/Map;

    const-string v1, "right"

    sget-object v2, LX/Ckc;->RIGHT:LX/Ckc;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931177
    sget-object v0, LX/Ckc;->sTypeMap:Ljava/util/Map;

    const-string v1, "left_flush"

    sget-object v2, LX/Ckc;->LEFT_FLUSH:LX/Ckc;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931178
    sget-object v0, LX/Ckc;->sTypeMap:Ljava/util/Map;

    const-string v1, "left_of"

    sget-object v2, LX/Ckc;->LEFT_OF:LX/Ckc;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931179
    sget-object v0, LX/Ckc;->sTypeMap:Ljava/util/Map;

    const-string v1, "right_of"

    sget-object v2, LX/Ckc;->RIGHT_OF:LX/Ckc;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931180
    sget-object v0, LX/Ckc;->sTypeMap:Ljava/util/Map;

    const-string v1, "center"

    sget-object v2, LX/Ckc;->CENTER:LX/Ckc;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931181
    sget-object v0, LX/Ckc;->sTypeMap:Ljava/util/Map;

    const-string v1, "centered_in"

    sget-object v2, LX/Ckc;->CENTER_IN:LX/Ckc;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931182
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1931165
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/Ckc;
    .locals 1

    .prologue
    .line 1931160
    if-nez p0, :cond_1

    .line 1931161
    sget-object v0, LX/Ckc;->LEFT:LX/Ckc;

    .line 1931162
    :cond_0
    :goto_0
    return-object v0

    .line 1931163
    :cond_1
    sget-object v0, LX/Ckc;->sTypeMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckc;

    .line 1931164
    if-nez v0, :cond_0

    sget-object v0, LX/Ckc;->LEFT:LX/Ckc;

    goto :goto_0
.end method

.method public static hasElementArgument(LX/Ckc;)Z
    .locals 1

    .prologue
    .line 1931183
    sget-object v0, LX/Ckc;->LEFT_OF:LX/Ckc;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/Ckc;->RIGHT_OF:LX/Ckc;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/Ckc;->CENTER_IN:LX/Ckc;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasGridArgument(LX/Ckc;)Z
    .locals 1

    .prologue
    .line 1931159
    const/4 v0, 0x0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ckc;
    .locals 1

    .prologue
    .line 1931158
    const-class v0, LX/Ckc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ckc;

    return-object v0
.end method

.method public static values()[LX/Ckc;
    .locals 1

    .prologue
    .line 1931157
    sget-object v0, LX/Ckc;->$VALUES:[LX/Ckc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ckc;

    return-object v0
.end method
