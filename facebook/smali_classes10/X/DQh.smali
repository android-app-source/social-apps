.class public final LX/DQh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1995107
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1995108
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1995109
    :goto_0
    return v1

    .line 1995110
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 1995111
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1995112
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1995113
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1995114
    const-string v7, "count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1995115
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1995116
    :cond_1
    const-string v7, "edges"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1995117
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1995118
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_2

    .line 1995119
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_2

    .line 1995120
    invoke-static {p0, p1}, LX/DQf;->b(LX/15w;LX/186;)I

    move-result v6

    .line 1995121
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1995122
    :cond_2
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1995123
    goto :goto_1

    .line 1995124
    :cond_3
    const-string v7, "page_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1995125
    invoke-static {p0, p1}, LX/DQg;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1995126
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1995127
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1995128
    if-eqz v0, :cond_6

    .line 1995129
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 1995130
    :cond_6
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1995131
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1995132
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1995133
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1995134
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1995135
    if-eqz v0, :cond_0

    .line 1995136
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995137
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1995138
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1995139
    if-eqz v0, :cond_2

    .line 1995140
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995141
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1995142
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1995143
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/DQf;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995144
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1995145
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1995146
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1995147
    if-eqz v0, :cond_7

    .line 1995148
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995149
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1995150
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1995151
    if-eqz v1, :cond_3

    .line 1995152
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995153
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1995154
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1995155
    if-eqz v1, :cond_4

    .line 1995156
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995157
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1995158
    :cond_4
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1995159
    if-eqz v1, :cond_5

    .line 1995160
    const-string v2, "has_previous_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995161
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1995162
    :cond_5
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1995163
    if-eqz v1, :cond_6

    .line 1995164
    const-string v2, "start_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995165
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1995166
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1995167
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1995168
    return-void
.end method
