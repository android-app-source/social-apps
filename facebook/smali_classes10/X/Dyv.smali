.class public LX/Dyv;
.super LX/Dyu;
.source ""


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field public b:LX/9jN;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066060
    invoke-direct {p0}, LX/Dyu;-><init>()V

    .line 2066061
    iput-object p1, p0, LX/Dyv;->a:Landroid/view/LayoutInflater;

    .line 2066062
    return-void
.end method


# virtual methods
.method public final a()LX/9jL;
    .locals 1

    .prologue
    .line 2066063
    sget-object v0, LX/9jL;->AddHome:LX/9jL;

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2066064
    if-nez p1, :cond_0

    .line 2066065
    iget-object v0, p0, LX/Dyv;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030098

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 2066066
    :cond_0
    return-object p1
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2066067
    iget-object v0, p0, LX/Dyv;->b:LX/9jN;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dyv;->b:LX/9jN;

    .line 2066068
    iget-object v1, v0, LX/9jN;->f:Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    move-object v0, v1

    .line 2066069
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;->HOME_CREATION:Lcom/facebook/graphql/enums/GraphQLCheckinPromptType;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2066070
    if-eqz v0, :cond_0

    .line 2066071
    new-instance v0, Landroid/util/Pair;

    sget-object v1, LX/9jL;->AddHome:LX/9jL;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2066072
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 2066073
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2066074
    const/4 v0, 0x1

    return v0
.end method
