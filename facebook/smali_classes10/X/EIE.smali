.class public LX/EIE;
.super Landroid/view/TextureView;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xf
.end annotation


# instance fields
.field private a:LX/EID;

.field private b:F

.field private c:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2101284
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 2101285
    sget-object v0, LX/EID;->CENTER_CROP_THRESHOLD:LX/EID;

    invoke-virtual {p0, v0}, LX/EIE;->setScaleType(LX/EID;)V

    .line 2101286
    return-void
.end method

.method private a()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    .line 2101224
    invoke-direct {p0}, LX/EIE;->getTextureWidth()F

    move-result v2

    .line 2101225
    invoke-direct {p0}, LX/EIE;->getTextureHeight()F

    move-result v4

    .line 2101226
    cmpl-float v0, v2, v1

    if-eqz v0, :cond_0

    cmpl-float v0, v4, v1

    if-nez v0, :cond_1

    .line 2101227
    :cond_0
    :goto_0
    return-void

    .line 2101228
    :cond_1
    iget-object v0, p0, LX/EIE;->a:LX/EID;

    .line 2101229
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 2101230
    invoke-virtual {p0}, LX/EIE;->getWidth()I

    move-result v1

    int-to-float v3, v1

    .line 2101231
    invoke-virtual {p0}, LX/EIE;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 2101232
    div-float v2, v4, v2

    .line 2101233
    div-float v4, v1, v3

    .line 2101234
    iget-object v6, p0, LX/EIE;->a:LX/EID;

    sget-object v7, LX/EID;->CENTER_CROP_THRESHOLD:LX/EID;

    if-ne v6, v7, :cond_2

    .line 2101235
    sub-float v0, v2, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v4

    float-to-double v6, v0

    const-wide v8, 0x3fbeb851eb851eb8L    # 0.12

    cmpg-double v0, v6, v8

    if-gtz v0, :cond_4

    sget-object v0, LX/EID;->CENTER_CROP:LX/EID;

    .line 2101236
    :cond_2
    :goto_1
    sget-object v6, LX/EID;->FIT:LX/EID;

    if-ne v0, v6, :cond_6

    .line 2101237
    cmpl-float v0, v4, v2

    if-lez v0, :cond_5

    .line 2101238
    mul-float v0, v3, v2

    move v2, v3

    .line 2101239
    :goto_2
    sub-float v4, v3, v2

    div-float/2addr v4, v10

    .line 2101240
    sub-float v6, v1, v0

    div-float/2addr v6, v10

    .line 2101241
    div-float/2addr v2, v3

    div-float/2addr v0, v1

    invoke-virtual {v5, v2, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 2101242
    invoke-virtual {v5, v4, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 2101243
    :cond_3
    :goto_3
    invoke-virtual {p0, v5}, LX/EIE;->setTransform(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 2101244
    :cond_4
    sget-object v0, LX/EID;->FIT:LX/EID;

    goto :goto_1

    .line 2101245
    :cond_5
    div-float v0, v1, v2

    move v2, v0

    move v0, v1

    .line 2101246
    goto :goto_2

    .line 2101247
    :cond_6
    sget-object v6, LX/EID;->CENTER_CROP:LX/EID;

    if-ne v0, v6, :cond_3

    .line 2101248
    cmpl-float v0, v4, v2

    if-lez v0, :cond_7

    .line 2101249
    div-float v0, v1, v2

    move v2, v0

    move v0, v1

    .line 2101250
    :goto_4
    div-float v4, v3, v10

    float-to-int v4, v4

    .line 2101251
    div-float v6, v1, v10

    float-to-int v6, v6

    .line 2101252
    div-float/2addr v2, v3

    div-float/2addr v0, v1

    int-to-float v1, v4

    int-to-float v3, v6

    invoke-virtual {v5, v2, v0, v1, v3}, Landroid/graphics/Matrix;->setScale(FFFF)V

    goto :goto_3

    .line 2101253
    :cond_7
    mul-float v0, v3, v2

    move v2, v3

    goto :goto_4
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 2101254
    invoke-virtual {p0}, LX/EIE;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTextureHeight()F
    .locals 1

    .prologue
    .line 2101255
    invoke-direct {p0}, LX/EIE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/EIE;->c:F

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/EIE;->b:F

    goto :goto_0
.end method

.method private getTextureWidth()F
    .locals 1

    .prologue
    .line 2101256
    invoke-direct {p0}, LX/EIE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LX/EIE;->b:F

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/EIE;->c:F

    goto :goto_0
.end method


# virtual methods
.method public final a(FF)V
    .locals 0

    .prologue
    .line 2101257
    iput p1, p0, LX/EIE;->b:F

    .line 2101258
    iput p2, p0, LX/EIE;->c:F

    .line 2101259
    invoke-virtual {p0}, LX/EIE;->requestLayout()V

    .line 2101260
    invoke-direct {p0}, LX/EIE;->a()V

    .line 2101261
    return-void
.end method

.method public final onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v7, -0x80000000

    const/4 v6, 0x0

    .line 2101262
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 2101263
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 2101264
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 2101265
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2101266
    iget-object v2, p0, LX/EIE;->a:LX/EID;

    sget-object v5, LX/EID;->DYNAMIC_WIDTH:LX/EID;

    if-ne v2, v5, :cond_3

    invoke-direct {p0}, LX/EIE;->getTextureHeight()F

    move-result v2

    cmpl-float v2, v2, v6

    if-lez v2, :cond_3

    invoke-direct {p0}, LX/EIE;->getTextureWidth()F

    move-result v2

    cmpl-float v2, v2, v6

    if-lez v2, :cond_3

    .line 2101267
    int-to-float v2, v0

    invoke-direct {p0}, LX/EIE;->getTextureWidth()F

    move-result v5

    invoke-direct {p0}, LX/EIE;->getTextureHeight()F

    move-result v6

    div-float/2addr v5, v6

    mul-float/2addr v2, v5

    float-to-int v2, v2

    .line 2101268
    :goto_0
    if-ne v3, v8, :cond_1

    .line 2101269
    :goto_1
    if-eq v4, v8, :cond_0

    .line 2101270
    if-ne v4, v7, :cond_0

    .line 2101271
    invoke-static {v0, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2101272
    :cond_0
    invoke-virtual {p0, v1, v0}, LX/EIE;->setMeasuredDimension(II)V

    .line 2101273
    return-void

    .line 2101274
    :cond_1
    if-ne v3, v7, :cond_2

    .line 2101275
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 2101276
    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x64128b52

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2101277
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/TextureView;->onSizeChanged(IIII)V

    .line 2101278
    invoke-direct {p0}, LX/EIE;->a()V

    .line 2101279
    const/16 v1, 0x2d

    const v2, -0x233351c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setScaleType(LX/EID;)V
    .locals 1

    .prologue
    .line 2101280
    iget-object v0, p0, LX/EIE;->a:LX/EID;

    if-ne p1, v0, :cond_0

    .line 2101281
    :goto_0
    return-void

    .line 2101282
    :cond_0
    iput-object p1, p0, LX/EIE;->a:LX/EID;

    .line 2101283
    invoke-virtual {p0}, LX/EIE;->requestLayout()V

    goto :goto_0
.end method
