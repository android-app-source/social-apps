.class public LX/ENA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLStory;

.field private final b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field private final c:Landroid/content/Context;

.field private final d:LX/0hy;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/CvY;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/search/results/model/SearchResultsMutableContext;Landroid/content/Context;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/CvY;)V
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2112181
    iput-object p1, p0, LX/ENA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2112182
    iput-object p2, p0, LX/ENA;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2112183
    iput-object p3, p0, LX/ENA;->c:Landroid/content/Context;

    .line 2112184
    iput-object p4, p0, LX/ENA;->d:LX/0hy;

    .line 2112185
    iput-object p5, p0, LX/ENA;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2112186
    iput-object p6, p0, LX/ENA;->f:LX/CvY;

    .line 2112187
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x49c82ba6

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2112188
    iget-object v1, p0, LX/ENA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ENA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ENA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2112189
    :cond_0
    const v1, -0x3ff159e

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2112190
    :goto_0
    return-void

    .line 2112191
    :cond_1
    new-instance v1, LX/89k;

    invoke-direct {v1}, LX/89k;-><init>()V

    iget-object v2, p0, LX/ENA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v2}, LX/89k;->e(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v1

    iget-object v2, p0, LX/ENA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v2}, LX/89k;->f(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v1

    iget-object v2, p0, LX/ENA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    .line 2112192
    iput-object v2, v1, LX/89k;->b:Ljava/lang/String;

    .line 2112193
    move-object v1, v1

    .line 2112194
    iget-object v2, p0, LX/ENA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    .line 2112195
    iput-object v2, v1, LX/89k;->c:Ljava/lang/String;

    .line 2112196
    move-object v1, v1

    .line 2112197
    invoke-virtual {v1}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v1

    .line 2112198
    iget-object v2, p0, LX/ENA;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v2, v3, :cond_2

    .line 2112199
    iget-object v2, p0, LX/ENA;->f:LX/CvY;

    iget-object v3, p0, LX/ENA;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iget-object v5, p0, LX/ENA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    .line 2112200
    sget-object v6, LX/CvJ;->ITEM_IN_MODULE_TAPPED:LX/CvJ;

    invoke-static {v6, v3}, LX/CvY;->a(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p1, "tapped_result_entity_id"

    invoke-virtual {v6, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p1, "results_module_role"

    invoke-virtual {v6, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2112201
    invoke-static {v2, v3, v6}, LX/CvY;->a(LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2112202
    :cond_2
    iget-object v2, p0, LX/ENA;->d:LX/0hy;

    invoke-interface {v2, v1}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v1

    .line 2112203
    if-eqz v1, :cond_3

    .line 2112204
    iget-object v2, p0, LX/ENA;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/ENA;->c:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2112205
    :cond_3
    const v1, -0xbc0e08

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
