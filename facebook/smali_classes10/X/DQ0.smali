.class public final LX/DQ0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/9hN;

.field public final synthetic f:Lcom/facebook/groups/info/GroupInfoAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoAdapter;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;LX/9hN;)V
    .locals 0

    .prologue
    .line 1993998
    iput-object p1, p0, LX/DQ0;->f:Lcom/facebook/groups/info/GroupInfoAdapter;

    iput-object p2, p0, LX/DQ0;->a:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    iput-object p3, p0, LX/DQ0;->b:Ljava/util/List;

    iput-object p4, p0, LX/DQ0;->c:Ljava/lang/String;

    iput-object p5, p0, LX/DQ0;->d:Ljava/lang/String;

    iput-object p6, p0, LX/DQ0;->e:LX/9hN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x23b0b6ac

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1993999
    new-instance v1, LX/9hE;

    iget-object v2, p0, LX/DQ0;->a:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    invoke-direct {v1, v2}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    iget-object v2, p0, LX/DQ0;->b:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9hE;->b(LX/0Px;)LX/9hE;

    move-result-object v1

    sget-object v2, LX/74S;->GROUPS_INFO_PAGE_PHOTO_ITEM:LX/74S;

    invoke-virtual {v1, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v1

    const/4 v2, 0x0

    .line 1994000
    iput-boolean v2, v1, LX/9hD;->m:Z

    .line 1994001
    move-object v1, v1

    .line 1994002
    iget-object v2, p0, LX/DQ0;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v1

    iget-object v2, p0, LX/DQ0;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v1

    iget-object v2, p0, LX/DQ0;->f:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v2, v2, Lcom/facebook/groups/info/GroupInfoAdapter;->l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 1994003
    iput-object v2, v1, LX/9hD;->y:Ljava/lang/String;

    .line 1994004
    move-object v1, v1

    .line 1994005
    const v2, 0x41e065f

    .line 1994006
    iput v2, v1, LX/9hD;->x:I

    .line 1994007
    move-object v1, v1

    .line 1994008
    invoke-virtual {v1}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 1994009
    iget-object v2, p0, LX/DQ0;->f:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v2, v2, Lcom/facebook/groups/info/GroupInfoAdapter;->f:LX/23R;

    iget-object v3, p0, LX/DQ0;->f:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v3, v3, Lcom/facebook/groups/info/GroupInfoAdapter;->b:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v3}, Lcom/facebook/widget/listview/BetterListView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/DQ0;->e:LX/9hN;

    invoke-interface {v2, v3, v1, v4}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 1994010
    const v1, 0x54d91af9

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
