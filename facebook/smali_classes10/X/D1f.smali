.class public final enum LX/D1f;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D1f;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D1f;

.field public static final enum FETCH_LOCATIONS_TASK:LX/D1f;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1957302
    new-instance v0, LX/D1f;

    const-string v1, "FETCH_LOCATIONS_TASK"

    invoke-direct {v0, v1, v2}, LX/D1f;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D1f;->FETCH_LOCATIONS_TASK:LX/D1f;

    .line 1957303
    const/4 v0, 0x1

    new-array v0, v0, [LX/D1f;

    sget-object v1, LX/D1f;->FETCH_LOCATIONS_TASK:LX/D1f;

    aput-object v1, v0, v2

    sput-object v0, LX/D1f;->$VALUES:[LX/D1f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1957304
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D1f;
    .locals 1

    .prologue
    .line 1957305
    const-class v0, LX/D1f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D1f;

    return-object v0
.end method

.method public static values()[LX/D1f;
    .locals 1

    .prologue
    .line 1957306
    sget-object v0, LX/D1f;->$VALUES:[LX/D1f;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D1f;

    return-object v0
.end method
