.class public LX/DiN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:F

.field public e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/DiO;

.field public final h:LX/DiR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/DiO;LX/DiR;)V
    .locals 0

    .prologue
    .line 2032023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2032024
    iput-object p1, p0, LX/DiN;->a:Ljava/lang/String;

    .line 2032025
    iput-object p2, p0, LX/DiN;->b:Ljava/lang/String;

    .line 2032026
    iput-object p3, p0, LX/DiN;->c:Ljava/lang/String;

    .line 2032027
    iput p4, p0, LX/DiN;->d:F

    .line 2032028
    iput-object p5, p0, LX/DiN;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2032029
    iput-object p6, p0, LX/DiN;->f:Ljava/lang/String;

    .line 2032030
    iput-object p7, p0, LX/DiN;->g:LX/DiO;

    .line 2032031
    iput-object p8, p0, LX/DiN;->h:LX/DiR;

    .line 2032032
    return-void
.end method

.method public static a(LX/0lF;J)LX/DiN;
    .locals 17
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2032033
    const-string v2, "id"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 2032034
    const-string v2, "type"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 2032035
    const-string v4, "label"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 2032036
    const-string v5, "icon"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 2032037
    const-string v6, "confidence"

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->f(LX/0lF;)F

    move-result v6

    .line 2032038
    const-string v7, "message_id"

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v8

    .line 2032039
    const-string v7, "data"

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    .line 2032040
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2032041
    :cond_0
    const/4 v2, 0x0

    .line 2032042
    :goto_0
    return-object v2

    .line 2032043
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, LX/DiO;->fromId(I)LX/DiO;

    move-result-object v9

    .line 2032044
    if-nez v9, :cond_2

    .line 2032045
    const/4 v2, 0x0

    goto :goto_0

    .line 2032046
    :cond_2
    const-string v2, "thread_key"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 2032047
    if-nez v2, :cond_3

    .line 2032048
    const/4 v2, 0x0

    goto :goto_0

    .line 2032049
    :cond_3
    const/4 v7, 0x0

    .line 2032050
    const-string v11, "other_user_fbid"

    invoke-virtual {v2, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v11

    invoke-static {v11}, LX/16N;->c(LX/0lF;)J

    move-result-wide v12

    .line 2032051
    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-eqz v11, :cond_5

    .line 2032052
    move-wide/from16 v0, p1

    invoke-static {v12, v13, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v7

    .line 2032053
    :cond_4
    :goto_1
    if-nez v7, :cond_6

    .line 2032054
    const/4 v2, 0x0

    goto :goto_0

    .line 2032055
    :cond_5
    const-string v11, "thread_fbid"

    invoke-virtual {v2, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->c(LX/0lF;)J

    move-result-wide v12

    .line 2032056
    const-wide/16 v14, 0x0

    cmp-long v2, v12, v14

    if-eqz v2, :cond_4

    .line 2032057
    invoke-static {v12, v13}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v7

    goto :goto_1

    .line 2032058
    :cond_6
    invoke-static {v9, v10}, LX/DiN;->a(LX/DiO;LX/0lF;)LX/DiR;

    move-result-object v10

    .line 2032059
    new-instance v2, LX/DiN;

    invoke-direct/range {v2 .. v10}, LX/DiN;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/DiO;LX/DiR;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;ILX/0lF;)LX/DiN;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2032060
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2032061
    :cond_0
    const/4 v1, 0x0

    .line 2032062
    :goto_0
    return-object v1

    .line 2032063
    :cond_1
    invoke-static/range {p6 .. p6}, LX/DiO;->fromId(I)LX/DiO;

    move-result-object v8

    .line 2032064
    if-nez v8, :cond_2

    .line 2032065
    const/4 v1, 0x0

    goto :goto_0

    .line 2032066
    :cond_2
    move-object/from16 v0, p7

    invoke-static {v8, v0}, LX/DiN;->a(LX/DiO;LX/0lF;)LX/DiR;

    move-result-object v9

    .line 2032067
    new-instance v1, LX/DiN;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v9}, LX/DiN;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/DiO;LX/DiR;)V

    goto :goto_0
.end method

.method private static a(LX/DiO;LX/0lF;)LX/DiR;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2032068
    sget-object v1, LX/DiM;->a:[I

    invoke-virtual {p0}, LX/DiO;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2032069
    :goto_0
    :pswitch_0
    return-object v0

    .line 2032070
    :pswitch_1
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-string v4, "timestamp"

    invoke-virtual {p1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->c(LX/0lF;)J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    .line 2032071
    new-instance v5, LX/DiS;

    invoke-direct {v5, v3, v4}, LX/DiS;-><init>(J)V

    move-object v0, v5

    .line 2032072
    goto :goto_0

    .line 2032073
    :pswitch_2
    const-string v3, "amount"

    invoke-virtual {p1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->c(LX/0lF;)J

    move-result-wide v3

    .line 2032074
    const-string v5, "currency"

    invoke-virtual {p1, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 2032075
    const-string v6, "type"

    invoke-virtual {p1, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 2032076
    new-instance v7, LX/DiT;

    invoke-direct {v7, v3, v4, v5, v6}, LX/DiT;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    move-object v0, v7

    .line 2032077
    goto :goto_0

    .line 2032078
    :pswitch_3
    const-string v0, "topic"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 2032079
    const-string v1, "options"

    invoke-static {p1, v1}, LX/16N;->b(LX/0lF;Ljava/lang/String;)LX/0Px;

    move-result-object v1

    .line 2032080
    new-instance v2, LX/DiU;

    invoke-direct {v2, v0, v1}, LX/DiU;-><init>(Ljava/lang/String;LX/0Px;)V

    move-object v0, v2

    .line 2032081
    goto :goto_0

    .line 2032082
    :pswitch_4
    const-string v0, "sticker_id"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 2032083
    new-instance v1, LX/DiW;

    invoke-direct {v1, v0}, LX/DiW;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 2032084
    goto :goto_0

    .line 2032085
    :pswitch_5
    const-string v0, "provider"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 2032086
    const-string v1, "destination"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 2032087
    new-instance v2, LX/DiX;

    invoke-direct {v2, v0, v1}, LX/DiX;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 2032088
    goto :goto_0

    .line 2032089
    :pswitch_6
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-string v4, "timestamp"

    invoke-virtual {p1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->c(LX/0lF;)J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    .line 2032090
    new-instance v5, LX/DiV;

    invoke-direct {v5, v3, v4}, LX/DiV;-><init>(J)V

    move-object v0, v5

    .line 2032091
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
