.class public final LX/Cpl;
.super LX/Chm;
.source ""


# instance fields
.field public final synthetic a:LX/CpT;


# direct methods
.method public constructor <init>(LX/CpT;)V
    .locals 0

    .prologue
    .line 1938664
    iput-object p1, p0, LX/Cpl;->a:LX/CpT;

    invoke-direct {p0}, LX/Chm;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 11

    .prologue
    .line 1938665
    check-cast p1, LX/CiX;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1938666
    iget-object v0, p1, LX/CiX;->b:LX/Cqw;

    move-object v6, v0

    .line 1938667
    iget-object v0, v6, LX/Cqw;->e:LX/Cqu;

    move-object v0, v0

    .line 1938668
    sget-object v3, LX/Cqu;->EXPANDED:LX/Cqu;

    if-ne v0, v3, :cond_5

    move v0, v1

    .line 1938669
    :goto_0
    iget-object v3, v6, LX/Cqw;->f:LX/Cqt;

    move-object v3, v3

    .line 1938670
    sget-object v4, LX/Cqt;->PORTRAIT:LX/Cqt;

    if-ne v3, v4, :cond_6

    move v3, v1

    .line 1938671
    :goto_1
    iget-object v4, p1, LX/CiX;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 1938672
    iget-object v5, p0, LX/Cpl;->a:LX/CpT;

    .line 1938673
    iget-object v7, v5, LX/Cos;->a:LX/Ctg;

    move-object v5, v7

    .line 1938674
    if-ne v4, v5, :cond_7

    move v4, v1

    .line 1938675
    :goto_2
    iget-object v5, p0, LX/Cpl;->a:LX/CpT;

    invoke-virtual {v5}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v7

    .line 1938676
    if-eqz v4, :cond_e

    .line 1938677
    iget-object v4, p0, LX/Cpl;->a:LX/CpT;

    iget-object v5, p0, LX/Cpl;->a:LX/CpT;

    iget-object v5, v5, LX/CpT;->p:LX/0Uh;

    invoke-virtual {v4, v5}, LX/CpT;->a(LX/0Uh;)LX/Cso;

    move-result-object v8

    .line 1938678
    if-nez v0, :cond_8

    move v5, v1

    :goto_3
    if-nez v3, :cond_9

    move v4, v1

    :goto_4
    invoke-virtual {v8, v5, v4}, LX/Cso;->a(ZZ)LX/Cuo;

    move-result-object v4

    .line 1938679
    invoke-virtual {v7, v4}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->a(LX/Cuo;)V

    .line 1938680
    iget-object v4, p0, LX/Cpl;->a:LX/CpT;

    iget-object v5, v4, LX/CpT;->r:LX/Cuw;

    if-nez v3, :cond_a

    move v4, v1

    .line 1938681
    :goto_5
    iput-boolean v4, v5, LX/Cuw;->j:Z

    .line 1938682
    iget-object v4, p0, LX/Cpl;->a:LX/CpT;

    iget-boolean v4, v4, LX/CpT;->P:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, LX/Cpl;->a:LX/CpT;

    iget-boolean v4, v4, LX/CpT;->Q:Z

    if-eqz v4, :cond_1

    .line 1938683
    iget-object v4, p0, LX/Cpl;->a:LX/CpT;

    iget-object v4, v4, LX/CpT;->R:LX/CuJ;

    if-eqz v3, :cond_0

    if-eqz v0, :cond_b

    :cond_0
    :goto_6
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 1938684
    iput-boolean v1, v4, LX/CuJ;->l:Z

    .line 1938685
    iget-object v9, v4, LX/CuJ;->f:LX/CqV;

    iget-boolean v3, v4, LX/CuJ;->l:Z

    if-nez v3, :cond_f

    move v3, v5

    :goto_7
    sget-object v10, LX/CqU;->SPHERICAL_PHOTO:LX/CqU;

    invoke-virtual {v9, v3, v10}, LX/CqV;->a(ZLX/CqU;)V

    .line 1938686
    iget-object v3, v4, LX/CuJ;->f:LX/CqV;

    iget-boolean v9, v4, LX/CuJ;->l:Z

    if-nez v9, :cond_10

    :goto_8
    sget-object v8, LX/CqU;->SPHERICAL_PHOTO:LX/CqU;

    const/4 v9, 0x0

    invoke-virtual {v3, v5, v8, v9}, LX/CqV;->a(ZLX/CqU;Landroid/view/View;)V

    .line 1938687
    invoke-virtual {v4}, LX/Cts;->h()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->requestDisallowInterceptTouchEvent(Z)V

    .line 1938688
    iget-object v1, p0, LX/Cpl;->a:LX/CpT;

    iget-object v1, v1, LX/CpT;->T:LX/Cpq;

    if-eqz v1, :cond_1

    .line 1938689
    if-eqz v0, :cond_c

    iget-object v1, p0, LX/Cpl;->a:LX/CpT;

    iget-object v1, v1, LX/CpT;->T:LX/Cpq;

    .line 1938690
    :goto_9
    invoke-virtual {v1, v7}, LX/Cpq;->a(Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;)V

    .line 1938691
    :cond_1
    iget-object v1, p0, LX/Cpl;->a:LX/CpT;

    iget-boolean v1, v1, LX/CpT;->C:Z

    if-nez v1, :cond_2

    .line 1938692
    iget-object v1, p0, LX/Cpl;->a:LX/CpT;

    .line 1938693
    iget-object v3, v6, LX/Cqw;->f:LX/Cqt;

    move-object v3, v3

    .line 1938694
    invoke-virtual {v1}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v7

    .line 1938695
    if-nez v7, :cond_11

    .line 1938696
    :cond_2
    :goto_a
    iget-object v1, p0, LX/Cpl;->a:LX/CpT;

    iget-object v1, v1, LX/CpT;->T:LX/Cpq;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/Cpl;->a:LX/CpT;

    iget-boolean v1, v1, LX/CpT;->Q:Z

    if-eqz v1, :cond_3

    .line 1938697
    if-eqz v0, :cond_d

    .line 1938698
    iget-boolean v1, p1, LX/CiX;->d:Z

    move v1, v1

    .line 1938699
    if-eqz v1, :cond_d

    .line 1938700
    iget-object v0, p0, LX/Cpl;->a:LX/CpT;

    .line 1938701
    invoke-virtual {v0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v1

    .line 1938702
    if-nez v1, :cond_14

    .line 1938703
    :cond_3
    :goto_b
    iget-boolean v0, p1, LX/CiX;->d:Z

    move v0, v0

    .line 1938704
    if-eqz v0, :cond_4

    .line 1938705
    iget-object v0, p0, LX/Cpl;->a:LX/CpT;

    .line 1938706
    iget-object v1, v6, LX/Cqw;->f:LX/Cqt;

    move-object v1, v1

    .line 1938707
    iget-boolean v2, v0, LX/CpT;->C:Z

    if-eqz v2, :cond_15

    .line 1938708
    :cond_4
    :goto_c
    iget-object v0, p0, LX/Cpl;->a:LX/CpT;

    .line 1938709
    iget-boolean v1, p1, LX/CiX;->d:Z

    move v1, v1

    .line 1938710
    iput-boolean v1, v0, LX/CpT;->P:Z

    .line 1938711
    return-void

    :cond_5
    move v0, v2

    .line 1938712
    goto/16 :goto_0

    :cond_6
    move v3, v2

    .line 1938713
    goto/16 :goto_1

    :cond_7
    move v4, v2

    .line 1938714
    goto/16 :goto_2

    :cond_8
    move v5, v2

    .line 1938715
    goto/16 :goto_3

    :cond_9
    move v4, v2

    goto/16 :goto_4

    :cond_a
    move v4, v2

    .line 1938716
    goto/16 :goto_5

    :cond_b
    move v1, v2

    .line 1938717
    goto/16 :goto_6

    .line 1938718
    :cond_c
    iget-object v1, p0, LX/Cpl;->a:LX/CpT;

    iget-object v1, v1, LX/CpT;->S:LX/Cpq;

    goto :goto_9

    .line 1938719
    :cond_d
    if-nez v0, :cond_3

    iget-object v0, p0, LX/Cpl;->a:LX/CpT;

    iget-boolean v0, v0, LX/CpT;->P:Z

    if-eqz v0, :cond_3

    .line 1938720
    iget-object v0, p0, LX/Cpl;->a:LX/CpT;

    .line 1938721
    invoke-static {v0, v2}, LX/CpT;->b(LX/CpT;I)V

    .line 1938722
    goto :goto_b

    .line 1938723
    :cond_e
    if-eqz v0, :cond_4

    .line 1938724
    iget-object v0, p0, LX/Cpl;->a:LX/CpT;

    iget-object v0, v0, LX/CpT;->r:LX/Cuw;

    sget-object v1, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    invoke-virtual {v0, v1}, LX/Cuw;->a(LX/Cuj;)Z

    goto :goto_c

    :cond_f
    move v3, v8

    .line 1938725
    goto/16 :goto_7

    :cond_10
    move v5, v8

    .line 1938726
    goto/16 :goto_8

    .line 1938727
    :cond_11
    const-class v4, LX/CuJ;

    invoke-virtual {v1, v4}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v4

    check-cast v4, LX/CuJ;

    .line 1938728
    if-eqz v4, :cond_2

    .line 1938729
    iget-boolean v5, v1, LX/CpT;->C:Z

    if-nez v5, :cond_12

    const/4 v5, 0x1

    :goto_d
    invoke-virtual {v7}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->getY()F

    move-result v7

    float-to-int v7, v7

    .line 1938730
    iput-object v3, v4, LX/CuJ;->h:LX/Cqt;

    .line 1938731
    sget-object v1, LX/Cqt;->PORTRAIT:LX/Cqt;

    if-ne v3, v1, :cond_13

    const/4 v1, 0x1

    :goto_e
    iput-boolean v1, v4, LX/CuJ;->g:Z

    .line 1938732
    iput-boolean v5, v4, LX/CuJ;->i:Z

    .line 1938733
    iput v7, v4, LX/CuJ;->j:I

    .line 1938734
    goto/16 :goto_a

    :cond_12
    const/4 v5, 0x0

    goto :goto_d

    .line 1938735
    :cond_13
    const/4 v1, 0x0

    goto :goto_e

    .line 1938736
    :cond_14
    const-class v2, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    .line 1938737
    if-eqz v1, :cond_3

    .line 1938738
    iget-object v1, v0, LX/Cos;->a:LX/Ctg;

    move-object v2, v1

    .line 1938739
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v1

    .line 1938740
    invoke-interface {v2}, LX/Ctg;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v3

    invoke-interface {v2, v3}, LX/Ctg;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    .line 1938741
    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v1}, LX/Ctg;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1938742
    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 1938743
    invoke-static {v0, v1}, LX/CpT;->b(LX/CpT;I)V

    goto/16 :goto_b

    .line 1938744
    :cond_15
    invoke-virtual {v0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v2

    .line 1938745
    if-eqz v2, :cond_4

    .line 1938746
    const-class v3, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 1938747
    if-eqz v2, :cond_4

    instance-of v3, v2, LX/CuM;

    if-eqz v3, :cond_4

    .line 1938748
    check-cast v2, LX/CuM;

    .line 1938749
    iput-object v1, v2, LX/CuM;->s:LX/Cqt;

    .line 1938750
    iget-object v3, v2, LX/CuM;->s:LX/Cqt;

    invoke-virtual {v3}, LX/Cqt;->isLandscape()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 1938751
    iget-object v3, v2, LX/CuM;->s:LX/Cqt;

    sget-object v0, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    if-ne v3, v0, :cond_16

    sget-object v3, LX/7Cm;->RENDER_AXIS_ROTATE_90_LEFT:LX/7Cm;

    :goto_f
    invoke-static {v2, v3}, LX/CuM;->setTextureOrientation(LX/CuM;LX/7Cm;)V

    .line 1938752
    :goto_10
    goto/16 :goto_c

    .line 1938753
    :cond_16
    sget-object v3, LX/7Cm;->RENDER_AXIS_ROTATE_90_RIGHT:LX/7Cm;

    goto :goto_f

    .line 1938754
    :cond_17
    sget-object v3, LX/7Cm;->RENDER_AXIS_DEFAULT:LX/7Cm;

    invoke-static {v2, v3}, LX/CuM;->setTextureOrientation(LX/CuM;LX/7Cm;)V

    goto :goto_10
.end method
