.class public final LX/EtS;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EtT;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/CharSequence;

.field public c:I

.field public d:Z

.field public e:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/Apr;",
            ">;"
        }
    .end annotation
.end field


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2178051
    const-string v0, "FigListItemActionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2178052
    if-ne p0, p1, :cond_1

    .line 2178053
    :cond_0
    :goto_0
    return v0

    .line 2178054
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2178055
    goto :goto_0

    .line 2178056
    :cond_3
    check-cast p1, LX/EtS;

    .line 2178057
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2178058
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2178059
    if-eq v2, v3, :cond_0

    .line 2178060
    iget v2, p0, LX/EtS;->a:I

    iget v3, p1, LX/EtS;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2178061
    goto :goto_0

    .line 2178062
    :cond_4
    iget-object v2, p0, LX/EtS;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/EtS;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/EtS;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2178063
    goto :goto_0

    .line 2178064
    :cond_6
    iget-object v2, p1, LX/EtS;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_5

    .line 2178065
    :cond_7
    iget v2, p0, LX/EtS;->c:I

    iget v3, p1, LX/EtS;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2178066
    goto :goto_0

    .line 2178067
    :cond_8
    iget-boolean v2, p0, LX/EtS;->d:Z

    iget-boolean v3, p1, LX/EtS;->d:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 2178068
    goto :goto_0

    .line 2178069
    :cond_9
    iget-object v2, p0, LX/EtS;->e:LX/1dQ;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/EtS;->e:LX/1dQ;

    iget-object v3, p1, LX/EtS;->e:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2178070
    goto :goto_0

    .line 2178071
    :cond_b
    iget-object v2, p1, LX/EtS;->e:LX/1dQ;

    if-nez v2, :cond_a

    .line 2178072
    :cond_c
    iget-object v2, p0, LX/EtS;->f:LX/1dQ;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/EtS;->f:LX/1dQ;

    iget-object v3, p1, LX/EtS;->f:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2178073
    goto :goto_0

    .line 2178074
    :cond_d
    iget-object v2, p1, LX/EtS;->f:LX/1dQ;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
