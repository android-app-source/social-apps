.class public final LX/ERD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Vr;

.field public final synthetic b:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

.field public final synthetic c:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;LX/1Vr;Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;)V
    .locals 0

    .prologue
    .line 2120555
    iput-object p1, p0, LX/ERD;->c:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    iput-object p2, p0, LX/ERD;->a:LX/1Vr;

    iput-object p3, p0, LX/ERD;->b:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x47a04765

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2120556
    iget-object v1, p0, LX/ERD;->c:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    iget-object v1, v1, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->c:LX/EQh;

    .line 2120557
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "shoebox_moments_interstitial_help"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2120558
    iget-object v3, v1, LX/EQh;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2120559
    iget-object v1, p0, LX/ERD;->a:LX/1Vr;

    iget-object v2, p0, LX/ERD;->b:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    .line 2120560
    iget-object v3, v2, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->i:Ljava/lang/String;

    move-object v2, v3

    .line 2120561
    iget-object v3, p0, LX/ERD;->c:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    iget-object v3, v3, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, LX/1Vr;->b(Ljava/lang/String;Landroid/content/Context;)V

    .line 2120562
    const v1, 0x1312f479

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
