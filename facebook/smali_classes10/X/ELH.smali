.class public final LX/ELH;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ELI;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public c:Landroid/view/View$OnClickListener;

.field public d:Landroid/view/View$OnClickListener;

.field public final synthetic e:LX/ELI;


# direct methods
.method public constructor <init>(LX/ELI;)V
    .locals 1

    .prologue
    .line 2108386
    iput-object p1, p0, LX/ELH;->e:LX/ELI;

    .line 2108387
    move-object v0, p1

    .line 2108388
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2108389
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2108390
    const-string v0, "SearchResultsEntityDeepLinkComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2108391
    if-ne p0, p1, :cond_1

    .line 2108392
    :cond_0
    :goto_0
    return v0

    .line 2108393
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2108394
    goto :goto_0

    .line 2108395
    :cond_3
    check-cast p1, LX/ELH;

    .line 2108396
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2108397
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2108398
    if-eq v2, v3, :cond_0

    .line 2108399
    iget-object v2, p0, LX/ELH;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ELH;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELH;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2108400
    goto :goto_0

    .line 2108401
    :cond_5
    iget-object v2, p1, LX/ELH;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 2108402
    :cond_6
    iget-object v2, p0, LX/ELH;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ELH;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELH;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2108403
    goto :goto_0

    .line 2108404
    :cond_8
    iget-object v2, p1, LX/ELH;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 2108405
    :cond_9
    iget-object v2, p0, LX/ELH;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/ELH;->c:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/ELH;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2108406
    goto :goto_0

    .line 2108407
    :cond_b
    iget-object v2, p1, LX/ELH;->c:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_a

    .line 2108408
    :cond_c
    iget-object v2, p0, LX/ELH;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/ELH;->d:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/ELH;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2108409
    goto :goto_0

    .line 2108410
    :cond_d
    iget-object v2, p1, LX/ELH;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
