.class public final enum LX/DiC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DiC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DiC;

.field public static final enum IN_APP_ACTIVE_10S:LX/DiC;

.field public static final enum IN_APP_ACTIVE_30S:LX/DiC;

.field public static final enum IN_APP_IDLE:LX/DiC;

.field public static final enum NOT_IN_APP:LX/DiC;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2031857
    new-instance v0, LX/DiC;

    const-string v1, "IN_APP_ACTIVE_10S"

    invoke-direct {v0, v1, v2}, LX/DiC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DiC;->IN_APP_ACTIVE_10S:LX/DiC;

    .line 2031858
    new-instance v0, LX/DiC;

    const-string v1, "IN_APP_ACTIVE_30S"

    invoke-direct {v0, v1, v3}, LX/DiC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DiC;->IN_APP_ACTIVE_30S:LX/DiC;

    .line 2031859
    new-instance v0, LX/DiC;

    const-string v1, "IN_APP_IDLE"

    invoke-direct {v0, v1, v4}, LX/DiC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DiC;->IN_APP_IDLE:LX/DiC;

    .line 2031860
    new-instance v0, LX/DiC;

    const-string v1, "NOT_IN_APP"

    invoke-direct {v0, v1, v5}, LX/DiC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DiC;->NOT_IN_APP:LX/DiC;

    .line 2031861
    const/4 v0, 0x4

    new-array v0, v0, [LX/DiC;

    sget-object v1, LX/DiC;->IN_APP_ACTIVE_10S:LX/DiC;

    aput-object v1, v0, v2

    sget-object v1, LX/DiC;->IN_APP_ACTIVE_30S:LX/DiC;

    aput-object v1, v0, v3

    sget-object v1, LX/DiC;->IN_APP_IDLE:LX/DiC;

    aput-object v1, v0, v4

    sget-object v1, LX/DiC;->NOT_IN_APP:LX/DiC;

    aput-object v1, v0, v5

    sput-object v0, LX/DiC;->$VALUES:[LX/DiC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2031862
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DiC;
    .locals 1

    .prologue
    .line 2031863
    const-class v0, LX/DiC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DiC;

    return-object v0
.end method

.method public static values()[LX/DiC;
    .locals 1

    .prologue
    .line 2031864
    sget-object v0, LX/DiC;->$VALUES:[LX/DiC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DiC;

    return-object v0
.end method
