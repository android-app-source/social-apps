.class public final LX/DWT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    .line 2007429
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2007430
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2007431
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2007432
    const/4 v2, 0x0

    .line 2007433
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2007434
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2007435
    :goto_1
    move v1, v2

    .line 2007436
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2007437
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 2007438
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2007439
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2007440
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2007441
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2007442
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2007443
    const-string v4, "concise_text"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2007444
    const/4 v3, 0x0

    .line 2007445
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 2007446
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2007447
    :goto_3
    move v1, v3

    .line 2007448
    goto :goto_2

    .line 2007449
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2007450
    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 2007451
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    .line 2007452
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2007453
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2007454
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2007455
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2007456
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2007457
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2007458
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_4

    .line 2007459
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2007460
    invoke-virtual {p1, v3, v1}, LX/186;->b(II)V

    .line 2007461
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v1, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2007462
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2007463
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2007464
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    .line 2007465
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2007466
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2007467
    if-eqz v2, :cond_1

    .line 2007468
    const-string v3, "concise_text"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007469
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2007470
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2007471
    if-eqz v3, :cond_0

    .line 2007472
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007473
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2007474
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2007475
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2007476
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2007477
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2007478
    return-void
.end method
