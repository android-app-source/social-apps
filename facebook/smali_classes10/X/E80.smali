.class public LX/E80;
.super LX/Cfm;
.source ""


# instance fields
.field private final a:LX/E7d;


# direct methods
.method public constructor <init>(LX/E7d;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2082014
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_WITH_ATTRIBUTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    invoke-direct {p0, v0}, LX/Cfm;-><init>(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V

    .line 2082015
    iput-object p1, p0, LX/E80;->a:LX/E7d;

    .line 2082016
    return-void
.end method


# virtual methods
.method public final c()LX/Cfk;
    .locals 10

    .prologue
    .line 2082017
    iget-object v0, p0, LX/E80;->a:LX/E7d;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PHOTOS_WITH_ATTRIBUTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    .line 2082018
    new-instance v2, LX/E7c;

    const-class v3, LX/E7f;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/E7f;

    const/16 v4, 0x139b

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v5

    check-cast v5, LX/E1i;

    invoke-static {v0}, LX/3Tx;->a(LX/0QB;)LX/3Tx;

    move-result-object v6

    check-cast v6, LX/3Tx;

    invoke-static {v0}, LX/3Tz;->b(LX/0QB;)LX/3Tz;

    move-result-object v7

    check-cast v7, LX/3Tz;

    invoke-static {v0}, Lcom/facebook/reaction/ReactionUtil;->b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v8

    check-cast v8, Lcom/facebook/reaction/ReactionUtil;

    move-object v9, v1

    invoke-direct/range {v2 .. v9}, LX/E7c;-><init>(LX/E7f;LX/0Or;LX/E1i;LX/3Tx;LX/3Tz;Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V

    .line 2082019
    move-object v0, v2

    .line 2082020
    return-object v0
.end method
