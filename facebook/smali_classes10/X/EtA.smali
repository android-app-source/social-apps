.class public abstract LX/EtA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<SuccessResultType:",
        "Ljava/lang/Object;",
        "FailureResultType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EtB",
            "<*TSuccessResultType;>;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Et9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2177311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2177312
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EtA;->a:Ljava/util/List;

    .line 2177313
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EtA;->b:Ljava/util/List;

    .line 2177314
    return-void
.end method

.method public static l(LX/EtA;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/Et9;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2177306
    iget-object v1, p0, LX/EtA;->b:Ljava/util/List;

    monitor-enter v1

    .line 2177307
    :try_start_0
    iget-object v0, p0, LX/EtA;->b:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2177308
    monitor-exit v1

    .line 2177309
    return-object v0

    .line 2177310
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static m(LX/EtA;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/EtB",
            "<*TSuccessResultType;>;>;"
        }
    .end annotation

    .prologue
    .line 2177301
    iget-object v1, p0, LX/EtA;->a:Ljava/util/List;

    monitor-enter v1

    .line 2177302
    :try_start_0
    iget-object v0, p0, LX/EtA;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2177303
    monitor-exit v1

    .line 2177304
    return-object v0

    .line 2177305
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static n()Z
    .locals 2

    .prologue
    .line 2177300
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 2177273
    invoke-static {p0}, LX/EtA;->m(LX/EtA;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EtB;

    .line 2177274
    invoke-virtual {v0}, LX/EtB;->a()V

    .line 2177275
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2177276
    :cond_0
    invoke-static {p0}, LX/EtA;->l(LX/EtA;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Et9;

    .line 2177277
    invoke-virtual {v0}, LX/Et9;->a()V

    .line 2177278
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2177279
    :cond_1
    return-void
.end method

.method public abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TSuccessResultType;)V"
        }
    .end annotation
.end method

.method public b()V
    .locals 0

    .prologue
    .line 2177297
    invoke-virtual {p0}, LX/EtA;->h()V

    .line 2177298
    invoke-virtual {p0}, LX/EtA;->g()V

    .line 2177299
    return-void
.end method

.method public abstract b(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFailureResultType;)V"
        }
    .end annotation
.end method

.method public c()V
    .locals 5

    .prologue
    .line 2177292
    invoke-static {p0}, LX/EtA;->m(LX/EtA;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EtB;

    .line 2177293
    invoke-virtual {v0}, LX/EtB;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2177294
    invoke-virtual {v0}, LX/EtB;->c()V

    .line 2177295
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2177296
    :cond_1
    return-void
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TFailureResultType;)V"
        }
    .end annotation

    .prologue
    .line 2177288
    invoke-static {}, LX/EtA;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2177289
    invoke-virtual {p0, p1}, LX/EtA;->b(Ljava/lang/Object;)V

    .line 2177290
    :goto_0
    return-void

    .line 2177291
    :cond_0
    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v0

    new-instance v1, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkController$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkController$4;-><init>(LX/EtA;Ljava/lang/Object;)V

    const v2, -0x520ad2a5

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 2177284
    invoke-static {p0}, LX/EtA;->l(LX/EtA;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Et9;

    .line 2177285
    invoke-virtual {v0}, LX/Et9;->b()V

    .line 2177286
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2177287
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 2177280
    invoke-static {p0}, LX/EtA;->m(LX/EtA;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EtB;

    .line 2177281
    invoke-virtual {v0}, LX/EtB;->b()V

    .line 2177282
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2177283
    :cond_0
    return-void
.end method
