.class public final enum LX/DQO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DQO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DQO;

.field public static final enum ADD_TO_HOME_SCREEN:LX/DQO;

.field public static final enum ADMIN_ACTIVITY:LX/DQO;

.field public static final enum ARCHIVE_UNARCHIVE_GROUP:LX/DQO;

.field public static final enum BROWSE_ALL_SUBGROUPS:LX/DQO;

.field public static final enum CHANNELS:LX/DQO;

.field public static final enum COMPANIES:LX/DQO;

.field public static final enum COVER_PHOTO:LX/DQO;

.field public static final enum CREATE_GROUP_CHAT:LX/DQO;

.field public static final enum CREATE_SUBGROUP:LX/DQO;

.field public static final enum EDIT_GROUP_SETTINGS:LX/DQO;

.field public static final enum EDIT_NOTIFICATION_SETTINGS:LX/DQO;

.field public static final enum EVENTS:LX/DQO;

.field public static final enum FAVORITES:LX/DQO;

.field public static final enum FILES:LX/DQO;

.field public static final enum FOLLOW_GROUP:LX/DQO;

.field public static final enum FOR_SALE_POSTS:LX/DQO;

.field public static final enum LEAVE_GROUP:LX/DQO;

.field public static final enum MEMBERS:LX/DQO;

.field public static final enum MEMBER_REQUESTS:LX/DQO;

.field public static final enum PENDING_POSTS:LX/DQO;

.field public static final enum PHOTOS:LX/DQO;

.field public static final enum REPORTED_POSTS:LX/DQO;

.field public static final enum REPORT_GROUP:LX/DQO;

.field public static final enum SEARCH_GROUP:LX/DQO;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1994302
    new-instance v0, LX/DQO;

    const-string v1, "REPORTED_POSTS"

    invoke-direct {v0, v1, v3}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->REPORTED_POSTS:LX/DQO;

    .line 1994303
    new-instance v0, LX/DQO;

    const-string v1, "PENDING_POSTS"

    invoke-direct {v0, v1, v4}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->PENDING_POSTS:LX/DQO;

    .line 1994304
    new-instance v0, LX/DQO;

    const-string v1, "MEMBER_REQUESTS"

    invoke-direct {v0, v1, v5}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->MEMBER_REQUESTS:LX/DQO;

    .line 1994305
    new-instance v0, LX/DQO;

    const-string v1, "MEMBERS"

    invoke-direct {v0, v1, v6}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->MEMBERS:LX/DQO;

    .line 1994306
    new-instance v0, LX/DQO;

    const-string v1, "EDIT_GROUP_SETTINGS"

    invoke-direct {v0, v1, v7}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->EDIT_GROUP_SETTINGS:LX/DQO;

    .line 1994307
    new-instance v0, LX/DQO;

    const-string v1, "ADMIN_ACTIVITY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->ADMIN_ACTIVITY:LX/DQO;

    .line 1994308
    new-instance v0, LX/DQO;

    const-string v1, "COVER_PHOTO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->COVER_PHOTO:LX/DQO;

    .line 1994309
    new-instance v0, LX/DQO;

    const-string v1, "SEARCH_GROUP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->SEARCH_GROUP:LX/DQO;

    .line 1994310
    new-instance v0, LX/DQO;

    const-string v1, "FAVORITES"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->FAVORITES:LX/DQO;

    .line 1994311
    new-instance v0, LX/DQO;

    const-string v1, "CREATE_GROUP_CHAT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->CREATE_GROUP_CHAT:LX/DQO;

    .line 1994312
    new-instance v0, LX/DQO;

    const-string v1, "CREATE_SUBGROUP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->CREATE_SUBGROUP:LX/DQO;

    .line 1994313
    new-instance v0, LX/DQO;

    const-string v1, "BROWSE_ALL_SUBGROUPS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->BROWSE_ALL_SUBGROUPS:LX/DQO;

    .line 1994314
    new-instance v0, LX/DQO;

    const-string v1, "ADD_TO_HOME_SCREEN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->ADD_TO_HOME_SCREEN:LX/DQO;

    .line 1994315
    new-instance v0, LX/DQO;

    const-string v1, "EDIT_NOTIFICATION_SETTINGS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->EDIT_NOTIFICATION_SETTINGS:LX/DQO;

    .line 1994316
    new-instance v0, LX/DQO;

    const-string v1, "PHOTOS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->PHOTOS:LX/DQO;

    .line 1994317
    new-instance v0, LX/DQO;

    const-string v1, "EVENTS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->EVENTS:LX/DQO;

    .line 1994318
    new-instance v0, LX/DQO;

    const-string v1, "FILES"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->FILES:LX/DQO;

    .line 1994319
    new-instance v0, LX/DQO;

    const-string v1, "FOR_SALE_POSTS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->FOR_SALE_POSTS:LX/DQO;

    .line 1994320
    new-instance v0, LX/DQO;

    const-string v1, "REPORT_GROUP"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->REPORT_GROUP:LX/DQO;

    .line 1994321
    new-instance v0, LX/DQO;

    const-string v1, "FOLLOW_GROUP"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->FOLLOW_GROUP:LX/DQO;

    .line 1994322
    new-instance v0, LX/DQO;

    const-string v1, "LEAVE_GROUP"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->LEAVE_GROUP:LX/DQO;

    .line 1994323
    new-instance v0, LX/DQO;

    const-string v1, "ARCHIVE_UNARCHIVE_GROUP"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->ARCHIVE_UNARCHIVE_GROUP:LX/DQO;

    .line 1994324
    new-instance v0, LX/DQO;

    const-string v1, "COMPANIES"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->COMPANIES:LX/DQO;

    .line 1994325
    new-instance v0, LX/DQO;

    const-string v1, "CHANNELS"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/DQO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DQO;->CHANNELS:LX/DQO;

    .line 1994326
    const/16 v0, 0x18

    new-array v0, v0, [LX/DQO;

    sget-object v1, LX/DQO;->REPORTED_POSTS:LX/DQO;

    aput-object v1, v0, v3

    sget-object v1, LX/DQO;->PENDING_POSTS:LX/DQO;

    aput-object v1, v0, v4

    sget-object v1, LX/DQO;->MEMBER_REQUESTS:LX/DQO;

    aput-object v1, v0, v5

    sget-object v1, LX/DQO;->MEMBERS:LX/DQO;

    aput-object v1, v0, v6

    sget-object v1, LX/DQO;->EDIT_GROUP_SETTINGS:LX/DQO;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DQO;->ADMIN_ACTIVITY:LX/DQO;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DQO;->COVER_PHOTO:LX/DQO;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DQO;->SEARCH_GROUP:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/DQO;->FAVORITES:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/DQO;->CREATE_GROUP_CHAT:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/DQO;->CREATE_SUBGROUP:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/DQO;->BROWSE_ALL_SUBGROUPS:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/DQO;->ADD_TO_HOME_SCREEN:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/DQO;->EDIT_NOTIFICATION_SETTINGS:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/DQO;->PHOTOS:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/DQO;->EVENTS:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/DQO;->FILES:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/DQO;->FOR_SALE_POSTS:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/DQO;->REPORT_GROUP:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/DQO;->FOLLOW_GROUP:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/DQO;->LEAVE_GROUP:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/DQO;->ARCHIVE_UNARCHIVE_GROUP:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/DQO;->COMPANIES:LX/DQO;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/DQO;->CHANNELS:LX/DQO;

    aput-object v2, v0, v1

    sput-object v0, LX/DQO;->$VALUES:[LX/DQO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1994327
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DQO;
    .locals 1

    .prologue
    .line 1994328
    const-class v0, LX/DQO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DQO;

    return-object v0
.end method

.method public static values()[LX/DQO;
    .locals 1

    .prologue
    .line 1994329
    sget-object v0, LX/DQO;->$VALUES:[LX/DQO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DQO;

    return-object v0
.end method
