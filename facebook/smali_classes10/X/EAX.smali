.class public final LX/EAX;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Lcom/facebook/reviews/handler/DeleteReviewHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/reviews/handler/DeleteReviewHandler;LX/4BY;Landroid/content/Context;Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2085650
    iput-object p1, p0, LX/EAX;->e:Lcom/facebook/reviews/handler/DeleteReviewHandler;

    iput-object p2, p0, LX/EAX;->a:LX/4BY;

    iput-object p3, p0, LX/EAX;->b:Landroid/content/Context;

    iput-object p4, p0, LX/EAX;->c:Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;

    iput-object p5, p0, LX/EAX;->d:Ljava/lang/String;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 6

    .prologue
    .line 2085651
    iget-object v0, p0, LX/EAX;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2085652
    iget-object v0, p0, LX/EAX;->e:Lcom/facebook/reviews/handler/DeleteReviewHandler;

    iget-object v1, p0, LX/EAX;->b:Landroid/content/Context;

    iget-object v2, p0, LX/EAX;->c:Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;

    .line 2085653
    iget-object v3, v2, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2085654
    iget-object v3, p0, LX/EAX;->d:Ljava/lang/String;

    .line 2085655
    iget-object v4, v0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->d:LX/79D;

    .line 2085656
    const-string v5, "reviews_delete_review_failure"

    invoke-static {v4, v5, v3, v2}, LX/79D;->b(LX/79D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2085657
    iget-object v4, v0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->f:LX/0kL;

    new-instance v5, LX/27k;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0814eb

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v5, p0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v5}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2085658
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2085659
    iget-object v0, p0, LX/EAX;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2085660
    iget-object v0, p0, LX/EAX;->e:Lcom/facebook/reviews/handler/DeleteReviewHandler;

    iget-object v1, p0, LX/EAX;->a:LX/4BY;

    iget-object v2, p0, LX/EAX;->c:Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;

    .line 2085661
    iget-object v3, v2, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2085662
    iget-object v3, p0, LX/EAX;->d:Ljava/lang/String;

    .line 2085663
    iget-object p0, v0, Lcom/facebook/reviews/handler/DeleteReviewHandler;->b:LX/BNE;

    new-instance p1, LX/EAY;

    invoke-direct {p1, v0, v1, v2, v3}, LX/EAY;-><init>(Lcom/facebook/reviews/handler/DeleteReviewHandler;LX/4BY;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2, p1}, LX/BNE;->a(Ljava/lang/String;LX/BNC;)V

    .line 2085664
    return-void
.end method
