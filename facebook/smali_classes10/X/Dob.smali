.class public LX/Dob;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field private static volatile e:LX/Dob;


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/2P6;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2042107
    new-array v0, v3, [Ljava/lang/String;

    sget-object v1, LX/2P2;->a:LX/0U1;

    .line 2042108
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 2042109
    aput-object v1, v0, v2

    sput-object v0, LX/Dob;->a:[Ljava/lang/String;

    .line 2042110
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, LX/2P2;->a:LX/0U1;

    .line 2042111
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 2042112
    aput-object v1, v0, v2

    sget-object v1, LX/2P2;->b:LX/0U1;

    .line 2042113
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2042114
    aput-object v1, v0, v3

    const/4 v1, 0x2

    sget-object v2, LX/2P2;->c:LX/0U1;

    .line 2042115
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2042116
    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/2P2;->d:LX/0U1;

    .line 2042117
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2042118
    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/2P2;->e:LX/0U1;

    .line 2042119
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2042120
    aput-object v2, v0, v1

    sput-object v0, LX/Dob;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2P6;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;",
            "LX/2P6;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042104
    iput-object p1, p0, LX/Dob;->c:LX/0Or;

    .line 2042105
    iput-object p2, p0, LX/Dob;->d:LX/2P6;

    .line 2042106
    return-void
.end method

.method public static a(LX/0QB;)LX/Dob;
    .locals 5

    .prologue
    .line 2042048
    sget-object v0, LX/Dob;->e:LX/Dob;

    if-nez v0, :cond_1

    .line 2042049
    const-class v1, LX/Dob;

    monitor-enter v1

    .line 2042050
    :try_start_0
    sget-object v0, LX/Dob;->e:LX/Dob;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2042051
    if-eqz v2, :cond_0

    .line 2042052
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2042053
    new-instance v4, LX/Dob;

    const/16 v3, 0xdc6

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2P6;->a(LX/0QB;)LX/2P6;

    move-result-object v3

    check-cast v3, LX/2P6;

    invoke-direct {v4, p0, v3}, LX/Dob;-><init>(LX/0Or;LX/2P6;)V

    .line 2042054
    move-object v0, v4

    .line 2042055
    sput-object v0, LX/Dob;->e:LX/Dob;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042056
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2042057
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2042058
    :cond_1
    sget-object v0, LX/Dob;->e:LX/Dob;

    return-object v0

    .line 2042059
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2042060
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)Z
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "Recycle"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 2042085
    sget-object v0, LX/2P2;->a:LX/0U1;

    .line 2042086
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2042087
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 2042088
    iget-object v0, p0, LX/Dob;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 2042089
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2042090
    const-string v1, "thread_participants"

    sget-object v2, LX/Dob;->a:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 2042091
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result v0

    .line 2042092
    if-eqz v2, :cond_0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2042093
    :cond_0
    if-eqz v8, :cond_1

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_1
    return v0

    .line 2042094
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2042095
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v2, :cond_2

    if-eqz v1, :cond_4

    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_2
    :goto_1
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2042096
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2042097
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_2
    if-eqz v8, :cond_3

    if-eqz v9, :cond_5

    :try_start_7
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_3
    :goto_3
    throw v0

    .line 2042098
    :catch_2
    move-exception v2

    :try_start_8
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2042099
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 2042100
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 2042101
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_3

    .line 2042102
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_0
.end method

.method public final b(J)Lcom/facebook/user/model/User;
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "Recycle"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 2042061
    sget-object v0, LX/2P2;->a:LX/0U1;

    .line 2042062
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2042063
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 2042064
    iget-object v0, p0, LX/Dob;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 2042065
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2042066
    const-string v1, "thread_participants"

    sget-object v2, LX/Dob;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 2042067
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2042068
    iget-object v0, p0, LX/Dob;->d:LX/2P6;

    .line 2042069
    sget-object v1, LX/2P2;->a:LX/0U1;

    invoke-virtual {v1, v2}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, LX/2P6;->a(LX/2P6;Landroid/database/Cursor;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v1

    move-object v0, v1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 2042070
    if-eqz v2, :cond_0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2042071
    :cond_0
    if-eqz v8, :cond_1

    invoke-virtual {v8}, LX/2gJ;->close()V

    .line 2042072
    :cond_1
    :goto_0
    return-object v0

    .line 2042073
    :cond_2
    if-eqz v2, :cond_3

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2042074
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_4
    move-object v0, v9

    .line 2042075
    goto :goto_0

    .line 2042076
    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2042077
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_1
    if-eqz v2, :cond_5

    if-eqz v1, :cond_7

    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_5
    :goto_2
    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 2042078
    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2042079
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_3
    if-eqz v8, :cond_6

    if-eqz v9, :cond_8

    :try_start_8
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    :cond_6
    :goto_4
    throw v0

    .line 2042080
    :catch_2
    move-exception v2

    :try_start_9
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2042081
    :catchall_2
    move-exception v0

    goto :goto_3

    .line 2042082
    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_2

    .line 2042083
    :catch_3
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_8
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_4

    .line 2042084
    :catchall_3
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method
