.class public LX/Cl5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static t:LX/0Xm;


# instance fields
.field public final a:LX/K25;

.field public final b:LX/0So;

.field public final c:LX/Chv;

.field public final d:LX/Cig;

.field private final e:LX/ClD;

.field private final f:Landroid/content/Context;

.field public final g:LX/ClO;

.field private final h:LX/8bZ;

.field public i:LX/Chz;

.field public j:LX/Cik;

.field public k:J

.field public l:J

.field public m:J

.field public n:J

.field public o:D

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:LX/Cl4;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0So;LX/K25;LX/Chv;LX/Cig;LX/ClD;LX/ClO;LX/8bZ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1931952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1931953
    sget-object v0, LX/Cl4;->IDLE:LX/Cl4;

    iput-object v0, p0, LX/Cl5;->s:LX/Cl4;

    .line 1931954
    iput-object p2, p0, LX/Cl5;->b:LX/0So;

    .line 1931955
    iput-object p3, p0, LX/Cl5;->a:LX/K25;

    .line 1931956
    iput-object p4, p0, LX/Cl5;->c:LX/Chv;

    .line 1931957
    iput-object p5, p0, LX/Cl5;->d:LX/Cig;

    .line 1931958
    iput-object p6, p0, LX/Cl5;->e:LX/ClD;

    .line 1931959
    iput-object p1, p0, LX/Cl5;->f:Landroid/content/Context;

    .line 1931960
    iput-object p7, p0, LX/Cl5;->g:LX/ClO;

    .line 1931961
    iput-object p8, p0, LX/Cl5;->h:LX/8bZ;

    .line 1931962
    iget-object v0, p0, LX/Cl5;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/Cl5;->k:J

    .line 1931963
    new-instance v0, LX/Cl2;

    invoke-direct {v0, p0}, LX/Cl2;-><init>(LX/Cl5;)V

    iput-object v0, p0, LX/Cl5;->i:LX/Chz;

    .line 1931964
    new-instance v0, LX/Cl3;

    invoke-direct {v0, p0}, LX/Cl3;-><init>(LX/Cl5;)V

    iput-object v0, p0, LX/Cl5;->j:LX/Cik;

    .line 1931965
    iget-object v0, p0, LX/Cl5;->c:LX/Chv;

    iget-object v1, p0, LX/Cl5;->i:LX/Chz;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1931966
    iget-object v0, p0, LX/Cl5;->d:LX/Cig;

    iget-object v1, p0, LX/Cl5;->j:LX/Cik;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1931967
    return-void
.end method

.method public static a(LX/0QB;)LX/Cl5;
    .locals 12

    .prologue
    .line 1931910
    const-class v1, LX/Cl5;

    monitor-enter v1

    .line 1931911
    :try_start_0
    sget-object v0, LX/Cl5;->t:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1931912
    sput-object v2, LX/Cl5;->t:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1931913
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1931914
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1931915
    new-instance v3, LX/Cl5;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    .line 1931916
    invoke-static {v0}, LX/1Ay;->a(LX/0QB;)LX/1Ay;

    move-result-object v6

    check-cast v6, LX/1Ay;

    invoke-static {v6}, LX/K24;->a(LX/1Ay;)LX/K25;

    move-result-object v6

    move-object v6, v6

    .line 1931917
    check-cast v6, LX/K25;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v7

    check-cast v7, LX/Chv;

    invoke-static {v0}, LX/Cig;->a(LX/0QB;)LX/Cig;

    move-result-object v8

    check-cast v8, LX/Cig;

    invoke-static {v0}, LX/ClD;->a(LX/0QB;)LX/ClD;

    move-result-object v9

    check-cast v9, LX/ClD;

    invoke-static {v0}, LX/ClO;->a(LX/0QB;)LX/ClO;

    move-result-object v10

    check-cast v10, LX/ClO;

    invoke-static {v0}, LX/8bZ;->b(LX/0QB;)LX/8bZ;

    move-result-object v11

    check-cast v11, LX/8bZ;

    invoke-direct/range {v3 .. v11}, LX/Cl5;-><init>(Landroid/content/Context;LX/0So;LX/K25;LX/Chv;LX/Cig;LX/ClD;LX/ClO;LX/8bZ;)V

    .line 1931918
    move-object v0, v3

    .line 1931919
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1931920
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Cl5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1931921
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1931922
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static f(LX/Cl5;)V
    .locals 15

    .prologue
    .line 1931923
    iget-object v0, p0, LX/Cl5;->p:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1931924
    :cond_0
    :goto_0
    return-void

    .line 1931925
    :cond_1
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1931926
    iget-object v0, p0, LX/Cl5;->e:LX/ClD;

    invoke-virtual {v0}, LX/ClD;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1931927
    const-string v0, "article_ID"

    iget-object v1, p0, LX/Cl5;->q:Ljava/lang/String;

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931928
    const-string v0, "article_depth_level"

    iget-object v1, p0, LX/Cl5;->e:LX/ClD;

    iget-object v2, p0, LX/Cl5;->f:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/ClD;->a(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931929
    const-string v0, "article_chaining_ID"

    iget-object v1, p0, LX/Cl5;->e:LX/ClD;

    iget-object v2, p0, LX/Cl5;->f:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/ClD;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931930
    const-string v0, "article_aggregate_view_time"

    iget-wide v2, p0, LX/Cl5;->n:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931931
    const-string v0, "article_aggregate_load_time"

    iget-wide v2, p0, LX/Cl5;->o:D

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931932
    const-string v0, "click_source"

    iget-object v1, p0, LX/Cl5;->r:Ljava/lang/String;

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931933
    iget-object v0, p0, LX/Cl5;->h:LX/8bZ;

    invoke-virtual {v0}, LX/8bZ;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "tablet"

    .line 1931934
    :goto_1
    const-string v1, "device_type"

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931935
    iget-object v0, p0, LX/Cl5;->a:LX/K25;

    if-eqz v0, :cond_0

    .line 1931936
    iget-object v0, p0, LX/Cl5;->a:LX/K25;

    iget-object v1, p0, LX/Cl5;->p:Ljava/lang/String;

    iget-wide v2, p0, LX/Cl5;->l:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    iget-wide v2, p0, LX/Cl5;->l:J

    long-to-double v2, v2

    :goto_2
    iget-wide v4, p0, LX/Cl5;->n:J

    long-to-double v4, v4

    .line 1931937
    iget-object v7, v0, LX/K25;->a:LX/1Ay;

    if-eqz v7, :cond_2

    .line 1931938
    iget-object v7, v0, LX/K25;->a:LX/1Ay;

    move-object v8, v1

    move-wide v9, v2

    move-wide v11, v4

    move-object v13, v6

    invoke-virtual/range {v7 .. v13}, LX/1Ay;->a(Ljava/lang/String;DDLjava/util/Map;)V

    .line 1931939
    :cond_2
    goto/16 :goto_0

    .line 1931940
    :cond_3
    const-string v0, "phone"

    goto :goto_1

    .line 1931941
    :cond_4
    iget-object v2, p0, LX/Cl5;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/Cl5;->k:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    goto :goto_2
.end method


# virtual methods
.method public final b()V
    .locals 8

    .prologue
    .line 1931942
    iget-object v0, p0, LX/Cl5;->s:LX/Cl4;

    sget-object v1, LX/Cl4;->TRACKING:LX/Cl4;

    if-ne v0, v1, :cond_1

    .line 1931943
    const-wide/16 v6, 0x0

    .line 1931944
    iget-object v2, p0, LX/Cl5;->p:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/Cl5;->m:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_2

    .line 1931945
    :cond_0
    :goto_0
    sget-object v0, LX/Cl4;->PAUSED:LX/Cl4;

    iput-object v0, p0, LX/Cl5;->s:LX/Cl4;

    .line 1931946
    :cond_1
    return-void

    .line 1931947
    :cond_2
    iget-object v2, p0, LX/Cl5;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    .line 1931948
    iget-wide v4, p0, LX/Cl5;->l:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 1931949
    iget-wide v4, p0, LX/Cl5;->m:J

    sub-long/2addr v2, v4

    .line 1931950
    iget-wide v4, p0, LX/Cl5;->n:J

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/Cl5;->n:J

    .line 1931951
    iget-wide v2, p0, LX/Cl5;->o:D

    iget-wide v4, p0, LX/Cl5;->l:J

    long-to-double v4, v4

    add-double/2addr v2, v4

    iput-wide v2, p0, LX/Cl5;->o:D

    goto :goto_0
.end method
