.class public final LX/DuG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 2056164
    const/4 v12, 0x0

    .line 2056165
    const/4 v11, 0x0

    .line 2056166
    const/4 v10, 0x0

    .line 2056167
    const/4 v9, 0x0

    .line 2056168
    const/4 v8, 0x0

    .line 2056169
    const/4 v7, 0x0

    .line 2056170
    const/4 v6, 0x0

    .line 2056171
    const/4 v5, 0x0

    .line 2056172
    const/4 v4, 0x0

    .line 2056173
    const/4 v3, 0x0

    .line 2056174
    const/4 v2, 0x0

    .line 2056175
    const/4 v1, 0x0

    .line 2056176
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 2056177
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2056178
    const/4 v1, 0x0

    .line 2056179
    :goto_0
    return v1

    .line 2056180
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2056181
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_e

    .line 2056182
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 2056183
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2056184
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 2056185
    const-string v14, "__type__"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2

    const-string v14, "__typename"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 2056186
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v12

    goto :goto_1

    .line 2056187
    :cond_3
    const-string v14, "description"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 2056188
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 2056189
    :cond_4
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 2056190
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 2056191
    :cond_5
    const-string v14, "item_availability"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 2056192
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLProductAvailability;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 2056193
    :cond_6
    const-string v14, "item_details"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 2056194
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2056195
    :cond_7
    const-string v14, "merchant_logo"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 2056196
    invoke-static/range {p0 .. p1}, LX/DuE;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 2056197
    :cond_8
    const-string v14, "name"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 2056198
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 2056199
    :cond_9
    const-string v14, "platform_images"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 2056200
    invoke-static/range {p0 .. p1}, LX/DuF;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 2056201
    :cond_a
    const-string v14, "platform_photo_urls"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 2056202
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2056203
    :cond_b
    const-string v14, "product_item_price"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 2056204
    invoke-static/range {p0 .. p1}, LX/DuA;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2056205
    :cond_c
    const-string v14, "reference_url"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_d

    .line 2056206
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 2056207
    :cond_d
    const-string v14, "seller"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 2056208
    invoke-static/range {p0 .. p1}, LX/DuK;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 2056209
    :cond_e
    const/16 v13, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 2056210
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 2056211
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 2056212
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 2056213
    const/4 v10, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 2056214
    const/4 v9, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 2056215
    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 2056216
    const/4 v7, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 2056217
    const/4 v6, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 2056218
    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 2056219
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 2056220
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 2056221
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2056222
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 2056223
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2056224
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2056225
    if-eqz v0, :cond_0

    .line 2056226
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056227
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2056228
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056229
    if-eqz v0, :cond_1

    .line 2056230
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056231
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056232
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056233
    if-eqz v0, :cond_2

    .line 2056234
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056235
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056236
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2056237
    if-eqz v0, :cond_3

    .line 2056238
    const-string v0, "item_availability"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056239
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056240
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2056241
    if-eqz v0, :cond_4

    .line 2056242
    const-string v0, "item_details"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056243
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2056244
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056245
    if-eqz v0, :cond_5

    .line 2056246
    const-string v1, "merchant_logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056247
    invoke-static {p0, v0, p2}, LX/DuE;->a(LX/15i;ILX/0nX;)V

    .line 2056248
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056249
    if-eqz v0, :cond_6

    .line 2056250
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056251
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056252
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056253
    if-eqz v0, :cond_8

    .line 2056254
    const-string v1, "platform_images"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056255
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2056256
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 2056257
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/DuF;->a(LX/15i;ILX/0nX;)V

    .line 2056258
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2056259
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2056260
    :cond_8
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 2056261
    if-eqz v0, :cond_9

    .line 2056262
    const-string v0, "platform_photo_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056263
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2056264
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056265
    if-eqz v0, :cond_a

    .line 2056266
    const-string v1, "product_item_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056267
    invoke-static {p0, v0, p2}, LX/DuA;->a(LX/15i;ILX/0nX;)V

    .line 2056268
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056269
    if-eqz v0, :cond_b

    .line 2056270
    const-string v1, "reference_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056271
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056272
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2056273
    if-eqz v0, :cond_c

    .line 2056274
    const-string v1, "seller"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056275
    invoke-static {p0, v0, p2}, LX/DuK;->a(LX/15i;ILX/0nX;)V

    .line 2056276
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2056277
    return-void
.end method
