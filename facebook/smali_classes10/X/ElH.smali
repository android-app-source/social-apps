.class public LX/ElH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Eko;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2164151
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2164152
    :try_start_1
    const/16 v1, 0x400

    new-array v1, v1, [B

    .line 2164153
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 2164154
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2164155
    :cond_0
    :try_start_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 2164156
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 2164157
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    return-object v0

    .line 2164158
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2164159
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v0
.end method


# virtual methods
.method public final a(LX/Ekj;LX/ElA;)V
    .locals 4

    .prologue
    const/16 v2, 0x190

    .line 2164160
    invoke-virtual {p2}, LX/ElA;->a()I

    move-result v0

    .line 2164161
    if-lt v0, v2, :cond_2

    .line 2164162
    invoke-virtual {p2}, LX/ElA;->c()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, LX/ElH;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    .line 2164163
    if-lt v0, v2, :cond_0

    const/16 v2, 0x1f4

    if-ge v0, v2, :cond_0

    .line 2164164
    invoke-static {v1}, LX/ElI;->a(Ljava/lang/String;)V

    .line 2164165
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2164166
    invoke-virtual {p2}, LX/ElA;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2164167
    if-eqz v1, :cond_1

    .line 2164168
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2164169
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2164170
    :cond_1
    new-instance v1, LX/ElU;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/ElU;-><init>(ILjava/lang/String;)V

    throw v1

    .line 2164171
    :cond_2
    return-void
.end method
