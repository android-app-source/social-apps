.class public final LX/Dcn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;)V
    .locals 0

    .prologue
    .line 2018940
    iput-object p1, p0, LX/Dcn;->a:Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 4

    .prologue
    .line 2018941
    iget-object v0, p0, LX/Dcn;->a:Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;

    iget-object v0, v0, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->c:LX/DbN;

    iget-object v1, p0, LX/Dcn;->a:Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;

    iget-object v1, v1, Lcom/facebook/localcontent/photos/PhotosByCategoryTabPagerFragment;->h:Ljava/lang/String;

    .line 2018942
    iget-object v2, v0, LX/DbN;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "photos_by_category_tab_tap"

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "photos_by_category"

    .line 2018943
    iput-object p0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2018944
    move-object v3, v3

    .line 2018945
    const-string p0, "page_id"

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "tab_position"

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2018946
    return-void
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2018947
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2018948
    return-void
.end method
