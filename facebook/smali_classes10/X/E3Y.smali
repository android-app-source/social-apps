.class public final LX/E3Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E2S;

.field public final synthetic b:LX/9u8;

.field public final synthetic c:LX/1Pq;

.field public final synthetic d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic e:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;LX/E2S;LX/9u8;LX/1Pq;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 2074730
    iput-object p1, p0, LX/E3Y;->f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;

    iput-object p2, p0, LX/E3Y;->a:LX/E2S;

    iput-object p3, p0, LX/E3Y;->b:LX/9u8;

    iput-object p4, p0, LX/E3Y;->c:LX/1Pq;

    iput-object p5, p0, LX/E3Y;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p6, p0, LX/E3Y;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x2

    const v1, 0x72f382fd

    invoke-static {v0, v8, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2074731
    iget-object v0, p0, LX/E3Y;->a:LX/E2S;

    .line 2074732
    iget-object v1, v0, LX/E2S;->a:LX/03R;

    move-object v3, v1

    .line 2074733
    sget-object v0, LX/03R;->YES:LX/03R;

    if-ne v3, v0, :cond_0

    .line 2074734
    iget-object v0, p0, LX/E3Y;->f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->g:LX/3mF;

    iget-object v1, p0, LX/E3Y;->b:LX/9u8;

    invoke-interface {v1}, LX/9u8;->aF()Ljava/lang/String;

    move-result-object v1

    const-string v4, "mobile_group_join"

    const-string v5, "ALLOW_READD"

    invoke-virtual {v0, v1, v4, v5}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2074735
    iget-object v0, p0, LX/E3Y;->a:LX/E2S;

    sget-object v4, LX/03R;->NO:LX/03R;

    invoke-virtual {v0, v4}, LX/E2S;->a(LX/03R;)V

    .line 2074736
    iget-object v0, p0, LX/E3Y;->c:LX/1Pq;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    iget-object v4, p0, LX/E3Y;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074737
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v5

    .line 2074738
    iget-object v5, p0, LX/E3Y;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074739
    iget-object v6, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v6

    .line 2074740
    sget-object v6, LX/Cfc;->GROUP_LEAVE_TAP:LX/Cfc;

    iget-object v7, p0, LX/E3Y;->b:LX/9u8;

    invoke-interface {v7}, LX/9u8;->aF()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v4, v5, v6, v7}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;)V

    move-object v0, v1

    .line 2074741
    :goto_0
    iget-object v1, p0, LX/E3Y;->c:LX/1Pq;

    new-array v4, v8, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x0

    iget-object v6, p0, LX/E3Y;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v6, v4, v5

    invoke-interface {v1, v4}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2074742
    new-instance v1, LX/E3X;

    invoke-direct {v1, p0, v3}, LX/E3X;-><init>(LX/E3Y;LX/03R;)V

    iget-object v3, p0, LX/E3Y;->f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;

    iget-object v3, v3, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2074743
    const v0, 0x5ff709aa

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 2074744
    :cond_0
    iget-object v0, p0, LX/E3Y;->f:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionGroupDescriptionPartDefinition;->g:LX/3mF;

    iget-object v1, p0, LX/E3Y;->b:LX/9u8;

    invoke-interface {v1}, LX/9u8;->aF()Ljava/lang/String;

    move-result-object v1

    const-string v4, "mobile_group_join"

    invoke-virtual {v0, v1, v4}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2074745
    iget-object v0, p0, LX/E3Y;->a:LX/E2S;

    sget-object v4, LX/03R;->YES:LX/03R;

    invoke-virtual {v0, v4}, LX/E2S;->a(LX/03R;)V

    .line 2074746
    iget-object v0, p0, LX/E3Y;->c:LX/1Pq;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    iget-object v4, p0, LX/E3Y;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074747
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v5

    .line 2074748
    iget-object v5, p0, LX/E3Y;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074749
    iget-object v6, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v6

    .line 2074750
    sget-object v6, LX/Cfc;->GROUP_JOIN_TAP:LX/Cfc;

    iget-object v7, p0, LX/E3Y;->b:LX/9u8;

    invoke-interface {v7}, LX/9u8;->aF()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v4, v5, v6, v7}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method
