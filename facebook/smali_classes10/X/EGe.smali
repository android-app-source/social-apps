.class public LX/EGe;
.super LX/0te;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements LX/EC9;
.implements LX/EF7;
.implements LX/EGd;
.implements LX/ECD;
.implements LX/4mc;


# static fields
.field public static final x:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/EFt;

.field private B:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EFk;",
            ">;"
        }
    .end annotation
.end field

.field public C:LX/EC0;

.field public D:LX/EI9;

.field private final E:Landroid/os/IBinder;

.field public F:I

.field public G:I

.field public H:F

.field private I:I

.field public J:I

.field public K:I

.field public L:I

.field private M:Z

.field public N:Z

.field public O:LX/0Yd;

.field public P:Z

.field public Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

.field public R:Landroid/view/View;

.field public S:Z

.field public T:J

.field public U:Z

.field public V:Z

.field public W:LX/EHn;

.field public X:Z

.field private Y:LX/0xh;

.field private Z:J

.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1r1;",
            ">;"
        }
    .end annotation
.end field

.field private aa:J

.field public ab:J

.field public ac:J

.field public ad:I

.field private ae:Z

.field public af:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public ag:LX/EFp;

.field private ah:LX/EFq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ai:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDT;",
            ">;"
        }
    .end annotation
.end field

.field public aj:Z

.field private ak:LX/1ql;

.field public final b:Ljava/lang/String;

.field public c:Landroid/view/WindowManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/D9W;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/2S7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/6c4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/0So;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/1Ml;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/3A0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/3Eb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/EDC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/EFs;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:Ljava/util/concurrent/ScheduledFuture;

.field public u:Ljava/util/concurrent/ScheduledFuture;

.field public v:Ljava/util/concurrent/ScheduledFuture;

.field public w:Ljava/util/concurrent/ScheduledFuture;

.field public y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field public z:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EFj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2098019
    const-class v0, LX/EGe;

    sput-object v0, LX/EGe;->x:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 2097993
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2097994
    const-string v0, "homekey"

    iput-object v0, p0, LX/EGe;->b:Ljava/lang/String;

    .line 2097995
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2097996
    iput-object v0, p0, LX/EGe;->y:LX/0Ot;

    .line 2097997
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2097998
    iput-object v0, p0, LX/EGe;->z:LX/0Ot;

    .line 2097999
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2098000
    iput-object v0, p0, LX/EGe;->B:LX/0Ot;

    .line 2098001
    new-instance v0, LX/EGa;

    invoke-direct {v0, p0}, LX/EGa;-><init>(LX/EGe;)V

    iput-object v0, p0, LX/EGe;->E:Landroid/os/IBinder;

    .line 2098002
    const v0, 0x3fd55555

    iput v0, p0, LX/EGe;->H:F

    .line 2098003
    iput-wide v2, p0, LX/EGe;->T:J

    .line 2098004
    iput-boolean v1, p0, LX/EGe;->U:Z

    .line 2098005
    iput-boolean v4, p0, LX/EGe;->V:Z

    .line 2098006
    iput-object v5, p0, LX/EGe;->W:LX/EHn;

    .line 2098007
    iput-boolean v1, p0, LX/EGe;->X:Z

    .line 2098008
    iput-object v5, p0, LX/EGe;->Y:LX/0xh;

    .line 2098009
    iput-wide v6, p0, LX/EGe;->Z:J

    .line 2098010
    iput-wide v6, p0, LX/EGe;->aa:J

    .line 2098011
    iput-wide v2, p0, LX/EGe;->ab:J

    .line 2098012
    iput-wide v2, p0, LX/EGe;->ac:J

    .line 2098013
    iput v1, p0, LX/EGe;->ad:I

    .line 2098014
    iput-boolean v1, p0, LX/EGe;->ae:Z

    .line 2098015
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2098016
    iput-object v0, p0, LX/EGe;->ai:LX/0Ot;

    .line 2098017
    iput-boolean v4, p0, LX/EGe;->aj:Z

    .line 2098018
    return-void
.end method

.method public static F(LX/EGe;)I
    .locals 3

    .prologue
    const/16 v1, 0x96

    .line 2097984
    invoke-virtual {p0}, LX/EGe;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2097985
    :goto_0
    return v0

    .line 2097986
    :cond_0
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2097987
    invoke-virtual {p0}, LX/EGe;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 2097988
    goto :goto_0

    .line 2097989
    :cond_1
    const/16 v0, 0xb4

    goto :goto_0

    .line 2097990
    :cond_2
    invoke-static {p0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-static {p0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2097991
    :cond_4
    const/16 v0, 0x78

    goto :goto_0

    :cond_5
    move v0, v1

    .line 2097992
    goto :goto_0
.end method

.method private G()V
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 2097909
    iget-object v0, p0, LX/EGe;->C:LX/EC0;

    if-eqz v0, :cond_0

    .line 2097910
    :goto_0
    return-void

    .line 2097911
    :cond_0
    const v0, 0x7f0e05a4

    invoke-virtual {p0, v0}, LX/EGe;->setTheme(I)V

    .line 2097912
    invoke-virtual {p0}, LX/EGe;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "status_bar_height"

    const-string v2, "dimen"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2097913
    if-lez v0, :cond_1

    .line 2097914
    invoke-virtual {p0}, LX/EGe;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/EGe;->L:I

    .line 2097915
    :cond_1
    invoke-static {p0}, LX/EGe;->N$redex0(LX/EGe;)V

    .line 2097916
    const/16 v0, 0x78

    const/16 v1, 0xc8

    invoke-static {p0, v0, v1}, LX/EGe;->b(LX/EGe;II)V

    .line 2097917
    iget-object v0, p0, LX/EGe;->ah:LX/EFq;

    new-instance v1, LX/EGb;

    invoke-direct {v1, p0}, LX/EGb;-><init>(LX/EGe;)V

    .line 2097918
    new-instance v4, LX/EFp;

    invoke-direct {v4, v1}, LX/EFp;-><init>(LX/EGb;)V

    .line 2097919
    const/16 v2, 0x3257

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v2

    check-cast v2, LX/0wW;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 2097920
    iput-object v6, v4, LX/EFp;->c:LX/0Ot;

    iput-object v2, v4, LX/EFp;->d:LX/0wW;

    iput-object v3, v4, LX/EFp;->e:Landroid/content/Context;

    .line 2097921
    move-object v0, v4

    .line 2097922
    iput-object v0, p0, LX/EGe;->ag:LX/EFp;

    .line 2097923
    iget-object v0, p0, LX/EGe;->ag:LX/EFp;

    const/4 v9, 0x0

    const-wide v10, 0x3fd3333333333333L    # 0.3

    .line 2097924
    new-instance v6, Landroid/view/GestureDetector;

    iget-object v7, v0, LX/EFp;->e:Landroid/content/Context;

    new-instance v8, LX/EFn;

    invoke-direct {v8, v0}, LX/EFn;-><init>(LX/EFp;)V

    invoke-direct {v6, v7, v8}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v6, v0, LX/EFp;->f:Landroid/view/GestureDetector;

    .line 2097925
    iget-object v6, v0, LX/EFp;->f:Landroid/view/GestureDetector;

    invoke-virtual {v6, v9}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 2097926
    iget-object v6, v0, LX/EFp;->d:LX/0wW;

    invoke-virtual {v6}, LX/0wW;->a()LX/0wd;

    move-result-object v6

    sget-object v7, LX/EFp;->b:LX/0wT;

    invoke-virtual {v6, v7}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v6

    .line 2097927
    iput-wide v10, v6, LX/0wd;->k:D

    .line 2097928
    move-object v6, v6

    .line 2097929
    iput-wide v10, v6, LX/0wd;->l:D

    .line 2097930
    move-object v6, v6

    .line 2097931
    new-instance v7, LX/EFo;

    invoke-direct {v7, v0}, LX/EFo;-><init>(LX/EFp;)V

    invoke-virtual {v6, v7}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v6

    iput-object v6, v0, LX/EFp;->p:LX/0wd;

    .line 2097932
    iget-object v6, v0, LX/EFp;->d:LX/0wW;

    invoke-virtual {v6}, LX/0wW;->a()LX/0wd;

    move-result-object v6

    sget-object v7, LX/EFp;->b:LX/0wT;

    invoke-virtual {v6, v7}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v6

    .line 2097933
    iput-wide v10, v6, LX/0wd;->k:D

    .line 2097934
    move-object v6, v6

    .line 2097935
    iput-wide v10, v6, LX/0wd;->l:D

    .line 2097936
    move-object v6, v6

    .line 2097937
    new-instance v7, LX/EFo;

    invoke-direct {v7, v0}, LX/EFo;-><init>(LX/EFp;)V

    invoke-virtual {v6, v7}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v6

    iput-object v6, v0, LX/EFp;->q:LX/0wd;

    .line 2097938
    new-instance v0, LX/EFt;

    iget-object v1, p0, LX/EGe;->j:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v0, v1}, LX/EFt;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    iput-object v0, p0, LX/EGe;->A:LX/EFt;

    .line 2097939
    iget-object v0, p0, LX/EGe;->i:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/3Dx;->ek:S

    invoke-interface {v0, v1, v2, v3, v5}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/EGe;->U:Z

    .line 2097940
    iget-object v0, p0, LX/EGe;->i:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/3Dx;->ei:S

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/EGe;->V:Z

    .line 2097941
    new-instance v0, LX/EGT;

    invoke-direct {v0, p0}, LX/EGT;-><init>(LX/EGe;)V

    iput-object v0, p0, LX/EGe;->C:LX/EC0;

    .line 2097942
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, LX/EGe;->C:LX/EC0;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EC0;)V

    .line 2097943
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097944
    iget-object v1, v0, LX/EDx;->bb:LX/EIq;

    move-object v0, v1

    .line 2097945
    invoke-virtual {v0, p0}, LX/EIq;->a(LX/ECD;)V

    .line 2097946
    new-instance v0, LX/EI9;

    invoke-direct {v0, p0}, LX/EI9;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/EGe;->D:LX/EI9;

    .line 2097947
    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ay()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/EI9;->setPeerName(Ljava/lang/String;)V

    .line 2097948
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    const/16 v1, 0x32

    .line 2097949
    iput v1, v0, LX/EI9;->S:I

    .line 2097950
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    .line 2097951
    iput-object p0, v0, LX/EI9;->V:LX/EGd;

    .line 2097952
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    iget-object v1, p0, LX/EGe;->ag:LX/EFp;

    invoke-virtual {v0, v1}, LX/EI9;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2097953
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    new-instance v1, LX/EGU;

    invoke-direct {v1, p0}, LX/EGU;-><init>(LX/EGe;)V

    .line 2097954
    iput-object v1, v0, LX/EI9;->Q:LX/7fE;

    .line 2097955
    iget-object v0, p0, LX/EGe;->ag:LX/EFp;

    invoke-virtual {v0}, LX/EFp;->g()V

    .line 2097956
    invoke-static {p0}, LX/EGe;->F(LX/EGe;)I

    move-result v0

    invoke-static {p0}, LX/EGe;->F(LX/EGe;)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, LX/EGe;->H:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {p0, v0, v1, v5}, LX/EGe;->a$redex0(LX/EGe;IIZ)V

    .line 2097957
    new-instance v0, LX/EHn;

    invoke-direct {v0, p0}, LX/EHn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/EGe;->W:LX/EHn;

    .line 2097958
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    iget-object v1, p0, LX/EGe;->W:LX/EHn;

    new-instance v2, LX/EGV;

    invoke-direct {v2, p0}, LX/EGV;-><init>(LX/EGe;)V

    invoke-virtual {v0, v1, v2}, LX/EFs;->a(Landroid/view/View;LX/EFr;)V

    .line 2097959
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    new-instance v1, LX/EGW;

    invoke-direct {v1, p0}, LX/EGW;-><init>(LX/EGe;)V

    .line 2097960
    iput-object v1, v0, LX/EFs;->g:LX/EGW;

    .line 2097961
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    .line 2097962
    iget v2, p0, LX/EGe;->F:I

    move v2, v2

    .line 2097963
    iget v3, p0, LX/EGe;->G:I

    move v3, v3

    .line 2097964
    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, LX/EI9;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2097965
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/EI9;->setVisibility(I)V

    .line 2097966
    new-instance v0, LX/0Yd;

    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    new-instance v3, LX/EGL;

    invoke-direct {v3, p0}, LX/EGL;-><init>(LX/EGe;)V

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    new-instance v3, LX/EGY;

    invoke-direct {v3, p0}, LX/EGY;-><init>(LX/EGe;)V

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v2, "android.intent.action.USER_PRESENT"

    new-instance v3, LX/EGX;

    invoke-direct {v3, p0}, LX/EGX;-><init>(LX/EGe;)V

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, LX/EGe;->O:LX/0Yd;

    .line 2097967
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2097968
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2097969
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2097970
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2097971
    iget-object v1, p0, LX/EGe;->O:LX/0Yd;

    invoke-virtual {p0, v1, v0}, LX/EGe;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2097972
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    invoke-virtual {v0}, LX/EFs;->b()V

    .line 2097973
    const-wide/16 v8, 0x1388

    .line 2097974
    const/4 v6, 0x0

    .line 2097975
    iget-object v7, p0, LX/EGe;->r:LX/0Uh;

    const/16 v10, 0x601

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, LX/0Uh;->a(IZ)Z

    move-result v7

    .line 2097976
    if-eqz v7, :cond_2

    .line 2097977
    iget-object v6, p0, LX/EGe;->k:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v7, Lcom/facebook/rtc/services/BackgroundVideoCallService$6;

    invoke-direct {v7, p0}, Lcom/facebook/rtc/services/BackgroundVideoCallService$6;-><init>(LX/EGe;)V

    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide v10, v8

    invoke-interface/range {v6 .. v12}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v6

    .line 2097978
    :cond_2
    move-object v0, v6

    .line 2097979
    iput-object v0, p0, LX/EGe;->v:Ljava/util/concurrent/ScheduledFuture;

    .line 2097980
    iget-object v0, p0, LX/EGe;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDT;

    new-instance v1, LX/EGR;

    invoke-direct {v1, p0}, LX/EGR;-><init>(LX/EGe;)V

    .line 2097981
    iget-object v2, v0, LX/EDT;->d:LX/EDP;

    invoke-virtual {v2, p0}, LX/EDP;->a(LX/EC9;)LX/EDO;

    move-result-object v2

    iput-object v2, v0, LX/EDT;->c:LX/EDO;

    .line 2097982
    iput-object v1, v0, LX/EDT;->j:LX/EDR;

    .line 2097983
    goto/16 :goto_0
.end method

.method public static I$redex0(LX/EGe;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2097907
    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    if-nez v1, :cond_1

    .line 2097908
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v1}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v1

    sget-object v2, LX/EI8;->GROUP_COUNTDOWN:LX/EI8;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private J()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2097903
    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    if-nez v1, :cond_1

    .line 2097904
    :cond_0
    :goto_0
    return v0

    .line 2097905
    :cond_1
    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v1}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v1

    .line 2097906
    sget-object v2, LX/EI8;->END_CALL_STATE:LX/EI8;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/EI8;->END_CALL_STATE_WITH_RETRY:LX/EI8;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static N$redex0(LX/EGe;)V
    .locals 3

    .prologue
    .line 2097894
    iget-object v0, p0, LX/EGe;->c:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2097895
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2097896
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2097897
    invoke-virtual {p0}, LX/EGe;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 2097898
    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v2, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/EGe;->J:I

    .line 2097899
    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/EGe;->K:I

    .line 2097900
    :goto_0
    return-void

    .line 2097901
    :cond_0
    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v2, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/EGe;->J:I

    .line 2097902
    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/EGe;->K:I

    goto :goto_0
.end method

.method public static Q(LX/EGe;)Z
    .locals 2

    .prologue
    .line 2097889
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097890
    invoke-virtual {v0}, LX/EDx;->N()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2097891
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/EDx;->B(LX/EDx;Z)Z

    move-result v1

    move v1, v1

    .line 2097892
    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2097893
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static R$redex0(LX/EGe;)Z
    .locals 1

    .prologue
    .line 2097884
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aa()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->M()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097885
    iget-boolean p0, v0, LX/EDx;->aq:Z

    move v0, p0

    .line 2097886
    if-nez v0, :cond_1

    .line 2097887
    const/4 v0, 0x1

    .line 2097888
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static T(LX/EGe;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const-wide/16 v6, -0x1

    .line 2097877
    iget-wide v0, p0, LX/EGe;->ab:J

    cmp-long v0, v0, v8

    if-lez v0, :cond_0

    .line 2097878
    iget-wide v0, p0, LX/EGe;->Z:J

    iget-object v2, p0, LX/EGe;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/EGe;->ab:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/EGe;->Z:J

    .line 2097879
    iput-wide v6, p0, LX/EGe;->ab:J

    .line 2097880
    :cond_0
    iget-wide v0, p0, LX/EGe;->ac:J

    cmp-long v0, v0, v8

    if-lez v0, :cond_1

    .line 2097881
    iget-wide v0, p0, LX/EGe;->aa:J

    iget-object v2, p0, LX/EGe;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/EGe;->ac:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/EGe;->aa:J

    .line 2097882
    iput-wide v6, p0, LX/EGe;->ac:J

    .line 2097883
    :cond_1
    return-void
.end method

.method public static V(LX/EGe;)Z
    .locals 1

    .prologue
    .line 2097786
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/EGe;->ai(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/EGe;->aj(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/EGe;->X:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EGe;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2097787
    :cond_0
    const/4 v0, 0x0

    .line 2097788
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static W(LX/EGe;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2097861
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ag()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2097862
    :goto_0
    return v0

    .line 2097863
    :cond_0
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097864
    iget-boolean v2, v0, LX/EDx;->aB:Z

    move v0, v2

    .line 2097865
    if-nez v0, :cond_3

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->t()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2097866
    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/EGe;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 2097867
    goto :goto_0

    .line 2097868
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static X(LX/EGe;)V
    .locals 2

    .prologue
    .line 2097856
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    .line 2097857
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v1}, LX/EI9;->getPeerView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EDx;->a(Landroid/view/View;)V

    .line 2097858
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097859
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bs()V

    .line 2097860
    :cond_0
    return-void
.end method

.method public static Y(LX/EGe;)V
    .locals 2

    .prologue
    .line 2097853
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    .line 2097854
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v1}, LX/EI9;->getPeerView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EDx;->b(Landroid/view/View;)V

    .line 2097855
    :cond_0
    return-void
.end method

.method public static Z(LX/EGe;)V
    .locals 2

    .prologue
    .line 2097841
    monitor-enter p0

    .line 2097842
    :try_start_0
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getSelfTextureView()Landroid/view/TextureView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/TextureView;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097843
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->X()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2097844
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getSelfTextureView()Landroid/view/TextureView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-static {v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Landroid/graphics/SurfaceTexture;)V

    .line 2097845
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getSelfTextureView()Landroid/view/TextureView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 2097846
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2097847
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2097848
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bs()V

    .line 2097849
    :cond_1
    return-void

    .line 2097850
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->V()LX/EHZ;

    move-result-object v0

    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v1}, LX/EI9;->getSelfTextureView()Landroid/view/TextureView;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EHZ;->a(Landroid/view/TextureView;)V

    .line 2097851
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/EDv;->STARTED:LX/EDv;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EDv;)V

    goto :goto_0

    .line 2097852
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 2097840
    invoke-virtual {p0}, LX/EGe;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    float-to-int v0, v0

    mul-int/2addr v0, p1

    return v0
.end method

.method private static a(LX/EGe;LX/0Or;LX/0Ot;LX/0Ot;Landroid/view/WindowManager;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/D9W;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2S7;LX/0ad;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ScheduledExecutorService;LX/6c4;LX/0So;LX/1Ml;LX/3A0;LX/3Eb;LX/EDC;LX/0Uh;LX/EFs;LX/0Ot;LX/EFq;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EGe;",
            "LX/0Or",
            "<",
            "LX/1r1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EFj;",
            ">;",
            "Landroid/view/WindowManager;",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/D9W;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2S7;",
            "LX/0ad;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/6c4;",
            "LX/0So;",
            "LX/1Ml;",
            "LX/3A0;",
            "LX/3Eb;",
            "LX/EDC;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/EFs;",
            "LX/0Ot",
            "<",
            "LX/EFk;",
            ">;",
            "LX/EFq;",
            "LX/0Ot",
            "<",
            "LX/EDT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2097839
    iput-object p1, p0, LX/EGe;->a:LX/0Or;

    iput-object p2, p0, LX/EGe;->y:LX/0Ot;

    iput-object p3, p0, LX/EGe;->z:LX/0Ot;

    iput-object p4, p0, LX/EGe;->c:Landroid/view/WindowManager;

    iput-object p5, p0, LX/EGe;->d:Landroid/content/Context;

    iput-object p6, p0, LX/EGe;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object p7, p0, LX/EGe;->f:LX/D9W;

    iput-object p8, p0, LX/EGe;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p9, p0, LX/EGe;->h:LX/2S7;

    iput-object p10, p0, LX/EGe;->i:LX/0ad;

    iput-object p11, p0, LX/EGe;->j:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p12, p0, LX/EGe;->k:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p13, p0, LX/EGe;->l:LX/6c4;

    iput-object p14, p0, LX/EGe;->m:LX/0So;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/EGe;->n:LX/1Ml;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/EGe;->o:LX/3A0;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/EGe;->p:LX/3Eb;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/EGe;->q:LX/EDC;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/EGe;->r:LX/0Uh;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/EGe;->s:LX/EFs;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/EGe;->B:LX/0Ot;

    move-object/from16 v0, p22

    iput-object v0, p0, LX/EGe;->ah:LX/EFq;

    move-object/from16 v0, p23

    iput-object v0, p0, LX/EGe;->ai:LX/0Ot;

    return-void
.end method

.method public static a(LX/EGe;Landroid/view/View;Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2097823
    invoke-direct {p0, p1}, LX/EGe;->a(Landroid/view/View;)V

    .line 2097824
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    if-eqz v0, :cond_2

    .line 2097825
    :goto_0
    invoke-static {p0}, LX/EGe;->an(LX/EGe;)V

    .line 2097826
    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    .line 2097827
    iget-object v1, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v1, v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->setMessage(Landroid/text/Spanned;)V

    .line 2097828
    if-eq p3, v2, :cond_0

    .line 2097829
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    .line 2097830
    iput p3, v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->n:I

    .line 2097831
    :cond_0
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2097832
    if-ne p3, v2, :cond_1

    .line 2097833
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->e()V

    .line 2097834
    :cond_1
    return-void

    .line 2097835
    :cond_2
    new-instance v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    iget-object v1, p0, LX/EGe;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    .line 2097836
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->setMaxLines(I)V

    .line 2097837
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    new-instance v1, LX/EGP;

    invoke-direct {v1, p0}, LX/EGP;-><init>(LX/EGe;)V

    invoke-virtual {v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2097838
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    iget-object v1, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    new-instance p1, LX/EGQ;

    invoke-direct {p1, p0}, LX/EGQ;-><init>(LX/EGe;)V

    invoke-virtual {v0, v1, p1}, LX/EFs;->a(Landroid/view/View;LX/EFr;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2097817
    iget-object v0, p0, LX/EGe;->R:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2097818
    iget-object v0, p0, LX/EGe;->R:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 2097819
    instance-of v1, v0, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v1, :cond_0

    .line 2097820
    iget-object v1, p0, LX/EGe;->R:Landroid/view/View;

    check-cast v0, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v1, v0}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2097821
    :cond_0
    iput-object p1, p0, LX/EGe;->R:Landroid/view/View;

    .line 2097822
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 27

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v25

    move-object/from16 v2, p0

    check-cast v2, LX/EGe;

    const/16 v3, 0x390

    move-object/from16 v0, v25

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x3257

    move-object/from16 v0, v25

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x325a

    move-object/from16 v0, v25

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {v25 .. v25}, LX/0q4;->a(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    const-class v7, Landroid/content/Context;

    move-object/from16 v0, v25

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static/range {v25 .. v25}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    const-class v9, LX/D9W;

    move-object/from16 v0, v25

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/D9W;

    invoke-static/range {v25 .. v25}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v25 .. v25}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object v11

    check-cast v11, LX/2S7;

    invoke-static/range {v25 .. v25}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static/range {v25 .. v25}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {v25 .. v25}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v14

    check-cast v14, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {v25 .. v25}, LX/6c4;->a(LX/0QB;)LX/6c4;

    move-result-object v15

    check-cast v15, LX/6c4;

    invoke-static/range {v25 .. v25}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v16

    check-cast v16, LX/0So;

    invoke-static/range {v25 .. v25}, LX/1Ml;->a(LX/0QB;)LX/1Ml;

    move-result-object v17

    check-cast v17, LX/1Ml;

    invoke-static/range {v25 .. v25}, LX/3A0;->a(LX/0QB;)LX/3A0;

    move-result-object v18

    check-cast v18, LX/3A0;

    invoke-static/range {v25 .. v25}, LX/3Eb;->a(LX/0QB;)LX/3Eb;

    move-result-object v19

    check-cast v19, LX/3Eb;

    invoke-static/range {v25 .. v25}, LX/EDC;->a(LX/0QB;)LX/EDC;

    move-result-object v20

    check-cast v20, LX/EDC;

    invoke-static/range {v25 .. v25}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v21

    check-cast v21, LX/0Uh;

    invoke-static/range {v25 .. v25}, LX/EFs;->a(LX/0QB;)LX/EFs;

    move-result-object v22

    check-cast v22, LX/EFs;

    const/16 v23, 0x325b

    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const-class v24, LX/EFq;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/EFq;

    const/16 v26, 0x3256

    invoke-static/range {v25 .. v26}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v25

    invoke-static/range {v2 .. v25}, LX/EGe;->a(LX/EGe;LX/0Or;LX/0Ot;LX/0Ot;Landroid/view/WindowManager;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/D9W;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2S7;LX/0ad;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ScheduledExecutorService;LX/6c4;LX/0So;LX/1Ml;LX/3A0;LX/3Eb;LX/EDC;LX/0Uh;LX/EFs;LX/0Ot;LX/EFq;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(LX/EGe;IIZ)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    const v5, 0x3fb33333    # 1.4f

    .line 2097789
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-nez v0, :cond_0

    .line 2097790
    :goto_0
    return-void

    .line 2097791
    :cond_0
    invoke-static {p0}, LX/EGe;->F(LX/EGe;)I

    move-result v2

    .line 2097792
    invoke-virtual {p0}, LX/EGe;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2097793
    int-to-float v0, v2

    mul-float/2addr v0, v5

    float-to-int p2, v0

    move p1, v2

    .line 2097794
    :cond_1
    :goto_1
    if-eqz p3, :cond_6

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->h()I

    move-result v0

    .line 2097795
    :goto_2
    if-ne v0, v7, :cond_2

    .line 2097796
    add-int/lit8 v0, v0, 0x1

    .line 2097797
    :cond_2
    mul-int/lit8 v1, v0, 0x3c

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2097798
    if-le p1, p2, :cond_3

    .line 2097799
    add-int/lit8 v1, v1, 0x1e

    .line 2097800
    :cond_3
    invoke-direct {p0, v1}, LX/EGe;->a(I)I

    move-result v4

    iput v4, p0, LX/EGe;->I:I

    .line 2097801
    if-le p1, p2, :cond_8

    .line 2097802
    if-le v1, v2, :cond_7

    .line 2097803
    iget v2, p0, LX/EGe;->I:I

    .line 2097804
    iget v1, p0, LX/EGe;->I:I

    int-to-float v1, v1

    const v4, 0x3f36db6e

    mul-float/2addr v1, v4

    float-to-int v1, v1

    .line 2097805
    :goto_3
    invoke-static {p0, v2, v1}, LX/EGe;->b(LX/EGe;II)V

    .line 2097806
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    iget v2, p0, LX/EGe;->F:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x3

    iget v3, p0, LX/EGe;->G:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, LX/EGe;->H:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    goto :goto_0

    .line 2097807
    :cond_4
    invoke-static {p0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2097808
    invoke-virtual {p0}, LX/EGe;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v6, :cond_5

    .line 2097809
    int-to-float v0, v2

    mul-float/2addr v0, v5

    float-to-int p2, v0

    move p1, v2

    goto/16 :goto_1

    .line 2097810
    :cond_5
    int-to-float v0, v2

    mul-float/2addr v0, v5

    float-to-int p1, v0

    move p2, v2

    .line 2097811
    goto/16 :goto_1

    :cond_6
    move v0, v3

    .line 2097812
    goto/16 :goto_2

    .line 2097813
    :cond_7
    iget v1, p0, LX/EGe;->I:I

    int-to-float v1, v1

    mul-float/2addr v1, v5

    float-to-int v2, v1

    .line 2097814
    iget v1, p0, LX/EGe;->I:I

    goto :goto_3

    .line 2097815
    :cond_8
    iget v1, p0, LX/EGe;->I:I

    int-to-float v1, v1

    mul-float/2addr v1, v5

    float-to-int v1, v1

    .line 2097816
    iget v2, p0, LX/EGe;->I:I

    goto :goto_3
.end method

.method public static a$redex0(LX/EGe;ZZZ)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x64

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2098309
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2098310
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2098311
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 2098312
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/EI9;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2098313
    :cond_0
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getMeasuredHeight()I

    move-result v0

    .line 2098314
    iget-object v3, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v3}, LX/EI9;->getMeasuredWidth()I

    move-result v3

    .line 2098315
    if-eqz p1, :cond_6

    if-lez v0, :cond_6

    if-lez v3, :cond_6

    iget v4, p0, LX/EGe;->G:I

    if-ne v0, v4, :cond_1

    iget v4, p0, LX/EGe;->F:I

    if-eq v3, v4, :cond_6

    .line 2098316
    :cond_1
    new-instance v4, LX/EGc;

    iget-object v5, p0, LX/EGe;->D:LX/EI9;

    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    new-instance v3, Landroid/graphics/Point;

    iget v7, p0, LX/EGe;->F:I

    iget v8, p0, LX/EGe;->G:I

    invoke-direct {v3, v7, v8}, Landroid/graphics/Point;-><init>(II)V

    invoke-direct {v4, v5, v6, v3}, LX/EGc;-><init>(Landroid/view/View;Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 2098317
    invoke-virtual {v4, v10, v11}, LX/EGc;->setDuration(J)V

    .line 2098318
    iget v3, p0, LX/EGe;->G:I

    if-ge v0, v3, :cond_5

    move v0, v1

    .line 2098319
    :goto_1
    if-nez p2, :cond_3

    .line 2098320
    iget-object v3, p0, LX/EGe;->D:LX/EI9;

    invoke-static {p0}, LX/EGe;->W(LX/EGe;)Z

    move-result v5

    if-eqz p3, :cond_2

    invoke-static {p0}, LX/EGe;->V(LX/EGe;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v2, v1

    :cond_2
    invoke-virtual {v3, p2, v1, v5, v2}, LX/EI9;->a(ZZZZ)V

    .line 2098321
    invoke-virtual {v4, v10, v11}, LX/EGc;->setStartTime(J)V

    .line 2098322
    :cond_3
    new-instance v1, LX/EGN;

    invoke-direct {v1, p0, v0, p2}, LX/EGN;-><init>(LX/EGe;ZZ)V

    invoke-virtual {v4, v1}, LX/EGc;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2098323
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0, v4}, LX/EI9;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2098324
    :goto_2
    return-void

    :cond_4
    move v0, v2

    .line 2098325
    goto :goto_0

    :cond_5
    move v0, v2

    .line 2098326
    goto :goto_1

    .line 2098327
    :cond_6
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v2, p0, LX/EGe;->F:I

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2098328
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v2, p0, LX/EGe;->G:I

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2098329
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->requestLayout()V

    .line 2098330
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-static {p0}, LX/EGe;->W(LX/EGe;)Z

    move-result v2

    invoke-static {p0}, LX/EGe;->V(LX/EGe;)Z

    move-result v3

    invoke-virtual {v0, p2, v1, v2, v3}, LX/EI9;->a(ZZZZ)V

    goto :goto_2
.end method

.method public static aB(LX/EGe;)Z
    .locals 12

    .prologue
    .line 2098020
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    .line 2098021
    iget-boolean v1, v0, LX/EFs;->d:Z

    move v0, v1

    .line 2098022
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EGe;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDT;

    sget-object v1, LX/EDS;->VIDEO_CHAT_HEAD:LX/EDS;

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2098023
    iget-object v4, v0, LX/EDT;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    .line 2098024
    iget-object v7, v4, LX/EDx;->bl:LX/7TQ;

    move-object v4, v7

    .line 2098025
    sget-object v7, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    if-ne v4, v7, :cond_0

    iget-object v4, v0, LX/EDT;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    invoke-virtual {v4}, LX/EDx;->ag()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, LX/EDT;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    invoke-virtual {v4}, LX/EDx;->j()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v0, LX/EDT;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    .line 2098026
    iget v7, v4, LX/EDx;->ah:I

    move v4, v7

    .line 2098027
    if-gt v4, v5, :cond_2

    .line 2098028
    :cond_0
    :goto_0
    move v0, v6

    .line 2098029
    if-eqz v0, :cond_1

    .line 2098030
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, LX/EGe;->ai:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDT;

    sget-object v2, LX/EDS;->VIDEO_CHAT_HEAD:LX/EDS;

    invoke-virtual {v1, v2}, LX/EDT;->b(LX/EDS;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/EDx;->a(J)V

    .line 2098031
    const/4 v0, 0x1

    .line 2098032
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2098033
    :cond_2
    sget-object v4, LX/EDS;->VIDEO_CHAT_HEAD:LX/EDS;

    if-ne v1, v4, :cond_3

    .line 2098034
    iget-object v4, v0, LX/EDT;->k:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ad;

    sget-short v7, LX/3Dx;->dZ:S

    invoke-interface {v4, v7, v6}, LX/0ad;->a(SZ)Z

    move-result v7

    .line 2098035
    iget-object v4, v0, LX/EDT;->k:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ad;

    sget-short v8, LX/3Dx;->dY:S

    invoke-interface {v4, v8, v6}, LX/0ad;->a(SZ)Z

    move-result v8

    .line 2098036
    if-eqz v7, :cond_0

    .line 2098037
    iget-object v4, v0, LX/EDT;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    invoke-virtual {v4}, LX/EDx;->q()Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v8, :cond_0

    .line 2098038
    :cond_3
    iget-object v4, v0, LX/EDT;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    .line 2098039
    iget-boolean v7, v4, LX/EDx;->bQ:Z

    move v4, v7

    .line 2098040
    if-nez v4, :cond_0

    .line 2098041
    iget-object v4, v0, LX/EDT;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    .line 2098042
    iget-wide v10, v4, LX/EDx;->ac:J

    move-wide v8, v10

    .line 2098043
    iput-wide v8, v0, LX/EDT;->g:J

    .line 2098044
    iget-object v4, v0, LX/EDT;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    invoke-virtual {v4}, LX/EDx;->aJ()Z

    move-result v4

    iput-boolean v4, v0, LX/EDT;->h:Z

    .line 2098045
    iget-object v4, v0, LX/EDT;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    invoke-virtual {v4}, LX/EDx;->h()Z

    move-result v4

    iput-boolean v4, v0, LX/EDT;->i:Z

    .line 2098046
    iget-object v4, v0, LX/EDT;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2S7;

    .line 2098047
    iget v7, v4, LX/2S7;->u:I

    move v4, v7

    .line 2098048
    if-eqz v4, :cond_4

    move v4, v5

    .line 2098049
    :goto_2
    iget-object v7, v0, LX/EDT;->c:LX/EDO;

    iget-boolean v8, v0, LX/EDT;->i:Z

    invoke-virtual {v7, v4, v8}, LX/EDO;->a(ZZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2098050
    iget-object v4, v0, LX/EDT;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2S7;

    const-string v6, "rating_shown"

    const-string v7, "1"

    invoke-virtual {v4, v6, v7}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v5

    .line 2098051
    goto/16 :goto_0

    :cond_4
    move v4, v6

    .line 2098052
    goto :goto_2
.end method

.method public static aC(LX/EGe;)V
    .locals 4

    .prologue
    .line 2098300
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    iget-object v1, p0, LX/EGe;->W:LX/EHn;

    const/16 v2, 0x80

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/EFs;->a(Landroid/view/View;IZ)V

    .line 2098301
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    .line 2098302
    iget-boolean v1, v0, LX/EFs;->d:Z

    move v0, v1

    .line 2098303
    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/EGe;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2098304
    iget-object v0, p0, LX/EGe;->ak:LX/1ql;

    if-nez v0, :cond_0

    .line 2098305
    iget-object v0, p0, LX/EGe;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1r1;

    .line 2098306
    const v1, 0x3000000a

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    iput-object v0, p0, LX/EGe;->ak:LX/1ql;

    .line 2098307
    :cond_0
    iget-object v0, p0, LX/EGe;->ak:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->c()V

    .line 2098308
    :cond_1
    return-void
.end method

.method public static aD(LX/EGe;)V
    .locals 4

    .prologue
    .line 2098296
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    iget-object v1, p0, LX/EGe;->W:LX/EHn;

    const/16 v2, 0x80

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/EFs;->a(Landroid/view/View;IZ)V

    .line 2098297
    iget-object v0, p0, LX/EGe;->ak:LX/1ql;

    if-eqz v0, :cond_0

    .line 2098298
    iget-object v0, p0, LX/EGe;->ak:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 2098299
    :cond_0
    return-void
.end method

.method public static aF(LX/EGe;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2098286
    iget-boolean v0, p0, LX/EGe;->P:Z

    move v0, v0

    .line 2098287
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2098288
    :cond_0
    :goto_0
    return-void

    .line 2098289
    :cond_1
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v2, LX/EG5;->ChatHead:LX/EG5;

    invoke-virtual {v0, v2}, LX/EDx;->a(LX/EG5;)LX/EFy;

    move-result-object v2

    .line 2098290
    if-eqz v2, :cond_0

    .line 2098291
    invoke-static {p0}, LX/EGe;->al(LX/EGe;)V

    .line 2098292
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_2

    .line 2098293
    iget-object v3, p0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->A()Z

    move-result v4

    invoke-static {p0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v4, v0}, LX/EI9;->a(ZZ)V

    .line 2098294
    :cond_2
    invoke-virtual {v2, v1}, LX/EFy;->a(Z)V

    goto :goto_0

    .line 2098295
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static aG(LX/EGe;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2098263
    iget-boolean v0, p0, LX/EGe;->P:Z

    move v0, v0

    .line 2098264
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-nez v0, :cond_1

    .line 2098265
    :cond_0
    :goto_0
    return-void

    .line 2098266
    :cond_1
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/EG5;->ChatHead:LX/EG5;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EG5;)LX/EFy;

    move-result-object v1

    .line 2098267
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098268
    iget-object v2, v0, LX/EDx;->af:LX/EGE;

    move-object v0, v2

    .line 2098269
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2098270
    iget-boolean v2, v0, LX/EGE;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2098271
    iget-boolean v2, v0, LX/EGE;->i:Z

    if-eqz v2, :cond_5

    .line 2098272
    iget-wide v2, v0, LX/EGE;->h:J

    invoke-virtual {v1, v2, v3, v4}, LX/EFy;->b(JLandroid/view/View;)V

    .line 2098273
    iget-wide v2, v0, LX/EGE;->h:J

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getPeerView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/EFy;->b(JLandroid/view/View;)V

    .line 2098274
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2098275
    :cond_2
    iget v0, p0, LX/EGe;->F:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_4

    .line 2098276
    iget v0, p0, LX/EGe;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EGe;->F:I

    .line 2098277
    :goto_1
    iget v0, p0, LX/EGe;->F:I

    iget v1, p0, LX/EGe;->F:I

    int-to-float v1, v1

    iget v2, p0, LX/EGe;->H:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {p0, v0, v1}, LX/EGe;->b(LX/EGe;II)V

    .line 2098278
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, LX/EGe;->F:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2098279
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, LX/EGe;->G:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2098280
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->requestLayout()V

    .line 2098281
    :cond_3
    :goto_2
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    .line 2098282
    invoke-virtual {v0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EI9;->setViewType(LX/EI8;)V

    .line 2098283
    goto/16 :goto_0

    .line 2098284
    :cond_4
    iget v0, p0, LX/EGe;->F:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/EGe;->F:I

    goto :goto_1

    .line 2098285
    :cond_5
    iget-wide v2, v0, LX/EGE;->h:J

    invoke-virtual {v1, v2, v3, v4}, LX/EFy;->b(JLandroid/view/View;)V

    goto :goto_2
.end method

.method public static declared-synchronized aa(LX/EGe;)V
    .locals 2

    .prologue
    .line 2098257
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2098258
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->X()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2098259
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->V()LX/EHZ;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EHZ;->a(Landroid/view/TextureView;)V

    .line 2098260
    :cond_0
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/EDv;->STOPPED:LX/EDv;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EDv;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2098261
    :cond_1
    monitor-exit p0

    return-void

    .line 2098262
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static af(LX/EGe;)V
    .locals 3

    .prologue
    .line 2098250
    invoke-static {p0}, LX/EGe;->am(LX/EGe;)V

    .line 2098251
    invoke-static {p0}, LX/EGe;->ah(LX/EGe;)V

    .line 2098252
    invoke-static {p0}, LX/EGe;->al(LX/EGe;)V

    .line 2098253
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    .line 2098254
    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->A()Z

    move-result v2

    invoke-static {p0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/EI9;->a(ZZ)V

    .line 2098255
    :cond_0
    return-void

    .line 2098256
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ag(LX/EGe;)Z
    .locals 2

    .prologue
    .line 2098245
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098246
    iget-boolean v1, v0, LX/EDx;->cg:Z

    move v0, v1

    .line 2098247
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    .line 2098248
    iget-boolean v1, v0, LX/EFs;->d:Z

    move v0, v1

    .line 2098249
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ah(LX/EGe;)V
    .locals 4

    .prologue
    .line 2098231
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/EGe;->P:Z

    if-nez v0, :cond_1

    .line 2098232
    :cond_0
    :goto_0
    return-void

    .line 2098233
    :cond_1
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    .line 2098234
    iget-boolean v1, v0, LX/EI9;->z:Z

    move v0, v1

    .line 2098235
    if-nez v0, :cond_0

    .line 2098236
    invoke-static {p0}, LX/EGe;->N$redex0(LX/EGe;)V

    .line 2098237
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    new-instance v1, Landroid/graphics/Point;

    .line 2098238
    iget v2, p0, LX/EGe;->F:I

    move v2, v2

    .line 2098239
    iget v3, p0, LX/EGe;->G:I

    move v3, v3

    .line 2098240
    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    iget v2, p0, LX/EGe;->H:F

    .line 2098241
    iput-object v1, v0, LX/EI9;->w:Landroid/graphics/Point;

    .line 2098242
    iput v2, v0, LX/EI9;->x:F

    .line 2098243
    const/4 v3, 0x1

    invoke-static {v0, v3}, LX/EI9;->c(LX/EI9;Z)V

    .line 2098244
    invoke-static {p0}, LX/EGe;->an(LX/EGe;)V

    goto :goto_0
.end method

.method public static ai(LX/EGe;)Z
    .locals 2

    .prologue
    .line 2098228
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098229
    iget-boolean v1, v0, LX/EDx;->aB:Z

    move v0, v1

    .line 2098230
    if-eqz v0, :cond_0

    invoke-static {p0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static aj(LX/EGe;)Z
    .locals 2

    .prologue
    .line 2098225
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098226
    iget-boolean v1, v0, LX/EDx;->aB:Z

    move v0, v1

    .line 2098227
    if-nez v0, :cond_0

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static al(LX/EGe;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2098176
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/EGe;->P:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/EGe;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2098177
    :cond_0
    :goto_0
    return-void

    .line 2098178
    :cond_1
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->j()Z

    move-result v1

    .line 2098179
    invoke-static {p0}, LX/EGe;->ai(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2098180
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    sget-object v3, LX/EI8;->OUTGOING_INSTANT:LX/EI8;

    invoke-virtual {v0, v3}, LX/EI9;->setViewType(LX/EI8;)V

    .line 2098181
    if-nez v1, :cond_2

    .line 2098182
    iget-boolean v0, p0, LX/EGe;->ae:Z

    invoke-static {p0, v0, v2}, LX/EGe;->b(LX/EGe;ZZ)V

    .line 2098183
    :cond_2
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098184
    iget-boolean v3, v0, LX/EDx;->ce:Z

    move v0, v3

    .line 2098185
    if-eqz v0, :cond_11

    .line 2098186
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    .line 2098187
    iget-boolean v3, v0, LX/EI9;->N:Z

    move v0, v3

    .line 2098188
    if-nez v0, :cond_11

    .line 2098189
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2098190
    iget-object v3, p0, LX/EGe;->q:LX/EDC;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098191
    iget-wide v6, v0, LX/EDx;->ak:J

    move-wide v4, v6

    .line 2098192
    sget-object v0, LX/EDB;->INSTANT_VIDEO_STARTED:LX/EDB;

    invoke-virtual {v3, v4, v5, v2, v0}, LX/EDC;->a(JZLX/EDB;)Z

    .line 2098193
    :cond_3
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    .line 2098194
    iget-boolean v3, v0, LX/EI9;->N:Z

    if-eq v3, v2, :cond_4

    .line 2098195
    iput-boolean v2, v0, LX/EI9;->N:Z

    .line 2098196
    iget-object v3, v0, LX/EI9;->a:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/facebook/rtc/views/RtcVideoChatHeadView$1;

    invoke-direct {v4, v0}, Lcom/facebook/rtc/views/RtcVideoChatHeadView$1;-><init>(LX/EI9;)V

    const v5, -0x6fb606e3

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2098197
    :cond_4
    move v0, v1

    .line 2098198
    :goto_1
    if-eqz v0, :cond_5

    .line 2098199
    iget-boolean v1, p0, LX/EGe;->ae:Z

    invoke-static {p0, v1, v0}, LX/EGe;->b(LX/EGe;ZZ)V

    .line 2098200
    :cond_5
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getViewType()LX/EI8;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098201
    iget-boolean v1, v0, LX/EDx;->ce:Z

    move v0, v1

    .line 2098202
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2098203
    iput-boolean v2, p0, LX/EGe;->ae:Z

    goto/16 :goto_0

    .line 2098204
    :cond_6
    invoke-static {p0}, LX/EGe;->aj(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2098205
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    sget-object v1, LX/EI8;->INCOMING_INSTANT:LX/EI8;

    invoke-virtual {v0, v1}, LX/EI9;->setViewType(LX/EI8;)V

    move v0, v2

    .line 2098206
    goto :goto_1

    .line 2098207
    :cond_7
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098208
    iget-boolean v4, v0, LX/EDx;->bZ:Z

    move v0, v4

    .line 2098209
    if-eqz v0, :cond_8

    .line 2098210
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    sget-object v1, LX/EI8;->GROUP_COUNTDOWN:LX/EI8;

    invoke-virtual {v0, v1}, LX/EI9;->setViewType(LX/EI8;)V

    move v0, v3

    .line 2098211
    goto :goto_1

    .line 2098212
    :cond_8
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aR()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2098213
    :cond_9
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    sget-object v1, LX/EI8;->NONE:LX/EI8;

    invoke-virtual {v0, v1}, LX/EI9;->setViewType(LX/EI8;)V

    move v0, v2

    .line 2098214
    goto :goto_1

    .line 2098215
    :cond_a
    invoke-static {p0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {p0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2098216
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    sget-object v3, LX/EI8;->BOTH:LX/EI8;

    invoke-virtual {v0, v3}, LX/EI9;->setViewType(LX/EI8;)V

    move v0, v1

    goto/16 :goto_1

    .line 2098217
    :cond_b
    invoke-static {p0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {p0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2098218
    :cond_c
    iget-object v3, p0, LX/EGe;->D:LX/EI9;

    invoke-static {p0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, LX/EI8;->PEER:LX/EI8;

    :goto_2
    invoke-virtual {v3, v0}, LX/EI9;->setViewType(LX/EI8;)V

    move v0, v1

    goto/16 :goto_1

    :cond_d
    sget-object v0, LX/EI8;->SELF:LX/EI8;

    goto :goto_2

    .line 2098219
    :cond_e
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2098220
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    sget-object v1, LX/EI8;->END_CALL_STATE_VOICEMAIL:LX/EI8;

    invoke-virtual {v0, v1}, LX/EI9;->setViewType(LX/EI8;)V

    move v0, v2

    .line 2098221
    goto/16 :goto_1

    .line 2098222
    :cond_f
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->af()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, LX/EGe;->U:Z

    if-nez v0, :cond_10

    .line 2098223
    invoke-virtual {p0}, LX/EGe;->z()V

    move v0, v3

    goto/16 :goto_1

    .line 2098224
    :cond_10
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    sget-object v3, LX/EI8;->NONE:LX/EI8;

    invoke-virtual {v0, v3}, LX/EI9;->setViewType(LX/EI8;)V

    :cond_11
    move v0, v1

    goto/16 :goto_1
.end method

.method public static am(LX/EGe;)V
    .locals 4

    .prologue
    .line 2098169
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    if-eqz v0, :cond_0

    .line 2098170
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    iget-object v1, p0, LX/EGe;->W:LX/EHn;

    iget-object v2, p0, LX/EGe;->ag:LX/EFp;

    .line 2098171
    iget v3, v2, LX/EFp;->h:I

    move v2, v3

    .line 2098172
    iget-object v3, p0, LX/EGe;->ag:LX/EFp;

    .line 2098173
    iget p0, v3, LX/EFp;->i:I

    move v3, p0

    .line 2098174
    invoke-virtual {v0, v1, v2, v3}, LX/EFs;->a(Landroid/view/View;II)V

    .line 2098175
    :cond_0
    return-void
.end method

.method public static an(LX/EGe;)V
    .locals 9

    .prologue
    .line 2098116
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    if-nez v0, :cond_1

    .line 2098117
    :cond_0
    :goto_0
    return-void

    .line 2098118
    :cond_1
    const/4 v0, 0x6

    invoke-direct {p0, v0}, LX/EGe;->a(I)I

    move-result v5

    .line 2098119
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098120
    iget-object v1, v0, LX/EDx;->ck:LX/EDt;

    move-object v0, v1

    .line 2098121
    invoke-virtual {v0}, LX/EDt;->isLeft()Z

    move-result v6

    .line 2098122
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    iget-object v1, p0, LX/EGe;->W:LX/EHn;

    .line 2098123
    iget-object v2, v0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$LayoutParams;

    .line 2098124
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2098125
    iget-boolean v3, v0, LX/EFs;->d:Z

    move v3, v3

    .line 2098126
    if-eqz v3, :cond_8

    .line 2098127
    check-cast v2, Landroid/view/WindowManager$LayoutParams;

    .line 2098128
    new-instance v3, Landroid/graphics/Point;

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-direct {v3, v4, v2}, Landroid/graphics/Point;-><init>(II)V

    move-object v2, v3

    .line 2098129
    :goto_1
    move-object v0, v2

    .line 2098130
    iget v4, v0, Landroid/graphics/Point;->x:I

    .line 2098131
    iget v3, v0, Landroid/graphics/Point;->y:I

    .line 2098132
    iget v0, p0, LX/EGe;->F:I

    move v2, v0

    .line 2098133
    iget v0, p0, LX/EGe;->G:I

    move v1, v0

    .line 2098134
    iget-object v0, p0, LX/EGe;->W:LX/EHn;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EGe;->W:LX/EHn;

    invoke-virtual {v0}, LX/EHn;->getPaddingTop()I

    move-result v0

    .line 2098135
    :goto_2
    iget-object v7, p0, LX/EGe;->R:Landroid/view/View;

    if-eqz v7, :cond_7

    .line 2098136
    iget-object v7, p0, LX/EGe;->R:Landroid/view/View;

    .line 2098137
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2098138
    invoke-virtual {v7, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2098139
    invoke-virtual {v7}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2098140
    new-instance v0, LX/EGO;

    invoke-direct {v0, p0, v7}, LX/EGO;-><init>(LX/EGe;Landroid/view/View;)V

    .line 2098141
    invoke-virtual {v7, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2098142
    invoke-virtual {v7}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 2098143
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 2098144
    :cond_3
    iget-object v2, p0, LX/EGe;->s:LX/EFs;

    .line 2098145
    iget-boolean v8, v2, LX/EFs;->d:Z

    move v2, v8

    .line 2098146
    if-eqz v2, :cond_5

    .line 2098147
    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v4

    .line 2098148
    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v3

    .line 2098149
    :goto_3
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 2098150
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v3

    move v8, v3

    move v3, v1

    move v1, v4

    move v4, v2

    move v2, v8

    .line 2098151
    :goto_4
    if-eqz v6, :cond_6

    .line 2098152
    add-int/2addr v1, v4

    .line 2098153
    iget-object v4, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    sget-object v5, LX/D9U;->LEFT:LX/D9U;

    invoke-virtual {v4, v5}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->setOrigin(LX/D9U;)V

    .line 2098154
    iget-object v4, p0, LX/EGe;->s:LX/EFs;

    iget-object v5, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    const/16 v6, 0x33

    invoke-virtual {v4, v5, v6}, LX/EFs;->a(Landroid/view/View;I)V

    .line 2098155
    :goto_5
    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 2098156
    iget-object v2, p0, LX/EGe;->s:LX/EFs;

    .line 2098157
    iget-boolean v3, v2, LX/EFs;->d:Z

    move v2, v3

    .line 2098158
    if-nez v2, :cond_4

    .line 2098159
    iget v2, p0, LX/EGe;->L:I

    sub-int/2addr v0, v2

    .line 2098160
    :cond_4
    iget-object v2, p0, LX/EGe;->s:LX/EFs;

    iget-object v3, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v2, v3, v1, v0}, LX/EFs;->a(Landroid/view/View;II)V

    goto/16 :goto_0

    .line 2098161
    :cond_5
    iget v2, v1, Landroid/graphics/Rect;->left:I

    .line 2098162
    iget v1, v1, Landroid/graphics/Rect;->top:I

    goto :goto_3

    .line 2098163
    :cond_6
    iget-object v1, p0, LX/EGe;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 2098164
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v1, v4

    sub-int/2addr v1, v5

    .line 2098165
    iget-object v4, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    sget-object v5, LX/D9U;->RIGHT:LX/D9U;

    invoke-virtual {v4, v5}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->setOrigin(LX/D9U;)V

    .line 2098166
    iget-object v4, p0, LX/EGe;->s:LX/EFs;

    iget-object v5, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    const/16 v6, 0x35

    invoke-virtual {v4, v5, v6}, LX/EFs;->a(Landroid/view/View;I)V

    goto :goto_5

    :cond_7
    move v8, v1

    move v1, v2

    move v2, v8

    goto :goto_4

    .line 2098167
    :cond_8
    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 2098168
    new-instance v3, Landroid/graphics/Point;

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-direct {v3, v4, v2}, Landroid/graphics/Point;-><init>(II)V

    move-object v2, v3

    goto/16 :goto_1
.end method

.method public static ap(LX/EGe;)V
    .locals 4

    .prologue
    .line 2098099
    iget-boolean v0, p0, LX/EGe;->P:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/EGe;->ar(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2098100
    :cond_0
    :goto_0
    return-void

    .line 2098101
    :cond_1
    iget-object v0, p0, LX/EGe;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/EDJ;->h:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 2098102
    const/4 v0, 0x3

    if-ge v1, v0, :cond_2

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/EGe;->M:Z

    if-nez v0, :cond_2

    .line 2098103
    const v0, 0x7f0807d4

    invoke-virtual {p0, v0}, LX/EGe;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2098104
    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-static {p0, v2, v0, v3}, LX/EGe;->a(LX/EGe;Landroid/view/View;Ljava/lang/String;I)V

    .line 2098105
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EGe;->M:Z

    .line 2098106
    iget-object v0, p0, LX/EGe;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/EDJ;->h:LX/0Tn;

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0

    .line 2098107
    :cond_2
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getExpressionButton()Landroid/view/View;

    move-result-object v0

    .line 2098108
    iget-boolean v1, p0, LX/EGe;->P:Z

    if-eqz v1, :cond_3

    if-nez v0, :cond_4

    .line 2098109
    :cond_3
    :goto_1
    goto :goto_0

    .line 2098110
    :cond_4
    iget-object v1, p0, LX/EGe;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/EDJ;->k:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 2098111
    const/4 v2, 0x3

    if-ge v1, v2, :cond_3

    iget-boolean v2, p0, LX/EGe;->N:Z

    if-nez v2, :cond_3

    .line 2098112
    const v2, 0x7f080806

    invoke-virtual {p0, v2}, LX/EGe;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2098113
    const/16 v3, 0x7530

    invoke-static {p0, v0, v2, v3}, LX/EGe;->a(LX/EGe;Landroid/view/View;Ljava/lang/String;I)V

    .line 2098114
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EGe;->N:Z

    .line 2098115
    iget-object v0, p0, LX/EGe;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/EDJ;->k:LX/0Tn;

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_1
.end method

.method public static ar(LX/EGe;)Z
    .locals 1

    .prologue
    .line 2097672
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    if-eqz v0, :cond_0

    .line 2097673
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->isShown()Z

    move-result v0

    .line 2097674
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static at(LX/EGe;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2098088
    invoke-direct {p0, v2}, LX/EGe;->a(Landroid/view/View;)V

    .line 2098089
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    if-eqz v0, :cond_2

    .line 2098090
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2098091
    iget-object v0, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2098092
    :cond_0
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    iget-object v1, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    .line 2098093
    iget-object v3, v0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2098094
    iget-boolean v3, v0, LX/EFs;->b:Z

    if-eqz v3, :cond_1

    .line 2098095
    invoke-static {v0, v1}, LX/EFs;->c(LX/EFs;Landroid/view/View;)V

    .line 2098096
    :cond_1
    iget-object v3, v0, LX/EFs;->e:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2098097
    iput-object v2, p0, LX/EGe;->Q:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    .line 2098098
    :cond_2
    return-void
.end method

.method public static av(LX/EGe;)V
    .locals 6

    .prologue
    .line 2098070
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-nez v0, :cond_0

    .line 2098071
    :goto_0
    return-void

    .line 2098072
    :cond_0
    iget-object v0, p0, LX/EGe;->ag:LX/EFp;

    const/4 v1, 0x0

    .line 2098073
    iput-boolean v1, v0, LX/EFp;->n:Z

    .line 2098074
    iget v2, v0, LX/EFp;->i:I

    invoke-static {v0, v2}, LX/EFp;->a$redex0(LX/EFp;I)I

    move-result v2

    iput v2, v0, LX/EFp;->i:I

    .line 2098075
    iget-object v2, v0, LX/EFp;->q:LX/0wd;

    if-eqz v2, :cond_1

    .line 2098076
    iget-object v2, v0, LX/EFp;->q:LX/0wd;

    iget v3, v0, LX/EFp;->i:I

    int-to-double v3, v3

    invoke-virtual {v2, v3, v4}, LX/0wd;->a(D)LX/0wd;

    .line 2098077
    iget-object v2, v0, LX/EFp;->q:LX/0wd;

    iget v3, v0, LX/EFp;->i:I

    int-to-double v3, v3

    invoke-virtual {v2, v3, v4}, LX/0wd;->b(D)LX/0wd;

    .line 2098078
    :cond_1
    iget-object v2, v0, LX/EFp;->p:LX/0wd;

    if-eqz v2, :cond_3

    .line 2098079
    iget-object v2, v0, LX/EFp;->g:LX/EGb;

    invoke-virtual {v2}, LX/EGb;->g()Landroid/graphics/Rect;

    move-result-object v2

    .line 2098080
    iget v3, v0, LX/EFp;->h:I

    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    if-le v3, v4, :cond_2

    const/4 v1, 0x1

    .line 2098081
    :cond_2
    if-eqz v1, :cond_4

    iget v1, v2, Landroid/graphics/Rect;->right:I

    .line 2098082
    :goto_1
    iget-object v2, v0, LX/EFp;->p:LX/0wd;

    iget v3, v0, LX/EFp;->h:I

    int-to-double v3, v3

    invoke-virtual {v2, v3, v4}, LX/0wd;->a(D)LX/0wd;

    .line 2098083
    iget-object v2, v0, LX/EFp;->p:LX/0wd;

    int-to-double v3, v1

    invoke-virtual {v2, v3, v4}, LX/0wd;->b(D)LX/0wd;

    .line 2098084
    iget v2, v0, LX/EFp;->h:I

    if-ne v2, v1, :cond_3

    .line 2098085
    iget-object v1, v0, LX/EFp;->g:LX/EGb;

    invoke-virtual {v1}, LX/EGb;->k()V

    .line 2098086
    :cond_3
    goto :goto_0

    .line 2098087
    :cond_4
    iget v1, v2, Landroid/graphics/Rect;->left:I

    goto :goto_1
.end method

.method public static aw(LX/EGe;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2098064
    iget-object v0, p0, LX/EGe;->t:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2098065
    iget-object v0, p0, LX/EGe;->t:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2098066
    const/4 v0, 0x0

    iput-object v0, p0, LX/EGe;->t:Ljava/util/concurrent/ScheduledFuture;

    .line 2098067
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    .line 2098068
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0, v1}, LX/EI9;->a(Z)V

    .line 2098069
    :cond_0
    return-void
.end method

.method public static ax(LX/EGe;)V
    .locals 2

    .prologue
    .line 2098060
    iget-object v0, p0, LX/EGe;->u:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2098061
    iget-object v0, p0, LX/EGe;->u:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2098062
    const/4 v0, 0x0

    iput-object v0, p0, LX/EGe;->u:Ljava/util/concurrent/ScheduledFuture;

    .line 2098063
    :cond_0
    return-void
.end method

.method private static b(LX/EGe;II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2097869
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2097870
    if-eqz p2, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2097871
    iput p1, p0, LX/EGe;->F:I

    .line 2097872
    iput p2, p0, LX/EGe;->G:I

    .line 2097873
    int-to-float v0, p2

    int-to-float v1, p1

    div-float/2addr v0, v1

    iput v0, p0, LX/EGe;->H:F

    .line 2097874
    return-void

    :cond_0
    move v0, v2

    .line 2097875
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2097876
    goto :goto_1
.end method

.method public static b(LX/EGe;ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2098053
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2098054
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/EGe;->P:Z

    if-nez v0, :cond_1

    .line 2098055
    :cond_0
    :goto_0
    return-void

    .line 2098056
    :cond_1
    iput-boolean p2, p0, LX/EGe;->X:Z

    .line 2098057
    iget v0, p0, LX/EGe;->F:I

    iget v1, p0, LX/EGe;->G:I

    iget-boolean v2, p0, LX/EGe;->X:Z

    invoke-static {p0, v0, v1, v2}, LX/EGe;->a$redex0(LX/EGe;IIZ)V

    .line 2098058
    invoke-static {p0, p1, p2, v3}, LX/EGe;->a$redex0(LX/EGe;ZZZ)V

    .line 2098059
    invoke-static {p0, v3}, LX/EGe;->d(LX/EGe;Z)V

    goto :goto_0
.end method

.method public static b$redex0(LX/EGe;Ljava/lang/String;)V
    .locals 12
    .param p0    # LX/EGe;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v8, 0x1388

    const/4 v6, 0x1

    .line 2097535
    iget-object v0, p0, LX/EGe;->m:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/EGe;->T:J

    sub-long v2, v0, v2

    .line 2097536
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097537
    iget-boolean v1, v0, LX/EDx;->cg:Z

    move v0, v1

    .line 2097538
    if-nez v0, :cond_2

    iget-wide v0, p0, LX/EGe;->T:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-ltz v0, :cond_2

    cmp-long v0, v2, v8

    if-gtz v0, :cond_2

    .line 2097539
    iget-object v0, p0, LX/EGe;->t:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 2097540
    invoke-static {p0}, LX/EGe;->aw(LX/EGe;)V

    .line 2097541
    :cond_0
    :goto_0
    return-void

    .line 2097542
    :cond_1
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    .line 2097543
    iget-object v0, p0, LX/EGe;->j:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/services/BackgroundVideoCallService$17;

    invoke-direct {v1, p0, p1}, Lcom/facebook/rtc/services/BackgroundVideoCallService$17;-><init>(LX/EGe;Ljava/lang/String;)V

    sub-long v2, v8, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/EGe;->t:Ljava/util/concurrent/ScheduledFuture;

    .line 2097544
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0, v6}, LX/EI9;->a(Z)V

    goto :goto_0

    .line 2097545
    :cond_2
    iput-boolean v6, p0, LX/EGe;->S:Z

    .line 2097546
    invoke-static {p0}, LX/EGe;->N$redex0(LX/EGe;)V

    .line 2097547
    invoke-virtual {p0}, LX/EGe;->v()Z

    .line 2097548
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2097549
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EDx;->h(Z)V

    .line 2097550
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.facebook.rtc.fbwebrtc.intent.action.SHOW_UI"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "CONTACT_ID"

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097551
    iget-wide v10, v0, LX/EDx;->ak:J

    move-wide v4, v10

    .line 2097552
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10010000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 2097553
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2097554
    invoke-virtual {v0, p1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2097555
    :cond_4
    iget-object v1, p0, LX/EGe;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/EGe;->d:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static c(LX/EGe;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2097648
    iget-boolean v0, p0, LX/EGe;->P:Z

    if-eqz v0, :cond_2

    .line 2097649
    invoke-static {p0}, LX/EGe;->aw(LX/EGe;)V

    .line 2097650
    invoke-static {p0, v4}, LX/EGe;->d(LX/EGe;Z)V

    .line 2097651
    iget-object v0, p0, LX/EGe;->W:LX/EHn;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LX/EHn;->setVisibility(I)V

    .line 2097652
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->g()V

    .line 2097653
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    sget-object v1, LX/EI8;->HIDDEN:LX/EI8;

    invoke-virtual {v0, v1}, LX/EI9;->setViewType(LX/EI8;)V

    .line 2097654
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    iget-object v1, p0, LX/EGe;->W:LX/EHn;

    const/16 v2, 0x10

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/EFs;->a(Landroid/view/View;IZ)V

    .line 2097655
    invoke-static {p0}, LX/EGe;->aD(LX/EGe;)V

    .line 2097656
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v4}, LX/EDx;->h(Z)V

    .line 2097657
    iget-boolean v0, p0, LX/EGe;->V:Z

    if-nez v0, :cond_0

    .line 2097658
    iget-object v0, p0, LX/EGe;->l:LX/6c4;

    .line 2097659
    new-instance v1, Landroid/content/Intent;

    sget-object v2, LX/3RB;->A:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2097660
    invoke-static {v0, v1}, LX/6c4;->a(LX/6c4;Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2097661
    :cond_0
    iput-boolean v4, p0, LX/EGe;->P:Z

    .line 2097662
    if-eqz p1, :cond_1

    .line 2097663
    invoke-static {p0}, LX/EGe;->Y(LX/EGe;)V

    .line 2097664
    invoke-static {p0}, LX/EGe;->aa(LX/EGe;)V

    .line 2097665
    :cond_1
    invoke-static {p0}, LX/EGe;->at(LX/EGe;)V

    .line 2097666
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0, v4, v4, v4, v4}, LX/EI9;->a(ZZZZ)V

    .line 2097667
    iget-object v0, p0, LX/EGe;->W:LX/EHn;

    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0, v1}, LX/EHn;->removeView(Landroid/view/View;)V

    .line 2097668
    iget-object v0, p0, LX/EGe;->W:LX/EHn;

    invoke-virtual {v0, v4}, LX/EHn;->setBackgroundResource(I)V

    .line 2097669
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097670
    iput-boolean v4, v0, LX/EDx;->aX:Z

    .line 2097671
    :cond_2
    return-void
.end method

.method public static d(LX/EGe;Z)V
    .locals 5

    .prologue
    .line 2097642
    iget-object v0, p0, LX/EGe;->w:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2097643
    iget-object v0, p0, LX/EGe;->w:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2097644
    const/4 v0, 0x0

    iput-object v0, p0, LX/EGe;->w:Ljava/util/concurrent/ScheduledFuture;

    .line 2097645
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v0, p0, LX/EGe;->X:Z

    if-eqz v0, :cond_1

    invoke-static {p0}, LX/EGe;->ag(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2097646
    iget-object v0, p0, LX/EGe;->j:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/services/BackgroundVideoCallService$12;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/services/BackgroundVideoCallService$12;-><init>(LX/EGe;)V

    const-wide/16 v2, 0x1388

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/EGe;->w:Ljava/util/concurrent/ScheduledFuture;

    .line 2097647
    :cond_1
    return-void
.end method


# virtual methods
.method public final B()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2097626
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/EGe;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2097627
    :cond_0
    :goto_0
    return v1

    .line 2097628
    :cond_1
    invoke-static {p0}, LX/EGe;->at(LX/EGe;)V

    .line 2097629
    invoke-static {p0}, LX/EGe;->aj(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->t()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2097630
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    .line 2097631
    iget-object v2, v0, LX/EI9;->m:Landroid/view/View;

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/EI9;->m:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 2097632
    iget-object v2, v0, LX/EI9;->m:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->callOnClick()Z

    .line 2097633
    :cond_2
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->j()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p0}, LX/EGe;->I$redex0(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2097634
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->j()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {p0, v1, v0}, LX/EGe;->b(LX/EGe;ZZ)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2097635
    :cond_4
    invoke-static {p0}, LX/EGe;->ai(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p0}, LX/EGe;->aj(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, LX/EGe;->X:Z

    if-nez v0, :cond_7

    invoke-static {p0}, LX/EGe;->I$redex0(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2097636
    :cond_5
    const/4 v0, 0x0

    .line 2097637
    :goto_2
    move v0, v0

    .line 2097638
    if-eqz v0, :cond_6

    .line 2097639
    invoke-virtual {p0}, LX/EGe;->z()V

    goto :goto_0

    .line 2097640
    :cond_6
    invoke-static {p0}, LX/EGe;->I$redex0(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097641
    invoke-static {p0, v1, v1}, LX/EGe;->b(LX/EGe;ZZ)V

    goto :goto_0

    :cond_7
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public final C()V
    .locals 2

    .prologue
    .line 2097622
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/EDv;->STARTED:LX/EDv;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EDv;)V

    .line 2097623
    invoke-static {p0}, LX/EGe;->Z(LX/EGe;)V

    .line 2097624
    invoke-static {p0}, LX/EGe;->af(LX/EGe;)V

    .line 2097625
    return-void
.end method

.method public final a()LX/ECA;
    .locals 1

    .prologue
    .line 2097621
    iget-object v0, p0, LX/EGe;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECA;

    return-object v0
.end method

.method public final a(LX/EIp;)V
    .locals 0

    .prologue
    .line 2097620
    return-void
.end method

.method public final a(Landroid/app/Dialog;)V
    .locals 2

    .prologue
    .line 2097618
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x7d7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    .line 2097619
    return-void
.end method

.method public final a(Landroid/net/Uri;JJLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2097617
    return-void
.end method

.method public final a(Landroid/view/Window;)V
    .locals 2
    .param p1    # Landroid/view/Window;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2097598
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    if-eqz v0, :cond_0

    .line 2097599
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    const/4 p0, 0x1

    .line 2097600
    iget-boolean v1, v0, LX/EFs;->d:Z

    move v1, v1

    .line 2097601
    if-nez v1, :cond_0

    if-nez p1, :cond_1

    .line 2097602
    :cond_0
    :goto_0
    return-void

    .line 2097603
    :cond_1
    iget-object v1, v0, LX/EFs;->f:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    .line 2097604
    :goto_1
    if-ne p1, v1, :cond_3

    .line 2097605
    iput-boolean p0, v0, LX/EFs;->c:Z

    .line 2097606
    iget-boolean v1, v0, LX/EFs;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/EFs;->g:LX/EGW;

    if-eqz v1, :cond_0

    .line 2097607
    iget-object v1, v0, LX/EFs;->g:LX/EGW;

    invoke-virtual {v1}, LX/EGW;->a()V

    goto :goto_0

    .line 2097608
    :cond_2
    iget-object v1, v0, LX/EFs;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Window;

    goto :goto_1

    .line 2097609
    :cond_3
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/EFs;->f:Ljava/lang/ref/WeakReference;

    .line 2097610
    iput-boolean p0, v0, LX/EFs;->c:Z

    .line 2097611
    iget-boolean v1, v0, LX/EFs;->b:Z

    if-eqz v1, :cond_0

    .line 2097612
    invoke-virtual {v0}, LX/EFs;->c()V

    .line 2097613
    if-eqz p1, :cond_4

    .line 2097614
    invoke-virtual {v0}, LX/EFs;->b()V

    .line 2097615
    :cond_4
    iget-object v1, v0, LX/EFs;->g:LX/EGW;

    if-eqz v1, :cond_0

    .line 2097616
    iget-object v1, v0, LX/EFs;->g:LX/EGW;

    invoke-virtual {v1}, LX/EGW;->a()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2097560
    if-eqz p1, :cond_1

    .line 2097561
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097562
    iget-object v1, v0, LX/EDx;->al:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v2, v1

    .line 2097563
    if-eqz v2, :cond_0

    .line 2097564
    iget-object v0, p0, LX/EGe;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EFk;

    iget-object v1, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->aJ()Z

    move-result v1

    const-string v3, "vch_retry_video"

    .line 2097565
    iget-object v6, v0, LX/EFk;->b:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/EG4;

    invoke-interface {v6}, LX/EG4;->d()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v7

    .line 2097566
    if-nez v7, :cond_7

    .line 2097567
    :goto_0
    return-void

    .line 2097568
    :cond_0
    iget-object v1, p0, LX/EGe;->o:LX/3A0;

    iget-object v2, p0, LX/EGe;->d:Landroid/content/Context;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097569
    iget-wide v6, v0, LX/EDx;->ak:J

    move-wide v4, v6

    .line 2097570
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    const-string v3, "vch_retry_video"

    invoke-virtual {v1, v2, v0, v3}, LX/3A0;->a(Landroid/content/Context;Lcom/facebook/user/model/UserKey;Ljava/lang/String;)LX/EFc;

    goto :goto_0

    .line 2097571
    :cond_1
    iget-object v3, p0, LX/EGe;->p:LX/3Eb;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097572
    iget-wide v6, v0, LX/EDx;->ac:J

    move-wide v4, v6

    .line 2097573
    invoke-virtual {v3, v4, v5, v1}, LX/3Eb;->a(JZ)V

    .line 2097574
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097575
    iget-boolean v3, v0, LX/EDx;->aB:Z

    move v0, v3

    .line 2097576
    if-nez v0, :cond_3

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->s()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2097577
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v2, v1}, LX/EDx;->a(ZZ)V

    .line 2097578
    iget-object v3, p0, LX/EGe;->q:LX/EDC;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097579
    iget-wide v6, v0, LX/EDx;->ak:J

    move-wide v4, v6

    .line 2097580
    sget-object v0, LX/EDB;->INSTANT_VIDEO_RECIPROCATED:LX/EDB;

    invoke-virtual {v3, v4, v5, v2, v0}, LX/EDC;->a(JZLX/EDB;)Z

    .line 2097581
    :cond_2
    :goto_1
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0, v1, v1, v1, v2}, LX/EI9;->a(ZZZZ)V

    goto :goto_0

    .line 2097582
    :cond_3
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097583
    iget-boolean v3, v0, LX/EDx;->aB:Z

    move v0, v3

    .line 2097584
    if-nez v0, :cond_2

    .line 2097585
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2097586
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ba()V

    .line 2097587
    invoke-static {p0}, LX/EGe;->X(LX/EGe;)V

    .line 2097588
    invoke-virtual {p0}, LX/EGe;->C()V

    .line 2097589
    :goto_2
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    .line 2097590
    iput-boolean v2, v0, LX/EI9;->L:Z

    .line 2097591
    iget-object v4, p0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->A()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    invoke-static {p0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v3

    if-nez v3, :cond_6

    move v3, v1

    :goto_4
    invoke-virtual {v4, v0, v3}, LX/EI9;->a(ZZ)V

    goto :goto_1

    .line 2097592
    :cond_4
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bb()V

    goto :goto_2

    :cond_5
    move v0, v2

    .line 2097593
    goto :goto_3

    :cond_6
    move v3, v2

    goto :goto_4

    .line 2097594
    :cond_7
    iget-object v6, v7, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    invoke-virtual {v6}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->a()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2097595
    iget-object v6, v0, LX/EFk;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/3A0;

    iget-object v8, v7, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    iget-object v8, v8, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    iget-object v9, v0, LX/EFk;->a:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    move v9, v1

    move-object v10, v3

    invoke-virtual/range {v6 .. v11}, LX/3A0;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Ljava/lang/String;ZLjava/lang/String;Landroid/content/Context;)V

    .line 2097596
    :goto_5
    goto/16 :goto_0

    .line 2097597
    :cond_8
    iget-object v6, v0, LX/EFk;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/3A0;

    iget-object v8, v0, LX/EFk;->a:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-virtual {v6, v8, v7, v1, v3}, LX/3A0;->a(Landroid/content/Context;Lcom/facebook/messaging/model/threads/ThreadSummary;ZLjava/lang/String;)V

    goto :goto_5
.end method

.method public final a(ZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2097468
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    .line 2097469
    iget-boolean p1, v0, LX/EFs;->d:Z

    move v0, p1

    .line 2097470
    if-nez v0, :cond_1

    .line 2097471
    :cond_0
    :goto_0
    return-void

    .line 2097472
    :cond_1
    invoke-static {p0}, LX/EGe;->W(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, LX/EGe;->X:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2097473
    invoke-static {p0}, LX/EGe;->W(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/EGe;->X:Z

    if-eqz v0, :cond_2

    .line 2097474
    invoke-static {p0, p2, v1}, LX/EGe;->b(LX/EGe;ZZ)V

    .line 2097475
    :cond_2
    invoke-static {p0}, LX/EGe;->at(LX/EGe;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2097476
    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2097477
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    const v1, 0x7f080790

    invoke-virtual {p0, v1}, LX/EGe;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EI9;->a(Ljava/lang/String;)V

    .line 2097478
    invoke-static {p0}, LX/EGe;->af(LX/EGe;)V

    .line 2097479
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 2097480
    if-eqz p1, :cond_2

    .line 2097481
    invoke-static {p0}, LX/EGe;->ar(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EGe;->R:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    iget-object v1, p0, LX/EGe;->R:Landroid/view/View;

    .line 2097482
    iget-object p1, v0, LX/EI9;->h:Landroid/view/View;

    if-eq p1, v1, :cond_0

    iget-object p1, v0, LX/EI9;->i:Landroid/view/View;

    if-ne p1, v1, :cond_4

    :cond_0
    const/4 p1, 0x1

    :goto_0
    move v0, p1

    .line 2097483
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EGe;->R:Landroid/view/View;

    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v1}, LX/EI9;->getExpressionButton()Landroid/view/View;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 2097484
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getExpressionButton()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EGe;->R:Landroid/view/View;

    .line 2097485
    invoke-static {p0}, LX/EGe;->an(LX/EGe;)V

    .line 2097486
    :goto_1
    return-void

    .line 2097487
    :cond_1
    invoke-static {p0}, LX/EGe;->ap(LX/EGe;)V

    goto :goto_1

    .line 2097488
    :cond_2
    invoke-static {p0}, LX/EGe;->ar(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EGe;->R:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EGe;->R:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2097489
    invoke-static {p0}, LX/EGe;->at(LX/EGe;)V

    .line 2097490
    :cond_3
    goto :goto_1

    :cond_4
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2097491
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097492
    iget-object v1, v0, LX/EDx;->T:LX/2Tm;

    if-eqz v1, :cond_0

    .line 2097493
    iget-object v1, v0, LX/EDx;->T:LX/2Tm;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LX/2Tm;->b(Z)V

    .line 2097494
    :cond_0
    invoke-virtual {v0, v2}, LX/EDx;->g(Z)V

    .line 2097495
    invoke-virtual {v0}, LX/EDx;->aY()V

    .line 2097496
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, LX/EDx;->a(ZZ)V

    .line 2097497
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2097498
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097499
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097500
    iget-object v1, v0, LX/EDx;->bb:LX/EIq;

    move-object v0, v1

    .line 2097501
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EIq;->a(Z)Z

    .line 2097502
    :goto_0
    return-void

    .line 2097503
    :cond_0
    invoke-virtual {p0}, LX/EGe;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2097504
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EDx;->o(Z)Z

    goto :goto_0

    .line 2097505
    :cond_1
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/7TQ;)V

    .line 2097506
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->x()V

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2097507
    invoke-static {p0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2097508
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097509
    iput-boolean v2, v0, LX/EDx;->aq:Z

    .line 2097510
    invoke-virtual {p0}, LX/EGe;->C()V

    .line 2097511
    :goto_0
    invoke-static {p0}, LX/EGe;->af(LX/EGe;)V

    .line 2097512
    iget-boolean v0, p0, LX/EGe;->P:Z

    if-eqz v0, :cond_0

    .line 2097513
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0, v1, v1, v1, v2}, LX/EI9;->a(ZZZZ)V

    .line 2097514
    :cond_0
    return-void

    .line 2097515
    :cond_1
    invoke-static {p0}, LX/EGe;->aa(LX/EGe;)V

    .line 2097516
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097517
    iput-boolean v1, v0, LX/EDx;->aq:Z

    .line 2097518
    goto :goto_0
.end method

.method public final f()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2097519
    iget-boolean v0, p0, LX/EGe;->P:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    .line 2097520
    iget-object v4, p0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->A()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_0
    invoke-static {p0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v4, v0, v1}, LX/EI9;->a(ZZ)V

    .line 2097521
    :cond_0
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->A()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1}, LX/EDx;->g(Z)V

    .line 2097522
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    .line 2097523
    :goto_3
    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v1, v2, v0, v2, v3}, LX/EI9;->a(ZZZZ)V

    .line 2097524
    return-void

    :cond_1
    move v0, v3

    .line 2097525
    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v1, v3

    .line 2097526
    goto :goto_2

    :cond_4
    move v0, v2

    .line 2097527
    goto :goto_3
.end method

.method public final g()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2097528
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->J()V

    .line 2097529
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v2, v2, v1}, LX/EI9;->a(ZZZZ)V

    .line 2097530
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 2097531
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097532
    iget-object v1, v0, LX/EDx;->bb:LX/EIq;

    move-object v0, v1

    .line 2097533
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EIq;->a(Z)Z

    .line 2097534
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 2097556
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097557
    iget-object v1, v0, LX/EDx;->bb:LX/EIq;

    move-object v0, v1

    .line 2097558
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EIq;->a(Z)Z

    .line 2097559
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 2097711
    const-string v0, "SHOW_EXPRESSION_UI"

    invoke-static {p0, v0}, LX/EGe;->b$redex0(LX/EGe;Ljava/lang/String;)V

    .line 2097712
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2097784
    const-string v0, "SHOW_GROUP_CALL_ROSTER"

    invoke-static {p0, v0}, LX/EGe;->b$redex0(LX/EGe;Ljava/lang/String;)V

    .line 2097785
    return-void
.end method

.method public final l()LX/0gc;
    .locals 1

    .prologue
    .line 2097779
    iget-object v0, p0, LX/EGe;->W:LX/EHn;

    .line 2097780
    iget-object p0, v0, LX/EHn;->a:LX/3qV;

    if-nez p0, :cond_0

    .line 2097781
    const/4 p0, 0x0

    .line 2097782
    :goto_0
    move-object v0, p0

    .line 2097783
    return-object v0

    :cond_0
    iget-object p0, v0, LX/EHn;->a:LX/3qV;

    invoke-virtual {p0}, LX/0k3;->p()LX/0gc;

    move-result-object p0

    goto :goto_0
.end method

.method public final n()J
    .locals 2

    .prologue
    .line 2097778
    iget-wide v0, p0, LX/EGe;->Z:J

    return-wide v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 2097777
    iget v0, p0, LX/EGe;->ad:I

    return v0
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2097776
    iget-object v0, p0, LX/EGe;->E:Landroid/os/IBinder;

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x7e3721f9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2097773
    invoke-static {p0, p0}, LX/EGe;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2097774
    invoke-direct {p0}, LX/EGe;->G()V

    .line 2097775
    const/16 v1, 0x25

    const v2, -0x45828bae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v0, 0x24

    const v1, 0x529fe35d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2097720
    iget-object v0, p0, LX/EGe;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDT;

    invoke-virtual {v0}, LX/EDT;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097721
    iput-boolean v2, p0, LX/EGe;->aj:Z

    .line 2097722
    iget-object v0, p0, LX/EGe;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDT;

    .line 2097723
    iget-object v5, v0, LX/EDT;->c:LX/EDO;

    invoke-virtual {v5}, LX/EDO;->b()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2097724
    iget-object v5, v0, LX/EDT;->c:LX/EDO;

    invoke-virtual {v5}, LX/EDO;->a()V

    .line 2097725
    :cond_0
    :goto_0
    invoke-static {p0}, LX/EGe;->aD(LX/EGe;)V

    .line 2097726
    invoke-static {p0}, LX/EGe;->at(LX/EGe;)V

    .line 2097727
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    invoke-virtual {v0}, LX/EFs;->c()V

    .line 2097728
    iget-object v0, p0, LX/EGe;->A:LX/EFt;

    invoke-virtual {v0}, LX/EFt;->a()V

    .line 2097729
    iput-object v3, p0, LX/EGe;->A:LX/EFt;

    .line 2097730
    invoke-static {p0}, LX/EGe;->aw(LX/EGe;)V

    .line 2097731
    invoke-static {p0}, LX/EGe;->ax(LX/EGe;)V

    .line 2097732
    iget-object v0, p0, LX/EGe;->v:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 2097733
    iget-object v0, p0, LX/EGe;->v:Ljava/util/concurrent/ScheduledFuture;

    const/4 v5, 0x1

    invoke-interface {v0, v5}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2097734
    const/4 v0, 0x0

    iput-object v0, p0, LX/EGe;->v:Ljava/util/concurrent/ScheduledFuture;

    .line 2097735
    :cond_1
    invoke-static {p0, v2}, LX/EGe;->d(LX/EGe;Z)V

    .line 2097736
    iget-object v0, p0, LX/EGe;->af:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_2

    .line 2097737
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    iget-object v2, p0, LX/EGe;->af:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v0, v2}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2097738
    :cond_2
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    const/4 v8, 0x4

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 2097739
    invoke-virtual {v0}, LX/EI9;->getSelfTextureView()Landroid/view/TextureView;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 2097740
    invoke-virtual {v0}, LX/EI9;->getSelfTextureView()Landroid/view/TextureView;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 2097741
    :cond_3
    iget-object v5, v0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v5, v6}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setVideoSizeChangedListener(LX/7fE;)V

    .line 2097742
    iput-object v6, v0, LX/EI9;->Q:LX/7fE;

    .line 2097743
    invoke-virtual {v0, v6}, LX/EI9;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2097744
    iput-object v6, v0, LX/EI9;->V:LX/EGd;

    .line 2097745
    invoke-virtual {v0}, LX/EI9;->clearAnimation()V

    .line 2097746
    iput-object v6, v0, LX/EI9;->U:LX/EHG;

    .line 2097747
    new-array v5, v8, [Landroid/view/View;

    iget-object v6, v0, LX/EI9;->n:Landroid/view/View;

    aput-object v6, v5, v2

    const/4 v6, 0x1

    iget-object v7, v0, LX/EI9;->m:Landroid/view/View;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v7, v0, LX/EI9;->o:Landroid/view/View;

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-object v7, v0, LX/EI9;->H:Landroid/view/View;

    aput-object v7, v5, v6

    .line 2097748
    :goto_1
    if-ge v2, v8, :cond_5

    aget-object v6, v5, v2

    .line 2097749
    if-eqz v6, :cond_4

    .line 2097750
    invoke-virtual {v6}, Landroid/view/View;->clearAnimation()V

    .line 2097751
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2097752
    :cond_5
    iget-object v2, v0, LX/EI9;->t:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2097753
    iget-object v2, v0, LX/EI9;->t:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;

    invoke-virtual {v2}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->b()V

    .line 2097754
    :cond_6
    iput-object v3, p0, LX/EGe;->D:LX/EI9;

    .line 2097755
    iget-object v0, p0, LX/EGe;->ag:LX/EFp;

    const/4 v5, 0x0

    .line 2097756
    iget-object v2, v0, LX/EFp;->p:LX/0wd;

    invoke-virtual {v2}, LX/0wd;->a()V

    .line 2097757
    iget-object v2, v0, LX/EFp;->q:LX/0wd;

    invoke-virtual {v2}, LX/0wd;->a()V

    .line 2097758
    iput-object v5, v0, LX/EFp;->p:LX/0wd;

    .line 2097759
    iput-object v5, v0, LX/EFp;->q:LX/0wd;

    .line 2097760
    iput-object v3, p0, LX/EGe;->ag:LX/EFp;

    .line 2097761
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    invoke-virtual {v0}, LX/EFs;->c()V

    .line 2097762
    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    .line 2097763
    iput-object v3, v0, LX/EFs;->g:LX/EGW;

    .line 2097764
    iput-object v3, p0, LX/EGe;->s:LX/EFs;

    .line 2097765
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v2, p0, LX/EGe;->C:LX/EC0;

    invoke-virtual {v0, v2}, LX/EDx;->b(LX/EC0;)V

    .line 2097766
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097767
    iget-object v2, v0, LX/EDx;->bb:LX/EIq;

    move-object v0, v2

    .line 2097768
    invoke-virtual {v0, p0}, LX/EIq;->b(LX/ECD;)V

    .line 2097769
    iput-object v3, p0, LX/EGe;->C:LX/EC0;

    .line 2097770
    iget-object v0, p0, LX/EGe;->O:LX/0Yd;

    invoke-virtual {p0, v0}, LX/EGe;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2097771
    iput-object v3, p0, LX/EGe;->O:LX/0Yd;

    .line 2097772
    const/16 v0, 0x25

    const v2, 0x5d29f84

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_7
    goto/16 :goto_0
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x3ef3b694

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2097719
    const/4 v1, 0x1

    const/16 v2, 0x25

    const v3, 0x24642ef0

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public final declared-synchronized onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 2097713
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->X()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2097714
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getSelfTextureView()Landroid/view/TextureView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-static {v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2097715
    :goto_0
    monitor-exit p0

    return-void

    .line 2097716
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->V()LX/EHZ;

    move-result-object v0

    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v1}, LX/EI9;->getSelfTextureView()Landroid/view/TextureView;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EHZ;->a(Landroid/view/TextureView;)V

    .line 2097717
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/EDv;->STARTED:LX/EDv;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EDv;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2097718
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 2097675
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097676
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2097677
    :cond_0
    const/4 v0, 0x0

    monitor-exit p0

    return v0

    .line 2097678
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 2097710
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 2097709
    return-void
.end method

.method public final onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 2097708
    invoke-super {p0, p1}, LX/0te;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public final p()J
    .locals 2

    .prologue
    .line 2097707
    iget-wide v0, p0, LX/EGe;->aa:J

    return-wide v0
.end method

.method public final q()J
    .locals 2

    .prologue
    .line 2097696
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_3

    .line 2097697
    iget-object v0, p0, LX/EGe;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDT;

    invoke-virtual {v0}, LX/EDT;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097698
    iget-object v0, p0, LX/EGe;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDT;

    sget-object v1, LX/EDS;->VIDEO_CHAT_HEAD:LX/EDS;

    invoke-virtual {v0, v1}, LX/EDT;->b(LX/EDS;)J

    move-result-wide v0

    .line 2097699
    :goto_0
    return-wide v0

    .line 2097700
    :cond_0
    iget-object v0, p0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2097701
    const-wide/32 v0, 0x1d4c0

    goto :goto_0

    .line 2097702
    :cond_1
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v1, LX/EI8;->END_CALL_STATE:LX/EI8;

    if-ne v0, v1, :cond_2

    .line 2097703
    const-wide/16 v0, 0x7d0

    goto :goto_0

    .line 2097704
    :cond_2
    iget-object v0, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    sget-object v1, LX/EI8;->END_CALL_STATE_WITH_RETRY:LX/EI8;

    if-ne v0, v1, :cond_3

    .line 2097705
    const-wide/16 v0, 0x4e20

    goto :goto_0

    .line 2097706
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2097692
    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    if-nez v1, :cond_1

    .line 2097693
    :cond_0
    :goto_0
    return v0

    .line 2097694
    :cond_1
    iget-object v1, p0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v1}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v1

    .line 2097695
    sget-object v2, LX/EI8;->END_CALL_STATE:LX/EI8;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/EI8;->END_CALL_STATE_WITH_RETRY:LX/EI8;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/EI8;->END_CALL_STATE_VOICEMAIL:LX/EI8;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final v()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2097688
    invoke-virtual {p0, v0, v0}, LX/EGe;->a(ZZ)V

    .line 2097689
    invoke-static {p0}, LX/EGe;->T(LX/EGe;)V

    .line 2097690
    invoke-static {p0, v0}, LX/EGe;->c(LX/EGe;Z)V

    .line 2097691
    const/4 v0, 0x1

    return v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 2097684
    iget-boolean v0, p0, LX/EGe;->P:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGe;->s:LX/EFs;

    .line 2097685
    iget-boolean p0, v0, LX/EFs;->d:Z

    move p0, p0

    .line 2097686
    if-eqz p0, :cond_1

    iget-boolean p0, v0, LX/EFs;->b:Z

    :goto_0
    move v0, p0

    .line 2097687
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    iget-boolean p0, v0, LX/EFs;->b:Z

    if-eqz p0, :cond_2

    iget-boolean p0, v0, LX/EFs;->c:Z

    if-eqz p0, :cond_2

    const/4 p0, 0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 2097681
    iget-object v0, p0, LX/EGe;->ag:LX/EFp;

    .line 2097682
    iget-boolean p0, v0, LX/EFp;->o:Z

    move v0, p0

    .line 2097683
    return v0
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 2097679
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/EGe;->b$redex0(LX/EGe;Ljava/lang/String;)V

    .line 2097680
    return-void
.end method
