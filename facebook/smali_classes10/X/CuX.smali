.class public LX/CuX;
.super LX/2oy;
.source ""


# instance fields
.field public a:LX/CuW;

.field public b:Landroid/view/GestureDetector;

.field public c:LX/3IC;

.field public d:Z

.field private e:Z

.field public f:LX/Ctg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1946738
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/CuX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1946739
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1946752
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/CuX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1946753
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1946745
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1946746
    iput-boolean v0, p0, LX/CuX;->d:Z

    .line 1946747
    iput-boolean v0, p0, LX/CuX;->e:Z

    .line 1946748
    new-instance v0, LX/CuW;

    invoke-direct {v0, p0}, LX/CuW;-><init>(LX/CuX;)V

    iput-object v0, p0, LX/CuX;->a:LX/CuW;

    .line 1946749
    new-instance v0, LX/3IC;

    iget-object p2, p0, LX/CuX;->a:LX/CuW;

    iget-object p3, p0, LX/CuX;->a:LX/CuW;

    invoke-direct {v0, p1, p2, p3}, LX/3IC;-><init>(Landroid/content/Context;LX/3IA;LX/3IB;)V

    iput-object v0, p0, LX/CuX;->c:LX/3IC;

    .line 1946750
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/CuX;->getContext()Landroid/content/Context;

    move-result-object p2

    new-instance p3, LX/CuV;

    invoke-direct {p3, p0}, LX/CuV;-><init>(LX/CuX;)V

    invoke-direct {v0, p2, p3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/CuX;->b:Landroid/view/GestureDetector;

    .line 1946751
    return-void
.end method


# virtual methods
.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x76adf9eb

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1946740
    iget-object v2, p0, LX/CuX;->b:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    iput-boolean v2, p0, LX/CuX;->e:Z

    .line 1946741
    iget-boolean v2, p0, LX/CuX;->e:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/CuX;->f:LX/Ctg;

    if-eqz v2, :cond_0

    .line 1946742
    iget-object v2, p0, LX/CuX;->f:LX/Ctg;

    if-nez v2, :cond_2

    .line 1946743
    :cond_0
    :goto_0
    iget-boolean v2, p0, LX/CuX;->e:Z

    if-nez v2, :cond_1

    iget-object v2, p0, LX/CuX;->c:LX/3IC;

    invoke-virtual {v2, p1}, LX/3IC;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    const v2, -0x56f87337

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1946744
    :cond_2
    iget-object v2, p0, LX/CuX;->f:LX/Ctg;

    sget-object v3, LX/Crd;->CLICK_MEDIA:LX/Crd;

    invoke-interface {v2, v3}, LX/Cre;->a(LX/Crd;)Z

    goto :goto_0
.end method
