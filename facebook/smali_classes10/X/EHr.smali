.class public final LX/EHr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/EI9;


# direct methods
.method public constructor <init>(LX/EI9;Z)V
    .locals 0

    .prologue
    .line 2100449
    iput-object p1, p0, LX/EHr;->b:LX/EI9;

    iput-boolean p2, p0, LX/EHr;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2100450
    iget-object v0, p0, LX/EHr;->b:LX/EI9;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/EI9;->setButtonsHolderVisibility(LX/EI9;Z)V

    .line 2100451
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2100448
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2100445
    iget-boolean v0, p0, LX/EHr;->a:Z

    if-eqz v0, :cond_0

    .line 2100446
    iget-object v0, p0, LX/EHr;->b:LX/EI9;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/EI9;->setButtonsHolderVisibility(LX/EI9;Z)V

    .line 2100447
    :cond_0
    return-void
.end method
