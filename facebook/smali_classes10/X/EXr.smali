.class public final LX/EXr;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EXp;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXr;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EXr;


# instance fields
.field public bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public method_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EXj;",
            ">;"
        }
    .end annotation
.end field

.field public name_:Ljava/lang/Object;

.field public options_:LX/EXv;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2135565
    new-instance v0, LX/EXo;

    invoke-direct {v0}, LX/EXo;-><init>()V

    sput-object v0, LX/EXr;->a:LX/EWZ;

    .line 2135566
    new-instance v0, LX/EXr;

    invoke-direct {v0}, LX/EXr;-><init>()V

    .line 2135567
    sput-object v0, LX/EXr;->c:LX/EXr;

    invoke-direct {v0}, LX/EXr;->q()V

    .line 2135568
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2135569
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2135570
    iput-byte v0, p0, LX/EXr;->memoizedIsInitialized:B

    .line 2135571
    iput v0, p0, LX/EXr;->memoizedSerializedSize:I

    .line 2135572
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2135573
    iput-object v0, p0, LX/EXr;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v6, 0x2

    .line 2135574
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2135575
    iput-byte v1, p0, LX/EXr;->memoizedIsInitialized:B

    .line 2135576
    iput v1, p0, LX/EXr;->memoizedSerializedSize:I

    .line 2135577
    invoke-direct {p0}, LX/EXr;->q()V

    .line 2135578
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 2135579
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 2135580
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2135581
    sparse-switch v0, :sswitch_data_0

    .line 2135582
    invoke-virtual {p0, p1, v5, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 2135583
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 2135584
    goto :goto_0

    .line 2135585
    :sswitch_1
    iget v0, p0, LX/EXr;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EXr;->bitField0_:I

    .line 2135586
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EXr;->name_:Ljava/lang/Object;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2135587
    :catch_0
    move-exception v0

    .line 2135588
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2135589
    move-object v0, v0

    .line 2135590
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2135591
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_1

    .line 2135592
    iget-object v1, p0, LX/EXr;->method_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXr;->method_:Ljava/util/List;

    .line 2135593
    :cond_1
    invoke-virtual {v5}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EXr;->unknownFields:LX/EZQ;

    .line 2135594
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2135595
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v6, :cond_2

    .line 2135596
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    .line 2135597
    or-int/lit8 v1, v1, 0x2

    .line 2135598
    :cond_2
    iget-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    sget-object v2, LX/EXj;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2135599
    :catch_1
    move-exception v0

    .line 2135600
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2135601
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2135602
    move-object v0, v2

    .line 2135603
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2135604
    :sswitch_3
    const/4 v0, 0x0

    .line 2135605
    :try_start_4
    iget v2, p0, LX/EXr;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v6, :cond_6

    .line 2135606
    iget-object v0, p0, LX/EXr;->options_:LX/EXv;

    invoke-virtual {v0}, LX/EXv;->j()LX/EXu;

    move-result-object v0

    move-object v2, v0

    .line 2135607
    :goto_1
    sget-object v0, LX/EXv;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EXv;

    iput-object v0, p0, LX/EXr;->options_:LX/EXv;

    .line 2135608
    if-eqz v2, :cond_3

    .line 2135609
    iget-object v0, p0, LX/EXr;->options_:LX/EXv;

    invoke-virtual {v2, v0}, LX/EXu;->a(LX/EXv;)LX/EXu;

    .line 2135610
    invoke-virtual {v2}, LX/EXu;->l()LX/EXv;

    move-result-object v0

    iput-object v0, p0, LX/EXr;->options_:LX/EXv;

    .line 2135611
    :cond_3
    iget v0, p0, LX/EXr;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EXr;->bitField0_:I
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2135612
    :cond_4
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v6, :cond_5

    .line 2135613
    iget-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    .line 2135614
    :cond_5
    invoke-virtual {v5}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXr;->unknownFields:LX/EZQ;

    .line 2135615
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2135616
    return-void

    :cond_6
    move-object v2, v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2135617
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2135618
    iput-byte v1, p0, LX/EXr;->memoizedIsInitialized:B

    .line 2135619
    iput v1, p0, LX/EXr;->memoizedSerializedSize:I

    .line 2135620
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXr;->unknownFields:LX/EZQ;

    .line 2135621
    return-void
.end method

.method private static c(LX/EXr;)LX/EXq;
    .locals 1

    .prologue
    .line 2135622
    invoke-static {}, LX/EXq;->n()LX/EXq;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EXq;->a(LX/EXr;)LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method private p()LX/EWc;
    .locals 2

    .prologue
    .line 2135623
    iget-object v0, p0, LX/EXr;->name_:Ljava/lang/Object;

    .line 2135624
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2135625
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2135626
    iput-object v0, p0, LX/EXr;->name_:Ljava/lang/Object;

    .line 2135627
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 2135641
    const-string v0, ""

    iput-object v0, p0, LX/EXr;->name_:Ljava/lang/Object;

    .line 2135642
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    .line 2135643
    sget-object v0, LX/EXv;->c:LX/EXv;

    move-object v0, v0

    .line 2135644
    iput-object v0, p0, LX/EXr;->options_:LX/EXv;

    .line 2135645
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2135646
    new-instance v0, LX/EXq;

    invoke-direct {v0, p1}, LX/EXq;-><init>(LX/EYd;)V

    .line 2135647
    return-object v0
.end method

.method public final a(I)LX/EXj;
    .locals 1

    .prologue
    .line 2135648
    iget-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXj;

    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2135555
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2135556
    iget v0, p0, LX/EXr;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2135557
    invoke-direct {p0}, LX/EXr;->p()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2135558
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2135559
    iget-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v2, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2135560
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2135561
    :cond_1
    iget v0, p0, LX/EXr;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 2135562
    const/4 v0, 0x3

    iget-object v1, p0, LX/EXr;->options_:LX/EXv;

    invoke-virtual {p1, v0, v1}, LX/EWf;->b(ILX/EWW;)V

    .line 2135563
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2135564
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2135628
    iget-byte v0, p0, LX/EXr;->memoizedIsInitialized:B

    .line 2135629
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2135630
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 2135631
    :goto_1
    invoke-virtual {p0}, LX/EXr;->l()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 2135632
    invoke-virtual {p0, v0}, LX/EXr;->a(I)LX/EXj;

    move-result-object v3

    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2135633
    iput-byte v1, p0, LX/EXr;->memoizedIsInitialized:B

    goto :goto_0

    .line 2135634
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2135635
    :cond_3
    invoke-virtual {p0}, LX/EXr;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2135636
    iget-object v0, p0, LX/EXr;->options_:LX/EXv;

    move-object v0, v0

    .line 2135637
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2135638
    iput-byte v1, p0, LX/EXr;->memoizedIsInitialized:B

    goto :goto_0

    .line 2135639
    :cond_4
    iput-byte v2, p0, LX/EXr;->memoizedIsInitialized:B

    move v1, v2

    .line 2135640
    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2135525
    iget v0, p0, LX/EXr;->memoizedSerializedSize:I

    .line 2135526
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2135527
    :goto_0
    return v0

    .line 2135528
    :cond_0
    iget v0, p0, LX/EXr;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 2135529
    invoke-direct {p0}, LX/EXr;->p()LX/EWc;

    move-result-object v0

    invoke-static {v3, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v0

    .line 2135530
    :goto_2
    iget-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2135531
    iget-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v4, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2135532
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2135533
    :cond_1
    iget v0, p0, LX/EXr;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 2135534
    const/4 v0, 0x3

    iget-object v1, p0, LX/EXr;->options_:LX/EXv;

    invoke-static {v0, v1}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2135535
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2135536
    iput v0, p0, LX/EXr;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2135538
    iget-object v0, p0, LX/EXr;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2135539
    sget-object v0, LX/EYC;->p:LX/EYn;

    const-class v1, LX/EXr;

    const-class v2, LX/EXq;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2135540
    sget-object v0, LX/EXr;->a:LX/EWZ;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2135541
    iget-object v0, p0, LX/EXr;->name_:Ljava/lang/Object;

    .line 2135542
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2135543
    check-cast v0, Ljava/lang/String;

    .line 2135544
    :goto_0
    return-object v0

    .line 2135545
    :cond_0
    check-cast v0, LX/EWc;

    .line 2135546
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2135547
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2135548
    iput-object v1, p0, LX/EXr;->name_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2135549
    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 2135537
    iget-object v0, p0, LX/EXr;->method_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2135550
    iget v0, p0, LX/EXr;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2135551
    invoke-static {p0}, LX/EXr;->c(LX/EXr;)LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2135552
    invoke-static {}, LX/EXq;->n()LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2135553
    invoke-static {p0}, LX/EXr;->c(LX/EXr;)LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2135554
    sget-object v0, LX/EXr;->c:LX/EXr;

    return-object v0
.end method
