.class public LX/Ctm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "LX/Ct1;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ya;"
    }
.end annotation


# instance fields
.field private final a:LX/Ct1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public b:F


# direct methods
.method public constructor <init>(LX/Ct1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 1945585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1945586
    iput-object p1, p0, LX/Ctm;->a:LX/Ct1;

    .line 1945587
    invoke-static {p0}, LX/Ctm;->e(LX/Ctm;)Landroid/view/View;

    move-result-object v0

    new-instance p1, LX/Ctl;

    invoke-direct {p1, p0}, LX/Ctl;-><init>(LX/Ctm;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1945588
    return-void
.end method

.method public static e(LX/Ctm;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1945584
    iget-object v0, p0, LX/Ctm;->a:LX/Ct1;

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/Ctm;)LX/Ctg;
    .locals 1

    .prologue
    .line 1945583
    invoke-static {p0}, LX/Ctm;->e(LX/Ctm;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, LX/Ctg;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1945581
    invoke-static {p0}, LX/Ctm;->f(LX/Ctm;)LX/Ctg;

    move-result-object v0

    .line 1945582
    invoke-static {p0}, LX/Ctm;->e(LX/Ctm;)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Ctg;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method
