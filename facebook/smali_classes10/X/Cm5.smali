.class public abstract LX/Cm5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Clr;


# instance fields
.field private final a:Landroid/os/Bundle;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:LX/Cmv;

.field private final e:LX/Cmv;

.field private final f:LX/Cml;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1933182
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/Cm5;-><init>(Ljava/lang/String;I)V

    .line 1933183
    return-void
.end method

.method public constructor <init>(LX/Cm7;)V
    .locals 1

    .prologue
    .line 1933174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933175
    iget v0, p1, LX/Cm7;->a:I

    iput v0, p0, LX/Cm5;->b:I

    .line 1933176
    invoke-virtual {p1}, LX/Cm7;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cm5;->c:Ljava/lang/String;

    .line 1933177
    iget-object v0, p1, LX/Cm7;->c:LX/Cmv;

    iput-object v0, p0, LX/Cm5;->d:LX/Cmv;

    .line 1933178
    iget-object v0, p1, LX/Cm7;->d:LX/Cmv;

    iput-object v0, p0, LX/Cm5;->e:LX/Cmv;

    .line 1933179
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/Cm5;->a:Landroid/os/Bundle;

    .line 1933180
    iget-object v0, p1, LX/Cm7;->e:LX/Cml;

    iput-object v0, p0, LX/Cm5;->f:LX/Cml;

    .line 1933181
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1933166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933167
    iput-object p1, p0, LX/Cm5;->c:Ljava/lang/String;

    .line 1933168
    iput p2, p0, LX/Cm5;->b:I

    .line 1933169
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/Cm5;->a:Landroid/os/Bundle;

    .line 1933170
    sget-object v0, LX/Cmv;->a:LX/Cmv;

    iput-object v0, p0, LX/Cm5;->d:LX/Cmv;

    .line 1933171
    sget-object v0, LX/Cmv;->a:LX/Cmv;

    iput-object v0, p0, LX/Cm5;->e:LX/Cmv;

    .line 1933172
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cm5;->f:LX/Cml;

    .line 1933173
    return-void
.end method


# virtual methods
.method public jc_()LX/Cmv;
    .locals 1

    .prologue
    .line 1933165
    iget-object v0, p0, LX/Cm5;->d:LX/Cmv;

    return-object v0
.end method

.method public jd_()Z
    .locals 1

    .prologue
    .line 1933164
    iget-object v0, p0, LX/Cm5;->d:LX/Cmv;

    invoke-virtual {v0}, LX/Cmv;->d()Z

    move-result v0

    return v0
.end method

.method public je_()LX/Cmv;
    .locals 1

    .prologue
    .line 1933184
    iget-object v0, p0, LX/Cm5;->e:LX/Cmv;

    return-object v0
.end method

.method public jf_()Z
    .locals 1

    .prologue
    .line 1933163
    iget-object v0, p0, LX/Cm5;->e:LX/Cmv;

    invoke-virtual {v0}, LX/Cmv;->d()Z

    move-result v0

    return v0
.end method

.method public final lw_()LX/Cml;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933162
    iget-object v0, p0, LX/Cm5;->f:LX/Cml;

    return-object v0
.end method

.method public lx_()I
    .locals 1

    .prologue
    .line 1933161
    iget v0, p0, LX/Cm5;->b:I

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933160
    iget-object v0, p0, LX/Cm5;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1933159
    iget-object v0, p0, LX/Cm5;->a:Landroid/os/Bundle;

    return-object v0
.end method
