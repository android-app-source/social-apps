.class public final LX/E5j;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5k;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/net/Uri;

.field public c:Landroid/view/View$OnClickListener;


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2078816
    const-string v0, "ReactionIconHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2078817
    if-ne p0, p1, :cond_1

    .line 2078818
    :cond_0
    :goto_0
    return v0

    .line 2078819
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2078820
    goto :goto_0

    .line 2078821
    :cond_3
    check-cast p1, LX/E5j;

    .line 2078822
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2078823
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2078824
    if-eq v2, v3, :cond_0

    .line 2078825
    iget-object v2, p0, LX/E5j;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E5j;->a:Ljava/lang/String;

    iget-object v3, p1, LX/E5j;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2078826
    goto :goto_0

    .line 2078827
    :cond_5
    iget-object v2, p1, LX/E5j;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2078828
    :cond_6
    iget-object v2, p0, LX/E5j;->b:Landroid/net/Uri;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E5j;->b:Landroid/net/Uri;

    iget-object v3, p1, LX/E5j;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2078829
    goto :goto_0

    .line 2078830
    :cond_8
    iget-object v2, p1, LX/E5j;->b:Landroid/net/Uri;

    if-nez v2, :cond_7

    .line 2078831
    :cond_9
    iget-object v2, p0, LX/E5j;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/E5j;->c:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/E5j;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2078832
    goto :goto_0

    .line 2078833
    :cond_a
    iget-object v2, p1, LX/E5j;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
