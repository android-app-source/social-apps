.class public LX/E5U;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/E5U;


# instance fields
.field private final a:LX/E4q;


# direct methods
.method public constructor <init>(LX/E4q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078456
    iput-object p1, p0, LX/E5U;->a:LX/E4q;

    .line 2078457
    return-void
.end method

.method public static a(LX/0QB;)LX/E5U;
    .locals 4

    .prologue
    .line 2078458
    sget-object v0, LX/E5U;->b:LX/E5U;

    if-nez v0, :cond_1

    .line 2078459
    const-class v1, LX/E5U;

    monitor-enter v1

    .line 2078460
    :try_start_0
    sget-object v0, LX/E5U;->b:LX/E5U;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078461
    if-eqz v2, :cond_0

    .line 2078462
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078463
    new-instance p0, LX/E5U;

    invoke-static {v0}, LX/E4q;->a(LX/0QB;)LX/E4q;

    move-result-object v3

    check-cast v3, LX/E4q;

    invoke-direct {p0, v3}, LX/E5U;-><init>(LX/E4q;)V

    .line 2078464
    move-object v0, p0

    .line 2078465
    sput-object v0, LX/E5U;->b:LX/E5U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078466
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078467
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078468
    :cond_1
    sget-object v0, LX/E5U;->b:LX/E5U;

    return-object v0

    .line 2078469
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078470
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Landroid/text/SpannableStringBuilder;ZLX/174;Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)LX/1Dg;
    .locals 8
    .param p2    # Landroid/text/SpannableStringBuilder;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/174;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 2078471
    iget-object v0, p0, LX/E5U;->a:LX/E4q;

    invoke-virtual {v0, p1}, LX/E4q;->c(LX/1De;)LX/E4o;

    move-result-object v7

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2078472
    if-eqz p5, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_DARK_PARAGRAPH_LONG_TRUNCATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p5, v2, :cond_0

    .line 2078473
    const v2, 0x7f0a010c

    .line 2078474
    :goto_0
    move v2, v2

    .line 2078475
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2078476
    if-eqz p5, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_DARK_PARAGRAPH_LONG_TRUNCATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p5, v2, :cond_1

    .line 2078477
    const v2, 0x7f0b0050

    .line 2078478
    :goto_1
    move v2, v2

    .line 2078479
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2078480
    if-eqz p5, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_DARK_PARAGRAPH_LONG_TRUNCATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p5, v1, :cond_2

    .line 2078481
    const/4 v1, 0x5

    .line 2078482
    :goto_2
    move v6, v1

    .line 2078483
    move-object v1, p2

    move v2, p3

    move-object v3, p4

    invoke-static/range {v0 .. v6}, LX/E6H;->a(Landroid/content/res/Resources;Landroid/text/SpannableStringBuilder;ZLX/174;III)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/E4o;->a(Landroid/text/SpannableStringBuilder;)LX/E4o;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/E4o;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)LX/E4o;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_0
    const v2, 0x7f0a010e

    goto :goto_0

    :cond_1
    const v2, 0x7f0b004e

    goto :goto_1

    :cond_2
    const/4 v1, 0x2

    goto :goto_2
.end method
