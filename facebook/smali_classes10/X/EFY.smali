.class public LX/EFY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2095981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2095982
    return-void
.end method

.method public static a(Lcom/facebook/rtc/helpers/RtcCallStartParams;LX/EDx;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2095983
    iget-object v1, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    sget-object v2, LX/EFe;->GROUP_CALL_JOIN:LX/EFe;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    sget-object v2, LX/EFe;->GROUP_CALL_START:LX/EFe;

    if-ne v1, v2, :cond_2

    .line 2095984
    :cond_0
    invoke-virtual {p1}, LX/EDx;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, LX/EDx;->ao()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, LX/EDx;->ao()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2095985
    :cond_1
    :goto_0
    return v0

    .line 2095986
    :cond_2
    iget-wide v6, p1, LX/EDx;->ak:J

    move-wide v2, v6

    .line 2095987
    iget-wide v4, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2095988
    :cond_3
    sget-object v0, LX/EFX;->a:[I

    iget-object v1, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    invoke-virtual {v1}, LX/EFe;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2095989
    invoke-virtual {p1}, LX/EDx;->q()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2095990
    invoke-virtual {p1}, LX/EDx;->aW()V

    .line 2095991
    :cond_4
    :goto_1
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2095992
    :pswitch_1
    invoke-virtual {p1}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2095993
    invoke-virtual {p1}, LX/EDx;->N()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, LX/EDx;->K()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2095994
    iget-object v0, p1, LX/EDx;->bq:LX/EGe;

    if-eqz v0, :cond_5

    iget-object v0, p1, LX/EDx;->ar:LX/EDv;

    sget-object v1, LX/EDv;->STARTED:LX/EDv;

    if-ne v0, v1, :cond_5

    .line 2095995
    iget-object v0, p1, LX/EDx;->bq:LX/EGe;

    .line 2095996
    invoke-static {v0}, LX/EGe;->aa(LX/EGe;)V

    .line 2095997
    const/4 v1, 0x0

    invoke-static {v1}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Landroid/graphics/SurfaceTexture;)V

    .line 2095998
    iget-object v1, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    sget-object p1, LX/EDv;->STOPPED:LX/EDv;

    invoke-virtual {v1, p1}, LX/EDx;->a(LX/EDv;)V

    .line 2095999
    invoke-static {v0}, LX/EGe;->af(LX/EGe;)V

    .line 2096000
    :cond_5
    :goto_2
    goto :goto_1

    .line 2096001
    :cond_6
    invoke-virtual {p1}, LX/EDx;->N()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2096002
    invoke-virtual {p1}, LX/EDx;->t()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2096003
    invoke-virtual {p1}, LX/EDx;->bd()V

    goto :goto_2

    .line 2096004
    :cond_7
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/EDx;->a(ZZ)V

    goto :goto_2

    .line 2096005
    :cond_8
    sget-object v0, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    invoke-virtual {p1, v0}, LX/EDx;->a(LX/7TQ;)V

    .line 2096006
    invoke-virtual {p1}, LX/EDx;->x()V

    goto :goto_2

    .line 2096007
    :cond_9
    invoke-virtual {p1}, LX/EDx;->aW()V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
