.class public LX/E25;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E23;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E25;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2072192
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E25;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072201
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2072202
    iput-object p1, p0, LX/E25;->b:LX/0Ot;

    .line 2072203
    return-void
.end method

.method public static a(LX/0QB;)LX/E25;
    .locals 4

    .prologue
    .line 2072204
    sget-object v0, LX/E25;->c:LX/E25;

    if-nez v0, :cond_1

    .line 2072205
    const-class v1, LX/E25;

    monitor-enter v1

    .line 2072206
    :try_start_0
    sget-object v0, LX/E25;->c:LX/E25;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072207
    if-eqz v2, :cond_0

    .line 2072208
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072209
    new-instance v3, LX/E25;

    const/16 p0, 0x30b5

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E25;-><init>(LX/0Ot;)V

    .line 2072210
    move-object v0, v3

    .line 2072211
    sput-object v0, LX/E25;->c:LX/E25;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072212
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072213
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072214
    :cond_1
    sget-object v0, LX/E25;->c:LX/E25;

    return-object v0

    .line 2072215
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072216
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2072195
    check-cast p2, LX/E24;

    .line 2072196
    iget-object v0, p0, LX/E25;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;

    iget-object v2, p2, LX/E24;->a:LX/5sY;

    iget-object v3, p2, LX/E24;->b:LX/9qL;

    iget-object v4, p2, LX/E24;->c:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    iget-object v5, p2, LX/E24;->d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-object v1, p1

    const/4 p2, 0x2

    .line 2072197
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    if-ne v5, v6, :cond_0

    sget v6, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->b:I

    move v7, v6

    .line 2072198
    :goto_0
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    iget-object v6, v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->d:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1Ad;

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v6

    sget-object p1, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, p1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v6

    invoke-virtual {v6}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v6

    invoke-static {v1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object p0

    const p1, 0x7f0a010a

    invoke-virtual {p0, p1}, LX/1nh;->i(I)LX/1nh;

    move-result-object p0

    invoke-virtual {v6, p0}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-static {v4}, LX/CgW;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;)I

    move-result v7

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    invoke-static {v4}, LX/CgW;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;)I

    move-result v7

    invoke-interface {v6, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    invoke-interface {v8, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/reaction/feed/corecomponents/spec/ReactionCoreImageTextComponentSpec;->c:LX/E28;

    invoke-virtual {v7, v1}, LX/E28;->c(LX/1De;)LX/E26;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/E26;->a(LX/9qL;)LX/E26;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v7, v8}, LX/1Di;->a(F)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2072199
    return-object v0

    .line 2072200
    :cond_0
    invoke-static {v5}, LX/CgW;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;)I

    move-result v6

    move v7, v6

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2072193
    invoke-static {}, LX/1dS;->b()V

    .line 2072194
    const/4 v0, 0x0

    return-object v0
.end method
