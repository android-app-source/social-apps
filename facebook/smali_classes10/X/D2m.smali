.class public final LX/D2m;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:LX/D2o;


# direct methods
.method public constructor <init>(LX/D2o;)V
    .locals 0

    .prologue
    .line 1959267
    iput-object p1, p0, LX/D2m;->a:LX/D2o;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 1959257
    iget-object v0, p0, LX/D2m;->a:LX/D2o;

    const/4 v1, 0x0

    .line 1959258
    iput-object v1, v0, LX/D2o;->f:Landroid/animation/Animator;

    .line 1959259
    iget-object v0, p0, LX/D2m;->a:LX/D2o;

    iget-object v0, v0, LX/D2o;->d:Landroid/widget/ImageView;

    iget-object v1, p0, LX/D2m;->a:LX/D2o;

    iget-object v1, v1, LX/D2o;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1959260
    iget-object v0, p0, LX/D2m;->a:LX/D2o;

    iget-object v0, v0, LX/D2o;->h:Landroid/graphics/Rect;

    iget-object v1, p0, LX/D2m;->a:LX/D2o;

    iget-object v1, v1, LX/D2o;->j:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    neg-int v1, v1

    iget-object v2, p0, LX/D2m;->a:LX/D2o;

    iget-object v2, v2, LX/D2o;->j:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 1959261
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 1959262
    iget-object v0, p0, LX/D2m;->a:LX/D2o;

    const/4 v1, 0x0

    .line 1959263
    iput-object v1, v0, LX/D2o;->f:Landroid/animation/Animator;

    .line 1959264
    iget-object v0, p0, LX/D2m;->a:LX/D2o;

    iget-object v0, v0, LX/D2o;->d:Landroid/widget/ImageView;

    iget-object v1, p0, LX/D2m;->a:LX/D2o;

    iget-object v1, v1, LX/D2o;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1959265
    iget-object v0, p0, LX/D2m;->a:LX/D2o;

    iget-object v0, v0, LX/D2o;->h:Landroid/graphics/Rect;

    iget-object v1, p0, LX/D2m;->a:LX/D2o;

    iget-object v1, v1, LX/D2o;->j:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    neg-int v1, v1

    iget-object v2, p0, LX/D2m;->a:LX/D2o;

    iget-object v2, v2, LX/D2o;->j:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 1959266
    return-void
.end method
