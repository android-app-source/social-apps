.class public LX/D8z;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final a:LX/0wT;


# instance fields
.field public final b:Lcom/facebook/widget/CustomFrameLayout;

.field public final c:Lcom/facebook/widget/CustomFrameLayout;

.field public final d:LX/0wd;

.field public final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:LX/D8w;

.field public final h:Landroid/view/GestureDetector;

.field public i:LX/0wW;

.field public j:LX/0wc;

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/Runnable;

.field private n:Z

.field public o:LX/IvM;

.field public p:LX/D8y;

.field public q:Landroid/view/ViewGroup;

.field public r:LX/D8x;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 1969584
    new-instance v0, LX/0wT;

    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    const-wide/high16 v4, 0x401c000000000000L    # 7.0

    invoke-direct {v0, v2, v3, v4, v5}, LX/0wT;-><init>(DD)V

    sput-object v0, LX/D8z;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1969656
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1969657
    sget-object v0, LX/D8x;->HIDDEN:LX/D8x;

    iput-object v0, p0, LX/D8z;->r:LX/D8x;

    .line 1969658
    const-class v0, LX/D8z;

    invoke-static {v0, p0}, LX/D8z;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1969659
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1969660
    const v1, 0x7f03040e

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1969661
    iget-object v0, p0, LX/D8z;->i:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/D8z;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/D8z;->d:LX/0wd;

    .line 1969662
    new-instance v0, LX/D8w;

    invoke-direct {v0, p0}, LX/D8w;-><init>(LX/D8z;)V

    iput-object v0, p0, LX/D8z;->g:LX/D8w;

    .line 1969663
    const v0, 0x7f0d0c64

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    iput-object v0, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    .line 1969664
    const v0, 0x7f0d0c63

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D8z;->f:Landroid/view/View;

    .line 1969665
    const v0, 0x7f0d0c65

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D8z;->e:Landroid/view/View;

    .line 1969666
    const v0, 0x7f0d0553

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    iput-object v0, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    .line 1969667
    iget-object v0, p0, LX/D8z;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1969668
    sget-object v0, LX/D8y;->HOOK_SHOT_BOTTOM:LX/D8y;

    iput-object v0, p0, LX/D8z;->p:LX/D8y;

    .line 1969669
    invoke-virtual {p0, v2}, LX/D8z;->setFocusableInTouchMode(Z)V

    .line 1969670
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/D8s;

    invoke-direct {v1, p0}, LX/D8s;-><init>(LX/D8z;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/D8z;->h:Landroid/view/GestureDetector;

    .line 1969671
    iget-object v0, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    new-instance v1, LX/D8t;

    invoke-direct {v1, p0}, LX/D8t;-><init>(LX/D8z;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1969672
    iget-object v0, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    new-instance v1, LX/D8u;

    invoke-direct {v1, p0}, LX/D8u;-><init>(LX/D8z;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1969673
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1969652
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    .line 1969653
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1969654
    :goto_0
    return-void

    .line 1969655
    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/D8z;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v1

    check-cast v1, LX/0wW;

    invoke-static {p0}, LX/0wc;->a(LX/0QB;)LX/0wc;

    move-result-object p0

    check-cast p0, LX/0wc;

    iput-object v1, p1, LX/D8z;->i:LX/0wW;

    iput-object p0, p1, LX/D8z;->j:LX/0wc;

    return-void
.end method

.method public static e(LX/D8z;)Z
    .locals 2

    .prologue
    .line 1969651
    iget-object v0, p0, LX/D8z;->r:LX/D8x;

    sget-object v1, LX/D8x;->REVEALING:LX/D8x;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/D8z;->r:LX/D8x;

    sget-object v1, LX/D8x;->REVEALED:LX/D8x;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h$redex0(LX/D8z;)V
    .locals 2

    .prologue
    .line 1969646
    iget-object v0, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setClickable(Z)V

    .line 1969647
    iget-object v0, p0, LX/D8z;->o:LX/IvM;

    if-eqz v0, :cond_0

    .line 1969648
    iget-object v1, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v1}, LX/0wc;->a(Landroid/view/View;)V

    .line 1969649
    iget-object v1, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v1}, LX/0wc;->a(Landroid/view/View;)V

    .line 1969650
    :cond_0
    return-void
.end method

.method public static i$redex0(LX/D8z;)V
    .locals 2

    .prologue
    .line 1969642
    iget-object v0, p0, LX/D8z;->o:LX/IvM;

    if-eqz v0, :cond_0

    .line 1969643
    iget-object v1, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v1}, LX/0wc;->a(Landroid/view/View;)V

    .line 1969644
    iget-object v1, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v1}, LX/0wc;->a(Landroid/view/View;)V

    .line 1969645
    :cond_0
    return-void
.end method

.method public static j$redex0(LX/D8z;)V
    .locals 2

    .prologue
    .line 1969635
    sget-object v0, LX/D8x;->HIDDEN:LX/D8x;

    iput-object v0, p0, LX/D8z;->r:LX/D8x;

    .line 1969636
    iget-object v0, p0, LX/D8z;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1969637
    iget-object v0, p0, LX/D8z;->o:LX/IvM;

    if-eqz v0, :cond_0

    .line 1969638
    iget-object v1, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 1969639
    iget-object v1, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 1969640
    iget-object v0, p0, LX/D8z;->o:LX/IvM;

    invoke-virtual {v0}, LX/IvM;->a()V

    .line 1969641
    :cond_0
    return-void
.end method

.method public static k(LX/D8z;)V
    .locals 2

    .prologue
    .line 1969628
    sget-object v0, LX/D8x;->REVEALED:LX/D8x;

    iput-object v0, p0, LX/D8z;->r:LX/D8x;

    .line 1969629
    iget-object v0, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setClickable(Z)V

    .line 1969630
    invoke-virtual {p0}, LX/D8z;->requestFocus()Z

    .line 1969631
    iget-object v0, p0, LX/D8z;->o:LX/IvM;

    if-eqz v0, :cond_0

    .line 1969632
    iget-object v1, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 1969633
    iget-object v1, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 1969634
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Z)LX/D8z;
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1969674
    invoke-static {p0}, LX/D8z;->e(LX/D8z;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1969675
    :goto_0
    return-object p0

    .line 1969676
    :cond_0
    iget-boolean v0, p0, LX/D8z;->k:Z

    if-nez v0, :cond_1

    .line 1969677
    iput-boolean v6, p0, LX/D8z;->l:Z

    .line 1969678
    new-instance v0, Lcom/facebook/widget/dialog/CustomDialog$4;

    invoke-direct {v0, p0, p1}, Lcom/facebook/widget/dialog/CustomDialog$4;-><init>(LX/D8z;Z)V

    iput-object v0, p0, LX/D8z;->m:Ljava/lang/Runnable;

    goto :goto_0

    .line 1969679
    :cond_1
    sget-object v0, LX/D8x;->REVEALING:LX/D8x;

    iput-object v0, p0, LX/D8z;->r:LX/D8x;

    .line 1969680
    iget-boolean v0, p0, LX/D8z;->n:Z

    if-nez v0, :cond_2

    .line 1969681
    const-wide/16 v9, 0x0

    .line 1969682
    iget-object v7, p0, LX/D8z;->d:LX/0wd;

    invoke-virtual {v7, v9, v10}, LX/0wd;->b(D)LX/0wd;

    move-result-object v7

    invoke-virtual {v7, v9, v10}, LX/0wd;->a(D)LX/0wd;

    move-result-object v7

    invoke-virtual {v7}, LX/0wd;->j()LX/0wd;

    .line 1969683
    iget-object v7, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/facebook/widget/CustomFrameLayout;->setAlpha(F)V

    .line 1969684
    iget-object v7, p0, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {p0}, LX/D8z;->getHeight()I

    move-result v8

    int-to-float v8, v8

    const v9, 0x3f8ccccd    # 1.1f

    mul-float/2addr v8, v9

    invoke-virtual {v7, v8}, Landroid/view/View;->setTranslationY(F)V

    .line 1969685
    iget-object v7, p0, LX/D8z;->e:Landroid/view/View;

    const/high16 v8, 0x41c80000    # 25.0f

    invoke-virtual {v7, v8}, Landroid/view/View;->setRotation(F)V

    .line 1969686
    iput-boolean v6, p0, LX/D8z;->n:Z

    .line 1969687
    :cond_2
    iget-object v0, p0, LX/D8z;->f:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1969688
    if-eqz p1, :cond_3

    .line 1969689
    iget-object v0, p0, LX/D8z;->d:LX/0wd;

    .line 1969690
    iput-boolean v5, v0, LX/0wd;->c:Z

    .line 1969691
    move-object v0, v0

    .line 1969692
    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    .line 1969693
    :cond_3
    invoke-static {p0}, LX/D8z;->i$redex0(LX/D8z;)V

    .line 1969694
    iget-object v0, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setAlpha(F)V

    .line 1969695
    iget-object v0, p0, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 1969696
    iget-object v0, p0, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setRotation(F)V

    .line 1969697
    iget-object v0, p0, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 1969698
    iget-object v0, p0, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    .line 1969699
    iget-object v0, p0, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1969700
    iget-object v0, p0, LX/D8z;->d:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1969701
    invoke-static {p0}, LX/D8z;->k(LX/D8z;)V

    goto/16 :goto_0
.end method

.method public final a(I)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1969625
    iget-object v2, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomFrameLayout;->getChildCount()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 1969626
    iget-object v0, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 1969627
    goto :goto_0
.end method

.method public final b(Z)LX/D8z;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1969608
    iget-object v0, p0, LX/D8z;->r:LX/D8x;

    sget-object v1, LX/D8x;->HIDING:LX/D8x;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/D8z;->r:LX/D8x;

    sget-object v1, LX/D8x;->HIDDEN:LX/D8x;

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1969609
    if-eqz v0, :cond_1

    .line 1969610
    :goto_1
    return-object p0

    .line 1969611
    :cond_1
    sget-object v0, LX/D8x;->HIDING:LX/D8x;

    iput-object v0, p0, LX/D8z;->r:LX/D8x;

    .line 1969612
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D8z;->l:Z

    .line 1969613
    const/4 v0, 0x0

    iput-object v0, p0, LX/D8z;->m:Ljava/lang/Runnable;

    .line 1969614
    if-eqz p1, :cond_2

    .line 1969615
    iget-object v0, p0, LX/D8z;->d:LX/0wd;

    const/4 v1, 0x1

    .line 1969616
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1969617
    move-object v0, v0

    .line 1969618
    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    goto :goto_1

    .line 1969619
    :cond_2
    invoke-static {p0}, LX/D8z;->h$redex0(LX/D8z;)V

    .line 1969620
    iget-object v0, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setAlpha(F)V

    .line 1969621
    iget-object v0, p0, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {p0}, LX/D8z;->getHeight()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3f8ccccd    # 1.1f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 1969622
    iget-object v0, p0, LX/D8z;->e:Landroid/view/View;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    .line 1969623
    iget-object v0, p0, LX/D8z;->d:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1969624
    invoke-static {p0}, LX/D8z;->j$redex0(LX/D8z;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1eeaca4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1969605
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1969606
    iget-object v1, p0, LX/D8z;->d:LX/0wd;

    iget-object v2, p0, LX/D8z;->g:LX/D8w;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1969607
    const/16 v1, 0x2d

    const v2, 0x6fc11457

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1969603
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D8z;->n:Z

    .line 1969604
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xd998390

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1969600
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1969601
    iget-object v1, p0, LX/D8z;->d:LX/0wd;

    iget-object v2, p0, LX/D8z;->g:LX/D8w;

    invoke-virtual {v1, v2}, LX/0wd;->b(LX/0xi;)LX/0wd;

    .line 1969602
    const/16 v1, 0x2d

    const v2, -0x12949c76

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1969595
    iget-object v0, p0, LX/D8z;->r:LX/D8x;

    sget-object p2, LX/D8x;->REVEALED:LX/D8x;

    if-ne v0, p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1969596
    if-eqz v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/D8z;->o:LX/IvM;

    if-eqz v0, :cond_0

    .line 1969597
    iget-object v0, p0, LX/D8z;->o:LX/IvM;

    invoke-virtual {v0}, LX/IvM;->c()V

    .line 1969598
    const/4 v0, 0x1

    .line 1969599
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 1969589
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1969590
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D8z;->k:Z

    .line 1969591
    iget-boolean v0, p0, LX/D8z;->l:Z

    if-eqz v0, :cond_0

    .line 1969592
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D8z;->l:Z

    .line 1969593
    iget-object v0, p0, LX/D8z;->m:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/D8z;->post(Ljava/lang/Runnable;)Z

    .line 1969594
    :cond_0
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1969587
    iget-object v0, p0, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v0, p1}, LX/D8z;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1969588
    return-void
.end method

.method public setContainerLayout(Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 1

    .prologue
    .line 1969585
    iget-object v0, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1969586
    return-void
.end method
