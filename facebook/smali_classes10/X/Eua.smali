.class public final LX/Eua;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Eub;


# direct methods
.method public constructor <init>(LX/Eub;)V
    .locals 0

    .prologue
    .line 2179734
    iput-object p1, p0, LX/Eua;->a:LX/Eub;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2179735
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2179736
    if-eqz p1, :cond_0

    .line 2179737
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179738
    if-eqz v0, :cond_0

    .line 2179739
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179740
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel$SearchResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2179741
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179742
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel$SearchResultsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2179743
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2179744
    :goto_0
    return-object v0

    .line 2179745
    :cond_1
    iget-object v1, p0, LX/Eua;->a:LX/Eub;

    .line 2179746
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179747
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel$SearchResultsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/Eub;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2179748
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179749
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
