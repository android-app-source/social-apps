.class public LX/DHp;
.super LX/DHn;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1982542
    const v0, 0x7f0313f9

    invoke-direct {p0, p1, v0}, LX/DHp;-><init>(Landroid/content/Context;I)V

    .line 1982543
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1982532
    invoke-direct {p0, p1, p2}, LX/DHn;-><init>(Landroid/content/Context;I)V

    .line 1982533
    const v0, 0x7f0d1507

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/DHp;->a:Landroid/widget/LinearLayout;

    .line 1982534
    const v0, 0x7f0d0539

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    iput-object v0, p0, LX/DHp;->b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    .line 1982535
    return-void
.end method


# virtual methods
.method public getInlineVideoAttachmentView()Lcom/facebook/feedplugins/video/RichVideoAttachmentView;
    .locals 1

    .prologue
    .line 1982541
    iget-object v0, p0, LX/DHp;->b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    return-object v0
.end method

.method public setWidth(I)V
    .locals 2

    .prologue
    .line 1982536
    iget-object v0, p0, LX/DHp;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1982537
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1982538
    iget-object v1, p0, LX/DHp;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1982539
    iget-object v0, p0, LX/DHp;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 1982540
    return-void
.end method
