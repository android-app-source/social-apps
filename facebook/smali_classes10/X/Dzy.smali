.class public LX/Dzy;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/Locale;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;Ljava/util/Locale;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2067685
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2067686
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Dzy;->d:Ljava/util/List;

    .line 2067687
    iput-object p1, p0, LX/Dzy;->a:Landroid/content/Context;

    .line 2067688
    iput-object p2, p0, LX/Dzy;->b:Landroid/view/LayoutInflater;

    .line 2067689
    iput-object p3, p0, LX/Dzy;->c:Ljava/util/Locale;

    .line 2067690
    return-void
.end method

.method private a(I)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .locals 1

    .prologue
    .line 2067684
    iget-object v0, p0, LX/Dzy;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    return-object v0
.end method

.method private b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2067691
    iget-object v0, p0, LX/Dzy;->c:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    .line 2067692
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->l()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2067693
    :goto_0
    iget-object v3, p0, LX/Dzy;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00a8

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    int-to-long v6, v0

    invoke-virtual {v2, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2067694
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->l()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;->a()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2067683
    iget-object v0, p0, LX/Dzy;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2067682
    invoke-direct {p0, p1}, LX/Dzy;->a(I)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2067681
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2067664
    invoke-virtual {p0, p1}, LX/Dzy;->getItemViewType(I)I

    .line 2067665
    iget-object v0, p0, LX/Dzy;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0303aa

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2067666
    invoke-direct {p0, p1}, LX/Dzy;->a(I)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    .line 2067667
    const v0, 0x7f0d0b9c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2067668
    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 2067669
    invoke-static {v3}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v5

    .line 2067670
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2067671
    new-instance p1, Landroid/text/style/StyleSpan;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 p2, 0x0

    const/16 p3, 0x11

    invoke-virtual {v6, p1, p2, v5, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2067672
    move-object v3, v6

    .line 2067673
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2067674
    invoke-static {v2}, LX/5m4;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Ljava/lang/String;

    move-result-object v3

    .line 2067675
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2067676
    :goto_0
    move-object v3, v3

    .line 2067677
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2067678
    invoke-direct {p0, v2}, LX/Dzy;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2067679
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2067680
    return-object v1

    :cond_0
    const-string v3, ""

    goto :goto_0
.end method
