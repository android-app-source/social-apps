.class public LX/E8M;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/reaction/common/ReactionCardNode;

.field private b:Landroid/content/Context;

.field public c:LX/CXm;

.field public d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

.field public e:LX/2ja;

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:LX/CXj;

.field public i:LX/7va;

.field public j:LX/1Ck;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/CXj;LX/7va;LX/1Ck;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2082326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2082327
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/E8M;->f:Z

    .line 2082328
    iput-object p1, p0, LX/E8M;->b:Landroid/content/Context;

    .line 2082329
    iput-object p2, p0, LX/E8M;->h:LX/CXj;

    .line 2082330
    iput-object p3, p0, LX/E8M;->i:LX/7va;

    .line 2082331
    iput-object p4, p0, LX/E8M;->j:LX/1Ck;

    .line 2082332
    return-void
.end method

.method public static b(LX/0QB;)LX/E8M;
    .locals 5

    .prologue
    .line 2082333
    new-instance v4, LX/E8M;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v1

    check-cast v1, LX/CXj;

    invoke-static {p0}, LX/7va;->b(LX/0QB;)LX/7va;

    move-result-object v2

    check-cast v2, LX/7va;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-direct {v4, v0, v1, v2, v3}, LX/E8M;-><init>(Landroid/content/Context;LX/CXj;LX/7va;LX/1Ck;)V

    .line 2082334
    return-object v4
.end method

.method public static b$redex0(LX/E8M;Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 2082335
    iget-boolean v0, p0, LX/E8M;->f:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0830fa

    .line 2082336
    :goto_0
    iget-boolean v1, p0, LX/E8M;->f:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0a00d2

    .line 2082337
    :goto_1
    iget-object v2, p0, LX/E8M;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2082338
    iget-object v0, p0, LX/E8M;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2082339
    iget-object v0, p0, LX/E8M;->d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/9sr;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/9sr;

    move-result-object v0

    iget-boolean v1, p0, LX/E8M;->f:Z

    .line 2082340
    iput-boolean v1, v0, LX/9sr;->aa:Z

    .line 2082341
    move-object v0, v0

    .line 2082342
    invoke-virtual {v0}, LX/9sr;->a()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 2082343
    iget-object v1, p0, LX/E8M;->a:Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2082344
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionCardNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    .line 2082345
    new-instance p1, LX/9qs;

    invoke-direct {p1}, LX/9qs;-><init>()V

    .line 2082346
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    iput-object p0, p1, LX/9qs;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2082347
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object p0

    iput-object p0, p1, LX/9qs;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2082348
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    move-result-object p0

    iput-object p0, p1, LX/9qs;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 2082349
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->l()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    move-result-object p0

    iput-object p0, p1, LX/9qs;->d:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionImageFieldsModel;

    .line 2082350
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object p0

    iput-object p0, p1, LX/9qs;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2082351
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->gX_()LX/0Px;

    move-result-object p0

    iput-object p0, p1, LX/9qs;->f:LX/0Px;

    .line 2082352
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->gY_()LX/0Px;

    move-result-object p0

    iput-object p0, p1, LX/9qs;->g:LX/0Px;

    .line 2082353
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->n()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object p0

    iput-object p0, p1, LX/9qs;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2082354
    move-object v2, p1

    .line 2082355
    iput-object v0, v2, LX/9qs;->b:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2082356
    move-object v2, v2

    .line 2082357
    invoke-virtual {v2}, LX/9qs;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v2

    .line 2082358
    iget-object p1, v1, Lcom/facebook/reaction/common/ReactionCardNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-static {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object p1

    invoke-static {p1}, LX/9qr;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)LX/9qr;

    move-result-object p1

    .line 2082359
    iput-object v2, p1, LX/9qr;->j:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    .line 2082360
    move-object v2, p1

    .line 2082361
    invoke-virtual {v2}, LX/9qr;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/reaction/common/ReactionCardNode;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2082362
    return-void

    .line 2082363
    :cond_0
    const v0, 0x7f0830f8

    goto/16 :goto_0

    .line 2082364
    :cond_1
    const v1, 0x7f0a010e

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/common/ReactionCardNode;Landroid/widget/TextView;LX/2ja;)V
    .locals 5

    .prologue
    .line 2082365
    iput-object p1, p0, LX/E8M;->a:Lcom/facebook/reaction/common/ReactionCardNode;

    .line 2082366
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/E8M;->d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    .line 2082367
    iput-object p3, p0, LX/E8M;->e:LX/2ja;

    .line 2082368
    iget-object v0, p0, LX/E8M;->d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E8M;->d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E8M;->d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2082369
    :cond_0
    :goto_0
    return-void

    .line 2082370
    :cond_1
    iget-object v0, p0, LX/E8M;->d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ab()Z

    move-result v0

    iput-boolean v0, p0, LX/E8M;->f:Z

    .line 2082371
    iget-object v0, p0, LX/E8M;->d:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitHeaderFieldsModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/E8M;->g:Ljava/lang/String;

    .line 2082372
    invoke-static {p0, p2}, LX/E8M;->b$redex0(LX/E8M;Landroid/widget/TextView;)V

    .line 2082373
    new-instance v0, LX/E8J;

    invoke-direct {v0, p0, p2}, LX/E8J;-><init>(LX/E8M;Landroid/widget/TextView;)V

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2082374
    new-instance v1, LX/E8K;

    iget-object v2, p0, LX/E8M;->g:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, p0, v2, p2}, LX/E8K;-><init>(LX/E8M;Ljava/lang/Long;Landroid/widget/TextView;)V

    iput-object v1, p0, LX/E8M;->c:LX/CXm;

    .line 2082375
    iget-object v1, p0, LX/E8M;->h:LX/CXj;

    iget-object v2, p0, LX/E8M;->c:LX/CXm;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2082376
    goto :goto_0
.end method
