.class public LX/E6L;
.super Lcom/facebook/widget/PhotoButton;
.source ""


# instance fields
.field public a:LX/E6O;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/E6N;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2080022
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/E6L;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2080023
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2080015
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/PhotoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2080016
    const-class v0, LX/E6L;

    invoke-static {v0, p0}, LX/E6L;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2080017
    const v0, 0x7f020726

    invoke-virtual {p0, v0}, LX/E6L;->setImageResource(I)V

    .line 2080018
    invoke-virtual {p0}, LX/E6L;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1621

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 2080019
    invoke-virtual {p0, v0, v0, v0, v0}, LX/E6L;->setPadding(IIII)V

    .line 2080020
    new-instance v0, LX/E6K;

    invoke-direct {v0, p0}, LX/E6K;-><init>(LX/E6L;)V

    invoke-virtual {p0, v0}, LX/E6L;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080021
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/E6L;

    const-class p0, LX/E6O;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/E6O;

    iput-object v1, p1, LX/E6L;->a:LX/E6O;

    return-void
.end method


# virtual methods
.method public final a(LX/2jY;Landroid/support/v4/app/Fragment;)Z
    .locals 12

    .prologue
    .line 2080010
    iget-object v0, p0, LX/E6L;->a:LX/E6O;

    invoke-virtual {p0}, LX/E6L;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2080011
    new-instance v2, LX/E6N;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v6

    check-cast v6, LX/2d1;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v7

    check-cast v7, LX/17W;

    invoke-static {v0}, LX/CeT;->a(LX/0QB;)LX/CeT;

    move-result-object v8

    check-cast v8, LX/CeT;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x12c4

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    move-object v3, p2

    move-object v4, p1

    move-object v5, v1

    invoke-direct/range {v2 .. v11}, LX/E6N;-><init>(Landroid/support/v4/app/Fragment;LX/2jY;Landroid/content/Context;LX/2d1;LX/17W;LX/CeT;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0SG;)V

    .line 2080012
    move-object v0, v2

    .line 2080013
    iput-object v0, p0, LX/E6L;->b:LX/E6N;

    .line 2080014
    iget-object v0, p0, LX/E6L;->b:LX/E6N;

    invoke-virtual {v0}, LX/E6N;->a()Z

    move-result v0

    return v0
.end method
