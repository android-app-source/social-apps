.class public final LX/E24;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E25;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/5sY;

.field public b:LX/9qL;

.field public c:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

.field public d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

.field public final synthetic e:LX/E25;


# direct methods
.method public constructor <init>(LX/E25;)V
    .locals 1

    .prologue
    .line 2072167
    iput-object p1, p0, LX/E24;->e:LX/E25;

    .line 2072168
    move-object v0, p1

    .line 2072169
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2072170
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2072191
    const-string v0, "ReactionCoreImageTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2072171
    if-ne p0, p1, :cond_1

    .line 2072172
    :cond_0
    :goto_0
    return v0

    .line 2072173
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2072174
    goto :goto_0

    .line 2072175
    :cond_3
    check-cast p1, LX/E24;

    .line 2072176
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2072177
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2072178
    if-eq v2, v3, :cond_0

    .line 2072179
    iget-object v2, p0, LX/E24;->a:LX/5sY;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E24;->a:LX/5sY;

    iget-object v3, p1, LX/E24;->a:LX/5sY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2072180
    goto :goto_0

    .line 2072181
    :cond_5
    iget-object v2, p1, LX/E24;->a:LX/5sY;

    if-nez v2, :cond_4

    .line 2072182
    :cond_6
    iget-object v2, p0, LX/E24;->b:LX/9qL;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E24;->b:LX/9qL;

    iget-object v3, p1, LX/E24;->b:LX/9qL;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2072183
    goto :goto_0

    .line 2072184
    :cond_8
    iget-object v2, p1, LX/E24;->b:LX/9qL;

    if-nez v2, :cond_7

    .line 2072185
    :cond_9
    iget-object v2, p0, LX/E24;->c:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E24;->c:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    iget-object v3, p1, LX/E24;->c:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2072186
    goto :goto_0

    .line 2072187
    :cond_b
    iget-object v2, p1, LX/E24;->c:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    if-nez v2, :cond_a

    .line 2072188
    :cond_c
    iget-object v2, p0, LX/E24;->d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/E24;->d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    iget-object v3, p1, LX/E24;->d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2072189
    goto :goto_0

    .line 2072190
    :cond_d
    iget-object v2, p1, LX/E24;->d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
