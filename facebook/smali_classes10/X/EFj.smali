.class public final LX/EFj;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field public d:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2096168
    const-class v0, LX/EFj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EFj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2096169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096170
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2096171
    iput-object v0, p0, LX/EFj;->c:LX/0Ot;

    .line 2096172
    return-void
.end method

.method public static a(LX/7TQ;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2096173
    sget-object v0, LX/EFh;->a:[I

    invoke-virtual {p0}, LX/7TQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2096174
    sget-object v0, LX/EFj;->a:Ljava/lang/String;

    const-string v1, "New end call reason not added to logging: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, LX/7TQ;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2096175
    invoke-virtual {p0}, LX/7TQ;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2096176
    :pswitch_0
    const-string v0, "IgnoreCall"

    goto :goto_0

    .line 2096177
    :pswitch_1
    const-string v0, "HangupCall"

    goto :goto_0

    .line 2096178
    :pswitch_2
    const-string v0, "InAnotherCall"

    goto :goto_0

    .line 2096179
    :pswitch_3
    const-string v0, "CallEndAcceptAfterHangUp"

    goto :goto_0

    .line 2096180
    :pswitch_4
    const-string v0, "NoAnswerTimeout"

    goto :goto_0

    .line 2096181
    :pswitch_5
    const-string v0, "IncomingTimeout"

    goto :goto_0

    .line 2096182
    :pswitch_6
    const-string v0, "OtherInstanceHandled"

    goto :goto_0

    .line 2096183
    :pswitch_7
    const-string v0, "SignalingMessageFailed"

    goto :goto_0

    .line 2096184
    :pswitch_8
    const-string v0, "ConnectionDropped"

    goto :goto_0

    .line 2096185
    :pswitch_9
    const-string v0, "ClientInterrupted"

    goto :goto_0

    .line 2096186
    :pswitch_a
    const-string v0, "WebRTCError"

    goto :goto_0

    .line 2096187
    :pswitch_b
    const-string v0, "ClientError"

    goto :goto_0

    .line 2096188
    :pswitch_c
    const-string v0, "NoPermission"

    goto :goto_0

    .line 2096189
    :pswitch_d
    const-string v0, "OtherNotCapable"

    goto :goto_0

    .line 2096190
    :pswitch_e
    const-string v0, "NoUIShown"

    goto :goto_0

    .line 2096191
    :pswitch_f
    const-string v0, "VersionUnsupported"

    goto :goto_0

    .line 2096192
    :pswitch_10
    const-string v0, "CallerNotVisible"

    goto :goto_0

    .line 2096193
    :pswitch_11
    const-string v0, "CarrierBlocked"

    goto :goto_0

    .line 2096194
    :pswitch_12
    const-string v0, "OtherCarrierBlocked"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_9
        :pswitch_c
        :pswitch_5
        :pswitch_d
        :pswitch_12
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_8
        :pswitch_2
        :pswitch_11
        :pswitch_1
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method

.method public static a(LX/EFj;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2096195
    iget-object v0, p0, LX/EFj;->d:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2096196
    const v0, 0x7f08073a

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2096197
    iget-object v0, p0, LX/EFj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2096198
    iget-object v0, p0, LX/EFj;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2096199
    const v0, 0x7f0807a4

    .line 2096200
    :goto_0
    iget-object v1, p0, LX/EFj;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2096201
    :cond_0
    const v0, 0x7f0807a3

    goto :goto_0

    .line 2096202
    :cond_1
    iget-object v0, p0, LX/EFj;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2096203
    const v0, 0x7f08072a

    goto :goto_0

    .line 2096204
    :cond_2
    const v0, 0x7f080711

    goto :goto_0
.end method

.method public final a(LX/7TQ;LX/EFi;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2096205
    sget-object v0, LX/EFh;->a:[I

    invoke-virtual {p1}, LX/7TQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2096206
    :goto_0
    invoke-direct {p0}, LX/EFj;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 2096207
    :pswitch_0
    iget-object v0, p0, LX/EFj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2096208
    iget-boolean v1, v0, LX/EDx;->aB:Z

    move v0, v1

    .line 2096209
    if-eqz v0, :cond_0

    .line 2096210
    sget-object v0, LX/EFh;->b:[I

    invoke-virtual {p2}, LX/EFi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2096211
    const v0, 0x7f08073e

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v0, v0

    .line 2096212
    goto :goto_1

    .line 2096213
    :cond_0
    invoke-direct {p0}, LX/EFj;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2096214
    :pswitch_1
    sget-object v0, LX/EFh;->b:[I

    invoke-virtual {p2}, LX/EFi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    .line 2096215
    const v0, 0x7f08073b

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    move-object v0, v0

    .line 2096216
    goto :goto_1

    .line 2096217
    :pswitch_2
    sget-object v0, LX/EFh;->b:[I

    invoke-virtual {p2}, LX/EFi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    .line 2096218
    iget-object v0, p0, LX/EFj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->az()Ljava/lang/String;

    move-result-object v0

    .line 2096219
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2096220
    iget-object v0, p0, LX/EFj;->d:Landroid/content/Context;

    const v1, 0x7f0807ea

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2096221
    :goto_4
    move-object v0, v0

    .line 2096222
    goto :goto_1

    .line 2096223
    :pswitch_3
    iget-object v0, p0, LX/EFj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2096224
    iget-boolean v1, v0, LX/EDx;->aB:Z

    move v0, v1

    .line 2096225
    if-eqz v0, :cond_1

    .line 2096226
    sget-object v0, LX/EFh;->b:[I

    invoke-virtual {p2}, LX/EFi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    .line 2096227
    const v0, 0x7f08073d

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    :goto_5
    move-object v0, v0

    .line 2096228
    goto/16 :goto_1

    .line 2096229
    :cond_1
    invoke-direct {p0}, LX/EFj;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2096230
    :pswitch_4
    sget-object v0, LX/EFh;->b:[I

    invoke-virtual {p2}, LX/EFi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    .line 2096231
    const v0, 0x7f08073c

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    :goto_6
    move-object v0, v0

    .line 2096232
    goto/16 :goto_1

    .line 2096233
    :pswitch_5
    sget-object v0, LX/EFh;->b:[I

    invoke-virtual {p2}, LX/EFi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6

    .line 2096234
    const v0, 0x7f080740

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    :goto_7
    move-object v0, v0

    .line 2096235
    goto/16 :goto_1

    .line 2096236
    :pswitch_6
    sget-object v0, LX/EFh;->b:[I

    invoke-virtual {p2}, LX/EFi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7

    .line 2096237
    iget-object v1, p0, LX/EFj;->d:Landroid/content/Context;

    const v2, 0x7f08073f

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-object v0, p0, LX/EFj;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ay()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, p1

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_8
    move-object v0, v0

    .line 2096238
    goto/16 :goto_1

    .line 2096239
    :pswitch_7
    sget-object v0, LX/EFh;->b:[I

    invoke-virtual {p2}, LX/EFi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_8

    .line 2096240
    const v0, 0x7f080741

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    .line 2096241
    :goto_9
    goto/16 :goto_0

    .line 2096242
    :pswitch_8
    const v0, 0x7f080733

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 2096243
    :pswitch_9
    const v0, 0x7f080731

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2096244
    :pswitch_a
    const v0, 0x7f080732

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 2096245
    :cond_2
    iget-object v1, p0, LX/EFj;->d:Landroid/content/Context;

    const v2, 0x7f0807e9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v0, v3, p1

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 2096246
    :pswitch_b
    const v0, 0x7f080732

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 2096247
    :pswitch_c
    const v0, 0x7f080734

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    .line 2096248
    :pswitch_d
    const v0, 0x7f080730

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 2096249
    :pswitch_e
    const v0, 0x7f08072e

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    .line 2096250
    :pswitch_f
    const v0, 0x7f08072f

    invoke-static {p0, v0}, LX/EFj;->a(LX/EFj;I)Ljava/lang/String;

    goto :goto_9

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_a
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_b
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_c
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_d
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x1
        :pswitch_e
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_f
    .end packed-switch
.end method
