.class public final LX/Ds9;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/Ds8;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/3Cl;


# direct methods
.method public constructor <init>(LX/3Cl;LX/Ds8;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2050093
    iput-object p1, p0, LX/Ds9;->c:LX/3Cl;

    iput-object p2, p0, LX/Ds9;->a:LX/Ds8;

    iput-object p3, p0, LX/Ds9;->b:Landroid/view/View;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2050094
    iget-object v0, p0, LX/Ds9;->a:LX/Ds8;

    .line 2050095
    iget-object v1, v0, LX/Ds8;->e:LX/3Cl;

    iget-object v1, v1, LX/3Cl;->e:LX/2jO;

    invoke-virtual {v1}, LX/2jO;->a()V

    .line 2050096
    iget-object v2, v0, LX/Ds8;->a:LX/3Cv;

    sget-object v3, LX/3Cw;->NOTIFICATION:LX/3Cw;

    const/4 v4, 0x1

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v3, v4, v1}, LX/3Cv;->a(LX/3Cw;ZLandroid/graphics/Point;)V

    .line 2050097
    iget-object v1, v0, LX/Ds8;->e:LX/3Cl;

    iget-object v1, v1, LX/3Cl;->d:LX/33W;

    iget-object v2, v0, LX/Ds8;->b:LX/DqC;

    .line 2050098
    iget-object v3, v2, LX/DqC;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2050099
    const-string v3, "long_press"

    iget-object v4, v0, LX/Ds8;->c:Lcom/facebook/graphql/model/GraphQLStory;

    iget p0, v0, LX/Ds8;->d:I

    invoke-virtual {v1, v2, v3, v4, p0}, LX/33W;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;I)V

    .line 2050100
    return-void

    .line 2050101
    :cond_0
    const v1, 0x7f0d002b

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Point;

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2050102
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2050103
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2050104
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 2050105
    iget-object v0, p0, LX/Ds9;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a00d6

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2050106
    return-void
.end method
