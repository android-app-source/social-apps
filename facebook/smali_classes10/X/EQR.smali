.class public LX/EQR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2118608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Landroid/text/SpannableStringBuilder;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/16 v5, 0x21

    const/4 v1, 0x0

    .line 2118544
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez p0, :cond_1

    .line 2118545
    :cond_0
    :goto_0
    return-object v0

    .line 2118546
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2118547
    invoke-virtual {p0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    .line 2118548
    invoke-virtual {v2}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    .line 2118549
    :goto_1
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2118550
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2118551
    invoke-virtual {p0, v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2118552
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v3, v2, v1, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2118553
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2118554
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2118555
    if-eqz p3, :cond_2

    .line 2118556
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v0, p4, p3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 2118557
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v3, v0, v1, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    :goto_2
    move-object v0, v3

    .line 2118558
    goto :goto_0

    :cond_3
    move-object v2, v0

    move v0, v1

    .line 2118559
    goto :goto_1

    .line 2118560
    :cond_4
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;IILandroid/content/Context;)Landroid/text/SpannableStringBuilder;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v4, 0x21

    .line 2118596
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2118597
    const/4 v0, 0x0

    .line 2118598
    :cond_0
    :goto_0
    return-object v0

    .line 2118599
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2118600
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v1, p4, p2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 2118601
    const/4 v2, 0x0

    invoke-static {p0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2118602
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2118603
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2118604
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2118605
    if-eqz p3, :cond_0

    .line 2118606
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v1, p4, p3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 2118607
    invoke-static {p0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public static a(ZZLandroid/text/SpannableStringBuilder;Ljava/lang/String;IILandroid/content/Context;IILX/0Or;)Landroid/text/SpannableStringBuilder;
    .locals 9
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/timeline/util/IsWorkUserBadgeEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Landroid/text/SpannableStringBuilder;",
            "Ljava/lang/String;",
            "II",
            "Landroid/content/Context;",
            "II",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    .line 2118574
    const/4 v2, 0x0

    .line 2118575
    const/4 v1, 0x0

    .line 2118576
    if-eqz p0, :cond_6

    .line 2118577
    const/4 v2, 0x1

    move v7, v2

    .line 2118578
    :goto_0
    if-eqz p1, :cond_0

    sget-object v2, LX/03R;->YES:LX/03R;

    invoke-interface/range {p9 .. p9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2118579
    const/4 v1, 0x1

    .line 2118580
    :cond_0
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2118581
    :cond_1
    :goto_1
    return-object p2

    .line 2118582
    :cond_2
    if-nez v7, :cond_3

    if-eqz v1, :cond_1

    .line 2118583
    :cond_3
    const-string v1, "\u2060"

    invoke-virtual {p2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2118584
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 2118585
    if-eqz p3, :cond_5

    :goto_2
    invoke-virtual {p2, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2118586
    invoke-virtual {p6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v7, :cond_4

    move p5, p4

    :cond_4
    invoke-virtual {v1, p5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2118587
    new-instance v1, Landroid/graphics/drawable/InsetDrawable;

    const/4 v3, 0x0

    move/from16 v0, p8

    neg-int v4, v0

    const/4 v5, 0x0

    move/from16 v6, p8

    invoke-direct/range {v1 .. v6}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 2118588
    const/4 v3, 0x0

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    add-int v4, v4, p7

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    move/from16 v0, p7

    invoke-virtual {v1, v0, v3, v4, v2}, Landroid/graphics/drawable/InsetDrawable;->setBounds(IIII)V

    .line 2118589
    new-instance v2, Landroid/text/style/ImageSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const/16 v3, 0x21

    invoke-virtual {p2, v2, v8, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2118590
    if-eqz v7, :cond_1

    .line 2118591
    invoke-static {p4}, LX/EQR;->a(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2118592
    if-eqz v1, :cond_1

    .line 2118593
    new-instance v2, LX/EQQ;

    invoke-direct {v2, v1}, LX/EQQ;-><init>(Ljava/lang/Integer;)V

    .line 2118594
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const/16 v3, 0x21

    invoke-virtual {p2, v2, v8, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 2118595
    :cond_5
    const-string p3, "[badge]"

    goto :goto_2

    :cond_6
    move v7, v2

    goto :goto_0
.end method

.method private static a(I)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 2118568
    const v0, 0x7f0213c3

    if-ne p0, v0, :cond_0

    .line 2118569
    const v0, 0x7f08177b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2118570
    :goto_0
    return-object v0

    .line 2118571
    :cond_0
    const v0, 0x7f0213c2

    if-ne p0, v0, :cond_1

    .line 2118572
    const v0, 0x7f08177c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2118573
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2118561
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2118562
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2118563
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 2118564
    int-to-float v0, p4

    invoke-virtual {p0, v1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2118565
    :goto_0
    return-void

    .line 2118566
    :cond_0
    int-to-float v0, p3

    invoke-virtual {p0, v1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 2118567
    :cond_1
    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
