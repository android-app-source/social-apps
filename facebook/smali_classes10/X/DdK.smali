.class public final LX/DdK;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/service/model/MarkThreadsParams;

.field public final synthetic b:Lcom/facebook/messaging/cache/ReadThreadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/cache/ReadThreadManager;Lcom/facebook/messaging/service/model/MarkThreadsParams;)V
    .locals 0

    .prologue
    .line 2019442
    iput-object p1, p0, LX/DdK;->b:Lcom/facebook/messaging/cache/ReadThreadManager;

    iput-object p2, p0, LX/DdK;->a:Lcom/facebook/messaging/service/model/MarkThreadsParams;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 10

    .prologue
    .line 2019443
    iget-object v0, p0, LX/DdK;->a:Lcom/facebook/messaging/service/model/MarkThreadsParams;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2019444
    iget-object v1, p0, LX/DdK;->b:Lcom/facebook/messaging/cache/ReadThreadManager;

    iget-object v1, v1, Lcom/facebook/messaging/cache/ReadThreadManager;->g:LX/01J;

    iget-object v5, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v5}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2019445
    if-eqz v1, :cond_0

    iget-wide v6, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    iget-wide v8, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    cmp-long v1, v6, v8

    if-lez v1, :cond_0

    iget-wide v6, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-nez v1, :cond_1

    .line 2019446
    :cond_0
    iget-object v1, p0, LX/DdK;->b:Lcom/facebook/messaging/cache/ReadThreadManager;

    iget-object v1, v1, Lcom/facebook/messaging/cache/ReadThreadManager;->g:LX/01J;

    iget-object v5, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v5, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2019447
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2019448
    :cond_2
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2019449
    return-void
.end method
