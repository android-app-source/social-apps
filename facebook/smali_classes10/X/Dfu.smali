.class public final LX/Dfu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2028245
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2028246
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028247
    :goto_0
    return v1

    .line 2028248
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028249
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2028250
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2028251
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2028252
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2028253
    const-string v5, "client_unit_type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2028254
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInviteClientUnitType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 2028255
    :cond_2
    const-string v5, "contacts"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2028256
    invoke-static {p0, p1}, LX/Dft;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2028257
    :cond_3
    const-string v5, "title"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2028258
    const/4 v4, 0x0

    .line 2028259
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_9

    .line 2028260
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028261
    :goto_2
    move v0, v4

    .line 2028262
    goto :goto_1

    .line 2028263
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2028264
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2028265
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2028266
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2028267
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 2028268
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028269
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 2028270
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2028271
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2028272
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 2028273
    const-string v6, "text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2028274
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 2028275
    :cond_8
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2028276
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2028277
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_9
    move v0, v4

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2028278
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2028279
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2028280
    if-eqz v0, :cond_0

    .line 2028281
    const-string v0, "client_unit_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028282
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2028283
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2028284
    if-eqz v0, :cond_1

    .line 2028285
    const-string v1, "contacts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028286
    invoke-static {p0, v0, p2, p3}, LX/Dft;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2028287
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2028288
    if-eqz v0, :cond_3

    .line 2028289
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028290
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2028291
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2028292
    if-eqz v1, :cond_2

    .line 2028293
    const-string p1, "text"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028294
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2028295
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2028296
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2028297
    return-void
.end method
