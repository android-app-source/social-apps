.class public final LX/DFr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2eJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2eJ",
        "<",
        "LX/DFz;",
        "TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

.field public final synthetic c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;LX/0Px;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)V
    .locals 0

    .prologue
    .line 1978839
    iput-object p1, p0, LX/DFr;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;

    iput-object p2, p0, LX/DFr;->a:LX/0Px;

    iput-object p3, p0, LX/DFr;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1978840
    iget-object v0, p0, LX/DFr;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)LX/1RC;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1RC",
            "<",
            "LX/DFz;",
            "*-TE;*>;"
        }
    .end annotation

    .prologue
    .line 1978838
    iget-object v0, p0, LX/DFr;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;

    return-object v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1978837
    new-instance v1, LX/DFz;

    iget-object v0, p0, LX/DFr;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    iget-object v2, p0, LX/DFr;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    invoke-direct {v1, v0, v2}, LX/DFz;-><init>(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)V

    return-object v1
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 1978834
    iget-object v0, p0, LX/DFr;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    iget-object v1, p0, LX/DFr;->a:LX/0Px;

    invoke-static {v0, v1, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 1978835
    iget-object v0, p0, LX/DFr;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->c:LX/1g4;

    iget-object v1, p0, LX/DFr;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    invoke-virtual {v0, v1, p1}, LX/1g4;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 1978836
    return-void
.end method
