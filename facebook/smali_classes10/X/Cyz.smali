.class public LX/Cyz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cyx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Z

.field private c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1954088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954089
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1954090
    iput-object v0, p0, LX/Cyz;->a:LX/0Ot;

    .line 1954091
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cyz;->b:Z

    .line 1954092
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1954093
    iput-object v0, p0, LX/Cyz;->c:LX/0Rf;

    .line 1954094
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iput-object v0, p0, LX/Cyz;->d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1954095
    return-void
.end method

.method private static a(LX/Cyz;)LX/0m9;
    .locals 4

    .prologue
    .line 1954082
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 1954083
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1954084
    iget-object v0, p0, LX/Cyz;->c:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1954085
    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 1954086
    :cond_0
    const-string v0, "bootstrap_item_count"

    iget-object v3, p0, LX/Cyz;->c:LX/0Rf;

    invoke-virtual {v3}, LX/0Rf;->size()I

    move-result v3

    invoke-virtual {v1, v0, v3}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v0

    const-string v3, "bootstrap_item_ids"

    invoke-virtual {v0, v3, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1954087
    return-object v1
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 2

    .prologue
    .line 1954073
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne p0, v0, :cond_0

    .line 1954074
    sget-object v0, LX/Cyy;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1954075
    :cond_0
    :goto_0
    return-object p0

    .line 1954076
    :pswitch_0
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_USER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 1954077
    :pswitch_1
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 1954078
    :pswitch_2
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 1954079
    :pswitch_3
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 1954080
    :pswitch_4
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    .line 1954081
    :pswitch_5
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lcom/facebook/search/results/model/SearchResultsBridge;)Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ")",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954056
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v4

    .line 1954057
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1954058
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/model/SearchResultsBridge;->k()LX/Cz3;

    move-result-object v0

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v2

    .line 1954059
    invoke-static {v2}, LX/8eM;->g(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Ljava/lang/String;

    move-result-object v3

    .line 1954060
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/model/SearchResultsBridge;->k()LX/Cz3;

    move-result-object v0

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v0

    .line 1954061
    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, LX/0Rc;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1954062
    invoke-virtual {v1}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 1954063
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->l()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 1954064
    new-instance v6, LX/173;

    invoke-direct {v6}, LX/173;-><init>()V

    .line 1954065
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bs()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1954066
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bs()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/173;->a(Ljava/lang/String;)LX/173;

    .line 1954067
    :cond_0
    new-instance v7, LX/4XR;

    invoke-direct {v7}, LX/4XR;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4XR;->e(Ljava/lang/String;)LX/4XR;

    move-result-object v7

    new-instance v8, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;

    invoke-direct {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/model/SearchResultsBridge;->o()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a(I)Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4XR;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/4XR;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4XR;->g(Ljava/lang/String;)LX/4XR;

    move-result-object v7

    new-instance v8, LX/2dc;

    invoke-direct {v8}, LX/2dc;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/2dc;->b(Ljava/lang/String;)LX/2dc;

    move-result-object v8

    invoke-virtual {v8}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4XR;->f(Lcom/facebook/graphql/model/GraphQLImage;)LX/4XR;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->o()Z

    move-result v8

    invoke-virtual {v7, v8}, LX/4XR;->m(Z)LX/4XR;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4XR;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/4XR;

    move-result-object v7

    invoke-virtual {v6}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/4XR;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/4XR;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4XR;->h(Ljava/lang/String;)LX/4XR;

    move-result-object v6

    .line 1954068
    invoke-virtual {v6}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    .line 1954069
    const-string v7, ""

    invoke-virtual {v4, v6, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1954070
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1954071
    :cond_1
    invoke-static {v2}, LX/8eM;->i(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/8dH;

    move-result-object v8

    .line 1954072
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-static {v2}, LX/8eM;->e(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    invoke-static {v2}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/model/SearchResultsBridge;->o()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    if-eqz v8, :cond_2

    invoke-interface {v8}, LX/8dH;->c()Ljava/lang/String;

    move-result-object v7

    :goto_1
    if-eqz v8, :cond_4

    invoke-interface {v8}, LX/8dH;->j()LX/8dG;

    move-result-object v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, LX/8dH;->j()LX/8dG;

    move-result-object v8

    invoke-interface {v8}, LX/8dG;->a()Ljava/lang/String;

    move-result-object v8

    :goto_2
    const/4 v9, 0x0

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/model/SearchResultsBridge;->m()LX/0am;

    move-result-object v12

    invoke-virtual {v12}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v14

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v15

    invoke-direct/range {v0 .. v15}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;LX/0P1;LX/0Px;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;Ljava/lang/Integer;Ljava/lang/String;LX/D0L;LX/0Px;LX/0P1;)V

    return-object v0

    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    :cond_3
    const/4 v8, 0x0

    goto :goto_2

    :cond_4
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public static a(LX/Cyz;LX/0cA;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cA",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1954051
    invoke-virtual {p1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/Cyz;->c:LX/0Rf;

    .line 1954052
    iget-object v0, p0, LX/Cyz;->c:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1954053
    :goto_0
    return-void

    .line 1954054
    :cond_0
    iput-object p2, p0, LX/Cyz;->d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1954055
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cyz;->b:Z

    goto :goto_0
.end method

.method public static a(LX/Cyz;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z
    .locals 4

    .prologue
    .line 1954038
    iget-object v0, p0, LX/Cyz;->c:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1954039
    const/4 v0, 0x0

    .line 1954040
    :goto_0
    return v0

    .line 1954041
    :cond_0
    iget-object v0, p0, LX/Cyz;->d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne p1, v0, :cond_1

    .line 1954042
    const/4 v0, 0x1

    goto :goto_0

    .line 1954043
    :cond_1
    iget-object v0, p0, LX/Cyz;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyx;

    iget-object v1, p0, LX/Cyz;->d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const/4 v3, 0x0

    .line 1954044
    iget-object v2, v0, LX/Cyx;->c:Ljava/util/EnumMap;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    const-string p0, "bootstrap merge roles uninitialized"

    invoke-static {v2, p0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1954045
    iget-object v2, v0, LX/Cyx;->c:Ljava/util/EnumMap;

    invoke-virtual {v2, v1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1954046
    :goto_2
    move v0, v3

    .line 1954047
    goto :goto_0

    :cond_2
    move v2, v3

    .line 1954048
    goto :goto_1

    .line 1954049
    :cond_3
    iget-object v2, v0, LX/Cyx;->c:Ljava/util/EnumMap;

    invoke-virtual {v2, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/EnumSet;

    .line 1954050
    invoke-virtual {v2, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_2
.end method

.method public static b(LX/Cyv;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 2

    .prologue
    .line 1954035
    invoke-interface {p0}, LX/Cyv;->c()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 1954036
    invoke-interface {p0}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v1}, LX/8eM;->e(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    .line 1954037
    invoke-static {v0, v1}, LX/Cyz;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/search/model/SearchResultsBaseFeedUnit;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 3

    .prologue
    .line 1953938
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1953939
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1953940
    instance-of v2, p0, Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v2, :cond_1

    .line 1953941
    check-cast p0, Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 1953942
    invoke-virtual {p0}, Lcom/facebook/search/results/model/SearchResultsBridge;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 1953943
    invoke-virtual {p0}, Lcom/facebook/search/results/model/SearchResultsBridge;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    .line 1953944
    :cond_0
    :goto_0
    invoke-static {v1, v0}, LX/Cyz;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    return-object v0

    .line 1953945
    :cond_1
    instance-of v2, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    if-eqz v2, :cond_0

    .line 1953946
    check-cast p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 1953947
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 1953948
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v0

    .line 1953949
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v2}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Cyv;LX/CzA;LX/0Pz;LX/0Pz;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cyv;",
            "LX/CzA;",
            "LX/0Pz",
            "<+",
            "LX/Cyv;",
            ">;",
            "LX/0Pz",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1953985
    invoke-interface {p1}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v4

    .line 1953986
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->v()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v3, v0

    .line 1953987
    :goto_0
    if-nez v3, :cond_2

    .line 1953988
    :cond_0
    :goto_1
    return-void

    .line 1953989
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->v()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bp()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    .line 1953990
    :cond_2
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1953991
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    :goto_2
    if-ge v1, v7, :cond_4

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 1953992
    iget-object v8, p0, LX/Cyz;->c:LX/0Rf;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->l()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 1953993
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1953994
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1953995
    :cond_4
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1953996
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1953997
    invoke-static {p1}, LX/Cyz;->b(LX/Cyv;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v6

    .line 1953998
    new-instance v7, LX/Cz3;

    invoke-static {v4}, LX/8dO;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/8dO;

    move-result-object v0

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->v()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object v1

    invoke-static {v3}, LX/8dU;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;)LX/8dU;

    move-result-object v8

    .line 1953999
    iput-object v5, v8, LX/8dU;->a:LX/0Px;

    .line 1954000
    move-object v8, v8

    .line 1954001
    invoke-virtual {v8}, LX/8dU;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v8

    .line 1954002
    iput-object v8, v1, LX/8dQ;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    .line 1954003
    move-object v1, v1

    .line 1954004
    invoke-virtual {v1}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    .line 1954005
    iput-object v1, v0, LX/8dO;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 1954006
    move-object v0, v0

    .line 1954007
    invoke-virtual {v0}, LX/8dO;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-direct {v7, v0}, LX/Cz3;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V

    .line 1954008
    invoke-virtual {p2, v2}, LX/CzA;->b(I)LX/Cyv;

    move-result-object v0

    .line 1954009
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1954010
    invoke-interface {v0}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v1, v2

    .line 1954011
    :goto_3
    if-ge v1, v10, :cond_5

    invoke-virtual {v9, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 1954012
    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1954013
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1954014
    :cond_5
    iget-boolean v0, p0, LX/Cyz;->b:Z

    if-eqz v0, :cond_6

    .line 1954015
    invoke-virtual {v8, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1954016
    invoke-static {p0}, LX/Cyz;->a(LX/Cyz;)LX/0m9;

    move-result-object v0

    .line 1954017
    new-instance v1, LX/Cz3;

    invoke-static {v4}, LX/8dO;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/8dO;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->v()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v4

    invoke-static {v4}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object v4

    invoke-static {v3}, LX/8dU;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;)LX/8dU;

    move-result-object v3

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1954018
    iput-object v6, v3, LX/8dU;->a:LX/0Px;

    .line 1954019
    move-object v3, v3

    .line 1954020
    invoke-virtual {v3}, LX/8dU;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v3

    .line 1954021
    iput-object v3, v4, LX/8dQ;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    .line 1954022
    move-object v3, v4

    .line 1954023
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1954024
    iput-object v0, v3, LX/8dQ;->A:Ljava/lang/String;

    .line 1954025
    move-object v0, v3

    .line 1954026
    invoke-virtual {v0}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 1954027
    iput-object v0, v5, LX/8dO;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 1954028
    move-object v0, v5

    .line 1954029
    invoke-virtual {v0}, LX/8dO;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Cz3;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V

    .line 1954030
    invoke-virtual {p2, v2, v1}, LX/CzA;->a(ILX/Cyv;)LX/Cyv;

    .line 1954031
    invoke-interface {v1}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-interface {v7}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {p4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 1954032
    :cond_6
    iget-object v0, p0, LX/Cyz;->d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v6, :cond_0

    .line 1954033
    const/4 v0, 0x1

    new-array v0, v0, [LX/Cyv;

    aput-object v7, v0, v2

    invoke-virtual {p3, v0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 1954034
    invoke-interface {v7}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-interface {v7}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {p4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/search/model/SearchResultsBaseFeedUnit;Ljava/util/List;Ljava/util/List;LX/0Pz;LX/0Pz;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;>;",
            "LX/0Pz",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;",
            "LX/0Pz",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1953953
    instance-of v0, p1, Lcom/facebook/search/results/model/SearchResultsBridge;

    if-eqz v0, :cond_6

    .line 1953954
    check-cast p1, Lcom/facebook/search/results/model/SearchResultsBridge;

    invoke-static {p1}, LX/Cyz;->a(Lcom/facebook/search/results/model/SearchResultsBridge;)Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-result-object p1

    .line 1953955
    :goto_0
    move-object v2, p1

    .line 1953956
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1953957
    new-instance v4, LX/0P2;

    invoke-direct {v4}, LX/0P2;-><init>()V

    .line 1953958
    iget-object v0, v2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    move-object v0, v0

    .line 1953959
    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 1953960
    invoke-virtual {v2, v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1953961
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, LX/Cyz;->c:LX/0Rf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1953962
    if-eqz v1, :cond_1

    :goto_2
    invoke-virtual {v4, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1953963
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1953964
    :cond_1
    const-string v1, ""

    goto :goto_2

    .line 1953965
    :cond_2
    invoke-virtual {v4}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 1953966
    invoke-virtual {v1}, LX/0P1;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1953967
    :cond_3
    :goto_3
    return-void

    .line 1953968
    :cond_4
    invoke-static {v2}, LX/Cyz;->b(Lcom/facebook/search/model/SearchResultsBaseFeedUnit;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    .line 1953969
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m()LX/0am;

    move-result-object v5

    invoke-static {v2, v4, v1, v0, v5}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0P1;LX/0Px;LX/0am;)Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-result-object v5

    .line 1953970
    iget-boolean v0, p0, LX/Cyz;->b:Z

    if-eqz v0, :cond_5

    .line 1953971
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 1953972
    new-instance v6, LX/0P2;

    invoke-direct {v6}, LX/0P2;-><init>()V

    .line 1953973
    iget-object v7, p0, LX/Cyz;->c:LX/0Rf;

    invoke-virtual {v3, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1953974
    iget-object v7, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    move-object v0, v7

    .line 1953975
    invoke-virtual {v6, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 1953976
    invoke-virtual {v6, v1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 1953977
    invoke-static {p0}, LX/Cyz;->a(LX/Cyz;)LX/0m9;

    move-result-object v0

    .line 1953978
    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    invoke-static {v2, v4, v1, v3, v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0P1;LX/0Px;LX/0am;)Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-result-object v0

    .line 1953979
    invoke-interface {p2, v8, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1953980
    invoke-interface {p3, v8, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1953981
    invoke-static {v0, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {p5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1953982
    :cond_5
    iget-object v0, p0, LX/Cyz;->d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v4, :cond_3

    .line 1953983
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/search/model/SearchResultsBaseFeedUnit;

    aput-object v5, v0, v8

    invoke-virtual {p4, v0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 1953984
    invoke-static {v5, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {p5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    :cond_6
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    goto/16 :goto_0
.end method

.method public final c(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Px",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1953950
    iget-boolean v0, p0, LX/Cyz;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/Cyz;->b:Z

    .line 1953951
    return-void

    .line 1953952
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
