.class public final LX/Est;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/Esu;


# direct methods
.method public constructor <init>(LX/Esu;Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2176622
    iput-object p1, p0, LX/Est;->c:LX/Esu;

    iput-object p2, p0, LX/Est;->a:Landroid/view/View;

    iput-object p3, p0, LX/Est;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    .line 2176623
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2176624
    const v1, 0x7f0d2f04

    if-ne v0, v1, :cond_1

    .line 2176625
    iget-object v0, p0, LX/Est;->c:LX/Esu;

    iget-object v0, v0, LX/Esu;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Est;->c:LX/Esu;

    iget-object v1, v1, LX/Esu;->e:LX/C51;

    iget-object v2, p0, LX/Est;->c:LX/Esu;

    iget-object v2, v2, LX/Esu;->b:LX/1Cn;

    iget-object v3, p0, LX/Est;->c:LX/Esu;

    iget-object v3, v3, LX/Esu;->f:LX/03V;

    iget-object v4, p0, LX/Est;->c:LX/Esu;

    iget-object v4, v4, LX/Esu;->g:Ljava/lang/String;

    iget-object v5, p0, LX/Est;->c:LX/Esu;

    iget-object v5, v5, LX/Esu;->h:LX/1Nq;

    iget-object v6, p0, LX/Est;->c:LX/Esu;

    iget-object v6, v6, LX/Esu;->i:LX/1Kf;

    iget-object v7, p0, LX/Est;->c:LX/Esu;

    iget-object v7, v7, LX/Esu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v8, p0, LX/Est;->c:LX/Esu;

    iget-object v8, v8, LX/Esu;->j:LX/1Ps;

    invoke-static/range {v0 .. v8}, LX/Esx;->a(Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Cn;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iget-object v1, p0, LX/Est;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2176626
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2176627
    :cond_1
    const v1, 0x7f0d2f05

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Est;->c:LX/Esu;

    iget-object v0, v0, LX/Esu;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Est;->c:LX/Esu;

    iget-object v0, v0, LX/Esu;->c:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    if-eqz v0, :cond_0

    .line 2176628
    iget-object v0, p0, LX/Est;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2176629
    iget-object v1, p0, LX/Est;->c:LX/Esu;

    iget-object v1, v1, LX/Esu;->l:LX/Es5;

    iget-object v2, p0, LX/Est;->c:LX/Esu;

    iget-object v2, v2, LX/Esu;->c:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iget-object v3, p0, LX/Est;->b:Ljava/lang/String;

    sget-object v4, LX/CEz;->PROMOTION:LX/CEz;

    invoke-virtual {v1, v2, v0, v3, v4}, LX/Es5;->a(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Landroid/app/Activity;Ljava/lang/String;LX/CEz;)V

    goto :goto_0
.end method
