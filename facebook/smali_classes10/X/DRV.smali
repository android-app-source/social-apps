.class public LX/DRV;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Landroid/widget/ProgressBar;

.field public e:Landroid/widget/ImageView;

.field public f:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1998307
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DRV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1998308
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1998327
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/DRV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1998328
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1998318
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1998319
    const p1, 0x7f030854

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1998320
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/DRV;->setOrientation(I)V

    .line 1998321
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    const/4 p2, -0x1

    invoke-direct {p1, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, p1}, LX/DRV;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1998322
    const p1, 0x7f0d15b8

    invoke-virtual {p0, p1}, LX/DRV;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, LX/DRV;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1998323
    const p1, 0x7f0d15bb

    invoke-virtual {p0, p1}, LX/DRV;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, LX/DRV;->d:Landroid/widget/ProgressBar;

    .line 1998324
    const p1, 0x7f0d15b6

    invoke-virtual {p0, p1}, LX/DRV;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, LX/DRV;->e:Landroid/widget/ImageView;

    .line 1998325
    const p1, 0x7f0d15ba

    invoke-virtual {p0, p1}, LX/DRV;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, LX/DRV;->f:Landroid/widget/ImageView;

    .line 1998326
    return-void
.end method

.method public static b(LX/DRV;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1998309
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, LX/DRV;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/DRV;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1998310
    iget-object v1, p0, LX/DRV;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1998311
    iget v0, p0, LX/DRV;->a:I

    iget v1, p0, LX/DRV;->b:I

    if-ge v0, v1, :cond_0

    .line 1998312
    iget v0, p0, LX/DRV;->a:I

    mul-int/lit8 v0, v0, 0x64

    iget v1, p0, LX/DRV;->b:I

    div-int/2addr v0, v1

    .line 1998313
    iget-object v1, p0, LX/DRV;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1998314
    :goto_0
    return-void

    .line 1998315
    :cond_0
    iget-object v0, p0, LX/DRV;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1998316
    iget-object v0, p0, LX/DRV;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1998317
    iget-object v0, p0, LX/DRV;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
