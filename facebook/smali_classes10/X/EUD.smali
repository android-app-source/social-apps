.class public LX/EUD;
.super LX/ESt;
.source ""

# interfaces
.implements LX/ESs;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:LX/ESr;

.field public final b:LX/0oB;

.field public final c:LX/2xj;

.field private final d:LX/ETB;


# direct methods
.method public constructor <init>(LX/ESr;Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;LX/2xj;LX/ETB;)V
    .locals 2
    .param p2    # Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125898
    invoke-direct {p0}, LX/ESt;-><init>()V

    .line 2125899
    iput-object p1, p0, LX/EUD;->a:LX/ESr;

    .line 2125900
    iget-object v0, p0, LX/EUD;->a:LX/ESr;

    .line 2125901
    if-eqz p2, :cond_0

    .line 2125902
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/ESr;->f:Ljava/lang/ref/WeakReference;

    .line 2125903
    :cond_0
    iput-object p4, p0, LX/EUD;->d:LX/ETB;

    .line 2125904
    new-instance v0, LX/EUC;

    invoke-direct {v0, p0}, LX/EUC;-><init>(LX/EUD;)V

    iput-object v0, p0, LX/EUD;->b:LX/0oB;

    .line 2125905
    iput-object p3, p0, LX/EUD;->c:LX/2xj;

    .line 2125906
    iget-object v0, p0, LX/EUD;->c:LX/2xj;

    iget-object v1, p0, LX/EUD;->b:LX/0oB;

    invoke-virtual {v0, v1}, LX/2xj;->a(LX/0oB;)V

    .line 2125907
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;)V
    .locals 12

    .prologue
    .line 2125908
    iget-object v0, p0, LX/EUD;->d:LX/ETB;

    .line 2125909
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2125910
    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    .line 2125911
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2125912
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EUD;->a:LX/ESr;

    invoke-virtual {v0, p1}, LX/ESr;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    .line 2125913
    return-void

    .line 2125914
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v1

    invoke-static {v1}, LX/ESx;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 2125915
    instance-of v3, v1, Lcom/facebook/graphql/model/GraphQLActor;

    if-nez v3, :cond_3

    .line 2125916
    :cond_2
    :goto_1
    goto :goto_0

    :cond_3
    move-object v3, v1

    .line 2125917
    check-cast v3, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v10

    .line 2125918
    if-eqz v10, :cond_2

    iget-object v3, v0, LX/ETB;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1My;

    invoke-virtual {v3, v10}, LX/1My;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2125919
    iget-object v3, v0, LX/ETB;->B:LX/ET8;

    if-nez v3, :cond_4

    .line 2125920
    new-instance v3, LX/ET8;

    invoke-direct {v3, v0}, LX/ET8;-><init>(LX/ETB;)V

    iput-object v3, v0, LX/ETB;->B:LX/ET8;

    .line 2125921
    :cond_4
    invoke-static {v10}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v9

    .line 2125922
    iget-object v3, v0, LX/ETB;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1My;

    iget-object v11, v0, LX/ETB;->B:LX/ET8;

    new-instance v4, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v6, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v7, 0x0

    move-object v5, v1

    invoke-direct/range {v4 .. v9}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    invoke-virtual {v3, v11, v10, v4}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_1
.end method

.method public final b(Lcom/facebook/video/videohome/data/VideoHomeItem;)V
    .locals 1

    .prologue
    .line 2125923
    iget-object v0, p0, LX/EUD;->a:LX/ESr;

    invoke-virtual {v0, p1}, LX/ESr;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    .line 2125924
    return-void
.end method
