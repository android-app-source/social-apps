.class public LX/DFg;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DFe;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DFh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1978653
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DFg;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DFh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978639
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1978640
    iput-object p1, p0, LX/DFg;->b:LX/0Ot;

    .line 1978641
    return-void
.end method

.method public static a(LX/0QB;)LX/DFg;
    .locals 4

    .prologue
    .line 1978642
    const-class v1, LX/DFg;

    monitor-enter v1

    .line 1978643
    :try_start_0
    sget-object v0, LX/DFg;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978644
    sput-object v2, LX/DFg;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978645
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978646
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978647
    new-instance v3, LX/DFg;

    const/16 p0, 0x20eb

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DFg;-><init>(LX/0Ot;)V

    .line 1978648
    move-object v0, v3

    .line 1978649
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978650
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978651
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978652
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1978638
    const v0, 0x4be3d65

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1978628
    check-cast p2, LX/DFf;

    .line 1978629
    iget-object v0, p0, LX/DFg;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DFh;

    iget-object v1, p2, LX/DFf;->a:Ljava/lang/String;

    iget-object v2, p2, LX/DFf;->b:Ljava/lang/String;

    iget-object v3, p2, LX/DFf;->c:LX/2ep;

    .line 1978630
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x0

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/16 p0, 0x8

    const p2, 0x7f0b0060

    invoke-interface {v4, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x2

    invoke-interface {v4, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    .line 1978631
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b0052

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0a00d5

    invoke-virtual {p0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object p0

    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object p0

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const p2, 0x7f020a8d

    invoke-interface {p0, p2}, LX/1Di;->x(I)LX/1Di;

    move-result-object p0

    move-object p0, p0

    .line 1978632
    invoke-interface {v4, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 v1, 0x1

    .line 1978633
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    iget-object p2, v0, LX/DFh;->a:LX/2eq;

    invoke-virtual {p2, v2, v3}, LX/2eq;->a(Ljava/lang/String;LX/2ep;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b0050

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0a00d5

    invoke-virtual {p0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1ne;->b(Z)LX/1ne;

    move-result-object p0

    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object p0

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const p2, 0x7f0b0f61

    invoke-interface {p0, v1, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object p0

    move-object p0, p0

    .line 1978634
    invoke-interface {v4, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 1978635
    const p0, 0x4be3d65

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1978636
    invoke-interface {v4, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1978637
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1978619
    invoke-static {}, LX/1dS;->b()V

    .line 1978620
    iget v0, p1, LX/1dQ;->b:I

    .line 1978621
    packed-switch v0, :pswitch_data_0

    .line 1978622
    :goto_0
    return-object v1

    .line 1978623
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 1978624
    check-cast v0, LX/DFf;

    .line 1978625
    iget-object v2, p0, LX/DFg;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DFh;

    iget-object v3, v0, LX/DFf;->d:Ljava/lang/String;

    iget-object v4, v0, LX/DFf;->e:Ljava/lang/String;

    iget-object p1, v0, LX/DFf;->a:Ljava/lang/String;

    iget-object p2, v0, LX/DFf;->f:LX/162;

    .line 1978626
    iget-object p0, v2, LX/DFh;->a:LX/2eq;

    invoke-virtual {p0, v3, v4, p1, p2}, LX/2eq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;)V

    .line 1978627
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4be3d65
        :pswitch_0
    .end packed-switch
.end method
