.class public final LX/E4D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/9uc;

.field public final synthetic b:LX/E4E;


# direct methods
.method public constructor <init>(LX/E4E;LX/9uc;)V
    .locals 0

    .prologue
    .line 2076339
    iput-object p1, p0, LX/E4D;->b:LX/E4E;

    iput-object p2, p0, LX/E4D;->a:LX/9uc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, -0x1582dea1

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2076340
    iget-object v0, p0, LX/E4D;->b:LX/E4E;

    iget-object v0, v0, LX/E4E;->b:LX/1Pn;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    iget-object v2, p0, LX/E4D;->b:LX/E4E;

    iget-object v2, v2, LX/E4E;->h:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2076341
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2076342
    iget-object v3, p0, LX/E4D;->b:LX/E4E;

    iget-object v3, v3, LX/E4E;->h:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2076343
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2076344
    iget-object v4, p0, LX/E4D;->a:LX/9uc;

    invoke-interface {v4}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Cfl;

    iget-object v6, p0, LX/E4D;->b:LX/E4E;

    iget-object v6, v6, LX/E4E;->e:Ljava/lang/String;

    sget-object v7, LX/Cfc;->PHOTO_TAP:LX/Cfc;

    invoke-direct {v5, v6, v7}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;)V

    invoke-virtual {v0, v2, v3, v4, v5}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2076345
    iget-object v0, p0, LX/E4D;->b:LX/E4E;

    iget-object v0, v0, LX/E4E;->i:LX/3Tz;

    iget-object v2, p0, LX/E4D;->b:LX/E4E;

    iget-object v2, v2, LX/E4E;->a:Landroid/content/Context;

    iget-object v3, p0, LX/E4D;->b:LX/E4E;

    iget-object v3, v3, LX/E4E;->e:Ljava/lang/String;

    iget-object v4, p0, LX/E4D;->a:LX/9uc;

    invoke-interface {v4}, LX/9uc;->bS()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/E4D;->b:LX/E4E;

    .line 2076346
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2076347
    iget-object v6, v5, LX/E4E;->f:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/9uc;

    .line 2076348
    const/4 p0, 0x1

    new-array p0, p0, [LX/5sa;

    const/4 p1, 0x0

    invoke-interface {v6}, LX/9uc;->bS()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PhotoModel;

    move-result-object v6

    aput-object v6, p0, p1

    invoke-virtual {v7, p0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2076349
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v5, v6

    .line 2076350
    new-instance v6, LX/9hE;

    const-class v7, LX/23X;

    new-instance v9, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-direct {v9, v3}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    invoke-static {v7, v9}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v7

    invoke-direct {v6, v7}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 2076351
    if-eqz v5, :cond_2

    .line 2076352
    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    .line 2076353
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2076354
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2076355
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v5

    const/4 v9, 0x0

    move p0, v9

    :goto_1
    if-ge p0, v5, :cond_1

    invoke-virtual {v7, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/5sa;

    .line 2076356
    invoke-static {v9}, LX/5k9;->a(LX/5sa;)LX/5kD;

    move-result-object v9

    invoke-virtual {v3, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2076357
    add-int/lit8 v9, p0, 0x1

    move p0, v9

    goto :goto_1

    .line 2076358
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    invoke-virtual {v6, v9}, LX/9hD;->a(LX/0Px;)LX/9hD;

    .line 2076359
    :cond_2
    move-object v6, v6

    .line 2076360
    sget-object v7, LX/74S;->REACTION_PHOTO_ITEM:LX/74S;

    invoke-virtual {v6, v7}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v6

    const/4 v7, 0x1

    .line 2076361
    iput-boolean v7, v6, LX/9hD;->m:Z

    .line 2076362
    move-object v6, v6

    .line 2076363
    iget-object v7, v0, LX/3Tz;->a:LX/23R;

    invoke-virtual {v6}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v6

    const/4 v9, 0x0

    invoke-interface {v7, v2, v6, v9}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2076364
    const v0, -0x62952d5e

    invoke-static {v8, v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
