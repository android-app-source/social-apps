.class public final LX/CpJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CmW;

.field public final synthetic b:LX/CpO;


# direct methods
.method public constructor <init>(LX/CpO;LX/CmW;)V
    .locals 0

    .prologue
    .line 1936920
    iput-object p1, p0, LX/CpJ;->b:LX/CpO;

    iput-object p2, p0, LX/CpJ;->a:LX/CmW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5e76f398

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1936921
    iget-object v1, p0, LX/CpJ;->b:LX/CpO;

    iget-object v2, p0, LX/CpJ;->a:LX/CmW;

    .line 1936922
    iget-object v3, v2, LX/CmW;->i:Ljava/lang/String;

    move-object v2, v3

    .line 1936923
    iget-object v3, p0, LX/CpJ;->a:LX/CmW;

    .line 1936924
    iget-object p0, v3, LX/CmW;->j:Ljava/lang/String;

    move-object v3, p0

    .line 1936925
    if-nez v2, :cond_0

    .line 1936926
    iget-object v5, v1, LX/CpO;->d:LX/03V;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, LX/CpO;->p:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_openURL"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Attempting to open url when the url parameter is null"

    invoke-static {v6, v7}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v6

    invoke-virtual {v6}, LX/0VK;->g()LX/0VG;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/03V;->a(LX/0VG;)V

    .line 1936927
    :goto_0
    const v1, 0x45b1a7d1

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1936928
    :cond_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1936929
    const/4 v5, 0x1

    :try_start_0
    invoke-static {v2, v5}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v5

    .line 1936930
    invoke-virtual {v6}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v7

    const-string v8, "store"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1936931
    const/4 v5, 0x0

    .line 1936932
    const-string v7, "appsite_data"

    invoke-virtual {v6, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1936933
    :try_start_1
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1936934
    const-string v7, "android"

    invoke-virtual {v8, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    move-result-object v10

    .line 1936935
    if-eqz v10, :cond_1

    .line 1936936
    const/4 v7, 0x0

    move v9, v7

    move-object v7, v5
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    :try_start_3
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v9, v8, :cond_2

    .line 1936937
    invoke-virtual {v10, v9}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object p0

    .line 1936938
    const-string v8, "package"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1

    :try_start_4
    move-result-object v8
    :try_end_4
    .catch Ljava/net/URISyntaxException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1

    .line 1936939
    :try_start_5
    const-string v5, "appsite_url"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/net/URISyntaxException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_1

    :try_start_6
    move-result-object v7

    .line 1936940
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    move-object v5, v8

    goto :goto_1

    :cond_1
    move-object v7, v5

    :cond_2
    move-object v8, v5

    move-object v5, v7

    .line 1936941
    :goto_2
    if-eqz v5, :cond_6

    if-eqz v8, :cond_6

    .line 1936942
    new-instance v7, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-direct {v7, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1936943
    const/high16 v9, 0x10000000

    invoke-virtual {v7, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1936944
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1936945
    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 1936946
    const/high16 v9, 0x10000

    invoke-virtual {v5, v7, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 1936947
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 1936948
    iget-object v10, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1936949
    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->name:Ljava/lang/String;

    invoke-direct {v8, v9, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-object v5, v7

    .line 1936950
    :goto_3
    move-object v5, v5

    .line 1936951
    :cond_4
    :goto_4
    const-string v6, "com.android.browser.headers"

    invoke-static {}, LX/Cs3;->a()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1936952
    iget-object v6, v1, LX/CpO;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1936953
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1936954
    iget-object v6, v1, LX/CpO;->f:LX/Ckw;

    invoke-virtual {v6, v2, v5}, LX/Ckw;->b(Ljava/lang/String;Ljava/util/Map;)V

    .line 1936955
    iget-object v5, v1, LX/CpO;->g:LX/8bG;

    invoke-virtual {v5, v3}, LX/8bG;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/net/URISyntaxException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_1

    .line 1936956
    goto/16 :goto_0

    .line 1936957
    :catch_0
    move-exception v5

    move-object v6, v2

    .line 1936958
    :goto_5
    iget-object v7, v1, LX/CpO;->d:LX/03V;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, LX/CpO;->p:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_startActivityForUrl"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Error trying to create Intent from url:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v6

    .line 1936959
    iput-object v5, v6, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1936960
    move-object v5, v6

    .line 1936961
    invoke-virtual {v5}, LX/0VK;->g()LX/0VG;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/03V;->a(LX/0VG;)V

    goto/16 :goto_0

    .line 1936962
    :cond_5
    :try_start_7
    invoke-virtual {v6}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v7

    const-string v8, "open_link"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1936963
    const-string v5, "link"

    invoke-virtual {v6, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1936964
    const/4 v5, 0x1

    invoke-static {v2, v5}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v5

    .line 1936965
    iget-object v6, v1, LX/CpO;->o:LX/Cig;

    new-instance v7, LX/Cin;

    invoke-direct {v7}, LX/Cin;-><init>()V

    invoke-virtual {v6, v7}, LX/0b4;->a(LX/0b7;)V
    :try_end_7
    .catch Ljava/net/URISyntaxException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_4

    .line 1936966
    :catch_1
    move-exception v5

    move-object v6, v2

    goto :goto_5

    .line 1936967
    :catch_2
    move-exception v7

    move-object v8, v5

    move-object p1, v5

    move-object v5, v7

    move-object v7, p1

    .line 1936968
    :goto_6
    iget-object v9, v1, LX/CpO;->d:LX/03V;

    sget-object v10, LX/CpO;->p:Ljava/lang/String;

    const-string p0, " Error parsing appsite_data"

    invoke-static {v10, p0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v10

    .line 1936969
    iput-object v5, v10, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1936970
    move-object v5, v10

    .line 1936971
    invoke-virtual {v5}, LX/0VK;->g()LX/0VG;

    move-result-object v5

    invoke-virtual {v9, v5}, LX/03V;->a(LX/0VG;)V

    move-object v5, v8

    move-object v8, v7

    goto/16 :goto_2

    .line 1936972
    :cond_6
    const-string v5, "store_url"

    invoke-virtual {v6, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1936973
    iget-object v7, v1, LX/CpO;->e:LX/17T;

    invoke-virtual {v7, v5}, LX/17T;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    goto/16 :goto_3

    .line 1936974
    :catch_3
    move-exception v8

    move-object p1, v8

    move-object v8, v7

    move-object v7, v5

    move-object v5, p1

    goto :goto_6

    :catch_4
    move-exception v5

    move-object p1, v8

    move-object v8, v7

    move-object v7, p1

    goto :goto_6
.end method
