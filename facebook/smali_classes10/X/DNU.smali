.class public LX/DNU;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements LX/1Cw;


# instance fields
.field private a:LX/1Cw;

.field private b:LX/1Cw;

.field private final c:LX/EQS;


# direct methods
.method public constructor <init>(LX/1Cw;LX/1Cw;)V
    .locals 2

    .prologue
    .line 1991738
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1991739
    iput-object p1, p0, LX/DNU;->a:LX/1Cw;

    .line 1991740
    iput-object p2, p0, LX/DNU;->b:LX/1Cw;

    .line 1991741
    new-instance v0, LX/EQS;

    invoke-direct {v0, p0}, LX/EQS;-><init>(Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, LX/DNU;->c:LX/EQS;

    .line 1991742
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    iget-object v1, p0, LX/DNU;->c:LX/EQS;

    invoke-interface {v0, v1}, LX/1Cw;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1991743
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    iget-object v1, p0, LX/DNU;->c:LX/EQS;

    invoke-interface {v0, v1}, LX/1Cw;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1991744
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1991735
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getViewTypeCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1991736
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0, p1, p2}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1991737
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    iget-object v1, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v1}, LX/1Cw;->getViewTypeCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1, p2}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1991730
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    if-eqz v0, :cond_0

    .line 1991731
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    iget-object v1, p0, LX/DNU;->c:LX/EQS;

    invoke-interface {v0, v1}, LX/1Cw;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1991732
    :cond_0
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    if-eqz v0, :cond_1

    .line 1991733
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    iget-object v1, p0, LX/DNU;->c:LX/EQS;

    invoke-interface {v0, v1}, LX/1Cw;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1991734
    :cond_1
    return-void
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 1991726
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1991727
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 1991728
    :goto_0
    return-void

    .line 1991729
    :cond_0
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    iget-object v1, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v1}, LX/1Cw;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method public final a(LX/1Cw;LX/1Cw;)V
    .locals 2

    .prologue
    .line 1991707
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    iget-object v1, p0, LX/DNU;->c:LX/EQS;

    invoke-interface {v0, v1}, LX/1Cw;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1991708
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    iget-object v1, p0, LX/DNU;->c:LX/EQS;

    invoke-interface {v0, v1}, LX/1Cw;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1991709
    iput-object p1, p0, LX/DNU;->a:LX/1Cw;

    .line 1991710
    iput-object p2, p0, LX/DNU;->b:LX/1Cw;

    .line 1991711
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    iget-object v1, p0, LX/DNU;->c:LX/EQS;

    invoke-interface {v0, v1}, LX/1Cw;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1991712
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    iget-object v1, p0, LX/DNU;->c:LX/EQS;

    invoke-interface {v0, v1}, LX/1Cw;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1991713
    const v0, 0x66d569c

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1991714
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1991725
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->areAllItemsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->areAllItemsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 1991724
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    iget-object v1, p0, LX/DNU;->b:LX/1Cw;

    invoke-interface {v1}, LX/1Cw;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1991745
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1991746
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1991747
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    iget-object v1, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v1}, LX/1Cw;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, LX/1Cw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1991721
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1991722
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItemId(I)J

    move-result-wide v0

    .line 1991723
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    iget-object v1, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v1}, LX/1Cw;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, LX/1Cw;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    .line 1991718
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1991719
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->getItemViewType(I)I

    move-result v0

    .line 1991720
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getViewTypeCount()I

    move-result v0

    iget-object v1, p0, LX/DNU;->b:LX/1Cw;

    iget-object v2, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v2}, LX/1Cw;->getCount()I

    move-result v2

    sub-int v2, p1, v2

    invoke-interface {v1, v2}, LX/1Cw;->getItemViewType(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1991715
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1991716
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0, p1, p2, p3}, LX/1Cw;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1991717
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    iget-object v1, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v1}, LX/1Cw;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1, p2, p3}, LX/1Cw;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 2

    .prologue
    .line 1991706
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getViewTypeCount()I

    move-result v0

    iget-object v1, p0, LX/DNU;->b:LX/1Cw;

    invoke-interface {v1}, LX/1Cw;->getViewTypeCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1991705
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 1991704
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 1991701
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1991702
    iget-object v0, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v0, p1}, LX/1Cw;->isEnabled(I)Z

    move-result v0

    .line 1991703
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/DNU;->b:LX/1Cw;

    iget-object v1, p0, LX/DNU;->a:LX/1Cw;

    invoke-interface {v1}, LX/1Cw;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, LX/1Cw;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
