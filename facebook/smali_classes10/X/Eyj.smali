.class public final LX/Eyj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/PersonYouMayKnow;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Br;


# direct methods
.method public constructor <init>(LX/3Br;)V
    .locals 0

    .prologue
    .line 2186473
    iput-object p1, p0, LX/Eyj;->a:LX/3Br;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2186474
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186475
    check-cast p1, Ljava/util/List;

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2186476
    if-nez p1, :cond_1

    .line 2186477
    :cond_0
    :goto_0
    return-void

    .line 2186478
    :cond_1
    new-instance v4, LX/0cA;

    invoke-direct {v4}, LX/0cA;-><init>()V

    .line 2186479
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    move v2, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 2186480
    iget-object v6, p0, LX/Eyj;->a:LX/3Br;

    iget-object v6, v6, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v6, v6, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v8

    .line 2186481
    iget-object v7, v6, LX/2iK;->l:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/friends/model/PersonYouMayKnow;

    move-object v6, v7

    .line 2186482
    if-eqz v6, :cond_6

    .line 2186483
    iget-boolean v7, v0, Lcom/facebook/friends/model/PersonYouMayKnow;->g:Z

    move v7, v7

    .line 2186484
    if-eqz v7, :cond_2

    .line 2186485
    invoke-virtual {v4, v6}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move v2, v3

    .line 2186486
    goto :goto_1

    .line 2186487
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    invoke-virtual {v6}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v8

    if-eq v7, v8, :cond_6

    .line 2186488
    invoke-virtual {v6}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2186489
    iput-object v1, v6, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2186490
    invoke-virtual {v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/friends/model/PersonYouMayKnow;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    move v0, v3

    :goto_2
    move v1, v0

    .line 2186491
    goto :goto_1

    .line 2186492
    :cond_3
    if-eqz v2, :cond_5

    .line 2186493
    iget-object v0, p0, LX/Eyj;->a:LX/3Br;

    iget-object v0, v0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2iK;->a(Ljava/util/Set;)V

    .line 2186494
    :cond_4
    :goto_3
    if-eqz v1, :cond_0

    .line 2186495
    iget-object v0, p0, LX/Eyj;->a:LX/3Br;

    iget-object v0, v0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 2186496
    iget-object v0, p0, LX/Eyj;->a:LX/3Br;

    iget-object v0, v0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2186497
    iget-object v0, p0, LX/Eyj;->a:LX/3Br;

    iget-object v0, v0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->m(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    goto :goto_0

    .line 2186498
    :cond_5
    if-eqz v1, :cond_4

    .line 2186499
    iget-object v0, p0, LX/Eyj;->a:LX/3Br;

    iget-object v0, v0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v0}, LX/2iK;->m()V

    .line 2186500
    iget-object v0, p0, LX/Eyj;->a:LX/3Br;

    iget-object v0, v0, LX/3Br;->b:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    const v2, 0x771f6e9a

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_2
.end method
