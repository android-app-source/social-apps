.class public final enum LX/EDu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EDu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EDu;

.field public static final enum NONE:LX/EDu;

.field public static final enum RECIPROCATED:LX/EDu;

.field public static final enum REMOTE_AUDIO_ONLY:LX/EDu;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2091986
    new-instance v0, LX/EDu;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/EDu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDu;->NONE:LX/EDu;

    .line 2091987
    new-instance v0, LX/EDu;

    const-string v1, "REMOTE_AUDIO_ONLY"

    invoke-direct {v0, v1, v3}, LX/EDu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDu;->REMOTE_AUDIO_ONLY:LX/EDu;

    .line 2091988
    new-instance v0, LX/EDu;

    const-string v1, "RECIPROCATED"

    invoke-direct {v0, v1, v4}, LX/EDu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDu;->RECIPROCATED:LX/EDu;

    .line 2091989
    const/4 v0, 0x3

    new-array v0, v0, [LX/EDu;

    sget-object v1, LX/EDu;->NONE:LX/EDu;

    aput-object v1, v0, v2

    sget-object v1, LX/EDu;->REMOTE_AUDIO_ONLY:LX/EDu;

    aput-object v1, v0, v3

    sget-object v1, LX/EDu;->RECIPROCATED:LX/EDu;

    aput-object v1, v0, v4

    sput-object v0, LX/EDu;->$VALUES:[LX/EDu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2091990
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EDu;
    .locals 1

    .prologue
    .line 2091991
    const-class v0, LX/EDu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EDu;

    return-object v0
.end method

.method public static values()[LX/EDu;
    .locals 1

    .prologue
    .line 2091992
    sget-object v0, LX/EDu;->$VALUES:[LX/EDu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EDu;

    return-object v0
.end method
