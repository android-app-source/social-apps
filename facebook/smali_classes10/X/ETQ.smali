.class public LX/ETQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;
.implements Ljava/util/Collection;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;",
        "Ljava/util/Collection",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/ESs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2124979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2124980
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    .line 2124981
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/ETQ;->b:Ljava/util/Set;

    .line 2124982
    return-void
.end method

.method private a(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2124932
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124933
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2124934
    if-eqz v2, :cond_1

    .line 2124935
    iget-object v3, p0, LX/ETQ;->b:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ESs;

    .line 2124936
    invoke-interface {v3, v2}, LX/ESs;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_1

    .line 2124937
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2124938
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    invoke-virtual {v0}, LX/ETQ;->a()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/ETQ;->a(Ljava/util/Collection;)V

    goto :goto_0

    .line 2124939
    :cond_2
    return-void
.end method

.method public static b(LX/ETQ;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2124940
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124941
    iget-object v2, p0, LX/ETQ;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ESs;

    .line 2124942
    invoke-interface {v2, v0}, LX/ESs;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)V

    goto :goto_1

    .line 2124943
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2124944
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    invoke-static {v2, v0}, LX/ETQ;->b(LX/ETQ;Ljava/util/Collection;)V

    goto :goto_0

    .line 2124945
    :cond_2
    invoke-direct {p0, p1}, LX/ETQ;->a(Ljava/util/Collection;)V

    .line 2124946
    return-void
.end method

.method public static c(LX/ETQ;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2124947
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124948
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2124949
    if-eqz v2, :cond_1

    .line 2124950
    iget-object v3, p0, LX/ETQ;->b:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ESs;

    .line 2124951
    invoke-interface {v3, v2}, LX/ESs;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_1

    .line 2124952
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2124953
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    invoke-virtual {v0}, LX/ETQ;->a()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/ETQ;->c(LX/ETQ;Ljava/util/Collection;)V

    goto :goto_0

    .line 2124954
    :cond_2
    return-void
.end method

.method private d(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2124955
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p0}, LX/ETQ;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 2124956
    const/4 v0, 0x0

    .line 2124957
    :goto_0
    move v0, v0

    .line 2124958
    if-eqz v0, :cond_1

    .line 2124959
    const/4 v0, 0x1

    .line 2124960
    :cond_0
    :goto_1
    return v0

    .line 2124961
    :cond_1
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    .line 2124962
    if-eqz v0, :cond_0

    .line 2124963
    invoke-static {p0, p1}, LX/ETQ;->c(LX/ETQ;Ljava/util/Collection;)V

    goto :goto_1

    .line 2124964
    :cond_2
    invoke-virtual {p0}, LX/ETQ;->clear()V

    .line 2124965
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 6

    .prologue
    .line 2124966
    const/4 v1, -0x1

    .line 2124967
    const/4 v0, 0x0

    .line 2124968
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2124969
    iget-object v2, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2124970
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124971
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2124972
    if-gez v2, :cond_0

    move v2, v1

    .line 2124973
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 2124974
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2124975
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2124976
    goto :goto_0

    .line 2124977
    :cond_2
    invoke-static {p0, v3}, LX/ETQ;->c(LX/ETQ;Ljava/util/Collection;)V

    .line 2124978
    return v2
.end method

.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2124926
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2124990
    invoke-virtual {p0, p1}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2124983
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2124984
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124985
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2124986
    invoke-interface {v3}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    .line 2124987
    if-eqz v3, :cond_0

    invoke-virtual {v3, p1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2124988
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2124989
    :cond_1
    return-object v1
.end method

.method public final a(LX/ESs;)V
    .locals 1

    .prologue
    .line 2125020
    iget-object v0, p0, LX/ETQ;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2125021
    return-void
.end method

.method public final a(ILjava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2124996
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    .line 2124997
    if-eqz v0, :cond_0

    .line 2124998
    invoke-static {p0, p2}, LX/ETQ;->b(LX/ETQ;Ljava/util/Collection;)V

    .line 2124999
    :cond_0
    return v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2125000
    move v1, v0

    .line 2125001
    :goto_0
    invoke-virtual {p0}, LX/ETQ;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 2125002
    invoke-virtual {p0, v0}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v2

    .line 2125003
    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2125004
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2125005
    invoke-virtual {p0, v0}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v3

    .line 2125006
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2125007
    invoke-static {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;->a(LX/9uc;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v4

    invoke-static {v4}, LX/9vT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;)LX/9vT;

    move-result-object v4

    .line 2125008
    iput-object p1, v4, LX/9vT;->an:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2125009
    move-object v4, v4

    .line 2125010
    invoke-virtual {v4}, LX/9vT;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v4

    .line 2125011
    invoke-virtual {v3, v4}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a(LX/9uc;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, LX/ETQ;->b(ILcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v3

    move v3, v3

    .line 2125012
    or-int/2addr v1, v3

    .line 2125013
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2125014
    invoke-virtual {v2}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/ETQ;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 2125015
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2125016
    :cond_2
    if-eqz v1, :cond_3

    .line 2125017
    iget-object v0, p0, LX/ETQ;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ESs;

    .line 2125018
    invoke-interface {v0, p1}, LX/ESs;->c(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_1

    .line 2125019
    :cond_3
    return v1
.end method

.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 2

    .prologue
    .line 2124992
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 2124993
    if-eqz v0, :cond_0

    .line 2124994
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-static {p0, v1}, LX/ETQ;->b(LX/ETQ;Ljava/util/Collection;)V

    .line 2124995
    :cond_0
    return v0
.end method

.method public final synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2124991
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {p0, p1}, LX/ETQ;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    return v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2124927
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-result v0

    .line 2124928
    if-eqz v0, :cond_0

    .line 2124929
    invoke-static {p0, p1}, LX/ETQ;->b(LX/ETQ;Ljava/util/Collection;)V

    .line 2124930
    :cond_0
    return v0
.end method

.method public final b(Lcom/facebook/video/videohome/data/VideoHomeItem;)I
    .locals 1

    .prologue
    .line 2124931
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 1

    .prologue
    .line 2124886
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2124887
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2124888
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124889
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2124890
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2124891
    :cond_1
    return-object v1
.end method

.method public final b(LX/ESs;)V
    .locals 1

    .prologue
    .line 2124892
    iget-object v0, p0, LX/ETQ;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2124893
    return-void
.end method

.method public final b(ILcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 1

    .prologue
    .line 2124894
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2124895
    iget-object v0, p0, LX/ETQ;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ESs;

    .line 2124896
    invoke-interface {v0, p2}, LX/ESs;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)V

    goto :goto_0

    .line 2124897
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final clear()V
    .locals 2

    .prologue
    .line 2124898
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2124899
    iget-object v1, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2124900
    invoke-static {p0, v0}, LX/ETQ;->c(LX/ETQ;Ljava/util/Collection;)V

    .line 2124901
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2124902
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2124903
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 2124904
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2124905
    new-instance v0, LX/ETP;

    invoke-direct {v0, p0}, LX/ETP;-><init>(LX/ETQ;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2124906
    instance-of v0, p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-nez v0, :cond_1

    .line 2124907
    const/4 v0, 0x0

    .line 2124908
    :cond_0
    :goto_0
    return v0

    .line 2124909
    :cond_1
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124910
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 2124911
    if-eqz v0, :cond_0

    .line 2124912
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-static {p0, v1}, LX/ETQ;->c(LX/ETQ;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2124913
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2124914
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 2124915
    invoke-virtual {p0, v0}, LX/ETQ;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2124916
    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2124917
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/ETQ;->d(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2124918
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2124919
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124920
    invoke-interface {p1, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2124921
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2124922
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/ETQ;->d(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2124923
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2124924
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 2124925
    iget-object v0, p0, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
