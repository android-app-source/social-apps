.class public LX/D7k;
.super LX/2my;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/1nA;

.field private final e:LX/17d;

.field public f:Landroid/view/ViewGroup;

.field public g:Landroid/view/ViewGroup;

.field public h:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/17Y;LX/2n0;LX/2n3;LX/17V;LX/0Zb;LX/0hB;LX/2nF;LX/2nG;LX/1nA;LX/2nH;LX/17d;)V
    .locals 11
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1967575
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p10

    invoke-direct/range {v1 .. v10}, LX/2my;-><init>(LX/17Y;LX/2n0;LX/2n3;LX/17V;LX/0Zb;LX/0hB;LX/2nF;LX/2nG;LX/2nH;)V

    .line 1967576
    move-object/from16 v0, p9

    iput-object v0, p0, LX/D7k;->d:LX/1nA;

    .line 1967577
    move-object/from16 v0, p11

    iput-object v0, p0, LX/D7k;->e:LX/17d;

    .line 1967578
    return-void
.end method

.method public static b(LX/0QB;)LX/D7k;
    .locals 15

    .prologue
    .line 1967564
    const-class v1, LX/D7k;

    monitor-enter v1

    .line 1967565
    :try_start_0
    sget-object v0, LX/D7k;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1967566
    sput-object v2, LX/D7k;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1967567
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1967568
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1967569
    new-instance v3, LX/D7k;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-static {v0}, LX/2n0;->b(LX/0QB;)LX/2n0;

    move-result-object v5

    check-cast v5, LX/2n0;

    invoke-static {v0}, LX/2n3;->b(LX/0QB;)LX/2n3;

    move-result-object v6

    check-cast v6, LX/2n3;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v7

    check-cast v7, LX/17V;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v9

    check-cast v9, LX/0hB;

    invoke-static {v0}, LX/2nF;->a(LX/0QB;)LX/2nF;

    move-result-object v10

    check-cast v10, LX/2nF;

    invoke-static {v0}, LX/2nG;->a(LX/0QB;)LX/2nG;

    move-result-object v11

    check-cast v11, LX/2nG;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v12

    check-cast v12, LX/1nA;

    invoke-static {v0}, LX/2nH;->b(LX/0QB;)LX/2nH;

    move-result-object v13

    check-cast v13, LX/2nH;

    invoke-static {v0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v14

    check-cast v14, LX/17d;

    invoke-direct/range {v3 .. v14}, LX/D7k;-><init>(LX/17Y;LX/2n0;LX/2n3;LX/17V;LX/0Zb;LX/0hB;LX/2nF;LX/2nG;LX/1nA;LX/2nH;LX/17d;)V

    .line 1967570
    move-object v0, v3

    .line 1967571
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1967572
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D7k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1967573
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1967574
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 1967561
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/2my;->a:LX/2nF;

    invoke-virtual {v0}, LX/2nF;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1967562
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1bd9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 1967563
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1967560
    const/4 v0, 0x0

    return v0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1967579
    invoke-super {p0}, LX/2my;->a()V

    .line 1967580
    iget-object v0, p0, LX/D7k;->g:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1967581
    iget-object v0, p0, LX/D7k;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 1967582
    iput-object v1, p0, LX/D7k;->g:Landroid/view/ViewGroup;

    .line 1967583
    :cond_0
    iget-object v0, p0, LX/D7k;->f:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2my;->a:LX/2nF;

    invoke-virtual {v0}, LX/2nF;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1967584
    :cond_1
    :goto_0
    iput-object v1, p0, LX/D7k;->f:Landroid/view/ViewGroup;

    .line 1967585
    iput-object v1, p0, LX/D7k;->h:Landroid/view/View$OnClickListener;

    .line 1967586
    return-void

    .line 1967587
    :cond_2
    iget-object v0, p0, LX/D7k;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1967588
    iget-object v0, p0, LX/D7k;->f:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(ILandroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D8b;LX/D8V;ILX/D8S;)V
    .locals 3
    .param p9    # LX/D8S;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/D8b;",
            "Lcom/facebook/video/watchandmore/core/OnExitWatchAndMoreListener;",
            "I",
            "Lcom/facebook/video/watchandmore/core/WatchAndMoreContentAnimationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1967534
    invoke-super/range {p0 .. p9}, LX/2my;->a(ILandroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D8b;LX/D8V;ILX/D8S;)V

    .line 1967535
    invoke-static {p5}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1967536
    if-eqz v1, :cond_0

    .line 1967537
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1967538
    if-eqz v0, :cond_0

    .line 1967539
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1967540
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1967541
    if-eqz v0, :cond_0

    .line 1967542
    iget-object v2, p0, LX/D7k;->d:LX/1nA;

    invoke-virtual {v2, v1, v0}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, LX/D7k;->h:Landroid/view/View$OnClickListener;

    .line 1967543
    :cond_0
    iput-object p2, p0, LX/D7k;->g:Landroid/view/ViewGroup;

    .line 1967544
    iput-object p3, p0, LX/D7k;->f:Landroid/view/ViewGroup;

    .line 1967545
    const/4 p2, 0x0

    .line 1967546
    if-eqz p4, :cond_1

    iget-object v0, p0, LX/D7k;->f:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D7k;->h:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2my;->a:LX/2nF;

    invoke-virtual {v0}, LX/2nF;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1967547
    :cond_1
    :goto_0
    return-void

    .line 1967548
    :cond_2
    iget-object v0, p0, LX/D7k;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1967549
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03160b

    iget-object v2, p0, LX/D7k;->f:Landroid/view/ViewGroup;

    const/4 p1, 0x1

    invoke-virtual {v0, v1, v2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1967550
    const/4 v0, 0x0

    .line 1967551
    iget-object v1, p0, LX/2my;->a:LX/2nF;

    invoke-virtual {v1}, LX/2nF;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1967552
    iget-object v0, p0, LX/D7k;->f:Landroid/view/ViewGroup;

    const v1, 0x7f0d318e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1967553
    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 1967554
    :cond_3
    :goto_1
    if-eqz v0, :cond_1

    .line 1967555
    iget-object v1, p0, LX/D7k;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1967556
    :cond_4
    iget-object v1, p0, LX/2my;->a:LX/2nF;

    invoke-virtual {v1}, LX/2nF;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1967557
    iget-object v0, p0, LX/D7k;->f:Landroid/view/ViewGroup;

    const v1, 0x7f0d318f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1967558
    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 1967559
    const v1, 0x7f0d3190

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public final c()LX/D8g;
    .locals 1

    .prologue
    .line 1967533
    sget-object v0, LX/D8g;->WATCH_AND_INSTALL:LX/D8g;

    return-object v0
.end method

.method public final c(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/content/Intent;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1967514
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move-object v0, v1

    .line 1967515
    :cond_1
    :goto_0
    return-object v0

    .line 1967516
    :cond_2
    iget-object v2, p0, LX/2my;->b:LX/2nH;

    .line 1967517
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1967518
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2nH;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 1967519
    if-nez v0, :cond_3

    move-object v0, v1

    .line 1967520
    goto :goto_0

    .line 1967521
    :cond_3
    iget-object v2, p0, LX/D7k;->e:LX/17d;

    .line 1967522
    invoke-static {v0}, LX/17d;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1967523
    if-nez v4, :cond_5

    .line 1967524
    const/4 v4, 0x0

    .line 1967525
    :goto_1
    move-object v0, v4

    .line 1967526
    if-nez v0, :cond_4

    move-object v0, v1

    .line 1967527
    goto :goto_0

    .line 1967528
    :cond_4
    const-string v1, "watch_and_browse_is_in_watch_and_install"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1967529
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_1

    iget-object v1, p0, LX/2my;->a:LX/2nF;

    invoke-virtual {v1}, LX/2nF;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1967530
    const-string v1, "BrowserLiteIntent.EXTRA_WEBVIEW_LAYTER_TYPE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 1967531
    :cond_5
    invoke-static {v2, v4}, LX/17d;->e(LX/17d;Landroid/net/Uri;)LX/31z;

    move-result-object p2

    .line 1967532
    invoke-static {p1, v4, p2}, LX/17d;->c(Landroid/content/Context;Landroid/net/Uri;LX/31z;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_1
.end method

.method public final e()LX/D7g;
    .locals 2

    .prologue
    .line 1967511
    iget-object v0, p0, LX/2my;->c:LX/D7g;

    if-nez v0, :cond_0

    .line 1967512
    new-instance v0, LX/D7j;

    invoke-direct {v0, p0}, LX/D7j;-><init>(LX/D7k;)V

    iput-object v0, p0, LX/D7k;->c:LX/D7g;

    .line 1967513
    :cond_0
    iget-object v0, p0, LX/2my;->c:LX/D7g;

    return-object v0
.end method
