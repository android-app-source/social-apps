.class public LX/CpT;
.super LX/Cos;
.source ""

# interfaces
.implements LX/CnE;
.implements LX/CnG;
.implements LX/CnH;
.implements LX/CnI;
.implements LX/CnJ;
.implements LX/Coc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cos",
        "<",
        "LX/CoA;",
        "Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;",
        ">;",
        "Lcom/facebook/richdocument/view/block/VideoBlockView;"
    }
.end annotation


# instance fields
.field public final A:Z

.field public final B:Z

.field public final C:Z

.field public D:Z

.field private E:LX/Cuh;

.field private F:LX/7MZ;

.field private G:LX/Cuu;

.field private H:LX/2pM;

.field private I:LX/CuK;

.field private J:LX/Cud;

.field public K:LX/Cpp;

.field private L:Landroid/os/Bundle;

.field public M:Ljava/lang/String;

.field public N:Ljava/lang/String;

.field public final O:LX/Cun;

.field public P:Z

.field public Q:Z

.field public R:LX/CuJ;

.field public S:LX/Cpq;

.field public T:LX/Cpq;

.field public final U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final V:LX/ChN;

.field private final W:LX/Ci3;

.field private final X:LX/Chm;

.field private Y:I

.field private Z:I

.field private final a:Z

.field private aa:Ljava/lang/String;

.field public ab:Ljava/lang/String;

.field private ac:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field public ad:Z

.field public ae:Z

.field public b:LX/1Bv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ciy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/Ckq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Cso;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Cuw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/CqV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Z

.field public w:LX/Cub;

.field public x:Z

.field public y:I

.field private final z:Z


# direct methods
.method public constructor <init>(LX/Ctg;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1937702
    invoke-direct {p0, p1, p2}, LX/Cos;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 1937703
    iput-boolean v3, p0, LX/CpT;->P:Z

    .line 1937704
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CpT;->U:Ljava/util/List;

    .line 1937705
    new-instance v0, LX/Cpj;

    invoke-direct {v0, p0}, LX/Cpj;-><init>(LX/CpT;)V

    iput-object v0, p0, LX/CpT;->V:LX/ChN;

    .line 1937706
    new-instance v0, LX/Cpk;

    invoke-direct {v0, p0}, LX/Cpk;-><init>(LX/CpT;)V

    iput-object v0, p0, LX/CpT;->W:LX/Ci3;

    .line 1937707
    new-instance v0, LX/Cpl;

    invoke-direct {v0, p0}, LX/Cpl;-><init>(LX/CpT;)V

    iput-object v0, p0, LX/CpT;->X:LX/Chm;

    .line 1937708
    iput v2, p0, LX/CpT;->y:I

    .line 1937709
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, LX/CpT;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1937710
    iput-boolean v3, p0, LX/CpT;->v:Z

    .line 1937711
    iget-object v0, p0, LX/CpT;->k:LX/0Uh;

    const/16 v1, 0x3ee

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CpT;->a:Z

    .line 1937712
    iget-object v0, p0, LX/CpT;->k:LX/0Uh;

    const/16 v1, 0xa4

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CpT;->C:Z

    .line 1937713
    iput-boolean v3, p0, LX/CpT;->D:Z

    .line 1937714
    iget-object v0, p0, LX/CpT;->p:LX/0Uh;

    const/16 v1, 0x6f

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CpT;->z:Z

    .line 1937715
    new-instance v0, LX/Cun;

    invoke-direct {v0}, LX/Cun;-><init>()V

    iput-object v0, p0, LX/CpT;->O:LX/Cun;

    .line 1937716
    iget-object v0, p0, LX/CpT;->l:LX/0ad;

    sget-short v1, LX/2yD;->l:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CpT;->A:Z

    .line 1937717
    iget-object v0, p0, LX/CpT;->l:LX/0ad;

    sget-short v1, LX/2yD;->k:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CpT;->B:Z

    .line 1937718
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v0

    .line 1937719
    iget-boolean v1, p0, LX/CpT;->v:Z

    .line 1937720
    iput-boolean v1, v0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->t:Z

    .line 1937721
    iget-object v1, p0, LX/CpT;->p:LX/0Uh;

    invoke-virtual {p0, v1}, LX/CpT;->a(LX/0Uh;)LX/Cso;

    move-result-object v1

    .line 1937722
    iput-object v1, v0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->r:LX/Cso;

    .line 1937723
    invoke-virtual {p0, p1}, LX/CpT;->a(LX/Ctg;)V

    .line 1937724
    iget-object v0, p0, LX/CpT;->e:LX/Chv;

    iget-object v1, p0, LX/CpT;->V:LX/ChN;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1937725
    iget-object v0, p0, LX/CpT;->e:LX/Chv;

    iget-object v1, p0, LX/CpT;->W:LX/Ci3;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1937726
    iget-object v0, p0, LX/CpT;->k:LX/0Uh;

    const/16 v1, 0xb7

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1937727
    invoke-virtual {p0}, LX/CpT;->w()LX/Cud;

    move-result-object v0

    iput-object v0, p0, LX/CpT;->J:LX/Cud;

    .line 1937728
    invoke-direct {p0}, LX/CpT;->o()LX/Cuu;

    move-result-object v0

    iput-object v0, p0, LX/CpT;->G:LX/Cuu;

    .line 1937729
    :cond_0
    return-void
.end method

.method private a(IIDLX/Csx;Lcom/facebook/video/engine/VideoPlayerParams;)LX/Cpq;
    .locals 3

    .prologue
    .line 1937730
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    .line 1937731
    const-string v1, "CoverImageParamsKey"

    invoke-virtual {v0, v1, p5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1937732
    invoke-virtual {p0, p6, p3, p4, v0}, LX/CpT;->a(Lcom/facebook/video/engine/VideoPlayerParams;DLX/0P2;)LX/2pa;

    move-result-object v0

    .line 1937733
    new-instance v1, LX/Cpq;

    invoke-direct {v1, p1, p2, v0}, LX/Cpq;-><init>(IILX/2pa;)V

    return-object v1
.end method

.method private static a(LX/CpT;LX/1Bv;LX/Ciy;LX/Chi;LX/Chv;LX/0Uh;LX/0ad;Ljava/lang/Boolean;LX/Ckq;LX/Cso;LX/0Uh;LX/Ckw;LX/Cuw;LX/03V;LX/0hB;LX/CqV;)V
    .locals 0

    .prologue
    .line 1937734
    iput-object p1, p0, LX/CpT;->b:LX/1Bv;

    iput-object p2, p0, LX/CpT;->c:LX/Ciy;

    iput-object p3, p0, LX/CpT;->d:LX/Chi;

    iput-object p4, p0, LX/CpT;->e:LX/Chv;

    iput-object p5, p0, LX/CpT;->k:LX/0Uh;

    iput-object p6, p0, LX/CpT;->l:LX/0ad;

    iput-object p7, p0, LX/CpT;->m:Ljava/lang/Boolean;

    iput-object p8, p0, LX/CpT;->n:LX/Ckq;

    iput-object p9, p0, LX/CpT;->o:LX/Cso;

    iput-object p10, p0, LX/CpT;->p:LX/0Uh;

    iput-object p11, p0, LX/CpT;->q:LX/Ckw;

    iput-object p12, p0, LX/CpT;->r:LX/Cuw;

    iput-object p13, p0, LX/CpT;->s:LX/03V;

    iput-object p14, p0, LX/CpT;->t:LX/0hB;

    iput-object p15, p0, LX/CpT;->u:LX/CqV;

    return-void
.end method

.method private a(Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;)V
    .locals 9

    .prologue
    .line 1937735
    new-instance v3, LX/CuX;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, LX/CuX;-><init>(Landroid/content/Context;)V

    .line 1937736
    invoke-static {p1, v3}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937737
    const-class v0, LX/CuJ;

    invoke-virtual {p0, v0}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1937738
    const-class v0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    .line 1937739
    new-instance v0, LX/CuJ;

    iget-object v1, p0, LX/CpT;->t:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    .line 1937740
    iget-object v2, p0, LX/Cos;->a:LX/Ctg;

    move-object v2, v2

    .line 1937741
    iget-object v4, p0, LX/CpT;->J:LX/Cud;

    iget-object v6, p0, LX/CpT;->G:LX/Cuu;

    if-nez v6, :cond_1

    const/4 v6, 0x0

    :goto_0
    iget-object v7, p0, LX/CpT;->I:LX/CuK;

    iget-object v8, p0, LX/CpT;->u:LX/CqV;

    invoke-direct/range {v0 .. v8}, LX/CuJ;-><init>(ILX/Ctg;LX/CuX;LX/Cud;Lcom/facebook/video/player/plugins/Video360HeadingPlugin;Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;LX/CuK;LX/CqV;)V

    iput-object v0, p0, LX/CpT;->R:LX/CuJ;

    .line 1937742
    iget-object v0, p0, LX/CpT;->R:LX/CuJ;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1937743
    :cond_0
    return-void

    .line 1937744
    :cond_1
    iget-object v6, p0, LX/CpT;->G:LX/Cuu;

    .line 1937745
    iget-object v7, v6, LX/Cuu;->o:Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

    move-object v6, v7

    .line 1937746
    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, LX/CpT;

    invoke-static {v15}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v1

    check-cast v1, LX/1Bv;

    invoke-static {v15}, LX/Ciy;->a(LX/0QB;)LX/Ciy;

    move-result-object v2

    check-cast v2, LX/Ciy;

    invoke-static {v15}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v3

    check-cast v3, LX/Chi;

    invoke-static {v15}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v4

    check-cast v4, LX/Chv;

    invoke-static {v15}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v15}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v15}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-static {v15}, LX/Ckq;->a(LX/0QB;)LX/Ckq;

    move-result-object v8

    check-cast v8, LX/Ckq;

    invoke-static {v15}, LX/Cso;->b(LX/0QB;)LX/Cso;

    move-result-object v9

    check-cast v9, LX/Cso;

    invoke-static {v15}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v15}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v11

    check-cast v11, LX/Ckw;

    invoke-static {v15}, LX/Cuw;->a(LX/0QB;)LX/Cuw;

    move-result-object v12

    check-cast v12, LX/Cuw;

    invoke-static {v15}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-static {v15}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v14

    check-cast v14, LX/0hB;

    invoke-static {v15}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object v15

    check-cast v15, LX/CqV;

    invoke-static/range {v0 .. v15}, LX/CpT;->a(LX/CpT;LX/1Bv;LX/Ciy;LX/Chi;LX/Chv;LX/0Uh;LX/0ad;Ljava/lang/Boolean;LX/Ckq;LX/Cso;LX/0Uh;LX/Ckw;LX/Cuw;LX/03V;LX/0hB;LX/CqV;)V

    return-void
.end method

.method private static b(II)D
    .locals 2

    .prologue
    .line 1937747
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, p0

    mul-float/2addr v0, v1

    int-to-float v1, p1

    div-float/2addr v0, v1

    float-to-double v0, v0

    return-wide v0
.end method

.method public static b(LX/CpT;I)V
    .locals 2

    .prologue
    .line 1937748
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v0

    .line 1937749
    if-nez v0, :cond_1

    .line 1937750
    :cond_0
    :goto_0
    return-void

    .line 1937751
    :cond_1
    const-class v1, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    .line 1937752
    if-eqz v0, :cond_0

    .line 1937753
    iget-object v1, v0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    move-object v1, v1

    .line 1937754
    if-eqz v1, :cond_0

    .line 1937755
    invoke-virtual {v1}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1937756
    if-eqz v0, :cond_0

    .line 1937757
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 1937758
    invoke-virtual {v1, v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public static e(LX/CpT;Z)V
    .locals 3

    .prologue
    .line 1937759
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v0

    .line 1937760
    if-nez v0, :cond_1

    .line 1937761
    :cond_0
    :goto_0
    return-void

    .line 1937762
    :cond_1
    if-nez p1, :cond_2

    .line 1937763
    const-class v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(Ljava/lang/Class;)Z

    goto :goto_0

    .line 1937764
    :cond_2
    const-class v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1937765
    new-instance v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;-><init>(Landroid/content/Context;)V

    .line 1937766
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937767
    goto :goto_0
.end method

.method private n()D
    .locals 2

    .prologue
    .line 1937768
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, LX/CpT;->t:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget-object v1, p0, LX/CpT;->t:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    return-wide v0
.end method

.method private o()LX/Cuu;
    .locals 5

    .prologue
    .line 1937769
    const/4 v1, 0x0

    .line 1937770
    :try_start_0
    new-instance v0, LX/Cuu;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/Cuu;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1937771
    :goto_0
    return-object v0

    .line 1937772
    :catch_0
    move-exception v0

    .line 1937773
    iget-object v2, p0, LX/CpT;->s:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Error while attempting to create VideoSeekBarPlugin"

    invoke-static {v3, v4}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v3

    .line 1937774
    iput-object v0, v3, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1937775
    move-object v0, v3

    .line 1937776
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/03V;->a(LX/0VG;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/facebook/video/engine/VideoPlayerParams;DLX/0P2;)LX/2pa;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "D",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "LX/2pa;"
        }
    .end annotation

    .prologue
    .line 1937777
    new-instance v0, LX/2pZ;

    invoke-direct {v0}, LX/2pZ;-><init>()V

    .line 1937778
    iput-object p1, v0, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1937779
    move-object v0, v0

    .line 1937780
    iput-wide p2, v0, LX/2pZ;->e:D

    .line 1937781
    move-object v0, v0

    .line 1937782
    invoke-virtual {p4}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    return-object v0
.end method

.method public a(LX/0Uh;)LX/Cso;
    .locals 1

    .prologue
    .line 1937783
    iget-object v0, p0, LX/CpT;->o:LX/Cso;

    return-object v0
.end method

.method public a(LX/Cli;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 1937784
    iput-object p2, p0, LX/CpT;->ab:Ljava/lang/String;

    .line 1937785
    iget-object v0, p1, LX/Cli;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1937786
    iput-object v0, p0, LX/CpT;->aa:Ljava/lang/String;

    .line 1937787
    iget-object v0, p1, LX/Cli;->j:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-object v0, v0

    .line 1937788
    iput-object v0, p0, LX/CpT;->ac:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1937789
    iget v0, p1, LX/Cli;->g:I

    move v0, v0

    .line 1937790
    iput v0, p0, LX/CpT;->Y:I

    .line 1937791
    iget v0, p1, LX/Cli;->h:I

    move v0, v0

    .line 1937792
    iput v0, p0, LX/CpT;->Z:I

    .line 1937793
    iget-boolean v0, p1, LX/Cli;->i:Z

    move v0, v0

    .line 1937794
    iput-boolean v0, p0, LX/CpT;->Q:Z

    .line 1937795
    const-class v0, LX/Cu1;

    invoke-virtual {p0, v0}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v0

    check-cast v0, LX/Cu1;

    .line 1937796
    if-eqz v0, :cond_0

    .line 1937797
    iput-object p2, v0, LX/Cu1;->b:Ljava/lang/String;

    .line 1937798
    :cond_0
    iget-object v0, p0, LX/CpT;->ac:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-static {v0}, LX/CoV;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Cqw;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Cqw;)V

    .line 1937799
    iget-object v0, p1, LX/Cli;->k:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-object v0, v0

    .line 1937800
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    if-ne v0, v2, :cond_5

    iget-object v0, p0, LX/CpT;->b:LX/1Bv;

    invoke-virtual {v0}, LX/1Bv;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/CpT;->x:Z

    .line 1937801
    invoke-virtual {p0, p1}, LX/CpT;->c(LX/Cli;)V

    .line 1937802
    invoke-virtual {p0, p1}, LX/CpT;->b(LX/Cli;)V

    .line 1937803
    iget-boolean v0, p1, LX/Cli;->i:Z

    move v0, v0

    .line 1937804
    if-nez v0, :cond_6

    move v0, v1

    .line 1937805
    :goto_1
    if-eqz v0, :cond_9

    .line 1937806
    const-class v2, LX/CuI;

    invoke-virtual {p0, v2}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1937807
    new-instance v2, LX/CuI;

    .line 1937808
    iget-object v3, p0, LX/Cos;->a:LX/Ctg;

    move-object v3, v3

    .line 1937809
    invoke-direct {v2, v3}, LX/CuI;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v2}, LX/Cos;->a(LX/Ctr;)V

    .line 1937810
    :cond_1
    :goto_2
    invoke-static {p0, v8}, LX/CpT;->e(LX/CpT;Z)V

    .line 1937811
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v9

    .line 1937812
    iget-boolean v0, p1, LX/Cli;->i:Z

    move v0, v0

    .line 1937813
    const-class v2, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v9, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v2

    check-cast v2, Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 1937814
    if-nez v0, :cond_a

    if-eqz v2, :cond_a

    .line 1937815
    :goto_3
    iget v0, p1, LX/Cli;->c:I

    move v2, v0

    .line 1937816
    iget v0, p1, LX/Cli;->d:I

    move v3, v0

    .line 1937817
    invoke-static {v2, v3}, LX/CpT;->b(II)D

    move-result-wide v4

    .line 1937818
    new-instance v6, LX/Csx;

    .line 1937819
    iget-object v0, p1, LX/Cli;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1937820
    iget-object v7, p1, LX/Cli;->f:Ljava/lang/String;

    move-object v7, v7

    .line 1937821
    iget v10, p1, LX/Cli;->g:I

    move v10, v10

    .line 1937822
    iget v11, p1, LX/Cli;->h:I

    move v11, v11

    .line 1937823
    invoke-direct {v6, v0, v7, v10, v11}, LX/Csx;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 1937824
    iget-boolean v0, p1, LX/Cli;->i:Z

    move v0, v0

    .line 1937825
    if-eqz v0, :cond_7

    iget-boolean v0, p0, LX/CpT;->ae:Z

    if-eqz v0, :cond_7

    move v0, v1

    .line 1937826
    :goto_4
    if-eqz v0, :cond_2

    .line 1937827
    iget-object v1, p0, LX/CpT;->t:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v2

    .line 1937828
    iget-object v1, p0, LX/CpT;->t:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v3

    .line 1937829
    invoke-direct {p0}, LX/CpT;->n()D

    move-result-wide v4

    .line 1937830
    :cond_2
    iget-object v1, p1, LX/Cli;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v7, v1

    .line 1937831
    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LX/CpT;->a(IIDLX/Csx;Lcom/facebook/video/engine/VideoPlayerParams;)LX/Cpq;

    move-result-object v1

    iput-object v1, p0, LX/CpT;->S:LX/Cpq;

    .line 1937832
    const/4 v1, 0x0

    iput-object v1, p0, LX/CpT;->T:LX/Cpq;

    .line 1937833
    if-nez v0, :cond_3

    .line 1937834
    iget-boolean v0, p1, LX/Cli;->i:Z

    move v0, v0

    .line 1937835
    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/CpT;->C:Z

    if-eqz v0, :cond_3

    .line 1937836
    iget-object v0, p0, LX/CpT;->t:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v2

    iget-object v0, p0, LX/CpT;->t:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v3

    invoke-direct {p0}, LX/CpT;->n()D

    move-result-wide v4

    .line 1937837
    iget-object v0, p1, LX/Cli;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v7, v0

    .line 1937838
    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LX/CpT;->a(IIDLX/Csx;Lcom/facebook/video/engine/VideoPlayerParams;)LX/Cpq;

    move-result-object v0

    iput-object v0, p0, LX/CpT;->T:LX/Cpq;

    .line 1937839
    :cond_3
    iget-boolean v0, p1, LX/Cli;->i:Z

    move v0, v0

    .line 1937840
    if-eqz v0, :cond_8

    .line 1937841
    new-instance v0, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    .line 1937842
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v1

    .line 1937843
    invoke-static {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937844
    invoke-direct {p0, v9}, LX/CpT;->a(Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;)V

    .line 1937845
    :goto_5
    iget-object v0, p0, LX/CpT;->S:LX/Cpq;

    invoke-virtual {v0, v9}, LX/Cpq;->a(Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;)V

    .line 1937846
    iget-object v0, p0, LX/CpT;->I:LX/CuK;

    if-eqz v0, :cond_4

    .line 1937847
    iget-object v0, p0, LX/CpT;->I:LX/CuK;

    .line 1937848
    invoke-virtual {v0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1937849
    new-instance v2, LX/Ctq;

    invoke-virtual {v0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/Ctq;-><init>(Landroid/content/Context;)V

    iput-object v2, v0, LX/CuK;->a:LX/Ctq;

    .line 1937850
    const-class v2, LX/Ctq;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(Ljava/lang/Class;)Z

    .line 1937851
    iget-object v2, v0, LX/CuK;->a:LX/Ctq;

    .line 1937852
    invoke-static {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937853
    iget-object v1, v0, LX/CuK;->a:LX/Ctq;

    .line 1937854
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v1, LX/Ctq;->f:Ljava/lang/ref/WeakReference;

    .line 1937855
    :cond_4
    new-instance v0, LX/Cpo;

    invoke-direct {v0, p0}, LX/Cpo;-><init>(LX/CpT;)V

    .line 1937856
    iput-object v0, v9, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->p:LX/Cpn;

    .line 1937857
    return-void

    :cond_5
    move v0, v8

    .line 1937858
    goto/16 :goto_0

    :cond_6
    move v0, v8

    .line 1937859
    goto/16 :goto_1

    :cond_7
    move v0, v8

    .line 1937860
    goto/16 :goto_4

    .line 1937861
    :cond_8
    const-class v0, LX/CuJ;

    invoke-virtual {p0, v0}, LX/Cos;->b(Ljava/lang/Class;)V

    .line 1937862
    const-class v0, LX/CuX;

    invoke-virtual {v9, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(Ljava/lang/Class;)Z

    .line 1937863
    goto :goto_5

    .line 1937864
    :cond_9
    const-class v2, LX/CuI;

    invoke-virtual {p0, v2}, LX/Cos;->b(Ljava/lang/Class;)V

    goto/16 :goto_2

    .line 1937865
    :cond_a
    invoke-virtual {v9}, Lcom/facebook/video/player/RichVideoPlayer;->l()Z

    .line 1937866
    iget-boolean v2, p0, LX/CpT;->C:Z

    if-eqz v2, :cond_b

    .line 1937867
    new-instance v2, LX/CuO;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/CuO;-><init>(Landroid/content/Context;)V

    .line 1937868
    invoke-static {v9, v2}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937869
    goto/16 :goto_3

    .line 1937870
    :cond_b
    new-instance v2, LX/CuM;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/CuM;-><init>(Landroid/content/Context;)V

    .line 1937871
    invoke-static {v9, v2}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937872
    const/4 v2, 0x1

    invoke-static {p0, v2}, LX/CpT;->e(LX/CpT;Z)V

    goto/16 :goto_3
.end method

.method public a(LX/Ctg;)V
    .locals 2

    .prologue
    .line 1937873
    new-instance v0, LX/CuI;

    invoke-direct {v0, p1}, LX/CuI;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1937874
    new-instance v0, LX/CuA;

    invoke-direct {v0, p1}, LX/CuA;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1937875
    new-instance v0, LX/Cu1;

    invoke-direct {v0, p1}, LX/Cu1;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1937876
    iget-boolean v0, p0, LX/CpT;->v:Z

    if-eqz v0, :cond_0

    .line 1937877
    new-instance v0, LX/Cub;

    invoke-direct {v0, p1}, LX/Cub;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/CpT;->w:LX/Cub;

    .line 1937878
    iget-object v0, p0, LX/CpT;->w:LX/Cub;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1937879
    new-instance v0, LX/Cuh;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Cuh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/CpT;->E:LX/Cuh;

    .line 1937880
    iget-object v0, p0, LX/CpT;->E:LX/Cuh;

    iget-object v1, p0, LX/CpT;->w:LX/Cub;

    .line 1937881
    iput-object v1, v0, LX/Cuh;->c:LX/Cub;

    .line 1937882
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v0

    iget-object v1, p0, LX/CpT;->E:LX/Cuh;

    .line 1937883
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937884
    :cond_0
    iget-object v0, p0, LX/CpT;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1937885
    new-instance v0, LX/7MZ;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7MZ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/CpT;->F:LX/7MZ;

    .line 1937886
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v0

    iget-object v1, p0, LX/CpT;->F:LX/7MZ;

    .line 1937887
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937888
    :cond_1
    invoke-virtual {p0}, LX/CpT;->x()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1937889
    new-instance v0, LX/CuK;

    invoke-direct {v0, p1}, LX/CuK;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/CpT;->I:LX/CuK;

    .line 1937890
    iget-object v0, p0, LX/CpT;->I:LX/CuK;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 1937891
    iget-object v0, p0, LX/CpT;->w:LX/Cub;

    if-eqz v0, :cond_2

    .line 1937892
    iget-object v0, p0, LX/CpT;->w:LX/Cub;

    iget-object v1, p0, LX/CpT;->I:LX/CuK;

    .line 1937893
    iput-object v1, v0, LX/Cub;->s:LX/CuK;

    .line 1937894
    :cond_2
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1937641
    invoke-super {p0, p1}, LX/Cos;->a(Landroid/os/Bundle;)V

    .line 1937642
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CpT;->ad:Z

    .line 1937643
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->z()V

    .line 1937644
    iput-object v1, p0, LX/CpT;->K:LX/Cpp;

    .line 1937645
    iput-object v1, p0, LX/CpT;->L:Landroid/os/Bundle;

    .line 1937646
    iget-object v0, p0, LX/CpT;->r:LX/Cuw;

    invoke-virtual {v0}, LX/Cuw;->a()V

    .line 1937647
    iput-object v1, p0, LX/CpT;->aa:Ljava/lang/String;

    .line 1937648
    iput-object v1, p0, LX/CpT;->S:LX/Cpq;

    .line 1937649
    iput-object v1, p0, LX/CpT;->T:LX/Cpq;

    .line 1937650
    return-void
.end method

.method public final b(LX/Cli;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1937651
    iget-boolean v0, p0, LX/CpT;->v:Z

    if-eqz v0, :cond_6

    .line 1937652
    iget-object v0, p0, LX/CpT;->J:LX/Cud;

    if-nez v0, :cond_0

    .line 1937653
    invoke-virtual {p0}, LX/CpT;->w()LX/Cud;

    move-result-object v0

    iput-object v0, p0, LX/CpT;->J:LX/Cud;

    .line 1937654
    :cond_0
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1937655
    iget-object v2, p0, LX/CpT;->J:LX/Cud;

    invoke-interface {v0, v2}, LX/Cq9;->a(LX/CnQ;)V

    .line 1937656
    iget-object v0, p1, LX/Cli;->l:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-object v0, v0

    .line 1937657
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->CONTROLS:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    if-ne v0, v2, :cond_7

    move v0, v1

    .line 1937658
    :goto_0
    iget-boolean v2, p1, LX/Cli;->i:Z

    move v2, v2

    .line 1937659
    if-eqz v2, :cond_1

    invoke-virtual {p0}, LX/CpT;->s()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 1937660
    :cond_1
    if-eqz v0, :cond_3

    iget-boolean v1, p0, LX/CpT;->D:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, LX/CpT;->a:Z

    if-eqz v1, :cond_3

    .line 1937661
    iget-object v1, p0, LX/CpT;->G:LX/Cuu;

    if-nez v1, :cond_2

    .line 1937662
    invoke-direct {p0}, LX/CpT;->o()LX/Cuu;

    move-result-object v1

    iput-object v1, p0, LX/CpT;->G:LX/Cuu;

    .line 1937663
    :cond_2
    iget-object v1, p0, LX/CpT;->G:LX/Cuu;

    if-eqz v1, :cond_3

    .line 1937664
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v1

    iget-object v2, p0, LX/CpT;->G:LX/Cuu;

    .line 1937665
    invoke-static {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937666
    iget-object v1, p0, LX/CpT;->G:LX/Cuu;

    .line 1937667
    iget-object v2, v1, LX/Cuu;->o:Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

    move-object v1, v2

    .line 1937668
    iget-object v2, p0, LX/Cos;->a:LX/Ctg;

    move-object v2, v2

    .line 1937669
    invoke-interface {v2, v1}, LX/Cq9;->a(LX/CnQ;)V

    .line 1937670
    iget-object v2, p0, LX/Cos;->i:LX/CoV;

    move-object v2, v2

    .line 1937671
    invoke-virtual {v2, v1}, LX/CoV;->a(Landroid/view/View;)V

    .line 1937672
    iget-object v1, p0, LX/CpT;->w:LX/Cub;

    iget-object v2, p0, LX/CpT;->G:LX/Cuu;

    .line 1937673
    iput-object v2, v1, LX/Cub;->j:LX/Cuu;

    .line 1937674
    :cond_3
    iget-boolean v1, p1, LX/Cli;->i:Z

    move v1, v1

    .line 1937675
    if-eqz v1, :cond_8

    iget-object v1, p0, LX/CpT;->w:LX/Cub;

    if-eqz v1, :cond_8

    .line 1937676
    iget-object v1, p0, LX/CpT;->H:LX/2pM;

    if-nez v1, :cond_4

    .line 1937677
    new-instance v1, LX/2pM;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/2pM;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/CpT;->H:LX/2pM;

    .line 1937678
    :cond_4
    iget-object v1, p0, LX/CpT;->w:LX/Cub;

    iget-object v2, p0, LX/CpT;->H:LX/2pM;

    .line 1937679
    iput-object v2, v1, LX/Cub;->k:LX/2pM;

    .line 1937680
    :goto_1
    iget-object v1, p0, LX/CpT;->w:LX/Cub;

    iget-object v2, p0, LX/CpT;->J:LX/Cud;

    iget-boolean v3, p0, LX/CpT;->x:Z

    iget-object v4, p0, LX/CpT;->r:LX/Cuw;

    .line 1937681
    iput-object v2, v1, LX/Cub;->i:LX/Cud;

    .line 1937682
    iput-boolean v3, v1, LX/Cub;->l:Z

    .line 1937683
    iput-boolean v0, v1, LX/Cub;->m:Z

    .line 1937684
    iput-object v4, v1, LX/Cub;->r:LX/Cuw;

    .line 1937685
    iget-boolean v5, v1, LX/Cub;->n:Z

    .line 1937686
    iput-boolean v5, v2, LX/Cud;->e:Z

    .line 1937687
    iget-object v5, v1, LX/Cub;->r:LX/Cuw;

    invoke-virtual {v2, v5, v1}, LX/Cud;->a(LX/Cuw;LX/Cub;)V

    .line 1937688
    invoke-virtual {v1}, LX/Cub;->a()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v5

    .line 1937689
    iget-object p0, v1, LX/Cub;->k:LX/2pM;

    if-nez p0, :cond_9

    .line 1937690
    const-class p0, LX/2pM;

    invoke-virtual {v5, p0}, Lcom/facebook/video/player/RichVideoPlayer;->b(Ljava/lang/Class;)Z

    .line 1937691
    :cond_5
    :goto_2
    iget-object v5, v1, LX/Cub;->a:LX/Cju;

    const p0, 0x7f0d016b

    invoke-interface {v5, p0}, LX/Cju;->c(I)I

    move-result v5

    .line 1937692
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 p1, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v2, p0, v5, p1}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1937693
    :cond_6
    return-void

    .line 1937694
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1937695
    :cond_8
    iget-object v1, p0, LX/CpT;->w:LX/Cub;

    const/4 v2, 0x0

    .line 1937696
    iput-object v2, v1, LX/Cub;->k:LX/2pM;

    .line 1937697
    goto :goto_1

    .line 1937698
    :cond_9
    const-class p0, LX/2pM;

    invoke-virtual {v5, p0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object p0

    if-nez p0, :cond_5

    .line 1937699
    iget-object p0, v1, LX/Cub;->k:LX/2pM;

    .line 1937700
    invoke-static {v5, p0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937701
    goto :goto_2
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 1937575
    invoke-super {p0, p1}, LX/Cos;->b(Landroid/os/Bundle;)V

    .line 1937576
    iget-object v0, p0, LX/CpT;->n:LX/Ckq;

    iget-object v1, p0, LX/CpT;->ab:Ljava/lang/String;

    iget v2, p0, LX/CpT;->Y:I

    iget v3, p0, LX/CpT;->Z:I

    iget-object v4, p0, LX/CpT;->ac:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iget-boolean v5, p0, LX/CpT;->ad:Z

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    iget-boolean v6, p0, LX/CpT;->ad:Z

    invoke-virtual/range {v0 .. v6}, LX/Ckq;->a(Ljava/lang/String;IILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;ZZ)V

    .line 1937577
    invoke-virtual {p0, p1}, LX/CpT;->e(Landroid/os/Bundle;)V

    .line 1937578
    iput-object p1, p0, LX/CpT;->L:Landroid/os/Bundle;

    .line 1937579
    new-instance v0, LX/Cpm;

    invoke-direct {v0, p0, p1}, LX/Cpm;-><init>(LX/CpT;Landroid/os/Bundle;)V

    .line 1937580
    iget-object v1, p0, LX/Cos;->f:LX/CoR;

    move-object v1, v1

    .line 1937581
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v2

    new-instance v3, LX/CoQ;

    sget-object v4, LX/CoP;->PERCENTAGE:LX/CoP;

    iget v5, p0, LX/CpT;->y:I

    invoke-direct {v3, v4, v5}, LX/CoQ;-><init>(LX/CoP;I)V

    invoke-virtual {v1, v2, v3, v0}, LX/CoR;->a(Landroid/view/View;LX/CoQ;LX/CoO;)V

    .line 1937582
    iget-object v0, p0, LX/CpT;->e:LX/Chv;

    iget-object v1, p0, LX/CpT;->X:LX/Chm;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1937583
    return-void

    .line 1937584
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public c(LX/Cli;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 1937585
    iget-object v1, p1, LX/Cli;->j:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-object v1, v1

    .line 1937586
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-ne v1, v2, :cond_0

    move v3, v7

    .line 1937587
    :goto_0
    iget-object v1, p1, LX/Cli;->l:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-object v1, v1

    .line 1937588
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->CONTROLS:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    if-ne v1, v2, :cond_1

    move v1, v7

    .line 1937589
    :goto_1
    iget-object v2, p1, LX/Cli;->j:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-object v2, v2

    .line 1937590
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->NON_INTERACTIVE:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-eq v2, v4, :cond_2

    move v6, v7

    .line 1937591
    :goto_2
    iget-boolean v0, p1, LX/Cli;->i:Z

    move v0, v0

    .line 1937592
    if-eqz v0, :cond_3

    invoke-virtual {p0}, LX/CpT;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    move v4, v7

    .line 1937593
    :goto_3
    iget-object v0, p0, LX/CpT;->r:LX/Cuw;

    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v1

    .line 1937594
    iget-object v2, p0, LX/CpT;->O:LX/Cun;

    move-object v2, v2

    .line 1937595
    iget-boolean v5, p0, LX/CpT;->x:Z

    .line 1937596
    iget-boolean v8, p1, LX/Cli;->i:Z

    move v8, v8

    .line 1937597
    iget-object v9, p0, LX/CpT;->p:LX/0Uh;

    invoke-virtual {p0, v9}, LX/CpT;->a(LX/0Uh;)LX/Cso;

    move-result-object v9

    invoke-virtual/range {v0 .. v9}, LX/Cuw;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/Cun;ZZZZZZLX/Cso;)V

    .line 1937598
    return-void

    :cond_0
    move v3, v0

    .line 1937599
    goto :goto_0

    :cond_1
    move v1, v0

    .line 1937600
    goto :goto_1

    :cond_2
    move v6, v0

    .line 1937601
    goto :goto_2

    :cond_3
    move v4, v1

    goto :goto_3
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1937602
    invoke-super {p0, p1}, LX/Cos;->c(Landroid/os/Bundle;)V

    .line 1937603
    iget-object v0, p0, LX/Cos;->f:LX/CoR;

    move-object v0, v0

    .line 1937604
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CoR;->a(Landroid/view/View;)V

    .line 1937605
    iget-object v0, p0, LX/CpT;->e:LX/Chv;

    iget-object v1, p0, LX/CpT;->X:LX/Chm;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1937606
    invoke-virtual {p0}, LX/CpT;->g()V

    .line 1937607
    return-void
.end method

.method public final d()LX/CnN;
    .locals 6

    .prologue
    .line 1937608
    iget-object v0, p0, LX/Cos;->i:LX/CoV;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/CpT;->d:LX/Chi;

    iget-object v3, p0, LX/CpT;->aa:Ljava/lang/String;

    const v4, 0x4ed245b

    const/16 v5, 0x3ed

    invoke-virtual/range {v0 .. v5}, LX/CoV;->a(Landroid/content/Context;LX/Chi;Ljava/lang/String;II)LX/CnN;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1937609
    iget-object v0, p0, LX/CpT;->r:LX/Cuw;

    .line 1937610
    iput-object p1, v0, LX/Cuw;->d:Landroid/os/Bundle;

    .line 1937611
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 1937612
    new-instance v1, Lcom/facebook/richdocument/view/block/impl/VideoBlockViewImpl$5;

    invoke-direct {v1, p0, p0}, Lcom/facebook/richdocument/view/block/impl/VideoBlockViewImpl$5;-><init>(LX/CpT;LX/Coc;)V

    .line 1937613
    iget-object v0, p0, LX/CpT;->c:LX/Ciy;

    invoke-virtual {v0}, LX/Ciy;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1937614
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1937615
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1937616
    :goto_0
    return-void

    .line 1937617
    :cond_0
    iget-object v0, p0, LX/Cod;->a:LX/1a1;

    move-object v0, v0

    .line 1937618
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1937619
    :cond_1
    iget-object v0, p0, LX/CpT;->U:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 1937620
    new-instance v1, Lcom/facebook/richdocument/view/block/impl/VideoBlockViewImpl$6;

    invoke-direct {v1, p0, p0}, Lcom/facebook/richdocument/view/block/impl/VideoBlockViewImpl$6;-><init>(LX/CpT;LX/Coc;)V

    .line 1937621
    iget-object v0, p0, LX/CpT;->c:LX/Ciy;

    invoke-virtual {v0}, LX/Ciy;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1937622
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1937623
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1937624
    :goto_0
    return-void

    .line 1937625
    :cond_0
    iget-object v0, p0, LX/Cod;->a:LX/1a1;

    move-object v0, v0

    .line 1937626
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1937627
    :cond_1
    iget-object v0, p0, LX/CpT;->U:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final p()Landroid/view/View;
    .locals 1

    .prologue
    .line 1937628
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v0

    return-object v0
.end method

.method public final q()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1937629
    invoke-virtual {p0}, LX/CpT;->u()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1937630
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v1

    sget-object v2, LX/2qV;->PREPARED:LX/2qV;

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->A()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1937631
    :cond_1
    return v0
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 1937632
    iget-object v0, p0, LX/CpT;->r:LX/Cuw;

    sget-object v1, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    invoke-virtual {v0, v1}, LX/Cuw;->a(LX/Cuj;)Z

    .line 1937633
    return-void
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 1937634
    const/4 v0, 0x1

    return v0
.end method

.method public t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;
    .locals 1

    .prologue
    .line 1937640
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    return-object v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 1937635
    iget-boolean v0, p0, LX/CpT;->x:Z

    return v0
.end method

.method public w()LX/Cud;
    .locals 2

    .prologue
    .line 1937636
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1937637
    iget-object v1, p0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 1937638
    invoke-interface {v1}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;->a(Landroid/content/Context;Landroid/view/ViewGroup;)LX/Cud;

    move-result-object v0

    return-object v0
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 1937639
    iget-boolean v0, p0, LX/CpT;->z:Z

    return v0
.end method
