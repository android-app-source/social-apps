.class public LX/Eui;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/0TD;

.field public c:LX/2dl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field public e:I


# direct methods
.method private constructor <init>(LX/0TD;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2179871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179872
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179873
    iput-object v0, p0, LX/Eui;->d:LX/0Ot;

    .line 2179874
    const/4 v0, 0x0

    iput v0, p0, LX/Eui;->e:I

    .line 2179875
    iput-object p1, p0, LX/Eui;->b:LX/0TD;

    .line 2179876
    invoke-static {}, LX/Eui;->d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/Eui;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2179877
    return-void
.end method

.method public static a(LX/0QB;)LX/Eui;
    .locals 1

    .prologue
    .line 2179931
    invoke-static {p0}, LX/Eui;->b(LX/0QB;)LX/Eui;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Euc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179930
    new-instance v0, LX/Euf;

    invoke-direct {v0, p0}, LX/Euf;-><init>(LX/Eui;)V

    iget-object v1, p0, LX/Eui;->b:LX/0TD;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/Eui;Lcom/facebook/common/callercontext/CallerContext;ILX/0zS;Ljava/lang/String;)LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I",
            "LX/0zS;",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179918
    const-string v0, "You must provide a caller context"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2179919
    new-instance v0, LX/EvI;

    invoke-direct {v0}, LX/EvI;-><init>()V

    move-object v0, v0

    .line 2179920
    const-string v1, "after"

    iget-object v2, p0, LX/Eui;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "context"

    invoke-virtual {v1, v2, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2179921
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2179922
    invoke-virtual {v0, p3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    const-string v2, "FC_SUGGESTIONS_QUERY"

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    .line 2179923
    iput-object v2, v1, LX/0zO;->d:Ljava/util/Set;

    .line 2179924
    move-object v1, v1

    .line 2179925
    iput-object p1, v1, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2179926
    move-object v1, v1

    .line 2179927
    const/4 v2, 0x1

    .line 2179928
    iput-boolean v2, v1, LX/0zO;->p:Z

    .line 2179929
    return-object v0
.end method

.method private b(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;I)LX/0zO;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I",
            "LX/2hC;",
            "LX/0zS;",
            "I)",
            "LX/0zO",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2179905
    const-string v0, "You must provide a caller context"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2179906
    invoke-static {}, LX/Eve;->a()LX/Evd;

    move-result-object v0

    .line 2179907
    const-string v1, "location"

    iget-object v2, p3, LX/2hC;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "max"

    const-string v3, "250"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after"

    iget-object v3, p0, LX/Eui;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2179908
    const/4 v1, -0x1

    if-eq p5, v1, :cond_0

    .line 2179909
    const-string v1, "has_custom_picture_size"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v1

    const-string v2, "custom_picture_size"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2179910
    :cond_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2179911
    invoke-virtual {v0, p4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    const-string v2, "FC_SUGGESTIONS_QUERY"

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    .line 2179912
    iput-object v2, v1, LX/0zO;->d:Ljava/util/Set;

    .line 2179913
    move-object v1, v1

    .line 2179914
    iput-object p1, v1, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2179915
    move-object v1, v1

    .line 2179916
    iput-boolean v4, v1, LX/0zO;->p:Z

    .line 2179917
    return-object v0
.end method

.method public static b(LX/0QB;)LX/Eui;
    .locals 3

    .prologue
    .line 2179901
    new-instance v1, LX/Eui;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, LX/0TD;

    invoke-direct {v1, v0}, LX/Eui;-><init>(LX/0TD;)V

    .line 2179902
    invoke-static {p0}, LX/2dl;->a(LX/0QB;)LX/2dl;

    move-result-object v0

    check-cast v0, LX/2dl;

    const/16 v2, 0xafc

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 2179903
    iput-object v0, v1, LX/Eui;->c:LX/2dl;

    iput-object v2, v1, LX/Eui;->d:LX/0Ot;

    .line 2179904
    return-object v1
.end method

.method public static b(LX/Eui;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2179900
    new-instance v0, LX/Eug;

    invoke-direct {v0, p0}, LX/Eug;-><init>(LX/Eui;)V

    iget-object v1, p0, LX/Eui;->b:LX/0TD;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 2

    .prologue
    .line 2179896
    new-instance v0, LX/4a7;

    invoke-direct {v0}, LX/4a7;-><init>()V

    const/4 v1, 0x1

    .line 2179897
    iput-boolean v1, v0, LX/4a7;->b:Z

    .line 2179898
    move-object v0, v0

    .line 2179899
    invoke-virtual {v0}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I",
            "LX/2hC;",
            "LX/0zS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Euc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179895
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/Eui;->a(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I",
            "LX/2hC;",
            "LX/0zS;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Euc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179893
    invoke-direct/range {p0 .. p5}, LX/Eui;->b(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;I)LX/0zO;

    move-result-object v0

    .line 2179894
    iget-object v1, p0, LX/Eui;->c:LX/2dl;

    const-string v2, "FC_SUGGESTIONS_QUERY"

    invoke-virtual {v1, v0, v2}, LX/2dl;->a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Eui;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;ILX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I",
            "LX/2hC;",
            "LX/0zS;",
            "I",
            "LX/0TF",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Euc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179887
    invoke-direct/range {p0 .. p5}, LX/Eui;->b(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;I)LX/0zO;

    move-result-object v1

    .line 2179888
    iget-object v0, p0, LX/Eui;->c:LX/2dl;

    iget-object v2, p0, LX/Eui;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1My;

    new-instance v3, LX/Eud;

    invoke-direct {v3, p0, p6}, LX/Eud;-><init>(LX/Eui;LX/0TF;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2179889
    iget-object v5, v1, LX/0zO;->m:LX/0gW;

    move-object v5, v5

    .line 2179890
    iget-object v6, v5, LX/0gW;->f:Ljava/lang/String;

    move-object v5, v6

    .line 2179891
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, LX/Eui;->e:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, LX/Eui;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "FC_SUGGESTIONS_QUERY"

    invoke-virtual/range {v0 .. v5}, LX/2dl;->a(LX/0zO;LX/1My;LX/0TF;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2179892
    invoke-direct {p0, v0}, LX/Eui;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I",
            "LX/2hC;",
            "LX/0zS;",
            "LX/0TF",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Euc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179886
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, LX/Eui;->a(Lcom/facebook/common/callercontext/CallerContext;ILX/2hC;LX/0zS;ILX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2179885
    iget-object v0, p0, LX/Eui;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2179880
    invoke-static {}, LX/Eui;->d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/Eui;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2179881
    iget v0, p0, LX/Eui;->e:I

    if-lez v0, :cond_0

    .line 2179882
    iget-object v0, p0, LX/Eui;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->b()V

    .line 2179883
    const/4 v0, 0x0

    iput v0, p0, LX/Eui;->e:I

    .line 2179884
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2179878
    iget-object v0, p0, LX/Eui;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 2179879
    return-void
.end method
