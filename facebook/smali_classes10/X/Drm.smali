.class public final LX/Drm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;)V
    .locals 0

    .prologue
    .line 2049658
    iput-object p1, p0, LX/Drm;->a:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2049659
    iget-object v0, p0, LX/Drm;->a:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->d:LX/03V;

    sget-object v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failure prefetch VideoHome notification: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2049660
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2049661
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2049662
    if-eqz p1, :cond_0

    .line 2049663
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2049664
    if-nez v0, :cond_1

    .line 2049665
    :cond_0
    :goto_0
    return-void

    .line 2049666
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2049667
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2049668
    iget-object v1, p0, LX/Drm;->a:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v1, v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->t:LX/3Q4;

    .line 2049669
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_3

    .line 2049670
    :cond_2
    :goto_1
    goto :goto_0

    .line 2049671
    :cond_3
    iget-object p0, v1, LX/3Q4;->b:LX/3Q5;

    invoke-virtual {p0, v0}, LX/2AG;->b(Ljava/lang/Object;)V

    goto :goto_1
.end method
