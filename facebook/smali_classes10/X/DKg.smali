.class public LX/DKg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Sh;

.field private final c:LX/0So;

.field private final d:LX/0Sg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/88p;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:LX/88n;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<TT;>;"
        }
    .end annotation
.end field

.field public i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field public l:I

.field public m:J


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Callable;LX/0TF;LX/0So;LX/0Sh;LX/0Sg;)V
    .locals 3
    .param p5    # LX/0Sg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable;",
            "LX/0TF",
            "<TT;>;",
            "LX/0So;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1987433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1987434
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/DKg;->e:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1987435
    const/4 v0, 0x0

    iput v0, p0, LX/DKg;->l:I

    .line 1987436
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/DKg;->m:J

    .line 1987437
    iput-object p1, p0, LX/DKg;->a:Ljava/util/concurrent/Callable;

    .line 1987438
    iput-object p3, p0, LX/DKg;->c:LX/0So;

    .line 1987439
    iput-object p4, p0, LX/DKg;->b:LX/0Sh;

    .line 1987440
    iput-object v2, p0, LX/DKg;->f:LX/88p;

    .line 1987441
    iput-object v2, p0, LX/DKg;->g:LX/88n;

    .line 1987442
    iput-object p5, p0, LX/DKg;->d:LX/0Sg;

    .line 1987443
    invoke-direct {p0, p2}, LX/DKg;->a(LX/0TF;)V

    .line 1987444
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Callable;LX/0TF;LX/0So;LX/0Sh;LX/0Sg;LX/88n;LX/88p;)V
    .locals 2
    .param p5    # LX/0Sg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable;",
            "LX/0TF",
            "<TT;>;",
            "LX/0So;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/88n;",
            "LX/88p;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1987421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1987422
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/DKg;->e:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1987423
    const/4 v0, 0x0

    iput v0, p0, LX/DKg;->l:I

    .line 1987424
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/DKg;->m:J

    .line 1987425
    iput-object p1, p0, LX/DKg;->a:Ljava/util/concurrent/Callable;

    .line 1987426
    iput-object p3, p0, LX/DKg;->c:LX/0So;

    .line 1987427
    iput-object p4, p0, LX/DKg;->b:LX/0Sh;

    .line 1987428
    iput-object p6, p0, LX/DKg;->g:LX/88n;

    .line 1987429
    iput-object p7, p0, LX/DKg;->f:LX/88p;

    .line 1987430
    iput-object p5, p0, LX/DKg;->d:LX/0Sg;

    .line 1987431
    invoke-direct {p0, p2}, LX/DKg;->a(LX/0TF;)V

    .line 1987432
    return-void
.end method

.method private a(LX/0TF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1987419
    new-instance v0, LX/DKf;

    invoke-direct {v0, p0, p1}, LX/DKf;-><init>(LX/DKg;LX/0TF;)V

    iput-object v0, p0, LX/DKg;->h:LX/0Ve;

    .line 1987420
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(J)V
    .locals 5

    .prologue
    .line 1987375
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/DKg;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    add-long/2addr v0, p1

    .line 1987376
    iget-boolean v2, p0, LX/DKg;->k:Z

    if-nez v2, :cond_0

    iget-wide v2, p0, LX/DKg;->m:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 1987377
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1987378
    :cond_1
    :try_start_1
    iput-wide v0, p0, LX/DKg;->m:J

    .line 1987379
    iget v0, p0, LX/DKg;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/DKg;->l:I

    .line 1987380
    iget-object v1, p0, LX/DKg;->b:LX/0Sh;

    new-instance v2, Lcom/facebook/groups/datautil/GroupsIdempotentRequestRunner$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/groups/datautil/GroupsIdempotentRequestRunner$2;-><init>(LX/DKg;I)V

    invoke-virtual {v1, v2, p1, p2}, LX/0Sh;->b(Ljava/lang/Runnable;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1987381
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1987394
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/DKg;->j:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/DKg;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 1987395
    :goto_0
    monitor-exit p0

    return v0

    .line 1987396
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/DKg;->j:Z

    .line 1987397
    iget v0, p0, LX/DKg;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/DKg;->l:I

    .line 1987398
    const-wide v4, 0x7fffffffffffffffL

    iput-wide v4, p0, LX/DKg;->m:J

    .line 1987399
    iget-object v0, p0, LX/DKg;->d:LX/0Sg;

    if-eqz v0, :cond_2

    .line 1987400
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/DKg;->e:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1987401
    iget-object v0, p0, LX/DKg;->d:LX/0Sg;

    iget-object v3, p0, LX/DKg;->e:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, v3}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1987402
    :cond_2
    iget-object v0, p0, LX/DKg;->g:LX/88n;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/DKg;->f:LX/88p;

    if-eqz v0, :cond_3

    .line 1987403
    iget-object v0, p0, LX/DKg;->g:LX/88n;

    iget-object v2, p0, LX/DKg;->f:LX/88p;

    iget-object v3, p0, LX/DKg;->a:Ljava/util/concurrent/Callable;

    iget-object v4, p0, LX/DKg;->h:LX/0Ve;

    invoke-virtual {v0, v2, v3, v4}, LX/88n;->a(LX/88p;Ljava/util/concurrent/Callable;LX/0Ve;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 1987404
    goto :goto_0

    .line 1987405
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/DKg;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v0, p0, LX/DKg;->i:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1987406
    :try_start_3
    iget-object v0, p0, LX/DKg;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_4

    .line 1987407
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DKg;->j:Z

    .line 1987408
    :cond_4
    :goto_1
    iget-boolean v0, p0, LX/DKg;->j:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v0, :cond_6

    move v0, v2

    .line 1987409
    goto :goto_0

    .line 1987410
    :catch_0
    move-exception v0

    .line 1987411
    :try_start_4
    iget-object v3, p0, LX/DKg;->h:LX/0Ve;

    invoke-interface {v3, v0}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1987412
    :try_start_5
    iget-object v0, p0, LX/DKg;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_4

    .line 1987413
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DKg;->j:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 1987414
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1987415
    :catchall_1
    move-exception v0

    :try_start_6
    iget-object v1, p0, LX/DKg;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v1, :cond_5

    .line 1987416
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/DKg;->j:Z

    :cond_5
    throw v0

    .line 1987417
    :cond_6
    iget-object v0, p0, LX/DKg;->b:LX/0Sh;

    iget-object v2, p0, LX/DKg;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v3, p0, LX/DKg;->h:LX/0Ve;

    invoke-virtual {v0, v2, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v0, v1

    .line 1987418
    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 1987385
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/DKg;->j:Z

    if-eqz v0, :cond_1

    .line 1987386
    iget-object v0, p0, LX/DKg;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1987387
    iget-object v0, p0, LX/DKg;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1987388
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/DKg;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1987389
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DKg;->j:Z

    .line 1987390
    :cond_1
    iget v0, p0, LX/DKg;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/DKg;->l:I

    .line 1987391
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/DKg;->m:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1987392
    monitor-exit p0

    return-void

    .line 1987393
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1987382
    invoke-virtual {p0}, LX/DKg;->b()V

    .line 1987383
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DKg;->k:Z

    .line 1987384
    return-void
.end method
