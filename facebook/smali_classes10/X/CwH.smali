.class public final LX/CwH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CwA;


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:I

.field public C:I

.field public D:Z

.field public E:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Boolean;

.field public g:LX/CwF;

.field public h:Ljava/lang/String;

.field public i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

.field public j:Ljava/lang/String;

.field public k:Lcom/facebook/search/model/ReactionSearchData;

.field public l:LX/CwI;

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:D

.field public p:Ljava/lang/String;

.field public q:Z

.field public r:Ljava/lang/String;

.field public s:LX/103;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Z

.field public w:Z

.field public x:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1950323
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/CwH;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1950297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950298
    const-string v0, ""

    iput-object v0, p0, LX/CwH;->b:Ljava/lang/String;

    .line 1950299
    const-string v0, ""

    iput-object v0, p0, LX/CwH;->d:Ljava/lang/String;

    .line 1950300
    const-string v0, ""

    iput-object v0, p0, LX/CwH;->e:Ljava/lang/String;

    .line 1950301
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 1950302
    sget-object v0, LX/CwF;->keyword:LX/CwF;

    iput-object v0, p0, LX/CwH;->g:LX/CwF;

    .line 1950303
    iput-object v2, p0, LX/CwH;->h:Ljava/lang/String;

    .line 1950304
    iput-object v2, p0, LX/CwH;->i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 1950305
    iput-object v2, p0, LX/CwH;->j:Ljava/lang/String;

    .line 1950306
    sget-object v0, LX/CwI;->SUGGESTION:LX/CwI;

    iput-object v0, p0, LX/CwH;->l:LX/CwI;

    .line 1950307
    iput-boolean v3, p0, LX/CwH;->n:Z

    .line 1950308
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, LX/CwH;->o:D

    .line 1950309
    iput-object v2, p0, LX/CwH;->p:Ljava/lang/String;

    .line 1950310
    iput-object v2, p0, LX/CwH;->r:Ljava/lang/String;

    .line 1950311
    iput-object v2, p0, LX/CwH;->s:LX/103;

    .line 1950312
    iput-object v2, p0, LX/CwH;->t:Ljava/lang/String;

    .line 1950313
    iput-object v2, p0, LX/CwH;->u:Ljava/lang/String;

    .line 1950314
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CwH;->v:Z

    .line 1950315
    iput-boolean v3, p0, LX/CwH;->w:Z

    .line 1950316
    sget-object v0, LX/CwH;->a:LX/0Px;

    iput-object v0, p0, LX/CwH;->x:LX/0Px;

    .line 1950317
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1950318
    iput-object v0, p0, LX/CwH;->y:LX/0P1;

    .line 1950319
    iput-object v2, p0, LX/CwH;->A:LX/0P1;

    .line 1950320
    iput v3, p0, LX/CwH;->B:I

    .line 1950321
    iput v3, p0, LX/CwH;->C:I

    .line 1950322
    iput-boolean v3, p0, LX/CwH;->D:Z

    return-void
.end method

.method public static a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;
    .locals 4

    .prologue
    .line 1950223
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    .line 1950224
    iput-object v1, v0, LX/CwH;->b:Ljava/lang/String;

    .line 1950225
    move-object v0, v0

    .line 1950226
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->b()Ljava/lang/String;

    move-result-object v1

    .line 1950227
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 1950228
    move-object v0, v0

    .line 1950229
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->c()Ljava/lang/String;

    move-result-object v1

    .line 1950230
    iput-object v1, v0, LX/CwH;->d:Ljava/lang/String;

    .line 1950231
    move-object v0, v0

    .line 1950232
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jA_()Ljava/lang/String;

    move-result-object v1

    .line 1950233
    iput-object v1, v0, LX/CwH;->e:Ljava/lang/String;

    .line 1950234
    move-object v0, v0

    .line 1950235
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->e()Ljava/lang/Boolean;

    move-result-object v1

    .line 1950236
    iput-object v1, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 1950237
    move-object v0, v0

    .line 1950238
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v1

    .line 1950239
    iput-object v1, v0, LX/CwH;->g:LX/CwF;

    .line 1950240
    move-object v0, v0

    .line 1950241
    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1950242
    iput-object v1, v0, LX/CwH;->h:Ljava/lang/String;

    .line 1950243
    move-object v0, v0

    .line 1950244
    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    move-object v1, v1

    .line 1950245
    iput-object v1, v0, LX/CwH;->i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 1950246
    move-object v0, v0

    .line 1950247
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n()Lcom/facebook/search/model/ReactionSearchData;

    move-result-object v1

    .line 1950248
    iput-object v1, v0, LX/CwH;->k:Lcom/facebook/search/model/ReactionSearchData;

    .line 1950249
    move-object v0, v0

    .line 1950250
    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v1, v1

    .line 1950251
    iput-object v1, v0, LX/CwH;->l:LX/CwI;

    .line 1950252
    move-object v0, v0

    .line 1950253
    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->m:Ljava/lang/String;

    move-object v1, v1

    .line 1950254
    iput-object v1, v0, LX/CwH;->m:Ljava/lang/String;

    .line 1950255
    move-object v0, v0

    .line 1950256
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->k()LX/103;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/CwH;->a(Ljava/lang/String;Ljava/lang/String;LX/103;)LX/CwH;

    move-result-object v0

    .line 1950257
    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->s:Ljava/lang/String;

    move-object v1, v1

    .line 1950258
    iput-object v1, v0, LX/CwH;->t:Ljava/lang/String;

    .line 1950259
    move-object v0, v0

    .line 1950260
    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->t:Ljava/lang/String;

    move-object v1, v1

    .line 1950261
    iput-object v1, v0, LX/CwH;->u:Ljava/lang/String;

    .line 1950262
    move-object v0, v0

    .line 1950263
    iget-boolean v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->z:Z

    move v1, v1

    .line 1950264
    iput-boolean v1, v0, LX/CwH;->q:Z

    .line 1950265
    move-object v0, v0

    .line 1950266
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jC_()LX/0Px;

    move-result-object v1

    .line 1950267
    iput-object v1, v0, LX/CwH;->x:LX/0Px;

    .line 1950268
    move-object v0, v0

    .line 1950269
    invoke-virtual {p0}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v1

    .line 1950270
    iput-boolean v1, v0, LX/CwH;->w:Z

    .line 1950271
    move-object v0, v0

    .line 1950272
    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->u:Ljava/lang/String;

    move-object v1, v1

    .line 1950273
    iput-object v1, v0, LX/CwH;->z:Ljava/lang/String;

    .line 1950274
    move-object v0, v0

    .line 1950275
    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->v:LX/0P1;

    move-object v1, v1

    .line 1950276
    iput-object v1, v0, LX/CwH;->A:LX/0P1;

    .line 1950277
    move-object v0, v0

    .line 1950278
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->h()LX/0P1;

    move-result-object v1

    .line 1950279
    iput-object v1, v0, LX/CwH;->y:LX/0P1;

    .line 1950280
    move-object v0, v0

    .line 1950281
    iget v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->x:I

    move v1, v1

    .line 1950282
    iput v1, v0, LX/CwH;->B:I

    .line 1950283
    move-object v0, v0

    .line 1950284
    iget v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->y:I

    move v1, v1

    .line 1950285
    iput v1, v0, LX/CwH;->C:I

    .line 1950286
    move-object v0, v0

    .line 1950287
    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->D:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 1950288
    iput-object v1, v0, LX/CwH;->E:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1950289
    move-object v0, v0

    .line 1950290
    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->E:Ljava/lang/String;

    move-object v1, v1

    .line 1950291
    iput-object v1, v0, LX/CwH;->F:Ljava/lang/String;

    .line 1950292
    move-object v0, v0

    .line 1950293
    iget-boolean v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->F:Z

    move v1, v1

    .line 1950294
    iput-boolean v1, v0, LX/CwH;->G:Z

    .line 1950295
    move-object v0, v0

    .line 1950296
    return-object v0
.end method

.method public static a(Ljava/lang/String;LX/CwI;)LX/CwH;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1950324
    invoke-static {p0, v0, v0, p1}, LX/CwH;->a(Ljava/lang/String;Ljava/lang/String;LX/CwF;LX/CwI;)LX/CwH;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;LX/CwF;LX/CwI;)LX/CwH;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/CwF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1950204
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    .line 1950205
    iput-object p0, v0, LX/CwH;->b:Ljava/lang/String;

    .line 1950206
    move-object v0, v0

    .line 1950207
    iput-object p0, v0, LX/CwH;->d:Ljava/lang/String;

    .line 1950208
    move-object v0, v0

    .line 1950209
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, LX/7BG;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1950210
    :cond_0
    iput-object p1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 1950211
    move-object v0, v0

    .line 1950212
    const-string v1, "content"

    .line 1950213
    iput-object v1, v0, LX/CwH;->e:Ljava/lang/String;

    .line 1950214
    move-object v0, v0

    .line 1950215
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1950216
    iput-object v1, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 1950217
    move-object v0, v0

    .line 1950218
    iput-object p3, v0, LX/CwH;->l:LX/CwI;

    .line 1950219
    move-object v0, v0

    .line 1950220
    if-eqz p2, :cond_1

    .line 1950221
    iput-object p2, v0, LX/CwH;->g:LX/CwF;

    .line 1950222
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final synthetic a()LX/CwB;
    .locals 1

    .prologue
    .line 1950203
    invoke-virtual {p0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/103;)LX/CwH;
    .locals 0

    .prologue
    .line 1950199
    iput-object p1, p0, LX/CwH;->r:Ljava/lang/String;

    .line 1950200
    iput-object p2, p0, LX/CwH;->p:Ljava/lang/String;

    .line 1950201
    iput-object p3, p0, LX/CwH;->s:LX/103;

    .line 1950202
    return-object p0
.end method

.method public final c()Lcom/facebook/search/model/KeywordTypeaheadUnit;
    .locals 2

    .prologue
    .line 1950198
    new-instance v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-direct {v0, p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;-><init>(LX/CwH;)V

    return-object v0
.end method
