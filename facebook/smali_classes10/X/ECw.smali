.class public LX/ECw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/rtc/expression/RtcVideoExpressionLoader$Listener;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1Zp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/EGF;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AMO;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/ECv;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0V8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2090646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2090647
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2090648
    iput-object v0, p0, LX/ECw;->g:LX/0Ot;

    .line 2090649
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2090650
    iput-object v0, p0, LX/ECw;->i:LX/0Ot;

    .line 2090651
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ECw;->c:Ljava/util/List;

    .line 2090652
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/ECw;->h:Ljava/util/HashMap;

    .line 2090653
    return-void
.end method

.method private a(LX/EGF;LX/ECv;)V
    .locals 2

    .prologue
    .line 2090615
    iget-object v0, p1, LX/EGF;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090616
    :goto_0
    return-void

    .line 2090617
    :cond_0
    iget-object v0, p0, LX/ECw;->h:Ljava/util/HashMap;

    iget-object v1, p1, LX/EGF;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a$redex0(LX/ECw;LX/EGF;Z)V
    .locals 3

    .prologue
    .line 2090635
    if-eqz p2, :cond_1

    sget-object v0, LX/ECv;->COMPLETED:LX/ECv;

    :goto_0
    invoke-direct {p0, p1, v0}, LX/ECw;->a(LX/EGF;LX/ECv;)V

    .line 2090636
    iget-object v0, p0, LX/ECw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    .line 2090637
    if-eqz p2, :cond_2

    .line 2090638
    iget-object v2, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    invoke-virtual {v2}, LX/EDx;->bh()V

    .line 2090639
    iget-object v2, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    iget p0, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->i:I

    invoke-virtual {v2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    iget p0, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->i:I

    invoke-virtual {v2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EGF;

    iget-object v2, v2, LX/EGF;->b:Ljava/lang/String;

    iget-object p0, p1, LX/EGF;->b:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2090640
    iget v2, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->i:I

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->e(I)V

    .line 2090641
    :cond_0
    goto :goto_1

    .line 2090642
    :cond_1
    sget-object v0, LX/ECv;->NOT_STARTED:LX/ECv;

    goto :goto_0

    .line 2090643
    :cond_2
    iget-object v2, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    invoke-virtual {v2}, LX/EDx;->bh()V

    .line 2090644
    goto :goto_1

    .line 2090645
    :cond_3
    return-void
.end method

.method public static a$redex0(LX/ECw;Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 2090654
    invoke-virtual {p1}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v8, v0, LX/1vs;->b:I

    .line 2090655
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1

    .line 2090656
    if-nez v8, :cond_1

    .line 2090657
    :cond_0
    return-void

    .line 2090658
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2090659
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2090660
    new-instance v0, LX/EGF;

    const-string v1, ""

    const-string v2, "No Mask"

    const-string v3, ""

    const-string v4, ""

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/EGF;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2090661
    const v0, 0x33c06189

    invoke-static {v7, v8, v6, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2090662
    if-eqz v0, :cond_2

    .line 2090663
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v0

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2090664
    if-eqz v0, :cond_2

    .line 2090665
    const-class v2, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel;

    invoke-virtual {v1, v0, v6, v2}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    :goto_1
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    move v2, v6

    .line 2090666
    :goto_2
    if-ge v2, v4, :cond_2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel;

    .line 2090667
    if-eqz v0, :cond_3

    .line 2090668
    invoke-static {v0}, LX/EGF;->a(Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel;)LX/EGF;

    move-result-object v0

    .line 2090669
    if-eqz v0, :cond_3

    iget-object v5, v0, LX/EGF;->d:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, v0, LX/EGF;->e:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2090670
    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2090671
    iget-boolean v5, p0, LX/ECw;->f:Z

    if-eqz v5, :cond_3

    invoke-virtual {p0}, LX/ECw;->a()Z

    move-result v5

    if-nez v5, :cond_3

    .line 2090672
    invoke-virtual {p0, v0}, LX/ECw;->c(LX/EGF;)V

    .line 2090673
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2090674
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 2090675
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 2090676
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2090677
    move-object v1, v0

    goto :goto_1

    .line 2090678
    :cond_6
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/ECw;->e:LX/0Px;

    .line 2090679
    iget-object v0, p0, LX/ECw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    .line 2090680
    iget-object v2, p0, LX/ECw;->e:LX/0Px;

    .line 2090681
    iput-object v2, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    .line 2090682
    iget-object v3, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->bh()V

    .line 2090683
    goto :goto_3
.end method


# virtual methods
.method public final a(LX/EGF;)LX/ECv;
    .locals 3

    .prologue
    .line 2090626
    iget-object v0, p1, LX/EGF;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090627
    sget-object v0, LX/ECv;->COMPLETED:LX/ECv;

    .line 2090628
    :goto_0
    return-object v0

    .line 2090629
    :cond_0
    iget-object v0, p0, LX/ECw;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMO;

    iget-object v1, p1, LX/EGF;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/AMO;->c(LX/0Px;)Z

    move-result v0

    .line 2090630
    iget-object v1, p0, LX/ECw;->h:Ljava/util/HashMap;

    iget-object v2, p1, LX/EGF;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2090631
    iget-object v0, p0, LX/ECw;->h:Ljava/util/HashMap;

    iget-object v1, p1, LX/EGF;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECv;

    goto :goto_0

    .line 2090632
    :cond_1
    if-eqz v0, :cond_2

    sget-object v0, LX/ECv;->COMPLETED:LX/ECv;

    .line 2090633
    :goto_1
    invoke-direct {p0, p1, v0}, LX/ECw;->a(LX/EGF;LX/ECv;)V

    goto :goto_0

    .line 2090634
    :cond_2
    sget-object v0, LX/ECv;->NOT_STARTED:LX/ECv;

    goto :goto_1
.end method

.method public final a()Z
    .locals 6

    .prologue
    .line 2090622
    const/4 v1, 0x0

    .line 2090623
    iget-object v0, p0, LX/ECw;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0V8;

    sget-object v2, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v0, v2}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 2090624
    iget-object v0, p0, LX/ECw;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0V8;

    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    const-wide/32 v2, 0x1400000

    invoke-virtual {v0, v1, v2, v3}, LX/0V8;->a(LX/0VA;J)Z

    move-result v0

    .line 2090625
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final c(LX/EGF;)V
    .locals 3

    .prologue
    .line 2090618
    invoke-virtual {p0, p1}, LX/ECw;->a(LX/EGF;)LX/ECv;

    move-result-object v0

    sget-object v1, LX/ECv;->NOT_STARTED:LX/ECv;

    if-ne v0, v1, :cond_0

    .line 2090619
    sget-object v0, LX/ECv;->IN_PROGRESS:LX/ECv;

    invoke-direct {p0, p1, v0}, LX/ECw;->a(LX/EGF;LX/ECv;)V

    .line 2090620
    iget-object v0, p0, LX/ECw;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMO;

    iget-object v1, p1, LX/EGF;->a:LX/0Px;

    new-instance v2, LX/ECt;

    invoke-direct {v2, p0, p1}, LX/ECt;-><init>(LX/ECw;LX/EGF;)V

    invoke-virtual {v0, v1, v2}, LX/AMO;->a(LX/0Px;LX/AMN;)V

    .line 2090621
    :cond_0
    return-void
.end method
