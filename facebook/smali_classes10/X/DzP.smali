.class public LX/DzP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/places/create/BellerophonLoggerData;

.field public final b:LX/0Zb;

.field private final c:LX/0So;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2066899
    iput-object p1, p0, LX/DzP;->b:LX/0Zb;

    .line 2066900
    iput-object p2, p0, LX/DzP;->c:LX/0So;

    .line 2066901
    return-void
.end method

.method public static b(LX/0QB;)LX/DzP;
    .locals 3

    .prologue
    .line 2066931
    new-instance v2, LX/DzP;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-direct {v2, v0, v1}, LX/DzP;-><init>(LX/0Zb;LX/0So;)V

    .line 2066932
    return-object v2
.end method

.method public static b(LX/DzP;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 6

    .prologue
    .line 2066902
    iget-object v0, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2066903
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    .line 2066904
    iget-object v2, v1, Lcom/facebook/places/create/BellerophonLoggerData;->a:Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    .line 2066905
    iget-object v1, v2, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->b:Ljava/lang/String;

    move-object v2, v1

    .line 2066906
    move-object v1, v2

    .line 2066907
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2066908
    move-object v0, v0

    .line 2066909
    const-string v1, "bellerophon"

    .line 2066910
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2066911
    move-object v0, v0

    .line 2066912
    const-string v1, "place_picker_session_id"

    iget-object v2, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    .line 2066913
    iget-object v3, v2, Lcom/facebook/places/create/BellerophonLoggerData;->a:Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    .line 2066914
    iget-object v2, v3, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->a:Ljava/lang/String;

    move-object v3, v2

    .line 2066915
    move-object v2, v3

    .line 2066916
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "bellerophon_session_id"

    iget-object v2, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    .line 2066917
    iget-object v3, v2, Lcom/facebook/places/create/BellerophonLoggerData;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2066918
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2066919
    iget-object v1, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    invoke-virtual {v1}, Lcom/facebook/places/create/BellerophonLoggerData;->f()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    invoke-virtual {v1}, Lcom/facebook/places/create/BellerophonLoggerData;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2066920
    iget-object v1, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    .line 2066921
    iget-object v2, v1, Lcom/facebook/places/create/BellerophonLoggerData;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2066922
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2066923
    const-string v1, "result_list"

    iget-object v2, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    invoke-virtual {v2}, Lcom/facebook/places/create/BellerophonLoggerData;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2066924
    const-string v1, "result_id"

    iget-object v2, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    .line 2066925
    iget-object v3, v2, Lcom/facebook/places/create/BellerophonLoggerData;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2066926
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2066927
    :cond_0
    iget-object v1, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    invoke-virtual {v1}, Lcom/facebook/places/create/BellerophonLoggerData;->d()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 2066928
    iget-object v1, p0, LX/DzP;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-object v1, p0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    invoke-virtual {v1}, Lcom/facebook/places/create/BellerophonLoggerData;->d()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 2066929
    const-string v1, "place_picker_milliseconds_since_start"

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2066930
    :cond_1
    return-object v0
.end method
