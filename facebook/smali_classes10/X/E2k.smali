.class public final LX/E2k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Pq;

.field public final synthetic b:Lcom/facebook/reaction/common/ReactionCardNode;

.field public final synthetic c:Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;LX/1Pq;Lcom/facebook/reaction/common/ReactionCardNode;)V
    .locals 0

    .prologue
    .line 2072896
    iput-object p1, p0, LX/E2k;->c:Lcom/facebook/reaction/feed/rows/ReactionCollapsableStoryIconHeaderPartDefinition;

    iput-object p2, p0, LX/E2k;->a:LX/1Pq;

    iput-object p3, p0, LX/E2k;->b:Lcom/facebook/reaction/common/ReactionCardNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, -0x69b2baa3

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 2072890
    iget-object v0, p0, LX/E2k;->a:LX/1Pq;

    check-cast v0, LX/1Pr;

    new-instance v1, LX/E2U;

    iget-object v5, p0, LX/E2k;->b:Lcom/facebook/reaction/common/ReactionCardNode;

    invoke-virtual {v5}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, LX/E2U;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/E2k;->b:Lcom/facebook/reaction/common/ReactionCardNode;

    invoke-interface {v0, v1, v5}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2V;

    .line 2072891
    iget-boolean v1, v0, LX/E2V;->a:Z

    move v1, v1

    .line 2072892
    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, LX/E2V;->a(Z)V

    .line 2072893
    iget-object v0, p0, LX/E2k;->a:LX/1Pq;

    new-array v1, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/E2k;->b:Lcom/facebook/reaction/common/ReactionCardNode;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2072894
    const v0, 0x62a3ee12

    invoke-static {v0, v4}, LX/02F;->a(II)V

    return-void

    :cond_0
    move v1, v3

    .line 2072895
    goto :goto_0
.end method
