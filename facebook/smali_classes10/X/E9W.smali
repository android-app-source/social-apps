.class public final enum LX/E9W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/E9W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/E9W;

.field public static final enum BLANK_FOOTER:LX/E9W;

.field public static final enum DEFAULT_HEADER:LX/E9W;

.field public static final enum LOADING_MORE:LX/E9W;

.field public static final enum NO_HEADER:LX/E9W;

.field public static final enum PLACES_TO_REVIEW:LX/E9W;

.field public static final enum REVIEW_WITH_NO_ATTACHMENT:LX/E9W;

.field public static final enum SEE_MORE:LX/E9W;

.field public static final enum USER_REVIEW:LX/E9W;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2084485
    new-instance v0, LX/E9W;

    const-string v1, "DEFAULT_HEADER"

    invoke-direct {v0, v1, v3}, LX/E9W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E9W;->DEFAULT_HEADER:LX/E9W;

    .line 2084486
    new-instance v0, LX/E9W;

    const-string v1, "NO_HEADER"

    invoke-direct {v0, v1, v4}, LX/E9W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E9W;->NO_HEADER:LX/E9W;

    .line 2084487
    new-instance v0, LX/E9W;

    const-string v1, "REVIEW_WITH_NO_ATTACHMENT"

    invoke-direct {v0, v1, v5}, LX/E9W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E9W;->REVIEW_WITH_NO_ATTACHMENT:LX/E9W;

    .line 2084488
    new-instance v0, LX/E9W;

    const-string v1, "USER_REVIEW"

    invoke-direct {v0, v1, v6}, LX/E9W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E9W;->USER_REVIEW:LX/E9W;

    .line 2084489
    new-instance v0, LX/E9W;

    const-string v1, "PLACES_TO_REVIEW"

    invoke-direct {v0, v1, v7}, LX/E9W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E9W;->PLACES_TO_REVIEW:LX/E9W;

    .line 2084490
    new-instance v0, LX/E9W;

    const-string v1, "BLANK_FOOTER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/E9W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E9W;->BLANK_FOOTER:LX/E9W;

    .line 2084491
    new-instance v0, LX/E9W;

    const-string v1, "SEE_MORE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/E9W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E9W;->SEE_MORE:LX/E9W;

    .line 2084492
    new-instance v0, LX/E9W;

    const-string v1, "LOADING_MORE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/E9W;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E9W;->LOADING_MORE:LX/E9W;

    .line 2084493
    const/16 v0, 0x8

    new-array v0, v0, [LX/E9W;

    sget-object v1, LX/E9W;->DEFAULT_HEADER:LX/E9W;

    aput-object v1, v0, v3

    sget-object v1, LX/E9W;->NO_HEADER:LX/E9W;

    aput-object v1, v0, v4

    sget-object v1, LX/E9W;->REVIEW_WITH_NO_ATTACHMENT:LX/E9W;

    aput-object v1, v0, v5

    sget-object v1, LX/E9W;->USER_REVIEW:LX/E9W;

    aput-object v1, v0, v6

    sget-object v1, LX/E9W;->PLACES_TO_REVIEW:LX/E9W;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/E9W;->BLANK_FOOTER:LX/E9W;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/E9W;->SEE_MORE:LX/E9W;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/E9W;->LOADING_MORE:LX/E9W;

    aput-object v2, v0, v1

    sput-object v0, LX/E9W;->$VALUES:[LX/E9W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2084482
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/E9W;
    .locals 1

    .prologue
    .line 2084484
    const-class v0, LX/E9W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/E9W;

    return-object v0
.end method

.method public static values()[LX/E9W;
    .locals 1

    .prologue
    .line 2084483
    sget-object v0, LX/E9W;->$VALUES:[LX/E9W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/E9W;

    return-object v0
.end method
