.class public final LX/Erl;
.super LX/Erj;
.source ""


# instance fields
.field public final c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic d:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

.field private final e:Ljava/lang/String;

.field public final f:Lcom/facebook/feed/model/ClientFeedUnitEdge;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/model/ClientFeedUnitEdge;)V
    .locals 1

    .prologue
    .line 2173380
    iput-object p1, p0, LX/Erl;->d:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    .line 2173381
    iget-object v0, p3, Lcom/facebook/feed/model/ClientFeedUnitEdge;->C:Ljava/lang/String;

    move-object v0, v0

    .line 2173382
    invoke-direct {p0, p1, v0}, LX/Erj;-><init>(Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;Ljava/lang/String;)V

    .line 2173383
    iput-object p2, p0, LX/Erl;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2173384
    iget-object v0, p0, LX/Erl;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Erl;->e:Ljava/lang/String;

    .line 2173385
    iput-object p3, p0, LX/Erl;->f:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 2173386
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2173387
    iget-object v0, p0, LX/Erl;->d:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/Erj;->a:Ljava/lang/String;

    iget-object v3, p0, LX/Erl;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    .line 2173388
    iget-object v0, p0, LX/Erl;->d:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16u;

    iget-object v1, p0, LX/Erl;->f:Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0, v1}, LX/16u;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 2173389
    new-instance v1, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$InstantArticleFetcher$1;

    invoke-direct {v1, p0}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade$InstantArticleFetcher$1;-><init>(LX/Erl;)V

    .line 2173390
    iget-object v0, p0, LX/Erl;->d:Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    const v2, 0x39d5d772

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2173391
    return-void
.end method
