.class public final enum LX/DiQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DiQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DiQ;

.field public static final enum ADD_ACTIONS:LX/DiQ;

.field public static final enum CLEAR_ACTIONS:LX/DiQ;


# instance fields
.field private mId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2032119
    new-instance v0, LX/DiQ;

    const-string v1, "ADD_ACTIONS"

    invoke-direct {v0, v1, v3, v2}, LX/DiQ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiQ;->ADD_ACTIONS:LX/DiQ;

    .line 2032120
    new-instance v0, LX/DiQ;

    const-string v1, "CLEAR_ACTIONS"

    invoke-direct {v0, v1, v2, v4}, LX/DiQ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DiQ;->CLEAR_ACTIONS:LX/DiQ;

    .line 2032121
    new-array v0, v4, [LX/DiQ;

    sget-object v1, LX/DiQ;->ADD_ACTIONS:LX/DiQ;

    aput-object v1, v0, v3

    sget-object v1, LX/DiQ;->CLEAR_ACTIONS:LX/DiQ;

    aput-object v1, v0, v2

    sput-object v0, LX/DiQ;->$VALUES:[LX/DiQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2032122
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2032123
    iput p3, p0, LX/DiQ;->mId:I

    .line 2032124
    return-void
.end method

.method public static fromId(I)LX/DiQ;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2032125
    invoke-static {}, LX/DiQ;->values()[LX/DiQ;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2032126
    invoke-virtual {v0}, LX/DiQ;->getId()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 2032127
    :goto_1
    return-object v0

    .line 2032128
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2032129
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/DiQ;
    .locals 1

    .prologue
    .line 2032130
    const-class v0, LX/DiQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DiQ;

    return-object v0
.end method

.method public static values()[LX/DiQ;
    .locals 1

    .prologue
    .line 2032131
    sget-object v0, LX/DiQ;->$VALUES:[LX/DiQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DiQ;

    return-object v0
.end method


# virtual methods
.method public final getId()I
    .locals 1

    .prologue
    .line 2032132
    iget v0, p0, LX/DiQ;->mId:I

    return v0
.end method
