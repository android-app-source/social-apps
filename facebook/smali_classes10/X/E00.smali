.class public LX/E00;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/9kE;

.field private b:LX/0tX;


# direct methods
.method public constructor <init>(LX/9kE;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2067708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2067709
    iput-object p1, p0, LX/E00;->a:LX/9kE;

    .line 2067710
    iput-object p2, p0, LX/E00;->b:LX/0tX;

    .line 2067711
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2067712
    iget-object v0, p0, LX/E00;->a:LX/9kE;

    invoke-virtual {v0}, LX/9kE;->c()V

    .line 2067713
    return-void
.end method

.method public final a(LX/E01;LX/0TF;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E01;",
            "LX/0TF",
            "<",
            "LX/E02;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2067714
    invoke-static {}, LX/5m3;->a()LX/5lz;

    move-result-object v0

    .line 2067715
    const-string v1, "query"

    .line 2067716
    new-instance v4, LX/4DI;

    invoke-direct {v4}, LX/4DI;-><init>()V

    .line 2067717
    iget-object v5, p1, LX/E01;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2067718
    invoke-virtual {v4, v5}, LX/4DI;->a(Ljava/lang/String;)LX/4DI;

    .line 2067719
    iget v5, p1, LX/E01;->c:I

    move v5, v5

    .line 2067720
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 2067721
    const-string v6, "category"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2067722
    iget-object v5, p1, LX/E01;->b:Landroid/location/Location;

    move-object v5, v5

    .line 2067723
    if-eqz v5, :cond_2

    .line 2067724
    new-instance v6, LX/3Aj;

    invoke-direct {v6}, LX/3Aj;-><init>()V

    .line 2067725
    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    .line 2067726
    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    .line 2067727
    invoke-virtual {v5}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-eqz v7, :cond_0

    .line 2067728
    invoke-virtual {v5}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 2067729
    :cond_0
    invoke-virtual {v5}, Landroid/location/Location;->hasSpeed()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2067730
    invoke-virtual {v5}, Landroid/location/Location;->getSpeed()F

    move-result v5

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/3Aj;->d(Ljava/lang/Double;)LX/3Aj;

    .line 2067731
    :cond_1
    invoke-virtual {v4, v6}, LX/4DI;->a(LX/3Aj;)LX/4DI;

    .line 2067732
    :cond_2
    iget-object v5, p1, LX/E01;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2067733
    if-eqz v5, :cond_3

    .line 2067734
    iget-object v5, p1, LX/E01;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2067735
    const-string v6, "address"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2067736
    :cond_3
    iget-object v5, p1, LX/E01;->e:Ljava/lang/String;

    move-object v5, v5

    .line 2067737
    if-eqz v5, :cond_4

    .line 2067738
    iget-object v5, p1, LX/E01;->e:Ljava/lang/String;

    move-object v5, v5

    .line 2067739
    const-string v6, "city"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2067740
    :cond_4
    iget-object v5, p1, LX/E01;->f:Ljava/lang/String;

    move-object v5, v5

    .line 2067741
    if-eqz v5, :cond_5

    .line 2067742
    iget-object v5, p1, LX/E01;->f:Ljava/lang/String;

    move-object v5, v5

    .line 2067743
    const-string v6, "website"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2067744
    :cond_5
    iget-object v5, p1, LX/E01;->g:Ljava/lang/String;

    move-object v5, v5

    .line 2067745
    if-eqz v5, :cond_6

    .line 2067746
    iget-object v5, p1, LX/E01;->g:Ljava/lang/String;

    move-object v5, v5

    .line 2067747
    const-string v6, "phone"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2067748
    :cond_6
    move-object v2, v4

    .line 2067749
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    const-string v2, "search_context"

    const-string v3, "bellerophon"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2067750
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2067751
    iget-object v1, p0, LX/E00;->a:LX/9kE;

    iget-object v2, p0, LX/E00;->b:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v2, LX/Dzz;

    invoke-direct {v2, p0, p2}, LX/Dzz;-><init>(LX/E00;LX/0TF;)V

    invoke-virtual {v1, v0, v2}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2067752
    return-void
.end method
