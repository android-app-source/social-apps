.class public LX/Eft;
.super LX/Efq;
.source ""


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/Efs;LX/AKu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;",
            ">;",
            "Lcom/facebook/audience/snacks/view/bucket/SnacksBaseBucketView$ViewListener;",
            "LX/AKu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2155521
    invoke-direct {p0, p1, p3, p4}, LX/Efq;-><init>(Landroid/content/Context;LX/Efs;LX/AKu;)V

    .line 2155522
    iput-object p2, p0, LX/Eft;->a:LX/0Px;

    .line 2155523
    return-void
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 2155524
    iget-object v0, p0, LX/Eft;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final e(I)LX/Efo;
    .locals 4

    .prologue
    .line 2155525
    new-instance v1, LX/Eg1;

    .line 2155526
    iget-object v0, p0, LX/Efq;->a:Landroid/content/Context;

    move-object v0, v0

    .line 2155527
    invoke-direct {v1, v0}, LX/Eg1;-><init>(Landroid/content/Context;)V

    .line 2155528
    iget-object v0, p0, LX/Eft;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155529
    iget-object v2, v1, LX/Efo;->f:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->setLoading(Z)V

    .line 2155530
    iput-object v0, v1, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155531
    new-instance v2, LX/Efj;

    .line 2155532
    iget-object v3, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v3, v3

    .line 2155533
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-direct {v2, v3}, LX/Efj;-><init>(I)V

    iput-object v2, v1, LX/Eg1;->h:LX/Efj;

    .line 2155534
    iget-object v2, v1, LX/Eg1;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08315e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2155535
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p0, 0x0

    .line 2155536
    iget-object p1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object p1, p1

    .line 2155537
    invoke-virtual {p1}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/7hK;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, p0

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2155538
    iget-object v3, v1, LX/Eg1;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2155539
    invoke-virtual {v1}, LX/Efo;->i()V

    .line 2155540
    iget-object v2, v1, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155541
    iget-object v3, v2, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object p1, v3

    .line 2155542
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v3, 0x0

    move p0, v3

    :goto_0
    if-ge p0, v0, :cond_0

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 2155543
    invoke-static {v1, v3}, LX/Eg1;->a(LX/Eg1;Lcom/facebook/audience/snacks/model/SnacksFriendThread;)V

    .line 2155544
    add-int/lit8 v3, p0, 0x1

    move p0, v3

    goto :goto_0

    .line 2155545
    :cond_0
    iget-object v2, v1, LX/Efo;->h:LX/Efj;

    iget-object v3, v1, LX/Efo;->b:LX/Efl;

    invoke-virtual {v2, v3}, LX/Efj;->a(LX/Efl;)V

    .line 2155546
    return-object v1
.end method
