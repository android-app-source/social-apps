.class public final LX/Ex1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V
    .locals 0

    .prologue
    .line 2183963
    iput-object p1, p0, LX/Ex1;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2183962
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2183953
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    const/4 v1, 0x4

    .line 2183954
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2183955
    iget-object v0, p0, LX/Ex1;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->l:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2183956
    :goto_0
    return-void

    .line 2183957
    :cond_0
    iget-object v0, p0, LX/Ex1;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->l:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 2183958
    iget-object v0, p0, LX/Ex1;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->l:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2183959
    :cond_1
    iget-object v1, p0, LX/Ex1;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, p0, LX/Ex1;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 2183960
    iput-wide v2, v1, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->q:J

    .line 2183961
    iget-object v0, p0, LX/Ex1;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    new-instance v1, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment$8$1;

    invoke-direct {v1, p0}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment$8$1;-><init>(LX/Ex1;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->b(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method
