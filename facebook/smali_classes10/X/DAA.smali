.class public LX/DAA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final b:LX/2UU;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/2UU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1970854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970855
    iput-object p1, p0, LX/DAA;->a:Landroid/content/ContentResolver;

    .line 1970856
    iput-object p2, p0, LX/DAA;->b:LX/2UU;

    .line 1970857
    return-void
.end method

.method public static a(LX/DAA;I)Lcom/facebook/contacts/model/PhonebookContact;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 1970858
    :try_start_0
    iget-object v0, p0, LX/DAA;->a:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, LX/6Mz;->a:[Ljava/lang/String;

    const-string v3, "contact_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1970859
    :try_start_1
    iget-object v0, p0, LX/DAA;->b:LX/2UU;

    invoke-virtual {v0, v1}, LX/2UU;->a(Landroid/database/Cursor;)LX/6NA;

    move-result-object v0

    .line 1970860
    invoke-virtual {v0}, LX/6Mz;->c()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-eq v2, v8, :cond_2

    .line 1970861
    if-eqz v1, :cond_0

    .line 1970862
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v6

    :cond_1
    :goto_0
    return-object v0

    .line 1970863
    :cond_2
    :try_start_2
    invoke-virtual {v0}, LX/0Rr;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookContact;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1970864
    if-eqz v1, :cond_1

    .line 1970865
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1970866
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_3

    .line 1970867
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 1970868
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_1
.end method
