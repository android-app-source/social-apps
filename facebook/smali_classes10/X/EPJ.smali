.class public final LX/EPJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/8dH;

.field public final synthetic b:LX/Cxi;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final synthetic d:LX/CzL;

.field public final synthetic e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public final synthetic f:LX/CyI;

.field public final synthetic g:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final synthetic h:Ljava/lang/String;

.field public final synthetic i:Z

.field public final synthetic j:LX/EPK;


# direct methods
.method public constructor <init>(LX/EPK;LX/8dH;LX/Cxi;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/CzL;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/CyI;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2116523
    iput-object p1, p0, LX/EPJ;->j:LX/EPK;

    iput-object p2, p0, LX/EPJ;->a:LX/8dH;

    iput-object p3, p0, LX/EPJ;->b:LX/Cxi;

    iput-object p4, p0, LX/EPJ;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object p5, p0, LX/EPJ;->d:LX/CzL;

    iput-object p6, p0, LX/EPJ;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iput-object p7, p0, LX/EPJ;->f:LX/CyI;

    iput-object p8, p0, LX/EPJ;->g:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object p9, p0, LX/EPJ;->h:Ljava/lang/String;

    iput-boolean p10, p0, LX/EPJ;->i:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 17

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const v3, 0x6c1424c5

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v16

    .line 2116524
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPJ;->a:LX/8dH;

    invoke-interface {v1}, LX/8dH;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    .line 2116525
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPJ;->a:LX/8dH;

    invoke-interface {v1}, LX/8dH;->fn_()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/EPK;->b(LX/0Px;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v5

    .line 2116526
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPJ;->a:LX/8dH;

    invoke-interface {v1}, LX/8dH;->j()LX/8dG;

    move-result-object v10

    .line 2116527
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPJ;->b:LX/Cxi;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    .line 2116528
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2116529
    invoke-static {v2, v5}, LX/8eM;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 2116530
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->j:LX/EPK;

    iget-object v2, v2, LX/EPK;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, LX/CvY;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->b:LX/Cxi;

    check-cast v2, LX/CxP;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/EPJ;->d:LX/CzL;

    invoke-interface {v2, v4}, LX/CxP;->b(LX/CzL;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, LX/EPJ;->d:LX/CzL;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->j:LX/EPK;

    iget-object v2, v2, LX/EPK;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/EPJ;->d:LX/CzL;

    invoke-virtual {v4}, LX/CzL;->l()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/EPJ;->b:LX/Cxi;

    check-cast v6, LX/CxP;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/EPJ;->d:LX/CzL;

    invoke-interface {v6, v7}, LX/CxP;->b(LX/CzL;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/EPJ;->d:LX/CzL;

    invoke-virtual {v7}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v7

    invoke-static {v7}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    if-nez v8, :cond_0

    move-object/from16 v0, p0

    iget-object v8, v0, LX/EPJ;->d:LX/CzL;

    invoke-virtual {v8}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v8

    invoke-static {v8}, LX/8eM;->d(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    :cond_0
    invoke-static/range {v1 .. v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;IILcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-virtual {v9, v1, v11, v12, v2}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2116531
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->a:LX/8dH;

    invoke-interface {v2}, LX/8dH;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->a:LX/8dH;

    invoke-interface {v2}, LX/8dH;->a()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v13

    .line 2116532
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->f:LX/CyI;

    if-nez v2, :cond_4

    .line 2116533
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->j:LX/EPK;

    iget-object v2, v2, LX/EPK;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1nD;

    if-eqz v10, :cond_2

    invoke-interface {v10}, LX/8dG;->a()Ljava/lang/String;

    move-result-object v6

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->a:LX/8dH;

    invoke-interface {v2}, LX/8dH;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v2

    invoke-interface {v2}, LX/8ef;->m()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->b:LX/Cxi;

    check-cast v2, LX/CxV;

    invoke-static {v2}, LX/EPK;->a(LX/CxV;)LX/8ci;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, LX/EPJ;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/EPJ;->g:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v14

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v15, v0, LX/EPJ;->i:Z

    invoke-virtual/range {v4 .. v15}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/0Px;LX/0Px;Z)Landroid/content/Intent;

    move-result-object v2

    .line 2116534
    const-string v3, "search_theme"

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m()LX/7BH;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2116535
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPJ;->j:LX/EPK;

    iget-object v1, v1, LX/EPK;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2116536
    :goto_3
    const v1, 0x2fd90500

    move/from16 v0, v16

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2116537
    :cond_1
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v13

    goto :goto_0

    .line 2116538
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->h:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v14

    goto :goto_2

    .line 2116539
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EPJ;->b:LX/Cxi;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EPJ;->f:LX/CyI;

    invoke-interface {v1, v2}, LX/Cxi;->a(LX/CyI;)V

    goto :goto_3
.end method
