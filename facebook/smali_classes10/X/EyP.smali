.class public interface abstract LX/EyP;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract getSubtitleText()Ljava/lang/CharSequence;
.end method

.method public abstract getTag(I)Ljava/lang/Object;
.end method

.method public abstract getTitleText()Ljava/lang/CharSequence;
.end method

.method public abstract setBackgroundResource(I)V
.end method

.method public abstract setContentDescription(Ljava/lang/CharSequence;)V
.end method

.method public abstract setOnClickListener(Landroid/view/View$OnClickListener;)V
.end method

.method public abstract setSubtitleText(I)V
.end method

.method public abstract setSubtitleText(Ljava/lang/CharSequence;)V
.end method

.method public abstract setTag(ILjava/lang/Object;)V
.end method

.method public abstract setThumbnailUri(Ljava/lang/String;)V
.end method

.method public abstract setTitleText(Ljava/lang/CharSequence;)V
.end method
