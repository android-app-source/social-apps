.class public final LX/DJQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DJc;


# direct methods
.method public constructor <init>(LX/DJc;)V
    .locals 0

    .prologue
    .line 1985247
    iput-object p1, p0, LX/DJQ;->a:LX/DJc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x505b26f7

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1985248
    iget-object v1, p0, LX/DJQ;->a:LX/DJc;

    .line 1985249
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v3

    .line 1985250
    sget-object v4, LX/9jG;->HIDE_GEOHUBS:LX/9jG;

    .line 1985251
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v5

    invoke-virtual {v5}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v5

    .line 1985252
    if-nez v5, :cond_3

    .line 1985253
    :cond_0
    :goto_0
    move-object v4, v4

    .line 1985254
    iput-object v4, v3, LX/9jF;->q:LX/9jG;

    .line 1985255
    move-object v3, v3

    .line 1985256
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v4

    invoke-virtual {v4}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v4

    .line 1985257
    iput-object v4, v3, LX/9jF;->g:Ljava/lang/String;

    .line 1985258
    move-object v3, v3

    .line 1985259
    iget-object v4, v1, LX/DJc;->m:LX/DIv;

    .line 1985260
    iget-object v5, v4, LX/DIv;->k:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    move-object v4, v5

    .line 1985261
    if-eqz v4, :cond_2

    .line 1985262
    new-instance v5, Landroid/location/Location;

    const-string v6, ""

    invoke-direct {v5, v6}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1985263
    iget-wide v7, v4, Lcom/facebook/ipc/composer/model/ProductItemPlace;->latitude:D

    invoke-virtual {v5, v7, v8}, Landroid/location/Location;->setLatitude(D)V

    .line 1985264
    iget-wide v7, v4, Lcom/facebook/ipc/composer/model/ProductItemPlace;->longitude:D

    invoke-virtual {v5, v7, v8}, Landroid/location/Location;->setLongitude(D)V

    .line 1985265
    invoke-static {v5}, Lcom/facebook/ipc/composer/model/ComposerLocation;->a(Landroid/location/Location;)Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v5

    .line 1985266
    iput-object v5, v3, LX/9jF;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1985267
    iget-object v4, v4, Lcom/facebook/ipc/composer/model/ProductItemPlace;->name:Ljava/lang/String;

    .line 1985268
    iput-object v4, v3, LX/9jF;->m:Ljava/lang/String;

    .line 1985269
    :cond_1
    :goto_1
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v4

    invoke-virtual {v4}, LX/B5j;->f()LX/Ar6;

    move-result-object v4

    .line 1985270
    iget-object v5, v1, LX/AQ9;->b:Landroid/content/Context;

    move-object v5, v5

    .line 1985271
    invoke-virtual {v3}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v3

    invoke-static {v5, v3}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v3

    const/16 v5, 0x52

    invoke-interface {v4, v3, v5}, LX/Ar6;->a(Landroid/content/Intent;I)V

    .line 1985272
    const v1, -0x4ffbc257

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1985273
    :cond_2
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v4

    invoke-virtual {v4}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j7;->getViewerCoordinates()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v4

    .line 1985274
    iput-object v4, v3, LX/9jF;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1985275
    iget-object v4, v1, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v4}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getStructuredLocationText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1985276
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1985277
    iput-object v4, v3, LX/9jF;->B:Ljava/lang/String;

    .line 1985278
    goto :goto_1

    .line 1985279
    :cond_3
    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getProductItemLocationPickerSettings()Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    move-result-object v5

    .line 1985280
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->getUseNeighborhoodDataSource()Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v4, LX/9jG;->FORSALE_POST:LX/9jG;

    goto/16 :goto_0
.end method
