.class public final LX/Ebl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2144465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/EYQ;)LX/EYa;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2144466
    sput-object p1, LX/Eck;->E:LX/EYQ;

    .line 2144467
    sget-object v0, LX/Eck;->E:LX/EYQ;

    move-object v0, v0

    .line 2144468
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144469
    sput-object v0, LX/Eck;->a:LX/EYF;

    .line 2144470
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->a:LX/EYF;

    const/16 v2, 0xd

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "SessionVersion"

    aput-object v3, v2, v5

    const-string v3, "LocalIdentityPublic"

    aput-object v3, v2, v6

    const-string v3, "RemoteIdentityPublic"

    aput-object v3, v2, v7

    const-string v3, "RootKey"

    aput-object v3, v2, v8

    const-string v3, "PreviousCounter"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "SenderChain"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "ReceiverChains"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "PendingKeyExchange"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "PendingPreKey"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "RemoteRegistrationId"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "LocalRegistrationId"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "NeedsRefresh"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "AliceBaseKey"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144471
    sput-object v0, LX/Eck;->b:LX/EYn;

    .line 2144472
    sget-object v0, LX/Eck;->a:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144473
    sput-object v0, LX/Eck;->c:LX/EYF;

    .line 2144474
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->c:LX/EYF;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "SenderRatchetKey"

    aput-object v3, v2, v5

    const-string v3, "SenderRatchetKeyPrivate"

    aput-object v3, v2, v6

    const-string v3, "ChainKey"

    aput-object v3, v2, v7

    const-string v3, "MessageKeys"

    aput-object v3, v2, v8

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144475
    sput-object v0, LX/Eck;->d:LX/EYn;

    .line 2144476
    sget-object v0, LX/Eck;->c:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144477
    sput-object v0, LX/Eck;->e:LX/EYF;

    .line 2144478
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->e:LX/EYF;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "Index"

    aput-object v3, v2, v5

    const-string v3, "Key"

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144479
    sput-object v0, LX/Eck;->f:LX/EYn;

    .line 2144480
    sget-object v0, LX/Eck;->c:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144481
    sput-object v0, LX/Eck;->g:LX/EYF;

    .line 2144482
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->g:LX/EYF;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "Index"

    aput-object v3, v2, v5

    const-string v3, "CipherKey"

    aput-object v3, v2, v6

    const-string v3, "MacKey"

    aput-object v3, v2, v7

    const-string v3, "Iv"

    aput-object v3, v2, v8

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144483
    sput-object v0, LX/Eck;->h:LX/EYn;

    .line 2144484
    sget-object v0, LX/Eck;->a:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144485
    sput-object v0, LX/Eck;->i:LX/EYF;

    .line 2144486
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->i:LX/EYF;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Sequence"

    aput-object v3, v2, v5

    const-string v3, "LocalBaseKey"

    aput-object v3, v2, v6

    const-string v3, "LocalBaseKeyPrivate"

    aput-object v3, v2, v7

    const-string v3, "LocalRatchetKey"

    aput-object v3, v2, v8

    const-string v3, "LocalRatchetKeyPrivate"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "LocalIdentityKey"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "LocalIdentityKeyPrivate"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144487
    sput-object v0, LX/Eck;->j:LX/EYn;

    .line 2144488
    sget-object v0, LX/Eck;->a:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144489
    sput-object v0, LX/Eck;->k:LX/EYF;

    .line 2144490
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->k:LX/EYF;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "PreKeyId"

    aput-object v3, v2, v5

    const-string v3, "SignedPreKeyId"

    aput-object v3, v2, v6

    const-string v3, "BaseKey"

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144491
    sput-object v0, LX/Eck;->l:LX/EYn;

    .line 2144492
    sget-object v0, LX/Eck;->E:LX/EYQ;

    move-object v0, v0

    .line 2144493
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144494
    sput-object v0, LX/Eck;->m:LX/EYF;

    .line 2144495
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->m:LX/EYF;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "CurrentSession"

    aput-object v3, v2, v5

    const-string v3, "PreviousSessions"

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144496
    sput-object v0, LX/Eck;->n:LX/EYn;

    .line 2144497
    sget-object v0, LX/Eck;->E:LX/EYQ;

    move-object v0, v0

    .line 2144498
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144499
    sput-object v0, LX/Eck;->o:LX/EYF;

    .line 2144500
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->o:LX/EYF;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "Id"

    aput-object v3, v2, v5

    const-string v3, "PublicKey"

    aput-object v3, v2, v6

    const-string v3, "PrivateKey"

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144501
    sput-object v0, LX/Eck;->p:LX/EYn;

    .line 2144502
    sget-object v0, LX/Eck;->E:LX/EYQ;

    move-object v0, v0

    .line 2144503
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144504
    sput-object v0, LX/Eck;->q:LX/EYF;

    .line 2144505
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->q:LX/EYF;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Id"

    aput-object v3, v2, v5

    const-string v3, "PublicKey"

    aput-object v3, v2, v6

    const-string v3, "PrivateKey"

    aput-object v3, v2, v7

    const-string v3, "Signature"

    aput-object v3, v2, v8

    const-string v3, "Timestamp"

    aput-object v3, v2, v9

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144506
    sput-object v0, LX/Eck;->r:LX/EYn;

    .line 2144507
    sget-object v0, LX/Eck;->E:LX/EYQ;

    move-object v0, v0

    .line 2144508
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144509
    sput-object v0, LX/Eck;->s:LX/EYF;

    .line 2144510
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->s:LX/EYF;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "PublicKey"

    aput-object v3, v2, v5

    const-string v3, "PrivateKey"

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144511
    sput-object v0, LX/Eck;->t:LX/EYn;

    .line 2144512
    sget-object v0, LX/Eck;->E:LX/EYQ;

    move-object v0, v0

    .line 2144513
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144514
    sput-object v0, LX/Eck;->u:LX/EYF;

    .line 2144515
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->u:LX/EYF;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "SenderKeyId"

    aput-object v3, v2, v5

    const-string v3, "SenderChainKey"

    aput-object v3, v2, v6

    const-string v3, "SenderSigningKey"

    aput-object v3, v2, v7

    const-string v3, "SenderMessageKeys"

    aput-object v3, v2, v8

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144516
    sput-object v0, LX/Eck;->v:LX/EYn;

    .line 2144517
    sget-object v0, LX/Eck;->u:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144518
    sput-object v0, LX/Eck;->w:LX/EYF;

    .line 2144519
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->w:LX/EYF;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "Iteration"

    aput-object v3, v2, v5

    const-string v3, "Seed"

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144520
    sput-object v0, LX/Eck;->x:LX/EYn;

    .line 2144521
    sget-object v0, LX/Eck;->u:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144522
    sput-object v0, LX/Eck;->y:LX/EYF;

    .line 2144523
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->y:LX/EYF;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "Iteration"

    aput-object v3, v2, v5

    const-string v3, "Seed"

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144524
    sput-object v0, LX/Eck;->z:LX/EYn;

    .line 2144525
    sget-object v0, LX/Eck;->u:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144526
    sput-object v0, LX/Eck;->A:LX/EYF;

    .line 2144527
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->A:LX/EYF;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "Public"

    aput-object v3, v2, v5

    const-string v3, "Private"

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144528
    sput-object v0, LX/Eck;->B:LX/EYn;

    .line 2144529
    sget-object v0, LX/Eck;->E:LX/EYQ;

    move-object v0, v0

    .line 2144530
    invoke-virtual {v0}, LX/EYQ;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYF;

    .line 2144531
    sput-object v0, LX/Eck;->C:LX/EYF;

    .line 2144532
    new-instance v0, LX/EYn;

    sget-object v1, LX/Eck;->C:LX/EYF;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "SenderKeyStates"

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v2}, LX/EYn;-><init>(LX/EYF;[Ljava/lang/String;)V

    .line 2144533
    sput-object v0, LX/Eck;->D:LX/EYn;

    .line 2144534
    const/4 v0, 0x0

    return-object v0
.end method
