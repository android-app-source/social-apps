.class public final enum LX/CoP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CoP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CoP;

.field public static final enum PERCENTAGE:LX/CoP;

.field public static final enum PIXEL:LX/CoP;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1935607
    new-instance v0, LX/CoP;

    const-string v1, "PIXEL"

    invoke-direct {v0, v1, v2}, LX/CoP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CoP;->PIXEL:LX/CoP;

    .line 1935608
    new-instance v0, LX/CoP;

    const-string v1, "PERCENTAGE"

    invoke-direct {v0, v1, v3}, LX/CoP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CoP;->PERCENTAGE:LX/CoP;

    .line 1935609
    const/4 v0, 0x2

    new-array v0, v0, [LX/CoP;

    sget-object v1, LX/CoP;->PIXEL:LX/CoP;

    aput-object v1, v0, v2

    sget-object v1, LX/CoP;->PERCENTAGE:LX/CoP;

    aput-object v1, v0, v3

    sput-object v0, LX/CoP;->$VALUES:[LX/CoP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1935604
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CoP;
    .locals 1

    .prologue
    .line 1935606
    const-class v0, LX/CoP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CoP;

    return-object v0
.end method

.method public static values()[LX/CoP;
    .locals 1

    .prologue
    .line 1935605
    sget-object v0, LX/CoP;->$VALUES:[LX/CoP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CoP;

    return-object v0
.end method
