.class public LX/D2M;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/D3w;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1958611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958612
    return-void
.end method

.method public static b(LX/0QB;)LX/D2M;
    .locals 4

    .prologue
    .line 1958607
    new-instance v0, LX/D2M;

    invoke-direct {v0}, LX/D2M;-><init>()V

    .line 1958608
    const/16 v1, 0x37b1

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x1430

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0xafd

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    .line 1958609
    iput-object v1, v0, LX/D2M;->a:LX/0Or;

    iput-object v2, v0, LX/D2M;->b:LX/0Or;

    iput-object v3, v0, LX/D2M;->c:LX/0Or;

    .line 1958610
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1958598
    if-nez p1, :cond_0

    .line 1958599
    :goto_0
    return-void

    .line 1958600
    :cond_0
    new-instance v0, LX/5vx;

    invoke-direct {v0}, LX/5vx;-><init>()V

    move-object v0, v0

    .line 1958601
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "media_type"

    sget-object v3, LX/0wF;->IMAGEWEBP:LX/0wF;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1958602
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 1958603
    iget-object v0, p0, LX/D2M;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    .line 1958604
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 1958605
    iget-object v0, p0, LX/D2M;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    .line 1958606
    new-instance v2, LX/D2L;

    invoke-direct {v2, p0, p1}, LX/D2L;-><init>(LX/D2M;Landroid/content/Context;)V

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
