.class public LX/Erw;
.super LX/16T;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Erw;


# instance fields
.field public a:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2173832
    invoke-direct {p0}, LX/16T;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Erw;
    .locals 3

    .prologue
    .line 2173833
    sget-object v0, LX/Erw;->b:LX/Erw;

    if-nez v0, :cond_1

    .line 2173834
    const-class v1, LX/Erw;

    monitor-enter v1

    .line 2173835
    :try_start_0
    sget-object v0, LX/Erw;->b:LX/Erw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2173836
    if-eqz v2, :cond_0

    .line 2173837
    :try_start_1
    new-instance v0, LX/Erw;

    invoke-direct {v0}, LX/Erw;-><init>()V

    .line 2173838
    move-object v0, v0

    .line 2173839
    sput-object v0, LX/Erw;->b:LX/Erw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173840
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2173841
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2173842
    :cond_1
    sget-object v0, LX/Erw;->b:LX/Erw;

    return-object v0

    .line 2173843
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2173844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 2173845
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2173846
    const-string v0, "4520"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2173847
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->OFFLINE_FEED_BOOKMARK_TRIGGER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
