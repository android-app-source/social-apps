.class public LX/ETH;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:J


# instance fields
.field public final c:Landroid/os/Handler;

.field public d:Z

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/3AY;",
            ">;"
        }
    .end annotation
.end field

.field public f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2124611
    const-class v0, LX/ETH;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ETH;->a:Ljava/lang/String;

    .line 2124612
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, LX/ETH;->b:J

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 2
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2124613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2124614
    sget-wide v0, LX/ETH;->b:J

    iput-wide v0, p0, LX/ETH;->f:J

    .line 2124615
    iput-object p1, p0, LX/ETH;->c:Landroid/os/Handler;

    .line 2124616
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/ETH;->e:Ljava/util/Set;

    .line 2124617
    return-void
.end method

.method public static e(LX/ETH;)V
    .locals 2

    .prologue
    .line 2124618
    iget-object v0, p0, LX/ETH;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2124619
    return-void
.end method


# virtual methods
.method public final a(LX/3AY;)V
    .locals 1

    .prologue
    .line 2124620
    iget-object v0, p0, LX/ETH;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2124621
    :goto_0
    return-void

    .line 2124622
    :cond_0
    iget-object v0, p0, LX/ETH;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final declared-synchronized a(Z)V
    .locals 9

    .prologue
    .line 2124623
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/ETH;->e(LX/ETH;)V

    .line 2124624
    iput-boolean p1, p0, LX/ETH;->d:Z

    .line 2124625
    if-nez p1, :cond_0

    iget-wide v0, p0, LX/ETH;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2124626
    iget-wide v0, p0, LX/ETH;->f:J

    .line 2124627
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    .line 2124628
    iget-object v4, p0, LX/ETH;->c:Landroid/os/Handler;

    .line 2124629
    new-instance v5, Lcom/facebook/video/videohome/data/VideoHomeDataStalenessManager$1;

    invoke-direct {v5, p0}, Lcom/facebook/video/videohome/data/VideoHomeDataStalenessManager$1;-><init>(LX/ETH;)V

    move-object v5, v5

    .line 2124630
    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    const v8, -0x213ffac4

    invoke-static {v4, v5, v6, v7, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2124631
    :cond_0
    monitor-exit p0

    return-void

    .line 2124632
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2124633
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/ETH;->a(Z)V

    .line 2124634
    return-void
.end method
