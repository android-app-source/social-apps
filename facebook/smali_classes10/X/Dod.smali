.class public abstract LX/Dod;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final c:LX/0U1;

.field private final d:LX/0U1;


# direct methods
.method public constructor <init>(LX/0Or;Ljava/lang/String;LX/0U1;LX/0U1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2gJ;",
            ">;",
            "Ljava/lang/String;",
            "LX/0U1;",
            "LX/0U1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2042205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042206
    iput-object p1, p0, LX/Dod;->a:LX/0Or;

    .line 2042207
    iput-object p2, p0, LX/Dod;->b:Ljava/lang/String;

    .line 2042208
    iput-object p3, p0, LX/Dod;->c:LX/0U1;

    .line 2042209
    iput-object p4, p0, LX/Dod;->d:LX/0U1;

    .line 2042210
    return-void
.end method


# virtual methods
.method public final a(LX/2PC;I)I
    .locals 1

    .prologue
    .line 2042211
    invoke-virtual {p0, p1}, LX/Dod;->a(LX/2PC;)Ljava/lang/String;

    move-result-object v0

    .line 2042212
    if-nez v0, :cond_0

    .line 2042213
    :goto_0
    return p2

    .line 2042214
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 2042215
    :catch_0
    goto :goto_0
.end method

.method public final a(LX/2PC;J)J
    .locals 2

    .prologue
    .line 2042232
    invoke-virtual {p0, p1}, LX/Dod;->a(LX/2PC;)Ljava/lang/String;

    move-result-object v0

    .line 2042233
    if-nez v0, :cond_0

    .line 2042234
    :goto_0
    return-wide p2

    .line 2042235
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    goto :goto_0

    .line 2042236
    :catch_0
    goto :goto_0
.end method

.method public a(LX/2PC;)Ljava/lang/String;
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 2042216
    iget-object v0, p0, LX/Dod;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 2042217
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2042218
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v3, p0, LX/Dod;->d:LX/0U1;

    .line 2042219
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2042220
    aput-object v3, v2, v1

    .line 2042221
    iget-object v1, p0, LX/Dod;->c:LX/0U1;

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 2042222
    iget-object v1, p0, LX/Dod;->b:Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v1

    .line 2042223
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2042224
    iget-object v0, p0, LX/Dod;->d:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2042225
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2042226
    if-eqz v8, :cond_0

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 2042227
    :cond_1
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2042228
    if-eqz v8, :cond_2

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_2
    move-object v0, v9

    goto :goto_0

    .line 2042229
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2042230
    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2042231
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_1
    if-eqz v8, :cond_3

    if-eqz v9, :cond_4

    :try_start_6
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    :cond_3
    :goto_2
    throw v0

    :catch_1
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_1
.end method

.method public a(LX/2PC;Ljava/lang/String;)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2042177
    iget-object v0, p0, LX/Dod;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;

    .line 2042178
    :try_start_0
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2042179
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2042180
    iget-object v4, p0, LX/Dod;->c:LX/0U1;

    .line 2042181
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2042182
    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042183
    iget-object v4, p0, LX/Dod;->d:LX/0U1;

    .line 2042184
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2042185
    invoke-virtual {v3, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042186
    iget-object v4, p0, LX/Dod;->b:Ljava/lang/String;

    const/4 v5, 0x0

    const v6, 0x477a749a

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v1, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, 0x90b8d1

    invoke-static {v1}, LX/03h;->a(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2042187
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2gJ;->close()V

    .line 2042188
    :cond_0
    return-void

    .line 2042189
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042190
    :catchall_0
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v1

    :catch_1
    move-exception v0

    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_1

    :catchall_1
    move-exception v1

    goto :goto_0
.end method

.method public final a(LX/2PC;[B)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2042191
    iget-object v0, p0, LX/Dod;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;

    .line 2042192
    :try_start_0
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2042193
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2042194
    iget-object v4, p0, LX/Dod;->c:LX/0U1;

    .line 2042195
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2042196
    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042197
    iget-object v4, p0, LX/Dod;->d:LX/0U1;

    .line 2042198
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2042199
    invoke-virtual {v3, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2042200
    iget-object v4, p0, LX/Dod;->b:Ljava/lang/String;

    const/4 v5, 0x0

    const v6, 0x597f87af

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v1, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, 0x47c0efd1

    invoke-static {v1}, LX/03h;->a(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2042201
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2gJ;->close()V

    .line 2042202
    :cond_0
    return-void

    .line 2042203
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042204
    :catchall_0
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v1

    :catch_1
    move-exception v0

    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_1

    :catchall_1
    move-exception v1

    goto :goto_0
.end method

.method public final a(LX/2PC;Z)Z
    .locals 4

    .prologue
    .line 2042172
    invoke-virtual {p0, p1}, LX/Dod;->a(LX/2PC;)Ljava/lang/String;

    move-result-object v0

    .line 2042173
    if-nez v0, :cond_0

    .line 2042174
    :goto_0
    return p2

    .line 2042175
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    goto :goto_0

    .line 2042176
    :catch_0
    goto :goto_0
.end method

.method public final b(LX/2PC;I)V
    .locals 1

    .prologue
    .line 2042170
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/Dod;->a(LX/2PC;Ljava/lang/String;)V

    .line 2042171
    return-void
.end method

.method public final b(LX/2PC;J)V
    .locals 2

    .prologue
    .line 2042168
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/Dod;->a(LX/2PC;Ljava/lang/String;)V

    .line 2042169
    return-void
.end method

.method public final b(LX/2PC;Z)V
    .locals 1

    .prologue
    .line 2042165
    if-eqz p2, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, p1, v0}, LX/Dod;->a(LX/2PC;Ljava/lang/String;)V

    .line 2042166
    return-void

    .line 2042167
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public final b(LX/2PC;)[B
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 2042149
    iget-object v0, p0, LX/Dod;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/2gJ;

    .line 2042150
    :try_start_0
    invoke-virtual {v8}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2042151
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v3, p0, LX/Dod;->d:LX/0U1;

    .line 2042152
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2042153
    aput-object v3, v2, v1

    .line 2042154
    iget-object v1, p0, LX/Dod;->c:LX/0U1;

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0U1;->a(Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 2042155
    iget-object v1, p0, LX/Dod;->b:Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v1

    .line 2042156
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2042157
    iget-object v0, p0, LX/Dod;->d:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->f(Landroid/database/Cursor;)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2042158
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2042159
    if-eqz v8, :cond_0

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 2042160
    :cond_1
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2042161
    if-eqz v8, :cond_2

    invoke-virtual {v8}, LX/2gJ;->close()V

    :cond_2
    move-object v0, v9

    goto :goto_0

    .line 2042162
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2042163
    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2042164
    :catchall_1
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    :goto_1
    if-eqz v8, :cond_3

    if-eqz v9, :cond_4

    :try_start_6
    invoke-virtual {v8}, LX/2gJ;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    :cond_3
    :goto_2
    throw v0

    :catch_1
    move-exception v1

    invoke-static {v9, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v8}, LX/2gJ;->close()V

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_1
.end method

.method public final c(LX/2PC;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 2042140
    iget-object v0, p0, LX/Dod;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2gJ;

    const/4 v2, 0x0

    .line 2042141
    :try_start_0
    invoke-virtual {v0}, LX/2gJ;->m()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2042142
    iget-object v3, p0, LX/Dod;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/Dod;->c:LX/0U1;

    .line 2042143
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 2042144
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = ?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {p1}, LX/0To;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2042145
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/2gJ;->close()V

    .line 2042146
    :cond_0
    return-void

    .line 2042147
    :catch_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042148
    :catchall_0
    move-exception v2

    move-object v8, v2

    move-object v2, v1

    move-object v1, v8

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v0}, LX/2gJ;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_1
    throw v1

    :catch_1
    move-exception v0

    invoke-static {v2, v0}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, LX/2gJ;->close()V

    goto :goto_1

    :catchall_1
    move-exception v1

    goto :goto_0
.end method
