.class public LX/ERK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/ERK;


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2120767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120768
    iput-object p1, p0, LX/ERK;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2120769
    return-void
.end method

.method public static a(LX/0QB;)LX/ERK;
    .locals 4

    .prologue
    .line 2120754
    sget-object v0, LX/ERK;->b:LX/ERK;

    if-nez v0, :cond_1

    .line 2120755
    const-class v1, LX/ERK;

    monitor-enter v1

    .line 2120756
    :try_start_0
    sget-object v0, LX/ERK;->b:LX/ERK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2120757
    if-eqz v2, :cond_0

    .line 2120758
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2120759
    new-instance p0, LX/ERK;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/ERK;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2120760
    move-object v0, p0

    .line 2120761
    sput-object v0, LX/ERK;->b:LX/ERK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2120762
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2120763
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2120764
    :cond_1
    sget-object v0, LX/ERK;->b:LX/ERK;

    return-object v0

    .line 2120765
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2120766
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 2120750
    iget-object v0, p0, LX/ERK;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 2120751
    sget-object v1, LX/2TR;->d:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 2120752
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2120753
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 2120749
    iget-object v0, p0, LX/ERK;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2TR;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
