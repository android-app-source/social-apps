.class public final LX/E5W;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5X;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:I

.field public c:Ljava/lang/CharSequence;

.field public d:I

.field public e:I

.field public f:Landroid/view/View$OnClickListener;

.field public final synthetic g:LX/E5X;


# direct methods
.method public constructor <init>(LX/E5X;)V
    .locals 1

    .prologue
    .line 2078506
    iput-object p1, p0, LX/E5W;->g:LX/E5X;

    .line 2078507
    move-object v0, p1

    .line 2078508
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2078509
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2078510
    const-string v0, "ReactionUndoableMessageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2078511
    if-ne p0, p1, :cond_1

    .line 2078512
    :cond_0
    :goto_0
    return v0

    .line 2078513
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2078514
    goto :goto_0

    .line 2078515
    :cond_3
    check-cast p1, LX/E5W;

    .line 2078516
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2078517
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2078518
    if-eq v2, v3, :cond_0

    .line 2078519
    iget-object v2, p0, LX/E5W;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E5W;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/E5W;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2078520
    goto :goto_0

    .line 2078521
    :cond_5
    iget-object v2, p1, LX/E5W;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 2078522
    :cond_6
    iget v2, p0, LX/E5W;->b:I

    iget v3, p1, LX/E5W;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2078523
    goto :goto_0

    .line 2078524
    :cond_7
    iget-object v2, p0, LX/E5W;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/E5W;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/E5W;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 2078525
    goto :goto_0

    .line 2078526
    :cond_9
    iget-object v2, p1, LX/E5W;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_8

    .line 2078527
    :cond_a
    iget v2, p0, LX/E5W;->d:I

    iget v3, p1, LX/E5W;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 2078528
    goto :goto_0

    .line 2078529
    :cond_b
    iget v2, p0, LX/E5W;->e:I

    iget v3, p1, LX/E5W;->e:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 2078530
    goto :goto_0

    .line 2078531
    :cond_c
    iget-object v2, p0, LX/E5W;->f:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/E5W;->f:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/E5W;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2078532
    goto :goto_0

    .line 2078533
    :cond_d
    iget-object v2, p1, LX/E5W;->f:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
