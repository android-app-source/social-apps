.class public LX/D39;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/D39;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/2n3;

.field public final d:LX/0Zb;

.field public e:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2n3;LX/0Zb;LX/0Uh;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/ui/browser/gating/IsInAppBrowserEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2n3;",
            "LX/0Zb;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1959734
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1959735
    iput-object p1, p0, LX/D39;->a:LX/0Or;

    .line 1959736
    iput-object p2, p0, LX/D39;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1959737
    iput-object p3, p0, LX/D39;->c:LX/2n3;

    .line 1959738
    iput-object p4, p0, LX/D39;->d:LX/0Zb;

    .line 1959739
    iput-object p5, p0, LX/D39;->e:LX/0Uh;

    .line 1959740
    sget-object v0, LX/0ax;->fc:Ljava/lang/String;

    const-string v1, "temporary_url_extra"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/browser/lite/BrowserLiteActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 1959741
    return-void
.end method

.method public static a(LX/0QB;)LX/D39;
    .locals 9

    .prologue
    .line 1959742
    sget-object v0, LX/D39;->f:LX/D39;

    if-nez v0, :cond_1

    .line 1959743
    const-class v1, LX/D39;

    monitor-enter v1

    .line 1959744
    :try_start_0
    sget-object v0, LX/D39;->f:LX/D39;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1959745
    if-eqz v2, :cond_0

    .line 1959746
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1959747
    new-instance v3, LX/D39;

    const/16 v4, 0x36f

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2n3;->b(LX/0QB;)LX/2n3;

    move-result-object v6

    check-cast v6, LX/2n3;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v3 .. v8}, LX/D39;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2n3;LX/0Zb;LX/0Uh;)V

    .line 1959748
    move-object v0, v3

    .line 1959749
    sput-object v0, LX/D39;->f:LX/D39;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1959750
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1959751
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1959752
    :cond_1
    sget-object v0, LX/D39;->f:LX/D39;

    return-object v0

    .line 1959753
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1959754
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1959755
    invoke-super {p0, p1, p2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1959756
    if-eqz v0, :cond_0

    const-string v1, "temporary_url_extra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1959757
    const-string v1, "temporary_url_extra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1959758
    const/4 v3, 0x0

    .line 1959759
    iget-object v2, p0, LX/D39;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    invoke-virtual {v2, v3}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/D39;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p2, LX/1C0;->a:LX/0Tn;

    invoke-interface {v2, p2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/D39;->e:LX/0Uh;

    const/16 p2, 0x38f

    invoke-virtual {v2, p2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1959760
    if-eqz v2, :cond_1

    invoke-static {v1}, LX/047;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1959761
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1959762
    const-string v1, "temporary_url_extra"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1959763
    const-string v1, "iab_click_source"

    const-string v2, "fblink"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1959764
    iget-object v1, p0, LX/D39;->c:LX/2n3;

    invoke-virtual {v1, v0, p1}, LX/2n3;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1959765
    :goto_1
    const/4 p2, 0x0

    .line 1959766
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "iab_disabled_opening_webview_uri"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "webview"

    .line 1959767
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1959768
    move-object v2, v1

    .line 1959769
    const-string v3, "iab_disabled"

    iget-object v1, p0, LX/D39;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    invoke-virtual {v1, p2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "iab_user_disabled"

    iget-object v3, p0, LX/D39;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p1, LX/1C0;->a:LX/0Tn;

    invoke-interface {v3, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1959770
    iget-object v2, p0, LX/D39;->d:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1959771
    :cond_0
    return-object v0

    .line 1959772
    :cond_1
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    sget-object v2, LX/0ax;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "extbrowser"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "url"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 1959773
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_0
.end method
