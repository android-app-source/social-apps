.class public final enum LX/D9J;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D9J;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D9J;

.field public static final enum CHAT_THREAD:LX/D9J;

.field public static final enum INBOX_HEAD:LX/D9J;

.field public static final enum OMNI_PICKER:LX/D9J;

.field public static final enum UNSET:LX/D9J;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1969973
    new-instance v0, LX/D9J;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v2}, LX/D9J;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D9J;->UNSET:LX/D9J;

    .line 1969974
    new-instance v0, LX/D9J;

    const-string v1, "INBOX_HEAD"

    invoke-direct {v0, v1, v3}, LX/D9J;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D9J;->INBOX_HEAD:LX/D9J;

    .line 1969975
    new-instance v0, LX/D9J;

    const-string v1, "CHAT_THREAD"

    invoke-direct {v0, v1, v4}, LX/D9J;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D9J;->CHAT_THREAD:LX/D9J;

    .line 1969976
    new-instance v0, LX/D9J;

    const-string v1, "OMNI_PICKER"

    invoke-direct {v0, v1, v5}, LX/D9J;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D9J;->OMNI_PICKER:LX/D9J;

    .line 1969977
    const/4 v0, 0x4

    new-array v0, v0, [LX/D9J;

    sget-object v1, LX/D9J;->UNSET:LX/D9J;

    aput-object v1, v0, v2

    sget-object v1, LX/D9J;->INBOX_HEAD:LX/D9J;

    aput-object v1, v0, v3

    sget-object v1, LX/D9J;->CHAT_THREAD:LX/D9J;

    aput-object v1, v0, v4

    sget-object v1, LX/D9J;->OMNI_PICKER:LX/D9J;

    aput-object v1, v0, v5

    sput-object v0, LX/D9J;->$VALUES:[LX/D9J;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1969972
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D9J;
    .locals 1

    .prologue
    .line 1969970
    const-class v0, LX/D9J;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D9J;

    return-object v0
.end method

.method public static values()[LX/D9J;
    .locals 1

    .prologue
    .line 1969971
    sget-object v0, LX/D9J;->$VALUES:[LX/D9J;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D9J;

    return-object v0
.end method
