.class public final LX/Dxx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V
    .locals 0

    .prologue
    .line 2063510
    iput-object p1, p0, LX/Dxx;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x7f772dd1

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2063511
    iget-object v0, p0, LX/Dxx;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->T:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dxx;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->T:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2063512
    :cond_0
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 2063513
    iget-object v0, p0, LX/Dxx;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2063514
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    .line 2063515
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2063516
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 2063517
    :goto_1
    iget-object v1, p0, LX/Dxx;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-static {}, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->newBuilder()LX/A5S;

    move-result-object v2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2063518
    iput-object v0, v2, LX/A5S;->j:LX/0Px;

    .line 2063519
    move-object v0, v2

    .line 2063520
    const v2, 0x7f0811f3

    .line 2063521
    iput v2, v0, LX/A5S;->b:I

    .line 2063522
    move-object v0, v0

    .line 2063523
    invoke-virtual {v0}, LX/A5S;->a()Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->a(Landroid/content/Context;Lcom/facebook/tagging/conversion/FriendSelectorConfig;)Landroid/content/Intent;

    move-result-object v1

    .line 2063524
    iget-object v0, p0, LX/Dxx;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Dxx;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-interface {v0, v1, v8, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2063525
    const v0, -0x4b3219ad

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    .line 2063526
    :cond_2
    iget-object v0, p0, LX/Dxx;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->T:Ljava/util/Set;

    goto :goto_1
.end method
