.class public final LX/EwB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ewi;

.field public final synthetic b:LX/EwG;


# direct methods
.method public constructor <init>(LX/EwG;LX/Ewi;)V
    .locals 0

    .prologue
    .line 2182778
    iput-object p1, p0, LX/EwB;->b:LX/EwG;

    iput-object p2, p0, LX/EwB;->a:LX/Ewi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v0, 0x1

    const v1, -0x68232575

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2182779
    iget-object v1, p0, LX/EwB;->a:LX/Ewi;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v2}, LX/Eus;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2182780
    iget-object v1, p0, LX/EwB;->a:LX/Ewi;

    const/4 v2, 0x0

    .line 2182781
    iput-boolean v2, v1, LX/Eut;->f:Z

    .line 2182782
    iget-object v1, p0, LX/EwB;->b:LX/EwG;

    const v2, 0x2048efdd

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2182783
    iget-object v1, p0, LX/EwB;->b:LX/EwG;

    invoke-static {v1}, LX/EwG;->e(LX/EwG;)V

    .line 2182784
    iget-object v1, p0, LX/EwB;->b:LX/EwG;

    invoke-static {v1}, LX/EwG;->f$redex0(LX/EwG;)V

    .line 2182785
    iget-object v1, p0, LX/EwB;->b:LX/EwG;

    invoke-static {v1}, LX/EwG;->g(LX/EwG;)V

    .line 2182786
    iget-object v1, p0, LX/EwB;->b:LX/EwG;

    iget-object v1, v1, LX/EwG;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3UJ;

    iget-object v2, p0, LX/EwB;->a:LX/Ewi;

    invoke-virtual {v2}, LX/Eus;->a()J

    move-result-wide v2

    sget-object v4, LX/2hA;->FRIENDS_CENTER_REQUESTS:LX/2hA;

    sget-object v5, LX/2na;->CONFIRM:LX/2na;

    iget-object v6, p0, LX/EwB;->b:LX/EwG;

    iget-object v7, p0, LX/EwB;->a:LX/Ewi;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {v6, v7, v8}, LX/EwG;->a$redex0(LX/EwG;LX/Ewi;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/84H;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/3UJ;->a(JLX/2hA;LX/2na;LX/84H;)V

    .line 2182787
    const v1, -0x6132fc10

    invoke-static {v9, v9, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
