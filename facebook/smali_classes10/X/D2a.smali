.class public final LX/D2a;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D2b;


# direct methods
.method public constructor <init>(LX/D2b;)V
    .locals 0

    .prologue
    .line 1958851
    iput-object p1, p0, LX/D2a;->a:LX/D2b;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1958852
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 1958853
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    .line 1958854
    sget-object v0, LX/D2Y;->a:[I

    .line 1958855
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1958856
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v1, v2

    .line 1958857
    invoke-virtual {v1}, LX/8LS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1958858
    :cond_0
    :goto_0
    return-void

    .line 1958859
    :pswitch_0
    iget-object v0, p0, LX/D2a;->a:LX/D2b;

    iget-object v0, v0, LX/D2b;->a:Ljava/lang/String;

    .line 1958860
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1958861
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1958862
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1958863
    iget-object v0, p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1958864
    iget-object v1, p0, LX/D2a;->a:LX/D2b;

    iget-object v1, v1, LX/D2b;->c:LX/D2T;

    iget-object v2, p0, LX/D2a;->a:LX/D2b;

    iget-object v2, v2, LX/D2b;->a:Ljava/lang/String;

    .line 1958865
    invoke-static {v1, v2}, LX/D2T;->e(LX/D2T;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1958866
    iget-object v3, v1, LX/D2T;->g:LX/D2S;

    .line 1958867
    new-instance p1, LX/D2R;

    invoke-direct {p1, v3}, LX/D2R;-><init>(LX/D2S;)V

    move-object v3, p1

    .line 1958868
    invoke-virtual {v3, v0}, LX/D2R;->b(Ljava/lang/String;)LX/D2R;

    move-result-object v3

    invoke-virtual {v3}, LX/D2R;->a()LX/D2S;

    move-result-object v3

    iput-object v3, v1, LX/D2T;->g:LX/D2S;

    .line 1958869
    invoke-static {v1}, LX/D2T;->g(LX/D2T;)V

    .line 1958870
    :cond_1
    iget-object v1, p0, LX/D2a;->a:LX/D2b;

    iget-object v1, v1, LX/D2b;->d:LX/BQP;

    invoke-virtual {v1}, LX/BQP;->e()V

    .line 1958871
    iget-object v1, p0, LX/D2a;->a:LX/D2b;

    iget-object v1, v1, LX/D2b;->f:LX/D2X;

    .line 1958872
    new-instance v3, LX/D2W;

    invoke-static {v1}, LX/CSD;->a(LX/0QB;)LX/CSD;

    move-result-object v5

    check-cast v5, LX/CSD;

    invoke-static {v1}, LX/D2T;->a(LX/0QB;)LX/D2T;

    move-result-object v6

    check-cast v6, LX/D2T;

    invoke-static {v1}, LX/BQP;->a(LX/0QB;)LX/BQP;

    move-result-object v7

    check-cast v7, LX/BQP;

    invoke-static {v1}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    move-object v4, v0

    invoke-direct/range {v3 .. v8}, LX/D2W;-><init>(Ljava/lang/String;LX/CSD;LX/D2T;LX/BQP;Landroid/os/Handler;)V

    .line 1958873
    move-object v0, v3

    .line 1958874
    iget-object v3, v0, LX/D2W;->c:LX/CSD;

    iget-object v4, v0, LX/D2W;->g:LX/D2V;

    sget-object v5, LX/D2W;->a:[Ljava/lang/String;

    .line 1958875
    invoke-static {v3, v4, v5}, LX/CSD;->b(LX/CSD;LX/D2V;[Ljava/lang/String;)V

    .line 1958876
    iget-object v3, v0, LX/D2W;->f:Landroid/os/Handler;

    iget-object v4, v0, LX/D2W;->h:Ljava/lang/Runnable;

    const-wide/32 v5, 0x5265c00

    const v7, 0x68e998d8

    invoke-static {v3, v4, v5, v6, v7}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1958877
    iget-object v0, p0, LX/D2a;->a:LX/D2b;

    invoke-static {v0}, LX/D2b;->b(LX/D2b;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
