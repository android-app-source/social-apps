.class public final LX/Dwg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V
    .locals 0

    .prologue
    .line 2061222
    iput-object p1, p0, LX/Dwg;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2061223
    iget-object v0, p0, LX/Dwg;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->y:LX/03V;

    sget-object v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->L:Ljava/lang/String;

    const-string v2, "Failed on GraphQLSubscription callback"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2061224
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2061225
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2061226
    if-eqz p1, :cond_0

    .line 2061227
    iget-object v1, p0, LX/Dwg;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, p0, LX/Dwg;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {v0}, LX/4Vp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/4Vp;

    move-result-object v2

    .line 2061228
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2061229
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2061230
    iput-object v0, v2, LX/4Vp;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2061231
    move-object v0, v2

    .line 2061232
    invoke-virtual {v0}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    .line 2061233
    iput-object v0, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2061234
    iget-object v0, p0, LX/Dwg;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-static {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m$redex0(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    .line 2061235
    :cond_0
    return-void
.end method
