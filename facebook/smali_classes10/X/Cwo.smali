.class public LX/Cwo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Cwo;


# instance fields
.field private final a:LX/A0U;


# direct methods
.method public constructor <init>(LX/A0U;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1950990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950991
    iput-object p1, p0, LX/Cwo;->a:LX/A0U;

    .line 1950992
    return-void
.end method

.method public static a(LX/0QB;)LX/Cwo;
    .locals 4

    .prologue
    .line 1950993
    sget-object v0, LX/Cwo;->b:LX/Cwo;

    if-nez v0, :cond_1

    .line 1950994
    const-class v1, LX/Cwo;

    monitor-enter v1

    .line 1950995
    :try_start_0
    sget-object v0, LX/Cwo;->b:LX/Cwo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1950996
    if-eqz v2, :cond_0

    .line 1950997
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1950998
    new-instance p0, LX/Cwo;

    invoke-static {v0}, LX/A0U;->a(LX/0QB;)LX/A0U;

    move-result-object v3

    check-cast v3, LX/A0U;

    invoke-direct {p0, v3}, LX/Cwo;-><init>(LX/A0U;)V

    .line 1950999
    move-object v0, p0

    .line 1951000
    sput-object v0, LX/Cwo;->b:LX/Cwo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1951001
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1951002
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1951003
    :cond_1
    sget-object v0, LX/Cwo;->b:LX/Cwo;

    return-object v0

    .line 1951004
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1951005
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;)Lcom/facebook/search/model/EntityTypeaheadUnit;
    .locals 6

    .prologue
    .line 1951006
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1951007
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;->b()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1951008
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;->b()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1951009
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;->b()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1951010
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 1951011
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    .line 1951012
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->fO_()Ljava/lang/String;

    move-result-object v3

    .line 1951013
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1951014
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Missing id for entity of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1951015
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;->b()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->fO_()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1951016
    new-instance v0, LX/7C4;

    sget-object v3, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Missing name for entity with type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", id "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1951017
    :cond_1
    new-instance v4, LX/Cw7;

    invoke-direct {v4}, LX/Cw7;-><init>()V

    .line 1951018
    iput-object v1, v4, LX/Cw7;->a:Ljava/lang/String;

    .line 1951019
    move-object v1, v4

    .line 1951020
    invoke-virtual {v1, v2}, LX/Cw7;->a(I)LX/Cw7;

    move-result-object v1

    .line 1951021
    iput-object v3, v1, LX/Cw7;->b:Ljava/lang/String;

    .line 1951022
    move-object v1, v1

    .line 1951023
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1951024
    iput-object v2, v1, LX/Cw7;->g:Ljava/lang/String;

    .line 1951025
    move-object v1, v1

    .line 1951026
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1951027
    iput-object v2, v1, LX/Cw7;->f:Ljava/lang/String;

    .line 1951028
    move-object v1, v1

    .line 1951029
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->e()Z

    move-result v2

    .line 1951030
    iput-boolean v2, v1, LX/Cw7;->h:Z

    .line 1951031
    move-object v1, v1

    .line 1951032
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x41e065f

    if-ne v2, v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->c()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1951033
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;->b()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->c()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel$GroupPhotorealisticIconModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1951034
    iput-object v0, v1, LX/Cw7;->e:Landroid/net/Uri;

    .line 1951035
    :cond_2
    :goto_0
    invoke-virtual {v1}, LX/Cw7;->x()Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-result-object v0

    return-object v0

    .line 1951036
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->fP_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1951037
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;->b()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel$NodeModel;->fP_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1951038
    iput-object v0, v1, LX/Cw7;->e:Landroid/net/Uri;

    .line 1951039
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel;)Lcom/facebook/search/model/EntityTypeaheadUnit;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1951040
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1951041
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1951042
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1951043
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v5

    .line 1951044
    invoke-virtual {v5}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v6

    .line 1951045
    invoke-virtual {v5}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    .line 1951046
    invoke-virtual {v5}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;->fz_()Ljava/lang/String;

    move-result-object v8

    .line 1951047
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951048
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing id for entity of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1951049
    :cond_0
    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1951050
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing name for entity with type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1951051
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 1951052
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v9, p0, LX/Cwo;->a:LX/A0U;

    invoke-virtual {v9, v1, v0}, LX/A0U;->a(LX/15i;I)LX/0Px;

    move-result-object v9

    .line 1951053
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 1951054
    invoke-virtual {v9, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 1951055
    :goto_0
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v0

    if-le v0, v3, :cond_2

    .line 1951056
    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 1951057
    :cond_2
    :goto_1
    new-instance v0, LX/Cw7;

    invoke-direct {v0}, LX/Cw7;-><init>()V

    .line 1951058
    iput-object v6, v0, LX/Cw7;->a:Ljava/lang/String;

    .line 1951059
    move-object v0, v0

    .line 1951060
    invoke-virtual {v0, v7}, LX/Cw7;->a(I)LX/Cw7;

    move-result-object v0

    .line 1951061
    iput-object v8, v0, LX/Cw7;->b:Ljava/lang/String;

    .line 1951062
    move-object v0, v0

    .line 1951063
    iput-object v1, v0, LX/Cw7;->g:Ljava/lang/String;

    .line 1951064
    move-object v0, v0

    .line 1951065
    iput-object v2, v0, LX/Cw7;->f:Ljava/lang/String;

    .line 1951066
    move-object v0, v0

    .line 1951067
    invoke-virtual {v5}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;->e()Z

    move-result v1

    .line 1951068
    iput-boolean v1, v0, LX/Cw7;->h:Z

    .line 1951069
    move-object v1, v0

    .line 1951070
    invoke-virtual {v5}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x41e065f

    if-ne v0, v2, :cond_5

    .line 1951071
    invoke-virtual {v5}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1951072
    if-eqz v0, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_6

    .line 1951073
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1951074
    iput-object v0, v1, LX/Cw7;->e:Landroid/net/Uri;

    .line 1951075
    :cond_3
    :goto_3
    invoke-virtual {v1}, LX/Cw7;->x()Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v4

    .line 1951076
    goto :goto_2

    :cond_5
    move v0, v4

    goto :goto_2

    .line 1951077
    :cond_6
    invoke-virtual {v5}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;->fA_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1951078
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchUserWithFiltersQueryModel$FilteredQueryModel$ResultsModel$EdgesModel$NodeModel;->fA_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1951079
    iput-object v0, v1, LX/Cw7;->e:Landroid/net/Uri;

    .line 1951080
    goto :goto_3

    :cond_7
    move-object v1, v2

    goto :goto_0

    :cond_8
    move-object v1, v2

    goto :goto_1
.end method
