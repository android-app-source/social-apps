.class public final LX/D2g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/7TD;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

.field public final synthetic b:LX/D2h;


# direct methods
.method public constructor <init>(LX/D2h;Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;)V
    .locals 0

    .prologue
    .line 1958999
    iput-object p1, p0, LX/D2g;->b:LX/D2h;

    iput-object p2, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1959000
    iget-object v0, p0, LX/D2g;->b:LX/D2h;

    iget-object v0, v0, LX/D2h;->k:LX/1Er;

    const-string v1, "profile-video-cropped"

    const-string v2, ".mp4"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v2

    .line 1959001
    :try_start_0
    iget-object v0, p0, LX/D2g;->b:LX/D2h;

    iget-object v0, v0, LX/D2h;->n:LX/2MV;

    iget-object v1, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1959002
    iget-object v3, v1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    move-object v1, v3

    .line 1959003
    invoke-interface {v0, v1}, LX/2MV;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v3

    .line 1959004
    iget-object v0, p0, LX/D2g;->b:LX/D2h;

    iget-object v0, v0, LX/D2h;->p:LX/7St;

    iget v1, v3, LX/60x;->b:I

    iget v4, v3, LX/60x;->c:I

    invoke-virtual {v0, v1, v4}, LX/7St;->a(II)V

    .line 1959005
    iget-object v0, p0, LX/D2g;->b:LX/D2h;

    iget-object v0, v0, LX/D2h;->q:LX/7SE;

    invoke-virtual {v0}, LX/7SE;->a()LX/7SD;

    move-result-object v4

    .line 1959006
    iget-object v0, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1959007
    iget-object v1, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v1

    .line 1959008
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1959009
    iget-object v1, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v1

    .line 1959010
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1959011
    iget-object v0, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1959012
    iget-object v1, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v1

    .line 1959013
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1959014
    invoke-virtual {v4, v0}, LX/7SD;->a(Landroid/net/Uri;)LX/7SD;

    .line 1959015
    :cond_0
    iget-object v0, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1959016
    iget-object v1, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v1

    .line 1959017
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1959018
    iget-object v1, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v1

    .line 1959019
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->shouldFlipHorizontally()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/7Sv;->MIRROR_HORIZONTALLY:LX/7Sv;

    move-object v1, v0

    .line 1959020
    :goto_0
    iget-object v0, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->h()Landroid/graphics/RectF;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1959021
    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v12, 0x40000000    # 2.0f

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    .line 1959022
    invoke-static {v3}, LX/7TJ;->c(LX/60x;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget v5, v3, LX/60x;->c:I

    :goto_1
    move v5, v5

    .line 1959023
    invoke-static {v3}, LX/7TJ;->c(LX/60x;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget v6, v3, LX/60x;->b:I

    :goto_2
    move v6, v6

    .line 1959024
    int-to-float v7, v5

    div-float/2addr v7, v0

    .line 1959025
    int-to-float v8, v6

    div-float/2addr v8, v0

    .line 1959026
    int-to-float v9, v6

    cmpg-float v9, v7, v9

    if-gez v9, :cond_3

    .line 1959027
    int-to-float v5, v6

    sub-float/2addr v5, v7

    div-float/2addr v5, v12

    .line 1959028
    int-to-float v6, v6

    div-float v6, v5, v6

    .line 1959029
    new-instance v5, Landroid/graphics/RectF;

    add-float v7, v10, v6

    sub-float v6, v11, v6

    invoke-direct {v5, v10, v7, v11, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1959030
    :goto_3
    move-object v0, v5

    .line 1959031
    :goto_4
    invoke-static {}, LX/7TH;->newBuilder()LX/7TI;

    move-result-object v3

    new-instance v5, Ljava/io/File;

    iget-object v6, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1959032
    iget-object v7, v6, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    move-object v6, v7

    .line 1959033
    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1959034
    iput-object v5, v3, LX/7TI;->a:Ljava/io/File;

    .line 1959035
    move-object v3, v3

    .line 1959036
    iput-object v2, v3, LX/7TI;->b:Ljava/io/File;

    .line 1959037
    move-object v2, v3

    .line 1959038
    iput-object v0, v2, LX/7TI;->d:Landroid/graphics/RectF;

    .line 1959039
    move-object v0, v2

    .line 1959040
    iget-object v2, p0, LX/D2g;->b:LX/D2h;

    iget-object v2, v2, LX/D2h;->p:LX/7St;

    .line 1959041
    iput-object v2, v0, LX/7TI;->c:LX/2Md;

    .line 1959042
    move-object v0, v0

    .line 1959043
    iget-object v2, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->j()I

    move-result v2

    .line 1959044
    iput v2, v0, LX/7TI;->f:I

    .line 1959045
    move-object v0, v0

    .line 1959046
    iget-object v2, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->k()I

    move-result v2

    .line 1959047
    iput v2, v0, LX/7TI;->g:I

    .line 1959048
    move-object v0, v0

    .line 1959049
    iget-object v2, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1959050
    iget-object v3, v2, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    if-eqz v3, :cond_6

    iget-object v3, v2, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->isVideoMuted()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_5
    move v2, v3

    .line 1959051
    iput-boolean v2, v0, LX/7TI;->i:Z

    .line 1959052
    move-object v0, v0

    .line 1959053
    invoke-virtual {v4}, LX/7SD;->a()LX/0Px;

    move-result-object v2

    .line 1959054
    iput-object v2, v0, LX/7TI;->n:LX/0Px;

    .line 1959055
    move-object v0, v0

    .line 1959056
    iput-object v1, v0, LX/7TI;->e:LX/7Sv;

    .line 1959057
    move-object v0, v0

    .line 1959058
    invoke-virtual {v0}, LX/7TI;->o()LX/7TH;

    move-result-object v0

    .line 1959059
    iget-object v1, p0, LX/D2g;->b:LX/D2h;

    iget-object v1, v1, LX/D2h;->o:LX/7TG;

    invoke-virtual {v1, v0}, LX/7TG;->a(LX/7TH;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1959060
    const v1, 0x746f4ed4

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7TD;

    .line 1959061
    :goto_6
    return-object v0

    .line 1959062
    :cond_1
    sget-object v0, LX/7Sv;->NONE:LX/7Sv;

    move-object v1, v0

    goto/16 :goto_0

    .line 1959063
    :cond_2
    iget-object v0, p0, LX/D2g;->a:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->i()Landroid/graphics/RectF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_4

    .line 1959064
    :catch_0
    move-exception v0

    .line 1959065
    const-string v1, "ProfileVideoUploader"

    const-string v2, "Failed to transcode"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1959066
    const/4 v0, 0x0

    goto :goto_6

    .line 1959067
    :cond_3
    :try_start_1
    int-to-float v6, v5

    sub-float/2addr v6, v8

    div-float/2addr v6, v12

    .line 1959068
    int-to-float v5, v5

    div-float/2addr v6, v5

    .line 1959069
    new-instance v5, Landroid/graphics/RectF;

    add-float v7, v10, v6

    sub-float v6, v11, v6

    invoke-direct {v5, v7, v10, v6, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto/16 :goto_3

    :cond_4
    iget v5, v3, LX/60x;->b:I

    goto/16 :goto_1

    :cond_5
    iget v6, v3, LX/60x;->c:I

    goto/16 :goto_2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_5
.end method
