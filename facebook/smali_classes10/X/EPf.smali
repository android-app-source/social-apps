.class public LX/EPf;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;
.implements LX/1a7;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2117365
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EPf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2117366
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2117362
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2117363
    const v0, 0x7f030a26

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2117364
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2117367
    iget-boolean v0, p0, LX/EPf;->a:Z

    return v0
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 2117361
    const/4 v0, 0x1

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x20be9d9d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2117357
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2117358
    const/4 v1, 0x1

    .line 2117359
    iput-boolean v1, p0, LX/EPf;->a:Z

    .line 2117360
    const/16 v1, 0x2d

    const v2, -0x4d4f9ed8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6d5f9b90

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2117353
    const/4 v1, 0x0

    .line 2117354
    iput-boolean v1, p0, LX/EPf;->a:Z

    .line 2117355
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2117356
    const/16 v1, 0x2d

    const v2, -0x71eeb82c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
