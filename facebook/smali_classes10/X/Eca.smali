.class public final LX/Eca;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EcY;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Eca;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/Eca;


# instance fields
.field public bitField0_:I

.field public localBaseKeyPrivate_:LX/EWc;

.field public localBaseKey_:LX/EWc;

.field public localIdentityKeyPrivate_:LX/EWc;

.field public localIdentityKey_:LX/EWc;

.field public localRatchetKeyPrivate_:LX/EWc;

.field public localRatchetKey_:LX/EWc;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public sequence_:I

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2147357
    new-instance v0, LX/EcX;

    invoke-direct {v0}, LX/EcX;-><init>()V

    sput-object v0, LX/Eca;->a:LX/EWZ;

    .line 2147358
    new-instance v0, LX/Eca;

    invoke-direct {v0}, LX/Eca;-><init>()V

    .line 2147359
    sput-object v0, LX/Eca;->c:LX/Eca;

    invoke-direct {v0}, LX/Eca;->G()V

    .line 2147360
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2147352
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2147353
    iput-byte v0, p0, LX/Eca;->memoizedIsInitialized:B

    .line 2147354
    iput v0, p0, LX/Eca;->memoizedSerializedSize:I

    .line 2147355
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2147356
    iput-object v0, p0, LX/Eca;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2147312
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2147313
    iput-byte v0, p0, LX/Eca;->memoizedIsInitialized:B

    .line 2147314
    iput v0, p0, LX/Eca;->memoizedSerializedSize:I

    .line 2147315
    invoke-direct {p0}, LX/Eca;->G()V

    .line 2147316
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2147317
    const/4 v0, 0x0

    .line 2147318
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2147319
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2147320
    sparse-switch v3, :sswitch_data_0

    .line 2147321
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2147322
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2147323
    goto :goto_0

    .line 2147324
    :sswitch_1
    iget v3, p0, LX/Eca;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/Eca;->bitField0_:I

    .line 2147325
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/Eca;->sequence_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2147326
    :catch_0
    move-exception v0

    .line 2147327
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2147328
    move-object v0, v0

    .line 2147329
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2147330
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/Eca;->unknownFields:LX/EZQ;

    .line 2147331
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2147332
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/Eca;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/Eca;->bitField0_:I

    .line 2147333
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Eca;->localBaseKey_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2147334
    :catch_1
    move-exception v0

    .line 2147335
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2147336
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2147337
    move-object v0, v1

    .line 2147338
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2147339
    :sswitch_3
    :try_start_4
    iget v3, p0, LX/Eca;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, LX/Eca;->bitField0_:I

    .line 2147340
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Eca;->localBaseKeyPrivate_:LX/EWc;

    goto :goto_0

    .line 2147341
    :sswitch_4
    iget v3, p0, LX/Eca;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, LX/Eca;->bitField0_:I

    .line 2147342
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Eca;->localRatchetKey_:LX/EWc;

    goto :goto_0

    .line 2147343
    :sswitch_5
    iget v3, p0, LX/Eca;->bitField0_:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, LX/Eca;->bitField0_:I

    .line 2147344
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Eca;->localRatchetKeyPrivate_:LX/EWc;

    goto :goto_0

    .line 2147345
    :sswitch_6
    iget v3, p0, LX/Eca;->bitField0_:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, LX/Eca;->bitField0_:I

    .line 2147346
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Eca;->localIdentityKey_:LX/EWc;

    goto :goto_0

    .line 2147347
    :sswitch_7
    iget v3, p0, LX/Eca;->bitField0_:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, LX/Eca;->bitField0_:I

    .line 2147348
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Eca;->localIdentityKeyPrivate_:LX/EWc;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2147349
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Eca;->unknownFields:LX/EZQ;

    .line 2147350
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2147351
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2147307
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2147308
    iput-byte v1, p0, LX/Eca;->memoizedIsInitialized:B

    .line 2147309
    iput v1, p0, LX/Eca;->memoizedSerializedSize:I

    .line 2147310
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Eca;->unknownFields:LX/EZQ;

    .line 2147311
    return-void
.end method

.method private G()V
    .locals 1

    .prologue
    .line 2147299
    const/4 v0, 0x0

    iput v0, p0, LX/Eca;->sequence_:I

    .line 2147300
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eca;->localBaseKey_:LX/EWc;

    .line 2147301
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eca;->localBaseKeyPrivate_:LX/EWc;

    .line 2147302
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eca;->localRatchetKey_:LX/EWc;

    .line 2147303
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eca;->localRatchetKeyPrivate_:LX/EWc;

    .line 2147304
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eca;->localIdentityKey_:LX/EWc;

    .line 2147305
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eca;->localIdentityKeyPrivate_:LX/EWc;

    .line 2147306
    return-void
.end method

.method public static a(LX/Eca;)LX/EcZ;
    .locals 1

    .prologue
    .line 2147298
    invoke-static {}, LX/EcZ;->u()LX/EcZ;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EcZ;->a(LX/Eca;)LX/EcZ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final C()LX/EcZ;
    .locals 1

    .prologue
    .line 2147297
    invoke-static {p0}, LX/Eca;->a(LX/Eca;)LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2147295
    new-instance v0, LX/EcZ;

    invoke-direct {v0, p1}, LX/EcZ;-><init>(LX/EYd;)V

    .line 2147296
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2147278
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2147279
    iget v0, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2147280
    iget v0, p0, LX/Eca;->sequence_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2147281
    :cond_0
    iget v0, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2147282
    iget-object v0, p0, LX/Eca;->localBaseKey_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2147283
    :cond_1
    iget v0, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2147284
    const/4 v0, 0x3

    iget-object v1, p0, LX/Eca;->localBaseKeyPrivate_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2147285
    :cond_2
    iget v0, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 2147286
    iget-object v0, p0, LX/Eca;->localRatchetKey_:LX/EWc;

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2147287
    :cond_3
    iget v0, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2147288
    const/4 v0, 0x5

    iget-object v1, p0, LX/Eca;->localRatchetKeyPrivate_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2147289
    :cond_4
    iget v0, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 2147290
    const/4 v0, 0x7

    iget-object v1, p0, LX/Eca;->localIdentityKey_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2147291
    :cond_5
    iget v0, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 2147292
    iget-object v0, p0, LX/Eca;->localIdentityKeyPrivate_:LX/EWc;

    invoke-virtual {p1, v4, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2147293
    :cond_6
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2147294
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2147246
    iget-byte v1, p0, LX/Eca;->memoizedIsInitialized:B

    .line 2147247
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2147248
    :goto_0
    return v0

    .line 2147249
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2147250
    :cond_1
    iput-byte v0, p0, LX/Eca;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2147258
    iget v0, p0, LX/Eca;->memoizedSerializedSize:I

    .line 2147259
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2147260
    :goto_0
    return v0

    .line 2147261
    :cond_0
    const/4 v0, 0x0

    .line 2147262
    iget v1, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2147263
    iget v0, p0, LX/Eca;->sequence_:I

    invoke-static {v2, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2147264
    :cond_1
    iget v1, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2147265
    iget-object v1, p0, LX/Eca;->localBaseKey_:LX/EWc;

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147266
    :cond_2
    iget v1, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 2147267
    const/4 v1, 0x3

    iget-object v2, p0, LX/Eca;->localBaseKeyPrivate_:LX/EWc;

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147268
    :cond_3
    iget v1, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 2147269
    iget-object v1, p0, LX/Eca;->localRatchetKey_:LX/EWc;

    invoke-static {v4, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147270
    :cond_4
    iget v1, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 2147271
    const/4 v1, 0x5

    iget-object v2, p0, LX/Eca;->localRatchetKeyPrivate_:LX/EWc;

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147272
    :cond_5
    iget v1, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 2147273
    const/4 v1, 0x7

    iget-object v2, p0, LX/Eca;->localIdentityKey_:LX/EWc;

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147274
    :cond_6
    iget v1, p0, LX/Eca;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 2147275
    iget-object v1, p0, LX/Eca;->localIdentityKeyPrivate_:LX/EWc;

    invoke-static {v5, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147276
    :cond_7
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2147277
    iput v0, p0, LX/Eca;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2147257
    iget-object v0, p0, LX/Eca;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2147256
    sget-object v0, LX/Eck;->j:LX/EYn;

    const-class v1, LX/Eca;

    const-class v2, LX/EcZ;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Eca;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2147255
    sget-object v0, LX/Eca;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2147254
    invoke-virtual {p0}, LX/Eca;->C()LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2147253
    invoke-static {}, LX/EcZ;->u()LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2147252
    invoke-virtual {p0}, LX/Eca;->C()LX/EcZ;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2147251
    sget-object v0, LX/Eca;->c:LX/Eca;

    return-object v0
.end method
