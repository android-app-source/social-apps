.class public final LX/E21;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E22;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/5sY;

.field public b:F

.field public final synthetic c:LX/E22;


# direct methods
.method public constructor <init>(LX/E22;)V
    .locals 1

    .prologue
    .line 2072087
    iput-object p1, p0, LX/E21;->c:LX/E22;

    .line 2072088
    move-object v0, p1

    .line 2072089
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2072090
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2072091
    const-string v0, "ReactionCoreImageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2072092
    if-ne p0, p1, :cond_1

    .line 2072093
    :cond_0
    :goto_0
    return v0

    .line 2072094
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2072095
    goto :goto_0

    .line 2072096
    :cond_3
    check-cast p1, LX/E21;

    .line 2072097
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2072098
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2072099
    if-eq v2, v3, :cond_0

    .line 2072100
    iget-object v2, p0, LX/E21;->a:LX/5sY;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E21;->a:LX/5sY;

    iget-object v3, p1, LX/E21;->a:LX/5sY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2072101
    goto :goto_0

    .line 2072102
    :cond_5
    iget-object v2, p1, LX/E21;->a:LX/5sY;

    if-nez v2, :cond_4

    .line 2072103
    :cond_6
    iget v2, p0, LX/E21;->b:F

    iget v3, p1, LX/E21;->b:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 2072104
    goto :goto_0
.end method
