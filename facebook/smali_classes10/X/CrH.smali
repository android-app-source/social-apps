.class public LX/CrH;
.super LX/Cqj;
.source ""


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 1940765
    invoke-direct {p0, p1, p2}, LX/Cqj;-><init>(LX/Ctg;LX/CrK;)V

    .line 1940766
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->a:LX/Cqw;

    .line 1940767
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940768
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940769
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->b:LX/Cqw;

    .line 1940770
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940771
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940772
    return-void
.end method


# virtual methods
.method public final l()V
    .locals 3

    .prologue
    .line 1940773
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1940774
    :goto_0
    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1940775
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    .line 1940776
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p0, v1, v2}, LX/Cqi;->a(II)V

    .line 1940777
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/Cqi;->b(II)V

    .line 1940778
    return-void
.end method
