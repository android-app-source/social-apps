.class public LX/Dpa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;


# instance fields
.field public final facebook_hmac:[B

.field public final has_prekey_material:Ljava/lang/Boolean;

.field public final is_message:Ljava/lang/Boolean;

.field public final is_multi:Ljava/lang/Boolean;

.field public final is_retry:Ljava/lang/Boolean;

.field public final sender_hmac:[B

.field public final serialized_salamander:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/4 v3, 0x2

    .line 2046394
    new-instance v0, LX/1sv;

    const-string v1, "SalamanderPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Dpa;->b:LX/1sv;

    .line 2046395
    new-instance v0, LX/1sw;

    const-string v1, "serialized_salamander"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpa;->c:LX/1sw;

    .line 2046396
    new-instance v0, LX/1sw;

    const-string v1, "sender_hmac"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpa;->d:LX/1sw;

    .line 2046397
    new-instance v0, LX/1sw;

    const-string v1, "facebook_hmac"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpa;->e:LX/1sw;

    .line 2046398
    new-instance v0, LX/1sw;

    const-string v1, "has_prekey_material"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpa;->f:LX/1sw;

    .line 2046399
    new-instance v0, LX/1sw;

    const-string v1, "is_message"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpa;->g:LX/1sw;

    .line 2046400
    new-instance v0, LX/1sw;

    const-string v1, "is_multi"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpa;->h:LX/1sw;

    .line 2046401
    new-instance v0, LX/1sw;

    const-string v1, "is_retry"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpa;->i:LX/1sw;

    .line 2046402
    const/4 v0, 0x1

    sput-boolean v0, LX/Dpa;->a:Z

    return-void
.end method

.method public constructor <init>([B[B[BLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 2046288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2046289
    iput-object p1, p0, LX/Dpa;->serialized_salamander:[B

    .line 2046290
    iput-object p2, p0, LX/Dpa;->sender_hmac:[B

    .line 2046291
    iput-object p3, p0, LX/Dpa;->facebook_hmac:[B

    .line 2046292
    iput-object p4, p0, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    .line 2046293
    iput-object p5, p0, LX/Dpa;->is_message:Ljava/lang/Boolean;

    .line 2046294
    iput-object p6, p0, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    .line 2046295
    iput-object p7, p0, LX/Dpa;->is_retry:Ljava/lang/Boolean;

    .line 2046296
    return-void
.end method

.method public static b(LX/1su;)LX/Dpa;
    .locals 11

    .prologue
    const/16 v10, 0xb

    const/4 v9, 0x2

    const/4 v7, 0x0

    .line 2046365
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v6, v7

    move-object v5, v7

    move-object v4, v7

    move-object v3, v7

    move-object v2, v7

    move-object v1, v7

    .line 2046366
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 2046367
    iget-byte v8, v0, LX/1sw;->b:B

    if-eqz v8, :cond_7

    .line 2046368
    iget-short v8, v0, LX/1sw;->c:S

    packed-switch v8, :pswitch_data_0

    .line 2046369
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046370
    :pswitch_0
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_0

    .line 2046371
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v1

    goto :goto_0

    .line 2046372
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046373
    :pswitch_1
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_1

    .line 2046374
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v2

    goto :goto_0

    .line 2046375
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046376
    :pswitch_2
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_2

    .line 2046377
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v3

    goto :goto_0

    .line 2046378
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046379
    :pswitch_3
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v9, :cond_3

    .line 2046380
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0

    .line 2046381
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046382
    :pswitch_4
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v9, :cond_4

    .line 2046383
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    .line 2046384
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046385
    :pswitch_5
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v9, :cond_5

    .line 2046386
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_0

    .line 2046387
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046388
    :pswitch_6
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v9, :cond_6

    .line 2046389
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto/16 :goto_0

    .line 2046390
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2046391
    :cond_7
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2046392
    new-instance v0, LX/Dpa;

    invoke-direct/range {v0 .. v7}, LX/Dpa;-><init>([B[B[BLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 2046393
    return-object v0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2046297
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2046298
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2046299
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2046300
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SalamanderPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046301
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046302
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046303
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046304
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046305
    const-string v4, "serialized_salamander"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046306
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046307
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046308
    iget-object v4, p0, LX/Dpa;->serialized_salamander:[B

    if-nez v4, :cond_3

    .line 2046309
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046310
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046311
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046312
    const-string v4, "sender_hmac"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046313
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046314
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046315
    iget-object v4, p0, LX/Dpa;->sender_hmac:[B

    if-nez v4, :cond_4

    .line 2046316
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046317
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046318
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046319
    const-string v4, "facebook_hmac"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046320
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046321
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046322
    iget-object v4, p0, LX/Dpa;->facebook_hmac:[B

    if-nez v4, :cond_5

    .line 2046323
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046324
    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046325
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046326
    const-string v4, "has_prekey_material"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046327
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046328
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046329
    iget-object v4, p0, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    if-nez v4, :cond_6

    .line 2046330
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046331
    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046332
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046333
    const-string v4, "is_message"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046334
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046335
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046336
    iget-object v4, p0, LX/Dpa;->is_message:Ljava/lang/Boolean;

    if-nez v4, :cond_7

    .line 2046337
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046338
    :goto_7
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046339
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046340
    const-string v4, "is_multi"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046341
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046342
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046343
    iget-object v4, p0, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    if-nez v4, :cond_8

    .line 2046344
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046345
    :goto_8
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046346
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046347
    const-string v4, "is_retry"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046348
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046349
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046350
    iget-object v0, p0, LX/Dpa;->is_retry:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    .line 2046351
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046352
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046353
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046354
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2046355
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2046356
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2046357
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2046358
    :cond_3
    iget-object v4, p0, LX/Dpa;->serialized_salamander:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2046359
    :cond_4
    iget-object v4, p0, LX/Dpa;->sender_hmac:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2046360
    :cond_5
    iget-object v4, p0, LX/Dpa;->facebook_hmac:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2046361
    :cond_6
    iget-object v4, p0, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2046362
    :cond_7
    iget-object v4, p0, LX/Dpa;->is_message:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2046363
    :cond_8
    iget-object v4, p0, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 2046364
    :cond_9
    iget-object v0, p0, LX/Dpa;->is_retry:Ljava/lang/Boolean;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2046403
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2046404
    iget-object v0, p0, LX/Dpa;->serialized_salamander:[B

    if-eqz v0, :cond_0

    .line 2046405
    sget-object v0, LX/Dpa;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046406
    iget-object v0, p0, LX/Dpa;->serialized_salamander:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2046407
    :cond_0
    iget-object v0, p0, LX/Dpa;->sender_hmac:[B

    if-eqz v0, :cond_1

    .line 2046408
    sget-object v0, LX/Dpa;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046409
    iget-object v0, p0, LX/Dpa;->sender_hmac:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2046410
    :cond_1
    iget-object v0, p0, LX/Dpa;->facebook_hmac:[B

    if-eqz v0, :cond_2

    .line 2046411
    sget-object v0, LX/Dpa;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046412
    iget-object v0, p0, LX/Dpa;->facebook_hmac:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2046413
    :cond_2
    iget-object v0, p0, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2046414
    sget-object v0, LX/Dpa;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046415
    iget-object v0, p0, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2046416
    :cond_3
    iget-object v0, p0, LX/Dpa;->is_message:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 2046417
    sget-object v0, LX/Dpa;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046418
    iget-object v0, p0, LX/Dpa;->is_message:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2046419
    :cond_4
    iget-object v0, p0, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 2046420
    sget-object v0, LX/Dpa;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046421
    iget-object v0, p0, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2046422
    :cond_5
    iget-object v0, p0, LX/Dpa;->is_retry:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 2046423
    sget-object v0, LX/Dpa;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046424
    iget-object v0, p0, LX/Dpa;->is_retry:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2046425
    :cond_6
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2046426
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2046427
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2046228
    if-nez p1, :cond_1

    .line 2046229
    :cond_0
    :goto_0
    return v0

    .line 2046230
    :cond_1
    instance-of v1, p1, LX/Dpa;

    if-eqz v1, :cond_0

    .line 2046231
    check-cast p1, LX/Dpa;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2046232
    if-nez p1, :cond_3

    .line 2046233
    :cond_2
    :goto_1
    move v0, v2

    .line 2046234
    goto :goto_0

    .line 2046235
    :cond_3
    iget-object v0, p0, LX/Dpa;->serialized_salamander:[B

    if-eqz v0, :cond_12

    move v0, v1

    .line 2046236
    :goto_2
    iget-object v3, p1, LX/Dpa;->serialized_salamander:[B

    if-eqz v3, :cond_13

    move v3, v1

    .line 2046237
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2046238
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046239
    iget-object v0, p0, LX/Dpa;->serialized_salamander:[B

    iget-object v3, p1, LX/Dpa;->serialized_salamander:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046240
    :cond_5
    iget-object v0, p0, LX/Dpa;->sender_hmac:[B

    if-eqz v0, :cond_14

    move v0, v1

    .line 2046241
    :goto_4
    iget-object v3, p1, LX/Dpa;->sender_hmac:[B

    if-eqz v3, :cond_15

    move v3, v1

    .line 2046242
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2046243
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046244
    iget-object v0, p0, LX/Dpa;->sender_hmac:[B

    iget-object v3, p1, LX/Dpa;->sender_hmac:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046245
    :cond_7
    iget-object v0, p0, LX/Dpa;->facebook_hmac:[B

    if-eqz v0, :cond_16

    move v0, v1

    .line 2046246
    :goto_6
    iget-object v3, p1, LX/Dpa;->facebook_hmac:[B

    if-eqz v3, :cond_17

    move v3, v1

    .line 2046247
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2046248
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046249
    iget-object v0, p0, LX/Dpa;->facebook_hmac:[B

    iget-object v3, p1, LX/Dpa;->facebook_hmac:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046250
    :cond_9
    iget-object v0, p0, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    if-eqz v0, :cond_18

    move v0, v1

    .line 2046251
    :goto_8
    iget-object v3, p1, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    if-eqz v3, :cond_19

    move v3, v1

    .line 2046252
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2046253
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046254
    iget-object v0, p0, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046255
    :cond_b
    iget-object v0, p0, LX/Dpa;->is_message:Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 2046256
    :goto_a
    iget-object v3, p1, LX/Dpa;->is_message:Ljava/lang/Boolean;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 2046257
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2046258
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046259
    iget-object v0, p0, LX/Dpa;->is_message:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Dpa;->is_message:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046260
    :cond_d
    iget-object v0, p0, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 2046261
    :goto_c
    iget-object v3, p1, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 2046262
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 2046263
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046264
    iget-object v0, p0, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046265
    :cond_f
    iget-object v0, p0, LX/Dpa;->is_retry:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 2046266
    :goto_e
    iget-object v3, p1, LX/Dpa;->is_retry:Ljava/lang/Boolean;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 2046267
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 2046268
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046269
    iget-object v0, p0, LX/Dpa;->is_retry:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Dpa;->is_retry:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_11
    move v2, v1

    .line 2046270
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 2046271
    goto/16 :goto_2

    :cond_13
    move v3, v2

    .line 2046272
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 2046273
    goto/16 :goto_4

    :cond_15
    move v3, v2

    .line 2046274
    goto/16 :goto_5

    :cond_16
    move v0, v2

    .line 2046275
    goto/16 :goto_6

    :cond_17
    move v3, v2

    .line 2046276
    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 2046277
    goto/16 :goto_8

    :cond_19
    move v3, v2

    .line 2046278
    goto :goto_9

    :cond_1a
    move v0, v2

    .line 2046279
    goto :goto_a

    :cond_1b
    move v3, v2

    .line 2046280
    goto :goto_b

    :cond_1c
    move v0, v2

    .line 2046281
    goto :goto_c

    :cond_1d
    move v3, v2

    .line 2046282
    goto :goto_d

    :cond_1e
    move v0, v2

    .line 2046283
    goto :goto_e

    :cond_1f
    move v3, v2

    .line 2046284
    goto :goto_f
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2046227
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2046285
    sget-boolean v0, LX/Dpa;->a:Z

    .line 2046286
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Dpa;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2046287
    return-object v0
.end method
