.class public final LX/EZN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/EZO;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2139365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static c()LX/EZN;
    .locals 3

    .prologue
    .line 2139293
    new-instance v0, LX/EZN;

    invoke-direct {v0}, LX/EZN;-><init>()V

    .line 2139294
    new-instance v1, LX/EZO;

    invoke-direct {v1}, LX/EZO;-><init>()V

    iput-object v1, v0, LX/EZN;->a:LX/EZO;

    .line 2139295
    return-object v0
.end method


# virtual methods
.method public final a(J)LX/EZN;
    .locals 3

    .prologue
    .line 2139360
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2139361
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2139362
    iput-object v1, v0, LX/EZO;->b:Ljava/util/List;

    .line 2139363
    :cond_0
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->b:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2139364
    return-object p0
.end method

.method public final a(LX/EWc;)LX/EZN;
    .locals 2

    .prologue
    .line 2139355
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->e:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2139356
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2139357
    iput-object v1, v0, LX/EZO;->e:Ljava/util/List;

    .line 2139358
    :cond_0
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2139359
    return-object p0
.end method

.method public final a(LX/EZO;)LX/EZN;
    .locals 2

    .prologue
    .line 2139329
    iget-object v0, p1, LX/EZO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2139330
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2139331
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2139332
    iput-object v1, v0, LX/EZO;->b:Ljava/util/List;

    .line 2139333
    :cond_0
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->b:Ljava/util/List;

    iget-object v1, p1, LX/EZO;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2139334
    :cond_1
    iget-object v0, p1, LX/EZO;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2139335
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->c:Ljava/util/List;

    if-nez v0, :cond_2

    .line 2139336
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2139337
    iput-object v1, v0, LX/EZO;->c:Ljava/util/List;

    .line 2139338
    :cond_2
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->c:Ljava/util/List;

    iget-object v1, p1, LX/EZO;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2139339
    :cond_3
    iget-object v0, p1, LX/EZO;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2139340
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->d:Ljava/util/List;

    if-nez v0, :cond_4

    .line 2139341
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2139342
    iput-object v1, v0, LX/EZO;->d:Ljava/util/List;

    .line 2139343
    :cond_4
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->d:Ljava/util/List;

    iget-object v1, p1, LX/EZO;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2139344
    :cond_5
    iget-object v0, p1, LX/EZO;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2139345
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->e:Ljava/util/List;

    if-nez v0, :cond_6

    .line 2139346
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2139347
    iput-object v1, v0, LX/EZO;->e:Ljava/util/List;

    .line 2139348
    :cond_6
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->e:Ljava/util/List;

    iget-object v1, p1, LX/EZO;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2139349
    :cond_7
    iget-object v0, p1, LX/EZO;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2139350
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->f:Ljava/util/List;

    if-nez v0, :cond_8

    .line 2139351
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2139352
    iput-object v1, v0, LX/EZO;->f:Ljava/util/List;

    .line 2139353
    :cond_8
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->f:Ljava/util/List;

    iget-object v1, p1, LX/EZO;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2139354
    :cond_9
    return-object p0
.end method

.method public final a()LX/EZO;
    .locals 2

    .prologue
    .line 2139296
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2139297
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 2139298
    iput-object v1, v0, LX/EZO;->b:Ljava/util/List;

    .line 2139299
    :goto_0
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->c:Ljava/util/List;

    if-nez v0, :cond_1

    .line 2139300
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 2139301
    iput-object v1, v0, LX/EZO;->c:Ljava/util/List;

    .line 2139302
    :goto_1
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->d:Ljava/util/List;

    if-nez v0, :cond_2

    .line 2139303
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 2139304
    iput-object v1, v0, LX/EZO;->d:Ljava/util/List;

    .line 2139305
    :goto_2
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->e:Ljava/util/List;

    if-nez v0, :cond_3

    .line 2139306
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 2139307
    iput-object v1, v0, LX/EZO;->e:Ljava/util/List;

    .line 2139308
    :goto_3
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v0, v0, LX/EZO;->f:Ljava/util/List;

    if-nez v0, :cond_4

    .line 2139309
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 2139310
    iput-object v1, v0, LX/EZO;->f:Ljava/util/List;

    .line 2139311
    :goto_4
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    .line 2139312
    const/4 v1, 0x0

    iput-object v1, p0, LX/EZN;->a:LX/EZO;

    .line 2139313
    return-object v0

    .line 2139314
    :cond_0
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v1, p0, LX/EZN;->a:LX/EZO;

    iget-object v1, v1, LX/EZO;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 2139315
    iput-object v1, v0, LX/EZO;->b:Ljava/util/List;

    .line 2139316
    goto :goto_0

    .line 2139317
    :cond_1
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v1, p0, LX/EZN;->a:LX/EZO;

    iget-object v1, v1, LX/EZO;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 2139318
    iput-object v1, v0, LX/EZO;->c:Ljava/util/List;

    .line 2139319
    goto :goto_1

    .line 2139320
    :cond_2
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v1, p0, LX/EZN;->a:LX/EZO;

    iget-object v1, v1, LX/EZO;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 2139321
    iput-object v1, v0, LX/EZO;->d:Ljava/util/List;

    .line 2139322
    goto :goto_2

    .line 2139323
    :cond_3
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v1, p0, LX/EZN;->a:LX/EZO;

    iget-object v1, v1, LX/EZO;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 2139324
    iput-object v1, v0, LX/EZO;->e:Ljava/util/List;

    .line 2139325
    goto :goto_3

    .line 2139326
    :cond_4
    iget-object v0, p0, LX/EZN;->a:LX/EZO;

    iget-object v1, p0, LX/EZN;->a:LX/EZO;

    iget-object v1, v1, LX/EZO;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 2139327
    iput-object v1, v0, LX/EZO;->f:Ljava/util/List;

    .line 2139328
    goto :goto_4
.end method
