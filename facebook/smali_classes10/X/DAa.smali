.class public LX/DAa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Ov;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1971379
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DAa;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1971380
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1971381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1971382
    iput-object p1, p0, LX/DAa;->a:Ljava/lang/String;

    .line 1971383
    iput-object p2, p0, LX/DAa;->b:Ljava/lang/String;

    .line 1971384
    return-void
.end method


# virtual methods
.method public final a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "ARG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3L9",
            "<TT;TARG;>;TARG;)TT;"
        }
    .end annotation

    .prologue
    .line 1971385
    invoke-interface {p1, p0, p2}, LX/3L9;->a(LX/DAa;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1971386
    iget-object v0, p0, LX/DAa;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1971387
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "header: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/DAa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1971388
    iget-object v1, p0, LX/DAa;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1971389
    const-string v1, ", key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/DAa;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1971390
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
