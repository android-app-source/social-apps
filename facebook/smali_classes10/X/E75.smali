.class public final LX/E75;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:LX/E76;


# direct methods
.method public constructor <init>(LX/E76;ZZ)V
    .locals 0

    .prologue
    .line 2081063
    iput-object p1, p0, LX/E75;->c:LX/E76;

    iput-boolean p2, p0, LX/E75;->a:Z

    iput-boolean p3, p0, LX/E75;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2081069
    iget-object v0, p0, LX/E75;->c:LX/E76;

    iget-object v0, v0, LX/E76;->c:Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;

    iget-object v0, v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->e:Ljava/util/HashMap;

    iget-object v1, p0, LX/E75;->c:LX/E76;

    iget-object v1, v1, LX/E76;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v1

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, LX/E75;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2081070
    iget-object v0, p0, LX/E75;->c:LX/E76;

    iget-object v0, v0, LX/E76;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget-boolean v1, p0, LX/E75;->a:Z

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setSelected(Z)V

    .line 2081071
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2081066
    iget-object v0, p0, LX/E75;->c:LX/E76;

    iget-object v0, v0, LX/E76;->c:Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;

    iget-object v0, v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->e:Ljava/util/HashMap;

    iget-object v1, p0, LX/E75;->c:LX/E76;

    iget-object v1, v1, LX/E76;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v1

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, LX/E75;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2081067
    iget-object v0, p0, LX/E75;->c:LX/E76;

    iget-object v0, v0, LX/E76;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget-boolean v1, p0, LX/E75;->b:Z

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setSelected(Z)V

    .line 2081068
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2081065
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2081064
    return-void
.end method
