.class public final enum LX/Dhg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dhg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dhg;

.field public static final enum MASK:LX/Dhg;

.field public static final enum PARTICLE:LX/Dhg;

.field public static final enum STYLE_TRANSFER:LX/Dhg;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2031173
    new-instance v0, LX/Dhg;

    const-string v1, "MASK"

    invoke-direct {v0, v1, v2}, LX/Dhg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhg;->MASK:LX/Dhg;

    .line 2031174
    new-instance v0, LX/Dhg;

    const-string v1, "PARTICLE"

    invoke-direct {v0, v1, v3}, LX/Dhg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhg;->PARTICLE:LX/Dhg;

    .line 2031175
    new-instance v0, LX/Dhg;

    const-string v1, "STYLE_TRANSFER"

    invoke-direct {v0, v1, v4}, LX/Dhg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhg;->STYLE_TRANSFER:LX/Dhg;

    .line 2031176
    const/4 v0, 0x3

    new-array v0, v0, [LX/Dhg;

    sget-object v1, LX/Dhg;->MASK:LX/Dhg;

    aput-object v1, v0, v2

    sget-object v1, LX/Dhg;->PARTICLE:LX/Dhg;

    aput-object v1, v0, v3

    sget-object v1, LX/Dhg;->STYLE_TRANSFER:LX/Dhg;

    aput-object v1, v0, v4

    sput-object v0, LX/Dhg;->$VALUES:[LX/Dhg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2031172
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dhg;
    .locals 1

    .prologue
    .line 2031170
    const-class v0, LX/Dhg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dhg;

    return-object v0
.end method

.method public static values()[LX/Dhg;
    .locals 1

    .prologue
    .line 2031171
    sget-object v0, LX/Dhg;->$VALUES:[LX/Dhg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dhg;

    return-object v0
.end method
