.class public final LX/Ecj;
.super LX/EWp;
.source ""

# interfaces
.implements LX/Ech;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ecj;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/Ecj;


# instance fields
.field public bitField0_:I

.field public id_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public privateKey_:LX/EWc;

.field public publicKey_:LX/EWc;

.field public signature_:LX/EWc;

.field public timestamp_:J

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2147895
    new-instance v0, LX/Ecg;

    invoke-direct {v0}, LX/Ecg;-><init>()V

    sput-object v0, LX/Ecj;->a:LX/EWZ;

    .line 2147896
    new-instance v0, LX/Ecj;

    invoke-direct {v0}, LX/Ecj;-><init>()V

    .line 2147897
    sput-object v0, LX/Ecj;->c:LX/Ecj;

    invoke-direct {v0}, LX/Ecj;->A()V

    .line 2147898
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2147899
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2147900
    iput-byte v0, p0, LX/Ecj;->memoizedIsInitialized:B

    .line 2147901
    iput v0, p0, LX/Ecj;->memoizedSerializedSize:I

    .line 2147902
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2147903
    iput-object v0, p0, LX/Ecj;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2147904
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2147905
    iput-byte v0, p0, LX/Ecj;->memoizedIsInitialized:B

    .line 2147906
    iput v0, p0, LX/Ecj;->memoizedSerializedSize:I

    .line 2147907
    invoke-direct {p0}, LX/Ecj;->A()V

    .line 2147908
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2147909
    const/4 v0, 0x0

    .line 2147910
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2147911
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2147912
    sparse-switch v3, :sswitch_data_0

    .line 2147913
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2147914
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2147915
    goto :goto_0

    .line 2147916
    :sswitch_1
    iget v3, p0, LX/Ecj;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/Ecj;->bitField0_:I

    .line 2147917
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/Ecj;->id_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2147918
    :catch_0
    move-exception v0

    .line 2147919
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2147920
    move-object v0, v0

    .line 2147921
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2147922
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/Ecj;->unknownFields:LX/EZQ;

    .line 2147923
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2147924
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/Ecj;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/Ecj;->bitField0_:I

    .line 2147925
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Ecj;->publicKey_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2147926
    :catch_1
    move-exception v0

    .line 2147927
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2147928
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2147929
    move-object v0, v1

    .line 2147930
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2147931
    :sswitch_3
    :try_start_4
    iget v3, p0, LX/Ecj;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, LX/Ecj;->bitField0_:I

    .line 2147932
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Ecj;->privateKey_:LX/EWc;

    goto :goto_0

    .line 2147933
    :sswitch_4
    iget v3, p0, LX/Ecj;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, LX/Ecj;->bitField0_:I

    .line 2147934
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Ecj;->signature_:LX/EWc;

    goto :goto_0

    .line 2147935
    :sswitch_5
    iget v3, p0, LX/Ecj;->bitField0_:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, LX/Ecj;->bitField0_:I

    .line 2147936
    invoke-virtual {p1}, LX/EWd;->g()J

    move-result-wide v4

    iput-wide v4, p0, LX/Ecj;->timestamp_:J
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2147937
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ecj;->unknownFields:LX/EZQ;

    .line 2147938
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2147939
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x29 -> :sswitch_5
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2147948
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2147949
    iput-byte v1, p0, LX/Ecj;->memoizedIsInitialized:B

    .line 2147950
    iput v1, p0, LX/Ecj;->memoizedSerializedSize:I

    .line 2147951
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ecj;->unknownFields:LX/EZQ;

    .line 2147952
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 2147940
    const/4 v0, 0x0

    iput v0, p0, LX/Ecj;->id_:I

    .line 2147941
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ecj;->publicKey_:LX/EWc;

    .line 2147942
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ecj;->privateKey_:LX/EWc;

    .line 2147943
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ecj;->signature_:LX/EWc;

    .line 2147944
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Ecj;->timestamp_:J

    .line 2147945
    return-void
.end method

.method private static a(LX/Ecj;)LX/Eci;
    .locals 1

    .prologue
    .line 2147853
    invoke-static {}, LX/Eci;->u()LX/Eci;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/Eci;->a(LX/Ecj;)LX/Eci;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2147946
    new-instance v0, LX/Eci;

    invoke-direct {v0, p1}, LX/Eci;-><init>(LX/EYd;)V

    .line 2147947
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2147877
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2147878
    iget v0, p0, LX/Ecj;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2147879
    iget v0, p0, LX/Ecj;->id_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2147880
    :cond_0
    iget v0, p0, LX/Ecj;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2147881
    iget-object v0, p0, LX/Ecj;->publicKey_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2147882
    :cond_1
    iget v0, p0, LX/Ecj;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2147883
    const/4 v0, 0x3

    iget-object v1, p0, LX/Ecj;->privateKey_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2147884
    :cond_2
    iget v0, p0, LX/Ecj;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2147885
    iget-object v0, p0, LX/Ecj;->signature_:LX/EWc;

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2147886
    :cond_3
    iget v0, p0, LX/Ecj;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2147887
    const/4 v0, 0x5

    iget-wide v2, p0, LX/Ecj;->timestamp_:J

    invoke-virtual {p1, v0, v2, v3}, LX/EWf;->c(IJ)V

    .line 2147888
    :cond_4
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2147889
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2147890
    iget-byte v1, p0, LX/Ecj;->memoizedIsInitialized:B

    .line 2147891
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2147892
    :goto_0
    return v0

    .line 2147893
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2147894
    :cond_1
    iput-byte v0, p0, LX/Ecj;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2147861
    iget v0, p0, LX/Ecj;->memoizedSerializedSize:I

    .line 2147862
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2147863
    :goto_0
    return v0

    .line 2147864
    :cond_0
    const/4 v0, 0x0

    .line 2147865
    iget v1, p0, LX/Ecj;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2147866
    iget v0, p0, LX/Ecj;->id_:I

    invoke-static {v2, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2147867
    :cond_1
    iget v1, p0, LX/Ecj;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2147868
    iget-object v1, p0, LX/Ecj;->publicKey_:LX/EWc;

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147869
    :cond_2
    iget v1, p0, LX/Ecj;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 2147870
    const/4 v1, 0x3

    iget-object v2, p0, LX/Ecj;->privateKey_:LX/EWc;

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147871
    :cond_3
    iget v1, p0, LX/Ecj;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2147872
    iget-object v1, p0, LX/Ecj;->signature_:LX/EWc;

    invoke-static {v4, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147873
    :cond_4
    iget v1, p0, LX/Ecj;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 2147874
    const/4 v1, 0x5

    iget-wide v2, p0, LX/Ecj;->timestamp_:J

    invoke-static {v1, v2, v3}, LX/EWf;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2147875
    :cond_5
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2147876
    iput v0, p0, LX/Ecj;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2147860
    iget-object v0, p0, LX/Ecj;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2147859
    sget-object v0, LX/Eck;->r:LX/EYn;

    const-class v1, LX/Ecj;

    const-class v2, LX/Eci;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ecj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2147858
    sget-object v0, LX/Ecj;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2147857
    invoke-static {p0}, LX/Ecj;->a(LX/Ecj;)LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2147856
    invoke-static {}, LX/Eci;->u()LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2147855
    invoke-static {p0}, LX/Ecj;->a(LX/Ecj;)LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2147854
    sget-object v0, LX/Ecj;->c:LX/Ecj;

    return-object v0
.end method
