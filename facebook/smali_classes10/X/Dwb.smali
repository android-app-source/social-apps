.class public final LX/Dwb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V
    .locals 0

    .prologue
    .line 2060855
    iput-object p1, p0, LX/Dwb;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2060856
    iget-object v0, p0, LX/Dwb;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->y:LX/03V;

    sget-object v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->L:Ljava/lang/String;

    const-string v2, "Failed on fetch album feedback"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2060857
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 2060858
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2060859
    if-eqz p1, :cond_0

    .line 2060860
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2060861
    if-nez v0, :cond_1

    .line 2060862
    :cond_0
    :goto_0
    return-void

    .line 2060863
    :cond_1
    iget-object v0, p0, LX/Dwb;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {v0}, LX/4Vp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/4Vp;

    move-result-object v1

    .line 2060864
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2060865
    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel;->a()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    move-result-object v0

    .line 2060866
    if-nez v0, :cond_2

    .line 2060867
    const/4 v2, 0x0

    .line 2060868
    :goto_1
    move-object v0, v2

    .line 2060869
    iput-object v0, v1, LX/4Vp;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2060870
    move-object v0, v1

    .line 2060871
    iget-object v1, p0, LX/Dwb;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v0}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    .line 2060872
    iput-object v0, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2060873
    iget-object v0, p0, LX/Dwb;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-static {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m$redex0(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    goto :goto_0

    .line 2060874
    :cond_2
    new-instance v4, LX/3dM;

    invoke-direct {v4}, LX/3dM;-><init>()V

    .line 2060875
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->b()Z

    move-result v2

    .line 2060876
    iput-boolean v2, v4, LX/3dM;->d:Z

    .line 2060877
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->c()Z

    move-result v2

    .line 2060878
    iput-boolean v2, v4, LX/3dM;->e:Z

    .line 2060879
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->d()Z

    move-result v2

    invoke-virtual {v4, v2}, LX/3dM;->c(Z)LX/3dM;

    .line 2060880
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->e()Z

    move-result v2

    .line 2060881
    iput-boolean v2, v4, LX/3dM;->g:Z

    .line 2060882
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->ac_()Z

    move-result v2

    .line 2060883
    iput-boolean v2, v4, LX/3dM;->h:Z

    .line 2060884
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->ad_()Z

    move-result v2

    .line 2060885
    iput-boolean v2, v4, LX/3dM;->i:Z

    .line 2060886
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->j()Z

    move-result v2

    invoke-virtual {v4, v2}, LX/3dM;->g(Z)LX/3dM;

    .line 2060887
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->s()Z

    move-result v2

    .line 2060888
    iput-boolean v2, v4, LX/3dM;->k:Z

    .line 2060889
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->k()Z

    move-result v2

    .line 2060890
    iput-boolean v2, v4, LX/3dM;->l:Z

    .line 2060891
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->t()LX/175;

    move-result-object v2

    .line 2060892
    if-nez v2, :cond_5

    .line 2060893
    const/4 v3, 0x0

    .line 2060894
    :goto_2
    move-object v2, v3

    .line 2060895
    iput-object v2, v4, LX/3dM;->o:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2060896
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2060897
    iput-object v2, v4, LX/3dM;->p:Ljava/lang/String;

    .line 2060898
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->u()I

    move-result v2

    .line 2060899
    iput v2, v4, LX/3dM;->q:I

    .line 2060900
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->v()Ljava/lang/String;

    move-result-object v2

    .line 2060901
    iput-object v2, v4, LX/3dM;->r:Ljava/lang/String;

    .line 2060902
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->m()Z

    move-result v2

    invoke-virtual {v4, v2}, LX/3dM;->j(Z)LX/3dM;

    .line 2060903
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->w()Z

    move-result v2

    invoke-virtual {v4, v2}, LX/3dM;->k(Z)LX/3dM;

    .line 2060904
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->n()Ljava/lang/String;

    move-result-object v2

    .line 2060905
    iput-object v2, v4, LX/3dM;->y:Ljava/lang/String;

    .line 2060906
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v2

    .line 2060907
    if-nez v2, :cond_a

    .line 2060908
    const/4 v3, 0x0

    .line 2060909
    :goto_3
    move-object v2, v3

    .line 2060910
    iput-object v2, v4, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 2060911
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->o()Z

    move-result v2

    invoke-virtual {v4, v2}, LX/3dM;->l(Z)LX/3dM;

    .line 2060912
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->p()Ljava/lang/String;

    move-result-object v2

    .line 2060913
    iput-object v2, v4, LX/3dM;->D:Ljava/lang/String;

    .line 2060914
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->y()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    move-result-object v2

    .line 2060915
    if-nez v2, :cond_e

    .line 2060916
    const/4 v3, 0x0

    .line 2060917
    :goto_4
    move-object v2, v3

    .line 2060918
    invoke-virtual {v4, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 2060919
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v2

    .line 2060920
    if-nez v2, :cond_f

    .line 2060921
    const/4 v3, 0x0

    .line 2060922
    :goto_5
    move-object v2, v3

    .line 2060923
    invoke-virtual {v4, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 2060924
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->q()Ljava/lang/String;

    move-result-object v2

    .line 2060925
    iput-object v2, v4, LX/3dM;->J:Ljava/lang/String;

    .line 2060926
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->A()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    move-result-object v2

    .line 2060927
    if-nez v2, :cond_16

    .line 2060928
    const/4 v3, 0x0

    .line 2060929
    :goto_6
    move-object v2, v3

    .line 2060930
    iput-object v2, v4, LX/3dM;->K:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 2060931
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->B()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    move-result-object v2

    .line 2060932
    if-nez v2, :cond_17

    .line 2060933
    const/4 v3, 0x0

    .line 2060934
    :goto_7
    move-object v2, v3

    .line 2060935
    iput-object v2, v4, LX/3dM;->L:Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    .line 2060936
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->C()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2060937
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2060938
    const/4 v2, 0x0

    move v3, v2

    :goto_8
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->C()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    .line 2060939
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->C()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    .line 2060940
    if-nez v2, :cond_18

    .line 2060941
    const/4 v6, 0x0

    .line 2060942
    :goto_9
    move-object v2, v6

    .line 2060943
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2060944
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_8

    .line 2060945
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2060946
    iput-object v2, v4, LX/3dM;->N:LX/0Px;

    .line 2060947
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->D()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v2

    .line 2060948
    if-nez v2, :cond_19

    .line 2060949
    const/4 v3, 0x0

    .line 2060950
    :goto_a
    move-object v2, v3

    .line 2060951
    invoke-virtual {v4, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 2060952
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    .line 2060953
    if-nez v2, :cond_1b

    .line 2060954
    const/4 v3, 0x0

    .line 2060955
    :goto_b
    move-object v2, v3

    .line 2060956
    invoke-virtual {v4, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 2060957
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->E()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    move-result-object v2

    .line 2060958
    if-nez v2, :cond_20

    .line 2060959
    const/4 v3, 0x0

    .line 2060960
    :goto_c
    move-object v2, v3

    .line 2060961
    iput-object v2, v4, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 2060962
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->F()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    .line 2060963
    if-nez v2, :cond_22

    .line 2060964
    const/4 v3, 0x0

    .line 2060965
    :goto_d
    move-object v2, v3

    .line 2060966
    iput-object v2, v4, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 2060967
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->G()I

    move-result v2

    invoke-virtual {v4, v2}, LX/3dM;->b(I)LX/3dM;

    .line 2060968
    invoke-virtual {v4}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    goto/16 :goto_1

    .line 2060969
    :cond_5
    new-instance v6, LX/173;

    invoke-direct {v6}, LX/173;-><init>()V

    .line 2060970
    invoke-interface {v2}, LX/175;->b()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 2060971
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2060972
    const/4 v3, 0x0

    move v5, v3

    :goto_e
    invoke-interface {v2}, LX/175;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_6

    .line 2060973
    invoke-interface {v2}, LX/175;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1W5;

    .line 2060974
    if-nez v3, :cond_8

    .line 2060975
    const/4 v8, 0x0

    .line 2060976
    :goto_f
    move-object v3, v8

    .line 2060977
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2060978
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_e

    .line 2060979
    :cond_6
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2060980
    iput-object v3, v6, LX/173;->e:LX/0Px;

    .line 2060981
    :cond_7
    invoke-interface {v2}, LX/175;->a()Ljava/lang/String;

    move-result-object v3

    .line 2060982
    iput-object v3, v6, LX/173;->f:Ljava/lang/String;

    .line 2060983
    invoke-virtual {v6}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    goto/16 :goto_2

    .line 2060984
    :cond_8
    new-instance v8, LX/4W6;

    invoke-direct {v8}, LX/4W6;-><init>()V

    .line 2060985
    invoke-interface {v3}, LX/1W5;->a()LX/171;

    move-result-object v9

    .line 2060986
    if-nez v9, :cond_9

    .line 2060987
    const/4 v10, 0x0

    .line 2060988
    :goto_10
    move-object v9, v10

    .line 2060989
    iput-object v9, v8, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 2060990
    invoke-interface {v3}, LX/1W5;->b()I

    move-result v9

    .line 2060991
    iput v9, v8, LX/4W6;->c:I

    .line 2060992
    invoke-interface {v3}, LX/1W5;->c()I

    move-result v9

    .line 2060993
    iput v9, v8, LX/4W6;->d:I

    .line 2060994
    invoke-virtual {v8}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v8

    goto :goto_f

    .line 2060995
    :cond_9
    new-instance v10, LX/170;

    invoke-direct {v10}, LX/170;-><init>()V

    .line 2060996
    invoke-interface {v9}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    .line 2060997
    iput-object v11, v10, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2060998
    invoke-interface {v9}, LX/171;->c()LX/0Px;

    move-result-object v11

    .line 2060999
    iput-object v11, v10, LX/170;->b:LX/0Px;

    .line 2061000
    invoke-interface {v9}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v11

    .line 2061001
    iput-object v11, v10, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 2061002
    invoke-interface {v9}, LX/171;->e()Ljava/lang/String;

    move-result-object v11

    .line 2061003
    iput-object v11, v10, LX/170;->o:Ljava/lang/String;

    .line 2061004
    invoke-interface {v9}, LX/171;->v_()Ljava/lang/String;

    move-result-object v11

    .line 2061005
    iput-object v11, v10, LX/170;->A:Ljava/lang/String;

    .line 2061006
    invoke-interface {v9}, LX/171;->w_()Ljava/lang/String;

    move-result-object v11

    .line 2061007
    iput-object v11, v10, LX/170;->X:Ljava/lang/String;

    .line 2061008
    invoke-interface {v9}, LX/171;->j()Ljava/lang/String;

    move-result-object v11

    .line 2061009
    iput-object v11, v10, LX/170;->Y:Ljava/lang/String;

    .line 2061010
    invoke-virtual {v10}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v10

    goto :goto_10

    .line 2061011
    :cond_a
    new-instance v6, LX/4Wx;

    invoke-direct {v6}, LX/4Wx;-><init>()V

    .line 2061012
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 2061013
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2061014
    const/4 v3, 0x0

    move v5, v3

    :goto_11
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_b

    .line 2061015
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;

    .line 2061016
    if-nez v3, :cond_d

    .line 2061017
    const/4 v8, 0x0

    .line 2061018
    :goto_12
    move-object v3, v8

    .line 2061019
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2061020
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_11

    .line 2061021
    :cond_b
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2061022
    iput-object v3, v6, LX/4Wx;->b:LX/0Px;

    .line 2061023
    :cond_c
    invoke-virtual {v6}, LX/4Wx;->a()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v3

    goto/16 :goto_3

    .line 2061024
    :cond_d
    new-instance v8, LX/3dL;

    invoke-direct {v8}, LX/3dL;-><init>()V

    .line 2061025
    invoke-virtual {v3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    .line 2061026
    iput-object v9, v8, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2061027
    invoke-virtual {v3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v9

    .line 2061028
    iput-object v9, v8, LX/3dL;->ag:Ljava/lang/String;

    .line 2061029
    invoke-virtual {v8}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v8

    goto :goto_12

    .line 2061030
    :cond_e
    new-instance v3, LX/3dN;

    invoke-direct {v3}, LX/3dN;-><init>()V

    .line 2061031
    invoke-virtual {v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;->a()I

    move-result v5

    .line 2061032
    iput v5, v3, LX/3dN;->b:I

    .line 2061033
    invoke-virtual {v3}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v3

    goto/16 :goto_4

    .line 2061034
    :cond_f
    new-instance v6, LX/3dO;

    invoke-direct {v6}, LX/3dO;-><init>()V

    .line 2061035
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v3

    .line 2061036
    iput v3, v6, LX/3dO;->b:I

    .line 2061037
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->b()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_11

    .line 2061038
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2061039
    const/4 v3, 0x0

    move v5, v3

    :goto_13
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_10

    .line 2061040
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;

    .line 2061041
    if-nez v3, :cond_12

    .line 2061042
    const/4 v8, 0x0

    .line 2061043
    :goto_14
    move-object v3, v8

    .line 2061044
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2061045
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_13

    .line 2061046
    :cond_10
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2061047
    iput-object v3, v6, LX/3dO;->c:LX/0Px;

    .line 2061048
    :cond_11
    invoke-virtual {v6}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v3

    goto/16 :goto_5

    .line 2061049
    :cond_12
    new-instance v8, LX/4YX;

    invoke-direct {v8}, LX/4YX;-><init>()V

    .line 2061050
    invoke-virtual {v3}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->a()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;

    move-result-object v9

    .line 2061051
    if-nez v9, :cond_13

    .line 2061052
    const/4 v10, 0x0

    .line 2061053
    :goto_15
    move-object v9, v10

    .line 2061054
    iput-object v9, v8, LX/4YX;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 2061055
    invoke-virtual {v3}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel;->b()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;

    move-result-object v9

    .line 2061056
    if-nez v9, :cond_14

    .line 2061057
    const/4 v10, 0x0

    .line 2061058
    :goto_16
    move-object v9, v10

    .line 2061059
    iput-object v9, v8, LX/4YX;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2061060
    invoke-virtual {v8}, LX/4YX;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;

    move-result-object v8

    goto :goto_14

    .line 2061061
    :cond_13
    new-instance v10, LX/4WM;

    invoke-direct {v10}, LX/4WM;-><init>()V

    .line 2061062
    invoke-virtual {v9}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$FeedbackReactionInfoModel;->a()I

    move-result v11

    .line 2061063
    iput v11, v10, LX/4WM;->f:I

    .line 2061064
    invoke-virtual {v10}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v10

    goto :goto_15

    .line 2061065
    :cond_14
    new-instance v10, LX/3dL;

    invoke-direct {v10}, LX/3dL;-><init>()V

    .line 2061066
    invoke-virtual {v9}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    .line 2061067
    iput-object v11, v10, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2061068
    invoke-virtual {v9}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v11

    .line 2061069
    iput-object v11, v10, LX/3dL;->E:Ljava/lang/String;

    .line 2061070
    invoke-virtual {v9}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$ProfilePictureReactorsEdgeFieldsModel$NodeModel;->d()Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultProfilePictureModel;

    move-result-object v11

    .line 2061071
    if-nez v11, :cond_15

    .line 2061072
    const/4 v3, 0x0

    .line 2061073
    :goto_17
    move-object v11, v3

    .line 2061074
    iput-object v11, v10, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2061075
    invoke-virtual {v10}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v10

    goto :goto_16

    .line 2061076
    :cond_15
    new-instance v3, LX/2dc;

    invoke-direct {v3}, LX/2dc;-><init>()V

    .line 2061077
    invoke-virtual {v11}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultProfilePictureModel;->a()I

    move-result v9

    .line 2061078
    iput v9, v3, LX/2dc;->c:I

    .line 2061079
    invoke-virtual {v11}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultProfilePictureModel;->b()Ljava/lang/String;

    move-result-object v9

    .line 2061080
    iput-object v9, v3, LX/2dc;->h:Ljava/lang/String;

    .line 2061081
    invoke-virtual {v11}, Lcom/facebook/api/graphql/reactions/FetchReactorsGraphQLModels$DefaultProfilePictureModel;->c()I

    move-result v9

    .line 2061082
    iput v9, v3, LX/2dc;->i:I

    .line 2061083
    invoke-virtual {v3}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    goto :goto_17

    .line 2061084
    :cond_16
    new-instance v3, LX/4Ya;

    invoke-direct {v3}, LX/4Ya;-><init>()V

    .line 2061085
    invoke-virtual {v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;->a()I

    move-result v5

    .line 2061086
    iput v5, v3, LX/4Ya;->b:I

    .line 2061087
    invoke-virtual {v3}, LX/4Ya;->a()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v3

    goto/16 :goto_6

    .line 2061088
    :cond_17
    new-instance v3, LX/4Yj;

    invoke-direct {v3}, LX/4Yj;-><init>()V

    .line 2061089
    invoke-virtual {v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;->a()I

    move-result v5

    .line 2061090
    iput v5, v3, LX/4Yj;->b:I

    .line 2061091
    invoke-virtual {v3}, LX/4Yj;->a()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v3

    goto/16 :goto_7

    .line 2061092
    :cond_18
    new-instance v6, LX/4WL;

    invoke-direct {v6}, LX/4WL;-><init>()V

    .line 2061093
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;->a()I

    move-result v7

    .line 2061094
    iput v7, v6, LX/4WL;->b:I

    .line 2061095
    invoke-virtual {v6}, LX/4WL;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v6

    goto/16 :goto_9

    .line 2061096
    :cond_19
    new-instance v3, LX/4ZH;

    invoke-direct {v3}, LX/4ZH;-><init>()V

    .line 2061097
    invoke-virtual {v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a()I

    move-result v5

    .line 2061098
    iput v5, v3, LX/4ZH;->b:I

    .line 2061099
    invoke-virtual {v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->b()LX/0Px;

    move-result-object v5

    .line 2061100
    iput-object v5, v3, LX/4ZH;->c:LX/0Px;

    .line 2061101
    invoke-virtual {v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->c()LX/0us;

    move-result-object v5

    .line 2061102
    if-nez v5, :cond_1a

    .line 2061103
    const/4 v6, 0x0

    .line 2061104
    :goto_18
    move-object v5, v6

    .line 2061105
    iput-object v5, v3, LX/4ZH;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2061106
    invoke-virtual {v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->d()I

    move-result v5

    .line 2061107
    iput v5, v3, LX/4ZH;->e:I

    .line 2061108
    invoke-virtual {v3}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v3

    goto/16 :goto_a

    .line 2061109
    :cond_1a
    new-instance v6, LX/17L;

    invoke-direct {v6}, LX/17L;-><init>()V

    .line 2061110
    invoke-interface {v5}, LX/0us;->a()Ljava/lang/String;

    move-result-object v7

    .line 2061111
    iput-object v7, v6, LX/17L;->c:Ljava/lang/String;

    .line 2061112
    invoke-interface {v5}, LX/0us;->b()Z

    move-result v7

    .line 2061113
    iput-boolean v7, v6, LX/17L;->d:Z

    .line 2061114
    invoke-interface {v5}, LX/0us;->c()Z

    move-result v7

    .line 2061115
    iput-boolean v7, v6, LX/17L;->e:Z

    .line 2061116
    invoke-interface {v5}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v7

    .line 2061117
    iput-object v7, v6, LX/17L;->f:Ljava/lang/String;

    .line 2061118
    invoke-virtual {v6}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v6

    goto :goto_18

    .line 2061119
    :cond_1b
    new-instance v6, LX/3dQ;

    invoke-direct {v6}, LX/3dQ;-><init>()V

    .line 2061120
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_1d

    .line 2061121
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2061122
    const/4 v3, 0x0

    move v5, v3

    :goto_19
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_1c

    .line 2061123
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    .line 2061124
    if-nez v3, :cond_1e

    .line 2061125
    const/4 v8, 0x0

    .line 2061126
    :goto_1a
    move-object v3, v8

    .line 2061127
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2061128
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_19

    .line 2061129
    :cond_1c
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2061130
    iput-object v3, v6, LX/3dQ;->b:LX/0Px;

    .line 2061131
    :cond_1d
    invoke-virtual {v6}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v3

    goto/16 :goto_b

    .line 2061132
    :cond_1e
    new-instance v8, LX/4ZJ;

    invoke-direct {v8}, LX/4ZJ;-><init>()V

    .line 2061133
    invoke-virtual {v3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    .line 2061134
    if-nez v9, :cond_1f

    .line 2061135
    const/4 v11, 0x0

    .line 2061136
    :goto_1b
    move-object v9, v11

    .line 2061137
    iput-object v9, v8, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 2061138
    invoke-virtual {v3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->b()I

    move-result v9

    .line 2061139
    iput v9, v8, LX/4ZJ;->c:I

    .line 2061140
    invoke-virtual {v8}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v8

    goto :goto_1a

    .line 2061141
    :cond_1f
    new-instance v11, LX/4WM;

    invoke-direct {v11}, LX/4WM;-><init>()V

    .line 2061142
    const/4 p1, 0x0

    invoke-virtual {v10, v9, p1}, LX/15i;->j(II)I

    move-result p1

    .line 2061143
    iput p1, v11, LX/4WM;->f:I

    .line 2061144
    invoke-virtual {v11}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v11

    goto :goto_1b

    .line 2061145
    :cond_20
    new-instance v3, LX/4XY;

    invoke-direct {v3}, LX/4XY;-><init>()V

    .line 2061146
    invoke-virtual {v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 2061147
    iput-object v5, v3, LX/4XY;->ag:Ljava/lang/String;

    .line 2061148
    invoke-virtual {v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2061149
    iput-object v5, v3, LX/4XY;->aT:Ljava/lang/String;

    .line 2061150
    invoke-virtual {v2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;->d()LX/1Fb;

    move-result-object v5

    .line 2061151
    if-nez v5, :cond_21

    .line 2061152
    const/4 v6, 0x0

    .line 2061153
    :goto_1c
    move-object v5, v6

    .line 2061154
    iput-object v5, v3, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2061155
    invoke-virtual {v3}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    goto/16 :goto_c

    .line 2061156
    :cond_21
    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    .line 2061157
    invoke-interface {v5}, LX/1Fb;->a()I

    move-result v2

    .line 2061158
    iput v2, v6, LX/2dc;->c:I

    .line 2061159
    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    .line 2061160
    iput-object v2, v6, LX/2dc;->h:Ljava/lang/String;

    .line 2061161
    invoke-interface {v5}, LX/1Fb;->c()I

    move-result v2

    .line 2061162
    iput v2, v6, LX/2dc;->i:I

    .line 2061163
    invoke-virtual {v6}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    goto :goto_1c

    .line 2061164
    :cond_22
    new-instance v3, LX/33O;

    invoke-direct {v3}, LX/33O;-><init>()V

    .line 2061165
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2061166
    iput-object v5, v3, LX/33O;->aI:Ljava/lang/String;

    .line 2061167
    invoke-virtual {v3}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    goto/16 :goto_d
.end method
