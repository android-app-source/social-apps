.class public final LX/EsS;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/EsS;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EsQ;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/EsT;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2175669
    const/4 v0, 0x0

    sput-object v0, LX/EsS;->a:LX/EsS;

    .line 2175670
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EsS;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2175671
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2175672
    new-instance v0, LX/EsT;

    invoke-direct {v0}, LX/EsT;-><init>()V

    iput-object v0, p0, LX/EsS;->c:LX/EsT;

    .line 2175673
    return-void
.end method

.method public static declared-synchronized q()LX/EsS;
    .locals 2

    .prologue
    .line 2175674
    const-class v1, LX/EsS;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/EsS;->a:LX/EsS;

    if-nez v0, :cond_0

    .line 2175675
    new-instance v0, LX/EsS;

    invoke-direct {v0}, LX/EsS;-><init>()V

    sput-object v0, LX/EsS;->a:LX/EsS;

    .line 2175676
    :cond_0
    sget-object v0, LX/EsS;->a:LX/EsS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2175677
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2175678
    check-cast p2, LX/EsR;

    .line 2175679
    iget-object v0, p2, LX/EsR;->a:Ljava/lang/String;

    const/4 p2, 0x2

    const/4 v4, 0x1

    const/4 p0, 0x0

    .line 2175680
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v4}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f021932

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    const v3, 0x7f02192c

    invoke-virtual {v2, v3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b1cf3

    invoke-interface {v2, p0, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b1cf8

    invoke-interface {v2, v4, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b1cf8

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, LX/1ne;->g(F)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b1cf3

    invoke-interface {v2, p0, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b1cf9

    invoke-interface {v2, p2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x7

    const v4, 0x7f0b1cf3

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2175681
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2175682
    invoke-static {}, LX/1dS;->b()V

    .line 2175683
    const/4 v0, 0x0

    return-object v0
.end method
