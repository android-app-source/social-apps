.class public final LX/DjV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DjW;


# direct methods
.method public constructor <init>(LX/DjW;)V
    .locals 0

    .prologue
    .line 2033312
    iput-object p1, p0, LX/DjV;->a:LX/DjW;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2033313
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2033314
    iget-object v0, p0, LX/DjV;->a:LX/DjW;

    iget-object v0, v0, LX/DjW;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2033315
    iget-object v0, p0, LX/DjV;->a:LX/DjW;

    iget-object v0, v0, LX/DjW;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/RejectAppointmentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2033316
    return-void
.end method
