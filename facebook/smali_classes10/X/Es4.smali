.class public LX/Es4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;
.implements LX/0jQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/17z",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/0jQ;"
    }
.end annotation


# instance fields
.field public final a:F

.field public final b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

.field public final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1X9;

.field public final e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1X9;F)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1X9;",
            "F)V"
        }
    .end annotation

    .prologue
    .line 2174302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2174303
    iput-object p1, p0, LX/Es4;->b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    .line 2174304
    iput-object p2, p0, LX/Es4;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174305
    iput-object p3, p0, LX/Es4;->d:LX/1X9;

    .line 2174306
    iput p4, p0, LX/Es4;->a:F

    .line 2174307
    iget-object v0, p0, LX/Es4;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/Es4;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174308
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 2174299
    iget-object v0, p0, LX/Es4;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Es4;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174300
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 2174301
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method

.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2174298
    iget-object v0, p0, LX/Es4;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method
