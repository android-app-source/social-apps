.class public final LX/DzT;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0am",
        "<",
        "Landroid/location/Location;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 0

    .prologue
    .line 2067050
    iput-object p1, p0, LX/DzT;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2067051
    iget-object v0, p0, LX/DzT;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 2067052
    iput-object v1, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u:LX/0am;

    .line 2067053
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2067054
    check-cast p1, LX/0am;

    .line 2067055
    iget-object v0, p0, LX/DzT;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    .line 2067056
    iput-object p1, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->v:LX/0am;

    .line 2067057
    iget-object v0, p0, LX/DzT;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-static {v0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067058
    iget-object v0, p0, LX/DzT;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 2067059
    iput-object v1, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u:LX/0am;

    .line 2067060
    return-void
.end method
