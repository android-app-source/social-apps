.class public LX/EkK;
.super LX/2g7;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/upload/annotation/IsContactsUploadBackgroundTaskEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2163305
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 2163306
    iput-object p1, p0, LX/EkK;->a:Ljava/lang/String;

    .line 2163307
    iput-object p2, p0, LX/EkK;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2163308
    iput-object p3, p0, LX/EkK;->c:LX/0Or;

    .line 2163309
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 3
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2163310
    iget-object v0, p0, LX/EkK;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2163311
    :goto_0
    return v0

    .line 2163312
    :cond_0
    iget-object v0, p0, LX/EkK;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/EkK;->a:Ljava/lang/String;

    invoke-static {v2}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2163313
    iget-object v2, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
