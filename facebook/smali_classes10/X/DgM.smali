.class public final LX/DgM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3rk;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;)V
    .locals 0

    .prologue
    .line 2029336
    iput-object p1, p0, LX/DgM;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2029337
    iget-object v0, p0, LX/DgM;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    .line 2029338
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->w:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    if-eqz v1, :cond_0

    .line 2029339
    :goto_0
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iget-object p0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->v:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    invoke-virtual {v1, p0}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    iget-object p0, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->w:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    invoke-virtual {v1, p0}, LX/0hH;->c(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2029340
    const/4 v0, 0x1

    return v0

    .line 2029341
    :cond_0
    new-instance v1, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;-><init>()V

    .line 2029342
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object p0

    invoke-virtual {p0}, LX/0gc;->a()LX/0hH;

    move-result-object p0

    const p1, 0x7f0d19ee

    invoke-virtual {p0, p1, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2029343
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->b()Z

    goto :goto_0
.end method

.method public final b(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 2029344
    iget-object v0, p0, LX/DgM;->a:Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;

    .line 2029345
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object p0

    invoke-virtual {p0}, LX/0gc;->a()LX/0hH;

    move-result-object p0

    iget-object p1, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->w:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    invoke-virtual {p0, p1}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object p0

    iget-object p1, v0, Lcom/facebook/messaging/location/sending/LocationSendingDialogFragment;->v:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    invoke-virtual {p0, p1}, LX/0hH;->c(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object p0

    invoke-virtual {p0}, LX/0hH;->b()I

    .line 2029346
    const/4 v0, 0x1

    return v0
.end method
