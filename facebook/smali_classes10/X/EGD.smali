.class public final enum LX/EGD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EGD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EGD;

.field public static final enum CONNECTED:LX/EGD;

.field public static final enum CONNECTING:LX/EGD;

.field public static final enum CONNECTION_DROPPED:LX/EGD;

.field public static final enum CONTACTING:LX/EGD;

.field public static final enum DISCONNECTED:LX/EGD;

.field public static final enum IN_ANOTHER_CALL:LX/EGD;

.field public static final enum NO_ANSWER:LX/EGD;

.field public static final enum PARTICIPANT_LIMIT_REACHED:LX/EGD;

.field public static final enum REJECTED:LX/EGD;

.field public static final enum RINGING:LX/EGD;

.field public static final enum UNKNOWN:LX/EGD;

.field public static final enum UNREACHABLE:LX/EGD;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2096861
    new-instance v0, LX/EGD;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->UNKNOWN:LX/EGD;

    .line 2096862
    new-instance v0, LX/EGD;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v4}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->DISCONNECTED:LX/EGD;

    .line 2096863
    new-instance v0, LX/EGD;

    const-string v1, "NO_ANSWER"

    invoke-direct {v0, v1, v5}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->NO_ANSWER:LX/EGD;

    .line 2096864
    new-instance v0, LX/EGD;

    const-string v1, "REJECTED"

    invoke-direct {v0, v1, v6}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->REJECTED:LX/EGD;

    .line 2096865
    new-instance v0, LX/EGD;

    const-string v1, "UNREACHABLE"

    invoke-direct {v0, v1, v7}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->UNREACHABLE:LX/EGD;

    .line 2096866
    new-instance v0, LX/EGD;

    const-string v1, "CONNECTION_DROPPED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->CONNECTION_DROPPED:LX/EGD;

    .line 2096867
    new-instance v0, LX/EGD;

    const-string v1, "CONTACTING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->CONTACTING:LX/EGD;

    .line 2096868
    new-instance v0, LX/EGD;

    const-string v1, "RINGING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->RINGING:LX/EGD;

    .line 2096869
    new-instance v0, LX/EGD;

    const-string v1, "CONNECTING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->CONNECTING:LX/EGD;

    .line 2096870
    new-instance v0, LX/EGD;

    const-string v1, "CONNECTED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->CONNECTED:LX/EGD;

    .line 2096871
    new-instance v0, LX/EGD;

    const-string v1, "PARTICIPANT_LIMIT_REACHED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->PARTICIPANT_LIMIT_REACHED:LX/EGD;

    .line 2096872
    new-instance v0, LX/EGD;

    const-string v1, "IN_ANOTHER_CALL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/EGD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EGD;->IN_ANOTHER_CALL:LX/EGD;

    .line 2096873
    const/16 v0, 0xc

    new-array v0, v0, [LX/EGD;

    sget-object v1, LX/EGD;->UNKNOWN:LX/EGD;

    aput-object v1, v0, v3

    sget-object v1, LX/EGD;->DISCONNECTED:LX/EGD;

    aput-object v1, v0, v4

    sget-object v1, LX/EGD;->NO_ANSWER:LX/EGD;

    aput-object v1, v0, v5

    sget-object v1, LX/EGD;->REJECTED:LX/EGD;

    aput-object v1, v0, v6

    sget-object v1, LX/EGD;->UNREACHABLE:LX/EGD;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EGD;->CONNECTION_DROPPED:LX/EGD;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EGD;->CONTACTING:LX/EGD;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EGD;->RINGING:LX/EGD;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EGD;->CONNECTING:LX/EGD;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/EGD;->CONNECTED:LX/EGD;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/EGD;->PARTICIPANT_LIMIT_REACHED:LX/EGD;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/EGD;->IN_ANOTHER_CALL:LX/EGD;

    aput-object v2, v0, v1

    sput-object v0, LX/EGD;->$VALUES:[LX/EGD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2096874
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EGD;
    .locals 1

    .prologue
    .line 2096875
    const-class v0, LX/EGD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EGD;

    return-object v0
.end method

.method public static values()[LX/EGD;
    .locals 1

    .prologue
    .line 2096876
    sget-object v0, LX/EGD;->$VALUES:[LX/EGD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EGD;

    return-object v0
.end method
