.class public final enum LX/CiM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CiM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CiM;

.field public static final enum SCROLL_FOCUSED_VIEW_TO_RECT:LX/CiM;

.field public static final enum SET_FOCUSED_VIEW:LX/CiM;

.field public static final enum UNSET_FOCUSED_VIEW:LX/CiM;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1928769
    new-instance v0, LX/CiM;

    const-string v1, "SET_FOCUSED_VIEW"

    invoke-direct {v0, v1, v2}, LX/CiM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiM;->SET_FOCUSED_VIEW:LX/CiM;

    .line 1928770
    new-instance v0, LX/CiM;

    const-string v1, "UNSET_FOCUSED_VIEW"

    invoke-direct {v0, v1, v3}, LX/CiM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiM;->UNSET_FOCUSED_VIEW:LX/CiM;

    .line 1928771
    new-instance v0, LX/CiM;

    const-string v1, "SCROLL_FOCUSED_VIEW_TO_RECT"

    invoke-direct {v0, v1, v4}, LX/CiM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CiM;->SCROLL_FOCUSED_VIEW_TO_RECT:LX/CiM;

    .line 1928772
    const/4 v0, 0x3

    new-array v0, v0, [LX/CiM;

    sget-object v1, LX/CiM;->SET_FOCUSED_VIEW:LX/CiM;

    aput-object v1, v0, v2

    sget-object v1, LX/CiM;->UNSET_FOCUSED_VIEW:LX/CiM;

    aput-object v1, v0, v3

    sget-object v1, LX/CiM;->SCROLL_FOCUSED_VIEW_TO_RECT:LX/CiM;

    aput-object v1, v0, v4

    sput-object v0, LX/CiM;->$VALUES:[LX/CiM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1928766
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CiM;
    .locals 1

    .prologue
    .line 1928768
    const-class v0, LX/CiM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CiM;

    return-object v0
.end method

.method public static values()[LX/CiM;
    .locals 1

    .prologue
    .line 1928767
    sget-object v0, LX/CiM;->$VALUES:[LX/CiM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CiM;

    return-object v0
.end method
