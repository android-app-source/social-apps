.class public final enum LX/EI8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EI8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EI8;

.field public static final enum BOTH:LX/EI8;

.field public static final enum END_CALL_STATE:LX/EI8;

.field public static final enum END_CALL_STATE_VOICEMAIL:LX/EI8;

.field public static final enum END_CALL_STATE_WITH_RETRY:LX/EI8;

.field public static final enum GROUP_COUNTDOWN:LX/EI8;

.field public static final enum HIDDEN:LX/EI8;

.field public static final enum INCOMING_INSTANT:LX/EI8;

.field public static final enum NONE:LX/EI8;

.field public static final enum OUTGOING_INSTANT:LX/EI8;

.field public static final enum PEER:LX/EI8;

.field public static final enum SELF:LX/EI8;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2100598
    new-instance v0, LX/EI8;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->NONE:LX/EI8;

    .line 2100599
    new-instance v0, LX/EI8;

    const-string v1, "SELF"

    invoke-direct {v0, v1, v4}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->SELF:LX/EI8;

    .line 2100600
    new-instance v0, LX/EI8;

    const-string v1, "PEER"

    invoke-direct {v0, v1, v5}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->PEER:LX/EI8;

    .line 2100601
    new-instance v0, LX/EI8;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v6}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->BOTH:LX/EI8;

    .line 2100602
    new-instance v0, LX/EI8;

    const-string v1, "GROUP_COUNTDOWN"

    invoke-direct {v0, v1, v7}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->GROUP_COUNTDOWN:LX/EI8;

    .line 2100603
    new-instance v0, LX/EI8;

    const-string v1, "INCOMING_INSTANT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->INCOMING_INSTANT:LX/EI8;

    .line 2100604
    new-instance v0, LX/EI8;

    const-string v1, "OUTGOING_INSTANT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->OUTGOING_INSTANT:LX/EI8;

    .line 2100605
    new-instance v0, LX/EI8;

    const-string v1, "END_CALL_STATE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->END_CALL_STATE:LX/EI8;

    .line 2100606
    new-instance v0, LX/EI8;

    const-string v1, "END_CALL_STATE_WITH_RETRY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->END_CALL_STATE_WITH_RETRY:LX/EI8;

    .line 2100607
    new-instance v0, LX/EI8;

    const-string v1, "END_CALL_STATE_VOICEMAIL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->END_CALL_STATE_VOICEMAIL:LX/EI8;

    .line 2100608
    new-instance v0, LX/EI8;

    const-string v1, "HIDDEN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/EI8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI8;->HIDDEN:LX/EI8;

    .line 2100609
    const/16 v0, 0xb

    new-array v0, v0, [LX/EI8;

    sget-object v1, LX/EI8;->NONE:LX/EI8;

    aput-object v1, v0, v3

    sget-object v1, LX/EI8;->SELF:LX/EI8;

    aput-object v1, v0, v4

    sget-object v1, LX/EI8;->PEER:LX/EI8;

    aput-object v1, v0, v5

    sget-object v1, LX/EI8;->BOTH:LX/EI8;

    aput-object v1, v0, v6

    sget-object v1, LX/EI8;->GROUP_COUNTDOWN:LX/EI8;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EI8;->INCOMING_INSTANT:LX/EI8;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EI8;->OUTGOING_INSTANT:LX/EI8;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EI8;->END_CALL_STATE:LX/EI8;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EI8;->END_CALL_STATE_WITH_RETRY:LX/EI8;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/EI8;->END_CALL_STATE_VOICEMAIL:LX/EI8;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/EI8;->HIDDEN:LX/EI8;

    aput-object v2, v0, v1

    sput-object v0, LX/EI8;->$VALUES:[LX/EI8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2100610
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EI8;
    .locals 1

    .prologue
    .line 2100611
    const-class v0, LX/EI8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EI8;

    return-object v0
.end method

.method public static values()[LX/EI8;
    .locals 1

    .prologue
    .line 2100612
    sget-object v0, LX/EI8;->$VALUES:[LX/EI8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EI8;

    return-object v0
.end method
