.class public final enum LX/DoN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DoN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DoN;

.field public static final enum ACCEPT_GEN_ERROR:LX/DoN;

.field public static final enum ACCEPT_PROCESS_ERROR:LX/DoN;

.field public static final enum DECRYPT_ERROR:LX/DoN;

.field public static final enum NEW:LX/DoN;

.field public static final enum PRE_KEY_ERROR:LX/DoN;

.field public static final enum RECEIVER_ACCEPT_PENDING:LX/DoN;

.field public static final enum RUNNING:LX/DoN;

.field public static final enum SENDER_ACCEPT_PENDING:LX/DoN;

.field private static final sStateMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/DoN;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2041673
    new-instance v1, LX/DoN;

    const-string v2, "NEW"

    invoke-direct {v1, v2, v0, v0}, LX/DoN;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DoN;->NEW:LX/DoN;

    .line 2041674
    new-instance v1, LX/DoN;

    const-string v2, "SENDER_ACCEPT_PENDING"

    invoke-direct {v1, v2, v5, v5}, LX/DoN;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DoN;->SENDER_ACCEPT_PENDING:LX/DoN;

    .line 2041675
    new-instance v1, LX/DoN;

    const-string v2, "RECEIVER_ACCEPT_PENDING"

    invoke-direct {v1, v2, v6, v6}, LX/DoN;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DoN;->RECEIVER_ACCEPT_PENDING:LX/DoN;

    .line 2041676
    new-instance v1, LX/DoN;

    const-string v2, "RUNNING"

    invoke-direct {v1, v2, v7, v7}, LX/DoN;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DoN;->RUNNING:LX/DoN;

    .line 2041677
    new-instance v1, LX/DoN;

    const-string v2, "PRE_KEY_ERROR"

    invoke-direct {v1, v2, v8, v8}, LX/DoN;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DoN;->PRE_KEY_ERROR:LX/DoN;

    .line 2041678
    new-instance v1, LX/DoN;

    const-string v2, "ACCEPT_GEN_ERROR"

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, LX/DoN;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DoN;->ACCEPT_GEN_ERROR:LX/DoN;

    .line 2041679
    new-instance v1, LX/DoN;

    const-string v2, "ACCEPT_PROCESS_ERROR"

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v1, v2, v3, v4}, LX/DoN;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DoN;->ACCEPT_PROCESS_ERROR:LX/DoN;

    .line 2041680
    new-instance v1, LX/DoN;

    const-string v2, "DECRYPT_ERROR"

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v1, v2, v3, v4}, LX/DoN;-><init>(Ljava/lang/String;II)V

    sput-object v1, LX/DoN;->DECRYPT_ERROR:LX/DoN;

    .line 2041681
    const/16 v1, 0x8

    new-array v1, v1, [LX/DoN;

    sget-object v2, LX/DoN;->NEW:LX/DoN;

    aput-object v2, v1, v0

    sget-object v2, LX/DoN;->SENDER_ACCEPT_PENDING:LX/DoN;

    aput-object v2, v1, v5

    sget-object v2, LX/DoN;->RECEIVER_ACCEPT_PENDING:LX/DoN;

    aput-object v2, v1, v6

    sget-object v2, LX/DoN;->RUNNING:LX/DoN;

    aput-object v2, v1, v7

    sget-object v2, LX/DoN;->PRE_KEY_ERROR:LX/DoN;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, LX/DoN;->ACCEPT_GEN_ERROR:LX/DoN;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, LX/DoN;->ACCEPT_PROCESS_ERROR:LX/DoN;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, LX/DoN;->DECRYPT_ERROR:LX/DoN;

    aput-object v3, v1, v2

    sput-object v1, LX/DoN;->$VALUES:[LX/DoN;

    .line 2041682
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, LX/DoN;->sStateMap:Ljava/util/Map;

    .line 2041683
    invoke-static {}, LX/DoN;->values()[LX/DoN;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2041684
    sget-object v4, LX/DoN;->sStateMap:Ljava/util/Map;

    invoke-virtual {v3}, LX/DoN;->getValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2041685
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2041686
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2041687
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2041688
    iput p3, p0, LX/DoN;->mValue:I

    .line 2041689
    return-void
.end method

.method public static from(I)LX/DoN;
    .locals 2

    .prologue
    .line 2041690
    sget-object v0, LX/DoN;->sStateMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DoN;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/DoN;
    .locals 1

    .prologue
    .line 2041691
    const-class v0, LX/DoN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DoN;

    return-object v0
.end method

.method public static values()[LX/DoN;
    .locals 1

    .prologue
    .line 2041692
    sget-object v0, LX/DoN;->$VALUES:[LX/DoN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DoN;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 2041693
    iget v0, p0, LX/DoN;->mValue:I

    return v0
.end method
