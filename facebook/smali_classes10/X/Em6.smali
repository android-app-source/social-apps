.class public LX/Em6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2165078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2165079
    iput-object p1, p0, LX/Em6;->a:LX/0Zb;

    .line 2165080
    return-void
.end method

.method public static b(LX/0QB;)LX/Em6;
    .locals 2

    .prologue
    .line 2165090
    new-instance v1, LX/Em6;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/Em6;-><init>(LX/0Zb;)V

    .line 2165091
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 2165081
    iget-object v0, p0, LX/Em6;->a:LX/0Zb;

    const-string v1, "successful_deeplink"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2165082
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2165083
    const-string v1, "incoming_uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2165084
    const-string v1, "translated_uri"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2165085
    const-string v1, "ref"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2165086
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2165087
    const-string v2, "referral"

    invoke-virtual {v0, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2165088
    :cond_0
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2165089
    :cond_1
    return-void
.end method
