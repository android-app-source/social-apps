.class public final enum LX/Ddl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ddl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ddl;

.field public static final enum LOADING:LX/Ddl;

.field public static final enum SEND:LX/Ddl;

.field public static final enum SENDING:LX/Ddl;

.field public static final enum SENT:LX/Ddl;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2019993
    new-instance v0, LX/Ddl;

    const-string v1, "SEND"

    invoke-direct {v0, v1, v2}, LX/Ddl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ddl;->SEND:LX/Ddl;

    .line 2019994
    new-instance v0, LX/Ddl;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/Ddl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ddl;->LOADING:LX/Ddl;

    .line 2019995
    new-instance v0, LX/Ddl;

    const-string v1, "SENDING"

    invoke-direct {v0, v1, v4}, LX/Ddl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ddl;->SENDING:LX/Ddl;

    .line 2019996
    new-instance v0, LX/Ddl;

    const-string v1, "SENT"

    invoke-direct {v0, v1, v5}, LX/Ddl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ddl;->SENT:LX/Ddl;

    .line 2019997
    const/4 v0, 0x4

    new-array v0, v0, [LX/Ddl;

    sget-object v1, LX/Ddl;->SEND:LX/Ddl;

    aput-object v1, v0, v2

    sget-object v1, LX/Ddl;->LOADING:LX/Ddl;

    aput-object v1, v0, v3

    sget-object v1, LX/Ddl;->SENDING:LX/Ddl;

    aput-object v1, v0, v4

    sget-object v1, LX/Ddl;->SENT:LX/Ddl;

    aput-object v1, v0, v5

    sput-object v0, LX/Ddl;->$VALUES:[LX/Ddl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2019998
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ddl;
    .locals 1

    .prologue
    .line 2019999
    const-class v0, LX/Ddl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ddl;

    return-object v0
.end method

.method public static values()[LX/Ddl;
    .locals 1

    .prologue
    .line 2020000
    sget-object v0, LX/Ddl;->$VALUES:[LX/Ddl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ddl;

    return-object v0
.end method
