.class public final enum LX/DdQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DdQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DdQ;

.field public static final enum MESSAGE_ADDED:LX/DdQ;

.field public static final enum MESSAGE_DELETED:LX/DdQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2019509
    new-instance v0, LX/DdQ;

    const-string v1, "MESSAGE_ADDED"

    invoke-direct {v0, v1, v2}, LX/DdQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DdQ;->MESSAGE_ADDED:LX/DdQ;

    .line 2019510
    new-instance v0, LX/DdQ;

    const-string v1, "MESSAGE_DELETED"

    invoke-direct {v0, v1, v3}, LX/DdQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DdQ;->MESSAGE_DELETED:LX/DdQ;

    .line 2019511
    const/4 v0, 0x2

    new-array v0, v0, [LX/DdQ;

    sget-object v1, LX/DdQ;->MESSAGE_ADDED:LX/DdQ;

    aput-object v1, v0, v2

    sget-object v1, LX/DdQ;->MESSAGE_DELETED:LX/DdQ;

    aput-object v1, v0, v3

    sput-object v0, LX/DdQ;->$VALUES:[LX/DdQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2019512
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DdQ;
    .locals 1

    .prologue
    .line 2019508
    const-class v0, LX/DdQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DdQ;

    return-object v0
.end method

.method public static values()[LX/DdQ;
    .locals 1

    .prologue
    .line 2019507
    sget-object v0, LX/DdQ;->$VALUES:[LX/DdQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DdQ;

    return-object v0
.end method
