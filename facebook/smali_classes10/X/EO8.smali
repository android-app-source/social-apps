.class public LX/EO8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/EPO;


# direct methods
.method public constructor <init>(LX/EPO;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EPO;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2113973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2113974
    iput-object p1, p0, LX/EO8;->d:LX/EPO;

    .line 2113975
    iput-object p2, p0, LX/EO8;->a:LX/0Ot;

    .line 2113976
    iput-object p3, p0, LX/EO8;->b:LX/0Ot;

    .line 2113977
    iput-object p4, p0, LX/EO8;->c:LX/0Ot;

    .line 2113978
    return-void
.end method

.method public static a(LX/0QB;)LX/EO8;
    .locals 7

    .prologue
    .line 2113979
    const-class v1, LX/EO8;

    monitor-enter v1

    .line 2113980
    :try_start_0
    sget-object v0, LX/EO8;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113981
    sput-object v2, LX/EO8;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113982
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113983
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113984
    new-instance v4, LX/EO8;

    invoke-static {v0}, LX/EPO;->a(LX/0QB;)LX/EPO;

    move-result-object v3

    check-cast v3, LX/EPO;

    const/16 v5, 0x455

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1140

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x32d4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, LX/EO8;-><init>(LX/EPO;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2113985
    move-object v0, v4

    .line 2113986
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113987
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EO8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113988
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;LX/CzL;LX/CxV;)V
    .locals 11
    .param p2    # LX/CzL;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/CxV;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/CzL",
            "<",
            "LX/8d7;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    .line 2113990
    iget-object v0, p0, LX/EO8;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/CvY;

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    move-object v0, p3

    check-cast v0, LX/CxP;

    invoke-interface {v0, p2}, LX/CxP;->b(LX/CzL;)I

    move-result v10

    iget-object v0, p0, LX/EO8;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPINION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {p2}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    .line 2113991
    iget-object v3, p2, LX/CzL;->d:LX/0am;

    move-object v3, v3

    .line 2113992
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v4

    move-object v5, p3

    check-cast v5, LX/CxP;

    invoke-interface {v5, p2}, LX/CxP;->b(LX/CzL;)I

    move-result v5

    const/4 v6, 0x1

    .line 2113993
    iget-object v7, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v7, v7

    .line 2113994
    invoke-static {v7}, LX/8eM;->d(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;IILcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {v8, v9, v10, p2, v0}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2113995
    iget-object v0, p0, LX/EO8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nD;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2113996
    iget-object v2, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 2113997
    check-cast v2, LX/8d7;

    invoke-interface {v2}, LX/8d7;->aP()LX/8dH;

    move-result-object v2

    invoke-interface {v2}, LX/8dH;->j()LX/8dG;

    move-result-object v2

    invoke-interface {v2}, LX/8dG;->a()Ljava/lang/String;

    move-result-object v2

    .line 2113998
    iget-object v3, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 2113999
    check-cast v3, LX/8d7;

    invoke-interface {v3}, LX/8d7;->aP()LX/8dH;

    move-result-object v3

    invoke-interface {v3}, LX/8dH;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v4

    invoke-interface {v4}, LX/8ef;->m()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/8ci;->l:LX/8ci;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->OPINION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    new-instance v7, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v8, -0x2114909c

    invoke-direct {v7, v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v8

    .line 2114000
    iget-object v9, v8, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v8, v9

    .line 2114001
    invoke-virtual/range {v0 .. v8}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v1

    .line 2114002
    iget-object v0, p0, LX/EO8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2114003
    return-void
.end method
