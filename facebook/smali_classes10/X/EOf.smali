.class public final LX/EOf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EOS;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;LX/CzL;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2114927
    iput-object p1, p0, LX/EOf;->c:Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;

    iput-object p2, p0, LX/EOf;->a:LX/CzL;

    iput-object p3, p0, LX/EOf;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(LX/CzL;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Landroid/view/View;)V
    .locals 17
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d2;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2114917
    invoke-virtual/range {p1 .. p1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, LX/8d2;

    .line 2114918
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EOf;->a:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bN()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v1

    .line 2114919
    new-instance v2, LX/EOe;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/EOe;-><init>(LX/EOf;)V

    invoke-static {v1, v2}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2114920
    invoke-static {v1}, LX/9hF;->f(LX/0Px;)LX/9hE;

    move-result-object v1

    .line 2114921
    sget-object v2, LX/74S;->SEARCH_PHOTOS_GRID_MODULE:LX/74S;

    invoke-virtual {v1, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v1

    invoke-interface {v9}, LX/8d2;->dW_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/9hD;->e(Z)LX/9hD;

    move-result-object v1

    invoke-virtual {v1}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v2

    .line 2114922
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EOf;->c:Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/23R;

    invoke-virtual/range {p5 .. p5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v1, v3, v2, v4}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2114923
    invoke-virtual/range {p1 .. p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v8

    .line 2114924
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EOf;->c:Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;

    iget-object v13, v1, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->h:LX/CvY;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EOf;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v14

    sget-object v15, LX/8ch;->CLICK:LX/8ch;

    invoke-virtual/range {p1 .. p1}, LX/CzL;->d()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EOf;->c:Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->b(Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;)LX/CvY;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/EOf;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, LX/CzL;->l()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/EOf;->b:LX/1Pn;

    check-cast v4, LX/CxP;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/EOf;->a:LX/CzL;

    invoke-interface {v4, v5}, LX/CxP;->b(LX/CzL;)I

    move-result v4

    invoke-virtual/range {p1 .. p1}, LX/CzL;->d()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v6

    invoke-static {v6}, LX/8eM;->j(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v6

    invoke-virtual/range {p1 .. p1}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v7

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v8

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v9}, LX/8d2;->dW_()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    const/4 v10, 0x1

    :goto_0
    move-object/from16 v11, p3

    move-object/from16 v12, p4

    invoke-static/range {v1 .. v12}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v1, v13

    move-object v2, v14

    move-object v3, v15

    move/from16 v4, v16

    move-object/from16 v5, p1

    invoke-virtual/range {v1 .. v6}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2114925
    return-void

    .line 2114926
    :cond_0
    const/4 v10, 0x0

    goto :goto_0
.end method
