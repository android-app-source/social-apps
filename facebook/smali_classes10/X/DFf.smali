.class public final LX/DFf;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DFg;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/2ep;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:LX/162;

.field public final synthetic g:LX/DFg;


# direct methods
.method public constructor <init>(LX/DFg;)V
    .locals 1

    .prologue
    .line 1978588
    iput-object p1, p0, LX/DFf;->g:LX/DFg;

    .line 1978589
    move-object v0, p1

    .line 1978590
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1978591
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1978592
    const-string v0, "PersonYouMayKnowTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1978593
    if-ne p0, p1, :cond_1

    .line 1978594
    :cond_0
    :goto_0
    return v0

    .line 1978595
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1978596
    goto :goto_0

    .line 1978597
    :cond_3
    check-cast p1, LX/DFf;

    .line 1978598
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1978599
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1978600
    if-eq v2, v3, :cond_0

    .line 1978601
    iget-object v2, p0, LX/DFf;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DFf;->a:Ljava/lang/String;

    iget-object v3, p1, LX/DFf;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1978602
    goto :goto_0

    .line 1978603
    :cond_5
    iget-object v2, p1, LX/DFf;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1978604
    :cond_6
    iget-object v2, p0, LX/DFf;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DFf;->b:Ljava/lang/String;

    iget-object v3, p1, LX/DFf;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1978605
    goto :goto_0

    .line 1978606
    :cond_8
    iget-object v2, p1, LX/DFf;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1978607
    :cond_9
    iget-object v2, p0, LX/DFf;->c:LX/2ep;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/DFf;->c:LX/2ep;

    iget-object v3, p1, LX/DFf;->c:LX/2ep;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1978608
    goto :goto_0

    .line 1978609
    :cond_b
    iget-object v2, p1, LX/DFf;->c:LX/2ep;

    if-nez v2, :cond_a

    .line 1978610
    :cond_c
    iget-object v2, p0, LX/DFf;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/DFf;->d:Ljava/lang/String;

    iget-object v3, p1, LX/DFf;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1978611
    goto :goto_0

    .line 1978612
    :cond_e
    iget-object v2, p1, LX/DFf;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 1978613
    :cond_f
    iget-object v2, p0, LX/DFf;->e:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/DFf;->e:Ljava/lang/String;

    iget-object v3, p1, LX/DFf;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1978614
    goto :goto_0

    .line 1978615
    :cond_11
    iget-object v2, p1, LX/DFf;->e:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 1978616
    :cond_12
    iget-object v2, p0, LX/DFf;->f:LX/162;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/DFf;->f:LX/162;

    iget-object v3, p1, LX/DFf;->f:LX/162;

    invoke-virtual {v2, v3}, LX/162;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1978617
    goto/16 :goto_0

    .line 1978618
    :cond_13
    iget-object v2, p1, LX/DFf;->f:LX/162;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
