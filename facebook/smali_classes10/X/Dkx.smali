.class public final LX/Dkx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2036651
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 2036652
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2036653
    :goto_0
    return v1

    .line 2036654
    :cond_0
    const-string v12, "start_time"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2036655
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    .line 2036656
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 2036657
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2036658
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2036659
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2036660
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2036661
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 2036662
    :cond_2
    const-string v12, "product_item"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 2036663
    invoke-static {p0, p1}, LX/Dku;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2036664
    :cond_3
    const-string v12, "suggested_time_range"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2036665
    invoke-static {p0, p1}, LX/Dkv;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2036666
    :cond_4
    const-string v12, "user"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2036667
    invoke-static {p0, p1}, LX/Dkw;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2036668
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2036669
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2036670
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 2036671
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 2036672
    if-eqz v0, :cond_7

    .line 2036673
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2036674
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2036675
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2036676
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    move v9, v1

    move v10, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2036677
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2036678
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2036679
    if-eqz v0, :cond_0

    .line 2036680
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036681
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2036682
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2036683
    if-eqz v0, :cond_1

    .line 2036684
    const-string v1, "product_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036685
    invoke-static {p0, v0, p2}, LX/Dku;->a(LX/15i;ILX/0nX;)V

    .line 2036686
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2036687
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 2036688
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036689
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2036690
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2036691
    if-eqz v0, :cond_3

    .line 2036692
    const-string v1, "suggested_time_range"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036693
    invoke-static {p0, v0, p2}, LX/Dkv;->a(LX/15i;ILX/0nX;)V

    .line 2036694
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2036695
    if-eqz v0, :cond_4

    .line 2036696
    const-string v1, "user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2036697
    invoke-static {p0, v0, p2, p3}, LX/Dkw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2036698
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2036699
    return-void
.end method
