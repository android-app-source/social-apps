.class public final LX/Epy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V
    .locals 0

    .prologue
    .line 2170825
    iput-object p1, p0, LX/Epy;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2170821
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2170822
    :goto_0
    return-void

    .line 2170823
    :pswitch_0
    iget-object v0, p0, LX/Epy;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    sget-object v1, LX/ErX;->INVITE_SEARCH:LX/ErX;

    invoke-virtual {v0, v1}, LX/ErW;->b(LX/ErX;)V

    goto :goto_0

    .line 2170824
    :pswitch_1
    iget-object v0, p0, LX/Epy;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->A:LX/ErW;

    sget-object v1, LX/ErX;->INVITE_SEARCH:LX/ErX;

    invoke-virtual {v0, v1}, LX/ErW;->a(LX/ErX;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2170820
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2170819
    return-void
.end method
