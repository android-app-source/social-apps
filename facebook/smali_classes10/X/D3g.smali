.class public final enum LX/D3g;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D3g;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D3g;

.field public static final enum HOUR:LX/D3g;

.field public static final enum MILLISECONDS:LX/D3g;

.field public static final enum MINUTES:LX/D3g;

.field public static final enum SECONDS:LX/D3g;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1960192
    new-instance v0, LX/D3g;

    const-string v1, "MILLISECONDS"

    invoke-direct {v0, v1, v2}, LX/D3g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D3g;->MILLISECONDS:LX/D3g;

    .line 1960193
    new-instance v0, LX/D3g;

    const-string v1, "SECONDS"

    invoke-direct {v0, v1, v3}, LX/D3g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D3g;->SECONDS:LX/D3g;

    .line 1960194
    new-instance v0, LX/D3g;

    const-string v1, "MINUTES"

    invoke-direct {v0, v1, v4}, LX/D3g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D3g;->MINUTES:LX/D3g;

    .line 1960195
    new-instance v0, LX/D3g;

    const-string v1, "HOUR"

    invoke-direct {v0, v1, v5}, LX/D3g;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D3g;->HOUR:LX/D3g;

    .line 1960196
    const/4 v0, 0x4

    new-array v0, v0, [LX/D3g;

    sget-object v1, LX/D3g;->MILLISECONDS:LX/D3g;

    aput-object v1, v0, v2

    sget-object v1, LX/D3g;->SECONDS:LX/D3g;

    aput-object v1, v0, v3

    sget-object v1, LX/D3g;->MINUTES:LX/D3g;

    aput-object v1, v0, v4

    sget-object v1, LX/D3g;->HOUR:LX/D3g;

    aput-object v1, v0, v5

    sput-object v0, LX/D3g;->$VALUES:[LX/D3g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1960197
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D3g;
    .locals 1

    .prologue
    .line 1960198
    const-class v0, LX/D3g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D3g;

    return-object v0
.end method

.method public static values()[LX/D3g;
    .locals 1

    .prologue
    .line 1960199
    sget-object v0, LX/D3g;->$VALUES:[LX/D3g;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D3g;

    return-object v0
.end method
