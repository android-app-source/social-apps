.class public final LX/DMU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/events/GroupEventsBaseFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/events/GroupEventsBaseFragment;)V
    .locals 0

    .prologue
    .line 1989854
    iput-object p1, p0, LX/DMU;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 1989855
    iget-object v0, p0, LX/DMU;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-boolean v0, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DMU;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    const/4 v1, 0x0

    .line 1989856
    iget-object p1, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->j:LX/DMJ;

    invoke-virtual {p1}, LX/DMJ;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1989857
    :cond_0
    :goto_0
    move v0, v1

    .line 1989858
    if-eqz v0, :cond_1

    .line 1989859
    iget-object v0, p0, LX/DMU;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    invoke-static {v0}, Lcom/facebook/groups/events/GroupEventsBaseFragment;->l(Lcom/facebook/groups/events/GroupEventsBaseFragment;)V

    .line 1989860
    :cond_1
    return-void

    .line 1989861
    :cond_2
    if-lez p3, :cond_0

    if-lez p4, :cond_0

    .line 1989862
    add-int p1, p2, p3

    add-int/lit8 p1, p1, 0x3

    if-le p1, p4, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 1989863
    return-void
.end method
