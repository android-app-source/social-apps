.class public final LX/DPv;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/DaR;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/groups/info/GroupInfoAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;II)V
    .locals 0

    .prologue
    .line 1993939
    iput-object p1, p0, LX/DPv;->d:Lcom/facebook/groups/info/GroupInfoAdapter;

    iput-object p3, p0, LX/DPv;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;

    iput p4, p0, LX/DPv;->b:I

    iput p5, p0, LX/DPv;->c:I

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1993940
    check-cast p1, LX/DaR;

    .line 1993941
    iget-object v0, p0, LX/DPv;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v1

    .line 1993942
    iget-object v0, p0, LX/DPv;->d:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v0, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->c:LX/DMP;

    invoke-virtual {p1, v0}, LX/DaR;->setGroupEventRsvpUpdateListener(LX/DMP;)V

    .line 1993943
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1993944
    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1993945
    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    if-lez v0, :cond_1

    const-string v0, "future"

    .line 1993946
    :goto_0
    move-object v2, v0

    .line 1993947
    iget v0, p0, LX/DPv;->b:I

    iget v3, p0, LX/DPv;->c:I

    add-int/lit8 v3, v3, -0x1

    if-eq v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1, v1, v2, v0}, LX/DaR;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;Z)V

    .line 1993948
    new-instance v0, LX/DPu;

    invoke-direct {v0, p0}, LX/DPu;-><init>(LX/DPv;)V

    invoke-virtual {p1, v0}, LX/DaR;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1993949
    return-void

    .line 1993950
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1993951
    :cond_1
    const-string v0, "past"

    goto :goto_0

    .line 1993952
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    if-lez v0, :cond_3

    const-string v0, "future"

    goto :goto_0

    :cond_3
    const-string v0, "past"

    goto :goto_0
.end method
