.class public final LX/EqG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/EventsExtendedInviteActivity;)V
    .locals 0

    .prologue
    .line 2171240
    iput-object p1, p0, LX/EqG;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;ZZ)V
    .locals 2

    .prologue
    .line 2171241
    if-eqz p2, :cond_1

    .line 2171242
    iget-object v0, p0, LX/EqG;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2171243
    iget-object v0, p0, LX/EqG;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    .line 2171244
    if-eqz p3, :cond_0

    .line 2171245
    iget-object v0, p0, LX/EqG;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->selectAll()V

    .line 2171246
    iget-object v0, p0, LX/EqG;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v1, p0, LX/EqG;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v1, v1, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->C:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 2171247
    invoke-static {v0, v1}, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->a$redex0(Lcom/facebook/events/invite/EventsExtendedInviteActivity;Landroid/view/View;)V

    .line 2171248
    :cond_0
    :goto_0
    return-void

    .line 2171249
    :cond_1
    iget-object v0, p0, LX/EqG;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2171250
    iget-object v0, p0, LX/EqG;->a:Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteActivity;->A:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Z)V

    goto :goto_0
.end method
