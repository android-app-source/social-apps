.class public final LX/EKX;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/EKX;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EKV;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/EKY;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2106912
    const/4 v0, 0x0

    sput-object v0, LX/EKX;->a:LX/EKX;

    .line 2106913
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EKX;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2106914
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2106915
    new-instance v0, LX/EKY;

    invoke-direct {v0}, LX/EKY;-><init>()V

    iput-object v0, p0, LX/EKX;->c:LX/EKY;

    .line 2106916
    return-void
.end method

.method public static declared-synchronized q()LX/EKX;
    .locals 2

    .prologue
    .line 2106917
    const-class v1, LX/EKX;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/EKX;->a:LX/EKX;

    if-nez v0, :cond_0

    .line 2106918
    new-instance v0, LX/EKX;

    invoke-direct {v0}, LX/EKX;-><init>()V

    sput-object v0, LX/EKX;->a:LX/EKX;

    .line 2106919
    :cond_0
    sget-object v0, LX/EKX;->a:LX/EKX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2106920
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2106921
    check-cast p2, LX/EKW;

    .line 2106922
    iget-object v0, p2, LX/EKW;->a:LX/0Px;

    const/4 v2, 0x2

    .line 2106923
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v1, v2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0a0097

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b1729

    invoke-interface {v1, v2}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b010f

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f08229e

    invoke-virtual {v2, v3}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a00a4

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x3f99999a    # 1.2f

    invoke-interface {v2, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2106924
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p0, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    .line 2106925
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p2

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const p2, 0x7f0b0050

    invoke-virtual {v1, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const p2, 0x7f0a00a4

    invoke-virtual {v1, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    sget-object p2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, p2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const p2, 0x7f0b172b

    invoke-interface {v1, p2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2106926
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2106927
    :cond_0
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    const v2, 0x7f08229f

    invoke-virtual {v1, v2}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a00a4

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2106928
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2106929
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2106930
    invoke-static {}, LX/1dS;->b()V

    .line 2106931
    const/4 v0, 0x0

    return-object v0
.end method
