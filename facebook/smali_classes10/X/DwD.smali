.class public LX/DwD;
.super Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;
.source ""


# instance fields
.field public k:D

.field public l:D


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2060168
    invoke-direct {p0, p1}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;-><init>(Landroid/content/Context;)V

    .line 2060169
    invoke-virtual {p0}, LX/DwD;->a()V

    .line 2060170
    return-void
.end method

.method private a(LX/0Px;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2060159
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v4, v0, :cond_1

    .line 2060160
    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2060161
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->L()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2060162
    int-to-double v6, v4

    iget-wide v8, p0, LX/DwD;->k:D

    iget-wide v10, p0, LX/DwD;->l:D

    add-double/2addr v8, v10

    mul-double/2addr v6, v8

    double-to-int v6, v6

    .line 2060163
    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    int-to-double v10, v6

    iget-wide v12, p0, LX/DwD;->k:D

    add-double/2addr v10, v12

    double-to-int v9, v10

    iget-wide v10, p0, LX/DwD;->k:D

    double-to-int v10, v10

    invoke-direct {v7, v6, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v1, v7

    .line 2060164
    iget-object v2, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v2}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->L()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2060165
    iget-object v3, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    const-string v5, "LoadSquareImageThumbnail"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Landroid/graphics/Rect;Landroid/net/Uri;Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;ILjava/lang/String;)V

    .line 2060166
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2060167
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2060171
    invoke-super {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a()V

    .line 2060172
    invoke-virtual {p0}, LX/DwD;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2060173
    invoke-virtual {p0}, LX/DwD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0b48

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, LX/DwD;->l:D

    .line 2060174
    int-to-double v0, v0

    iget-wide v2, p0, LX/DwD;->l:D

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    iput-wide v0, p0, LX/DwD;->k:D

    .line 2060175
    return-void
.end method

.method public final a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V
    .locals 1
    .param p8    # LX/Dw7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2060153
    invoke-super/range {p0 .. p8}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V

    .line 2060154
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2060155
    :cond_0
    :goto_0
    return-void

    .line 2060156
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c()V

    .line 2060157
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-direct {p0, v0}, LX/DwD;->a(LX/0Px;)V

    .line 2060158
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->b()V

    goto :goto_0
.end method

.method public final getNumOfItems()I
    .locals 1

    .prologue
    .line 2060152
    const/4 v0, 0x2

    return v0
.end method

.method public getRowHeight()I
    .locals 2

    .prologue
    .line 2060151
    iget-wide v0, p0, LX/DwD;->k:D

    double-to-int v0, v0

    return v0
.end method
