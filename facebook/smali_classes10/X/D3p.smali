.class public final LX/D3p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/394;


# instance fields
.field public final synthetic a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;)V
    .locals 0

    .prologue
    .line 1961164
    iput-object p1, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 3

    .prologue
    .line 1961165
    iget-object v0, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-boolean v0, v0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->H:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-object v0, v0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->I:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 1961166
    iget-object v0, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-object v0, v0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C1d;

    iget-object v1, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-object v1, v1, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->I:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/C1d;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1961167
    iget-object v0, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-object v0, v0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->F:LX/AVV;

    const-string v1, "explicit"

    iget-object v2, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-object v2, v2, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->I:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/AVV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1961168
    :cond_0
    return-void
.end method

.method public final a(LX/04g;LX/7Jv;)V
    .locals 3

    .prologue
    .line 1961169
    iget-boolean v0, p2, LX/7Jv;->b:Z

    if-eqz v0, :cond_0

    .line 1961170
    iget-object v0, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    invoke-virtual {v0}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->finish()V

    .line 1961171
    :cond_0
    iget-object v0, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-boolean v0, v0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->H:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-object v0, v0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->I:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_1

    .line 1961172
    iget-object v0, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-object v0, v0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C1d;

    iget-object v1, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-object v1, v1, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->I:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/C1d;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1961173
    iget-object v0, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-object v0, v0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->G:LX/Abd;

    iget-object v1, p2, LX/7Jv;->f:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1}, LX/Abd;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1961174
    iget-object v0, p0, LX/D3p;->a:Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;

    iget-object v0, v0, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->F:LX/AVV;

    const-string v1, "explicit"

    iget-object v2, p2, LX/7Jv;->f:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, v2}, LX/AVV;->b(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1961175
    :cond_1
    return-void
.end method
