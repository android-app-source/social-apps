.class public LX/DVw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2005934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2005935
    iput-object p1, p0, LX/DVw;->a:LX/0Or;

    .line 2005936
    iput-object p2, p0, LX/DVw;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2005937
    iput-object p3, p0, LX/DVw;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2005938
    return-void
.end method

.method public static a(LX/0QB;)LX/DVw;
    .locals 6

    .prologue
    .line 2005939
    const-class v1, LX/DVw;

    monitor-enter v1

    .line 2005940
    :try_start_0
    sget-object v0, LX/DVw;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2005941
    sput-object v2, LX/DVw;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2005942
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2005943
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2005944
    new-instance v5, LX/DVw;

    const/16 v3, 0xc

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v5, p0, v3, v4}, LX/DVw;-><init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2005945
    move-object v0, v5

    .line 2005946
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2005947
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DVw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2005948
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2005949
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2005950
    invoke-static {p1}, LX/DVw;->d(Landroid/support/v4/app/Fragment;)Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    move-result-object v2

    .line 2005951
    if-nez v2, :cond_0

    .line 2005952
    :goto_0
    return-void

    .line 2005953
    :cond_0
    new-array v3, v0, [Ljava/lang/CharSequence;

    aput-object p0, v3, v1

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static d(Landroid/support/v4/app/Fragment;)Lcom/facebook/widget/friendselector/FriendSelectorResultBar;
    .locals 2

    .prologue
    .line 2005954
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2005955
    if-nez v0, :cond_0

    .line 2005956
    const/4 v0, 0x0

    .line 2005957
    :goto_0
    return-object v0

    :cond_0
    const v1, 0x7f0d0f24

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    goto :goto_0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2005958
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/16 v0, 0x457f

    if-ne p1, v0, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "groups_custom_invite_message"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2005959
    const-string v0, "groups_custom_invite_message"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/DVw;->d:Ljava/lang/String;

    .line 2005960
    iget-object v0, p0, LX/DVw;->d:Ljava/lang/String;

    .line 2005961
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2005962
    invoke-static {p1}, LX/DVw;->d(Landroid/support/v4/app/Fragment;)Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    move-result-object v0

    .line 2005963
    if-nez v0, :cond_0

    .line 2005964
    :goto_0
    return-void

    .line 2005965
    :cond_0
    new-instance v1, LX/DVv;

    invoke-direct {v1, p0, p1}, LX/DVv;-><init>(LX/DVw;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(LX/BWx;)V

    goto :goto_0
.end method

.method public final b(Landroid/support/v4/app/Fragment;)V
    .locals 4

    .prologue
    .line 2005966
    invoke-static {p1}, LX/DVw;->d(Landroid/support/v4/app/Fragment;)Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    move-result-object v0

    .line 2005967
    if-nez v0, :cond_1

    .line 2005968
    :cond_0
    :goto_0
    return-void

    .line 2005969
    :cond_1
    iget-object v1, p0, LX/DVw;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/DZA;->f:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2005970
    iget-object v1, v0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->e:Landroid/view/View;

    move-object v0, v1

    .line 2005971
    new-instance v1, LX/0hs;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2005972
    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08308d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2005973
    sget-object v2, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 2005974
    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2005975
    iget-object v0, p0, LX/DVw;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/DZA;->f:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method
