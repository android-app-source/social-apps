.class public final LX/ETM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16X;


# instance fields
.field public final synthetic a:LX/ETN;


# direct methods
.method public constructor <init>(LX/ETN;)V
    .locals 0

    .prologue
    .line 2124725
    iput-object p1, p0, LX/ETM;->a:LX/ETN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/1ub;)V
    .locals 3

    .prologue
    .line 2124726
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ETM;->a:LX/ETN;

    iget-object v0, v0, LX/ETN;->e:LX/ETL;

    invoke-virtual {v0}, LX/ETL;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2124727
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2124728
    :cond_1
    :try_start_1
    iget-object v0, p1, LX/1ub;->a:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne v0, v1, :cond_3

    .line 2124729
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2124730
    iget-object v1, p1, LX/1ub;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2124731
    :try_start_2
    iget-object v1, p0, LX/ETM;->a:LX/ETN;

    iget-object v1, v1, LX/ETN;->b:LX/19w;

    invoke-virtual {v1, v0}, LX/19w;->a(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v1, -0x1e962c15

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, LX/15V;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2124732
    if-eqz v0, :cond_0

    .line 2124733
    :try_start_3
    iget-object v1, p0, LX/ETM;->a:LX/ETN;

    iget-object v1, v1, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0}, LX/ETN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    .line 2124734
    iget-object p1, v1, LX/ETQ;->a:Ljava/util/List;

    invoke-interface {p1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2124735
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-static {v1, p1}, LX/ETQ;->b(LX/ETQ;Ljava/util/Collection;)V

    .line 2124736
    iget-object v0, p0, LX/ETM;->a:LX/ETN;

    const/4 v1, 0x1

    .line 2124737
    iput-boolean v1, v0, LX/ETN;->m:Z

    .line 2124738
    iget-object v0, p0, LX/ETM;->a:LX/ETN;

    iget-object v0, v0, LX/ETN;->e:LX/ETL;

    invoke-virtual {v0}, LX/ETL;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ETM;->a:LX/ETN;

    iget-boolean v0, v0, LX/ETN;->n:Z

    if-nez v0, :cond_2

    .line 2124739
    iget-object v0, p0, LX/ETM;->a:LX/ETN;

    iget-object v0, v0, LX/ETN;->c:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/ETM;->a:LX/ETN;

    iget-object v1, v1, LX/ETN;->o:Ljava/lang/Runnable;

    const v2, 0x7872c425

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2124740
    :cond_2
    goto :goto_0

    .line 2124741
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2124742
    :catch_0
    move-exception v0

    .line 2124743
    :try_start_4
    sget-object v1, LX/ETN;->a:Ljava/lang/String;

    const-string v2, "Exception in getting story"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2124744
    :cond_3
    iget-object v0, p1, LX/1ub;->a:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/ETM;->a:LX/ETN;

    iget-object v1, p1, LX/1ub;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/ETN;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2124745
    iget-object v0, p0, LX/ETM;->a:LX/ETN;

    iget-object v0, v0, LX/ETN;->c:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/ETM;->a:LX/ETN;

    iget-object v1, v1, LX/ETN;->p:Ljava/lang/Runnable;

    const v2, 0x10ab02cf

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method
