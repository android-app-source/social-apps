.class public final LX/Eog;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Eoj;


# direct methods
.method public constructor <init>(LX/Eoj;)V
    .locals 0

    .prologue
    .line 2168374
    iput-object p1, p0, LX/Eog;->a:LX/Eoj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2168375
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2168376
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2168377
    if-eqz p1, :cond_0

    .line 2168378
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2168379
    if-nez v0, :cond_1

    .line 2168380
    :cond_0
    :goto_0
    return-void

    .line 2168381
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2168382
    check-cast v0, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2168383
    iget-object v1, p0, LX/Eog;->a:LX/Eoj;

    iget-object v1, v1, LX/Eoj;->g:LX/Eny;

    invoke-virtual {v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Eog;->a:LX/Eoj;

    iget-object v3, v3, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-static {v3}, LX/Eov;->a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;)LX/Eov;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    .line 2168384
    iput-object v4, v3, LX/Eov;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2168385
    move-object v3, v3

    .line 2168386
    invoke-virtual {v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v4

    .line 2168387
    iput-object v4, v3, LX/Eov;->w:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2168388
    move-object v3, v3

    .line 2168389
    invoke-virtual {v0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    .line 2168390
    iput-object v0, v3, LX/Eov;->u:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2168391
    move-object v0, v3

    .line 2168392
    invoke-virtual {v0}, LX/Eov;->a()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/Eny;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
