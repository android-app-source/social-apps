.class public final LX/EiJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/42q;


# instance fields
.field public final synthetic a:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/controller/ConfirmationFragmentController;)V
    .locals 0

    .prologue
    .line 2159271
    iput-object p1, p0, LX/EiJ;->a:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2159272
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2159273
    invoke-static {v0}, LX/EiG;->valueOfKey(Ljava/lang/String;)LX/EiG;

    move-result-object v0

    .line 2159274
    sget-object v1, LX/EiG;->CODE_SUCCESS:LX/EiG;

    if-ne v0, v1, :cond_0

    .line 2159275
    iget-object v0, p0, LX/EiJ;->a:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2159276
    :goto_0
    return-void

    .line 2159277
    :cond_0
    iget-object v1, p0, LX/EiJ;->a:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    iget-object v2, p0, LX/EiJ;->a:Lcom/facebook/confirmation/controller/ConfirmationFragmentController;

    iget-object v2, v2, Lcom/facebook/confirmation/controller/ConfirmationFragmentController;->a:LX/EiH;

    .line 2159278
    iget-object p0, v2, LX/EiH;->b:Ljava/util/Map;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EiI;

    .line 2159279
    if-nez p0, :cond_1

    .line 2159280
    iget-object p0, v2, LX/EiH;->b:Ljava/util/Map;

    sget-object p1, LX/EiG;->UNKNOWN_ERROR:LX/EiG;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EiI;

    .line 2159281
    :cond_1
    invoke-virtual {p0}, LX/EiI;->c()Landroid/content/Intent;

    move-result-object p0

    move-object v0, p0

    .line 2159282
    invoke-virtual {v1, v0}, Lcom/facebook/base/fragment/AbstractNavigableFragmentController;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
