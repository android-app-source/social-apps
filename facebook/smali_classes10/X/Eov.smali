.class public final LX/Eov;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z

.field public u:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2169022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;)LX/Eov;
    .locals 2

    .prologue
    .line 2169023
    new-instance v0, LX/Eov;

    invoke-direct {v0}, LX/Eov;-><init>()V

    .line 2169024
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->x()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->a:Ljava/lang/String;

    .line 2169025
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->l()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->b:Z

    .line 2169026
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->m()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->c:Z

    .line 2169027
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->d()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->d:Z

    .line 2169028
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->e()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->e:Z

    .line 2169029
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->p()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->f:Z

    .line 2169030
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->n()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->g:Z

    .line 2169031
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->D()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->h:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    .line 2169032
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->E()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->i:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    .line 2169033
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2169034
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->k:Ljava/lang/String;

    .line 2169035
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->q()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->l:Z

    .line 2169036
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->z()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->m:Z

    .line 2169037
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->r()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->n:Z

    .line 2169038
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->A()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->o:Z

    .line 2169039
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->ce_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->p:Ljava/lang/String;

    .line 2169040
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->F()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->q:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2169041
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2169042
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->H()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->s:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    .line 2169043
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->I()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->t:Z

    .line 2169044
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->u:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2169045
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->v:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    .line 2169046
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->w:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2169047
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->K()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->x:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    .line 2169048
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Eov;->y:Ljava/lang/String;

    .line 2169049
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->t()Z

    move-result v1

    iput-boolean v1, v0, LX/Eov;->z:Z

    .line 2169050
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;
    .locals 17

    .prologue
    .line 2169051
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 2169052
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Eov;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2169053
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Eov;->h:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardCoverPhotoFieldsModel;

    invoke-static {v1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2169054
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Eov;->i:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2169055
    move-object/from16 v0, p0

    iget-object v5, v0, LX/Eov;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2169056
    move-object/from16 v0, p0

    iget-object v6, v0, LX/Eov;->k:Ljava/lang/String;

    invoke-virtual {v1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2169057
    move-object/from16 v0, p0

    iget-object v7, v0, LX/Eov;->p:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2169058
    move-object/from16 v0, p0

    iget-object v8, v0, LX/Eov;->q:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2169059
    move-object/from16 v0, p0

    iget-object v9, v0, LX/Eov;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2169060
    move-object/from16 v0, p0

    iget-object v10, v0, LX/Eov;->s:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2169061
    move-object/from16 v0, p0

    iget-object v11, v0, LX/Eov;->u:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v1, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 2169062
    move-object/from16 v0, p0

    iget-object v12, v0, LX/Eov;->v:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2169063
    move-object/from16 v0, p0

    iget-object v13, v0, LX/Eov;->w:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v1, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 2169064
    move-object/from16 v0, p0

    iget-object v14, v0, LX/Eov;->x:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    invoke-static {v1, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 2169065
    move-object/from16 v0, p0

    iget-object v15, v0, LX/Eov;->y:Ljava/lang/String;

    invoke-virtual {v1, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 2169066
    const/16 v16, 0x1a

    move/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 2169067
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 2169068
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/Eov;->b:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2169069
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/Eov;->c:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2169070
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/Eov;->d:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2169071
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/Eov;->e:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2169072
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/Eov;->f:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2169073
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/Eov;->g:Z

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2169074
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 2169075
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 2169076
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 2169077
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 2169078
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/Eov;->l:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 2169079
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/Eov;->m:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 2169080
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/Eov;->n:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 2169081
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/Eov;->o:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 2169082
    const/16 v2, 0xf

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 2169083
    const/16 v2, 0x10

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 2169084
    const/16 v2, 0x11

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 2169085
    const/16 v2, 0x12

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 2169086
    const/16 v2, 0x13

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/Eov;->t:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 2169087
    const/16 v2, 0x14

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 2169088
    const/16 v2, 0x15

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 2169089
    const/16 v2, 0x16

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 2169090
    const/16 v2, 0x17

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 2169091
    const/16 v2, 0x18

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 2169092
    const/16 v2, 0x19

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/Eov;->z:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 2169093
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 2169094
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 2169095
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 2169096
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2169097
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2169098
    new-instance v2, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-direct {v2, v1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;-><init>(LX/15i;)V

    .line 2169099
    return-object v2
.end method
