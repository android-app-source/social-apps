.class public final LX/DBc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/394;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/RichVideoPlayer;

.field public final synthetic b:LX/395;

.field public final synthetic c:LX/392;

.field public final synthetic d:Lcom/facebook/feed/fragment/NewsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/fragment/NewsFeedFragment;Lcom/facebook/video/player/RichVideoPlayer;LX/395;LX/392;)V
    .locals 0

    .prologue
    .line 1973086
    iput-object p1, p0, LX/DBc;->d:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iput-object p2, p0, LX/DBc;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iput-object p3, p0, LX/DBc;->b:LX/395;

    iput-object p4, p0, LX/DBc;->c:LX/392;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 10

    .prologue
    .line 1973087
    iget-object v0, p0, LX/DBc;->a:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1973088
    iget-object v0, p0, LX/DBc;->d:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->l:LX/1C2;

    iget-object v1, p0, LX/DBc;->b:LX/395;

    invoke-virtual {v1}, LX/395;->s()LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/DBc;->b:LX/395;

    invoke-virtual {v4}, LX/395;->p()I

    move-result v4

    iget-object v5, p0, LX/DBc;->b:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/DBc;->b:LX/395;

    invoke-virtual {v6}, LX/395;->q()LX/04D;

    move-result-object v6

    new-instance v7, LX/AnR;

    iget-object v8, p0, LX/DBc;->b:LX/395;

    iget-object v9, p0, LX/DBc;->a:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1973089
    iget-object p0, v9, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v9, p0

    .line 1973090
    iget-object v9, v9, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-direct {v7, v8, v9}, LX/AnR;-><init>(LX/395;Lcom/facebook/video/engine/VideoPlayerParams;)V

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1973091
    return-void
.end method

.method public final a(LX/04g;LX/7Jv;)V
    .locals 10

    .prologue
    .line 1973092
    iget-object v0, p0, LX/DBc;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget v1, p2, LX/7Jv;->c:I

    invoke-virtual {v0, v1, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1973093
    iget-boolean v0, p2, LX/7Jv;->a:Z

    if-nez v0, :cond_0

    .line 1973094
    iget-object v0, p0, LX/DBc;->c:LX/392;

    invoke-interface {v0}, LX/392;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1973095
    :cond_0
    iget-object v0, p0, LX/DBc;->d:Lcom/facebook/feed/fragment/NewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/fragment/NewsFeedFragment;->t:LX/1Aw;

    iget-object v0, v0, LX/1Aw;->l:LX/1C2;

    iget-object v1, p0, LX/DBc;->b:LX/395;

    invoke-virtual {v1}, LX/395;->s()LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget v4, p2, LX/7Jv;->c:I

    iget-object v5, p0, LX/DBc;->b:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/DBc;->b:LX/395;

    invoke-virtual {v6}, LX/395;->q()LX/04D;

    move-result-object v6

    new-instance v7, LX/AnR;

    iget-object v8, p0, LX/DBc;->b:LX/395;

    iget-object v9, p0, LX/DBc;->a:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1973096
    iget-object p0, v9, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v9, p0

    .line 1973097
    iget-object v9, v9, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-direct {v7, v8, v9}, LX/AnR;-><init>(LX/395;Lcom/facebook/video/engine/VideoPlayerParams;)V

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1973098
    return-void
.end method
