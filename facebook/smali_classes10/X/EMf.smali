.class public LX/EMf;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EMd;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EMh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2111100
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EMf;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EMh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111101
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2111102
    iput-object p1, p0, LX/EMf;->b:LX/0Ot;

    .line 2111103
    return-void
.end method

.method public static a(LX/0QB;)LX/EMf;
    .locals 4

    .prologue
    .line 2111104
    const-class v1, LX/EMf;

    monitor-enter v1

    .line 2111105
    :try_start_0
    sget-object v0, LX/EMf;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111106
    sput-object v2, LX/EMf;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111107
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111108
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111109
    new-instance v3, LX/EMf;

    const/16 p0, 0x341e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EMf;-><init>(LX/0Ot;)V

    .line 2111110
    move-object v0, v3

    .line 2111111
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111112
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EMf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111113
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111114
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2111115
    check-cast p2, LX/EMe;

    .line 2111116
    iget-object v0, p0, LX/EMf;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/EMe;->a:Ljava/lang/String;

    iget-object v1, p2, LX/EMe;->b:Ljava/lang/String;

    iget-object v2, p2, LX/EMe;->c:LX/1dc;

    const/4 p2, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x6

    const/4 v4, 0x2

    const/4 v5, 0x1

    .line 2111117
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v8, 0x7

    const/4 p0, 0x3

    invoke-interface {v3, v8, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v8

    if-nez v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v8, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    const p0, 0x7f0106d0

    invoke-virtual {v3, p0}, LX/1ne;->o(I)LX/1ne;

    move-result-object v3

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    const p0, 0x7f0b0052

    invoke-virtual {v3, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v3, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v3, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object p0

    if-eqz v2, :cond_1

    move v3, v4

    :goto_1
    invoke-interface {p0, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object p0

    if-eqz v2, :cond_2

    move v3, v6

    :goto_2
    invoke-interface {p0, v6, v3}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v5, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v8, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    if-nez v1, :cond_3

    const/4 v3, 0x0

    :goto_3
    invoke-interface {v8, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2111118
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v5, v7}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    goto :goto_0

    :cond_1
    move v3, v5

    goto :goto_1

    :cond_2
    move v3, v7

    goto :goto_2

    :cond_3
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const p0, 0x7f0106d1

    invoke-virtual {v3, p0}, LX/1ne;->o(I)LX/1ne;

    move-result-object v3

    const p0, 0x7f0b0050

    invoke-virtual {v3, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    if-eqz v2, :cond_4

    move v7, v6

    :cond_4
    invoke-interface {v3, v6, v7}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v5, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2111119
    invoke-static {}, LX/1dS;->b()V

    .line 2111120
    const/4 v0, 0x0

    return-object v0
.end method
