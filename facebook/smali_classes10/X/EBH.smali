.class public final LX/EBH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/reviews/ui/PageReviewsFeedFragment;)V
    .locals 0

    .prologue
    .line 2087071
    iput-object p1, p0, LX/EBH;->a:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 4

    .prologue
    .line 2087062
    iget-object v0, p0, LX/EBH;->a:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->f:LX/EA0;

    const/4 v2, 0x1

    .line 2087063
    add-int v1, p2, p3

    sub-int v1, p4, v1

    const/16 v3, 0xc

    if-ge v1, v3, :cond_1

    move v1, v2

    .line 2087064
    :goto_0
    iget-object v3, v0, LX/EA0;->p:LX/0am;

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    .line 2087065
    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    iget-boolean v1, v0, LX/EA0;->k:Z

    if-nez v1, :cond_0

    .line 2087066
    iget-object v3, v0, LX/EA0;->f:LX/EAH;

    iget-object p0, v0, LX/EA0;->t:Ljava/lang/String;

    const/16 p1, 0xa

    iget-object v1, v0, LX/EA0;->p:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, p0, p1, v0, v1}, LX/EAH;->a(Ljava/lang/String;ILX/EA0;Ljava/lang/String;)V

    .line 2087067
    iput-boolean v2, v0, LX/EA0;->k:Z

    .line 2087068
    iget-object v1, v0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v1}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p()V

    .line 2087069
    :cond_0
    return-void

    .line 2087070
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2087061
    return-void
.end method
