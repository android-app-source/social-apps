.class public LX/Dqy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/base/activity/FbPreferenceActivity;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0tX;

.field public final d:LX/Dqw;

.field public final e:LX/33a;


# direct methods
.method public constructor <init>(Lcom/facebook/base/activity/FbPreferenceActivity;LX/1Ck;LX/0tX;LX/33a;)V
    .locals 1
    .param p1    # Lcom/facebook/base/activity/FbPreferenceActivity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048806
    iput-object p1, p0, LX/Dqy;->a:Lcom/facebook/base/activity/FbPreferenceActivity;

    .line 2048807
    iput-object p2, p0, LX/Dqy;->b:LX/1Ck;

    .line 2048808
    iput-object p3, p0, LX/Dqy;->c:LX/0tX;

    .line 2048809
    iput-object p4, p0, LX/Dqy;->e:LX/33a;

    .line 2048810
    new-instance v0, LX/Dqw;

    invoke-direct {v0, p1}, LX/Dqw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Dqy;->d:LX/Dqw;

    .line 2048811
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/PreferenceGroup;Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    .line 2048812
    new-instance v0, LX/Dqx;

    iget-object v1, p0, LX/Dqy;->a:Lcom/facebook/base/activity/FbPreferenceActivity;

    invoke-direct {v0, v1}, LX/Dqx;-><init>(Landroid/content/Context;)V

    .line 2048813
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2048814
    new-instance v1, LX/BAc;

    invoke-direct {v1}, LX/BAc;-><init>()V

    move-object v1, v1

    .line 2048815
    const-string v2, "fetch_num"

    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2048816
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2048817
    iget-object v2, p0, LX/Dqy;->c:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2048818
    iget-object v2, p0, LX/Dqy;->d:LX/Dqw;

    invoke-virtual {v2}, LX/Dqw;->getOnPreferenceClickListener()Landroid/preference/Preference$OnPreferenceClickListener;

    move-result-object v2

    if-nez v2, :cond_0

    .line 2048819
    iget-object v2, p0, LX/Dqy;->d:LX/Dqw;

    new-instance v3, LX/Dqs;

    invoke-direct {v3, p0, p1, p2}, LX/Dqs;-><init>(LX/Dqy;Landroid/preference/PreferenceGroup;Ljava/lang/Runnable;)V

    invoke-virtual {v2, v3}, LX/Dqw;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2048820
    :cond_0
    iget-object v2, p0, LX/Dqy;->b:LX/1Ck;

    const-string v3, "PUSH_SETTINGS_LOAD"

    new-instance v4, LX/Dqt;

    invoke-direct {v4, p0, p1, v0, p2}, LX/Dqt;-><init>(LX/Dqy;Landroid/preference/PreferenceGroup;Landroid/preference/Preference;Ljava/lang/Runnable;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2048821
    return-void
.end method
