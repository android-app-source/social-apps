.class public LX/D3X;
.super Landroid/preference/Preference;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Be;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1959918
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1959919
    const-string v0, "Clear prefetched data"

    invoke-virtual {p0, v0}, LX/D3X;->setTitle(Ljava/lang/CharSequence;)V

    .line 1959920
    const-string v0, "Clear prefetch db and cache files"

    invoke-virtual {p0, v0}, LX/D3X;->setSummary(Ljava/lang/CharSequence;)V

    .line 1959921
    new-instance v0, LX/D3W;

    invoke-direct {v0, p0, p3, p2, p1}, LX/D3W;-><init>(LX/D3X;Ljava/util/concurrent/ExecutorService;LX/1Be;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, LX/D3X;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1959922
    return-void
.end method

.method public static a(LX/0QB;)LX/D3X;
    .locals 4

    .prologue
    .line 1959923
    new-instance v3, LX/D3X;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1Be;->a(LX/0QB;)LX/1Be;

    move-result-object v1

    check-cast v1, LX/1Be;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v3, v0, v1, v2}, LX/D3X;-><init>(Landroid/content/Context;LX/1Be;Ljava/util/concurrent/ExecutorService;)V

    .line 1959924
    move-object v0, v3

    .line 1959925
    return-object v0
.end method
