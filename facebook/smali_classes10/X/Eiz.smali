.class public LX/Eiz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2160713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160714
    return-void
.end method

.method public static a(LX/0QB;)LX/Eiz;
    .locals 1

    .prologue
    .line 2160715
    new-instance v0, LX/Eiz;

    invoke-direct {v0}, LX/Eiz;-><init>()V

    .line 2160716
    move-object v0, v0

    .line 2160717
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2160710
    check-cast p1, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;

    .line 2160711
    const/4 v0, 0x6

    new-array v0, v0, [Lorg/apache/http/NameValuePair;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "normalized_contactpoint"

    iget-object v4, p1, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->a:Lcom/facebook/growth/model/Contactpoint;

    iget-object v4, v4, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "contactpoint_type"

    iget-object v4, p1, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->a:Lcom/facebook/growth/model/Contactpoint;

    iget-object v4, v4, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    invoke-virtual {v4}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "code"

    iget-object v4, p1, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "source"

    iget-object v4, p1, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->c:LX/Ej0;

    invoke-virtual {v4}, LX/Ej0;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "surface"

    iget-object v4, p1, Lcom/facebook/confirmation/protocol/ConfirmContactpointMethod$Params;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2160712
    new-instance v0, LX/14N;

    const-string v1, "confirmContactpoint"

    const-string v2, "POST"

    const-string v3, "method/user.confirmcontactpoint"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2160707
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2160708
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2160709
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
