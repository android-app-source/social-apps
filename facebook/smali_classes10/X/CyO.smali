.class public final LX/CyO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/util/List",
        "<",
        "LX/8cK;",
        ">;TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final synthetic b:LX/CyQ;

.field public final synthetic c:LX/CyS;


# direct methods
.method public constructor <init>(LX/CyS;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CyQ;)V
    .locals 0

    .prologue
    .line 1952669
    iput-object p1, p0, LX/CyO;->c:LX/CyS;

    iput-object p2, p0, LX/CyO;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iput-object p3, p0, LX/CyO;->b:LX/CyQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1952670
    check-cast p1, Ljava/util/List;

    .line 1952671
    iget-object v0, p0, LX/CyO;->c:LX/CyS;

    iget-object v0, v0, LX/CyS;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0TD;

    new-instance v1, LX/CyN;

    invoke-direct {v1, p0, p1}, LX/CyN;-><init>(LX/CyO;Ljava/util/List;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
