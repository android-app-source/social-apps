.class public final LX/Eqs;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Equ;


# direct methods
.method public constructor <init>(LX/Equ;)V
    .locals 0

    .prologue
    .line 2172204
    iput-object p1, p0, LX/Eqs;->a:LX/Equ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2172205
    iget-object v0, p0, LX/Eqs;->a:LX/Equ;

    const/4 v1, 0x0

    .line 2172206
    iput-boolean v1, v0, LX/Equ;->m:Z

    .line 2172207
    iget-object v0, p0, LX/Eqs;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->n:LX/Eqg;

    .line 2172208
    iget-object v1, v0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v1, v1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    const/4 v2, 0x1

    iget-object v3, v0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v3, v3, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->r:LX/0Px;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/ErB;->a(ZLX/0Px;Z)V

    .line 2172209
    iget-object v0, p0, LX/Eqs;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->u:LX/03V;

    sget-object v1, LX/Equ;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch contacts"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2172210
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2172211
    move-object v1, v1

    .line 2172212
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2172213
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2172175
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2172176
    if-nez p1, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 2172177
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Server returned null"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/Eqs;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2172178
    :goto_1
    return-void

    .line 2172179
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_0

    .line 2172180
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2172181
    iget-object v1, p0, LX/Eqs;->a:LX/Equ;

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v4, v5, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 2172182
    :goto_2
    iput-object v0, v1, LX/Equ;->p:Ljava/lang/String;

    .line 2172183
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2172184
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2172185
    invoke-virtual {v1, v0, v3}, LX/15i;->j(II)I

    move-result v0

    move v1, v0

    .line 2172186
    :goto_3
    iget-object v6, p0, LX/Eqs;->a:LX/Equ;

    iget-object v0, p0, LX/Eqs;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->p:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v4, v5, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 2172187
    :goto_4
    iput-boolean v0, v6, LX/Equ;->m:Z

    .line 2172188
    const v0, 0x3d76496b

    invoke-static {v4, v5, v3, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2172189
    iget-object v2, p0, LX/Eqs;->a:LX/Equ;

    iget-object v2, v2, LX/Equ;->n:LX/Eqg;

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_5
    invoke-static {v0}, LX/Equ;->b(LX/2uF;)LX/0Px;

    move-result-object v0

    iget-object v3, p0, LX/Eqs;->a:LX/Equ;

    iget-boolean v3, v3, LX/Equ;->m:Z

    .line 2172190
    invoke-static {v2, v1}, LX/Eqg;->a(LX/Eqg;I)V

    .line 2172191
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2172192
    iget-object v5, v2, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v5, v5, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->r:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2172193
    invoke-virtual {v4, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2172194
    iget-object v5, v2, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 2172195
    iput-object v4, v5, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->r:LX/0Px;

    .line 2172196
    iget-object v4, v2, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v4, v4, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    iget-object v5, v2, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v5, v5, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->r:LX/0Px;

    .line 2172197
    const/4 v2, 0x0

    invoke-virtual {v4, v2, v5, v3}, LX/ErB;->a(ZLX/0Px;Z)V

    .line 2172198
    goto/16 :goto_1

    .line 2172199
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2172200
    :cond_3
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v4, v5, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2172201
    :cond_4
    const v0, 0x7fffffff

    move v1, v0

    goto :goto_3

    :cond_5
    move v0, v3

    .line 2172202
    goto :goto_4

    .line 2172203
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_5
.end method
