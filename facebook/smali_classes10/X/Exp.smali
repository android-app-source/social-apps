.class public final LX/Exp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5P5;


# instance fields
.field public final synthetic a:LX/83X;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic d:LX/Exs;


# direct methods
.method public constructor <init>(LX/Exs;LX/83X;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 0

    .prologue
    .line 2185327
    iput-object p1, p0, LX/Exp;->d:LX/Exs;

    iput-object p2, p0, LX/Exp;->a:LX/83X;

    iput-object p3, p0, LX/Exp;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p4, p0, LX/Exp;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2185328
    iget-object v0, p0, LX/Exp;->d:LX/Exs;

    iget-object v0, v0, LX/Exs;->o:LX/Ey4;

    iget-object v1, p0, LX/Exp;->d:LX/Exs;

    iget-object v1, v1, LX/Exs;->u:Ljava/lang/String;

    iget-object v2, p0, LX/Exp;->d:LX/Exs;

    iget-boolean v2, v2, LX/Exs;->t:Z

    .line 2185329
    const-string p0, "success"

    invoke-static {v0, p0, v1, v2}, LX/Ey4;->a(LX/Ey4;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2185330
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2185331
    iget-object v0, p0, LX/Exp;->d:LX/Exs;

    iget-object v0, v0, LX/Exs;->o:LX/Ey4;

    iget-object v1, p0, LX/Exp;->d:LX/Exs;

    iget-object v1, v1, LX/Exs;->u:Ljava/lang/String;

    iget-object v2, p0, LX/Exp;->d:LX/Exs;

    iget-boolean v2, v2, LX/Exs;->t:Z

    .line 2185332
    const-string v3, "fail"

    invoke-static {v0, v3, v1, v2}, LX/Ey4;->a(LX/Ey4;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2185333
    iget-object v0, p0, LX/Exp;->a:LX/83X;

    iget-object v1, p0, LX/Exp;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-interface {v0, v1}, LX/2np;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185334
    iget-object v0, p0, LX/Exp;->a:LX/83X;

    iget-object v1, p0, LX/Exp;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-interface {v0, v1}, LX/83X;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185335
    iget-object v0, p0, LX/Exp;->d:LX/Exs;

    iget-object v1, p0, LX/Exp;->a:LX/83X;

    invoke-interface {v1}, LX/2lr;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/Exs;->b(LX/Exs;J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Exp;->d:LX/Exs;

    iget-object v0, v0, LX/Exs;->r:LX/Ew6;

    if-eqz v0, :cond_0

    .line 2185336
    iget-object v0, p0, LX/Exp;->d:LX/Exs;

    iget-object v0, v0, LX/Exs;->r:LX/Ew6;

    iget-object v1, p0, LX/Exp;->a:LX/83X;

    invoke-interface {v1}, LX/2lr;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/Ew6;->a(J)V

    .line 2185337
    :cond_0
    return-void
.end method
