.class public abstract LX/Crh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "LX/CnP;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/richdocument/view/util/CompositeRecyclableViewFactory$RecyclableViewFactory",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation
.end field

.field public final c:I

.field public final d:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;I)V
    .locals 2

    .prologue
    .line 1941310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941311
    if-nez p1, :cond_0

    .line 1941312
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A valid parent ViewGroup is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1941313
    :cond_0
    iput-object p1, p0, LX/Crh;->d:Landroid/view/ViewGroup;

    .line 1941314
    iput p2, p0, LX/Crh;->c:I

    .line 1941315
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, LX/Crh;->c:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/Crh;->a:Ljava/util/List;

    .line 1941316
    new-instance v0, Ljava/util/HashSet;

    iget v1, p0, LX/Crh;->c:I

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, LX/Crh;->b:Ljava/util/Set;

    .line 1941317
    return-void
.end method


# virtual methods
.method public abstract b()LX/CnP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method
