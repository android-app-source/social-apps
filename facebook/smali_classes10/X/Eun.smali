.class public LX/Eun;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final b:Ljava/lang/String;

.field public final c:I

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2179994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179995
    iput-object p1, p0, LX/Eun;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2179996
    iput-object p2, p0, LX/Eun;->b:Ljava/lang/String;

    .line 2179997
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/Eun;->c:I

    .line 2179998
    iput-object p4, p0, LX/Eun;->d:Ljava/lang/String;

    .line 2179999
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2180000
    iget-object v0, p0, LX/Eun;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p0, LX/Eun;->c:I

    iget-object v2, p0, LX/Eun;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2180001
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2180002
    iget-object v0, p0, LX/Eun;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    iget v2, p0, LX/Eun;->c:I

    iget-object v3, p0, LX/Eun;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, LX/Eun;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 2180003
    iput-boolean p1, v1, LX/0Yj;->n:Z

    .line 2180004
    move-object v1, v1

    .line 2180005
    invoke-virtual {v1}, LX/0Yj;->b()LX/0Yj;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 2180006
    return-void
.end method
