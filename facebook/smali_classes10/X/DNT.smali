.class public LX/DNT;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/DNS;


# instance fields
.field public final b:LX/0aG;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/1BY;

.field public e:I

.field public f:I

.field public g:Lcom/facebook/api/feedtype/FeedType;

.field public h:Lcom/facebook/api/feed/FeedFetchContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1991683
    new-instance v0, LX/DNS;

    invoke-direct {v0}, LX/DNS;-><init>()V

    sput-object v0, LX/DNT;->a:LX/DNS;

    return-void
.end method

.method public constructor <init>(LX/0aG;Ljava/util/concurrent/ExecutorService;LX/1BY;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1991684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1991685
    iput-object p1, p0, LX/DNT;->b:LX/0aG;

    .line 1991686
    iput-object p2, p0, LX/DNT;->c:Ljava/util/concurrent/Executor;

    .line 1991687
    iput-object p3, p0, LX/DNT;->d:LX/1BY;

    .line 1991688
    return-void
.end method

.method public static a(LX/0QB;)LX/DNT;
    .locals 1

    .prologue
    .line 1991689
    invoke-static {p0}, LX/DNT;->b(LX/0QB;)LX/DNT;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DNT;
    .locals 4

    .prologue
    .line 1991690
    new-instance v3, LX/DNT;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/1BY;->a(LX/0QB;)LX/1BY;

    move-result-object v2

    check-cast v2, LX/1BY;

    invoke-direct {v3, v0, v1, v2}, LX/DNT;-><init>(LX/0aG;Ljava/util/concurrent/ExecutorService;LX/1BY;)V

    .line 1991691
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feedtype/FeedType;II)V
    .locals 3

    .prologue
    .line 1991692
    iput-object p1, p0, LX/DNT;->g:Lcom/facebook/api/feedtype/FeedType;

    .line 1991693
    iput p2, p0, LX/DNT;->e:I

    .line 1991694
    iput p3, p0, LX/DNT;->f:I

    .line 1991695
    iget-object v0, p1, Lcom/facebook/api/feedtype/FeedType;->e:Ljava/lang/Object;

    move-object v0, v0

    .line 1991696
    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;

    .line 1991697
    new-instance v1, Lcom/facebook/api/feed/FeedFetchContext;

    .line 1991698
    iget-object v2, v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->a:Ljava/lang/String;

    move-object v0, v2

    .line 1991699
    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/facebook/api/feed/FeedFetchContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, LX/DNT;->h:Lcom/facebook/api/feed/FeedFetchContext;

    .line 1991700
    return-void
.end method
