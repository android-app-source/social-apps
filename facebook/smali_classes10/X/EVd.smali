.class public LX/EVd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0xX;

.field private b:Z

.field public c:LX/EV0;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0xX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2128536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2128537
    iput-object p1, p0, LX/EVd;->a:LX/0xX;

    .line 2128538
    return-void
.end method

.method public static a(LX/0QB;)LX/EVd;
    .locals 4

    .prologue
    .line 2128525
    const-class v1, LX/EVd;

    monitor-enter v1

    .line 2128526
    :try_start_0
    sget-object v0, LX/EVd;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2128527
    sput-object v2, LX/EVd;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2128528
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2128529
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2128530
    new-instance p0, LX/EVd;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v3

    check-cast v3, LX/0xX;

    invoke-direct {p0, v3}, LX/EVd;-><init>(LX/0xX;)V

    .line 2128531
    move-object v0, p0

    .line 2128532
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2128533
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EVd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2128534
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2128535
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/EVd;Z)V
    .locals 2

    .prologue
    .line 2128514
    iget-boolean v0, p0, LX/EVd;->b:Z

    if-ne p1, v0, :cond_0

    .line 2128515
    :goto_0
    return-void

    .line 2128516
    :cond_0
    iput-boolean p1, p0, LX/EVd;->b:Z

    .line 2128517
    iget-object v0, p0, LX/EVd;->c:LX/EV0;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EVd;->a:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2128518
    iget-object v0, p0, LX/EVd;->c:LX/EV0;

    .line 2128519
    iget-object v1, v0, LX/EV0;->a:LX/EV1;

    iget-object v1, v1, LX/EV1;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ETX;

    .line 2128520
    if-eqz v1, :cond_1

    .line 2128521
    check-cast v1, LX/1Pq;

    const/4 p1, 0x0

    new-array p1, p1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {v1, p1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2128522
    :cond_1
    const/4 v1, 0x0

    .line 2128523
    iput-object v1, p0, LX/EVd;->c:LX/EV0;

    .line 2128524
    :cond_2
    goto :goto_0
.end method


# virtual methods
.method public final d()Z
    .locals 1

    .prologue
    .line 2128513
    iget-boolean v0, p0, LX/EVd;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EVd;->a:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
