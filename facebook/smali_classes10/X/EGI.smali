.class public LX/EGI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final phoneNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2097019
    new-instance v0, LX/1sv;

    const-string v1, "PhoneAppPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/EGI;->b:LX/1sv;

    .line 2097020
    new-instance v0, LX/1sw;

    const-string v1, "phoneNumbers"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/EGI;->c:LX/1sw;

    .line 2097021
    sput-boolean v3, LX/EGI;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2097016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2097017
    iput-object p1, p0, LX/EGI;->phoneNumbers:Ljava/util/List;

    .line 2097018
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2097050
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2097051
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 2097052
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 2097053
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PhoneAppPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2097054
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097055
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097056
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097057
    iget-object v4, p0, LX/EGI;->phoneNumbers:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 2097058
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097059
    const-string v4, "phoneNumbers"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097060
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097061
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097062
    iget-object v0, p0, LX/EGI;->phoneNumbers:Ljava/util/List;

    if-nez v0, :cond_4

    .line 2097063
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097064
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097065
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2097066
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2097067
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 2097068
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 2097069
    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 2097070
    :cond_4
    iget-object v0, p0, LX/EGI;->phoneNumbers:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 2097040
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2097041
    iget-object v0, p0, LX/EGI;->phoneNumbers:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2097042
    iget-object v0, p0, LX/EGI;->phoneNumbers:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2097043
    sget-object v0, LX/EGI;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2097044
    new-instance v0, LX/1u3;

    const/16 v1, 0xb

    iget-object v2, p0, LX/EGI;->phoneNumbers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 2097045
    iget-object v0, p0, LX/EGI;->phoneNumbers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2097046
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2097047
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2097048
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2097049
    return-void
.end method

.method public final a(LX/EGI;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2097030
    if-nez p1, :cond_1

    .line 2097031
    :cond_0
    :goto_0
    return v2

    .line 2097032
    :cond_1
    iget-object v0, p0, LX/EGI;->phoneNumbers:Ljava/util/List;

    if-eqz v0, :cond_4

    move v0, v1

    .line 2097033
    :goto_1
    iget-object v3, p1, LX/EGI;->phoneNumbers:Ljava/util/List;

    if-eqz v3, :cond_5

    move v3, v1

    .line 2097034
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 2097035
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2097036
    iget-object v0, p0, LX/EGI;->phoneNumbers:Ljava/util/List;

    iget-object v3, p1, LX/EGI;->phoneNumbers:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v2, v1

    .line 2097037
    goto :goto_0

    :cond_4
    move v0, v2

    .line 2097038
    goto :goto_1

    :cond_5
    move v3, v2

    .line 2097039
    goto :goto_2
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2097026
    if-nez p1, :cond_1

    .line 2097027
    :cond_0
    :goto_0
    return v0

    .line 2097028
    :cond_1
    instance-of v1, p1, LX/EGI;

    if-eqz v1, :cond_0

    .line 2097029
    check-cast p1, LX/EGI;

    invoke-virtual {p0, p1}, LX/EGI;->a(LX/EGI;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2097025
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2097022
    sget-boolean v0, LX/EGI;->a:Z

    .line 2097023
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/EGI;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2097024
    return-object v0
.end method
