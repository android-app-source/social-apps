.class public LX/Cwp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Cwp;


# instance fields
.field private final a:LX/Cwq;


# direct methods
.method public constructor <init>(LX/Cwq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951095
    iput-object p1, p0, LX/Cwp;->a:LX/Cwq;

    .line 1951096
    return-void
.end method

.method public static final a(Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;)LX/CwL;
    .locals 4

    .prologue
    .line 1951097
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1951098
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951099
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->FETCH_NEEDLE_FILTERS_FAIL:LX/3Ql;

    const-string v2, "Missing ID for needle filter"

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1951100
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1951101
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->FETCH_NEEDLE_FILTERS_FAIL:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing Name for needle filter with id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1951102
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1951103
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->FETCH_NEEDLE_FILTERS_FAIL:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing Text for needle filter with id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1951104
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;->b()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;->b()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/Cwq;->a(Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v0

    .line 1951105
    :goto_0
    new-instance v1, LX/CwK;

    invoke-direct {v1}, LX/CwK;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1951106
    iput-object v2, v1, LX/CwK;->a:Ljava/lang/String;

    .line 1951107
    move-object v1, v1

    .line 1951108
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 1951109
    iput-object v2, v1, LX/CwK;->b:Ljava/lang/String;

    .line 1951110
    move-object v1, v1

    .line 1951111
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterInfoFragmentModel$MainFilterModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 1951112
    iput-object v2, v1, LX/CwK;->c:Ljava/lang/String;

    .line 1951113
    move-object v1, v1

    .line 1951114
    iput-object v0, v1, LX/CwK;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 1951115
    move-object v0, v1

    .line 1951116
    invoke-virtual {v0}, LX/CwK;->a()LX/CwL;

    move-result-object v0

    return-object v0

    .line 1951117
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Cwp;
    .locals 4

    .prologue
    .line 1951081
    sget-object v0, LX/Cwp;->b:LX/Cwp;

    if-nez v0, :cond_1

    .line 1951082
    const-class v1, LX/Cwp;

    monitor-enter v1

    .line 1951083
    :try_start_0
    sget-object v0, LX/Cwp;->b:LX/Cwp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1951084
    if-eqz v2, :cond_0

    .line 1951085
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1951086
    new-instance p0, LX/Cwp;

    invoke-static {v0}, LX/Cwq;->a(LX/0QB;)LX/Cwq;

    move-result-object v3

    check-cast v3, LX/Cwq;

    invoke-direct {p0, v3}, LX/Cwp;-><init>(LX/Cwq;)V

    .line 1951087
    move-object v0, p0

    .line 1951088
    sput-object v0, LX/Cwp;->b:LX/Cwp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1951089
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1951090
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1951091
    :cond_1
    sget-object v0, LX/Cwp;->b:LX/Cwp;

    return-object v0

    .line 1951092
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1951093
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
