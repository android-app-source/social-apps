.class public final LX/E4O;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E4P;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

.field public b:LX/E2S;

.field public c:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic e:LX/E4P;


# direct methods
.method public constructor <init>(LX/E4P;)V
    .locals 1

    .prologue
    .line 2076607
    iput-object p1, p0, LX/E4O;->e:LX/E4P;

    .line 2076608
    move-object v0, p1

    .line 2076609
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2076610
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2076606
    const-string v0, "ReactionGroupJoinButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2076586
    if-ne p0, p1, :cond_1

    .line 2076587
    :cond_0
    :goto_0
    return v0

    .line 2076588
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2076589
    goto :goto_0

    .line 2076590
    :cond_3
    check-cast p1, LX/E4O;

    .line 2076591
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2076592
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2076593
    if-eq v2, v3, :cond_0

    .line 2076594
    iget-object v2, p0, LX/E4O;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E4O;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p1, LX/E4O;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2076595
    goto :goto_0

    .line 2076596
    :cond_5
    iget-object v2, p1, LX/E4O;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-nez v2, :cond_4

    .line 2076597
    :cond_6
    iget-object v2, p0, LX/E4O;->b:LX/E2S;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E4O;->b:LX/E2S;

    iget-object v3, p1, LX/E4O;->b:LX/E2S;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2076598
    goto :goto_0

    .line 2076599
    :cond_8
    iget-object v2, p1, LX/E4O;->b:LX/E2S;

    if-nez v2, :cond_7

    .line 2076600
    :cond_9
    iget-object v2, p0, LX/E4O;->c:LX/1Pq;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E4O;->c:LX/1Pq;

    iget-object v3, p1, LX/E4O;->c:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2076601
    goto :goto_0

    .line 2076602
    :cond_b
    iget-object v2, p1, LX/E4O;->c:LX/1Pq;

    if-nez v2, :cond_a

    .line 2076603
    :cond_c
    iget-object v2, p0, LX/E4O;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/E4O;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/E4O;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2076604
    goto :goto_0

    .line 2076605
    :cond_d
    iget-object v2, p1, LX/E4O;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
