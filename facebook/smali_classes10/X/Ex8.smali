.class public final LX/Ex8;
.super LX/1Cd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2184150
    iput-object p1, p0, LX/Ex8;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-direct {p0}, LX/1Cd;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2184151
    instance-of v0, p1, LX/Eus;

    if-nez v0, :cond_1

    .line 2184152
    :cond_0
    :goto_0
    return-void

    .line 2184153
    :cond_1
    check-cast p1, LX/Eus;

    .line 2184154
    invoke-virtual {p1}, LX/Eus;->a()J

    move-result-wide v4

    .line 2184155
    iget-object v0, p0, LX/Ex8;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->g:Ljava/util/Set;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2184156
    iget-object v0, p0, LX/Ex8;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2184157
    iget-object v0, p0, LX/Ex8;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v1, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->J:LX/2hd;

    iget-object v0, p0, LX/Ex8;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v2, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->m:Ljava/lang/String;

    .line 2184158
    iget-object v0, p1, LX/Eus;->h:Ljava/lang/String;

    move-object v3, v0

    .line 2184159
    sget-object v6, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    invoke-virtual/range {v1 .. v6}, LX/2hd;->a(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 2184160
    invoke-virtual {p1}, LX/Eus;->p()V

    .line 2184161
    iget-object v0, p0, LX/Ex8;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->g:Ljava/util/Set;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2184162
    :cond_2
    iget-object v0, p0, LX/Ex8;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
