.class public abstract LX/E6s;
.super LX/Cfk;
.source ""


# instance fields
.field public a:LX/1P1;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/25T;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(LX/0Or;LX/3Tx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/25T;",
            ">;",
            "LX/3Tx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2080732
    invoke-direct {p0, p2}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2080733
    iput-object p1, p0, LX/E6s;->b:LX/0Or;

    .line 2080734
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 1

    .prologue
    .line 2080735
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2080736
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V
    .locals 2
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p6    # LX/Cgb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2080737
    invoke-super/range {p0 .. p6}, LX/Cfk;->a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V

    .line 2080738
    const v0, 0x7f031114

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2080739
    iget-object v1, p0, LX/Cfk;->c:Landroid/view/ViewGroup;

    move-object v1, v1

    .line 2080740
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2080741
    const v1, 0x7f0d2881

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, LX/E6s;->c:Landroid/support/v7/widget/RecyclerView;

    .line 2080742
    iget-object v0, p0, LX/E6s;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1P1;

    iput-object v0, p0, LX/E6s;->a:LX/1P1;

    .line 2080743
    iget-object v0, p0, LX/E6s;->a:LX/1P1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 2080744
    iget-object v0, p0, LX/E6s;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/E6s;->a:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2080745
    return-void
.end method
