.class public final LX/D12;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/D13;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D15;

.field public final b:Landroid/view/View$OnClickListener;

.field private final c:Landroid/view/View$OnClickListener;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/D15;)V
    .locals 1

    .prologue
    .line 1956233
    iput-object p1, p0, LX/D12;->a:LX/D15;

    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1956234
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1956235
    iput-object v0, p0, LX/D12;->d:LX/0Px;

    .line 1956236
    new-instance v0, LX/D10;

    invoke-direct {v0, p0, p1}, LX/D10;-><init>(LX/D12;LX/D15;)V

    iput-object v0, p0, LX/D12;->c:Landroid/view/View$OnClickListener;

    .line 1956237
    new-instance v0, LX/D11;

    invoke-direct {v0, p0, p1}, LX/D11;-><init>(LX/D12;LX/D15;)V

    iput-object v0, p0, LX/D12;->b:Landroid/view/View$OnClickListener;

    .line 1956238
    return-void
.end method

.method public static a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1956239
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setVisibility(I)V

    .line 1956240
    const v0, 0x7f0d01cf

    invoke-virtual {p0, v0, v1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    .line 1956241
    const v0, 0x7f0d01d1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    .line 1956242
    const v0, 0x7f0d01d0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    .line 1956243
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1956230
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1956231
    const v1, 0x7f030974

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1956232
    new-instance v1, LX/D13;

    invoke-direct {v1, v0}, LX/D13;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1956198
    check-cast p1, LX/D13;

    const/4 v2, 0x0

    .line 1956199
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 1956200
    iget-object v1, p0, LX/D12;->a:LX/D15;

    iget v1, v1, LX/D14;->c:I

    iput v1, v0, LX/1a3;->width:I

    .line 1956201
    if-nez p2, :cond_0

    iget-object v1, p0, LX/D12;->a:LX/D15;

    iget v1, v1, LX/D14;->b:F

    float-to-int v1, v1

    :goto_0
    iget-object v3, p0, LX/D12;->a:LX/D15;

    iget v3, v3, LX/D14;->b:F

    float-to-int v3, v3

    invoke-virtual {v0, v1, v2, v3, v2}, LX/1a3;->setMargins(IIII)V

    .line 1956202
    iget-object v0, p0, LX/D12;->d:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    .line 1956203
    iget-object v1, p1, LX/D13;->m:Lcom/facebook/fig/listitem/FigListItem;

    .line 1956204
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->jo_()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1956205
    iget-object v2, p0, LX/D12;->a:LX/D15;

    iget-object v2, v2, LX/D15;->g:Landroid/content/Context;

    const v3, 0x7f0a00fa

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setActionIconColor(I)V

    .line 1956206
    const v2, 0x7f0d01d2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/fig/listitem/FigListItem;->setTag(ILjava/lang/Object;)V

    .line 1956207
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1956208
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleMaxLines(I)V

    .line 1956209
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleTextAppearenceType(I)V

    .line 1956210
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 1956211
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 1956212
    iget-object v2, p0, LX/D12;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1956213
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 1956214
    const/4 p2, 0x0

    .line 1956215
    iget-object v3, p1, LX/D13;->l:Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    .line 1956216
    if-nez v1, :cond_1

    .line 1956217
    invoke-static {v3}, LX/D12;->a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)V

    .line 1956218
    :goto_1
    return-void

    :cond_0
    move v1, v2

    .line 1956219
    goto :goto_0

    .line 1956220
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    .line 1956221
    invoke-virtual {v2}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v4

    .line 1956222
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->PLACE_INFO:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1956223
    invoke-static {v3}, LX/D12;->a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)V

    goto :goto_1

    .line 1956224
    :cond_2
    invoke-virtual {v3, p2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setVisibility(I)V

    .line 1956225
    iget-object v4, p0, LX/D12;->a:LX/D15;

    iget-object v4, v4, LX/D15;->h:LX/BiM;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v2, v5}, LX/BiM;->a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;LX/Bi6;)V

    .line 1956226
    iget-object v4, p0, LX/D12;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1956227
    const v4, 0x7f0d01cf

    invoke-virtual {v3, v4, v2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    .line 1956228
    const v2, 0x7f0d01d1

    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->j()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    .line 1956229
    const v2, 0x7f0d01d0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1956197
    iget-object v0, p0, LX/D12;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
