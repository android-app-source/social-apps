.class public final LX/Dvy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2059472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2059471
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;

    invoke-direct {v0, p1}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2059470
    new-array v0, p1, [Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;

    return-object v0
.end method
