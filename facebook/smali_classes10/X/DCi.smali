.class public final LX/DCi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1974471
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1974472
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1974473
    :goto_0
    return v1

    .line 1974474
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1974475
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1974476
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1974477
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1974478
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1974479
    const-string v6, "context_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1974480
    invoke-static {p0, p1}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1974481
    :cond_2
    const-string v6, "question_id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1974482
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1974483
    :cond_3
    const-string v6, "question_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1974484
    invoke-static {p0, p1}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1974485
    :cond_4
    const-string v6, "thank_you_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1974486
    invoke-static {p0, p1}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1974487
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1974488
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1974489
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1974490
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1974491
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1974492
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1974493
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1974494
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1974495
    if-eqz v0, :cond_0

    .line 1974496
    const-string v1, "context_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1974497
    invoke-static {p0, v0, p2, p3}, LX/5Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1974498
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1974499
    if-eqz v0, :cond_1

    .line 1974500
    const-string v1, "question_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1974501
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1974502
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1974503
    if-eqz v0, :cond_2

    .line 1974504
    const-string v1, "question_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1974505
    invoke-static {p0, v0, p2, p3}, LX/5Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1974506
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1974507
    if-eqz v0, :cond_3

    .line 1974508
    const-string v1, "thank_you_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1974509
    invoke-static {p0, v0, p2, p3}, LX/5Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1974510
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1974511
    return-void
.end method
