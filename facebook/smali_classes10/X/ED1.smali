.class public LX/ED1;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/ED0;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/EFy;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/EIC;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/EGE;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;

.field public f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/EFy;LX/0Px;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/EFy;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/rtc/interfaces/SourcedConferenceCall;",
            "LX/0Px",
            "<",
            "LX/EGE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2090832
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2090833
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2090834
    iput-object v0, p0, LX/ED1;->c:LX/0Ot;

    .line 2090835
    iput-object p1, p0, LX/ED1;->e:Ljava/lang/String;

    .line 2090836
    iput-object p2, p0, LX/ED1;->a:LX/EFy;

    .line 2090837
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/ED1;->b:Ljava/util/Map;

    .line 2090838
    invoke-virtual {p0, p3}, LX/ED1;->a(LX/0Px;)V

    .line 2090839
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2090840
    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2090776
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2090777
    :cond_0
    :goto_0
    return-void

    .line 2090778
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 2090779
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public static c(LX/ED1;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2090842
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2090843
    :cond_0
    :goto_0
    return-void

    .line 2090844
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, LX/ED1;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2090845
    iget-object v0, p0, LX/ED1;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2090846
    iget-object v0, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2090847
    invoke-virtual {p0, v1}, LX/1OM;->i_(I)V

    goto :goto_0

    .line 2090848
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2090841
    iget-object v0, p0, LX/ED1;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    iget-object v0, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2090827
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031234

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2090828
    new-instance v1, LX/ED0;

    invoke-direct {v1, p0, v0}, LX/ED0;-><init>(LX/ED1;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;)LX/EIC;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2090829
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090830
    const/4 v0, 0x0

    .line 2090831
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/ED1;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EIC;

    goto :goto_0
.end method

.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/EGE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2090815
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2090816
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2090817
    iget-object v4, v0, LX/EGE;->b:Ljava/lang/String;

    iget-object v5, p0, LX/ED1;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2090818
    invoke-virtual {v0}, LX/EGE;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2090819
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2090820
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2090821
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_2

    .line 2090822
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/ED1;->d:LX/0Px;

    .line 2090823
    :goto_1
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2090824
    return-void

    .line 2090825
    :cond_2
    new-instance v0, LX/ECy;

    invoke-direct {v0, p0}, LX/ECy;-><init>(LX/ED1;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2090826
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/ED1;->d:LX/0Px;

    goto :goto_1
.end method

.method public final bridge synthetic a(LX/1a1;)V
    .locals 0

    .prologue
    .line 2090814
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 7

    .prologue
    .line 2090789
    check-cast p1, LX/ED0;

    .line 2090790
    iget-object v0, p0, LX/ED1;->d:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    const/4 p2, -0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2090791
    iget-object v1, p1, LX/ED0;->l:LX/ED1;

    iget-object v1, v1, LX/ED1;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    .line 2090792
    iget-object v4, v1, LX/EDx;->af:LX/EGE;

    move-object v4, v4

    .line 2090793
    if-eqz v4, :cond_2

    iget-object v1, v0, LX/EGE;->b:Ljava/lang/String;

    iget-object v5, v4, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p1, LX/ED0;->r:Z

    .line 2090794
    iget-object v1, p1, LX/ED0;->o:Lcom/facebook/user/tiles/UserTileView;

    new-instance v5, Lcom/facebook/user/model/UserKey;

    sget-object v6, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object p0, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-direct {v5, v6, p0}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-static {v5}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2090795
    iget-object v1, p1, LX/ED0;->m:Landroid/view/View;

    new-instance v5, LX/ECz;

    invoke-direct {v5, p1, v4, v0}, LX/ECz;-><init>(LX/ED0;LX/EGE;LX/EGE;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2090796
    iget-object v4, p1, LX/ED0;->q:Landroid/view/View;

    iget-boolean v1, p1, LX/ED0;->r:Z

    if-eqz v1, :cond_3

    iget-object v1, p1, LX/ED0;->l:LX/ED1;

    iget-boolean v1, v1, LX/ED1;->f:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2090797
    iget-object v1, p1, LX/ED0;->l:LX/ED1;

    iget-object v1, v1, LX/ED1;->b:Ljava/util/Map;

    iget-object v4, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EIC;

    .line 2090798
    iget-boolean v4, v0, LX/EGE;->i:Z

    if-eqz v4, :cond_0

    if-nez v1, :cond_5

    .line 2090799
    :cond_0
    iget-object v1, p1, LX/ED0;->o:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v1, v2}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2090800
    iget-object v1, p1, LX/ED0;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2090801
    iget-boolean v1, p1, LX/ED0;->r:Z

    if-eqz v1, :cond_4

    .line 2090802
    iget-object v1, p1, LX/ED0;->p:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2090803
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v1, v2

    .line 2090804
    goto :goto_0

    :cond_3
    move v1, v3

    .line 2090805
    goto :goto_1

    .line 2090806
    :cond_4
    iget-object v1, p1, LX/ED0;->p:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2090807
    :cond_5
    iget-object v4, p1, LX/ED0;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v4

    if-lez v4, :cond_6

    iget-object v4, p1, LX/ED0;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v2, v1, :cond_1

    .line 2090808
    :cond_6
    iget-object v2, p1, LX/ED0;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2090809
    invoke-static {v1}, LX/ED1;->a(Landroid/view/View;)V

    .line 2090810
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, p2, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, LX/EIC;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2090811
    iget-object v2, p1, LX/ED0;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2090812
    iget-object v1, p1, LX/ED0;->o:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v1, v3}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2090813
    iget-object v1, p1, LX/ED0;->p:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2090780
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2090781
    :goto_0
    return v0

    .line 2090782
    :cond_0
    iget-object v0, p0, LX/ED1;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EIC;

    .line 2090783
    if-nez v0, :cond_1

    move v0, v1

    .line 2090784
    goto :goto_0

    .line 2090785
    :cond_1
    iget-object v1, p0, LX/ED1;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090786
    invoke-static {v0}, LX/ED1;->a(Landroid/view/View;)V

    .line 2090787
    invoke-static {p0, p1}, LX/ED1;->c(LX/ED1;Ljava/lang/String;)V

    .line 2090788
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2090775
    iget-object v0, p0, LX/ED1;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
