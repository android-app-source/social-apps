.class public final LX/Dzb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 0

    .prologue
    .line 2067098
    iput-object p1, p0, LX/Dzb;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x7096d1aa

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2067099
    iget-object v1, p0, LX/Dzb;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/8AA;

    sget-object v3, LX/8AB;->PLACE_CREATION:LX/8AB;

    invoke-direct {v2, v3}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v2}, LX/8AA;->l()LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->i()LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->j()LX/8AA;

    move-result-object v2

    sget-object v3, LX/8A9;->LAUNCH_GENERIC_CROPPER:LX/8A9;

    invoke-virtual {v2, v3}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v1

    .line 2067100
    iget-object v2, p0, LX/Dzb;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->b:Lcom/facebook/content/SecureContextHelper;

    const/4 v3, 0x4

    iget-object v4, p0, LX/Dzb;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-interface {v2, v1, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2067101
    iget-object v1, p0, LX/Dzb;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v1, v1, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v2, p0, LX/Dzb;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    .line 2067102
    iget-object v3, v1, LX/96B;->a:LX/0Zb;

    const-string v4, "camera_button_tapped"

    invoke-static {v1, v2, v4}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2067103
    iget-object v1, p0, LX/Dzb;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v1, v1, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v2, p0, LX/Dzb;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v3, LX/96A;->PHOTO_PICKER:LX/96A;

    invoke-virtual {v1, v2, v3}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    .line 2067104
    const v1, -0x799cd5fe

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
