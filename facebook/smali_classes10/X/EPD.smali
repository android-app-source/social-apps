.class public final LX/EPD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic b:LX/1Ps;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/1Ps;)V
    .locals 0

    .prologue
    .line 2116187
    iput-object p1, p0, LX/EPD;->c:Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

    iput-object p2, p0, LX/EPD;->a:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object p3, p0, LX/EPD;->b:LX/1Ps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x1176965c

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2116188
    iget-object v0, p0, LX/EPD;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2116189
    iget-object v0, p0, LX/EPD;->c:Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

    iget-object v2, v0, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->d:LX/1nD;

    iget-object v0, p0, LX/EPD;->b:LX/1Ps;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    .line 2116190
    iget-object v3, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v0, v3

    .line 2116191
    iget-object v3, p0, LX/EPD;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/EPD;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/8ci;->h:LX/8ci;

    invoke-virtual {v2, v0, v3, v4, v5}, LX/1nD;->c(Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;LX/8ci;)Landroid/content/Intent;

    move-result-object v0

    .line 2116192
    iget-object v2, p0, LX/EPD;->c:Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/EPD;->c:Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

    iget-object v3, v3, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->f:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2116193
    :cond_0
    const v0, 0x3bc97c4a

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
