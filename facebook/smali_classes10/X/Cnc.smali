.class public LX/Cnc;
.super LX/CnT;
.source ""

# interfaces
.implements LX/02k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/BylineBlockView;",
        "Lcom/facebook/richdocument/model/data/BylineBlockData;",
        ">;",
        "LX/02k;"
    }
.end annotation


# instance fields
.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;)V
    .locals 2

    .prologue
    .line 1934003
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934004
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, LX/Cnc;

    const/16 v0, 0x455

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object p1, p0, LX/Cnc;->d:LX/0Ot;

    .line 1934005
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 11

    .prologue
    .line 1934006
    check-cast p1, LX/CmD;

    const/4 v2, 0x0

    .line 1934007
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934008
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    invoke-interface {v0, v2}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1934009
    iget-object v0, p1, LX/CmD;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;

    move-object v0, v0

    .line 1934010
    iget-object v1, p1, LX/CmD;->b:Ljava/util/List;

    move-object v1, v1

    .line 1934011
    new-instance v3, LX/Cle;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/Cle;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, LX/Cle;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;)LX/Cle;

    move-result-object v0

    invoke-virtual {v0}, LX/Cle;->a()LX/Clf;

    move-result-object v3

    .line 1934012
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934013
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    .line 1934014
    iget-object v4, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934015
    iget-object v5, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, v5

    .line 1934016
    invoke-virtual {v4, v3}, LX/CtG;->setText(LX/Clf;)V

    .line 1934017
    iget-object v4, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1934018
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1934019
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineProfileModel;

    .line 1934020
    new-instance v5, LX/Cnb;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineProfileModel;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineProfileModel;->b()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_1
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineProfileModel;->b()LX/1Fb;

    move-result-object v6

    invoke-interface {v6}, LX/1Fb;->c()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineProfileModel;->b()LX/1Fb;

    move-result-object v7

    invoke-interface {v7}, LX/1Fb;->c()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    new-instance v7, LX/Cna;

    invoke-direct {v7, p0, v0}, LX/Cna;-><init>(LX/Cnc;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineProfileModel;)V

    invoke-direct {v5, v1, v6, v7}, LX/Cnb;-><init>(Landroid/net/Uri;FLandroid/view/View$OnClickListener;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v1, v2

    goto :goto_1

    .line 1934021
    :cond_1
    invoke-interface {p1}, LX/Clr;->lw_()LX/Cml;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/Clr;->lw_()LX/Cml;

    move-result-object v0

    invoke-interface {v0}, LX/Cml;->b()LX/Cmr;

    move-result-object v2

    .line 1934022
    :cond_2
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934023
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1934024
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->i:LX/Cmr;

    .line 1934025
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v10, :cond_3

    sget-object v1, LX/Cmr;->CENTER:LX/Cmr;

    if-ne v2, v1, :cond_6

    .line 1934026
    :cond_3
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1934027
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->f:Landroid/widget/LinearLayout;

    invoke-static {v1, v4}, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->a(Landroid/view/View;Landroid/view/View;)V

    .line 1934028
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Cnb;

    .line 1934029
    new-instance v5, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 1934030
    iget v6, v1, LX/Cnb;->b:F

    move v6, v6

    .line 1934031
    invoke-virtual {v5, v6}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1934032
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x2

    const/4 v8, -0x1

    invoke-direct {v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1934033
    iget v7, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->g:I

    invoke-virtual {v6, v9, v9, v7, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1934034
    invoke-virtual {v5, v9}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1934035
    invoke-virtual {v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1934036
    iget v6, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->h:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget v7, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->h:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v5, v6, v7, v8}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1934037
    iget-object v6, v1, LX/Cnb;->a:Landroid/net/Uri;

    move-object v6, v6

    .line 1934038
    if-nez v6, :cond_5

    .line 1934039
    const v6, 0x7f02111f

    invoke-virtual {v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundResource(I)V

    .line 1934040
    :goto_3
    iget-object v6, v1, LX/Cnb;->c:Landroid/view/View$OnClickListener;

    move-object v6, v6

    .line 1934041
    if-eqz v6, :cond_4

    .line 1934042
    iget-object v6, v1, LX/Cnb;->c:Landroid/view/View$OnClickListener;

    move-object v1, v6

    .line 1934043
    invoke-virtual {v5, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1934044
    :cond_4
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 1934045
    :cond_5
    iget-object v6, v1, LX/Cnb;->a:Landroid/net/Uri;

    move-object v6, v6

    .line 1934046
    sget-object v7, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_3

    .line 1934047
    :cond_6
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v10, :cond_9

    .line 1934048
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->f:Landroid/widget/LinearLayout;

    iget-object v4, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1, v4}, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->a(Landroid/view/View;Landroid/view/View;)V

    .line 1934049
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1934050
    instance-of v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v4, :cond_7

    .line 1934051
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v4, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->g:I

    invoke-virtual {v1, v9, v9, v4, v9}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1934052
    :cond_7
    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Cnb;

    .line 1934053
    iget-object v4, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1934054
    iget v5, v1, LX/Cnb;->b:F

    move v5, v5

    .line 1934055
    invoke-virtual {v4, v5}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1934056
    iget-object v4, v1, LX/Cnb;->a:Landroid/net/Uri;

    move-object v4, v4

    .line 1934057
    if-nez v4, :cond_a

    .line 1934058
    iget-object v4, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v5, 0x7f02111f

    invoke-virtual {v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundResource(I)V

    .line 1934059
    :goto_4
    iget-object v4, v1, LX/Cnb;->c:Landroid/view/View$OnClickListener;

    move-object v4, v4

    .line 1934060
    if-eqz v4, :cond_8

    .line 1934061
    iget-object v4, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1934062
    iget-object v5, v1, LX/Cnb;->c:Landroid/view/View$OnClickListener;

    move-object v1, v5

    .line 1934063
    invoke-virtual {v4, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1934064
    :cond_8
    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget v4, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v5, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->h:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v1, v4, v5, v6}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1934065
    :cond_9
    invoke-static {v0}, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;)V

    .line 1934066
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934067
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->lw_()LX/Cml;

    move-result-object v1

    invoke-interface {v0, v1}, LX/CnG;->a(LX/Cml;)V

    .line 1934068
    return-void

    .line 1934069
    :cond_a
    iget-object v4, v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1934070
    iget-object v5, v1, LX/Cnb;->a:Landroid/net/Uri;

    move-object v5, v5

    .line 1934071
    sget-object v6, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_4
.end method
