.class public final LX/EOx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Ps;

.field public final synthetic b:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;LX/1Ps;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2115581
    iput-object p1, p0, LX/EOx;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

    iput-object p2, p0, LX/EOx;->a:LX/1Ps;

    iput-object p3, p0, LX/EOx;->b:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    iput-object p4, p0, LX/EOx;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v0, 0x1

    const v1, -0x5c429b26

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2115582
    iget-object v0, p0, LX/EOx;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->g:LX/CvY;

    iget-object v1, p0, LX/EOx;->a:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->OPEN_LINK_BY_IMAGE:LX/8ch;

    iget-object v3, p0, LX/EOx;->a:LX/1Ps;

    check-cast v3, LX/CxG;

    iget-object v4, p0, LX/EOx;->b:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    invoke-interface {v3, v4}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v3

    iget-object v4, p0, LX/EOx;->b:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    .line 2115583
    iget-object v5, p0, LX/EOx;->a:LX/1Ps;

    check-cast v5, LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v7

    iget-object v5, p0, LX/EOx;->a:LX/1Ps;

    check-cast v5, LX/CxG;

    iget-object v8, p0, LX/EOx;->b:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    invoke-interface {v5, v8}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v8

    sget-object v9, LX/8ch;->OPEN_LINK_BY_IMAGE:LX/8ch;

    iget-object v5, p0, LX/EOx;->b:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    invoke-virtual {v5}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->m()LX/0am;

    move-result-object v5

    invoke-virtual {v5}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v7, v8, v9, v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/8ch;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2115584
    iget-object v0, p0, LX/EOx;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->f:LX/1xP;

    iget-object v1, p0, LX/EOx;->c:Landroid/content/Context;

    iget-object v2, p0, LX/EOx;->b:Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v11, v11}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    .line 2115585
    const v0, -0x684a73db

    invoke-static {v10, v10, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
