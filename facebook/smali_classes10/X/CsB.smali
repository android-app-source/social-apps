.class public final synthetic LX/CsB;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1941877
    invoke-static {}, LX/CoS;->values()[LX/CoS;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/CsB;->b:[I

    :try_start_0
    sget-object v0, LX/CsB;->b:[I

    sget-object v1, LX/CoS;->EXPAND_COLLAPSE_TRIGGERED:LX/CoS;

    invoke-virtual {v1}, LX/CoS;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v0, LX/CsB;->b:[I

    sget-object v1, LX/CoS;->SCROLL_TRIGGERED:LX/CoS;

    invoke-virtual {v1}, LX/CoS;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    :try_start_2
    sget-object v0, LX/CsB;->b:[I

    sget-object v1, LX/CoS;->ENFORCED:LX/CoS;

    invoke-virtual {v1}, LX/CoS;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 1941878
    :goto_2
    invoke-static {}, LX/CiU;->values()[LX/CiU;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/CsB;->a:[I

    :try_start_3
    sget-object v0, LX/CsB;->a:[I

    sget-object v1, LX/CiU;->ON_PAUSE:LX/CiU;

    invoke-virtual {v1}, LX/CiU;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v0, LX/CsB;->a:[I

    sget-object v1, LX/CiU;->ON_RESUME:LX/CiU;

    invoke-virtual {v1}, LX/CiU;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    return-void

    :catch_0
    goto :goto_4

    :catch_1
    goto :goto_3

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_1

    :catch_4
    goto :goto_0
.end method
