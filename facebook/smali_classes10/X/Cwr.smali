.class public LX/Cwr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/2Sc;


# direct methods
.method public constructor <init>(LX/2Sc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951147
    iput-object p1, p0, LX/Cwr;->a:LX/2Sc;

    .line 1951148
    return-void
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/NullStateModuleSuggestionUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1951149
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1951150
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;

    .line 1951151
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->m()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_1

    move v1, v4

    :goto_1
    if-eqz v1, :cond_3

    move v1, v4

    :goto_2
    if-nez v1, :cond_0

    .line 1951152
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->m()LX/1vs;

    move-result-object v1

    iget-object v8, v1, LX/1vs;->a:LX/15i;

    iget v9, v1, LX/1vs;->b:I

    .line 1951153
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->m()LX/1vs;

    move-result-object v1

    iget-object v10, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v10, v1, v4}, LX/15i;->g(II)I

    move-result v11

    .line 1951154
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_5

    .line 1951155
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1951156
    invoke-virtual {v3, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1951157
    :goto_3
    new-instance v12, LX/CwP;

    invoke-direct {v12}, LX/CwP;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->q()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->q()Ljava/lang/String;

    move-result-object v3

    :goto_4
    invoke-virtual {v12, v3}, LX/CwP;->a(Ljava/lang/String;)LX/CwP;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->r()Ljava/lang/String;

    move-result-object v12

    .line 1951158
    iput-object v12, v3, LX/CwP;->b:Ljava/lang/String;

    .line 1951159
    move-object v3, v3

    .line 1951160
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->p()Ljava/lang/String;

    move-result-object v12

    .line 1951161
    iput-object v12, v3, LX/CwP;->c:Ljava/lang/String;

    .line 1951162
    move-object v3, v3

    .line 1951163
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->o()Ljava/lang/String;

    move-result-object v12

    .line 1951164
    iput-object v12, v3, LX/CwP;->d:Ljava/lang/String;

    .line 1951165
    move-object v3, v3

    .line 1951166
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->n()Ljava/lang/String;

    move-result-object v12

    .line 1951167
    iput-object v12, v3, LX/CwP;->e:Ljava/lang/String;

    .line 1951168
    move-object v12, v3

    .line 1951169
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->a()Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->a()Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 1951170
    :goto_5
    iput-object v3, v12, LX/CwP;->f:Ljava/lang/String;

    .line 1951171
    move-object v12, v12

    .line 1951172
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->a()Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->a()Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel$EntityModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 1951173
    :goto_6
    iput-object v3, v12, LX/CwP;->i:Ljava/lang/String;

    .line 1951174
    move-object v3, v12

    .line 1951175
    invoke-virtual {v8, v9, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 1951176
    iput-object v8, v3, LX/CwP;->j:Ljava/lang/String;

    .line 1951177
    move-object v3, v3

    .line 1951178
    invoke-virtual {v10, v11, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 1951179
    iput-object v8, v3, LX/CwP;->k:Ljava/lang/String;

    .line 1951180
    move-object v3, v3

    .line 1951181
    iput-object v1, v3, LX/CwP;->l:Ljava/lang/String;

    .line 1951182
    move-object v1, v3

    .line 1951183
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->s()Ljava/lang/String;

    move-result-object v3

    .line 1951184
    iput-object v3, v1, LX/CwP;->m:Ljava/lang/String;

    .line 1951185
    move-object v1, v1

    .line 1951186
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->j()Z

    move-result v3

    .line 1951187
    iput-boolean v3, v1, LX/CwP;->n:Z

    .line 1951188
    move-object v1, v1

    .line 1951189
    invoke-virtual {v1, p1}, LX/CwP;->n(Ljava/lang/String;)LX/CwP;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->l()Z

    move-result v0

    .line 1951190
    iput-boolean v0, v1, LX/CwP;->p:Z

    .line 1951191
    move-object v0, v1

    .line 1951192
    invoke-virtual {v0}, LX/CwP;->a()Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1951193
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->m()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1951194
    invoke-virtual {v3, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    move v1, v4

    goto/16 :goto_1

    :cond_2
    move v1, v5

    goto/16 :goto_1

    .line 1951195
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel$SuggestionsModel;->m()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1951196
    invoke-virtual {v3, v1, v4}, LX/15i;->g(II)I

    move-result v1

    if-nez v1, :cond_4

    move v1, v4

    goto/16 :goto_2

    :cond_4
    move v1, v5

    goto/16 :goto_2

    :cond_5
    move-object v1, v2

    .line 1951197
    goto/16 :goto_3

    :cond_6
    move-object v3, p1

    .line 1951198
    goto/16 :goto_4

    :cond_7
    move-object v3, v2

    goto :goto_5

    :cond_8
    move-object v3, v2

    goto :goto_6

    .line 1951199
    :cond_9
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel;)LX/Cwv;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1951200
    new-instance v1, LX/Cwv;

    invoke-direct {v1}, LX/Cwv;-><init>()V

    .line 1951201
    if-nez p1, :cond_1

    .line 1951202
    :try_start_0
    new-instance v0, LX/7C4;

    sget-object v2, LX/3Ql;->FETCH_NULL_STATE_MODULES_FAIL:LX/3Ql;

    const-string v3, "Missing provider"

    invoke-direct {v0, v2, v3}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 1951203
    :catch_0
    move-exception v0

    .line 1951204
    iget-object v2, p0, LX/Cwr;->a:LX/2Sc;

    invoke-virtual {v2, v0}, LX/2Sc;->a(LX/7C4;)V

    :cond_0
    move-object v0, v1

    .line 1951205
    :goto_0
    return-object v0

    .line 1951206
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel;->a()LX/0Px;

    move-result-object v0

    .line 1951207
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1951208
    new-instance v0, LX/Cwv;

    invoke-direct {v0}, LX/Cwv;-><init>()V

    goto :goto_0

    .line 1951209
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;

    .line 1951210
    const/4 v4, 0x0

    .line 1951211
    new-instance v7, LX/CwN;

    invoke-direct {v7}, LX/CwN;-><init>()V

    .line 1951212
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->n()LX/0Px;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, LX/Cwr;->a(Ljava/util/List;Ljava/lang/String;)LX/0Px;

    move-result-object v8

    .line 1951213
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v4

    move v6, v4

    :goto_2
    if-ge v5, v9, :cond_4

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    .line 1951214
    iget-boolean p1, v3, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->p:Z

    move v3, p1

    .line 1951215
    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_3
    add-int/2addr v6, v3

    .line 1951216
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    :cond_3
    move v3, v4

    .line 1951217
    goto :goto_3

    .line 1951218
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/CwN;->a(Ljava/lang/String;)LX/CwN;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->l()Ljava/lang/String;

    move-result-object v4

    .line 1951219
    iput-object v4, v3, LX/CwN;->b:Ljava/lang/String;

    .line 1951220
    move-object v3, v3

    .line 1951221
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->o()Ljava/lang/String;

    move-result-object v4

    .line 1951222
    iput-object v4, v3, LX/CwN;->c:Ljava/lang/String;

    .line 1951223
    move-object v3, v3

    .line 1951224
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 1951225
    iput-object v4, v3, LX/CwN;->d:Ljava/lang/String;

    .line 1951226
    move-object v3, v3

    .line 1951227
    iput-object v8, v3, LX/CwN;->e:LX/0Px;

    .line 1951228
    move-object v3, v3

    .line 1951229
    iput v6, v3, LX/CwN;->h:I

    .line 1951230
    move-object v3, v3

    .line 1951231
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->k()I

    move-result v4

    .line 1951232
    iput v4, v3, LX/CwN;->f:I

    .line 1951233
    move-object v3, v3

    .line 1951234
    invoke-virtual {v0}, Lcom/facebook/search/protocol/nullstate/FetchNullStateModulesGraphQLModels$NullStateModuleQueryModel$ProviderModel$ModulesModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/CwN;->e(Ljava/lang/String;)LX/CwN;

    move-result-object v3

    invoke-virtual {v3}, LX/CwN;->a()Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    move-result-object v3

    move-object v0, v3

    .line 1951235
    invoke-virtual {v1, v0}, LX/Cwv;->a(Lcom/facebook/search/model/NullStateModuleCollectionUnit;)V
    :try_end_1
    .catch LX/7C4; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
