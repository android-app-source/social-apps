.class public abstract LX/EtB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<IntermediateResultType:",
        "Ljava/lang/Object;",
        "FinalResultType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/EtA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EtA",
            "<TFinalResultType;*>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/Executor;

.field public c:Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EtB",
            "<TIntermediateResultType;TFinalResultType;>.Finisher<TIntermediateResultType;TFinalResultType;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/EtA;Ljava/util/concurrent/Executor;Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EtA;",
            "Ljava/util/concurrent/Executor;",
            "LX/EtB",
            "<TIntermediateResultType;TFinalResultType;>.Finisher<TIntermediateResultType;TFinalResultType;>;)V"
        }
    .end annotation

    .prologue
    .line 2177316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2177317
    iput-object p1, p0, LX/EtB;->a:LX/EtA;

    .line 2177318
    iput-object p2, p0, LX/EtB;->b:Ljava/util/concurrent/Executor;

    .line 2177319
    iput-object p3, p0, LX/EtB;->c:Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;

    .line 2177320
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method

.method public abstract d()Z
.end method
