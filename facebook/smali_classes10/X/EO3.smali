.class public final LX/EO3;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EO4;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/ENw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2113861
    invoke-static {}, LX/EO4;->q()LX/EO4;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2113862
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2113863
    const-string v0, "SearchResultsOpinionSearchQueryNameListComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2113864
    if-ne p0, p1, :cond_1

    .line 2113865
    :cond_0
    :goto_0
    return v0

    .line 2113866
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2113867
    goto :goto_0

    .line 2113868
    :cond_3
    check-cast p1, LX/EO3;

    .line 2113869
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2113870
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2113871
    if-eq v2, v3, :cond_0

    .line 2113872
    iget v2, p0, LX/EO3;->a:I

    iget v3, p1, LX/EO3;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2113873
    goto :goto_0

    .line 2113874
    :cond_4
    iget-object v2, p0, LX/EO3;->b:LX/0Px;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/EO3;->b:LX/0Px;

    iget-object v3, p1, LX/EO3;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2113875
    goto :goto_0

    .line 2113876
    :cond_6
    iget-object v2, p1, LX/EO3;->b:LX/0Px;

    if-nez v2, :cond_5

    .line 2113877
    :cond_7
    iget-object v2, p0, LX/EO3;->c:LX/ENw;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EO3;->c:LX/ENw;

    iget-object v3, p1, LX/EO3;->c:LX/ENw;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2113878
    goto :goto_0

    .line 2113879
    :cond_8
    iget-object v2, p1, LX/EO3;->c:LX/ENw;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
