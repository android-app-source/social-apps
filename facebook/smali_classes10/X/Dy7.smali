.class public LX/Dy7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/Dy7;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0rq;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0sa;

.field public final d:LX/0tG;

.field public final e:LX/0tE;

.field public final f:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0sa;LX/0tG;LX/0tE;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0rq;",
            ">;",
            "LX/0sa;",
            "LX/0tG;",
            "LX/0tE;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2063966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2063967
    iput-object p1, p0, LX/Dy7;->a:LX/0Ot;

    .line 2063968
    iput-object p2, p0, LX/Dy7;->b:LX/0Ot;

    .line 2063969
    iput-object p3, p0, LX/Dy7;->c:LX/0sa;

    .line 2063970
    iput-object p4, p0, LX/Dy7;->d:LX/0tG;

    .line 2063971
    iput-object p5, p0, LX/Dy7;->e:LX/0tE;

    .line 2063972
    iput-object p6, p0, LX/Dy7;->f:LX/0ad;

    .line 2063973
    return-void
.end method

.method public static a(LX/0QB;)LX/Dy7;
    .locals 10

    .prologue
    .line 2063974
    sget-object v0, LX/Dy7;->g:LX/Dy7;

    if-nez v0, :cond_1

    .line 2063975
    const-class v1, LX/Dy7;

    monitor-enter v1

    .line 2063976
    :try_start_0
    sget-object v0, LX/Dy7;->g:LX/Dy7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2063977
    if-eqz v2, :cond_0

    .line 2063978
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2063979
    new-instance v3, LX/Dy7;

    const/16 v4, 0xafd

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xb24

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v6

    check-cast v6, LX/0sa;

    invoke-static {v0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v7

    check-cast v7, LX/0tG;

    invoke-static {v0}, LX/0tE;->b(LX/0QB;)LX/0tE;

    move-result-object v8

    check-cast v8, LX/0tE;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, LX/Dy7;-><init>(LX/0Ot;LX/0Ot;LX/0sa;LX/0tG;LX/0tE;LX/0ad;)V

    .line 2063980
    move-object v0, v3

    .line 2063981
    sput-object v0, LX/Dy7;->g:LX/Dy7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2063982
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2063983
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2063984
    :cond_1
    sget-object v0, LX/Dy7;->g:LX/Dy7;

    return-object v0

    .line 2063985
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2063986
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
