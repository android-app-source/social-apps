.class public final enum LX/Epk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Epk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Epk;

.field public static final enum ERROR:LX/Epk;

.field public static final enum LOADING_MOVING_SPINNER:LX/Epk;

.field public static final enum READY:LX/Epk;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2170528
    new-instance v0, LX/Epk;

    const-string v1, "LOADING_MOVING_SPINNER"

    invoke-direct {v0, v1, v2}, LX/Epk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Epk;->LOADING_MOVING_SPINNER:LX/Epk;

    .line 2170529
    new-instance v0, LX/Epk;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, LX/Epk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Epk;->ERROR:LX/Epk;

    .line 2170530
    new-instance v0, LX/Epk;

    const-string v1, "READY"

    invoke-direct {v0, v1, v4}, LX/Epk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Epk;->READY:LX/Epk;

    .line 2170531
    const/4 v0, 0x3

    new-array v0, v0, [LX/Epk;

    sget-object v1, LX/Epk;->LOADING_MOVING_SPINNER:LX/Epk;

    aput-object v1, v0, v2

    sget-object v1, LX/Epk;->ERROR:LX/Epk;

    aput-object v1, v0, v3

    sget-object v1, LX/Epk;->READY:LX/Epk;

    aput-object v1, v0, v4

    sput-object v0, LX/Epk;->$VALUES:[LX/Epk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2170532
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Epk;
    .locals 1

    .prologue
    .line 2170533
    const-class v0, LX/Epk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Epk;

    return-object v0
.end method

.method public static values()[LX/Epk;
    .locals 1

    .prologue
    .line 2170534
    sget-object v0, LX/Epk;->$VALUES:[LX/Epk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Epk;

    return-object v0
.end method
