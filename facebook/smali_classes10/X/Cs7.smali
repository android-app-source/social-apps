.class public LX/Cs7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "LX/CnQ;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "LX/CnQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1941852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941853
    new-instance v0, Ljava/util/TreeSet;

    sget-object v1, LX/Cs6;->a:LX/Cs6;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/Cs7;->a:Ljava/util/TreeSet;

    .line 1941854
    return-void
.end method


# virtual methods
.method public final a(LX/ClT;)LX/CnQ;
    .locals 3

    .prologue
    .line 1941855
    iget-object v0, p0, LX/Cs7;->a:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1941856
    invoke-interface {v0}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v2

    .line 1941857
    iget-object p0, v2, LX/ClU;->a:LX/ClT;

    move-object v2, p0

    .line 1941858
    invoke-virtual {v2, p1}, LX/ClT;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1941859
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs a([LX/ClR;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LX/ClR;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/CnQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1941843
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1941844
    iget-object v0, p0, LX/Cs7;->a:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1941845
    array-length v4, p1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, p1, v1

    .line 1941846
    invoke-interface {v0}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v6

    .line 1941847
    iget-object p0, v6, LX/ClU;->e:LX/ClR;

    move-object v6, p0

    .line 1941848
    if-ne v6, v5, :cond_1

    .line 1941849
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1941850
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1941851
    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LX/CnQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1941842
    iget-object v0, p0, LX/Cs7;->a:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
