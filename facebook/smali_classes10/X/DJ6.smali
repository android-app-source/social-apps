.class public LX/DJ6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/DJl;

.field public final b:LX/0W9;

.field private final c:LX/0Xl;

.field private d:LX/0Yb;

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

.field public g:LX/21D;

.field public h:Landroid/content/Context;

.field public i:LX/DJr;


# direct methods
.method public constructor <init>(LX/DJl;LX/0W9;LX/0Xl;)V
    .locals 0
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1984862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1984863
    iput-object p1, p0, LX/DJ6;->a:LX/DJl;

    .line 1984864
    iput-object p2, p0, LX/DJ6;->b:LX/0W9;

    .line 1984865
    iput-object p3, p0, LX/DJ6;->c:LX/0Xl;

    .line 1984866
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1984874
    iget-object v0, p0, LX/DJ6;->d:LX/0Yb;

    if-eqz v0, :cond_0

    .line 1984875
    iget-object v0, p0, LX/DJ6;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1984876
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/21D;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1984867
    iput-object p1, p0, LX/DJ6;->e:Ljava/lang/String;

    .line 1984868
    iput-object p2, p0, LX/DJ6;->g:LX/21D;

    .line 1984869
    iput-object p3, p0, LX/DJ6;->h:Landroid/content/Context;

    .line 1984870
    iget-object v0, p0, LX/DJ6;->d:LX/0Yb;

    if-nez v0, :cond_0

    .line 1984871
    iget-object v0, p0, LX/DJ6;->c:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    new-instance v2, LX/DJ5;

    invoke-direct {v2, p0}, LX/DJ5;-><init>(LX/DJ6;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/DJ6;->d:LX/0Yb;

    .line 1984872
    :cond_0
    iget-object v0, p0, LX/DJ6;->d:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1984873
    return-void
.end method
