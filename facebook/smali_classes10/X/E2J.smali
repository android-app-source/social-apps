.class public final LX/E2J;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E2K;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/9uY;

.field public b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/5sc;

.field public d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public e:Ljava/lang/String;

.field public final synthetic f:LX/E2K;


# direct methods
.method public constructor <init>(LX/E2K;)V
    .locals 1

    .prologue
    .line 2072598
    iput-object p1, p0, LX/E2J;->f:LX/E2K;

    .line 2072599
    move-object v0, p1

    .line 2072600
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2072601
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2072602
    const-string v0, "ReactionToggleStateSaveButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2072603
    if-ne p0, p1, :cond_1

    .line 2072604
    :cond_0
    :goto_0
    return v0

    .line 2072605
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2072606
    goto :goto_0

    .line 2072607
    :cond_3
    check-cast p1, LX/E2J;

    .line 2072608
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2072609
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2072610
    if-eq v2, v3, :cond_0

    .line 2072611
    iget-object v2, p0, LX/E2J;->a:LX/9uY;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E2J;->a:LX/9uY;

    iget-object v3, p1, LX/E2J;->a:LX/9uY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2072612
    goto :goto_0

    .line 2072613
    :cond_5
    iget-object v2, p1, LX/E2J;->a:LX/9uY;

    if-nez v2, :cond_4

    .line 2072614
    :cond_6
    iget-object v2, p0, LX/E2J;->b:LX/1Pq;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E2J;->b:LX/1Pq;

    iget-object v3, p1, LX/E2J;->b:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2072615
    goto :goto_0

    .line 2072616
    :cond_8
    iget-object v2, p1, LX/E2J;->b:LX/1Pq;

    if-nez v2, :cond_7

    .line 2072617
    :cond_9
    iget-object v2, p0, LX/E2J;->c:LX/5sc;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E2J;->c:LX/5sc;

    iget-object v3, p1, LX/E2J;->c:LX/5sc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2072618
    goto :goto_0

    .line 2072619
    :cond_b
    iget-object v2, p1, LX/E2J;->c:LX/5sc;

    if-nez v2, :cond_a

    .line 2072620
    :cond_c
    iget-object v2, p0, LX/E2J;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/E2J;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/E2J;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2072621
    goto :goto_0

    .line 2072622
    :cond_e
    iget-object v2, p1, LX/E2J;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_d

    .line 2072623
    :cond_f
    iget-object v2, p0, LX/E2J;->e:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/E2J;->e:Ljava/lang/String;

    iget-object v3, p1, LX/E2J;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2072624
    goto :goto_0

    .line 2072625
    :cond_10
    iget-object v2, p1, LX/E2J;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
