.class public final LX/E0p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$FBCheckinNearbyCityQuery;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/E0q;


# direct methods
.method public constructor <init>(LX/E0q;LX/0TF;)V
    .locals 0

    .prologue
    .line 2069022
    iput-object p1, p0, LX/E0p;->b:LX/E0q;

    iput-object p2, p0, LX/E0p;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2069023
    iget-object v0, p0, LX/E0p;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2069024
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2069025
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;

    .line 2069026
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCheckinNearbyCityQueryModel$ClosestCityModel;

    move-result-object v0

    .line 2069027
    iget-object v1, p0, LX/E0p;->a:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2069028
    return-void
.end method
