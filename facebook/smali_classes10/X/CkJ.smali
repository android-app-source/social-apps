.class public LX/CkJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field private static k:LX/0Xm;


# instance fields
.field private final h:I

.field public final i:LX/Cju;

.field public j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cjt;",
            "LX/CkF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1930564
    const v0, 0x7f0d0177

    sput v0, LX/CkJ;->a:I

    .line 1930565
    const v0, 0x7f0d011b

    sput v0, LX/CkJ;->b:I

    .line 1930566
    const v0, 0x7f0d011c

    sput v0, LX/CkJ;->c:I

    .line 1930567
    const v0, 0x7f0d011d

    sput v0, LX/CkJ;->d:I

    .line 1930568
    const v0, 0x7f0d0176

    sput v0, LX/CkJ;->e:I

    .line 1930569
    const v0, 0x7f0d011e

    sput v0, LX/CkJ;->f:I

    .line 1930570
    const v0, 0x7f0d011f

    sput v0, LX/CkJ;->g:I

    return-void
.end method

.method public constructor <init>(LX/Cju;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1930571
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930572
    iput-object p1, p0, LX/CkJ;->i:LX/Cju;

    .line 1930573
    sget v0, LX/CkJ;->d:I

    invoke-interface {p1, v0}, LX/Cju;->c(I)I

    move-result v0

    iput v0, p0, LX/CkJ;->h:I

    .line 1930574
    new-instance v0, LX/CkE;

    invoke-direct {v0, p0}, LX/CkE;-><init>(LX/CkJ;)V

    iput-object v0, p0, LX/CkJ;->j:Ljava/util/Map;

    .line 1930575
    return-void
.end method

.method private static a(ILandroid/view/View;)I
    .locals 2

    .prologue
    .line 1930576
    const/4 v0, 0x0

    .line 1930577
    instance-of v1, p1, LX/Cjr;

    if-eqz v1, :cond_0

    .line 1930578
    check-cast p1, LX/Cjr;

    .line 1930579
    invoke-interface {p1}, LX/Cjr;->getExtraPaddingBottom()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1930580
    :cond_0
    sub-int v0, p0, v0

    return v0
.end method

.method public static a(LX/0QB;)LX/CkJ;
    .locals 4

    .prologue
    .line 1930581
    const-class v1, LX/CkJ;

    monitor-enter v1

    .line 1930582
    :try_start_0
    sget-object v0, LX/CkJ;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1930583
    sput-object v2, LX/CkJ;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1930584
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1930585
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1930586
    new-instance p0, LX/CkJ;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v3

    check-cast v3, LX/Cju;

    invoke-direct {p0, v3}, LX/CkJ;-><init>(LX/Cju;)V

    .line 1930587
    move-object v0, p0

    .line 1930588
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1930589
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CkJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1930590
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1930591
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/Cjt;LX/Clr;Landroid/view/View;LX/Cjt;LX/Clr;Landroid/view/View;)I
    .locals 3
    .param p2    # LX/Clr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/Clr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1930592
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 1930593
    :goto_0
    if-eqz p2, :cond_1

    instance-of v0, p2, LX/Cm5;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, LX/Cm5;

    invoke-virtual {v0}, LX/Cm5;->jf_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CkJ;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/CkI;

    if-nez v0, :cond_1

    .line 1930594
    check-cast p2, LX/Cm5;

    invoke-virtual {p2}, LX/Cm5;->je_()LX/Cmv;

    move-result-object v0

    .line 1930595
    iget-object v2, p0, LX/CkJ;->i:LX/Cju;

    invoke-static {v0, v2}, LX/Cn1;->a(LX/Cmv;LX/Cju;)I

    move-result v0

    move v2, v0

    .line 1930596
    :goto_1
    if-eqz p5, :cond_4

    instance-of v0, p5, LX/Cm5;

    if-eqz v0, :cond_4

    move-object v0, p5

    check-cast v0, LX/Cm5;

    invoke-virtual {v0}, LX/Cm5;->jd_()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/CkJ;->j:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/CkI;

    if-nez v0, :cond_4

    .line 1930597
    check-cast p5, LX/Cm5;

    invoke-virtual {p5}, LX/Cm5;->jc_()LX/Cmv;

    move-result-object v0

    .line 1930598
    iget-object v1, p0, LX/CkJ;->i:LX/Cju;

    invoke-static {v0, v1}, LX/Cn1;->a(LX/Cmv;LX/Cju;)I

    move-result v0

    .line 1930599
    :goto_2
    sget-object v1, LX/Cjt;->VIDEO_SEEK_BAR:LX/Cjt;

    if-ne p4, v1, :cond_7

    .line 1930600
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, p3}, LX/CkJ;->a(ILandroid/view/View;)I

    move-result v0

    .line 1930601
    :goto_3
    return v0

    .line 1930602
    :cond_0
    invoke-virtual {p6}, Landroid/view/View;->getContext()Landroid/content/Context;

    goto :goto_0

    .line 1930603
    :cond_1
    iget-object v0, p0, LX/CkJ;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1930604
    iget-object v0, p0, LX/CkJ;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CkF;

    invoke-interface {v0, p4}, LX/CkF;->a(LX/Cjt;)I

    move-result v0

    move v2, v0

    goto :goto_1

    .line 1930605
    :cond_2
    sget-object v0, LX/Cjt;->NONE:LX/Cjt;

    if-ne p1, v0, :cond_3

    move v2, v1

    .line 1930606
    goto :goto_1

    .line 1930607
    :cond_3
    iget v0, p0, LX/CkJ;->h:I

    move v2, v0

    goto :goto_1

    .line 1930608
    :cond_4
    iget-object v0, p0, LX/CkJ;->j:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1930609
    iget-object v0, p0, LX/CkJ;->j:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CkF;

    invoke-interface {v0, p1}, LX/CkF;->b(LX/Cjt;)I

    move-result v0

    goto :goto_2

    .line 1930610
    :cond_5
    sget-object v0, LX/Cjt;->NONE:LX/Cjt;

    if-ne p4, v0, :cond_6

    move v0, v1

    .line 1930611
    goto :goto_2

    .line 1930612
    :cond_6
    iget v0, p0, LX/CkJ;->h:I

    goto :goto_2

    .line 1930613
    :cond_7
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p3}, LX/CkJ;->a(ILandroid/view/View;)I

    move-result v0

    goto :goto_3
.end method

.method public final a(LX/CnQ;LX/CnQ;)I
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1930614
    if-nez p1, :cond_2

    move-object v0, v2

    :goto_0
    invoke-static {v0}, LX/Cjt;->from(LX/ClU;)LX/Cjt;

    move-result-object v1

    .line 1930615
    if-nez p2, :cond_3

    move-object v0, v2

    :goto_1
    invoke-static {v0}, LX/Cjt;->from(LX/ClU;)LX/Cjt;

    move-result-object v4

    .line 1930616
    if-ne v1, v4, :cond_4

    .line 1930617
    invoke-interface {p1}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1930618
    invoke-interface {p1}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v0

    .line 1930619
    iget-object v3, v0, LX/ClU;->a:LX/ClT;

    move-object v0, v3

    .line 1930620
    sget-object v3, LX/ClT;->TITLE:LX/ClT;

    if-ne v0, v3, :cond_4

    invoke-interface {p2}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v0

    .line 1930621
    iget-object v3, v0, LX/ClU;->a:LX/ClT;

    move-object v0, v3

    .line 1930622
    sget-object v3, LX/ClT;->SUBTITLE:LX/ClT;

    if-ne v0, v3, :cond_4

    instance-of v0, p1, Lcom/facebook/richdocument/view/widget/TextAnnotationView;

    if-eqz v0, :cond_4

    instance-of v0, p2, Lcom/facebook/richdocument/view/widget/TextAnnotationView;

    if-eqz v0, :cond_4

    .line 1930623
    check-cast p1, Lcom/facebook/richdocument/view/widget/TextAnnotationView;

    .line 1930624
    check-cast p2, Lcom/facebook/richdocument/view/widget/TextAnnotationView;

    .line 1930625
    iget-object v0, p1, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object v0, v0

    .line 1930626
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredWidth()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1930627
    iget-object v0, p2, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object v0, v0

    .line 1930628
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_1

    .line 1930629
    :cond_0
    iget-object v0, p1, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object v0, v0

    .line 1930630
    invoke-virtual {v0, v5, v5}, Lcom/facebook/richdocument/view/widget/RichTextView;->measure(II)V

    .line 1930631
    iget-object v0, p2, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object v0, v0

    .line 1930632
    invoke-virtual {v0, v5, v5}, Lcom/facebook/richdocument/view/widget/RichTextView;->measure(II)V

    .line 1930633
    :cond_1
    iget-object v0, p1, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object v0, v0

    .line 1930634
    iget v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->g:I

    move v0, v1

    .line 1930635
    iget-object v1, p2, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object v1, v1

    .line 1930636
    iget v2, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->h:I

    move v1, v2

    .line 1930637
    add-int/2addr v0, v1

    .line 1930638
    :goto_2
    return v0

    .line 1930639
    :cond_2
    invoke-interface {p1}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v0

    goto :goto_0

    .line 1930640
    :cond_3
    invoke-interface {p2}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v0

    goto :goto_1

    .line 1930641
    :cond_4
    if-eqz p1, :cond_5

    invoke-interface {p1}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v3

    :goto_3
    if-eqz p2, :cond_6

    invoke-interface {p2}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v6

    :goto_4
    move-object v0, p0

    move-object v5, v2

    invoke-virtual/range {v0 .. v6}, LX/CkJ;->a(LX/Cjt;LX/Clr;Landroid/view/View;LX/Cjt;LX/Clr;Landroid/view/View;)I

    move-result v0

    goto :goto_2

    :cond_5
    move-object v3, v2

    goto :goto_3

    :cond_6
    move-object v6, v2

    goto :goto_4
.end method
