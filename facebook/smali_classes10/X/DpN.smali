.class public LX/DpN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;


# instance fields
.field public final body:LX/DpO;

.field public final date_micros:Ljava/lang/Long;

.field public final msg_from:LX/DpM;

.field public final msg_to:LX/DpM;

.field public final nonce:[B

.field public final thread_fbid:Ljava/lang/Long;

.field public final type:Ljava/lang/Integer;

.field public final version:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0xb

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/16 v4, 0xc

    const/16 v3, 0xa

    .line 2044727
    new-instance v0, LX/1sv;

    const-string v1, "Packet"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpN;->b:LX/1sv;

    .line 2044728
    new-instance v0, LX/1sw;

    const-string v1, "version"

    invoke-direct {v0, v1, v6, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpN;->c:LX/1sw;

    .line 2044729
    new-instance v0, LX/1sw;

    const-string v1, "msg_to"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpN;->d:LX/1sw;

    .line 2044730
    new-instance v0, LX/1sw;

    const-string v1, "msg_from"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpN;->e:LX/1sw;

    .line 2044731
    new-instance v0, LX/1sw;

    const-string v1, "date_micros"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpN;->f:LX/1sw;

    .line 2044732
    new-instance v0, LX/1sw;

    const-string v1, "type"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpN;->g:LX/1sw;

    .line 2044733
    new-instance v0, LX/1sw;

    const-string v1, "body"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpN;->h:LX/1sw;

    .line 2044734
    new-instance v0, LX/1sw;

    const-string v1, "nonce"

    invoke-direct {v0, v1, v7, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpN;->i:LX/1sw;

    .line 2044735
    new-instance v0, LX/1sw;

    const-string v1, "thread_fbid"

    invoke-direct {v0, v1, v3, v7}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpN;->j:LX/1sw;

    .line 2044736
    sput-boolean v5, LX/DpN;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;LX/DpM;LX/DpM;Ljava/lang/Long;Ljava/lang/Integer;LX/DpO;[BLjava/lang/Long;)V
    .locals 0

    .prologue
    .line 2044717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2044718
    iput-object p1, p0, LX/DpN;->version:Ljava/lang/Integer;

    .line 2044719
    iput-object p2, p0, LX/DpN;->msg_to:LX/DpM;

    .line 2044720
    iput-object p3, p0, LX/DpN;->msg_from:LX/DpM;

    .line 2044721
    iput-object p4, p0, LX/DpN;->date_micros:Ljava/lang/Long;

    .line 2044722
    iput-object p5, p0, LX/DpN;->type:Ljava/lang/Integer;

    .line 2044723
    iput-object p6, p0, LX/DpN;->body:LX/DpO;

    .line 2044724
    iput-object p7, p0, LX/DpN;->nonce:[B

    .line 2044725
    iput-object p8, p0, LX/DpN;->thread_fbid:Ljava/lang/Long;

    .line 2044726
    return-void
.end method

.method private static a(LX/DpN;)V
    .locals 3

    .prologue
    .line 2044714
    iget-object v0, p0, LX/DpN;->type:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/DpQ;->a:LX/1sn;

    iget-object v1, p0, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2044715
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'type\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2044716
    :cond_0
    return-void
.end method

.method public static b(LX/1su;)LX/DpN;
    .locals 15

    .prologue
    const/16 v14, 0xa

    const/16 v13, 0x8

    const/16 v12, 0xc

    const/4 v8, 0x0

    .line 2044668
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v7, v8

    move-object v6, v8

    move-object v5, v8

    move-object v4, v8

    move-object v3, v8

    move-object v2, v8

    move-object v1, v8

    .line 2044669
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 2044670
    iget-byte v9, v0, LX/1sw;->b:B

    if-eqz v9, :cond_9

    .line 2044671
    iget-short v9, v0, LX/1sw;->c:S

    packed-switch v9, :pswitch_data_0

    .line 2044672
    :pswitch_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044673
    :pswitch_1
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v13, :cond_0

    .line 2044674
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 2044675
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044676
    :pswitch_2
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_1

    .line 2044677
    invoke-static {p0}, LX/DpM;->b(LX/1su;)LX/DpM;

    move-result-object v2

    goto :goto_0

    .line 2044678
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044679
    :pswitch_3
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_2

    .line 2044680
    invoke-static {p0}, LX/DpM;->b(LX/1su;)LX/DpM;

    move-result-object v3

    goto :goto_0

    .line 2044681
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044682
    :pswitch_4
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v14, :cond_3

    .line 2044683
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 2044684
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044685
    :pswitch_5
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v13, :cond_4

    .line 2044686
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0

    .line 2044687
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044688
    :pswitch_6
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_6

    .line 2044689
    new-instance v0, LX/DpO;

    invoke-direct {v0}, LX/DpO;-><init>()V

    .line 2044690
    new-instance v0, LX/DpO;

    invoke-direct {v0}, LX/DpO;-><init>()V

    .line 2044691
    const/4 v6, 0x0

    iput v6, v0, LX/DpO;->setField_:I

    .line 2044692
    const/4 v6, 0x0

    iput-object v6, v0, LX/DpO;->value_:Ljava/lang/Object;

    .line 2044693
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 2044694
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v6

    .line 2044695
    invoke-virtual {v0, p0, v6}, LX/DpO;->a(LX/1su;LX/1sw;)Ljava/lang/Object;

    move-result-object v9

    iput-object v9, v0, LX/DpO;->value_:Ljava/lang/Object;

    .line 2044696
    iget-object v9, v0, LX/6kT;->value_:Ljava/lang/Object;

    if-eqz v9, :cond_5

    .line 2044697
    iget-short v6, v6, LX/1sw;->c:S

    iput v6, v0, LX/DpO;->setField_:I

    .line 2044698
    :cond_5
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    .line 2044699
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2044700
    move-object v0, v0

    .line 2044701
    move-object v6, v0

    .line 2044702
    goto/16 :goto_0

    .line 2044703
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044704
    :pswitch_7
    iget-byte v9, v0, LX/1sw;->b:B

    const/16 v10, 0xb

    if-ne v9, v10, :cond_7

    .line 2044705
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v7

    goto/16 :goto_0

    .line 2044706
    :cond_7
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044707
    :pswitch_8
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v14, :cond_8

    .line 2044708
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    goto/16 :goto_0

    .line 2044709
    :cond_8
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2044710
    :cond_9
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2044711
    new-instance v0, LX/DpN;

    invoke-direct/range {v0 .. v8}, LX/DpN;-><init>(Ljava/lang/Integer;LX/DpM;LX/DpM;Ljava/lang/Long;Ljava/lang/Integer;LX/DpO;[BLjava/lang/Long;)V

    .line 2044712
    invoke-static {v0}, LX/DpN;->a(LX/DpN;)V

    .line 2044713
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2044737
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2044738
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v2, v0

    .line 2044739
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    move-object v1, v0

    .line 2044740
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "Packet"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2044741
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044742
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044743
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044744
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044745
    const-string v0, "version"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044746
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044747
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044748
    iget-object v0, p0, LX/DpN;->version:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 2044749
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044750
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044751
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044752
    const-string v0, "msg_to"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044753
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044754
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044755
    iget-object v0, p0, LX/DpN;->msg_to:LX/DpM;

    if-nez v0, :cond_5

    .line 2044756
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044757
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044758
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044759
    const-string v0, "msg_from"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044760
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044761
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044762
    iget-object v0, p0, LX/DpN;->msg_from:LX/DpM;

    if-nez v0, :cond_6

    .line 2044763
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044764
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044765
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044766
    const-string v0, "date_micros"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044767
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044768
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044769
    iget-object v0, p0, LX/DpN;->date_micros:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 2044770
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044771
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044772
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044773
    const-string v0, "type"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044774
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044775
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044776
    iget-object v0, p0, LX/DpN;->type:Ljava/lang/Integer;

    if-nez v0, :cond_8

    .line 2044777
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044778
    :cond_0
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044779
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044780
    const-string v0, "body"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044781
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044782
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044783
    iget-object v0, p0, LX/DpN;->body:LX/DpO;

    if-nez v0, :cond_a

    .line 2044784
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044785
    :goto_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044786
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044787
    const-string v0, "nonce"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044788
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044789
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044790
    iget-object v0, p0, LX/DpN;->nonce:[B

    if-nez v0, :cond_b

    .line 2044791
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044792
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044793
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044794
    const-string v0, "thread_fbid"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044795
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044796
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044797
    iget-object v0, p0, LX/DpN;->thread_fbid:Ljava/lang/Long;

    if-nez v0, :cond_c

    .line 2044798
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044799
    :goto_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044800
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044801
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2044802
    :cond_1
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 2044803
    :cond_2
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 2044804
    :cond_3
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 2044805
    :cond_4
    iget-object v0, p0, LX/DpN;->version:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2044806
    :cond_5
    iget-object v0, p0, LX/DpN;->msg_to:LX/DpM;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2044807
    :cond_6
    iget-object v0, p0, LX/DpN;->msg_from:LX/DpM;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2044808
    :cond_7
    iget-object v0, p0, LX/DpN;->date_micros:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2044809
    :cond_8
    sget-object v0, LX/DpQ;->b:Ljava/util/Map;

    iget-object v5, p0, LX/DpN;->type:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2044810
    if-eqz v0, :cond_9

    .line 2044811
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044812
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044813
    :cond_9
    iget-object v5, p0, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2044814
    if-eqz v0, :cond_0

    .line 2044815
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2044816
    :cond_a
    iget-object v0, p0, LX/DpN;->body:LX/DpO;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 2044817
    :cond_b
    iget-object v0, p0, LX/DpN;->nonce:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 2044818
    :cond_c
    iget-object v0, p0, LX/DpN;->thread_fbid:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2044639
    invoke-static {p0}, LX/DpN;->a(LX/DpN;)V

    .line 2044640
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2044641
    iget-object v0, p0, LX/DpN;->version:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2044642
    sget-object v0, LX/DpN;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044643
    iget-object v0, p0, LX/DpN;->version:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2044644
    :cond_0
    iget-object v0, p0, LX/DpN;->msg_to:LX/DpM;

    if-eqz v0, :cond_1

    .line 2044645
    sget-object v0, LX/DpN;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044646
    iget-object v0, p0, LX/DpN;->msg_to:LX/DpM;

    invoke-virtual {v0, p1}, LX/DpM;->a(LX/1su;)V

    .line 2044647
    :cond_1
    iget-object v0, p0, LX/DpN;->msg_from:LX/DpM;

    if-eqz v0, :cond_2

    .line 2044648
    sget-object v0, LX/DpN;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044649
    iget-object v0, p0, LX/DpN;->msg_from:LX/DpM;

    invoke-virtual {v0, p1}, LX/DpM;->a(LX/1su;)V

    .line 2044650
    :cond_2
    iget-object v0, p0, LX/DpN;->date_micros:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2044651
    sget-object v0, LX/DpN;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044652
    iget-object v0, p0, LX/DpN;->date_micros:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2044653
    :cond_3
    iget-object v0, p0, LX/DpN;->type:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2044654
    sget-object v0, LX/DpN;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044655
    iget-object v0, p0, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2044656
    :cond_4
    iget-object v0, p0, LX/DpN;->body:LX/DpO;

    if-eqz v0, :cond_5

    .line 2044657
    sget-object v0, LX/DpN;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044658
    iget-object v0, p0, LX/DpN;->body:LX/DpO;

    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    .line 2044659
    :cond_5
    iget-object v0, p0, LX/DpN;->nonce:[B

    if-eqz v0, :cond_6

    .line 2044660
    sget-object v0, LX/DpN;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044661
    iget-object v0, p0, LX/DpN;->nonce:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2044662
    :cond_6
    iget-object v0, p0, LX/DpN;->thread_fbid:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 2044663
    sget-object v0, LX/DpN;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044664
    iget-object v0, p0, LX/DpN;->thread_fbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2044665
    :cond_7
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2044666
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2044667
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2044571
    if-nez p1, :cond_1

    .line 2044572
    :cond_0
    :goto_0
    return v0

    .line 2044573
    :cond_1
    instance-of v1, p1, LX/DpN;

    if-eqz v1, :cond_0

    .line 2044574
    check-cast p1, LX/DpN;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2044575
    if-nez p1, :cond_3

    .line 2044576
    :cond_2
    :goto_1
    move v0, v2

    .line 2044577
    goto :goto_0

    .line 2044578
    :cond_3
    iget-object v0, p0, LX/DpN;->version:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    move v0, v1

    .line 2044579
    :goto_2
    iget-object v3, p1, LX/DpN;->version:Ljava/lang/Integer;

    if-eqz v3, :cond_15

    move v3, v1

    .line 2044580
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2044581
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044582
    iget-object v0, p0, LX/DpN;->version:Ljava/lang/Integer;

    iget-object v3, p1, LX/DpN;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044583
    :cond_5
    iget-object v0, p0, LX/DpN;->msg_to:LX/DpM;

    if-eqz v0, :cond_16

    move v0, v1

    .line 2044584
    :goto_4
    iget-object v3, p1, LX/DpN;->msg_to:LX/DpM;

    if-eqz v3, :cond_17

    move v3, v1

    .line 2044585
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2044586
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044587
    iget-object v0, p0, LX/DpN;->msg_to:LX/DpM;

    iget-object v3, p1, LX/DpN;->msg_to:LX/DpM;

    invoke-virtual {v0, v3}, LX/DpM;->a(LX/DpM;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044588
    :cond_7
    iget-object v0, p0, LX/DpN;->msg_from:LX/DpM;

    if-eqz v0, :cond_18

    move v0, v1

    .line 2044589
    :goto_6
    iget-object v3, p1, LX/DpN;->msg_from:LX/DpM;

    if-eqz v3, :cond_19

    move v3, v1

    .line 2044590
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2044591
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044592
    iget-object v0, p0, LX/DpN;->msg_from:LX/DpM;

    iget-object v3, p1, LX/DpN;->msg_from:LX/DpM;

    invoke-virtual {v0, v3}, LX/DpM;->a(LX/DpM;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044593
    :cond_9
    iget-object v0, p0, LX/DpN;->date_micros:Ljava/lang/Long;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 2044594
    :goto_8
    iget-object v3, p1, LX/DpN;->date_micros:Ljava/lang/Long;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 2044595
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2044596
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044597
    iget-object v0, p0, LX/DpN;->date_micros:Ljava/lang/Long;

    iget-object v3, p1, LX/DpN;->date_micros:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044598
    :cond_b
    iget-object v0, p0, LX/DpN;->type:Ljava/lang/Integer;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 2044599
    :goto_a
    iget-object v3, p1, LX/DpN;->type:Ljava/lang/Integer;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 2044600
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2044601
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044602
    iget-object v0, p0, LX/DpN;->type:Ljava/lang/Integer;

    iget-object v3, p1, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044603
    :cond_d
    iget-object v0, p0, LX/DpN;->body:LX/DpO;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 2044604
    :goto_c
    iget-object v3, p1, LX/DpN;->body:LX/DpO;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 2044605
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 2044606
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044607
    iget-object v0, p0, LX/DpN;->body:LX/DpO;

    iget-object v3, p1, LX/DpN;->body:LX/DpO;

    invoke-virtual {v0, v3}, LX/DpO;->a(LX/DpO;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044608
    :cond_f
    iget-object v0, p0, LX/DpN;->nonce:[B

    if-eqz v0, :cond_20

    move v0, v1

    .line 2044609
    :goto_e
    iget-object v3, p1, LX/DpN;->nonce:[B

    if-eqz v3, :cond_21

    move v3, v1

    .line 2044610
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 2044611
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044612
    iget-object v0, p0, LX/DpN;->nonce:[B

    iget-object v3, p1, LX/DpN;->nonce:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2044613
    :cond_11
    iget-object v0, p0, LX/DpN;->thread_fbid:Ljava/lang/Long;

    if-eqz v0, :cond_22

    move v0, v1

    .line 2044614
    :goto_10
    iget-object v3, p1, LX/DpN;->thread_fbid:Ljava/lang/Long;

    if-eqz v3, :cond_23

    move v3, v1

    .line 2044615
    :goto_11
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 2044616
    :cond_12
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2044617
    iget-object v0, p0, LX/DpN;->thread_fbid:Ljava/lang/Long;

    iget-object v3, p1, LX/DpN;->thread_fbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_13
    move v2, v1

    .line 2044618
    goto/16 :goto_1

    :cond_14
    move v0, v2

    .line 2044619
    goto/16 :goto_2

    :cond_15
    move v3, v2

    .line 2044620
    goto/16 :goto_3

    :cond_16
    move v0, v2

    .line 2044621
    goto/16 :goto_4

    :cond_17
    move v3, v2

    .line 2044622
    goto/16 :goto_5

    :cond_18
    move v0, v2

    .line 2044623
    goto/16 :goto_6

    :cond_19
    move v3, v2

    .line 2044624
    goto/16 :goto_7

    :cond_1a
    move v0, v2

    .line 2044625
    goto/16 :goto_8

    :cond_1b
    move v3, v2

    .line 2044626
    goto/16 :goto_9

    :cond_1c
    move v0, v2

    .line 2044627
    goto/16 :goto_a

    :cond_1d
    move v3, v2

    .line 2044628
    goto/16 :goto_b

    :cond_1e
    move v0, v2

    .line 2044629
    goto :goto_c

    :cond_1f
    move v3, v2

    .line 2044630
    goto :goto_d

    :cond_20
    move v0, v2

    .line 2044631
    goto :goto_e

    :cond_21
    move v3, v2

    .line 2044632
    goto :goto_f

    :cond_22
    move v0, v2

    .line 2044633
    goto :goto_10

    :cond_23
    move v3, v2

    .line 2044634
    goto :goto_11
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2044638
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2044635
    sget-boolean v0, LX/DpN;->a:Z

    .line 2044636
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpN;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2044637
    return-object v0
.end method
