.class public final LX/DKl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TResultType;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DKo;


# direct methods
.method public constructor <init>(LX/DKo;)V
    .locals 0

    .prologue
    .line 1987478
    iput-object p1, p0, LX/DKl;->a:LX/DKo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1987479
    iget-object v1, p0, LX/DKl;->a:LX/DKo;

    monitor-enter v1

    .line 1987480
    :try_start_0
    iget-object v0, p0, LX/DKl;->a:LX/DKo;

    const/4 v2, 0x1

    .line 1987481
    iput-boolean v2, v0, LX/DKo;->i:Z

    .line 1987482
    iget-object v0, p0, LX/DKl;->a:LX/DKo;

    invoke-virtual {v0}, LX/DKo;->a()V

    .line 1987483
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1987484
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1987485
    iget-object v1, p0, LX/DKl;->a:LX/DKo;

    monitor-enter v1

    .line 1987486
    if-eqz p1, :cond_0

    .line 1987487
    :try_start_0
    iget-object v0, p0, LX/DKl;->a:LX/DKo;

    invoke-static {v0, p1}, LX/DKo;->a$redex0(LX/DKo;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1987488
    :cond_0
    iget-object v0, p0, LX/DKl;->a:LX/DKo;

    const/4 v2, 0x1

    .line 1987489
    iput-boolean v2, v0, LX/DKo;->i:Z

    .line 1987490
    iget-object v0, p0, LX/DKl;->a:LX/DKo;

    invoke-virtual {v0}, LX/DKo;->a()V

    .line 1987491
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
