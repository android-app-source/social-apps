.class public final LX/DmJ;
.super LX/DmI;
.source ""


# instance fields
.field public final synthetic l:LX/DmQ;


# direct methods
.method public constructor <init>(LX/DmQ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2039126
    iput-object p1, p0, LX/DmJ;->l:LX/DmQ;

    invoke-direct {p0, p2}, LX/DmI;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;)V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2039127
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2039128
    iput-object v0, p0, LX/DmI;->l:Ljava/lang/String;

    .line 2039129
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->n()J

    move-result-wide v2

    .line 2039130
    iget-object v0, p0, LX/DmJ;->l:LX/DmQ;

    iget-object v0, v0, LX/DmQ;->h:LX/DnT;

    invoke-virtual {v0, v2, v3}, LX/DnT;->f(J)Ljava/lang/String;

    move-result-object v1

    .line 2039131
    iget-object v0, p0, LX/DmJ;->l:LX/DmQ;

    iget-object v0, v0, LX/DmQ;->h:LX/DnT;

    invoke-virtual {v0, v2, v3}, LX/DnT;->g(J)Ljava/lang/String;

    move-result-object v2

    .line 2039132
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v4

    .line 2039133
    :goto_0
    iget-object v3, p0, LX/DmJ;->l:LX/DmQ;

    iget-object v3, v3, LX/DmQ;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f082ba2

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2039134
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;->l()Ljava/lang/String;

    move-result-object v3

    .line 2039135
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->p()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->p()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->a()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v4

    .line 2039136
    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/DmJ;->l:LX/DmQ;

    iget-object v0, v0, LX/DmQ;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f082bf5

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, LX/DmJ;->l:LX/DmQ;

    iget-object v8, v8, LX/DmQ;->h:LX/DnT;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->p()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v5

    iget-object v5, p0, LX/DmJ;->l:LX/DmQ;

    iget-object v5, v5, LX/DmQ;->h:LX/DnT;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->p()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->a()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v4

    invoke-virtual {v0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2039137
    :goto_2
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    iget-object v5, p0, LX/DmJ;->l:LX/DmQ;

    iget-object v5, v5, LX/DmQ;->c:LX/DkQ;

    invoke-virtual {v5}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2039138
    if-eqz v0, :cond_4

    .line 2039139
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v0

    .line 2039140
    iget-object v5, p0, LX/DmH;->o:Landroid/widget/TextView;

    iget-object v6, p0, LX/DmJ;->l:LX/DmQ;

    invoke-static {v6, v0}, LX/DmQ;->a$redex0(LX/DmQ;Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2039141
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->o()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    .line 2039142
    invoke-virtual/range {v0 .. v5}, LX/DmH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2039143
    :goto_3
    return-void

    :cond_1
    move v0, v5

    .line 2039144
    goto/16 :goto_0

    :cond_2
    move v0, v5

    .line 2039145
    goto :goto_1

    .line 2039146
    :cond_3
    iget-object v0, p0, LX/DmJ;->l:LX/DmQ;

    iget-object v0, v0, LX/DmQ;->h:LX/DnT;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->p()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$SuggestedTimeRangeModel;->j()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, LX/DnT;->c(J)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 2039147
    :cond_4
    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/DmH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public onClick(Landroid/view/View;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2039148
    iget-object v0, p0, LX/DmJ;->l:LX/DmQ;

    iget-object v0, v0, LX/DmQ;->i:LX/Did;

    if-eqz v0, :cond_0

    .line 2039149
    iget-object v0, p0, LX/DmJ;->l:LX/DmQ;

    iget-object v0, v0, LX/DmQ;->i:LX/Did;

    invoke-virtual {v0, p2}, LX/Did;->a(Ljava/lang/String;)V

    .line 2039150
    :cond_0
    return-void
.end method
