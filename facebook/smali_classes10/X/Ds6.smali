.class public LX/Ds6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Drs;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Ds6;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/Random;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2050039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2050040
    iput-object p1, p0, LX/Ds6;->a:Landroid/content/Context;

    .line 2050041
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/Ds6;->b:Ljava/util/Random;

    .line 2050042
    return-void
.end method

.method public static a(LX/0QB;)LX/Ds6;
    .locals 4

    .prologue
    .line 2050043
    sget-object v0, LX/Ds6;->c:LX/Ds6;

    if-nez v0, :cond_1

    .line 2050044
    const-class v1, LX/Ds6;

    monitor-enter v1

    .line 2050045
    :try_start_0
    sget-object v0, LX/Ds6;->c:LX/Ds6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2050046
    if-eqz v2, :cond_0

    .line 2050047
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2050048
    new-instance p0, LX/Ds6;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/Ds6;-><init>(Landroid/content/Context;)V

    .line 2050049
    move-object v0, p0

    .line 2050050
    sput-object v0, LX/Ds6;->c:LX/Ds6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2050051
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2050052
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2050053
    :cond_1
    sget-object v0, LX/Ds6;->c:LX/Ds6;

    return-object v0

    .line 2050054
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2050055
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/Drw;)LX/3pb;
    .locals 5

    .prologue
    .line 2050056
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v1, v0

    .line 2050057
    iget-object v2, p0, LX/Ds6;->a:Landroid/content/Context;

    .line 2050058
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v0, v0

    .line 2050059
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, LX/Ds6;->b:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    .line 2050060
    iget-object v4, p1, LX/Drw;->d:Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;

    move-object v4, v4

    .line 2050061
    invoke-static {v1, v2, v0, v3, v4}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->g(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2050062
    new-instance v1, LX/3pX;

    const v2, 0x7f021567

    .line 2050063
    iget-object v3, p1, LX/Drw;->c:Lorg/json/JSONObject;

    move-object v3, v3

    .line 2050064
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v1}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 2050065
    const/4 v0, 0x0

    return v0
.end method
