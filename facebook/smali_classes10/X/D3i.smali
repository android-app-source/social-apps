.class public final LX/D3i;
.super Landroid/view/View;
.source ""


# instance fields
.field private A:LX/D3h;

.field public final synthetic a:Lcom/facebook/uicontrib/calendar/CalendarView;

.field private final b:Landroid/graphics/Rect;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field public e:[Ljava/lang/String;

.field private f:[Z

.field public g:Z

.field public h:Z

.field public i:Ljava/util/Calendar;

.field public j:I

.field public k:I

.field private l:I

.field public m:I

.field public n:I

.field public o:Z

.field public p:Z

.field private q:Z

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field public v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Lcom/facebook/uicontrib/calendar/CalendarView;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1960449
    iput-object p1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    .line 1960450
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1960451
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    .line 1960452
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    .line 1960453
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/D3i;->d:Landroid/graphics/Paint;

    .line 1960454
    iput v1, p0, LX/D3i;->j:I

    .line 1960455
    iput v1, p0, LX/D3i;->k:I

    .line 1960456
    iput v1, p0, LX/D3i;->l:I

    .line 1960457
    iput v1, p0, LX/D3i;->r:I

    .line 1960458
    iput v1, p0, LX/D3i;->s:I

    .line 1960459
    iput v1, p0, LX/D3i;->t:I

    .line 1960460
    iput v1, p0, LX/D3i;->u:I

    .line 1960461
    iput v1, p0, LX/D3i;->w:I

    .line 1960462
    iput v1, p0, LX/D3i;->x:I

    .line 1960463
    iput v1, p0, LX/D3i;->y:I

    .line 1960464
    iput v1, p0, LX/D3i;->z:I

    .line 1960465
    invoke-direct {p0}, LX/D3i;->d()V

    .line 1960466
    return-void
.end method

.method private a(FII)I
    .locals 2

    .prologue
    .line 1960429
    int-to-float v0, p2

    sub-float v0, p1, v0

    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    sub-int v1, p3, p2

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 1960430
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-static {v1}, Lcom/facebook/uicontrib/calendar/CalendarView;->h(Lcom/facebook/uicontrib/calendar/CalendarView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1960431
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    add-int/lit8 v1, v1, -0x1

    sub-int v0, v1, v0

    .line 1960432
    :cond_0
    return v0
.end method

.method public static synthetic a(LX/D3i;FII)I
    .locals 1

    .prologue
    .line 1960428
    invoke-direct {p0, p1, p2, p3}, LX/D3i;->a(FII)I

    move-result v0

    return v0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1960397
    iget-boolean v0, p0, LX/D3i;->q:Z

    if-nez v0, :cond_0

    .line 1960398
    :goto_0
    return-void

    .line 1960399
    :cond_0
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->h:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1960400
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget v2, p0, LX/D3i;->n:I

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int/2addr v2, v3

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 1960401
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget v2, p0, LX/D3i;->n:I

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    add-int/2addr v2, v3

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 1960402
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-static {v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->h(Lcom/facebook/uicontrib/calendar/CalendarView;)Z

    move-result v0

    .line 1960403
    iget v2, p0, LX/D3i;->m:I

    iget v3, p0, LX/D3i;->v:I

    div-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 1960404
    iget-boolean v3, p0, LX/D3i;->o:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, LX/D3i;->p:Z

    if-eqz v3, :cond_1

    .line 1960405
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget v1, p0, LX/D3i;->w:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1960406
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget v1, p0, LX/D3i;->z:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1960407
    :goto_1
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget-object v1, p0, LX/D3i;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 1960408
    :cond_1
    iget-boolean v3, p0, LX/D3i;->o:Z

    if-eqz v3, :cond_4

    .line 1960409
    if-eqz v0, :cond_3

    .line 1960410
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget v1, p0, LX/D3i;->w:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1960411
    iget-object v1, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_2

    iget v0, p0, LX/D3i;->m:I

    iget v2, p0, LX/D3i;->m:I

    iget v3, p0, LX/D3i;->v:I

    div-int/2addr v2, v3

    sub-int/2addr v0, v2

    :goto_2
    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1960412
    :goto_3
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget-object v1, p0, LX/D3i;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1960413
    :cond_2
    iget v0, p0, LX/D3i;->m:I

    goto :goto_2

    .line 1960414
    :cond_3
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget v1, p0, LX/D3i;->w:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1960415
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget v1, p0, LX/D3i;->m:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_3

    .line 1960416
    :cond_4
    iget-boolean v3, p0, LX/D3i;->p:Z

    if-eqz v3, :cond_7

    .line 1960417
    if-eqz v0, :cond_6

    .line 1960418
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    :cond_5
    move v4, v1

    move-object v1, v0

    move v0, v4

    .line 1960419
    :goto_4
    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1960420
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget v1, p0, LX/D3i;->y:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1960421
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget-object v1, p0, LX/D3i;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1960422
    :cond_6
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v3, :cond_5

    iget v1, p0, LX/D3i;->m:I

    iget v3, p0, LX/D3i;->v:I

    div-int/2addr v1, v3

    move v4, v1

    move-object v1, v0

    move v0, v4

    goto :goto_4

    .line 1960423
    :cond_7
    if-eqz v0, :cond_9

    .line 1960424
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1960425
    iget-object v1, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_8

    iget v0, p0, LX/D3i;->m:I

    iget v2, p0, LX/D3i;->m:I

    iget v3, p0, LX/D3i;->v:I

    div-int/2addr v2, v3

    sub-int/2addr v0, v2

    :goto_5
    iput v0, v1, Landroid/graphics/Rect;->right:I

    goto/16 :goto_1

    :cond_8
    iget v0, p0, LX/D3i;->m:I

    goto :goto_5

    .line 1960426
    :cond_9
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v2, :cond_a

    iget v1, p0, LX/D3i;->m:I

    iget v2, p0, LX/D3i;->v:I

    div-int/2addr v1, v2

    :cond_a
    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1960427
    iget-object v0, p0, LX/D3i;->b:Landroid/graphics/Rect;

    iget v1, p0, LX/D3i;->m:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto/16 :goto_1
.end method

.method private a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1960392
    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    if-eqz v2, :cond_2

    .line 1960393
    iget-boolean v2, p0, LX/D3i;->p:Z

    if-eqz v2, :cond_1

    iget v2, p0, LX/D3i;->u:I

    if-ne p1, v2, :cond_1

    .line 1960394
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1960395
    goto :goto_0

    .line 1960396
    :cond_2
    iget-boolean v2, p0, LX/D3i;->o:Z

    if-eqz v2, :cond_3

    iget v2, p0, LX/D3i;->t:I

    if-eq p1, v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1960359
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    .line 1960360
    iget v2, p0, LX/D3i;->n:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    float-to-int v0, v0

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    sub-int/2addr v0, v2

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->r:I

    sub-int v3, v0, v2

    .line 1960361
    iget v4, p0, LX/D3i;->v:I

    .line 1960362
    mul-int/lit8 v5, v4, 0x2

    .line 1960363
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1960364
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->e:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1960365
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-static {v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->h(Lcom/facebook/uicontrib/calendar/CalendarView;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v2, v1

    .line 1960366
    :goto_0
    add-int/lit8 v0, v4, -0x1

    if-ge v2, v0, :cond_2

    .line 1960367
    iget-object v6, p0, LX/D3i;->d:Landroid/graphics/Paint;

    iget-object v0, p0, LX/D3i;->f:[Z

    aget-boolean v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->j:I

    :goto_1
    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1960368
    mul-int/lit8 v0, v2, 0x2

    add-int/lit8 v0, v0, 0x1

    iget v6, p0, LX/D3i;->m:I

    mul-int/2addr v0, v6

    div-int/2addr v0, v5

    .line 1960369
    invoke-direct {p0, v2}, LX/D3i;->a(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1960370
    iget-object v6, p0, LX/D3i;->d:Landroid/graphics/Paint;

    iget-object v7, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v7, v7, Lcom/facebook/uicontrib/calendar/CalendarView;->i:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 1960371
    :cond_0
    iget-object v6, p0, LX/D3i;->e:[Ljava/lang/String;

    add-int/lit8 v7, v4, -0x1

    sub-int/2addr v7, v2

    aget-object v6, v6, v7

    int-to-float v0, v0

    int-to-float v7, v3

    iget-object v8, p0, LX/D3i;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v0, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1960372
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1960373
    :cond_1
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->k:I

    goto :goto_1

    .line 1960374
    :cond_2
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_3

    .line 1960375
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->n:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1960376
    iget v0, p0, LX/D3i;->m:I

    iget v2, p0, LX/D3i;->m:I

    div-int/2addr v2, v5

    sub-int/2addr v0, v2

    .line 1960377
    iget-object v2, p0, LX/D3i;->e:[Ljava/lang/String;

    aget-object v1, v2, v1

    int-to-float v0, v0

    int-to-float v2, v3

    iget-object v3, p0, LX/D3i;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1960378
    :cond_3
    return-void

    .line 1960379
    :cond_4
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_5

    .line 1960380
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->n:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1960381
    iget v0, p0, LX/D3i;->m:I

    div-int/2addr v0, v5

    .line 1960382
    iget-object v2, p0, LX/D3i;->e:[Ljava/lang/String;

    aget-object v1, v2, v1

    int-to-float v0, v0

    int-to-float v2, v3

    iget-object v6, p0, LX/D3i;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1960383
    const/4 v0, 0x1

    move v1, v0

    .line 1960384
    :cond_5
    :goto_2
    if-ge v1, v4, :cond_3

    .line 1960385
    iget-object v2, p0, LX/D3i;->d:Landroid/graphics/Paint;

    iget-object v0, p0, LX/D3i;->f:[Z

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->j:I

    :goto_3
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1960386
    mul-int/lit8 v0, v1, 0x2

    add-int/lit8 v0, v0, 0x1

    iget v2, p0, LX/D3i;->m:I

    mul-int/2addr v0, v2

    div-int/2addr v0, v5

    .line 1960387
    invoke-direct {p0, v1}, LX/D3i;->a(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1960388
    iget-object v2, p0, LX/D3i;->d:Landroid/graphics/Paint;

    iget-object v6, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v6, v6, Lcom/facebook/uicontrib/calendar/CalendarView;->i:I

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1960389
    :cond_6
    iget-object v2, p0, LX/D3i;->e:[Ljava/lang/String;

    aget-object v2, v2, v1

    int-to-float v0, v0

    int-to-float v6, v3

    iget-object v7, p0, LX/D3i;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1960390
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1960391
    :cond_7
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->k:I

    goto :goto_3
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1960345
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 1960346
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-gez v1, :cond_0

    .line 1960347
    add-int/lit8 v0, v0, 0x1

    .line 1960348
    :cond_0
    iget v1, p0, LX/D3i;->l:I

    if-ne v0, v1, :cond_1

    .line 1960349
    :goto_0
    return-void

    .line 1960350
    :cond_1
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->m:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1960351
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1960352
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-static {v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->h(Lcom/facebook/uicontrib/calendar/CalendarView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1960353
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_2

    iget v0, p0, LX/D3i;->m:I

    iget v1, p0, LX/D3i;->m:I

    iget v3, p0, LX/D3i;->v:I

    div-int/2addr v1, v3

    sub-int/2addr v0, v1

    int-to-float v0, v0

    :goto_1
    move v3, v0

    move v1, v2

    .line 1960354
    :goto_2
    iget-object v5, p0, LX/D3i;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 1960355
    :cond_2
    iget v0, p0, LX/D3i;->m:I

    int-to-float v0, v0

    goto :goto_1

    .line 1960356
    :cond_3
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_4

    iget v0, p0, LX/D3i;->m:I

    iget v1, p0, LX/D3i;->v:I

    div-int/2addr v0, v1

    int-to-float v0, v0

    .line 1960357
    :goto_3
    iget v1, p0, LX/D3i;->m:I

    int-to-float v3, v1

    move v1, v0

    goto :goto_2

    :cond_4
    move v0, v2

    .line 1960358
    goto :goto_3
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1960336
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 1960337
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1960338
    iget-object v0, p0, LX/D3i;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1960339
    iget-object v0, p0, LX/D3i;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 1960340
    iget-object v0, p0, LX/D3i;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1960341
    iget-object v0, p0, LX/D3i;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1960342
    iget-object v0, p0, LX/D3i;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1960343
    iget-object v0, p0, LX/D3i;->d:Landroid/graphics/Paint;

    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->e:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1960344
    return-void
.end method

.method private d(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1960433
    iget-boolean v0, p0, LX/D3i;->p:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/D3i;->o:Z

    if-nez v0, :cond_1

    .line 1960434
    :cond_0
    :goto_0
    return-void

    .line 1960435
    :cond_1
    iget-boolean v0, p0, LX/D3i;->p:Z

    if-eqz v0, :cond_2

    .line 1960436
    iget v0, p0, LX/D3i;->z:I

    iget v1, p0, LX/D3i;->y:I

    sub-int/2addr v0, v1

    .line 1960437
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    if-eqz v1, :cond_3

    .line 1960438
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->f:Landroid/graphics/drawable/Drawable;

    iget v2, p0, LX/D3i;->y:I

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int v3, v0, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, LX/D3i;->n:I

    iget-object v4, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v4, v4, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int/2addr v3, v4

    iget-object v4, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v4, v4, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, LX/D3i;->z:I

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int/2addr v0, v5

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v4, v0

    iget v4, p0, LX/D3i;->n:I

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    add-int/2addr v4, v5

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1960439
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1960440
    :cond_2
    :goto_1
    iget-boolean v0, p0, LX/D3i;->o:Z

    if-eqz v0, :cond_0

    .line 1960441
    iget v0, p0, LX/D3i;->x:I

    iget v1, p0, LX/D3i;->w:I

    sub-int/2addr v0, v1

    .line 1960442
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->W:Z

    if-eqz v1, :cond_4

    invoke-direct {p0}, LX/D3i;->e()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1960443
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->g:Landroid/graphics/drawable/Drawable;

    iget v2, p0, LX/D3i;->w:I

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int v3, v0, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, LX/D3i;->n:I

    iget-object v4, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v4, v4, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int/2addr v3, v4

    iget-object v4, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v4, v4, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, LX/D3i;->x:I

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int/2addr v0, v5

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v4, v0

    iget v4, p0, LX/D3i;->n:I

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    add-int/2addr v4, v5

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1960444
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 1960445
    :cond_3
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->g:Landroid/graphics/drawable/Drawable;

    iget v2, p0, LX/D3i;->y:I

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int v3, v0, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, LX/D3i;->n:I

    iget-object v4, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v4, v4, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int/2addr v3, v4

    iget-object v4, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v4, v4, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, LX/D3i;->z:I

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int/2addr v0, v5

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v4, v0

    iget v4, p0, LX/D3i;->n:I

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    add-int/2addr v4, v5

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1960446
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 1960447
    :cond_4
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->f:Landroid/graphics/drawable/Drawable;

    iget v2, p0, LX/D3i;->w:I

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int v3, v0, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, LX/D3i;->n:I

    iget-object v4, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v4, v4, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int/2addr v3, v4

    iget-object v4, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v4, v4, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, LX/D3i;->x:I

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    sub-int/2addr v0, v5

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v4, v0

    iget v4, p0, LX/D3i;->n:I

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    add-int/2addr v4, v5

    iget-object v5, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v5, v5, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1960448
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 1960229
    iget v0, p0, LX/D3i;->r:I

    if-lez v0, :cond_0

    iget v0, p0, LX/D3i;->r:I

    iget v1, p0, LX/D3i;->s:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 1960230
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-static {v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->h(Lcom/facebook/uicontrib/calendar/CalendarView;)Z

    move-result v1

    .line 1960231
    iget-boolean v0, p0, LX/D3i;->o:Z

    if-eqz v0, :cond_2

    .line 1960232
    iget v0, p0, LX/D3i;->r:I

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    sub-int/2addr v0, v2

    .line 1960233
    if-gez v0, :cond_0

    .line 1960234
    add-int/lit8 v0, v0, 0x7

    .line 1960235
    :cond_0
    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    .line 1960236
    add-int/lit8 v0, v0, 0x1

    .line 1960237
    :cond_1
    if-eqz v1, :cond_6

    .line 1960238
    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    add-int/lit8 v2, v2, -0x1

    sub-int v0, v2, v0

    iput v0, p0, LX/D3i;->t:I

    .line 1960239
    iget v0, p0, LX/D3i;->t:I

    iget v2, p0, LX/D3i;->m:I

    mul-int/2addr v0, v2

    iget v2, p0, LX/D3i;->v:I

    div-int/2addr v0, v2

    iput v0, p0, LX/D3i;->w:I

    .line 1960240
    :goto_0
    iget v0, p0, LX/D3i;->w:I

    iget v2, p0, LX/D3i;->m:I

    iget v3, p0, LX/D3i;->v:I

    div-int/2addr v2, v3

    add-int/2addr v0, v2

    iput v0, p0, LX/D3i;->x:I

    .line 1960241
    iget v0, p0, LX/D3i;->x:I

    iget v2, p0, LX/D3i;->w:I

    sub-int/2addr v0, v2

    .line 1960242
    iget v2, p0, LX/D3i;->n:I

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    sub-int/2addr v2, v3

    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v3, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->d:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1960243
    iget-object v3, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1960244
    iput v0, v3, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    .line 1960245
    :cond_2
    iget-boolean v0, p0, LX/D3i;->p:Z

    if-eqz v0, :cond_5

    .line 1960246
    iget v0, p0, LX/D3i;->s:I

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    sub-int/2addr v0, v2

    .line 1960247
    if-gez v0, :cond_3

    .line 1960248
    add-int/lit8 v0, v0, 0x7

    .line 1960249
    :cond_3
    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v2, :cond_4

    if-nez v1, :cond_4

    .line 1960250
    add-int/lit8 v0, v0, 0x1

    .line 1960251
    :cond_4
    iput v0, p0, LX/D3i;->u:I

    .line 1960252
    if-eqz v1, :cond_7

    .line 1960253
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    add-int/lit8 v1, v1, -0x1

    sub-int v0, v1, v0

    iput v0, p0, LX/D3i;->u:I

    .line 1960254
    iget v0, p0, LX/D3i;->u:I

    iget v1, p0, LX/D3i;->m:I

    mul-int/2addr v0, v1

    iget v1, p0, LX/D3i;->v:I

    div-int/2addr v0, v1

    iput v0, p0, LX/D3i;->y:I

    .line 1960255
    :goto_1
    iget v0, p0, LX/D3i;->y:I

    iget v1, p0, LX/D3i;->m:I

    iget v2, p0, LX/D3i;->v:I

    div-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, LX/D3i;->z:I

    .line 1960256
    iget v0, p0, LX/D3i;->z:I

    iget v1, p0, LX/D3i;->y:I

    sub-int/2addr v0, v1

    .line 1960257
    iget v1, p0, LX/D3i;->n:I

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->c:I

    sub-int/2addr v1, v2

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->d:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1960258
    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1960259
    iput v0, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->ac:I

    .line 1960260
    :cond_5
    return-void

    .line 1960261
    :cond_6
    iput v0, p0, LX/D3i;->t:I

    .line 1960262
    iget v0, p0, LX/D3i;->t:I

    iget v2, p0, LX/D3i;->m:I

    mul-int/2addr v0, v2

    iget v2, p0, LX/D3i;->v:I

    div-int/2addr v0, v2

    iput v0, p0, LX/D3i;->w:I

    goto/16 :goto_0

    .line 1960263
    :cond_7
    iput v0, p0, LX/D3i;->u:I

    .line 1960264
    iget v0, p0, LX/D3i;->u:I

    iget v1, p0, LX/D3i;->m:I

    mul-int/2addr v0, v1

    iget v1, p0, LX/D3i;->v:I

    div-int/2addr v0, v1

    iput v0, p0, LX/D3i;->y:I

    goto :goto_1
.end method


# virtual methods
.method public final a(IZIII)V
    .locals 8

    .prologue
    .line 1960265
    iput p3, p0, LX/D3i;->r:I

    .line 1960266
    iput p4, p0, LX/D3i;->s:I

    .line 1960267
    iget v0, p0, LX/D3i;->r:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/D3i;->o:Z

    .line 1960268
    iget v0, p0, LX/D3i;->s:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/D3i;->p:Z

    .line 1960269
    iput-boolean p2, p0, LX/D3i;->q:Z

    .line 1960270
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    add-int/lit8 v0, v0, 0x1

    :goto_2
    iput v0, p0, LX/D3i;->v:I

    .line 1960271
    iput p1, p0, LX/D3i;->l:I

    .line 1960272
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960273
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v1, 0x3

    iget v2, p0, LX/D3i;->l:I

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1960274
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 1960275
    iget v0, p0, LX/D3i;->v:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, LX/D3i;->e:[Ljava/lang/String;

    .line 1960276
    iget v0, p0, LX/D3i;->v:I

    new-array v0, v0, [Z

    iput-object v0, p0, LX/D3i;->f:[Z

    .line 1960277
    const/4 v0, 0x0

    .line 1960278
    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v1, :cond_9

    .line 1960279
    iget-object v0, p0, LX/D3i;->e:[Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v6, v6, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1960280
    const/4 v0, 0x1

    move v1, v0

    .line 1960281
    :goto_3
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->H:I

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v0, v2

    .line 1960282
    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v3, 0x5

    invoke-virtual {v2, v3, v0}, Ljava/util/Calendar;->add(II)V

    .line 1960283
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, LX/D3i;->i:Ljava/util/Calendar;

    .line 1960284
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, LX/D3i;->j:I

    .line 1960285
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D3i;->h:Z

    .line 1960286
    :goto_4
    iget v0, p0, LX/D3i;->v:I

    if-ge v1, v0, :cond_7

    .line 1960287
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-ne v0, p5, :cond_4

    const/4 v0, 0x1

    .line 1960288
    :goto_5
    iget-object v2, p0, LX/D3i;->f:[Z

    aput-boolean v0, v2, v1

    .line 1960289
    iget-boolean v2, p0, LX/D3i;->g:Z

    or-int/2addr v2, v0

    iput-boolean v2, p0, LX/D3i;->g:Z

    .line 1960290
    iget-boolean v2, p0, LX/D3i;->h:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_6
    and-int/2addr v0, v2

    iput-boolean v0, p0, LX/D3i;->h:Z

    .line 1960291
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->S:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    iget-object v2, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, v2, Lcom/facebook/uicontrib/calendar/CalendarView;->T:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1960292
    :cond_0
    iget-object v0, p0, LX/D3i;->e:[Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v0, v1

    .line 1960293
    :goto_7
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1960294
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1960295
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1960296
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1960297
    :cond_3
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->x:I

    goto/16 :goto_2

    .line 1960298
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 1960299
    :cond_5
    const/4 v0, 0x0

    goto :goto_6

    .line 1960300
    :cond_6
    iget-object v0, p0, LX/D3i;->e:[Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v6, v6, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_7

    .line 1960301
    :cond_7
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_8

    .line 1960302
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v1, 0x5

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1960303
    :cond_8
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->Q:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, LX/D3i;->k:I

    .line 1960304
    invoke-direct {p0}, LX/D3i;->f()V

    .line 1960305
    new-instance v0, LX/D3h;

    invoke-direct {v0, p0, p0}, LX/D3h;-><init>(LX/D3i;LX/D3i;)V

    iput-object v0, p0, LX/D3i;->A:LX/D3h;

    .line 1960306
    iget-object v0, p0, LX/D3i;->A:LX/D3h;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 1960307
    return-void

    :cond_9
    move v1, v0

    goto/16 :goto_3
.end method

.method public final a(FLjava/util/Calendar;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1960308
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-static {v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->h(Lcom/facebook/uicontrib/calendar/CalendarView;)Z

    move-result v0

    .line 1960309
    if-eqz v0, :cond_2

    .line 1960310
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/D3i;->m:I

    iget v2, p0, LX/D3i;->m:I

    iget v3, p0, LX/D3i;->v:I

    div-int/2addr v2, v3

    sub-int/2addr v0, v2

    :goto_0
    move v2, v1

    .line 1960311
    :goto_1
    int-to-float v3, v2

    cmpg-float v3, p1, v3

    if-ltz v3, :cond_0

    int-to-float v3, v0

    cmpl-float v3, p1, v3

    if-lez v3, :cond_4

    .line 1960312
    :cond_0
    invoke-virtual {p2}, Ljava/util/Calendar;->clear()V

    .line 1960313
    :goto_2
    return v1

    .line 1960314
    :cond_1
    iget v0, p0, LX/D3i;->m:I

    goto :goto_0

    .line 1960315
    :cond_2
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->w:Z

    if-eqz v0, :cond_3

    iget v0, p0, LX/D3i;->m:I

    iget v2, p0, LX/D3i;->v:I

    div-int/2addr v0, v2

    .line 1960316
    :goto_3
    iget v2, p0, LX/D3i;->m:I

    move v6, v2

    move v2, v0

    move v0, v6

    goto :goto_1

    :cond_3
    move v0, v1

    .line 1960317
    goto :goto_3

    .line 1960318
    :cond_4
    iget-object v1, p0, LX/D3i;->i:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1960319
    const/4 v1, 0x5

    invoke-direct {p0, p1, v2, v0}, LX/D3i;->a(FII)I

    move-result v0

    invoke-virtual {p2, v1, v0}, Ljava/util/Calendar;->add(II)V

    .line 1960320
    const/4 v1, 0x1

    goto :goto_2
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1960321
    iget-object v0, p0, LX/D3i;->A:LX/D3h;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D3i;->A:LX/D3h;

    invoke-virtual {v0, p1}, LX/1cr;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1960322
    const/4 v0, 0x1

    .line 1960323
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1960324
    invoke-direct {p0, p1}, LX/D3i;->a(Landroid/graphics/Canvas;)V

    .line 1960325
    invoke-direct {p0, p1}, LX/D3i;->d(Landroid/graphics/Canvas;)V

    .line 1960326
    invoke-direct {p0, p1}, LX/D3i;->b(Landroid/graphics/Canvas;)V

    .line 1960327
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-boolean v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->l:Z

    if-eqz v0, :cond_0

    .line 1960328
    invoke-direct {p0, p1}, LX/D3i;->c(Landroid/graphics/Canvas;)V

    .line 1960329
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1960330
    iget-object v0, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v0, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeight()I

    move-result v0

    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->B:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, LX/D3i;->a:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget v1, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->v:I

    div-int/2addr v0, v1

    iput v0, p0, LX/D3i;->n:I

    .line 1960331
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, LX/D3i;->n:I

    invoke-virtual {p0, v0, v1}, LX/D3i;->setMeasuredDimension(II)V

    .line 1960332
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x314ff9cc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1960333
    iput p1, p0, LX/D3i;->m:I

    .line 1960334
    invoke-direct {p0}, LX/D3i;->f()V

    .line 1960335
    const/16 v1, 0x2d

    const v2, -0x393215e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
