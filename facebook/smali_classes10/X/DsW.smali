.class public final LX/DsW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/widget/NotificationsFragment;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V
    .locals 1

    .prologue
    .line 2050472
    iput-object p1, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2050473
    const/4 v0, 0x0

    iput v0, p0, LX/DsW;->b:I

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 2050474
    iget-object v0, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->s:LX/2ix;

    invoke-virtual {v0, p1, p2}, LX/2ix;->a(LX/0g8;I)V

    .line 2050475
    iput p2, p0, LX/DsW;->b:I

    .line 2050476
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 3

    .prologue
    .line 2050477
    iget-object v0, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    .line 2050478
    iput p2, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->ah:I

    .line 2050479
    iget-object v0, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    .line 2050480
    iput p3, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->ai:I

    .line 2050481
    iget-object v1, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    add-int v0, p2, p3

    if-ne v0, p4, :cond_2

    add-int v0, p2, p3

    add-int/lit8 v0, v0, -0x1

    iget-object v2, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget v2, v2, Lcom/facebook/notifications/widget/NotificationsFragment;->aj:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2050482
    :goto_0
    iput v0, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->aj:I

    .line 2050483
    iget v0, p0, LX/DsW;->b:I

    if-eqz v0, :cond_0

    .line 2050484
    iget-object v0, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    const/4 v1, 0x1

    .line 2050485
    iput-boolean v1, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->an:Z

    .line 2050486
    :cond_0
    if-lez p4, :cond_1

    add-int v0, p2, p3

    if-lt v0, p4, :cond_1

    iget-object v0, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-boolean v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->ad:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {v0}, LX/DsH;->c()I

    move-result v0

    iget-object v1, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v1, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    .line 2050487
    iget v2, v1, LX/DsH;->f:I

    move v1, v2

    .line 2050488
    if-le v0, v1, :cond_3

    .line 2050489
    :cond_1
    :goto_1
    return-void

    .line 2050490
    :cond_2
    add-int v0, p2, p3

    iget-object v2, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget v2, v2, Lcom/facebook/notifications/widget/NotificationsFragment;->aj:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 2050491
    :cond_3
    iget-object v0, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-boolean v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->al:Z

    if-nez v0, :cond_1

    .line 2050492
    iget-object v0, p0, LX/DsW;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-static {v0}, Lcom/facebook/notifications/widget/NotificationsFragment;->s(Lcom/facebook/notifications/widget/NotificationsFragment;)V

    goto :goto_1
.end method
