.class public LX/E9f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[I

.field private static volatile d:LX/E9f;


# instance fields
.field public final b:LX/0wM;

.field public final c:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2084737
    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0a0108

    aput v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f0a0107

    aput v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f0a0105

    aput v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f0a0104

    aput v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x7f0a00d2

    aput v2, v0, v1

    sput-object v0, LX/E9f;->a:[I

    return-void
.end method

.method public constructor <init>(LX/0wM;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2084738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2084739
    iput-object p1, p0, LX/E9f;->b:LX/0wM;

    .line 2084740
    iput-object p2, p0, LX/E9f;->c:Landroid/content/res/Resources;

    .line 2084741
    return-void
.end method

.method public static a(LX/0QB;)LX/E9f;
    .locals 5

    .prologue
    .line 2084742
    sget-object v0, LX/E9f;->d:LX/E9f;

    if-nez v0, :cond_1

    .line 2084743
    const-class v1, LX/E9f;

    monitor-enter v1

    .line 2084744
    :try_start_0
    sget-object v0, LX/E9f;->d:LX/E9f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2084745
    if-eqz v2, :cond_0

    .line 2084746
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2084747
    new-instance p0, LX/E9f;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, LX/E9f;-><init>(LX/0wM;Landroid/content/res/Resources;)V

    .line 2084748
    move-object v0, p0

    .line 2084749
    sput-object v0, LX/E9f;->d:LX/E9f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2084750
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2084751
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2084752
    :cond_1
    sget-object v0, LX/E9f;->d:LX/E9f;

    return-object v0

    .line 2084753
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2084754
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
