.class public LX/CoA;
.super LX/CnT;
.source ""

# interfaces
.implements LX/Cny;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/VideoBlockView;",
        "LX/Cm4;",
        ">;",
        "LX/Cny;"
    }
.end annotation


# instance fields
.field public d:LX/CqM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CpT;)V
    .locals 3

    .prologue
    .line 1935102
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1935103
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, LX/CoA;

    invoke-static {p1}, LX/CqM;->a(LX/0QB;)LX/CqM;

    move-result-object v2

    check-cast v2, LX/CqM;

    const/16 v0, 0x31dc

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p1

    check-cast p1, LX/0Uh;

    iput-object v2, p0, LX/CoA;->d:LX/CqM;

    iput-object v0, p0, LX/CoA;->e:LX/0Ot;

    iput-object p1, p0, LX/CoA;->f:LX/0Uh;

    .line 1935104
    return-void
.end method


# virtual methods
.method public bridge synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 1935101
    check-cast p1, LX/Cm4;

    invoke-virtual {p0, p1}, LX/CoA;->a(LX/Cm4;)V

    return-void
.end method

.method public a(LX/Cm4;)V
    .locals 4

    .prologue
    .line 1935077
    invoke-static {p1}, LX/Co1;->a(LX/Clu;)Landroid/os/Bundle;

    move-result-object v1

    .line 1935078
    iget-object v0, p0, LX/CoA;->f:LX/0Uh;

    const/16 v2, 0xa4

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 1935079
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/CoA;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935080
    if-eqz p1, :cond_4

    invoke-interface {p1}, LX/Cm4;->r()LX/8Ys;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, LX/Cm4;->r()LX/8Ys;

    move-result-object v0

    invoke-interface {v0}, LX/8Ys;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1935081
    if-eqz v0, :cond_0

    .line 1935082
    const-string v0, "strategyType"

    sget-object v2, LX/CrN;->SPHERICAL_VIDEO:LX/CrN;

    invoke-virtual {v2}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1935083
    :cond_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935084
    check-cast v0, LX/CpT;

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1935085
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935086
    check-cast v0, LX/CpT;

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/Cm4;->r()LX/8Ys;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, LX/Cm4;->r()LX/8Ys;

    move-result-object v1

    invoke-interface {v1}, LX/8Ys;->d()Ljava/lang/String;

    move-result-object v1

    .line 1935087
    :goto_1
    iput-object v1, v0, LX/CpT;->N:Ljava/lang/String;

    .line 1935088
    iput-object v2, v0, LX/CpT;->M:Ljava/lang/String;

    .line 1935089
    invoke-interface {p1}, LX/Cm4;->r()LX/8Ys;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1935090
    iget-object v0, p0, LX/CoA;->d:LX/CqM;

    invoke-virtual {p0}, LX/CoA;->d()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/CqM;->a(LX/Cm4;Z)LX/Cli;

    move-result-object v1

    .line 1935091
    instance-of v0, p1, LX/Cm1;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1935092
    check-cast v0, LX/Cm1;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, LX/Cm0;->a(Z)V

    .line 1935093
    :cond_1
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935094
    check-cast v0, LX/CpT;

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CpT;->a(LX/Cli;Ljava/lang/String;)V

    .line 1935095
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v0

    .line 1935096
    iget-object v0, p0, LX/CoA;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 1935097
    iget-object v2, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v2

    .line 1935098
    invoke-interface {p1}, LX/Clr;->o()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, p1, v0, v2}, LX/Co1;->a(LX/CnG;LX/Clq;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;Landroid/os/Bundle;)V

    .line 1935099
    :cond_2
    return-void

    .line 1935100
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1935076
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1935075
    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 3

    .prologue
    .line 1935074
    iget-object v0, p0, LX/CoA;->f:LX/0Uh;

    const/16 v1, 0xa3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
