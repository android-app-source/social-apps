.class public final LX/DzR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9kF;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2066959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 2066972
    invoke-direct {p0}, LX/DzR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;Lcom/facebook/ipc/model/PageTopic;)V
    .locals 6

    .prologue
    .line 2066960
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/NewPlaceCreationActivity;

    .line 2066961
    invoke-static {p2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 2066962
    iput-object v1, v0, Lcom/facebook/places/create/NewPlaceCreationActivity;->q:LX/0am;

    .line 2066963
    iget-wide v2, p2, Lcom/facebook/ipc/model/PageTopic;->id:J

    const-wide v4, 0xb36f1da84d60L

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2066964
    iget-object v1, v0, Lcom/facebook/places/create/NewPlaceCreationActivity;->p:LX/96B;

    iget-object v2, v0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v3, LX/96A;->HOME_CREATION:LX/96A;

    invoke-virtual {v1, v2, v3}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    .line 2066965
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2066966
    const-string v2, "create_home_from_place_creation"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2066967
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/places/create/NewPlaceCreationActivity;->setResult(ILandroid/content/Intent;)V

    .line 2066968
    invoke-virtual {v0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->finish()V

    .line 2066969
    :goto_0
    return-void

    .line 2066970
    :cond_0
    invoke-static {v0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->b$redex0(Lcom/facebook/places/create/NewPlaceCreationActivity;)V

    .line 2066971
    goto :goto_0
.end method
