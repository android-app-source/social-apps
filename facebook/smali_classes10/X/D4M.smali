.class public LX/D4M;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1962059
    return-void
.end method

.method public static a(LX/0QB;)LX/D4M;
    .locals 3

    .prologue
    .line 1962060
    const-class v1, LX/D4M;

    monitor-enter v1

    .line 1962061
    :try_start_0
    sget-object v0, LX/D4M;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1962062
    sput-object v2, LX/D4M;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962063
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962064
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1962065
    new-instance v0, LX/D4M;

    invoke-direct {v0}, LX/D4M;-><init>()V

    .line 1962066
    move-object v0, v0

    .line 1962067
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1962068
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D4M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962069
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1962070
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
