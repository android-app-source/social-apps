.class public LX/DFD;
.super LX/3mW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/3mW",
        "<",
        "LX/DFC;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/2dy;

.field private final d:LX/1Pc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final e:LX/DF2;

.field private final f:LX/DF6;

.field private final g:LX/DFk;

.field private final h:LX/DFU;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2dy;LX/0Px;LX/1Pc;LX/25M;LX/DFU;LX/DF2;LX/DF6;LX/DFk;LX/2e2;)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/2dy;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2dy;",
            "LX/0Px",
            "<",
            "LX/DFC;",
            ">;TE;",
            "LX/25M;",
            "LX/DFU;",
            "LX/DF2;",
            "LX/DF6;",
            "LX/DFk;",
            "LX/2e2;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1977938
    new-instance v4, LX/DFO;

    invoke-virtual {p2}, LX/2dy;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    move-object/from16 v0, p10

    invoke-direct {v4, v0, v1}, LX/DFO;-><init>(LX/2e2;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V

    move-object v5, p4

    check-cast v5, LX/1Pq;

    invoke-virtual {p2}, LX/2dy;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, LX/3mW;-><init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)V

    .line 1977939
    iput-object p4, p0, LX/DFD;->d:LX/1Pc;

    .line 1977940
    iput-object p2, p0, LX/DFD;->c:LX/2dy;

    .line 1977941
    iput-object p6, p0, LX/DFD;->h:LX/DFU;

    .line 1977942
    iput-object p7, p0, LX/DFD;->e:LX/DF2;

    .line 1977943
    move-object/from16 v0, p8

    iput-object v0, p0, LX/DFD;->f:LX/DF6;

    .line 1977944
    move-object/from16 v0, p9

    iput-object v0, p0, LX/DFD;->g:LX/DFk;

    .line 1977945
    return-void
.end method

.method public static a(LX/2dy;LX/0ad;LX/2e2;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2dy;",
            "LX/0ad;",
            "LX/2e2;",
            ")",
            "LX/0Px",
            "<",
            "LX/DFC;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x0

    .line 1977921
    invoke-virtual {p0}, LX/2dy;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1977922
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1977923
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 1977924
    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/0Px;

    move-result-object v4

    .line 1977925
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1977926
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_0
    if-ge v2, v6, :cond_1

    .line 1977927
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 1977928
    invoke-static {v0, v2, p1}, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;ILX/0ad;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1977929
    new-instance v7, LX/DFC;

    const/4 v8, 0x1

    invoke-direct {v7, v8, v9}, LX/DFC;-><init>(ILcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1977930
    :cond_0
    new-instance v7, LX/DFC;

    invoke-direct {v7, v3, v1}, LX/DFC;-><init>(ILcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1977931
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1977932
    :cond_1
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/2dy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1977933
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1977934
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-static {v0}, LX/2e2;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1977935
    :cond_2
    new-instance v0, LX/DFC;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v9}, LX/DFC;-><init>(ILcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1977936
    :goto_1
    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 1977937
    :cond_3
    new-instance v0, LX/DFC;

    const/4 v1, 0x3

    invoke-direct {v0, v1, v9}, LX/DFC;-><init>(ILcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1977920
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 1977860
    check-cast p2, LX/DFC;

    .line 1977861
    iget v0, p2, LX/DFC;->b:I

    packed-switch v0, :pswitch_data_0

    .line 1977862
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A new/illegal pymk card type was added but not defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1977863
    :pswitch_0
    iget-object v0, p0, LX/DFD;->h:LX/DFU;

    const/4 v1, 0x0

    .line 1977864
    new-instance v2, LX/DFT;

    invoke-direct {v2, v0}, LX/DFT;-><init>(LX/DFU;)V

    .line 1977865
    iget-object v3, v0, LX/DFU;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DFS;

    .line 1977866
    if-nez v3, :cond_0

    .line 1977867
    new-instance v3, LX/DFS;

    invoke-direct {v3, v0}, LX/DFS;-><init>(LX/DFU;)V

    .line 1977868
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/DFS;->a$redex0(LX/DFS;LX/1De;IILX/DFT;)V

    .line 1977869
    move-object v2, v3

    .line 1977870
    move-object v1, v2

    .line 1977871
    move-object v0, v1

    .line 1977872
    iget-object v1, p0, LX/DFD;->d:LX/1Pc;

    .line 1977873
    iget-object v2, v0, LX/DFS;->a:LX/DFT;

    iput-object v1, v2, LX/DFT;->c:LX/1Pc;

    .line 1977874
    iget-object v2, v0, LX/DFS;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1977875
    move-object v0, v0

    .line 1977876
    iget-object v1, p0, LX/DFD;->c:LX/2dy;

    invoke-virtual {v1}, LX/2dy;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1977877
    iget-object v2, v0, LX/DFS;->a:LX/DFT;

    iput-object v1, v2, LX/DFT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1977878
    iget-object v2, v0, LX/DFS;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1977879
    move-object v0, v0

    .line 1977880
    iget-object v1, p2, LX/DFC;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 1977881
    iget-object v2, v0, LX/DFS;->a:LX/DFT;

    iput-object v1, v2, LX/DFT;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;

    .line 1977882
    iget-object v2, v0, LX/DFS;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1977883
    move-object v0, v0

    .line 1977884
    iget-object v1, p0, LX/3mX;->l:LX/3mj;

    move-object v1, v1

    .line 1977885
    iget-object v2, v0, LX/DFS;->a:LX/DFT;

    iput-object v1, v2, LX/DFT;->d:LX/3mj;

    .line 1977886
    iget-object v2, v0, LX/DFS;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1977887
    move-object v0, v0

    .line 1977888
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1977889
    :goto_0
    return-object v0

    .line 1977890
    :pswitch_1
    iget-object v0, p0, LX/DFD;->e:LX/DF2;

    const/4 v1, 0x0

    .line 1977891
    new-instance v2, LX/DF1;

    invoke-direct {v2, v0}, LX/DF1;-><init>(LX/DF2;)V

    .line 1977892
    sget-object v3, LX/DF2;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DF0;

    .line 1977893
    if-nez v3, :cond_1

    .line 1977894
    new-instance v3, LX/DF0;

    invoke-direct {v3}, LX/DF0;-><init>()V

    .line 1977895
    :cond_1
    invoke-static {v3, p1, v1, v1, v2}, LX/DF0;->a$redex0(LX/DF0;LX/1De;IILX/DF1;)V

    .line 1977896
    move-object v2, v3

    .line 1977897
    move-object v1, v2

    .line 1977898
    move-object v0, v1

    .line 1977899
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 1977900
    :pswitch_2
    iget-object v0, p0, LX/DFD;->g:LX/DFk;

    const/4 v1, 0x0

    .line 1977901
    new-instance v2, LX/DFj;

    invoke-direct {v2, v0}, LX/DFj;-><init>(LX/DFk;)V

    .line 1977902
    sget-object v3, LX/DFk;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DFi;

    .line 1977903
    if-nez v3, :cond_2

    .line 1977904
    new-instance v3, LX/DFi;

    invoke-direct {v3}, LX/DFi;-><init>()V

    .line 1977905
    :cond_2
    invoke-static {v3, p1, v1, v1, v2}, LX/DFi;->a$redex0(LX/DFi;LX/1De;IILX/DFj;)V

    .line 1977906
    move-object v2, v3

    .line 1977907
    move-object v1, v2

    .line 1977908
    move-object v0, v1

    .line 1977909
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 1977910
    :pswitch_3
    iget-object v0, p0, LX/DFD;->f:LX/DF6;

    const/4 v1, 0x0

    .line 1977911
    new-instance v2, LX/DF5;

    invoke-direct {v2, v0}, LX/DF5;-><init>(LX/DF6;)V

    .line 1977912
    sget-object v3, LX/DF6;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DF4;

    .line 1977913
    if-nez v3, :cond_3

    .line 1977914
    new-instance v3, LX/DF4;

    invoke-direct {v3}, LX/DF4;-><init>()V

    .line 1977915
    :cond_3
    invoke-static {v3, p1, v1, v1, v2}, LX/DF4;->a$redex0(LX/DF4;LX/1De;IILX/DF5;)V

    .line 1977916
    move-object v2, v3

    .line 1977917
    move-object v1, v2

    .line 1977918
    move-object v0, v1

    .line 1977919
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 1

    .prologue
    .line 1977856
    invoke-super {p0, p1}, LX/3mW;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 1977857
    iget-object v0, p0, LX/DFD;->c:LX/2dy;

    iget-object v0, v0, LX/2dy;->b:LX/2dx;

    .line 1977858
    iput-object p1, v0, LX/2dx;->a:Landroid/view/View;

    .line 1977859
    return-void
.end method

.method public final b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 2

    .prologue
    .line 1977849
    invoke-super {p0, p1}, LX/3mW;->b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 1977850
    iget-object v0, p0, LX/DFD;->c:LX/2dy;

    iget-object v0, v0, LX/2dy;->b:LX/2dx;

    const/4 v1, 0x0

    .line 1977851
    iput-object v1, v0, LX/2dx;->a:Landroid/view/View;

    .line 1977852
    return-void
.end method

.method public final synthetic e(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1977855
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1}, LX/DFD;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method

.method public final synthetic f(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1977854
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1}, LX/DFD;->b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1977853
    const/4 v0, 0x0

    return v0
.end method
