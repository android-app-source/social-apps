.class public LX/ECO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0Sh;

.field private final f:Landroid/content/res/Resources;

.field public final g:LX/ECG;

.field public final h:LX/EDe;

.field private final i:LX/0ad;

.field private j:F

.field public k:Z

.field public l:Landroid/media/MediaPlayer;

.field private m:Ljava/util/concurrent/Future;

.field public n:Z

.field public o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2089707
    const-class v0, LX/ECO;

    sput-object v0, LX/ECO;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Sh;Landroid/content/res/Resources;LX/ECH;LX/EDe;LX/0ad;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p7    # LX/EDe;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2089708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2089709
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ECO;->o:Z

    .line 2089710
    iput-object p1, p0, LX/ECO;->b:Landroid/content/Context;

    .line 2089711
    iput-object p2, p0, LX/ECO;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2089712
    iput-object p3, p0, LX/ECO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2089713
    iput-object p4, p0, LX/ECO;->e:LX/0Sh;

    .line 2089714
    iput-object p5, p0, LX/ECO;->f:Landroid/content/res/Resources;

    .line 2089715
    new-instance v0, LX/ECI;

    invoke-direct {v0, p0}, LX/ECI;-><init>(LX/ECO;)V

    .line 2089716
    new-instance p3, LX/ECG;

    invoke-static {p6}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    invoke-static {p6}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object p2

    check-cast p2, LX/2S7;

    invoke-direct {p3, p1, p2, v0}, LX/ECG;-><init>(Landroid/media/AudioManager;LX/2S7;LX/ECI;)V

    .line 2089717
    move-object v0, p3

    .line 2089718
    iput-object v0, p0, LX/ECO;->g:LX/ECG;

    .line 2089719
    iput-object p7, p0, LX/ECO;->h:LX/EDe;

    .line 2089720
    iput-object p8, p0, LX/ECO;->i:LX/0ad;

    .line 2089721
    const/4 v0, 0x0

    iput v0, p0, LX/ECO;->j:F

    .line 2089722
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 2089723
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/ECO;->c(LX/ECO;Z)V

    .line 2089724
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 2089725
    invoke-static {p0, p1}, LX/ECO;->e(LX/ECO;I)V

    .line 2089726
    return-void
.end method

.method private a(ZZZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const v3, 0x3ea3d70a    # 0.32f

    const/4 v2, 0x0

    .line 2089727
    invoke-static {p0, v2}, LX/ECO;->c(LX/ECO;Z)V

    .line 2089728
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 2089729
    if-eqz p3, :cond_2

    .line 2089730
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 2089731
    :goto_0
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    new-instance v1, LX/ECM;

    invoke-direct {v1, p0, p1, p2, p3}, LX/ECM;-><init>(LX/ECO;ZZZ)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 2089732
    if-eqz p3, :cond_4

    .line 2089733
    iget-object v0, p0, LX/ECO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/EGK;->f:LX/0Tn;

    iget v2, p0, LX/ECO;->j:F

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2089734
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 2089735
    iget-object v1, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0, v0}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 2089736
    :cond_0
    :goto_1
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 2089737
    :try_start_0
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    iget-object v1, p0, LX/ECO;->b:Landroid/content/Context;

    .line 2089738
    if-eqz p3, :cond_5

    .line 2089739
    const v2, 0x7f070075

    invoke-static {p0, v2}, LX/ECO;->d(LX/ECO;I)Landroid/net/Uri;

    move-result-object v2

    .line 2089740
    :goto_2
    move-object v2, v2

    .line 2089741
    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2089742
    invoke-direct {p0}, LX/ECO;->h()V

    .line 2089743
    :cond_1
    :goto_3
    return-void

    .line 2089744
    :cond_2
    invoke-static {}, LX/ECO;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2089745
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 2089746
    iput-boolean v4, p0, LX/ECO;->o:Z

    goto :goto_0

    .line 2089747
    :cond_3
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v4}, Landroid/media/MediaPlayer;->setLooping(Z)V

    goto :goto_0

    .line 2089748
    :cond_4
    if-eqz p2, :cond_0

    .line 2089749
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v3, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_1

    .line 2089750
    :catch_0
    invoke-virtual {p0}, LX/ECO;->f()V

    .line 2089751
    if-eqz p1, :cond_1

    .line 2089752
    invoke-static {p0, p2, p3}, LX/ECO;->b(LX/ECO;ZZ)V

    goto :goto_3

    .line 2089753
    :cond_5
    iget-object v2, p0, LX/ECO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/EGK;->d:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2089754
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2089755
    const v2, 0x7f070036

    invoke-static {p0, v2}, LX/ECO;->d(LX/ECO;I)Landroid/net/Uri;

    move-result-object v2

    goto :goto_2

    .line 2089756
    :cond_6
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_2
.end method

.method public static a$redex0(LX/ECO;IZI)V
    .locals 5

    .prologue
    .line 2089757
    invoke-virtual {p0}, LX/ECO;->d()V

    .line 2089758
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_2

    .line 2089759
    :cond_0
    if-eqz p2, :cond_1

    .line 2089760
    invoke-direct {p0, p1}, LX/ECO;->b(I)V

    .line 2089761
    :goto_0
    return-void

    .line 2089762
    :cond_1
    invoke-direct {p0, p1}, LX/ECO;->a(I)V

    goto :goto_0

    .line 2089763
    :cond_2
    int-to-float v0, p3

    const/high16 v1, 0x42480000    # 50.0f

    div-float/2addr v0, v1

    .line 2089764
    iget-object v1, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0, v0}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 2089765
    iget-object v0, p0, LX/ECO;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/audio/RtcAudioHandler$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/rtc/audio/RtcAudioHandler$3;-><init>(LX/ECO;IZI)V

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/ECO;->m:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2089766
    invoke-static {p0, v2}, LX/ECO;->c(LX/ECO;Z)V

    .line 2089767
    invoke-static {}, LX/ECO;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2089768
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 2089769
    iput-boolean v2, p0, LX/ECO;->o:Z

    .line 2089770
    :goto_0
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 2089771
    invoke-static {p0, p1}, LX/ECO;->e(LX/ECO;I)V

    .line 2089772
    return-void

    .line 2089773
    :cond_0
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    goto :goto_0
.end method

.method public static b(LX/ECO;ZZ)V
    .locals 3

    .prologue
    .line 2089774
    sget-object v0, LX/EGK;->d:LX/0Tn;

    .line 2089775
    iget-object v1, p0, LX/ECO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2089776
    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2089777
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2089778
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, LX/ECO;->a(ZZZ)V

    .line 2089779
    return-void
.end method

.method public static c(LX/ECO;Z)V
    .locals 3

    .prologue
    .line 2089780
    invoke-virtual {p0}, LX/ECO;->f()V

    .line 2089781
    invoke-virtual {p0}, LX/ECO;->d()V

    .line 2089782
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ECO;->o:Z

    .line 2089783
    if-eqz p1, :cond_1

    .line 2089784
    iget-object v0, p0, LX/ECO;->g:LX/ECG;

    .line 2089785
    iget-object v1, v0, LX/ECG;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-nez v1, :cond_0

    iget-object v1, v0, LX/ECG;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v1, :cond_3

    .line 2089786
    :cond_0
    :goto_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    .line 2089787
    return-void

    .line 2089788
    :cond_1
    iget-object v0, p0, LX/ECO;->g:LX/ECG;

    const/4 v2, 0x2

    .line 2089789
    iget-object v1, v0, LX/ECG;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-nez v1, :cond_2

    iget-object v1, v0, LX/ECG;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v1, :cond_4

    .line 2089790
    :cond_2
    :goto_1
    goto :goto_0

    .line 2089791
    :cond_3
    new-instance v1, LX/ECF;

    invoke-direct {v1, v0}, LX/ECF;-><init>(LX/ECG;)V

    iput-object v1, v0, LX/ECG;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 2089792
    iget-object v1, v0, LX/ECG;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x0

    const/4 p1, 0x2

    invoke-static {v0, v1, v2, p1}, LX/ECG;->a(LX/ECG;Landroid/media/AudioManager$OnAudioFocusChangeListener;II)Z

    goto :goto_0

    .line 2089793
    :cond_4
    new-instance v1, LX/ECF;

    invoke-direct {v1, v0}, LX/ECF;-><init>(LX/ECG;)V

    iput-object v1, v0, LX/ECG;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 2089794
    iget-object v1, v0, LX/ECG;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-static {v0, v1, v2, v2}, LX/ECG;->a(LX/ECG;Landroid/media/AudioManager$OnAudioFocusChangeListener;II)Z

    goto :goto_1
.end method

.method public static d(LX/ECO;I)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 2089795
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "android.resource"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, LX/ECO;->f:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, LX/ECO;->f:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, LX/ECO;->f:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/ECO;I)V
    .locals 5

    .prologue
    const v4, 0x3ea3d70a    # 0.32f

    const v3, 0x3e4ccccd    # 0.2f

    const/4 v2, 0x0

    .line 2089687
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 2089688
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    new-instance v1, LX/ECM;

    invoke-direct {v1, p0, v2, v2, v2}, LX/ECM;-><init>(LX/ECO;ZZZ)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 2089689
    const v0, 0x7f070071

    if-eq p1, v0, :cond_0

    const v0, 0x7f070072

    if-ne p1, v0, :cond_2

    .line 2089690
    :cond_0
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v3, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 2089691
    :cond_1
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    iget-object v1, p0, LX/ECO;->b:Landroid/content/Context;

    invoke-static {p0, p1}, LX/ECO;->d(LX/ECO;I)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2089692
    invoke-direct {p0}, LX/ECO;->h()V

    .line 2089693
    :goto_1
    return-void

    .line 2089694
    :cond_2
    const v0, 0x7f070075

    if-ne p1, v0, :cond_3

    .line 2089695
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v4, v4}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 2089696
    :cond_3
    const v0, 0x7f070073

    if-eq p1, v0, :cond_4

    const v0, 0x7f070074

    if-ne p1, v0, :cond_1

    .line 2089697
    :cond_4
    iget-object v0, p0, LX/ECO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/EGK;->e:LX/0Tn;

    const-string v2, "1.0"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2089698
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 2089699
    iget-object v1, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0, v0}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 2089700
    :catch_0
    invoke-virtual {p0}, LX/ECO;->f()V

    goto :goto_1
.end method

.method private h()V
    .locals 3

    .prologue
    .line 2089701
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 2089702
    :try_start_0
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2089703
    :goto_0
    return-void

    .line 2089704
    :catch_0
    move-exception v0

    .line 2089705
    sget-object v1, LX/ECO;->a:Ljava/lang/Class;

    const-string v2, "Failed to prepare mediaPlayer"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2089706
    invoke-virtual {p0}, LX/ECO;->f()V

    goto :goto_0
.end method

.method public static j(LX/ECO;)V
    .locals 1

    .prologue
    .line 2089684
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2089685
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 2089686
    :cond_0
    return-void
.end method

.method private static k()Z
    .locals 2

    .prologue
    .line 2089681
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2089682
    const/4 v0, 0x1

    .line 2089683
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2089676
    iget-object v0, p0, LX/ECO;->g:LX/ECG;

    invoke-virtual {v0}, LX/ECG;->e()V

    .line 2089677
    iget-object v0, p0, LX/ECO;->g:LX/ECG;

    invoke-virtual {v0}, LX/ECG;->d()V

    .line 2089678
    invoke-virtual {p0}, LX/ECO;->d()V

    .line 2089679
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ECO;->n:Z

    .line 2089680
    return-void
.end method

.method public final a(LX/ECN;)V
    .locals 5

    .prologue
    const/16 v2, 0x32

    const/4 v4, 0x0

    .line 2089655
    iget-object v0, p0, LX/ECO;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2089656
    iget-boolean v0, p0, LX/ECO;->k:Z

    if-eqz v0, :cond_0

    .line 2089657
    :goto_0
    return-void

    .line 2089658
    :cond_0
    sget-object v0, LX/ECL;->a:[I

    invoke-virtual {p1}, LX/ECN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2089659
    :pswitch_0
    const v0, 0x7f070029

    .line 2089660
    :goto_1
    invoke-direct {p0, v0}, LX/ECO;->a(I)V

    goto :goto_0

    .line 2089661
    :pswitch_1
    const v0, 0x7f07009c

    goto :goto_1

    .line 2089662
    :pswitch_2
    const v0, 0x7f07009a

    goto :goto_1

    .line 2089663
    :pswitch_3
    const v0, 0x7f07009b

    .line 2089664
    const/4 v1, 0x1

    invoke-static {p0, v1}, LX/ECO;->c(LX/ECO;Z)V

    .line 2089665
    iget-object v1, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    new-instance v2, LX/ECJ;

    invoke-direct {v2, p0}, LX/ECJ;-><init>(LX/ECO;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 2089666
    invoke-static {p0, v0}, LX/ECO;->e(LX/ECO;I)V

    .line 2089667
    goto :goto_0

    .line 2089668
    :pswitch_4
    const v0, 0x7f070099

    goto :goto_1

    .line 2089669
    :pswitch_5
    const v0, 0x7f07009d

    invoke-direct {p0, v0}, LX/ECO;->b(I)V

    goto :goto_0

    .line 2089670
    :pswitch_6
    const v0, 0x7f070028

    invoke-static {p0, v0, v4, v2}, LX/ECO;->a$redex0(LX/ECO;IZI)V

    goto :goto_0

    .line 2089671
    :pswitch_7
    const v0, 0x7f070047

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, v2}, LX/ECO;->a$redex0(LX/ECO;IZI)V

    goto :goto_0

    .line 2089672
    :pswitch_8
    const v0, 0x7f070071

    goto :goto_1

    .line 2089673
    :pswitch_9
    const v0, 0x7f070072

    goto :goto_1

    .line 2089674
    :pswitch_a
    iget-object v0, p0, LX/ECO;->i:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/3Dx;->aO:S

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    .line 2089675
    if-eqz v0, :cond_1

    const v0, 0x7f070073

    goto :goto_1

    :cond_1
    const v0, 0x7f070074

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 2089652
    iget-boolean v0, p0, LX/ECO;->k:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 2089653
    :goto_0
    return-void

    .line 2089654
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, LX/ECO;->a(ZZZ)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2089647
    iget-boolean v1, p0, LX/ECO;->n:Z

    if-eqz v1, :cond_0

    .line 2089648
    :goto_0
    return v0

    .line 2089649
    :cond_0
    invoke-virtual {p0}, LX/ECO;->d()V

    .line 2089650
    iput-boolean v0, p0, LX/ECO;->n:Z

    .line 2089651
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2089643
    iget-boolean v0, p0, LX/ECO;->k:Z

    if-eqz v0, :cond_0

    .line 2089644
    :goto_0
    return-void

    .line 2089645
    :cond_0
    invoke-virtual {p0}, LX/ECO;->d()V

    .line 2089646
    iget-object v0, p0, LX/ECO;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/audio/RtcAudioHandler$2;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/audio/RtcAudioHandler$2;-><init>(LX/ECO;)V

    const-wide/16 v2, 0x2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/ECO;->m:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2089625
    iget-object v0, p0, LX/ECO;->m:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 2089626
    iget-object v0, p0, LX/ECO;->m:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2089627
    const/4 v0, 0x0

    iput-object v0, p0, LX/ECO;->m:Ljava/util/concurrent/Future;

    .line 2089628
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2089636
    iget-object v0, p0, LX/ECO;->g:LX/ECG;

    invoke-virtual {v0}, LX/ECG;->e()V

    .line 2089637
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 2089638
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 2089639
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 2089640
    const/4 v0, 0x0

    iput-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    .line 2089641
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ECO;->o:Z

    .line 2089642
    return-void
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 2089631
    iget-boolean v0, p0, LX/ECO;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 2089632
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 2089633
    iget-object v0, p0, LX/ECO;->l:Landroid/media/MediaPlayer;

    new-instance v1, LX/ECK;

    invoke-direct {v1, p0}, LX/ECK;-><init>(LX/ECO;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 2089634
    :goto_0
    return-void

    .line 2089635
    :cond_0
    invoke-virtual {p0}, LX/ECO;->f()V

    goto :goto_0
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 2089629
    invoke-static {p0}, LX/ECO;->j(LX/ECO;)V

    .line 2089630
    return-void
.end method
