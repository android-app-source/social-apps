.class public final enum LX/EDr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EDr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EDr;

.field public static final enum DISCONNECTED:LX/EDr;

.field public static final enum FAIR:LX/EDr;

.field public static final enum GOOD:LX/EDr;

.field public static final enum POOR:LX/EDr;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2091950
    new-instance v0, LX/EDr;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v2}, LX/EDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDr;->DISCONNECTED:LX/EDr;

    .line 2091951
    new-instance v0, LX/EDr;

    const-string v1, "POOR"

    invoke-direct {v0, v1, v3}, LX/EDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDr;->POOR:LX/EDr;

    .line 2091952
    new-instance v0, LX/EDr;

    const-string v1, "FAIR"

    invoke-direct {v0, v1, v4}, LX/EDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDr;->FAIR:LX/EDr;

    .line 2091953
    new-instance v0, LX/EDr;

    const-string v1, "GOOD"

    invoke-direct {v0, v1, v5}, LX/EDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDr;->GOOD:LX/EDr;

    .line 2091954
    const/4 v0, 0x4

    new-array v0, v0, [LX/EDr;

    sget-object v1, LX/EDr;->DISCONNECTED:LX/EDr;

    aput-object v1, v0, v2

    sget-object v1, LX/EDr;->POOR:LX/EDr;

    aput-object v1, v0, v3

    sget-object v1, LX/EDr;->FAIR:LX/EDr;

    aput-object v1, v0, v4

    sget-object v1, LX/EDr;->GOOD:LX/EDr;

    aput-object v1, v0, v5

    sput-object v0, LX/EDr;->$VALUES:[LX/EDr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2091955
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EDr;
    .locals 1

    .prologue
    .line 2091956
    const-class v0, LX/EDr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EDr;

    return-object v0
.end method

.method public static values()[LX/EDr;
    .locals 1

    .prologue
    .line 2091957
    sget-object v0, LX/EDr;->$VALUES:[LX/EDr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EDr;

    return-object v0
.end method
