.class public LX/DD4;
.super LX/3mU;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

.field private final b:LX/DD9;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;LX/DD9;)V
    .locals 0

    .prologue
    .line 1974842
    invoke-direct {p0}, LX/3mU;-><init>()V

    .line 1974843
    iput-object p1, p0, LX/DD4;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    .line 1974844
    iput-object p2, p0, LX/DD4;->b:LX/DD9;

    .line 1974845
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1974846
    iget-object v0, p0, LX/DD4;->b:LX/DD9;

    iget-object v1, p0, LX/DD4;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v0, v1}, LX/DD9;->b(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)V

    .line 1974847
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 1974848
    iget-object v0, p0, LX/DD4;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DD4;->b:LX/DD9;

    iget-object v1, p0, LX/DD4;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v0, v1, p1}, LX/DD9;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DD4;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-static {v0}, LX/DD9;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
