.class public final enum LX/Ejs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ejs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ejs;

.field public static final enum CCU_CONTACTS_UPLOAD_FAILED:LX/Ejs;

.field public static final enum CCU_CONTACTS_UPLOAD_INFORMATION:LX/Ejs;

.field public static final enum CCU_CONTACTS_UPLOAD_SUCCEEDED:LX/Ejs;

.field public static final enum CCU_CREATE_SESSION_CHECK_SYNC:LX/Ejs;

.field public static final enum CCU_FRIENDABLE_INVITABLE_CACHE:LX/Ejs;

.field public static final enum CCU_INVALID_CONTACT_ID:LX/Ejs;

.field public static final enum CCU_SETTING:LX/Ejs;

.field public static final enum CCU_SETTING_FAILED:LX/Ejs;

.field public static final enum CLOSE_SESSION_FAILURE:LX/Ejs;

.field public static final enum CLOSE_SESSION_START:LX/Ejs;

.field public static final enum CLOSE_SESSION_SUCCESS:LX/Ejs;

.field public static final enum CREATE_SESSION_FAILURE:LX/Ejs;

.field public static final enum CREATE_SESSION_START:LX/Ejs;

.field public static final enum CREATE_SESSION_SUCCESS:LX/Ejs;


# instance fields
.field private final mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2162773
    new-instance v0, LX/Ejs;

    const-string v1, "CCU_SETTING"

    const-string v2, "ccu_setting_enable_disable_event"

    invoke-direct {v0, v1, v4, v2}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CCU_SETTING:LX/Ejs;

    .line 2162774
    new-instance v0, LX/Ejs;

    const-string v1, "CCU_SETTING_FAILED"

    const-string v2, "ccu_setting_failed_event"

    invoke-direct {v0, v1, v5, v2}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CCU_SETTING_FAILED:LX/Ejs;

    .line 2162775
    new-instance v0, LX/Ejs;

    const-string v1, "CCU_CREATE_SESSION_CHECK_SYNC"

    const-string v2, "ccu_create_session_check_sync_event"

    invoke-direct {v0, v1, v6, v2}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CCU_CREATE_SESSION_CHECK_SYNC:LX/Ejs;

    .line 2162776
    new-instance v0, LX/Ejs;

    const-string v1, "CCU_CONTACTS_UPLOAD_SUCCEEDED"

    const-string v2, "ccu_contacts_upload_succeeded_event"

    invoke-direct {v0, v1, v7, v2}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CCU_CONTACTS_UPLOAD_SUCCEEDED:LX/Ejs;

    .line 2162777
    new-instance v0, LX/Ejs;

    const-string v1, "CCU_CONTACTS_UPLOAD_FAILED"

    const-string v2, "ccu_contacts_upload_failed_event"

    invoke-direct {v0, v1, v8, v2}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CCU_CONTACTS_UPLOAD_FAILED:LX/Ejs;

    .line 2162778
    new-instance v0, LX/Ejs;

    const-string v1, "CCU_INVALID_CONTACT_ID"

    const/4 v2, 0x5

    const-string v3, "ccu_invalid_contact_id_event"

    invoke-direct {v0, v1, v2, v3}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CCU_INVALID_CONTACT_ID:LX/Ejs;

    .line 2162779
    new-instance v0, LX/Ejs;

    const-string v1, "CCU_CONTACTS_UPLOAD_INFORMATION"

    const/4 v2, 0x6

    const-string v3, "ccu_contacts_upload_information_event"

    invoke-direct {v0, v1, v2, v3}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CCU_CONTACTS_UPLOAD_INFORMATION:LX/Ejs;

    .line 2162780
    new-instance v0, LX/Ejs;

    const-string v1, "CCU_FRIENDABLE_INVITABLE_CACHE"

    const/4 v2, 0x7

    const-string v3, "ccu_friendable_invitable_cache_event"

    invoke-direct {v0, v1, v2, v3}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CCU_FRIENDABLE_INVITABLE_CACHE:LX/Ejs;

    .line 2162781
    new-instance v0, LX/Ejs;

    const-string v1, "CREATE_SESSION_START"

    const/16 v2, 0x8

    const-string v3, "create_session_start"

    invoke-direct {v0, v1, v2, v3}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CREATE_SESSION_START:LX/Ejs;

    .line 2162782
    new-instance v0, LX/Ejs;

    const-string v1, "CREATE_SESSION_SUCCESS"

    const/16 v2, 0x9

    const-string v3, "create_session_success"

    invoke-direct {v0, v1, v2, v3}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CREATE_SESSION_SUCCESS:LX/Ejs;

    .line 2162783
    new-instance v0, LX/Ejs;

    const-string v1, "CREATE_SESSION_FAILURE"

    const/16 v2, 0xa

    const-string v3, "create_session_failure"

    invoke-direct {v0, v1, v2, v3}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CREATE_SESSION_FAILURE:LX/Ejs;

    .line 2162784
    new-instance v0, LX/Ejs;

    const-string v1, "CLOSE_SESSION_START"

    const/16 v2, 0xb

    const-string v3, "close_session_start"

    invoke-direct {v0, v1, v2, v3}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CLOSE_SESSION_START:LX/Ejs;

    .line 2162785
    new-instance v0, LX/Ejs;

    const-string v1, "CLOSE_SESSION_SUCCESS"

    const/16 v2, 0xc

    const-string v3, "close_session_success"

    invoke-direct {v0, v1, v2, v3}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CLOSE_SESSION_SUCCESS:LX/Ejs;

    .line 2162786
    new-instance v0, LX/Ejs;

    const-string v1, "CLOSE_SESSION_FAILURE"

    const/16 v2, 0xd

    const-string v3, "close_session_failure"

    invoke-direct {v0, v1, v2, v3}, LX/Ejs;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ejs;->CLOSE_SESSION_FAILURE:LX/Ejs;

    .line 2162787
    const/16 v0, 0xe

    new-array v0, v0, [LX/Ejs;

    sget-object v1, LX/Ejs;->CCU_SETTING:LX/Ejs;

    aput-object v1, v0, v4

    sget-object v1, LX/Ejs;->CCU_SETTING_FAILED:LX/Ejs;

    aput-object v1, v0, v5

    sget-object v1, LX/Ejs;->CCU_CREATE_SESSION_CHECK_SYNC:LX/Ejs;

    aput-object v1, v0, v6

    sget-object v1, LX/Ejs;->CCU_CONTACTS_UPLOAD_SUCCEEDED:LX/Ejs;

    aput-object v1, v0, v7

    sget-object v1, LX/Ejs;->CCU_CONTACTS_UPLOAD_FAILED:LX/Ejs;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Ejs;->CCU_INVALID_CONTACT_ID:LX/Ejs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Ejs;->CCU_CONTACTS_UPLOAD_INFORMATION:LX/Ejs;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Ejs;->CCU_FRIENDABLE_INVITABLE_CACHE:LX/Ejs;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Ejs;->CREATE_SESSION_START:LX/Ejs;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Ejs;->CREATE_SESSION_SUCCESS:LX/Ejs;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Ejs;->CREATE_SESSION_FAILURE:LX/Ejs;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Ejs;->CLOSE_SESSION_START:LX/Ejs;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Ejs;->CLOSE_SESSION_SUCCESS:LX/Ejs;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Ejs;->CLOSE_SESSION_FAILURE:LX/Ejs;

    aput-object v2, v0, v1

    sput-object v0, LX/Ejs;->$VALUES:[LX/Ejs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2162767
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2162768
    iput-object p3, p0, LX/Ejs;->mEventName:Ljava/lang/String;

    .line 2162769
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ejs;
    .locals 1

    .prologue
    .line 2162770
    const-class v0, LX/Ejs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ejs;

    return-object v0
.end method

.method public static values()[LX/Ejs;
    .locals 1

    .prologue
    .line 2162771
    sget-object v0, LX/Ejs;->$VALUES:[LX/Ejs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ejs;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2162772
    iget-object v0, p0, LX/Ejs;->mEventName:Ljava/lang/String;

    return-object v0
.end method
