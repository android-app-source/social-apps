.class public final LX/ESC;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "LX/ESD;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultDraweeGridAdapter;)V
    .locals 0

    .prologue
    .line 2122561
    iput-object p1, p0, LX/ESC;->a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2122562
    check-cast p1, [Ljava/lang/String;

    const/4 v4, 0x0

    const/16 v3, 0x28

    const/4 v8, 0x0

    .line 2122563
    aget-object v1, p1, v4

    .line 2122564
    invoke-static {v3}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v9

    .line 2122565
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2122566
    new-instance v10, LX/ESD;

    invoke-direct {v10}, LX/ESD;-><init>()V

    .line 2122567
    :try_start_0
    iget-object v2, p0, LX/ESC;->a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    iget-object v2, v2, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->j:LX/11H;

    new-instance v3, LX/ERO;

    invoke-direct {v3}, LX/ERO;-><init>()V

    new-instance v4, LX/ERP;

    const/16 v5, 0x28

    invoke-direct {v4, v5, v1}, LX/ERP;-><init>(ILjava/lang/String;)V

    sget-object v1, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4, v1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/facebook/vault/protocol/VaultAllImagesGetResult;

    move-object v7, v0

    .line 2122568
    iget-object v1, v7, Lcom/facebook/vault/protocol/VaultAllImagesGetResult;->data:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2122569
    iget-object v1, v7, Lcom/facebook/vault/protocol/VaultAllImagesGetResult;->data:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/facebook/vault/protocol/VaultImageResultObject;

    move-object v4, v0

    .line 2122570
    iget-object v1, v4, Lcom/facebook/vault/protocol/VaultImageResultObject;->dateTaken:Ljava/lang/String;

    const-string v2, "T"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 2122571
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-ddHH:mm:ssZ"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    .line 2122572
    new-instance v1, Lcom/facebook/photos/base/photos/VaultRemotePhoto;

    iget-wide v2, v4, Lcom/facebook/vault/protocol/VaultImageResultObject;->fbid:J

    iget-object v4, v4, Lcom/facebook/vault/protocol/VaultImageResultObject;->uri:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-direct/range {v1 .. v6}, Lcom/facebook/photos/base/photos/VaultRemotePhoto;-><init>(JLjava/lang/String;J)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2122573
    :catch_0
    move-exception v1

    .line 2122574
    :try_start_1
    sget-object v2, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->b:Ljava/lang/String;

    const-string v3, "Exception from graph API call /me/vaultimages"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2122575
    iput-object v9, v10, LX/ESD;->a:Ljava/util/List;

    .line 2122576
    iput-object v8, v10, LX/ESD;->b:Ljava/lang/String;

    .line 2122577
    :goto_1
    return-object v10

    .line 2122578
    :cond_0
    :try_start_2
    iget-object v1, v7, Lcom/facebook/vault/protocol/VaultAllImagesGetResult;->paging:Lcom/facebook/vault/protocol/VaultPagingObject;

    if-nez v1, :cond_1

    move-object v1, v8

    .line 2122579
    :goto_2
    if-eqz v1, :cond_2

    .line 2122580
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2122581
    const-string v2, "after"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 2122582
    :goto_3
    iput-object v9, v10, LX/ESD;->a:Ljava/util/List;

    .line 2122583
    iput-object v1, v10, LX/ESD;->b:Ljava/lang/String;

    goto :goto_1

    .line 2122584
    :cond_1
    :try_start_3
    iget-object v1, v7, Lcom/facebook/vault/protocol/VaultAllImagesGetResult;->paging:Lcom/facebook/vault/protocol/VaultPagingObject;

    iget-object v1, v1, Lcom/facebook/vault/protocol/VaultPagingObject;->next:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 2122585
    :catchall_0
    move-exception v1

    iput-object v9, v10, LX/ESD;->a:Ljava/util/List;

    .line 2122586
    iput-object v8, v10, LX/ESD;->b:Ljava/lang/String;

    throw v1

    :cond_2
    move-object v1, v8

    goto :goto_3
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2122587
    check-cast p1, LX/ESD;

    .line 2122588
    iget-object v0, p1, LX/ESD;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2122589
    iget-object v0, p0, LX/ESC;->a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    iget-object v1, p1, LX/ESD;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->a(Ljava/util/List;)V

    .line 2122590
    iget-object v0, p1, LX/ESD;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2122591
    iget-object v0, p0, LX/ESC;->a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    iget-object v1, p1, LX/ESD;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->a:Ljava/lang/String;

    .line 2122592
    :goto_0
    iget-object v0, p0, LX/ESC;->a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->f:LX/ESB;

    sget-object v1, LX/ESB;->FETCHED_ALL:LX/ESB;

    if-eq v0, v1, :cond_0

    .line 2122593
    iget-object v0, p0, LX/ESC;->a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    sget-object v1, LX/ESB;->IDLE:LX/ESB;

    .line 2122594
    iput-object v1, v0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->f:LX/ESB;

    .line 2122595
    :cond_0
    return-void

    .line 2122596
    :cond_1
    iget-object v0, p0, LX/ESC;->a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    sget-object v1, LX/ESB;->FETCHED_ALL:LX/ESB;

    .line 2122597
    iput-object v1, v0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->f:LX/ESB;

    .line 2122598
    goto :goto_0

    .line 2122599
    :cond_2
    iget-object v0, p0, LX/ESC;->a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    sget-object v1, LX/ESB;->FETCHED_ALL:LX/ESB;

    .line 2122600
    iput-object v1, v0, Lcom/facebook/vault/ui/VaultDraweeGridAdapter;->f:LX/ESB;

    .line 2122601
    goto :goto_0
.end method
