.class public LX/Dxd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9hF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9hF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2063098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2063099
    iput-object p1, p0, LX/Dxd;->a:LX/0Ot;

    .line 2063100
    iput-object p2, p0, LX/Dxd;->b:LX/0Ot;

    .line 2063101
    return-void
.end method

.method public static a(LX/0QB;)LX/Dxd;
    .locals 1

    .prologue
    .line 2063097
    invoke-static {p0}, LX/Dxd;->b(LX/0QB;)LX/Dxd;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Dxd;
    .locals 3

    .prologue
    .line 2063065
    new-instance v0, LX/Dxd;

    const/16 v1, 0xf2f

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2e6c

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/Dxd;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2063066
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V
    .locals 3
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "LX/0Px",
            "<+",
            "LX/1U8;",
            ">;",
            "LX/74S;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2063090
    iget-object v0, p0, LX/Dxd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p2}, LX/9hF;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/9hE;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/9hE;->b(LX/0Px;)LX/9hE;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v2

    if-eqz p4, :cond_0

    invoke-static {p4}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    .line 2063091
    iput-boolean p7, v0, LX/9hD;->m:Z

    .line 2063092
    move-object v0, v0

    .line 2063093
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v2

    .line 2063094
    iget-object v0, p0, LX/Dxd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    invoke-interface {v0, p1, v2, v1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2063095
    return-void

    :cond_0
    move-object v0, v1

    .line 2063096
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V
    .locals 3
    .param p3    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "LX/0Px",
            "<+",
            "LX/1U8;",
            ">;",
            "LX/74S;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2063083
    invoke-static {p4}, LX/9hF;->c(LX/0Px;)LX/9hE;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v2

    if-eqz p3, :cond_0

    invoke-static {p3}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    .line 2063084
    iput-boolean p6, v0, LX/9hD;->m:Z

    .line 2063085
    move-object v0, v0

    .line 2063086
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v2

    .line 2063087
    iget-object v0, p0, LX/Dxd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    invoke-interface {v0, p1, v2, v1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2063088
    return-void

    :cond_0
    move-object v0, v1

    .line 2063089
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V
    .locals 3
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "LX/0Px",
            "<+",
            "LX/1U8;",
            ">;",
            "LX/74S;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2063076
    iget-object v0, p0, LX/Dxd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p2}, LX/9hF;->b(Ljava/lang/String;)LX/9hE;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/9hE;->b(LX/0Px;)LX/9hE;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v2

    if-eqz p4, :cond_0

    invoke-static {p4}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    .line 2063077
    iput-boolean p7, v0, LX/9hD;->m:Z

    .line 2063078
    move-object v0, v0

    .line 2063079
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v2

    .line 2063080
    iget-object v0, p0, LX/Dxd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    invoke-interface {v0, p1, v2, v1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2063081
    return-void

    :cond_0
    move-object v0, v1

    .line 2063082
    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V
    .locals 4
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "LX/0Px",
            "<+",
            "LX/1U8;",
            ">;",
            "LX/74S;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2063067
    iget-object v0, p0, LX/Dxd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2063068
    new-instance v0, LX/9hE;

    const-class v2, LX/23c;

    new-instance v3, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-direct {v3, p2}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v2

    invoke-direct {v0, v2}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    move-object v0, v0

    .line 2063069
    invoke-virtual {v0, p5}, LX/9hE;->b(LX/0Px;)LX/9hE;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v2

    if-eqz p4, :cond_0

    invoke-static {p4}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    .line 2063070
    iput-boolean p7, v0, LX/9hD;->m:Z

    .line 2063071
    move-object v0, v0

    .line 2063072
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v2

    .line 2063073
    iget-object v0, p0, LX/Dxd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    invoke-interface {v0, p1, v2, v1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2063074
    return-void

    :cond_0
    move-object v0, v1

    .line 2063075
    goto :goto_0
.end method
