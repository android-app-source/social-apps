.class public final enum LX/ED4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ED4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ED4;

.field public static final enum AUDIO:LX/ED4;

.field public static final enum VIDEO:LX/ED4;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2090853
    new-instance v0, LX/ED4;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v2}, LX/ED4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ED4;->AUDIO:LX/ED4;

    .line 2090854
    new-instance v0, LX/ED4;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/ED4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ED4;->VIDEO:LX/ED4;

    .line 2090855
    const/4 v0, 0x2

    new-array v0, v0, [LX/ED4;

    sget-object v1, LX/ED4;->AUDIO:LX/ED4;

    aput-object v1, v0, v2

    sget-object v1, LX/ED4;->VIDEO:LX/ED4;

    aput-object v1, v0, v3

    sput-object v0, LX/ED4;->$VALUES:[LX/ED4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2090856
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ED4;
    .locals 1

    .prologue
    .line 2090857
    const-class v0, LX/ED4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ED4;

    return-object v0
.end method

.method public static values()[LX/ED4;
    .locals 1

    .prologue
    .line 2090858
    sget-object v0, LX/ED4;->$VALUES:[LX/ED4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ED4;

    return-object v0
.end method
