.class public final LX/Dwf;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLAlbum;

.field public final synthetic b:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;Lcom/facebook/graphql/model/GraphQLAlbum;)V
    .locals 0

    .prologue
    .line 2061189
    iput-object p1, p0, LX/Dwf;->b:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iput-object p2, p0, LX/Dwf;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 7

    .prologue
    .line 2061190
    iget-object v0, p0, LX/Dwf;->b:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->n:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_0

    .line 2061191
    iget-object v0, p0, LX/Dwf;->b:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->J:LX/9bD;

    iget-object v1, p0, LX/Dwf;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0, v1}, LX/9bD;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/9bC;

    move-result-object v0

    iget-object v1, p0, LX/Dwf;->b:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 2061192
    new-instance v2, LX/4mb;

    invoke-virtual {v0}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/4mb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0811de

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, LX/9bC;->d(LX/9bC;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    move-result-object v2

    invoke-virtual {v0}, LX/9b8;->b()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0811f7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2061193
    new-instance v4, LX/9bB;

    invoke-direct {v4, v0, v1}, LX/9bB;-><init>(LX/9bC;Landroid/app/Activity;)V

    move-object v4, v4

    .line 2061194
    invoke-virtual {v2, v3, v4}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    move-result-object v2

    .line 2061195
    invoke-virtual {v2}, LX/4mb;->show()Landroid/app/AlertDialog;

    .line 2061196
    :goto_0
    return-void

    .line 2061197
    :cond_0
    iget-object v0, p0, LX/Dwf;->b:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->l:LX/DxZ;

    iget-object v1, p0, LX/Dwf;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0, v1}, LX/DxZ;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/DxY;

    move-result-object v0

    .line 2061198
    iget-object v1, p0, LX/Dwf;->b:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, LX/Dwf;->b:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v1, p0, LX/Dwf;->b:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2061199
    iget-object p0, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz p0, :cond_2

    iget-object p0, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_2

    iget-object p0, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object p0

    if-eqz p0, :cond_2

    iget-object p0, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    if-eqz p0, :cond_2

    iget-object p0, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p0

    if-eqz p0, :cond_2

    iget-object p0, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_2

    iget-object p0, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    if-eqz p0, :cond_2

    move p0, v4

    .line 2061200
    :goto_1
    if-eqz p0, :cond_3

    iget-object p0, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2061201
    iget-boolean v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move p0, v1

    .line 2061202
    if-nez p0, :cond_3

    :goto_2
    move v4, v4

    .line 2061203
    const/4 v5, 0x0

    move-object v1, p1

    const/4 p2, 0x0

    .line 2061204
    new-instance p0, LX/6WS;

    iget-object v6, v0, LX/9b8;->a:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct {p0, v6}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2061205
    const v6, 0x7f110002

    invoke-virtual {p0, v6}, LX/5OM;->b(I)V

    .line 2061206
    if-nez v5, :cond_1

    .line 2061207
    invoke-virtual {p0}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    const p1, 0x7f0d31f9

    invoke-virtual {v6, p1}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 2061208
    invoke-interface {v6, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2061209
    :cond_1
    if-nez v4, :cond_4

    .line 2061210
    invoke-virtual {p0}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    const p1, 0x7f0d31fa

    invoke-virtual {v6, p1}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 2061211
    invoke-interface {v6, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2061212
    :goto_3
    new-instance v6, LX/DxX;

    invoke-direct {v6, v0, v2, v3}, LX/DxX;-><init>(LX/DxY;Landroid/app/Activity;Landroid/support/v4/app/Fragment;)V

    move-object v6, v6

    .line 2061213
    iput-object v6, p0, LX/5OM;->p:LX/5OO;

    .line 2061214
    invoke-virtual {p0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2061215
    invoke-virtual {p0, p2}, LX/0ht;->c(Z)V

    .line 2061216
    invoke-virtual {p0}, LX/0ht;->d()V

    .line 2061217
    goto/16 :goto_0

    :cond_2
    move p0, v5

    .line 2061218
    goto :goto_1

    :cond_3
    move v4, v5

    .line 2061219
    goto :goto_2

    .line 2061220
    :cond_4
    invoke-virtual {p0}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    const p1, 0x7f0d31fb

    invoke-virtual {v6, p1}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 2061221
    invoke-interface {v6, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3
.end method
