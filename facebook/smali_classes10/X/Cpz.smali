.class public final LX/Cpz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CqA;


# direct methods
.method public constructor <init>(LX/CqA;)V
    .locals 0

    .prologue
    .line 1938842
    iput-object p1, p0, LX/Cpz;->a:LX/CqA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x30d0bba4    # -2.9404928E9f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1938843
    iget-object v1, p0, LX/Cpz;->a:LX/CqA;

    .line 1938844
    new-instance v3, LX/CsI;

    iget-object v4, v1, LX/CqA;->s:LX/Crz;

    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/CsI;-><init>(LX/Crz;Landroid/content/Context;)V

    .line 1938845
    iget-object v4, v1, LX/CqA;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v3, v4}, LX/0ht;->c(Landroid/view/View;)V

    .line 1938846
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/5OM;->a(Z)V

    .line 1938847
    sget-object v4, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v3, v4}, LX/0ht;->a(LX/3AV;)V

    .line 1938848
    new-instance v4, LX/Cq0;

    invoke-direct {v4, v1}, LX/Cq0;-><init>(LX/CqA;)V

    .line 1938849
    iput-object v4, v3, LX/0ht;->I:LX/2yQ;

    .line 1938850
    invoke-virtual {v3}, LX/5OM;->c()LX/5OG;

    move-result-object v4

    .line 1938851
    if-eqz v4, :cond_0

    .line 1938852
    const v5, 0x7f081c67

    invoke-virtual {v4, v5}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v5

    const p0, 0x7f02081d

    invoke-virtual {v5, p0}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v5

    new-instance p0, LX/Cq8;

    invoke-direct {p0, v1}, LX/Cq8;-><init>(LX/CqA;)V

    invoke-interface {v5, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1938853
    const v5, 0x7f081c68

    invoke-virtual {v4, v5}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v4

    const v5, 0x7f0208ef

    invoke-virtual {v4, v5}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v4

    new-instance v5, LX/Cpr;

    invoke-direct {v5, v1}, LX/Cpr;-><init>(LX/CqA;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1938854
    invoke-virtual {v3}, LX/0ht;->d()V

    .line 1938855
    :cond_0
    const v1, 0x4f912403

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
