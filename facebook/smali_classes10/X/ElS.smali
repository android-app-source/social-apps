.class public final LX/ElS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2164425
    return-void
.end method

.method private static a(Ljava/io/DataInputStream;)I
    .locals 4

    .prologue
    .line 2164418
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v0

    .line 2164419
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v1

    .line 2164420
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    .line 2164421
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v3

    .line 2164422
    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    and-int/lit16 v1, v2, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    and-int/lit16 v1, v3, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    .line 2164423
    return v0
.end method

.method public static a(Ljava/io/InputStream;)Ljava/nio/ByteBuffer;
    .locals 5

    .prologue
    .line 2164401
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2164402
    :try_start_0
    invoke-static {v1}, LX/ElS;->a(Ljava/io/DataInputStream;)I

    move-result v0

    .line 2164403
    invoke-static {v1}, LX/ElS;->a(Ljava/io/DataInputStream;)I

    move-result v2

    .line 2164404
    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x4

    .line 2164405
    if-nez v0, :cond_0

    .line 2164406
    invoke-static {v1, v2}, LX/ElS;->a(Ljava/io/DataInputStream;I)[B

    move-result-object v0

    .line 2164407
    new-instance v2, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 2164408
    invoke-static {v2}, LX/ElI;->a(Ljava/lang/String;)V

    .line 2164409
    new-instance v0, LX/ElR;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Expected error reply, got: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LX/ElR;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2164410
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    throw v0

    .line 2164411
    :cond_0
    if-lez v2, :cond_1

    .line 2164412
    int-to-long v2, v2

    :try_start_1
    invoke-static {v1, v2, v3}, LX/ElS;->a(Ljava/io/InputStream;J)V

    .line 2164413
    :cond_1
    invoke-static {v1, v0}, LX/ElS;->a(Ljava/io/DataInputStream;I)[B

    move-result-object v0

    .line 2164414
    invoke-virtual {v1}, Ljava/io/DataInputStream;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 2164415
    const-string v2, "FlatbufferGraphQLProtocol"

    const-string v3, "Ignoring junk after flatbuffer data"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2164416
    :cond_2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2164417
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    return-object v0
.end method

.method private static a(Ljava/io/InputStream;J)V
    .locals 5

    .prologue
    .line 2164397
    invoke-virtual {p0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    .line 2164398
    cmp-long v2, v0, p1

    if-eqz v2, :cond_0

    .line 2164399
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Expected "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes; got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2164400
    :cond_0
    return-void
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/OutputStream;I[B)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2164378
    move v0, p2

    .line 2164379
    :goto_0
    if-lez v0, :cond_1

    .line 2164380
    array-length v1, p3

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2164381
    invoke-virtual {p0, p3, v2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 2164382
    if-gez v1, :cond_0

    .line 2164383
    new-instance v1, Ljava/io/EOFException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes, got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-int v0, p2, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2164384
    :cond_0
    invoke-virtual {p1, p3, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 2164385
    sub-int/2addr v0, v1

    .line 2164386
    goto :goto_0

    .line 2164387
    :cond_1
    return-void
.end method

.method private static a(Ljava/io/DataInputStream;I)[B
    .locals 3

    .prologue
    .line 2164388
    if-gez p1, :cond_0

    .line 2164389
    new-instance v0, Ljava/io/EOFException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2164390
    :cond_0
    const/high16 v0, 0x40000

    if-le p1, v0, :cond_1

    .line 2164391
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2164392
    const/16 v1, 0x800

    new-array v1, v1, [B

    invoke-static {p0, v0, p1, v1}, LX/ElS;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I[B)V

    .line 2164393
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 2164394
    :goto_0
    return-object v0

    .line 2164395
    :cond_1
    new-array v0, p1, [B

    .line 2164396
    invoke-virtual {p0, v0}, Ljava/io/DataInputStream;->readFully([B)V

    goto :goto_0
.end method
