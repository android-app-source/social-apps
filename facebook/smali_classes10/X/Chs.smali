.class public LX/Chs;
.super LX/ChM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/ChM",
        "<",
        "LX/CiK;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v7/widget/RecyclerView;

.field private final b:LX/1P1;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 1928615
    invoke-direct {p0}, LX/ChM;-><init>()V

    .line 1928616
    iput-object p1, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    .line 1928617
    iget-object v0, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    iput-object v0, p0, LX/Chs;->b:LX/1P1;

    .line 1928618
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/CiK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1928649
    const-class v0, LX/CiK;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 1928619
    check-cast p1, LX/CiK;

    const/4 v4, 0x0

    .line 1928620
    iget-object v0, p1, LX/CiK;->b:Landroid/view/View;

    move-object v1, v0

    .line 1928621
    iget-object v0, p1, LX/CiK;->a:LX/CiJ;

    move-object v0, v0

    .line 1928622
    sget-object v2, LX/CiJ;->NATIVE_ADS_VIDEO_SET_FOCUSED_VIEW:LX/CiJ;

    if-ne v0, v2, :cond_2

    .line 1928623
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v2, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    if-eq v0, v2, :cond_1

    move-object v0, v1

    .line 1928624
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_0

    .line 1928625
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    .line 1928626
    :cond_0
    iput-object v0, p0, LX/Chs;->c:Landroid/view/View;

    .line 1928627
    :cond_1
    :goto_1
    return-void

    .line 1928628
    :cond_2
    sget-object v2, LX/CiJ;->NATIVE_ADS_VIDEO_UNSET_FOCUSED_VIEW:LX/CiJ;

    if-ne v0, v2, :cond_4

    .line 1928629
    iget-object v0, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    if-ne v1, v0, :cond_1

    .line 1928630
    iget-object v0, p0, LX/Chs;->c:Landroid/view/View;

    instance-of v0, v0, LX/Cre;

    if-eqz v0, :cond_3

    .line 1928631
    iget-object v0, p0, LX/Chs;->c:Landroid/view/View;

    check-cast v0, LX/Cre;

    sget-object v1, LX/Crd;->UNFOCUSED:LX/Crd;

    invoke-interface {v0, v1}, LX/Cre;->a(LX/Crd;)Z

    .line 1928632
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, LX/Chs;->c:Landroid/view/View;

    goto :goto_1

    .line 1928633
    :cond_4
    sget-object v2, LX/CiJ;->NATIVE_ADS_VIDEO_SCROLL_FOCUSED_VIEW_TO_RECT:LX/CiJ;

    if-ne v0, v2, :cond_1

    .line 1928634
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v2, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    if-eq v0, v2, :cond_1

    move-object v0, v1

    .line 1928635
    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v2, v2, Landroid/support/v7/widget/RecyclerView;

    if-nez v2, :cond_5

    .line 1928636
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_2

    .line 1928637
    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    iget-object v3, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, LX/Chs;->c:Landroid/view/View;

    if-ne v0, v2, :cond_1

    .line 1928638
    iget-object v0, p1, LX/CiK;->c:Landroid/graphics/Rect;

    move-object v0, v0

    .line 1928639
    iget-object v2, p0, LX/Chs;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    .line 1928640
    iget-object v3, p0, LX/Chs;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/2addr v1, v3

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v1, v0

    .line 1928641
    if-eqz v2, :cond_6

    iget-object v1, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v1

    invoke-virtual {v1}, LX/1OR;->g()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1928642
    iget-object v0, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2, v4}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    goto :goto_1

    .line 1928643
    :cond_6
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v1

    invoke-virtual {v1}, LX/1OR;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1928644
    iget-object v1, p0, LX/Chs;->b:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    if-nez v1, :cond_7

    if-gez v0, :cond_7

    .line 1928645
    iget-object v1, p0, LX/Chs;->b:LX/1P1;

    invoke-virtual {v1, v4}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 1928646
    if-gez v1, :cond_1

    .line 1928647
    iget-object v2, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v2, v4, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    goto/16 :goto_1

    .line 1928648
    :cond_7
    iget-object v1, p0, LX/Chs;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v4, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    goto/16 :goto_1
.end method
