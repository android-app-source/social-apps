.class public LX/Cyn;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cve;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cyf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0zi;

.field private h:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "LX/0zi;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/CyU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CyU",
            "<",
            "LX/Cym;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/Cyc;

.field public n:Ljava/lang/String;

.field public o:LX/Cyd;

.field public p:LX/Cyd;

.field public final q:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "LX/Cym;",
            ">;"
        }
    .end annotation
.end field

.field public r:Z

.field public s:LX/Fdy;

.field public t:Lcom/facebook/search/results/model/SearchResultsMutableContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1953760
    const-class v0, LX/Cyn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Cyn;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1953608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1953609
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953610
    iput-object v0, p0, LX/Cyn;->b:LX/0Ot;

    .line 1953611
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953612
    iput-object v0, p0, LX/Cyn;->c:LX/0Ot;

    .line 1953613
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953614
    iput-object v0, p0, LX/Cyn;->d:LX/0Ot;

    .line 1953615
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953616
    iput-object v0, p0, LX/Cyn;->e:LX/0Ot;

    .line 1953617
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/Cyn;->f:Ljava/util/Map;

    .line 1953618
    sget-object v0, LX/Cyd;->UNKNOWN:LX/Cyd;

    iput-object v0, p0, LX/Cyn;->o:LX/Cyd;

    .line 1953619
    sget-object v0, LX/Cyd;->UNKNOWN:LX/Cyd;

    iput-object v0, p0, LX/Cyn;->p:LX/Cyd;

    .line 1953620
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LX/Cyn;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 1953621
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cyn;->t:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1953622
    return-void
.end method

.method private a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/0Px;LX/Cyl;)V
    .locals 7
    .param p2    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/Cyl;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1953745
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Cyn;->j:Ljava/util/List;

    .line 1953746
    iput-boolean v2, p0, LX/Cyn;->r:Z

    .line 1953747
    invoke-static {p0, p1, p2, p4}, LX/Cyn;->a(LX/Cyn;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/Cyl;)Z

    move-result v1

    .line 1953748
    new-instance v3, LX/CyU;

    new-instance v4, LX/Cyh;

    invoke-direct {v4, p0}, LX/Cyh;-><init>(LX/Cyn;)V

    iget-object v0, p0, LX/Cyn;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyf;

    invoke-virtual {v0}, LX/Cyf;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v3, v4, v0}, LX/CyU;-><init>(LX/CyT;Z)V

    iput-object v3, p0, LX/Cyn;->i:LX/CyU;

    .line 1953749
    new-instance v2, LX/Cyi;

    invoke-direct {v2, p0, p4, p1}, LX/Cyi;-><init>(LX/Cyn;LX/Cyl;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 1953750
    :try_start_0
    const-string v0, "%s:start-batch-request"

    sget-object v1, LX/Cyn;->a:Ljava/lang/String;

    const v3, -0x5db21233

    invoke-static {v0, v1, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1953751
    iget-object v0, p0, LX/Cyn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/2SU;->g:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1953752
    iget-object v0, p0, LX/Cyn;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyf;

    iget-object v4, p0, LX/Cyn;->j:Ljava/util/List;

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/Cyf;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0rl;LX/0Px;Ljava/util/List;LX/0Px;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, LX/Cyn;->h:Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1953753
    :goto_1
    const v0, -0x3ae08bcf

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1953754
    return-void

    :cond_0
    move v0, v2

    .line 1953755
    goto :goto_0

    .line 1953756
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/Cyn;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyf;

    iget-object v4, p0, LX/Cyn;->j:Ljava/util/List;

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    .line 1953757
    invoke-static/range {v0 .. v5}, LX/Cyf;->c(LX/Cyf;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0rl;LX/0Px;Ljava/util/List;LX/0Px;)LX/0zi;

    move-result-object v6

    move-object v0, v6

    .line 1953758
    iput-object v0, p0, LX/Cyn;->g:LX/0zi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1953759
    :catchall_0
    move-exception v0

    const v1, 0x3cccac5d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(LX/Cyn;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/Cyl;)Z
    .locals 4
    .param p1    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "LX/Cyl;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1953739
    :try_start_0
    const-string v0, "%s:start-bem-request"

    sget-object v1, LX/Cyn;->a:Ljava/lang/String;

    const v2, -0x319e4ece

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1953740
    iget-object v0, p0, LX/Cyn;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyf;

    new-instance v1, LX/Cyj;

    invoke-direct {v1, p0, p1, p3}, LX/Cyj;-><init>(LX/Cyn;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Cyl;)V

    const/4 v3, 0x0

    .line 1953741
    iget-object v2, v0, LX/Cyf;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short p0, LX/100;->aK:S

    invoke-interface {v2, p0, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/Cyf;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short p0, LX/100;->bx:S

    invoke-interface {v2, p0, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1953742
    :cond_0
    iget-object v2, v0, LX/Cyf;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CyS;

    new-instance p3, LX/CyV;

    iget-object v3, v0, LX/Cyf;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    iget-object p0, v0, LX/Cyf;->l:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Cvk;

    invoke-direct {p3, p1, v3, p0}, LX/CyV;-><init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;Landroid/content/res/Resources;LX/Cvk;)V

    invoke-virtual {v2, p1, p2, v1, p3}, LX/CyS;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/CyR;LX/CyQ;)Z

    move-result v2

    .line 1953743
    :goto_0
    move v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1953744
    const v1, 0x36c44b77

    invoke-static {v1}, LX/02m;->a(I)V

    return v0

    :catchall_0
    move-exception v0

    const v1, 0x57fa3fa4

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    move v2, v3

    goto :goto_0
.end method

.method public static a$redex0(LX/Cyn;LX/Cym;)V
    .locals 2

    .prologue
    .line 1953731
    iget-object v0, p0, LX/Cyn;->s:LX/Fdy;

    if-nez v0, :cond_0

    .line 1953732
    iget-object v0, p0, LX/Cyn;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 1953733
    :goto_0
    return-void

    .line 1953734
    :cond_0
    invoke-virtual {p1}, LX/Cym;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Cyn;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1953735
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cyn;->t:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1953736
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/Cyn;->s:LX/Fdy;

    invoke-virtual {v0, p1}, LX/Fdy;->a(LX/Cym;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1953737
    :catch_0
    move-exception v0

    .line 1953738
    iget-object v1, p0, LX/Cyn;->s:LX/Fdy;

    invoke-virtual {v1, v0}, LX/Fdy;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/Cyn;LX/Cym;Z)V
    .locals 3

    .prologue
    .line 1953710
    iget-object v0, p0, LX/Cyn;->f:Ljava/util/Map;

    invoke-virtual {p1}, LX/Cym;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953711
    iget-object v0, p1, LX/Cym;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    move-object v0, v0

    .line 1953712
    const/4 v2, 0x0

    .line 1953713
    if-eqz v0, :cond_2

    .line 1953714
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1953715
    if-eqz v1, :cond_2

    .line 1953716
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1953717
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1953718
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1953719
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->j()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v1

    .line 1953720
    :goto_0
    iput-object v1, p0, LX/Cyn;->k:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    .line 1953721
    iget-object v1, p0, LX/Cyn;->k:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    if-eqz v1, :cond_0

    .line 1953722
    iget-object v1, p0, LX/Cyn;->k:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    invoke-virtual {v1}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/Cyn;->n:Ljava/lang/String;

    .line 1953723
    :cond_0
    iget-object v0, p1, LX/Cym;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    move-object v0, v0

    .line 1953724
    if-eqz v0, :cond_1

    .line 1953725
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1953726
    if-eqz v1, :cond_1

    .line 1953727
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1953728
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/Cyn;->l:Ljava/lang/String;

    .line 1953729
    :cond_1
    invoke-static {p0, p1}, LX/Cyn;->a$redex0(LX/Cyn;LX/Cym;)V

    .line 1953730
    return-void

    :cond_2
    move-object v1, v2

    goto :goto_0
.end method

.method public static a$redex0(LX/Cyn;Ljava/lang/Throwable;LX/Fdy;LX/Cyl;)V
    .locals 1

    .prologue
    .line 1953703
    sget-object v0, LX/Cyc;->LOADED_LAST:LX/Cyc;

    iput-object v0, p0, LX/Cyn;->m:LX/Cyc;

    .line 1953704
    invoke-virtual {p3}, LX/Cyl;->b()V

    .line 1953705
    iget-object v0, p0, LX/Cyn;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1953706
    if-eqz p2, :cond_0

    .line 1953707
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cyn;->t:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1953708
    invoke-virtual {p2, p1}, LX/Fdy;->a(Ljava/lang/Throwable;)V

    .line 1953709
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1953702
    const-string v0, "bootstrap_entities"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static k(LX/Cyn;)V
    .locals 1

    .prologue
    .line 1953698
    iget-object v0, p0, LX/Cyn;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1953699
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cyn;->r:Z

    .line 1953700
    :goto_0
    return-void

    .line 1953701
    :cond_0
    invoke-static {p0}, LX/Cyn;->l(LX/Cyn;)V

    goto :goto_0
.end method

.method public static l(LX/Cyn;)V
    .locals 13

    .prologue
    .line 1953658
    sget-object v0, LX/Cyc;->LOADED_LAST:LX/Cyc;

    iput-object v0, p0, LX/Cyn;->m:LX/Cyc;

    .line 1953659
    iget-object v0, p0, LX/Cyn;->k:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    if-eqz v0, :cond_0

    .line 1953660
    iget-object v0, p0, LX/Cyn;->k:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/Cyc;->LOADED_WITH_NEXT:LX/Cyc;

    :goto_0
    iput-object v0, p0, LX/Cyn;->m:LX/Cyc;

    .line 1953661
    iget-object v0, p0, LX/Cyn;->k:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cyn;->n:Ljava/lang/String;

    .line 1953662
    :cond_0
    iget-object v0, p0, LX/Cyn;->s:LX/Fdy;

    if-eqz v0, :cond_2

    .line 1953663
    iget-object v0, p0, LX/Cyn;->s:LX/Fdy;

    iget-object v1, p0, LX/Cyn;->m:LX/Cyc;

    iget-object v2, p0, LX/Cyn;->l:Ljava/lang/String;

    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 1953664
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-boolean v3, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->X:Z

    if-nez v3, :cond_4

    sget-object v3, LX/Cyc;->LOADED_WITH_NEXT:LX/Cyc;

    if-ne v1, v3, :cond_4

    iget-boolean v3, v0, LX/Fdy;->b:Z

    if-eqz v3, :cond_4

    move v3, v11

    .line 1953665
    :goto_1
    if-eqz v3, :cond_5

    .line 1953666
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v10, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->R:LX/EQG;

    .line 1953667
    :cond_1
    :goto_2
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-static {v3, v10, v12}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a$redex0(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/EQG;Z)V

    .line 1953668
    iget-boolean v3, v0, LX/Fdy;->b:Z

    if-eqz v3, :cond_b

    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-boolean v3, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->X:Z

    if-nez v3, :cond_b

    sget-object v3, LX/Cyc;->LOADED_LAST:LX/Cyc;

    if-eq v1, v3, :cond_b

    .line 1953669
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 1953670
    iput-boolean v11, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->X:Z

    .line 1953671
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 1953672
    iget v4, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    add-int/lit8 v5, v4, 0x1

    iput v5, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    .line 1953673
    :goto_3
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-static {v3}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->D(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    .line 1953674
    :cond_2
    return-void

    .line 1953675
    :cond_3
    sget-object v0, LX/Cyc;->LOADED_LAST:LX/Cyc;

    goto :goto_0

    :cond_4
    move v3, v12

    .line 1953676
    goto :goto_1

    .line 1953677
    :cond_5
    sget-object v3, LX/Cyc;->LOADED_LAST:LX/Cyc;

    if-eq v1, v3, :cond_8

    .line 1953678
    iget-boolean v3, v0, LX/Fdy;->b:Z

    if-eqz v3, :cond_7

    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-boolean v3, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->X:Z

    if-eqz v3, :cond_7

    .line 1953679
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v3, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v3}, LX/CzA;->a()Z

    move-result v3

    if-eqz v3, :cond_6

    sget-object v3, LX/EQG;->EMPTY:LX/EQG;

    .line 1953680
    :goto_4
    iget-object v4, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v4, v4, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    sget-object v5, LX/Cyc;->LOADED_LAST:LX/Cyc;

    .line 1953681
    iput-object v5, v4, LX/Cyn;->m:LX/Cyc;

    .line 1953682
    move-object v10, v3

    goto :goto_2

    .line 1953683
    :cond_6
    sget-object v3, LX/EQG;->LOADING_FINISHED:LX/EQG;

    goto :goto_4

    .line 1953684
    :cond_7
    sget-object v10, LX/EQG;->LOADING_MORE:LX/EQG;

    goto :goto_2

    .line 1953685
    :cond_8
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v3, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v3}, LX/CzA;->a()Z

    move-result v3

    if-eqz v3, :cond_9

    sget-object v3, LX/EQG;->EMPTY:LX/EQG;

    move-object v10, v3

    .line 1953686
    :goto_5
    sget-object v3, LX/EQG;->EMPTY:LX/EQG;

    if-ne v10, v3, :cond_1

    .line 1953687
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v3, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->p:LX/FgI;

    iget-object v4, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 1953688
    iget-object v5, v4, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v4, v5

    .line 1953689
    iget-object v5, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v5, v5, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    .line 1953690
    sget-object v6, LX/0Q7;->a:LX/0Px;

    move-object v6, v6

    .line 1953691
    iget-object v7, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-boolean v7, v7, Lcom/facebook/search/results/fragment/SearchResultsFragment;->W:Z

    if-eqz v7, :cond_a

    iget-object v7, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v7, v7, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/FcD;

    invoke-virtual {v7}, LX/FcD;->g()LX/0Px;

    move-result-object v7

    :goto_6
    iget-object v8, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget v8, v8, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    move-object v9, v2

    invoke-virtual/range {v3 .. v9}, LX/FgI;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;LX/0Px;LX/0Px;ILjava/lang/String;)V

    .line 1953692
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v3, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v3}, LX/CzA;->b()V

    goto/16 :goto_2

    .line 1953693
    :cond_9
    sget-object v3, LX/EQG;->LOADING_FINISHED:LX/EQG;

    move-object v10, v3

    goto :goto_5

    .line 1953694
    :cond_a
    const/4 v7, 0x0

    goto :goto_6

    .line 1953695
    :cond_b
    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 1953696
    iput-boolean v12, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->X:Z

    .line 1953697
    goto/16 :goto_3
.end method

.method public static m(LX/Cyn;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1953649
    iget-object v0, p0, LX/Cyn;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v2, LX/2SU;->g:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 1953650
    if-eqz v2, :cond_2

    :try_start_0
    iget-object v0, p0, LX/Cyn;->h:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Cyn;->h:Ljava/util/concurrent/Future;

    const v3, 0x64d11dac

    invoke-static {v0, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zi;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1953651
    :goto_0
    if-eqz v0, :cond_0

    .line 1953652
    invoke-virtual {v0}, LX/0zi;->a()V

    .line 1953653
    if-eqz v2, :cond_3

    .line 1953654
    iput-object v1, p0, LX/Cyn;->h:Ljava/util/concurrent/Future;

    .line 1953655
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 1953656
    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, LX/Cyn;->g:LX/0zi;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    :goto_2
    move-object v0, v1

    goto :goto_0

    .line 1953657
    :cond_3
    iput-object v1, p0, LX/Cyn;->g:LX/0zi;

    goto :goto_1

    :catch_1
    goto :goto_2
.end method


# virtual methods
.method public final a(LX/Cyd;)V
    .locals 3

    .prologue
    .line 1953643
    sget-object v0, LX/Cyk;->a:[I

    invoke-virtual {p1}, LX/Cyd;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1953644
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported request type"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/Cyd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1953645
    :pswitch_0
    iget-object v0, p0, LX/Cyn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "search_results_loader_task"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1953646
    :goto_0
    :pswitch_1
    return-void

    .line 1953647
    :pswitch_2
    iget-object v0, p0, LX/Cyn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "search_results_loader_more_task"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 1953648
    :pswitch_3
    invoke-static {p0}, LX/Cyn;->m(LX/Cyn;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/Fdy;)V
    .locals 0

    .prologue
    .line 1953641
    iput-object p1, p0, LX/Cyn;->s:LX/Fdy;

    .line 1953642
    return-void
.end method

.method public final a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1953624
    iget-object v0, p0, LX/Cyn;->t:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Cyn;->t:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Cyn;->m:LX/Cyc;

    sget-object v1, LX/Cyc;->IN_PROGRESS:LX/Cyc;

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1953625
    if-eqz v0, :cond_0

    .line 1953626
    :goto_1
    return-void

    .line 1953627
    :cond_0
    iput-object p1, p0, LX/Cyn;->t:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1953628
    sget-object v0, LX/Cyc;->IN_PROGRESS:LX/Cyc;

    iput-object v0, p0, LX/Cyn;->m:LX/Cyc;

    .line 1953629
    iget-object v0, p0, LX/Cyn;->o:LX/Cyd;

    invoke-virtual {p0, v0}, LX/Cyn;->a(LX/Cyd;)V

    .line 1953630
    iget-object v0, p0, LX/Cyn;->p:LX/Cyd;

    iput-object v0, p0, LX/Cyn;->o:LX/Cyd;

    .line 1953631
    iget-object v0, p0, LX/Cyn;->o:LX/Cyd;

    .line 1953632
    sget-object v1, LX/Cyd;->UNKNOWN:LX/Cyd;

    if-ne v0, v1, :cond_4

    sget-object v1, LX/Cyd;->STREAMING:LX/Cyd;

    :goto_2
    move-object v0, v1

    .line 1953633
    iput-object v0, p0, LX/Cyn;->p:LX/Cyd;

    .line 1953634
    new-instance v7, LX/Cyl;

    invoke-direct {v7, p0}, LX/Cyl;-><init>(LX/Cyn;)V

    .line 1953635
    invoke-virtual {v7}, LX/Cyl;->a()V

    .line 1953636
    iget-object v0, p0, LX/Cyn;->p:LX/Cyd;

    sget-object v1, LX/Cyd;->STREAMING:LX/Cyd;

    if-ne v0, v1, :cond_1

    .line 1953637
    invoke-direct {p0, p1, p2, p3, v7}, LX/Cyn;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/0Px;LX/Cyl;)V

    goto :goto_1

    .line 1953638
    :cond_1
    iget-object v0, p0, LX/Cyn;->p:LX/Cyd;

    sget-object v1, LX/Cyd;->LOAD:LX/Cyd;

    if-ne v0, v1, :cond_2

    const-string v4, "search_results_loader_task"

    .line 1953639
    :goto_3
    iget-object v0, p0, LX/Cyn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/1Ck;

    iget-object v0, p0, LX/Cyn;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyf;

    iget-object v2, p0, LX/Cyn;->n:Ljava/lang/String;

    iget-object v5, p0, LX/Cyn;->p:LX/Cyd;

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/Cyf;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;LX/0Px;Ljava/lang/String;LX/Cyd;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/Cyg;

    invoke-direct {v1, p0, v7, p1, v4}, LX/Cyg;-><init>(LX/Cyn;LX/Cyl;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;)V

    invoke-virtual {v6, v4, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1

    .line 1953640
    :cond_2
    const-string v4, "search_results_loader_more_task"

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    sget-object v1, LX/Cyd;->LOAD_MORE:LX/Cyd;

    goto :goto_2
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 1953623
    iget-object v0, p0, LX/Cyn;->m:LX/Cyc;

    sget-object v1, LX/Cyc;->IN_PROGRESS:LX/Cyc;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/Cyn;->m:LX/Cyc;

    sget-object v1, LX/Cyc;->LOADED_LAST:LX/Cyc;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1953595
    const/4 v0, 0x0

    .line 1953596
    iput-object v0, p0, LX/Cyn;->t:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1953597
    iput-object v0, p0, LX/Cyn;->i:LX/CyU;

    .line 1953598
    invoke-static {p0}, LX/Cyn;->m(LX/Cyn;)V

    .line 1953599
    iget-object v0, p0, LX/Cyn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "search_results_loader_task"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1953600
    iget-object v0, p0, LX/Cyn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "search_results_loader_more_task"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1953601
    sget-object v0, LX/Cyc;->NOT_SENT:LX/Cyc;

    iput-object v0, p0, LX/Cyn;->m:LX/Cyc;

    .line 1953602
    sget-object v0, LX/Cyd;->UNKNOWN:LX/Cyd;

    iput-object v0, p0, LX/Cyn;->o:LX/Cyd;

    .line 1953603
    sget-object v0, LX/Cyd;->UNKNOWN:LX/Cyd;

    iput-object v0, p0, LX/Cyn;->p:LX/Cyd;

    .line 1953604
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cyn;->n:Ljava/lang/String;

    .line 1953605
    iget-object v0, p0, LX/Cyn;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1953606
    iget-object v0, p0, LX/Cyn;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 1953607
    return-void
.end method
