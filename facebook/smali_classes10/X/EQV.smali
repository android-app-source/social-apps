.class public LX/EQV;
.super Landroid/widget/GridView;
.source ""


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/EQT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2118793
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 2118794
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EQV;->a:Ljava/util/ArrayList;

    .line 2118795
    invoke-direct {p0}, LX/EQV;->a()V

    .line 2118796
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2118789
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2118790
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EQV;->a:Ljava/util/ArrayList;

    .line 2118791
    invoke-direct {p0}, LX/EQV;->a()V

    .line 2118792
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2118785
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2118786
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EQV;->a:Ljava/util/ArrayList;

    .line 2118787
    invoke-direct {p0}, LX/EQV;->a()V

    .line 2118788
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2118783
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/GridView;->setClipChildren(Z)V

    .line 2118784
    return-void
.end method


# virtual methods
.method public getHeaderViewCount()I
    .locals 1

    .prologue
    .line 2118782
    iget-object v0, p0, LX/EQV;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public onMeasure(II)V
    .locals 2

    .prologue
    .line 2118777
    invoke-super {p0, p1, p2}, Landroid/widget/GridView;->onMeasure(II)V

    .line 2118778
    invoke-virtual {p0}, LX/EQV;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 2118779
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/EQU;

    if-eqz v1, :cond_0

    .line 2118780
    check-cast v0, LX/EQU;

    invoke-virtual {p0}, LX/EQV;->getNumColumns()I

    move-result v1

    invoke-virtual {v0, v1}, LX/EQU;->a(I)V

    .line 2118781
    :cond_0
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 2118776
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, LX/EQV;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 3

    .prologue
    .line 2118768
    iget-object v0, p0, LX/EQV;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2118769
    new-instance v0, LX/EQU;

    iget-object v1, p0, LX/EQV;->a:Ljava/util/ArrayList;

    invoke-direct {v0, v1, p1}, LX/EQU;-><init>(Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V

    .line 2118770
    invoke-virtual {p0}, LX/EQV;->getNumColumns()I

    move-result v1

    .line 2118771
    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 2118772
    invoke-virtual {v0, v1}, LX/EQU;->a(I)V

    .line 2118773
    :cond_0
    invoke-super {p0, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2118774
    :goto_0
    return-void

    .line 2118775
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public setClipChildren(Z)V
    .locals 0

    .prologue
    .line 2118767
    return-void
.end method
