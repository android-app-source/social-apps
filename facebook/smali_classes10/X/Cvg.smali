.class public final enum LX/Cvg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cvg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cvg;

.field public static final enum LIVE:LX/Cvg;

.field public static final enum NATIVE:LX/Cvg;

.field public static final enum WEB:LX/Cvg;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1949122
    new-instance v0, LX/Cvg;

    const-string v1, "LIVE"

    const-string v2, "live"

    invoke-direct {v0, v1, v3, v2}, LX/Cvg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cvg;->LIVE:LX/Cvg;

    .line 1949123
    new-instance v0, LX/Cvg;

    const-string v1, "NATIVE"

    const-string v2, "native"

    invoke-direct {v0, v1, v4, v2}, LX/Cvg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cvg;->NATIVE:LX/Cvg;

    .line 1949124
    new-instance v0, LX/Cvg;

    const-string v1, "WEB"

    const-string v2, "web"

    invoke-direct {v0, v1, v5, v2}, LX/Cvg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cvg;->WEB:LX/Cvg;

    .line 1949125
    const/4 v0, 0x3

    new-array v0, v0, [LX/Cvg;

    sget-object v1, LX/Cvg;->LIVE:LX/Cvg;

    aput-object v1, v0, v3

    sget-object v1, LX/Cvg;->NATIVE:LX/Cvg;

    aput-object v1, v0, v4

    sget-object v1, LX/Cvg;->WEB:LX/Cvg;

    aput-object v1, v0, v5

    sput-object v0, LX/Cvg;->$VALUES:[LX/Cvg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1949129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1949130
    iput-object p3, p0, LX/Cvg;->value:Ljava/lang/String;

    .line 1949131
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cvg;
    .locals 1

    .prologue
    .line 1949128
    const-class v0, LX/Cvg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cvg;

    return-object v0
.end method

.method public static values()[LX/Cvg;
    .locals 1

    .prologue
    .line 1949127
    sget-object v0, LX/Cvg;->$VALUES:[LX/Cvg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cvg;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1949126
    iget-object v0, p0, LX/Cvg;->value:Ljava/lang/String;

    return-object v0
.end method
