.class public final LX/EMM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/EMN;


# direct methods
.method public constructor <init>(LX/EMN;LX/CzL;)V
    .locals 0

    .prologue
    .line 2110586
    iput-object p1, p0, LX/EMM;->b:LX/EMN;

    iput-object p2, p0, LX/EMM;->a:LX/CzL;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2110587
    iget-object v0, p0, LX/EMM;->b:LX/EMN;

    iget-object v0, v0, LX/EMN;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    iget-object v1, p0, LX/EMM;->b:LX/EMN;

    iget-object v1, v1, LX/EMN;->a:LX/CzL;

    iget-object v2, p0, LX/EMM;->a:LX/CzL;

    iget-object v3, p0, LX/EMM;->b:LX/EMN;

    iget-object v3, v3, LX/EMN;->b:LX/CxA;

    invoke-static {v0, v1, v2, p1, v3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->a$redex0(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;LX/CzL;LX/CzL;Ljava/lang/Throwable;LX/CxA;)V

    .line 2110588
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2110589
    iget-object v0, p0, LX/EMM;->b:LX/EMN;

    iget-object v0, v0, LX/EMN;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v1, p0, LX/EMM;->b:LX/EMN;

    iget-object v1, v1, LX/EMN;->b:LX/CxA;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    iget-object v2, p0, LX/EMM;->a:LX/CzL;

    invoke-static {v2}, LX/CvY;->a(LX/CzL;)LX/CvJ;

    move-result-object v2

    iget-object v3, p0, LX/EMM;->b:LX/EMN;

    iget-object v3, v3, LX/EMN;->b:LX/CxA;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/EMM;->a:LX/CzL;

    invoke-interface {v3, v4}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, LX/EMM;->a:LX/CzL;

    iget-object v5, p0, LX/EMM;->b:LX/EMN;

    iget-object v5, v5, LX/EMN;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    iget-object v5, v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CvY;

    iget-object v6, p0, LX/EMM;->b:LX/EMN;

    iget-object v6, v6, LX/EMN;->b:LX/CxA;

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    iget-object v7, p0, LX/EMM;->a:LX/CzL;

    invoke-static {v6, v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzL;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CvJ;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2110590
    return-void
.end method
