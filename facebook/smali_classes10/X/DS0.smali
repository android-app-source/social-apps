.class public LX/DS0;
.super LX/B1d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B1d",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/B1b;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/B1b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1999089
    invoke-direct {p0, p1, p2, p7}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;)V

    .line 1999090
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1999091
    iput-object v0, p0, LX/DS0;->i:LX/0Px;

    .line 1999092
    iput-object p3, p0, LX/DS0;->f:Ljava/lang/String;

    .line 1999093
    iput-object p4, p0, LX/DS0;->e:Ljava/lang/String;

    .line 1999094
    iput-object p5, p0, LX/DS0;->g:Ljava/lang/String;

    .line 1999095
    iput p6, p0, LX/DS0;->h:I

    .line 1999096
    return-void
.end method

.method private static a(LX/0Pz;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/DSf;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1999097
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;

    .line 1999098
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1999099
    sget-object v5, LX/DSd;->NONE:LX/DSd;

    .line 1999100
    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1999101
    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x7f489e48

    if-ne v0, v1, :cond_2

    .line 1999102
    sget-object v5, LX/DSd;->EMAIL_INVITE:LX/DSd;

    .line 1999103
    :cond_1
    :goto_1
    new-instance v0, LX/DSf;

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v1

    sget-object v2, LX/DSb;->NOT_ADMIN:LX/DSb;

    sget-object v3, LX/DSc;->NOT_BLOCKED:LX/DSc;

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;

    move-result-object v4

    if-nez v4, :cond_3

    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->l()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    move-result-object v6

    sget-object v7, LX/DSe;->COMMUNITY_MEMBER:LX/DSe;

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->l()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, LX/DSf;-><init>(LX/DUV;LX/DSb;LX/DSc;LX/DS2;LX/DSd;Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;LX/DSe;Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;)V

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1999104
    :cond_2
    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x58017a73

    if-ne v0, v1, :cond_1

    .line 1999105
    sget-object v5, LX/DSd;->ONSITE_INVITE:LX/DSd;

    goto :goto_1

    .line 1999106
    :cond_3
    new-instance v4, LX/DS2;

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->j()J

    move-result-wide v10

    invoke-direct {v4, v6, v7, v10, v11}, LX/DS2;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_2

    .line 1999107
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0gW;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1999108
    new-instance v0, LX/DUr;

    invoke-direct {v0}, LX/DUr;-><init>()V

    move-object v0, v0

    .line 1999109
    const-string v1, "other_end_cursor"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "group_id"

    iget-object v3, p0, LX/DS0;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "from_community"

    iget-object v3, p0, LX/DS0;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "search_term"

    iget-object v3, p0, LX/DS0;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_size"

    iget v3, p0, LX/DS0;->h:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1999110
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1999111
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1999112
    iget-object v0, p0, LX/DS0;->i:LX/0Px;

    invoke-virtual {v5, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1999113
    if-eqz p1, :cond_0

    .line 1999114
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999115
    if-eqz v0, :cond_0

    .line 1999116
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999117
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1999118
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999119
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->a()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p0, LX/DS0;->d:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1999120
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999121
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1999122
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999123
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v5, v0}, LX/DS0;->a(LX/0Pz;Ljava/util/List;)V

    .line 1999124
    :cond_0
    if-eqz p1, :cond_4

    .line 1999125
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999126
    if-eqz v0, :cond_4

    .line 1999127
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999128
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1999129
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999130
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1999131
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999132
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1999133
    :goto_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iput-object v5, p0, LX/DS0;->i:LX/0Px;

    .line 1999134
    if-eqz v0, :cond_1

    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iput-object v3, p0, LX/DS0;->a:Ljava/lang/String;

    .line 1999135
    if-eqz v0, :cond_2

    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, LX/15i;->h(II)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v1, v4

    :cond_3
    iput-boolean v1, p0, LX/DS0;->b:Z

    .line 1999136
    invoke-virtual {p0}, LX/B1d;->g()V

    .line 1999137
    return-void

    :cond_4
    move v0, v1

    move-object v2, v3

    goto :goto_0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1999138
    iget-object v0, p0, LX/DS0;->i:LX/0Px;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1999139
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1999140
    iput-object v0, p0, LX/DS0;->i:LX/0Px;

    .line 1999141
    return-void
.end method
