.class public LX/EQ6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Uh;

.field private final b:LX/EQ7;

.field private final c:LX/Cww;

.field private final d:LX/0SG;

.field private final e:LX/EQ5;

.field private final f:LX/EQ4;

.field private final g:LX/EQ3;


# direct methods
.method public constructor <init>(LX/0Uh;LX/EQ7;LX/Cww;LX/0SG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2118201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2118202
    new-instance v0, LX/EQ5;

    invoke-direct {v0}, LX/EQ5;-><init>()V

    iput-object v0, p0, LX/EQ6;->e:LX/EQ5;

    .line 2118203
    new-instance v0, LX/EQ4;

    invoke-direct {v0, p0}, LX/EQ4;-><init>(LX/EQ6;)V

    iput-object v0, p0, LX/EQ6;->f:LX/EQ4;

    .line 2118204
    new-instance v0, LX/EQ3;

    invoke-direct {v0}, LX/EQ3;-><init>()V

    iput-object v0, p0, LX/EQ6;->g:LX/EQ3;

    .line 2118205
    iput-object p1, p0, LX/EQ6;->a:LX/0Uh;

    .line 2118206
    iput-object p2, p0, LX/EQ6;->b:LX/EQ7;

    .line 2118207
    iput-object p3, p0, LX/EQ6;->c:LX/Cww;

    .line 2118208
    iput-object p4, p0, LX/EQ6;->d:LX/0SG;

    .line 2118209
    return-void
.end method

.method public static b(LX/0QB;)LX/EQ6;
    .locals 7

    .prologue
    .line 2118210
    new-instance v4, LX/EQ6;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    .line 2118211
    new-instance v6, LX/EQ7;

    .line 2118212
    new-instance v1, LX/A0P;

    invoke-direct {v1}, LX/A0P;-><init>()V

    .line 2118213
    move-object v1, v1

    .line 2118214
    move-object v1, v1

    .line 2118215
    check-cast v1, LX/A0P;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {v6, v1, v2, v3, v5}, LX/EQ7;-><init>(LX/A0P;Ljava/util/concurrent/ExecutorService;LX/18V;LX/03V;)V

    .line 2118216
    move-object v1, v6

    .line 2118217
    check-cast v1, LX/EQ7;

    invoke-static {p0}, LX/Cww;->a(LX/0QB;)LX/Cww;

    move-result-object v2

    check-cast v2, LX/Cww;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {v4, v0, v1, v2, v3}, LX/EQ6;-><init>(LX/0Uh;LX/EQ7;LX/Cww;LX/0SG;)V

    .line 2118218
    return-object v4
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;)V
    .locals 9
    .param p1    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2118219
    iget-object v0, p0, LX/EQ6;->g:LX/EQ3;

    invoke-virtual {p2, v0}, Lcom/facebook/search/model/TypeaheadUnit;->a(LX/Cwh;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EQ2;

    .line 2118220
    sget-object v1, LX/EQ2;->DO_NOT_UPDATE:LX/EQ2;

    if-ne v0, v1, :cond_0

    .line 2118221
    :goto_0
    return-void

    .line 2118222
    :cond_0
    invoke-static {p1}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2118223
    sget-object v1, LX/A0S;->VIDEO_SEARCH:LX/A0S;

    move-object v2, v1

    .line 2118224
    :goto_1
    new-instance v4, LX/A0R;

    invoke-direct {v4}, LX/A0R;-><init>()V

    iget-object v1, p0, LX/EQ6;->c:LX/Cww;

    invoke-virtual {p2, v1}, Lcom/facebook/search/model/TypeaheadUnit;->a(LX/Cwh;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2118225
    iput-object v1, v4, LX/A0R;->c:Ljava/lang/String;

    .line 2118226
    move-object v4, v4

    .line 2118227
    iget-object v1, p0, LX/EQ6;->e:LX/EQ5;

    invoke-virtual {p2, v1}, Lcom/facebook/search/model/TypeaheadUnit;->a(LX/Cwh;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2118228
    iput-object v1, v4, LX/A0R;->b:Ljava/lang/String;

    .line 2118229
    move-object v4, v4

    .line 2118230
    iget-object v1, p0, LX/EQ6;->f:LX/EQ4;

    invoke-virtual {p2, v1}, Lcom/facebook/search/model/TypeaheadUnit;->a(LX/Cwh;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2118231
    iput-object v1, v4, LX/A0R;->d:Ljava/lang/String;

    .line 2118232
    move-object v1, v4

    .line 2118233
    iput-object p3, v1, LX/A0R;->a:Ljava/lang/String;

    .line 2118234
    move-object v1, v1

    .line 2118235
    iget-object v4, p0, LX/EQ6;->d:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 2118236
    iput-wide v4, v1, LX/A0R;->e:J

    .line 2118237
    move-object v1, v1

    .line 2118238
    iput-object v2, v1, LX/A0R;->f:LX/A0S;

    .line 2118239
    move-object v1, v1

    .line 2118240
    sget-object v2, LX/EQ2;->USE_SERP_ENDPOINT:LX/EQ2;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    .line 2118241
    :goto_2
    iput-boolean v0, v1, LX/A0R;->g:Z

    .line 2118242
    move-object v0, v1

    .line 2118243
    new-instance v1, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;

    invoke-direct {v1, v0}, Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;-><init>(LX/A0R;)V

    move-object v0, v1

    .line 2118244
    iget-object v1, p0, LX/EQ6;->b:LX/EQ7;

    .line 2118245
    iget-object v2, v1, LX/EQ7;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesActivityLogUpdater$1;

    invoke-direct {v3, v1, v0}, Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesActivityLogUpdater$1;-><init>(LX/EQ7;Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;)V

    const v4, -0x43d0a5be

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2118246
    goto :goto_0

    .line 2118247
    :cond_1
    iget-object v1, p0, LX/EQ6;->a:LX/0Uh;

    sget v2, LX/2SU;->e:I

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2118248
    sget-object v1, LX/A0S;->GRAPH_SEARCH:LX/A0S;

    move-object v2, v1

    goto :goto_1

    .line 2118249
    :cond_2
    sget-object v1, LX/A0S;->SIMPLE_SEARCH:LX/A0S;

    move-object v2, v1

    goto :goto_1

    :cond_3
    move v0, v3

    .line 2118250
    goto :goto_2
.end method
