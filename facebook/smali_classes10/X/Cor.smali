.class public LX/Cor;
.super LX/Coi;
.source ""

# interfaces
.implements LX/CoW;


# instance fields
.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:I

.field private final c:Lcom/facebook/richdocument/view/widget/RichTextView;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1936139
    invoke-direct {p0, p1}, LX/Coi;-><init>(Landroid/view/View;)V

    .line 1936140
    const-class v0, LX/Cor;

    invoke-static {v0, p0}, LX/Cor;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936141
    const v0, 0x7f0d1846

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/Cor;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936142
    iget-object v0, p0, LX/Cor;->a:LX/Ck0;

    iget-object v1, p0, LX/Cor;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v3, 0x7f0d011e

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1936143
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0622

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/Cor;->b:I

    .line 1936144
    return-void
.end method

.method public static a(Landroid/view/View;)LX/CoY;
    .locals 1

    .prologue
    .line 1936138
    new-instance v0, LX/Cor;

    invoke-direct {v0, p0}, LX/Cor;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cor;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object p0

    check-cast p0, LX/Ck0;

    iput-object p0, p1, LX/Cor;->a:LX/Ck0;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1936137
    iget v0, p0, LX/Cor;->b:I

    return v0
.end method
