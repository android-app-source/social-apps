.class public LX/EPK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/CyI;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "LX/CyI;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2116604
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/CyI;->MARKETPLACE:LX/CyI;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/CyI;->MARKETPLACE:LX/CyI;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/CyI;->MARKETPLACE:LX/CyI;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/CyI;->PHOTOS:LX/CyI;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/CyI;->LATEST:LX/CyI;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_MIXED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/CyI;->VIDEOS:LX/CyI;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/EPK;->a:LX/0P1;

    .line 2116605
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const v1, -0x6eeaf2f1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, LX/CyI;->MARKETPLACE:LX/CyI;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0x5852ec13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, LX/CyI;->PHOTOS:LX/CyI;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, -0x30cf2d19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, LX/CyI;->VIDEOS:LX/CyI;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/EPK;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2116592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2116593
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116594
    iput-object v0, p0, LX/EPK;->c:LX/0Ot;

    .line 2116595
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116596
    iput-object v0, p0, LX/EPK;->d:LX/0Ot;

    .line 2116597
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116598
    iput-object v0, p0, LX/EPK;->e:LX/0Ot;

    .line 2116599
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116600
    iput-object v0, p0, LX/EPK;->f:LX/0Ot;

    .line 2116601
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116602
    iput-object v0, p0, LX/EPK;->g:LX/0Ot;

    .line 2116603
    return-void
.end method

.method public static a(LX/CxV;)LX/8ci;
    .locals 2

    .prologue
    .line 2116540
    sget-object v0, LX/8ci;->L:LX/0Rf;

    invoke-interface {p0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    .line 2116541
    iget-object p0, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v1, p0

    .line 2116542
    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/8ci;->m:LX/8ci;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/8ci;->l:LX/8ci;

    goto :goto_0
.end method

.method public static a(LX/0ad;LX/0Uh;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0am;)LX/CyI;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;)",
            "LX/CyI;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2116578
    const/4 v0, 0x0

    .line 2116579
    if-nez p3, :cond_3

    .line 2116580
    :cond_0
    :goto_0
    move-object v0, v0

    .line 2116581
    if-eqz v0, :cond_1

    .line 2116582
    :goto_1
    return-object v0

    .line 2116583
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, p2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    invoke-virtual {p4}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, p0, :cond_4

    .line 2116584
    sget-object v0, LX/CyI;->PLACES:LX/CyI;

    .line 2116585
    :goto_2
    move-object v0, v0

    .line 2116586
    goto :goto_1

    .line 2116587
    :cond_3
    sget-object p0, LX/EPK;->b:LX/0P1;

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2116588
    sget-object v0, LX/EPK;->b:LX/0P1;

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyI;

    goto :goto_0

    .line 2116589
    :cond_4
    sget-object v0, LX/EPK;->a:LX/0P1;

    invoke-virtual {v0, p2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2116590
    sget-object v0, LX/EPK;->a:LX/0P1;

    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyI;

    goto :goto_2

    .line 2116591
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(LX/0ad;LX/0Uh;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;LX/0Px;)Z
    .locals 3
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2116568
    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    :goto_0
    invoke-static {p0, p1, p2, p3, v0}, LX/EPK;->a(LX/0ad;LX/0Uh;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0am;)LX/CyI;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 2116569
    :goto_1
    return v0

    .line 2116570
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0

    .line 2116571
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne p2, v0, :cond_2

    if-eqz p4, :cond_2

    move v0, v1

    .line 2116572
    goto :goto_1

    .line 2116573
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne p2, v0, :cond_3

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p5, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 2116574
    goto :goto_1

    .line 2116575
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->NEWS_LINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p5, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 2116576
    goto :goto_1

    .line 2116577
    :cond_4
    if-eqz p4, :cond_5

    invoke-static {p5}, LX/EPK;->b(LX/0Px;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq p2, v0, :cond_5

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p5, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/EPK;
    .locals 6

    .prologue
    .line 2116564
    new-instance v0, LX/EPK;

    invoke-direct {v0}, LX/EPK;-><init>()V

    .line 2116565
    const/16 v1, 0x1032

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xac0

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x32d4

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x1140

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x455

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 2116566
    iput-object v1, v0, LX/EPK;->c:LX/0Ot;

    iput-object v2, v0, LX/EPK;->d:LX/0Ot;

    iput-object v3, v0, LX/EPK;->e:LX/0Ot;

    iput-object v4, v0, LX/EPK;->f:LX/0Ot;

    iput-object v5, v0, LX/EPK;->g:LX/0Ot;

    .line 2116567
    return-object v0
.end method

.method public static b(LX/0Px;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;)",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2116558
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2116559
    :cond_0
    :goto_0
    return-object v0

    .line 2116560
    :cond_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2116561
    invoke-static {v0}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2116562
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2116563
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CzL;LX/Cxi;)Landroid/view/View$OnClickListener;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/Cxi;",
            ":",
            "LX/CxP;",
            ":",
            "LX/CxV;",
            ">(",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModuleInterfaces$SearchResultsSeeMoreQueryModule;",
            ">;TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2116553
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2116554
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116555
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v5

    .line 2116556
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116557
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/EPK;->a(LX/CzL;LX/Cxi;ZLjava/lang/String;LX/8dH;Lcom/facebook/graphql/enums/GraphQLObjectType;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/CzL;LX/Cxi;Z)Landroid/view/View$OnClickListener;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/Cxi;",
            ":",
            "LX/CxP;",
            ":",
            "LX/CxV;",
            ">(",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModuleInterfaces$SearchResultsSeeMoreQueryModule;",
            ">;TE;Z)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2116548
    const/4 v4, 0x0

    .line 2116549
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116550
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v5

    .line 2116551
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116552
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v6}, LX/EPK;->a(LX/CzL;LX/Cxi;ZLjava/lang/String;LX/8dH;Lcom/facebook/graphql/enums/GraphQLObjectType;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/CzL;LX/Cxi;ZLjava/lang/String;LX/8dH;Lcom/facebook/graphql/enums/GraphQLObjectType;)Landroid/view/View$OnClickListener;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/Cxi;",
            ":",
            "LX/CxP;",
            ":",
            "LX/CxV;",
            ">(",
            "LX/CzL",
            "<*>;TE;Z",
            "Ljava/lang/String;",
            "LX/8dH;",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2116543
    invoke-static/range {p5 .. p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2116544
    invoke-virtual {p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v6

    .line 2116545
    invoke-virtual {p1}, LX/CzL;->g()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 2116546
    iget-object v0, p0, LX/EPK;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    iget-object v1, p0, LX/EPK;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-virtual {p1}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-static {v0, v1, v6, v8, v2}, LX/EPK;->a(LX/0ad;LX/0Uh;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0am;)LX/CyI;

    move-result-object v7

    .line 2116547
    new-instance v0, LX/EPJ;

    move-object v1, p0

    move-object/from16 v2, p5

    move-object v3, p2

    move-object/from16 v4, p6

    move-object v5, p1

    move-object v9, p4

    move v10, p3

    invoke-direct/range {v0 .. v10}, LX/EPJ;-><init>(LX/EPK;LX/8dH;LX/Cxi;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/CzL;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/CyI;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Z)V

    return-object v0
.end method
