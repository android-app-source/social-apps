.class public final LX/Dwt;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;)V
    .locals 0

    .prologue
    .line 2061738
    iput-object p1, p0, LX/Dwt;->a:Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 2061739
    if-nez p2, :cond_0

    .line 2061740
    :goto_0
    return-void

    .line 2061741
    :cond_0
    iget v0, p2, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v0, v0

    .line 2061742
    const v1, 0x7f02034c

    if-ne v0, v1, :cond_1

    .line 2061743
    iget-object v0, p0, LX/Dwt;->a:Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9at;

    sget-object v1, LX/9au;->ALBUMSTAB:LX/9au;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/9at;->a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;

    move-result-object v1

    .line 2061744
    iget-object v0, p0, LX/Dwt;->a:Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Dwt;->a:Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2061745
    :cond_1
    iget v0, p2, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v0, v0

    .line 2061746
    const v1, 0x7f020bf6

    if-ne v0, v1, :cond_2

    .line 2061747
    iget-object v0, p0, LX/Dwt;->a:Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Hz;

    invoke-interface {v0}, LX/8Hz;->a()Landroid/content/Intent;

    move-result-object v1

    .line 2061748
    iget-object v0, p0, LX/Dwt;->a:Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Dwt;->a:Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2061749
    :cond_2
    iget-object v0, p0, LX/Dwt;->a:Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxb;

    iget-object v1, p0, LX/Dwt;->a:Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    invoke-virtual {v0, v1}, LX/Dxb;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method
