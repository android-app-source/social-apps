.class public final LX/Cmf;
.super LX/Cm8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cm8",
        "<",
        "LX/Cm4;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/8Ys;

.field public b:LX/8Yr;

.field public final c:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field public final d:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

.field public final e:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

.field public final f:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

.field public g:Z

.field public h:Z

.field public i:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;


# direct methods
.method private constructor <init>(ILX/8Ys;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;)V
    .locals 0

    .prologue
    .line 1933507
    invoke-direct {p0, p1}, LX/Cm8;-><init>(I)V

    .line 1933508
    iput-object p2, p0, LX/Cmf;->a:LX/8Ys;

    .line 1933509
    iput-object p3, p0, LX/Cmf;->c:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1933510
    iput-object p4, p0, LX/Cmf;->d:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    .line 1933511
    iput-object p5, p0, LX/Cmf;->e:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    .line 1933512
    iput-object p6, p0, LX/Cmf;->f:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    .line 1933513
    return-void
.end method

.method public constructor <init>(LX/8Ys;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;)V
    .locals 7

    .prologue
    .line 1933515
    const/4 v1, 0x5

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/Cmf;-><init>(ILX/8Ys;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;)V

    .line 1933516
    return-void
.end method


# virtual methods
.method public final b()LX/Clr;
    .locals 2

    .prologue
    .line 1933514
    new-instance v0, LX/Cmg;

    invoke-direct {v0, p0}, LX/Cmg;-><init>(LX/Cmf;)V

    return-object v0
.end method
