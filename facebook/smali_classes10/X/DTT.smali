.class public final LX/DTT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/DTZ;


# direct methods
.method public constructor <init>(LX/DTZ;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2000409
    iput-object p1, p0, LX/DTT;->c:LX/DTZ;

    iput-object p2, p0, LX/DTT;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DTT;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    .line 2000396
    new-instance v1, LX/DTS;

    invoke-direct {v1, p0}, LX/DTS;-><init>(LX/DTT;)V

    .line 2000397
    iget-object v0, p0, LX/DTT;->c:LX/DTZ;

    iget-object v2, p0, LX/DTT;->b:Landroid/content/Context;

    const v3, 0x7f082fab

    const v4, 0x7f082fb2

    iget-object v5, p0, LX/DTT;->c:LX/DTZ;

    const v6, 0x7f082fb3

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, LX/DTZ;->a$redex0(LX/DTZ;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v5

    .line 2000398
    new-instance v6, LX/0ju;

    invoke-direct {v6, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2000399
    iget-object v7, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000400
    iget-object v7, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p0, 0x7f082f93

    invoke-virtual {v7, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance p0, LX/DTH;

    invoke-direct {p0, v0}, LX/DTH;-><init>(LX/DTZ;)V

    invoke-virtual {v6, v7, p0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000401
    iget-object v7, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000402
    invoke-virtual {v6, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000403
    invoke-virtual {v6}, LX/0ju;->a()LX/2EJ;

    move-result-object v6

    .line 2000404
    invoke-virtual {v6}, LX/2EJ;->show()V

    .line 2000405
    const v7, 0x7f0d0578

    invoke-virtual {v6, v7}, LX/2EJ;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2000406
    if-eqz v6, :cond_0

    instance-of v7, v6, Landroid/widget/TextView;

    if-eqz v7, :cond_0

    .line 2000407
    check-cast v6, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2000408
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
