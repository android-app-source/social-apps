.class public LX/DKA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1986713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1986714
    return-void
.end method

.method public static a(LX/0QB;)LX/DKA;
    .locals 4

    .prologue
    .line 1986736
    const-class v1, LX/DKA;

    monitor-enter v1

    .line 1986737
    :try_start_0
    sget-object v0, LX/DKA;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1986738
    sput-object v2, LX/DKA;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1986739
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1986740
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1986741
    new-instance p0, LX/DKA;

    invoke-direct {p0}, LX/DKA;-><init>()V

    .line 1986742
    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    .line 1986743
    iput-object v3, p0, LX/DKA;->a:Landroid/content/res/Resources;

    .line 1986744
    move-object v0, p0

    .line 1986745
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1986746
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DKA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1986747
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1986748
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 1986749
    iget-object v0, p0, LX/DKA;->a:Landroid/content/res/Resources;

    .line 1986750
    sget-object p0, LX/99C;->b:LX/99A;

    invoke-static {v0, p1, p0}, LX/99C;->a(Landroid/content/res/Resources;ILX/99A;)V

    .line 1986751
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)I
    .locals 2
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .prologue
    .line 1986715
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "housing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1986716
    const v0, 0x7f0218bc

    invoke-direct {p0, v0}, LX/DKA;->a(I)V

    .line 1986717
    if-eqz p2, :cond_0

    const v0, 0x7f0218bc

    .line 1986718
    :goto_0
    return v0

    .line 1986719
    :cond_0
    const v0, 0x7f0218bb

    goto :goto_0

    .line 1986720
    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "free"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1986721
    const v0, 0x7f0218ba

    invoke-direct {p0, v0}, LX/DKA;->a(I)V

    .line 1986722
    if-eqz p2, :cond_2

    const v0, 0x7f0218ba

    goto :goto_0

    :cond_2
    const v0, 0x7f0218b9

    goto :goto_0

    .line 1986723
    :cond_3
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "jobs"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1986724
    const v0, 0x7f0218be

    invoke-direct {p0, v0}, LX/DKA;->a(I)V

    .line 1986725
    if-eqz p2, :cond_4

    const v0, 0x7f0218be

    goto :goto_0

    :cond_4
    const v0, 0x7f0218bd

    goto :goto_0

    .line 1986726
    :cond_5
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "textbook"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1986727
    const v0, 0x7f0218c0

    invoke-direct {p0, v0}, LX/DKA;->a(I)V

    .line 1986728
    if-eqz p2, :cond_6

    const v0, 0x7f0218c0

    goto :goto_0

    :cond_6
    const v0, 0x7f0218bf

    goto :goto_0

    .line 1986729
    :cond_7
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "events"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1986730
    const v0, 0x7f0218b8

    invoke-direct {p0, v0}, LX/DKA;->a(I)V

    .line 1986731
    if-eqz p2, :cond_8

    const v0, 0x7f0218b8

    goto :goto_0

    :cond_8
    const v0, 0x7f0218b7

    goto :goto_0

    .line 1986732
    :cond_9
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "tips"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1986733
    const v0, 0x7f0218c2

    invoke-direct {p0, v0}, LX/DKA;->a(I)V

    .line 1986734
    if-eqz p2, :cond_a

    const v0, 0x7f0218c2

    goto/16 :goto_0

    :cond_a
    const v0, 0x7f0218c1

    goto/16 :goto_0

    .line 1986735
    :cond_b
    const v0, 0x7f0219a5

    goto/16 :goto_0
.end method
