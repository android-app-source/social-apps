.class public final LX/Ega;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2156939
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 2156940
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2156941
    :goto_0
    return v1

    .line 2156942
    :cond_0
    const-string v6, "visible_count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2156943
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 2156944
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_4

    .line 2156945
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2156946
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2156947
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2156948
    const-string v6, "edges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2156949
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2156950
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_2

    .line 2156951
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_2

    .line 2156952
    invoke-static {p0, p1}, LX/G6d;->b(LX/15w;LX/186;)I

    move-result v5

    .line 2156953
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2156954
    :cond_2
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 2156955
    goto :goto_1

    .line 2156956
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2156957
    :cond_4
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2156958
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2156959
    if-eqz v0, :cond_5

    .line 2156960
    invoke-virtual {p1, v2, v3, v1}, LX/186;->a(III)V

    .line 2156961
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2156962
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2156963
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2156964
    if-eqz v0, :cond_1

    .line 2156965
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2156966
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2156967
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 2156968
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/G6d;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2156969
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2156970
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2156971
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2156972
    if-eqz v0, :cond_2

    .line 2156973
    const-string v1, "visible_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2156974
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2156975
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2156976
    return-void
.end method
