.class public final LX/D4t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BUv;


# instance fields
.field public final synthetic a:LX/9rj;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/D4u;


# direct methods
.method public constructor <init>(LX/D4u;LX/9rj;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1962997
    iput-object p1, p0, LX/D4t;->c:LX/D4u;

    iput-object p2, p0, LX/D4t;->a:LX/9rj;

    iput-object p3, p0, LX/D4t;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 10

    .prologue
    .line 1962998
    iget-object v0, p0, LX/D4t;->c:LX/D4u;

    iget-object v0, v0, LX/D4u;->b:LX/D33;

    iget-object v1, p0, LX/D4t;->a:LX/9rj;

    if-eqz p1, :cond_0

    const-string v2, "PINNED"

    :goto_0
    const-string v3, "TOPICAL_LIVE_VIDEOS"

    iget-object v4, p0, LX/D4t;->b:Ljava/lang/String;

    const-string v5, "VIDEO_CHANNEL_HEADER"

    const/4 v6, 0x0

    .line 1962999
    new-instance v7, LX/A5r;

    invoke-direct {v7}, LX/A5r;-><init>()V

    move-object v8, v7

    .line 1963000
    const-string v7, "input"

    new-instance v9, LX/4KH;

    invoke-direct {v9}, LX/4KH;-><init>()V

    iget-object p0, v0, LX/D33;->c:Ljava/lang/String;

    .line 1963001
    const-string p1, "actor_id"

    invoke-virtual {v9, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1963002
    move-object v9, v9

    .line 1963003
    invoke-interface {v1}, LX/9rj;->b()Ljava/lang/String;

    move-result-object p0

    .line 1963004
    const-string p1, "video_channel_id"

    invoke-virtual {v9, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1963005
    move-object v9, v9

    .line 1963006
    const-string p0, "pin_state"

    invoke-virtual {v9, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1963007
    move-object v9, v9

    .line 1963008
    const-string p0, "channel_type"

    invoke-virtual {v9, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1963009
    move-object v9, v9

    .line 1963010
    const-string p0, "video_home_session_id"

    invoke-virtual {v9, p0, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1963011
    move-object v9, v9

    .line 1963012
    const-string p0, "surface"

    invoke-virtual {v9, p0, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1963013
    move-object v9, v9

    .line 1963014
    invoke-virtual {v8, v7, v9}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1963015
    invoke-static {v1}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->a(LX/9rj;)Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    move-result-object v7

    .line 1963016
    new-instance v9, LX/A63;

    invoke-direct {v9}, LX/A63;-><init>()V

    .line 1963017
    invoke-virtual {v7}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->b()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v9, LX/A63;->a:Ljava/lang/String;

    .line 1963018
    invoke-virtual {v7}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->c()I

    move-result p0

    iput p0, v9, LX/A63;->b:I

    .line 1963019
    invoke-virtual {v7}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->l()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    move-result-object p0

    iput-object p0, v9, LX/A63;->c:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$SquareHeaderImageModel;

    .line 1963020
    invoke-virtual {v7}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->e()Z

    move-result p0

    iput-boolean p0, v9, LX/A63;->d:Z

    .line 1963021
    invoke-virtual {v7}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->gN_()Z

    move-result p0

    iput-boolean p0, v9, LX/A63;->e:Z

    .line 1963022
    invoke-virtual {v7}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->gO_()I

    move-result p0

    iput p0, v9, LX/A63;->f:I

    .line 1963023
    invoke-virtual {v7}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->m()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    move-result-object p0

    iput-object p0, v9, LX/A63;->g:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelSubtitleModel;

    .line 1963024
    invoke-virtual {v7}, Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;->n()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    move-result-object p0

    iput-object p0, v9, LX/A63;->h:Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel$VideoChannelTitleModel;

    .line 1963025
    move-object v9, v9

    .line 1963026
    const-string v7, "PINNED"

    if-ne v2, v7, :cond_1

    const/4 v7, 0x1

    .line 1963027
    :goto_1
    iput-boolean v7, v9, LX/A63;->e:Z

    .line 1963028
    move-object v7, v9

    .line 1963029
    invoke-virtual {v7}, LX/A63;->a()Lcom/facebook/topics/protocol/TopicFavoritesQueryModels$VideoTopicFragmentModel;

    move-result-object v7

    .line 1963030
    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v8

    invoke-virtual {v8, v7}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v7

    .line 1963031
    iget-object v8, v0, LX/D33;->a:LX/1Ck;

    const-string v9, "MUTATE_VIDEO_CHANNEL_PIN_STATE_KEY"

    iget-object p0, v0, LX/D33;->b:LX/0tX;

    invoke-virtual {p0, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-virtual {v8, v9, v7, v6}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1963032
    return-void

    .line 1963033
    :cond_0
    const-string v2, "UNPINNED"

    goto/16 :goto_0

    .line 1963034
    :cond_1
    const/4 v7, 0x0

    goto :goto_1
.end method
