.class public LX/D3c;
.super LX/4ok;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1960150
    invoke-direct {p0, p1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 1960151
    sget-object v0, LX/1C0;->h:LX/0Tn;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/D3c;->setKey(Ljava/lang/String;)V

    .line 1960152
    const-string v0, "Enable Browser Preview on all network conditions. This setting overrides assigned QE parameter"

    invoke-virtual {p0, v0}, LX/D3c;->setSummary(Ljava/lang/CharSequence;)V

    .line 1960153
    const-string v0, "Enable Preview Regardless of Network"

    invoke-virtual {p0, v0}, LX/D3c;->setTitle(Ljava/lang/CharSequence;)V

    .line 1960154
    return-void
.end method
