.class public final LX/DcD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Landroid/os/Parcelable;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

.field public final synthetic c:LX/DcE;


# direct methods
.method public constructor <init>(LX/DcE;ZLcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;)V
    .locals 0

    .prologue
    .line 2018074
    iput-object p1, p0, LX/DcD;->c:LX/DcE;

    iput-boolean p2, p0, LX/DcD;->a:Z

    iput-object p3, p0, LX/DcD;->b:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2018075
    iget-object v0, p0, LX/DcD;->b:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    .line 2018076
    iget-object v1, v0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->c:LX/DbM;

    iget-object v2, v0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->i:Ljava/lang/String;

    .line 2018077
    iget-object v3, v1, LX/DbM;->a:LX/0Zb;

    const-string p0, "page_menu_management"

    const-string p1, "menu_management_load_failed"

    invoke-static {p0, p1, v2}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v3, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2018078
    iget-object v1, v0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->k:LX/4BY;

    invoke-virtual {v1}, LX/4BY;->dismiss()V

    .line 2018079
    iget-object v1, v0, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f080039

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2018080
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2018081
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2018082
    check-cast p1, Ljava/util/List;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2018083
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2018084
    :goto_0
    if-nez v0, :cond_1

    move-object v1, v2

    .line 2018085
    :goto_1
    iget-boolean v0, p0, LX/DcD;->a:Z

    if-eqz v0, :cond_3

    .line 2018086
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_2

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2018087
    :goto_2
    iget-object v2, p0, LX/DcD;->b:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    .line 2018088
    iput-object v0, v2, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->j:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2018089
    invoke-virtual {v2, v1}, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->a(Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V

    .line 2018090
    :goto_3
    return-void

    :cond_0
    move-object v0, v2

    .line 2018091
    goto :goto_0

    .line 2018092
    :cond_1
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 2018093
    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    move-object v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 2018094
    goto :goto_2

    .line 2018095
    :cond_3
    iget-object v0, p0, LX/DcD;->b:Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->a(Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V

    goto :goto_3
.end method
