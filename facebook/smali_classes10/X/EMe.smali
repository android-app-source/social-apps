.class public final LX/EMe;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EMf;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/EMf;


# direct methods
.method public constructor <init>(LX/EMf;)V
    .locals 1

    .prologue
    .line 2111078
    iput-object p1, p0, LX/EMe;->d:LX/EMf;

    .line 2111079
    move-object v0, p1

    .line 2111080
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2111081
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2111082
    const-string v0, "SearchResultsCommerceHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2111083
    if-ne p0, p1, :cond_1

    .line 2111084
    :cond_0
    :goto_0
    return v0

    .line 2111085
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2111086
    goto :goto_0

    .line 2111087
    :cond_3
    check-cast p1, LX/EMe;

    .line 2111088
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2111089
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2111090
    if-eq v2, v3, :cond_0

    .line 2111091
    iget-object v2, p0, LX/EMe;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EMe;->a:Ljava/lang/String;

    iget-object v3, p1, LX/EMe;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2111092
    goto :goto_0

    .line 2111093
    :cond_5
    iget-object v2, p1, LX/EMe;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2111094
    :cond_6
    iget-object v2, p0, LX/EMe;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EMe;->b:Ljava/lang/String;

    iget-object v3, p1, LX/EMe;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2111095
    goto :goto_0

    .line 2111096
    :cond_8
    iget-object v2, p1, LX/EMe;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2111097
    :cond_9
    iget-object v2, p0, LX/EMe;->c:LX/1dc;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/EMe;->c:LX/1dc;

    iget-object v3, p1, LX/EMe;->c:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2111098
    goto :goto_0

    .line 2111099
    :cond_a
    iget-object v2, p1, LX/EMe;->c:LX/1dc;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
