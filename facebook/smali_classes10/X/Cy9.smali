.class public LX/Cy9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cxd;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:LX/Cx5;

.field public final b:LX/1Pq;

.field public final c:LX/CxV;

.field public final d:LX/Cxc;

.field public final e:LX/1Pn;

.field public final f:LX/CvY;

.field public final g:LX/1Ck;

.field public final h:LX/CyD;

.field public final i:LX/2hZ;

.field public final j:LX/2dj;

.field private final k:LX/CYK;

.field private final l:LX/9XE;

.field public final m:LX/2Sc;

.field public final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/D0N;

.field public final p:LX/Cz2;

.field public q:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Cx5;LX/1Pq;LX/CxV;LX/Cxc;LX/1Pn;LX/Cz2;LX/CvY;LX/1Ck;LX/CyD;LX/2hZ;LX/2dj;LX/CYK;LX/9XE;LX/2Sc;LX/0Or;LX/D0N;)V
    .locals 2
    .param p1    # LX/Cx5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/CxV;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Cxc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/1Pn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/Cz2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cx5;",
            "LX/1Pq;",
            "LX/CxV;",
            "LX/Cxc;",
            "LX/1Pn;",
            "LX/Cz2;",
            "LX/CvY;",
            "LX/1Ck;",
            "LX/CyD;",
            "LX/2hZ;",
            "LX/2dj;",
            "LX/CYK;",
            "LX/9XE;",
            "LX/2Sc;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/D0N;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952439
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/Cy9;->q:LX/0Ot;

    .line 1952440
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/Cy9;->r:LX/0Ot;

    .line 1952441
    iput-object p1, p0, LX/Cy9;->a:LX/Cx5;

    .line 1952442
    iput-object p2, p0, LX/Cy9;->b:LX/1Pq;

    .line 1952443
    iput-object p3, p0, LX/Cy9;->c:LX/CxV;

    .line 1952444
    iput-object p4, p0, LX/Cy9;->d:LX/Cxc;

    .line 1952445
    iput-object p5, p0, LX/Cy9;->e:LX/1Pn;

    .line 1952446
    iput-object p7, p0, LX/Cy9;->f:LX/CvY;

    .line 1952447
    iput-object p8, p0, LX/Cy9;->g:LX/1Ck;

    .line 1952448
    iput-object p9, p0, LX/Cy9;->h:LX/CyD;

    .line 1952449
    iput-object p10, p0, LX/Cy9;->i:LX/2hZ;

    .line 1952450
    iput-object p11, p0, LX/Cy9;->j:LX/2dj;

    .line 1952451
    iput-object p12, p0, LX/Cy9;->k:LX/CYK;

    .line 1952452
    iput-object p13, p0, LX/Cy9;->l:LX/9XE;

    .line 1952453
    iput-object p6, p0, LX/Cy9;->p:LX/Cz2;

    .line 1952454
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Cy9;->m:LX/2Sc;

    .line 1952455
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Cy9;->n:LX/0Or;

    .line 1952456
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Cy9;->o:LX/D0N;

    .line 1952457
    return-void
.end method

.method public static a(LX/Cy9;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cy9;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1952420
    iput-object p1, p0, LX/Cy9;->q:LX/0Ot;

    iput-object p2, p0, LX/Cy9;->r:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 13

    .prologue
    .line 1952429
    iget-object v0, p0, LX/Cy9;->o:LX/D0N;

    invoke-virtual {v0, p1}, LX/D0N;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1952430
    sget-object v5, LX/0ax;->ai:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1952431
    iget-object v5, p0, LX/Cy9;->n:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/17W;

    iget-object v7, p0, LX/Cy9;->e:LX/1Pn;

    invoke-interface {v7}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v5, v7, v6}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1952432
    iget-object v5, p0, LX/Cy9;->f:LX/CvY;

    iget-object v6, p0, LX/Cy9;->c:LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    iget-object v7, p0, LX/Cy9;->p:LX/Cz2;

    invoke-interface {v7, p1}, LX/Cz2;->b(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v7

    iget-object v8, p0, LX/Cy9;->p:LX/Cz2;

    invoke-interface {v8, p1}, LX/Cz2;->a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v8

    iget-object v10, p0, LX/Cy9;->c:LX/CxV;

    invoke-interface {v10}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v10

    iget-object v11, p0, LX/Cy9;->d:LX/Cxc;

    invoke-interface {v11, p1}, LX/Cxc;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v11

    iget-object v12, p0, LX/Cy9;->d:LX/Cxc;

    invoke-interface {v12, p1}, LX/Cxc;->e(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v12

    invoke-static {p1, v10, v11, v12}, LX/CvY;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    move-object v9, p1

    invoke-virtual/range {v5 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952433
    :goto_0
    return-void

    .line 1952434
    :cond_0
    invoke-static {p1}, LX/CzN;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1952435
    iget-object v1, p0, LX/Cy9;->a:LX/Cx5;

    invoke-interface {v1, p1, v0}, LX/Cx5;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1952436
    iget-object v1, p0, LX/Cy9;->b:LX/1Pq;

    invoke-interface {v1}, LX/1Pq;->iN_()V

    .line 1952437
    iget-object v1, p0, LX/Cy9;->g:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "apply_mutation_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Cy4;

    invoke-direct {v3, p0, p1}, LX/Cy4;-><init>(LX/Cy9;Lcom/facebook/graphql/model/GraphQLNode;)V

    new-instance v4, LX/Cy5;

    invoke-direct {v4, p0, v0, p1}, LX/Cy5;-><init>(LX/Cy9;Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLNode;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 17

    .prologue
    .line 1952421
    invoke-static/range {p1 .. p1}, LX/9ZY;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/9Ys;

    move-result-object v2

    .line 1952422
    if-eqz v2, :cond_0

    invoke-interface {v2}, LX/9Ys;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1952423
    :cond_0
    :goto_0
    return-void

    .line 1952424
    :cond_1
    invoke-interface {v2}, LX/9Ys;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v9

    .line 1952425
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 1952426
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Cy9;->k:LX/CYK;

    new-instance v3, LX/CY6;

    invoke-direct {v3}, LX/CY6;-><init>()V

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, LX/CY6;->a(J)LX/CY6;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/CY6;->a(Ljava/lang/String;)LX/CY6;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/CY6;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;)LX/CY6;

    move-result-object v3

    invoke-virtual {v3}, LX/CY6;->a()LX/CY7;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/CYK;->c(LX/CY7;)V

    .line 1952427
    move-object/from16 v0, p0

    iget-object v11, v0, LX/Cy9;->f:LX/CvY;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Cy9;->c:LX/CxV;

    invoke-interface {v2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v12

    sget-object v13, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v14, LX/7CM;->OPEN_PAGE_CTA:LX/7CM;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Cy9;->p:LX/Cz2;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, LX/Cz2;->b(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Cy9;->p:LX/Cz2;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, LX/Cz2;->a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Cy9;->c:LX/CxV;

    invoke-interface {v2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v2

    sget-object v3, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v4, LX/7CM;->OPEN_PAGE_CTA:LX/7CM;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/Cy9;->d:LX/Cxc;

    move-object/from16 v0, p1

    invoke-interface {v6, v0}, LX/Cxc;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    move-object v2, v11

    move-object v3, v12

    move-object v4, v13

    move-object v5, v14

    move v6, v15

    move-object/from16 v7, v16

    invoke-virtual/range {v2 .. v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952428
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Cy9;->l:LX/9XE;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v9}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->MOBILE_PAGE_PRESENCE_CALL_TO_ACTION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    invoke-virtual/range {v3 .. v8}, LX/9XE;->a(JLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;)V

    goto/16 :goto_0
.end method
