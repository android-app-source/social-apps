.class public LX/EK7;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EK5;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2106022
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EK7;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106057
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2106058
    iput-object p1, p0, LX/EK7;->b:LX/0Ot;

    .line 2106059
    return-void
.end method

.method public static a(LX/0QB;)LX/EK7;
    .locals 4

    .prologue
    .line 2106046
    const-class v1, LX/EK7;

    monitor-enter v1

    .line 2106047
    :try_start_0
    sget-object v0, LX/EK7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106048
    sput-object v2, LX/EK7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106049
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106050
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106051
    new-instance v3, LX/EK7;

    const/16 p0, 0x33bd

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EK7;-><init>(LX/0Ot;)V

    .line 2106052
    move-object v0, v3

    .line 2106053
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106054
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EK7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106055
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106056
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2106045
    const v0, 0x7b98d2fd

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 17

    .prologue
    .line 2106042
    check-cast p2, LX/EK6;

    .line 2106043
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EK7;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/EK6;->a:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/EK6;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v5, v0, LX/EK6;->c:Ljava/lang/String;

    move-object/from16 v0, p2

    iget v6, v0, LX/EK6;->d:I

    move-object/from16 v0, p2

    iget-object v7, v0, LX/EK6;->e:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget-object v8, v0, LX/EK6;->f:Ljava/lang/CharSequence;

    move-object/from16 v0, p2

    iget v9, v0, LX/EK6;->g:I

    move-object/from16 v0, p2

    iget v10, v0, LX/EK6;->h:I

    move-object/from16 v0, p2

    iget v11, v0, LX/EK6;->i:I

    move-object/from16 v0, p2

    iget-object v12, v0, LX/EK6;->j:Ljava/lang/String;

    move-object/from16 v0, p2

    iget v13, v0, LX/EK6;->k:I

    move-object/from16 v0, p2

    iget v14, v0, LX/EK6;->l:I

    move-object/from16 v0, p2

    iget v15, v0, LX/EK6;->m:I

    move-object/from16 v0, p2

    iget v0, v0, LX/EK6;->n:I

    move/from16 v16, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v16}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/lang/CharSequence;IIILjava/lang/String;IIII)LX/1Dg;

    move-result-object v1

    .line 2106044
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2106031
    invoke-static {}, LX/1dS;->b()V

    .line 2106032
    iget v0, p1, LX/1dQ;->b:I

    .line 2106033
    packed-switch v0, :pswitch_data_0

    .line 2106034
    :goto_0
    return-object v2

    .line 2106035
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2106036
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2106037
    check-cast v1, LX/EK6;

    .line 2106038
    iget-object p1, p0, LX/EK7;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/EK6;->o:Landroid/view/View$OnClickListener;

    .line 2106039
    if-eqz p1, :cond_0

    .line 2106040
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2106041
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7b98d2fd
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/EK5;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2106023
    new-instance v1, LX/EK6;

    invoke-direct {v1, p0}, LX/EK6;-><init>(LX/EK7;)V

    .line 2106024
    sget-object v2, LX/EK7;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EK5;

    .line 2106025
    if-nez v2, :cond_0

    .line 2106026
    new-instance v2, LX/EK5;

    invoke-direct {v2}, LX/EK5;-><init>()V

    .line 2106027
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EK5;->a$redex0(LX/EK5;LX/1De;IILX/EK6;)V

    .line 2106028
    move-object v1, v2

    .line 2106029
    move-object v0, v1

    .line 2106030
    return-object v0
.end method
