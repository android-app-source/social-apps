.class public final LX/D8P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D8S;


# direct methods
.method public constructor <init>(LX/D8S;)V
    .locals 0

    .prologue
    .line 1968555
    iput-object p1, p0, LX/D8P;->a:LX/D8S;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1968556
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v3, 0x0

    .line 1968557
    if-nez p1, :cond_1

    .line 1968558
    :cond_0
    :goto_0
    return-object v3

    .line 1968559
    :cond_1
    iget-object v0, p0, LX/D8P;->a:LX/D8S;

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1968560
    iput-object v1, v0, LX/D8S;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968561
    iget-object v0, p0, LX/D8P;->a:LX/D8S;

    iget-object v1, p0, LX/D8P;->a:LX/D8S;

    iget-object v2, p0, LX/D8P;->a:LX/D8S;

    iget-object v2, v2, LX/D8S;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1, v2}, LX/D8S;->a(LX/D8S;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2pa;

    move-result-object v1

    .line 1968562
    iput-object v1, v0, LX/D8S;->s:LX/2pa;

    .line 1968563
    iget-object v0, p0, LX/D8P;->a:LX/D8S;

    iget-object v0, v0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D8P;->a:LX/D8S;

    iget-object v0, v0, LX/D8S;->s:LX/2pa;

    if-eqz v0, :cond_0

    .line 1968564
    iget-object v0, p0, LX/D8P;->a:LX/D8S;

    iget-object v1, v0, LX/D8S;->k:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    iget-object v0, p0, LX/D8P;->a:LX/D8S;

    iget-object v2, v0, LX/D8S;->s:LX/2pa;

    iget-object v0, p0, LX/D8P;->a:LX/D8S;

    iget-object v0, v0, LX/D8S;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968565
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1968566
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->a(LX/2pa;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    goto :goto_0
.end method
