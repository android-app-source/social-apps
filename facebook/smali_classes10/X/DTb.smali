.class public LX/DTb;
.super LX/2s5;
.source ""


# instance fields
.field private a:Landroid/content/res/Resources;

.field public b:Landroid/os/Bundle;

.field private c:I


# direct methods
.method public constructor <init>(LX/0gc;Landroid/os/Bundle;Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 2000630
    invoke-direct {p0, p1}, LX/2s5;-><init>(LX/0gc;)V

    .line 2000631
    iput-object p2, p0, LX/DTb;->b:Landroid/os/Bundle;

    .line 2000632
    iput-object p3, p0, LX/DTb;->a:Landroid/content/res/Resources;

    .line 2000633
    iget-object v0, p0, LX/DTb;->b:Landroid/os/Bundle;

    const-string v1, "group_admin_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    .line 2000634
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eq v1, v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v1, v0, :cond_1

    .line 2000635
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, LX/DTb;->c:I

    .line 2000636
    :goto_0
    return-void

    .line 2000637
    :cond_1
    const/4 v0, 0x2

    iput v0, p0, LX/DTb;->c:I

    goto :goto_0
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2000625
    packed-switch p1, :pswitch_data_0

    .line 2000626
    const-string v0, ""

    :goto_0
    return-object v0

    .line 2000627
    :pswitch_0
    iget-object v0, p0, LX/DTb;->a:Landroid/content/res/Resources;

    const v1, 0x7f082fbd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2000628
    :pswitch_1
    iget-object v0, p0, LX/DTb;->a:Landroid/content/res/Resources;

    const v1, 0x7f082fbe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2000629
    :pswitch_2
    iget-object v0, p0, LX/DTb;->a:Landroid/content/res/Resources;

    const v1, 0x7f082fbf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2000616
    packed-switch p1, :pswitch_data_0

    .line 2000617
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2000618
    :pswitch_0
    new-instance v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;-><init>()V

    .line 2000619
    iget-object v1, p0, LX/DTb;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0

    .line 2000620
    :pswitch_1
    new-instance v0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/GroupAdminListFragment;-><init>()V

    .line 2000621
    iget-object v1, p0, LX/DTb;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0

    .line 2000622
    :pswitch_2
    new-instance v0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;-><init>()V

    .line 2000623
    iget-object v1, p0, LX/DTb;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2000624
    iget v0, p0, LX/DTb;->c:I

    return v0
.end method
