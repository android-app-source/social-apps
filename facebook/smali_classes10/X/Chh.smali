.class public final LX/Chh;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<TRESU",
        "LT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/11o;

.field public final synthetic b:Landroid/os/Handler;

.field public final synthetic c:LX/CH4;

.field public final synthetic d:LX/CGs;

.field public final synthetic e:LX/Chc;

.field public final synthetic f:LX/Chc;

.field public final synthetic g:Z

.field public final synthetic h:LX/Chi;


# direct methods
.method public constructor <init>(LX/Chi;LX/11o;Landroid/os/Handler;LX/CH4;LX/CGs;LX/Chc;LX/Chc;Z)V
    .locals 0

    .prologue
    .line 1928458
    iput-object p1, p0, LX/Chh;->h:LX/Chi;

    iput-object p2, p0, LX/Chh;->a:LX/11o;

    iput-object p3, p0, LX/Chh;->b:Landroid/os/Handler;

    iput-object p4, p0, LX/Chh;->c:LX/CH4;

    iput-object p5, p0, LX/Chh;->d:LX/CGs;

    iput-object p6, p0, LX/Chh;->e:LX/Chc;

    iput-object p7, p0, LX/Chh;->f:LX/Chc;

    iput-boolean p8, p0, LX/Chh;->g:Z

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1928459
    iget-object v0, p0, LX/Chh;->a:LX/11o;

    if-eqz v0, :cond_0

    .line 1928460
    iget-object v0, p0, LX/Chh;->a:LX/11o;

    const-string v1, "rich_document_fetch"

    const v2, -0x34fcfcb5    # -8586059.0f

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1928461
    :cond_0
    iget-object v0, p0, LX/Chh;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/richdocument/RichDocumentInfo$2$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/richdocument/RichDocumentInfo$2$1;-><init>(LX/Chh;Lcom/facebook/fbservice/service/ServiceException;)V

    const v2, -0x5e450851

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1928462
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRESU",
            "LT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1928463
    iget-object v0, p0, LX/Chh;->a:LX/11o;

    if-eqz v0, :cond_0

    .line 1928464
    iget-object v0, p0, LX/Chh;->a:LX/11o;

    const-string v1, "rich_document_fetch"

    const v2, -0x5d757e8a

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1928465
    :cond_0
    iget-object v0, p0, LX/Chh;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/richdocument/RichDocumentInfo$2$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/richdocument/RichDocumentInfo$2$2;-><init>(LX/Chh;Ljava/lang/Object;)V

    const v2, -0x1b90fd26

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1928466
    return-void
.end method
