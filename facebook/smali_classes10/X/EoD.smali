.class public final LX/EoD;
.super LX/Eo7;
.source ""


# instance fields
.field public final synthetic a:LX/EoF;


# direct methods
.method public constructor <init>(LX/EoF;)V
    .locals 0

    .prologue
    .line 2167983
    iput-object p1, p0, LX/EoD;->a:LX/EoF;

    invoke-direct {p0}, LX/Eo7;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0b7;)Z
    .locals 2

    .prologue
    .line 2167984
    check-cast p1, LX/Eo6;

    .line 2167985
    iget-object v0, p1, LX/Eo6;->c:Ljava/lang/Object;

    move-object v0, v0

    .line 2167986
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EoD;->a:LX/EoF;

    iget-object v0, v0, LX/EoF;->e:Ljava/lang/Object;

    .line 2167987
    iget-object v1, p1, LX/Eo6;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2167988
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EoD;->a:LX/EoF;

    iget-object v0, v0, LX/EoF;->g:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 2167989
    iget-object v1, p1, LX/Eo6;->c:Ljava/lang/Object;

    move-object v1, v1

    .line 2167990
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0b7;)V
    .locals 13

    .prologue
    .line 2167991
    check-cast p1, LX/Eo6;

    .line 2167992
    iget-object v0, p0, LX/EoD;->a:LX/EoF;

    .line 2167993
    iget-object v1, p1, LX/Eo6;->c:Ljava/lang/Object;

    move-object v1, v1

    .line 2167994
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2167995
    iput-object v1, v0, LX/EoF;->g:Ljava/lang/Object;

    .line 2167996
    iget-object v0, p0, LX/EoD;->a:LX/EoF;

    iget-object v1, p0, LX/EoD;->a:LX/EoF;

    iget-object v1, v1, LX/EoF;->b:LX/En9;

    .line 2167997
    iget-object v2, p1, LX/Eo6;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2167998
    iget-object v3, p0, LX/EoD;->a:LX/EoF;

    iget-object v3, v3, LX/EoF;->g:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, LX/En9;->a(Ljava/lang/String;Ljava/lang/Object;)LX/Emg;

    move-result-object v1

    .line 2167999
    iput-object v1, v0, LX/EoF;->f:LX/Emf;

    .line 2168000
    iget-object v0, p0, LX/EoD;->a:LX/EoF;

    const/4 v8, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 2168001
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v4

    .line 2168002
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2168003
    :goto_0
    return-void

    .line 2168004
    :cond_0
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, LX/EoH;

    .line 2168005
    iput-object v0, v12, LX/EoH;->a:LX/EoF;

    .line 2168006
    invoke-virtual {v12}, LX/EoH;->getCardViews()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 2168007
    iget-object v4, v0, LX/EoF;->g:Ljava/lang/Object;

    iget-object v6, v0, LX/EoF;->g:Ljava/lang/Object;

    invoke-static {v0, v6}, LX/EoF;->c(LX/EoF;Ljava/lang/Object;)LX/EoJ;

    move-result-object v6

    invoke-interface {v6}, LX/EoJ;->a()Ljava/lang/Class;

    move-result-object v6

    invoke-static {v0, v6}, LX/EoF;->a(LX/EoF;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v6

    invoke-static {v0, v4, v6, v12}, LX/EoF;->a(LX/EoF;Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 2168008
    iget-object v6, v12, LX/EoH;->b:Lcom/facebook/widget/CustomFrameLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;I)V

    .line 2168009
    invoke-virtual {v12}, LX/EoH;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x10e0001

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    .line 2168010
    new-instance v4, LX/8Hs;

    int-to-long v6, v6

    iget-object v9, v0, LX/EoF;->d:LX/4mV;

    const/4 v11, 0x0

    invoke-direct/range {v4 .. v11}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;FF)V

    .line 2168011
    iget-object v6, v4, LX/8Hs;->e:LX/4mU;

    invoke-virtual {v6, v10}, LX/4mU;->e(F)V

    .line 2168012
    new-instance v6, LX/EoE;

    invoke-direct {v6, v0, v12, v5}, LX/EoE;-><init>(LX/EoF;LX/EoH;Landroid/view/View;)V

    .line 2168013
    iput-object v6, v4, LX/8Hs;->j:LX/EoE;

    .line 2168014
    invoke-virtual {v4}, LX/8Hs;->d()V

    goto :goto_0
.end method
