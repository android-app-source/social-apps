.class public final LX/EJk;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/CxV;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;LX/CzL;LX/CxV;)V
    .locals 1

    .prologue
    .line 2105075
    iput-object p1, p0, LX/EJk;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    iput-object p2, p0, LX/EJk;->a:LX/CzL;

    iput-object p3, p0, LX/EJk;->b:LX/CxV;

    invoke-direct {p0}, LX/2eI;-><init>()V

    .line 2105076
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EJk;->d:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2105067
    move v1, v0

    .line 2105068
    :goto_0
    iget-object v2, p0, LX/EJk;->a:LX/CzL;

    invoke-virtual {v2}, LX/CzL;->k()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2105069
    iget-object v2, p0, LX/EJk;->a:LX/CzL;

    invoke-static {v2, v0}, LX/CzM;->a(LX/CzL;I)LX/CzL;

    move-result-object v2

    .line 2105070
    invoke-static {v2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->a(LX/CzL;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2105071
    add-int/lit8 v1, v1, 0x1

    .line 2105072
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2105073
    :cond_1
    add-int/lit8 v0, v1, 0x1

    .line 2105074
    return v0
.end method

.method public final a(LX/2eI;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2105077
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/EJk;->a:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->k()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2105078
    iget-object v1, p0, LX/EJk;->a:LX/CzL;

    invoke-static {v1, v0}, LX/CzM;->a(LX/CzL;I)LX/CzL;

    move-result-object v1

    .line 2105079
    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->a(LX/CzL;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2105080
    iget-object v2, p0, LX/EJk;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;

    invoke-virtual {p1, v2, v1}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2105081
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2105082
    :cond_1
    iget-object v0, p0, LX/EJk;->a:LX/CzL;

    .line 2105083
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2105084
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2105085
    iget-object v0, p0, LX/EJk;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->f:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;

    iget-object v1, p0, LX/EJk;->a:LX/CzL;

    invoke-virtual {p1, v0, v1}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2105086
    :cond_2
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2105063
    if-ltz p1, :cond_0

    iget-boolean v0, p0, LX/EJk;->d:Z

    if-nez v0, :cond_0

    .line 2105064
    iget-object v0, p0, LX/EJk;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->g:LX/CvY;

    iget-object v1, p0, LX/EJk;->b:LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CvY;->d(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2105065
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EJk;->d:Z

    .line 2105066
    :cond_0
    return-void
.end method
