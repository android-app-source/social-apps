.class public final LX/DLF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/DLJ;


# direct methods
.method public constructor <init>(LX/DLJ;)V
    .locals 0

    .prologue
    .line 1988008
    iput-object p1, p0, LX/DLF;->a:LX/DLJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 1988009
    iget-object v0, p0, LX/DLF;->a:LX/DLJ;

    const/4 v2, 0x0

    .line 1988010
    iget-object v1, v0, LX/DLJ;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v3, 0x4f7

    invoke-virtual {v1, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    const-string v3, "dropbox"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1988011
    if-eqz v0, :cond_2

    .line 1988012
    iget-object v0, p0, LX/DLF;->a:LX/DLJ;

    iget-object v0, v0, LX/DLJ;->w:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    if-eqz v0, :cond_1

    .line 1988013
    iget-object v0, p0, LX/DLF;->a:LX/DLJ;

    iget-object v0, v0, LX/DLJ;->w:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object v1, p0, LX/DLF;->a:LX/DLJ;

    iget-object v1, v1, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/DLF;->a:LX/DLJ;

    iget-object v2, v2, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/DLF;->a:LX/DLJ;

    iget v3, v3, LX/DLJ;->u:I

    .line 1988014
    iget-object v4, v0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->y:LX/0i5;

    sget-object p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->m:[Ljava/lang/String;

    new-instance p1, LX/DLS;

    invoke-direct {p1, v0, v1, v2, v3}, LX/DLS;-><init>(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v4, p0, p1}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 1988015
    :cond_1
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 1988016
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1988017
    iget-object v1, p0, LX/DLF;->a:LX/DLJ;

    iget-object v1, v1, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1988018
    iget-object v1, p0, LX/DLF;->a:LX/DLJ;

    iget-object v1, v1, LX/DLJ;->l:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DLF;->a:LX/DLJ;

    invoke-virtual {v2}, LX/DLJ;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_0
.end method
