.class public LX/Db9;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field public static final c:Ljava/lang/String;


# instance fields
.field public a:LX/Db0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/CompoundButton;

.field public g:Z

.field public h:LX/DZN;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2016784
    const-class v0, LX/Db9;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Db9;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2016796
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2016797
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Db9;->g:Z

    .line 2016798
    const-class v0, LX/Db9;

    invoke-static {v0, p0}, LX/Db9;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2016799
    iget-object v0, p0, LX/Db9;->a:LX/Db0;

    invoke-virtual {v0, p1}, LX/Db0;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03145c

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2016800
    const v0, 0x7f0d2e65

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2016801
    const v2, 0x7f031003

    move v1, v2

    .line 2016802
    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2016803
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2016804
    const v0, 0x7f0d0a78

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Db9;->d:Landroid/widget/TextView;

    .line 2016805
    const v0, 0x7f0d0a79

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Db9;->e:Landroid/widget/TextView;

    .line 2016806
    const v0, 0x7f0d2e66

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, LX/Db9;->f:Landroid/widget/CompoundButton;

    .line 2016807
    iget-object v0, p0, LX/Db9;->f:Landroid/widget/CompoundButton;

    new-instance v1, LX/Db7;

    invoke-direct {v1, p0}, LX/Db7;-><init>(LX/Db9;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2016808
    new-instance v0, LX/Db8;

    invoke-direct {v0, p0}, LX/Db8;-><init>(LX/Db9;)V

    invoke-virtual {p0, v0}, LX/Db9;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2016809
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, LX/Db9;

    invoke-static {v2}, LX/Db0;->a(LX/0QB;)LX/Db0;

    move-result-object v1

    check-cast v1, LX/Db0;

    const/16 p0, 0x259

    invoke-static {v2, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, LX/Db9;->a:LX/Db0;

    iput-object v2, p1, LX/Db9;->b:LX/0Ot;

    return-void
.end method


# virtual methods
.method public setDelegate(LX/DZN;)V
    .locals 2

    .prologue
    .line 2016787
    if-eqz p1, :cond_0

    .line 2016788
    iput-object p1, p0, LX/Db9;->h:LX/DZN;

    .line 2016789
    iget-object v0, p0, LX/Db9;->d:Landroid/widget/TextView;

    iget-object v1, p0, LX/Db9;->h:LX/DZN;

    invoke-interface {v1}, LX/DZN;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2016790
    iget-object v0, p0, LX/Db9;->f:Landroid/widget/CompoundButton;

    iget-object v1, p0, LX/Db9;->h:LX/DZN;

    invoke-interface {v1}, LX/DZN;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2016791
    iget-object v0, p0, LX/Db9;->h:LX/DZN;

    invoke-interface {v0}, LX/DZN;->b()Z

    move-result v0

    .line 2016792
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Db9;->g:Z

    .line 2016793
    iget-object v1, p0, LX/Db9;->f:Landroid/widget/CompoundButton;

    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 2016794
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Db9;->g:Z

    .line 2016795
    :cond_0
    return-void
.end method

.method public setSwitchDesciption(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2016785
    iget-object v0, p0, LX/Db9;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2016786
    return-void
.end method
