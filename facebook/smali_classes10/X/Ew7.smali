.class public final LX/Ew7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ew6;


# instance fields
.field public final synthetic a:LX/EwG;


# direct methods
.method public constructor <init>(LX/EwG;)V
    .locals 0

    .prologue
    .line 2182716
    iput-object p1, p0, LX/Ew7;->a:LX/EwG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 10

    .prologue
    .line 2182717
    iget-object v0, p0, LX/Ew7;->a:LX/EwG;

    const/4 v5, 0x0

    .line 2182718
    move v4, v5

    :goto_0
    iget-object v3, v0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_4

    .line 2182719
    iget-object v3, v0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Ewe;

    .line 2182720
    sget-object v6, LX/Ewp;->PERSON_YOU_MAY_KNOW:LX/Ewp;

    invoke-interface {v3}, LX/Ewe;->lg_()LX/Ewp;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/Ewp;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, LX/Ewp;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/Ewp;

    invoke-interface {v3}, LX/Ewe;->lg_()LX/Ewp;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/Ewp;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    const/4 v6, 0x1

    .line 2182721
    :goto_1
    if-eqz v6, :cond_3

    check-cast v3, LX/Eus;

    invoke-virtual {v3}, LX/Eus;->a()J

    move-result-wide v7

    cmp-long v3, v7, p1

    if-nez v3, :cond_3

    .line 2182722
    :goto_2
    move v0, v4

    .line 2182723
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 2182724
    iget-object v0, p0, LX/Ew7;->a:LX/EwG;

    const v1, -0x2b5ef84e

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2182725
    :cond_1
    return-void

    :cond_2
    move v6, v5

    .line 2182726
    goto :goto_1

    .line 2182727
    :cond_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 2182728
    :cond_4
    const/4 v4, -0x1

    goto :goto_2
.end method
