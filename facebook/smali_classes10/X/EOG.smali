.class public final LX/EOG;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EOH;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleInterfaces$SearchResultsOpinionModule;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/C33;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1Ps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/EOH;


# direct methods
.method public constructor <init>(LX/EOH;)V
    .locals 1

    .prologue
    .line 2114207
    iput-object p1, p0, LX/EOG;->d:LX/EOH;

    .line 2114208
    move-object v0, p1

    .line 2114209
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2114210
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2114211
    const-string v0, "SearchResultsOpinionSearchQueryStorySelectorComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2114212
    if-ne p0, p1, :cond_1

    .line 2114213
    :cond_0
    :goto_0
    return v0

    .line 2114214
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2114215
    goto :goto_0

    .line 2114216
    :cond_3
    check-cast p1, LX/EOG;

    .line 2114217
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2114218
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2114219
    if-eq v2, v3, :cond_0

    .line 2114220
    iget-object v2, p0, LX/EOG;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EOG;->a:LX/CzL;

    iget-object v3, p1, LX/EOG;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2114221
    goto :goto_0

    .line 2114222
    :cond_5
    iget-object v2, p1, LX/EOG;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2114223
    :cond_6
    iget-object v2, p0, LX/EOG;->b:LX/0Px;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EOG;->b:LX/0Px;

    iget-object v3, p1, LX/EOG;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2114224
    goto :goto_0

    .line 2114225
    :cond_8
    iget-object v2, p1, LX/EOG;->b:LX/0Px;

    if-nez v2, :cond_7

    .line 2114226
    :cond_9
    iget-object v2, p0, LX/EOG;->c:LX/1Ps;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/EOG;->c:LX/1Ps;

    iget-object v3, p1, LX/EOG;->c:LX/1Ps;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2114227
    goto :goto_0

    .line 2114228
    :cond_a
    iget-object v2, p1, LX/EOG;->c:LX/1Ps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
