.class public LX/Doy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;

.field private static final f:Ljava/lang/Object;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2PJ;

.field public final d:LX/Dp0;

.field public final e:LX/Doz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042886
    const-class v0, LX/Doy;

    sput-object v0, LX/Doy;->a:Ljava/lang/Class;

    .line 2042887
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Doy;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2PJ;LX/Dp0;LX/Doz;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2PJ;",
            "LX/Dp0;",
            "LX/Doz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042889
    iput-object p1, p0, LX/Doy;->b:LX/0Or;

    .line 2042890
    iput-object p2, p0, LX/Doy;->c:LX/2PJ;

    .line 2042891
    iput-object p3, p0, LX/Doy;->d:LX/Dp0;

    .line 2042892
    iput-object p4, p0, LX/Doy;->e:LX/Doz;

    .line 2042893
    return-void
.end method

.method public static a(LX/0QB;)LX/Doy;
    .locals 10

    .prologue
    .line 2042857
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2042858
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2042859
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2042860
    if-nez v1, :cond_0

    .line 2042861
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2042862
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2042863
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2042864
    sget-object v1, LX/Doy;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2042865
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2042866
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2042867
    :cond_1
    if-nez v1, :cond_4

    .line 2042868
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2042869
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2042870
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2042871
    new-instance v9, LX/Doy;

    const/16 v1, 0x15e8

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2PJ;->a(LX/0QB;)LX/2PJ;

    move-result-object v1

    check-cast v1, LX/2PJ;

    invoke-static {v0}, LX/Dp0;->a(LX/0QB;)LX/Dp0;

    move-result-object v7

    check-cast v7, LX/Dp0;

    invoke-static {v0}, LX/Doz;->a(LX/0QB;)LX/Doz;

    move-result-object v8

    check-cast v8, LX/Doz;

    invoke-direct {v9, p0, v1, v7, v8}, LX/Doy;-><init>(LX/0Or;LX/2PJ;LX/Dp0;LX/Doz;)V

    .line 2042872
    move-object v1, v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2042873
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2042874
    if-nez v1, :cond_2

    .line 2042875
    sget-object v0, LX/Doy;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doy;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2042876
    :goto_1
    if-eqz v0, :cond_3

    .line 2042877
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042878
    :goto_3
    check-cast v0, LX/Doy;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2042879
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2042880
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2042881
    :catchall_1
    move-exception v0

    .line 2042882
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042883
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2042884
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2042885
    :cond_2
    :try_start_8
    sget-object v0, LX/Doy;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doy;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/Doy;LX/Eap;[BI[BI[B[B)V
    .locals 9

    .prologue
    .line 2042853
    new-instance v0, LX/Ebf;

    iget-object v1, p0, LX/Doy;->e:LX/Doz;

    invoke-virtual {v1}, LX/Doz;->b()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p4, v3}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v4

    const/4 v3, 0x0

    invoke-static {p6, v3}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v6

    new-instance v8, LX/Eae;

    const/4 v3, 0x0

    invoke-direct {v8, p2, v3}, LX/Eae;-><init>([BI)V

    move v3, p3

    move v5, p5

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, LX/Ebf;-><init>(IIILX/Eat;ILX/Eat;[BLX/Eae;)V

    .line 2042854
    new-instance v1, LX/Eam;

    iget-object v2, p0, LX/Doy;->e:LX/Doz;

    invoke-direct {v1, v2, p1}, LX/Eam;-><init>(LX/DoS;LX/Eap;)V

    .line 2042855
    invoke-virtual {v1, v0}, LX/Eam;->a(LX/Ebf;)V

    .line 2042856
    return-void
.end method
