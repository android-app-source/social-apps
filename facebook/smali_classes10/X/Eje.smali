.class public final LX/Eje;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "contactUploadSession"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "contactUploadSession"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$FriendableContactsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$InvitableContactsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2161930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2161931
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 2161932
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2161933
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v3, p0, LX/Eje;->a:LX/15i;

    iget v5, p0, LX/Eje;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v1, 0x728b971d

    invoke-static {v3, v5, v1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;

    move-result-object v1

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2161934
    iget-object v3, p0, LX/Eje;->c:LX/0Px;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 2161935
    iget-object v5, p0, LX/Eje;->d:LX/0Px;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2161936
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2161937
    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 2161938
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 2161939
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2161940
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2161941
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2161942
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2161943
    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2161944
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2161945
    new-instance v1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;-><init>(LX/15i;)V

    .line 2161946
    return-object v1

    .line 2161947
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
