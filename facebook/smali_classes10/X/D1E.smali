.class public final LX/D1E;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/D1F;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D1G;

.field public final b:Landroid/view/View$OnClickListener;

.field private final c:Landroid/view/View$OnClickListener;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/D1G;)V
    .locals 1

    .prologue
    .line 1956400
    iput-object p1, p0, LX/D1E;->a:LX/D1G;

    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1956401
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1956402
    iput-object v0, p0, LX/D1E;->d:LX/0Px;

    .line 1956403
    new-instance v0, LX/D1C;

    invoke-direct {v0, p0, p1}, LX/D1C;-><init>(LX/D1E;LX/D1G;)V

    iput-object v0, p0, LX/D1E;->c:Landroid/view/View$OnClickListener;

    .line 1956404
    new-instance v0, LX/D1D;

    invoke-direct {v0, p0, p1}, LX/D1D;-><init>(LX/D1E;LX/D1G;)V

    iput-object v0, p0, LX/D1E;->b:Landroid/view/View$OnClickListener;

    .line 1956405
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1956406
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1956407
    const v1, 0x7f0313ca

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1956408
    new-instance v1, LX/D1F;

    invoke-direct {v1, v0}, LX/D1F;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1956409
    check-cast p1, LX/D1F;

    const/4 v2, 0x0

    .line 1956410
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 1956411
    iget-object v1, p0, LX/D1E;->a:LX/D1G;

    iget v1, v1, LX/D14;->c:I

    iput v1, v0, LX/1a3;->width:I

    .line 1956412
    if-nez p2, :cond_0

    iget-object v1, p0, LX/D1E;->a:LX/D1G;

    iget v1, v1, LX/D14;->b:F

    float-to-int v1, v1

    :goto_0
    iget-object v3, p0, LX/D1E;->a:LX/D1G;

    iget v3, v3, LX/D14;->b:F

    float-to-int v3, v3

    invoke-virtual {v0, v1, v2, v3, v2}, LX/1a3;->setMargins(IIII)V

    .line 1956413
    iget-object v0, p0, LX/D1E;->d:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    .line 1956414
    iget-object v1, p1, LX/D1F;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1956415
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->jo_()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1956416
    const v3, 0x7f0d01d2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTag(ILjava/lang/Object;)V

    .line 1956417
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1956418
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 1956419
    iget-object v3, p0, LX/D1E;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1956420
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 1956421
    :goto_1
    iget-object v3, p1, LX/D1F;->l:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1956422
    const/4 p2, 0x0

    .line 1956423
    iget-object v3, p1, LX/D1F;->l:LX/0Px;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;

    .line 1956424
    if-lt v2, v1, :cond_2

    .line 1956425
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setVisibility(I)V

    .line 1956426
    const v4, 0x7f0d01cf

    invoke-virtual {v3, v4, p2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    .line 1956427
    const v4, 0x7f0d01d1

    invoke-virtual {v3, v4, p2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    .line 1956428
    const v4, 0x7f0d01d0

    invoke-virtual {v3, v4, p2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    .line 1956429
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v1, v2

    .line 1956430
    goto :goto_0

    .line 1956431
    :cond_1
    return-void

    .line 1956432
    :cond_2
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setVisibility(I)V

    .line 1956433
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    .line 1956434
    iget-object v5, p0, LX/D1E;->a:LX/D1G;

    iget-object v5, v5, LX/D1G;->f:LX/BiM;

    invoke-virtual {v5, v3, v4, p2}, LX/BiM;->a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;LX/Bi6;)V

    .line 1956435
    iget-object v5, p0, LX/D1E;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1956436
    const v5, 0x7f0d01cf

    invoke-virtual {v3, v5, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    .line 1956437
    const v4, 0x7f0d01d1

    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->j()Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel$PageModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    .line 1956438
    const v4, 0x7f0d01d0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setTag(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1956439
    iget-object v0, p0, LX/D1E;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
