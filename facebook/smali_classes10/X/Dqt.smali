.class public final LX/Dqt;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/preference/PreferenceGroup;

.field public final synthetic b:Landroid/preference/Preference;

.field public final synthetic c:Ljava/lang/Runnable;

.field public final synthetic d:LX/Dqy;


# direct methods
.method public constructor <init>(LX/Dqy;Landroid/preference/PreferenceGroup;Landroid/preference/Preference;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2048728
    iput-object p1, p0, LX/Dqt;->d:LX/Dqy;

    iput-object p2, p0, LX/Dqt;->a:Landroid/preference/PreferenceGroup;

    iput-object p3, p0, LX/Dqt;->b:Landroid/preference/Preference;

    iput-object p4, p0, LX/Dqt;->c:Ljava/lang/Runnable;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2048729
    iget-object v0, p0, LX/Dqt;->a:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, LX/Dqt;->b:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 2048730
    iget-object v0, p0, LX/Dqt;->a:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, LX/Dqt;->d:LX/Dqy;

    iget-object v1, v1, LX/Dqy;->d:LX/Dqw;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2048731
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2048732
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2048733
    iget-object v0, p0, LX/Dqt;->a:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, LX/Dqt;->b:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 2048734
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2048735
    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;

    .line 2048736
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_1

    .line 2048737
    iget-object v0, p0, LX/Dqt;->a:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, LX/Dqt;->d:LX/Dqy;

    iget-object v1, v1, LX/Dqy;->d:LX/Dqw;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2048738
    :cond_0
    :goto_0
    return-void

    .line 2048739
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$NotifAndroidSettingsModel$NodesModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2048740
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 2048741
    :goto_1
    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2048742
    iget-object v0, p0, LX/Dqt;->a:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, LX/Dqt;->d:LX/Dqy;

    iget-object v1, v1, LX/Dqy;->d:LX/Dqw;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 2048743
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2048744
    move-object v1, v0

    goto :goto_1

    .line 2048745
    :cond_3
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    :goto_2
    if-ge v2, v3, :cond_4

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$NotifAndroidSettingsModel$NodesModel;

    .line 2048746
    iget-object v4, p0, LX/Dqt;->a:Landroid/preference/PreferenceGroup;

    iget-object v5, p0, LX/Dqt;->d:LX/Dqy;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$NotifAndroidSettingsModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$NotifAndroidSettingsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$NotifAndroidSettingsModel$NodesModel;->k()Z

    move-result v0

    .line 2048747
    new-instance v8, LX/4ok;

    iget-object p1, v5, LX/Dqy;->a:Lcom/facebook/base/activity/FbPreferenceActivity;

    invoke-direct {v8, p1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2048748
    invoke-virtual {v8, v6}, LX/4ok;->setKey(Ljava/lang/String;)V

    .line 2048749
    const/4 p1, 0x0

    invoke-virtual {v8, p1}, LX/4ok;->setPersistent(Z)V

    .line 2048750
    invoke-virtual {v8, v7}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2048751
    invoke-virtual {v8, v0}, LX/4ok;->setChecked(Z)V

    .line 2048752
    new-instance p1, LX/Dqu;

    invoke-direct {p1, v5, v6}, LX/Dqu;-><init>(LX/Dqy;Ljava/lang/String;)V

    invoke-virtual {v8, p1}, LX/4ok;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2048753
    move-object v0, v8

    .line 2048754
    invoke-virtual {v4, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 2048755
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2048756
    :cond_4
    iget-object v0, p0, LX/Dqt;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2048757
    iget-object v0, p0, LX/Dqt;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
