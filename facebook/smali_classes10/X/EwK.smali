.class public final LX/EwK;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/Euc;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 0

    .prologue
    .line 2183133
    iput-object p1, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2183134
    iget-object v0, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    const-string v1, "FETCH_PYMK"

    invoke-static {v0, p1, v1}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->a$redex0(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2183135
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2183136
    check-cast p1, LX/Euc;

    const/4 v0, 0x0

    .line 2183137
    iget-object v1, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v1}, LX/62l;->f()V

    .line 2183138
    iget-object v1, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    invoke-virtual {v1}, LX/Eui;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2183139
    iget-object v1, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    invoke-virtual {v1, v0}, LX/EwG;->a(Z)V

    .line 2183140
    iget-object v1, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2183141
    iget-object v1, p1, LX/Euc;->a:LX/0Px;

    move-object v1, v1

    .line 2183142
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2183143
    iget-object v1, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-static {v1}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183144
    :cond_0
    iget-object v1, p1, LX/Euc;->a:LX/0Px;

    move-object v2, v1

    .line 2183145
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2183146
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v4, v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->m()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2183147
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2183148
    iget-object v6, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v6, v6, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2183149
    iget-object v6, p1, LX/Euc;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2183150
    invoke-static {v0, v6}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->b(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;Ljava/lang/String;)LX/Ewo;

    move-result-object v0

    .line 2183151
    iget-object v6, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v6, v6, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v6, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2183152
    iget-object v4, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v4, v4, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2183153
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2183154
    :cond_2
    iget-object v0, p0, LX/EwK;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183155
    return-void
.end method
