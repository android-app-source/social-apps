.class public LX/E1G;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Landroid/widget/TextView;

.field public b:LX/0jo;

.field public c:LX/CeT;

.field public d:LX/0SG;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2069629
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/E1G;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2069630
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2069631
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2069632
    const-class v0, LX/E1G;

    invoke-static {v0, p0}, LX/E1G;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2069633
    const v0, 0x7f030f81

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2069634
    const v0, 0x7f0d256d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/E1G;->a:Landroid/widget/TextView;

    .line 2069635
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/E1G;

    invoke-static {p0}, LX/0oR;->a(LX/0QB;)LX/0oR;

    move-result-object v1

    check-cast v1, LX/0jo;

    invoke-static {p0}, LX/CeT;->a(LX/0QB;)LX/CeT;

    move-result-object v2

    check-cast v2, LX/CeT;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object p0

    check-cast p0, LX/0SG;

    iput-object v1, p1, LX/E1G;->b:LX/0jo;

    iput-object v2, p1, LX/E1G;->c:LX/CeT;

    iput-object p0, p1, LX/E1G;->d:LX/0SG;

    return-void
.end method

.method public static a$redex0(LX/E1G;Ljava/lang/String;LX/0gc;I)V
    .locals 3
    .param p0    # LX/E1G;
        .annotation build Lcom/facebook/graphql/calls/GravityNegativeFeedbackOptions;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2069636
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2069637
    if-eqz p1, :cond_0

    .line 2069638
    const-string v1, "negative_feedback_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2069639
    :cond_0
    iget-object v1, p0, LX/E1G;->b:LX/0jo;

    invoke-interface {v1, p3}, LX/0jo;->a(I)LX/0jq;

    move-result-object v1

    invoke-interface {v1, v0}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2069640
    invoke-virtual {p2}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2069641
    return-void
.end method


# virtual methods
.method public final a(LX/0gc;II)V
    .locals 1

    .prologue
    .line 2069642
    iget-object v0, p0, LX/E1G;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 2069643
    new-instance v0, LX/E1F;

    invoke-direct {v0, p0, p1, p2}, LX/E1F;-><init>(LX/E1G;LX/0gc;I)V

    invoke-virtual {p0, v0}, LX/E1G;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2069644
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0gc;Lcom/facebook/placetips/settings/PlaceTipsLocationData;ILjava/lang/String;I)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GravityNegativeFeedbackOptions;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/placetips/settings/PlaceTipsLocationData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2069645
    iget-object v0, p0, LX/E1G;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p6}, Landroid/widget/TextView;->setText(I)V

    .line 2069646
    new-instance v0, LX/E1E;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    move v6, p4

    invoke-direct/range {v0 .. v6}, LX/E1E;-><init>(LX/E1G;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/placetips/settings/PlaceTipsLocationData;LX/0gc;I)V

    invoke-virtual {p0, v0}, LX/E1G;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2069647
    return-void
.end method
