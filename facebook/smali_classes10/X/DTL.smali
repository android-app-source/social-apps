.class public final LX/DTL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/DTZ;


# direct methods
.method public constructor <init>(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2000319
    iput-object p1, p0, LX/DTL;->d:LX/DTZ;

    iput-object p2, p0, LX/DTL;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DTL;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DTL;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    .line 2000320
    new-instance v1, LX/DTK;

    invoke-direct {v1, p0}, LX/DTK;-><init>(LX/DTL;)V

    .line 2000321
    iget-object v0, p0, LX/DTL;->d:LX/DTZ;

    iget-object v2, p0, LX/DTL;->c:Landroid/content/Context;

    iget-object v3, p0, LX/DTL;->b:Ljava/lang/String;

    const v4, 0x7f082f8a

    const v5, 0x7f082f8b

    iget-object v6, p0, LX/DTL;->d:LX/DTZ;

    iget-object v6, v6, LX/DTZ;->l:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->SECRET:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v6, v7, :cond_0

    const v6, 0x7f082f8d

    .line 2000322
    :goto_0
    new-instance v7, LX/0ju;

    invoke-direct {v7, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2000323
    iget-object v8, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000324
    iget-object v8, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p0, 0x7f082f93

    invoke-virtual {v8, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance p0, LX/DTG;

    invoke-direct {p0, v0}, LX/DTG;-><init>(LX/DTZ;)V

    invoke-virtual {v7, v8, p0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2000325
    iget-object v8, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000326
    iget-object v8, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v3, p0, p1

    invoke-virtual {v8, v6, p0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2000327
    invoke-virtual {v7}, LX/0ju;->a()LX/2EJ;

    move-result-object v7

    invoke-virtual {v7}, LX/2EJ;->show()V

    .line 2000328
    const/4 v0, 0x1

    return v0

    .line 2000329
    :cond_0
    const v6, 0x7f082f8c

    goto :goto_0
.end method
