.class public final LX/DyK;
.super LX/9bf;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V
    .locals 0

    .prologue
    .line 2064139
    iput-object p1, p0, LX/DyK;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-direct {p0}, LX/9bf;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2064140
    check-cast p1, LX/9be;

    .line 2064141
    iget-object v0, p0, LX/DyK;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v1, p1, LX/9be;->a:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2064142
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v3

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->OTHER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-ne v3, p0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->A()Ljava/lang/String;

    move-result-object v3

    const-string p0, "FamilyAlbum"

    if-ne v3, p0, :cond_0

    .line 2064143
    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->x:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8I0;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object p1

    .line 2064144
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2064145
    iget-object v2, v3, LX/8I0;->c:LX/17Y;

    sget-object v1, LX/0ax;->bw:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, p0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2064146
    move-object p0, v2

    .line 2064147
    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->v:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v3, p0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2064148
    :goto_1
    return-void

    .line 2064149
    :cond_0
    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->x:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8I0;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, LX/8I0;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    .line 2064150
    const-string v3, "extra_album_selected"

    invoke-static {p1, v3, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2064151
    const-string v3, "extra_photo_tab_mode_params"

    iget-object p0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->r:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {p1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2064152
    const-string p0, "pick_hc_pic"

    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->y:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Dxa;

    .line 2064153
    iget-boolean v2, v3, LX/Dxa;->a:Z

    move v3, v2

    .line 2064154
    invoke-virtual {p1, p0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2064155
    const-string p0, "pick_pic_lite"

    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->y:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Dxa;

    .line 2064156
    iget-boolean v2, v3, LX/Dxa;->b:Z

    move v3, v2

    .line 2064157
    invoke-virtual {p1, p0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2064158
    const-string v3, "disable_adding_photos_to_albums"

    iget-boolean p0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->k:Z

    invoke-virtual {p1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2064159
    const-string v3, "owner_id"

    iget-object p0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-virtual {p1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2064160
    const-string v3, "is_page"

    iget-boolean p0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->h:Z

    invoke-virtual {p1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2064161
    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->z:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2064162
    iget-boolean p0, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v3, p0

    .line 2064163
    if-eqz v3, :cond_2

    .line 2064164
    const-string v3, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    iget-object p0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->z:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {p1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2064165
    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->i:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 2064166
    const-string v3, "extra_pages_admin_permissions"

    iget-object p0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {p1, v3, p0}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2064167
    :cond_1
    const-string v3, "extra_composer_target_data"

    .line 2064168
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p0, p0

    .line 2064169
    const-string v2, "extra_composer_target_data"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p0

    invoke-virtual {p1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2064170
    :cond_2
    const/4 p0, -0x1

    .line 2064171
    invoke-static {v0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->n(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2064172
    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->y:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Dxa;

    .line 2064173
    iget-boolean v2, v3, LX/Dxa;->a:Z

    move v3, v2

    .line 2064174
    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->y:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Dxa;

    .line 2064175
    iget-boolean v2, v3, LX/Dxa;->b:Z

    move v3, v2

    .line 2064176
    if-eqz v3, :cond_5

    .line 2064177
    :cond_3
    const/16 v3, 0x26b9

    move p0, v3

    .line 2064178
    :cond_4
    :goto_2
    if-lez p0, :cond_7

    .line 2064179
    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->v:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v3, p1, p0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto/16 :goto_1

    .line 2064180
    :cond_5
    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->r:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v3, :cond_6

    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->r:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v3}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->d()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2064181
    const/16 v3, 0x26bb

    move p0, v3

    goto :goto_2

    .line 2064182
    :cond_6
    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->r:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->r:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v3}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->e()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2064183
    const/16 v3, 0x26ba

    move p0, v3

    goto :goto_2

    .line 2064184
    :cond_7
    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->v:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v3, p1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 2064185
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0
.end method
