.class public final LX/ELc;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/ELd;


# direct methods
.method public constructor <init>(LX/ELd;LX/CzL;)V
    .locals 0

    .prologue
    .line 2109340
    iput-object p1, p0, LX/ELc;->b:LX/ELd;

    iput-object p2, p0, LX/ELc;->a:LX/CzL;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2109341
    iget-object v0, p0, LX/ELc;->b:LX/ELd;

    iget-object v1, v0, LX/ELd;->a:LX/CzL;

    iget-object v2, p0, LX/ELc;->a:LX/CzL;

    iget-object v0, p0, LX/ELc;->b:LX/ELd;

    iget-object v0, v0, LX/ELd;->b:LX/1Pr;

    check-cast v0, LX/CxP;

    iget-object v3, p0, LX/ELc;->b:LX/ELd;

    iget-object v3, v3, LX/ELd;->h:LX/0Ot;

    const/4 p0, 0x0

    .line 2109342
    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2Sc;

    sget-object v5, LX/3Ql;->FAILED_MUTATION:LX/3Ql;

    invoke-virtual {v4, v5, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    move-object v4, v0

    .line 2109343
    check-cast v4, LX/CxA;

    invoke-interface {v4, v2}, LX/CxA;->a(LX/CzL;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2109344
    new-instance v5, LX/ELU;

    .line 2109345
    iget-object v4, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2109346
    check-cast v4, LX/8d0;

    invoke-interface {v4}, LX/8d0;->dW_()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4, p0}, LX/ELU;-><init>(Ljava/lang/String;Z)V

    move-object v4, v0

    .line 2109347
    check-cast v4, LX/1Pr;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-interface {v4, v5, p0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    move-object v4, v0

    .line 2109348
    check-cast v4, LX/CxA;

    invoke-interface {v4, v2, v1}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2109349
    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2109350
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2109351
    iget-object v0, p0, LX/ELc;->b:LX/ELd;

    iget-object v0, v0, LX/ELd;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition$1$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition$1$2$1;-><init>(LX/ELc;)V

    const v2, 0x5a3efdc5

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2109352
    iget-object v0, p0, LX/ELc;->b:LX/ELd;

    iget-object v0, v0, LX/ELd;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v1, p0, LX/ELc;->b:LX/ELd;

    iget-object v1, v1, LX/ELd;->b:LX/1Pr;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/CvJ;->INLINE_PAGE_LIKE:LX/CvJ;

    iget-object v3, p0, LX/ELc;->b:LX/ELd;

    iget-object v3, v3, LX/ELd;->b:LX/1Pr;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/ELc;->a:LX/CzL;

    invoke-interface {v3, v4}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, LX/ELc;->a:LX/CzL;

    iget-object v5, p0, LX/ELc;->b:LX/ELd;

    iget-object v5, v5, LX/ELd;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CvY;

    iget-object v6, p0, LX/ELc;->b:LX/ELd;

    iget-object v6, v6, LX/ELd;->b:LX/1Pr;

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    iget-object v7, p0, LX/ELc;->a:LX/CzL;

    .line 2109353
    sget-object v8, LX/CvJ;->INLINE_PAGE_LIKE:LX/CvJ;

    .line 2109354
    iget-object v5, v7, LX/CzL;->a:Ljava/lang/Object;

    move-object v5, v5

    .line 2109355
    check-cast v5, LX/8d0;

    invoke-interface {v5}, LX/8d0;->dW_()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v7}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object p1

    .line 2109356
    iget-object v5, v7, LX/CzL;->d:LX/0am;

    move-object v5, v5

    .line 2109357
    invoke-virtual {v5}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v8, p0, v6, p1, v5}, LX/CvY;->a(LX/CvJ;Ljava/lang/String;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v5, v5

    .line 2109358
    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CvJ;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2109359
    return-void
.end method
