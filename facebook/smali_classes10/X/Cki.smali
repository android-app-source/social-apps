.class public final LX/Cki;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$InstantArticleLinkCoverConfigFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/03V;

.field private final b:Ljava/lang/String;

.field private final c:LX/CkU;


# direct methods
.method public constructor <init>(LX/03V;Ljava/lang/String;LX/CkU;)V
    .locals 0

    .prologue
    .line 1931258
    invoke-direct {p0}, LX/2h1;-><init>()V

    .line 1931259
    iput-object p1, p0, LX/Cki;->a:LX/03V;

    .line 1931260
    iput-object p2, p0, LX/Cki;->b:Ljava/lang/String;

    .line 1931261
    iput-object p3, p0, LX/Cki;->c:LX/CkU;

    .line 1931262
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 6

    .prologue
    .line 1931254
    iget-object v0, p0, LX/Cki;->c:LX/CkU;

    if-eqz v0, :cond_0

    .line 1931255
    iget-object v0, p0, LX/Cki;->a:LX/03V;

    sget-object v1, LX/Ckj;->a:Ljava/lang/String;

    const-string v2, "Error fetching link cover for article %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/Cki;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1931256
    iget-object v0, p0, LX/Cki;->c:LX/CkU;

    invoke-virtual {v0, p1}, LX/CkU;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1931257
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1931250
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1931251
    iget-object v0, p0, LX/Cki;->c:LX/CkU;

    if-eqz v0, :cond_0

    .line 1931252
    iget-object v0, p0, LX/Cki;->c:LX/CkU;

    invoke-virtual {v0, p1}, LX/0Vd;->onSuccess(Ljava/lang/Object;)V

    .line 1931253
    :cond_0
    return-void
.end method
