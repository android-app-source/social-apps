.class public final LX/EJu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

.field public final synthetic b:LX/1Ps;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)V
    .locals 0

    .prologue
    .line 2105615
    iput-object p1, p0, LX/EJu;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    iput-object p2, p0, LX/EJu;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    iput-object p3, p0, LX/EJu;->b:LX/1Ps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x1e22dcba

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2105616
    iget-object v0, p0, LX/EJu;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105617
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v0, v2

    .line 2105618
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v2, :cond_1

    .line 2105619
    iget-object v0, p0, LX/EJu;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->d:LX/7j6;

    iget-object v2, p0, LX/EJu;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->jE_()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/7iP;->GLOBAL_SEARCH:LX/7iP;

    invoke-virtual {v0, v2, v3}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 2105620
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EJu;->b:LX/1Ps;

    check-cast v0, LX/Cxh;

    iget-object v2, p0, LX/EJu;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-interface {v0, v2}, LX/Cxh;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)V

    .line 2105621
    const v0, -0x331d69c4

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2105622
    :cond_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v2, :cond_0

    .line 2105623
    iget-object v0, p0, LX/EJu;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105624
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v2

    .line 2105625
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gh()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2105626
    new-instance v2, LX/89k;

    invoke-direct {v2}, LX/89k;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    .line 2105627
    iput-object v3, v2, LX/89k;->b:Ljava/lang/String;

    .line 2105628
    move-object v2, v2

    .line 2105629
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 2105630
    iput-object v0, v2, LX/89k;->c:Ljava/lang/String;

    .line 2105631
    move-object v0, v2

    .line 2105632
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 2105633
    iget-object v2, p0, LX/EJu;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->e:LX/0hy;

    invoke-interface {v2, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v2

    .line 2105634
    if-eqz v2, :cond_0

    .line 2105635
    iget-object v0, p0, LX/EJu;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    iget-object v3, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/EJu;->b:LX/1Ps;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
