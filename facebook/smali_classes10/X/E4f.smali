.class public LX/E4f;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E4f;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E4g;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E4f",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E4g;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077037
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2077038
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/E4f;->b:LX/0Zi;

    .line 2077039
    iput-object p1, p0, LX/E4f;->a:LX/0Ot;

    .line 2077040
    return-void
.end method

.method public static a(LX/0QB;)LX/E4f;
    .locals 4

    .prologue
    .line 2077041
    sget-object v0, LX/E4f;->c:LX/E4f;

    if-nez v0, :cond_1

    .line 2077042
    const-class v1, LX/E4f;

    monitor-enter v1

    .line 2077043
    :try_start_0
    sget-object v0, LX/E4f;->c:LX/E4f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077044
    if-eqz v2, :cond_0

    .line 2077045
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077046
    new-instance v3, LX/E4f;

    const/16 p0, 0x310f

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E4f;-><init>(LX/0Ot;)V

    .line 2077047
    move-object v0, v3

    .line 2077048
    sput-object v0, LX/E4f;->c:LX/E4f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077049
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077050
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077051
    :cond_1
    sget-object v0, LX/E4f;->c:LX/E4f;

    return-object v0

    .line 2077052
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077053
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2077054
    check-cast p2, LX/E4e;

    .line 2077055
    iget-object v0, p0, LX/E4f;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/E4e;->a:Ljava/lang/String;

    const/16 p2, 0x8

    const/4 p0, 0x2

    .line 2077056
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v2

    const v3, 0x7f0a083f

    invoke-virtual {v2, v3}, LX/1nh;->i(I)LX/1nh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v3

    const v4, 0x7f0a009a

    invoke-virtual {v3, v4}, LX/1nh;->i(I)LX/1nh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b1f7c

    invoke-interface {v2, p2, v3}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b1f7b

    invoke-interface {v2, p2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2077057
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2077058
    invoke-static {}, LX/1dS;->b()V

    .line 2077059
    const/4 v0, 0x0

    return-object v0
.end method
