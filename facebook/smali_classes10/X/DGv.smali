.class public final LX/DGv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20Z;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:LX/1Pv;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pv;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1981042
    iput-object p1, p0, LX/DGv;->d:Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    iput-object p2, p0, LX/DGv;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p3, p0, LX/DGv;->b:LX/1Pv;

    iput-object p4, p0, LX/DGv;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/20X;)V
    .locals 11

    .prologue
    .line 1981027
    sget-object v0, LX/DGw;->a:[I

    invoke-virtual {p2}, LX/20X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1981028
    :goto_0
    return-void

    .line 1981029
    :pswitch_0
    iget-object v0, p0, LX/DGv;->d:Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    iget-object v1, p0, LX/DGv;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v2, p0, LX/DGv;->b:LX/1Pv;

    .line 1981030
    check-cast v2, LX/1Po;

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-static {v3}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object v3

    .line 1981031
    const-string v4, "linkSetsShareFooter"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object p1

    invoke-virtual {p1}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object p1

    invoke-static {v3, v4, p1}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    sget-object v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    .line 1981032
    iget-object v4, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->k:LX/1Kf;

    const/4 p1, 0x0

    iget-object p2, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->c:Landroid/content/Context;

    invoke-interface {v4, p1, v3, p2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1981033
    iget-object v0, p0, LX/DGv;->d:Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    iget-object v1, p0, LX/DGv;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->a$redex0(Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    goto :goto_0

    .line 1981034
    :pswitch_1
    iget-object v0, p0, LX/DGv;->d:Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    iget-object v1, p0, LX/DGv;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/DGv;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p0, LX/DGv;->b:LX/1Pv;

    .line 1981035
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1981036
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/0sa;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    .line 1981037
    if-nez v5, :cond_0

    .line 1981038
    const/4 v4, 0x7

    invoke-static {v0, v1, v4}, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->a$redex0(Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    .line 1981039
    :cond_0
    iget-object v4, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->o:LX/5up;

    if-nez v5, :cond_1

    const/4 v7, 0x1

    :goto_1
    const-string v8, "native_article"

    const-string v9, "toggle_button"

    new-instance v10, LX/DGx;

    invoke-direct {v10, v0, v1, v3}, LX/DGx;-><init>(Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pv;)V

    move-object v5, v1

    move-object v6, v2

    invoke-virtual/range {v4 .. v10}, LX/5up;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;ZLjava/lang/String;Ljava/lang/String;LX/2h1;)V

    .line 1981040
    goto :goto_0

    .line 1981041
    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
