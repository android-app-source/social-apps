.class public final LX/DFX;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DFY;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/2ep;

.field public b:Z

.field public c:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

.field public d:LX/1Fa;

.field public e:LX/3mj;

.field public f:LX/1Pc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic g:LX/DFY;


# direct methods
.method public constructor <init>(LX/DFY;)V
    .locals 1

    .prologue
    .line 1978393
    iput-object p1, p0, LX/DFX;->g:LX/DFY;

    .line 1978394
    move-object v0, p1

    .line 1978395
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1978396
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1978397
    const-string v0, "PersonYouMayKnowFriendButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1978398
    if-ne p0, p1, :cond_1

    .line 1978399
    :cond_0
    :goto_0
    return v0

    .line 1978400
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1978401
    goto :goto_0

    .line 1978402
    :cond_3
    check-cast p1, LX/DFX;

    .line 1978403
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1978404
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1978405
    if-eq v2, v3, :cond_0

    .line 1978406
    iget-object v2, p0, LX/DFX;->a:LX/2ep;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DFX;->a:LX/2ep;

    iget-object v3, p1, LX/DFX;->a:LX/2ep;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1978407
    goto :goto_0

    .line 1978408
    :cond_5
    iget-object v2, p1, LX/DFX;->a:LX/2ep;

    if-nez v2, :cond_4

    .line 1978409
    :cond_6
    iget-boolean v2, p0, LX/DFX;->b:Z

    iget-boolean v3, p1, LX/DFX;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1978410
    goto :goto_0

    .line 1978411
    :cond_7
    iget-object v2, p0, LX/DFX;->c:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/DFX;->c:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    iget-object v3, p1, LX/DFX;->c:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1978412
    goto :goto_0

    .line 1978413
    :cond_9
    iget-object v2, p1, LX/DFX;->c:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    if-nez v2, :cond_8

    .line 1978414
    :cond_a
    iget-object v2, p0, LX/DFX;->d:LX/1Fa;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/DFX;->d:LX/1Fa;

    iget-object v3, p1, LX/DFX;->d:LX/1Fa;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1978415
    goto :goto_0

    .line 1978416
    :cond_c
    iget-object v2, p1, LX/DFX;->d:LX/1Fa;

    if-nez v2, :cond_b

    .line 1978417
    :cond_d
    iget-object v2, p0, LX/DFX;->e:LX/3mj;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/DFX;->e:LX/3mj;

    iget-object v3, p1, LX/DFX;->e:LX/3mj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1978418
    goto :goto_0

    .line 1978419
    :cond_f
    iget-object v2, p1, LX/DFX;->e:LX/3mj;

    if-nez v2, :cond_e

    .line 1978420
    :cond_10
    iget-object v2, p0, LX/DFX;->f:LX/1Pc;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/DFX;->f:LX/1Pc;

    iget-object v3, p1, LX/DFX;->f:LX/1Pc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1978421
    goto :goto_0

    .line 1978422
    :cond_11
    iget-object v2, p1, LX/DFX;->f:LX/1Pc;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
