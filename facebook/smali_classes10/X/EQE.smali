.class public final enum LX/EQE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EQE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EQE;

.field public static final enum GLOWING_STORIES:LX/EQE;

.field public static final enum SPINNING_WHEEL:LX/EQE;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2118441
    new-instance v0, LX/EQE;

    const-string v1, "SPINNING_WHEEL"

    invoke-direct {v0, v1, v2}, LX/EQE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQE;->SPINNING_WHEEL:LX/EQE;

    .line 2118442
    new-instance v0, LX/EQE;

    const-string v1, "GLOWING_STORIES"

    invoke-direct {v0, v1, v3}, LX/EQE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQE;->GLOWING_STORIES:LX/EQE;

    .line 2118443
    const/4 v0, 0x2

    new-array v0, v0, [LX/EQE;

    sget-object v1, LX/EQE;->SPINNING_WHEEL:LX/EQE;

    aput-object v1, v0, v2

    sget-object v1, LX/EQE;->GLOWING_STORIES:LX/EQE;

    aput-object v1, v0, v3

    sput-object v0, LX/EQE;->$VALUES:[LX/EQE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2118444
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EQE;
    .locals 1

    .prologue
    .line 2118445
    const-class v0, LX/EQE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EQE;

    return-object v0
.end method

.method public static values()[LX/EQE;
    .locals 1

    .prologue
    .line 2118446
    sget-object v0, LX/EQE;->$VALUES:[LX/EQE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EQE;

    return-object v0
.end method
