.class public LX/E9C;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CeT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2083998
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2083999
    const-class v0, LX/E9C;

    invoke-static {v0, p0}, LX/E9C;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2084000
    const v0, 0x7f031165

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2084001
    const v0, 0x7f0d2902

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/E9C;->c:Landroid/widget/TextView;

    .line 2084002
    const v0, 0x7f0d2903

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/E9C;->d:Landroid/widget/TextView;

    .line 2084003
    const v0, 0x7f0d2904

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/E9C;->e:Landroid/widget/TextView;

    .line 2084004
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/E9C;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-static {p0}, LX/CeT;->a(LX/0QB;)LX/CeT;

    move-result-object p0

    check-cast p0, LX/CeT;

    iput-object v1, p1, LX/E9C;->a:LX/17W;

    iput-object p0, p1, LX/E9C;->b:LX/CeT;

    return-void
.end method
