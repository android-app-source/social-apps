.class public final LX/E9c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/EAa;

.field public final synthetic b:LX/5tj;

.field public final synthetic c:LX/E9e;


# direct methods
.method public constructor <init>(LX/E9e;LX/EAa;LX/5tj;)V
    .locals 0

    .prologue
    .line 2084581
    iput-object p1, p0, LX/E9c;->c:LX/E9e;

    iput-object p2, p0, LX/E9c;->a:LX/EAa;

    iput-object p3, p0, LX/E9c;->b:LX/5tj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x6f24295c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2084582
    iget-object v1, p0, LX/E9c;->a:LX/EAa;

    iget-object v2, p0, LX/E9c;->b:LX/5tj;

    .line 2084583
    invoke-interface {v2}, LX/5tj;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v2}, LX/5tj;->bh_()LX/1VU;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, LX/5tj;->bh_()LX/1VU;

    move-result-object v4

    invoke-interface {v4}, LX/1VU;->l()LX/17A;

    move-result-object v4

    if-nez v4, :cond_1

    .line 2084584
    :cond_0
    iget-object v4, v1, LX/EAa;->b:LX/03V;

    const-class v5, LX/EAa;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Missing information to like review "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, LX/5tj;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2084585
    :goto_0
    const v1, -0x200b8860

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2084586
    :cond_1
    invoke-interface {v2}, LX/5tj;->bh_()LX/1VU;

    move-result-object v4

    invoke-interface {v4}, LX/1VU;->s_()Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    .line 2084587
    :goto_1
    new-instance v5, LX/21A;

    invoke-direct {v5}, LX/21A;-><init>()V

    const-string v6, "pages_public_view"

    .line 2084588
    iput-object v6, v5, LX/21A;->c:Ljava/lang/String;

    .line 2084589
    move-object v5, v5

    .line 2084590
    invoke-virtual {v5}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v5

    .line 2084591
    iget-object v6, v1, LX/EAa;->c:LX/3id;

    invoke-static {v2}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->a(LX/5tj;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v7

    invoke-static {v7}, LX/9JZ;->a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v7

    const/4 p0, 0x0

    new-instance p1, LX/EAZ;

    invoke-direct {p1, v1, v2, v4}, LX/EAZ;-><init>(LX/EAa;LX/5tj;Z)V

    invoke-virtual {v6, v7, v5, p0, p1}, LX/3id;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/1L9;)V

    .line 2084592
    goto :goto_0

    .line 2084593
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method
