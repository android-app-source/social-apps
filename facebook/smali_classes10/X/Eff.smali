.class public final LX/Eff;
.super LX/3wN;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;)V
    .locals 0

    .prologue
    .line 2154910
    iput-object p1, p0, LX/Eff;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;

    invoke-direct {p0}, LX/3wN;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2154911
    iget-object v0, p0, LX/Eff;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;

    iget-object v0, v0, Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final a(II)Z
    .locals 2

    .prologue
    .line 2154912
    iget-object v0, p0, LX/Eff;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;

    iget-object v0, v0, Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AFW;

    .line 2154913
    iget-object v1, v0, LX/AFW;->a:LX/7h0;

    move-object v0, v1

    .line 2154914
    iget-object v1, v0, LX/7h0;->i:Ljava/lang/String;

    move-object v1, v1

    .line 2154915
    iget-object v0, p0, LX/Eff;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;

    iget-object v0, v0, Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AFW;

    .line 2154916
    iget-object p0, v0, LX/AFW;->a:LX/7h0;

    move-object v0, p0

    .line 2154917
    iget-object p0, v0, LX/7h0;->i:Ljava/lang/String;

    move-object v0, p0

    .line 2154918
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2154919
    iget-object v0, p0, LX/Eff;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;

    iget-object v0, v0, Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final b(II)Z
    .locals 3

    .prologue
    .line 2154920
    iget-object v0, p0, LX/Eff;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;

    iget-object v0, v0, Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AFW;

    .line 2154921
    iget-object v1, p0, LX/Eff;->a:Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;

    iget-object v1, v1, Lcom/facebook/audience/direct/ui/SnacksInboxView$DiffCalculatorRunnable;->b:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AFW;

    .line 2154922
    invoke-static {v0, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->c(LX/AFW;LX/AFW;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->d(LX/AFW;LX/AFW;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
