.class public abstract LX/Edp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<TK;",
            "LX/Edo",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2151474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2151475
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/Edp;->a:LX/01J;

    .line 2151476
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 2151457
    if-eqz p1, :cond_0

    .line 2151458
    iget-object v0, p0, LX/Edp;->a:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Edo;

    .line 2151459
    if-eqz v0, :cond_0

    .line 2151460
    iget v1, v0, LX/Edo;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/Edo;->a:I

    .line 2151461
    iget-object v0, v0, LX/Edo;->b:Ljava/lang/Object;

    .line 2151462
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 2151472
    iget-object v0, p0, LX/Edp;->a:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 2151473
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2151465
    iget-object v1, p0, LX/Edp;->a:LX/01J;

    invoke-virtual {v1}, LX/01J;->size()I

    move-result v1

    const/16 v2, 0x1f4

    if-lt v1, v2, :cond_1

    .line 2151466
    :cond_0
    :goto_0
    return v0

    .line 2151467
    :cond_1
    if-eqz p1, :cond_0

    .line 2151468
    new-instance v1, LX/Edo;

    invoke-direct {v1}, LX/Edo;-><init>()V

    .line 2151469
    iput-object p2, v1, LX/Edo;->b:Ljava/lang/Object;

    .line 2151470
    iget-object v0, p0, LX/Edp;->a:LX/01J;

    invoke-virtual {v0, p1, v1}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2151471
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 2151463
    iget-object v0, p0, LX/Edp;->a:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Edo;

    .line 2151464
    if-eqz v0, :cond_0

    iget-object v0, v0, LX/Edo;->b:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
