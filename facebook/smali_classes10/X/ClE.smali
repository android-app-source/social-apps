.class public final LX/ClE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field public final synthetic a:LX/ClG;


# direct methods
.method public constructor <init>(LX/ClG;)V
    .locals 0

    .prologue
    .line 1932190
    iput-object p1, p0, LX/ClE;->a:LX/ClG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final doFrame(J)V
    .locals 10

    .prologue
    .line 1932191
    iget-object v0, p0, LX/ClE;->a:LX/ClG;

    iget-boolean v0, v0, LX/ClG;->c:Z

    if-eqz v0, :cond_0

    .line 1932192
    iget-object v0, p0, LX/ClE;->a:LX/ClG;

    const/4 v1, 0x0

    .line 1932193
    iget-wide v3, v0, LX/ClG;->d:J

    const-wide/16 v5, -0x1

    cmp-long v2, v3, v5

    if-nez v2, :cond_1

    .line 1932194
    iput-wide p1, v0, LX/ClG;->d:J

    .line 1932195
    :goto_0
    iget-object v0, p0, LX/ClE;->a:LX/ClG;

    iget-object v0, v0, LX/ClG;->a:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1932196
    :cond_0
    return-void

    .line 1932197
    :cond_1
    iget-wide v3, v0, LX/ClG;->d:J

    sub-long v3, p1, v3

    .line 1932198
    long-to-float v2, v3

    iget-wide v5, v0, LX/ClG;->g:J

    long-to-float v5, v5

    div-float/2addr v2, v5

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    .line 1932199
    iget-object v5, v0, LX/ClG;->e:LX/ClF;

    sget-object v6, LX/ClF;->ONLY_DROPS:LX/ClF;

    if-ne v5, v6, :cond_3

    .line 1932200
    iget v1, v0, LX/ClG;->h:I

    int-to-float v1, v1

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, LX/ClG;->h:I

    .line 1932201
    :cond_2
    iput-wide p1, v0, LX/ClG;->d:J

    goto :goto_0

    .line 1932202
    :cond_3
    long-to-double v3, v3

    const-wide v5, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    mul-double/2addr v3, v5

    .line 1932203
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "frame interval = %.2f ms "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1932204
    iget v6, v0, LX/ClG;->b:I

    int-to-double v7, v6

    cmpl-double v3, v3, v7

    if-lez v3, :cond_2

    .line 1932205
    :goto_1
    int-to-float v3, v1

    cmpg-float v3, v3, v2

    if-gez v3, :cond_2

    .line 1932206
    const-string v3, "*"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1932207
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
