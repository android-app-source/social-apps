.class public LX/ClH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/Ckw;

.field private final b:LX/0So;


# direct methods
.method public constructor <init>(LX/Ckw;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1932244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1932245
    iput-object p1, p0, LX/ClH;->a:LX/Ckw;

    .line 1932246
    iput-object p2, p0, LX/ClH;->b:LX/0So;

    .line 1932247
    return-void
.end method

.method public static a(LX/0QB;)LX/ClH;
    .locals 5

    .prologue
    .line 1932248
    const-class v1, LX/ClH;

    monitor-enter v1

    .line 1932249
    :try_start_0
    sget-object v0, LX/ClH;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1932250
    sput-object v2, LX/ClH;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1932251
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1932252
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1932253
    new-instance p0, LX/ClH;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v3

    check-cast v3, LX/Ckw;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/ClH;-><init>(LX/Ckw;LX/0So;)V

    .line 1932254
    move-object v0, p0

    .line 1932255
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1932256
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ClH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1932257
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1932258
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZLandroid/webkit/WebView;)V
    .locals 7

    .prologue
    .line 1932259
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1932260
    :goto_0
    return-void

    .line 1932261
    :cond_0
    iget-object v0, p0, LX/ClH;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 1932262
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1932263
    if-eqz p3, :cond_1

    instance-of v0, p3, LX/CsX;

    if-eqz v0, :cond_1

    move-object v0, p3

    .line 1932264
    check-cast v0, LX/CsX;

    invoke-virtual {v0}, LX/CsX;->getWebViewHorizontalScrollRange()I

    move-result v0

    .line 1932265
    check-cast p3, LX/CsX;

    invoke-virtual {p3}, LX/CsX;->getWebViewVerticalScrollRange()I

    move-result v4

    .line 1932266
    if-lez v0, :cond_1

    if-lez v4, :cond_1

    .line 1932267
    const-string v5, "ad_width"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932268
    const-string v5, "ad_height"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932269
    const-string v5, "ad_aspect_ratio"

    int-to-float v0, v0

    int-to-float v4, v4

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932270
    :cond_1
    const-string v0, "block_id"

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932271
    const-string v0, "is_ad_network"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932272
    const-string v0, "finish_loading_raw_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932273
    iget-object v0, p0, LX/ClH;->a:LX/Ckw;

    const-string v2, "android_native_article_webview_ad_impression"

    invoke-virtual {v0, v2, v1}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method
