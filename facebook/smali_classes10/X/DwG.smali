.class public LX/DwG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2060218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 6
    .param p0    # Lcom/facebook/graphql/model/GraphQLAlbum;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const v5, 0x41e065f

    const v4, 0x403827a

    .line 2060219
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2060220
    :cond_0
    :goto_0
    return-object v0

    .line 2060221
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 2060222
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    .line 2060223
    if-eq v2, v5, :cond_2

    if-eq v2, v4, :cond_2

    const v3, 0x25d6af

    if-ne v2, v3, :cond_0

    .line 2060224
    :cond_2
    if-ne v2, v5, :cond_3

    .line 2060225
    sget-object v0, LX/2rw;->GROUP:LX/2rw;

    .line 2060226
    :goto_1
    new-instance v2, LX/89I;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v2, v4, v5, v0}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v0

    .line 2060227
    iput-object v0, v2, LX/89I;->c:Ljava/lang/String;

    .line 2060228
    move-object v0, v2

    .line 2060229
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    goto :goto_0

    .line 2060230
    :cond_3
    if-ne v2, v4, :cond_4

    .line 2060231
    sget-object v0, LX/2rw;->EVENT:LX/2rw;

    goto :goto_1

    .line 2060232
    :cond_4
    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    goto :goto_1
.end method

.method public static a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z
    .locals 2
    .param p0    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2060233
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLAlbum;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLAlbum;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2060234
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z
    .locals 2
    .param p0    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2060235
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->GROUP:LX/2rw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z
    .locals 2
    .param p0    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2060236
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->EVENT:LX/2rw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
