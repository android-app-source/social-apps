.class public final LX/D3Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:LX/D3Z;


# direct methods
.method public constructor <init>(LX/D3Z;)V
    .locals 0

    .prologue
    .line 1959966
    iput-object p1, p0, LX/D3Y;->a:LX/D3Z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1959967
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 1959968
    iget-object v0, p0, LX/D3Y;->a:LX/D3Z;

    iget-object v0, v0, LX/D3Z;->b:Landroid/content/Context;

    const-string v1, "This test should be run on OS level Lollipop or above!"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1959969
    :goto_0
    return v3

    .line 1959970
    :cond_0
    iget-object v0, p0, LX/D3Y;->a:LX/D3Z;

    iget-object v0, v0, LX/D3Z;->b:Landroid/content/Context;

    const-string v1, "Starting test"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1959971
    iget-object v0, p0, LX/D3Y;->a:LX/D3Z;

    iget-object v0, v0, LX/D3Z;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/ui/browser/prefs/BrowserCookieTestPreference$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/ui/browser/prefs/BrowserCookieTestPreference$1$1;-><init>(LX/D3Y;)V

    const v2, 0xf71753e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
