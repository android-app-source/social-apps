.class public final LX/EKz;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/EKz;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EKx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/EL0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2107529
    const/4 v0, 0x0

    sput-object v0, LX/EKz;->a:LX/EKz;

    .line 2107530
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EKz;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2107531
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2107532
    new-instance v0, LX/EL0;

    invoke-direct {v0}, LX/EL0;-><init>()V

    iput-object v0, p0, LX/EKz;->c:LX/EL0;

    .line 2107533
    return-void
.end method

.method public static declared-synchronized q()LX/EKz;
    .locals 2

    .prologue
    .line 2107534
    const-class v1, LX/EKz;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/EKz;->a:LX/EKz;

    if-nez v0, :cond_0

    .line 2107535
    new-instance v0, LX/EKz;

    invoke-direct {v0}, LX/EKz;-><init>()V

    sput-object v0, LX/EKz;->a:LX/EKz;

    .line 2107536
    :cond_0
    sget-object v0, LX/EKz;->a:LX/EKz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2107537
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2107538
    check-cast p2, LX/EKy;

    .line 2107539
    iget-object v0, p2, LX/EKy;->a:Ljava/lang/CharSequence;

    .line 2107540
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const p0, 0x7f0b0050

    invoke-virtual {v2, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const p0, 0x7f0a00a4

    invoke-virtual {v2, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 p0, 0x3

    const p2, 0x7f0b010f

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2107541
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2107542
    invoke-static {}, LX/1dS;->b()V

    .line 2107543
    const/4 v0, 0x0

    return-object v0
.end method
