.class public final enum LX/Ctd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ctd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ctd;

.field public static final enum BOTTOM:LX/Ctd;

.field public static final enum CENTER:LX/Ctd;

.field public static final enum TOP:LX/Ctd;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1945137
    new-instance v0, LX/Ctd;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v2}, LX/Ctd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ctd;->TOP:LX/Ctd;

    .line 1945138
    new-instance v0, LX/Ctd;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v3}, LX/Ctd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ctd;->CENTER:LX/Ctd;

    .line 1945139
    new-instance v0, LX/Ctd;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v4}, LX/Ctd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ctd;->BOTTOM:LX/Ctd;

    .line 1945140
    const/4 v0, 0x3

    new-array v0, v0, [LX/Ctd;

    sget-object v1, LX/Ctd;->TOP:LX/Ctd;

    aput-object v1, v0, v2

    sget-object v1, LX/Ctd;->CENTER:LX/Ctd;

    aput-object v1, v0, v3

    sget-object v1, LX/Ctd;->BOTTOM:LX/Ctd;

    aput-object v1, v0, v4

    sput-object v0, LX/Ctd;->$VALUES:[LX/Ctd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1945134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ctd;
    .locals 1

    .prologue
    .line 1945136
    const-class v0, LX/Ctd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ctd;

    return-object v0
.end method

.method public static values()[LX/Ctd;
    .locals 1

    .prologue
    .line 1945135
    sget-object v0, LX/Ctd;->$VALUES:[LX/Ctd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ctd;

    return-object v0
.end method
