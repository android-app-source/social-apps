.class public LX/En3;
.super LX/En2;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/En2",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static final a:LX/En3;


# instance fields
.field private b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2166597
    new-instance v0, LX/En3;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/En3;-><init>(I)V

    .line 2166598
    sput-object v0, LX/En3;->a:LX/En3;

    invoke-virtual {v0}, LX/En3;->a()V

    .line 2166599
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 2166600
    invoke-direct {p0, p1}, LX/En2;-><init>(I)V

    .line 2166601
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/En3;->b:Z

    .line 2166602
    return-void
.end method

.method private constructor <init>(ILX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2166603
    invoke-direct {p0, p1, p2}, LX/En2;-><init>(ILjava/util/List;)V

    .line 2166604
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/En3;->b:Z

    .line 2166605
    return-void
.end method

.method public static a(ILX/0Px;)LX/En3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "LX/0Px",
            "<TT;>;)",
            "LX/En3",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2166606
    new-instance v0, LX/En3;

    invoke-direct {v0, p0, p1}, LX/En3;-><init>(ILX/0Px;)V

    .line 2166607
    invoke-virtual {v0}, LX/En3;->a()V

    .line 2166608
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2166609
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/En3;->b:Z

    .line 2166610
    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 2166611
    iget-boolean v0, p0, LX/En3;->b:Z

    if-eqz v0, :cond_0

    .line 2166612
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2166613
    :cond_0
    invoke-super {p0, p1, p2}, LX/En2;->a(ILjava/lang/Object;)V

    .line 2166614
    return-void
.end method
