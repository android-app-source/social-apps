.class public final LX/DTc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DLO;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V
    .locals 0

    .prologue
    .line 2000638
    iput-object p1, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2000639
    iget-object v0, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->f:LX/DVC;

    .line 2000640
    iget-object v1, v0, LX/DVC;->a:Landroid/content/res/Resources;

    const p0, 0x7f020970

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v0, v1

    .line 2000641
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2000642
    iget-object v0, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->a:Landroid/content/res/Resources;

    const v1, 0x7f082fc4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2000643
    iget-object v0, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v1, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->g:LX/DVF;

    iget-object v0, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v2, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->q:Ljava/lang/String;

    iget-object v0, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    .line 2000644
    iget-object v4, v3, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v4

    .line 2000645
    const-string v4, "group_url"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    iget-object v3, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, LX/DVF;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 2000646
    iget-object v1, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2000647
    return-void

    .line 2000648
    :cond_0
    iget-object v0, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->m()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2000649
    iget-object v0, p0, LX/DTc;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    .line 2000650
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2000651
    const-string v1, "is_viewer_joined"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
