.class public final LX/Dfx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2028381
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 2028382
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028383
    :goto_0
    return v1

    .line 2028384
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028385
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 2028386
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2028387
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2028388
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2028389
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2028390
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2028391
    :cond_2
    const-string v6, "phones"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2028392
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2028393
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_3

    .line 2028394
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2028395
    const/4 v6, 0x0

    .line 2028396
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_b

    .line 2028397
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028398
    :goto_3
    move v5, v6

    .line 2028399
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2028400
    :cond_3
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2028401
    goto :goto_1

    .line 2028402
    :cond_4
    const-string v6, "represented_profile"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2028403
    invoke-static {p0, p1}, LX/Dfw;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2028404
    :cond_5
    const-string v6, "structured_name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2028405
    invoke-static {p0, p1}, LX/3EF;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2028406
    :cond_6
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2028407
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2028408
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2028409
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2028410
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2028411
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 2028412
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028413
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 2028414
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2028415
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2028416
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 2028417
    const-string v8, "primary_field"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2028418
    const/4 v7, 0x0

    .line 2028419
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_11

    .line 2028420
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028421
    :goto_5
    move v5, v7

    .line 2028422
    goto :goto_4

    .line 2028423
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2028424
    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 2028425
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_b
    move v5, v6

    goto :goto_4

    .line 2028426
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028427
    :cond_d
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_10

    .line 2028428
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2028429
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2028430
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_d

    if-eqz v9, :cond_d

    .line 2028431
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 2028432
    :cond_e
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_6

    .line 2028433
    :cond_f
    const-string v10, "phone"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 2028434
    invoke-static {p0, p1}, LX/6Mp;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_6

    .line 2028435
    :cond_10
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2028436
    invoke-virtual {p1, v7, v8}, LX/186;->b(II)V

    .line 2028437
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v5}, LX/186;->b(II)V

    .line 2028438
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_5

    :cond_11
    move v5, v7

    move v8, v7

    goto :goto_6
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2028439
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2028440
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2028441
    if-eqz v0, :cond_0

    .line 2028442
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028443
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2028444
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2028445
    if-eqz v0, :cond_5

    .line 2028446
    const-string v1, "phones"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028447
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2028448
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 2028449
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 2028450
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2028451
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2028452
    if-eqz v3, :cond_3

    .line 2028453
    const-string v4, "primary_field"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028454
    const/4 v2, 0x0

    .line 2028455
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2028456
    invoke-virtual {p0, v3, v2}, LX/15i;->g(II)I

    move-result v4

    .line 2028457
    if-eqz v4, :cond_1

    .line 2028458
    const-string v4, "__type__"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028459
    invoke-static {p0, v3, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2028460
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2028461
    if-eqz v4, :cond_2

    .line 2028462
    const-string v2, "phone"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028463
    invoke-static {p0, v4, p2}, LX/6Mp;->a(LX/15i;ILX/0nX;)V

    .line 2028464
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2028465
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2028466
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2028467
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2028468
    :cond_5
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2028469
    if-eqz v0, :cond_6

    .line 2028470
    const-string v1, "represented_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028471
    invoke-static {p0, v0, p2, p3}, LX/Dfw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2028472
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2028473
    if-eqz v0, :cond_7

    .line 2028474
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028475
    invoke-static {p0, v0, p2, p3}, LX/3EF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2028476
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2028477
    return-void
.end method
