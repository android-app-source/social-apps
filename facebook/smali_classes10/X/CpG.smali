.class public LX/CpG;
.super LX/Coi;
.source ""

# interfaces
.implements LX/CoY;


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CIg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:I

.field public final h:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1936828
    invoke-direct {p0, p1}, LX/Coi;-><init>(Landroid/view/View;)V

    .line 1936829
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b12ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/CpG;->c:I

    .line 1936830
    const-class v0, LX/CpG;

    invoke-static {v0, p0}, LX/CpG;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936831
    const v0, 0x7f0d2a3c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/CpG;->h:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936832
    iget-object v0, p0, LX/CpG;->h:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936833
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936834
    iget-object v1, p0, LX/CpG;->a:LX/Cju;

    const v2, 0x7f0d011e

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/CtG;->setMinimumWidth(I)V

    .line 1936835
    iget-object v0, p0, LX/CpG;->h:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936836
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936837
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/CtG;->setGravity(I)V

    .line 1936838
    const v0, 0x7f0d2a3d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/CpG;->i:Landroid/widget/TextView;

    .line 1936839
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1936840
    iget-object v0, p0, LX/CpG;->i:Landroid/widget/TextView;

    iget-object v1, p0, LX/CpG;->a:LX/Cju;

    const v2, 0x7f0d011d

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/TextView;->setPaddingRelative(IIII)V

    .line 1936841
    :goto_0
    new-instance v0, LX/Cn7;

    new-instance v1, LX/Cn4;

    iget-object v2, p0, LX/CpG;->a:LX/Cju;

    invoke-direct {v1, v2}, LX/Cn4;-><init>(LX/Cju;)V

    new-instance v2, LX/Cn2;

    invoke-direct {v2}, LX/Cn2;-><init>()V

    invoke-direct {v0, v1, v4, v4, v2}, LX/Cn7;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;)V

    .line 1936842
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1936843
    return-void

    .line 1936844
    :cond_0
    iget-object v0, p0, LX/CpG;->i:Landroid/widget/TextView;

    iget-object v1, p0, LX/CpG;->a:LX/Cju;

    const v2, 0x7f0d011d

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CpG;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v1

    check-cast v1, LX/Cju;

    invoke-static {p0}, LX/CIg;->a(LX/0QB;)LX/CIg;

    move-result-object p0

    check-cast p0, LX/CIg;

    iput-object v1, p1, LX/CpG;->a:LX/Cju;

    iput-object p0, p1, LX/CpG;->b:LX/CIg;

    return-void
.end method
