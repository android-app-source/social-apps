.class public LX/Doc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/1Hy;

.field private static volatile e:LX/Doc;


# instance fields
.field public final b:LX/1Hn;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Doj;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1Hb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042121
    sget-object v0, LX/1Hy;->KEY_256:LX/1Hy;

    sput-object v0, LX/Doc;->a:LX/1Hy;

    return-void
.end method

.method public constructor <init>(LX/1Hn;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Hn;",
            "LX/0Or",
            "<",
            "LX/Doj;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042123
    iput-object p1, p0, LX/Doc;->b:LX/1Hn;

    .line 2042124
    iput-object p2, p0, LX/Doc;->c:LX/0Or;

    .line 2042125
    const-string v0, ""

    invoke-static {v0}, LX/1Hb;->a(Ljava/lang/String;)LX/1Hb;

    move-result-object v0

    iput-object v0, p0, LX/Doc;->d:LX/1Hb;

    .line 2042126
    return-void
.end method

.method public static a(LX/0QB;)LX/Doc;
    .locals 5

    .prologue
    .line 2042127
    sget-object v0, LX/Doc;->e:LX/Doc;

    if-nez v0, :cond_1

    .line 2042128
    const-class v1, LX/Doc;

    monitor-enter v1

    .line 2042129
    :try_start_0
    sget-object v0, LX/Doc;->e:LX/Doc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2042130
    if-eqz v2, :cond_0

    .line 2042131
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2042132
    new-instance v4, LX/Doc;

    invoke-static {v0}, LX/1Hn;->a(LX/0QB;)LX/1Hn;

    move-result-object v3

    check-cast v3, LX/1Hn;

    const/16 p0, 0x2a15

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/Doc;-><init>(LX/1Hn;LX/0Or;)V

    .line 2042133
    move-object v0, v4

    .line 2042134
    sput-object v0, LX/Doc;->e:LX/Doc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042135
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2042136
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2042137
    :cond_1
    sget-object v0, LX/Doc;->e:LX/Doc;

    return-object v0

    .line 2042138
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2042139
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
