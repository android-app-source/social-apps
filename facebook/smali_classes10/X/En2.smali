.class public LX/En2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 2166595
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0}, LX/En2;-><init>(ILjava/util/List;)V

    .line 2166596
    return-void
.end method

.method public constructor <init>(ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2166591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166592
    iput-object p2, p0, LX/En2;->a:Ljava/util/List;

    .line 2166593
    iput p1, p0, LX/En2;->b:I

    .line 2166594
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)I
    .locals 1

    .prologue
    .line 2166590
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/En2;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/2addr v0, p1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ILjava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 2166579
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/En2;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2166580
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Adding a value at %d would cause a discontinuity in the array"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2166581
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2166582
    :cond_0
    :try_start_1
    iget v0, p0, LX/En2;->b:I

    sub-int v0, p1, v0

    .line 2166583
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 2166584
    iget-object v0, p0, LX/En2;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2166585
    iget v0, p0, LX/En2;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/En2;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2166586
    :goto_0
    monitor-exit p0

    return-void

    .line 2166587
    :cond_1
    :try_start_2
    iget-object v1, p0, LX/En2;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 2166588
    iget-object v0, p0, LX/En2;->a:Ljava/util/List;

    iget-object v1, p0, LX/En2;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 2166589
    :cond_2
    iget-object v1, p0, LX/En2;->a:Ljava/util/List;

    invoke-interface {v1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 2166578
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/En2;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 2166577
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/En2;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 2166576
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/En2;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()I
    .locals 1

    .prologue
    .line 2166575
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/En2;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(I)Z
    .locals 2

    .prologue
    .line 2166567
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/En2;->b:I

    sub-int v0, p1, v0

    .line 2166568
    const/4 v1, -0x1

    if-lt v0, v1, :cond_0

    iget-object v1, p0, LX/En2;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2166569
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 2166574
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/En2;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2166570
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/En2;->a:Ljava/util/List;

    instance-of v0, v0, LX/0Px;

    if-eqz v0, :cond_0

    .line 2166571
    iget-object v0, p0, LX/En2;->a:Ljava/util/List;

    check-cast v0, LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2166572
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/En2;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 2166573
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
