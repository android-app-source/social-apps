.class public abstract LX/Ecy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lorg/xmlpull/v1/XmlPullParser;

.field private final b:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 1

    .prologue
    .line 2148100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2148101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/Ecy;->b:Ljava/lang/StringBuilder;

    .line 2148102
    iput-object p1, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    .line 2148103
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 2148127
    const/4 v0, 0x2

    .line 2148128
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 2148129
    if-eq v1, v0, :cond_1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 2148130
    :cond_1
    move v0, v1

    .line 2148131
    if-eq v0, v2, :cond_3

    .line 2148132
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ApnsXmlProcessor: expecting start tag @"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ecy;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2148133
    :catch_0
    move-exception v0

    .line 2148134
    const-string v1, "MmsLib"

    const-string v2, "XmlResourceParser: I/O failure"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2148135
    :cond_2
    :goto_0
    return-void

    .line 2148136
    :cond_3
    :try_start_1
    invoke-virtual {p0}, LX/Ecy;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2148137
    const-string v0, "MmsLib"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Carrier config does not start with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ecy;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 2148138
    :catch_1
    move-exception v0

    .line 2148139
    const-string v1, "MmsLib"

    const-string v2, "XmlResourceParser: parsing failure"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2148140
    :cond_4
    :goto_1
    iget-object v0, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    .line 2148141
    if-ne v0, v2, :cond_5

    .line 2148142
    invoke-virtual {p0}, LX/Ecy;->a()V

    goto :goto_1

    .line 2148143
    :cond_5
    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    .line 2148144
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expecting start or end tag @"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ecy;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2148104
    iget-object v1, p0, LX/Ecy;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2148105
    iget-object v1, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    if-eqz v1, :cond_3

    .line 2148106
    :try_start_0
    iget-object v1, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    .line 2148107
    iget-object v2, p0, LX/Ecy;->b:Ljava/lang/StringBuilder;

    .line 2148108
    packed-switch v1, :pswitch_data_0

    .line 2148109
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    move-object v3, v3

    .line 2148110
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148111
    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 2148112
    :cond_0
    iget-object v1, p0, LX/Ecy;->b:Ljava/lang/StringBuilder;

    const/16 v2, 0x3c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148113
    :goto_1
    iget-object v1, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2148114
    iget-object v1, p0, LX/Ecy;->b:Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v2, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148115
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2148116
    :cond_1
    iget-object v0, p0, LX/Ecy;->b:Ljava/lang/StringBuilder;

    const-string v1, "/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148117
    :cond_2
    iget-object v0, p0, LX/Ecy;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2148118
    :goto_2
    return-object v0

    .line 2148119
    :catch_0
    move-exception v0

    .line 2148120
    const-string v1, "MmsLib"

    const-string v2, "XmlResourceParser exception"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2148121
    :cond_3
    const-string v0, "Unknown"

    goto :goto_2

    .line 2148122
    :pswitch_0
    const-string v3, "START_DOCUMENT"

    goto :goto_0

    .line 2148123
    :pswitch_1
    const-string v3, "END_DOCUMENT"

    goto :goto_0

    .line 2148124
    :pswitch_2
    const-string v3, "START_TAG"

    goto :goto_0

    .line 2148125
    :pswitch_3
    const-string v3, "END_TAG"

    goto :goto_0

    .line 2148126
    :pswitch_4
    const-string v3, "TEXT"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
