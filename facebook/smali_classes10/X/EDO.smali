.class public LX/EDO;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/util/Random;

.field private c:LX/0ad;

.field private d:LX/EC9;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2091344
    const-class v0, LX/EDO;

    sput-object v0, LX/EDO;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/Random;LX/EC9;LX/0ad;)V
    .locals 0
    .param p1    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p2    # LX/EC9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2091291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2091292
    iput-object p1, p0, LX/EDO;->b:Ljava/util/Random;

    .line 2091293
    iput-object p2, p0, LX/EDO;->d:LX/EC9;

    .line 2091294
    iput-object p3, p0, LX/EDO;->c:LX/0ad;

    .line 2091295
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2091338
    iget-object v0, p0, LX/EDO;->d:LX/EC9;

    invoke-interface {v0}, LX/EC9;->l()LX/0gc;

    move-result-object v0

    .line 2091339
    const-string v1, "dialog"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;

    .line 2091340
    if-eqz v0, :cond_0

    .line 2091341
    invoke-virtual {v0}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->b()V

    .line 2091342
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2091343
    :cond_0
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 2091328
    new-instance v0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;

    invoke-direct {v0}, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;-><init>()V

    .line 2091329
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2091330
    const-string v2, "rating"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2091331
    const-string v2, "reason_key"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2091332
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2091333
    move-object v0, v0

    .line 2091334
    iget-object v1, p0, LX/EDO;->d:LX/EC9;

    invoke-interface {v1}, LX/EC9;->l()LX/0gc;

    move-result-object v1

    .line 2091335
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v2, v0, v3}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2091336
    invoke-virtual {v1}, LX/0gc;->b()Z

    .line 2091337
    return-void
.end method

.method public final a(IZ)V
    .locals 4

    .prologue
    .line 2091317
    new-instance v0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;

    invoke-direct {v0}, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;-><init>()V

    .line 2091318
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2091319
    const-string v2, "rating"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2091320
    const-string v2, "use_video"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2091321
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2091322
    move-object v0, v0

    .line 2091323
    iget-object v1, p0, LX/EDO;->d:LX/EC9;

    invoke-interface {v1}, LX/EC9;->l()LX/0gc;

    move-result-object v1

    .line 2091324
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v2, v0, v3}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->c()I

    .line 2091325
    invoke-virtual {v1}, LX/0gc;->b()Z

    .line 2091326
    invoke-virtual {v0}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->m()V

    .line 2091327
    return-void
.end method

.method public final a(ZZ)Z
    .locals 6

    .prologue
    const/16 v5, 0x64

    const/4 v0, 0x0

    .line 2091301
    iget-object v1, p0, LX/EDO;->c:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/3Dx;->fy:I

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    .line 2091302
    if-nez p1, :cond_1

    iget-object v2, p0, LX/EDO;->b:Ljava/util/Random;

    invoke-virtual {v2, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    if-lt v2, v1, :cond_1

    .line 2091303
    :cond_0
    :goto_0
    return v0

    .line 2091304
    :cond_1
    iget-object v1, p0, LX/EDO;->d:LX/EC9;

    invoke-interface {v1}, LX/EC9;->l()LX/0gc;

    move-result-object v1

    .line 2091305
    if-eqz v1, :cond_0

    .line 2091306
    iget-object v2, p0, LX/EDO;->c:LX/0ad;

    sget-short v3, LX/3Dx;->u:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2091307
    new-instance v2, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;

    invoke-direct {v2}, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;-><init>()V

    .line 2091308
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2091309
    const-string v4, "is_conference"

    invoke-virtual {v3, v4, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2091310
    const-string v4, "show_call_again"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2091311
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2091312
    move-object v0, v2

    .line 2091313
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v2, v0, v3}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->c()I

    .line 2091314
    invoke-virtual {v1}, LX/0gc;->b()Z

    .line 2091315
    invoke-virtual {v0}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->m()V

    .line 2091316
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2091296
    iget-object v0, p0, LX/EDO;->d:LX/EC9;

    invoke-interface {v0}, LX/EC9;->l()LX/0gc;

    move-result-object v0

    .line 2091297
    if-nez v0, :cond_0

    move v0, v1

    .line 2091298
    :goto_0
    return v0

    .line 2091299
    :cond_0
    const-string v2, "dialog"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 2091300
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
