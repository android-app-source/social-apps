.class public LX/DqO;
.super LX/D8z;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/LayoutInflater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3Q7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Landroid/view/View;

.field public f:Z

.field public g:Z

.field public h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2048296
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/DqO;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2048297
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 2048298
    invoke-direct {p0, p1, p2, p3}, LX/D8z;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2048299
    const-class v0, LX/DqO;

    invoke-static {v0, p0}, LX/DqO;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2048300
    iget-object v0, p0, LX/DqO;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030a52

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/DqO;->e:Landroid/view/View;

    .line 2048301
    iget-object v0, p0, LX/DqO;->e:Landroid/view/View;

    .line 2048302
    iget-object v1, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomFrameLayout;->removeAllViews()V

    .line 2048303
    iget-object v1, p0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;)V

    .line 2048304
    invoke-virtual {p0}, LX/DqO;->b()V

    .line 2048305
    const v0, 0x7f0d1a20

    invoke-virtual {p0, v0}, LX/D8z;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2048306
    const v1, 0x7f081161

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2048307
    const v1, 0x7f081162

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 2048308
    new-instance v1, LX/DqL;

    invoke-direct {v1, p0}, LX/DqL;-><init>(LX/DqO;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2048309
    iget-object v1, p0, LX/DqO;->c:LX/3Q7;

    invoke-virtual {v1}, LX/3Q7;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2048310
    const v1, 0x7f0d1a1f

    invoke-virtual {p0, v1}, LX/D8z;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/SwitchCompat;

    .line 2048311
    new-instance v2, LX/DqM;

    invoke-direct {v2, p0, v0}, LX/DqM;-><init>(LX/DqO;Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2048312
    const v2, 0x7f0d1a21

    invoke-virtual {p0, v2}, LX/D8z;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 2048313
    new-instance v3, LX/DqN;

    invoke-direct {v3, p0, v0, v1, v2}, LX/DqN;-><init>(LX/DqO;Lcom/facebook/fbui/widget/contentview/CheckedContentView;Lcom/facebook/widget/SwitchCompat;Landroid/widget/Button;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2048314
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/DqO;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    invoke-static {p0}, LX/3Q7;->a(LX/0QB;)LX/3Q7;

    move-result-object v3

    check-cast v3, LX/3Q7;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p0

    check-cast p0, LX/0Zb;

    iput-object v1, p1, LX/DqO;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v2, p1, LX/DqO;->b:Landroid/view/LayoutInflater;

    iput-object v3, p1, LX/DqO;->c:LX/3Q7;

    iput-object p0, p1, LX/DqO;->d:LX/0Zb;

    return-void
.end method

.method public static a$redex0(LX/DqO;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2048315
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "lockscreen_notification_setting_status"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "notifications"

    .line 2048316
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2048317
    move-object v0, v0

    .line 2048318
    const-string v1, "lockscreen_notification_on"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "source"

    const-string v2, "info_icon"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "setting"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2048319
    iget-object v1, p0, LX/DqO;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2048320
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 2048321
    const v0, 0x7f0d1a1f

    invoke-virtual {p0, v0}, LX/D8z;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    .line 2048322
    iget-object v1, p0, LX/DqO;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0hM;->F:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    iput-boolean v1, p0, LX/DqO;->f:Z

    .line 2048323
    iget-boolean v1, p0, LX/DqO;->f:Z

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2048324
    iget-boolean v0, p0, LX/DqO;->f:Z

    iput-boolean v0, p0, LX/DqO;->g:Z

    .line 2048325
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 2048326
    iget-object v0, p0, LX/DqO;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2048327
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 2048328
    if-eq p1, v1, :cond_0

    const/4 v1, -0x2

    :cond_0
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2048329
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2048330
    iget-object v0, p0, LX/DqO;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2048331
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2048332
    iget-object v1, p0, LX/DqO;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2048333
    return-void
.end method
