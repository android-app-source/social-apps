.class public final LX/EbI;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EbG;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EbI;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EbI;


# instance fields
.field public baseKey_:LX/EWc;

.field public bitField0_:I

.field public identityKey_:LX/EWc;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public message_:LX/EWc;

.field public preKeyId_:I

.field public registrationId_:I

.field public signedPreKeyId_:I

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2143344
    new-instance v0, LX/EbF;

    invoke-direct {v0}, LX/EbF;-><init>()V

    sput-object v0, LX/EbI;->a:LX/EWZ;

    .line 2143345
    new-instance v0, LX/EbI;

    invoke-direct {v0}, LX/EbI;-><init>()V

    .line 2143346
    sput-object v0, LX/EbI;->c:LX/EbI;

    invoke-direct {v0}, LX/EbI;->C()V

    .line 2143347
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2143348
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2143349
    iput-byte v0, p0, LX/EbI;->memoizedIsInitialized:B

    .line 2143350
    iput v0, p0, LX/EbI;->memoizedSerializedSize:I

    .line 2143351
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2143352
    iput-object v0, p0, LX/EbI;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2143353
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2143354
    iput-byte v0, p0, LX/EbI;->memoizedIsInitialized:B

    .line 2143355
    iput v0, p0, LX/EbI;->memoizedSerializedSize:I

    .line 2143356
    invoke-direct {p0}, LX/EbI;->C()V

    .line 2143357
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2143358
    const/4 v0, 0x0

    .line 2143359
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2143360
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2143361
    sparse-switch v3, :sswitch_data_0

    .line 2143362
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2143363
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2143364
    goto :goto_0

    .line 2143365
    :sswitch_1
    iget v3, p0, LX/EbI;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/EbI;->bitField0_:I

    .line 2143366
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EbI;->preKeyId_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2143367
    :catch_0
    move-exception v0

    .line 2143368
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2143369
    move-object v0, v0

    .line 2143370
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2143371
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EbI;->unknownFields:LX/EZQ;

    .line 2143372
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2143373
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/EbI;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, LX/EbI;->bitField0_:I

    .line 2143374
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EbI;->baseKey_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2143375
    :catch_1
    move-exception v0

    .line 2143376
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2143377
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2143378
    move-object v0, v1

    .line 2143379
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2143380
    :sswitch_3
    :try_start_4
    iget v3, p0, LX/EbI;->bitField0_:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, LX/EbI;->bitField0_:I

    .line 2143381
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EbI;->identityKey_:LX/EWc;

    goto :goto_0

    .line 2143382
    :sswitch_4
    iget v3, p0, LX/EbI;->bitField0_:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, LX/EbI;->bitField0_:I

    .line 2143383
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EbI;->message_:LX/EWc;

    goto :goto_0

    .line 2143384
    :sswitch_5
    iget v3, p0, LX/EbI;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/EbI;->bitField0_:I

    .line 2143385
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EbI;->registrationId_:I

    goto :goto_0

    .line 2143386
    :sswitch_6
    iget v3, p0, LX/EbI;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, LX/EbI;->bitField0_:I

    .line 2143387
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EbI;->signedPreKeyId_:I
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2143388
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EbI;->unknownFields:LX/EZQ;

    .line 2143389
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2143390
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2143391
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2143392
    iput-byte v1, p0, LX/EbI;->memoizedIsInitialized:B

    .line 2143393
    iput v1, p0, LX/EbI;->memoizedSerializedSize:I

    .line 2143394
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EbI;->unknownFields:LX/EZQ;

    .line 2143395
    return-void
.end method

.method private C()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2143396
    iput v0, p0, LX/EbI;->registrationId_:I

    .line 2143397
    iput v0, p0, LX/EbI;->preKeyId_:I

    .line 2143398
    iput v0, p0, LX/EbI;->signedPreKeyId_:I

    .line 2143399
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbI;->baseKey_:LX/EWc;

    .line 2143400
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbI;->identityKey_:LX/EWc;

    .line 2143401
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbI;->message_:LX/EWc;

    .line 2143402
    return-void
.end method

.method private static a(LX/EbI;)LX/EbH;
    .locals 1

    .prologue
    .line 2143403
    invoke-static {}, LX/EbH;->u()LX/EbH;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EbH;->a(LX/EbI;)LX/EbH;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2143404
    new-instance v0, LX/EbH;

    invoke-direct {v0, p1}, LX/EbH;-><init>(LX/EYd;)V

    .line 2143405
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2143406
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2143407
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_0

    .line 2143408
    iget v0, p0, LX/EbI;->preKeyId_:I

    invoke-virtual {p1, v2, v0}, LX/EWf;->c(II)V

    .line 2143409
    :cond_0
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 2143410
    iget-object v0, p0, LX/EbI;->baseKey_:LX/EWc;

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2143411
    :cond_1
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 2143412
    const/4 v0, 0x3

    iget-object v1, p0, LX/EbI;->identityKey_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2143413
    :cond_2
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    .line 2143414
    iget-object v0, p0, LX/EbI;->message_:LX/EWc;

    invoke-virtual {p1, v4, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2143415
    :cond_3
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 2143416
    const/4 v0, 0x5

    iget v1, p0, LX/EbI;->registrationId_:I

    invoke-virtual {p1, v0, v1}, LX/EWf;->c(II)V

    .line 2143417
    :cond_4
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_5

    .line 2143418
    const/4 v0, 0x6

    iget v1, p0, LX/EbI;->signedPreKeyId_:I

    invoke-virtual {p1, v0, v1}, LX/EWf;->c(II)V

    .line 2143419
    :cond_5
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2143420
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2143421
    iget-byte v1, p0, LX/EbI;->memoizedIsInitialized:B

    .line 2143422
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2143423
    :goto_0
    return v0

    .line 2143424
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2143425
    :cond_1
    iput-byte v0, p0, LX/EbI;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2143426
    iget v0, p0, LX/EbI;->memoizedSerializedSize:I

    .line 2143427
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2143428
    :goto_0
    return v0

    .line 2143429
    :cond_0
    const/4 v0, 0x0

    .line 2143430
    iget v1, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 2143431
    iget v0, p0, LX/EbI;->preKeyId_:I

    invoke-static {v3, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2143432
    :cond_1
    iget v1, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    .line 2143433
    iget-object v1, p0, LX/EbI;->baseKey_:LX/EWc;

    invoke-static {v4, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143434
    :cond_2
    iget v1, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_3

    .line 2143435
    const/4 v1, 0x3

    iget-object v2, p0, LX/EbI;->identityKey_:LX/EWc;

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143436
    :cond_3
    iget v1, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_4

    .line 2143437
    iget-object v1, p0, LX/EbI;->message_:LX/EWc;

    invoke-static {v5, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143438
    :cond_4
    iget v1, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_5

    .line 2143439
    const/4 v1, 0x5

    iget v2, p0, LX/EbI;->registrationId_:I

    invoke-static {v1, v2}, LX/EWf;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143440
    :cond_5
    iget v1, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_6

    .line 2143441
    const/4 v1, 0x6

    iget v2, p0, LX/EbI;->signedPreKeyId_:I

    invoke-static {v1, v2}, LX/EWf;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143442
    :cond_6
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2143443
    iput v0, p0, LX/EbI;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2143343
    iget-object v0, p0, LX/EbI;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2143342
    sget-object v0, LX/EbV;->d:LX/EYn;

    const-class v1, LX/EbI;

    const-class v2, LX/EbH;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EbI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2143332
    sget-object v0, LX/EbI;->a:LX/EWZ;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2143333
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 2143334
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 2143335
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2143336
    invoke-static {p0}, LX/EbI;->a(LX/EbI;)LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2143337
    invoke-static {}, LX/EbH;->u()LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2143338
    invoke-static {p0}, LX/EbI;->a(LX/EbI;)LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2143339
    sget-object v0, LX/EbI;->c:LX/EbI;

    return-object v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 2143340
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Z
    .locals 2

    .prologue
    .line 2143341
    iget v0, p0, LX/EbI;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
