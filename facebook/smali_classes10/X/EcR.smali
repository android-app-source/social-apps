.class public final LX/EcR;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EcP;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcR;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EcR;


# instance fields
.field public bitField0_:I

.field public index_:I

.field public key_:LX/EWc;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2146717
    new-instance v0, LX/EcO;

    invoke-direct {v0}, LX/EcO;-><init>()V

    sput-object v0, LX/EcR;->a:LX/EWZ;

    .line 2146718
    new-instance v0, LX/EcR;

    invoke-direct {v0}, LX/EcR;-><init>()V

    .line 2146719
    sput-object v0, LX/EcR;->c:LX/EcR;

    invoke-direct {v0}, LX/EcR;->r()V

    .line 2146720
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2146756
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2146757
    iput-byte v0, p0, LX/EcR;->memoizedIsInitialized:B

    .line 2146758
    iput v0, p0, LX/EcR;->memoizedSerializedSize:I

    .line 2146759
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2146760
    iput-object v0, p0, LX/EcR;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2146726
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2146727
    iput-byte v0, p0, LX/EcR;->memoizedIsInitialized:B

    .line 2146728
    iput v0, p0, LX/EcR;->memoizedSerializedSize:I

    .line 2146729
    invoke-direct {p0}, LX/EcR;->r()V

    .line 2146730
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2146731
    const/4 v0, 0x0

    .line 2146732
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2146733
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2146734
    sparse-switch v3, :sswitch_data_0

    .line 2146735
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2146736
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2146737
    goto :goto_0

    .line 2146738
    :sswitch_1
    iget v3, p0, LX/EcR;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/EcR;->bitField0_:I

    .line 2146739
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EcR;->index_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2146740
    :catch_0
    move-exception v0

    .line 2146741
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2146742
    move-object v0, v0

    .line 2146743
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2146744
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EcR;->unknownFields:LX/EZQ;

    .line 2146745
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2146746
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/EcR;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/EcR;->bitField0_:I

    .line 2146747
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EcR;->key_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2146748
    :catch_1
    move-exception v0

    .line 2146749
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2146750
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2146751
    move-object v0, v1

    .line 2146752
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2146753
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcR;->unknownFields:LX/EZQ;

    .line 2146754
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2146755
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2146721
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2146722
    iput-byte v1, p0, LX/EcR;->memoizedIsInitialized:B

    .line 2146723
    iput v1, p0, LX/EcR;->memoizedSerializedSize:I

    .line 2146724
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcR;->unknownFields:LX/EZQ;

    .line 2146725
    return-void
.end method

.method public static a(LX/EcR;)LX/EcQ;
    .locals 1

    .prologue
    .line 2146716
    invoke-static {}, LX/EcQ;->w()LX/EcQ;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EcQ;->a(LX/EcR;)LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method private r()V
    .locals 1

    .prologue
    .line 2146713
    const/4 v0, 0x0

    iput v0, p0, LX/EcR;->index_:I

    .line 2146714
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcR;->key_:LX/EWc;

    .line 2146715
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2146711
    new-instance v0, LX/EcQ;

    invoke-direct {v0, p1}, LX/EcQ;-><init>(LX/EYd;)V

    .line 2146712
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2146704
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2146705
    iget v0, p0, LX/EcR;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2146706
    iget v0, p0, LX/EcR;->index_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2146707
    :cond_0
    iget v0, p0, LX/EcR;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2146708
    iget-object v0, p0, LX/EcR;->key_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2146709
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2146710
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2146761
    iget-byte v1, p0, LX/EcR;->memoizedIsInitialized:B

    .line 2146762
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2146763
    :goto_0
    return v0

    .line 2146764
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2146765
    :cond_1
    iput-byte v0, p0, LX/EcR;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2146694
    iget v0, p0, LX/EcR;->memoizedSerializedSize:I

    .line 2146695
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2146696
    :goto_0
    return v0

    .line 2146697
    :cond_0
    const/4 v0, 0x0

    .line 2146698
    iget v1, p0, LX/EcR;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2146699
    iget v0, p0, LX/EcR;->index_:I

    invoke-static {v2, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2146700
    :cond_1
    iget v1, p0, LX/EcR;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2146701
    iget-object v1, p0, LX/EcR;->key_:LX/EWc;

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2146702
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2146703
    iput v0, p0, LX/EcR;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2146693
    iget-object v0, p0, LX/EcR;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2146692
    sget-object v0, LX/Eck;->f:LX/EYn;

    const-class v1, LX/EcR;

    const-class v2, LX/EcQ;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2146691
    sget-object v0, LX/EcR;->a:LX/EWZ;

    return-object v0
.end method

.method public final o()LX/EcQ;
    .locals 1

    .prologue
    .line 2146690
    invoke-static {p0}, LX/EcR;->a(LX/EcR;)LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2146686
    invoke-virtual {p0}, LX/EcR;->o()LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2146689
    invoke-static {}, LX/EcQ;->w()LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2146688
    invoke-virtual {p0}, LX/EcR;->o()LX/EcQ;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2146687
    sget-object v0, LX/EcR;->c:LX/EcR;

    return-object v0
.end method
