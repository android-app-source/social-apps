.class public LX/EeX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final downloadId:J

.field public final downloadManagerReason:I

.field public final downloadManagerStatus:I

.field public final downloadProgress:J

.field public final downloadSize:J

.field public final extras:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/Serializable;",
            ">;"
        }
    .end annotation
.end field

.field public final failureReason:Ljava/lang/Throwable;

.field public final isBackgroundMode:Z

.field public final isDiffDownloadEnabled:Z

.field public final isSelfUpdate:Z

.field public final isWifiOnly:Z

.field public final localDiffDownloadFile:Ljava/io/File;

.field public final localFile:Ljava/io/File;

.field public mDownloadSpeedTracker:LX/Eea;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final operationState$:Ljava/lang/Integer;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "operationState"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation
.end field

.field public final operationUuid:Ljava/lang/String;

.field public final releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;


# direct methods
.method public constructor <init>(Lcom/facebook/appupdate/ReleaseInfo;Ljava/lang/String;ZZZZLjava/util/Map;)V
    .locals 21
    .param p7    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/appupdate/ReleaseInfo;",
            "Ljava/lang/String;",
            "ZZZZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/Serializable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2152880
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-wide/16 v8, -0x1

    const-wide/16 v10, -0x1

    const-wide/16 v12, -0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, -0x1

    const/16 v18, -0x1

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move-object/from16 v7, p2

    move-object/from16 v19, p7

    invoke-direct/range {v0 .. v20}, LX/EeX;-><init>(Lcom/facebook/appupdate/ReleaseInfo;ZZZZLjava/lang/Integer;Ljava/lang/String;JJJLjava/io/File;Ljava/io/File;Ljava/lang/Throwable;IILjava/util/Map;LX/Eea;)V

    .line 2152881
    return-void
.end method

.method public constructor <init>(Lcom/facebook/appupdate/ReleaseInfo;ZZZZLjava/lang/Integer;Ljava/lang/String;JJJLjava/io/File;Ljava/io/File;Ljava/lang/Throwable;IILjava/util/Map;LX/Eea;)V
    .locals 2
    .param p19    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p20    # LX/Eea;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/appupdate/ReleaseInfo;",
            "ZZZZ",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "JJJ",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "Ljava/lang/Throwable;",
            "II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/Serializable;",
            ">;",
            "LX/Eea;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2152860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152861
    iput-object p1, p0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    .line 2152862
    iput-boolean p2, p0, LX/EeX;->isBackgroundMode:Z

    .line 2152863
    iput-boolean p3, p0, LX/EeX;->isDiffDownloadEnabled:Z

    .line 2152864
    iput-boolean p4, p0, LX/EeX;->isSelfUpdate:Z

    .line 2152865
    iput-boolean p5, p0, LX/EeX;->isWifiOnly:Z

    .line 2152866
    iput-object p6, p0, LX/EeX;->operationState$:Ljava/lang/Integer;

    .line 2152867
    iput-object p7, p0, LX/EeX;->operationUuid:Ljava/lang/String;

    .line 2152868
    iput-wide p8, p0, LX/EeX;->downloadId:J

    .line 2152869
    iput-wide p10, p0, LX/EeX;->downloadProgress:J

    .line 2152870
    iput-wide p12, p0, LX/EeX;->downloadSize:J

    .line 2152871
    move-object/from16 v0, p14

    iput-object v0, p0, LX/EeX;->localFile:Ljava/io/File;

    .line 2152872
    move-object/from16 v0, p15

    iput-object v0, p0, LX/EeX;->localDiffDownloadFile:Ljava/io/File;

    .line 2152873
    move-object/from16 v0, p16

    iput-object v0, p0, LX/EeX;->failureReason:Ljava/lang/Throwable;

    .line 2152874
    move/from16 v0, p17

    iput v0, p0, LX/EeX;->downloadManagerStatus:I

    .line 2152875
    move/from16 v0, p18

    iput v0, p0, LX/EeX;->downloadManagerReason:I

    .line 2152876
    if-nez p19, :cond_0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    :goto_0
    iput-object v1, p0, LX/EeX;->extras:Ljava/util/HashMap;

    .line 2152877
    move-object/from16 v0, p20

    iput-object v0, p0, LX/EeX;->mDownloadSpeedTracker:LX/Eea;

    .line 2152878
    return-void

    .line 2152879
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    move-object/from16 v0, p19

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_0
.end method

.method private f()F
    .locals 1

    .prologue
    .line 2152837
    iget-object v0, p0, LX/EeX;->mDownloadSpeedTracker:LX/Eea;

    if-nez v0, :cond_0

    .line 2152838
    const/4 v0, 0x0

    .line 2152839
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/EeX;->mDownloadSpeedTracker:LX/Eea;

    .line 2152840
    iget p0, v0, LX/Eea;->mAverageBytesPerSecond:F

    move v0, p0

    .line 2152841
    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 2152856
    invoke-virtual {p0}, LX/EeX;->c()Lorg/json/JSONObject;

    move-result-object v0

    .line 2152857
    cmp-long v1, p3, p1

    if-ltz v1, :cond_0

    .line 2152858
    const-string v1, "time_elapsed"

    sub-long v2, p3, p1

    invoke-static {v0, v1, v2, v3}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;J)V

    .line 2152859
    :cond_0
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2152855
    iget-boolean v0, p0, LX/EeX;->isDiffDownloadEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {v0}, Lcom/facebook/appupdate/ReleaseInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 2152851
    iget-object v0, p0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {v0}, Lcom/facebook/appupdate/ReleaseInfo;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 2152852
    invoke-virtual {p0}, LX/EeX;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2152853
    const-string v1, "diff_algorithm"

    sget-object v2, LX/Eeh;->BSDIFF:LX/Eeh;

    invoke-virtual {v2}, LX/Eeh;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2152854
    :cond_0
    return-object v0
.end method

.method public final d()LX/Eeh;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2152848
    invoke-virtual {p0}, LX/EeX;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2152849
    sget-object v0, LX/Eeh;->BSDIFF:LX/Eeh;

    .line 2152850
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()F
    .locals 6

    .prologue
    .line 2152842
    invoke-direct {p0}, LX/EeX;->f()F

    move-result v0

    .line 2152843
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    .line 2152844
    const/high16 v0, -0x40800000    # -1.0f

    .line 2152845
    :goto_0
    return v0

    .line 2152846
    :cond_0
    iget-object v1, p0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-wide v2, v1, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    iget-wide v4, p0, LX/EeX;->downloadProgress:J

    sub-long/2addr v2, v4

    .line 2152847
    long-to-float v1, v2

    div-float v0, v1, v0

    const/high16 v1, 0x42700000    # 60.0f

    div-float/2addr v0, v1

    goto :goto_0
.end method
