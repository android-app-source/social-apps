.class public final enum LX/DAk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DAk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DAk;

.field public static final enum ALL_CONTACT_CAPPED:LX/DAk;

.field public static final enum NEARBY:LX/DAk;

.field public static final enum PHONE_CALLLOGS:LX/DAk;

.field public static final enum PROMOTION:LX/DAk;

.field public static final enum RECENT_CALLS:LX/DAk;

.field public static final enum RTC_CALLLOGS:LX/DAk;

.field public static final enum RTC_ONGOING_GROUP_CALLS:LX/DAk;

.field public static final enum RTC_VOICEMAILS:LX/DAk;

.field public static final enum TOP:LX/DAk;

.field public static final enum TOP_CONTACT:LX/DAk;

.field public static final enum TOP_PHONE_CONTACT:LX/DAk;

.field public static final enum TOP_PUSHABLE:LX/DAk;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1971424
    new-instance v0, LX/DAk;

    const-string v1, "RECENT_CALLS"

    invoke-direct {v0, v1, v3}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->RECENT_CALLS:LX/DAk;

    .line 1971425
    new-instance v0, LX/DAk;

    const-string v1, "TOP_PUSHABLE"

    invoke-direct {v0, v1, v4}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->TOP_PUSHABLE:LX/DAk;

    .line 1971426
    new-instance v0, LX/DAk;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v5}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->TOP:LX/DAk;

    .line 1971427
    new-instance v0, LX/DAk;

    const-string v1, "TOP_CONTACT"

    invoke-direct {v0, v1, v6}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->TOP_CONTACT:LX/DAk;

    .line 1971428
    new-instance v0, LX/DAk;

    const-string v1, "TOP_PHONE_CONTACT"

    invoke-direct {v0, v1, v7}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->TOP_PHONE_CONTACT:LX/DAk;

    .line 1971429
    new-instance v0, LX/DAk;

    const-string v1, "NEARBY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->NEARBY:LX/DAk;

    .line 1971430
    new-instance v0, LX/DAk;

    const-string v1, "ALL_CONTACT_CAPPED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->ALL_CONTACT_CAPPED:LX/DAk;

    .line 1971431
    new-instance v0, LX/DAk;

    const-string v1, "PROMOTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->PROMOTION:LX/DAk;

    .line 1971432
    new-instance v0, LX/DAk;

    const-string v1, "RTC_CALLLOGS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->RTC_CALLLOGS:LX/DAk;

    .line 1971433
    new-instance v0, LX/DAk;

    const-string v1, "RTC_ONGOING_GROUP_CALLS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->RTC_ONGOING_GROUP_CALLS:LX/DAk;

    .line 1971434
    new-instance v0, LX/DAk;

    const-string v1, "RTC_VOICEMAILS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->RTC_VOICEMAILS:LX/DAk;

    .line 1971435
    new-instance v0, LX/DAk;

    const-string v1, "PHONE_CALLLOGS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/DAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAk;->PHONE_CALLLOGS:LX/DAk;

    .line 1971436
    const/16 v0, 0xc

    new-array v0, v0, [LX/DAk;

    sget-object v1, LX/DAk;->RECENT_CALLS:LX/DAk;

    aput-object v1, v0, v3

    sget-object v1, LX/DAk;->TOP_PUSHABLE:LX/DAk;

    aput-object v1, v0, v4

    sget-object v1, LX/DAk;->TOP:LX/DAk;

    aput-object v1, v0, v5

    sget-object v1, LX/DAk;->TOP_CONTACT:LX/DAk;

    aput-object v1, v0, v6

    sget-object v1, LX/DAk;->TOP_PHONE_CONTACT:LX/DAk;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DAk;->NEARBY:LX/DAk;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DAk;->ALL_CONTACT_CAPPED:LX/DAk;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DAk;->PROMOTION:LX/DAk;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/DAk;->RTC_CALLLOGS:LX/DAk;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/DAk;->RTC_ONGOING_GROUP_CALLS:LX/DAk;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/DAk;->RTC_VOICEMAILS:LX/DAk;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/DAk;->PHONE_CALLLOGS:LX/DAk;

    aput-object v2, v0, v1

    sput-object v0, LX/DAk;->$VALUES:[LX/DAk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1971437
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DAk;
    .locals 1

    .prologue
    .line 1971438
    const-class v0, LX/DAk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DAk;

    return-object v0
.end method

.method public static values()[LX/DAk;
    .locals 1

    .prologue
    .line 1971439
    sget-object v0, LX/DAk;->$VALUES:[LX/DAk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DAk;

    return-object v0
.end method
