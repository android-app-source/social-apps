.class public LX/D1U;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/D1U;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1956850
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1956851
    sget-object v0, LX/0ax;->k:Ljava/lang/String;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "{north}"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "{west}"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "{south}"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "{east}"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "{ad_id NOT_SET}"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "{page_set_id NOT_SET}"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "{parent_page_id NOT_SET}"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/storelocator/StoreLocatorActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 1956852
    return-void
.end method

.method public static a(LX/0QB;)LX/D1U;
    .locals 3

    .prologue
    .line 1956853
    sget-object v0, LX/D1U;->a:LX/D1U;

    if-nez v0, :cond_1

    .line 1956854
    const-class v1, LX/D1U;

    monitor-enter v1

    .line 1956855
    :try_start_0
    sget-object v0, LX/D1U;->a:LX/D1U;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1956856
    if-eqz v2, :cond_0

    .line 1956857
    :try_start_1
    new-instance v0, LX/D1U;

    invoke-direct {v0}, LX/D1U;-><init>()V

    .line 1956858
    move-object v0, v0

    .line 1956859
    sput-object v0, LX/D1U;->a:LX/D1U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1956860
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1956861
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1956862
    :cond_1
    sget-object v0, LX/D1U;->a:LX/D1U;

    return-object v0

    .line 1956863
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1956864
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1956865
    const/4 v0, 0x1

    return v0
.end method
