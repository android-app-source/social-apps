.class public final LX/DzB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DzG;


# direct methods
.method public constructor <init>(LX/DzG;)V
    .locals 0

    .prologue
    .line 2066480
    iput-object p1, p0, LX/DzB;->a:LX/DzG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x21dfedf7

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2066481
    iget-object v0, p0, LX/DzB;->a:LX/DzG;

    iget-object v0, v0, LX/DzG;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 2066482
    if-nez v0, :cond_0

    .line 2066483
    const v0, 0x7eca8f51

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2066484
    :goto_0
    return-void

    .line 2066485
    :cond_0
    iget-object v0, p0, LX/DzB;->a:LX/DzG;

    iget-object v0, v0, LX/DzG;->c:LX/9j5;

    .line 2066486
    iget-object v2, v0, LX/9j5;->a:LX/0Zb;

    const-string v3, "place_picker_nonintrusive_error_button_location_settings"

    invoke-static {v0, v3}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2066487
    iget-object v0, p0, LX/DzB;->a:LX/DzG;

    const-string v2, "niem_location_settings_click"

    invoke-static {v0, v2}, LX/DzG;->a$redex0(LX/DzG;Ljava/lang/String;)V

    .line 2066488
    iget-object v0, p0, LX/DzB;->a:LX/DzG;

    iget-object v0, v0, LX/DzG;->i:LX/0ad;

    sget-short v2, LX/5HH;->b:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2066489
    iget-object v0, p0, LX/DzB;->a:LX/DzG;

    iget-object v0, v0, LX/DzG;->c:LX/9j5;

    .line 2066490
    iget-object v2, v0, LX/9j5;->a:LX/0Zb;

    const-string v3, "place_picker_nonintrusive_error_gms_upsell_shown"

    invoke-static {v0, v3}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2066491
    iget-object v0, p0, LX/DzB;->a:LX/DzG;

    iget-object v0, v0, LX/DzG;->o:LX/6Zb;

    new-instance v2, LX/2si;

    invoke-direct {v2}, LX/2si;-><init>()V

    const-string v3, "surface_checkin_niem_controller"

    const-string v4, "mechanism_niem"

    invoke-virtual {v0, v2, v3, v4}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 2066492
    :goto_1
    const v0, 0x2772283a

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2066493
    :cond_1
    iget-object v0, p0, LX/DzB;->a:LX/DzG;

    invoke-static {v0}, LX/DzG;->y(LX/DzG;)V

    goto :goto_1
.end method
