.class public LX/E5X;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E5V;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E5X;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E5Y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078534
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5X;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E5Y;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078535
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078536
    iput-object p1, p0, LX/E5X;->b:LX/0Ot;

    .line 2078537
    return-void
.end method

.method public static a(LX/0QB;)LX/E5X;
    .locals 4

    .prologue
    .line 2078538
    sget-object v0, LX/E5X;->c:LX/E5X;

    if-nez v0, :cond_1

    .line 2078539
    const-class v1, LX/E5X;

    monitor-enter v1

    .line 2078540
    :try_start_0
    sget-object v0, LX/E5X;->c:LX/E5X;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078541
    if-eqz v2, :cond_0

    .line 2078542
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078543
    new-instance v3, LX/E5X;

    const/16 p0, 0x312b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E5X;-><init>(LX/0Ot;)V

    .line 2078544
    move-object v0, v3

    .line 2078545
    sput-object v0, LX/E5X;->c:LX/E5X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078546
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078547
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078548
    :cond_1
    sget-object v0, LX/E5X;->c:LX/E5X;

    return-object v0

    .line 2078549
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078550
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2078551
    check-cast p2, LX/E5W;

    .line 2078552
    iget-object v0, p0, LX/E5X;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E5Y;

    iget-object v2, p2, LX/E5W;->a:Ljava/lang/CharSequence;

    iget v3, p2, LX/E5W;->b:I

    iget-object v4, p2, LX/E5W;->c:Ljava/lang/CharSequence;

    iget v5, p2, LX/E5W;->d:I

    iget v6, p2, LX/E5W;->e:I

    move-object v1, p1

    const/4 p2, 0x6

    const/4 p0, 0x2

    .line 2078553
    iget-object v7, v0, LX/E5Y;->a:LX/1vg;

    invoke-virtual {v7, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/2xv;->h(I)LX/2xv;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/2xv;->j(I)LX/2xv;

    move-result-object v7

    invoke-virtual {v7}, LX/1n6;->b()LX/1dc;

    move-result-object v7

    .line 2078554
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const v9, 0x7f01072b

    invoke-interface {v8, v9}, LX/1Dh;->U(I)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v9

    const/4 p0, 0x1

    invoke-interface {v9, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v9

    const p0, 0x7f010717

    invoke-interface {v9, p2, p0}, LX/1Dh;->p(II)LX/1Dh;

    move-result-object v9

    const/16 p0, 0x8

    const p1, 0x7f0b163a

    invoke-interface {v9, p0, p1}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0, v7}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const p0, 0x7f0b163c

    invoke-interface {v7, p2, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    invoke-interface {v9, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    const p0, 0x7f0b0050

    invoke-virtual {v9, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v9

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v9, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v9

    invoke-interface {v7, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    const p0, 0x7f0b0050

    invoke-virtual {v9, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v9

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v9, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v9

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const p1, 0x7f081150

    invoke-virtual {v1, p1}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    const-string p1, " "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v9

    invoke-interface {v7, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    const p0, 0x7f0b0050

    invoke-virtual {v9, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v9

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v9, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    .line 2078555
    const p0, -0x560667d9

    const/4 p1, 0x0

    invoke-static {v1, p0, p1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2078556
    invoke-interface {v9, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v9

    invoke-interface {v7, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 2078557
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2078558
    invoke-static {}, LX/1dS;->b()V

    .line 2078559
    iget v0, p1, LX/1dQ;->b:I

    .line 2078560
    packed-switch v0, :pswitch_data_0

    .line 2078561
    :goto_0
    return-object v2

    .line 2078562
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2078563
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2078564
    check-cast v1, LX/E5W;

    .line 2078565
    iget-object p1, p0, LX/E5X;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/E5W;->f:Landroid/view/View$OnClickListener;

    .line 2078566
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2078567
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x560667d9
        :pswitch_0
    .end packed-switch
.end method
