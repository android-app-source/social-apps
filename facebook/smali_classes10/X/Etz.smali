.class public LX/Etz;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ety;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/center/components/FriendItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2178867
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Etz;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friending/center/components/FriendItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2178868
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2178869
    iput-object p1, p0, LX/Etz;->b:LX/0Ot;

    .line 2178870
    return-void
.end method

.method public static a(LX/0QB;)LX/Etz;
    .locals 4

    .prologue
    .line 2178871
    const-class v1, LX/Etz;

    monitor-enter v1

    .line 2178872
    :try_start_0
    sget-object v0, LX/Etz;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2178873
    sput-object v2, LX/Etz;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2178874
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2178875
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2178876
    new-instance v3, LX/Etz;

    const/16 p0, 0x2229

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Etz;-><init>(LX/0Ot;)V

    .line 2178877
    move-object v0, v3

    .line 2178878
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2178879
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Etz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2178880
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2178881
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 9

    .prologue
    .line 2178882
    check-cast p2, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;

    .line 2178883
    iget-object v0, p0, LX/Etz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;

    iget-object v1, p2, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    const/4 v6, 0x0

    .line 2178884
    sget-object v2, LX/0ax;->bE:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2178885
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2178886
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v5

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V

    .line 2178887
    iget-object v3, v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v8, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2178888
    return-void
.end method

.method private b(Landroid/view/View;LX/1X1;)V
    .locals 8

    .prologue
    .line 2178889
    check-cast p2, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;

    .line 2178890
    iget-object v0, p0, LX/Etz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;

    iget-object v1, p2, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2178891
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/Eu1;

    invoke-direct {v7, v0, v1}, LX/Eu1;-><init>(Lcom/facebook/friending/center/components/FriendItemComponentSpec;Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)V

    move-object v4, p1

    invoke-static/range {v2 .. v7}, LX/Ey3;->a(Landroid/content/Context;LX/17W;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;LX/Eu0;)V

    .line 2178892
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2178893
    check-cast p2, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;

    .line 2178894
    iget-object v0, p0, LX/Etz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;

    iget-object v1, p2, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    iget-object v2, p2, Lcom/facebook/friending/center/components/FriendItemComponent$FriendItemComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    const/4 p2, 0x2

    .line 2178895
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0207fa

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    .line 2178896
    const v4, 0x13f8bf4d

    const/4 p0, 0x0

    invoke-static {p1, v4, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2178897
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->b:LX/EuG;

    invoke-virtual {v4, p1}, LX/EuG;->c(LX/1De;)LX/EuF;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/EuF;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/EuF;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/EuF;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/EuF;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->c:LX/EuD;

    invoke-virtual {v4, p1}, LX/EuD;->c(LX/1De;)LX/EuB;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/EuB;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/EuB;

    move-result-object v4

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, LX/EuB;->a(Z)LX/EuB;

    move-result-object v4

    sget-object p0, LX/2h7;->FRIENDS_CENTER_FRIENDS:LX/2h7;

    invoke-virtual {v4, p0}, LX/EuB;->a(LX/2h7;)LX/EuB;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    .line 2178898
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, p0, :cond_1

    .line 2178899
    iget-object v3, v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Eu4;

    invoke-virtual {v3, p1}, LX/Eu4;->c(LX/1De;)LX/Eu2;

    move-result-object v3

    const p0, 0x7f080f76

    invoke-virtual {v3, p0}, LX/Eu2;->h(I)LX/Eu2;

    move-result-object v3

    const p0, 0x7f080f9d

    invoke-virtual {v3, p0}, LX/Eu2;->j(I)LX/Eu2;

    move-result-object v3

    const p0, 0x7f020b7e

    invoke-virtual {v3, p0}, LX/Eu2;->k(I)LX/Eu2;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const p0, 0x7f0b08a7

    invoke-interface {v3, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    .line 2178900
    const p0, 0x13f8cace

    const/4 v0, 0x0

    invoke-static {p1, p0, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2178901
    invoke-interface {v3, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    const p0, 0x7f0b08a6

    invoke-interface {v3, p2, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2178902
    :cond_0
    :goto_0
    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2178903
    return-object v0

    .line 2178904
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, p0, :cond_2

    .line 2178905
    iget-object v3, v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Eto;

    invoke-virtual {v3, p1}, LX/Eto;->c(LX/1De;)LX/Etn;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/Etn;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/Etn;

    move-result-object v3

    sget-object p0, LX/2h7;->FRIENDS_CENTER_FRIENDS:LX/2h7;

    invoke-virtual {v3, p0}, LX/Etn;->a(LX/2h7;)LX/Etn;

    move-result-object v3

    const/4 p0, 0x1

    invoke-virtual {v3, p0}, LX/Etn;->a(Z)LX/Etn;

    move-result-object v3

    const p0, 0x7f0b004e

    invoke-virtual {v3, p0}, LX/Etn;->h(I)LX/Etn;

    move-result-object v3

    const p0, 0x7f0b08a6

    invoke-virtual {v3, p0}, LX/Etn;->i(I)LX/Etn;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const p0, 0x7f0b08a7

    invoke-interface {v3, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto :goto_0

    .line 2178906
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, p0, :cond_0

    .line 2178907
    iget-object v3, v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Ets;

    invoke-virtual {v3, p1}, LX/Ets;->c(LX/1De;)LX/Etq;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/Etq;->a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/Etq;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2178908
    invoke-static {}, LX/1dS;->b()V

    .line 2178909
    iget v0, p1, LX/1dQ;->b:I

    .line 2178910
    sparse-switch v0, :sswitch_data_0

    .line 2178911
    :goto_0
    return-object v2

    .line 2178912
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2178913
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/Etz;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 2178914
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2178915
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/Etz;->b(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x13f8bf4d -> :sswitch_0
        0x13f8cace -> :sswitch_1
    .end sparse-switch
.end method
