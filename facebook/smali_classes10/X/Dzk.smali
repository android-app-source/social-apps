.class public final LX/Dzk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 0

    .prologue
    .line 2067141
    iput-object p1, p0, LX/Dzk;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2067142
    iget-object v0, p0, LX/Dzk;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-static {v0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->A(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067143
    iget-object v0, p0, LX/Dzk;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->d:LX/E07;

    new-instance v1, LX/Dzm;

    iget-object v2, p0, LX/Dzk;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {v1, v2}, LX/Dzm;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v0, p1, v1}, LX/E07;->a(Ljava/lang/Throwable;LX/Dzm;)V

    .line 2067144
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2067145
    check-cast p1, Ljava/lang/Long;

    .line 2067146
    iget-object v0, p0, LX/Dzk;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v1, p0, LX/Dzk;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v1, v1, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/96B;->c(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;J)V

    .line 2067147
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2067148
    const-string v1, "extra_place"

    new-instance v2, LX/5m9;

    invoke-direct {v2}, LX/5m9;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2067149
    iput-object v3, v2, LX/5m9;->f:Ljava/lang/String;

    .line 2067150
    move-object v2, v2

    .line 2067151
    iget-object v3, p0, LX/Dzk;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-static {v3}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)Lcom/facebook/places/create/PlaceCreationState;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/places/create/PlaceCreationState;->a:Ljava/lang/String;

    .line 2067152
    iput-object v3, v2, LX/5m9;->h:Ljava/lang/String;

    .line 2067153
    move-object v2, v2

    .line 2067154
    invoke-virtual {v2}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2067155
    iget-object v1, p0, LX/Dzk;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2067156
    iget-object v0, p0, LX/Dzk;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2067157
    return-void
.end method
