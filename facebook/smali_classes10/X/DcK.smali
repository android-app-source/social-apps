.class public final LX/DcK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/Dc9;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/Dc9;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 2018209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2018210
    iput-object p1, p0, LX/DcK;->a:LX/0QB;

    .line 2018211
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2018212
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/DcK;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2018213
    packed-switch p2, :pswitch_data_0

    .line 2018214
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2018215
    :pswitch_0
    new-instance v1, LX/DcA;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v1, v0}, LX/DcA;-><init>(Lcom/facebook/content/SecureContextHelper;)V

    .line 2018216
    move-object v0, v1

    .line 2018217
    :goto_0
    return-object v0

    .line 2018218
    :pswitch_1
    new-instance v1, LX/DcH;

    invoke-static {p1}, LX/Dc5;->a(LX/0QB;)LX/Dc5;

    move-result-object v0

    check-cast v0, LX/Dc5;

    invoke-direct {v1, v0}, LX/DcH;-><init>(LX/Dc5;)V

    .line 2018219
    move-object v0, v1

    .line 2018220
    goto :goto_0

    .line 2018221
    :pswitch_2
    new-instance p2, LX/DcI;

    invoke-static {p1}, LX/Dc5;->a(LX/0QB;)LX/Dc5;

    move-result-object v0

    check-cast v0, LX/Dc5;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object p0

    check-cast p0, LX/17Y;

    invoke-direct {p2, v0, v1, p0}, LX/DcI;-><init>(LX/Dc5;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 2018222
    move-object v0, p2

    .line 2018223
    goto :goto_0

    .line 2018224
    :pswitch_3
    new-instance p2, LX/DcJ;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p1}, LX/Dc5;->a(LX/0QB;)LX/Dc5;

    move-result-object v1

    check-cast v1, LX/Dc5;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p2, v0, v1, p0}, LX/DcJ;-><init>(LX/03V;LX/Dc5;Lcom/facebook/content/SecureContextHelper;)V

    .line 2018225
    move-object v0, p2

    .line 2018226
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2018227
    const/4 v0, 0x4

    return v0
.end method
