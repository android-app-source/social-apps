.class public final LX/E7b;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;)V
    .locals 0

    .prologue
    .line 2081816
    iput-object p1, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    .line 2081817
    iget-object v0, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v0, v0, LX/E6s;->a:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    .line 2081818
    iget-object v1, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v1, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->j:LX/E7X;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    if-le v0, v1, :cond_0

    .line 2081819
    iget-object v1, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    .line 2081820
    iget-boolean v2, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->e:Z

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->h:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 2081821
    :cond_0
    :goto_0
    if-lez p2, :cond_1

    .line 2081822
    iget-object v1, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v2, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v2, v2, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->l:Ljava/lang/String;

    iget-object v3, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v3, v3, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->m:Ljava/lang/String;

    .line 2081823
    invoke-virtual {v1, v2, v3}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2081824
    :cond_1
    iget-object v1, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v2, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v2, v2, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->l:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    .line 2081825
    invoke-virtual {v1, v2, v0}, LX/Cfk;->a(Ljava/lang/String;I)V

    .line 2081826
    iget-object v0, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v1, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v1, v1, LX/E6s;->a:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    .line 2081827
    iput v1, v0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->g:I

    .line 2081828
    iget-object v0, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v0, v0, LX/E6s;->a:LX/1P1;

    iget-object v1, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget v1, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->g:I

    invoke-virtual {v0, v1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2081829
    iget-object v1, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v2, p0, LX/E7b;->a:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    .line 2081830
    iget-object v3, v2, LX/E6s;->c:Landroid/support/v7/widget/RecyclerView;

    move-object v2, v3

    .line 2081831
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    .line 2081832
    iput v0, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->f:I

    .line 2081833
    return-void

    .line 2081834
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->e:Z

    .line 2081835
    iget-object v2, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->h:Ljava/lang/String;

    iget-object v3, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->l:Ljava/lang/String;

    const/4 p1, 0x5

    sget-object p3, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3, p1, p3}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method
