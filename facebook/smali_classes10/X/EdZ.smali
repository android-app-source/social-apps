.class public final LX/EdZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public final synthetic b:LX/Edc;

.field private c:LX/Eda;

.field private d:LX/Eda;


# direct methods
.method public constructor <init>(LX/Edc;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2149580
    iput-object p1, p0, LX/EdZ;->b:LX/Edc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2149581
    iput-object v0, p0, LX/EdZ;->c:LX/Eda;

    .line 2149582
    iput-object v0, p0, LX/EdZ;->d:LX/Eda;

    .line 2149583
    const/4 v0, 0x0

    iput v0, p0, LX/EdZ;->a:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2149603
    iget-object v0, p0, LX/EdZ;->d:LX/Eda;

    if-eqz v0, :cond_0

    .line 2149604
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "BUG: Invalid newbuf() before copy()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149605
    :cond_0
    new-instance v0, LX/Eda;

    invoke-direct {v0}, LX/Eda;-><init>()V

    .line 2149606
    iget-object v1, p0, LX/EdZ;->b:LX/Edc;

    iget-object v1, v1, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    iput-object v1, v0, LX/Eda;->a:Ljava/io/ByteArrayOutputStream;

    .line 2149607
    iget-object v1, p0, LX/EdZ;->b:LX/Edc;

    iget v1, v1, LX/Edc;->b:I

    iput v1, v0, LX/Eda;->b:I

    .line 2149608
    iget-object v1, p0, LX/EdZ;->c:LX/Eda;

    iput-object v1, v0, LX/Eda;->c:LX/Eda;

    .line 2149609
    iput-object v0, p0, LX/EdZ;->c:LX/Eda;

    .line 2149610
    iget v0, p0, LX/EdZ;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EdZ;->a:I

    .line 2149611
    iget-object v0, p0, LX/EdZ;->b:LX/Edc;

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, v0, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    .line 2149612
    iget-object v0, p0, LX/EdZ;->b:LX/Edc;

    iput v2, v0, LX/Edc;->b:I

    .line 2149613
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2149593
    iget-object v0, p0, LX/EdZ;->b:LX/Edc;

    iget-object v0, v0, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    .line 2149594
    iget-object v1, p0, LX/EdZ;->b:LX/Edc;

    iget v1, v1, LX/Edc;->b:I

    .line 2149595
    iget-object v2, p0, LX/EdZ;->b:LX/Edc;

    iget-object v3, p0, LX/EdZ;->c:LX/Eda;

    iget-object v3, v3, LX/Eda;->a:Ljava/io/ByteArrayOutputStream;

    iput-object v3, v2, LX/Edc;->a:Ljava/io/ByteArrayOutputStream;

    .line 2149596
    iget-object v2, p0, LX/EdZ;->b:LX/Edc;

    iget-object v3, p0, LX/EdZ;->c:LX/Eda;

    iget v3, v3, LX/Eda;->b:I

    iput v3, v2, LX/Edc;->b:I

    .line 2149597
    iget-object v2, p0, LX/EdZ;->c:LX/Eda;

    iput-object v2, p0, LX/EdZ;->d:LX/Eda;

    .line 2149598
    iget-object v2, p0, LX/EdZ;->c:LX/Eda;

    iget-object v2, v2, LX/Eda;->c:LX/Eda;

    iput-object v2, p0, LX/EdZ;->c:LX/Eda;

    .line 2149599
    iget v2, p0, LX/EdZ;->a:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/EdZ;->a:I

    .line 2149600
    iget-object v2, p0, LX/EdZ;->d:LX/Eda;

    iput-object v0, v2, LX/Eda;->a:Ljava/io/ByteArrayOutputStream;

    .line 2149601
    iget-object v0, p0, LX/EdZ;->d:LX/Eda;

    iput v1, v0, LX/Eda;->b:I

    .line 2149602
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2149590
    iget-object v0, p0, LX/EdZ;->b:LX/Edc;

    iget-object v1, p0, LX/EdZ;->d:LX/Eda;

    iget-object v1, v1, LX/Eda;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, LX/EdZ;->d:LX/Eda;

    iget v3, v3, LX/Eda;->b:I

    invoke-virtual {v0, v1, v2, v3}, LX/Edc;->a([BII)V

    .line 2149591
    const/4 v0, 0x0

    iput-object v0, p0, LX/EdZ;->d:LX/Eda;

    .line 2149592
    return-void
.end method

.method public final d()LX/Edb;
    .locals 3

    .prologue
    .line 2149584
    new-instance v0, LX/Edb;

    iget-object v1, p0, LX/EdZ;->b:LX/Edc;

    invoke-direct {v0, v1}, LX/Edb;-><init>(LX/Edc;)V

    .line 2149585
    iget-object v1, p0, LX/EdZ;->b:LX/Edc;

    iget v1, v1, LX/Edc;->b:I

    .line 2149586
    iput v1, v0, LX/Edb;->b:I

    .line 2149587
    iget v1, p0, LX/EdZ;->a:I

    .line 2149588
    iput v1, v0, LX/Edb;->c:I

    .line 2149589
    return-object v0
.end method
