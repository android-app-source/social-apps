.class public final LX/E0O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/home/HomeActivity;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/home/HomeActivity;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2068249
    iput-object p1, p0, LX/E0O;->a:Lcom/facebook/places/create/home/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2068250
    iput-object p2, p0, LX/E0O;->b:Landroid/view/View;

    .line 2068251
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2068252
    iget-object v0, p0, LX/E0O;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    .line 2068253
    const v1, 0x7f0d162b

    if-ne v0, v1, :cond_1

    .line 2068254
    iget-object v0, p0, LX/E0O;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    .line 2068255
    :cond_0
    :goto_0
    return-void

    .line 2068256
    :cond_1
    const v1, 0x7f0d1631

    if-ne v0, v1, :cond_2

    .line 2068257
    iget-object v0, p0, LX/E0O;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->c:Ljava/lang/String;

    goto :goto_0

    .line 2068258
    :cond_2
    const v1, 0x7f0d1630

    if-ne v0, v1, :cond_0

    .line 2068259
    iget-object v0, p0, LX/E0O;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2068260
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2068261
    return-void
.end method
