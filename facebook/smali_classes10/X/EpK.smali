.class public LX/EpK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EpB;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/EpB",
        "<",
        "LX/5wQ;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/EpK;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EpH;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EpG;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/Epa;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Fqp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2170209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170210
    return-void
.end method

.method public static a(LX/0QB;)LX/EpK;
    .locals 7

    .prologue
    .line 2170194
    sget-object v0, LX/EpK;->e:LX/EpK;

    if-nez v0, :cond_1

    .line 2170195
    const-class v1, LX/EpK;

    monitor-enter v1

    .line 2170196
    :try_start_0
    sget-object v0, LX/EpK;->e:LX/EpK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2170197
    if-eqz v2, :cond_0

    .line 2170198
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2170199
    new-instance v5, LX/EpK;

    invoke-direct {v5}, LX/EpK;-><init>()V

    .line 2170200
    const/16 v3, 0x1adc

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v3, 0x1adb

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const-class v3, LX/Epa;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Epa;

    const-class v4, LX/Fqp;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Fqp;

    .line 2170201
    iput-object v6, v5, LX/EpK;->a:LX/0Or;

    iput-object p0, v5, LX/EpK;->b:LX/0Or;

    iput-object v3, v5, LX/EpK;->c:LX/Epa;

    iput-object v4, v5, LX/EpK;->d:LX/Fqp;

    .line 2170202
    move-object v0, v5

    .line 2170203
    sput-object v0, LX/EpK;->e:LX/EpK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2170204
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2170205
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2170206
    :cond_1
    sget-object v0, LX/EpK;->e:LX/EpK;

    return-object v0

    .line 2170207
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2170208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/5wQ;Landroid/view/View;ILX/2h7;LX/5P2;LX/EpL;)V
    .locals 11
    .param p3    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param

    .prologue
    .line 2170174
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2170175
    iget-object v0, p0, LX/EpK;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EpG;

    move-object v1, p1

    move v2, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    .line 2170176
    invoke-virtual/range {v0 .. v6}, LX/EpG;->a(LX/5wN;ILandroid/content/Context;LX/2h7;LX/5P2;LX/EpL;)Z

    move-result v0

    .line 2170177
    if-eqz v0, :cond_1

    .line 2170178
    :cond_0
    :goto_0
    return-void

    .line 2170179
    :cond_1
    iget-object v0, p0, LX/EpK;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EpH;

    .line 2170180
    invoke-virtual {v0, p1, p3, v3}, LX/EpH;->a(LX/5wO;ILandroid/content/Context;)Z

    move-result v0

    .line 2170181
    if-nez v0, :cond_0

    .line 2170182
    invoke-static {p1, p3, v3}, LX/EpC;->a(LX/5wL;ILandroid/content/Context;)Z

    move-result v0

    .line 2170183
    if-nez v0, :cond_0

    .line 2170184
    packed-switch p3, :pswitch_data_0

    .line 2170185
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown item type for PersonCardActionBarPresenter.handlePersonActionBarItem "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2170186
    :pswitch_0
    invoke-interface {p1}, LX/5wQ;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 2170187
    :goto_1
    if-eqz v1, :cond_3

    invoke-interface {p1}, LX/5wQ;->j()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    .line 2170188
    :goto_2
    iget-object v2, p0, LX/EpK;->d:LX/Fqp;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, p2, v1, v0, v4}, LX/Fqp;->a(Landroid/view/View;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)LX/Fqo;

    move-result-object v0

    .line 2170189
    new-instance v4, LX/EpJ;

    move-object v5, p0

    move-object v6, p4

    move-object/from16 v7, p5

    move-object v8, v3

    move-object v9, p1

    move-object/from16 v10, p6

    invoke-direct/range {v4 .. v10}, LX/EpJ;-><init>(LX/EpK;LX/2h7;LX/5P2;Landroid/content/Context;LX/5wQ;LX/EpL;)V

    invoke-virtual {v0, v4}, LX/Fqo;->a(LX/EpI;)V

    .line 2170190
    invoke-virtual {v0}, LX/Fqo;->a()V

    .line 2170191
    invoke-virtual {v0}, LX/Fqo;->e()V

    goto :goto_0

    .line 2170192
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    .line 2170193
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Landroid/view/View;ILX/2h7;LX/5P2;LX/EpL;)V
    .locals 7
    .param p3    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param

    .prologue
    .line 2170173
    move-object v1, p1

    check-cast v1, LX/5wQ;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LX/EpK;->a(LX/5wQ;Landroid/view/View;ILX/2h7;LX/5P2;LX/EpL;)V

    return-void
.end method
