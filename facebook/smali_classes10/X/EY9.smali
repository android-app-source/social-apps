.class public final LX/EY9;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EY8;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EY9;",
        ">;",
        "LX/EY8;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:Ljava/lang/Object;

.field public c:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2136495
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2136496
    const-string v0, ""

    iput-object v0, p0, LX/EY9;->b:Ljava/lang/Object;

    .line 2136497
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2136498
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2136499
    const-string v0, ""

    iput-object v0, p0, LX/EY9;->b:Ljava/lang/Object;

    .line 2136500
    return-void
.end method

.method private d(LX/EWY;)LX/EY9;
    .locals 1

    .prologue
    .line 2136501
    instance-of v0, p1, LX/EYA;

    if-eqz v0, :cond_0

    .line 2136502
    check-cast p1, LX/EYA;

    invoke-virtual {p0, p1}, LX/EY9;->a(LX/EYA;)LX/EY9;

    move-result-object p0

    .line 2136503
    :goto_0
    return-object p0

    .line 2136504
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EY9;
    .locals 4

    .prologue
    .line 2136505
    const/4 v2, 0x0

    .line 2136506
    :try_start_0
    sget-object v0, LX/EYA;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYA;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2136507
    if-eqz v0, :cond_0

    .line 2136508
    invoke-virtual {p0, v0}, LX/EY9;->a(LX/EYA;)LX/EY9;

    .line 2136509
    :cond_0
    return-object p0

    .line 2136510
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2136511
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2136512
    check-cast v0, LX/EYA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2136513
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2136514
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2136515
    invoke-virtual {p0, v1}, LX/EY9;->a(LX/EYA;)LX/EY9;

    :cond_1
    throw v0

    .line 2136516
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static m()LX/EY9;
    .locals 1

    .prologue
    .line 2136517
    new-instance v0, LX/EY9;

    invoke-direct {v0}, LX/EY9;-><init>()V

    return-object v0
.end method

.method private n()LX/EY9;
    .locals 2

    .prologue
    .line 2136518
    invoke-static {}, LX/EY9;->m()LX/EY9;

    move-result-object v0

    invoke-direct {p0}, LX/EY9;->x()LX/EYA;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EY9;->a(LX/EYA;)LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method private w()LX/EYA;
    .locals 2

    .prologue
    .line 2136519
    invoke-direct {p0}, LX/EY9;->x()LX/EYA;

    move-result-object v0

    .line 2136520
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2136521
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2136522
    :cond_0
    return-object v0
.end method

.method private x()LX/EYA;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2136523
    new-instance v2, LX/EYA;

    invoke-direct {v2, p0}, LX/EYA;-><init>(LX/EWj;)V

    .line 2136524
    iget v3, p0, LX/EY9;->a:I

    .line 2136525
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2136526
    :goto_0
    iget-object v1, p0, LX/EY9;->b:Ljava/lang/Object;

    .line 2136527
    iput-object v1, v2, LX/EYA;->namePart_:Ljava/lang/Object;

    .line 2136528
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2136529
    or-int/lit8 v0, v0, 0x2

    .line 2136530
    :cond_0
    iget-boolean v1, p0, LX/EY9;->c:Z

    .line 2136531
    iput-boolean v1, v2, LX/EYA;->isExtension_:Z

    .line 2136532
    iput v0, v2, LX/EYA;->bitField0_:I

    .line 2136533
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2136534
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2136535
    invoke-direct {p0, p1}, LX/EY9;->d(LX/EWY;)LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2136536
    invoke-direct {p0, p1, p2}, LX/EY9;->d(LX/EWd;LX/EYZ;)LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EYA;)LX/EY9;
    .locals 2

    .prologue
    .line 2136468
    sget-object v0, LX/EYA;->c:LX/EYA;

    move-object v0, v0

    .line 2136469
    if-ne p1, v0, :cond_0

    .line 2136470
    :goto_0
    return-object p0

    .line 2136471
    :cond_0
    invoke-virtual {p1}, LX/EYA;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2136472
    iget v0, p0, LX/EY9;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EY9;->a:I

    .line 2136473
    iget-object v0, p1, LX/EYA;->namePart_:Ljava/lang/Object;

    iput-object v0, p0, LX/EY9;->b:Ljava/lang/Object;

    .line 2136474
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136475
    :cond_1
    invoke-virtual {p1}, LX/EYA;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2136476
    iget-boolean v0, p1, LX/EYA;->isExtension_:Z

    move v0, v0

    .line 2136477
    iget v1, p0, LX/EY9;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, LX/EY9;->a:I

    .line 2136478
    iput-boolean v0, p0, LX/EY9;->c:Z

    .line 2136479
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136480
    :cond_2
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2136537
    const/4 v1, 0x1

    .line 2136538
    iget v2, p0, LX/EY9;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_2

    :goto_0
    move v1, v1

    .line 2136539
    if-nez v1, :cond_1

    .line 2136540
    :cond_0
    :goto_1
    return v0

    .line 2136541
    :cond_1
    iget v1, p0, LX/EY9;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2136542
    if-eqz v1, :cond_0

    .line 2136543
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2136494
    invoke-direct {p0, p1, p2}, LX/EY9;->d(LX/EWd;LX/EYZ;)LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2136544
    invoke-direct {p0}, LX/EY9;->n()LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2136493
    invoke-direct {p0, p1, p2}, LX/EY9;->d(LX/EWd;LX/EYZ;)LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2136492
    invoke-direct {p0}, LX/EY9;->n()LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2136491
    invoke-direct {p0, p1}, LX/EY9;->d(LX/EWY;)LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2136490
    invoke-direct {p0}, LX/EY9;->n()LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2136489
    sget-object v0, LX/EYC;->J:LX/EYn;

    const-class v1, LX/EYA;

    const-class v2, LX/EY9;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2136488
    sget-object v0, LX/EYC;->I:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2136487
    invoke-direct {p0}, LX/EY9;->n()LX/EY9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2136486
    invoke-direct {p0}, LX/EY9;->x()LX/EYA;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2136485
    invoke-direct {p0}, LX/EY9;->w()LX/EYA;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2136484
    invoke-direct {p0}, LX/EY9;->x()LX/EYA;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2136483
    invoke-direct {p0}, LX/EY9;->w()LX/EYA;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2136481
    sget-object v0, LX/EYA;->c:LX/EYA;

    move-object v0, v0

    .line 2136482
    return-object v0
.end method
