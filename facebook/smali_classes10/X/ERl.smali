.class public final enum LX/ERl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ERl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ERl;

.field public static final enum AFTER:LX/ERl;

.field public static final enum BEFORE:LX/ERl;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2121393
    new-instance v0, LX/ERl;

    const-string v1, "BEFORE"

    invoke-direct {v0, v1, v2}, LX/ERl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ERl;->BEFORE:LX/ERl;

    .line 2121394
    new-instance v0, LX/ERl;

    const-string v1, "AFTER"

    invoke-direct {v0, v1, v3}, LX/ERl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ERl;->AFTER:LX/ERl;

    .line 2121395
    const/4 v0, 0x2

    new-array v0, v0, [LX/ERl;

    sget-object v1, LX/ERl;->BEFORE:LX/ERl;

    aput-object v1, v0, v2

    sget-object v1, LX/ERl;->AFTER:LX/ERl;

    aput-object v1, v0, v3

    sput-object v0, LX/ERl;->$VALUES:[LX/ERl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2121390
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ERl;
    .locals 1

    .prologue
    .line 2121392
    const-class v0, LX/ERl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ERl;

    return-object v0
.end method

.method public static values()[LX/ERl;
    .locals 1

    .prologue
    .line 2121391
    sget-object v0, LX/ERl;->$VALUES:[LX/ERl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ERl;

    return-object v0
.end method
