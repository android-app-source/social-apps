.class public LX/Cxs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pf;
.implements LX/CxV;


# instance fields
.field private final a:LX/1Pz;

.field private final b:LX/1Q0;

.field private final c:LX/1QL;

.field private final d:LX/1Q2;

.field private final e:LX/1Q3;

.field private final f:LX/1Q4;

.field private final g:LX/1QM;

.field private final h:LX/1QP;

.field private final i:LX/1Q7;

.field private final j:LX/1QQ;

.field private final k:LX/1QB;

.field private final l:LX/1QR;

.field private final m:LX/1QD;

.field private final n:LX/1QS;

.field private final o:LX/1QF;

.field private final p:LX/1QG;

.field private final q:LX/99Q;

.field private final r:LX/CxW;

.field private final s:LX/1PU;

.field private final t:LX/1QK;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/1PY;LX/1Pz;LX/1Q0;LX/1Q1;LX/1Q2;LX/1Q3;LX/1Q4;LX/1Q5;LX/1Q6;LX/1Q7;LX/1QA;LX/1QB;LX/1QC;LX/1QD;LX/1QE;LX/1QF;LX/1QG;LX/99Q;LX/CxX;LX/1QI;LX/1QK;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952172
    iput-object p7, p0, LX/Cxs;->a:LX/1Pz;

    .line 1952173
    iput-object p8, p0, LX/Cxs;->b:LX/1Q0;

    .line 1952174
    invoke-virtual {p9, p0}, LX/1Q1;->a(LX/1Po;)LX/1QL;

    move-result-object v1

    iput-object v1, p0, LX/Cxs;->c:LX/1QL;

    .line 1952175
    iput-object p10, p0, LX/Cxs;->d:LX/1Q2;

    .line 1952176
    iput-object p11, p0, LX/Cxs;->e:LX/1Q3;

    .line 1952177
    iput-object p12, p0, LX/Cxs;->f:LX/1Q4;

    .line 1952178
    invoke-virtual {p13, p1}, LX/1Q5;->a(Ljava/lang/String;)LX/1QM;

    move-result-object v1

    iput-object v1, p0, LX/Cxs;->g:LX/1QM;

    .line 1952179
    invoke-static {p2}, LX/1Q6;->a(Landroid/content/Context;)LX/1QP;

    move-result-object v1

    iput-object v1, p0, LX/Cxs;->h:LX/1QP;

    .line 1952180
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Cxs;->i:LX/1Q7;

    .line 1952181
    invoke-static {p3}, LX/1QA;->a(LX/1PT;)LX/1QQ;

    move-result-object v1

    iput-object v1, p0, LX/Cxs;->j:LX/1QQ;

    .line 1952182
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Cxs;->k:LX/1QB;

    .line 1952183
    invoke-static {p4}, LX/1QC;->a(Ljava/lang/Runnable;)LX/1QR;

    move-result-object v1

    iput-object v1, p0, LX/Cxs;->l:LX/1QR;

    .line 1952184
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Cxs;->m:LX/1QD;

    .line 1952185
    move-object/from16 v0, p20

    invoke-virtual {v0, p0}, LX/1QE;->a(LX/1Pf;)LX/1QS;

    move-result-object v1

    iput-object v1, p0, LX/Cxs;->n:LX/1QS;

    .line 1952186
    move-object/from16 v0, p21

    iput-object v0, p0, LX/Cxs;->o:LX/1QF;

    .line 1952187
    move-object/from16 v0, p22

    iput-object v0, p0, LX/Cxs;->p:LX/1QG;

    .line 1952188
    move-object/from16 v0, p23

    iput-object v0, p0, LX/Cxs;->q:LX/99Q;

    .line 1952189
    invoke-static {p5}, LX/CxX;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)LX/CxW;

    move-result-object v1

    iput-object v1, p0, LX/Cxs;->r:LX/CxW;

    .line 1952190
    move-object/from16 v0, p25

    invoke-virtual {v0, p6}, LX/1QI;->a(LX/1PY;)LX/1PU;

    move-result-object v1

    iput-object v1, p0, LX/Cxs;->s:LX/1PU;

    .line 1952191
    move-object/from16 v0, p26

    iput-object v0, p0, LX/Cxs;->t:LX/1QK;

    .line 1952192
    return-void
.end method


# virtual methods
.method public final a()LX/1Q9;
    .locals 1

    .prologue
    .line 1952193
    iget-object v0, p0, LX/Cxs;->i:LX/1Q7;

    invoke-virtual {v0}, LX/1Q7;->a()LX/1Q9;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;
    .locals 6

    .prologue
    .line 1952194
    iget-object v0, p0, LX/Cxs;->b:LX/1Q0;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Q0;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1952195
    iget-object v0, p0, LX/Cxs;->o:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1952196
    iget-object v0, p0, LX/Cxs;->o:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1R6;)V
    .locals 1

    .prologue
    .line 1952197
    iget-object v0, p0, LX/Cxs;->l:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a(LX/1R6;)V

    .line 1952198
    return-void
.end method

.method public final a(LX/1Rb;)V
    .locals 1

    .prologue
    .line 1952199
    iget-object v0, p0, LX/Cxs;->q:LX/99Q;

    invoke-virtual {v0, p1}, LX/99Q;->a(LX/1Rb;)V

    .line 1952200
    return-void
.end method

.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1952201
    iget-object v0, p0, LX/Cxs;->k:LX/1QB;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1QB;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1952202
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1952203
    iget-object v0, p0, LX/Cxs;->q:LX/99Q;

    invoke-virtual {v0, p1, p2}, LX/99Q;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1952204
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1952205
    iget-object v0, p0, LX/Cxs;->q:LX/99Q;

    invoke-virtual {v0, p1, p2, p3}, LX/99Q;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1952206
    return-void
.end method

.method public final a(LX/22C;)V
    .locals 1

    .prologue
    .line 1952207
    iget-object v0, p0, LX/Cxs;->f:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->a(LX/22C;)V

    .line 1952208
    return-void
.end method

.method public final a(LX/34p;)V
    .locals 1

    .prologue
    .line 1952209
    iget-object v0, p0, LX/Cxs;->f:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->a(LX/34p;)V

    .line 1952210
    return-void
.end method

.method public final a(LX/5Oj;)V
    .locals 1

    .prologue
    .line 1952213
    iget-object v0, p0, LX/Cxs;->s:LX/1PU;

    invoke-virtual {v0, p1}, LX/1PU;->a(LX/5Oj;)V

    .line 1952214
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1952211
    iget-object v0, p0, LX/Cxs;->p:LX/1QG;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1QG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1952212
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1952232
    iget-object v0, p0, LX/Cxs;->c:LX/1QL;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1QL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1952233
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 1952230
    iget-object v0, p0, LX/Cxs;->d:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1952231
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1952228
    iget-object v0, p0, LX/Cxs;->d:LX/1Q2;

    invoke-virtual {v0, p1, p2}, LX/1Q2;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V

    .line 1952229
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1952226
    iget-object v0, p0, LX/Cxs;->k:LX/1QB;

    invoke-virtual {v0, p1}, LX/1QB;->a(Ljava/lang/String;)V

    .line 1952227
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1952224
    iget-object v0, p0, LX/Cxs;->a:LX/1Pz;

    invoke-virtual {v0, p1, p2}, LX/1Pz;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1952225
    return-void
.end method

.method public final a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 1952234
    iget-object v0, p0, LX/Cxs;->l:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1952235
    return-void
.end method

.method public final a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1952222
    iget-object v0, p0, LX/Cxs;->l:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Ljava/lang/Object;)V

    .line 1952223
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 1952221
    iget-object v0, p0, LX/Cxs;->o:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/1R6;)V
    .locals 1

    .prologue
    .line 1952219
    iget-object v0, p0, LX/Cxs;->l:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->b(LX/1R6;)V

    .line 1952220
    return-void
.end method

.method public final b(LX/22C;)V
    .locals 1

    .prologue
    .line 1952217
    iget-object v0, p0, LX/Cxs;->f:LX/1Q4;

    invoke-virtual {v0, p1}, LX/1Q4;->b(LX/22C;)V

    .line 1952218
    return-void
.end method

.method public final b(LX/5Oj;)V
    .locals 1

    .prologue
    .line 1952215
    iget-object v0, p0, LX/Cxs;->s:LX/1PU;

    invoke-virtual {v0, p1}, LX/1PU;->b(LX/5Oj;)V

    .line 1952216
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 1952167
    iget-object v0, p0, LX/Cxs;->d:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1952168
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1952169
    iget-object v0, p0, LX/Cxs;->o:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->b(Ljava/lang/String;)V

    .line 1952170
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1952136
    iget-object v0, p0, LX/Cxs;->a:LX/1Pz;

    invoke-virtual {v0, p1, p2}, LX/1Pz;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1952137
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1952138
    iget-object v0, p0, LX/Cxs;->m:LX/1QD;

    invoke-virtual {v0, p1}, LX/1QD;->b(Z)V

    .line 1952139
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 1952140
    iget-object v0, p0, LX/Cxs;->j:LX/1QQ;

    invoke-virtual {v0}, LX/1QQ;->c()LX/1PT;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 1952141
    iget-object v0, p0, LX/Cxs;->d:LX/1Q2;

    invoke-virtual {v0, p1}, LX/1Q2;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 1952142
    iget-object v0, p0, LX/Cxs;->n:LX/1QS;

    invoke-virtual {v0}, LX/1QS;->e()LX/1SX;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 1952143
    iget-object v0, p0, LX/Cxs;->p:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsModule()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "analyticsModule"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 1952144
    iget-object v0, p0, LX/Cxs;->g:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getAnalyticsModule()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1952145
    iget-object v0, p0, LX/Cxs;->h:LX/1QP;

    invoke-virtual {v0}, LX/1QP;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getFontFoundry()LX/1QO;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "fontFoundry"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 1952146
    iget-object v0, p0, LX/Cxs;->g:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getFontFoundry()LX/1QO;

    move-result-object v0

    return-object v0
.end method

.method public getNavigator()LX/5KM;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "navigator"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 1952147
    iget-object v0, p0, LX/Cxs;->g:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getNavigator()LX/5KM;

    move-result-object v0

    return-object v0
.end method

.method public getTraitCollection()LX/1QN;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "traitCollection"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 1952148
    iget-object v0, p0, LX/Cxs;->g:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getTraitCollection()LX/1QN;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 1952149
    iget-object v0, p0, LX/Cxs;->p:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1952135
    iget-object v0, p0, LX/Cxs;->p:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->i()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 1952151
    iget-object v0, p0, LX/Cxs;->p:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 1952152
    iget-object v0, p0, LX/Cxs;->l:LX/1QR;

    invoke-virtual {v0}, LX/1QR;->iN_()V

    .line 1952153
    return-void
.end method

.method public final iO_()Z
    .locals 1

    .prologue
    .line 1952154
    iget-object v0, p0, LX/Cxs;->m:LX/1QD;

    invoke-virtual {v0}, LX/1QD;->iO_()Z

    move-result v0

    return v0
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1952155
    iget-object v0, p0, LX/Cxs;->p:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->j()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1952156
    iget-object v0, p0, LX/Cxs;->p:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->k()V

    .line 1952157
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1952158
    iget-object v0, p0, LX/Cxs;->q:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->l()Z

    move-result v0

    return v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 1952159
    iget-object v0, p0, LX/Cxs;->q:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->m()LX/1f9;

    move-result-object v0

    return-object v0
.end method

.method public final m_(Z)V
    .locals 1

    .prologue
    .line 1952160
    iget-object v0, p0, LX/Cxs;->l:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->m_(Z)V

    .line 1952161
    return-void
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 1952162
    iget-object v0, p0, LX/Cxs;->q:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->o()LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1952163
    iget-object v0, p0, LX/Cxs;->q:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->p()V

    .line 1952164
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 1952165
    iget-object v0, p0, LX/Cxs;->q:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->q()Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1952166
    iget-object v0, p0, LX/Cxs;->t:LX/1QK;

    invoke-virtual {v0}, LX/1QK;->r()Z

    move-result v0

    return v0
.end method

.method public final t()Lcom/facebook/search/results/model/SearchResultsMutableContext;
    .locals 1

    .prologue
    .line 1952150
    iget-object v0, p0, LX/Cxs;->r:LX/CxW;

    invoke-virtual {v0}, LX/CxW;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    return-object v0
.end method
