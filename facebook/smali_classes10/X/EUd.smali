.class public final LX/EUd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeBadgeQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0hn;


# direct methods
.method public constructor <init>(LX/0hn;)V
    .locals 0

    .prologue
    .line 2126263
    iput-object p1, p0, LX/EUd;->a:LX/0hn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2126261
    sget-object v0, LX/0hn;->d:Ljava/lang/String;

    const-string v1, "Video Home badge count query failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2126262
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2126264
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2126265
    if-eqz p1, :cond_0

    .line 2126266
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2126267
    if-nez v0, :cond_2

    .line 2126268
    :cond_0
    sget-object v0, LX/0hn;->d:Ljava/lang/String;

    const-string v1, "Video Home badge count query succeeded but result was null."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2126269
    :cond_1
    :goto_0
    return-void

    .line 2126270
    :cond_2
    iget-object v0, p0, LX/EUd;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xB;

    sget-object v1, LX/12j;->VIDEO_HOME:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    .line 2126271
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2126272
    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeBadgeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeBadgeQueryModel;->j()I

    move-result v1

    .line 2126273
    new-instance v0, LX/EUb;

    invoke-direct {v0}, LX/EUb;-><init>()V

    .line 2126274
    iput v1, v0, LX/EUb;->a:I

    .line 2126275
    move-object v2, v0

    .line 2126276
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2126277
    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeBadgeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeBadgeQueryModel;->k()I

    move-result v0

    .line 2126278
    iput v0, v2, LX/EUb;->b:I

    .line 2126279
    move-object v2, v2

    .line 2126280
    iget-object v3, p0, LX/EUd;->a:LX/0hn;

    .line 2126281
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2126282
    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeBadgeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeBadgeQueryModel;->a()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, LX/0hn;->a(J)J

    move-result-wide v4

    .line 2126283
    iput-wide v4, v2, LX/EUb;->d:J

    .line 2126284
    move-object v0, v2

    .line 2126285
    invoke-virtual {v0}, LX/EUb;->a()LX/EUc;

    move-result-object v2

    .line 2126286
    iget-object v0, p0, LX/EUd;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AW;

    iget-object v3, p0, LX/EUd;->a:LX/0hn;

    iget-object v3, v3, LX/0hn;->w:LX/095;

    invoke-virtual {v0, v3, v1}, LX/3AW;->b(LX/095;I)V

    .line 2126287
    iget-object v1, p0, LX/EUd;->a:LX/0hn;

    .line 2126288
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2126289
    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeBadgeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomeBadgeQueryModel;->k()I

    move-result v0

    .line 2126290
    iput v0, v1, LX/0hn;->v:I

    .line 2126291
    iget-object v0, p0, LX/EUd;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v1, LX/1vy;->PREFETCH_CONTROLLER:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2126292
    iget-object v0, p0, LX/EUd;->a:LX/0hn;

    .line 2126293
    iget-wide v6, v2, LX/EUc;->d:J

    move-wide v2, v6

    .line 2126294
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_3

    .line 2126295
    sget-object v6, LX/095;->PERIODIC:LX/095;

    invoke-static {v0, v6}, LX/0hn;->a(LX/0hn;LX/095;)Lcom/facebook/common/async/CancellableRunnable;

    move-result-object v6

    iput-object v6, v0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    .line 2126296
    iget-object v6, v0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    if-eqz v6, :cond_3

    .line 2126297
    iget-object v6, v0, LX/0hn;->i:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Sh;

    iget-object v7, v0, LX/0hn;->a:Lcom/facebook/common/async/CancellableRunnable;

    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v2

    invoke-virtual {v6, v7, v8, v9}, LX/0Sh;->b(Ljava/lang/Runnable;J)V

    .line 2126298
    :cond_3
    goto/16 :goto_0
.end method
