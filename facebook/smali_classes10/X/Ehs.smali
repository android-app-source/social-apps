.class public final LX/Ehs;
.super LX/440;
.source ""


# instance fields
.field public b:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:I


# direct methods
.method public constructor <init>(LX/18b;Landroid/net/NetworkInfo;)V
    .locals 1
    .param p2    # Landroid/net/NetworkInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2158852
    invoke-direct {p0, p1}, LX/440;-><init>(LX/18b;)V

    .line 2158853
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    :goto_0
    iput v0, p0, LX/Ehs;->c:I

    .line 2158854
    return-void

    .line 2158855
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/telephony/SignalStrength;)V
    .locals 2

    .prologue
    .line 2158857
    invoke-super {p0, p1}, LX/440;->a(Landroid/telephony/SignalStrength;)V

    .line 2158858
    iget-object v0, p0, LX/Ehs;->b:Landroid/util/Pair;

    if-nez v0, :cond_0

    .line 2158859
    iget-object v0, p0, LX/440;->a:LX/18b;

    iget v1, p0, LX/Ehs;->c:I

    invoke-virtual {v0, v1, p1}, LX/18b;->a(ILandroid/telephony/SignalStrength;)Landroid/util/Pair;

    move-result-object v0

    iput-object v0, p0, LX/Ehs;->b:Landroid/util/Pair;

    .line 2158860
    :cond_0
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2158856
    iget v0, p0, LX/Ehs;->c:I

    invoke-static {v0}, LX/0km;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
