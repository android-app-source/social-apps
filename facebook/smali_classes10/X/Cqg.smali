.class public final LX/Cqg;
.super LX/Cqf;
.source ""


# instance fields
.field public final synthetic g:LX/Cql;


# direct methods
.method public constructor <init>(LX/Cql;LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V
    .locals 7

    .prologue
    .line 1940335
    iput-object p1, p0, LX/Cqg;->g:LX/Cql;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    return-void
.end method


# virtual methods
.method public final g()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 1940317
    invoke-virtual {p0}, LX/Cqf;->k()F

    move-result v0

    .line 1940318
    invoke-virtual {p0}, LX/Cqf;->l()F

    move-result v4

    .line 1940319
    sub-float v1, v4, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v6, v1

    sget-wide v8, LX/CoL;->n:D

    cmpg-double v1, v6, v8

    if-gtz v1, :cond_1

    .line 1940320
    iget-object v1, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v1, v1

    .line 1940321
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 1940322
    iget-object v2, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v2, v2

    .line 1940323
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 1940324
    cmpl-float v0, v4, v0

    if-ltz v0, :cond_0

    .line 1940325
    iget-object v0, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v0, v0

    .line 1940326
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 1940327
    int-to-float v0, v2

    mul-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1940328
    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    move v10, v2

    move v2, v1

    move v1, v10

    .line 1940329
    :goto_0
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-direct {v4, v2, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1940330
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/CrW;

    invoke-direct {v1, v4}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v1}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940331
    :goto_1
    return-void

    .line 1940332
    :cond_0
    int-to-float v0, v1

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1940333
    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    move v10, v1

    move v1, v0

    move v0, v10

    move v11, v3

    move v3, v2

    move v2, v11

    goto :goto_0

    .line 1940334
    :cond_1
    invoke-super {p0}, LX/Cqf;->g()V

    goto :goto_1
.end method
