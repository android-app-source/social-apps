.class public LX/EFy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EFv;


# instance fields
.field public a:LX/EFw;

.field public b:LX/EG5;


# direct methods
.method public constructor <init>(LX/EFw;LX/EG5;)V
    .locals 0
    .param p1    # LX/EFw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/EG5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2096697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096698
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096699
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096700
    iput-object p1, p0, LX/EFy;->a:LX/EFw;

    .line 2096701
    iput-object p2, p0, LX/EFy;->b:LX/EG5;

    .line 2096702
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 2096703
    iget-object v0, p0, LX/EFy;->a:LX/EFw;

    iget-object v1, p0, LX/EFy;->b:LX/EG5;

    invoke-virtual {v0, v1, p1}, LX/EFw;->a(LX/EG5;Z)V

    .line 2096704
    return-void
.end method

.method public final a(JLandroid/view/View;)Z
    .locals 1

    .prologue
    .line 2096696
    iget-object v0, p0, LX/EFy;->a:LX/EFw;

    invoke-virtual {v0, p1, p2, p3}, LX/EFw;->a(JLandroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public final b(JLandroid/view/View;)V
    .locals 5
    .param p3    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2096682
    iget-object v0, p0, LX/EFy;->a:LX/EFw;

    iget-object v1, p0, LX/EFy;->b:LX/EG5;

    const/4 v3, 0x0

    .line 2096683
    iget-boolean v2, v0, LX/EFw;->d:Z

    if-eqz v2, :cond_1

    .line 2096684
    sget-object v2, LX/EFw;->a:Ljava/lang/String;

    const-string v3, "Unable to set Renderer window because conference call has ended"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096685
    :cond_0
    :goto_0
    return-void

    .line 2096686
    :cond_1
    const-string v2, "setRendererWindow"

    invoke-virtual {v0, v1, v2}, LX/EFw;->a(LX/EG5;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2096687
    invoke-virtual {v0, p1, p2, p3}, LX/EFw;->a(JLandroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2096688
    if-eqz p3, :cond_3

    .line 2096689
    const v2, 0x7f0d002e

    invoke-virtual {p3, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    .line 2096690
    if-eqz v2, :cond_2

    .line 2096691
    iget-object v4, v0, LX/EFw;->j:Ljava/util/Map;

    check-cast v2, Ljava/lang/Long;

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096692
    :cond_2
    const v2, 0x7f0d002e

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3, v2, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2096693
    :cond_3
    iget-object v4, v0, LX/EFw;->j:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    if-nez p3, :cond_4

    move-object v2, v3

    :goto_1
    invoke-interface {v4, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096694
    iget-object v2, v0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v2, p1, p2, p3}, Lcom/facebook/webrtc/ConferenceCall;->setRendererWindow(JLandroid/view/View;)V

    goto :goto_0

    .line 2096695
    :cond_4
    invoke-virtual {p3}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1
.end method
