.class public final LX/Dab;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2015706
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2015707
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2015708
    :goto_0
    return v1

    .line 2015709
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2015710
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2015711
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2015712
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2015713
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2015714
    const-string v3, "photo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2015715
    const/4 v2, 0x0

    .line 2015716
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2015717
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2015718
    :goto_2
    move v0, v2

    .line 2015719
    goto :goto_1

    .line 2015720
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2015721
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2015722
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2015723
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2015724
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 2015725
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2015726
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2015727
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 2015728
    const-string v4, "image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2015729
    const/4 v3, 0x0

    .line 2015730
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_b

    .line 2015731
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2015732
    :goto_4
    move v0, v3

    .line 2015733
    goto :goto_3

    .line 2015734
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2015735
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2015736
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    .line 2015737
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2015738
    :cond_9
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_a

    .line 2015739
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2015740
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2015741
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_9

    if-eqz v4, :cond_9

    .line 2015742
    const-string v5, "uri"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2015743
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 2015744
    :cond_a
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2015745
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2015746
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_4

    :cond_b
    move v0, v3

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2015747
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2015748
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2015749
    if-eqz v0, :cond_2

    .line 2015750
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2015751
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2015752
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2015753
    if-eqz v1, :cond_1

    .line 2015754
    const-string p1, "image"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2015755
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2015756
    const/4 p1, 0x0

    invoke-virtual {p0, v1, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 2015757
    if-eqz p1, :cond_0

    .line 2015758
    const-string v0, "uri"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2015759
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2015760
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2015761
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2015762
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2015763
    return-void
.end method
