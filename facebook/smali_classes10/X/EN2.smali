.class public final LX/EN2;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EN3;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Landroid/view/View$OnClickListener;

.field public f:Landroid/view/View$OnClickListener;

.field public final synthetic g:LX/EN3;


# direct methods
.method public constructor <init>(LX/EN3;)V
    .locals 1

    .prologue
    .line 2111788
    iput-object p1, p0, LX/EN2;->g:LX/EN3;

    .line 2111789
    move-object v0, p1

    .line 2111790
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2111791
    sget v0, LX/EN4;->a:I

    iput v0, p0, LX/EN2;->c:I

    .line 2111792
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2111793
    const-string v0, "SearchResultsTitleSeeMoreComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2111794
    if-ne p0, p1, :cond_1

    .line 2111795
    :cond_0
    :goto_0
    return v0

    .line 2111796
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2111797
    goto :goto_0

    .line 2111798
    :cond_3
    check-cast p1, LX/EN2;

    .line 2111799
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2111800
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2111801
    if-eq v2, v3, :cond_0

    .line 2111802
    iget-object v2, p0, LX/EN2;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EN2;->a:Ljava/lang/String;

    iget-object v3, p1, LX/EN2;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2111803
    goto :goto_0

    .line 2111804
    :cond_5
    iget-object v2, p1, LX/EN2;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2111805
    :cond_6
    iget-object v2, p0, LX/EN2;->b:LX/1dc;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EN2;->b:LX/1dc;

    iget-object v3, p1, LX/EN2;->b:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2111806
    goto :goto_0

    .line 2111807
    :cond_8
    iget-object v2, p1, LX/EN2;->b:LX/1dc;

    if-nez v2, :cond_7

    .line 2111808
    :cond_9
    iget v2, p0, LX/EN2;->c:I

    iget v3, p1, LX/EN2;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2111809
    goto :goto_0

    .line 2111810
    :cond_a
    iget-object v2, p0, LX/EN2;->d:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/EN2;->d:Ljava/lang/String;

    iget-object v3, p1, LX/EN2;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 2111811
    goto :goto_0

    .line 2111812
    :cond_c
    iget-object v2, p1, LX/EN2;->d:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 2111813
    :cond_d
    iget-object v2, p0, LX/EN2;->e:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/EN2;->e:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/EN2;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 2111814
    goto :goto_0

    .line 2111815
    :cond_f
    iget-object v2, p1, LX/EN2;->e:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_e

    .line 2111816
    :cond_10
    iget-object v2, p0, LX/EN2;->f:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/EN2;->f:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/EN2;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2111817
    goto :goto_0

    .line 2111818
    :cond_11
    iget-object v2, p1, LX/EN2;->f:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
