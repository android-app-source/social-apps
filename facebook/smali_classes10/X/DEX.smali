.class public LX/DEX;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Lcom/facebook/fbui/facepile/FacepileView;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

.field public e:Z

.field public f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1977104
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 1977105
    const v0, 0x7f030839

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1977106
    const v0, 0x7f0d157c

    invoke-virtual {p0, v0}, LX/DEX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/DEX;->b:Landroid/widget/TextView;

    .line 1977107
    const v0, 0x7f0d157d

    invoke-virtual {p0, v0}, LX/DEX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/DEX;->c:Landroid/widget/TextView;

    .line 1977108
    const v0, 0x7f0d157e

    invoke-virtual {p0, v0}, LX/DEX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    iput-object v0, p0, LX/DEX;->d:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    .line 1977109
    const v0, 0x7f0d1579

    invoke-virtual {p0, v0}, LX/DEX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/DEX;->a:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1977110
    iget-object v0, p0, LX/DEX;->d:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    invoke-virtual {p0}, LX/DEX;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a0046

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->setDividerColor(I)V

    .line 1977111
    iget-object v0, p0, LX/DEX;->d:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    invoke-virtual {p0}, LX/DEX;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0b0033

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->setDividerThicknessPx(I)V

    .line 1977112
    iget-object v0, p0, LX/DEX;->d:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    invoke-virtual {p0}, LX/DEX;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0b0f55

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->setDividerMarginPx(I)V

    .line 1977113
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 1977100
    if-eqz p2, :cond_0

    invoke-virtual {p0}, LX/DEX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1, p2, v0}, LX/DES;->a(ILjava/lang/String;Landroid/content/res/Resources;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 1977101
    :goto_0
    iget-object v1, p0, LX/DEX;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1977102
    return-void

    .line 1977103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0Px;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1977097
    iget-object v0, p0, LX/DEX;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceCountForOverflow(I)V

    .line 1977098
    iget-object v0, p0, LX/DEX;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1977099
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1977096
    iget-boolean v0, p0, LX/DEX;->e:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7d58507b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1977114
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1977115
    const/4 v1, 0x1

    .line 1977116
    iput-boolean v1, p0, LX/DEX;->e:Z

    .line 1977117
    const/16 v1, 0x2d

    const v2, 0x641d24f5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x57a5e55c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1977092
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1977093
    const/4 v1, 0x0

    .line 1977094
    iput-boolean v1, p0, LX/DEX;->e:Z

    .line 1977095
    const/16 v1, 0x2d

    const v2, 0x77c194a1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setButtonOnclickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1977086
    iget-object v0, p0, LX/DEX;->d:Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;

    invoke-virtual {v0, p1}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1977087
    return-void
.end method

.method public setFaces(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1977090
    iget-object v0, p0, LX/DEX;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1977091
    return-void
.end method

.method public setSuggestGroupName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1977088
    iget-object v0, p0, LX/DEX;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1977089
    return-void
.end method
