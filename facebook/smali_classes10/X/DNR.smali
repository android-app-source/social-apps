.class public LX/DNR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:Z

.field public B:Z

.field private C:LX/0So;

.field public D:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private E:J

.field private F:Z

.field public b:LX/2lS;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0fz;

.field public final d:LX/0kb;

.field private final e:LX/0Xw;

.field public final f:LX/DZ9;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Sg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:LX/88n;

.field private final j:LX/0Sh;

.field private final k:LX/0ad;

.field public final l:LX/DKg;

.field public final m:LX/DKg;

.field private final n:LX/DNP;

.field public final o:LX/16I;

.field public final p:LX/2hU;

.field public final q:Landroid/app/Activity;

.field public r:LX/DNT;

.field private s:J

.field private t:I

.field public u:LX/DNQ;

.field public v:Z

.field public w:Z

.field private x:J

.field private y:I

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1991599
    const-class v0, LX/DNR;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DNR;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;LX/16I;LX/2hU;LX/0fz;LX/0So;LX/0Sh;LX/0kb;LX/0Xw;LX/DZ9;LX/0Ot;LX/0ad;LX/0Sg;LX/88n;)V
    .locals 4
    .param p10    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/16I;",
            "LX/2hU;",
            "LX/0fz;",
            "LX/0So;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0kb;",
            "LX/0Xw;",
            "Lcom/facebook/groups/perf/GroupsPerfLogger;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/88n;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1991651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1991652
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/DNR;->F:Z

    .line 1991653
    iput-object p4, p0, LX/DNR;->c:LX/0fz;

    .line 1991654
    iput-object p7, p0, LX/DNR;->d:LX/0kb;

    .line 1991655
    iput-object p8, p0, LX/DNR;->e:LX/0Xw;

    .line 1991656
    iput-object p9, p0, LX/DNR;->f:LX/DZ9;

    .line 1991657
    iput-object p2, p0, LX/DNR;->o:LX/16I;

    .line 1991658
    iput-object p3, p0, LX/DNR;->p:LX/2hU;

    .line 1991659
    iput-object p1, p0, LX/DNR;->q:Landroid/app/Activity;

    .line 1991660
    iput-object p5, p0, LX/DNR;->C:LX/0So;

    .line 1991661
    iput-object p6, p0, LX/DNR;->j:LX/0Sh;

    .line 1991662
    iput-object p10, p0, LX/DNR;->g:LX/0Ot;

    .line 1991663
    iput-object p11, p0, LX/DNR;->k:LX/0ad;

    .line 1991664
    iget-object v1, p0, LX/DNR;->k:LX/0ad;

    sget-short v2, LX/88j;->e:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1991665
    move-object/from16 v0, p12

    iput-object v0, p0, LX/DNR;->h:LX/0Sg;

    .line 1991666
    :goto_0
    move-object/from16 v0, p13

    iput-object v0, p0, LX/DNR;->i:LX/88n;

    .line 1991667
    new-instance v1, LX/DNK;

    invoke-direct {v1, p0}, LX/DNK;-><init>(LX/DNR;)V

    new-instance v2, LX/DNL;

    invoke-direct {v2, p0}, LX/DNL;-><init>(LX/DNR;)V

    sget-object v3, LX/88p;->INITIAL_FEED_OLDER:LX/88p;

    invoke-direct {p0, v1, v2, v3}, LX/DNR;->a(Ljava/util/concurrent/Callable;LX/0TF;LX/88p;)LX/DKg;

    move-result-object v1

    iput-object v1, p0, LX/DNR;->l:LX/DKg;

    .line 1991668
    new-instance v1, LX/DNM;

    invoke-direct {v1, p0}, LX/DNM;-><init>(LX/DNR;)V

    new-instance v2, LX/DNN;

    invoke-direct {v2, p0}, LX/DNN;-><init>(LX/DNR;)V

    sget-object v3, LX/88p;->INITIAL_FEED_NEWER:LX/88p;

    invoke-direct {p0, v1, v2, v3}, LX/DNR;->a(Ljava/util/concurrent/Callable;LX/0TF;LX/88p;)LX/DKg;

    move-result-object v1

    iput-object v1, p0, LX/DNR;->m:LX/DKg;

    .line 1991669
    new-instance v1, LX/DNP;

    invoke-direct {v1, p0}, LX/DNP;-><init>(LX/DNR;)V

    iput-object v1, p0, LX/DNR;->n:LX/DNP;

    .line 1991670
    return-void

    .line 1991671
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, LX/DNR;->h:LX/0Sg;

    goto :goto_0
.end method

.method private a(Ljava/util/concurrent/Callable;LX/0TF;LX/88p;)LX/DKg;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable;",
            "LX/0TF",
            "<",
            "LX/44w",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedHomeStories;",
            "LX/0ta;",
            ">;>;",
            "LX/88p;",
            ")",
            "LX/DKg;"
        }
    .end annotation

    .prologue
    .line 1991648
    iget-object v0, p0, LX/DNR;->k:LX/0ad;

    sget-short v1, LX/88j;->f:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1991649
    new-instance v0, LX/DKg;

    iget-object v3, p0, LX/DNR;->C:LX/0So;

    iget-object v4, p0, LX/DNR;->j:LX/0Sh;

    iget-object v5, p0, LX/DNR;->h:LX/0Sg;

    iget-object v6, p0, LX/DNR;->i:LX/88n;

    move-object v1, p1

    move-object v2, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, LX/DKg;-><init>(Ljava/util/concurrent/Callable;LX/0TF;LX/0So;LX/0Sh;LX/0Sg;LX/88n;LX/88p;)V

    .line 1991650
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/DKg;

    iget-object v3, p0, LX/DNR;->C:LX/0So;

    iget-object v4, p0, LX/DNR;->j:LX/0Sh;

    iget-object v5, p0, LX/DNR;->h:LX/0Sg;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/DKg;-><init>(Ljava/util/concurrent/Callable;LX/0TF;LX/0So;LX/0Sh;LX/0Sg;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/DNR;
    .locals 1

    .prologue
    .line 1991647
    invoke-static {p0}, LX/DNR;->b(LX/0QB;)LX/DNR;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Z
    .locals 1

    .prologue
    .line 1991643
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    if-eqz v0, :cond_0

    .line 1991644
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    .line 1991645
    invoke-static {v0}, LX/DNR;->a(Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;)Z

    move-result v0

    .line 1991646
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1991637
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1991638
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;->k()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v1

    .line 1991639
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1991640
    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    .line 1991641
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->m()Z

    move-result v0

    .line 1991642
    :cond_0
    return v0
.end method

.method public static a$redex0(LX/DNR;LX/44w;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/44w",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedHomeStories;",
            "LX/0ta;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1991627
    iget-object v0, p0, LX/DNR;->D:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/DNR;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1991628
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DNR;->F:Z

    .line 1991629
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1991630
    invoke-static {v0}, LX/DNR;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1991631
    const-string v2, "%020d:"

    iget-wide v4, p0, LX/DNR;->E:J

    iget-object v3, p0, LX/DNR;->D:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    int-to-long v6, v3

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1991632
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    goto :goto_0

    .line 1991633
    :cond_1
    invoke-direct {p0, v0}, LX/DNR;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v2

    .line 1991634
    if-eqz v2, :cond_0

    .line 1991635
    invoke-static {v0, v2}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    goto :goto_0

    .line 1991636
    :cond_2
    return-void
.end method

.method public static a$redex0(LX/DNR;LX/DKg;)V
    .locals 4

    .prologue
    .line 1991624
    iget v0, p0, LX/DNR;->y:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/DNR;->y:I

    iget v1, p0, LX/DNR;->t:I

    if-ge v0, v1, :cond_0

    .line 1991625
    iget-wide v0, p0, LX/DNR;->x:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/DNR;->x:J

    invoke-virtual {p1, v0, v1}, LX/DKg;->a(J)V

    .line 1991626
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/DNR;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1991613
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1991614
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 1991615
    instance-of v1, v0, Lcom/facebook/graphql/error/GraphQLError;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/facebook/graphql/error/GraphQLError;

    .line 1991616
    :goto_0
    if-eqz v0, :cond_0

    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    const v1, 0x198f03

    if-ne v0, v1, :cond_0

    .line 1991617
    invoke-direct {p0}, LX/DNR;->r()V

    .line 1991618
    iget-object v0, p0, LX/DNR;->r:LX/DNT;

    .line 1991619
    iget-object v1, v0, LX/DNT;->b:LX/0aG;

    const-string v2, "feed_clear_cache"

    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    const p1, -0x5d1253cd

    invoke-static {v1, v2, p0, p1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    .line 1991620
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    .line 1991621
    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    .line 1991622
    :cond_0
    return-void

    .line 1991623
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/DNR;
    .locals 14

    .prologue
    .line 1991607
    new-instance v0, LX/DNR;

    invoke-static {p0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-static {p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v2

    check-cast v2, LX/16I;

    invoke-static {p0}, LX/2hU;->b(LX/0QB;)LX/2hU;

    move-result-object v3

    check-cast v3, LX/2hU;

    invoke-static {p0}, LX/0fz;->b(LX/0QB;)LX/0fz;

    move-result-object v4

    check-cast v4, LX/0fz;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v7

    check-cast v7, LX/0kb;

    invoke-static {p0}, LX/29m;->a(LX/0QB;)LX/0Xw;

    move-result-object v8

    check-cast v8, LX/0Xw;

    .line 1991608
    new-instance v9, LX/DZ9;

    invoke-direct {v9}, LX/DZ9;-><init>()V

    .line 1991609
    move-object v9, v9

    .line 1991610
    move-object v9, v9

    .line 1991611
    check-cast v9, LX/DZ9;

    const/16 v10, 0x1430

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v12

    check-cast v12, LX/0Sg;

    invoke-static {p0}, LX/88n;->b(LX/0QB;)LX/88n;

    move-result-object v13

    check-cast v13, LX/88n;

    invoke-direct/range {v0 .. v13}, LX/DNR;-><init>(Landroid/app/Activity;LX/16I;LX/2hU;LX/0fz;LX/0So;LX/0Sh;LX/0kb;LX/0Xw;LX/DZ9;LX/0Ot;LX/0ad;LX/0Sg;LX/88n;)V

    .line 1991612
    return-object v0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1991600
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1991601
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/DNR;->D:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/DNR;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1991602
    iget-object v1, p0, LX/DNR;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1991603
    const-string v1, "%020d:"

    iget-wide v2, p0, LX/DNR;->E:J

    iget-object v4, p0, LX/DNR;->D:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int v0, v4, v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1991604
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1991605
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1991606
    :cond_1
    const-string v0, "%020d:"

    iget-wide v2, p0, LX/DNR;->E:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static b(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1991594
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1, p0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 1991595
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1991596
    invoke-static {v0}, LX/DNR;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1991597
    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1991598
    :cond_1
    return-object v1
.end method

.method public static p(LX/DNR;)V
    .locals 2

    .prologue
    .line 1991592
    iget-object v0, p0, LX/DNR;->u:LX/DNQ;

    iget-boolean v1, p0, LX/DNR;->z:Z

    invoke-interface {v0, v1}, LX/DNQ;->a(Z)V

    .line 1991593
    return-void
.end method

.method public static q(LX/DNR;)V
    .locals 2

    .prologue
    .line 1991672
    iget-wide v0, p0, LX/DNR;->s:J

    iput-wide v0, p0, LX/DNR;->x:J

    .line 1991673
    const/4 v0, 0x0

    iput v0, p0, LX/DNR;->y:I

    .line 1991674
    return-void
.end method

.method private r()V
    .locals 3

    .prologue
    .line 1991516
    iget-object v0, p0, LX/DNR;->m:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->b()V

    .line 1991517
    iget-object v0, p0, LX/DNR;->l:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->b()V

    .line 1991518
    iget-object v0, p0, LX/DNR;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/groups/feed/controller/GroupsFeedPager$5;

    invoke-direct {v1, p0}, Lcom/facebook/groups/feed/controller/GroupsFeedPager$5;-><init>(LX/DNR;)V

    const v2, -0x49f50244

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1991519
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DNR;->v:Z

    .line 1991520
    invoke-static {p0}, LX/DNR;->q(LX/DNR;)V

    .line 1991521
    return-void
.end method


# virtual methods
.method public final a(LX/DNT;LX/DNQ;JILX/2lS;Ljava/util/ArrayList;)V
    .locals 3
    .param p6    # LX/2lS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DNT;",
            "LX/DNQ;",
            "JI",
            "LX/2lS;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1991523
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DNT;

    iput-object v0, p0, LX/DNR;->r:LX/DNT;

    .line 1991524
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DNQ;

    iput-object v0, p0, LX/DNR;->u:LX/DNQ;

    .line 1991525
    iput-object p6, p0, LX/DNR;->b:LX/2lS;

    .line 1991526
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1991527
    iput-wide p3, p0, LX/DNR;->s:J

    .line 1991528
    iput p5, p0, LX/DNR;->t:I

    .line 1991529
    if-eqz p7, :cond_0

    .line 1991530
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/DNR;->D:Ljava/util/ArrayList;

    .line 1991531
    :cond_0
    iget-object v0, p0, LX/DNR;->C:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/DNR;->E:J

    .line 1991532
    return-void

    .line 1991533
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 1991534
    iput-boolean p1, p0, LX/DNR;->z:Z

    .line 1991535
    iget-boolean v0, p0, LX/DNR;->z:Z

    if-eqz v0, :cond_4

    .line 1991536
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1991537
    iget-object v0, p0, LX/DNR;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1991538
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    invoke-static {v1}, LX/DNR;->a(Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1991539
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1991540
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 1991541
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1991542
    iget-object p1, p0, LX/DNR;->D:Ljava/util/ArrayList;

    if-eqz p1, :cond_0

    iget-object p1, p0, LX/DNR;->D:Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1991543
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1991544
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1991545
    iget-object v2, p0, LX/DNR;->c:LX/0fz;

    invoke-virtual {v2, v0}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    goto :goto_1

    .line 1991546
    :cond_3
    invoke-virtual {p0}, LX/DNR;->b()V

    .line 1991547
    :cond_4
    iget-object v0, p0, LX/DNR;->m:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->a()Z

    .line 1991548
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1991522
    iget-object v0, p0, LX/DNR;->D:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNR;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1991549
    iget-boolean v0, p0, LX/DNR;->F:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/DNR;->z:Z

    if-eqz v0, :cond_1

    .line 1991550
    :cond_0
    iget-object v0, p0, LX/DNR;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-object v1, v0

    .line 1991551
    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Ljava/lang/String;)V

    goto :goto_0

    .line 1991552
    :cond_1
    iget-object v0, p0, LX/DNR;->D:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 1991553
    iget-object v0, p0, LX/DNR;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1991554
    :cond_2
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1991555
    iput-boolean v4, p0, LX/DNR;->v:Z

    .line 1991556
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DNR;->w:Z

    .line 1991557
    invoke-static {p0}, LX/DNR;->q(LX/DNR;)V

    .line 1991558
    iget-object v0, p0, LX/DNR;->e:LX/0Xw;

    iget-object v1, p0, LX/DNR;->n:LX/DNP;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 1991559
    iget-object v0, p0, LX/DNR;->m:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->a()Z

    .line 1991560
    iget-object v0, p0, LX/DNR;->l:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->a()Z

    .line 1991561
    iput-boolean v4, p0, LX/DNR;->w:Z

    .line 1991562
    return-void
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 1991563
    invoke-direct {p0}, LX/DNR;->r()V

    .line 1991564
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1991565
    iget-boolean v0, p0, LX/DNR;->v:Z

    if-nez v0, :cond_0

    .line 1991566
    iget-object v0, p0, LX/DNR;->l:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->a()Z

    .line 1991567
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1991568
    invoke-direct {p0}, LX/DNR;->r()V

    .line 1991569
    iget-object v0, p0, LX/DNR;->m:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->b()V

    .line 1991570
    iget-object v0, p0, LX/DNR;->l:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->b()V

    .line 1991571
    iget-object v0, p0, LX/DNR;->e:LX/0Xw;

    iget-object v1, p0, LX/DNR;->n:LX/DNP;

    invoke-virtual {v0, v1}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;)V

    .line 1991572
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1991573
    iget-object v0, p0, LX/DNR;->e:LX/0Xw;

    iget-object v1, p0, LX/DNR;->n:LX/DNP;

    invoke-virtual {v0, v1}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;)V

    .line 1991574
    return-void
.end method

.method public final j()V
    .locals 4

    .prologue
    .line 1991575
    iget-object v0, p0, LX/DNR;->e:LX/0Xw;

    iget-object v1, p0, LX/DNR;->n:LX/DNP;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 1991576
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 1991577
    invoke-direct {p0}, LX/DNR;->r()V

    .line 1991578
    iget-object v0, p0, LX/DNR;->m:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->d()V

    .line 1991579
    iget-object v0, p0, LX/DNR;->l:LX/DKg;

    invoke-virtual {v0}, LX/DKg;->d()V

    .line 1991580
    iget-object v0, p0, LX/DNR;->e:LX/0Xw;

    iget-object v1, p0, LX/DNR;->n:LX/DNP;

    invoke-virtual {v0, v1}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;)V

    .line 1991581
    return-void
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1991582
    iget-object v0, p0, LX/DNR;->m:LX/DKg;

    .line 1991583
    iget-boolean v1, v0, LX/DKg;->j:Z

    move v0, v1

    .line 1991584
    if-nez v0, :cond_0

    iget-object v0, p0, LX/DNR;->l:LX/DKg;

    .line 1991585
    iget-boolean v1, v0, LX/DKg;->j:Z

    move v0, v1

    .line 1991586
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1991587
    const-class v0, LX/DNR;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v1

    const-string v2, "loading"

    iget-object v0, p0, LX/DNR;->m:LX/DKg;

    .line 1991588
    iget-boolean v3, v0, LX/DKg;->j:Z

    move v0, v3

    .line 1991589
    if-nez v0, :cond_0

    iget-object v0, p0, LX/DNR;->l:LX/DKg;

    .line 1991590
    iget-boolean v3, v0, LX/DKg;->j:Z

    move v0, v3

    .line 1991591
    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "no"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "totalStories"

    iget-object v2, p0, LX/DNR;->c:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "freshStories"

    iget-object v2, p0, LX/DNR;->c:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->w()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "mHasReachedEndOfFeed"

    iget-boolean v2, p0, LX/DNR;->v:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "yes"

    goto :goto_0
.end method
