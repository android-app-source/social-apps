.class public LX/DNX;
.super LX/DNA;
.source ""


# instance fields
.field private f:Z

.field private g:Z

.field public h:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private i:LX/DRf;

.field public j:Ljava/lang/String;

.field private k:Lcom/facebook/feed/banner/GenericNotificationBanner;

.field private l:LX/DNU;

.field public m:LX/1Cw;

.field private n:LX/1Cw;

.field private o:LX/1Qq;


# direct methods
.method public constructor <init>(LX/0Or;LX/1B1;LX/0kb;LX/DCC;LX/DCE;LX/0bH;LX/DCI;LX/1LV;LX/DCL;LX/DCO;LX/DCQ;LX/1CY;LX/0Ot;LX/1Db;LX/0Ot;LX/0ad;LX/1Kt;LX/88l;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/groups/feed/controller/annotations/IsGroupsFeedVPVLoggingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1B1;",
            "LX/0kb;",
            "LX/DCC;",
            "LX/DCE;",
            "LX/0bH;",
            "LX/DCI;",
            "LX/1LV;",
            "LX/DCL;",
            "LX/DCO;",
            "LX/DCQ;",
            "LX/1CY;",
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;",
            "LX/1Db;",
            "LX/0Ot",
            "<",
            "LX/2cm;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            "LX/88l;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1991801
    invoke-direct/range {p0 .. p18}, LX/DNA;-><init>(LX/0Or;LX/1B1;LX/0kb;LX/DCC;LX/DCE;LX/0bH;LX/DCI;LX/1LV;LX/DCL;LX/DCO;LX/DCQ;LX/1CY;LX/0Ot;LX/1Db;LX/0Ot;LX/0ad;LX/1Kt;LX/88l;)V

    .line 1991802
    const/4 v0, 0x0

    iput-object v0, p0, LX/DNX;->j:Ljava/lang/String;

    .line 1991803
    return-void
.end method

.method public static a(LX/0QB;)LX/DNX;
    .locals 1

    .prologue
    .line 1991813
    invoke-static {p0}, LX/DNX;->b(LX/0QB;)LX/DNX;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DNX;
    .locals 21

    .prologue
    .line 1991814
    new-instance v2, LX/DNX;

    const/16 v3, 0x31d

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/1B1;->a(LX/0QB;)LX/1B1;

    move-result-object v4

    check-cast v4, LX/1B1;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/DCC;->a(LX/0QB;)LX/DCC;

    move-result-object v6

    check-cast v6, LX/DCC;

    invoke-static/range {p0 .. p0}, LX/DCE;->a(LX/0QB;)LX/DCE;

    move-result-object v7

    check-cast v7, LX/DCE;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v8

    check-cast v8, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/DCI;->a(LX/0QB;)LX/DCI;

    move-result-object v9

    check-cast v9, LX/DCI;

    invoke-static/range {p0 .. p0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v10

    check-cast v10, LX/1LV;

    invoke-static/range {p0 .. p0}, LX/DCL;->a(LX/0QB;)LX/DCL;

    move-result-object v11

    check-cast v11, LX/DCL;

    invoke-static/range {p0 .. p0}, LX/DCO;->a(LX/0QB;)LX/DCO;

    move-result-object v12

    check-cast v12, LX/DCO;

    invoke-static/range {p0 .. p0}, LX/DCQ;->a(LX/0QB;)LX/DCQ;

    move-result-object v13

    check-cast v13, LX/DCQ;

    invoke-static/range {p0 .. p0}, LX/1CY;->a(LX/0QB;)LX/1CY;

    move-result-object v14

    check-cast v14, LX/1CY;

    const/16 v15, 0x1a75

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v16

    check-cast v16, LX/1Db;

    const/16 v17, 0xf4d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v18

    check-cast v18, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/23N;->a(LX/0QB;)LX/23N;

    move-result-object v19

    check-cast v19, LX/1Kt;

    invoke-static/range {p0 .. p0}, LX/88l;->a(LX/0QB;)LX/88l;

    move-result-object v20

    check-cast v20, LX/88l;

    invoke-direct/range {v2 .. v20}, LX/DNX;-><init>(LX/0Or;LX/1B1;LX/0kb;LX/DCC;LX/DCE;LX/0bH;LX/DCI;LX/1LV;LX/DCL;LX/DCO;LX/DCQ;LX/1CY;LX/0Ot;LX/1Db;LX/0Ot;LX/0ad;LX/1Kt;LX/88l;)V

    .line 1991815
    return-object v2
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1991816
    iget-object v0, p0, LX/DNX;->o:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0fz;)V
    .locals 3

    .prologue
    .line 1991817
    iget-object v0, p0, LX/DNA;->e:LX/DNB;

    invoke-interface {v0}, LX/DNB;->d()LX/1Cv;

    move-result-object v0

    iput-object v0, p0, LX/DNX;->m:LX/1Cw;

    .line 1991818
    iget-object v0, p0, LX/DNA;->e:LX/DNB;

    iget-object v1, p0, LX/DNA;->e:LX/DNB;

    iget-object v2, p0, LX/DNA;->d:LX/0g8;

    invoke-interface {v1, v2}, LX/DNB;->a(LX/0g8;)LX/1Pf;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/DNB;->a(LX/0g1;LX/1Pf;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/DNX;->o:LX/1Qq;

    .line 1991819
    iget-boolean v0, p0, LX/DNX;->g:Z

    if-eqz v0, :cond_0

    .line 1991820
    iget-object v0, p0, LX/DNA;->e:LX/DNB;

    invoke-interface {v0}, LX/DNB;->e()LX/DRf;

    move-result-object v0

    iput-object v0, p0, LX/DNX;->i:LX/DRf;

    .line 1991821
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;ZLX/0fu;ZZ)V
    .locals 9

    .prologue
    .line 1991834
    move/from16 v0, p9

    iput-boolean v0, p0, LX/DNX;->g:Z

    .line 1991835
    iput-boolean p6, p0, LX/DNX;->f:Z

    .line 1991836
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0830e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/DNX;->j:Ljava/lang/String;

    .line 1991837
    const v1, 0x7f0d0595

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v1, p0, LX/DNX;->h:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 1991838
    const v1, 0x7f0d08bd

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/banner/GenericNotificationBanner;

    iput-object v1, p0, LX/DNX;->k:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 1991839
    invoke-virtual {p0}, LX/DNX;->s()V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p7

    move/from16 v8, p8

    .line 1991840
    invoke-super/range {v1 .. v8}, LX/DNA;->a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;LX/0fu;Z)V

    .line 1991841
    return-void
.end method

.method public final a(ZZ)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1991822
    iget-boolean v0, p0, LX/DNX;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNA;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1991823
    iget-object v0, p0, LX/DNA;->c:Landroid/view/View;

    const v3, 0x7f0d0d65

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1991824
    if-eqz v3, :cond_0

    .line 1991825
    iget-object v0, p0, LX/DNA;->c:Landroid/view/View;

    const v4, 0x7f0d01f9

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1991826
    iget-object v0, p0, LX/DNA;->c:Landroid/view/View;

    const v5, 0x7f0d01f8

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1991827
    if-eqz p2, :cond_1

    iget-object v0, p0, LX/DNA;->a:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1991828
    if-eqz p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1991829
    if-nez p1, :cond_3

    if-nez p2, :cond_3

    :goto_2
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1991830
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1991831
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1991832
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1991833
    goto :goto_2
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1991853
    iget-object v0, p0, LX/DNA;->c:Landroid/view/View;

    const v1, 0x7f0d01f9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1991854
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1991855
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v1, p0, LX/DNX;->d:LX/0g8;

    .line 1991856
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    invoke-interface {v0}, LX/0g8;->k()V

    .line 1991857
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->b(Z)V

    .line 1991858
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0g8;->d(Z)V

    .line 1991859
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    iget-object v1, p0, LX/DNA;->c:Landroid/view/View;

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->f(Landroid/view/View;)V

    .line 1991860
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 1991861
    if-nez p1, :cond_0

    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    invoke-interface {v0}, LX/0g8;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1991862
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/DNX;->a(ZZ)V

    .line 1991863
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1991842
    iget-object v0, p0, LX/DNX;->m:LX/1Cw;

    if-eqz v0, :cond_0

    .line 1991843
    iget-object v0, p0, LX/DNX;->m:LX/1Cw;

    iput-object v0, p0, LX/DNX;->n:LX/1Cw;

    .line 1991844
    :cond_0
    iget-boolean v0, p0, LX/DNX;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DNX;->i:LX/DRf;

    if-nez v0, :cond_1

    .line 1991845
    iget-object v0, p0, LX/DNA;->e:LX/DNB;

    invoke-interface {v0}, LX/DNB;->e()LX/DRf;

    move-result-object v0

    iput-object v0, p0, LX/DNX;->i:LX/DRf;

    .line 1991846
    :cond_1
    iget-object v0, p0, LX/DNX;->n:LX/1Cw;

    if-eqz v0, :cond_3

    .line 1991847
    iget-boolean v0, p0, LX/DNX;->g:Z

    if-nez v0, :cond_2

    .line 1991848
    new-instance v0, LX/DNV;

    iget-object v1, p0, LX/DNX;->n:LX/1Cw;

    iget-object v2, p0, LX/DNX;->o:LX/1Qq;

    invoke-direct {v0, v1, v2}, LX/DNV;-><init>(LX/1Cw;LX/1Cw;)V

    iput-object v0, p0, LX/DNX;->l:LX/DNU;

    .line 1991849
    :goto_0
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    iget-object v1, p0, LX/DNX;->l:LX/DNU;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 1991850
    :goto_1
    return-void

    .line 1991851
    :cond_2
    new-instance v0, LX/DNV;

    iget-object v1, p0, LX/DNX;->n:LX/1Cw;

    iget-object v2, p0, LX/DNX;->i:LX/DRf;

    invoke-direct {v0, v1, v2}, LX/DNV;-><init>(LX/1Cw;LX/1Cw;)V

    iput-object v0, p0, LX/DNX;->l:LX/DNU;

    goto :goto_0

    .line 1991852
    :cond_3
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    iget-object v1, p0, LX/DNX;->o:LX/1Qq;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    goto :goto_1
.end method

.method public final e(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 1991804
    invoke-virtual {p0}, LX/DNX;->n()V

    .line 1991805
    return-void
.end method

.method public final e(Z)V
    .locals 3

    .prologue
    .line 1991806
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/DNX;->i:LX/DRf;

    if-nez v0, :cond_0

    .line 1991807
    iget-object v0, p0, LX/DNA;->e:LX/DNB;

    invoke-interface {v0}, LX/DNB;->e()LX/DRf;

    move-result-object v0

    iput-object v0, p0, LX/DNX;->i:LX/DRf;

    .line 1991808
    :cond_0
    iget-object v0, p0, LX/DNX;->n:LX/1Cw;

    if-eqz v0, :cond_1

    .line 1991809
    if-nez p1, :cond_2

    .line 1991810
    iget-object v0, p0, LX/DNX;->l:LX/DNU;

    iget-object v1, p0, LX/DNX;->n:LX/1Cw;

    iget-object v2, p0, LX/DNX;->o:LX/1Qq;

    invoke-virtual {v0, v1, v2}, LX/DNU;->a(LX/1Cw;LX/1Cw;)V

    .line 1991811
    :cond_1
    :goto_0
    return-void

    .line 1991812
    :cond_2
    iget-object v0, p0, LX/DNX;->l:LX/DNU;

    iget-object v1, p0, LX/DNX;->n:LX/1Cw;

    iget-object v2, p0, LX/DNX;->i:LX/DRf;

    invoke-virtual {v0, v1, v2}, LX/DNU;->a(LX/1Cw;LX/1Cw;)V

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1991769
    iget-object v0, p0, LX/DNX;->n:LX/1Cw;

    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1991770
    iget-object v0, p0, LX/DNX;->o:LX/1Qq;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 1991771
    iget-object v0, p0, LX/DNX;->o:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1991772
    iget-object v0, p0, LX/DNX;->o:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 1991773
    iget-object v0, p0, LX/DNX;->i:LX/DRf;

    if-eqz v0, :cond_0

    .line 1991774
    iget-object v0, p0, LX/DNX;->i:LX/DRf;

    const v1, -0x76644264

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1991775
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1991776
    invoke-super {p0}, LX/DNA;->n()V

    .line 1991777
    iget-object v0, p0, LX/DNX;->l:LX/DNU;

    if-eqz v0, :cond_0

    .line 1991778
    iget-object v0, p0, LX/DNX;->l:LX/DNU;

    invoke-virtual {v0}, LX/DNU;->a()V

    .line 1991779
    :cond_0
    iget-object v0, p0, LX/DNX;->h:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_1

    .line 1991780
    iget-object v0, p0, LX/DNX;->h:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 1991781
    iput-object v1, p0, LX/DNX;->h:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 1991782
    :cond_1
    return-void
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 1991783
    iget-object v0, p0, LX/DNX;->h:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, LX/DNW;

    invoke-direct {v1, p0}, LX/DNW;-><init>(LX/DNX;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 1991784
    return-void
.end method

.method public final q()V
    .locals 2

    .prologue
    .line 1991785
    iget-object v0, p0, LX/DNX;->h:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1991786
    if-eqz v0, :cond_0

    .line 1991787
    iget-object v0, p0, LX/DNX;->h:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 1991788
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1991789
    iget-object v0, p0, LX/DNX;->n:LX/1Cw;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()V
    .locals 3

    .prologue
    .line 1991790
    iget-object v0, p0, LX/DNA;->e:LX/DNB;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNX;->k:Lcom/facebook/feed/banner/GenericNotificationBanner;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNA;->e:LX/DNB;

    iget-object v1, p0, LX/DNA;->a:LX/0kb;

    iget-object v2, p0, LX/DNX;->k:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-interface {v0, v1, v2}, LX/DNB;->a(LX/0kb;Lcom/facebook/feed/banner/GenericNotificationBanner;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1991791
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    if-eqz v0, :cond_0

    .line 1991792
    iget-object v0, p0, LX/DNA;->a:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    .line 1991793
    if-eqz v0, :cond_1

    .line 1991794
    iget-object v0, p0, LX/DNX;->k:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {v0}, LX/4nk;->a()V

    .line 1991795
    :cond_0
    :goto_0
    return-void

    .line 1991796
    :cond_1
    iget-object v0, p0, LX/DNX;->k:Lcom/facebook/feed/banner/GenericNotificationBanner;

    sget-object v1, LX/DBa;->NO_CONNECTION:LX/DBa;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    goto :goto_0
.end method

.method public final u()Lcom/facebook/widget/recyclerview/BetterRecyclerView;
    .locals 1

    .prologue
    .line 1991797
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    check-cast v0, LX/0g7;

    .line 1991798
    iget-object p0, v0, LX/0g7;->c:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, p0

    .line 1991799
    return-object v0
.end method

.method public final w_(I)I
    .locals 1

    .prologue
    .line 1991800
    iget-object v0, p0, LX/DNX;->o:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->h_(I)I

    move-result v0

    return v0
.end method
