.class public LX/EHn;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:LX/3qV;

.field public b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2100412
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2100413
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x57516cdc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2100414
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 2100415
    iget-object v1, p0, LX/EHn;->a:LX/3qV;

    if-nez v1, :cond_0

    .line 2100416
    new-instance v1, LX/3qV;

    invoke-direct {v1, p0}, LX/3qV;-><init>(Landroid/view/ViewGroup;)V

    iput-object v1, p0, LX/EHn;->a:LX/3qV;

    .line 2100417
    iget-object v1, p0, LX/EHn;->a:LX/3qV;

    iget-object v2, p0, LX/EHn;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, LX/3p4;->a(Landroid/os/Bundle;)V

    .line 2100418
    const/4 v1, 0x0

    iput-object v1, p0, LX/EHn;->b:Landroid/os/Bundle;

    .line 2100419
    iget-object v1, p0, LX/EHn;->a:LX/3qV;

    invoke-virtual {v1}, LX/3p4;->a()V

    .line 2100420
    :cond_0
    iget-object v1, p0, LX/EHn;->a:LX/3qV;

    invoke-virtual {v1}, LX/3p4;->c()V

    .line 2100421
    const/16 v1, 0x2d

    const v2, 0x55bdcfc6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x36914e64

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2100422
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 2100423
    iget-object v1, p0, LX/EHn;->a:LX/3qV;

    invoke-virtual {v1}, LX/3p4;->d()V

    .line 2100424
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, LX/EHn;->b:Landroid/os/Bundle;

    .line 2100425
    iget-object v1, p0, LX/EHn;->a:LX/3qV;

    iget-object v2, p0, LX/EHn;->b:Landroid/os/Bundle;

    .line 2100426
    iget-object v4, v1, LX/3p4;->b:LX/0jz;

    invoke-virtual {v4}, LX/0jz;->k()Landroid/os/Parcelable;

    move-result-object v4

    .line 2100427
    if-eqz v4, :cond_0

    .line 2100428
    const-string v5, "android:support:fragments"

    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2100429
    :cond_0
    iget-object v1, p0, LX/EHn;->a:LX/3qV;

    invoke-virtual {v1}, LX/3p4;->f()V

    .line 2100430
    const/4 v1, 0x0

    iput-object v1, p0, LX/EHn;->a:LX/3qV;

    .line 2100431
    const/16 v1, 0x2d

    const v2, -0x1a45be63

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
