.class public final LX/EXR;
.super LX/EX1;
.source ""

# interfaces
.implements LX/EXN;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EX1",
        "<",
        "LX/EXR;",
        ">;",
        "LX/EXN;"
    }
.end annotation


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXR;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EXR;


# instance fields
.field public bitField0_:I

.field public ctype_:LX/EXQ;

.field public deprecated_:Z

.field public experimentalMapKey_:Ljava/lang/Object;

.field public lazy_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public packed_:Z

.field public uninterpretedOption_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field private final unknownFields:LX/EZQ;

.field public weak_:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2133563
    new-instance v0, LX/EXM;

    invoke-direct {v0}, LX/EXM;-><init>()V

    sput-object v0, LX/EXR;->a:LX/EWZ;

    .line 2133564
    new-instance v0, LX/EXR;

    invoke-direct {v0}, LX/EXR;-><init>()V

    .line 2133565
    sput-object v0, LX/EXR;->c:LX/EXR;

    invoke-direct {v0}, LX/EXR;->D()V

    .line 2133566
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2133558
    invoke-direct {p0}, LX/EX1;-><init>()V

    .line 2133559
    iput-byte v0, p0, LX/EXR;->memoizedIsInitialized:B

    .line 2133560
    iput v0, p0, LX/EXR;->memoizedSerializedSize:I

    .line 2133561
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2133562
    iput-object v0, p0, LX/EXR;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/16 v6, 0x40

    const/4 v2, 0x1

    .line 2133508
    invoke-direct {p0}, LX/EX1;-><init>()V

    .line 2133509
    iput-byte v1, p0, LX/EXR;->memoizedIsInitialized:B

    .line 2133510
    iput v1, p0, LX/EXR;->memoizedSerializedSize:I

    .line 2133511
    invoke-direct {p0}, LX/EXR;->D()V

    .line 2133512
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v3

    move v1, v0

    .line 2133513
    :cond_0
    :goto_0
    if-nez v1, :cond_4

    .line 2133514
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v4

    .line 2133515
    sparse-switch v4, :sswitch_data_0

    .line 2133516
    invoke-virtual {p0, p1, v3, p2, v4}, LX/EX1;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 2133517
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 2133518
    goto :goto_0

    .line 2133519
    :sswitch_1
    invoke-virtual {p1}, LX/EWd;->m()I

    move-result v4

    .line 2133520
    invoke-static {v4}, LX/EXQ;->valueOf(I)LX/EXQ;

    move-result-object v5

    .line 2133521
    if-nez v5, :cond_2

    .line 2133522
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, LX/EZM;->a(II)LX/EZM;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2133523
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 2133524
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2133525
    move-object v0, v0

    .line 2133526
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2133527
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x40

    if-ne v1, v6, :cond_1

    .line 2133528
    iget-object v1, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    .line 2133529
    :cond_1
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EXR;->unknownFields:LX/EZQ;

    .line 2133530
    invoke-virtual {p0}, LX/EX1;->E()V

    throw v0

    .line 2133531
    :cond_2
    :try_start_2
    iget v4, p0, LX/EXR;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/EXR;->bitField0_:I

    .line 2133532
    iput-object v5, p0, LX/EXR;->ctype_:LX/EXQ;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2133533
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 2133534
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2133535
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2133536
    move-object v0, v2

    .line 2133537
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2133538
    :sswitch_2
    :try_start_4
    iget v4, p0, LX/EXR;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, LX/EXR;->bitField0_:I

    .line 2133539
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXR;->packed_:Z

    goto :goto_0

    .line 2133540
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 2133541
    :sswitch_3
    iget v4, p0, LX/EXR;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, LX/EXR;->bitField0_:I

    .line 2133542
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXR;->deprecated_:Z

    goto :goto_0

    .line 2133543
    :sswitch_4
    iget v4, p0, LX/EXR;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, LX/EXR;->bitField0_:I

    .line 2133544
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXR;->lazy_:Z

    goto/16 :goto_0

    .line 2133545
    :sswitch_5
    iget v4, p0, LX/EXR;->bitField0_:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, LX/EXR;->bitField0_:I

    .line 2133546
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v4

    iput-object v4, p0, LX/EXR;->experimentalMapKey_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 2133547
    :sswitch_6
    iget v4, p0, LX/EXR;->bitField0_:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, LX/EXR;->bitField0_:I

    .line 2133548
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EXR;->weak_:Z

    goto/16 :goto_0

    .line 2133549
    :sswitch_7
    and-int/lit8 v4, v0, 0x40

    if-eq v4, v6, :cond_3

    .line 2133550
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    .line 2133551
    or-int/lit8 v0, v0, 0x40

    .line 2133552
    :cond_3
    iget-object v4, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    sget-object v5, LX/EYB;->a:LX/EWZ;

    invoke-virtual {p1, v5, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 2133553
    :cond_4
    and-int/lit8 v0, v0, 0x40

    if-ne v0, v6, :cond_5

    .line 2133554
    iget-object v0, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    .line 2133555
    :cond_5
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXR;->unknownFields:LX/EZQ;

    .line 2133556
    invoke-virtual {p0}, LX/EX1;->E()V

    .line 2133557
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
        0x4a -> :sswitch_5
        0x50 -> :sswitch_6
        0x1f3a -> :sswitch_7
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWy",
            "<",
            "LX/EXR;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 2133503
    invoke-direct {p0, p1}, LX/EX1;-><init>(LX/EWy;)V

    .line 2133504
    iput-byte v0, p0, LX/EXR;->memoizedIsInitialized:B

    .line 2133505
    iput v0, p0, LX/EXR;->memoizedSerializedSize:I

    .line 2133506
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXR;->unknownFields:LX/EZQ;

    .line 2133507
    return-void
.end method

.method private B()LX/EWc;
    .locals 2

    .prologue
    .line 2133498
    iget-object v0, p0, LX/EXR;->experimentalMapKey_:Ljava/lang/Object;

    .line 2133499
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2133500
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2133501
    iput-object v0, p0, LX/EXR;->experimentalMapKey_:Ljava/lang/Object;

    .line 2133502
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private D()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2133490
    sget-object v0, LX/EXQ;->STRING:LX/EXQ;

    iput-object v0, p0, LX/EXR;->ctype_:LX/EXQ;

    .line 2133491
    iput-boolean v1, p0, LX/EXR;->packed_:Z

    .line 2133492
    iput-boolean v1, p0, LX/EXR;->lazy_:Z

    .line 2133493
    iput-boolean v1, p0, LX/EXR;->deprecated_:Z

    .line 2133494
    const-string v0, ""

    iput-object v0, p0, LX/EXR;->experimentalMapKey_:Ljava/lang/Object;

    .line 2133495
    iput-boolean v1, p0, LX/EXR;->weak_:Z

    .line 2133496
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    .line 2133497
    return-void
.end method

.method public static a(LX/EXR;)LX/EXO;
    .locals 1

    .prologue
    .line 2133489
    invoke-static {}, LX/EXO;->x()LX/EXO;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EXO;->a(LX/EXR;)LX/EXO;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2133487
    new-instance v0, LX/EXO;

    invoke-direct {v0, p1}, LX/EXO;-><init>(LX/EYd;)V

    .line 2133488
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 2133467
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2133468
    invoke-virtual {p0}, LX/EX1;->G()LX/EYf;

    move-result-object v2

    .line 2133469
    iget v0, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2133470
    iget-object v0, p0, LX/EXR;->ctype_:LX/EXQ;

    invoke-virtual {v0}, LX/EXQ;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, LX/EWf;->d(II)V

    .line 2133471
    :cond_0
    iget v0, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 2133472
    iget-boolean v0, p0, LX/EXR;->packed_:Z

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(IZ)V

    .line 2133473
    :cond_1
    iget v0, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 2133474
    const/4 v0, 0x3

    iget-boolean v1, p0, LX/EXR;->deprecated_:Z

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(IZ)V

    .line 2133475
    :cond_2
    iget v0, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 2133476
    const/4 v0, 0x5

    iget-boolean v1, p0, LX/EXR;->lazy_:Z

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(IZ)V

    .line 2133477
    :cond_3
    iget v0, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2133478
    const/16 v0, 0x9

    invoke-direct {p0}, LX/EXR;->B()LX/EWc;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2133479
    :cond_4
    iget v0, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 2133480
    const/16 v0, 0xa

    iget-boolean v1, p0, LX/EXR;->weak_:Z

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(IZ)V

    .line 2133481
    :cond_5
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 2133482
    const/16 v3, 0x3e7

    iget-object v0, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2133483
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2133484
    :cond_6
    const/high16 v0, 0x20000000

    invoke-virtual {v2, v0, p1}, LX/EYf;->a(ILX/EWf;)V

    .line 2133485
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2133486
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2133567
    iget-byte v0, p0, LX/EXR;->memoizedIsInitialized:B

    .line 2133568
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2133569
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 2133570
    :goto_1
    iget-object v3, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move v3, v3

    .line 2133571
    if-ge v0, v3, :cond_3

    .line 2133572
    iget-object v3, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EYB;

    move-object v3, v3

    .line 2133573
    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2133574
    iput-byte v1, p0, LX/EXR;->memoizedIsInitialized:B

    goto :goto_0

    .line 2133575
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2133576
    :cond_3
    invoke-virtual {p0}, LX/EX1;->F()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2133577
    iput-byte v1, p0, LX/EXR;->memoizedIsInitialized:B

    goto :goto_0

    .line 2133578
    :cond_4
    iput-byte v2, p0, LX/EXR;->memoizedIsInitialized:B

    move v1, v2

    .line 2133579
    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2133446
    iget v0, p0, LX/EXR;->memoizedSerializedSize:I

    .line 2133447
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2133448
    :goto_0
    return v0

    .line 2133449
    :cond_0
    iget v0, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 2133450
    iget-object v0, p0, LX/EXR;->ctype_:LX/EXQ;

    invoke-virtual {v0}, LX/EXQ;->getNumber()I

    move-result v0

    invoke-static {v3, v0}, LX/EWf;->h(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2133451
    :goto_1
    iget v2, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2133452
    iget-boolean v2, p0, LX/EXR;->packed_:Z

    invoke-static {v4, v2}, LX/EWf;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2133453
    :cond_1
    iget v2, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 2133454
    const/4 v2, 0x3

    iget-boolean v3, p0, LX/EXR;->deprecated_:Z

    invoke-static {v2, v3}, LX/EWf;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2133455
    :cond_2
    iget v2, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 2133456
    const/4 v2, 0x5

    iget-boolean v3, p0, LX/EXR;->lazy_:Z

    invoke-static {v2, v3}, LX/EWf;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2133457
    :cond_3
    iget v2, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4

    .line 2133458
    const/16 v2, 0x9

    invoke-direct {p0}, LX/EXR;->B()LX/EWc;

    move-result-object v3

    invoke-static {v2, v3}, LX/EWf;->c(ILX/EWc;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2133459
    :cond_4
    iget v2, p0, LX/EXR;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 2133460
    const/16 v2, 0xa

    iget-boolean v3, p0, LX/EXR;->weak_:Z

    invoke-static {v2, v3}, LX/EWf;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    move v2, v0

    .line 2133461
    :goto_2
    iget-object v0, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 2133462
    const/16 v3, 0x3e7

    iget-object v0, p0, LX/EXR;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2133463
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2133464
    :cond_6
    invoke-virtual {p0}, LX/EX1;->H()I

    move-result v0

    add-int/2addr v0, v2

    .line 2133465
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2133466
    iput v0, p0, LX/EXR;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2133445
    iget-object v0, p0, LX/EXR;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2133444
    sget-object v0, LX/EYC;->x:LX/EYn;

    const-class v1, LX/EXR;

    const-class v2, LX/EXO;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2133443
    sget-object v0, LX/EXR;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2133442
    invoke-virtual {p0}, LX/EXR;->z()LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2133441
    invoke-static {}, LX/EXO;->x()LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2133437
    invoke-virtual {p0}, LX/EXR;->z()LX/EXO;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2133440
    sget-object v0, LX/EXR;->c:LX/EXR;

    return-object v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2133439
    invoke-super {p0}, LX/EX1;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final z()LX/EXO;
    .locals 1

    .prologue
    .line 2133438
    invoke-static {p0}, LX/EXR;->a(LX/EXR;)LX/EXO;

    move-result-object v0

    return-object v0
.end method
