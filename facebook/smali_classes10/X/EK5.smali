.class public final LX/EK5;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/EK7;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/EK6;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2105949
    invoke-direct {p0}, LX/1X5;-><init>()V

    return-void
.end method

.method public static a$redex0(LX/EK5;LX/1De;IILX/EK6;)V
    .locals 0

    .prologue
    .line 2105960
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2105961
    iput-object p4, p0, LX/EK5;->a:LX/EK6;

    .line 2105962
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)LX/EK5;
    .locals 1

    .prologue
    .line 2105958
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    iput-object p1, v0, LX/EK6;->o:Landroid/view/View$OnClickListener;

    .line 2105959
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/EK5;
    .locals 1

    .prologue
    .line 2105956
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    iput-object p1, v0, LX/EK6;->a:Ljava/lang/CharSequence;

    .line 2105957
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2105952
    invoke-super {p0}, LX/1X5;->a()V

    .line 2105953
    const/4 v0, 0x0

    iput-object v0, p0, LX/EK5;->a:LX/EK6;

    .line 2105954
    sget-object v0, LX/EK7;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2105955
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)LX/EK5;
    .locals 1

    .prologue
    .line 2105950
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    iput-object p1, v0, LX/EK6;->b:Ljava/lang/CharSequence;

    .line 2105951
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/EK5;
    .locals 1

    .prologue
    .line 2105930
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    iput-object p1, v0, LX/EK6;->c:Ljava/lang/String;

    .line 2105931
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)LX/EK5;
    .locals 1

    .prologue
    .line 2105947
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    iput-object p1, v0, LX/EK6;->e:Ljava/lang/CharSequence;

    .line 2105948
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/EK5;
    .locals 1

    .prologue
    .line 2105963
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    iput-object p1, v0, LX/EK6;->j:Ljava/lang/String;

    .line 2105964
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/EK7;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2105944
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    .line 2105945
    invoke-virtual {p0}, LX/EK5;->a()V

    .line 2105946
    return-object v0
.end method

.method public final h(I)LX/EK5;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param

    .prologue
    .line 2105942
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    iput p1, v0, LX/EK6;->d:I

    .line 2105943
    return-object p0
.end method

.method public final i(I)LX/EK5;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 2105940
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    iput p1, v0, LX/EK6;->g:I

    .line 2105941
    return-object p0
.end method

.method public final j(I)LX/EK5;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 2105938
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, LX/EK6;->h:I

    .line 2105939
    return-object p0
.end method

.method public final k(I)LX/EK5;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 2105936
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    invoke-virtual {p0, p1}, LX/1Dp;->e(I)I

    move-result v1

    iput v1, v0, LX/EK6;->i:I

    .line 2105937
    return-object p0
.end method

.method public final m(I)LX/EK5;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 2105934
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    iput p1, v0, LX/EK6;->l:I

    .line 2105935
    return-object p0
.end method

.method public final n(I)LX/EK5;
    .locals 1

    .prologue
    .line 2105932
    iget-object v0, p0, LX/EK5;->a:LX/EK6;

    iput p1, v0, LX/EK6;->n:I

    .line 2105933
    return-object p0
.end method
