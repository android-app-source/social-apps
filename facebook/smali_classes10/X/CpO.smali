.class public LX/CpO;
.super LX/Cod;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Cnz;",
        ">;"
    }
.end annotation


# static fields
.field public static final p:Ljava/lang/String;


# instance fields
.field public A:Z

.field private B:LX/CoZ;

.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/17T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/8bG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Ckv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/8bZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/8bO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Cig;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final q:Lcom/facebook/widget/CustomLinearLayout;

.field private final r:Lcom/facebook/widget/CustomLinearLayout;

.field private final s:Lcom/facebook/richdocument/view/widget/RichTextView;

.field private final t:Landroid/view/View;

.field public final u:Lcom/facebook/fbui/glyph/GlyphView;

.field public v:Ljava/lang/String;

.field private w:J

.field private x:Landroid/os/Bundle;

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1937049
    const-class v0, LX/CpO;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CpO;->p:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1937097
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1937098
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, LX/CpO;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1937099
    const v0, 0x7f0d16ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, LX/CpO;->q:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937100
    const v0, 0x7f0d16b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, LX/CpO;->r:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937101
    iget-object v0, p0, LX/CpO;->q:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16bd

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/CpO;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937102
    iget-object v0, p0, LX/CpO;->q:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16be

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/CpO;->u:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1937103
    iget-object v0, p0, LX/CpO;->q:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16ad

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/CpO;->t:Landroid/view/View;

    .line 1937104
    iget-object v0, p0, LX/CpO;->a:LX/Ck0;

    iget-object v1, p0, LX/CpO;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v5, 0x7f0d011b

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937105
    new-instance v0, LX/Cmz;

    new-instance v1, LX/Cn4;

    iget-object v2, p0, LX/CpO;->b:LX/Cju;

    invoke-direct {v1, v2}, LX/Cn4;-><init>(LX/Cju;)V

    invoke-direct {v0, v1, v6, v6, v6}, LX/Cmz;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;)V

    .line 1937106
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1937107
    const/4 v5, 0x0

    .line 1937108
    iget-object v0, p0, LX/CpO;->m:LX/8bZ;

    invoke-virtual {v0}, LX/8bZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/CpO;->n:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, LX/Cmv;->c:LX/Cmv;

    .line 1937109
    :goto_0
    new-instance v1, LX/Cmy;

    new-instance v2, LX/Cmw;

    sget-object v3, LX/Cmv;->b:LX/Cmv;

    sget-object v4, LX/Cmv;->b:LX/Cmv;

    invoke-direct {v2, v0, v3, v0, v4}, LX/Cmw;-><init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V

    const/4 v0, 0x0

    invoke-direct {v1, v2, v5, v5, v0}, LX/Cmy;-><init>(LX/Cmw;LX/Cmr;LX/Cmp;I)V

    invoke-virtual {p0, v1}, LX/Cod;->a(LX/Cml;)V

    .line 1937110
    return-void

    .line 1937111
    :cond_1
    sget-object v0, LX/Cmv;->b:LX/Cmv;

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)LX/CpO;
    .locals 1

    .prologue
    .line 1937096
    new-instance v0, LX/CpO;

    invoke-direct {v0, p0}, LX/CpO;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method private static a(LX/CpO;LX/Ck0;LX/Cju;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/17T;LX/Ckw;LX/8bG;LX/0SG;LX/Ckv;LX/0Uh;LX/17W;LX/Crz;LX/8bZ;LX/8bO;LX/Cig;)V
    .locals 0

    .prologue
    .line 1937095
    iput-object p1, p0, LX/CpO;->a:LX/Ck0;

    iput-object p2, p0, LX/CpO;->b:LX/Cju;

    iput-object p3, p0, LX/CpO;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object p4, p0, LX/CpO;->d:LX/03V;

    iput-object p5, p0, LX/CpO;->e:LX/17T;

    iput-object p6, p0, LX/CpO;->f:LX/Ckw;

    iput-object p7, p0, LX/CpO;->g:LX/8bG;

    iput-object p8, p0, LX/CpO;->h:LX/0SG;

    iput-object p9, p0, LX/CpO;->i:LX/Ckv;

    iput-object p10, p0, LX/CpO;->j:LX/0Uh;

    iput-object p11, p0, LX/CpO;->k:LX/17W;

    iput-object p12, p0, LX/CpO;->l:LX/Crz;

    iput-object p13, p0, LX/CpO;->m:LX/8bZ;

    iput-object p14, p0, LX/CpO;->n:LX/8bO;

    iput-object p15, p0, LX/CpO;->o:LX/Cig;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, LX/CpO;

    invoke-static {v15}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v1

    check-cast v1, LX/Ck0;

    invoke-static {v15}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v2

    check-cast v2, LX/Cju;

    invoke-static {v15}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v15}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v15}, LX/17T;->b(LX/0QB;)LX/17T;

    move-result-object v5

    check-cast v5, LX/17T;

    invoke-static {v15}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v6

    check-cast v6, LX/Ckw;

    invoke-static {v15}, LX/8bG;->b(LX/0QB;)LX/8bG;

    move-result-object v7

    check-cast v7, LX/8bG;

    invoke-static {v15}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v15}, LX/Ckv;->a(LX/0QB;)LX/Ckv;

    move-result-object v9

    check-cast v9, LX/Ckv;

    invoke-static {v15}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v15}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v11

    check-cast v11, LX/17W;

    invoke-static {v15}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v12

    check-cast v12, LX/Crz;

    invoke-static {v15}, LX/8bZ;->b(LX/0QB;)LX/8bZ;

    move-result-object v13

    check-cast v13, LX/8bZ;

    invoke-static {v15}, LX/8bO;->b(LX/0QB;)LX/8bO;

    move-result-object v14

    check-cast v14, LX/8bO;

    invoke-static {v15}, LX/Cig;->a(LX/0QB;)LX/Cig;

    move-result-object v15

    check-cast v15, LX/Cig;

    invoke-static/range {v0 .. v15}, LX/CpO;->a(LX/CpO;LX/Ck0;LX/Cju;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/17T;LX/Ckw;LX/8bG;LX/0SG;LX/Ckv;LX/0Uh;LX/17W;LX/Crz;LX/8bZ;LX/8bO;LX/Cig;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1937092
    iget-object v0, p0, LX/CpO;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937093
    iget-object v0, p0, LX/CpO;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1937094
    return-void
.end method

.method public final a(J)V
    .locals 7

    .prologue
    .line 1937086
    iget-object v0, p0, LX/CpO;->i:LX/Ckv;

    iget-object v1, p0, LX/CpO;->y:Ljava/lang/String;

    .line 1937087
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1937088
    :cond_0
    :goto_0
    return-void

    .line 1937089
    :cond_1
    iget-object v3, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Cku;

    .line 1937090
    iget-wide v5, v3, LX/Cku;->f:J

    long-to-float v4, v5

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_0

    .line 1937091
    iput-wide p1, v3, LX/Cku;->f:J

    goto :goto_0
.end method

.method public final a(LX/8bN;)V
    .locals 2

    .prologue
    .line 1937078
    if-eqz p1, :cond_0

    .line 1937079
    sget-object v0, LX/CpN;->a:[I

    invoke-virtual {p1}, LX/8bN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1937080
    :cond_0
    :goto_0
    iget-boolean v0, p0, LX/CpO;->A:Z

    if-eqz v0, :cond_1

    .line 1937081
    new-instance v0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;

    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;-><init>(Landroid/view/View;LX/CpO;)V

    iput-object v0, p0, LX/CpO;->B:LX/CoZ;

    .line 1937082
    :cond_1
    return-void

    .line 1937083
    :pswitch_0
    new-instance v0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;

    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;-><init>(Landroid/view/View;LX/CpO;)V

    iput-object v0, p0, LX/CpO;->B:LX/CoZ;

    goto :goto_0

    .line 1937084
    :pswitch_1
    new-instance v0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;

    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;-><init>(Landroid/view/View;LX/CpO;)V

    iput-object v0, p0, LX/CpO;->B:LX/CoZ;

    goto :goto_0

    .line 1937085
    :pswitch_2
    new-instance v0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;

    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;-><init>(Landroid/view/View;LX/CpO;)V

    iput-object v0, p0, LX/CpO;->B:LX/CoZ;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/CmV;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1937063
    iget-object v0, p0, LX/CpO;->y:Ljava/lang/String;

    .line 1937064
    iput-object v0, p1, LX/CmV;->b:Ljava/lang/String;

    .line 1937065
    iget-object v0, p0, LX/CpO;->x:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, LX/CmV;->a(Landroid/os/Bundle;)LX/CmV;

    .line 1937066
    new-instance v0, LX/CmW;

    invoke-direct {v0, p1}, LX/CmW;-><init>(LX/CmV;)V

    move-object v0, v0

    .line 1937067
    iget-object v1, v0, LX/CmW;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1937068
    iput-object v1, p0, LX/CpO;->z:Ljava/lang/String;

    .line 1937069
    new-instance v1, LX/CpJ;

    invoke-direct {v1, p0, v0}, LX/CpJ;-><init>(LX/CpO;LX/CmW;)V

    .line 1937070
    iget-object v2, p0, LX/CpO;->B:LX/CoZ;

    invoke-interface {v2, v1}, LX/CoZ;->a(Landroid/view/View$OnClickListener;)V

    .line 1937071
    iget-object v1, p0, LX/CpO;->j:LX/0Uh;

    const/16 v2, 0x507

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1937072
    iget-object v1, p0, LX/CpO;->u:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1937073
    iget-object v1, p0, LX/CpO;->u:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/CpK;

    invoke-direct {v2, p0}, LX/CpK;-><init>(LX/CpO;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1937074
    iget-object v1, p0, LX/CpO;->u:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v2, 0x8

    invoke-static {v1, v2}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v1

    .line 1937075
    iget-object v2, p0, LX/CpO;->r:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/CustomLinearLayout;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 1937076
    :cond_0
    iget-object v1, p0, LX/CpO;->B:LX/CoZ;

    invoke-interface {v1, v0}, LX/CoZ;->a(LX/CmW;)V

    .line 1937077
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1937061
    iget-object v0, p0, LX/CpO;->B:LX/CoZ;

    invoke-interface {v0}, LX/CoZ;->b()V

    .line 1937062
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1937055
    invoke-super {p0, p1}, LX/Cod;->b(Landroid/os/Bundle;)V

    .line 1937056
    iput-object p1, p0, LX/CpO;->x:Landroid/os/Bundle;

    .line 1937057
    invoke-virtual {p0}, LX/CpO;->p()V

    .line 1937058
    iget-object v0, p0, LX/CpO;->B:LX/CoZ;

    if-eqz v0, :cond_0

    .line 1937059
    iget-object v0, p0, LX/CpO;->B:LX/CoZ;

    invoke-interface {v0, p1}, LX/CoZ;->a(Landroid/os/Bundle;)V

    .line 1937060
    :cond_0
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1937050
    invoke-super {p0, p1}, LX/Cod;->c(Landroid/os/Bundle;)V

    .line 1937051
    invoke-virtual {p0}, LX/CpO;->q()V

    .line 1937052
    iget-object v0, p0, LX/CpO;->B:LX/CoZ;

    if-eqz v0, :cond_0

    .line 1937053
    iget-object v0, p0, LX/CpO;->B:LX/CoZ;

    invoke-interface {v0, p1}, LX/CoZ;->b(Landroid/os/Bundle;)V

    .line 1937054
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1937112
    iget-boolean v0, p0, LX/CpO;->A:Z

    if-eqz v0, :cond_0

    .line 1937113
    iget-object v0, p0, LX/CpO;->u:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1937114
    iget-object v0, p0, LX/CpO;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937115
    :cond_0
    iget-object v0, p0, LX/CpO;->B:LX/CoZ;

    invoke-interface {v0}, LX/CoZ;->c()V

    .line 1937116
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 1936993
    iget-object v0, p0, LX/CpO;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CpO;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/CpO;->w:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1936994
    iget-object v0, p0, LX/CpO;->g:LX/8bG;

    iget-object v1, p0, LX/CpO;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/8bG;->a(Ljava/lang/String;)V

    .line 1936995
    iget-object v0, p0, LX/CpO;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/CpO;->w:J

    .line 1936996
    :cond_0
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1936997
    iget-object v0, p0, LX/CpO;->B:LX/CoZ;

    invoke-interface {v0}, LX/CoZ;->a()Z

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 4

    .prologue
    .line 1936998
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1936999
    iget-object v1, p0, LX/CpO;->b:LX/Cju;

    const v2, 0x7f0d0121

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    iget-object v2, p0, LX/CpO;->b:LX/Cju;

    const v3, 0x7f0d0122

    invoke-interface {v2, v3}, LX/Cju;->c(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 1937000
    sub-int/2addr v0, v1

    return v0
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 1937001
    invoke-virtual {p0}, LX/CpO;->h()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3ff47ae1    # 1.91f

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public final k()V
    .locals 6

    .prologue
    .line 1937002
    iget-object v0, p0, LX/CpO;->i:LX/Ckv;

    iget-object v1, p0, LX/CpO;->y:Ljava/lang/String;

    .line 1937003
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1937004
    :cond_0
    :goto_0
    return-void

    .line 1937005
    :cond_1
    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cku;

    .line 1937006
    invoke-virtual {v2}, LX/Cku;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-wide v4, v2, LX/Cku;->d:J

    long-to-float v3, v4

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    .line 1937007
    iget-object v3, v0, LX/Ckv;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v2, LX/Cku;->d:J

    goto :goto_0
.end method

.method public final l()V
    .locals 6

    .prologue
    .line 1937008
    iget-object v0, p0, LX/CpO;->i:LX/Ckv;

    iget-object v1, p0, LX/CpO;->y:Ljava/lang/String;

    .line 1937009
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1937010
    :cond_0
    :goto_0
    return-void

    .line 1937011
    :cond_1
    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cku;

    .line 1937012
    invoke-virtual {v2}, LX/Cku;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-wide v4, v2, LX/Cku;->e:J

    long-to-float v3, v4

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    .line 1937013
    iget-object v3, v0, LX/Ckv;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v2, LX/Cku;->e:J

    goto :goto_0
.end method

.method public final m()V
    .locals 6

    .prologue
    .line 1937014
    iget-object v0, p0, LX/CpO;->i:LX/Ckv;

    iget-object v1, p0, LX/CpO;->y:Ljava/lang/String;

    .line 1937015
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1937016
    :cond_0
    :goto_0
    return-void

    .line 1937017
    :cond_1
    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cku;

    .line 1937018
    if-eqz v2, :cond_0

    iget-wide v4, v2, LX/Cku;->h:J

    long-to-float v3, v4

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    .line 1937019
    iget-object v3, v0, LX/Ckv;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v2, LX/Cku;->h:J

    goto :goto_0
.end method

.method public final n()V
    .locals 6

    .prologue
    .line 1937020
    iget-object v0, p0, LX/CpO;->i:LX/Ckv;

    iget-object v1, p0, LX/CpO;->y:Ljava/lang/String;

    .line 1937021
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1937022
    :cond_0
    :goto_0
    return-void

    .line 1937023
    :cond_1
    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cku;

    .line 1937024
    invoke-virtual {v2}, LX/Cku;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-wide v4, v2, LX/Cku;->c:J

    long-to-float v3, v4

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    .line 1937025
    iget-object v3, v0, LX/Ckv;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v2, LX/Cku;->c:J

    goto :goto_0
.end method

.method public final o()V
    .locals 6

    .prologue
    .line 1937026
    iget-object v0, p0, LX/CpO;->i:LX/Ckv;

    iget-object v1, p0, LX/CpO;->y:Ljava/lang/String;

    .line 1937027
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1937028
    :cond_0
    :goto_0
    return-void

    .line 1937029
    :cond_1
    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cku;

    .line 1937030
    invoke-virtual {v2}, LX/Cku;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-wide v4, v2, LX/Cku;->b:J

    long-to-float v3, v4

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    .line 1937031
    iget-object v3, v0, LX/Ckv;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v2, LX/Cku;->b:J

    goto :goto_0
.end method

.method public final p()V
    .locals 6

    .prologue
    .line 1937032
    iget-object v0, p0, LX/CpO;->i:LX/Ckv;

    iget-object v1, p0, LX/CpO;->y:Ljava/lang/String;

    .line 1937033
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1937034
    :cond_0
    :goto_0
    return-void

    .line 1937035
    :cond_1
    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cku;

    .line 1937036
    iget-wide v4, v2, LX/Cku;->f:J

    long-to-float v3, v4

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    .line 1937037
    iget-object v3, v0, LX/Ckv;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v2, LX/Cku;->f:J

    goto :goto_0
.end method

.method public final q()V
    .locals 6

    .prologue
    .line 1937038
    iget-object v0, p0, LX/CpO;->i:LX/Ckv;

    iget-object v1, p0, LX/CpO;->y:Ljava/lang/String;

    .line 1937039
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1937040
    :cond_0
    :goto_0
    return-void

    .line 1937041
    :cond_1
    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cku;

    .line 1937042
    iget-wide v4, v2, LX/Cku;->g:J

    long-to-float v3, v4

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    .line 1937043
    iget-object v3, v0, LX/Ckv;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v2, LX/Cku;->g:J

    goto :goto_0
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 1937044
    iget-object v0, p0, LX/CpO;->i:LX/Ckv;

    iget-object v1, p0, LX/CpO;->y:Ljava/lang/String;

    .line 1937045
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1937046
    :cond_0
    :goto_0
    return-void

    .line 1937047
    :cond_1
    iget-object v2, v0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cku;

    .line 1937048
    const/4 p0, 0x1

    iput-boolean p0, v2, LX/Cku;->a:Z

    goto :goto_0
.end method
