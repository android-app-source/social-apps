.class public LX/E74;
.super LX/Cfk;
.source ""


# instance fields
.field private final a:LX/E1i;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/3Tx;LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081041
    invoke-direct {p0, p1}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2081042
    iput-object p2, p0, LX/E74;->a:LX/E1i;

    .line 2081043
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 8

    .prologue
    .line 2081044
    iget-object v0, p0, LX/Cfk;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2081045
    iget-object v1, p0, LX/E74;->b:Ljava/lang/String;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->VIEW_PAGE_SERVICES:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E74;->a:LX/E1i;

    .line 2081046
    iget-object v4, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v4, v4

    .line 2081047
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->B()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$PageModel;->b()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->D()LX/9Zu;

    move-result-object v7

    invoke-interface {v7}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v4, v5, v6, v7}, LX/E1i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    invoke-static {v0, v1, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2081048
    const v0, 0x7f03116d

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v3

    .line 2081049
    const v0, 0x7f0d2446

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2081050
    const v1, 0x7f0d2917

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2081051
    const v2, 0x7f0d2918

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2081052
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2081053
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->D()LX/9Zu;

    move-result-object v0

    invoke-interface {v0}, LX/9Zu;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081054
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->D()LX/9Zu;

    move-result-object v0

    invoke-interface {v0}, LX/9Zu;->c()Ljava/lang/String;

    move-result-object v0

    .line 2081055
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2081056
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2081057
    :goto_0
    return-object v3

    .line 2081058
    :cond_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2081059
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)Z
    .locals 1

    .prologue
    .line 2081060
    iput-object p1, p0, LX/E74;->b:Ljava/lang/String;

    .line 2081061
    invoke-super {p0, p1, p2, p3}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2081062
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->B()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->B()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->D()LX/9Zu;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->D()LX/9Zu;

    move-result-object v0

    invoke-interface {v0}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->D()LX/9Zu;

    move-result-object v0

    invoke-interface {v0}, LX/9Zu;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
