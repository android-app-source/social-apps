.class public final LX/DrY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fig/listitem/FigListItem;

.field public final synthetic b:LX/DrZ;

.field public final synthetic c:LX/1Pn;

.field public final synthetic d:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;Lcom/facebook/fig/listitem/FigListItem;LX/DrZ;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2049524
    iput-object p1, p0, LX/DrY;->d:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;

    iput-object p2, p0, LX/DrY;->a:Lcom/facebook/fig/listitem/FigListItem;

    iput-object p3, p0, LX/DrY;->b:LX/DrZ;

    iput-object p4, p0, LX/DrY;->c:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x327ee67d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2049525
    iget-object v0, p0, LX/DrY;->a:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0}, Lcom/facebook/fig/listitem/FigListItem;->getActionState()Z

    move-result v2

    .line 2049526
    iget-object v0, p0, LX/DrY;->b:LX/DrZ;

    iget-object v0, v0, LX/DrZ;->c:LX/Drc;

    .line 2049527
    iput-boolean v2, v0, LX/Drc;->a:Z

    .line 2049528
    if-eqz v2, :cond_0

    iget-object v0, p0, LX/DrY;->b:LX/DrZ;

    iget-object v0, v0, LX/DrZ;->a:LX/BCO;

    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2049529
    :goto_0
    iget-object v3, p0, LX/DrY;->a:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2049530
    iget-object v0, p0, LX/DrY;->d:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;

    iget-object v3, v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;->b:LX/33W;

    iget-object v0, p0, LX/DrY;->c:LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    if-eqz v2, :cond_1

    iget-object v0, p0, LX/DrY;->b:LX/DrZ;

    iget-object v0, v0, LX/DrZ;->a:LX/BCO;

    :goto_1
    sget-object v5, LX/8D0;->TOGGLE:LX/8D0;

    new-instance v6, LX/DrX;

    invoke-direct {v6, p0, v2}, LX/DrX;-><init>(LX/DrY;Z)V

    invoke-virtual {v3, v4, v0, v5, v6}, LX/33W;->a(Landroid/content/Context;LX/BCO;LX/8D0;LX/Dq2;)V

    .line 2049531
    const v0, 0x1019419b

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2049532
    :cond_0
    iget-object v0, p0, LX/DrY;->b:LX/DrZ;

    iget-object v0, v0, LX/DrZ;->b:LX/BCO;

    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel$ImageSourceModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2049533
    :cond_1
    iget-object v0, p0, LX/DrY;->b:LX/DrZ;

    iget-object v0, v0, LX/DrZ;->b:LX/BCO;

    goto :goto_1
.end method
