.class public LX/Egl;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Egk;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2157258
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Egl;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2157207
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2157208
    iput-object p1, p0, LX/Egl;->b:LX/0Ot;

    .line 2157209
    return-void
.end method

.method public static a(LX/0QB;)LX/Egl;
    .locals 4

    .prologue
    .line 2157210
    const-class v1, LX/Egl;

    monitor-enter v1

    .line 2157211
    :try_start_0
    sget-object v0, LX/Egl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2157212
    sput-object v2, LX/Egl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2157213
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2157214
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2157215
    new-instance v3, LX/Egl;

    const/16 p0, 0x180d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Egl;-><init>(LX/0Ot;)V

    .line 2157216
    move-object v0, v3

    .line 2157217
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2157218
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Egl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2157219
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2157220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2157221
    invoke-static {}, LX/1dS;->b()V

    .line 2157222
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 5

    .prologue
    .line 2157223
    check-cast p3, LX/Egj;

    .line 2157224
    iget-object v0, p0, LX/Egl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;

    iget-object v1, p3, LX/Egj;->b:Ljava/lang/String;

    iget-object v2, p3, LX/Egj;->c:Ljava/util/List;

    .line 2157225
    iget-object v3, v0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->a:LX/BdA;

    .line 2157226
    new-instance v4, LX/Bd7;

    invoke-direct {v4, v3}, LX/Bd7;-><init>(LX/BdA;)V

    .line 2157227
    iget-object p0, v3, LX/BdA;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Bd6;

    .line 2157228
    if-nez p0, :cond_0

    .line 2157229
    new-instance p0, LX/Bd6;

    invoke-direct {p0, v3}, LX/Bd6;-><init>(LX/BdA;)V

    .line 2157230
    :cond_0
    iput-object v4, p0, LX/BcN;->a:LX/BcO;

    .line 2157231
    iput-object v4, p0, LX/Bd6;->a:LX/Bd7;

    .line 2157232
    iget-object v3, p0, LX/Bd6;->e:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 2157233
    move-object v4, p0

    .line 2157234
    move-object v3, v4

    .line 2157235
    new-instance v4, LX/Egm;

    invoke-direct {v4, v0, p1}, LX/Egm;-><init>(Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;LX/BcP;)V

    .line 2157236
    iget-object p0, v3, LX/Bd6;->a:LX/Bd7;

    iput-object v4, p0, LX/Bd7;->f:LX/Egm;

    .line 2157237
    iget-object p0, v3, LX/Bd6;->e:Ljava/util/BitSet;

    const/4 p3, 0x0

    invoke-virtual {p0, p3}, Ljava/util/BitSet;->set(I)V

    .line 2157238
    move-object v3, v3

    .line 2157239
    invoke-virtual {v3}, LX/Bd6;->b()LX/BcO;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2157240
    iget-object v3, v0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->b:LX/Egq;

    .line 2157241
    new-instance v4, LX/Egp;

    invoke-direct {v4, v3}, LX/Egp;-><init>(LX/Egq;)V

    .line 2157242
    sget-object p0, LX/Egq;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Ego;

    .line 2157243
    if-nez p0, :cond_1

    .line 2157244
    new-instance p0, LX/Ego;

    invoke-direct {p0}, LX/Ego;-><init>()V

    .line 2157245
    :cond_1
    iput-object v4, p0, LX/BcN;->a:LX/BcO;

    .line 2157246
    iput-object v4, p0, LX/Ego;->a:LX/Egp;

    .line 2157247
    iget-object v3, p0, LX/Ego;->d:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 2157248
    move-object v4, p0

    .line 2157249
    move-object v3, v4

    .line 2157250
    iget-object v4, v3, LX/Ego;->a:LX/Egp;

    iput-object v1, v4, LX/Egp;->b:Ljava/lang/String;

    .line 2157251
    iget-object v4, v3, LX/Ego;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, Ljava/util/BitSet;->set(I)V

    .line 2157252
    move-object v3, v3

    .line 2157253
    iget-object v4, v3, LX/Ego;->a:LX/Egp;

    iput-object v2, v4, LX/Egp;->c:Ljava/util/List;

    .line 2157254
    iget-object v4, v3, LX/Ego;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, Ljava/util/BitSet;->set(I)V

    .line 2157255
    move-object v3, v3

    .line 2157256
    invoke-static {p1}, LX/BcS;->a(LX/BcP;)LX/BcQ;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Ego;->b(LX/BcQ;)LX/Ego;

    move-result-object v3

    invoke-virtual {v3}, LX/Ego;->b()LX/BcO;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2157257
    return-void
.end method
