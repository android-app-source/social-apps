.class public final LX/Dmd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

.field public final synthetic b:LX/Dmn;


# direct methods
.method public constructor <init>(LX/Dmn;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V
    .locals 0

    .prologue
    .line 2039582
    iput-object p1, p0, LX/Dmd;->b:LX/Dmn;

    iput-object p2, p0, LX/Dmd;->a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x494d9f9

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2039583
    iget-object v1, p0, LX/Dmd;->b:LX/Dmn;

    iget-object v1, v1, LX/Dmn;->d:LX/Dih;

    iget-object v2, p0, LX/Dmd;->a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->f:Ljava/lang/String;

    iget-object v3, p0, LX/Dmd;->a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    iget-object v4, p0, LX/Dmd;->a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-virtual {v1, v2, v3, v4}, LX/Dih;->c(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)V

    .line 2039584
    iget-object v1, p0, LX/Dmd;->a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->b:Ljava/lang/String;

    invoke-static {v1}, LX/DkQ;->f(Ljava/lang/String;)LX/DkQ;

    move-result-object v1

    .line 2039585
    iget-object v2, p0, LX/Dmd;->b:LX/Dmn;

    iget-object v2, v2, LX/Dmn;->b:Landroid/content/Context;

    iget-object v3, p0, LX/Dmd;->a:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/facebook/messaging/professionalservices/booking/activities/AppointmentActivity;->a(Landroid/content/Context;LX/DkQ;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2039586
    iget-object v2, p0, LX/Dmd;->b:LX/Dmn;

    iget-object v2, v2, LX/Dmn;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Dmd;->b:LX/Dmn;

    iget-object v3, v3, LX/Dmn;->b:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2039587
    const v1, -0x1a7317ba    # -8.3177E22f

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
