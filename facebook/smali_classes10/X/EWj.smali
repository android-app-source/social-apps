.class public abstract LX/EWj;
.super LX/EWV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<BuilderType:",
        "LX/EWj;",
        ">",
        "LX/EWV",
        "<TBuilderType;>;"
    }
.end annotation


# instance fields
.field private a:LX/EYd;

.field private b:LX/EYe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EWj",
            "<TBuilderType;>.BuilderParentImpl;"
        }
    .end annotation
.end field

.field public c:Z

.field private d:LX/EZQ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2130984
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWj;-><init>(LX/EYd;)V

    .line 2130985
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2130967
    invoke-direct {p0}, LX/EWV;-><init>()V

    .line 2130968
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2130969
    iput-object v0, p0, LX/EWj;->d:LX/EZQ;

    .line 2130970
    iput-object p1, p0, LX/EWj;->a:LX/EYd;

    .line 2130971
    return-void
.end method

.method public static l(LX/EWj;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2130972
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 2130973
    invoke-virtual {p0}, LX/EWj;->d()LX/EYn;

    move-result-object v0

    iget-object v0, v0, LX/EYn;->a:LX/EYF;

    .line 2130974
    invoke-virtual {v0}, LX/EYF;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2130975
    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2130976
    invoke-virtual {p0, v0}, LX/EWj;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2130977
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2130978
    invoke-virtual {v2, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2130979
    :cond_1
    invoke-virtual {p0, v0}, LX/EWj;->a(LX/EYP;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2130980
    invoke-virtual {p0, v0}, LX/EWj;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2130981
    :cond_2
    return-object v2
.end method


# virtual methods
.method public synthetic a(LX/EYP;Ljava/lang/Object;)LX/EWU;
    .locals 1

    .prologue
    .line 2130982
    invoke-virtual {p0, p1, p2}, LX/EWj;->d(LX/EYP;Ljava/lang/Object;)LX/EWj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EZQ;)LX/EWV;
    .locals 1

    .prologue
    .line 2130983
    invoke-virtual {p0, p1}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2130986
    invoke-virtual {p0}, LX/EWj;->e()LX/EYF;

    move-result-object v0

    invoke-virtual {v0}, LX/EYF;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2130987
    invoke-virtual {v0}, LX/EYP;->k()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2130988
    invoke-virtual {p0, v0}, LX/EWj;->a(LX/EYP;)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 2130989
    :goto_0
    return v0

    .line 2130990
    :cond_1
    invoke-virtual {v0}, LX/EYP;->f()LX/EYN;

    move-result-object v3

    sget-object v4, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v3, v4, :cond_0

    .line 2130991
    invoke-virtual {v0}, LX/EYP;->m()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2130992
    invoke-virtual {p0, v0}, LX/EWj;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2130993
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    .line 2130994
    invoke-interface {v0}, LX/EWQ;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2130995
    goto :goto_0

    .line 2130996
    :cond_3
    invoke-virtual {p0, v0}, LX/EWj;->a(LX/EYP;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v0}, LX/EWj;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWY;

    invoke-interface {v0}, LX/EWQ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2130997
    goto :goto_0

    .line 2130998
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(LX/EYP;)Z
    .locals 1

    .prologue
    .line 2131009
    invoke-virtual {p0}, LX/EWj;->d()LX/EYn;

    move-result-object v0

    invoke-static {v0, p1}, LX/EYn;->a$redex0(LX/EYn;LX/EYP;)LX/EYg;

    move-result-object v0

    invoke-interface {v0, p0}, LX/EYg;->b(LX/EWj;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(LX/EYP;Ljava/lang/Object;)LX/EWU;
    .locals 1

    .prologue
    .line 2130999
    invoke-virtual {p0, p1, p2}, LX/EWj;->c(LX/EYP;Ljava/lang/Object;)LX/EWj;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/EZQ;)LX/EWU;
    .locals 1

    .prologue
    .line 2131000
    iput-object p1, p0, LX/EWj;->d:LX/EZQ;

    .line 2131001
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2131002
    return-object p0
.end method

.method public synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2131003
    invoke-virtual {p0}, LX/EWj;->f()LX/EWj;

    move-result-object v0

    return-object v0
.end method

.method public b(LX/EYP;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2131004
    invoke-virtual {p0}, LX/EWj;->d()LX/EYn;

    move-result-object v0

    invoke-static {v0, p1}, LX/EYn;->a$redex0(LX/EYn;LX/EYP;)LX/EYg;

    move-result-object v0

    invoke-interface {v0, p0}, LX/EYg;->a(LX/EWj;)Ljava/lang/Object;

    move-result-object v0

    .line 2131005
    invoke-virtual {p1}, LX/EYP;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2131006
    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2131007
    :cond_0
    return-object v0
.end method

.method public synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2131008
    invoke-virtual {p0}, LX/EWj;->f()LX/EWj;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/EYP;)LX/EWU;
    .locals 1

    .prologue
    .line 2130964
    invoke-virtual {p0}, LX/EWj;->d()LX/EYn;

    move-result-object v0

    invoke-static {v0, p1}, LX/EYn;->a$redex0(LX/EYn;LX/EYP;)LX/EYg;

    move-result-object v0

    invoke-interface {v0}, LX/EYg;->newBuilder()LX/EWU;

    move-result-object v0

    return-object v0
.end method

.method public c(LX/EYP;Ljava/lang/Object;)LX/EWj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2130965
    invoke-virtual {p0}, LX/EWj;->d()LX/EYn;

    move-result-object v0

    invoke-static {v0, p1}, LX/EYn;->a$redex0(LX/EYn;LX/EYP;)LX/EYg;

    move-result-object v0

    invoke-interface {v0, p0, p2}, LX/EYg;->a(LX/EWj;Ljava/lang/Object;)V

    .line 2130966
    return-object p0
.end method

.method public final c(LX/EZQ;)LX/EWj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EZQ;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2130961
    iget-object v0, p0, LX/EWj;->d:LX/EZQ;

    invoke-static {v0}, LX/EZQ;->a(LX/EZQ;)LX/EZM;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZM;->a(LX/EZQ;)LX/EZM;

    move-result-object v0

    invoke-virtual {v0}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EWj;->d:LX/EZQ;

    .line 2130962
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2130963
    return-object p0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2130960
    invoke-virtual {p0}, LX/EWj;->f()LX/EWj;

    move-result-object v0

    return-object v0
.end method

.method public d(LX/EYP;Ljava/lang/Object;)LX/EWj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ")TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2130958
    invoke-virtual {p0}, LX/EWj;->d()LX/EYn;

    move-result-object v0

    invoke-static {v0, p1}, LX/EYn;->a$redex0(LX/EYn;LX/EYP;)LX/EYg;

    move-result-object v0

    invoke-interface {v0, p0, p2}, LX/EYg;->b(LX/EWj;Ljava/lang/Object;)V

    .line 2130959
    return-object p0
.end method

.method public abstract d()LX/EYn;
.end method

.method public e()LX/EYF;
    .locals 1

    .prologue
    .line 2130957
    invoke-virtual {p0}, LX/EWj;->d()LX/EYn;

    move-result-object v0

    iget-object v0, v0, LX/EYn;->a:LX/EYF;

    return-object v0
.end method

.method public f()LX/EWj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TBuilderType;"
        }
    .end annotation

    .prologue
    .line 2130956
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is supposed to be overridden by subclasses."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2130955
    iget-object v0, p0, LX/EWj;->d:LX/EZQ;

    return-object v0
.end method

.method public kb_()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2130954
    invoke-static {p0}, LX/EWj;->l(LX/EWj;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 2130940
    const/4 v0, 0x0

    iput-object v0, p0, LX/EWj;->a:LX/EYd;

    .line 2130941
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2130951
    iget-object v0, p0, LX/EWj;->a:LX/EYd;

    if-eqz v0, :cond_0

    .line 2130952
    invoke-virtual {p0}, LX/EWj;->q()V

    .line 2130953
    :cond_0
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 2130949
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EWj;->c:Z

    .line 2130950
    return-void
.end method

.method public final s()LX/EYd;
    .locals 2

    .prologue
    .line 2130946
    iget-object v0, p0, LX/EWj;->b:LX/EYe;

    if-nez v0, :cond_0

    .line 2130947
    new-instance v0, LX/EYe;

    invoke-direct {v0, p0}, LX/EYe;-><init>(LX/EWj;)V

    iput-object v0, p0, LX/EWj;->b:LX/EYe;

    .line 2130948
    :cond_0
    iget-object v0, p0, LX/EWj;->b:LX/EYe;

    return-object v0
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 2130942
    iget-boolean v0, p0, LX/EWj;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EWj;->a:LX/EYd;

    if-eqz v0, :cond_0

    .line 2130943
    iget-object v0, p0, LX/EWj;->a:LX/EYd;

    invoke-interface {v0}, LX/EYd;->a()V

    .line 2130944
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EWj;->c:Z

    .line 2130945
    :cond_0
    return-void
.end method
