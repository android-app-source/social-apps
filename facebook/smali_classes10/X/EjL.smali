.class public LX/EjL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/15i;I)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2161273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2161274
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/EjL;->a:J

    .line 2161275
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EjL;->b:Ljava/lang/String;

    .line 2161276
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EjL;->c:Ljava/lang/String;

    .line 2161277
    return-void
.end method

.method public constructor <init>(Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$InvitableContactsModel;)V
    .locals 2

    .prologue
    .line 2161278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2161279
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$InvitableContactsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/EjL;->a:J

    .line 2161280
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$InvitableContactsModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EjL;->b:Ljava/lang/String;

    .line 2161281
    invoke-virtual {p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$InvitableContactsModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EjL;->c:Ljava/lang/String;

    .line 2161282
    return-void
.end method
