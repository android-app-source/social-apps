.class public LX/Crb;
.super Landroid/view/OrientationEventListener;
.source ""

# interfaces
.implements LX/02k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/view/OrientationEventListener;",
        "LX/02k;"
    }
.end annotation


# instance fields
.field public a:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CrZ;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/CrY;

.field private e:LX/CrY;

.field private f:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1941232
    invoke-direct {p0, p1}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    .line 1941233
    sget-object v0, LX/CrY;->PORTRAIT:LX/CrY;

    iput-object v0, p0, LX/Crb;->d:LX/CrY;

    .line 1941234
    iput-object p1, p0, LX/Crb;->b:Landroid/content/Context;

    .line 1941235
    const-class v0, LX/Crb;

    invoke-static {v0, p0}, LX/Crb;->a(Ljava/lang/Class;LX/02k;)V

    .line 1941236
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/Crb;->c:Ljava/util/List;

    .line 1941237
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Crb;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p0

    check-cast p0, LX/0So;

    iput-object p0, p1, LX/Crb;->a:LX/0So;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1941191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Crb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1941192
    invoke-virtual {p0}, LX/Crb;->disable()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1941193
    monitor-exit p0

    return-void

    .line 1941194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CrZ;)V
    .locals 1

    .prologue
    .line 1941238
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/Crb;->canDetectOrientation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Crb;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1941239
    iget-object v0, p0, LX/Crb;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1941240
    invoke-virtual {p0}, LX/Crb;->enable()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1941241
    :cond_0
    monitor-exit p0

    return-void

    .line 1941242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/CrZ;)V
    .locals 1

    .prologue
    .line 1941227
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Crb;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1941228
    iget-object v0, p0, LX/Crb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941229
    invoke-virtual {p0}, LX/Crb;->disable()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1941230
    :cond_0
    monitor-exit p0

    return-void

    .line 1941231
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1941226
    iget-object v0, p0, LX/Crb;->b:Landroid/content/Context;

    return-object v0
.end method

.method public final onOrientationChanged(I)V
    .locals 6

    .prologue
    .line 1941195
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x0

    .line 1941196
    invoke-virtual {p0}, LX/Crb;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 1941197
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 1941198
    if-nez v0, :cond_2

    .line 1941199
    :cond_1
    :goto_0
    return-void

    .line 1941200
    :cond_2
    sget v0, LX/CoL;->v:I

    .line 1941201
    sget-object v1, LX/Cra;->a:[I

    iget-object v2, p0, LX/Crb;->d:LX/CrY;

    invoke-virtual {v2}, LX/CrY;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1941202
    const/4 v0, 0x0

    :goto_1
    move-object v0, v0

    .line 1941203
    iget-object v1, p0, LX/Crb;->e:LX/CrY;

    if-eq v1, v0, :cond_3

    .line 1941204
    iput-object v0, p0, LX/Crb;->e:LX/CrY;

    .line 1941205
    iget-object v1, p0, LX/Crb;->a:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/Crb;->f:J

    .line 1941206
    :cond_3
    iget-object v1, p0, LX/Crb;->e:LX/CrY;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Crb;->e:LX/CrY;

    iget-object v2, p0, LX/Crb;->d:LX/CrY;

    if-eq v1, v2, :cond_1

    .line 1941207
    iget-object v1, p0, LX/Crb;->a:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/Crb;->f:J

    sub-long/2addr v2, v4

    sget v1, LX/CoL;->w:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 1941208
    iget-object v1, p0, LX/Crb;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CrZ;

    .line 1941209
    invoke-interface {v1, v0}, LX/CrZ;->a(LX/CrY;)V

    goto :goto_2

    .line 1941210
    :cond_4
    iput-object v0, p0, LX/Crb;->d:LX/CrY;

    goto :goto_0

    .line 1941211
    :pswitch_0
    rsub-int/lit8 v1, v0, 0x5a

    if-lt p1, v1, :cond_5

    add-int/lit16 v1, v0, 0x10e

    if-lt p1, v1, :cond_6

    .line 1941212
    :cond_5
    sget-object v0, LX/CrY;->PORTRAIT:LX/CrY;

    goto :goto_1

    .line 1941213
    :cond_6
    rsub-int/lit8 v0, v0, 0x5a

    if-lt p1, v0, :cond_7

    const/16 v0, 0xb4

    if-ge p1, v0, :cond_7

    .line 1941214
    sget-object v0, LX/CrY;->LANDSCAPE_RIGHT:LX/CrY;

    goto :goto_1

    .line 1941215
    :cond_7
    sget-object v0, LX/CrY;->LANDSCAPE_LEFT:LX/CrY;

    goto :goto_1

    .line 1941216
    :pswitch_1
    if-lt p1, v0, :cond_8

    const/16 v1, 0xe1

    if-ge p1, v1, :cond_8

    .line 1941217
    sget-object v0, LX/CrY;->LANDSCAPE_RIGHT:LX/CrY;

    goto :goto_1

    .line 1941218
    :cond_8
    if-lt p1, v0, :cond_9

    add-int/lit16 v0, v0, 0x10e

    if-lt p1, v0, :cond_a

    .line 1941219
    :cond_9
    sget-object v0, LX/CrY;->PORTRAIT:LX/CrY;

    goto :goto_1

    .line 1941220
    :cond_a
    sget-object v0, LX/CrY;->LANDSCAPE_LEFT:LX/CrY;

    goto :goto_1

    .line 1941221
    :pswitch_2
    rsub-int v1, v0, 0x168

    if-ge p1, v1, :cond_b

    const/16 v1, 0x87

    if-lt p1, v1, :cond_b

    .line 1941222
    sget-object v0, LX/CrY;->LANDSCAPE_LEFT:LX/CrY;

    goto :goto_1

    .line 1941223
    :cond_b
    rsub-int/lit8 v1, v0, 0x5a

    if-lt p1, v1, :cond_c

    rsub-int v0, v0, 0x168

    if-lt p1, v0, :cond_d

    .line 1941224
    :cond_c
    sget-object v0, LX/CrY;->PORTRAIT:LX/CrY;

    goto/16 :goto_1

    .line 1941225
    :cond_d
    sget-object v0, LX/CrY;->LANDSCAPE_RIGHT:LX/CrY;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
