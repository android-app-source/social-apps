.class public final LX/DDi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DDk;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;LX/DDk;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1975953
    iput-object p1, p0, LX/DDi;->d:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;

    iput-object p2, p0, LX/DDi;->a:LX/DDk;

    iput-object p3, p0, LX/DDi;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/DDi;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x7b00bfdf

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1975954
    sget-object v1, LX/DDj;->a:[I

    iget-object v2, p0, LX/DDi;->a:LX/DDk;

    invoke-virtual {v2}, LX/DDk;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1975955
    iget-object v1, p0, LX/DDi;->d:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->d:LX/9LP;

    iget-object v2, p0, LX/DDi;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v3, LX/6Vy;->FEED_STORY:LX/6Vy;

    invoke-virtual {v1, v2, v3}, LX/9LP;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/6Vy;)V

    .line 1975956
    :goto_0
    const v1, -0x3e0d0acd

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1975957
    :pswitch_0
    iget-object v1, p0, LX/DDi;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1975958
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1975959
    sget-object v2, LX/0ax;->ge:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1975960
    iget-object v2, p0, LX/DDi;->d:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;->b:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
