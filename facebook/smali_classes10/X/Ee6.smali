.class public LX/Ee6;
.super LX/Ee3;
.source ""


# instance fields
.field public final c:Landroid/net/ConnectivityManager;

.field public d:I

.field private final e:[J

.field private final f:[J

.field private final g:[J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x4

    .line 2152004
    invoke-direct {p0}, LX/Ee3;-><init>()V

    .line 2152005
    new-array v0, v1, [J

    iput-object v0, p0, LX/Ee6;->e:[J

    .line 2152006
    new-array v0, v2, [J

    iput-object v0, p0, LX/Ee6;->f:[J

    .line 2152007
    new-array v0, v2, [J

    iput-object v0, p0, LX/Ee6;->g:[J

    .line 2152008
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, LX/Ee6;->c:Landroid/net/ConnectivityManager;

    .line 2152009
    iget-object v0, p0, LX/Ee6;->c:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 2152010
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, LX/Ee6;->d:I

    .line 2152011
    invoke-direct {p0}, LX/Ee6;->l()V

    .line 2152012
    return-void

    .line 2152013
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    goto :goto_0
.end method

.method public static k(LX/Ee6;)V
    .locals 8

    .prologue
    const/4 v2, 0x4

    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 2151996
    invoke-direct {p0}, LX/Ee6;->l()V

    .line 2151997
    iget v1, p0, LX/Ee6;->d:I

    if-nez v1, :cond_0

    move v1, v0

    .line 2151998
    :goto_0
    if-eq v1, v3, :cond_2

    .line 2151999
    :goto_1
    if-ge v0, v2, :cond_2

    .line 2152000
    iget-object v3, p0, LX/Ee6;->e:[J

    or-int v4, v1, v0

    iget-object v5, p0, LX/Ee6;->f:[J

    aget-wide v6, v5, v0

    aput-wide v6, v3, v4

    .line 2152001
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2152002
    :cond_0
    iget v1, p0, LX/Ee6;->d:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    move v1, v2

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_0

    .line 2152003
    :cond_2
    return-void
.end method

.method private l()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2151984
    iget-object v1, p0, LX/Ee6;->f:[J

    sget v2, LX/Ee3;->a:I

    invoke-static {v2}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v2

    iget-object v4, p0, LX/Ee6;->g:[J

    aget-wide v4, v4, v0

    sub-long/2addr v2, v4

    aput-wide v2, v1, v0

    .line 2151985
    iget-object v1, p0, LX/Ee6;->f:[J

    sget v2, LX/Ee3;->a:I

    invoke-static {v2}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v2

    iget-object v4, p0, LX/Ee6;->g:[J

    aget-wide v4, v4, v6

    sub-long/2addr v2, v4

    aput-wide v2, v1, v6

    .line 2151986
    iget-object v1, p0, LX/Ee6;->f:[J

    sget v2, LX/Ee3;->a:I

    invoke-static {v2}, Landroid/net/TrafficStats;->getUidRxPackets(I)J

    move-result-wide v2

    iget-object v4, p0, LX/Ee6;->g:[J

    aget-wide v4, v4, v7

    sub-long/2addr v2, v4

    aput-wide v2, v1, v7

    .line 2151987
    iget-object v1, p0, LX/Ee6;->f:[J

    sget v2, LX/Ee3;->a:I

    invoke-static {v2}, Landroid/net/TrafficStats;->getUidTxPackets(I)J

    move-result-wide v2

    iget-object v4, p0, LX/Ee6;->g:[J

    aget-wide v4, v4, v8

    sub-long/2addr v2, v4

    aput-wide v2, v1, v8

    .line 2151988
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 2151989
    iget-object v1, p0, LX/Ee6;->g:[J

    aget-wide v2, v1, v0

    iget-object v4, p0, LX/Ee6;->f:[J

    aget-wide v4, v4, v0

    add-long/2addr v2, v4

    aput-wide v2, v1, v0

    .line 2151990
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2151991
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2151992
    invoke-static {p0}, LX/Ee6;->k(LX/Ee6;)V

    .line 2151993
    iget-object v0, p0, LX/Ee6;->e:[J

    iget-object v1, p0, LX/Ee3;->b:[J

    const/16 v2, 0x8

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2151994
    iget-object v0, p0, LX/Ee6;->e:[J

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 2151995
    const/4 v0, 0x1

    return v0
.end method
