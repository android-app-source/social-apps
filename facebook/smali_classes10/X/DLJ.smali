.class public LX/DLJ;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public A:Landroid/view/View$OnClickListener;

.field public B:Landroid/view/View$OnClickListener;

.field public C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

.field public D:LX/3Af;

.field public E:I

.field public F:I

.field public j:Landroid/content/res/Resources;

.field public k:LX/6RZ;

.field public l:Lcom/facebook/content/SecureContextHelper;

.field private m:LX/DLU;

.field public n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public p:Landroid/widget/TextView;

.field public q:Landroid/widget/TextView;

.field public r:Landroid/widget/TextView;

.field private s:Ljava/lang/String;

.field public t:LX/DLI;

.field public u:I

.field public v:Landroid/widget/ImageButton;

.field public w:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

.field private x:Landroid/widget/ProgressBar;

.field public y:Landroid/widget/ImageView;

.field public z:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;Landroid/content/res/Resources;LX/6RZ;Lcom/facebook/content/SecureContextHelper;LX/DLU;LX/0Or;LX/0Ot;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/groups/docsandfiles/fragment/GroupDocOrFileListViewItem$GroupsDocOrFileRowListener;",
            "Landroid/content/res/Resources;",
            "LX/6RZ;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/DLU;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1988092
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1988093
    iput-object p2, p0, LX/DLJ;->w:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    .line 1988094
    iput-object p3, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    .line 1988095
    iput-object p4, p0, LX/DLJ;->k:LX/6RZ;

    .line 1988096
    iput-object p5, p0, LX/DLJ;->l:Lcom/facebook/content/SecureContextHelper;

    .line 1988097
    iput-object p6, p0, LX/DLJ;->m:LX/DLU;

    .line 1988098
    iput-object p7, p0, LX/DLJ;->n:LX/0Or;

    .line 1988099
    iput-object p8, p0, LX/DLJ;->o:LX/0Ot;

    .line 1988100
    const/4 p3, 0x0

    .line 1988101
    const p1, 0x7f030809

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1988102
    iget-object p1, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const p2, 0x7f0a0859

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p0, p1}, LX/DLJ;->setBackgroundColor(I)V

    .line 1988103
    iget-object p1, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const p2, 0x7f0b2041

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, LX/DLJ;->E:I

    .line 1988104
    iget p1, p0, LX/DLJ;->E:I

    invoke-virtual {p0, p1, p3, p3, p3}, LX/DLJ;->setPadding(IIII)V

    .line 1988105
    iget p1, p0, LX/DLJ;->E:I

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1988106
    const/16 p1, 0x11

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 1988107
    const p1, 0x7f0d152e

    invoke-virtual {p0, p1}, LX/DLJ;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/DLJ;->p:Landroid/widget/TextView;

    .line 1988108
    const p1, 0x7f0d152f

    invoke-virtual {p0, p1}, LX/DLJ;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/DLJ;->q:Landroid/widget/TextView;

    .line 1988109
    const p1, 0x7f0d1530

    invoke-virtual {p0, p1}, LX/DLJ;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/DLJ;->r:Landroid/widget/TextView;

    .line 1988110
    iget-object p1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object p1, p1

    .line 1988111
    check-cast p1, Landroid/widget/ImageButton;

    iput-object p1, p0, LX/DLJ;->v:Landroid/widget/ImageButton;

    .line 1988112
    new-instance p1, LX/DLA;

    invoke-direct {p1, p0}, LX/DLA;-><init>(LX/DLJ;)V

    iput-object p1, p0, LX/DLJ;->A:Landroid/view/View$OnClickListener;

    .line 1988113
    new-instance p1, LX/DLB;

    invoke-direct {p1, p0}, LX/DLB;-><init>(LX/DLJ;)V

    iput-object p1, p0, LX/DLJ;->B:Landroid/view/View$OnClickListener;

    .line 1988114
    return-void
.end method

.method public static a(LX/DLJ;Z)V
    .locals 3

    .prologue
    .line 1988076
    if-eqz p1, :cond_2

    iget-object v0, p0, LX/DLJ;->y:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    .line 1988077
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/DLJ;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/DLJ;->y:Landroid/widget/ImageView;

    .line 1988078
    iget-object v0, p0, LX/DLJ;->y:Landroid/widget/ImageView;

    iget-object v1, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v2, 0x7f0209aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1988079
    iget-object v0, p0, LX/DLJ;->y:Landroid/widget/ImageView;

    iget-object v1, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v2, 0x7f0830b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1988080
    iget-object v0, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v1, 0x7f0b203e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/DLJ;->F:I

    .line 1988081
    iget-object v0, p0, LX/DLJ;->y:Landroid/widget/ImageView;

    new-instance v1, LX/DLC;

    invoke-direct {v1, p0}, LX/DLC;-><init>(LX/DLJ;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1988082
    :cond_0
    :goto_0
    iget-object v0, p0, LX/DLJ;->z:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_1

    .line 1988083
    new-instance v0, LX/DLD;

    invoke-direct {v0, p0}, LX/DLD;-><init>(LX/DLJ;)V

    iput-object v0, p0, LX/DLJ;->z:Landroid/view/View$OnClickListener;

    .line 1988084
    :cond_1
    iget-object v0, p0, LX/DLJ;->v:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v2, 0x7f020818

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1988085
    iget-object v0, p0, LX/DLJ;->v:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/DLJ;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1988086
    iget-object v0, p0, LX/DLJ;->v:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v2, 0x7f0830b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1988087
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DLJ;->setClickable(Z)V

    .line 1988088
    return-void

    .line 1988089
    :cond_2
    if-nez p1, :cond_0

    iget-object v0, p0, LX/DLJ;->x:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    .line 1988090
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, LX/DLJ;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/DLJ;->x:Landroid/widget/ProgressBar;

    .line 1988091
    iget-object v0, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v1, 0x7f0b203e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/DLJ;->F:I

    goto :goto_0
.end method

.method private static h(LX/DLJ;)V
    .locals 2

    .prologue
    .line 1988115
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/DLJ;->a(LX/DLJ;Z)V

    .line 1988116
    iget-object v0, p0, LX/DLJ;->x:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 1988117
    iget v0, p0, LX/DLJ;->F:I

    iget v1, p0, LX/DLJ;->F:I

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->d(II)V

    .line 1988118
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 1988119
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DLJ;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1988120
    return-void
.end method

.method public static k(LX/DLJ;)V
    .locals 3

    .prologue
    .line 1988074
    iget-object v0, p0, LX/DLJ;->l:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/DLJ;->m:LX/DLU;

    invoke-interface {v1}, LX/DLU;->a()LX/DLU;

    move-result-object v1

    iget-object v2, p0, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->k()Ljava/lang/String;

    invoke-interface {v1}, LX/DLU;->b()LX/DLU;

    move-result-object v1

    iget-object v2, p0, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->m()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OriginalPostModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OriginalPostModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/DLU;->a(Ljava/lang/String;)LX/DLU;

    move-result-object v1

    invoke-interface {v1}, LX/DLU;->c()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, LX/DLJ;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1988075
    return-void
.end method


# virtual methods
.method public final a(LX/DLH;Ljava/lang/String;I)V
    .locals 11

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f000000    # 0.5f

    .line 1988036
    iput-object p2, p0, LX/DLJ;->s:Ljava/lang/String;

    .line 1988037
    iput p3, p0, LX/DLJ;->u:I

    .line 1988038
    iget-object v0, p1, LX/DLH;->a:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    .line 1988039
    iget-object v1, p1, LX/DLH;->b:LX/DLI;

    iput-object v1, p0, LX/DLJ;->t:LX/DLI;

    .line 1988040
    iput-object v0, p0, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    .line 1988041
    iget-object v1, p0, LX/DLJ;->p:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1988042
    iget-object v1, p0, LX/DLJ;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->n()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OwnerModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1988043
    iget-object v0, p0, LX/DLJ;->t:LX/DLI;

    .line 1988044
    sget-object v1, LX/DLG;->a:[I

    invoke-virtual {v0}, LX/DLI;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1988045
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1}, LX/DLJ;->setAlpha(F)V

    .line 1988046
    :goto_0
    sget-object v0, LX/DLG;->a:[I

    iget-object v1, p0, LX/DLJ;->t:LX/DLI;

    invoke-virtual {v1}, LX/DLI;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 1988047
    invoke-virtual {p0, v4}, LX/DLJ;->setAlpha(F)V

    .line 1988048
    new-instance v5, Ljava/util/Date;

    iget-object v6, p0, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-virtual {v6}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->a()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long/2addr v7, v9

    invoke-direct {v5, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 1988049
    iget-object v6, p0, LX/DLJ;->r:Landroid/widget/TextView;

    iget-object v7, p0, LX/DLJ;->k:LX/6RZ;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v5, v9}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1988050
    iget-object v5, p0, LX/DLJ;->v:Landroid/widget/ImageButton;

    iget-object v6, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v7, 0x7f020726

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1988051
    iget-object v5, p0, LX/DLJ;->v:Landroid/widget/ImageButton;

    iget-object v6, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v7, 0x7f0830b2

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1988052
    iget-object v5, p0, LX/DLJ;->v:Landroid/widget/ImageButton;

    iget-object v6, p0, LX/DLJ;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1988053
    iget-object v5, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    if-eqz v5, :cond_0

    .line 1988054
    iget-object v5, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    invoke-virtual {p0, v5}, LX/DLJ;->removeView(Landroid/view/View;)V

    .line 1988055
    :cond_0
    iget-object v5, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v6, 0x7f020945

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1988056
    iget-object v5, p0, LX/DLJ;->B:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v5}, LX/DLJ;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1988057
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/DLJ;->setClickable(Z)V

    .line 1988058
    :goto_1
    return-void

    .line 1988059
    :pswitch_0
    iget-object v0, p0, LX/DLJ;->r:Landroid/widget/TextView;

    iget-object v1, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v2, 0x7f0830ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1988060
    invoke-virtual {p0, v3}, LX/DLJ;->setAlpha(F)V

    .line 1988061
    invoke-static {p0}, LX/DLJ;->h(LX/DLJ;)V

    goto :goto_1

    .line 1988062
    :pswitch_1
    iget-object v0, p0, LX/DLJ;->r:Landroid/widget/TextView;

    iget-object v1, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v2, 0x7f0830ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1988063
    invoke-virtual {p0, v3}, LX/DLJ;->setAlpha(F)V

    .line 1988064
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/DLJ;->a(LX/DLJ;Z)V

    .line 1988065
    iget-object v0, p0, LX/DLJ;->y:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 1988066
    iget v0, p0, LX/DLJ;->F:I

    iget v1, p0, LX/DLJ;->F:I

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->d(II)V

    .line 1988067
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 1988068
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DLJ;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1988069
    goto :goto_1

    .line 1988070
    :pswitch_2
    iget-object v0, p0, LX/DLJ;->r:Landroid/widget/TextView;

    iget-object v1, p0, LX/DLJ;->j:Landroid/content/res/Resources;

    const v2, 0x7f0830ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1988071
    invoke-virtual {p0, v4}, LX/DLJ;->setAlpha(F)V

    .line 1988072
    invoke-static {p0}, LX/DLJ;->h(LX/DLJ;)V

    goto :goto_1

    .line 1988073
    :pswitch_3
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {p0, v1}, LX/DLJ;->setAlpha(F)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4639a7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1988032
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onDetachedFromWindow()V

    .line 1988033
    iget-object v1, p0, LX/DLJ;->D:LX/3Af;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/DLJ;->D:LX/3Af;

    invoke-virtual {v1}, LX/3Af;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1988034
    iget-object v1, p0, LX/DLJ;->D:LX/3Af;

    invoke-virtual {v1}, LX/3Af;->dismiss()V

    .line 1988035
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x3ac8edb8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
