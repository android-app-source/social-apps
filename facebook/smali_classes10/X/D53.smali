.class public final LX/D53;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "LX/0jT;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;)V
    .locals 0

    .prologue
    .line 1963248
    iput-object p1, p0, LX/D53;->a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1963249
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1963250
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1963251
    if-eqz p1, :cond_0

    .line 1963252
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1963253
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    .line 1963254
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1963255
    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1963256
    iget-object v1, p0, LX/D53;->a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;ZLjava/lang/String;)V

    .line 1963257
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1963258
    iget-object v1, p0, LX/D53;->a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v0

    invoke-static {v1, v0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;Z)V

    .line 1963259
    :cond_0
    return-void
.end method
