.class public final LX/DPz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9hN;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:Ljava/util/List;

.field public final synthetic d:Lcom/facebook/groups/info/GroupInfoAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoAdapter;ILjava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1993989
    iput-object p1, p0, LX/DPz;->d:Lcom/facebook/groups/info/GroupInfoAdapter;

    iput p2, p0, LX/DPz;->a:I

    iput-object p3, p0, LX/DPz;->b:Ljava/util/List;

    iput-object p4, p0, LX/DPz;->c:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/9hO;
    .locals 3

    .prologue
    .line 1993990
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, LX/DPz;->a:I

    if-ge v1, v0, :cond_1

    .line 1993991
    iget-object v0, p0, LX/DPz;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1993992
    iget-object v0, p0, LX/DPz;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1993993
    iget-object v2, p0, LX/DPz;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1993994
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-static {v0, v1}, LX/9hO;->a(Lcom/facebook/drawee/view/DraweeView;LX/1bf;)LX/9hO;

    move-result-object v0

    .line 1993995
    :goto_1
    return-object v0

    .line 1993996
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1993997
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
