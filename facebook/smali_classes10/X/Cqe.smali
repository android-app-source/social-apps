.class public final enum LX/Cqe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cqe;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cqe;

.field public static final enum OVERLAY_MEDIA:LX/Cqe;

.field public static final enum OVERLAY_VIEWPORT:LX/Cqe;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1940094
    new-instance v0, LX/Cqe;

    const-string v1, "OVERLAY_VIEWPORT"

    invoke-direct {v0, v1, v2}, LX/Cqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    .line 1940095
    new-instance v0, LX/Cqe;

    const-string v1, "OVERLAY_MEDIA"

    invoke-direct {v0, v1, v3}, LX/Cqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    .line 1940096
    const/4 v0, 0x2

    new-array v0, v0, [LX/Cqe;

    sget-object v1, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    aput-object v1, v0, v2

    sget-object v1, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    aput-object v1, v0, v3

    sput-object v0, LX/Cqe;->$VALUES:[LX/Cqe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1940097
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cqe;
    .locals 1

    .prologue
    .line 1940098
    const-class v0, LX/Cqe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cqe;

    return-object v0
.end method

.method public static values()[LX/Cqe;
    .locals 1

    .prologue
    .line 1940099
    sget-object v0, LX/Cqe;->$VALUES:[LX/Cqe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cqe;

    return-object v0
.end method
