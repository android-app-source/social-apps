.class public LX/Eb3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljavax/crypto/spec/SecretKeySpec;

.field public final b:Ljavax/crypto/spec/SecretKeySpec;

.field public final c:Ljavax/crypto/spec/IvParameterSpec;


# direct methods
.method public constructor <init>([B)V
    .locals 4

    .prologue
    .line 2142923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142924
    const/16 v0, 0x20

    const/16 v1, 0x20

    const/16 v2, 0x10

    :try_start_0
    invoke-static {p1, v0, v1, v2}, LX/Eco;->a([BIII)[[B

    move-result-object v0

    .line 2142925
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    const-string v3, "AES"

    invoke-direct {v1, v2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v1, p0, LX/Eb3;->a:Ljavax/crypto/spec/SecretKeySpec;

    .line 2142926
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    const-string v3, "HmacSHA256"

    invoke-direct {v1, v2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v1, p0, LX/Eb3;->b:Ljavax/crypto/spec/SecretKeySpec;

    .line 2142927
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    const/4 v2, 0x2

    aget-object v0, v0, v2

    invoke-direct {v1, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    iput-object v1, p0, LX/Eb3;->c:Ljavax/crypto/spec/IvParameterSpec;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2142928
    return-void

    .line 2142929
    :catch_0
    move-exception v0

    .line 2142930
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method
