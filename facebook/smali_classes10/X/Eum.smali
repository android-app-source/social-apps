.class public LX/Eum;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2179970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179971
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Eum;->b:Ljava/lang/String;

    .line 2179972
    iput-object p1, p0, LX/Eum;->a:LX/0Zb;

    .line 2179973
    return-void
.end method

.method public static a(LX/0QB;)LX/Eum;
    .locals 4

    .prologue
    .line 2179974
    const-class v1, LX/Eum;

    monitor-enter v1

    .line 2179975
    :try_start_0
    sget-object v0, LX/Eum;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2179976
    sput-object v2, LX/Eum;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2179977
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2179978
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2179979
    new-instance p0, LX/Eum;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/Eum;-><init>(LX/0Zb;)V

    .line 2179980
    move-object v0, p0

    .line 2179981
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2179982
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Eum;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2179983
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2179984
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/Eum;LX/Eul;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2179985
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p1, LX/Eul;->analyticsName:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "friends_center"

    .line 2179986
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2179987
    move-object v0, v0

    .line 2179988
    iget-object v1, p0, LX/Eum;->b:Ljava/lang/String;

    .line 2179989
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2179990
    move-object v0, v0

    .line 2179991
    return-object v0
.end method


# virtual methods
.method public final d(I)V
    .locals 3

    .prologue
    .line 2179992
    iget-object v0, p0, LX/Eum;->a:LX/0Zb;

    sget-object v1, LX/Eul;->FRIENDS_CENTER_SUGGESTIONS_TAB_IMPRESSION:LX/Eul;

    invoke-static {p0, v1}, LX/Eum;->a(LX/Eum;LX/Eul;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "number_of_suggestions_user_saw"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2179993
    return-void
.end method
