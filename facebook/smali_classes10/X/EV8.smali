.class public LX/EV8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2127419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/0Px;)LX/8Cj;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "LX/8Cj;"
        }
    .end annotation

    .prologue
    .line 2127420
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2127421
    const v0, 0x7f0b1698    # 1.8488E38f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 2127422
    new-instance v0, LX/8Cj;

    const/4 v2, 0x0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/4Ab;->d(F)LX/4Ab;

    move-result-object v5

    const v6, 0x106000d

    .line 2127423
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v7

    .line 2127424
    if-nez v7, :cond_1

    .line 2127425
    :cond_0
    :goto_0
    move v6, v6

    .line 2127426
    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6, v4}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v4

    const v5, 0x7f0b1697

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const v6, 0x7f0b1699

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, LX/8Cj;-><init>(LX/0Px;Landroid/view/View$OnClickListener;ILX/4Ab;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0

    .line 2127427
    :cond_1
    new-instance v8, Landroid/util/TypedValue;

    invoke-direct {v8}, Landroid/util/TypedValue;-><init>()V

    .line 2127428
    const v9, 0x7f01072b

    const/4 v10, 0x1

    invoke-virtual {v7, v9, v8, v10}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2127429
    iget v7, v8, Landroid/util/TypedValue;->resourceId:I

    if-eqz v7, :cond_0

    .line 2127430
    iget v6, v8, Landroid/util/TypedValue;->resourceId:I

    goto :goto_0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2127431
    iget-object v1, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2127432
    invoke-interface {v1}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2127433
    if-nez v1, :cond_1

    .line 2127434
    :cond_0
    :goto_0
    return v0

    .line 2127435
    :cond_1
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2127436
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EVb;)Z
    .locals 7

    .prologue
    .line 2127437
    const/4 v0, 0x0

    .line 2127438
    if-nez p0, :cond_2

    .line 2127439
    :cond_0
    :goto_0
    move v0, v0

    .line 2127440
    if-eqz v0, :cond_1

    invoke-static {p0}, LX/EV8;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2127441
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2127442
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2127443
    if-eqz v0, :cond_1

    check-cast p0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2127444
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2127445
    iget-object v3, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2127446
    invoke-interface {v3}, LX/9uc;->ck()Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    move-result-object v3

    .line 2127447
    if-nez v0, :cond_5

    move v0, v1

    .line 2127448
    :goto_2
    move v0, v0

    .line 2127449
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_3

    .line 2127450
    :cond_2
    iget-object v1, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2127451
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 2127452
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BLOCK_NO_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v1, v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_EM:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v1, v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_HYBRID:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v1, v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_BRICK:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eq v1, v2, :cond_3

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2127453
    :cond_5
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->NONE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    if-ne v3, v4, :cond_6

    move v0, v2

    .line 2127454
    goto :goto_2

    .line 2127455
    :cond_6
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 2127456
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-nez v0, :cond_8

    :cond_7
    move v0, v1

    .line 2127457
    goto :goto_2

    .line 2127458
    :cond_8
    iget-object v0, p1, LX/EVb;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_9

    .line 2127459
    iget-object v0, p1, LX/EVb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zB;

    .line 2127460
    if-eqz v0, :cond_9

    .line 2127461
    invoke-static {p0}, LX/2zB;->c(Lcom/facebook/video/videohome/data/VideoHomeItem;)Ljava/lang/String;

    move-result-object v5

    .line 2127462
    if-nez v5, :cond_d

    .line 2127463
    const/4 v5, 0x0

    .line 2127464
    :goto_4
    move v0, v5

    .line 2127465
    if-eqz v0, :cond_9

    move v0, v2

    .line 2127466
    goto :goto_2

    .line 2127467
    :cond_9
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v2

    .line 2127468
    goto :goto_2

    .line 2127469
    :cond_a
    iget-object v0, p1, LX/EVb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14v;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/14v;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    .line 2127470
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->SEAL:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    if-ne v3, v5, :cond_b

    .line 2127471
    iget-object v3, p1, LX/EVb;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 2127472
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v4, v5, :cond_e

    const/4 v4, 0x1

    :goto_5
    move v3, v4

    .line 2127473
    or-int/2addr v0, v3

    .line 2127474
    :cond_b
    if-nez v0, :cond_c

    move v0, v1

    goto/16 :goto_2

    :cond_c
    move v0, v2

    goto/16 :goto_2

    :cond_d
    iget-object v6, v0, LX/2zB;->b:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    goto :goto_4

    :cond_e
    const/4 v4, 0x0

    goto :goto_5
.end method

.method public static a(Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETB;LX/EVb;)Z
    .locals 3

    .prologue
    .line 2127475
    iget-object v0, p1, LX/ETB;->t:LX/ETQ;

    move-object v0, v0

    .line 2127476
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/ETQ;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2127477
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127478
    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2127479
    invoke-static {v0}, LX/EV8;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0, p2}, LX/EV8;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/EVb;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127480
    :cond_1
    const/4 v0, 0x1

    .line 2127481
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 6

    .prologue
    .line 2127482
    instance-of v0, p0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v0, :cond_0

    .line 2127483
    check-cast p0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    const/4 v1, 0x0

    .line 2127484
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2127485
    :goto_0
    move v0, v0

    .line 2127486
    :goto_1
    return v0

    :cond_0
    const/4 v1, 0x0

    .line 2127487
    invoke-static {p0}, LX/Cfu;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v3

    .line 2127488
    invoke-static {v3}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 2127489
    :goto_2
    move v0, v0

    .line 2127490
    goto :goto_1

    .line 2127491
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    invoke-virtual {v0}, LX/ETQ;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2127492
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2127493
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v3, :cond_2

    .line 2127494
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2127495
    goto :goto_0

    .line 2127496
    :cond_4
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_6

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2127497
    invoke-interface {v0}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v5, :cond_5

    .line 2127498
    const/4 v0, 0x1

    goto :goto_2

    .line 2127499
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_6
    move v0, v1

    .line 2127500
    goto :goto_2
.end method
