.class public final LX/DGz;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DH0;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Wk;

.field public c:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:LX/20Z;

.field public final synthetic e:LX/DH0;


# direct methods
.method public constructor <init>(LX/DH0;)V
    .locals 1

    .prologue
    .line 1981129
    iput-object p1, p0, LX/DGz;->e:LX/DH0;

    .line 1981130
    move-object v0, p1

    .line 1981131
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1981132
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1981108
    const-string v0, "LinkSetsShareSaveFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1981109
    if-ne p0, p1, :cond_1

    .line 1981110
    :cond_0
    :goto_0
    return v0

    .line 1981111
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1981112
    goto :goto_0

    .line 1981113
    :cond_3
    check-cast p1, LX/DGz;

    .line 1981114
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1981115
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1981116
    if-eq v2, v3, :cond_0

    .line 1981117
    iget-object v2, p0, LX/DGz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DGz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/DGz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1981118
    goto :goto_0

    .line 1981119
    :cond_5
    iget-object v2, p1, LX/DGz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1981120
    :cond_6
    iget-object v2, p0, LX/DGz;->b:LX/1Wk;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DGz;->b:LX/1Wk;

    iget-object v3, p1, LX/DGz;->b:LX/1Wk;

    invoke-virtual {v2, v3}, LX/1Wk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1981121
    goto :goto_0

    .line 1981122
    :cond_8
    iget-object v2, p1, LX/DGz;->b:LX/1Wk;

    if-nez v2, :cond_7

    .line 1981123
    :cond_9
    iget-object v2, p0, LX/DGz;->c:LX/1Pr;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/DGz;->c:LX/1Pr;

    iget-object v3, p1, LX/DGz;->c:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1981124
    goto :goto_0

    .line 1981125
    :cond_b
    iget-object v2, p1, LX/DGz;->c:LX/1Pr;

    if-nez v2, :cond_a

    .line 1981126
    :cond_c
    iget-object v2, p0, LX/DGz;->d:LX/20Z;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/DGz;->d:LX/20Z;

    iget-object v3, p1, LX/DGz;->d:LX/20Z;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1981127
    goto :goto_0

    .line 1981128
    :cond_d
    iget-object v2, p1, LX/DGz;->d:LX/20Z;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
