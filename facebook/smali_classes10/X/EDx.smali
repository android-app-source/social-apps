.class public LX/EDx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ECD;
.implements Lcom/facebook/webrtc/ConferenceCall$Listener;
.implements Lcom/facebook/webrtc/IWebrtcUiInterface;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static bj:I

.field private static final cC:Ljava/lang/Object;

.field private static final cj:LX/EDt;

.field private static final d:[J

.field private static final e:[J


# instance fields
.field public final A:LX/0Xl;

.field public final B:LX/0Xl;

.field private final C:LX/0aU;

.field public final D:Landroid/os/Handler;

.field public final E:LX/00H;

.field private final F:LX/EG0;

.field private final G:Landroid/os/Vibrator;

.field private final H:LX/12x;

.field private final I:LX/0lB;

.field public final J:LX/ECO;

.field public final K:LX/0yH;

.field public final L:LX/0u7;

.field public final M:LX/0Uh;

.field public final N:LX/2Oi;

.field private final O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/rtc/interfaces/IRtcUiCallback;",
            ">;"
        }
    .end annotation
.end field

.field public P:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/interfaces/IRtcUiCallback;",
            ">;"
        }
    .end annotation
.end field

.field public final Q:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/EG2;",
            ">;"
        }
    .end annotation
.end field

.field public final R:LX/EG4;

.field public final S:LX/0kL;

.field public T:LX/2Tm;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Landroid/telephony/PhoneStateListener;

.field private V:LX/2Zz;

.field public W:Landroid/os/IBinder;

.field public X:Landroid/os/IBinder$DeathRecipient;

.field private Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

.field public Z:LX/0Yb;

.field public a:LX/6c4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aA:Z

.field public aB:Z

.field public aC:Z

.field private aD:J

.field private aE:Ljava/lang/String;

.field public aF:Z

.field private aG:I

.field public aH:Z

.field public aI:J

.field private aJ:J

.field public aK:J

.field public aL:J

.field public aM:J

.field public aN:J

.field private aO:I

.field public aP:Z

.field public aQ:Z

.field public aR:Z

.field private aS:Z

.field public aT:Z

.field public aU:Ljava/lang/reflect/Method;

.field public aV:Z

.field private aW:Landroid/content/ServiceConnection;

.field public aX:Z

.field public aY:Z

.field public aZ:Z

.field public aa:LX/0Yb;

.field public ab:Z

.field public ac:J

.field public ad:LX/EFw;

.field public ae:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/EGE;",
            ">;"
        }
    .end annotation
.end field

.field public af:LX/EGE;

.field public ag:Z

.field public ah:I

.field public ai:Z

.field public aj:Z

.field public ak:J

.field public al:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public am:I

.field private an:Z

.field public ao:Z

.field public ap:Z

.field public aq:Z

.field public ar:LX/EDv;

.field public as:Z

.field public at:I

.field public au:I

.field public av:Ljava/lang/String;

.field public aw:J

.field private ax:Z

.field private ay:I

.field private az:I

.field public b:LX/3Eb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private bA:Z

.field private bB:Z

.field public final bC:LX/0ad;

.field public bD:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/Window;",
            ">;"
        }
    .end annotation
.end field

.field private bE:I

.field private bF:J

.field private bG:J

.field private bH:J

.field private bI:J

.field private bJ:J

.field public bK:Z

.field public bL:Landroid/view/View;

.field public bM:Z

.field public bN:LX/EDo;

.field private bO:Ljava/lang/String;

.field public bP:LX/03R;

.field public bQ:Z

.field public bR:LX/EDu;

.field private bS:Ljava/util/concurrent/ScheduledFuture;

.field public bT:Ljava/util/concurrent/ScheduledFuture;

.field public bU:Ljava/util/concurrent/ScheduledFuture;

.field public bV:Ljava/util/concurrent/ScheduledFuture;

.field public bW:Ljava/util/concurrent/ScheduledFuture;

.field public bX:Z

.field public bY:LX/03R;

.field public bZ:Z

.field public ba:Z

.field public bb:LX/EIq;

.field public bc:Z

.field public bd:Z

.field public be:Z

.field public bf:Z

.field public bg:Z

.field public bh:Z

.field private bi:LX/EDq;

.field private bk:LX/3RT;

.field public bl:LX/7TQ;

.field public bm:Z

.field public bn:Ljava/lang/String;

.field public bo:Ljava/lang/String;

.field public bp:Z

.field public bq:LX/EGe;

.field public br:Z

.field public bs:Z

.field public bt:Z

.field public bu:Z

.field public bv:LX/03R;

.field public bw:Z

.field public bx:Z

.field private by:Z

.field public bz:Z

.field public c:LX/EDC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private cA:LX/EFx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final cB:Ljava/lang/Runnable;

.field public ca:I

.field public cb:Z

.field public cc:Z

.field public cd:Z

.field public ce:Z

.field public cf:Z

.field public cg:Z

.field public ch:Z

.field public ci:LX/EHZ;

.field public ck:LX/EDt;

.field public cl:LX/EDs;

.field public cm:LX/EDr;

.field private final cn:LX/3A1;

.field public co:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public cp:LX/EDw;

.field private cq:LX/EDw;

.field public cr:Lcom/facebook/presence/ThreadPresenceManager;

.field private final cs:LX/ECY;

.field public final ct:LX/1Ml;

.field private final cu:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EC2;",
            ">;"
        }
    .end annotation
.end field

.field private cv:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private cw:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EG6;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public cx:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public cy:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79G;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private cz:LX/EFz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:Landroid/app/ActivityManager;

.field public g:[Ljava/lang/String;

.field public final h:Landroid/content/Context;

.field public final i:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final j:Lcom/facebook/content/SecureContextHelper;

.field private final k:LX/EG8;

.field public final l:LX/2S7;

.field public final m:LX/3GP;

.field public final n:LX/34I;

.field public final o:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final p:LX/ECx;

.field public final q:LX/2XQ;

.field public final r:Landroid/media/AudioManager;

.field private final s:LX/D9r;

.field public final t:Landroid/telephony/TelephonyManager;

.field public final u:Ljava/util/concurrent/Executor;

.field public final v:Ljava/util/concurrent/ScheduledExecutorService;

.field private final w:LX/EG9;

.field private final x:LX/0SG;

.field private final y:LX/0So;

.field private final z:LX/0Sh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2092992
    const/4 v0, 0x3

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, LX/EDx;->d:[J

    .line 2092993
    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_1

    sput-object v0, LX/EDx;->e:[J

    .line 2092994
    const/4 v0, 0x1

    sput v0, LX/EDx;->bj:I

    .line 2092995
    sget-object v0, LX/EDt;->TOP_RIGHT:LX/EDt;

    sput-object v0, LX/EDx;->cj:LX/EDt;

    .line 2092996
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/EDx;->cC:Ljava/lang/Object;

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x320
        0x72e
    .end array-data

    .line 2092997
    :array_1
    .array-data 8
        0x0
        0x64
        0x12c
        0x64
    .end array-data
.end method

.method public constructor <init>(LX/12x;LX/0Sh;LX/D9r;Landroid/media/AudioManager;LX/3GP;LX/0SG;LX/EG0;Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Xl;LX/00H;LX/0lB;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/os/Handler;LX/ECx;LX/0aU;LX/0Xl;LX/2S7;LX/EG9;LX/0So;LX/2XQ;LX/EG8;LX/ECP;LX/3A1;Lcom/facebook/content/SecureContextHelper;Landroid/telephony/TelephonyManager;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Vibrator;LX/3RT;LX/34I;LX/ECY;LX/1Ml;LX/0yH;Lcom/facebook/presence/ThreadPresenceManager;LX/0ad;Landroid/app/ActivityManager;LX/EG4;LX/0u7;LX/0kL;LX/0Uh;LX/2Oi;LX/EIq;)V
    .locals 9
    .param p10    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbAppBroadcast;
        .end annotation
    .end param
    .param p14    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p17    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p27    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p28    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2092998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2092999
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/EDx;->O:Ljava/util/List;

    .line 2093000
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->P:LX/0Px;

    .line 2093001
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v1, p0, LX/EDx;->Q:Ljava/util/concurrent/atomic/AtomicReference;

    .line 2093002
    const/4 v1, 0x0

    iput-object v1, p0, LX/EDx;->aU:Ljava/lang/reflect/Method;

    .line 2093003
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->aV:Z

    .line 2093004
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->bc:Z

    .line 2093005
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->bd:Z

    .line 2093006
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->br:Z

    .line 2093007
    sget-object v1, LX/03R;->UNSET:LX/03R;

    iput-object v1, p0, LX/EDx;->bv:LX/03R;

    .line 2093008
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->by:Z

    .line 2093009
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->bz:Z

    .line 2093010
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->bA:Z

    .line 2093011
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->bB:Z

    .line 2093012
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->bK:Z

    .line 2093013
    sget-object v1, LX/03R;->UNSET:LX/03R;

    iput-object v1, p0, LX/EDx;->bP:LX/03R;

    .line 2093014
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->bQ:Z

    .line 2093015
    const/4 v1, 0x0

    iput-object v1, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    .line 2093016
    const/4 v1, 0x0

    iput-object v1, p0, LX/EDx;->bV:Ljava/util/concurrent/ScheduledFuture;

    .line 2093017
    sget-object v1, LX/03R;->UNSET:LX/03R;

    iput-object v1, p0, LX/EDx;->bY:LX/03R;

    .line 2093018
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->cc:Z

    .line 2093019
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->cd:Z

    .line 2093020
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->ce:Z

    .line 2093021
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->cf:Z

    .line 2093022
    sget-object v1, LX/EDx;->cj:LX/EDt;

    iput-object v1, p0, LX/EDx;->ck:LX/EDt;

    .line 2093023
    sget-object v1, LX/EDs;->NONE:LX/EDs;

    iput-object v1, p0, LX/EDx;->cl:LX/EDs;

    .line 2093024
    sget-object v1, LX/EDr;->DISCONNECTED:LX/EDr;

    iput-object v1, p0, LX/EDx;->cm:LX/EDr;

    .line 2093025
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->cu:Ljava/util/List;

    .line 2093026
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->cv:LX/0Ot;

    .line 2093027
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->cw:LX/0Ot;

    .line 2093028
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->cx:LX/0Ot;

    .line 2093029
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->cy:LX/0Ot;

    .line 2093030
    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$47;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$47;-><init>(LX/EDx;)V

    iput-object v1, p0, LX/EDx;->cB:Ljava/lang/Runnable;

    .line 2093031
    iput-object p1, p0, LX/EDx;->H:LX/12x;

    .line 2093032
    iput-object p2, p0, LX/EDx;->z:LX/0Sh;

    .line 2093033
    iput-object p3, p0, LX/EDx;->s:LX/D9r;

    .line 2093034
    move-object/from16 v0, p35

    iput-object v0, p0, LX/EDx;->cr:Lcom/facebook/presence/ThreadPresenceManager;

    .line 2093035
    iput-object p4, p0, LX/EDx;->r:Landroid/media/AudioManager;

    .line 2093036
    iput-object p5, p0, LX/EDx;->m:LX/3GP;

    .line 2093037
    iput-object p6, p0, LX/EDx;->x:LX/0SG;

    .line 2093038
    move-object/from16 v0, p7

    iput-object v0, p0, LX/EDx;->F:LX/EG0;

    .line 2093039
    move-object/from16 v0, p8

    iput-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    .line 2093040
    move-object/from16 v0, p9

    iput-object v0, p0, LX/EDx;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2093041
    move-object/from16 v0, p10

    iput-object v0, p0, LX/EDx;->A:LX/0Xl;

    .line 2093042
    move-object/from16 v0, p11

    iput-object v0, p0, LX/EDx;->E:LX/00H;

    .line 2093043
    move-object/from16 v0, p12

    iput-object v0, p0, LX/EDx;->I:LX/0lB;

    .line 2093044
    move-object/from16 v0, p13

    iput-object v0, p0, LX/EDx;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2093045
    move-object/from16 v0, p14

    iput-object v0, p0, LX/EDx;->D:Landroid/os/Handler;

    .line 2093046
    move-object/from16 v0, p15

    iput-object v0, p0, LX/EDx;->p:LX/ECx;

    .line 2093047
    move-object/from16 v0, p16

    iput-object v0, p0, LX/EDx;->C:LX/0aU;

    .line 2093048
    move-object/from16 v0, p17

    iput-object v0, p0, LX/EDx;->B:LX/0Xl;

    .line 2093049
    move-object/from16 v0, p18

    iput-object v0, p0, LX/EDx;->l:LX/2S7;

    .line 2093050
    move-object/from16 v0, p19

    iput-object v0, p0, LX/EDx;->w:LX/EG9;

    .line 2093051
    move-object/from16 v0, p20

    iput-object v0, p0, LX/EDx;->y:LX/0So;

    .line 2093052
    move-object/from16 v0, p21

    iput-object v0, p0, LX/EDx;->q:LX/2XQ;

    .line 2093053
    move-object/from16 v0, p22

    iput-object v0, p0, LX/EDx;->k:LX/EG8;

    .line 2093054
    new-instance v1, LX/EDe;

    invoke-direct {v1, p0}, LX/EDe;-><init>(LX/EDx;)V

    move-object/from16 v0, p23

    invoke-virtual {v0, v1}, LX/ECP;->a(LX/EDe;)LX/ECO;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->J:LX/ECO;

    .line 2093055
    move-object/from16 v0, p24

    iput-object v0, p0, LX/EDx;->cn:LX/3A1;

    .line 2093056
    move-object/from16 v0, p25

    iput-object v0, p0, LX/EDx;->j:Lcom/facebook/content/SecureContextHelper;

    .line 2093057
    move-object/from16 v0, p26

    iput-object v0, p0, LX/EDx;->t:Landroid/telephony/TelephonyManager;

    .line 2093058
    move-object/from16 v0, p27

    iput-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    .line 2093059
    move-object/from16 v0, p28

    iput-object v0, p0, LX/EDx;->v:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2093060
    move-object/from16 v0, p29

    iput-object v0, p0, LX/EDx;->G:Landroid/os/Vibrator;

    .line 2093061
    move-object/from16 v0, p30

    iput-object v0, p0, LX/EDx;->bk:LX/3RT;

    .line 2093062
    move-object/from16 v0, p31

    iput-object v0, p0, LX/EDx;->n:LX/34I;

    .line 2093063
    move-object/from16 v0, p34

    iput-object v0, p0, LX/EDx;->K:LX/0yH;

    .line 2093064
    move-object/from16 v0, p39

    iput-object v0, p0, LX/EDx;->L:LX/0u7;

    .line 2093065
    const/4 v1, 0x0

    iput v1, p0, LX/EDx;->am:I

    .line 2093066
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->an:Z

    .line 2093067
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->bf:Z

    .line 2093068
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->bg:Z

    .line 2093069
    sget-object v1, LX/EDv;->STOPPED:LX/EDv;

    iput-object v1, p0, LX/EDx;->ar:LX/EDv;

    .line 2093070
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/EDx;->aq:Z

    .line 2093071
    iget-object v1, p0, LX/EDx;->q:LX/2XQ;

    invoke-virtual {p0}, LX/EDx;->aa()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/2XQ;->a(Z)V

    .line 2093072
    move-object/from16 v0, p33

    iput-object v0, p0, LX/EDx;->ct:LX/1Ml;

    .line 2093073
    move-object/from16 v0, p32

    iput-object v0, p0, LX/EDx;->cs:LX/ECY;

    .line 2093074
    move-object/from16 v0, p36

    iput-object v0, p0, LX/EDx;->bC:LX/0ad;

    .line 2093075
    move-object/from16 v0, p37

    iput-object v0, p0, LX/EDx;->f:Landroid/app/ActivityManager;

    .line 2093076
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, LX/EDx;->h:Landroid/content/Context;

    const v4, 0x7f080782

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/EDx;->h:Landroid/content/Context;

    const v4, 0x7f080783

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LX/EDx;->h:Landroid/content/Context;

    const v4, 0x7f080784

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v1, p0, LX/EDx;->g:[Ljava/lang/String;

    .line 2093077
    move-object/from16 v0, p38

    iput-object v0, p0, LX/EDx;->R:LX/EG4;

    .line 2093078
    move-object/from16 v0, p40

    iput-object v0, p0, LX/EDx;->S:LX/0kL;

    .line 2093079
    new-instance v1, LX/EDf;

    invoke-direct {v1, p0}, LX/EDf;-><init>(LX/EDx;)V

    iput-object v1, p0, LX/EDx;->V:LX/2Zz;

    .line 2093080
    move-object/from16 v0, p41

    iput-object v0, p0, LX/EDx;->M:LX/0Uh;

    .line 2093081
    move-object/from16 v0, p42

    iput-object v0, p0, LX/EDx;->N:LX/2Oi;

    .line 2093082
    const/4 v1, 0x3

    new-array v2, v1, [S

    const/4 v1, 0x0

    sget-short v3, LX/3Dx;->ej:S

    aput-short v3, v2, v1

    const/4 v1, 0x1

    sget-short v3, LX/3Dx;->ef:S

    aput-short v3, v2, v1

    const/4 v1, 0x2

    sget-short v3, LX/3Dx;->eb:S

    aput-short v3, v2, v1

    .line 2093083
    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x3

    if-ge v1, v3, :cond_0

    aget-short v3, v2, v1

    .line 2093084
    iget-boolean v4, p0, LX/EDx;->bM:Z

    iget-object v5, p0, LX/EDx;->bC:LX/0ad;

    sget-object v6, LX/0c0;->Cached:LX/0c0;

    sget-object v7, LX/0c1;->Off:LX/0c1;

    const/4 v8, 0x0

    invoke-interface {v5, v6, v7, v3, v8}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v3

    or-int/2addr v3, v4

    iput-boolean v3, p0, LX/EDx;->bM:Z

    .line 2093085
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2093086
    :cond_0
    iget-object v1, p0, LX/EDx;->M:LX/0Uh;

    const/16 v2, 0x515

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/EDx;->bK:Z

    .line 2093087
    invoke-direct {p0}, LX/EDx;->bH()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2093088
    iget-object v1, p0, LX/EDx;->cr:Lcom/facebook/presence/ThreadPresenceManager;

    sget-object v2, LX/2gE;->THREAD_PRESENCE_CAPABILITY_INSTANT:LX/2gE;

    invoke-virtual {v2}, LX/2gE;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/presence/ThreadPresenceManager;->a(I)V

    .line 2093089
    :goto_1
    invoke-static {p0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(LX/EDx;)V

    .line 2093090
    invoke-virtual {p0}, LX/EDx;->X()Z

    move-result v1

    invoke-static {v1}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Z)V

    .line 2093091
    invoke-direct {p0}, LX/EDx;->cD()Z

    move-result v1

    iput-boolean v1, p0, LX/EDx;->cg:Z

    .line 2093092
    iget-object v1, p0, LX/EDx;->h:Landroid/content/Context;

    check-cast v1, Landroid/app/Application;

    new-instance v2, LX/EDh;

    invoke-direct {v2, p0}, LX/EDh;-><init>(LX/EDx;)V

    invoke-virtual {v1, v2}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 2093093
    move-object/from16 v0, p43

    iput-object v0, p0, LX/EDx;->bb:LX/EIq;

    .line 2093094
    iget-object v1, p0, LX/EDx;->bb:LX/EIq;

    invoke-virtual {v1, p0}, LX/EIq;->a(LX/ECD;)V

    .line 2093095
    return-void

    .line 2093096
    :cond_1
    iget-object v1, p0, LX/EDx;->cr:Lcom/facebook/presence/ThreadPresenceManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/presence/ThreadPresenceManager;->a(I)V

    goto :goto_1
.end method

.method private A(Z)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 2093097
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    if-eqz v0, :cond_1

    .line 2093098
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    .line 2093099
    iget-boolean v7, v0, LX/EFw;->d:Z

    if-eqz v7, :cond_2

    .line 2093100
    const-wide/16 v7, -0x1

    .line 2093101
    :goto_0
    move-wide v2, v7

    .line 2093102
    const-wide/16 v4, -0x1

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, LX/EDx;->initializeCall(JJZ)V

    .line 2093103
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {p0}, LX/EDx;->U()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EFw;->b(Ljava/lang/String;)V

    .line 2093104
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    iget-object v1, p0, LX/EDx;->cp:LX/EDw;

    iget v1, v1, LX/EDw;->a:I

    iget-object v2, p0, LX/EDx;->cp:LX/EDw;

    iget v2, v2, LX/EDw;->b:I

    iget-object v3, p0, LX/EDx;->cp:LX/EDw;

    iget v3, v3, LX/EDw;->c:I

    invoke-virtual {v0, v1, v2, v3}, LX/EFw;->a(III)V

    .line 2093105
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v0, p1}, LX/EFw;->b(Z)V

    .line 2093106
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093107
    iget-object v0, p0, LX/EDx;->cy:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79G;

    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v6, v2}, LX/79G;->a(ZZLjava/lang/String;)V

    .line 2093108
    :cond_0
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    .line 2093109
    iget-boolean v1, v0, LX/EFw;->d:Z

    if-eqz v1, :cond_3

    .line 2093110
    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v7, v0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v7}, Lcom/facebook/webrtc/ConferenceCall;->callId()J

    move-result-wide v7

    goto :goto_0

    .line 2093111
    :cond_3
    iget-object v1, v0, LX/EFw;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->j()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2093112
    iget-object v1, v0, LX/EFw;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/79G;

    invoke-virtual {v1}, LX/79G;->c()V

    .line 2093113
    :cond_4
    iget-object v1, v0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v1}, Lcom/facebook/webrtc/ConferenceCall;->join()V

    goto :goto_1
.end method

.method public static B(LX/EDx;Z)Z
    .locals 1

    .prologue
    .line 2093114
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EDx;->ae:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 2093115
    :cond_0
    const/4 v0, 0x0

    .line 2093116
    :goto_0
    return v0

    :cond_1
    new-instance v0, LX/EDj;

    invoke-direct {v0, p0}, LX/EDj;-><init>(LX/EDx;)V

    invoke-static {p0, v0, p1}, LX/EDx;->a(LX/EDx;Lcom/android/internal/util/Predicate;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/EDx;LX/0Tn;I)I
    .locals 2

    .prologue
    .line 2093117
    iget-object v0, p0, LX/EDx;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2093118
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2093119
    :goto_0
    return p2

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/EDx;
    .locals 7

    .prologue
    .line 2093120
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2093121
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2093122
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2093123
    if-nez v1, :cond_0

    .line 2093124
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2093125
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2093126
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2093127
    sget-object v1, LX/EDx;->cC:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2093128
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2093129
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2093130
    :cond_1
    if-nez v1, :cond_4

    .line 2093131
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2093132
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2093133
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/EDx;->b(LX/0QB;)LX/EDx;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2093134
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2093135
    if-nez v1, :cond_2

    .line 2093136
    sget-object v0, LX/EDx;->cC:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2093137
    :goto_1
    if-eqz v0, :cond_3

    .line 2093138
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2093139
    :goto_3
    check-cast v0, LX/EDx;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2093140
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2093141
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2093142
    :catchall_1
    move-exception v0

    .line 2093143
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2093144
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2093145
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2093146
    :cond_2
    :try_start_8
    sget-object v0, LX/EDx;->cC:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static synthetic a(LX/EDx;JJZLjava/lang/String;)V
    .locals 8

    .prologue
    .line 2093147
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2093148
    iput-boolean p5, p0, LX/EDx;->bw:Z

    .line 2093149
    const-string v1, "instant_video"

    invoke-virtual {v1, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->bP:LX/03R;

    .line 2093150
    const-string v1, "pstn_upsell"

    invoke-virtual {v1, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, LX/EDx;->bQ:Z

    .line 2093151
    if-nez p5, :cond_0

    iget-object v1, p0, LX/EDx;->bP:LX/03R;

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v1, v3

    :goto_0
    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->bv:LX/03R;

    .line 2093152
    iput-wide p3, p0, LX/EDx;->ak:J

    .line 2093153
    iget-wide v5, p0, LX/EDx;->ac:J

    cmp-long v1, v5, p1

    if-eqz v1, :cond_2

    .line 2093154
    const-string v1, "WebrtcUiHandler"

    const-string v2, "Can\'t start call since original call is ended already"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093155
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 2093156
    goto :goto_0

    .line 2093157
    :cond_2
    iget-object v1, p0, LX/EDx;->bP:LX/03R;

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2093158
    invoke-static {p0}, LX/EDx;->bG(LX/EDx;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v1, :cond_6

    .line 2093159
    iget-object v1, p0, LX/EDx;->T:LX/2Tm;

    invoke-virtual {p0}, LX/EDx;->U()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v2, v2, v2}, LX/2Tm;->a(Ljava/lang/String;ZZZ)V

    .line 2093160
    :cond_3
    iget-boolean v1, p0, LX/EDx;->bQ:Z

    if-eqz v1, :cond_4

    .line 2093161
    iget-object v1, p0, LX/EDx;->Q:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EG2;

    .line 2093162
    if-eqz v1, :cond_7

    invoke-interface {v1}, LX/EG2;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v1, :cond_7

    .line 2093163
    iget-object v1, p0, LX/EDx;->T:LX/2Tm;

    invoke-virtual {p0}, LX/EDx;->U()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v2, v2, v2}, LX/2Tm;->a(Ljava/lang/String;ZZZ)V

    .line 2093164
    :cond_4
    invoke-virtual {p0}, LX/EDx;->at()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2093165
    iget-object v1, p0, LX/EDx;->bb:LX/EIq;

    move-object v1, v1

    .line 2093166
    invoke-virtual {v1}, LX/EIq;->a()V

    .line 2093167
    :cond_5
    iget-object v1, p0, LX/EDx;->K:LX/0yH;

    sget-object v4, LX/0yY;->VOIP_INCOMING_CALL_INTERSTITIAL:LX/0yY;

    invoke-virtual {v1, v4}, LX/0yH;->a(LX/0yY;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2093168
    iget-object v1, p0, LX/EDx;->bC:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-short v5, LX/3Dx;->fm:S

    invoke-interface {v1, v4, v5, v2}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2093169
    :goto_2
    if-eqz v3, :cond_9

    .line 2093170
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/EDx;->h:Landroid/content/Context;

    const-class v3, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2093171
    const-string v2, "ACTION_INCOMING_CALL"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2093172
    const-string v2, "EXTRA_DIRECT_VIDEO"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2093173
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2093174
    iget-object v2, p0, LX/EDx;->j:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2093175
    goto/16 :goto_1

    .line 2093176
    :cond_6
    sget-object v1, LX/7TQ;->CallEndNoUIError:LX/7TQ;

    invoke-virtual {p0, v1}, LX/EDx;->a(LX/7TQ;)V

    goto/16 :goto_1

    .line 2093177
    :cond_7
    sget-object v1, LX/7TQ;->CallEndNoUIError:LX/7TQ;

    invoke-virtual {p0, v1}, LX/EDx;->a(LX/7TQ;)V

    goto/16 :goto_1

    :cond_8
    move v3, v2

    .line 2093178
    goto :goto_2

    .line 2093179
    :cond_9
    invoke-virtual {p0, p5, v2}, LX/EDx;->b(ZZ)V

    goto/16 :goto_1

    :cond_a
    move v3, v2

    goto :goto_2
.end method

.method private static a(LX/EDx;LX/0Ot;LX/6c4;LX/0Ot;LX/3Eb;LX/EDC;LX/0Ot;LX/0Ot;LX/EFz;LX/EFx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EDx;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;",
            ">;",
            "LX/6c4;",
            "LX/0Ot",
            "<",
            "LX/EG6;",
            ">;",
            "LX/3Eb;",
            "LX/EDC;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/79G;",
            ">;",
            "LX/EFz;",
            "LX/EFx;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2093180
    iput-object p1, p0, LX/EDx;->cv:LX/0Ot;

    iput-object p2, p0, LX/EDx;->a:LX/6c4;

    iput-object p3, p0, LX/EDx;->cw:LX/0Ot;

    iput-object p4, p0, LX/EDx;->b:LX/3Eb;

    iput-object p5, p0, LX/EDx;->c:LX/EDC;

    iput-object p6, p0, LX/EDx;->cx:LX/0Ot;

    iput-object p7, p0, LX/EDx;->cy:LX/0Ot;

    iput-object p8, p0, LX/EDx;->cz:LX/EFz;

    iput-object p9, p0, LX/EDx;->cA:LX/EFx;

    return-void
.end method

.method public static a(LX/EDx;LX/0Px;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 2093181
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EDx;->ae:Ljava/util/Map;

    .line 2093182
    if-eqz p1, :cond_1

    .line 2093183
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_1

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2093184
    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v4

    .line 2093185
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2093186
    iget-object v1, p0, LX/EDx;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2093187
    iget-object v3, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v3

    .line 2093188
    invoke-virtual {v2, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2093189
    new-instance v1, LX/EGE;

    sget-object v3, LX/EGD;->UNKNOWN:LX/EGD;

    invoke-direct/range {v1 .. v7}, LX/EGE;-><init>(Ljava/lang/String;LX/EGD;JJ)V

    .line 2093190
    iget-object v2, p0, LX/EDx;->ae:Ljava/util/Map;

    iget-object v3, v1, LX/EGE;->b:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2093191
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2093192
    :cond_1
    new-instance v1, LX/EGE;

    iget-object v0, p0, LX/EDx;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2093193
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2093194
    sget-object v3, LX/EGD;->CONNECTED:LX/EGD;

    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v4

    invoke-direct/range {v1 .. v7}, LX/EGE;-><init>(Ljava/lang/String;LX/EGD;JJ)V

    .line 2093195
    iget-object v0, p0, LX/EDx;->ae:Ljava/util/Map;

    iget-object v2, v1, LX/EGE;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2093196
    invoke-static {p0}, LX/EDx;->cC(LX/EDx;)V

    .line 2093197
    const/4 v0, 0x1

    iput v0, p0, LX/EDx;->ah:I

    .line 2093198
    return-void
.end method

.method public static a(LX/EDx;Lcom/facebook/rtc/helpers/RtcCallStartParams;Lcom/facebook/webrtc/ConferenceCall;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 2093199
    invoke-direct {p0}, LX/EDx;->bv()V

    .line 2093200
    iput-wide v4, p0, LX/EDx;->ac:J

    .line 2093201
    iget-wide v0, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    iput-wide v0, p0, LX/EDx;->ak:J

    .line 2093202
    iget-boolean v0, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->bv:LX/03R;

    .line 2093203
    iget-boolean v0, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    iput-boolean v0, p0, LX/EDx;->bw:Z

    .line 2093204
    invoke-virtual {p1}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->c()Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->bP:LX/03R;

    .line 2093205
    invoke-virtual {p1}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d()Z

    move-result v0

    iput-boolean v0, p0, LX/EDx;->bQ:Z

    .line 2093206
    iput-boolean v6, p0, LX/EDx;->aB:Z

    .line 2093207
    iget-wide v0, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->l:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_4

    .line 2093208
    iget-object v0, p0, LX/EDx;->x:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->l:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/EDx;->aD:J

    .line 2093209
    iget-wide v0, p0, LX/EDx;->aD:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    iget-wide v0, p0, LX/EDx;->aD:J

    const-wide/32 v2, 0x14997000

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 2093210
    :cond_0
    iput-wide v4, p0, LX/EDx;->aD:J

    .line 2093211
    :cond_1
    :goto_0
    invoke-static {p0, p2}, LX/EDx;->a(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;)V

    .line 2093212
    iput-object p1, p0, LX/EDx;->Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    .line 2093213
    iget-object v0, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    iput-object v0, p0, LX/EDx;->aE:Ljava/lang/String;

    .line 2093214
    iget-object v0, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->i:Ljava/lang/String;

    iput-object v0, p0, LX/EDx;->bO:Ljava/lang/String;

    .line 2093215
    iget-object v0, p1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->h:Ljava/lang/String;

    .line 2093216
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2093217
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->al:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2093218
    :cond_2
    invoke-static {p0}, LX/EDx;->bz(LX/EDx;)V

    .line 2093219
    invoke-direct {p0}, LX/EDx;->bB()V

    .line 2093220
    invoke-virtual {p1}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2093221
    invoke-static {p0, v6}, LX/EDx;->q(LX/EDx;Z)V

    .line 2093222
    :cond_3
    iget-boolean v0, p0, LX/EDx;->bQ:Z

    if-nez v0, :cond_5

    .line 2093223
    iget-object v0, p0, LX/EDx;->b:LX/3Eb;

    iget-wide v2, p0, LX/EDx;->ac:J

    invoke-virtual {v0, v2, v3, v6}, LX/3Eb;->a(JZ)V

    .line 2093224
    :goto_1
    return-void

    .line 2093225
    :cond_4
    iput-wide v4, p0, LX/EDx;->aD:J

    goto :goto_0

    .line 2093226
    :cond_5
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EDx;->r(Z)V

    goto :goto_1
.end method

.method public static a(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;)V
    .locals 4

    .prologue
    .line 2093227
    if-nez p1, :cond_0

    .line 2093228
    const/4 v0, 0x0

    iput-object v0, p0, LX/EDx;->ad:LX/EFw;

    .line 2093229
    :goto_0
    return-void

    .line 2093230
    :cond_0
    iget-object v0, p0, LX/EDx;->cA:LX/EFx;

    .line 2093231
    new-instance v1, LX/EFw;

    invoke-direct {v1, p1}, LX/EFw;-><init>(Lcom/facebook/webrtc/ConferenceCall;)V

    .line 2093232
    const/16 v2, 0x3266

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x3257

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 2093233
    iput-object v2, v1, LX/EFw;->b:LX/0Ot;

    iput-object v3, v1, LX/EFw;->c:LX/0Ot;

    .line 2093234
    move-object v0, v1

    .line 2093235
    iput-object v0, p0, LX/EDx;->ad:LX/EFw;

    goto :goto_0
.end method

.method public static synthetic a(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;Ljava/lang/String;[Ljava/lang/String;IZ)V
    .locals 10

    .prologue
    .line 2093236
    const-wide/16 v2, 0x0

    const/4 v9, 0x3

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2093237
    :try_start_0
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    :goto_0
    move-object v1, p0

    move-object v6, p1

    .line 2093238
    invoke-static/range {v1 .. v6}, LX/EDx;->a$redex0(LX/EDx;JJLcom/facebook/webrtc/ConferenceCall;)V

    .line 2093239
    iput-boolean p5, p0, LX/EDx;->aj:Z

    .line 2093240
    const/4 v0, 0x1

    .line 2093241
    const/4 v1, 0x2

    if-eq p4, v1, :cond_0

    if-ne p4, v0, :cond_9

    :cond_0
    :goto_1
    move v0, v0

    .line 2093242
    if-eqz v0, :cond_3

    .line 2093243
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/EDx;->ak:J

    .line 2093244
    if-ne p4, v7, :cond_1

    move v0, v7

    :goto_2
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->bv:LX/03R;

    .line 2093245
    if-ne p4, v7, :cond_2

    :goto_3
    iput-boolean v7, p0, LX/EDx;->bw:Z

    .line 2093246
    :goto_4
    invoke-virtual {p0, v8, v8}, LX/EDx;->b(ZZ)V

    .line 2093247
    return-void

    .line 2093248
    :catch_0
    move-exception v0

    .line 2093249
    const-string v1, "WebrtcUiHandler"

    const-string v4, "Error getting conference caller id"

    invoke-static {v1, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v4, v2

    goto :goto_0

    :cond_1
    move v0, v8

    .line 2093250
    goto :goto_2

    :cond_2
    move v7, v8

    .line 2093251
    goto :goto_3

    .line 2093252
    :cond_3
    invoke-virtual {p0}, LX/EDx;->ap()Ljava/lang/String;

    move-result-object v0

    .line 2093253
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2093254
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->al:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2093255
    iget-boolean v0, p0, LX/EDx;->ao:Z

    if-eqz v0, :cond_4

    .line 2093256
    if-ne p4, v9, :cond_5

    move v0, v7

    :goto_5
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->bv:LX/03R;

    .line 2093257
    if-ne p4, v9, :cond_6

    :goto_6
    iput-boolean v7, p0, LX/EDx;->bw:Z

    .line 2093258
    :cond_4
    iget-object v0, p0, LX/EDx;->R:LX/EG4;

    invoke-interface {v0}, LX/EG4;->d()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2093259
    invoke-static {v0}, LX/3A0;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)[Ljava/lang/String;

    move-result-object v0

    .line 2093260
    if-nez v0, :cond_7

    .line 2093261
    const-string v0, "WebrtcUiHandler"

    const-string v1, "Cannot get participants from ThreadSummary, falling back to participants list"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093262
    invoke-static {p3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/EDx;->a(LX/EDx;LX/0Px;)V

    goto :goto_4

    :cond_5
    move v0, v8

    .line 2093263
    goto :goto_5

    :cond_6
    move v7, v8

    .line 2093264
    goto :goto_6

    .line 2093265
    :cond_7
    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/EDx;->a(LX/EDx;LX/0Px;)V

    goto :goto_4

    .line 2093266
    :cond_8
    const-string v0, "WebrtcUiHandler"

    const-string v1, "Unable to get group thread id for conference call"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic a(LX/EDx;ZIIZZIIIZ)V
    .locals 6

    .prologue
    .line 2093267
    move-object v0, p0

    move v1, p1

    move v2, p4

    move v3, p5

    move v4, p6

    move v5, p9

    invoke-static/range {v0 .. v5}, LX/EDx;->a(LX/EDx;ZZZIZ)V

    return-void
.end method

.method private static a(LX/EDx;ZZZ)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2092965
    iget-boolean v2, p0, LX/EDx;->an:Z

    if-eqz v2, :cond_0

    .line 2092966
    iget-wide v2, p0, LX/EDx;->bH:J

    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v4

    iget-wide v6, p0, LX/EDx;->bJ:J

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/EDx;->bH:J

    .line 2092967
    :cond_0
    if-eqz p1, :cond_1

    .line 2092968
    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v2

    iput-wide v2, p0, LX/EDx;->bJ:J

    .line 2092969
    :cond_1
    iput-boolean p1, p0, LX/EDx;->an:Z

    .line 2092970
    if-eqz p3, :cond_2

    .line 2092971
    if-nez p1, :cond_5

    iget-object v2, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2092972
    iget-object v2, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v2, v1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 2092973
    :cond_2
    :goto_0
    if-nez p2, :cond_3

    if-nez p1, :cond_3

    iget-object v2, p0, LX/EDx;->b:LX/3Eb;

    iget-wide v4, p0, LX/EDx;->ac:J

    invoke-virtual {v2, v4, v5}, LX/3Eb;->a(J)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2092974
    :cond_3
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2092975
    iget-object v2, p0, LX/EDx;->ad:LX/EFw;

    iget-boolean v3, p0, LX/EDx;->an:Z

    if-nez v3, :cond_6

    .line 2092976
    :goto_1
    iget-boolean v1, v2, LX/EFw;->d:Z

    if-eqz v1, :cond_a

    .line 2092977
    :cond_4
    :goto_2
    return-void

    .line 2092978
    :cond_5
    if-eqz p1, :cond_2

    iget-object v2, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2092979
    iget-object v2, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v2, v0}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 2092980
    goto :goto_1

    .line 2092981
    :cond_7
    iget-object v2, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v2, :cond_4

    .line 2092982
    iget-object v2, p0, LX/EDx;->T:LX/2Tm;

    iget-boolean v3, p0, LX/EDx;->an:Z

    if-nez v3, :cond_9

    .line 2092983
    :goto_3
    invoke-virtual {v2}, LX/2Tm;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2092984
    iget-object v1, v2, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v1, v0}, Lcom/facebook/webrtc/WebrtcEngine;->setAudioOn(Z)V

    .line 2092985
    :cond_8
    goto :goto_2

    :cond_9
    move v0, v1

    goto :goto_3

    .line 2092986
    :cond_a
    iget-object v1, v2, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v1, v0}, Lcom/facebook/webrtc/ConferenceCall;->configureAudio(Z)V

    goto :goto_2
.end method

.method private static a(LX/EDx;ZZZIZ)V
    .locals 4

    .prologue
    .line 2093280
    if-eqz p1, :cond_1

    iget-wide v0, p0, LX/EDx;->aM:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2093281
    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v0

    iput-wide v0, p0, LX/EDx;->aM:J

    .line 2093282
    iget-boolean v0, p0, LX/EDx;->aB:Z

    move v0, v0

    .line 2093283
    if-eqz v0, :cond_0

    .line 2093284
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    invoke-virtual {v0}, LX/ECO;->f()V

    .line 2093285
    iget-boolean v0, p0, LX/EDx;->bQ:Z

    move v0, v0

    .line 2093286
    if-nez v0, :cond_0

    .line 2093287
    invoke-static {p0}, LX/EDx;->cf(LX/EDx;)V

    .line 2093288
    :cond_0
    invoke-static {p0}, LX/EDx;->cH(LX/EDx;)V

    .line 2093289
    :cond_1
    invoke-static {}, LX/EDr;->values()[LX/EDr;

    move-result-object v0

    aget-object v0, v0, p4

    iput-object v0, p0, LX/EDx;->cm:LX/EDr;

    .line 2093290
    iget-boolean v0, p0, LX/EDx;->aH:Z

    if-ne v0, p1, :cond_7

    .line 2093291
    iget-boolean v0, p0, LX/EDx;->aH:Z

    if-eqz v0, :cond_2

    .line 2093292
    if-eqz p2, :cond_5

    .line 2093293
    sget-object v0, LX/EDs;->WEAK_CONNECTION:LX/EDs;

    iput-object v0, p0, LX/EDx;->cl:LX/EDs;

    .line 2093294
    :cond_2
    :goto_0
    iput-boolean p1, p0, LX/EDx;->aH:Z

    .line 2093295
    iget-boolean v0, p0, LX/EDx;->cc:Z

    if-eq v0, p5, :cond_3

    .line 2093296
    iput-boolean p5, p0, LX/EDx;->cc:Z

    .line 2093297
    iget-object v0, p0, LX/EDx;->cu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC2;

    .line 2093298
    invoke-interface {v0}, LX/EC2;->a()V

    goto :goto_1

    .line 2093299
    :cond_3
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_4

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2093300
    invoke-virtual {v0}, LX/EC0;->g()V

    .line 2093301
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2093302
    :cond_4
    return-void

    .line 2093303
    :cond_5
    if-eqz p3, :cond_6

    .line 2093304
    sget-object v0, LX/EDs;->WEAK_VIDEO_CONNECTION:LX/EDs;

    iput-object v0, p0, LX/EDx;->cl:LX/EDs;

    goto :goto_0

    .line 2093305
    :cond_6
    sget-object v0, LX/EDs;->NORMAL:LX/EDs;

    iput-object v0, p0, LX/EDx;->cl:LX/EDs;

    goto :goto_0

    .line 2093306
    :cond_7
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2093307
    iget-boolean v0, p0, LX/EDx;->aB:Z

    move v0, v0

    .line 2093308
    if-eqz v0, :cond_8

    invoke-virtual {p0}, LX/EDx;->aR()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2093309
    :cond_8
    if-eqz p1, :cond_e

    .line 2093310
    iget-boolean v0, p0, LX/EDx;->aB:Z

    if-nez v0, :cond_c

    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    invoke-virtual {v0}, LX/ECO;->b()Z

    move-result v0

    if-nez v0, :cond_c

    .line 2093311
    :cond_9
    :goto_3
    if-eqz p1, :cond_b

    .line 2093312
    iget-object v0, p0, LX/EDx;->cl:LX/EDs;

    sget-object v1, LX/EDs;->NONE:LX/EDs;

    if-ne v0, v1, :cond_a

    .line 2093313
    sget-object v0, LX/EDs;->NORMAL:LX/EDs;

    iput-object v0, p0, LX/EDx;->cl:LX/EDs;

    goto :goto_0

    .line 2093314
    :cond_a
    sget-object v0, LX/EDs;->RECONNECTED:LX/EDs;

    iput-object v0, p0, LX/EDx;->cl:LX/EDs;

    goto :goto_0

    .line 2093315
    :cond_b
    sget-object v0, LX/EDs;->RECONNECTING:LX/EDs;

    iput-object v0, p0, LX/EDx;->cl:LX/EDs;

    goto :goto_0

    .line 2093316
    :cond_c
    iget-boolean v0, p0, LX/EDx;->aT:Z

    if-eqz v0, :cond_d

    .line 2093317
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EDx;->aT:Z

    goto :goto_3

    .line 2093318
    :cond_d
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    sget-object v1, LX/ECN;->CONNECT:LX/ECN;

    invoke-virtual {v0, v1}, LX/ECO;->a(LX/ECN;)V

    goto :goto_3

    .line 2093319
    :cond_e
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    sget-object v1, LX/ECN;->DISCONNECT:LX/ECN;

    invoke-virtual {v0, v1}, LX/ECO;->a(LX/ECN;)V

    goto :goto_3
.end method

.method public static a(LX/EDx;[Ljava/lang/String;[I)V
    .locals 17

    .prologue
    .line 2093320
    const/4 v6, 0x0

    .line 2093321
    const/4 v5, 0x0

    .line 2093322
    const/4 v4, 0x0

    .line 2093323
    const/4 v3, 0x0

    .line 2093324
    if-nez p1, :cond_1

    .line 2093325
    const-string v2, "WebrtcUiHandler"

    const-string v3, "Empty new users for user state update."

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093326
    :cond_0
    :goto_0
    return-void

    .line 2093327
    :cond_1
    if-nez p2, :cond_2

    .line 2093328
    const-string v2, "WebrtcUiHandler"

    const-string v3, "Empty new states for user state update."

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2093329
    :cond_2
    move-object/from16 v0, p1

    array-length v2, v0

    move-object/from16 v0, p2

    array-length v7, v0

    if-eq v2, v7, :cond_3

    .line 2093330
    const-string v2, "WebrtcUiHandler"

    const-string v3, "Mismatch length between new users and new sates for user state update."

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2093331
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->ae:Ljava/util/Map;

    if-nez v2, :cond_4

    .line 2093332
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/EDx;->a(LX/EDx;LX/0Px;)V

    .line 2093333
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v16

    .line 2093334
    const/4 v2, 0x0

    move v10, v2

    move v11, v3

    move v12, v4

    move v13, v5

    move v14, v6

    :goto_1
    move-object/from16 v0, p1

    array-length v2, v0

    if-ge v10, v2, :cond_a

    .line 2093335
    aget-object v4, p1, v10

    .line 2093336
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v2}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 2093337
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EGE;

    iget-object v2, v2, LX/EGE;->a:LX/EGD;

    move-object v15, v2

    .line 2093338
    :goto_2
    invoke-static {}, LX/EGD;->values()[LX/EGD;

    move-result-object v2

    aget v3, p2, v10

    aget-object v5, v2, v3

    .line 2093339
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2093340
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EGE;

    .line 2093341
    iput-object v5, v2, LX/EGE;->a:LX/EGD;

    .line 2093342
    :goto_3
    sget-object v3, LX/EGD;->CONNECTED:LX/EGD;

    if-ne v5, v3, :cond_9

    .line 2093343
    add-int/lit8 v14, v14, 0x1

    .line 2093344
    :cond_5
    :goto_4
    sget-object v3, LX/EGD;->CONNECTED:LX/EGD;

    if-eq v15, v3, :cond_6

    sget-object v3, LX/EGD;->CONNECTED:LX/EGD;

    if-ne v5, v3, :cond_6

    .line 2093345
    invoke-virtual/range {p0 .. p0}, LX/EDx;->aU()J

    move-result-wide v6

    iput-wide v6, v2, LX/EGE;->f:J

    .line 2093346
    const/4 v13, 0x1

    .line 2093347
    :cond_6
    sget-object v3, LX/EGD;->CONNECTED:LX/EGD;

    if-ne v15, v3, :cond_12

    sget-object v3, LX/EGD;->CONNECTED:LX/EGD;

    if-eq v5, v3, :cond_12

    .line 2093348
    const-wide/16 v4, 0x0

    iput-wide v4, v2, LX/EGE;->f:J

    .line 2093349
    const/4 v12, 0x1

    move v3, v11

    move v4, v12

    move v5, v13

    move v6, v14

    .line 2093350
    :goto_5
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    move v11, v3

    move v12, v4

    move v13, v5

    move v14, v6

    goto :goto_1

    .line 2093351
    :cond_7
    sget-object v2, LX/EGD;->UNKNOWN:LX/EGD;

    move-object v15, v2

    goto :goto_2

    .line 2093352
    :cond_8
    new-instance v3, LX/EGE;

    invoke-virtual/range {p0 .. p0}, LX/EDx;->aU()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    invoke-direct/range {v3 .. v9}, LX/EGE;-><init>(Ljava/lang/String;LX/EGD;JJ)V

    .line 2093353
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v3

    goto :goto_3

    .line 2093354
    :cond_9
    sget-object v3, LX/EGD;->RINGING:LX/EGD;

    if-ne v5, v3, :cond_5

    .line 2093355
    const/4 v11, 0x1

    goto :goto_4

    .line 2093356
    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, LX/EDx;->ah:I

    invoke-virtual/range {p0 .. p0}, LX/EDx;->bk()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/EDx;->ah:I

    .line 2093357
    invoke-virtual/range {p0 .. p0}, LX/EDx;->j()Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->af:LX/EGE;

    if-eqz v2, :cond_c

    .line 2093358
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->ae:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/EDx;->af:LX/EGE;

    iget-object v3, v3, LX/EGE;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->af:LX/EGE;

    iget-object v2, v2, LX/EGE;->a:LX/EGD;

    sget-object v3, LX/EGD;->CONNECTED:LX/EGD;

    if-eq v2, v3, :cond_c

    .line 2093359
    :cond_b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, LX/EDx;->af:LX/EGE;

    .line 2093360
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/EDx;->ag:Z

    .line 2093361
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/EDx;->aZ:Z

    if-nez v2, :cond_d

    const/4 v2, 0x2

    move/from16 v0, v16

    if-gt v0, v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_d

    .line 2093362
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/EDx;->aZ:Z

    .line 2093363
    invoke-static/range {p0 .. p0}, LX/EDx;->cB(LX/EDx;)V

    .line 2093364
    :cond_d
    invoke-static/range {p0 .. p0}, LX/EDx;->cC(LX/EDx;)V

    .line 2093365
    invoke-virtual/range {p0 .. p0}, LX/EDx;->j()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2093366
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_6
    if-ge v3, v4, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EC0;

    .line 2093367
    invoke-virtual {v2}, LX/EC0;->p()V

    .line 2093368
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 2093369
    :cond_e
    invoke-virtual/range {p0 .. p0}, LX/EDx;->j()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2093370
    if-eqz v13, :cond_10

    .line 2093371
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->J:LX/ECO;

    sget-object v3, LX/ECN;->CONFERENCE_JOIN:LX/ECN;

    invoke-virtual {v2, v3}, LX/ECO;->a(LX/ECN;)V

    .line 2093372
    :cond_f
    :goto_7
    invoke-virtual/range {p0 .. p0}, LX/EDx;->af()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2093373
    if-lez v14, :cond_11

    .line 2093374
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v2}, LX/EFw;->c()Z

    move-result v2

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/EDx;->ba:Z

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, LX/EDx;->e(LX/EDx;ZZ)V

    .line 2093375
    invoke-static/range {p0 .. p0}, LX/EDx;->bw(LX/EDx;)V

    goto/16 :goto_0

    .line 2093376
    :cond_10
    if-eqz v12, :cond_f

    .line 2093377
    move-object/from16 v0, p0

    iget-object v2, v0, LX/EDx;->J:LX/ECO;

    sget-object v3, LX/ECN;->CONFERENCE_LEAVE:LX/ECN;

    invoke-virtual {v2, v3}, LX/ECO;->a(LX/ECN;)V

    goto :goto_7

    .line 2093378
    :cond_11
    if-eqz v11, :cond_0

    invoke-static/range {p0 .. p0}, LX/EDx;->cz(LX/EDx;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2093379
    invoke-static/range {p0 .. p0}, LX/EDx;->ch(LX/EDx;)V

    goto/16 :goto_0

    :cond_12
    move v3, v11

    move v4, v12

    move v5, v13

    move v6, v14

    goto/16 :goto_5
.end method

.method private a(JJ)Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 2093380
    iget-object v0, p0, LX/EDx;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, LX/EGK;->a(Ljava/lang/Long;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    .line 2093381
    iget-object v1, p0, LX/EDx;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2093382
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6, p3, p4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2093383
    sget-object v6, LX/EGK;->m:LX/0Tn;

    invoke-virtual {v6, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v5

    check-cast v5, LX/0Tn;

    const-string v6, "/thread_custom_notifications_enabled"

    invoke-virtual {v5, v6}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v5

    check-cast v5, LX/0Tn;

    move-object v2, v5

    .line 2093384
    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    .line 2093385
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2093386
    iget-object v0, p0, LX/EDx;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, LX/EGK;->a(Ljava/lang/Long;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2093387
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/EDx;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/EGK;->c:LX/0Tn;

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private static a(LX/EDx;JLX/7TQ;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2093388
    invoke-direct {p0, p1, p2, p3, p4}, LX/EDx;->b(JLX/7TQ;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2093389
    :cond_0
    :goto_0
    return v0

    .line 2093390
    :cond_1
    invoke-virtual {p0}, LX/EDx;->aX()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/EDx;->bC:LX/0ad;

    sget-short v2, LX/3Dx;->eg:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2093391
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(LX/EDx;Lcom/android/internal/util/Predicate;Z)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/util/Predicate",
            "<",
            "LX/EGE;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 2093392
    iget-object v0, p0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2093393
    if-nez p2, :cond_1

    iget-object v2, p0, LX/EDx;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2093394
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2093395
    iget-object v3, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2093396
    :cond_1
    invoke-interface {p1, v0}, Lcom/android/internal/util/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093397
    const/4 v0, 0x1

    .line 2093398
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/EDx;JJLcom/facebook/webrtc/ConferenceCall;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2093399
    if-nez p5, :cond_0

    iget-wide v0, p0, LX/EDx;->ak:J

    cmp-long v0, v0, p3

    if-nez v0, :cond_0

    iget v0, p0, LX/EDx;->am:I

    if-eqz v0, :cond_0

    .line 2093400
    iput-wide p1, p0, LX/EDx;->ac:J

    .line 2093401
    iput-boolean v2, p0, LX/EDx;->aB:Z

    .line 2093402
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EDx;->aC:Z

    .line 2093403
    :goto_0
    return-void

    .line 2093404
    :cond_0
    invoke-direct {p0}, LX/EDx;->bv()V

    .line 2093405
    iput-wide p1, p0, LX/EDx;->ac:J

    .line 2093406
    iput-wide p3, p0, LX/EDx;->ak:J

    .line 2093407
    iput-boolean v2, p0, LX/EDx;->aB:Z

    .line 2093408
    const/4 v0, 0x0

    iput-object v0, p0, LX/EDx;->aE:Ljava/lang/String;

    .line 2093409
    invoke-static {p0, p5}, LX/EDx;->a(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;)V

    .line 2093410
    invoke-static {p0}, LX/EDx;->bz(LX/EDx;)V

    .line 2093411
    invoke-direct {p0}, LX/EDx;->bB()V

    goto :goto_0
.end method

.method public static a$redex0(LX/EDx;LX/7TQ;JZLjava/lang/String;)V
    .locals 26

    .prologue
    .line 2093412
    invoke-static/range {p0 .. p0}, LX/EDx;->co(LX/EDx;)V

    .line 2093413
    invoke-static/range {p0 .. p0}, LX/EDx;->bw(LX/EDx;)V

    .line 2093414
    move-object/from16 v0, p0

    iget-object v8, v0, LX/EDx;->P:LX/0Px;

    .line 2093415
    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 2093416
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    if-ge v7, v9, :cond_0

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/EC0;

    .line 2093417
    invoke-virtual/range {p1 .. p1}, LX/7TQ;->ordinal()I

    invoke-virtual {v6}, LX/EC0;->h()V

    .line 2093418
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    .line 2093419
    :cond_0
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    invoke-virtual {v6}, LX/2S7;->b()V

    .line 2093420
    invoke-static/range {p0 .. p0}, LX/EDx;->cu(LX/EDx;)V

    .line 2093421
    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/EDx;->an:Z

    if-eqz v6, :cond_1

    .line 2093422
    move-object/from16 v0, p0

    iget-wide v6, v0, LX/EDx;->bH:J

    invoke-virtual/range {p0 .. p0}, LX/EDx;->aU()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/EDx;->bJ:J

    sub-long/2addr v8, v10

    add-long/2addr v6, v8

    move-object/from16 v0, p0

    iput-wide v6, v0, LX/EDx;->bH:J

    .line 2093423
    invoke-virtual/range {p0 .. p0}, LX/EDx;->aU()J

    move-result-wide v6

    move-object/from16 v0, p0

    iput-wide v6, v0, LX/EDx;->bJ:J

    .line 2093424
    :cond_1
    const/4 v12, 0x0

    .line 2093425
    const-wide/16 v10, 0x0

    .line 2093426
    const-wide/16 v8, 0x0

    .line 2093427
    const/4 v7, 0x0

    .line 2093428
    const/4 v6, 0x0

    .line 2093429
    move-object/from16 v0, p0

    iget-boolean v13, v0, LX/EDx;->bp:Z

    if-eqz v13, :cond_30

    .line 2093430
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v6}, LX/EGe;->n()J

    move-result-wide v10

    .line 2093431
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v6}, LX/EGe;->p()J

    move-result-wide v8

    .line 2093432
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v6}, LX/EGe;->o()I

    move-result v7

    .line 2093433
    invoke-virtual/range {p0 .. p0}, LX/EDx;->s()Z

    move-result v6

    .line 2093434
    invoke-virtual/range {p0 .. p0}, LX/EDx;->aU()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/EDx;->bI:J

    sub-long/2addr v12, v14

    sub-long/2addr v12, v10

    move-object/from16 v0, p0

    iput-wide v12, v0, LX/EDx;->bF:J

    .line 2093435
    move-object/from16 v0, p0

    iget-object v12, v0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v12}, LX/EGe;->y()Z

    move-result v12

    move/from16 v19, v7

    move-wide/from16 v20, v8

    move-wide/from16 v22, v10

    move/from16 v24, v12

    move v8, v6

    .line 2093436
    :goto_1
    const-wide/16 v6, 0x0

    cmp-long v6, p2, v6

    if-eqz v6, :cond_2

    .line 2093437
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v7}, Landroid/media/AudioManager;->getMode()I

    move-result v7

    invoke-virtual {v6, v7}, LX/2S7;->a(I)V

    .line 2093438
    :cond_2
    const/4 v6, 0x0

    invoke-static {v6}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Landroid/graphics/SurfaceTexture;)V

    .line 2093439
    invoke-virtual/range {p0 .. p0}, LX/EDx;->h()Z

    move-result v6

    if-eqz v6, :cond_3

    sget-object v6, LX/7TQ;->CallEndNoPermission:LX/7TQ;

    move-object/from16 v0, p1

    if-ne v0, v6, :cond_3

    .line 2093440
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->S:LX/0kL;

    new-instance v7, LX/27k;

    const v9, 0x7f08071a

    invoke-direct {v7, v9}, LX/27k;-><init>(I)V

    invoke-virtual {v6, v7}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2093441
    :cond_3
    if-nez p4, :cond_5

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/EDx;->ak:J

    const-wide/16 v10, 0x0

    cmp-long v6, v6, v10

    if-eqz v6, :cond_5

    sget-object v6, LX/7TQ;->CallEndNoPermission:LX/7TQ;

    move-object/from16 v0, p1

    if-eq v0, v6, :cond_4

    sget-object v6, LX/7TQ;->CallEndOtherNotCapable:LX/7TQ;

    move-object/from16 v0, p1

    if-ne v0, v6, :cond_5

    .line 2093442
    :cond_4
    sget-object v6, LX/7TQ;->CallEndNoPermission:LX/7TQ;

    move-object/from16 v0, p1

    if-ne v0, v6, :cond_8

    .line 2093443
    invoke-virtual/range {p0 .. p0}, LX/EDx;->az()Ljava/lang/String;

    move-result-object v6

    .line 2093444
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2093445
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->h:Landroid/content/Context;

    const v7, 0x7f0807ea

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2093446
    :goto_2
    sget-object v7, LX/79O;->a:Ljava/lang/String;

    move-object/from16 v25, v7

    move-object v7, v6

    move-object/from16 v6, v25

    .line 2093447
    :goto_3
    move-object/from16 v0, p0

    iget-object v9, v0, LX/EDx;->cn:LX/3A1;

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/EDx;->ak:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11, v7, v6}, LX/3A1;->a(Lcom/facebook/user/model/UserKey;ZLjava/lang/String;Ljava/lang/String;)V

    .line 2093448
    :cond_5
    invoke-virtual/range {p0 .. p0}, LX/EDx;->j()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2093449
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->cy:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/79G;

    invoke-static/range {p1 .. p1}, LX/EFj;->a(LX/7TQ;)Ljava/lang/String;

    move-result-object v7

    move-wide/from16 v0, p2

    invoke-virtual {v6, v7, v0, v1}, LX/79G;->a(Ljava/lang/String;J)V

    .line 2093450
    :cond_6
    move-object/from16 v0, p0

    iget v6, v0, LX/EDx;->am:I

    if-nez v6, :cond_9

    .line 2093451
    :goto_4
    return-void

    .line 2093452
    :cond_7
    move-object/from16 v0, p0

    iget-object v7, v0, LX/EDx;->h:Landroid/content/Context;

    const v9, 0x7f0807e9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v6, v10, v11

    invoke-virtual {v7, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 2093453
    :cond_8
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->h:Landroid/content/Context;

    const v7, 0x7f0807e8

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2093454
    sget-object v6, LX/79O;->b:Ljava/lang/String;

    goto :goto_3

    .line 2093455
    :cond_9
    move-object/from16 v0, p0

    iget-wide v6, v0, LX/EDx;->aw:J

    const-wide/16 v10, 0x0

    cmp-long v6, v6, v10

    if-eqz v6, :cond_a

    .line 2093456
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/EDx;->b(Landroid/view/View;)V

    .line 2093457
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-object v6, v0, LX/EDx;->av:Ljava/lang/String;

    .line 2093458
    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iput-wide v6, v0, LX/EDx;->aw:J

    .line 2093459
    :cond_a
    invoke-static/range {p0 .. p0}, LX/EDx;->bF(LX/EDx;)V

    .line 2093460
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->J:LX/ECO;

    invoke-virtual {v6}, LX/ECO;->a()V

    .line 2093461
    invoke-virtual/range {p0 .. p0}, LX/EDx;->al()V

    .line 2093462
    invoke-static/range {p0 .. p0}, LX/EDx;->bR(LX/EDx;)V

    .line 2093463
    invoke-static/range {p0 .. p0}, LX/EDx;->cc(LX/EDx;)V

    .line 2093464
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->U:Landroid/telephony/PhoneStateListener;

    if-eqz v6, :cond_b

    .line 2093465
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->t:Landroid/telephony/TelephonyManager;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/EDx;->U:Landroid/telephony/PhoneStateListener;

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v9}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 2093466
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-object v6, v0, LX/EDx;->U:Landroid/telephony/PhoneStateListener;

    .line 2093467
    :cond_b
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->L:LX/0u7;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/EDx;->V:LX/2Zz;

    invoke-virtual {v6, v7}, LX/0u7;->b(LX/2Zz;)V

    .line 2093468
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-object v6, v0, LX/EDx;->aU:Ljava/lang/reflect/Method;

    .line 2093469
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, LX/EDx;->bf:Z

    .line 2093470
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/EDx;->q(LX/EDx;Z)V

    .line 2093471
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7, v9}, LX/EDx;->a(LX/EDx;ZZZ)V

    .line 2093472
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, LX/EDx;->as:Z

    .line 2093473
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, LX/EDx;->ap:Z

    .line 2093474
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/EDx;->bl:LX/7TQ;

    .line 2093475
    invoke-virtual/range {p0 .. p0}, LX/EDx;->aU()J

    move-result-wide v6

    move-object/from16 v0, p0

    iput-wide v6, v0, LX/EDx;->aN:J

    .line 2093476
    sget-object v6, LX/EDs;->NONE:LX/EDs;

    move-object/from16 v0, p0

    iput-object v6, v0, LX/EDx;->cl:LX/EDs;

    .line 2093477
    sget-object v6, LX/EDr;->DISCONNECTED:LX/EDr;

    move-object/from16 v0, p0

    iput-object v6, v0, LX/EDx;->cm:LX/EDr;

    .line 2093478
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, LX/EDx;->bx:Z

    .line 2093479
    sget-object v6, LX/EDv;->STOPPED:LX/EDv;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/EDx;->a(LX/EDv;)V

    .line 2093480
    invoke-static/range {p0 .. p0}, LX/EDx;->ct(LX/EDx;)V

    .line 2093481
    const-wide/16 v6, 0x0

    cmp-long v6, p2, v6

    if-eqz v6, :cond_c

    .line 2093482
    sget-object v6, LX/7TQ;->CallEndConnectionDropped:LX/7TQ;

    move-object/from16 v0, p1

    if-ne v0, v6, :cond_15

    .line 2093483
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->J:LX/ECO;

    sget-object v7, LX/ECN;->DROPPED_CALL:LX/ECN;

    invoke-virtual {v6, v7}, LX/ECO;->a(LX/ECN;)V

    .line 2093484
    :cond_c
    :goto_5
    const-wide/16 v6, 0x0

    cmp-long v6, p2, v6

    if-eqz v6, :cond_f

    .line 2093485
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v8, v6}, LX/EDx;->d(LX/EDx;ZZ)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2093486
    invoke-virtual/range {p0 .. p0}, LX/EDx;->ag()Z

    move-result v6

    if-nez v6, :cond_d

    invoke-virtual/range {p0 .. p0}, LX/EDx;->aH()LX/7TQ;

    move-result-object v6

    sget-object v7, LX/7TQ;->CallEndIgnoreCall:LX/7TQ;

    if-ne v6, v7, :cond_16

    :cond_d
    const/16 v18, 0x1

    .line 2093487
    :goto_6
    move-object/from16 v0, p0

    iget-object v7, v0, LX/EDx;->c:LX/EDC;

    invoke-virtual/range {p0 .. p0}, LX/EDx;->ag()Z

    move-result v10

    invoke-virtual/range {p0 .. p0}, LX/EDx;->aJ()Z

    move-result v11

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/EDx;->ak:J

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/EDx;->aI:J

    invoke-static/range {p0 .. p0}, LX/EDx;->cw(LX/EDx;)J

    move-result-wide v8

    const-wide/16 v16, 0x3e8

    div-long v16, v8, v16

    move-wide/from16 v8, p2

    invoke-virtual/range {v7 .. v18}, LX/EDC;->a(JZZJJJZ)V

    .line 2093488
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/EDx;->aB:Z

    if-nez v6, :cond_f

    .line 2093489
    invoke-static/range {p0 .. p1}, LX/EDx;->b(LX/EDx;LX/7TQ;)V

    .line 2093490
    :cond_f
    const/4 v8, 0x0

    .line 2093491
    const/4 v14, 0x0

    .line 2093492
    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/EDx;->aF:Z

    if-eqz v6, :cond_17

    const-wide/32 v6, 0xea60

    .line 2093493
    :goto_7
    move-object/from16 v0, p0

    iget-wide v10, v0, LX/EDx;->ak:J

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-eqz v9, :cond_2f

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/EDx;->ak:J

    const-wide/16 v12, -0x1

    cmp-long v9, v10, v12

    if-eqz v9, :cond_2f

    move-object/from16 v0, p0

    iget-object v9, v0, LX/EDx;->ad:LX/EFw;

    if-eqz v9, :cond_10

    move-object/from16 v0, p0

    iget-object v9, v0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v9}, LX/EFw;->e()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_2f

    :cond_10
    const-wide/16 v10, 0x0

    cmp-long v9, p2, v10

    if-eqz v9, :cond_2f

    invoke-virtual/range {p0 .. p0}, LX/EDx;->ad()Z

    move-result v9

    if-eqz v9, :cond_2f

    const-string v9, "caller_camp_presence"

    move-object/from16 v0, p0

    iget-object v10, v0, LX/EDx;->aE:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2f

    const-string v9, "caller_camp_mqtt"

    move-object/from16 v0, p0

    iget-object v10, v0, LX/EDx;->aE:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2f

    const-string v9, "caller_camp_rtc"

    move-object/from16 v0, p0

    iget-object v10, v0, LX/EDx;->aE:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2f

    .line 2093494
    if-nez p4, :cond_1b

    .line 2093495
    sget-object v9, LX/7TQ;->CallEndOtherNotCapable:LX/7TQ;

    move-object/from16 v0, p1

    if-ne v0, v9, :cond_18

    invoke-static/range {p5 .. p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_18

    const-string v9, "MQTTSendOfferTimeOut"

    move-object/from16 v0, p5

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-lez v9, :cond_18

    .line 2093496
    const/4 v8, 0x1

    move-wide/from16 v16, v6

    move v13, v8

    .line 2093497
    :goto_8
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, LX/EDx;->aF:Z

    .line 2093498
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/EDx;->h(LX/EDx;I)V

    .line 2093499
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->q:LX/2XQ;

    invoke-virtual/range {p0 .. p0}, LX/EDx;->aa()Z

    move-result v7

    invoke-virtual {v6, v7}, LX/2XQ;->a(Z)V

    .line 2093500
    invoke-static/range {p5 .. p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1e

    .line 2093501
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    move-wide/from16 v0, p2

    move-object/from16 v2, p5

    invoke-virtual {v6, v0, v1, v2}, LX/2S7;->b(JLjava/lang/String;)V

    .line 2093502
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "chat_head_count"

    move/from16 v0, v19

    int-to-long v8, v0

    invoke-virtual {v6, v7, v8, v9}, LX/2S7;->b(Ljava/lang/String;J)Z

    .line 2093503
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "chat_head_duration"

    move-wide/from16 v0, v20

    invoke-virtual {v6, v7, v0, v1}, LX/2S7;->b(Ljava/lang/String;J)Z

    .line 2093504
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "chat_head_duration_overall"

    move-wide/from16 v0, v22

    invoke-virtual {v6, v7, v0, v1}, LX/2S7;->b(Ljava/lang/String;J)Z

    .line 2093505
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "dragged_chat_head"

    move/from16 v0, v24

    invoke-virtual {v6, v7, v0}, LX/2S7;->b(Ljava/lang/String;Z)Z

    .line 2093506
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "incall_count"

    move-object/from16 v0, p0

    iget v8, v0, LX/EDx;->bE:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093507
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "incall_duration"

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/EDx;->bF:J

    invoke-virtual {v6, v7, v8, v9}, LX/2S7;->b(Ljava/lang/String;J)Z

    .line 2093508
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "off_incall_duration"

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/EDx;->bG:J

    invoke-virtual {v6, v7, v8, v9}, LX/2S7;->b(Ljava/lang/String;J)Z

    .line 2093509
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "mute_duration"

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/EDx;->bH:J

    invoke-virtual {v6, v7, v8, v9}, LX/2S7;->b(Ljava/lang/String;J)Z

    .line 2093510
    invoke-virtual/range {p0 .. p0}, LX/EDx;->W()Z

    move-result v6

    if-eqz v6, :cond_11

    .line 2093511
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "rtc_video_duration_with_filter_enabled"

    invoke-virtual/range {p0 .. p0}, LX/EDx;->V()LX/EHZ;

    move-result-object v8

    invoke-virtual {v8}, LX/EHZ;->b()J

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, LX/2S7;->b(Ljava/lang/String;J)Z

    .line 2093512
    :cond_11
    invoke-virtual/range {p0 .. p0}, LX/EDx;->h()Z

    move-result v6

    if-eqz v6, :cond_13

    invoke-virtual/range {p0 .. p0}, LX/EDx;->j()Z

    move-result v6

    if-nez v6, :cond_13

    .line 2093513
    const-string v6, "voip"

    .line 2093514
    move-object/from16 v0, p0

    iget-boolean v7, v0, LX/EDx;->by:Z

    if-eqz v7, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v7, v0, LX/EDx;->bz:Z

    if-nez v7, :cond_1c

    .line 2093515
    const-string v6, "escalation_declined"

    .line 2093516
    :cond_12
    :goto_9
    move-object/from16 v0, p0

    iget-object v7, v0, LX/EDx;->l:LX/2S7;

    const-string v8, "call_type"

    invoke-virtual {v7, v8, v6}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093517
    :cond_13
    if-eqz v13, :cond_14

    .line 2093518
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "caller_camp"

    const-wide/16 v8, 0x1

    invoke-virtual {v6, v7, v8, v9}, LX/2S7;->b(Ljava/lang/String;J)Z

    .line 2093519
    :cond_14
    invoke-static/range {p0 .. p0}, LX/EDx;->cp(LX/EDx;)LX/0P1;

    move-result-object v6

    .line 2093520
    invoke-virtual {v6}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v6

    invoke-virtual {v6}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 2093521
    move-object/from16 v0, p0

    iget-object v9, v0, LX/EDx;->l:LX/2S7;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v7, "cb_"

    invoke-direct {v10, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v9, v7, v6}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 2093522
    :cond_15
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move-object/from16 v3, p1

    move/from16 v4, p4

    invoke-static {v0, v1, v2, v3, v4}, LX/EDx;->a(LX/EDx;JLX/7TQ;Z)Z

    move-result v6

    if-nez v6, :cond_c

    .line 2093523
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->J:LX/ECO;

    sget-object v7, LX/ECN;->END_CALL:LX/ECN;

    invoke-virtual {v6, v7}, LX/ECO;->a(LX/ECN;)V

    goto/16 :goto_5

    .line 2093524
    :cond_16
    const/16 v18, 0x0

    goto/16 :goto_6

    .line 2093525
    :cond_17
    const-wide/16 v6, 0x7530

    goto/16 :goto_7

    .line 2093526
    :cond_18
    invoke-virtual/range {p0 .. p0}, LX/EDx;->aU()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/EDx;->aJ:J

    sub-long/2addr v10, v12

    const-wide/16 v12, 0x2710

    cmp-long v9, v10, v12

    if-ltz v9, :cond_19

    sget-object v9, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    move-object/from16 v0, p1

    if-ne v0, v9, :cond_19

    move-object/from16 v0, p0

    iget-boolean v9, v0, LX/EDx;->aY:Z

    if-eqz v9, :cond_19

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/EDx;->aJ:J

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_19

    .line 2093527
    const/4 v8, 0x1

    move-wide/from16 v16, v6

    move v13, v8

    goto/16 :goto_8

    .line 2093528
    :cond_19
    invoke-virtual/range {p0 .. p0}, LX/EDx;->aU()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/EDx;->aJ:J

    sub-long/2addr v10, v12

    const-wide/16 v12, 0x2710

    cmp-long v9, v10, v12

    if-ltz v9, :cond_1a

    sget-object v9, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    move-object/from16 v0, p1

    if-ne v0, v9, :cond_1a

    move-object/from16 v0, p0

    iget-boolean v9, v0, LX/EDx;->aF:Z

    if-eqz v9, :cond_1a

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/EDx;->aJ:J

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_1a

    .line 2093529
    const/4 v8, 0x1

    :cond_1a
    move-wide/from16 v16, v6

    move v13, v8

    .line 2093530
    goto/16 :goto_8

    .line 2093531
    :cond_1b
    sget-object v9, LX/7TQ;->CallEndInAnotherCall:LX/7TQ;

    move-object/from16 v0, p1

    if-ne v0, v9, :cond_2f

    .line 2093532
    const/4 v8, 0x1

    .line 2093533
    const-wide/32 v6, 0xdbba0

    .line 2093534
    const-string v14, "callee_inanother_call"

    move-wide/from16 v16, v6

    move v13, v8

    goto/16 :goto_8

    .line 2093535
    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v7, v0, LX/EDx;->bz:Z

    if-eqz v7, :cond_1d

    .line 2093536
    const-string v6, "escalated"

    goto/16 :goto_9

    .line 2093537
    :cond_1d
    move-object/from16 v0, p0

    iget-boolean v7, v0, LX/EDx;->bw:Z

    if-eqz v7, :cond_12

    .line 2093538
    const-string v6, "direct_video"

    goto/16 :goto_9

    .line 2093539
    :cond_1e
    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/EDx;->aB:Z

    if-eqz v6, :cond_1f

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/EDx;->aD:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_1f

    .line 2093540
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "notif_delay"

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/EDx;->aD:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093541
    :cond_1f
    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iput-wide v6, v0, LX/EDx;->bI:J

    .line 2093542
    move-object/from16 v0, p0

    iget-wide v6, v0, LX/EDx;->aK:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_20

    .line 2093543
    move-object/from16 v0, p0

    iget-wide v6, v0, LX/EDx;->aL:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_25

    .line 2093544
    move-object/from16 v0, p0

    iget-wide v6, v0, LX/EDx;->aL:J

    move-object/from16 v0, p0

    iget-wide v8, v0, LX/EDx;->aK:J

    sub-long/2addr v6, v8

    .line 2093545
    move-object/from16 v0, p0

    iget-object v8, v0, LX/EDx;->l:LX/2S7;

    const-string v9, "ui_switch"

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v9, v6}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093546
    :cond_20
    :goto_b
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->M:LX/0Uh;

    const/16 v7, 0x600

    invoke-virtual {v6, v7}, LX/0Uh;->a(I)LX/03R;

    move-result-object v6

    sget-object v7, LX/03R;->YES:LX/03R;

    if-ne v6, v7, :cond_21

    .line 2093547
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "hw_vcap"

    invoke-static {}, LX/7fA;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093548
    :cond_21
    invoke-virtual/range {p0 .. p0}, LX/EDx;->aH()LX/7TQ;

    move-result-object v6

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, p4

    invoke-static {v0, v1, v2, v6, v3}, LX/EDx;->a(LX/EDx;JLX/7TQ;Z)Z

    move-result v6

    if-eqz v6, :cond_22

    .line 2093549
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, LX/EDx;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v7}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, LX/EDx;->y()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2093550
    move-object/from16 v0, p0

    iget-object v7, v0, LX/EDx;->bb:LX/EIq;

    invoke-virtual/range {p0 .. p0}, LX/EDx;->y()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9, v6}, LX/EIq;->a(JLjava/lang/String;)Z

    .line 2093551
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, LX/EDx;->P:LX/0Px;

    move-object/from16 v19, v0

    .line 2093552
    invoke-virtual/range {v19 .. v19}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_28

    .line 2093553
    const/4 v7, 0x0

    .line 2093554
    invoke-virtual/range {v19 .. v19}, LX/0Px;->size()I

    move-result v20

    const/4 v6, 0x0

    move v15, v6

    move/from16 v18, v7

    :goto_c
    move/from16 v0, v20

    if-ge v15, v0, :cond_29

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/EC0;

    .line 2093555
    invoke-virtual/range {p1 .. p1}, LX/7TQ;->ordinal()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, LX/EDx;->h()Z

    move-result v11

    if-nez v18, :cond_26

    const/4 v12, 0x1

    :goto_d
    move-wide/from16 v8, p2

    move/from16 v10, p4

    invoke-virtual/range {v6 .. v12}, LX/EC0;->a(IJZZZ)Z

    move-result v6

    if-nez v6, :cond_23

    if-eqz v18, :cond_27

    :cond_23
    const/4 v6, 0x1

    .line 2093556
    :goto_e
    if-eqz v6, :cond_24

    .line 2093557
    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, LX/EDx;->n(Z)V

    .line 2093558
    :cond_24
    add-int/lit8 v7, v15, 0x1

    move v15, v7

    move/from16 v18, v6

    goto :goto_c

    .line 2093559
    :cond_25
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    const-string v7, "ui_switch"

    const-string v8, "-1"

    invoke-virtual {v6, v7, v8}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 2093560
    :cond_26
    const/4 v12, 0x0

    goto :goto_d

    :cond_27
    const/4 v6, 0x0

    goto :goto_e

    .line 2093561
    :cond_28
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->bb:LX/EIq;

    invoke-virtual {v6}, LX/EIq;->c()Z

    move-result v6

    if-nez v6, :cond_29

    .line 2093562
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->l:LX/2S7;

    invoke-virtual {v6}, LX/2S7;->g()V

    .line 2093563
    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/EDx;->n(Z)V

    .line 2093564
    :cond_29
    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/EDx;->bp:Z

    if-eqz v6, :cond_2a

    .line 2093565
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v6}, LX/EGe;->r()Z

    move-result v6

    if-eqz v6, :cond_2e

    .line 2093566
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v6}, LX/EGe;->q()J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, LX/EDx;->a(J)V

    .line 2093567
    :cond_2a
    :goto_f
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->cs:LX/ECY;

    invoke-virtual {v6}, LX/ECY;->b()V

    .line 2093568
    if-eqz v13, :cond_2b

    .line 2093569
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->cs:LX/ECY;

    move-object/from16 v0, p0

    iget-wide v7, v0, LX/EDx;->ak:J

    move-object/from16 v0, p0

    iget-object v9, v0, LX/EDx;->bn:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/EDx;->bo:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/EDx;->bw:Z

    move-wide/from16 v12, v16

    invoke-virtual/range {v6 .. v14}, LX/ECY;->a(JLjava/lang/String;Ljava/lang/String;ZJLjava/lang/String;)Z

    .line 2093570
    :cond_2b
    invoke-virtual/range {p0 .. p0}, LX/EDx;->X()Z

    move-result v6

    if-eqz v6, :cond_2c

    .line 2093571
    invoke-virtual/range {p0 .. p0}, LX/EDx;->V()LX/EHZ;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/EHZ;->a(ILX/BA6;)V

    .line 2093572
    invoke-virtual/range {p0 .. p0}, LX/EDx;->V()LX/EHZ;

    move-result-object v6

    invoke-virtual {v6}, LX/EHZ;->h()V

    .line 2093573
    invoke-static/range {p0 .. p0}, LX/EDx;->bX(LX/EDx;)V

    .line 2093574
    :cond_2c
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->ad:LX/EFw;

    if-eqz v6, :cond_2d

    .line 2093575
    move-object/from16 v0, p0

    iget-object v6, v0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v6}, LX/EFw;->g()V

    .line 2093576
    :cond_2d
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/EDx;->a(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;)V

    goto/16 :goto_4

    .line 2093577
    :cond_2e
    invoke-static/range {p0 .. p0}, LX/EDx;->ci(LX/EDx;)V

    goto :goto_f

    :cond_2f
    move-wide/from16 v16, v6

    move v13, v8

    goto/16 :goto_8

    :cond_30
    move/from16 v19, v7

    move-wide/from16 v20, v8

    move-wide/from16 v22, v10

    move/from16 v24, v12

    move v8, v6

    goto/16 :goto_1
.end method

.method public static a$redex0(LX/EDx;LX/7TQ;Z)V
    .locals 5

    .prologue
    .line 2093578
    iget-object v0, p0, LX/EDx;->v:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$17;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$17;-><init>(LX/EDx;LX/7TQ;Z)V

    const-wide/16 v2, 0x5dc

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->bV:Ljava/util/concurrent/ScheduledFuture;

    .line 2093579
    return-void
.end method

.method private static b(LX/0QB;)LX/EDx;
    .locals 46

    .prologue
    .line 2093580
    new-instance v2, LX/EDx;

    invoke-static/range {p0 .. p0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v3

    check-cast v3, LX/12x;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/D9r;->a(LX/0QB;)LX/D9r;

    move-result-object v5

    check-cast v5, LX/D9r;

    invoke-static/range {p0 .. p0}, LX/19T;->a(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    invoke-static/range {p0 .. p0}, LX/3GP;->a(LX/0QB;)LX/3GP;

    move-result-object v7

    check-cast v7, LX/3GP;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/EBQ;->a(LX/0QB;)LX/EG0;

    move-result-object v9

    check-cast v9, LX/EG0;

    const-class v10, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0eQ;->a(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v11

    check-cast v11, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static/range {p0 .. p0}, LX/0aQ;->a(LX/0QB;)LX/0aQ;

    move-result-object v12

    check-cast v12, LX/0Xl;

    const-class v13, LX/00H;

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LX/00H;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v14

    check-cast v14, LX/0lB;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v15

    check-cast v15, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v16

    check-cast v16, Landroid/os/Handler;

    invoke-static/range {p0 .. p0}, LX/ECx;->a(LX/0QB;)LX/ECx;

    move-result-object v17

    check-cast v17, LX/ECx;

    invoke-static/range {p0 .. p0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v18

    check-cast v18, LX/0aU;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v19

    check-cast v19, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object v20

    check-cast v20, LX/2S7;

    invoke-static/range {p0 .. p0}, LX/EBZ;->a(LX/0QB;)LX/EG9;

    move-result-object v21

    check-cast v21, LX/EG9;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v22

    check-cast v22, LX/0So;

    invoke-static/range {p0 .. p0}, LX/2XQ;->a(LX/0QB;)LX/2XQ;

    move-result-object v23

    check-cast v23, LX/2XQ;

    invoke-static/range {p0 .. p0}, LX/EBY;->a(LX/0QB;)LX/EG8;

    move-result-object v24

    check-cast v24, LX/EG8;

    const-class v25, LX/ECP;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/ECP;

    invoke-static/range {p0 .. p0}, LX/3A1;->a(LX/0QB;)LX/3A1;

    move-result-object v26

    check-cast v26, LX/3A1;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v27

    check-cast v27, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/0e7;->a(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v28

    check-cast v28, Landroid/telephony/TelephonyManager;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v29

    check-cast v29, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v30

    check-cast v30, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/3RR;->a(LX/0QB;)Landroid/os/Vibrator;

    move-result-object v31

    check-cast v31, Landroid/os/Vibrator;

    invoke-static/range {p0 .. p0}, LX/EBW;->a(LX/0QB;)LX/3RT;

    move-result-object v32

    check-cast v32, LX/3RT;

    invoke-static/range {p0 .. p0}, LX/34I;->a(LX/0QB;)LX/34I;

    move-result-object v33

    check-cast v33, LX/34I;

    invoke-static/range {p0 .. p0}, LX/ECY;->a(LX/0QB;)LX/ECY;

    move-result-object v34

    check-cast v34, LX/ECY;

    invoke-static/range {p0 .. p0}, LX/1Ml;->a(LX/0QB;)LX/1Ml;

    move-result-object v35

    check-cast v35, LX/1Ml;

    invoke-static/range {p0 .. p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v36

    check-cast v36, LX/0yH;

    invoke-static/range {p0 .. p0}, Lcom/facebook/presence/ThreadPresenceManager;->a(LX/0QB;)Lcom/facebook/presence/ThreadPresenceManager;

    move-result-object v37

    check-cast v37, Lcom/facebook/presence/ThreadPresenceManager;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v38

    check-cast v38, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0VU;->a(LX/0QB;)Landroid/app/ActivityManager;

    move-result-object v39

    check-cast v39, Landroid/app/ActivityManager;

    invoke-static/range {p0 .. p0}, LX/EBU;->a(LX/0QB;)LX/EG4;

    move-result-object v40

    check-cast v40, LX/EG4;

    invoke-static/range {p0 .. p0}, LX/0u7;->a(LX/0QB;)LX/0u7;

    move-result-object v41

    check-cast v41, LX/0u7;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v42

    check-cast v42, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v43

    check-cast v43, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v44

    check-cast v44, LX/2Oi;

    invoke-static/range {p0 .. p0}, LX/EIq;->a(LX/0QB;)LX/EIq;

    move-result-object v45

    check-cast v45, LX/EIq;

    invoke-direct/range {v2 .. v45}, LX/EDx;-><init>(LX/12x;LX/0Sh;LX/D9r;Landroid/media/AudioManager;LX/3GP;LX/0SG;LX/EG0;Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Xl;LX/00H;LX/0lB;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/os/Handler;LX/ECx;LX/0aU;LX/0Xl;LX/2S7;LX/EG9;LX/0So;LX/2XQ;LX/EG8;LX/ECP;LX/3A1;Lcom/facebook/content/SecureContextHelper;Landroid/telephony/TelephonyManager;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Vibrator;LX/3RT;LX/34I;LX/ECY;LX/1Ml;LX/0yH;Lcom/facebook/presence/ThreadPresenceManager;LX/0ad;Landroid/app/ActivityManager;LX/EG4;LX/0u7;LX/0kL;LX/0Uh;LX/2Oi;LX/EIq;)V

    .line 2093581
    const/16 v3, 0x10e1

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/6c4;->a(LX/0QB;)LX/6c4;

    move-result-object v4

    check-cast v4, LX/6c4;

    const/16 v5, 0x3262

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/3Eb;->a(LX/0QB;)LX/3Eb;

    move-result-object v6

    check-cast v6, LX/3Eb;

    invoke-static/range {p0 .. p0}, LX/EDC;->a(LX/0QB;)LX/EDC;

    move-result-object v7

    check-cast v7, LX/EDC;

    const/16 v8, 0x542

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3266

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const-class v10, LX/EFz;

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/EFz;

    const-class v11, LX/EFx;

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/EFx;

    invoke-static/range {v2 .. v11}, LX/EDx;->a(LX/EDx;LX/0Ot;LX/6c4;LX/0Ot;LX/3Eb;LX/EDC;LX/0Ot;LX/0Ot;LX/EFz;LX/EFx;)V

    .line 2093582
    return-object v2
.end method

.method public static b(LX/EDx;JJ)V
    .locals 10

    .prologue
    .line 2093583
    iget-object v0, p0, LX/EDx;->bk:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2093584
    :goto_0
    return-void

    .line 2093585
    :cond_0
    iget-object v0, p0, LX/EDx;->cs:LX/ECY;

    .line 2093586
    new-instance v1, LX/ECa;

    iget-object v2, v0, LX/ECY;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v5

    const-wide/32 v7, 0x927c0

    move-object v2, v0

    move-wide v3, p1

    invoke-direct/range {v1 .. v8}, LX/ECa;-><init>(LX/ECY;JJJ)V

    .line 2093587
    iget-object v2, v0, LX/ECY;->t:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2093588
    invoke-static {v0, v1}, LX/ECY;->c(LX/ECY;LX/ECT;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2093589
    invoke-static {v0, v1}, LX/ECY;->d(LX/ECY;LX/ECT;)V

    .line 2093590
    :cond_1
    goto :goto_0
.end method

.method private static b(LX/EDx;LX/7TQ;)V
    .locals 1

    .prologue
    .line 2093591
    iget-boolean v0, p0, LX/EDx;->aP:Z

    if-nez v0, :cond_0

    sget-object v0, LX/7TQ;->CallEndOtherInstanceHandled:LX/7TQ;

    if-ne p1, v0, :cond_1

    .line 2093592
    :cond_0
    :goto_0
    return-void

    .line 2093593
    :cond_1
    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2093594
    iget-boolean v0, p0, LX/EDx;->bQ:Z

    move v0, v0

    .line 2093595
    if-nez v0, :cond_0

    .line 2093596
    iget-object v0, p0, LX/EDx;->bk:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093597
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2093598
    sget-object v0, LX/7TQ;->CallEndIncomingTimeout:LX/7TQ;

    if-ne p1, v0, :cond_0

    .line 2093599
    iget-object v0, p0, LX/EDx;->x:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    iget-object v0, p0, LX/EDx;->bk:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->f()Z

    goto :goto_0

    .line 2093600
    :cond_2
    iget-object v0, p0, LX/EDx;->bo:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093601
    iget-object v0, p0, LX/EDx;->bo:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 2093602
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EDx;->aR:Z

    goto :goto_0

    .line 2093603
    :cond_3
    iget-object v0, p0, LX/EDx;->x:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    iget-object v0, p0, LX/EDx;->bk:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->f()Z

    goto :goto_0
.end method

.method private b(JLX/7TQ;Z)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2093604
    iget-object v2, p0, LX/EDx;->E:LX/00H;

    .line 2093605
    iget-object v3, v2, LX/00H;->j:LX/01T;

    move-object v2, v3

    .line 2093606
    sget-object v3, LX/01T;->MESSENGER:LX/01T;

    if-ne v2, v3, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, LX/EDx;->aG()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2093607
    iget-boolean v2, p0, LX/EDx;->bQ:Z

    move v2, v2

    .line 2093608
    if-nez v2, :cond_0

    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2093609
    iget-wide v7, p0, LX/EDx;->ak:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v7

    .line 2093610
    iget-object v8, p0, LX/EDx;->N:LX/2Oi;

    invoke-virtual {v8, v7}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v7

    .line 2093611
    if-eqz v7, :cond_5

    .line 2093612
    iget-boolean v8, v7, Lcom/facebook/user/model/User;->B:Z

    move v7, v8

    .line 2093613
    :goto_0
    move v2, v7

    .line 2093614
    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 2093615
    :cond_1
    :goto_1
    return v0

    .line 2093616
    :cond_2
    invoke-virtual {p0}, LX/EDx;->ag()Z

    move-result v2

    .line 2093617
    iget-boolean v3, p0, LX/EDx;->aB:Z

    move v3, v3

    .line 2093618
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v4

    .line 2093619
    sget-object v5, LX/EDn;->b:[I

    invoke-virtual {p3}, LX/7TQ;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    move v0, v1

    .line 2093620
    goto :goto_1

    .line 2093621
    :pswitch_0
    if-eqz p4, :cond_3

    if-nez v2, :cond_3

    if-eqz v3, :cond_3

    if-eqz v4, :cond_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2093622
    :pswitch_1
    if-eqz v3, :cond_4

    if-eqz v4, :cond_1

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    const/4 v7, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static b$redex0(LX/EDx;J)V
    .locals 3

    .prologue
    .line 2093623
    iget-object v0, p0, LX/EDx;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2093624
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2093625
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {p1, p2, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2093626
    iget-object v0, p0, LX/EDx;->x:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    .line 2093627
    return-void
.end method

.method public static bA(LX/EDx;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2093628
    invoke-direct {p0}, LX/EDx;->bK()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aV()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2093629
    :cond_0
    :goto_0
    return v1

    .line 2093630
    :cond_1
    iget-boolean v0, p0, LX/EDx;->ch:Z

    if-nez v0, :cond_0

    .line 2093631
    iget-boolean v0, p0, LX/EDx;->bQ:Z

    if-nez v0, :cond_0

    .line 2093632
    iget-object v0, p0, LX/EDx;->bD:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EDx;->bD:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, LX/EDx;->ct:LX/1Ml;

    invoke-virtual {v0}, LX/1Ml;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093633
    :cond_3
    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2093634
    const/4 v1, 0x1

    goto :goto_0

    .line 2093635
    :cond_4
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2093636
    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2093637
    iget-boolean v0, p0, LX/EDx;->aB:Z

    move v0, v0

    .line 2093638
    if-eqz v0, :cond_8

    .line 2093639
    iget-object v0, p0, LX/EDx;->bC:LX/0ad;

    sget-short v2, LX/3Dx;->aN:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    :cond_5
    :goto_1
    move v1, v0

    .line 2093640
    goto :goto_0

    .line 2093641
    :cond_6
    iget-boolean v0, p0, LX/EDx;->aB:Z

    move v0, v0

    .line 2093642
    if-eqz v0, :cond_7

    .line 2093643
    iget-object v0, p0, LX/EDx;->bC:LX/0ad;

    sget-short v2, LX/3Dx;->ee:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2093644
    :goto_2
    iget-object v2, p0, LX/EDx;->bC:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget-short v5, LX/3Dx;->ek:S

    invoke-interface {v2, v3, v4, v5, v1}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v2

    .line 2093645
    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v3

    if-nez v3, :cond_5

    if-nez v2, :cond_5

    goto :goto_0

    .line 2093646
    :cond_7
    iget-object v0, p0, LX/EDx;->bC:LX/0ad;

    sget-short v2, LX/3Dx;->ec:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_1
.end method

.method private bB()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2092662
    iget-object v0, p0, LX/EDx;->cs:LX/ECY;

    iget-wide v4, p0, LX/EDx;->ak:J

    invoke-virtual {v0, v4, v5}, LX/ECY;->b(J)V

    .line 2092663
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-static {v0}, LX/3RK;->a(Landroid/content/Context;)LX/3RK;

    move-result-object v0

    .line 2092664
    iget-wide v4, p0, LX/EDx;->ak:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 2092665
    const/16 v4, 0x271a

    invoke-virtual {v0, v3, v4}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 2092666
    invoke-static {v3}, LX/0db;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    .line 2092667
    iget-object v5, p0, LX/EDx;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    .line 2092668
    invoke-interface {v5, v4}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 2092669
    invoke-interface {v5}, LX/0hN;->commit()V

    .line 2092670
    const-string v4, "10027"

    const/16 v5, 0x272b

    invoke-virtual {v0, v4, v5}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 2092671
    const/16 v4, 0x2729

    invoke-virtual {v0, v3, v4}, LX/3RK;->a(Ljava/lang/String;I)V

    .line 2092672
    iget-object v0, p0, LX/EDx;->bb:LX/EIq;

    invoke-virtual {v0}, LX/EIq;->a()V

    .line 2092673
    invoke-static {p0, v2}, LX/EDx;->h(LX/EDx;I)V

    .line 2092674
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/EDx;->bY:LX/03R;

    .line 2092675
    iget-object v0, p0, LX/EDx;->q:LX/2XQ;

    invoke-virtual {p0}, LX/EDx;->aa()Z

    move-result v3

    invoke-virtual {v0, v3}, LX/2XQ;->a(Z)V

    .line 2092676
    sget-object v0, LX/7TQ;->CallEndIgnoreCall:LX/7TQ;

    iput-object v0, p0, LX/EDx;->bl:LX/7TQ;

    .line 2092677
    iget-wide v6, p0, LX/EDx;->ak:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_b

    .line 2092678
    :goto_0
    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2092679
    invoke-direct {p0}, LX/EDx;->bV()I

    .line 2092680
    :cond_0
    iget-object v0, p0, LX/EDx;->x:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    iput-wide v4, p0, LX/EDx;->aI:J

    .line 2092681
    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v4

    iput-wide v4, p0, LX/EDx;->aJ:J

    .line 2092682
    iget-wide v4, p0, LX/EDx;->aJ:J

    iput-wide v4, p0, LX/EDx;->bI:J

    .line 2092683
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    iput v0, p0, LX/EDx;->aG:I

    .line 2092684
    invoke-direct {p0}, LX/EDx;->bW()V

    .line 2092685
    invoke-direct {p0}, LX/EDx;->by()V

    .line 2092686
    iget-object v0, p0, LX/EDx;->s:LX/D9r;

    const/4 v3, 0x0

    .line 2092687
    iget-boolean v4, v0, LX/D9r;->c:Z

    if-eqz v4, :cond_c

    .line 2092688
    :cond_1
    :goto_1
    move v0, v3

    .line 2092689
    iput-boolean v0, p0, LX/EDx;->bh:Z

    .line 2092690
    iget-object v0, p0, LX/EDx;->M:LX/0Uh;

    const/16 v3, 0x66f

    invoke-virtual {v0, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/EDx;->bh:Z

    if-nez v0, :cond_7

    :cond_2
    move v0, v2

    :goto_2
    iput-boolean v0, p0, LX/EDx;->br:Z

    .line 2092691
    iget-boolean v0, p0, LX/EDx;->br:Z

    iput-boolean v0, p0, LX/EDx;->bf:Z

    .line 2092692
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    iput-boolean v0, p0, LX/EDx;->be:Z

    .line 2092693
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    iget-boolean v3, p0, LX/EDx;->bf:Z

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 2092694
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    iget-boolean v3, p0, LX/EDx;->an:Z

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 2092695
    invoke-static {p0}, LX/EDx;->bS(LX/EDx;)V

    .line 2092696
    invoke-static {p0}, LX/EDx;->bR(LX/EDx;)V

    .line 2092697
    iget-object v0, p0, LX/EDx;->B:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v3, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v4, LX/EDW;

    invoke-direct {v4, p0}, LX/EDW;-><init>(LX/EDx;)V

    invoke-interface {v0, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v3, "com.facebook.rtc.fbwebrtc.RTC_VIDEO_CHAT_HEAD_SHRINK_BY_USER_INTERACTION"

    new-instance v4, LX/EDV;

    invoke-direct {v4, p0}, LX/EDV;-><init>(LX/EDx;)V

    invoke-interface {v0, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->aa:LX/0Yb;

    .line 2092698
    iget-object v0, p0, LX/EDx;->aa:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2092699
    iget-object v0, p0, LX/EDx;->A:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v3, "android.intent.action.HEADSET_PLUG"

    new-instance v4, LX/EDZ;

    invoke-direct {v4, p0}, LX/EDZ;-><init>(LX/EDx;)V

    invoke-interface {v0, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v3, "android.intent.action.SCREEN_ON"

    new-instance v4, LX/EDY;

    invoke-direct {v4, p0}, LX/EDY;-><init>(LX/EDx;)V

    invoke-interface {v0, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v3, "android.intent.action.SCREEN_OFF"

    new-instance v4, LX/EDX;

    invoke-direct {v4, p0}, LX/EDX;-><init>(LX/EDx;)V

    invoke-interface {v0, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->Z:LX/0Yb;

    .line 2092700
    iget-object v0, p0, LX/EDx;->Z:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2092701
    const/4 v5, 0x0

    .line 2092702
    invoke-static {p0}, LX/EDx;->bQ(LX/EDx;)V

    .line 2092703
    const/4 v3, 0x0

    .line 2092704
    :try_start_0
    const-string v0, "android.os.ServiceManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 2092705
    const-string v4, "getService"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v0, v4, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2092706
    const/4 v4, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "media.audio_flinger"

    aput-object v8, v6, v7

    invoke-virtual {v0, v4, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2092707
    :goto_3
    move-object v0, v0

    .line 2092708
    iput-object v0, p0, LX/EDx;->W:Landroid/os/IBinder;

    .line 2092709
    iget-object v0, p0, LX/EDx;->W:Landroid/os/IBinder;

    if-eqz v0, :cond_3

    .line 2092710
    new-instance v0, LX/EDa;

    invoke-direct {v0, p0}, LX/EDa;-><init>(LX/EDx;)V

    iput-object v0, p0, LX/EDx;->X:Landroid/os/IBinder$DeathRecipient;

    .line 2092711
    :try_start_1
    iget-object v0, p0, LX/EDx;->W:Landroid/os/IBinder;

    iget-object v3, p0, LX/EDx;->X:Landroid/os/IBinder$DeathRecipient;

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2092712
    :cond_3
    :goto_4
    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2092713
    iget-boolean v0, p0, LX/EDx;->bQ:Z

    move v0, v0

    .line 2092714
    if-nez v0, :cond_4

    .line 2092715
    invoke-virtual {p0}, LX/EDx;->aY()V

    .line 2092716
    :cond_4
    iget-boolean v0, p0, LX/EDx;->aB:Z

    if-nez v0, :cond_5

    iget-object v0, p0, LX/EDx;->K:LX/0yH;

    sget-object v3, LX/0yY;->VOIP_INCOMING_CALL_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v3}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/EDx;->bC:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-short v4, LX/3Dx;->fm:S

    invoke-interface {v0, v3, v4, v1}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2092717
    :cond_5
    invoke-direct {p0}, LX/EDx;->bE()V

    .line 2092718
    :cond_6
    invoke-direct {p0}, LX/EDx;->cj()V

    .line 2092719
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2092720
    iget-object v0, p0, LX/EDx;->bC:LX/0ad;

    sget v3, LX/3Dx;->aH:I

    invoke-interface {v0, v3, v1}, LX/0ad;->a(II)I

    move-result v0

    if-lez v0, :cond_8

    :goto_5
    iput-boolean v2, p0, LX/EDx;->bB:Z

    .line 2092721
    :goto_6
    return-void

    :cond_7
    move v0, v1

    .line 2092722
    goto/16 :goto_2

    :cond_8
    move v2, v1

    .line 2092723
    goto :goto_5

    .line 2092724
    :cond_9
    iget-object v0, p0, LX/EDx;->bC:LX/0ad;

    sget v3, LX/3Dx;->K:I

    invoke-interface {v0, v3, v1}, LX/0ad;->a(II)I

    move-result v0

    if-lez v0, :cond_a

    :goto_7
    iput-boolean v2, p0, LX/EDx;->bA:Z

    goto :goto_6

    :cond_a
    move v2, v1

    goto :goto_7

    .line 2092725
    :cond_b
    new-instance v6, LX/EDo;

    invoke-direct {v6, p0}, LX/EDo;-><init>(LX/EDx;)V

    iput-object v6, p0, LX/EDx;->bN:LX/EDo;

    goto/16 :goto_0

    :cond_c
    iget-boolean v4, v0, LX/D9r;->a:Z

    if-eqz v4, :cond_d

    iget-boolean v4, v0, LX/D9r;->b:Z

    if-eqz v4, :cond_1

    :cond_d
    const/4 v3, 0x1

    goto/16 :goto_1

    .line 2092726
    :catch_0
    move-exception v0

    .line 2092727
    const-string v3, "WebrtcUiHandler"

    const-string v4, "audio service is not available"

    invoke-static {v3, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2092728
    iput-object v5, p0, LX/EDx;->W:Landroid/os/IBinder;

    .line 2092729
    iput-object v5, p0, LX/EDx;->X:Landroid/os/IBinder$DeathRecipient;

    goto :goto_4

    :catch_1
    :goto_8
    move-object v0, v3

    goto/16 :goto_3

    .line 2092730
    :catch_2
    goto :goto_8

    :catch_3
    goto :goto_8

    :catch_4
    goto :goto_8
.end method

.method public static bC(LX/EDx;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 2092569
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2092570
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    invoke-virtual {v0, v1}, LX/2Tm;->c(Z)V

    .line 2092571
    :cond_0
    iget-object v0, p0, LX/EDx;->m:LX/3GP;

    invoke-virtual {v0, v1}, LX/3GP;->a(Z)Z

    move-result v0

    iput-boolean v0, p0, LX/EDx;->bg:Z

    .line 2092572
    iget-object v1, p0, LX/EDx;->l:LX/2S7;

    iget-wide v2, p0, LX/EDx;->ac:J

    iget-wide v4, p0, LX/EDx;->ak:J

    const-string v6, "bluetooth"

    const-string v7, "on"

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 2092573
    return-void
.end method

.method public static bD(LX/EDx;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2092574
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2092575
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    invoke-virtual {v0, v1}, LX/2Tm;->c(Z)V

    .line 2092576
    :cond_0
    iget-object v0, p0, LX/EDx;->m:LX/3GP;

    invoke-virtual {v0, v1}, LX/3GP;->a(Z)Z

    move-result v0

    iput-boolean v0, p0, LX/EDx;->bg:Z

    .line 2092577
    iget-object v1, p0, LX/EDx;->l:LX/2S7;

    iget-wide v2, p0, LX/EDx;->ac:J

    iget-wide v4, p0, LX/EDx;->ak:J

    const-string v6, "bluetooth"

    const-string v7, "off"

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 2092578
    return-void
.end method

.method private bE()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2092579
    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2092580
    iget-boolean v2, p0, LX/EDx;->bQ:Z

    move v2, v2

    .line 2092581
    if-eqz v2, :cond_1

    .line 2092582
    :cond_0
    :goto_0
    return-void

    .line 2092583
    :cond_1
    invoke-virtual {p0}, LX/EDx;->af()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, LX/EDx;->aB:Z

    if-nez v2, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_3

    .line 2092584
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 2092585
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, LX/EDx;->h:Landroid/content/Context;

    const-class v3, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2092586
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2092587
    const-string v2, "CONTACT_NAME"

    invoke-virtual {p0}, LX/EDx;->aA()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2092588
    :goto_2
    :try_start_0
    iget-object v2, p0, LX/EDx;->j:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 2092589
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EDx;->aS:Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2092590
    :catch_0
    move-exception v0

    .line 2092591
    const-string v2, "WebrtcUiHandler"

    const-string v3, "Cannot start in-call notification service due to security limit."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2092592
    goto :goto_1

    .line 2092593
    :cond_4
    const-string v2, "CONTACT_NAME"

    iget-object v3, p0, LX/EDx;->bo:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2
.end method

.method private static bF(LX/EDx;)V
    .locals 3

    .prologue
    .line 2092594
    iget-boolean v0, p0, LX/EDx;->aS:Z

    if-nez v0, :cond_0

    .line 2092595
    :goto_0
    return-void

    .line 2092596
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/EDx;->h:Landroid/content/Context;

    const-class v2, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2092597
    iget-object v1, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 2092598
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EDx;->aS:Z

    goto :goto_0
.end method

.method public static bG(LX/EDx;)Z
    .locals 2

    .prologue
    .line 2092599
    iget-wide v0, p0, LX/EDx;->ak:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    .line 2092600
    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, LX/EDx;->bH()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EDx;->cr:Lcom/facebook/presence/ThreadPresenceManager;

    invoke-virtual {v1, v0}, Lcom/facebook/presence/ThreadPresenceManager;->a(Lcom/facebook/user/model/UserKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2092601
    const/4 v0, 0x1

    .line 2092602
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bH()Z
    .locals 1

    .prologue
    .line 2092603
    invoke-virtual {p0}, LX/EDx;->aV()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->bK:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bK()Z
    .locals 2

    .prologue
    .line 2092604
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 2092605
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static bQ(LX/EDx;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2092606
    iget-object v0, p0, LX/EDx;->W:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 2092607
    :try_start_0
    iget-object v0, p0, LX/EDx;->W:Landroid/os/IBinder;

    iget-object v1, p0, LX/EDx;->X:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2092608
    :goto_0
    iput-object v3, p0, LX/EDx;->W:Landroid/os/IBinder;

    .line 2092609
    iput-object v3, p0, LX/EDx;->X:Landroid/os/IBinder$DeathRecipient;

    .line 2092610
    :cond_0
    return-void

    .line 2092611
    :catch_0
    move-exception v0

    .line 2092612
    const-string v1, "WebrtcUiHandler"

    const-string v2, "failed to unlinkToDeath"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static bR(LX/EDx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2092613
    iget-object v0, p0, LX/EDx;->aa:LX/0Yb;

    if-eqz v0, :cond_0

    .line 2092614
    iget-object v0, p0, LX/EDx;->aa:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2092615
    iput-object v1, p0, LX/EDx;->aa:LX/0Yb;

    .line 2092616
    :cond_0
    iget-object v0, p0, LX/EDx;->Z:LX/0Yb;

    if-eqz v0, :cond_1

    .line 2092617
    iget-object v0, p0, LX/EDx;->Z:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2092618
    iput-object v1, p0, LX/EDx;->Z:LX/0Yb;

    .line 2092619
    :cond_1
    invoke-static {p0}, LX/EDx;->bQ(LX/EDx;)V

    .line 2092620
    return-void
.end method

.method public static bS(LX/EDx;)V
    .locals 2

    .prologue
    .line 2092621
    iget-boolean v0, p0, LX/EDx;->bg:Z

    if-eqz v0, :cond_1

    .line 2092622
    sget-object v0, LX/EDq;->BLUETOOTH:LX/EDq;

    iput-object v0, p0, LX/EDx;->bi:LX/EDq;

    .line 2092623
    :goto_0
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2092624
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    iget-object v1, p0, LX/EDx;->n:LX/34I;

    invoke-virtual {v1}, LX/34I;->c()LX/7TP;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Tm;->a(LX/7TP;)V

    .line 2092625
    :cond_0
    return-void

    .line 2092626
    :cond_1
    iget-boolean v0, p0, LX/EDx;->bf:Z

    if-eqz v0, :cond_2

    .line 2092627
    sget-object v0, LX/EDq;->SPEAKERPHONE:LX/EDq;

    iput-object v0, p0, LX/EDx;->bi:LX/EDq;

    goto :goto_0

    .line 2092628
    :cond_2
    iget-boolean v0, p0, LX/EDx;->be:Z

    if-eqz v0, :cond_3

    .line 2092629
    sget-object v0, LX/EDq;->HEADSET:LX/EDq;

    iput-object v0, p0, LX/EDx;->bi:LX/EDq;

    goto :goto_0

    .line 2092630
    :cond_3
    sget-object v0, LX/EDq;->EARPIECE:LX/EDq;

    iput-object v0, p0, LX/EDx;->bi:LX/EDq;

    goto :goto_0
.end method

.method private bU()V
    .locals 2

    .prologue
    .line 2092631
    iget-object v0, p0, LX/EDx;->m:LX/3GP;

    .line 2092632
    iget-boolean v1, v0, LX/3GP;->h:Z

    move v0, v1

    .line 2092633
    if-eqz v0, :cond_0

    .line 2092634
    invoke-static {p0}, LX/EDx;->bD(LX/EDx;)V

    .line 2092635
    :cond_0
    return-void
.end method

.method private bV()I
    .locals 1

    .prologue
    .line 2092636
    iget-boolean v0, p0, LX/EDx;->ax:Z

    if-nez v0, :cond_2

    .line 2092637
    invoke-static {}, Lorg/webrtc/videoengine/VideoCaptureDeviceInfoAndroid;->c()V

    .line 2092638
    sget v0, Lorg/webrtc/videoengine/VideoCaptureDeviceInfoAndroid;->c:I

    move v0, v0

    .line 2092639
    iput v0, p0, LX/EDx;->ay:I

    .line 2092640
    sget v0, Lorg/webrtc/videoengine/VideoCaptureDeviceInfoAndroid;->d:I

    move v0, v0

    .line 2092641
    iput v0, p0, LX/EDx;->az:I

    .line 2092642
    iget v0, p0, LX/EDx;->ay:I

    if-gez v0, :cond_0

    .line 2092643
    iget v0, p0, LX/EDx;->az:I

    iput v0, p0, LX/EDx;->ay:I

    .line 2092644
    :cond_0
    iget v0, p0, LX/EDx;->az:I

    if-gez v0, :cond_1

    .line 2092645
    iget v0, p0, LX/EDx;->ay:I

    iput v0, p0, LX/EDx;->az:I

    .line 2092646
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EDx;->ax:Z

    .line 2092647
    :cond_2
    iget-boolean v0, p0, LX/EDx;->aA:Z

    if-eqz v0, :cond_3

    iget v0, p0, LX/EDx;->az:I

    :goto_0
    return v0

    :cond_3
    iget v0, p0, LX/EDx;->ay:I

    goto :goto_0
.end method

.method private bW()V
    .locals 14

    .prologue
    .line 2092648
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2092649
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    iget-object v1, p0, LX/EDx;->cp:LX/EDw;

    iget v1, v1, LX/EDw;->a:I

    iget-object v2, p0, LX/EDx;->cp:LX/EDw;

    iget v2, v2, LX/EDw;->b:I

    iget-object v3, p0, LX/EDx;->cp:LX/EDw;

    iget v3, v3, LX/EDw;->c:I

    invoke-virtual {v0, v1, v2, v3}, LX/EFw;->a(III)V

    .line 2092650
    :cond_0
    :goto_0
    return-void

    .line 2092651
    :cond_1
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2092652
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    iget-object v1, p0, LX/EDx;->cp:LX/EDw;

    iget v1, v1, LX/EDw;->a:I

    iget-object v2, p0, LX/EDx;->cp:LX/EDw;

    iget v2, v2, LX/EDw;->b:I

    iget-object v3, p0, LX/EDx;->cp:LX/EDw;

    iget v3, v3, LX/EDw;->c:I

    iget-object v4, p0, LX/EDx;->cq:LX/EDw;

    iget v4, v4, LX/EDw;->a:I

    iget-object v5, p0, LX/EDx;->cq:LX/EDw;

    iget v5, v5, LX/EDw;->b:I

    iget-object v6, p0, LX/EDx;->cq:LX/EDw;

    iget v6, v6, LX/EDw;->c:I

    .line 2092653
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2092654
    iget-object v7, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    move v8, v1

    move v9, v2

    move v10, v3

    move v11, v4

    move v12, v5

    move v13, v6

    invoke-virtual/range {v7 .. v13}, Lcom/facebook/webrtc/WebrtcEngine;->setVideoParameters(IIIIII)V

    .line 2092655
    :cond_2
    goto :goto_0
.end method

.method private static bX(LX/EDx;)V
    .locals 2

    .prologue
    .line 2092656
    iget-object v0, p0, LX/EDx;->ci:LX/EHZ;

    if-eqz v0, :cond_0

    .line 2092657
    iget-object v0, p0, LX/EDx;->ci:LX/EHZ;

    .line 2092658
    invoke-virtual {v0}, LX/EHZ;->f()V

    .line 2092659
    iget-object v1, v0, LX/EHZ;->e:LX/6Jt;

    invoke-virtual {v1}, LX/6Jt;->d()V

    .line 2092660
    const/4 v0, 0x0

    iput-object v0, p0, LX/EDx;->ci:LX/EHZ;

    .line 2092661
    :cond_0
    return-void
.end method

.method private bZ()J
    .locals 4

    .prologue
    .line 2092566
    invoke-virtual {p0}, LX/EDx;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2092567
    const-wide/16 v0, 0x0

    .line 2092568
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v0

    iget-wide v2, p0, LX/EDx;->aM:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public static bt(LX/EDx;)LX/EFy;
    .locals 1

    .prologue
    .line 2092731
    sget-object v0, LX/EG5;->Override:LX/EG5;

    invoke-virtual {p0, v0}, LX/EDx;->a(LX/EG5;)LX/EFy;

    move-result-object v0

    return-object v0
.end method

.method public static bu(LX/EDx;)V
    .locals 3

    .prologue
    .line 2092732
    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v0

    iput-wide v0, p0, LX/EDx;->aK:J

    .line 2092733
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EDx;->aL:J

    .line 2092734
    const-string v0, "com.facebook.rtc.fbwebrtc.intent.action.SHOW_UI"

    invoke-direct {p0, v0}, LX/EDx;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2092735
    iget-object v1, p0, LX/EDx;->j:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2092736
    return-void
.end method

.method private bv()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2092737
    iput-object v1, p0, LX/EDx;->Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    .line 2092738
    iput v3, p0, LX/EDx;->am:I

    .line 2092739
    invoke-static {p0, v1}, LX/EDx;->a(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;)V

    .line 2092740
    iput-boolean v3, p0, LX/EDx;->bZ:Z

    .line 2092741
    iput-boolean v3, p0, LX/EDx;->cb:Z

    .line 2092742
    iput v3, p0, LX/EDx;->ca:I

    .line 2092743
    iput-object v1, p0, LX/EDx;->ae:Ljava/util/Map;

    .line 2092744
    iput-object v1, p0, LX/EDx;->af:LX/EGE;

    .line 2092745
    iput-boolean v3, p0, LX/EDx;->ai:Z

    .line 2092746
    iput-boolean v3, p0, LX/EDx;->ag:Z

    .line 2092747
    iput-wide v4, p0, LX/EDx;->ac:J

    .line 2092748
    iput-wide v4, p0, LX/EDx;->ak:J

    .line 2092749
    iput-object v1, p0, LX/EDx;->al:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2092750
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/EDx;->bv:LX/03R;

    .line 2092751
    iput-boolean v3, p0, LX/EDx;->bw:Z

    .line 2092752
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/EDx;->bP:LX/03R;

    .line 2092753
    iput-boolean v3, p0, LX/EDx;->bQ:Z

    .line 2092754
    sget-object v0, LX/EDu;->NONE:LX/EDu;

    iput-object v0, p0, LX/EDx;->bR:LX/EDu;

    .line 2092755
    iput-boolean v3, p0, LX/EDx;->aB:Z

    .line 2092756
    iput-boolean v3, p0, LX/EDx;->aC:Z

    .line 2092757
    iput-wide v4, p0, LX/EDx;->aD:J

    .line 2092758
    iput-object v1, p0, LX/EDx;->aE:Ljava/lang/String;

    .line 2092759
    iput-boolean v3, p0, LX/EDx;->aF:Z

    .line 2092760
    iput-boolean v3, p0, LX/EDx;->aH:Z

    .line 2092761
    iput-boolean v3, p0, LX/EDx;->aP:Z

    .line 2092762
    iput-object v1, p0, LX/EDx;->bO:Ljava/lang/String;

    .line 2092763
    iput-wide v4, p0, LX/EDx;->aI:J

    .line 2092764
    iput-wide v4, p0, LX/EDx;->aJ:J

    .line 2092765
    iput-wide v4, p0, LX/EDx;->aK:J

    .line 2092766
    iput-wide v4, p0, LX/EDx;->aL:J

    .line 2092767
    iput-wide v4, p0, LX/EDx;->aM:J

    .line 2092768
    iput-wide v4, p0, LX/EDx;->aN:J

    .line 2092769
    iput-object v1, p0, LX/EDx;->bl:LX/7TQ;

    .line 2092770
    const-string v0, ""

    iput-object v0, p0, LX/EDx;->bo:Ljava/lang/String;

    .line 2092771
    const-string v0, ""

    iput-object v0, p0, LX/EDx;->bn:Ljava/lang/String;

    .line 2092772
    iput-object v1, p0, LX/EDx;->bN:LX/EDo;

    .line 2092773
    iput-boolean v3, p0, LX/EDx;->aQ:Z

    .line 2092774
    iput-boolean v3, p0, LX/EDx;->aR:Z

    .line 2092775
    iput v3, p0, LX/EDx;->aO:I

    .line 2092776
    iput-boolean v3, p0, LX/EDx;->aT:Z

    .line 2092777
    sget-object v0, LX/EDs;->NONE:LX/EDs;

    iput-object v0, p0, LX/EDx;->cl:LX/EDs;

    .line 2092778
    sget-object v0, LX/EDr;->DISCONNECTED:LX/EDr;

    iput-object v0, p0, LX/EDx;->cm:LX/EDr;

    .line 2092779
    iput-boolean v3, p0, LX/EDx;->ao:Z

    .line 2092780
    iput-boolean v3, p0, LX/EDx;->ap:Z

    .line 2092781
    sget-object v0, LX/EDv;->STOPPED:LX/EDv;

    iput-object v0, p0, LX/EDx;->ar:LX/EDv;

    .line 2092782
    iput-boolean v3, p0, LX/EDx;->aq:Z

    .line 2092783
    iput-boolean v3, p0, LX/EDx;->as:Z

    .line 2092784
    iput v3, p0, LX/EDx;->at:I

    .line 2092785
    iput v3, p0, LX/EDx;->au:I

    .line 2092786
    iput-boolean v3, p0, LX/EDx;->bm:Z

    .line 2092787
    const/4 v8, 0x0

    .line 2092788
    sget-object v0, LX/2S4;->M:LX/0Tn;

    invoke-static {p0, v0, v8}, LX/EDx;->a(LX/EDx;LX/0Tn;I)I

    move-result v7

    .line 2092789
    sget-object v0, LX/2S4;->N:LX/0Tn;

    invoke-static {p0, v0, v8}, LX/EDx;->a(LX/EDx;LX/0Tn;I)I

    move-result v0

    .line 2092790
    if-lez v7, :cond_0

    if-gtz v0, :cond_1

    .line 2092791
    :cond_0
    const/16 v7, 0x280

    .line 2092792
    const/16 v0, 0x180

    .line 2092793
    :cond_1
    new-instance v8, LX/EDw;

    const/4 v9, -0x1

    invoke-direct {v8, v7, v0, v9}, LX/EDw;-><init>(III)V

    move-object v0, v8

    .line 2092794
    iput-object v0, p0, LX/EDx;->cp:LX/EDw;

    .line 2092795
    new-instance v0, LX/EDw;

    invoke-direct {v0}, LX/EDw;-><init>()V

    iput-object v0, p0, LX/EDx;->cq:LX/EDw;

    .line 2092796
    iput-object v1, p0, LX/EDx;->av:Ljava/lang/String;

    .line 2092797
    iput-wide v4, p0, LX/EDx;->aw:J

    .line 2092798
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    .line 2092799
    iput v2, v0, LX/2S7;->F:I

    .line 2092800
    iput-boolean v3, p0, LX/EDx;->ax:Z

    .line 2092801
    iput v2, p0, LX/EDx;->ay:I

    .line 2092802
    iput v2, p0, LX/EDx;->az:I

    .line 2092803
    iput-boolean v3, p0, LX/EDx;->aA:Z

    .line 2092804
    iput-boolean v3, p0, LX/EDx;->aV:Z

    .line 2092805
    iput-boolean v3, p0, LX/EDx;->bd:Z

    .line 2092806
    iput-boolean v3, p0, LX/EDx;->be:Z

    .line 2092807
    iput-boolean v3, p0, LX/EDx;->bs:Z

    .line 2092808
    iput-boolean v3, p0, LX/EDx;->bt:Z

    .line 2092809
    iput-boolean v3, p0, LX/EDx;->bu:Z

    .line 2092810
    iput-boolean v3, p0, LX/EDx;->bx:Z

    .line 2092811
    iput-object v1, p0, LX/EDx;->bL:Landroid/view/View;

    .line 2092812
    iput-boolean v3, p0, LX/EDx;->aX:Z

    .line 2092813
    iput v3, p0, LX/EDx;->bE:I

    .line 2092814
    iput-wide v4, p0, LX/EDx;->bF:J

    .line 2092815
    iput-wide v4, p0, LX/EDx;->bG:J

    .line 2092816
    iput-wide v4, p0, LX/EDx;->bH:J

    .line 2092817
    iput-wide v4, p0, LX/EDx;->bI:J

    .line 2092818
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    invoke-virtual {v0, v2, v3, v3}, LX/2S7;->a(III)V

    .line 2092819
    invoke-static {p0, v3}, LX/EDx;->q(LX/EDx;Z)V

    .line 2092820
    invoke-static {p0, v3, v6, v6}, LX/EDx;->a(LX/EDx;ZZZ)V

    .line 2092821
    iput-boolean v3, p0, LX/EDx;->bg:Z

    .line 2092822
    iput-boolean v3, p0, LX/EDx;->bX:Z

    .line 2092823
    iput-boolean v3, p0, LX/EDx;->aY:Z

    .line 2092824
    invoke-virtual {p0, v3}, LX/EDx;->n(Z)V

    .line 2092825
    iput-boolean v3, p0, LX/EDx;->aZ:Z

    .line 2092826
    iput-boolean v3, p0, LX/EDx;->by:Z

    .line 2092827
    iput-boolean v3, p0, LX/EDx;->bz:Z

    .line 2092828
    iput-boolean v3, p0, LX/EDx;->cc:Z

    .line 2092829
    iput-boolean v3, p0, LX/EDx;->bB:Z

    .line 2092830
    iput-boolean v3, p0, LX/EDx;->bA:Z

    .line 2092831
    iput-boolean v3, p0, LX/EDx;->cd:Z

    .line 2092832
    iput-boolean v3, p0, LX/EDx;->ce:Z

    .line 2092833
    iput-boolean v3, p0, LX/EDx;->cf:Z

    .line 2092834
    iput-boolean v3, p0, LX/EDx;->ba:Z

    .line 2092835
    iput-boolean v3, p0, LX/EDx;->aj:Z

    .line 2092836
    sget-object v0, LX/EDx;->cj:LX/EDt;

    iput-object v0, p0, LX/EDx;->ck:LX/EDt;

    .line 2092837
    invoke-static {p0}, LX/EDx;->co(LX/EDx;)V

    .line 2092838
    iget-object v0, p0, LX/EDx;->bb:LX/EIq;

    invoke-virtual {v0}, LX/EIq;->a()V

    .line 2092839
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, LX/EDx;->h:Landroid/content/Context;

    const v2, 0x7f080782

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, LX/EDx;->h:Landroid/content/Context;

    const v2, 0x7f080783

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x2

    iget-object v2, p0, LX/EDx;->h:Landroid/content/Context;

    const v3, 0x7f080784

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 2092840
    iput-object v0, p0, LX/EDx;->g:[Ljava/lang/String;

    .line 2092841
    invoke-static {p0}, LX/EDx;->bw(LX/EDx;)V

    .line 2092842
    return-void
.end method

.method private static bw(LX/EDx;)V
    .locals 2

    .prologue
    .line 2092843
    iget-object v0, p0, LX/EDx;->bW:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2092844
    iget-object v0, p0, LX/EDx;->bW:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2092845
    const/4 v0, 0x0

    iput-object v0, p0, LX/EDx;->bW:Ljava/util/concurrent/ScheduledFuture;

    .line 2092846
    :cond_0
    return-void
.end method

.method private by()V
    .locals 5

    .prologue
    .line 2092847
    iget-object v0, p0, LX/EDx;->n:LX/34I;

    .line 2092848
    const-string v1, "android.permission.BLUETOOTH"

    .line 2092849
    iget-object v2, v0, LX/34I;->b:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    .line 2092850
    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2092851
    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-gt v0, v1, :cond_1

    .line 2092852
    :cond_0
    :goto_1
    return-void

    .line 2092853
    :cond_1
    iget-object v0, p0, LX/EDx;->m:LX/3GP;

    new-instance v1, LX/EDp;

    invoke-direct {v1, p0}, LX/EDp;-><init>(LX/EDx;)V

    .line 2092854
    invoke-virtual {v0}, LX/3GP;->a()V

    .line 2092855
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    iput-object v2, v0, LX/3GP;->e:Landroid/bluetooth/BluetoothAdapter;

    .line 2092856
    iget-object v2, v0, LX/3GP;->e:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v2, :cond_3

    iget-object v2, v0, LX/3GP;->e:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2092857
    iget-object v2, v0, LX/3GP;->e:Landroid/bluetooth/BluetoothAdapter;

    iget-object v3, v0, LX/3GP;->b:Landroid/content/Context;

    new-instance v4, LX/EDD;

    invoke-direct {v4, v0}, LX/EDD;-><init>(LX/3GP;)V

    const/4 p0, 0x1

    invoke-virtual {v2, v3, v4, p0}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 2092858
    new-instance v2, LX/EDE;

    invoke-direct {v2, v0}, LX/EDE;-><init>(LX/3GP;)V

    iput-object v2, v0, LX/3GP;->d:LX/EDE;

    .line 2092859
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 2092860
    const-string v3, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2092861
    const-string v3, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2092862
    iget-object v3, v0, LX/3GP;->b:Landroid/content/Context;

    iget-object v4, v0, LX/3GP;->d:LX/EDE;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2092863
    :goto_2
    iput-object v1, v0, LX/3GP;->g:LX/EDp;

    .line 2092864
    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 2092865
    :cond_3
    const/4 v2, 0x0

    iput-object v2, v0, LX/3GP;->e:Landroid/bluetooth/BluetoothAdapter;

    goto :goto_2
.end method

.method private static bz(LX/EDx;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2092866
    iget-object v0, p0, LX/EDx;->cv:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    invoke-virtual {v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->shouldEnableVideo()Z

    move-result v0

    iput-boolean v0, p0, LX/EDx;->ao:Z

    .line 2092867
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aq()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2092868
    :cond_0
    iget-boolean v0, p0, LX/EDx;->ao:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EDx;->M:LX/0Uh;

    const/16 v2, 0x5f6

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/EDx;->ao:Z

    .line 2092869
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 2092870
    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2092871
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/EDx;->h:Landroid/content/Context;

    const-class v2, Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2092872
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2092873
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2092874
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2092875
    return-object v0
.end method

.method private c(I)V
    .locals 1

    .prologue
    .line 2092876
    :try_start_0
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2092877
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static c(LX/EDx;ZZ)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2092878
    iput-boolean v0, p0, LX/EDx;->aY:Z

    .line 2092879
    iput-boolean v3, p0, LX/EDx;->ce:Z

    .line 2092880
    iget-object v1, p0, LX/EDx;->bS:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/EDx;->aX:Z

    if-eqz v1, :cond_0

    .line 2092881
    iget-object v1, p0, LX/EDx;->bS:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2092882
    const/4 v1, 0x0

    iput-object v1, p0, LX/EDx;->bS:Ljava/util/concurrent/ScheduledFuture;

    .line 2092883
    :cond_0
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2092884
    invoke-direct {p0}, LX/EDx;->ck()V

    .line 2092885
    :cond_1
    iput-boolean v0, p0, LX/EDx;->aF:Z

    .line 2092886
    iget-object v1, p0, LX/EDx;->q:LX/2XQ;

    invoke-virtual {p0}, LX/EDx;->aa()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/2XQ;->a(Z)V

    .line 2092887
    const/4 v2, 0x0

    .line 2092888
    iget-object v1, p0, LX/EDx;->t:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_2

    .line 2092889
    new-instance v1, LX/EDd;

    invoke-direct {v1, p0}, LX/EDd;-><init>(LX/EDx;)V

    iput-object v1, p0, LX/EDx;->U:Landroid/telephony/PhoneStateListener;

    .line 2092890
    iput-object v2, p0, LX/EDx;->aU:Ljava/lang/reflect/Method;

    .line 2092891
    :try_start_0
    const-class v1, Landroid/telephony/SignalStrength;

    const-string v2, "getLevel"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->aU:Ljava/lang/reflect/Method;

    .line 2092892
    iget-object v1, p0, LX/EDx;->aU:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2092893
    :goto_0
    iget-object v1, p0, LX/EDx;->t:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, LX/EDx;->U:Landroid/telephony/PhoneStateListener;

    const/16 v4, 0x120

    invoke-virtual {v1, v2, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 2092894
    :cond_2
    iget-object v1, p0, LX/EDx;->L:LX/0u7;

    iget-object v2, p0, LX/EDx;->V:LX/2Zz;

    invoke-virtual {v1, v2}, LX/0u7;->a(LX/2Zz;)V

    .line 2092895
    invoke-virtual {p0}, LX/EDx;->j()Z

    .line 2092896
    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2092897
    iget-boolean v1, p0, LX/EDx;->aC:Z

    move v1, v1

    .line 2092898
    if-eqz v1, :cond_4

    .line 2092899
    invoke-virtual {p0, v0, v3}, LX/EDx;->a(ZZ)V

    .line 2092900
    :cond_3
    :goto_1
    iput-boolean p2, p0, LX/EDx;->ba:Z

    .line 2092901
    iget-object v1, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_2
    if-ge v1, v2, :cond_c

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2092902
    invoke-virtual {v0, p1, p2}, LX/EC0;->a(ZZ)V

    .line 2092903
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2092904
    :cond_4
    iget-boolean v1, p0, LX/EDx;->aB:Z

    move v1, v1

    .line 2092905
    if-nez v1, :cond_5

    .line 2092906
    iget-object v1, p0, LX/EDx;->J:LX/ECO;

    invoke-virtual {v1, p1, v3}, LX/ECO;->a(ZZ)V

    .line 2092907
    invoke-direct {p0}, LX/EDx;->cd()V

    goto :goto_1

    .line 2092908
    :cond_5
    iget-boolean v1, p0, LX/EDx;->aB:Z

    move v1, v1

    .line 2092909
    if-eqz v1, :cond_3

    .line 2092910
    iget-object v1, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v1, :cond_6

    .line 2092911
    iget-object v1, p0, LX/EDx;->T:LX/2Tm;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/2Tm;->b(Z)V

    .line 2092912
    :cond_6
    invoke-virtual {p0, v0}, LX/EDx;->g(Z)V

    .line 2092913
    invoke-virtual {p0}, LX/EDx;->aY()V

    .line 2092914
    goto :goto_1

    .line 2092915
    :cond_7
    iget-object v1, p0, LX/EDx;->bv:LX/03R;

    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v1

    if-eq v1, p1, :cond_8

    .line 2092916
    iput-boolean v3, p0, LX/EDx;->bx:Z

    .line 2092917
    :cond_8
    invoke-static {p1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    iput-object v1, p0, LX/EDx;->bv:LX/03R;

    .line 2092918
    iput-boolean p1, p0, LX/EDx;->bw:Z

    .line 2092919
    iget-boolean v1, p0, LX/EDx;->aB:Z

    move v1, v1

    .line 2092920
    if-eqz v1, :cond_9

    .line 2092921
    invoke-static {p0}, LX/EDx;->ce(LX/EDx;)V

    goto :goto_1

    .line 2092922
    :cond_9
    invoke-virtual {p0}, LX/EDx;->al()V

    .line 2092923
    iget-boolean v1, p0, LX/EDx;->bQ:Z

    move v1, v1

    .line 2092924
    if-nez v1, :cond_a

    .line 2092925
    invoke-static {p0}, LX/EDx;->cf(LX/EDx;)V

    .line 2092926
    :cond_a
    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v1

    if-nez v1, :cond_b

    .line 2092927
    invoke-virtual {p0}, LX/EDx;->ai()V

    .line 2092928
    :cond_b
    iget-boolean v1, p0, LX/EDx;->aj:Z

    move v1, v1

    .line 2092929
    if-nez v1, :cond_3

    .line 2092930
    iget-object v1, p0, LX/EDx;->J:LX/ECO;

    invoke-virtual {v1}, LX/ECO;->c()V

    goto :goto_1

    .line 2092931
    :cond_c
    return-void

    :catch_0
    goto/16 :goto_0
.end method

.method private static cB(LX/EDx;)V
    .locals 0

    .prologue
    .line 2092932
    invoke-static {p0}, LX/EDx;->bz(LX/EDx;)V

    .line 2092933
    invoke-static {p0}, LX/EDx;->cI(LX/EDx;)V

    .line 2092934
    return-void
.end method

.method private static cC(LX/EDx;)V
    .locals 5

    .prologue
    .line 2092935
    iget-object v0, p0, LX/EDx;->ae:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2092936
    :cond_0
    :goto_0
    return-void

    .line 2092937
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2092938
    iget-object v0, p0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2092939
    iget-object v3, v0, LX/EGE;->d:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v0, LX/EGE;->c:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2092940
    :cond_3
    iget-object v3, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-static {v3}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    .line 2092941
    iget-object v3, p0, LX/EDx;->F:LX/EG0;

    invoke-interface {v3}, LX/EG0;->a()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v3

    .line 2092942
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 2092943
    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/EGE;->c:Ljava/lang/String;

    .line 2092944
    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, LX/EGE;->d:Ljava/lang/String;

    goto :goto_1

    .line 2092945
    :cond_4
    iget-object v0, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2092946
    :cond_5
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2092947
    iget-object v0, p0, LX/EDx;->F:LX/EG0;

    invoke-interface {v0}, LX/EG0;->c()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2092948
    new-instance v1, LX/EDi;

    invoke-direct {v1, p0}, LX/EDi;-><init>(LX/EDx;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method private cD()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2092949
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 2092950
    iget-object v0, p0, LX/EDx;->f:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 2092951
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v0, v1

    .line 2092952
    :goto_0
    return v0

    .line 2092953
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 2092954
    iget-object v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2092955
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v2, 0x64

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 2092956
    goto :goto_0
.end method

.method public static cH(LX/EDx;)V
    .locals 3

    .prologue
    .line 2092957
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2092958
    invoke-virtual {v0}, LX/EC0;->q()V

    .line 2092959
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2092960
    :cond_0
    return-void
.end method

.method public static cI(LX/EDx;)V
    .locals 3

    .prologue
    .line 2092961
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2092962
    invoke-virtual {v0}, LX/EC0;->o()V

    .line 2092963
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2092964
    :cond_0
    return-void
.end method

.method private cb()V
    .locals 1

    .prologue
    .line 2093880
    iget-object v0, p0, LX/EDx;->p:LX/ECx;

    invoke-virtual {v0}, LX/ECx;->e()V

    .line 2093881
    iget-object v0, p0, LX/EDx;->p:LX/ECx;

    invoke-virtual {v0}, LX/ECx;->c()V

    .line 2093882
    return-void
.end method

.method public static cc(LX/EDx;)V
    .locals 1

    .prologue
    .line 2093704
    iget-object v0, p0, LX/EDx;->p:LX/ECx;

    invoke-virtual {v0}, LX/ECx;->f()V

    .line 2093705
    iget-object v0, p0, LX/EDx;->p:LX/ECx;

    invoke-virtual {v0}, LX/ECx;->b()V

    .line 2093706
    iget-object v0, p0, LX/EDx;->p:LX/ECx;

    invoke-virtual {v0}, LX/ECx;->d()V

    .line 2093707
    return-void
.end method

.method private cd()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2093891
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v3

    .line 2093892
    iget-wide v4, p0, LX/EDx;->ak:J

    iget-object v0, p0, LX/EDx;->cv:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    invoke-virtual {v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->getUserId()J

    move-result-wide v6

    invoke-direct {p0, v4, v5, v6, v7}, LX/EDx;->a(JJ)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    .line 2093893
    :goto_0
    if-eq v3, v2, :cond_1

    const/4 v2, 0x2

    if-ne v3, v2, :cond_2

    if-eqz v0, :cond_2

    .line 2093894
    :cond_1
    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2093895
    iget-object v0, p0, LX/EDx;->G:Landroid/os/Vibrator;

    sget-object v1, LX/EDx;->e:[J

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 2093896
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v1

    .line 2093897
    goto :goto_0

    .line 2093898
    :cond_4
    iget-object v0, p0, LX/EDx;->G:Landroid/os/Vibrator;

    sget-object v2, LX/EDx;->d:[J

    invoke-virtual {v0, v2, v1}, Landroid/os/Vibrator;->vibrate([JI)V

    goto :goto_1
.end method

.method public static ce(LX/EDx;)V
    .locals 1

    .prologue
    .line 2093899
    iget-object v0, p0, LX/EDx;->G:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 2093900
    return-void
.end method

.method private static cf(LX/EDx;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2093901
    iget-boolean v0, p0, LX/EDx;->bg:Z

    if-eqz v0, :cond_0

    .line 2093902
    :goto_0
    return-void

    .line 2093903
    :cond_0
    iget-object v0, p0, LX/EDx;->n:LX/34I;

    const/4 v5, 0x3

    const/4 v1, -0x1

    const/4 v4, 0x0

    .line 2093904
    invoke-virtual {v0}, LX/34I;->a()Z

    move-result v3

    if-nez v3, :cond_7

    .line 2093905
    :cond_1
    :goto_1
    move v0, v1

    .line 2093906
    if-ltz v0, :cond_3

    .line 2093907
    invoke-direct {p0, v0}, LX/EDx;->c(I)V

    .line 2093908
    iget-object v1, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 2093909
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    iget-object v1, p0, LX/EDx;->n:LX/34I;

    invoke-virtual {v1}, LX/34I;->a()Z

    move-result v1

    .line 2093910
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "client_event"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2093911
    const-string v3, "content"

    const-string v4, "failed_set_audio_mode"

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2093912
    const-string v3, "modify_audio"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2093913
    const-string v3, "android_sdk"

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2093914
    invoke-static {v0, v2}, LX/2S7;->a(LX/2S7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2093915
    :cond_2
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    invoke-virtual {v0}, LX/2S7;->a()V

    goto :goto_0

    .line 2093916
    :cond_3
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    iget-boolean v1, p0, LX/EDx;->bf:Z

    if-eq v0, v1, :cond_4

    .line 2093917
    iget-boolean v0, p0, LX/EDx;->bf:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2093918
    :cond_4
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    iget-boolean v1, p0, LX/EDx;->bf:Z

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 2093919
    invoke-static {p0}, LX/EDx;->bS(LX/EDx;)V

    .line 2093920
    iget-boolean v0, p0, LX/EDx;->bf:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_5

    .line 2093921
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    iget-object v1, p0, LX/EDx;->n:LX/34I;

    invoke-virtual {v1}, LX/34I;->c()LX/7TP;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Tm;->a(LX/7TP;)V

    .line 2093922
    :cond_5
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2093923
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2093924
    :cond_6
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 2093925
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    invoke-virtual {v0}, LX/2S7;->a()V

    goto/16 :goto_0

    .line 2093926
    :cond_7
    iget-object v3, v0, LX/34I;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/2S4;->b:LX/0Tn;

    const-string v7, "-1"

    invoke-interface {v3, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 2093927
    const/4 v6, -0x2

    if-ne v3, v6, :cond_9

    .line 2093928
    iget-object v3, v0, LX/34I;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/2S4;->d:LX/0Tn;

    invoke-interface {v3, v6, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 2093929
    if-ltz v1, :cond_8

    if-le v1, v5, :cond_1

    .line 2093930
    :cond_8
    iget-object v1, v0, LX/34I;->f:LX/0Uh;

    const/16 v3, 0x66e

    invoke-virtual {v1, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_a

    move v1, v4

    .line 2093931
    goto/16 :goto_1

    .line 2093932
    :cond_9
    if-ltz v3, :cond_8

    move v1, v3

    .line 2093933
    goto/16 :goto_1

    .line 2093934
    :cond_a
    iget-object v1, v0, LX/34I;->f:LX/0Uh;

    const/16 v3, 0x66d

    invoke-virtual {v1, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2093935
    const/4 v1, 0x2

    goto/16 :goto_1

    :cond_b
    move v1, v5

    .line 2093936
    goto/16 :goto_1
.end method

.method public static ch(LX/EDx;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2093937
    invoke-direct {p0}, LX/EDx;->cb()V

    .line 2093938
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EDx;->aY:Z

    .line 2093939
    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->bQ:Z

    if-eqz v0, :cond_1

    .line 2093940
    :cond_0
    :goto_0
    return-void

    .line 2093941
    :cond_1
    iput-boolean v2, p0, LX/EDx;->aT:Z

    .line 2093942
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    invoke-virtual {v0}, LX/ECO;->b()Z

    .line 2093943
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    sget-object v1, LX/ECN;->RINGBACK:LX/ECN;

    invoke-virtual {v0, v1}, LX/ECO;->a(LX/ECN;)V

    .line 2093944
    iput-boolean v2, p0, LX/EDx;->aF:Z

    .line 2093945
    invoke-static {p0}, LX/EDx;->cI(LX/EDx;)V

    goto :goto_0
.end method

.method public static declared-synchronized ci(LX/EDx;)V
    .locals 3

    .prologue
    .line 2093946
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/EDx;->bp:Z

    if-eqz v0, :cond_0

    .line 2093947
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    iget-object v1, p0, LX/EDx;->aW:Landroid/content/ServiceConnection;

    const v2, 0x52f3c05

    invoke-static {v0, v1, v2}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    .line 2093948
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EDx;->bp:Z

    .line 2093949
    const/4 v0, 0x0

    iput-object v0, p0, LX/EDx;->bq:LX/EGe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2093950
    :cond_0
    monitor-exit p0

    return-void

    .line 2093951
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized cj()V
    .locals 5

    .prologue
    .line 2093952
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2093953
    iget-object v0, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2093954
    const/4 v0, 0x0

    iput-object v0, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    .line 2093955
    invoke-static {p0}, LX/EDx;->ci(LX/EDx;)V

    .line 2093956
    :cond_0
    invoke-virtual {p0}, LX/EDx;->aV()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/EDx;->bp:Z

    if-nez v0, :cond_2

    .line 2093957
    iget-object v0, p0, LX/EDx;->aW:Landroid/content/ServiceConnection;

    if-nez v0, :cond_1

    .line 2093958
    new-instance v0, LX/EDc;

    invoke-direct {v0, p0}, LX/EDc;-><init>(LX/EDx;)V

    iput-object v0, p0, LX/EDx;->aW:Landroid/content/ServiceConnection;

    .line 2093959
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/EDx;->h:Landroid/content/Context;

    const-class v2, LX/EGe;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2093960
    iget-object v1, p0, LX/EDx;->h:Landroid/content/Context;

    iget-object v2, p0, LX/EDx;->aW:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    const v4, 0x6fc2b31

    invoke-static {v1, v0, v2, v3, v4}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2093961
    :cond_2
    monitor-exit p0

    return-void

    .line 2093962
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private ck()V
    .locals 4

    .prologue
    .line 2093963
    iget-object v0, p0, LX/EDx;->D:Landroid/os/Handler;

    iget-object v1, p0, LX/EDx;->cB:Ljava/lang/Runnable;

    const v2, -0x42648bdb

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2093964
    iget-boolean v0, p0, LX/EDx;->bQ:Z

    move v0, v0

    .line 2093965
    if-nez v0, :cond_0

    .line 2093966
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    .line 2093967
    iget-object v1, v0, LX/ECO;->g:LX/ECG;

    .line 2093968
    invoke-virtual {v1}, LX/ECG;->d()V

    .line 2093969
    invoke-virtual {v1}, LX/ECG;->e()V

    .line 2093970
    new-instance v2, LX/ECF;

    invoke-direct {v2, v1}, LX/ECF;-><init>(LX/ECG;)V

    iput-object v2, v1, LX/ECG;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 2093971
    iget-object v2, v1, LX/ECG;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x0

    const/4 v0, 0x1

    invoke-static {v1, v2, v3, v0}, LX/ECG;->a(LX/ECG;Landroid/media/AudioManager$OnAudioFocusChangeListener;II)Z

    move-result v2

    .line 2093972
    if-eqz v2, :cond_0

    .line 2093973
    const-string v2, "Failed to get audio focus for call"

    invoke-static {v1, v2}, LX/ECG;->a$redex0(LX/ECG;Ljava/lang/String;)V

    .line 2093974
    :cond_0
    const/4 v0, 0x3

    invoke-static {p0, v0}, LX/EDx;->h(LX/EDx;I)V

    .line 2093975
    return-void
.end method

.method private static co(LX/EDx;)V
    .locals 2

    .prologue
    .line 2093976
    iget-object v0, p0, LX/EDx;->bV:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2093977
    iget-object v0, p0, LX/EDx;->bV:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2093978
    const/4 v0, 0x0

    iput-object v0, p0, LX/EDx;->bV:Ljava/util/concurrent/ScheduledFuture;

    .line 2093979
    :cond_0
    return-void
.end method

.method private static cp(LX/EDx;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2093883
    iget-object v0, p0, LX/EDx;->bO:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093884
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2093885
    :goto_0
    return-object v0

    .line 2093886
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/EDx;->I:LX/0lB;

    iget-object v1, p0, LX/EDx;->bO:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2093887
    iget-object v1, p0, LX/EDx;->I:LX/0lB;

    const-class v2, LX/0P1;

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2093888
    :catch_0
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2093889
    goto :goto_0
.end method

.method public static cr(LX/EDx;)V
    .locals 3

    .prologue
    .line 2093980
    invoke-virtual {p0}, LX/EDx;->ae()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2093981
    :cond_0
    return-void

    .line 2093982
    :cond_1
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2093983
    invoke-virtual {v0}, LX/EC0;->i()V

    .line 2093984
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static ct(LX/EDx;)V
    .locals 2

    .prologue
    .line 2093985
    iget-object v0, p0, LX/EDx;->D:Landroid/os/Handler;

    iget-object v1, p0, LX/EDx;->cB:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2093986
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/EDx;->x(LX/EDx;Z)V

    .line 2093987
    return-void
.end method

.method private static cu(LX/EDx;)V
    .locals 1

    .prologue
    .line 2093988
    iget-object v0, p0, LX/EDx;->cu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2093989
    return-void
.end method

.method private static cw(LX/EDx;)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 2093990
    iget-wide v2, p0, LX/EDx;->aN:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/EDx;->aM:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    .line 2093991
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-wide v0, p0, LX/EDx;->aN:J

    iget-wide v2, p0, LX/EDx;->aM:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method private static cz(LX/EDx;)Z
    .locals 1

    .prologue
    .line 2093998
    invoke-virtual {p0}, LX/EDx;->aS()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->aB:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->aF:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/EDx;ZZ)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2094046
    iget-object v0, p0, LX/EDx;->x:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, p0, LX/EDx;->aI:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    .line 2094047
    const-wide v6, 0x40b3880000000000L    # 5000.0

    cmpg-double v0, v4, v6

    if-gez v0, :cond_1

    move v0, v1

    .line 2094048
    :goto_0
    iget-object v3, p0, LX/EDx;->bl:LX/7TQ;

    move-object v3, v3

    .line 2094049
    sget-object v4, LX/7TQ;->CallEndNoPermission:LX/7TQ;

    if-ne v3, v4, :cond_2

    move v3, v1

    .line 2094050
    :goto_1
    iget-object v4, p0, LX/EDx;->bl:LX/7TQ;

    move-object v4, v4

    .line 2094051
    sget-object v5, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    if-ne v4, v5, :cond_3

    move v4, v1

    .line 2094052
    :goto_2
    if-eqz v3, :cond_4

    if-nez p2, :cond_4

    .line 2094053
    :cond_0
    :goto_3
    return v2

    :cond_1
    move v0, v2

    .line 2094054
    goto :goto_0

    :cond_2
    move v3, v2

    .line 2094055
    goto :goto_1

    :cond_3
    move v4, v2

    .line 2094056
    goto :goto_2

    .line 2094057
    :cond_4
    iget-boolean v5, p0, LX/EDx;->aB:Z

    if-eqz v5, :cond_0

    .line 2094058
    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v5

    if-eqz v5, :cond_5

    if-eqz p1, :cond_0

    .line 2094059
    :cond_5
    iget-boolean v5, p0, LX/EDx;->bQ:Z

    move v5, v5

    .line 2094060
    if-nez v5, :cond_0

    .line 2094061
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2094062
    iget-boolean v5, p0, LX/EDx;->aF:Z

    if-nez v5, :cond_6

    invoke-virtual {p0}, LX/EDx;->af()Z

    move-result v5

    if-nez v5, :cond_6

    if-eqz v4, :cond_6

    if-eqz v0, :cond_6

    if-eqz v3, :cond_0

    if-eqz p2, :cond_0

    :cond_6
    move v2, v1

    .line 2094063
    goto :goto_3
.end method

.method public static e(LX/EDx;ZZ)V
    .locals 2

    .prologue
    .line 2094064
    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v0

    iput-wide v0, p0, LX/EDx;->aM:J

    .line 2094065
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    invoke-virtual {v0}, LX/ECO;->d()V

    .line 2094066
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    invoke-virtual {v0}, LX/ECO;->f()V

    .line 2094067
    invoke-static {p0}, LX/EDx;->cf(LX/EDx;)V

    .line 2094068
    invoke-direct {p0}, LX/EDx;->ck()V

    .line 2094069
    invoke-static {p0, p1, p2}, LX/EDx;->c(LX/EDx;ZZ)V

    .line 2094070
    invoke-static {p0}, LX/EDx;->cH(LX/EDx;)V

    .line 2094071
    return-void
.end method

.method public static f(LX/EDx;I)V
    .locals 10

    .prologue
    .line 2094072
    iget-object v0, p0, LX/EDx;->C:LX/0aU;

    const-string v1, "WEBRTC_REMINDER_NOTIFICATION_ACTION"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2094073
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2094074
    const-string v0, "peer_id"

    iget-wide v2, p0, LX/EDx;->ak:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2094075
    const-string v0, "contact_name"

    iget-object v2, p0, LX/EDx;->bo:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2094076
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    sget v2, LX/EDx;->bj:I

    add-int/lit8 v3, v2, 0x1

    sput v3, LX/EDx;->bj:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v2, v1, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2094077
    iget-object v1, p0, LX/EDx;->H:LX/12x;

    const/4 v2, 0x0

    iget-object v3, p0, LX/EDx;->x:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/32 v6, 0xea60

    int-to-long v8, p1

    mul-long/2addr v6, v8

    add-long/2addr v4, v6

    .line 2094078
    invoke-virtual {v1, v2, v4, v5, v0}, LX/12x;->c(IJLandroid/app/PendingIntent;)V

    .line 2094079
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    const-string v1, "call_reminder"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2094080
    iget-object v0, p0, LX/EDx;->bk:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2094081
    iget-object v0, p0, LX/EDx;->S:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080786

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2094082
    :cond_0
    return-void
.end method

.method private static h(LX/EDx;I)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2094083
    iput p1, p0, LX/EDx;->am:I

    .line 2094084
    if-eqz p1, :cond_5

    .line 2094085
    invoke-virtual {p0}, LX/EDx;->af()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->aB:Z

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v0, v3, :cond_4

    :cond_0
    move v0, v2

    .line 2094086
    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/EDx;->aS:Z

    if-nez v0, :cond_1

    .line 2094087
    invoke-direct {p0}, LX/EDx;->bE()V

    .line 2094088
    :cond_1
    iget v0, p0, LX/EDx;->am:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    iget-object v0, p0, LX/EDx;->bC:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget-short v5, LX/3Dx;->ae:S

    invoke-interface {v0, v3, v4, v5, v1}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2094089
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    const-string v3, "cl_clicked"

    invoke-virtual {v0, v3, v1}, LX/2S7;->a(Ljava/lang/String;Z)V

    .line 2094090
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    const-string v3, "cl_enabled"

    invoke-virtual {v0, v3, v1}, LX/2S7;->a(Ljava/lang/String;Z)V

    .line 2094091
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    const-string v3, "cl_disabled"

    invoke-virtual {v0, v3, v1}, LX/2S7;->a(Ljava/lang/String;Z)V

    .line 2094092
    :cond_2
    if-eq p1, v2, :cond_3

    .line 2094093
    iget-boolean v0, p0, LX/EDx;->bQ:Z

    move v0, v0

    .line 2094094
    if-nez v0, :cond_3

    iget-object v0, p0, LX/EDx;->M:LX/0Uh;

    const/16 v2, 0x5e9

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2094095
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    .line 2094096
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v2}, Lcom/facebook/webrtc/WebrtcEngine;->isEndToEndEncrypted()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 2094097
    if-eqz v0, :cond_3

    .line 2094098
    iget-boolean v0, p0, LX/EDx;->bc:Z

    if-nez v0, :cond_3

    .line 2094099
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EDx;->bc:Z

    .line 2094100
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080814

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2094101
    iget-object v2, p0, LX/EDx;->S:LX/0kL;

    new-instance v3, LX/27k;

    invoke-direct {v3, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v3}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2094102
    :cond_3
    :goto_2
    invoke-static {p0, v1}, LX/EDx;->x(LX/EDx;Z)V

    .line 2094103
    return-void

    :cond_4
    move v0, v1

    .line 2094104
    goto :goto_0

    .line 2094105
    :cond_5
    iput-boolean v1, p0, LX/EDx;->bc:Z

    .line 2094106
    invoke-static {p0}, LX/EDx;->bF(LX/EDx;)V

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static p(LX/EDx;Z)Z
    .locals 2

    .prologue
    .line 2094045
    iget-object v1, p0, LX/EDx;->ct:LX/1Ml;

    if-nez p1, :cond_0

    sget-object v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->t:[Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->u:[Ljava/lang/String;

    goto :goto_0
.end method

.method private static q(LX/EDx;Z)V
    .locals 2

    .prologue
    .line 2094107
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2094108
    iget-object v1, p0, LX/EDx;->T:LX/2Tm;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, LX/2Tm;->b(Z)V

    .line 2094109
    :cond_0
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    .line 2094110
    iput-boolean p1, v0, LX/ECO;->k:Z

    .line 2094111
    return-void

    .line 2094112
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2094042
    invoke-static {p0, v1}, LX/EDx;->q(LX/EDx;Z)V

    .line 2094043
    const/4 v0, 0x0

    invoke-static {p0, v1, v0, p1}, LX/EDx;->a(LX/EDx;ZZZ)V

    .line 2094044
    return-void
.end method

.method public static u(LX/EDx;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2094025
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2094026
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/EDx;->by:Z

    .line 2094027
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, LX/EDx;->bu:Z

    if-eqz v1, :cond_1

    .line 2094028
    iput-boolean v0, p0, LX/EDx;->bu:Z

    .line 2094029
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2094030
    iget-object v1, p0, LX/EDx;->J:LX/ECO;

    sget-object v2, LX/ECN;->VIDEO_ON:LX/ECN;

    invoke-virtual {v1, v2}, LX/ECO;->a(LX/ECN;)V

    .line 2094031
    :cond_2
    iput-boolean p1, p0, LX/EDx;->bs:Z

    .line 2094032
    iget-boolean v1, p0, LX/EDx;->bQ:Z

    if-nez v1, :cond_3

    iget-object v1, p0, LX/EDx;->bC:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-short v3, LX/3Dx;->e:S

    invoke-interface {v1, v2, v3, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2094033
    invoke-static {p0, v0}, LX/EDx;->z(LX/EDx;Z)V

    .line 2094034
    :cond_3
    iget-object v1, p0, LX/EDx;->bC:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/3Dx;->f:S

    invoke-interface {v1, v2, v3, v4, v0}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2094035
    if-eqz p1, :cond_5

    .line 2094036
    invoke-direct {p0}, LX/EDx;->cd()V

    .line 2094037
    :cond_4
    :goto_0
    iget-object v1, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_6

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2094038
    invoke-virtual {v0, p1}, LX/EC0;->a(Z)V

    .line 2094039
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2094040
    :cond_5
    invoke-static {p0}, LX/EDx;->ce(LX/EDx;)V

    goto :goto_0

    .line 2094041
    :cond_6
    return-void
.end method

.method public static v(LX/EDx;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2094008
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2094009
    iget-object v2, p0, LX/EDx;->bT:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v2, :cond_0

    .line 2094010
    iget-object v2, p0, LX/EDx;->bT:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v2, v0}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2094011
    const/4 v2, 0x0

    iput-object v2, p0, LX/EDx;->bT:Ljava/util/concurrent/ScheduledFuture;

    .line 2094012
    :cond_0
    if-eqz p1, :cond_1

    .line 2094013
    iput-boolean v0, p0, LX/EDx;->bz:Z

    .line 2094014
    :cond_1
    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p1, :cond_3

    :cond_2
    :goto_0
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->bv:LX/03R;

    .line 2094015
    iput-boolean v1, p0, LX/EDx;->bt:Z

    .line 2094016
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_4

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2094017
    invoke-virtual {v0, p1}, LX/EC0;->b(Z)V

    .line 2094018
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 2094019
    goto :goto_0

    .line 2094020
    :cond_4
    if-eqz p1, :cond_5

    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2094021
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    .line 2094022
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2094023
    iget-object v1, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v1}, Lcom/facebook/webrtc/WebrtcEngine;->sendEscalationSuccess()V

    .line 2094024
    :cond_5
    return-void
.end method

.method public static x(LX/EDx;Z)V
    .locals 4

    .prologue
    .line 2093999
    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v0

    .line 2094000
    iget-boolean v1, p0, LX/EDx;->bQ:Z

    move v1, v1

    .line 2094001
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2094002
    const-string v3, "com.facebook.rtc.fbwebrtc.CALL_STATUS_AND_DURATION_UPDATE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2094003
    const-string v3, "CALL_STATUS_IS_INSTANT"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2094004
    const-string v0, "CALL_STATUS_EXTRA_IS_PSTN_UPSELL"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2094005
    const-string v0, "CALL_STATUS_IS_TIME_UPDATE_ONLY"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2094006
    iget-object v0, p0, LX/EDx;->B:LX/0Xl;

    invoke-interface {v0, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2094007
    return-void
.end method

.method public static z(LX/EDx;Z)V
    .locals 3

    .prologue
    .line 2093992
    const-string v0, "com.facebook.rtc.fbwebrtc.intent.action.SHOW_UI"

    invoke-direct {p0, v0}, LX/EDx;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2093993
    const-string v1, "AUTO_ACCEPT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2093994
    iget-object v1, p0, LX/EDx;->j:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2093995
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    .line 2093997
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->an:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()Z
    .locals 2

    .prologue
    .line 2093996
    iget-object v0, p0, LX/EDx;->bi:LX/EDq;

    sget-object v1, LX/EDq;->BLUETOOTH:LX/EDq;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final D()Z
    .locals 2

    .prologue
    .line 2093890
    iget-object v0, p0, LX/EDx;->bi:LX/EDq;

    sget-object v1, LX/EDq;->SPEAKERPHONE:LX/EDq;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final G()Z
    .locals 1

    .prologue
    .line 2093651
    iget-boolean v0, p0, LX/EDx;->ao:Z

    move v0, v0

    .line 2093652
    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->ap:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final I()Z
    .locals 2

    .prologue
    .line 2093653
    iget-boolean v0, p0, LX/EDx;->ao:Z

    move v0, v0

    .line 2093654
    if-eqz v0, :cond_0

    iget v0, p0, LX/EDx;->ay:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/EDx;->az:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/EDx;->ay:I

    iget v1, p0, LX/EDx;->az:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final J()V
    .locals 2

    .prologue
    .line 2093655
    invoke-virtual {p0}, LX/EDx;->I()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2093656
    :cond_0
    :goto_0
    return-void

    .line 2093657
    :cond_1
    iget-boolean v0, p0, LX/EDx;->aA:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/EDx;->aA:Z

    .line 2093658
    invoke-virtual {p0}, LX/EDx;->aP()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/EDx;->bt:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, LX/EDx;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093659
    :cond_2
    invoke-virtual {p0}, LX/EDx;->X()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2093660
    invoke-virtual {p0}, LX/EDx;->V()LX/EHZ;

    move-result-object v0

    .line 2093661
    iget-object v1, v0, LX/EHZ;->g:LX/6JF;

    sget-object p0, LX/6JF;->FRONT:LX/6JF;

    if-ne v1, p0, :cond_5

    sget-object v1, LX/6JF;->BACK:LX/6JF;

    :goto_2
    iput-object v1, v0, LX/EHZ;->g:LX/6JF;

    .line 2093662
    invoke-static {v0}, LX/EHZ;->p(LX/EHZ;)V

    .line 2093663
    goto :goto_0

    .line 2093664
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2093665
    :cond_4
    sget-object v0, LX/EDv;->STARTED:LX/EDv;

    invoke-virtual {p0, v0}, LX/EDx;->a(LX/EDv;)V

    goto :goto_0

    .line 2093666
    :cond_5
    sget-object v1, LX/6JF;->FRONT:LX/6JF;

    goto :goto_2
.end method

.method public final K()Z
    .locals 2

    .prologue
    .line 2093667
    iget-boolean v0, p0, LX/EDx;->ao:Z

    move v0, v0

    .line 2093668
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EDx;->ar:LX/EDv;

    sget-object v1, LX/EDv;->STARTED:LX/EDv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final M()Z
    .locals 2

    .prologue
    .line 2093669
    iget-object v0, p0, LX/EDx;->ar:LX/EDv;

    sget-object v1, LX/EDv;->PAUSED:LX/EDv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final N()Z
    .locals 1

    .prologue
    .line 2093670
    invoke-virtual {p0}, LX/EDx;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->as:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final OnUserStateUpdate(Lcom/facebook/webrtc/ConferenceCall;[Ljava/lang/String;[I)V
    .locals 3

    .prologue
    .line 2093671
    array-length v0, p2

    array-length v1, p3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2093672
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$50;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$50;-><init>(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;[Ljava/lang/String;[I)V

    const v2, -0x3ec704f1

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2093673
    return-void

    .line 2093674
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final U()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2093675
    invoke-direct {p0}, LX/EDx;->bV()I

    move-result v0

    invoke-static {v0}, Lorg/webrtc/videoengine/VideoCaptureDeviceInfoAndroid;->b(I)Ljava/lang/String;

    move-result-object v0

    .line 2093676
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final V()LX/EHZ;
    .locals 5

    .prologue
    .line 2093677
    iget-object v0, p0, LX/EDx;->ci:LX/EHZ;

    if-nez v0, :cond_0

    .line 2093678
    new-instance v0, LX/EHZ;

    iget-object v1, p0, LX/EDx;->h:Landroid/content/Context;

    iget-object v2, p0, LX/EDx;->l:LX/2S7;

    iget-object v3, p0, LX/EDx;->cp:LX/EDw;

    iget v3, v3, LX/EDw;->a:I

    iget-object v4, p0, LX/EDx;->cp:LX/EDw;

    iget v4, v4, LX/EDw;->b:I

    invoke-direct {v0, v1, v2, v3, v4}, LX/EHZ;-><init>(Landroid/content/Context;LX/2S7;II)V

    iput-object v0, p0, LX/EDx;->ci:LX/EHZ;

    .line 2093679
    iget-object v0, p0, LX/EDx;->ci:LX/EHZ;

    .line 2093680
    iput-object p0, v0, LX/EHZ;->q:LX/EDx;

    .line 2093681
    iget-object v0, p0, LX/EDx;->bC:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget v3, LX/3Dx;->et:I

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    .line 2093682
    iget-object v1, p0, LX/EDx;->ci:LX/EHZ;

    .line 2093683
    iput v0, v1, LX/EHZ;->L:I

    .line 2093684
    :cond_0
    iget-object v0, p0, LX/EDx;->ci:LX/EHZ;

    return-object v0
.end method

.method public final W()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2093685
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EDx;->bC:LX/0ad;

    sget-short v2, LX/3Dx;->aM:S

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2093686
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/EDx;->bC:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-short v3, LX/3Dx;->en:S

    invoke-interface {v1, v2, v3, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public final X()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2093687
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/EDx;->bC:LX/0ad;

    sget-short v3, LX/3Dx;->aM:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2093688
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, LX/EDx;->bC:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-short v4, LX/3Dx;->eo:S

    invoke-interface {v2, v3, v4, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, LX/EDx;->W()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final Y()Z
    .locals 1

    .prologue
    .line 2093689
    invoke-virtual {p0}, LX/EDx;->aT()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/EDx;->aZ()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, LX/EDx;->K()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, LX/EDx;->N()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2093690
    :cond_1
    const/4 v0, 0x0

    .line 2093691
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p0}, LX/EDx;->W()Z

    move-result v0

    goto :goto_0
.end method

.method public final Z()Lcom/facebook/webrtc/MediaCaptureSink;
    .locals 1

    .prologue
    .line 2093692
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2093693
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    .line 2093694
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result p0

    if-eqz p0, :cond_1

    iget-boolean p0, v0, LX/2Tm;->y:Z

    if-eqz p0, :cond_1

    .line 2093695
    iget-object p0, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {p0}, Lcom/facebook/webrtc/WebrtcEngine;->getMediaCaptureSink()Lcom/facebook/webrtc/MediaCaptureSink;

    move-result-object p0

    .line 2093696
    :goto_0
    move-object v0, p0

    .line 2093697
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final a(LX/EG5;)LX/EFy;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2093698
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    if-nez v0, :cond_0

    .line 2093699
    const/4 v0, 0x0

    .line 2093700
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    .line 2093701
    new-instance p0, LX/EFy;

    invoke-direct {p0, v0, p1}, LX/EFy;-><init>(LX/EFw;LX/EG5;)V

    .line 2093702
    move-object v0, p0

    .line 2093703
    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2093647
    invoke-virtual {p0}, LX/EDx;->aX()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EDx;->bq:LX/EGe;

    if-eqz v0, :cond_0

    .line 2093648
    iget-object v0, p0, LX/EDx;->bq:LX/EGe;

    .line 2093649
    iget-object v1, v0, LX/EGe;->A:LX/EFt;

    new-instance p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$1;

    invoke-direct {p0, v0}, Lcom/facebook/rtc/services/BackgroundVideoCallService$1;-><init>(LX/EGe;)V

    invoke-virtual {v1, p0}, LX/EFt;->a(Ljava/lang/Runnable;)V

    .line 2093650
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 2093708
    iput p1, p0, LX/EDx;->at:I

    .line 2093709
    iput p2, p0, LX/EDx;->au:I

    .line 2093710
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2093711
    invoke-virtual {v0, p1, p2}, LX/EC0;->a(II)V

    .line 2093712
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2093713
    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2093714
    iget-boolean v0, p0, LX/EDx;->bp:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v0}, LX/EGe;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2093715
    :cond_0
    :goto_0
    return-void

    .line 2093716
    :cond_1
    iget-object v0, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_2

    .line 2093717
    iget-object v0, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2093718
    const/4 v0, 0x0

    iput-object v0, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    .line 2093719
    :cond_2
    iget-object v0, p0, LX/EDx;->v:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$26;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$26;-><init>(LX/EDx;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method public final a(LX/7TQ;)V
    .locals 5

    .prologue
    .line 2093720
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2093721
    sget-object v0, LX/7TQ;->CallEndIgnoreCall:LX/7TQ;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aZ()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093722
    iget-object v0, p0, LX/EDx;->cy:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79G;

    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/79G;->a(ZZLjava/lang/String;)V

    .line 2093723
    :cond_0
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    const/4 v1, 0x1

    .line 2093724
    sget-object v2, LX/EDn;->b:[I

    invoke-virtual {p1}, LX/7TQ;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    .line 2093725
    :goto_0
    :sswitch_0
    move v1, v1

    .line 2093726
    iget-boolean v2, v0, LX/EFw;->d:Z

    if-eqz v2, :cond_4

    .line 2093727
    :cond_1
    :goto_1
    return-void

    .line 2093728
    :cond_2
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_1

    .line 2093729
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    iget-wide v2, p0, LX/EDx;->ac:J

    .line 2093730
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2093731
    iget-object v1, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {p1}, LX/7TQ;->ordinal()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/webrtc/WebrtcEngine;->endCall(JI)V

    .line 2093732
    :cond_3
    goto :goto_1

    .line 2093733
    :sswitch_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2093734
    :sswitch_2
    const/4 v1, 0x2

    goto :goto_0

    .line 2093735
    :sswitch_3
    const/4 v1, 0x3

    goto :goto_0

    .line 2093736
    :cond_4
    iget-object v2, v0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v2, v1}, Lcom/facebook/webrtc/ConferenceCall;->leave(I)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0xd -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(LX/EC0;)V
    .locals 3

    .prologue
    .line 2093737
    iget-object v0, p0, LX/EDx;->z:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2093738
    iget-object v1, p0, LX/EDx;->O:Ljava/util/List;

    monitor-enter v1

    .line 2093739
    :try_start_0
    iget-object v0, p0, LX/EDx;->O:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2093740
    iget-object v0, p0, LX/EDx;->O:Ljava/util/List;

    new-instance v2, LX/EDb;

    invoke-direct {v2, p0}, LX/EDb;-><init>(LX/EDx;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2093741
    iget-object v0, p0, LX/EDx;->O:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->P:LX/0Px;

    .line 2093742
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/EC2;)V
    .locals 1

    .prologue
    .line 2093743
    if-eqz p1, :cond_0

    .line 2093744
    iget-object v0, p0, LX/EDx;->cu:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2093745
    :cond_0
    return-void
.end method

.method public final a(LX/EDq;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2093746
    iget-boolean v0, p0, LX/EDx;->bQ:Z

    move v0, v0

    .line 2093747
    if-nez v0, :cond_0

    .line 2093748
    const/4 v0, 0x3

    invoke-direct {p0, v0}, LX/EDx;->c(I)V

    .line 2093749
    :cond_0
    sget-object v0, LX/EDn;->a:[I

    invoke-virtual {p1}, LX/EDq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2093750
    :cond_1
    :goto_0
    invoke-static {p0}, LX/EDx;->bS(LX/EDx;)V

    .line 2093751
    :cond_2
    return-void

    .line 2093752
    :pswitch_0
    iget-object v0, p0, LX/EDx;->m:LX/3GP;

    invoke-virtual {v0}, LX/3GP;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EDx;->m:LX/3GP;

    .line 2093753
    iget-boolean v1, v0, LX/3GP;->h:Z

    move v0, v1

    .line 2093754
    if-nez v0, :cond_1

    .line 2093755
    invoke-static {p0}, LX/EDx;->bC(LX/EDx;)V

    goto :goto_0

    .line 2093756
    :pswitch_1
    iget-boolean v0, p0, LX/EDx;->be:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, LX/EDx;->br:Z

    if-nez v0, :cond_2

    .line 2093757
    :cond_3
    invoke-direct {p0}, LX/EDx;->bU()V

    .line 2093758
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 2093759
    iput-boolean v2, p0, LX/EDx;->bf:Z

    goto :goto_0

    .line 2093760
    :pswitch_2
    invoke-direct {p0}, LX/EDx;->bU()V

    .line 2093761
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 2093762
    iput-boolean v3, p0, LX/EDx;->bf:Z

    goto :goto_0

    .line 2093763
    :pswitch_3
    invoke-direct {p0}, LX/EDx;->bU()V

    .line 2093764
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 2093765
    iput-boolean v2, p0, LX/EDx;->bf:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/EDv;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2093766
    iget-object v0, p0, LX/EDx;->ar:LX/EDv;

    if-ne v0, p1, :cond_1

    sget-object v0, LX/EDv;->STARTED:LX/EDv;

    if-eq p1, v0, :cond_1

    .line 2093767
    :cond_0
    :goto_0
    return-void

    .line 2093768
    :cond_1
    sget-object v0, LX/EDv;->STARTED:LX/EDv;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, LX/EDx;->b:LX/3Eb;

    iget-wide v4, p0, LX/EDx;->ac:J

    invoke-virtual {v0, v4, v5}, LX/3Eb;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093769
    :cond_2
    invoke-virtual {p0}, LX/EDx;->X()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2093770
    sget-object v0, LX/EDv;->STARTED:LX/EDv;

    if-ne p1, v0, :cond_4

    .line 2093771
    invoke-virtual {p0}, LX/EDx;->V()LX/EHZ;

    move-result-object v0

    .line 2093772
    iget-object v3, v0, LX/EHZ;->l:LX/6Ia;

    if-eqz v3, :cond_f

    iget-object v3, v0, LX/EHZ;->l:LX/6Ia;

    invoke-interface {v3}, LX/6Ia;->c()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 2093773
    :cond_3
    :goto_1
    iget-object v0, p0, LX/EDx;->M:LX/0Uh;

    const/16 v3, 0x60b

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2093774
    sput-boolean v0, Lorg/webrtc/videoengine/VideoCaptureDeviceInfoAndroid;->f:Z

    .line 2093775
    invoke-direct {p0}, LX/EDx;->bV()I

    move-result v3

    .line 2093776
    iget-object v0, p0, LX/EDx;->ar:LX/EDv;

    sget-object v4, LX/EDv;->STARTED:LX/EDv;

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    sget-object v0, LX/EDv;->STARTED:LX/EDv;

    if-ne p1, v0, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2093777
    sget-object v0, LX/EDv;->STARTED:LX/EDv;

    if-ne p1, v0, :cond_7

    if-gez v3, :cond_7

    .line 2093778
    const-string v0, "WebrtcUiHandler"

    const-string v1, "failed to find capable camera to turn on video"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2093779
    :cond_4
    invoke-virtual {p0}, LX/EDx;->V()LX/EHZ;

    move-result-object v0

    invoke-virtual {v0}, LX/EHZ;->f()V

    goto :goto_1

    :cond_5
    move v0, v2

    .line 2093780
    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    .line 2093781
    :cond_7
    iget-object v0, p0, LX/EDx;->ar:LX/EDv;

    .line 2093782
    iput-object p1, p0, LX/EDx;->ar:LX/EDv;

    .line 2093783
    iget-object v4, p0, LX/EDx;->l:LX/2S7;

    iget-object v5, p0, LX/EDx;->cp:LX/EDw;

    iget v5, v5, LX/EDw;->a:I

    iget-object v6, p0, LX/EDx;->cp:LX/EDw;

    iget v6, v6, LX/EDw;->b:I

    invoke-virtual {v4, v3, v5, v6}, LX/2S7;->a(III)V

    .line 2093784
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2093785
    iget-object v3, p0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {p0}, LX/EDx;->U()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/EFw;->b(Ljava/lang/String;)V

    .line 2093786
    if-eq v0, p1, :cond_8

    .line 2093787
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    sget-object v3, LX/EDv;->STARTED:LX/EDv;

    if-ne p1, v3, :cond_9

    :goto_4
    invoke-virtual {v0, v1}, LX/EFw;->b(Z)V

    .line 2093788
    iget-object v0, p0, LX/EDx;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2093789
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2093790
    invoke-virtual {p0, v0}, LX/EDx;->b(Ljava/lang/String;)LX/EGE;

    move-result-object v0

    .line 2093791
    if-eqz v0, :cond_8

    .line 2093792
    invoke-virtual {p0}, LX/EDx;->K()Z

    move-result v1

    iput-boolean v1, v0, LX/EGE;->i:Z

    .line 2093793
    :cond_8
    :goto_5
    invoke-static {p0}, LX/EDx;->cH(LX/EDx;)V

    goto/16 :goto_0

    :cond_9
    move v1, v2

    .line 2093794
    goto :goto_4

    .line 2093795
    :cond_a
    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v3

    if-nez v3, :cond_b

    sget-object v3, LX/EDv;->STARTED:LX/EDv;

    if-ne p1, v3, :cond_8

    .line 2093796
    :cond_b
    iget-object v3, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v3, :cond_8

    .line 2093797
    iget-object v3, p0, LX/EDx;->T:LX/2Tm;

    invoke-virtual {p0}, LX/EDx;->U()Ljava/lang/String;

    move-result-object v4

    .line 2093798
    invoke-virtual {v3}, LX/2Tm;->a()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 2093799
    iget-object v5, v3, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v5, v4}, Lcom/facebook/webrtc/WebrtcEngine;->setCameraId(Ljava/lang/String;)V

    .line 2093800
    :cond_c
    if-eq v0, p1, :cond_8

    .line 2093801
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    sget-object v3, LX/EDv;->STARTED:LX/EDv;

    if-ne p1, v3, :cond_e

    .line 2093802
    :goto_6
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2093803
    iget-object v2, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v2, v1}, Lcom/facebook/webrtc/WebrtcEngine;->setVideoOn(Z)V

    .line 2093804
    :cond_d
    goto :goto_5

    :cond_e
    move v1, v2

    goto :goto_6

    .line 2093805
    :cond_f
    iget-object v3, v0, LX/EHZ;->e:LX/6Jt;

    invoke-virtual {v3}, LX/6Jt;->b()V

    .line 2093806
    iget-object v3, v0, LX/EHZ;->l:LX/6Ia;

    if-nez v3, :cond_10

    .line 2093807
    invoke-static {v0}, LX/EHZ;->n(LX/EHZ;)LX/6Ia;

    move-result-object v3

    iput-object v3, v0, LX/EHZ;->l:LX/6Ia;

    .line 2093808
    :cond_10
    iget-object v3, v0, LX/EHZ;->l:LX/6Ia;

    iget-object v4, v0, LX/EHZ;->h:LX/6Ik;

    invoke-interface {v3, v4}, LX/6Ia;->a(LX/6Ik;)V

    goto/16 :goto_1
.end method

.method public final a(LX/EFy;LX/EGE;ZZ)V
    .locals 6
    .param p1    # LX/EFy;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/EGE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2093809
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/EDx;->ae:Ljava/util/Map;

    if-eqz v1, :cond_0

    if-nez p2, :cond_1

    .line 2093810
    :cond_0
    return-void

    .line 2093811
    :cond_1
    iget-object v1, p0, LX/EDx;->af:LX/EGE;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/EDx;->af:LX/EGE;

    iget-object v1, v1, LX/EGE;->b:Ljava/lang/String;

    iget-object v2, p2, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/EDx;->af:LX/EGE;

    iget-wide v2, v1, LX/EGE;->h:J

    iget-wide v4, p2, LX/EGE;->h:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    iget-boolean v1, p0, LX/EDx;->ag:Z

    if-eq v1, p3, :cond_0

    .line 2093812
    :cond_2
    iput-boolean p3, p0, LX/EDx;->ag:Z

    .line 2093813
    iput-object p2, p0, LX/EDx;->af:LX/EGE;

    .line 2093814
    iget-object v1, p2, LX/EGE;->g:Ljava/lang/String;

    iput-object v1, p0, LX/EDx;->av:Ljava/lang/String;

    .line 2093815
    iget-wide v2, p2, LX/EGE;->h:J

    iput-wide v2, p0, LX/EDx;->aw:J

    .line 2093816
    if-eqz p4, :cond_3

    .line 2093817
    invoke-virtual {p1, v0}, LX/EFy;->a(Z)V

    .line 2093818
    :cond_3
    iget-object v1, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2093819
    invoke-virtual {v0}, LX/EC0;->s()V

    .line 2093820
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(LX/EIp;)V
    .locals 1

    .prologue
    .line 2093821
    sget-object v0, LX/EIp;->STARTED:LX/EIp;

    if-ne p1, v0, :cond_1

    .line 2093822
    const/4 v0, 0x3

    invoke-static {p0, v0}, LX/EDx;->h(LX/EDx;I)V

    .line 2093823
    :cond_0
    :goto_0
    return-void

    .line 2093824
    :cond_1
    sget-object v0, LX/EIp;->FINISHED:LX/EIp;

    if-ne p1, v0, :cond_0

    .line 2093825
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/EDx;->h(LX/EDx;I)V

    .line 2093826
    invoke-static {p0}, LX/EDx;->ci(LX/EDx;)V

    .line 2093827
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    invoke-virtual {v0}, LX/2S7;->g()V

    .line 2093828
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/EDx;->n(Z)V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;JJLjava/lang/String;)V
    .locals 14

    .prologue
    .line 2093829
    iget-object v0, p0, LX/EDx;->bC:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/3Dx;->bY:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    .line 2093830
    invoke-virtual {p0}, LX/EDx;->aH()LX/7TQ;

    move-result-object v1

    sget-object v2, LX/7TQ;->CallEndNoPermission:LX/7TQ;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, LX/EDx;->d(LX/EDx;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2093831
    iget-object v1, p0, LX/EDx;->c:LX/EDC;

    invoke-virtual {p0}, LX/EDx;->z()J

    move-result-wide v2

    invoke-virtual {p0}, LX/EDx;->ag()Z

    move-result v4

    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v5

    iget-wide v6, p0, LX/EDx;->ak:J

    iget-wide v8, p0, LX/EDx;->aI:J

    invoke-static {p0}, LX/EDx;->cw(LX/EDx;)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-virtual {p0}, LX/EDx;->ag()Z

    move-result v12

    invoke-virtual/range {v1 .. v12}, LX/EDC;->a(JZZJJJZ)V

    .line 2093832
    :cond_0
    iget-object v0, p0, LX/EDx;->cw:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2093833
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2093834
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/EDx;->a(Landroid/view/View;Z)V

    .line 2093835
    return-void
.end method

.method public final a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 2093836
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2093837
    iget-wide v0, p0, LX/EDx;->aw:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 2093838
    const/4 v0, 0x0

    .line 2093839
    monitor-enter p0

    .line 2093840
    :try_start_0
    iget-object v1, p0, LX/EDx;->bL:Landroid/view/View;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2093841
    iput-object p1, p0, LX/EDx;->bL:Landroid/view/View;

    .line 2093842
    const/4 v0, 0x1

    .line 2093843
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2093844
    if-nez v0, :cond_1

    if-eqz p2, :cond_2

    .line 2093845
    :cond_1
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2093846
    invoke-static {p0}, LX/EDx;->bt(LX/EDx;)LX/EFy;

    move-result-object v0

    iget-wide v2, p0, LX/EDx;->aw:J

    invoke-virtual {v0, v2, v3, p1}, LX/EFy;->b(JLandroid/view/View;)V

    .line 2093847
    :cond_2
    :goto_0
    return-void

    .line 2093848
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2093849
    :cond_3
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_2

    .line 2093850
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    iget-wide v2, p0, LX/EDx;->aw:J

    invoke-virtual {v0, v2, v3, p1}, LX/2Tm;->a(JLandroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2093851
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EDx;->Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    if-nez v0, :cond_2

    .line 2093852
    :cond_0
    const-string v0, "WebrtcUiHandler"

    const-string v1, "Unable to startOrJoinGroupCall"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093853
    invoke-virtual {p0}, LX/EDx;->x()V

    .line 2093854
    :cond_1
    :goto_0
    return-void

    .line 2093855
    :cond_2
    if-eqz p1, :cond_3

    .line 2093856
    iget-object v0, p0, LX/EDx;->Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-object v0, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->m:LX/0Px;

    iget-object v1, p0, LX/EDx;->Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-object v1, v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->m:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 2093857
    :goto_1
    array-length v1, v0

    if-gtz v1, :cond_5

    .line 2093858
    iget-object v1, p0, LX/EDx;->Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-object v1, v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->o:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2093859
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    iget-object v1, p0, LX/EDx;->Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-object v1, v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->o:Ljava/lang/String;

    .line 2093860
    iget-boolean p1, v0, LX/EFw;->d:Z

    if-eqz p1, :cond_6

    .line 2093861
    :goto_2
    iput-boolean v2, p0, LX/EDx;->bZ:Z

    .line 2093862
    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2093863
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EDx;->aT:Z

    .line 2093864
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    sget-object v1, LX/ECN;->LOBBY_JOIN:LX/ECN;

    invoke-virtual {v0, v1}, LX/ECO;->a(LX/ECN;)V

    goto :goto_0

    .line 2093865
    :cond_3
    iget-object v0, p0, LX/EDx;->Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-object v0, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->n:LX/0Px;

    if-nez v0, :cond_4

    .line 2093866
    new-array v0, v2, [Ljava/lang/String;

    goto :goto_1

    .line 2093867
    :cond_4
    iget-object v0, p0, LX/EDx;->Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-object v0, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->n:LX/0Px;

    iget-object v1, p0, LX/EDx;->Y:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-object v1, v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->n:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_1

    .line 2093868
    :cond_5
    iget-object v1, p0, LX/EDx;->ad:LX/EFw;

    .line 2093869
    iget-boolean v3, v1, LX/EFw;->d:Z

    if-eqz v3, :cond_8

    .line 2093870
    :goto_3
    goto :goto_2

    .line 2093871
    :cond_6
    iget-object p1, v0, LX/EFw;->c:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/EDx;

    invoke-virtual {p1}, LX/EDx;->j()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 2093872
    iget-object p1, v0, LX/EFw;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/79G;

    invoke-virtual {p1}, LX/79G;->c()V

    .line 2093873
    :cond_7
    iget-object p1, v0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {p1, v1}, Lcom/facebook/webrtc/ConferenceCall;->join(Ljava/lang/String;)V

    goto :goto_2

    .line 2093874
    :cond_8
    iget-object v3, v1, LX/EFw;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->j()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2093875
    iget-object v3, v1, LX/EFw;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/79G;

    .line 2093876
    const-string v4, "RTC_ENGINE_START"

    const/4 p1, 0x0

    invoke-static {v3, v4, p1}, LX/79G;->a(LX/79G;Ljava/lang/String;LX/1rQ;)V

    .line 2093877
    :cond_9
    if-eqz v0, :cond_a

    array-length v3, v0

    if-lez v3, :cond_a

    const/4 v3, 0x1

    :goto_4
    iput-boolean v3, v1, LX/EFw;->f:Z

    .line 2093878
    iget-object v3, v1, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v3, v0}, Lcom/facebook/webrtc/ConferenceCall;->call([Ljava/lang/String;)V

    goto :goto_3

    .line 2093879
    :cond_a
    const/4 v3, 0x0

    goto :goto_4
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 2093268
    if-eqz p1, :cond_2

    .line 2093269
    sget-object v0, LX/EDu;->REMOTE_AUDIO_ONLY:LX/EDu;

    iput-object v0, p0, LX/EDx;->bR:LX/EDu;

    .line 2093270
    :goto_0
    iget-object v0, p0, LX/EDx;->bq:LX/EGe;

    if-eqz v0, :cond_1

    .line 2093271
    if-eqz p2, :cond_1

    .line 2093272
    const/4 v0, 0x0

    .line 2093273
    iget-object p1, p0, LX/EDx;->T:LX/2Tm;

    if-eqz p1, :cond_0

    .line 2093274
    iget-object p1, p0, LX/EDx;->T:LX/2Tm;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, LX/2Tm;->b(Z)V

    .line 2093275
    :cond_0
    invoke-virtual {p0, v0}, LX/EDx;->g(Z)V

    .line 2093276
    invoke-virtual {p0}, LX/EDx;->aY()V

    .line 2093277
    invoke-virtual {p0}, LX/EDx;->bd()V

    .line 2093278
    :cond_1
    return-void

    .line 2093279
    :cond_2
    sget-object v0, LX/EDu;->RECIPROCATED:LX/EDu;

    iput-object v0, p0, LX/EDx;->bR:LX/EDu;

    goto :goto_0
.end method

.method public final a([B)V
    .locals 1

    .prologue
    .line 2092987
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2092988
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    .line 2092989
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2092990
    iget-object p0, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {p0, p1}, Lcom/facebook/webrtc/WebrtcEngine;->sendGameCommand([B)V

    .line 2092991
    :cond_0
    return-void
.end method

.method public final aA()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2092166
    invoke-virtual {p0}, LX/EDx;->aB()Ljava/lang/String;

    move-result-object v0

    .line 2092167
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2092168
    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    const v1, 0x7f0807f4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2092169
    :cond_0
    :goto_0
    return-object v0

    .line 2092170
    :cond_1
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    const v1, 0x7f0807f1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aB()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2092198
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EDx;->al:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v0, :cond_1

    .line 2092199
    :cond_0
    const/4 v0, 0x0

    .line 2092200
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/EDx;->R:LX/EG4;

    invoke-interface {v0}, LX/EG4;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aC()Ljava/lang/String;
    .locals 13

    .prologue
    const/16 v12, 0xa0

    const-wide/16 v10, 0x3c

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2092201
    invoke-static {p0}, LX/EDx;->cw(LX/EDx;)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 2092202
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 2092203
    :cond_0
    const-string v0, ""

    .line 2092204
    :goto_0
    return-object v0

    .line 2092205
    :cond_1
    const-wide/16 v6, 0xe10

    div-long v6, v4, v6

    long-to-int v6, v6

    .line 2092206
    div-long v8, v4, v10

    rem-long/2addr v8, v10

    long-to-int v7, v8

    .line 2092207
    rem-long/2addr v4, v10

    long-to-int v4, v4

    .line 2092208
    if-lez v6, :cond_4

    move v3, v1

    .line 2092209
    :goto_1
    if-lez v7, :cond_5

    move v0, v1

    .line 2092210
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2092211
    if-eqz v3, :cond_2

    .line 2092212
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f0033

    new-array v8, v1, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-virtual {v0, v3, v6, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2092213
    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    .line 2092214
    :cond_2
    if-eqz v0, :cond_3

    .line 2092215
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f0034

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v2

    invoke-virtual {v0, v3, v7, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2092216
    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2092217
    :cond_3
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f0035

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-virtual {v0, v3, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2092218
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v3, v2

    .line 2092219
    goto :goto_1

    :cond_5
    move v0, v2

    .line 2092220
    goto :goto_2
.end method

.method public final aD()Z
    .locals 2

    .prologue
    .line 2092221
    iget v0, p0, LX/EDx;->aO:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/EDx;->aO:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aG()Z
    .locals 1

    .prologue
    .line 2092222
    iget-object v0, p0, LX/EDx;->cv:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    invoke-virtual {v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->shouldEnableAutomatedTestSupport()Z

    move-result v0

    return v0
.end method

.method public final aH()LX/7TQ;
    .locals 1

    .prologue
    .line 2092223
    iget-object v0, p0, LX/EDx;->bl:LX/7TQ;

    return-object v0
.end method

.method public final aI()Z
    .locals 2

    .prologue
    .line 2092224
    iget-object v0, p0, LX/EDx;->m:LX/3GP;

    invoke-virtual {v0}, LX/3GP;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aJ()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2092225
    iget-object v1, p0, LX/EDx;->bv:LX/03R;

    if-eqz v1, :cond_0

    .line 2092226
    iget-object v1, p0, LX/EDx;->bv:LX/03R;

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    .line 2092227
    :cond_0
    return v0
.end method

.method public final aM()Z
    .locals 1

    .prologue
    .line 2092228
    iget-boolean v0, p0, LX/EDx;->bu:Z

    move v0, v0

    .line 2092229
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aP()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/EDx;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aP()Z
    .locals 1

    .prologue
    .line 2092230
    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aR()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aQ()Z
    .locals 2

    .prologue
    .line 2092231
    invoke-virtual {p0}, LX/EDx;->aS()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->aB:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->aF:Z

    if-nez v0, :cond_1

    .line 2092232
    :cond_0
    iget v0, p0, LX/EDx;->am:I

    move v0, v0

    .line 2092233
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aR()Z
    .locals 1

    .prologue
    .line 2092234
    invoke-static {p0}, LX/EDx;->cz(LX/EDx;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aQ()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aS()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2092235
    iget v1, p0, LX/EDx;->am:I

    move v1, v1

    .line 2092236
    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aT()Z
    .locals 1

    .prologue
    .line 2092237
    iget v0, p0, LX/EDx;->am:I

    move v0, v0

    .line 2092238
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aU()J
    .locals 2

    .prologue
    .line 2092239
    iget-object v0, p0, LX/EDx;->y:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    return-wide v0
.end method

.method public final aV()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2092171
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EDx;->bC:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/3Dx;->aL:S

    invoke-interface {v1, v2, v3, v4, v0}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2092172
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2092173
    iget-object v2, p0, LX/EDx;->bY:LX/03R;

    invoke-virtual {v2}, LX/03R;->isSet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2092174
    iget-object v1, p0, LX/EDx;->bY:LX/03R;

    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v0

    .line 2092175
    :goto_1
    move v0, v0

    .line 2092176
    goto :goto_0

    .line 2092177
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_3

    move v0, v1

    .line 2092178
    :cond_2
    :goto_2
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v2

    iput-object v2, p0, LX/EDx;->bY:LX/03R;

    .line 2092179
    if-eqz v0, :cond_8

    iget-boolean v2, p0, LX/EDx;->bK:Z

    if-eqz v2, :cond_8

    .line 2092180
    iget-object v1, p0, LX/EDx;->cr:Lcom/facebook/presence/ThreadPresenceManager;

    sget-object v2, LX/2gE;->THREAD_PRESENCE_CAPABILITY_INSTANT:LX/2gE;

    invoke-virtual {v2}, LX/2gE;->getValue()I

    move-result v2

    .line 2092181
    iput v2, v1, Lcom/facebook/presence/ThreadPresenceManager;->r:I

    .line 2092182
    goto :goto_1

    .line 2092183
    :cond_3
    iget-object v2, p0, LX/EDx;->E:LX/00H;

    .line 2092184
    iget-object v3, v2, LX/00H;->j:LX/01T;

    move-object v2, v3

    .line 2092185
    sget-object v3, LX/01T;->MESSENGER:LX/01T;

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2092186
    goto :goto_2

    .line 2092187
    :cond_4
    iget-boolean v2, p0, LX/EDx;->bM:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, LX/EDx;->bK:Z

    if-nez v2, :cond_5

    move v0, v1

    .line 2092188
    goto :goto_2

    .line 2092189
    :cond_5
    iget-object v2, p0, LX/EDx;->ct:LX/1Ml;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "android.permission.RECORD_AUDIO"

    aput-object v4, v3, v1

    const-string v4, "android.permission.CAMERA"

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2092190
    goto :goto_2

    .line 2092191
    :cond_6
    iget-object v2, p0, LX/EDx;->ct:LX/1Ml;

    invoke-virtual {v2}, LX/1Ml;->a()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, LX/EDx;->bC:LX/0ad;

    sget-short v3, LX/3Dx;->ed:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 2092192
    goto :goto_2

    .line 2092193
    :cond_7
    iget-object v2, p0, LX/EDx;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dd;->d:LX/0Tn;

    invoke-interface {v2, v3, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/EDx;->bC:LX/0ad;

    sget-short v3, LX/3Dx;->ed:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 2092194
    goto :goto_2

    .line 2092195
    :cond_8
    iget-object v2, p0, LX/EDx;->cr:Lcom/facebook/presence/ThreadPresenceManager;

    .line 2092196
    iput v1, v2, Lcom/facebook/presence/ThreadPresenceManager;->r:I

    .line 2092197
    goto :goto_1
.end method

.method public final aW()V
    .locals 3

    .prologue
    .line 2092243
    const/4 v0, 0x0

    .line 2092244
    invoke-virtual {p0}, LX/EDx;->aT()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2092245
    :goto_0
    return-void

    .line 2092246
    :cond_0
    iget-object v1, p0, LX/EDx;->bq:LX/EGe;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/EDx;->bq:LX/EGe;

    .line 2092247
    iget-boolean v2, v1, LX/EGe;->P:Z

    move v1, v2

    .line 2092248
    if-eqz v1, :cond_2

    .line 2092249
    if-eqz v0, :cond_1

    .line 2092250
    iget-object v1, p0, LX/EDx;->bq:LX/EGe;

    .line 2092251
    const-string v2, "AUTO_ACCEPT"

    invoke-static {v1, v2}, LX/EGe;->b$redex0(LX/EGe;Ljava/lang/String;)V

    .line 2092252
    goto :goto_0

    .line 2092253
    :cond_1
    iget-object v1, p0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v1}, LX/EGe;->z()V

    goto :goto_0

    .line 2092254
    :cond_2
    invoke-static {p0, v0}, LX/EDx;->z(LX/EDx;Z)V

    goto :goto_0
.end method

.method public final aX()Z
    .locals 1

    .prologue
    .line 2092255
    iget-boolean v0, p0, LX/EDx;->aX:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aV()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aY()V
    .locals 1

    .prologue
    .line 2092256
    invoke-virtual {p0}, LX/EDx;->D()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->C()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2092257
    iget-boolean v0, p0, LX/EDx;->be:Z

    move v0, v0

    .line 2092258
    if-nez v0, :cond_0

    .line 2092259
    sget-object v0, LX/EDq;->SPEAKERPHONE:LX/EDq;

    invoke-virtual {p0, v0}, LX/EDx;->a(LX/EDq;)V

    .line 2092260
    :cond_0
    const/4 v0, 0x1

    .line 2092261
    iput-boolean v0, p0, LX/EDx;->bd:Z

    .line 2092262
    return-void
.end method

.method public final aZ()Z
    .locals 2

    .prologue
    .line 2092263
    iget v0, p0, LX/EDx;->am:I

    move v0, v0

    .line 2092264
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aa()Z
    .locals 1

    .prologue
    .line 2092265
    iget v0, p0, LX/EDx;->am:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->ab:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ac()Ljava/lang/String;
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v12, 0x0

    const-wide/16 v10, 0xe10

    const-wide/16 v8, 0x3c

    .line 2092266
    invoke-direct {p0}, LX/EDx;->bZ()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 2092267
    cmp-long v2, v0, v10

    if-gez v2, :cond_0

    .line 2092268
    iget-object v2, p0, LX/EDx;->h:Landroid/content/Context;

    const v3, 0x7f080743

    new-array v4, v14, [Ljava/lang/Object;

    div-long v6, v0, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v12

    rem-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v13

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2092269
    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, LX/EDx;->h:Landroid/content/Context;

    const v3, 0x7f080744

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    div-long v6, v0, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v12

    rem-long v6, v0, v10

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v13

    rem-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v14

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ad()Z
    .locals 1

    .prologue
    .line 2092270
    iget-boolean v0, p0, LX/EDx;->aB:Z

    return v0
.end method

.method public final addRemoteVideoTrack(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 2092271
    iget-object v6, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$31;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$31;-><init>(LX/EDx;Ljava/lang/String;Ljava/lang/String;J)V

    const v1, 0x43ec3371

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092272
    return-void
.end method

.method public final ae()Z
    .locals 2

    .prologue
    .line 2092273
    iget v0, p0, LX/EDx;->am:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/EDx;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final af()Z
    .locals 2

    .prologue
    .line 2092274
    iget v0, p0, LX/EDx;->am:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ag()Z
    .locals 4

    .prologue
    .line 2092275
    iget-wide v0, p0, LX/EDx;->aM:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ah()V
    .locals 5

    .prologue
    .line 2092276
    invoke-virtual {p0}, LX/EDx;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2092277
    :goto_0
    return-void

    .line 2092278
    :cond_0
    monitor-enter p0

    .line 2092279
    :try_start_0
    invoke-virtual {p0}, LX/EDx;->aa()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2092280
    monitor-exit p0

    goto :goto_0

    .line 2092281
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2092282
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/EDx;->q:LX/2XQ;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2XQ;->a(Z)V

    .line 2092283
    iget-object v0, p0, LX/EDx;->p:LX/ECx;

    invoke-virtual {v0}, LX/ECx;->e()V

    .line 2092284
    iget-object v0, p0, LX/EDx;->p:LX/ECx;

    invoke-virtual {v0}, LX/ECx;->c()V

    .line 2092285
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EDx;->ab:Z

    .line 2092286
    iget-object v0, p0, LX/EDx;->v:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$19;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$19;-><init>(LX/EDx;)V

    const-wide/16 v2, 0xfa0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2092287
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final ai()V
    .locals 1

    .prologue
    .line 2092288
    iget-object v0, p0, LX/EDx;->p:LX/ECx;

    invoke-virtual {v0}, LX/ECx;->a()V

    .line 2092289
    return-void
.end method

.method public final ak()Z
    .locals 1

    .prologue
    .line 2092240
    iget-object v0, p0, LX/EDx;->p:LX/ECx;

    .line 2092241
    iget-object p0, v0, LX/ECx;->b:Landroid/os/PowerManager;

    invoke-virtual {p0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result p0

    move v0, p0

    .line 2092242
    return v0
.end method

.method public final al()V
    .locals 1

    .prologue
    .line 2092015
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    invoke-virtual {v0}, LX/ECO;->f()V

    .line 2092016
    invoke-static {p0}, LX/EDx;->ce(LX/EDx;)V

    .line 2092017
    return-void
.end method

.method public final ao()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2092053
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    if-eqz v0, :cond_0

    .line 2092054
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v0}, LX/EFw;->d()Ljava/lang/String;

    move-result-object v0

    .line 2092055
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ap()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2092043
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    if-nez v0, :cond_0

    .line 2092044
    const/4 v0, 0x0

    .line 2092045
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v0}, LX/EFw;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 2092046
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2092047
    :cond_1
    :goto_1
    move-object v0, v1

    .line 2092048
    goto :goto_0

    .line 2092049
    :cond_2
    const-string v2, "GROUP:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2092050
    const-string v2, "GROUP:"

    const-string p0, ""

    invoke-virtual {v0, v2, p0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 2092051
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    move-object v1, v2

    .line 2092052
    goto :goto_1
.end method

.method public final aq()Z
    .locals 1

    .prologue
    .line 2092042
    invoke-virtual {p0}, LX/EDx;->ap()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final at()Z
    .locals 1

    .prologue
    .line 2092041
    iget-object v0, p0, LX/EDx;->bb:LX/EIq;

    invoke-virtual {v0}, LX/EIq;->c()Z

    move-result v0

    return v0
.end method

.method public final au()Z
    .locals 2

    .prologue
    .line 2092038
    iget-object v0, p0, LX/EDx;->bb:LX/EIq;

    .line 2092039
    iget-object v1, v0, LX/EIq;->f:LX/EIp;

    sget-object p0, LX/EIp;->RECORDING:LX/EIp;

    if-ne v1, p0, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2092040
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final aw()V
    .locals 1

    .prologue
    .line 2092036
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EDx;->aP:Z

    .line 2092037
    return-void
.end method

.method public final ay()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2092033
    iget-object v0, p0, LX/EDx;->bo:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2092034
    iget-object v0, p0, LX/EDx;->bn:Ljava/lang/String;

    .line 2092035
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EDx;->bo:Ljava/lang/String;

    goto :goto_0
.end method

.method public final az()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2092030
    iget-object v0, p0, LX/EDx;->bn:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2092031
    iget-object v0, p0, LX/EDx;->bo:Ljava/lang/String;

    .line 2092032
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EDx;->bn:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/EGE;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2092027
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EDx;->ae:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 2092028
    :cond_0
    const/4 v0, 0x0

    .line 2092029
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2092026
    return-void
.end method

.method public final b(LX/EC0;)V
    .locals 2

    .prologue
    .line 2092021
    iget-object v0, p0, LX/EDx;->z:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2092022
    iget-object v1, p0, LX/EDx;->O:Ljava/util/List;

    monitor-enter v1

    .line 2092023
    :try_start_0
    iget-object v0, p0, LX/EDx;->O:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2092024
    iget-object v0, p0, LX/EDx;->O:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->P:LX/0Px;

    .line 2092025
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(LX/EC2;)V
    .locals 1

    .prologue
    .line 2092018
    if-eqz p1, :cond_0

    .line 2092019
    iget-object v0, p0, LX/EDx;->cu:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2092020
    :cond_0
    return-void
.end method

.method public final b(LX/EG5;)V
    .locals 1

    .prologue
    .line 2092010
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    if-eqz v0, :cond_0

    .line 2092011
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    .line 2092012
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2092013
    iput-object p1, v0, LX/EFw;->k:LX/EG5;

    .line 2092014
    :cond_0
    return-void
.end method

.method public final b(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2092066
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08070a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, LX/EDx;->az()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2092067
    iget-object v1, p0, LX/EDx;->S:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2092068
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2092069
    const/4 v0, 0x0

    .line 2092070
    monitor-enter p0

    .line 2092071
    :try_start_0
    iget-object v1, p0, LX/EDx;->bL:Landroid/view/View;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_0

    iget-object v1, p0, LX/EDx;->bL:Landroid/view/View;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2092072
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/EDx;->bL:Landroid/view/View;

    .line 2092073
    const/4 v0, 0x1

    .line 2092074
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2092075
    if-eqz v0, :cond_2

    .line 2092076
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2092077
    invoke-static {p0}, LX/EDx;->bt(LX/EDx;)LX/EFy;

    move-result-object v0

    iget-wide v2, p0, LX/EDx;->aw:J

    invoke-virtual {v0, v2, v3, v4}, LX/EFy;->b(JLandroid/view/View;)V

    .line 2092078
    :cond_2
    :goto_0
    return-void

    .line 2092079
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2092080
    :cond_3
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_2

    .line 2092081
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    iget-wide v2, p0, LX/EDx;->aw:J

    invoke-virtual {v0, v2, v3, v4}, LX/2Tm;->a(JLandroid/view/View;)V

    goto :goto_0
.end method

.method public final b(ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2092082
    if-eqz p2, :cond_0

    .line 2092083
    invoke-direct {p0}, LX/EDx;->bE()V

    .line 2092084
    :cond_0
    iget v0, p0, LX/EDx;->am:I

    if-nez v0, :cond_2

    .line 2092085
    const-string v0, "WebrtcUiHandler"

    const-string v1, "Can\'t start call since it is ended already"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2092086
    :cond_1
    :goto_0
    return-void

    .line 2092087
    :cond_2
    iget-object v0, p0, LX/EDx;->t:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EDx;->t:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/EDx;->bQ:Z

    if-nez v0, :cond_3

    .line 2092088
    sget-object v0, LX/7TQ;->CallEndInAnotherCall:LX/7TQ;

    invoke-virtual {p0, v0}, LX/EDx;->a(LX/7TQ;)V

    goto :goto_0

    .line 2092089
    :cond_3
    invoke-static {p0}, LX/EDx;->bG(LX/EDx;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2092090
    invoke-static {p0}, LX/EDx;->bF(LX/EDx;)V

    .line 2092091
    invoke-direct {p0, v2}, LX/EDx;->r(Z)V

    .line 2092092
    iget-boolean v0, p0, LX/EDx;->bp:Z

    if-eqz v0, :cond_4

    .line 2092093
    invoke-virtual {p0, v2}, LX/EDx;->o(Z)Z

    .line 2092094
    :cond_4
    if-nez p2, :cond_1

    .line 2092095
    invoke-virtual {p0, p1}, LX/EDx;->i(Z)V

    goto :goto_0

    .line 2092096
    :cond_5
    iget-boolean v0, p0, LX/EDx;->bQ:Z

    if-eqz v0, :cond_6

    .line 2092097
    invoke-static {p0}, LX/EDx;->bF(LX/EDx;)V

    .line 2092098
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EDx;->r(Z)V

    goto :goto_0

    .line 2092099
    :cond_6
    const/4 v0, 0x2

    invoke-static {p0, v0}, LX/EDx;->h(LX/EDx;I)V

    .line 2092100
    iget-object v0, p0, LX/EDx;->q:LX/2XQ;

    invoke-virtual {p0}, LX/EDx;->aa()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/2XQ;->a(Z)V

    .line 2092101
    invoke-direct {p0}, LX/EDx;->cb()V

    .line 2092102
    if-nez p2, :cond_7

    .line 2092103
    invoke-virtual {p0, p1}, LX/EDx;->i(Z)V

    .line 2092104
    :cond_7
    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v0

    iput-wide v0, p0, LX/EDx;->aK:J

    .line 2092105
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EDx;->aL:J

    .line 2092106
    invoke-static {p0}, LX/EDx;->bA(LX/EDx;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2092107
    const-string v0, "com.facebook.rtc.fbwebrtc.intent.action.INCOMING_CALL"

    invoke-direct {p0, v0}, LX/EDx;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2092108
    iget-object v1, p0, LX/EDx;->j:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2092109
    :cond_8
    invoke-virtual {p0, v2}, LX/EDx;->o(Z)Z

    goto :goto_0
.end method

.method public final ba()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2092110
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2092111
    invoke-direct {p0, v2}, LX/EDx;->A(Z)V

    .line 2092112
    invoke-static {p0, v2, v2}, LX/EDx;->e(LX/EDx;ZZ)V

    .line 2092113
    :cond_0
    :goto_0
    return-void

    .line 2092114
    :cond_1
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2092115
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    invoke-virtual {p0}, LX/EDx;->U()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2, v2}, LX/2Tm;->a(Ljava/lang/String;ZZZ)V

    goto :goto_0
.end method

.method public final bb()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2092116
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2092117
    invoke-direct {p0, v2}, LX/EDx;->A(Z)V

    .line 2092118
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v0}, LX/EFw;->c()Z

    move-result v0

    invoke-static {p0, v0, v2}, LX/EDx;->e(LX/EDx;ZZ)V

    .line 2092119
    :cond_0
    :goto_0
    return-void

    .line 2092120
    :cond_1
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2092121
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    const-string v1, ""

    invoke-virtual {v0, v1, v2, v3, v3}, LX/2Tm;->a(Ljava/lang/String;ZZZ)V

    goto :goto_0
.end method

.method public final bd()V
    .locals 2

    .prologue
    .line 2092122
    iget-object v0, p0, LX/EDx;->bq:LX/EGe;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EDx;->ar:LX/EDv;

    sget-object v1, LX/EDv;->STARTED:LX/EDv;

    if-eq v0, v1, :cond_0

    .line 2092123
    iget-object v0, p0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v0}, LX/EGe;->C()V

    .line 2092124
    :cond_0
    return-void
.end method

.method public final bh()V
    .locals 3

    .prologue
    .line 2092125
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2092126
    invoke-virtual {v0}, LX/EC0;->t()V

    .line 2092127
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2092128
    :cond_0
    return-void
.end method

.method public final bj()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/EGE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2092129
    iget-object v0, p0, LX/EDx;->ae:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 2092130
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2092131
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final bk()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2092132
    iget-object v1, p0, LX/EDx;->ae:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 2092133
    :goto_0
    return v0

    .line 2092134
    :cond_0
    iget-object v1, p0, LX/EDx;->ae:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGE;

    .line 2092135
    invoke-virtual {v0}, LX/EGE;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2092136
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 2092137
    goto :goto_1

    :cond_1
    move v0, v1

    .line 2092138
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final bn()Z
    .locals 2

    .prologue
    .line 2092139
    new-instance v0, LX/EDl;

    invoke-direct {v0, p0}, LX/EDl;-><init>(LX/EDx;)V

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/EDx;->a(LX/EDx;Lcom/android/internal/util/Predicate;Z)Z

    move-result v0

    return v0
.end method

.method public final bo()LX/EGp;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2092140
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aT()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->bk()I

    move-result v0

    if-gt v0, v2, :cond_0

    .line 2092141
    iget-boolean v0, p0, LX/EDx;->bZ:Z

    move v0, v0

    .line 2092142
    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->cb:Z

    if-nez v0, :cond_1

    .line 2092143
    iget v0, p0, LX/EDx;->ah:I

    move v0, v0

    .line 2092144
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 2092145
    :cond_0
    const/4 v0, 0x0

    .line 2092146
    :goto_0
    return-object v0

    .line 2092147
    :cond_1
    iget v0, p0, LX/EDx;->ah:I

    move v0, v0

    .line 2092148
    if-le v0, v2, :cond_3

    .line 2092149
    invoke-virtual {p0}, LX/EDx;->bn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2092150
    sget-object v0, LX/EGp;->IN_CALL:LX/EGp;

    goto :goto_0

    .line 2092151
    :cond_2
    sget-object v0, LX/EGp;->RUNG_START:LX/EGp;

    goto :goto_0

    .line 2092152
    :cond_3
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    .line 2092153
    iget-boolean v1, v0, LX/EFw;->f:Z

    move v0, v1

    .line 2092154
    if-eqz v0, :cond_8

    .line 2092155
    const/4 v0, 0x0

    .line 2092156
    new-instance v1, LX/EDm;

    invoke-direct {v1, p0}, LX/EDm;-><init>(LX/EDx;)V

    invoke-static {p0, v1, v0}, LX/EDx;->a(LX/EDx;Lcom/android/internal/util/Predicate;Z)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v0, 0x1

    :cond_4
    move v0, v0

    .line 2092157
    if-nez v0, :cond_6

    const/4 v0, 0x0

    .line 2092158
    new-instance v1, LX/EDk;

    invoke-direct {v1, p0}, LX/EDk;-><init>(LX/EDx;)V

    invoke-static {p0, v1, v0}, LX/EDx;->a(LX/EDx;Lcom/android/internal/util/Predicate;Z)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v0, 0x1

    :cond_5
    move v0, v0

    .line 2092159
    if-nez v0, :cond_7

    .line 2092160
    :cond_6
    sget-object v0, LX/EGp;->RUNG_START:LX/EGp;

    goto :goto_0

    .line 2092161
    :cond_7
    sget-object v0, LX/EGp;->NOTIFY_START:LX/EGp;

    goto :goto_0

    .line 2092162
    :cond_8
    sget-object v0, LX/EGp;->NOTIFY_START:LX/EGp;

    goto :goto_0
.end method

.method public final br()Z
    .locals 1

    .prologue
    .line 2092163
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bs()V
    .locals 3

    .prologue
    .line 2092164
    iget-object v0, p0, LX/EDx;->bC:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget v2, LX/3Dx;->aR:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(LX/0c0;I)V

    .line 2092165
    return-void
.end method

.method public final c(Lcom/facebook/rtc/helpers/RtcCallStartParams;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2092056
    iget-object v0, p0, LX/EDx;->bS:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2092057
    iget-object v0, p0, LX/EDx;->bS:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2092058
    iput-object v5, p0, LX/EDx;->bS:Ljava/util/concurrent/ScheduledFuture;

    .line 2092059
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2092060
    iget-object v0, p0, LX/EDx;->v:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$5;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$5;-><init>(LX/EDx;)V

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->bS:Ljava/util/concurrent/ScheduledFuture;

    .line 2092061
    :cond_1
    invoke-static {p0, p1, v5}, LX/EDx;->a(LX/EDx;Lcom/facebook/rtc/helpers/RtcCallStartParams;Lcom/facebook/webrtc/ConferenceCall;)V

    .line 2092062
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/EDx;->bY:LX/03R;

    .line 2092063
    invoke-static {p0}, LX/EDx;->bA(LX/EDx;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/EDx;->bQ:Z

    if-nez v0, :cond_2

    .line 2092064
    invoke-static {p0}, LX/EDx;->bu(LX/EDx;)V

    .line 2092065
    :cond_2
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2092520
    iput-boolean v0, p0, LX/EDx;->bZ:Z

    .line 2092521
    iput v0, p0, LX/EDx;->ca:I

    .line 2092522
    sget-object v0, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    invoke-virtual {p0, v0}, LX/EDx;->a(LX/7TQ;)V

    .line 2092523
    invoke-virtual {p0}, LX/EDx;->x()V

    .line 2092524
    return-void
.end method

.method public final g(Z)V
    .locals 2

    .prologue
    .line 2092364
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, LX/EDx;->a(LX/EDx;ZZZ)V

    .line 2092365
    return-void
.end method

.method public final h(Z)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 2092366
    iget-object v0, p0, LX/EDx;->cq:LX/EDw;

    iget v0, v0, LX/EDw;->a:I

    .line 2092367
    iget-object v1, p0, LX/EDx;->cq:LX/EDw;

    iget v1, v1, LX/EDw;->b:I

    .line 2092368
    if-eqz p1, :cond_1

    .line 2092369
    iget-object v2, p0, LX/EDx;->cq:LX/EDw;

    iget-object v3, p0, LX/EDx;->bC:LX/0ad;

    sget v4, LX/3Dx;->ad:I

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    iput v3, v2, LX/EDw;->a:I

    .line 2092370
    iget-object v2, p0, LX/EDx;->cq:LX/EDw;

    iget-object v3, p0, LX/EDx;->bC:LX/0ad;

    sget v4, LX/3Dx;->ac:I

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    iput v3, v2, LX/EDw;->b:I

    .line 2092371
    :goto_0
    iget-object v2, p0, LX/EDx;->cq:LX/EDw;

    iget v2, v2, LX/EDw;->a:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, LX/EDx;->cq:LX/EDw;

    iget v0, v0, LX/EDw;->b:I

    if-eq v1, v0, :cond_0

    .line 2092372
    invoke-direct {p0}, LX/EDx;->bW()V

    .line 2092373
    :cond_0
    return-void

    .line 2092374
    :cond_1
    iget-object v2, p0, LX/EDx;->cq:LX/EDw;

    iput v5, v2, LX/EDw;->a:I

    .line 2092375
    iget-object v2, p0, LX/EDx;->cq:LX/EDw;

    iput v5, v2, LX/EDw;->b:I

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 2092376
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final handleError(I)V
    .locals 5

    .prologue
    .line 2092377
    const-string v0, "WebrtcUiHandler"

    const-string v1, "Handle error=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2092378
    invoke-virtual {p0}, LX/EDx;->al()V

    .line 2092379
    return-void
.end method

.method public final hideCallUI(ILjava/lang/String;JZLjava/lang/String;)V
    .locals 7

    .prologue
    .line 2092380
    iget-boolean v0, p0, LX/EDx;->aB:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->ag()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2092381
    iget-object v0, p0, LX/EDx;->a:LX/6c4;

    invoke-virtual {v0}, LX/6c4;->a()V

    .line 2092382
    :cond_0
    iput-boolean p5, p0, LX/EDx;->bm:Z

    .line 2092383
    invoke-static {}, LX/7TQ;->values()[LX/7TQ;

    move-result-object v0

    .line 2092384
    if-ltz p1, :cond_1

    array-length v1, v0

    if-lt p1, v1, :cond_2

    .line 2092385
    :cond_1
    const-string v0, "WebrtcUiHandler"

    const-string v1, "Invalid webrtc EndCallReason: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2092386
    sget-object v0, LX/7TQ;->CallEndWebRTCError:LX/7TQ;

    iput-object v0, p0, LX/EDx;->bl:LX/7TQ;

    .line 2092387
    :goto_0
    iget-object v6, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$44;

    move-object v1, p0

    move-wide v2, p3

    move v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$44;-><init>(LX/EDx;JZLjava/lang/String;)V

    const v1, 0x67918ce0

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092388
    return-void

    .line 2092389
    :cond_2
    aget-object v0, v0, p1

    iput-object v0, p0, LX/EDx;->bl:LX/7TQ;

    goto :goto_0
.end method

.method public final i(Z)V
    .locals 2

    .prologue
    .line 2092390
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    .line 2092391
    invoke-virtual {p0}, LX/EDx;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2092392
    :goto_0
    return-void

    .line 2092393
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2092394
    iget-object v0, p0, LX/EDx;->J:LX/ECO;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/ECO;->a(ZZ)V

    .line 2092395
    :cond_1
    invoke-direct {p0}, LX/EDx;->cd()V

    goto :goto_0
.end method

.method public final initializeCall(JJZ)V
    .locals 9

    .prologue
    .line 2092396
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$7;

    move-object v2, p0

    move v3, p5

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$7;-><init>(LX/EDx;ZJJ)V

    const v2, 0x6a95c996

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092397
    return-void
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2092398
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    .line 2092399
    iget-boolean v1, v0, LX/EFw;->d:Z

    if-eqz v1, :cond_2

    .line 2092400
    const/4 v1, 0x0

    .line 2092401
    :goto_0
    move v0, v1

    .line 2092402
    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->aZ:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v1, v0, LX/EFw;->h:Lcom/facebook/webrtc/ConferenceCall;

    invoke-virtual {v1}, Lcom/facebook/webrtc/ConferenceCall;->a()Z

    move-result v1

    goto :goto_0
.end method

.method public final k(Z)V
    .locals 3

    .prologue
    .line 2092403
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2092404
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EDx;->bs:Z

    .line 2092405
    iput-boolean p1, p0, LX/EDx;->bu:Z

    .line 2092406
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2092407
    invoke-virtual {p0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2092408
    iget-object v0, p0, LX/EDx;->cy:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79G;

    invoke-virtual {v0, p1}, LX/79G;->a(Z)V

    .line 2092409
    :cond_0
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v0, p1}, LX/EFw;->b(Z)V

    .line 2092410
    invoke-virtual {p0}, LX/EDx;->onEscalationSuccess()V

    .line 2092411
    :cond_1
    :goto_0
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$33;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$33;-><init>(LX/EDx;)V

    const v2, -0x1ac1e88f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092412
    return-void

    .line 2092413
    :cond_2
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_1

    .line 2092414
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    invoke-virtual {p0}, LX/EDx;->U()Ljava/lang/String;

    move-result-object v1

    .line 2092415
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2092416
    iget-object v2, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v2, p1, v1}, Lcom/facebook/webrtc/WebrtcEngine;->sendEscalationResponse(ZLjava/lang/String;)V

    .line 2092417
    :cond_3
    goto :goto_0
.end method

.method public final k()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2092418
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EDx;->bC:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/3Dx;->j:S

    invoke-interface {v1, v2, v3, v4, v0}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final l(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2092419
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2092420
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2092421
    iput-boolean v1, p0, LX/EDx;->by:Z

    .line 2092422
    iget-object v0, p0, LX/EDx;->ad:LX/EFw;

    invoke-virtual {v0, p1}, LX/EFw;->b(Z)V

    .line 2092423
    iput-boolean p1, p0, LX/EDx;->bt:Z

    .line 2092424
    if-eqz p1, :cond_1

    iget-boolean v0, p0, LX/EDx;->bX:Z

    if-eqz v0, :cond_1

    .line 2092425
    invoke-virtual {p0, v1}, LX/EDx;->onEscalationResponse(Z)V

    .line 2092426
    :cond_0
    :goto_0
    return-void

    .line 2092427
    :cond_1
    if-eqz p1, :cond_0

    .line 2092428
    iget-object v0, p0, LX/EDx;->v:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$34;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$34;-><init>(LX/EDx;)V

    const-wide/16 v2, 0x19

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/EDx;->bT:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0

    .line 2092429
    :cond_2
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    if-eqz v0, :cond_0

    .line 2092430
    iget-object v0, p0, LX/EDx;->T:LX/2Tm;

    invoke-virtual {p0}, LX/EDx;->U()Ljava/lang/String;

    move-result-object v1

    .line 2092431
    invoke-virtual {v0}, LX/2Tm;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2092432
    iget-object v2, v0, LX/2Tm;->v:Lcom/facebook/webrtc/WebrtcEngine;

    invoke-virtual {v2, p1, v1}, Lcom/facebook/webrtc/WebrtcEngine;->sendEscalationRequest(ZLjava/lang/String;)V

    .line 2092433
    :cond_3
    goto :goto_0
.end method

.method public final localMediaStateChanged(ZZZ)V
    .locals 0

    .prologue
    .line 2092434
    return-void
.end method

.method public final n(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2092435
    iget-object v0, p0, LX/EDx;->m:LX/3GP;

    invoke-virtual {v0}, LX/3GP;->a()V

    .line 2092436
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 2092437
    iget-object v0, p0, LX/EDx;->r:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 2092438
    if-eqz p1, :cond_0

    .line 2092439
    iget-boolean v0, p0, LX/EDx;->bQ:Z

    move v0, v0

    .line 2092440
    if-nez v0, :cond_0

    .line 2092441
    iget v0, p0, LX/EDx;->aG:I

    invoke-direct {p0, v0}, LX/EDx;->c(I)V

    .line 2092442
    :cond_0
    return-void
.end method

.method public final o(Z)Z
    .locals 14

    .prologue
    const/4 v0, 0x1

    .line 2092443
    iget-boolean v1, p0, LX/EDx;->aX:Z

    if-ne p1, v1, :cond_1

    .line 2092444
    :cond_0
    :goto_0
    return v0

    .line 2092445
    :cond_1
    if-nez p1, :cond_2

    iget-object v1, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_2

    .line 2092446
    iget-object v1, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2092447
    const/4 v1, 0x0

    iput-object v1, p0, LX/EDx;->bU:Ljava/util/concurrent/ScheduledFuture;

    .line 2092448
    invoke-static {p0}, LX/EDx;->ci(LX/EDx;)V

    goto :goto_0

    .line 2092449
    :cond_2
    iget-boolean v1, p0, LX/EDx;->bp:Z

    if-eqz v1, :cond_4

    .line 2092450
    if-nez p1, :cond_3

    .line 2092451
    iget-object v1, p0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v1}, LX/EGe;->v()Z

    .line 2092452
    iget v1, p0, LX/EDx;->bE:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/EDx;->bE:I

    .line 2092453
    :goto_1
    if-eqz v0, :cond_0

    .line 2092454
    iput-boolean p1, p0, LX/EDx;->aX:Z

    goto :goto_0

    .line 2092455
    :cond_3
    iget-object v0, p0, LX/EDx;->bq:LX/EGe;

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 2092456
    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->aa()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2092457
    iget-boolean v8, v0, LX/EGe;->P:Z

    if-eqz v8, :cond_6

    .line 2092458
    :goto_2
    move v0, v9

    .line 2092459
    goto :goto_1

    .line 2092460
    :cond_4
    if-nez p1, :cond_5

    .line 2092461
    iget-wide v2, p0, LX/EDx;->bI:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 2092462
    iget-wide v2, p0, LX/EDx;->bG:J

    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v4

    iget-wide v6, p0, LX/EDx;->bI:J

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/EDx;->bG:J

    goto :goto_0

    .line 2092463
    :cond_5
    iget-wide v2, p0, LX/EDx;->bF:J

    invoke-virtual {p0}, LX/EDx;->aU()J

    move-result-wide v4

    iget-wide v6, p0, LX/EDx;->bI:J

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/EDx;->bF:J

    goto :goto_0

    .line 2092464
    :cond_6
    iget-boolean v8, v0, LX/EGe;->U:Z

    if-nez v8, :cond_8

    .line 2092465
    invoke-static {v0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v8

    if-nez v8, :cond_7

    invoke-static {v0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v8

    if-eqz v8, :cond_c

    :cond_7
    const/4 v8, 0x1

    :goto_3
    move v8, v8

    .line 2092466
    if-nez v8, :cond_8

    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->aP()Z

    move-result v8

    if-nez v8, :cond_8

    invoke-static {v0}, LX/EGe;->aj(LX/EGe;)Z

    move-result v8

    if-nez v8, :cond_8

    invoke-static {v0}, LX/EGe;->ai(LX/EGe;)Z

    move-result v8

    if-nez v8, :cond_8

    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->at()Z

    move-result v8

    if-nez v8, :cond_8

    move v9, v10

    .line 2092467
    goto :goto_2

    .line 2092468
    :cond_8
    iget-boolean v8, v0, LX/EGe;->P:Z

    if-nez v8, :cond_a

    .line 2092469
    iget-object v8, v0, LX/EGe;->m:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v12

    iput-wide v12, v0, LX/EGe;->ab:J

    .line 2092470
    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->af()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 2092471
    iget-object v8, v0, LX/EGe;->m:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v12

    iput-wide v12, v0, LX/EGe;->ac:J

    .line 2092472
    :cond_9
    iget v8, v0, LX/EGe;->ad:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v0, LX/EGe;->ad:I

    .line 2092473
    :cond_a
    iget-object v11, v0, LX/EGe;->D:LX/EI9;

    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->aZ()Z

    move-result v8

    .line 2092474
    iput-boolean v8, v11, LX/EI9;->L:Z

    .line 2092475
    iget-object v11, v0, LX/EGe;->D:LX/EI9;

    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->aJ()Z

    move-result v8

    if-nez v8, :cond_b

    move v8, v9

    .line 2092476
    :goto_4
    iput-boolean v8, v11, LX/EI9;->K:Z

    .line 2092477
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 2092478
    iget-boolean v8, v0, LX/EGe;->P:Z

    if-eqz v8, :cond_d

    .line 2092479
    :goto_5
    goto/16 :goto_2

    :cond_b
    move v8, v10

    .line 2092480
    goto :goto_4

    :cond_c
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 2092481
    :cond_d
    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    sget-object v10, LX/EG5;->ChatHead:LX/EG5;

    invoke-virtual {v8, v10}, LX/EDx;->b(LX/EG5;)V

    .line 2092482
    iget-object v8, v0, LX/EGe;->W:LX/EHn;

    invoke-virtual {v8, v12}, LX/EHn;->setVisibility(I)V

    .line 2092483
    iget-object v8, v0, LX/EGe;->W:LX/EHn;

    iget-object v10, v0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v8, v10}, LX/EHn;->addView(Landroid/view/View;)V

    .line 2092484
    iget-object v8, v0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v8, v12}, LX/EI9;->setVisibility(I)V

    .line 2092485
    iget-object v8, v0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v8}, LX/EI9;->f()V

    .line 2092486
    iget-object v8, v0, LX/EGe;->s:LX/EFs;

    iget-object v10, v0, LX/EGe;->W:LX/EHn;

    const/16 v11, 0x10

    invoke-virtual {v8, v10, v11, v12}, LX/EFs;->a(Landroid/view/View;IZ)V

    .line 2092487
    invoke-static {v0}, LX/EGe;->aC(LX/EGe;)V

    .line 2092488
    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8, v13}, LX/EDx;->h(Z)V

    .line 2092489
    iget-boolean v8, v0, LX/EGe;->V:Z

    if-nez v8, :cond_e

    .line 2092490
    iget-object v8, v0, LX/EGe;->l:LX/6c4;

    .line 2092491
    new-instance v10, Landroid/content/Intent;

    sget-object v11, LX/3RB;->B:Ljava/lang/String;

    invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2092492
    invoke-static {v8, v10}, LX/6c4;->a(LX/6c4;Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2092493
    :cond_e
    iget-object v8, v0, LX/EGe;->W:LX/EHn;

    invoke-virtual {v0}, LX/EGe;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f021783

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/EHn;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2092494
    iput-boolean v13, v0, LX/EGe;->P:Z

    .line 2092495
    iput-boolean v12, v0, LX/EGe;->X:Z

    .line 2092496
    iput-boolean v12, v0, LX/EGe;->S:Z

    .line 2092497
    invoke-static {v0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 2092498
    invoke-static {v0}, LX/EGe;->X(LX/EGe;)V

    .line 2092499
    :cond_f
    invoke-static {v0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 2092500
    invoke-static {v0}, LX/EGe;->Z(LX/EGe;)V

    .line 2092501
    :cond_10
    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->aP()Z

    move-result v8

    if-nez v8, :cond_11

    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->aR()Z

    move-result v8

    if-eqz v8, :cond_15

    .line 2092502
    :cond_11
    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->aZ()Z

    move-result v8

    if-eqz v8, :cond_14

    .line 2092503
    iget-object v10, v0, LX/EGe;->D:LX/EI9;

    iget-object v8, v0, LX/EGe;->z:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EFj;

    invoke-virtual {v8}, LX/EFj;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8}, LX/EI9;->a(Ljava/lang/String;)V

    .line 2092504
    :cond_12
    :goto_6
    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->j()Z

    move-result v8

    if-eqz v8, :cond_13

    .line 2092505
    invoke-static {v0}, LX/EGe;->aF(LX/EGe;)V

    .line 2092506
    invoke-static {v0}, LX/EGe;->aG(LX/EGe;)V

    .line 2092507
    :cond_13
    invoke-static {v0}, LX/EGe;->af(LX/EGe;)V

    .line 2092508
    iget-object v8, v0, LX/EGe;->D:LX/EI9;

    .line 2092509
    new-instance v10, LX/EGM;

    invoke-direct {v10, v0, v8}, LX/EGM;-><init>(LX/EGe;Landroid/view/View;)V

    iput-object v10, v0, LX/EGe;->af:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 2092510
    iget-object v8, v0, LX/EGe;->D:LX/EI9;

    invoke-virtual {v8}, LX/EI9;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v8

    iget-object v10, v0, LX/EGe;->af:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v8, v10}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2092511
    iget-object v8, v0, LX/EGe;->ag:LX/EFp;

    invoke-virtual {v8}, LX/EFp;->g()V

    .line 2092512
    invoke-static {v0}, LX/EGe;->av(LX/EGe;)V

    goto/16 :goto_5

    .line 2092513
    :cond_14
    iget-object v8, v0, LX/EGe;->D:LX/EI9;

    const v10, 0x7f08072d

    invoke-virtual {v0, v10}, LX/EGe;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/EI9;->a(Ljava/lang/String;)V

    goto :goto_6

    .line 2092514
    :cond_15
    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->at()Z

    move-result v8

    if-eqz v8, :cond_12

    .line 2092515
    iget-object v8, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EDx;

    invoke-virtual {v8}, LX/EDx;->au()Z

    move-result v8

    if-eqz v8, :cond_16

    .line 2092516
    iget-object v8, v0, LX/EGe;->D:LX/EI9;

    const v10, 0x7f080790

    invoke-virtual {v0, v10}, LX/EGe;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/EI9;->a(Ljava/lang/String;)V

    goto :goto_6

    .line 2092517
    :cond_16
    iget-object v11, v0, LX/EGe;->D:LX/EI9;

    iget-object v8, v0, LX/EGe;->z:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EFj;

    iget-object v10, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v10}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/EDx;

    .line 2092518
    iget-object v12, v10, LX/EDx;->bl:LX/7TQ;

    move-object v10, v12

    .line 2092519
    sget-object v12, LX/EFi;->Activity:LX/EFi;

    invoke-virtual {v8, v10, v12}, LX/EFj;->a(LX/7TQ;LX/EFi;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v8}, LX/EI9;->a(Ljava/lang/String;)V

    goto/16 :goto_6
.end method

.method public final onAudioLevel(II)V
    .locals 3

    .prologue
    .line 2092361
    iget-boolean v0, p0, LX/EDx;->bA:Z

    if-nez v0, :cond_0

    .line 2092362
    :goto_0
    return-void

    .line 2092363
    :cond_0
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$40;

    invoke-direct {v1, p0, p2}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$40;-><init>(LX/EDx;I)V

    const v2, -0x61f653a4

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final onAudioLevelsUpdate(Lcom/facebook/webrtc/ConferenceCall;[Ljava/lang/String;[I)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2092525
    iget-boolean v1, p0, LX/EDx;->bB:Z

    if-nez v1, :cond_0

    .line 2092526
    :goto_0
    return-void

    .line 2092527
    :cond_0
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    array-length v1, p2

    array-length v2, p3

    if-eq v1, v2, :cond_2

    .line 2092528
    :cond_1
    const-string v0, "WebrtcUiHandler"

    const-string v1, "Malformed data for group audio level update"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2092529
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 2092530
    array-length v4, p2

    move v1, v0

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v5, p2, v0

    .line 2092531
    add-int/lit8 v2, v1, 0x1

    aget v1, p3, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2092532
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_1

    .line 2092533
    :cond_3
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$54;

    invoke-direct {v1, p0, v3}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$54;-><init>(LX/EDx;Ljava/util/Map;)V

    const v2, -0x4dfa7ab4

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final onCallEnded(Lcom/facebook/webrtc/ConferenceCall;ILjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2092534
    invoke-virtual {p1}, Lcom/facebook/webrtc/ConferenceCall;->callId()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object v1, p0

    move v2, p2

    move-object v3, p3

    move-object v7, p4

    invoke-virtual/range {v1 .. v7}, LX/EDx;->hideCallUI(ILjava/lang/String;JZLjava/lang/String;)V

    .line 2092535
    return-void
.end method

.method public final onCallJoined(Lcom/facebook/webrtc/ConferenceCall;)V
    .locals 0

    .prologue
    .line 2092536
    return-void
.end method

.method public final onDataMessage(Lcom/facebook/webrtc/ConferenceCall;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 2092537
    return-void
.end method

.method public final onDataReceived(Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 2092538
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$46;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$46;-><init>(LX/EDx;Ljava/lang/String;[B)V

    const v2, 0x727f3a67

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092539
    return-void
.end method

.method public final onDiscoveryRequest(JJLjava/lang/String;)V
    .locals 9

    .prologue
    .line 2092540
    iget-object v7, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$38;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$38;-><init>(LX/EDx;JJLjava/lang/String;)V

    const v1, 0x1f650853

    invoke-static {v7, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092541
    return-void
.end method

.method public final onDiscoveryResponse(JJLjava/lang/String;)V
    .locals 9

    .prologue
    .line 2092542
    iget-object v7, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$39;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$39;-><init>(LX/EDx;JJLjava/lang/String;)V

    const v1, 0x4f637fb5    # 3.81679744E9f

    invoke-static {v7, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092543
    return-void
.end method

.method public final onDominantSpeakerUpdate(Lcom/facebook/webrtc/ConferenceCall;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2092544
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$53;

    invoke-direct {v1, p0, p2}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$53;-><init>(LX/EDx;Ljava/lang/String;)V

    const v2, -0x5169688

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092545
    return-void
.end method

.method public final onEscalationRequest(Z)V
    .locals 3

    .prologue
    .line 2092546
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2092547
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$32;

    invoke-direct {v1, p0, p1}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$32;-><init>(LX/EDx;Z)V

    const v2, -0x3b0434c3

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092548
    return-void
.end method

.method public final onEscalationResponse(Z)V
    .locals 3

    .prologue
    .line 2092549
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2092550
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$35;

    invoke-direct {v1, p0, p1}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$35;-><init>(LX/EDx;Z)V

    const v2, -0x266dc24f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092551
    return-void
.end method

.method public final onEscalationSuccess()V
    .locals 3

    .prologue
    .line 2092552
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$36;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$36;-><init>(LX/EDx;)V

    const v2, 0x68de5a04

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092553
    return-void
.end method

.method public final onEscalationTimeout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2092554
    iput-boolean v2, p0, LX/EDx;->bu:Z

    .line 2092555
    iput-boolean v2, p0, LX/EDx;->bs:Z

    .line 2092556
    iput-boolean v2, p0, LX/EDx;->bt:Z

    .line 2092557
    invoke-virtual {p0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2092558
    iget-object v0, p0, LX/EDx;->bT:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2092559
    iget-object v0, p0, LX/EDx;->bT:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2092560
    const/4 v0, 0x0

    iput-object v0, p0, LX/EDx;->bT:Ljava/util/concurrent/ScheduledFuture;

    .line 2092561
    :cond_0
    invoke-virtual {p0, v2}, LX/EDx;->l(Z)V

    .line 2092562
    :cond_1
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$37;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$37;-><init>(LX/EDx;)V

    const v2, 0x369bccac

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092563
    return-void
.end method

.method public final onGameUpdate([B)V
    .locals 3

    .prologue
    .line 2092564
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$41;

    invoke-direct {v1, p0, p1}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$41;-><init>(LX/EDx;[B)V

    const v2, 0x42365182

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092565
    return-void
.end method

.method public final onIncomingCall(Lcom/facebook/webrtc/ConferenceCall;Ljava/lang/String;[Ljava/lang/String;IZ)V
    .locals 8

    .prologue
    .line 2092319
    iget-object v7, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$25;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$25;-><init>(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;Ljava/lang/String;[Ljava/lang/String;IZ)V

    const v1, 0xe3019cd

    invoke-static {v7, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092320
    return-void
.end method

.method public final onIncomingMissedCall(JJ)V
    .locals 7

    .prologue
    .line 2092291
    iget-object v6, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$45;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$45;-><init>(LX/EDx;JJ)V

    const v1, -0x32ae7c81

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092292
    return-void
.end method

.method public final onMediaConnectionUpdate(Lcom/facebook/webrtc/ConferenceCall;IIZI)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 2092293
    move-object v0, p0

    move v1, p4

    move v2, p2

    move v3, p3

    move v5, v4

    move v6, p5

    move v7, v4

    move v8, v4

    move v9, v4

    invoke-virtual/range {v0 .. v9}, LX/EDx;->showConnectionDetails(ZIIZZIIIZ)V

    .line 2092294
    return-void
.end method

.method public final onMediaStatusUpdate(Lcom/facebook/webrtc/ConferenceCall;[J[Ljava/lang/String;[Ljava/lang/String;[I[Z)V
    .locals 9

    .prologue
    .line 2092295
    iget-object v8, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;-><init>(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;[J[Ljava/lang/String;[Ljava/lang/String;[I[Z)V

    const v1, -0x142f64bb

    invoke-static {v8, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092296
    return-void
.end method

.method public final onPingAckMessageReceived(JJ)V
    .locals 0

    .prologue
    .line 2092297
    return-void
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 2092298
    iget-object v0, p0, LX/EDx;->bP:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public final remoteMediaStateChanged(ZZZ)V
    .locals 3

    .prologue
    .line 2092299
    iput-boolean p3, p0, LX/EDx;->cd:Z

    .line 2092300
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2092301
    invoke-virtual {v0}, LX/EC0;->v()V

    .line 2092302
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2092303
    :cond_0
    return-void
.end method

.method public final removeRemoteVideoTrack(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 2092304
    iget-object v6, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$42;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$42;-><init>(LX/EDx;Ljava/lang/String;Ljava/lang/String;J)V

    const v1, -0x33751abb    # -7.2821288E7f

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092305
    return-void
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 2092306
    iget-object v0, p0, LX/EDx;->bR:LX/EDu;

    sget-object v1, LX/EDu;->RECIPROCATED:LX/EDu;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setWebrtcManager(LX/2Tm;)V
    .locals 0

    .prologue
    .line 2092307
    iput-object p1, p0, LX/EDx;->T:LX/2Tm;

    .line 2092308
    return-void
.end method

.method public final showConnectionDetails(ZIIZZIIIZ)V
    .locals 12

    .prologue
    .line 2092309
    iget-object v11, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$43;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$43;-><init>(LX/EDx;ZIIZZIIIZ)V

    const v1, -0x4a98c8ce

    invoke-static {v11, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092310
    return-void
.end method

.method public final switchToContactingUI()V
    .locals 3

    .prologue
    .line 2092311
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$22;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$22;-><init>(LX/EDx;)V

    const v2, 0x7c63257f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092312
    return-void
.end method

.method public final switchToIncomingCallUI(JJZLjava/lang/String;)V
    .locals 9

    .prologue
    .line 2092313
    iget-object v8, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$24;

    move-object v1, p0

    move-wide v2, p3

    move-wide v4, p1

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$24;-><init>(LX/EDx;JJZLjava/lang/String;)V

    const v1, 0x2fd044ba

    invoke-static {v8, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092314
    return-void
.end method

.method public final switchToRingingUI()V
    .locals 3

    .prologue
    .line 2092315
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$23;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$23;-><init>(LX/EDx;)V

    const v2, 0xd864420

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092316
    return-void
.end method

.method public final switchToStreamingUI(ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 2092317
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$28;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$28;-><init>(LX/EDx;ZLjava/lang/String;)V

    const v2, -0x7a326eeb

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092318
    return-void
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 2092290
    iget-object v0, p0, LX/EDx;->bR:LX/EDu;

    sget-object v1, LX/EDu;->NONE:LX/EDu;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 2092321
    iget-boolean v0, p0, LX/EDx;->ch:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/EDx;->bp:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v0}, LX/EGe;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final updateRemoteVideoSupport(ZJ)V
    .locals 4

    .prologue
    .line 2092322
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$30;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$30;-><init>(LX/EDx;ZJ)V

    const v2, -0x5f7ad6b8

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092323
    return-void
.end method

.method public final updateStatesAndCallDuration()V
    .locals 0

    .prologue
    .line 2092324
    return-void
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 2092325
    invoke-direct {p0}, LX/EDx;->bK()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->M()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2092326
    iget-boolean v0, p0, LX/EDx;->aq:Z

    move v0, v0

    .line 2092327
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/EDx;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final webRTCControlRPC_AcceptIncomingCall(J)V
    .locals 3

    .prologue
    .line 2092328
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2092329
    invoke-virtual {v0}, LX/EC0;->a()V

    .line 2092330
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2092331
    :cond_0
    return-void
.end method

.method public final webRTCControlRPC_DisableVideo()V
    .locals 3

    .prologue
    .line 2092332
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$21;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$21;-><init>(LX/EDx;)V

    const v2, 0x2b011f74

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092333
    return-void
.end method

.method public final webRTCControlRPC_EnableVideo()V
    .locals 3

    .prologue
    .line 2092334
    sget-object v0, LX/03R;->YES:LX/03R;

    iput-object v0, p0, LX/EDx;->bv:LX/03R;

    .line 2092335
    iget-object v0, p0, LX/EDx;->u:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$20;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$20;-><init>(LX/EDx;)V

    const v2, -0x149fc0ac

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2092336
    return-void
.end method

.method public final webRTCControlRPC_StartOutgoingCall(JZ)V
    .locals 5

    .prologue
    .line 2092337
    iget-object v0, p0, LX/EDx;->l:LX/2S7;

    const-string v1, "rpc_server"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/2S7;->a(Ljava/lang/String;J)V

    .line 2092338
    iget-object v0, p0, LX/EDx;->C:LX/0aU;

    const-string v1, "RTC_START_CALL_ACTION"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2092339
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2092340
    const-string v0, "CONTACT_ID"

    invoke-virtual {v1, v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2092341
    const-string v0, "trigger"

    const-string v2, "rpc_server"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2092342
    const-string v0, "IS_VIDEO_CALL"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2092343
    iget-object v0, p0, LX/EDx;->h:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2092344
    return-void
.end method

.method public final webRTCControlRPC_ToggleSpeakerPhone()V
    .locals 3

    .prologue
    .line 2092345
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2092346
    invoke-virtual {v0}, LX/EC0;->d()V

    .line 2092347
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2092348
    :cond_0
    return-void
.end method

.method public final webRTCControlRPC_VolumeDown()V
    .locals 3

    .prologue
    .line 2092349
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2092350
    invoke-virtual {v0}, LX/EC0;->f()V

    .line 2092351
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2092352
    :cond_0
    return-void
.end method

.method public final webRTCControlRPC_VolumeUp()V
    .locals 3

    .prologue
    .line 2092353
    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2092354
    invoke-virtual {v0}, LX/EC0;->e()V

    .line 2092355
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2092356
    :cond_0
    return-void
.end method

.method public final x()V
    .locals 2

    .prologue
    .line 2092357
    sget-object v0, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/EDx;->a$redex0(LX/EDx;LX/7TQ;Z)V

    .line 2092358
    return-void
.end method

.method public final y()J
    .locals 2

    .prologue
    .line 2092359
    iget-wide v0, p0, LX/EDx;->ak:J

    return-wide v0
.end method

.method public final z()J
    .locals 2

    .prologue
    .line 2092360
    iget-wide v0, p0, LX/EDx;->ac:J

    return-wide v0
.end method
