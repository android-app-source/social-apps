.class public LX/D7a;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0fy;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/D7a;


# instance fields
.field public a:Landroid/view/ViewGroup;

.field public b:LX/1PJ;


# direct methods
.method public constructor <init>(LX/1PJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1967366
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 1967367
    iput-object p1, p0, LX/D7a;->b:LX/1PJ;

    .line 1967368
    return-void
.end method

.method public static a(LX/0QB;)LX/D7a;
    .locals 4

    .prologue
    .line 1967369
    sget-object v0, LX/D7a;->c:LX/D7a;

    if-nez v0, :cond_1

    .line 1967370
    const-class v1, LX/D7a;

    monitor-enter v1

    .line 1967371
    :try_start_0
    sget-object v0, LX/D7a;->c:LX/D7a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1967372
    if-eqz v2, :cond_0

    .line 1967373
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1967374
    new-instance p0, LX/D7a;

    invoke-static {v0}, LX/1PJ;->a(LX/0QB;)LX/1PJ;

    move-result-object v3

    check-cast v3, LX/1PJ;

    invoke-direct {p0, v3}, LX/D7a;-><init>(LX/1PJ;)V

    .line 1967375
    move-object v0, p0

    .line 1967376
    sput-object v0, LX/D7a;->c:LX/D7a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1967377
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1967378
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1967379
    :cond_1
    sget-object v0, LX/D7a;->c:LX/D7a;

    return-object v0

    .line 1967380
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1967381
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 2

    .prologue
    .line 1967382
    if-nez p2, :cond_0

    .line 1967383
    iget-object v0, p0, LX/D7a;->b:LX/1PJ;

    sget-object v1, LX/1PL;->IS_SCROLLING:LX/1PL;

    invoke-virtual {v0, v1}, LX/1PJ;->b(LX/1PL;)V

    .line 1967384
    :goto_0
    return-void

    .line 1967385
    :cond_0
    iget-object v0, p0, LX/D7a;->b:LX/1PJ;

    sget-object v1, LX/1PL;->IS_SCROLLING:LX/1PL;

    invoke-virtual {v0, v1}, LX/1PJ;->a(LX/1PL;)V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 1967386
    iget-object v0, p0, LX/D7a;->b:LX/1PJ;

    invoke-virtual {v0}, LX/1PJ;->b()V

    .line 1967387
    return-void
.end method
