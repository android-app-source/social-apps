.class public final LX/EYK;
.super Ljava/lang/Exception;
.source ""


# instance fields
.field private final description:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final proto:LX/EWY;


# direct methods
.method public constructor <init>(LX/EYE;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2136979
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, LX/EYE;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 2136980
    invoke-interface {p1}, LX/EYE;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EYK;->name:Ljava/lang/String;

    .line 2136981
    invoke-interface {p1}, LX/EYE;->h()LX/EWY;

    move-result-object v0

    iput-object v0, p0, LX/EYK;->proto:LX/EWY;

    .line 2136982
    iput-object p2, p0, LX/EYK;->description:Ljava/lang/String;

    .line 2136983
    return-void
.end method

.method public constructor <init>(LX/EYE;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2136984
    invoke-direct {p0, p1, p2}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    .line 2136985
    invoke-virtual {p0, p3}, LX/EYK;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 2136986
    return-void
.end method

.method public constructor <init>(LX/EYQ;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2136987
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, LX/EYQ;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 2136988
    invoke-virtual {p1}, LX/EYQ;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EYK;->name:Ljava/lang/String;

    .line 2136989
    iget-object v0, p1, LX/EYQ;->a:LX/EXV;

    move-object v0, v0

    .line 2136990
    iput-object v0, p0, LX/EYK;->proto:LX/EWY;

    .line 2136991
    iput-object p2, p0, LX/EYK;->description:Ljava/lang/String;

    .line 2136992
    return-void
.end method
