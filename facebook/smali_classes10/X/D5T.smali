.class public final LX/D5T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V
    .locals 0

    .prologue
    .line 1963649
    iput-object p1, p0, LX/D5T;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 3

    .prologue
    .line 1963650
    iget-object v0, p0, LX/D5T;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->o:LX/D4i;

    .line 1963651
    if-nez p2, :cond_1

    .line 1963652
    invoke-static {v0}, LX/D4i;->c$redex0(LX/D4i;)V

    .line 1963653
    :goto_0
    iget-object v0, p0, LX/D5T;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->q:LX/D4G;

    .line 1963654
    iget-object v1, v0, LX/D4G;->a:LX/D4D;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/D4D;->removeMessages(I)V

    .line 1963655
    if-nez p2, :cond_0

    .line 1963656
    iget-object v0, p0, LX/D5T;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->x:LX/1Kt;

    invoke-virtual {v0, p1}, LX/1Kt;->b(LX/0g8;)V

    .line 1963657
    :cond_0
    return-void

    .line 1963658
    :cond_1
    iget-object v1, v0, LX/D4i;->a:LX/D4g;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/D4g;->removeMessages(I)V

    .line 1963659
    invoke-static {v0}, LX/D4i;->e(LX/D4i;)V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 5

    .prologue
    .line 1963660
    iget-object v0, p0, LX/D5T;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    .line 1963661
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    if-eqz v1, :cond_0

    if-lez p3, :cond_0

    if-gtz p4, :cond_3

    .line 1963662
    :cond_0
    iget-object v0, p0, LX/D5T;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    .line 1963663
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->T:LX/0g8;

    invoke-interface {v1}, LX/0g8;->t()I

    move-result v1

    if-lez v1, :cond_7

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1963664
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D5T;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->getTop()I

    move-result v0

    if-eqz v0, :cond_2

    .line 1963665
    :cond_1
    iget-object v0, p0, LX/D5T;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->setVisibility(I)V

    .line 1963666
    :goto_1
    return-void

    .line 1963667
    :cond_2
    iget-object v0, p0, LX/D5T;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->setVisibility(I)V

    goto :goto_1

    .line 1963668
    :cond_3
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    add-int v2, p2, p3

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, LX/1Qr;->h_(I)I

    move-result v3

    .line 1963669
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->D:LX/0sV;

    iget v1, v1, LX/0sV;->f:I

    move v1, v1

    .line 1963670
    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    invoke-virtual {v2}, LX/D6S;->size()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1963671
    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_0

    .line 1963672
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    add-int p1, v3, v2

    invoke-virtual {v1, p1}, LX/D6S;->b(I)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1963673
    iget-object p1, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, p1

    .line 1963674
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1963675
    instance-of p1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez p1, :cond_5

    .line 1963676
    :cond_4
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1963677
    :cond_5
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1963678
    iget-object p1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->af:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 1963679
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object p1

    .line 1963680
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    if-eqz p2, :cond_6

    .line 1963681
    iget-object p2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->C:LX/3DC;

    sget-object p3, LX/379;->FEED:LX/379;

    invoke-virtual {p2, p1, p3}, LX/3DC;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z

    .line 1963682
    :cond_6
    iget-object p1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->af:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    goto :goto_0
.end method
