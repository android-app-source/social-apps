.class public final LX/DbD;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2016947
    new-instance v0, LX/0U1;

    const-string v1, "image_hash"

    const-string v2, "STRING PRIMARY KEY"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/DbD;->a:LX/0U1;

    .line 2016948
    new-instance v0, LX/0U1;

    const-string v1, "image_fbid"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/DbD;->b:LX/0U1;

    .line 2016949
    new-instance v0, LX/0U1;

    const-string v1, "date_taken"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/DbD;->c:LX/0U1;

    .line 2016950
    new-instance v0, LX/0U1;

    const-string v1, "upload_date"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/DbD;->d:LX/0U1;

    .line 2016951
    new-instance v0, LX/0U1;

    const-string v1, "failure_count"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/DbD;->e:LX/0U1;

    .line 2016952
    new-instance v0, LX/0U1;

    const-string v1, "upload_state"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/DbD;->f:LX/0U1;

    .line 2016953
    new-instance v0, LX/0U1;

    const-string v1, "shared"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/DbD;->g:LX/0U1;

    .line 2016954
    new-instance v0, LX/0U1;

    const-string v1, "queue_state"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/DbD;->h:LX/0U1;

    .line 2016955
    new-instance v0, LX/0U1;

    const-string v1, "last_attempt"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/DbD;->i:LX/0U1;

    return-void
.end method
