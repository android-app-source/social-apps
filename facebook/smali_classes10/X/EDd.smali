.class public final LX/EDd;
.super Landroid/telephony/PhoneStateListener;
.source ""


# instance fields
.field public final synthetic a:LX/EDx;


# direct methods
.method public constructor <init>(LX/EDx;)V
    .locals 0

    .prologue
    .line 2091574
    iput-object p1, p0, LX/EDd;->a:LX/EDx;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCallStateChanged(ILjava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 2091575
    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->l:LX/2S7;

    .line 2091576
    iget-wide v8, v0, LX/2S7;->w:J

    move-wide v2, v8

    .line 2091577
    if-ne p1, v1, :cond_1

    .line 2091578
    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    .line 2091579
    iput-boolean v1, v0, LX/EDx;->aQ:Z

    .line 2091580
    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-object v1, v0, LX/EDx;->l:LX/2S7;

    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-wide v4, v0, LX/EDx;->ak:J

    const-string v6, "pstn_call"

    const-string v7, "received"

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 2091581
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/telephony/PhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V

    .line 2091582
    return-void

    .line 2091583
    :cond_1
    if-nez p1, :cond_2

    .line 2091584
    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-boolean v0, v0, LX/EDx;->aQ:Z

    if-eqz v0, :cond_0

    .line 2091585
    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-object v1, v0, LX/EDx;->l:LX/2S7;

    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-wide v4, v0, LX/EDx;->ak:J

    const-string v6, "pstn_call"

    const-string v7, "declined"

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 2091586
    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    const/4 v1, 0x0

    .line 2091587
    iput-boolean v1, v0, LX/EDx;->aQ:Z

    .line 2091588
    goto :goto_0

    .line 2091589
    :cond_2
    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-object v1, v0, LX/EDx;->l:LX/2S7;

    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-wide v4, v0, LX/EDx;->ak:J

    const-string v6, "pstn_call"

    const-string v7, "accepted"

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 2091590
    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-boolean v0, v0, LX/EDx;->bQ:Z

    if-nez v0, :cond_0

    .line 2091591
    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    sget-object v1, LX/7TQ;->CallEndClientInterrupted:LX/7TQ;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/7TQ;)V

    goto :goto_0
.end method

.method public final onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 3

    .prologue
    .line 2091592
    const/4 v1, -0x1

    .line 2091593
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->aU:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 2091594
    :try_start_0
    iget-object v0, p0, LX/EDd;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->aU:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2091595
    :goto_0
    iget-object v1, p0, LX/EDd;->a:LX/EDx;

    iget-object v1, v1, LX/EDx;->l:LX/2S7;

    .line 2091596
    iput v0, v1, LX/2S7;->F:I

    .line 2091597
    return-void

    :catch_0
    :cond_0
    move v0, v1

    goto :goto_0
.end method
