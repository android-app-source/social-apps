.class public LX/CmS;
.super LX/Cm9;
.source ""

# interfaces
.implements LX/Clq;
.implements LX/Clr;
.implements LX/Clu;


# instance fields
.field public final a:I

.field public final b:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentLocationAnnotation;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;


# direct methods
.method public constructor <init>(LX/CmR;)V
    .locals 1

    .prologue
    .line 1933377
    invoke-direct {p0, p1}, LX/Cm9;-><init>(LX/Cm8;)V

    .line 1933378
    iget v0, p1, LX/CmR;->a:I

    iput v0, p0, LX/CmS;->a:I

    .line 1933379
    iget-object v0, p1, LX/CmR;->c:LX/0Px;

    iput-object v0, p0, LX/CmS;->c:LX/0Px;

    .line 1933380
    iget-object v0, p1, LX/CmR;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    iput-object v0, p0, LX/CmS;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    .line 1933381
    iget-object v0, p1, LX/CmR;->d:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, LX/CmS;->d:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1933382
    return-void
.end method


# virtual methods
.method public final d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 1933383
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->MAP:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1

    .prologue
    .line 1933384
    iget-object v0, p0, LX/CmS;->d:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method
