.class public final LX/Dy2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Dy3;


# direct methods
.method public constructor <init>(LX/Dy3;)V
    .locals 0

    .prologue
    .line 2063922
    iput-object p1, p0, LX/Dy2;->a:LX/Dy3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x35381570

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2063923
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063924
    if-eqz v0, :cond_1

    .line 2063925
    iget-object v2, p0, LX/Dy2;->a:LX/Dy3;

    iget-object v2, v2, LX/Dy3;->c:LX/Dy0;

    iget-object v3, p0, LX/Dy2;->a:LX/Dy3;

    iget-object v3, v3, LX/Dy3;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 2063926
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 2063927
    if-nez v3, :cond_0

    .line 2063928
    const-string v6, "privacy_option"

    invoke-static {v5, v6, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2063929
    :cond_0
    iget-object v6, v2, LX/Dy0;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;

    invoke-virtual {v6}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v6

    const/4 p1, -0x1

    invoke-virtual {v6, p1, v5}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2063930
    iget-object v5, v2, LX/Dy0;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;

    invoke-virtual {v5}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    .line 2063931
    iget-object v2, p0, LX/Dy2;->a:LX/Dy3;

    .line 2063932
    iput-object v0, v2, LX/Dy3;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063933
    :cond_1
    const v0, 0x63d7e93c

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
