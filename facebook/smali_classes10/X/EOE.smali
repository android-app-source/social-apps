.class public LX/EOE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1WX;

.field public final b:LX/C3U;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C3U",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/1WX;LX/C3U;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2114160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2114161
    iput-object p1, p0, LX/EOE;->a:LX/1WX;

    .line 2114162
    iput-object p2, p0, LX/EOE;->b:LX/C3U;

    .line 2114163
    iput-object p3, p0, LX/EOE;->c:LX/0ad;

    .line 2114164
    return-void
.end method

.method public static a(LX/0QB;)LX/EOE;
    .locals 6

    .prologue
    .line 2114165
    const-class v1, LX/EOE;

    monitor-enter v1

    .line 2114166
    :try_start_0
    sget-object v0, LX/EOE;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114167
    sput-object v2, LX/EOE;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114168
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114169
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114170
    new-instance p0, LX/EOE;

    invoke-static {v0}, LX/1WX;->a(LX/0QB;)LX/1WX;

    move-result-object v3

    check-cast v3, LX/1WX;

    invoke-static {v0}, LX/C3U;->a(LX/0QB;)LX/C3U;

    move-result-object v4

    check-cast v4, LX/C3U;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/EOE;-><init>(LX/1WX;LX/C3U;LX/0ad;)V

    .line 2114171
    move-object v0, p0

    .line 2114172
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114173
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EOE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114174
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114175
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
