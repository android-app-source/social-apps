.class public LX/Eub;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final b:LX/0TD;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field public e:I


# direct methods
.method public constructor <init>(LX/0TD;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2179750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179751
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179752
    iput-object v0, p0, LX/Eub;->c:LX/0Ot;

    .line 2179753
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179754
    iput-object v0, p0, LX/Eub;->d:LX/0Ot;

    .line 2179755
    const/4 v0, 0x0

    iput v0, p0, LX/Eub;->e:I

    .line 2179756
    iput-object p1, p0, LX/Eub;->b:LX/0TD;

    .line 2179757
    invoke-static {}, LX/Eub;->d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/Eub;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2179758
    return-void
.end method

.method public static a(LX/Eub;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2179759
    new-instance v0, LX/Eua;

    invoke-direct {v0, p0}, LX/Eua;-><init>(LX/Eub;)V

    iget-object v1, p0, LX/Eub;->b:LX/0TD;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/Eub;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;I)LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/lang/String;",
            "I)",
            "LX/0zO",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterSearchFriendsGraphQLModels$FriendsCenterSearchFriendsQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179760
    const-string v0, "You must provide a caller context"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2179761
    new-instance v0, LX/Evi;

    invoke-direct {v0}, LX/Evi;-><init>()V

    move-object v0, v0

    .line 2179762
    const-string v1, "query_param"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after_param"

    iget-object v3, p0, LX/Eub;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first_param"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2179763
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2179764
    iput-object p1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2179765
    move-object v0, v0

    .line 2179766
    return-object v0
.end method

.method public static d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 2

    .prologue
    .line 2179767
    new-instance v0, LX/4a7;

    invoke-direct {v0}, LX/4a7;-><init>()V

    const/4 v1, 0x1

    .line 2179768
    iput-boolean v1, v0, LX/4a7;->b:Z

    .line 2179769
    move-object v0, v0

    .line 2179770
    invoke-virtual {v0}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2179771
    iget-object v0, p0, LX/Eub;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    return v0
.end method
