.class public LX/DdS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public c:J

.field public d:J

.field public e:Z

.field public f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2019519
    const-class v0, LX/DdS;

    sput-object v0, LX/DdS;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 2019520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019521
    iput-wide v0, p0, LX/DdS;->c:J

    .line 2019522
    iput-wide v0, p0, LX/DdS;->d:J

    .line 2019523
    iput-object p1, p0, LX/DdS;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2019524
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 2019525
    iput-wide p1, p0, LX/DdS;->c:J

    .line 2019526
    iget-wide v0, p0, LX/DdS;->d:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 2019527
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/DdS;->d:J

    .line 2019528
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2019529
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DdS;->e:Z

    .line 2019530
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 2019531
    iget-wide v0, p0, LX/DdS;->c:J

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 2019532
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/DdS;->d:J

    .line 2019533
    :goto_0
    return-void

    .line 2019534
    :cond_0
    iput-wide p1, p0, LX/DdS;->d:J

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2019535
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DdS;->e:Z

    .line 2019536
    return-void
.end method
