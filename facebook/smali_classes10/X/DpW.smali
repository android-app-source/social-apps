.class public LX/DpW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final identity_key:[B

.field public final last_resort_key:LX/DpU;

.field public final signed_pre_key_with_id:LX/Dpf;

.field public final suggested_codename:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xc

    const/16 v3, 0xb

    .line 2045721
    new-instance v0, LX/1sv;

    const-string v1, "RegisterPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpW;->b:LX/1sv;

    .line 2045722
    new-instance v0, LX/1sw;

    const-string v1, "suggested_codename"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpW;->c:LX/1sw;

    .line 2045723
    new-instance v0, LX/1sw;

    const-string v1, "identity_key"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpW;->d:LX/1sw;

    .line 2045724
    new-instance v0, LX/1sw;

    const-string v1, "signed_pre_key_with_id"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpW;->e:LX/1sw;

    .line 2045725
    new-instance v0, LX/1sw;

    const-string v1, "last_resort_key"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpW;->f:LX/1sw;

    .line 2045726
    const/4 v0, 0x1

    sput-boolean v0, LX/DpW;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BLX/Dpf;LX/DpU;)V
    .locals 0

    .prologue
    .line 2045727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2045728
    iput-object p1, p0, LX/DpW;->suggested_codename:Ljava/lang/String;

    .line 2045729
    iput-object p2, p0, LX/DpW;->identity_key:[B

    .line 2045730
    iput-object p3, p0, LX/DpW;->signed_pre_key_with_id:LX/Dpf;

    .line 2045731
    iput-object p4, p0, LX/DpW;->last_resort_key:LX/DpU;

    .line 2045732
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2045733
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2045734
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2045735
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2045736
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RegisterPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045737
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045738
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045739
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045740
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045741
    const-string v4, "suggested_codename"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045742
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045743
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045744
    iget-object v4, p0, LX/DpW;->suggested_codename:Ljava/lang/String;

    if-nez v4, :cond_3

    .line 2045745
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045746
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045747
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045748
    const-string v4, "identity_key"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045749
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045750
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045751
    iget-object v4, p0, LX/DpW;->identity_key:[B

    if-nez v4, :cond_4

    .line 2045752
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045753
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045754
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045755
    const-string v4, "signed_pre_key_with_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045756
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045757
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045758
    iget-object v4, p0, LX/DpW;->signed_pre_key_with_id:LX/Dpf;

    if-nez v4, :cond_5

    .line 2045759
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045760
    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045761
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045762
    const-string v4, "last_resort_key"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045763
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045764
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045765
    iget-object v0, p0, LX/DpW;->last_resort_key:LX/DpU;

    if-nez v0, :cond_6

    .line 2045766
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045767
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045768
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045769
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2045770
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2045771
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2045772
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2045773
    :cond_3
    iget-object v4, p0, LX/DpW;->suggested_codename:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2045774
    :cond_4
    iget-object v4, p0, LX/DpW;->identity_key:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2045775
    :cond_5
    iget-object v4, p0, LX/DpW;->signed_pre_key_with_id:LX/Dpf;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2045776
    :cond_6
    iget-object v0, p0, LX/DpW;->last_resort_key:LX/DpU;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2045777
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2045778
    iget-object v0, p0, LX/DpW;->suggested_codename:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2045779
    sget-object v0, LX/DpW;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045780
    iget-object v0, p0, LX/DpW;->suggested_codename:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2045781
    :cond_0
    iget-object v0, p0, LX/DpW;->identity_key:[B

    if-eqz v0, :cond_1

    .line 2045782
    sget-object v0, LX/DpW;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045783
    iget-object v0, p0, LX/DpW;->identity_key:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2045784
    :cond_1
    iget-object v0, p0, LX/DpW;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v0, :cond_2

    .line 2045785
    sget-object v0, LX/DpW;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045786
    iget-object v0, p0, LX/DpW;->signed_pre_key_with_id:LX/Dpf;

    invoke-virtual {v0, p1}, LX/Dpf;->a(LX/1su;)V

    .line 2045787
    :cond_2
    iget-object v0, p0, LX/DpW;->last_resort_key:LX/DpU;

    if-eqz v0, :cond_3

    .line 2045788
    sget-object v0, LX/DpW;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045789
    iget-object v0, p0, LX/DpW;->last_resort_key:LX/DpU;

    invoke-virtual {v0, p1}, LX/DpU;->a(LX/1su;)V

    .line 2045790
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2045791
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2045792
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2045793
    if-nez p1, :cond_1

    .line 2045794
    :cond_0
    :goto_0
    return v0

    .line 2045795
    :cond_1
    instance-of v1, p1, LX/DpW;

    if-eqz v1, :cond_0

    .line 2045796
    check-cast p1, LX/DpW;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2045797
    if-nez p1, :cond_3

    .line 2045798
    :cond_2
    :goto_1
    move v0, v2

    .line 2045799
    goto :goto_0

    .line 2045800
    :cond_3
    iget-object v0, p0, LX/DpW;->suggested_codename:Ljava/lang/String;

    if-eqz v0, :cond_c

    move v0, v1

    .line 2045801
    :goto_2
    iget-object v3, p1, LX/DpW;->suggested_codename:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v3, v1

    .line 2045802
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2045803
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045804
    iget-object v0, p0, LX/DpW;->suggested_codename:Ljava/lang/String;

    iget-object v3, p1, LX/DpW;->suggested_codename:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2045805
    :cond_5
    iget-object v0, p0, LX/DpW;->identity_key:[B

    if-eqz v0, :cond_e

    move v0, v1

    .line 2045806
    :goto_4
    iget-object v3, p1, LX/DpW;->identity_key:[B

    if-eqz v3, :cond_f

    move v3, v1

    .line 2045807
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2045808
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045809
    iget-object v0, p0, LX/DpW;->identity_key:[B

    iget-object v3, p1, LX/DpW;->identity_key:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2045810
    :cond_7
    iget-object v0, p0, LX/DpW;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v0, :cond_10

    move v0, v1

    .line 2045811
    :goto_6
    iget-object v3, p1, LX/DpW;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v3, :cond_11

    move v3, v1

    .line 2045812
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2045813
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045814
    iget-object v0, p0, LX/DpW;->signed_pre_key_with_id:LX/Dpf;

    iget-object v3, p1, LX/DpW;->signed_pre_key_with_id:LX/Dpf;

    invoke-virtual {v0, v3}, LX/Dpf;->a(LX/Dpf;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2045815
    :cond_9
    iget-object v0, p0, LX/DpW;->last_resort_key:LX/DpU;

    if-eqz v0, :cond_12

    move v0, v1

    .line 2045816
    :goto_8
    iget-object v3, p1, LX/DpW;->last_resort_key:LX/DpU;

    if-eqz v3, :cond_13

    move v3, v1

    .line 2045817
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2045818
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045819
    iget-object v0, p0, LX/DpW;->last_resort_key:LX/DpU;

    iget-object v3, p1, LX/DpW;->last_resort_key:LX/DpU;

    invoke-virtual {v0, v3}, LX/DpU;->a(LX/DpU;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 2045820
    goto :goto_1

    :cond_c
    move v0, v2

    .line 2045821
    goto :goto_2

    :cond_d
    move v3, v2

    .line 2045822
    goto :goto_3

    :cond_e
    move v0, v2

    .line 2045823
    goto :goto_4

    :cond_f
    move v3, v2

    .line 2045824
    goto :goto_5

    :cond_10
    move v0, v2

    .line 2045825
    goto :goto_6

    :cond_11
    move v3, v2

    .line 2045826
    goto :goto_7

    :cond_12
    move v0, v2

    .line 2045827
    goto :goto_8

    :cond_13
    move v3, v2

    .line 2045828
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2045829
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2045830
    sget-boolean v0, LX/DpW;->a:Z

    .line 2045831
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpW;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2045832
    return-object v0
.end method
