.class public final LX/DQC;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/groups/info/GroupInfoAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;LX/0Px;)V
    .locals 0

    .prologue
    .line 1994084
    iput-object p1, p0, LX/DQC;->b:Lcom/facebook/groups/info/GroupInfoAdapter;

    iput-object p3, p0, LX/DQC;->a:LX/0Px;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 1994085
    const v2, 0x7f0d153e

    move v0, v2

    .line 1994086
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1994087
    :goto_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    iget-object v3, p0, LX/DQC;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1994088
    new-instance v2, LX/DRU;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/DRU;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 1994089
    :cond_0
    :goto_1
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    iget-object v3, p0, LX/DQC;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 1994090
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto :goto_1

    :cond_1
    move v3, v1

    .line 1994091
    :goto_2
    iget-object v1, p0, LX/DQC;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 1994092
    iget-object v1, p0, LX/DQC;->a:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$GroupTopicTagsModel;

    .line 1994093
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, LX/DRU;

    .line 1994094
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$GroupTopicTagsModel;->k()Ljava/lang/String;

    move-result-object v5

    .line 1994095
    :goto_3
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$GroupTopicTagsModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1994096
    :goto_4
    iget-object v6, p0, LX/DQC;->b:Lcom/facebook/groups/info/GroupInfoAdapter;

    .line 1994097
    iget-object v7, v6, Lcom/facebook/groups/info/GroupInfoAdapter;->e:LX/0ad;

    sget-short v8, LX/88j;->h:S

    const/4 p1, 0x0

    invoke-interface {v7, v8, p1}, LX/0ad;->a(SZ)Z

    move-result v7

    move v6, v7

    .line 1994098
    invoke-virtual {v2, v5, v1, v6}, LX/DRU;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1994099
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_2
    move-object v5, v4

    .line 1994100
    goto :goto_3

    :cond_3
    move-object v1, v4

    .line 1994101
    goto :goto_4

    .line 1994102
    :cond_4
    return-void
.end method
