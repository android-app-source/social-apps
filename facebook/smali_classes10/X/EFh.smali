.class public final synthetic LX/EFh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2096160
    invoke-static {}, LX/EFi;->values()[LX/EFi;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/EFh;->b:[I

    :try_start_0
    sget-object v0, LX/EFh;->b:[I

    sget-object v1, LX/EFi;->ChatHead:LX/EFi;

    invoke-virtual {v1}, LX/EFi;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_15

    :goto_0
    :try_start_1
    sget-object v0, LX/EFh;->b:[I

    sget-object v1, LX/EFi;->Activity:LX/EFi;

    invoke-virtual {v1}, LX/EFi;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_14

    .line 2096161
    :goto_1
    invoke-static {}, LX/7TQ;->values()[LX/7TQ;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/EFh;->a:[I

    :try_start_2
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndNoAnswerTimeout:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_13

    :goto_2
    :try_start_3
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndIgnoreCall:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_12

    :goto_3
    :try_start_4
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndClientInterrupted:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_11

    :goto_4
    :try_start_5
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndNoPermission:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_10

    :goto_5
    :try_start_6
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndIncomingTimeout:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_f

    :goto_6
    :try_start_7
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndOtherNotCapable:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_e

    :goto_7
    :try_start_8
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndOtherCarrierBlocked:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_d

    :goto_8
    :try_start_9
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndSignalingMessageFailed:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_c

    :goto_9
    :try_start_a
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndWebRTCError:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_b

    :goto_a
    :try_start_b
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndClientError:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_a

    :goto_b
    :try_start_c
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndNoUIError:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_9

    :goto_c
    :try_start_d
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndUnsupportedVersion:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_8

    :goto_d
    :try_start_e
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndCallerNotVisible:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_7

    :goto_e
    :try_start_f
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndConnectionDropped:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_6

    :goto_f
    :try_start_10
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndInAnotherCall:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_5

    :goto_10
    :try_start_11
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndCarrierBlocked:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_4

    :goto_11
    :try_start_12
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_3

    :goto_12
    :try_start_13
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndAcceptAfterHangUp:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_2

    :goto_13
    :try_start_14
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndOtherInstanceHandled:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_1

    :goto_14
    :try_start_15
    sget-object v0, LX/EFh;->a:[I

    sget-object v1, LX/7TQ;->CallEndClientEncryptionError:LX/7TQ;

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_0

    :goto_15
    return-void

    :catch_0
    goto :goto_15

    :catch_1
    goto :goto_14

    :catch_2
    goto :goto_13

    :catch_3
    goto :goto_12

    :catch_4
    goto :goto_11

    :catch_5
    goto :goto_10

    :catch_6
    goto :goto_f

    :catch_7
    goto :goto_e

    :catch_8
    goto :goto_d

    :catch_9
    goto :goto_c

    :catch_a
    goto/16 :goto_b

    :catch_b
    goto/16 :goto_a

    :catch_c
    goto/16 :goto_9

    :catch_d
    goto/16 :goto_8

    :catch_e
    goto/16 :goto_7

    :catch_f
    goto/16 :goto_6

    :catch_10
    goto/16 :goto_5

    :catch_11
    goto/16 :goto_4

    :catch_12
    goto/16 :goto_3

    :catch_13
    goto/16 :goto_2

    :catch_14
    goto/16 :goto_1

    :catch_15
    goto/16 :goto_0
.end method
