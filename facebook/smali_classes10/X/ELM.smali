.class public LX/ELM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2108610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)I
    .locals 1

    .prologue
    .line 2108705
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 2108706
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/EJZ;LX/0Uh;)Ljava/lang/CharSequence;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2108707
    invoke-static {p1}, LX/ELM;->a(LX/0Uh;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2108708
    iget-object v0, p0, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;Z)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2108709
    :goto_0
    return-object v0

    .line 2108710
    :cond_0
    iget-object v0, p0, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    .line 2108711
    :goto_1
    const/4 v1, 0x0

    invoke-static {p0, v1}, LX/ELM;->a(LX/EJZ;I)Ljava/lang/String;

    move-result-object v1

    .line 2108712
    if-nez v0, :cond_3

    move-object v0, v1

    .line 2108713
    :cond_1
    :goto_2
    move-object v0, v0

    .line 2108714
    goto :goto_0

    .line 2108715
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2108716
    :cond_3
    if-eqz v1, :cond_1

    .line 2108717
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;)Ljava/lang/CharSequence;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2108718
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    .line 2108719
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l()LX/0Px;

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 2108720
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-le v1, v2, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-static {v0}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2108721
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2108722
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;)Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2108723
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 2108724
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2108725
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;Z)Ljava/lang/CharSequence;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2108726
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 2108727
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l()LX/0Px;

    move-result-object v1

    :goto_0
    move-object v1, v1

    .line 2108728
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-static {v1}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2108729
    :goto_1
    if-eqz p2, :cond_2

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2108730
    :cond_0
    :goto_2
    move-object v0, v1

    .line 2108731
    return-object v0

    :cond_1
    move-object v1, v2

    .line 2108732
    goto :goto_1

    .line 2108733
    :cond_2
    if-eqz p0, :cond_3

    invoke-static {p0, v0}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2108734
    :cond_3
    if-eqz v2, :cond_0

    move-object v1, v2

    .line 2108735
    goto :goto_2

    .line 2108736
    :cond_4
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2108737
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;Z)Ljava/lang/CharSequence;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2108738
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    .line 2108739
    invoke-static {p1}, LX/A0T;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    invoke-static {p0, v0, p2}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;Z)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2108740
    if-nez v1, :cond_0

    .line 2108741
    :goto_0
    return-object v0

    .line 2108742
    :cond_0
    if-nez v0, :cond_1

    move-object v0, v1

    .line 2108743
    goto :goto_0

    .line 2108744
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2108745
    invoke-static {p0}, LX/ELM;->d(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2108746
    :cond_0
    :goto_0
    return-object v0

    .line 2108747
    :sswitch_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->ap()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2108748
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2108749
    :sswitch_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 2108750
    :sswitch_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kY()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2108751
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2108752
    :sswitch_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jG()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2108753
    :sswitch_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->S()LX/0Px;

    move-result-object v1

    .line 2108754
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p1, :cond_0

    invoke-static {p1, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2108755
    :sswitch_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2108756
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3ff252d0 -> :sswitch_4
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_2
        0x1eaef984 -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Ljava/lang/CharSequence;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2108757
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 2108758
    if-eqz v0, :cond_1

    .line 2108759
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 2108760
    invoke-static {v0}, LX/ELM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 2108761
    :cond_0
    :goto_0
    move-object v1, v2

    .line 2108762
    move-object v0, v1

    .line 2108763
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2108764
    :sswitch_0
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->L()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v3

    .line 2108765
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2108766
    :sswitch_1
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->v()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->v()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto :goto_0

    .line 2108767
    :sswitch_2
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->t()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v3

    .line 2108768
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2108769
    :sswitch_3
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2108770
    :sswitch_4
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->c()LX/0Px;

    move-result-object v3

    .line 2108771
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    if-eqz v1, :cond_0

    invoke-static {v1, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3ff252d0 -> :sswitch_4
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(ZLjava/lang/String;LX/0Ot;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 2108772
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/ELM;->a(ZLjava/lang/String;LX/0Ot;Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(ZLjava/lang/String;LX/0Ot;Z)Ljava/lang/CharSequence;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;Z)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 2108773
    if-nez p1, :cond_1

    .line 2108774
    const/4 p1, 0x0

    .line 2108775
    :cond_0
    :goto_0
    return-object p1

    .line 2108776
    :cond_1
    if-eqz p0, :cond_2

    .line 2108777
    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8i7;

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/8i7;->b(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object p1

    goto :goto_0

    .line 2108778
    :cond_2
    if-eqz p3, :cond_0

    .line 2108779
    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8i7;

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2108780
    iget-object p0, v0, LX/8i7;->e:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, p0}, LX/8i7;->a(Landroid/text/SpannableStringBuilder;Landroid/graphics/drawable/Drawable;)V

    .line 2108781
    move-object p1, v1

    .line 2108782
    goto :goto_0
.end method

.method public static a(LX/CzL;I)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2108667
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2108668
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2108669
    :goto_0
    return-object v0

    .line 2108670
    :cond_0
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2108671
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->o()Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2108672
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2108673
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->o()Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2108674
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_1
    move-object v1, v2

    .line 2108675
    :goto_1
    move-object v0, v1

    .line 2108676
    goto :goto_0

    .line 2108677
    :cond_2
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2108678
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2108679
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2108680
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    move-object v0, v1

    .line 2108681
    goto :goto_0

    .line 2108682
    :cond_4
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2108683
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    .line 2108684
    if-nez v0, :cond_5

    move-object v0, v1

    .line 2108685
    goto :goto_0

    .line 2108686
    :cond_5
    iget v2, p0, LX/CzL;->b:I

    move v2, v2

    .line 2108687
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fk_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v2

    .line 2108688
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move-object v0, v1

    .line 2108689
    goto :goto_0

    .line 2108690
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;->c()LX/0Px;

    move-result-object v0

    .line 2108691
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_8

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->a()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v0

    if-nez v0, :cond_9

    :cond_8
    move-object v0, v1

    .line 2108692
    goto/16 :goto_0

    .line 2108693
    :cond_9
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->a()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2108694
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;->a()LX/0Px;

    move-result-object v1

    .line 2108695
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge p1, v1, :cond_b

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_c

    :cond_b
    move v1, v3

    :goto_2
    if-eqz v1, :cond_e

    move-object v1, v2

    .line 2108696
    goto/16 :goto_1

    .line 2108697
    :cond_c
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2108698
    if-nez v1, :cond_d

    move v1, v3

    goto :goto_2

    :cond_d
    move v1, v4

    goto :goto_2

    .line 2108699
    :cond_e
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1
.end method

.method public static a(LX/EJZ;I)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2108700
    iget-object v0, p0, LX/EJZ;->b:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EJZ;->b:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2108701
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EJZ;->b:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2108702
    :goto_1
    return-object v0

    .line 2108703
    :cond_1
    iget-object v0, p0, LX/EJZ;->b:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel;

    .line 2108704
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel$SentenceModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;I)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2108662
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2108663
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2108664
    :goto_1
    return-object v0

    .line 2108665
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->k()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    .line 2108666
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2108655
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 2108656
    const/4 p0, 0x0

    .line 2108657
    :cond_0
    :goto_0
    return-object p0

    .line 2108658
    :cond_1
    if-eqz p1, :cond_0

    .line 2108659
    if-nez p0, :cond_2

    move-object p0, p1

    .line 2108660
    goto :goto_0

    .line 2108661
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(LX/0Uh;)Z
    .locals 2

    .prologue
    .line 2108654
    sget v0, LX/2SU;->aa:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z
    .locals 3
    .param p0    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2108650
    if-nez p0, :cond_1

    .line 2108651
    :cond_0
    :goto_0
    return v0

    .line 2108652
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->name()Ljava/lang/String;

    move-result-object v1

    .line 2108653
    const-string v2, "DISCOVERY_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "_CONSOLIDATED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 2108643
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    .line 2108644
    :goto_0
    const/4 v1, 0x0

    invoke-static {p0, v1}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;I)Ljava/lang/String;

    move-result-object v1

    .line 2108645
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2108646
    :cond_0
    :goto_1
    return-object v0

    .line 2108647
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2108648
    :cond_2
    if-eqz v1, :cond_0

    .line 2108649
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;Z)Ljava/lang/CharSequence;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2108636
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    .line 2108637
    invoke-static {p0, p1, p2}, LX/ELM;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;Z)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2108638
    if-nez v1, :cond_0

    .line 2108639
    :goto_0
    return-object v0

    .line 2108640
    :cond_0
    if-nez v0, :cond_1

    move-object v0, v1

    .line 2108641
    goto :goto_0

    .line 2108642
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Ljava/lang/CharSequence;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2108628
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 2108629
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2108630
    :goto_0
    invoke-static {p0}, LX/ELM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2108631
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2108632
    :cond_0
    :goto_1
    return-object v0

    .line 2108633
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2108634
    :cond_2
    if-eqz v1, :cond_0

    .line 2108635
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/net/Uri;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2108622
    invoke-static {p0}, LX/ELM;->g(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2108623
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2108624
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2108625
    :goto_0
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    .line 2108626
    goto :goto_0

    .line 2108627
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLNode;)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2108620
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 2108621
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLNode;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2108616
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dG()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v0

    .line 2108617
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;->j()LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2108618
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2108619
    goto :goto_0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2108614
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 2108615
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/graphql/model/GraphQLNode;)Z
    .locals 2

    .prologue
    .line 2108612
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 2108613
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x1eaef984

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/graphql/model/GraphQLNode;)Z
    .locals 2

    .prologue
    .line 2108611
    invoke-static {p0}, LX/ELM;->d(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v0

    const v1, 0x41e065f

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dG()Lcom/facebook/graphql/model/GraphQLGroupMembersConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
