.class public LX/DCw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/17W;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1974724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1974725
    iput-object p1, p0, LX/DCw;->a:LX/0Zb;

    .line 1974726
    iput-object p2, p0, LX/DCw;->b:LX/17W;

    .line 1974727
    return-void
.end method

.method public static a(LX/0QB;)LX/DCw;
    .locals 5

    .prologue
    .line 1974728
    const-class v1, LX/DCw;

    monitor-enter v1

    .line 1974729
    :try_start_0
    sget-object v0, LX/DCw;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1974730
    sput-object v2, LX/DCw;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1974731
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1974732
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1974733
    new-instance p0, LX/DCw;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-direct {p0, v3, v4}, LX/DCw;-><init>(LX/0Zb;LX/17W;)V

    .line 1974734
    move-object v0, p0

    .line 1974735
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1974736
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DCw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1974737
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1974738
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
