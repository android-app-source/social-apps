.class public LX/CvZ;
.super LX/1Cd;
.source ""


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/CvY;

.field private final c:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field private final d:LX/CzA;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CvU;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cz3;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;)V
    .locals 1
    .param p3    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/CzA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1948742
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 1948743
    iput-object p1, p0, LX/CvZ;->a:LX/0SG;

    .line 1948744
    iput-object p2, p0, LX/CvZ;->b:LX/CvY;

    .line 1948745
    iput-object p3, p0, LX/CvZ;->c:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1948746
    iput-object p4, p0, LX/CvZ;->d:LX/CzA;

    .line 1948747
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CvZ;->e:Ljava/util/List;

    .line 1948748
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CvZ;->f:Ljava/util/Map;

    .line 1948749
    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1948673
    invoke-static {p0}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 1948674
    if-nez v0, :cond_0

    .line 1948675
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1948676
    :goto_0
    return-object v0

    .line 1948677
    :cond_0
    const v1, -0x5389cb28

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    if-ne v1, v2, :cond_4

    .line 1948678
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1948679
    invoke-static {p0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 1948680
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1948681
    :goto_2
    if-eqz v0, :cond_1

    .line 1948682
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1948683
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1948684
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 1948685
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1948686
    goto :goto_0

    .line 1948687
    :cond_4
    const v1, 0x4ed245b

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    if-eq v1, v2, :cond_5

    const v1, 0x1eaef984

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    if-eq v1, v2, :cond_5

    const v1, -0x4fc08d5

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    if-ne v1, v2, :cond_8

    .line 1948688
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 1948689
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    .line 1948690
    :goto_3
    if-eqz v0, :cond_7

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1948691
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 1948692
    :cond_7
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1948693
    goto :goto_0

    .line 1948694
    :cond_8
    const v1, 0x4c808d5

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-ne v1, v0, :cond_a

    .line 1948695
    invoke-static {p0}, LX/8eM;->k(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1948696
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 1948697
    :cond_9
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1948698
    goto/16 :goto_0

    .line 1948699
    :cond_a
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1948700
    invoke-static {p0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v4, :cond_d

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 1948701
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v5

    if-eqz v5, :cond_c

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    .line 1948702
    :goto_5
    if-eqz v0, :cond_b

    .line 1948703
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1948704
    :cond_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1948705
    :cond_c
    const/4 v0, 0x0

    goto :goto_5

    .line 1948706
    :cond_d
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1948707
    goto/16 :goto_0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1948757
    iget-object v0, p0, LX/CvZ;->e:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1948758
    iget-object v1, p0, LX/CvZ;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1948759
    iget-object v1, p0, LX/CvZ;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1948760
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1948761
    iget-object v1, p0, LX/CvZ;->b:LX/CvY;

    iget-object v2, p0, LX/CvZ;->c:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v1, v2, v0}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;)V

    .line 1948762
    :cond_0
    return-void
.end method

.method private static b(LX/Cz3;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cz3;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1948736
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 1948737
    invoke-virtual {p0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    .line 1948738
    invoke-virtual {p0}, LX/Cz3;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1948739
    :goto_0
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    .line 1948740
    :sswitch_0
    const-string v2, "media_combined_module_number_of_annotated_results"

    invoke-static {v1}, LX/Cvf;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1948741
    :sswitch_1
    const-string v2, "thumbnail_decorations"

    invoke-static {v1}, LX/Cvh;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x5852ec13 -> :sswitch_0
        -0x5389cb28 -> :sswitch_1
        -0x30cf2d19 -> :sswitch_1
        0x4ed245b -> :sswitch_1
        0xd5fee8a -> :sswitch_1
        0x735086f1 -> :sswitch_1
    .end sparse-switch
.end method

.method private static c(Ljava/lang/Object;)LX/Cz3;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1948750
    instance-of v0, p0, LX/1Rk;

    if-eqz v0, :cond_0

    .line 1948751
    check-cast p0, LX/1Rk;

    .line 1948752
    iget-object v0, p0, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 1948753
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 1948754
    instance-of v1, v0, LX/Cz3;

    if-eqz v1, :cond_0

    .line 1948755
    check-cast v0, LX/Cz3;

    .line 1948756
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1948732
    invoke-static {p1}, LX/CvZ;->c(Ljava/lang/Object;)LX/Cz3;

    move-result-object v0

    .line 1948733
    if-nez v0, :cond_0

    .line 1948734
    :goto_0
    return-void

    .line 1948735
    :cond_0
    iget-object v1, p0, LX/CvZ;->f:Ljava/util/Map;

    iget-object v2, p0, LX/CvZ;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(LX/0g8;)V
    .locals 0

    .prologue
    .line 1948730
    invoke-direct {p0}, LX/CvZ;->a()V

    .line 1948731
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 14

    .prologue
    const/4 v11, 0x0

    .line 1948708
    invoke-static {p1}, LX/CvZ;->c(Ljava/lang/Object;)LX/Cz3;

    move-result-object v13

    .line 1948709
    const/4 v0, 0x0

    .line 1948710
    if-nez v13, :cond_6

    .line 1948711
    :cond_0
    :goto_0
    move v0, v0

    .line 1948712
    if-nez v0, :cond_2

    .line 1948713
    :cond_1
    :goto_1
    return-void

    .line 1948714
    :cond_2
    iget-object v0, p0, LX/CvZ;->f:Ljava/util/Map;

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1948715
    if-eqz v0, :cond_1

    .line 1948716
    iget-object v1, p0, LX/CvZ;->f:Ljava/util/Map;

    invoke-interface {v1, v13}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948717
    iget-object v1, p0, LX/CvZ;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v6, v2, v4

    .line 1948718
    const-wide/16 v2, 0xa

    cmp-long v1, v6, v2

    if-ltz v1, :cond_1

    .line 1948719
    invoke-virtual {v13}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v1}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4c808d5

    if-ne v1, v2, :cond_5

    .line 1948720
    invoke-virtual {v13}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->k()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v9

    .line 1948721
    invoke-virtual {v13}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v10

    .line 1948722
    :goto_2
    if-eqz v9, :cond_3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v9, v1, :cond_4

    .line 1948723
    :cond_3
    invoke-virtual {v13}, LX/Cz3;->c()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v9

    .line 1948724
    :cond_4
    new-instance v1, LX/CvU;

    iget-object v2, p0, LX/CvZ;->d:LX/CzA;

    invoke-virtual {v2, v13}, LX/CzA;->a(LX/Cyv;)I

    move-result v2

    invoke-virtual {v13}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v13}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-static {v0}, LX/CvZ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v8

    invoke-virtual {v13}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-static {v0}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    invoke-static {v13}, LX/CvZ;->b(LX/Cz3;)LX/0P1;

    move-result-object v13

    invoke-direct/range {v1 .. v13}, LX/CvU;-><init>(ILjava/lang/String;JJLX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0P1;)V

    .line 1948725
    iget-object v0, p0, LX/CvZ;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1948726
    iget-object v0, p0, LX/CvZ;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_1

    .line 1948727
    invoke-direct {p0}, LX/CvZ;->a()V

    goto/16 :goto_1

    :cond_5
    move-object v10, v11

    move-object v9, v11

    goto :goto_2

    .line 1948728
    :cond_6
    invoke-virtual {v13}, LX/Cz3;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    .line 1948729
    const v2, -0x59a2740a

    if-eq v1, v2, :cond_0

    const v2, -0x49f04592

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method
