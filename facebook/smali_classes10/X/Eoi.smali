.class public final LX/Eoi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Eoj;


# direct methods
.method public constructor <init>(LX/Eoj;)V
    .locals 0

    .prologue
    .line 2168400
    iput-object p1, p0, LX/Eoi;->a:LX/Eoj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x267cb6ae

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2168401
    iget-object v0, p0, LX/Eoi;->a:LX/Eoj;

    .line 2168402
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v1

    move-object v7, v1

    .line 2168403
    invoke-virtual {v7}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2168404
    const v0, -0x581a1e55

    invoke-static {v2, v2, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2168405
    :goto_0
    return-void

    .line 2168406
    :cond_0
    iget-object v0, p0, LX/Eoi;->a:LX/Eoj;

    iget-object v0, v0, LX/Eoj;->b:LX/Emj;

    sget-object v1, LX/Emo;->DEFAULT_ACTION:LX/Emo;

    iget-object v2, p0, LX/Eoi;->a:LX/Eoj;

    iget-object v2, v2, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    iget-object v5, p0, LX/Eoi;->a:LX/Eoj;

    iget-object v5, v5, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-interface/range {v0 .. v5}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2168407
    iget-object v0, p0, LX/Eoi;->a:LX/Eoj;

    iget-object v0, v0, LX/Eoj;->b:LX/Emj;

    iget-object v1, p0, LX/Eoi;->a:LX/Eoj;

    iget-object v1, v1, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-virtual {v1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Emj;->a(Ljava/lang/String;)V

    .line 2168408
    iget-object v0, p0, LX/Eoi;->a:LX/Eoj;

    iget-object v0, v0, LX/Eoj;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EoZ;

    invoke-virtual {v7}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ep9;

    invoke-virtual {v1}, LX/Ep9;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Eoi;->a:LX/Eoj;

    iget-object v2, v2, LX/Eoj;->a:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    invoke-virtual {v0, v1, v2}, LX/EoZ;->a(Landroid/content/Context;LX/Eon;)V

    .line 2168409
    const v0, -0x6e82695e    # -2.0004598E-28f

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto :goto_0
.end method
