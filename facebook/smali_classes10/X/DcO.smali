.class public final LX/DcO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;

.field public final synthetic b:LX/DcP;


# direct methods
.method public constructor <init>(LX/DcP;Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;)V
    .locals 0

    .prologue
    .line 2018302
    iput-object p1, p0, LX/DcO;->b:LX/DcP;

    iput-object p2, p0, LX/DcO;->a:Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2018303
    iget-object v0, p0, LX/DcO;->a:Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;

    .line 2018304
    invoke-static {v0}, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->e(Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;)V

    .line 2018305
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2018306
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2018307
    iget-object v1, p0, LX/DcO;->a:Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2018308
    if-nez v0, :cond_1

    move v2, v3

    :goto_1
    if-eqz v2, :cond_3

    move v2, v3

    :goto_2
    if-eqz v2, :cond_5

    .line 2018309
    invoke-static {v1}, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->e(Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;)V

    .line 2018310
    :goto_3
    return-void

    .line 2018311
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2018312
    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel;

    goto :goto_0

    .line 2018313
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2018314
    if-nez v2, :cond_2

    move v2, v3

    goto :goto_1

    :cond_2
    move v2, v4

    goto :goto_1

    .line 2018315
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel;->a()LX/1vs;

    move-result-object v2

    iget-object p0, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class p1, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel$PageProductListsModel$NodesModel;

    invoke-virtual {p0, v2, v4, p1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v2

    .line 2018316
    if-eqz v2, :cond_4

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    :goto_4
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    goto :goto_2

    .line 2018317
    :cond_4
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2018318
    goto :goto_4

    .line 2018319
    :cond_5
    iget-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v2, v4}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2018320
    iget-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 2018321
    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel;->a()LX/1vs;

    move-result-object v2

    iget-object p0, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class p1, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel$PageProductListsModel$NodesModel;

    invoke-virtual {p0, v2, v4, p1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    :goto_5
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-le v2, v3, :cond_8

    .line 2018322
    :goto_6
    if-eqz v3, :cond_6

    .line 2018323
    iget-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->k:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2018324
    :cond_6
    iget-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->l:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v4}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 2018325
    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel;->a()LX/1vs;

    move-result-object v2

    iget-object p0, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class p1, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$AvailableMenusQueryModel$PageProductListsModel$NodesModel;

    invoke-virtual {p0, v2, v4, p1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v2

    .line 2018326
    iget-object v4, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->d:LX/DcR;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object p0

    if-eqz v2, :cond_9

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2018327
    :goto_7
    iget-boolean p1, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->i:Z

    if-eqz p1, :cond_b

    .line 2018328
    if-eqz v3, :cond_a

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0b1c62

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 2018329
    :goto_8
    move v3, p1

    .line 2018330
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2018331
    new-instance v0, LX/DcQ;

    invoke-static {v4}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-direct {v0, p1, p0, v2, v3}, LX/DcQ;-><init>(LX/03V;LX/0gc;LX/0Px;Ljava/lang/Integer;)V

    .line 2018332
    move-object v2, v0

    .line 2018333
    iput-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->f:LX/DcQ;

    .line 2018334
    iget-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->l:Landroid/support/v4/view/ViewPager;

    iget-object v3, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->f:LX/DcQ;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2018335
    iget-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->k:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v3, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->l:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2018336
    iget-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;->l:Landroid/support/v4/view/ViewPager;

    new-instance v3, LX/DcS;

    invoke-direct {v3, v1}, LX/DcS;-><init>(Lcom/facebook/localcontent/menus/structured/StructuredMenuTabPagerFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    goto/16 :goto_3

    .line 2018337
    :cond_7
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2018338
    goto :goto_5

    :cond_8
    move v3, v4

    goto :goto_6

    .line 2018339
    :cond_9
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2018340
    goto :goto_7

    .line 2018341
    :cond_a
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0b1c61

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    goto :goto_8

    .line 2018342
    :cond_b
    if-eqz v3, :cond_c

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0b1c63

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    goto :goto_8

    :cond_c
    const/4 p1, 0x0

    goto :goto_8
.end method
