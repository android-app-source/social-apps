.class public LX/EkW;
.super LX/EkV;
.source ""


# direct methods
.method public constructor <init>(LX/1Se;LX/Ekf;[Ljava/lang/Object;LX/EkZ;)V
    .locals 0
    .param p4    # LX/EkZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2163573
    invoke-direct {p0, p1, p2, p3, p4}, LX/EkV;-><init>(LX/1Se;LX/Ekf;[Ljava/lang/Object;LX/EkZ;)V

    .line 2163574
    return-void
.end method

.method private static a(LX/EkU;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;J)J
    .locals 4

    .prologue
    .line 2163575
    invoke-virtual {p0, p1, p2}, LX/EkU;->a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2163576
    new-instance v2, Ljava/util/ArrayList;

    array-length v1, p2

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2163577
    array-length v3, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object p0, p2, v1

    .line 2163578
    if-eqz p0, :cond_0

    .line 2163579
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2163580
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2163581
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move-object v1, v1

    .line 2163582
    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {v0, p3, p4}, LX/EkS;->a(Landroid/database/Cursor;J)J

    move-result-wide v0

    .line 2163583
    return-wide v0
.end method


# virtual methods
.method public final a()J
    .locals 10

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x0

    .line 2163584
    iget-object v0, p0, LX/EkV;->b:LX/1Se;

    invoke-interface {v0}, LX/1Se;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2163585
    iget-object v2, p0, LX/EkV;->f:[Ljava/lang/String;

    array-length v2, v2

    new-array v2, v2, [Ljava/lang/String;

    .line 2163586
    iget-object v3, p0, LX/EkV;->a:[Ljava/lang/Object;

    iget-object v4, p0, LX/EkV;->g:[I

    .line 2163587
    const/4 v5, 0x0

    array-length v9, v2

    move v8, v5

    :goto_0
    if-ge v8, v9, :cond_1

    .line 2163588
    aget v5, v4, v8

    aget-object v5, v3, v5

    .line 2163589
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    aput-object v5, v2, v8

    .line 2163590
    add-int/lit8 v5, v8, 0x1

    move v8, v5

    goto :goto_0

    .line 2163591
    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    .line 2163592
    :cond_1
    move-object v2, v2

    .line 2163593
    iget-object v3, p0, LX/EkV;->c:LX/EkU;

    invoke-static {v3, v0, v2, v6, v7}, LX/EkW;->a(LX/EkU;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;J)J

    move-result-wide v2

    .line 2163594
    cmp-long v0, v2, v6

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v4, v0

    .line 2163595
    :goto_2
    if-eqz v4, :cond_3

    .line 2163596
    iget-object v0, p0, LX/EkV;->c:LX/EkU;

    iget-object v5, p0, LX/EkV;->b:LX/1Se;

    invoke-interface {v5}, LX/1Se;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/EkU;->a(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 2163597
    :goto_3
    iget-object v5, p0, LX/EkV;->a:[Ljava/lang/Object;

    array-length v5, v5

    .line 2163598
    :goto_4
    if-ge v1, v5, :cond_4

    .line 2163599
    add-int/lit8 v6, v1, 0x1

    iget-object v7, p0, LX/EkV;->a:[Ljava/lang/Object;

    aget-object v7, v7, v1

    invoke-static {v0, v6, v7}, Landroid/database/DatabaseUtils;->bindObjectToProgram(Landroid/database/sqlite/SQLiteProgram;ILjava/lang/Object;)V

    .line 2163600
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_2
    move v4, v1

    .line 2163601
    goto :goto_2

    .line 2163602
    :cond_3
    iget-object v0, p0, LX/EkV;->c:LX/EkU;

    iget-object v5, p0, LX/EkV;->b:LX/1Se;

    invoke-interface {v5}, LX/1Se;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/EkU;->b(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 2163603
    iget-object v5, p0, LX/EkV;->d:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0, v5, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    goto :goto_3

    .line 2163604
    :cond_4
    if-eqz v4, :cond_6

    .line 2163605
    const v1, -0x7983f1d8

    :try_start_0
    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    const v1, 0x762c8de

    invoke-static {v1}, LX/03h;->a(I)V

    .line 2163606
    :goto_5
    iget-object v1, p0, LX/EkV;->j:LX/EkZ;

    if-eqz v1, :cond_5

    .line 2163607
    iget-object v1, p0, LX/EkV;->j:LX/EkZ;

    .line 2163608
    iget-object v4, v1, LX/EkZ;->a:LX/Ekd;

    iget-object v4, v4, LX/Ekd;->e:LX/14r;

    iget v5, v4, LX/14r;->a:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, LX/14r;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2163609
    :cond_5
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    return-wide v2

    .line 2163610
    :cond_6
    const v1, 0x55086dcf

    :try_start_1
    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    const v1, 0x6e3414a3

    invoke-static {v1}, LX/03h;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 2163611
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    throw v1
.end method
