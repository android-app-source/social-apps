.class public final LX/D4A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7zh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7zh",
        "<",
        "LX/D5z;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D4G;


# direct methods
.method public constructor <init>(LX/D4G;)V
    .locals 0

    .prologue
    .line 1961875
    iput-object p1, p0, LX/D4A;->a:LX/D4G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1961876
    check-cast p1, LX/D5z;

    check-cast p2, LX/D5z;

    .line 1961877
    if-eqz p1, :cond_0

    .line 1961878
    iget-object v0, p0, LX/D4A;->a:LX/D4G;

    .line 1961879
    const/4 v1, 0x0

    iput-object v1, v0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    .line 1961880
    invoke-virtual {p1}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    iget-object v2, v0, LX/D4G;->b:LX/D4F;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2oa;)V

    .line 1961881
    invoke-virtual {p1}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    iget-object v2, v0, LX/D4G;->c:LX/D4E;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2oa;)V

    .line 1961882
    iget-object v1, v0, LX/D4G;->b:LX/D4F;

    .line 1961883
    const/4 v2, 0x0

    iput-object v2, v1, LX/D4F;->b:LX/2qV;

    .line 1961884
    :cond_0
    if-eqz p2, :cond_1

    .line 1961885
    iget-object v0, p0, LX/D4A;->a:LX/D4G;

    .line 1961886
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    .line 1961887
    invoke-virtual {p2}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    iget-object v2, v0, LX/D4G;->b:LX/D4F;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    .line 1961888
    invoke-virtual {p2}, LX/D5z;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    iget-object v2, v0, LX/D4G;->c:LX/D4E;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    .line 1961889
    :cond_1
    return-void
.end method
