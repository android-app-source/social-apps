.class public final LX/D88;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/68J;


# instance fields
.field public final synthetic a:LX/D89;


# direct methods
.method public constructor <init>(LX/D89;)V
    .locals 0

    .prologue
    .line 1968010
    iput-object p1, p0, LX/D88;->a:LX/D89;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/680;)V
    .locals 6

    .prologue
    .line 1968004
    iget-object v0, p0, LX/D88;->a:LX/D89;

    iget-object v0, v0, LX/D89;->a:LX/D8B;

    iget-object v0, v0, LX/D8B;->p:LX/D0y;

    iget-object v1, p0, LX/D88;->a:LX/D89;

    iget-object v1, v1, LX/D89;->a:LX/D8B;

    iget-object v1, v1, LX/D8B;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v1}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->f()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/D0y;->c(Ljava/util/Map;)V

    .line 1968005
    invoke-virtual {p1}, LX/680;->r()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1968006
    invoke-virtual {p1}, LX/680;->e()Landroid/location/Location;

    move-result-object v0

    .line 1968007
    if-eqz v0, :cond_0

    .line 1968008
    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    const/high16 v0, 0x41700000    # 15.0f

    invoke-static {v1, v0}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/67d;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/680;->a(LX/67d;)V

    .line 1968009
    :cond_0
    return-void
.end method
