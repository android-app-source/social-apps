.class public LX/EVk;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;
.implements LX/1a7;


# instance fields
.field public a:Z

.field private b:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2128914
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EVk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128915
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2128916
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128917
    const v0, 0x7f0315ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2128918
    const v0, 0x7f0d3124

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/EVk;->b:Landroid/widget/FrameLayout;

    .line 2128919
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2128920
    iget-boolean v0, p0, LX/EVk;->a:Z

    return v0
.end method

.method public final addView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2128921
    iget-object v0, p0, LX/EVk;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2128922
    return-void
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 2128923
    const/4 v0, 0x0

    return v0
.end method

.method public getContainer()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 2128924
    iget-object v0, p0, LX/EVk;->b:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x64e2cf7c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128925
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2128926
    const/4 v1, 0x1

    .line 2128927
    iput-boolean v1, p0, LX/EVk;->a:Z

    .line 2128928
    const/16 v1, 0x2d

    const v2, 0x1f6277ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5a477ff7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2128929
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2128930
    const/4 v1, 0x0

    .line 2128931
    iput-boolean v1, p0, LX/EVk;->a:Z

    .line 2128932
    const/16 v1, 0x2d

    const v2, -0x579111f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final removeAllViews()V
    .locals 1

    .prologue
    .line 2128933
    iget-object v0, p0, LX/EVk;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2128934
    return-void
.end method
