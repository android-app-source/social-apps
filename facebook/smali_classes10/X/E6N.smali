.class public LX/E6N;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/2d1;

.field public final c:LX/17W;

.field public final d:LX/CeT;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0SG;

.field public final h:Landroid/support/v4/app/Fragment;

.field public final i:LX/2jY;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;LX/2jY;Landroid/content/Context;LX/2d1;LX/17W;LX/CeT;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0SG;)V
    .locals 0
    .param p1    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "LX/2jY;",
            "Landroid/content/Context;",
            "LX/2d1;",
            "LX/17W;",
            "LX/CeT;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2080058
    iput-object p3, p0, LX/E6N;->a:Landroid/content/Context;

    .line 2080059
    iput-object p4, p0, LX/E6N;->b:LX/2d1;

    .line 2080060
    iput-object p5, p0, LX/E6N;->c:LX/17W;

    .line 2080061
    iput-object p6, p0, LX/E6N;->d:LX/CeT;

    .line 2080062
    iput-object p7, p0, LX/E6N;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2080063
    iput-object p8, p0, LX/E6N;->f:LX/0Ot;

    .line 2080064
    iput-object p9, p0, LX/E6N;->g:LX/0SG;

    .line 2080065
    iput-object p2, p0, LX/E6N;->i:LX/2jY;

    .line 2080066
    iput-object p1, p0, LX/E6N;->h:Landroid/support/v4/app/Fragment;

    .line 2080067
    return-void
.end method

.method public static c(LX/E6N;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2080068
    iget-object v0, p0, LX/E6N;->i:LX/2jY;

    .line 2080069
    iget-object v1, v0, LX/2jY;->x:Landroid/os/Bundle;

    move-object v0, v1

    .line 2080070
    if-eqz v0, :cond_0

    .line 2080071
    iget-object v0, p0, LX/E6N;->i:LX/2jY;

    .line 2080072
    iget-object v1, v0, LX/2jY;->x:Landroid/os/Bundle;

    move-object v0, v1

    .line 2080073
    const-string v1, "place_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2080074
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/E6N;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2080075
    iget-object v0, p0, LX/E6N;->i:LX/2jY;

    .line 2080076
    iget-object v1, v0, LX/2jY;->x:Landroid/os/Bundle;

    move-object v0, v1

    .line 2080077
    if-eqz v0, :cond_0

    .line 2080078
    iget-object v0, p0, LX/E6N;->i:LX/2jY;

    .line 2080079
    iget-object v1, v0, LX/2jY;->x:Landroid/os/Bundle;

    move-object v0, v1

    .line 2080080
    const-string v1, "place_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2080081
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/E6N;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2080082
    iget-object v0, p0, LX/E6N;->d:LX/CeT;

    invoke-virtual {v0}, LX/CeT;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2080083
    iget-object v1, p0, LX/E6N;->b:LX/2d1;

    .line 2080084
    iget-object v2, v1, LX/2d1;->a:LX/0Uh;

    const/16 v3, 0x4e9

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 2080085
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/E6N;->i:LX/2jY;

    .line 2080086
    iget-object v2, v1, LX/2jY;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2080087
    invoke-static {v1}, LX/2s8;->g(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2080088
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, LX/E6N;->e(LX/E6N;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, LX/E6N;->c(LX/E6N;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, LX/E6N;->d(LX/E6N;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
