.class public LX/CyL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/CyM;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/graphql/model/GraphQLVideo;

.field private final f:LX/1VK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1952643
    const-class v0, LX/CyL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CyL;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;ILX/1VK;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "I",
            "LX/1VK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1952645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952646
    iput-object p1, p0, LX/CyL;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1952647
    iput-object p2, p0, LX/CyL;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1952648
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/CyL;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/CyL;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1952649
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1952650
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CyL;->b:Ljava/lang/String;

    .line 1952651
    iput p3, p0, LX/CyL;->c:I

    .line 1952652
    iput-object p4, p0, LX/CyL;->f:LX/1VK;

    .line 1952653
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1952654
    iget-object v0, p0, LX/CyL;->f:LX/1VK;

    iget-object v1, p0, LX/CyL;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/CyL;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    iget v3, p0, LX/CyL;->c:I

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1VK;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;Ljava/lang/Integer;)LX/2oO;

    move-result-object v0

    .line 1952655
    new-instance v1, LX/CyM;

    iget-object v2, p0, LX/CyL;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/CyM;-><init>(Ljava/lang/String;LX/2oO;)V

    .line 1952656
    return-object v1
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1952644
    iget-object v0, p0, LX/CyL;->b:Ljava/lang/String;

    return-object v0
.end method
