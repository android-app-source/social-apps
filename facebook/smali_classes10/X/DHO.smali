.class public LX/DHO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DHP;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DHO",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DHP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981818
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1981819
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DHO;->b:LX/0Zi;

    .line 1981820
    iput-object p1, p0, LX/DHO;->a:LX/0Ot;

    .line 1981821
    return-void
.end method

.method public static a(LX/0QB;)LX/DHO;
    .locals 4

    .prologue
    .line 1981822
    const-class v1, LX/DHO;

    monitor-enter v1

    .line 1981823
    :try_start_0
    sget-object v0, LX/DHO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981824
    sput-object v2, LX/DHO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981825
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981826
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981827
    new-instance v3, LX/DHO;

    const/16 p0, 0x21cf

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DHO;-><init>(LX/0Ot;)V

    .line 1981828
    move-object v0, v3

    .line 1981829
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981830
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DHO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981831
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981832
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1981833
    check-cast p2, LX/DHN;

    .line 1981834
    iget-object v0, p0, LX/DHO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DHP;

    iget-object v1, p2, LX/DHN;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/DHN;->b:LX/1Pe;

    const/4 p2, 0x2

    const/4 v4, 0x0

    const/high16 p0, 0x3f800000    # 1.0f

    .line 1981835
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {v0, p1, v1, v4, v2}, LX/DHP;->a(LX/DHP;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/1Pe;)LX/1Di;

    move-result-object v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-interface {v4, v5}, LX/1Di;->c(F)LX/1Di;

    move-result-object v4

    const/4 v5, 0x3

    const v6, 0x7f0b0064

    invoke-interface {v4, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/16 v5, 0x10e

    invoke-interface {v4, v5}, LX/1Di;->v(I)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x40400000    # 3.0f

    invoke-interface {v4, v5}, LX/1Dh;->f(F)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, p1, v1, v5, v2}, LX/DHP;->a(LX/DHP;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/1Pe;)LX/1Di;

    move-result-object v5

    const/4 v6, 0x5

    const v7, 0x7f0b0064

    invoke-interface {v5, v6, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {v0, p1, v1, p2, v2}, LX/DHP;->a(LX/DHP;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/1Pe;)LX/1Di;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/16 v5, 0xb4

    invoke-interface {v4, v5}, LX/1Dh;->O(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1981836
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1981837
    invoke-static {}, LX/1dS;->b()V

    .line 1981838
    const/4 v0, 0x0

    return-object v0
.end method
