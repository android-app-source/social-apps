.class public final LX/DmG;
.super LX/DmF;
.source ""


# instance fields
.field public final synthetic l:LX/DmQ;


# direct methods
.method public constructor <init>(LX/DmQ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2039079
    iput-object p1, p0, LX/DmG;->l:LX/DmQ;

    invoke-direct {p0, p2}, LX/DmF;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2039080
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 2039081
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;

    move-result-object v4

    .line 2039082
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->q()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->q()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$UserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$UserModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 2039083
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 2039084
    :goto_1
    iget-object v2, p0, LX/DmG;->l:LX/DmQ;

    iget-object v2, v2, LX/DmQ;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f082ba2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2039085
    if-eqz v1, :cond_0

    invoke-virtual {v4}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2039086
    :cond_0
    iput-object v3, p0, LX/DmF;->l:Ljava/lang/String;

    .line 2039087
    if-eqz v1, :cond_1

    .line 2039088
    iput-object v4, p0, LX/DmF;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;

    .line 2039089
    :cond_1
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->q()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$UserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$UserModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2039090
    :goto_2
    iput-object v0, p0, LX/DmF;->o:Ljava/lang/String;

    .line 2039091
    iget-object v0, p0, LX/DmG;->l:LX/DmQ;

    iget-object v0, v0, LX/DmQ;->h:LX/DnT;

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->m()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, LX/DnT;->d(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, LX/DmE;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039092
    return-void

    :cond_2
    move v0, v2

    .line 2039093
    goto :goto_0

    :cond_3
    move v1, v2

    .line 2039094
    goto :goto_1

    .line 2039095
    :cond_4
    const-string v0, ""

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;Ljava/lang/String;Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;Ljava/lang/String;)V
    .locals 3
    .param p3    # Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2039096
    iget-object v0, p0, LX/DmG;->l:LX/DmQ;

    iget-object v0, v0, LX/DmQ;->f:LX/0Uh;

    const/16 v1, 0x61e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2039097
    iget-object v0, p0, LX/DmG;->l:LX/DmQ;

    .line 2039098
    new-instance v1, LX/3Af;

    iget-object v2, v0, LX/DmQ;->g:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2039099
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/3Af;->a(I)V

    .line 2039100
    new-instance v2, LX/7TY;

    iget-object p0, v0, LX/DmQ;->g:Landroid/content/Context;

    invoke-direct {v2, p0}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2039101
    const p0, 0x7f082b95

    invoke-virtual {v2, p0}, LX/34c;->e(I)LX/3Ai;

    move-result-object p0

    const p1, 0x7f02085b

    invoke-virtual {p0, p1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object p0

    new-instance p1, LX/DmK;

    invoke-direct {p1, v0, p2, p3}, LX/DmK;-><init>(LX/DmQ;Ljava/lang/String;Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2039102
    const p0, 0x7f082b96

    invoke-virtual {v2, p0}, LX/34c;->e(I)LX/3Ai;

    move-result-object p0

    const p1, 0x7f020818

    invoke-virtual {p0, p1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object p0

    new-instance p1, LX/DmL;

    invoke-direct {p1, v0, p4, p2}, LX/DmL;-><init>(LX/DmQ;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2039103
    move-object v2, v2

    .line 2039104
    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2039105
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 2039106
    :cond_0
    :goto_0
    return-void

    .line 2039107
    :cond_1
    iget-object v0, p0, LX/DmG;->l:LX/DmQ;

    iget-object v0, v0, LX/DmQ;->i:LX/Did;

    if-eqz v0, :cond_0

    .line 2039108
    iget-object v0, p0, LX/DmG;->l:LX/DmQ;

    invoke-static {v0, p2, p3}, LX/DmQ;->a$redex0(LX/DmQ;Ljava/lang/String;Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;)Landroid/content/Intent;

    move-result-object v0

    .line 2039109
    iget-object v1, p0, LX/DmG;->l:LX/DmQ;

    iget-object v1, v1, LX/DmQ;->i:LX/Did;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/Did;->a(Landroid/content/Intent;I)V

    goto :goto_0
.end method
