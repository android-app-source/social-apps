.class public final LX/D2A;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/1FJ",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D1w;

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:LX/D2B;


# direct methods
.method public constructor <init>(LX/D2B;LX/D1w;II)V
    .locals 0

    .prologue
    .line 1958209
    iput-object p1, p0, LX/D2A;->d:LX/D2B;

    iput-object p2, p0, LX/D2A;->a:LX/D1w;

    iput p3, p0, LX/D2A;->b:I

    iput p4, p0, LX/D2A;->c:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1958210
    const-class v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    const-string v1, "Failed to extract bitmaps %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1958211
    iget-object v0, p0, LX/D2A;->a:LX/D1w;

    iget v1, p0, LX/D2A;->b:I

    iget v2, p0, LX/D2A;->c:I

    .line 1958212
    iget-object v3, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-static {v3}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->g(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)I

    .line 1958213
    iget-object v3, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget v3, v3, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->l:I

    const/16 v4, 0xa

    if-ge v3, v4, :cond_0

    .line 1958214
    iget-object v3, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v3, v3, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b:LX/D2B;

    iget-object v4, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v4, v4, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->t:LX/D1w;

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-virtual {v3, v1, v4, p0, v2}, LX/D2B;->a(ILX/D1w;FI)V

    .line 1958215
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1958216
    check-cast p1, LX/1FJ;

    .line 1958217
    if-eqz p1, :cond_0

    .line 1958218
    iget-object v0, p0, LX/D2A;->a:LX/D1w;

    iget v1, p0, LX/D2A;->b:I

    const/4 v5, 0x0

    .line 1958219
    iget-object v2, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    const/4 v3, 0x0

    .line 1958220
    iput v3, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->l:I

    .line 1958221
    iget-object v2, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1958222
    :cond_0
    :goto_0
    return-void

    .line 1958223
    :cond_1
    iget-object v2, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->q:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1958224
    iget-object v2, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->h:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    .line 1958225
    iget-object v2, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    .line 1958226
    if-le v4, v1, :cond_0

    .line 1958227
    iget-object v2, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1958228
    const/4 v3, 0x0

    .line 1958229
    if-nez v1, :cond_2

    .line 1958230
    iget-object v3, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget v3, v3, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->o:F

    iget-object v4, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget v4, v4, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->o:F

    invoke-static {v3, v5, v5, v4}, LX/4Ab;->b(FFFF)LX/4Ab;

    move-result-object v3

    move-object v4, v3

    .line 1958231
    :goto_1
    iget-object v3, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, LX/1Uo;->a(Landroid/content/res/Resources;)LX/1Uo;

    move-result-object v5

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-direct {v6, p0, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    sget-object v3, LX/1Up;->g:LX/1Up;

    invoke-virtual {v5, v6, v3}, LX/1Uo;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)LX/1Uo;

    move-result-object v3

    .line 1958232
    iput-object v4, v3, LX/1Uo;->u:LX/4Ab;

    .line 1958233
    move-object v3, v3

    .line 1958234
    invoke-virtual {v3}, LX/1Uo;->u()LX/1af;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1958235
    iget-object v3, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f040039

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 1958236
    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 1958237
    :cond_2
    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_3

    .line 1958238
    iget-object v3, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget v3, v3, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->o:F

    iget-object v4, v0, LX/D1w;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget v4, v4, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->o:F

    invoke-static {v5, v3, v4, v5}, LX/4Ab;->b(FFFF)LX/4Ab;

    move-result-object v3

    move-object v4, v3

    goto :goto_1

    :cond_3
    move-object v4, v3

    goto :goto_1
.end method
