.class public final enum LX/DcY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DcY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DcY;

.field public static final enum HEADER:LX/DcY;

.field public static final enum MENU_ITEM:LX/DcY;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2018537
    new-instance v0, LX/DcY;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/DcY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DcY;->HEADER:LX/DcY;

    .line 2018538
    new-instance v0, LX/DcY;

    const-string v1, "MENU_ITEM"

    invoke-direct {v0, v1, v3}, LX/DcY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DcY;->MENU_ITEM:LX/DcY;

    .line 2018539
    const/4 v0, 0x2

    new-array v0, v0, [LX/DcY;

    sget-object v1, LX/DcY;->HEADER:LX/DcY;

    aput-object v1, v0, v2

    sget-object v1, LX/DcY;->MENU_ITEM:LX/DcY;

    aput-object v1, v0, v3

    sput-object v0, LX/DcY;->$VALUES:[LX/DcY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2018540
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DcY;
    .locals 1

    .prologue
    .line 2018541
    const-class v0, LX/DcY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DcY;

    return-object v0
.end method

.method public static values()[LX/DcY;
    .locals 1

    .prologue
    .line 2018542
    sget-object v0, LX/DcY;->$VALUES:[LX/DcY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DcY;

    return-object v0
.end method
