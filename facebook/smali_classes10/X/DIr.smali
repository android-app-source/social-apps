.class public final LX/DIr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:LX/DIv;


# direct methods
.method public constructor <init>(LX/DIv;)V
    .locals 0

    .prologue
    .line 1984485
    iput-object p1, p0, LX/DIr;->a:LX/DIv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1984482
    iget-object v0, p0, LX/DIr;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJT;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/DJT;->a(Lcom/facebook/ipc/composer/model/ProductItemPlace;)V

    .line 1984483
    iget-object v0, p0, LX/DIr;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->i:LX/03V;

    const-string v1, "ComposerSellController"

    const-string v2, "Couldn\'t complete StructuredLocationQuery."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1984484
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 13
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1984456
    iget-object v0, p0, LX/DIr;->a:LX/DIv;

    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 1984457
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1984458
    check-cast v2, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;

    .line 1984459
    invoke-virtual {v2}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationQueryModel;->a()Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;

    move-result-object v4

    .line 1984460
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;->k()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_2

    .line 1984461
    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;->k()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 1984462
    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;->k()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 1984463
    new-instance v10, LX/5Rm;

    invoke-direct {v10}, LX/5Rm;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;->l()Ljava/lang/String;

    move-result-object v11

    .line 1984464
    iput-object v11, v10, LX/5Rm;->a:Ljava/lang/String;

    .line 1984465
    move-object v10, v10

    .line 1984466
    invoke-virtual {v4}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommerceLocationInfoModels$StructuredLocationFragmentModel;->j()Ljava/lang/String;

    move-result-object v11

    .line 1984467
    iput-object v11, v10, LX/5Rm;->b:Ljava/lang/String;

    .line 1984468
    move-object v10, v10

    .line 1984469
    const/4 v11, 0x0

    invoke-virtual {v7, v6, v11}, LX/15i;->l(II)D

    move-result-wide v6

    .line 1984470
    iput-wide v6, v10, LX/5Rm;->c:D

    .line 1984471
    move-object v6, v10

    .line 1984472
    const/4 v7, 0x1

    invoke-virtual {v9, v8, v7}, LX/15i;->l(II)D

    move-result-wide v8

    .line 1984473
    iput-wide v8, v6, LX/5Rm;->d:D

    .line 1984474
    move-object v6, v6

    .line 1984475
    invoke-virtual {v6}, LX/5Rm;->a()Lcom/facebook/ipc/composer/model/ProductItemPlace;

    move-result-object v6

    move-object v2, v6

    .line 1984476
    :goto_1
    move-object v1, v2

    .line 1984477
    iput-object v1, v0, LX/DIv;->k:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    .line 1984478
    iget-object v0, p0, LX/DIr;->a:LX/DIv;

    iget-object v0, v0, LX/DIv;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJT;

    iget-object v1, p0, LX/DIr;->a:LX/DIv;

    iget-object v1, v1, LX/DIv;->k:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    invoke-virtual {v0, v1}, LX/DJT;->a(Lcom/facebook/ipc/composer/model/ProductItemPlace;)V

    .line 1984479
    return-void

    :cond_0
    move v2, v3

    .line 1984480
    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_0

    .line 1984481
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
