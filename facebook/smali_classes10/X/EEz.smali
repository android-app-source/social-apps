.class public final enum LX/EEz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EEz;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EEz;

.field public static final enum CONNECTED:LX/EEz;

.field public static final enum CONNECTING:LX/EEz;

.field public static final enum CONNECTION_DROPPED:LX/EEz;

.field public static final enum CONTACTING:LX/EEz;

.field public static final enum DISCONNECTED:LX/EEz;

.field public static final enum NO_ANSWER:LX/EEz;

.field public static final enum PARTICIPANT_LIMIT_REACHED:LX/EEz;

.field public static final enum REJECTED:LX/EEz;

.field public static final enum RINGING:LX/EEz;

.field public static final enum UNKNOWN:LX/EEz;

.field public static final enum UNREACHABLE:LX/EEz;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2094563
    new-instance v0, LX/EEz;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->UNKNOWN:LX/EEz;

    .line 2094564
    new-instance v0, LX/EEz;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v4}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->DISCONNECTED:LX/EEz;

    .line 2094565
    new-instance v0, LX/EEz;

    const-string v1, "NO_ANSWER"

    invoke-direct {v0, v1, v5}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->NO_ANSWER:LX/EEz;

    .line 2094566
    new-instance v0, LX/EEz;

    const-string v1, "REJECTED"

    invoke-direct {v0, v1, v6}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->REJECTED:LX/EEz;

    .line 2094567
    new-instance v0, LX/EEz;

    const-string v1, "UNREACHABLE"

    invoke-direct {v0, v1, v7}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->UNREACHABLE:LX/EEz;

    .line 2094568
    new-instance v0, LX/EEz;

    const-string v1, "CONNECTION_DROPPED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->CONNECTION_DROPPED:LX/EEz;

    .line 2094569
    new-instance v0, LX/EEz;

    const-string v1, "CONTACTING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->CONTACTING:LX/EEz;

    .line 2094570
    new-instance v0, LX/EEz;

    const-string v1, "RINGING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->RINGING:LX/EEz;

    .line 2094571
    new-instance v0, LX/EEz;

    const-string v1, "CONNECTING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->CONNECTING:LX/EEz;

    .line 2094572
    new-instance v0, LX/EEz;

    const-string v1, "CONNECTED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->CONNECTED:LX/EEz;

    .line 2094573
    new-instance v0, LX/EEz;

    const-string v1, "PARTICIPANT_LIMIT_REACHED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/EEz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEz;->PARTICIPANT_LIMIT_REACHED:LX/EEz;

    .line 2094574
    const/16 v0, 0xb

    new-array v0, v0, [LX/EEz;

    sget-object v1, LX/EEz;->UNKNOWN:LX/EEz;

    aput-object v1, v0, v3

    sget-object v1, LX/EEz;->DISCONNECTED:LX/EEz;

    aput-object v1, v0, v4

    sget-object v1, LX/EEz;->NO_ANSWER:LX/EEz;

    aput-object v1, v0, v5

    sget-object v1, LX/EEz;->REJECTED:LX/EEz;

    aput-object v1, v0, v6

    sget-object v1, LX/EEz;->UNREACHABLE:LX/EEz;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EEz;->CONNECTION_DROPPED:LX/EEz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EEz;->CONTACTING:LX/EEz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EEz;->RINGING:LX/EEz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EEz;->CONNECTING:LX/EEz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/EEz;->CONNECTED:LX/EEz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/EEz;->PARTICIPANT_LIMIT_REACHED:LX/EEz;

    aput-object v2, v0, v1

    sput-object v0, LX/EEz;->$VALUES:[LX/EEz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2094575
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EEz;
    .locals 1

    .prologue
    .line 2094576
    const-class v0, LX/EEz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EEz;

    return-object v0
.end method

.method public static values()[LX/EEz;
    .locals 1

    .prologue
    .line 2094577
    sget-object v0, LX/EEz;->$VALUES:[LX/EEz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EEz;

    return-object v0
.end method
