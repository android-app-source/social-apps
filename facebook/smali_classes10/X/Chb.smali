.class public final enum LX/Chb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Chb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Chb;

.field public static final enum ACTIVE:LX/Chb;

.field public static final enum INACTIVE:LX/Chb;

.field public static final enum INITIALIZING:LX/Chb;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1927954
    new-instance v0, LX/Chb;

    const-string v1, "INITIALIZING"

    invoke-direct {v0, v1, v2}, LX/Chb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Chb;->INITIALIZING:LX/Chb;

    .line 1927955
    new-instance v0, LX/Chb;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v3}, LX/Chb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Chb;->ACTIVE:LX/Chb;

    .line 1927956
    new-instance v0, LX/Chb;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v4}, LX/Chb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Chb;->INACTIVE:LX/Chb;

    .line 1927957
    const/4 v0, 0x3

    new-array v0, v0, [LX/Chb;

    sget-object v1, LX/Chb;->INITIALIZING:LX/Chb;

    aput-object v1, v0, v2

    sget-object v1, LX/Chb;->ACTIVE:LX/Chb;

    aput-object v1, v0, v3

    sget-object v1, LX/Chb;->INACTIVE:LX/Chb;

    aput-object v1, v0, v4

    sput-object v0, LX/Chb;->$VALUES:[LX/Chb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1927953
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Chb;
    .locals 1

    .prologue
    .line 1927951
    const-class v0, LX/Chb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Chb;

    return-object v0
.end method

.method public static values()[LX/Chb;
    .locals 1

    .prologue
    .line 1927952
    sget-object v0, LX/Chb;->$VALUES:[LX/Chb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Chb;

    return-object v0
.end method
