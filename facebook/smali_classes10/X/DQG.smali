.class public LX/DQG;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/groups/info/GroupInfoAdapter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1994260
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1994261
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/listview/BetterListView;LX/0gc;LX/DMP;LX/DPp;LX/5QT;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Lcom/facebook/groups/info/GroupInfoAdapter;
    .locals 18

    .prologue
    .line 1994258
    new-instance v0, Lcom/facebook/groups/info/GroupInfoAdapter;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v8

    check-cast v8, LX/23R;

    invoke-static/range {p0 .. p0}, LX/Bm1;->a(LX/0QB;)LX/Bm1;

    move-result-object v9

    check-cast v9, LX/Bm1;

    invoke-static/range {p0 .. p0}, LX/DPo;->a(LX/0QB;)LX/DPo;

    move-result-object v10

    check-cast v10, LX/DPo;

    invoke-static/range {p0 .. p0}, LX/DRS;->a(LX/0QB;)LX/DRS;

    move-result-object v11

    check-cast v11, LX/DRS;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/1nN;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v14

    check-cast v14, Ljava/lang/Boolean;

    invoke-static/range {p0 .. p0}, LX/8vR;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v15

    check-cast v15, Ljava/lang/Boolean;

    invoke-static/range {p0 .. p0}, LX/3my;->a(LX/0QB;)LX/3my;

    move-result-object v16

    check-cast v16, LX/3my;

    invoke-static/range {p0 .. p0}, LX/88k;->a(LX/0QB;)LX/88k;

    move-result-object v17

    check-cast v17, LX/88k;

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v17}, Lcom/facebook/groups/info/GroupInfoAdapter;-><init>(Lcom/facebook/widget/listview/BetterListView;LX/0gc;LX/DMP;LX/DPp;LX/5QT;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Landroid/content/res/Resources;LX/23R;LX/Bm1;LX/DPo;LX/DRS;LX/0ad;LX/0Uh;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/3my;LX/88k;)V

    .line 1994259
    return-object v0
.end method
