.class public LX/ESm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/7zZ;

.field public final b:LX/1Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Aa",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final c:LX/0oB;

.field public final d:LX/19j;


# direct methods
.method public constructor <init>(LX/7zZ;LX/1AZ;LX/0xX;LX/2xj;LX/19j;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2123497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2123498
    iput-object p1, p0, LX/ESm;->a:LX/7zZ;

    .line 2123499
    iget-object v0, p0, LX/ESm;->a:LX/7zZ;

    .line 2123500
    iput-boolean v2, v0, LX/7zZ;->g:Z

    .line 2123501
    iget-boolean v3, v0, LX/7zZ;->g:Z

    if-eqz v3, :cond_0

    .line 2123502
    invoke-static {v0}, LX/7zZ;->c(LX/7zZ;)V

    .line 2123503
    :cond_0
    invoke-virtual {p2, p1, v1, v1}, LX/1AZ;->a(LX/1AX;ZZ)LX/1Aa;

    move-result-object v0

    iput-object v0, p0, LX/ESm;->b:LX/1Aa;

    .line 2123504
    iget-object v0, p0, LX/ESm;->b:LX/1Aa;

    .line 2123505
    iget-object v1, p3, LX/0xX;->c:LX/0ad;

    sget v3, LX/0xY;->f:I

    const/16 p1, 0xc8

    invoke-interface {v1, v3, p1}, LX/0ad;->a(II)I

    move-result v1

    move v1, v1

    .line 2123506
    iget-object v3, v0, LX/1Aa;->h:LX/0ad;

    sget-short p1, LX/0ws;->ez:S

    const/4 p2, 0x0

    invoke-interface {v3, p1, p2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2123507
    iput v1, v0, LX/1Aa;->o:I

    .line 2123508
    :cond_1
    iget-object v0, p0, LX/ESm;->b:LX/1Aa;

    .line 2123509
    iput-boolean v2, v0, LX/1Aa;->n:Z

    .line 2123510
    new-instance v0, LX/ESl;

    invoke-direct {v0, p0}, LX/ESl;-><init>(LX/ESm;)V

    iput-object v0, p0, LX/ESm;->c:LX/0oB;

    .line 2123511
    iget-object v0, p0, LX/ESm;->c:LX/0oB;

    invoke-virtual {p4, v0}, LX/2xj;->a(LX/0oB;)V

    .line 2123512
    iput-object p5, p0, LX/ESm;->d:LX/19j;

    .line 2123513
    return-void
.end method

.method public static a(LX/0QB;)LX/ESm;
    .locals 9

    .prologue
    .line 2123514
    const-class v1, LX/ESm;

    monitor-enter v1

    .line 2123515
    :try_start_0
    sget-object v0, LX/ESm;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2123516
    sput-object v2, LX/ESm;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2123517
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2123518
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2123519
    new-instance v3, LX/ESm;

    invoke-static {v0}, LX/7zZ;->b(LX/0QB;)LX/7zZ;

    move-result-object v4

    check-cast v4, LX/7zZ;

    const-class v5, LX/1AZ;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1AZ;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v6

    check-cast v6, LX/0xX;

    invoke-static {v0}, LX/2xj;->a(LX/0QB;)LX/2xj;

    move-result-object v7

    check-cast v7, LX/2xj;

    invoke-static {v0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v8

    check-cast v8, LX/19j;

    invoke-direct/range {v3 .. v8}, LX/ESm;-><init>(LX/7zZ;LX/1AZ;LX/0xX;LX/2xj;LX/19j;)V

    .line 2123520
    move-object v0, v3

    .line 2123521
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2123522
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ESm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2123523
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2123524
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
