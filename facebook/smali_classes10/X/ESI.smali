.class public final LX/ESI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultSettingsActivity;)V
    .locals 0

    .prologue
    .line 2122798
    iput-object p1, p0, LX/ESI;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x3a972ab2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2122799
    check-cast p1, Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 2122800
    iget-object v2, p0, LX/ESI;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    iget-object v2, v2, Lcom/facebook/vault/ui/VaultSettingsActivity;->p:LX/ERK;

    invoke-virtual {v2, v1}, LX/ERK;->a(Z)V

    .line 2122801
    new-instance v2, Lcom/facebook/vault/ui/VaultSettingsActivity$1$1;

    invoke-direct {v2, p0, v1}, Lcom/facebook/vault/ui/VaultSettingsActivity$1$1;-><init>(LX/ESI;Z)V

    invoke-virtual {v2}, Lcom/facebook/vault/ui/VaultSettingsActivity$1$1;->start()V

    .line 2122802
    iget-object v2, p0, LX/ESI;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    invoke-static {v2, v1}, Lcom/facebook/vault/ui/VaultSettingsActivity;->b(Lcom/facebook/vault/ui/VaultSettingsActivity;Z)V

    .line 2122803
    if-eqz v1, :cond_0

    .line 2122804
    iget-object v1, p0, LX/ESI;->a:Lcom/facebook/vault/ui/VaultSettingsActivity;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSettingsActivity;->t:LX/2TK;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, LX/2TK;->c(I)V

    .line 2122805
    :cond_0
    const v1, -0x5dcdf13f

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
