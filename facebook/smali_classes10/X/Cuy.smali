.class public LX/Cuy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field private static final f:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1947589
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "permissions/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1947590
    sput-object v0, LX/Cuy;->f:LX/0Tn;

    const-string v1, "audio_never_ask_again_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Cuy;->a:LX/0Tn;

    .line 1947591
    sget-object v0, LX/Cuy;->f:LX/0Tn;

    const-string v1, "camera_never_ask_again_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Cuy;->b:LX/0Tn;

    .line 1947592
    sget-object v0, LX/Cuy;->f:LX/0Tn;

    const-string v1, "contacts_never_ask_again_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Cuy;->c:LX/0Tn;

    .line 1947593
    sget-object v0, LX/Cuy;->f:LX/0Tn;

    const-string v1, "location_never_ask_again_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Cuy;->d:LX/0Tn;

    .line 1947594
    sget-object v0, LX/Cuy;->f:LX/0Tn;

    const-string v1, "storage_never_ask_again_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Cuy;->e:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1947595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1947596
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1947597
    sget-object v0, LX/Cuy;->f:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
