.class public final enum LX/ES7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ES7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ES7;

.field public static final enum ALL:LX/ES7;

.field public static final enum NO_UPGRADES:LX/ES7;

.field public static final enum UPGRADE_ONLY:LX/ES7;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2122154
    new-instance v0, LX/ES7;

    const-string v1, "NO_UPGRADES"

    invoke-direct {v0, v1, v2}, LX/ES7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ES7;->NO_UPGRADES:LX/ES7;

    new-instance v0, LX/ES7;

    const-string v1, "UPGRADE_ONLY"

    invoke-direct {v0, v1, v3}, LX/ES7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ES7;->UPGRADE_ONLY:LX/ES7;

    new-instance v0, LX/ES7;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, LX/ES7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ES7;->ALL:LX/ES7;

    .line 2122155
    const/4 v0, 0x3

    new-array v0, v0, [LX/ES7;

    sget-object v1, LX/ES7;->NO_UPGRADES:LX/ES7;

    aput-object v1, v0, v2

    sget-object v1, LX/ES7;->UPGRADE_ONLY:LX/ES7;

    aput-object v1, v0, v3

    sget-object v1, LX/ES7;->ALL:LX/ES7;

    aput-object v1, v0, v4

    sput-object v0, LX/ES7;->$VALUES:[LX/ES7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2122153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ES7;
    .locals 1

    .prologue
    .line 2122152
    const-class v0, LX/ES7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ES7;

    return-object v0
.end method

.method public static values()[LX/ES7;
    .locals 1

    .prologue
    .line 2122151
    sget-object v0, LX/ES7;->$VALUES:[LX/ES7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ES7;

    return-object v0
.end method
