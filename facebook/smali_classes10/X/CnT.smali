.class public abstract LX/CnT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "LX/CnG;",
        "B",
        "LOCKDATA::Lcom/facebook/richdocument/model/data/BlockData;",
        ">",
        "Ljava/lang/Object;",
        "LX/02k;",
        "Lcom/facebook/richdocument/presenter/BlockPresenter",
        "<TV;TB",
        "LOCKDATA;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ciy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:LX/CnG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field private e:LX/1B1;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CnT",
            "<TV;TB",
            "LOCKDATA;",
            ">.RunOnStartUpTasksCompletedSubscriber;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CnG;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 1933893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933894
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CnT;->f:Ljava/util/List;

    .line 1933895
    iput-object p1, p0, LX/CnT;->d:LX/CnG;

    .line 1933896
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    invoke-interface {v0, p0}, LX/CnG;->a(LX/CnT;)V

    .line 1933897
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, LX/CnT;->e:LX/1B1;

    .line 1933898
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/CnT;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v2

    check-cast v2, LX/Chv;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-static {v0}, LX/Ciy;->a(LX/0QB;)LX/Ciy;

    move-result-object v0

    check-cast v0, LX/Ciy;

    iput-object v2, p0, LX/CnT;->a:LX/Chv;

    iput-object p1, p0, LX/CnT;->b:LX/03V;

    iput-object v0, p0, LX/CnT;->c:LX/Ciy;

    .line 1933899
    return-void
.end method


# virtual methods
.method public final a()LX/CnG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 1933892
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    return-object v0
.end method

.method public abstract a(LX/Clr;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TB",
            "LOCKDATA;",
            ")V"
        }
    .end annotation
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1933888
    iget-object v0, p0, LX/CnT;->e:LX/1B1;

    iget-object v1, p0, LX/CnT;->a:LX/Chv;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 1933889
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933890
    invoke-interface {v0, p1}, LX/CnG;->b(Landroid/os/Bundle;)V

    .line 1933891
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1933876
    iget-object v0, p0, LX/CnT;->c:LX/Ciy;

    invoke-virtual {v0}, LX/Ciy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1933877
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 1933878
    :goto_0
    return-void

    .line 1933879
    :cond_0
    new-instance v0, LX/CnS;

    invoke-direct {v0, p0, p1}, LX/CnS;-><init>(LX/CnT;Ljava/lang/Runnable;)V

    .line 1933880
    iget-object v1, p0, LX/CnT;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1933881
    iget-object v1, p0, LX/CnT;->a:LX/Chv;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1933884
    iget-object v0, p0, LX/CnT;->e:LX/1B1;

    iget-object v1, p0, LX/CnT;->a:LX/Chv;

    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 1933885
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933886
    invoke-interface {v0, p1}, LX/CnG;->c(Landroid/os/Bundle;)V

    .line 1933887
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1933882
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933883
    invoke-interface {v0}, LX/CnG;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
