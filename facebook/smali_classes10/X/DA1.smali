.class public final enum LX/DA1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DA1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DA1;

.field public static final enum CALL_LOG:LX/DA1;

.field public static final enum MMS_LOG:LX/DA1;

.field public static final enum SMS_LOG:LX/DA1;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1970731
    new-instance v0, LX/DA1;

    const-string v1, "CALL_LOG"

    invoke-direct {v0, v1, v2}, LX/DA1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DA1;->CALL_LOG:LX/DA1;

    .line 1970732
    new-instance v0, LX/DA1;

    const-string v1, "MMS_LOG"

    invoke-direct {v0, v1, v3}, LX/DA1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DA1;->MMS_LOG:LX/DA1;

    .line 1970733
    new-instance v0, LX/DA1;

    const-string v1, "SMS_LOG"

    invoke-direct {v0, v1, v4}, LX/DA1;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DA1;->SMS_LOG:LX/DA1;

    .line 1970734
    const/4 v0, 0x3

    new-array v0, v0, [LX/DA1;

    sget-object v1, LX/DA1;->CALL_LOG:LX/DA1;

    aput-object v1, v0, v2

    sget-object v1, LX/DA1;->MMS_LOG:LX/DA1;

    aput-object v1, v0, v3

    sget-object v1, LX/DA1;->SMS_LOG:LX/DA1;

    aput-object v1, v0, v4

    sput-object v0, LX/DA1;->$VALUES:[LX/DA1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1970735
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DA1;
    .locals 1

    .prologue
    .line 1970736
    const-class v0, LX/DA1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DA1;

    return-object v0
.end method

.method public static values()[LX/DA1;
    .locals 1

    .prologue
    .line 1970737
    sget-object v0, LX/DA1;->$VALUES:[LX/DA1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DA1;

    return-object v0
.end method
