.class public final LX/Ewz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V
    .locals 0

    .prologue
    .line 2183914
    iput-object p1, p0, LX/Ewz;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 2183915
    add-int v0, p2, p3

    add-int/lit8 v0, v0, 0x3

    if-lt v0, p4, :cond_1

    const/4 v0, 0x1

    .line 2183916
    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ewz;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->s:LX/Exw;

    invoke-virtual {v0}, LX/Exw;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2183917
    iget-object v0, p0, LX/Ewz;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    invoke-static {v0}, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->c(Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;)V

    .line 2183918
    :cond_0
    return-void

    .line 2183919
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 2183920
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 2183921
    iget-object v0, p0, LX/Ewz;->a:Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/search/FriendsCenterSearchFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    .line 2183922
    invoke-static {v0}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 2183923
    :cond_0
    return-void
.end method
