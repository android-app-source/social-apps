.class public final LX/ESA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/photos/base/photos/VaultPhoto;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultDraweeGridAdapter;)V
    .locals 0

    .prologue
    .line 2122550
    iput-object p1, p0, LX/ESA;->a:Lcom/facebook/vault/ui/VaultDraweeGridAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 5

    .prologue
    .line 2122551
    check-cast p1, Lcom/facebook/photos/base/photos/VaultPhoto;

    check-cast p2, Lcom/facebook/photos/base/photos/VaultPhoto;

    .line 2122552
    invoke-virtual {p1}, Lcom/facebook/photos/base/photos/VaultPhoto;->b()J

    move-result-wide v0

    .line 2122553
    invoke-virtual {p2}, Lcom/facebook/photos/base/photos/VaultPhoto;->b()J

    move-result-wide v2

    .line 2122554
    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 2122555
    const/4 v0, 0x0

    .line 2122556
    :goto_0
    return v0

    :cond_0
    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method
