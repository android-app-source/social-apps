.class public LX/D9u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1970664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970665
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1970649
    check-cast p1, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;

    .line 1970650
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1970651
    iget-object v0, p1, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1970652
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1970653
    iget-object v0, p1, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1970654
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1970655
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "mechanism"

    .line 1970656
    iget-object v2, p1, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1970657
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1970658
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "surface"

    .line 1970659
    iget-object v2, p1, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1970660
    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1970661
    new-instance v0, LX/14N;

    const-string v1, "deletePageReview"

    const-string v2, "DELETE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1970662
    iget-object v5, p1, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1970663
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/open_graph_ratings"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1970646
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1970647
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1970648
    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
