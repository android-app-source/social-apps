.class public LX/DRS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/res/Resources;

.field public b:LX/3my;

.field public c:LX/DOL;

.field public d:Lcom/facebook/content/SecureContextHelper;

.field public e:LX/DP5;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/3my;LX/DOL;Lcom/facebook/content/SecureContextHelper;LX/DP5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1998243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1998244
    iput-object p1, p0, LX/DRS;->a:Landroid/content/res/Resources;

    .line 1998245
    iput-object p2, p0, LX/DRS;->b:LX/3my;

    .line 1998246
    iput-object p3, p0, LX/DRS;->c:LX/DOL;

    .line 1998247
    iput-object p4, p0, LX/DRS;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1998248
    iput-object p5, p0, LX/DRS;->e:LX/DP5;

    .line 1998249
    return-void
.end method

.method public static a(LX/0QB;)LX/DRS;
    .locals 7

    .prologue
    .line 1998250
    new-instance v1, LX/DRS;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {p0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v3

    check-cast v3, LX/3my;

    invoke-static {p0}, LX/DOL;->a(LX/0QB;)LX/DOL;

    move-result-object v4

    check-cast v4, LX/DOL;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/DP5;->b(LX/0QB;)LX/DP5;

    move-result-object v6

    check-cast v6, LX/DP5;

    invoke-direct/range {v1 .. v6}, LX/DRS;-><init>(Landroid/content/res/Resources;LX/3my;LX/DOL;Lcom/facebook/content/SecureContextHelper;LX/DP5;)V

    .line 1998251
    move-object v0, v1

    .line 1998252
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/DML;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/DML",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1998253
    iget-object v0, p0, LX/DRS;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b2021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1998254
    sget-object v1, LX/DQN;->o:LX/DML;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1998255
    check-cast p1, LX/Daq;

    .line 1998256
    iget-object v1, p1, LX/Daq;->a:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 1998257
    iget-object v1, p1, LX/Daq;->a:Landroid/widget/TextView;

    iget-object p0, p1, LX/Daq;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result p0

    iget-object p2, p1, LX/Daq;->a:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result p2

    invoke-virtual {v1, v0, p0, v0, p2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1998258
    :cond_0
    iget-object v1, p1, LX/Daq;->b:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 1998259
    iget-object v1, p1, LX/Daq;->b:Landroid/widget/TextView;

    iget-object p0, p1, LX/Daq;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result p0

    iget-object p2, p1, LX/Daq;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result p2

    invoke-virtual {v1, v0, p0, v0, p2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1998260
    :cond_1
    :goto_0
    return-void

    .line 1998261
    :cond_2
    sget-object v1, LX/DQN;->m:LX/DML;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1998262
    check-cast p1, LX/DaR;

    .line 1998263
    const v1, 0x7f0d0f04

    invoke-virtual {p1, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1998264
    iget p0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget p2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v1, v0, p0, v0, p2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1998265
    iget-object v1, p1, LX/DaR;->j:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

    if-eqz v1, :cond_3

    .line 1998266
    iget-object v1, p1, LX/DaR;->j:Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

    invoke-virtual {v1, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->setHorizontalMargin(I)V

    .line 1998267
    :cond_3
    goto :goto_0

    .line 1998268
    :cond_4
    sget-object v1, LX/DQN;->n:LX/DML;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1998269
    check-cast p1, LX/Db3;

    .line 1998270
    const v1, 0x7f0d092b

    invoke-virtual {p1, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 1998271
    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getPaddingTop()I

    move-result p0

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getPaddingBottom()I

    move-result p2

    invoke-virtual {v1, v0, p0, v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 1998272
    iget-object v1, p1, LX/Db3;->a:Landroid/widget/TextView;

    iget-object p0, p1, LX/Db3;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result p0

    iget-object p2, p1, LX/Db3;->a:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result p2

    invoke-virtual {v1, v0, p0, v0, p2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1998273
    goto :goto_0
.end method
