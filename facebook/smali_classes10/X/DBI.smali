.class public LX/DBI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/events/model/Event;

.field public b:Lcom/facebook/events/common/EventAnalyticsParams;

.field public c:Landroid/content/Context;

.field public d:LX/1nQ;

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1nQ;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1nQ;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1972638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1972639
    iput-object p1, p0, LX/DBI;->c:Landroid/content/Context;

    .line 1972640
    iput-object p2, p0, LX/DBI;->d:LX/1nQ;

    .line 1972641
    iput-object p3, p0, LX/DBI;->e:LX/0Ot;

    .line 1972642
    return-void
.end method

.method public static b(LX/0QB;)LX/DBI;
    .locals 4

    .prologue
    .line 1972643
    new-instance v2, LX/DBI;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v1

    check-cast v1, LX/1nQ;

    const/16 v3, 0x3be

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/DBI;-><init>(Landroid/content/Context;LX/1nQ;LX/0Ot;)V

    .line 1972644
    return-object v2
.end method
