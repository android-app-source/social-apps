.class public final LX/Dzv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ccr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2067584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 2067576
    invoke-direct {p0}, LX/Dzv;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/places/create/citypicker/NewCityPickerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Z)V
    .locals 3

    .prologue
    .line 2067577
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2067578
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2067579
    const-string v2, "picked_city"

    invoke-static {v1, v2, p2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2067580
    const-string v2, "is_currently_there"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2067581
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2067582
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2067583
    return-void
.end method
