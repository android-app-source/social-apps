.class public LX/DbA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/DbA;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:Z

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/Boolean;LX/0Or;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/config/server/ShouldUsePreferredConfig;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/work/config/community/WorkCommunitySubdomain;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2016874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2016875
    iput-object p1, p0, LX/DbA;->a:LX/0Or;

    .line 2016876
    iput-object p2, p0, LX/DbA;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2016877
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/DbA;->c:Z

    .line 2016878
    iput-object p4, p0, LX/DbA;->d:LX/0Or;

    .line 2016879
    return-void
.end method

.method public static a(LX/0QB;)LX/DbA;
    .locals 7

    .prologue
    .line 2016880
    sget-object v0, LX/DbA;->e:LX/DbA;

    if-nez v0, :cond_1

    .line 2016881
    const-class v1, LX/DbA;

    monitor-enter v1

    .line 2016882
    :try_start_0
    sget-object v0, LX/DbA;->e:LX/DbA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2016883
    if-eqz v2, :cond_0

    .line 2016884
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2016885
    new-instance v5, LX/DbA;

    const/16 v3, 0x1474

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    const/16 p0, 0x160d

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v6, v3, v4, p0}, LX/DbA;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/Boolean;LX/0Or;)V

    .line 2016886
    move-object v0, v5

    .line 2016887
    sput-object v0, LX/DbA;->e:LX/DbA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2016888
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2016889
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2016890
    :cond_1
    sget-object v0, LX/DbA;->e:LX/DbA;

    return-object v0

    .line 2016891
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2016892
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2016893
    iget-object v0, p0, LX/DbA;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2016894
    const-string v0, "m.facebook.com"

    .line 2016895
    :goto_0
    return-object v0

    .line 2016896
    :cond_0
    iget-object v0, p0, LX/DbA;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->q:LX/0Tn;

    const-string v2, "default"

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2016897
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 2016898
    const-string v0, "m.facebook.com"

    goto :goto_0

    .line 2016899
    :sswitch_0
    const-string v2, "intern"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "dev"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v2, "sandbox"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v2, "default"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    .line 2016900
    :pswitch_0
    const-string v0, "m.intern.facebook.com"

    goto :goto_0

    .line 2016901
    :pswitch_1
    const-string v0, "m.dev.facebook.com"

    goto :goto_0

    .line 2016902
    :pswitch_2
    iget-object v0, p0, LX/DbA;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->r:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2016903
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2016904
    const-string v0, "m.facebook.com"

    goto :goto_0

    .line 2016905
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "m."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x468ec8ee -> :sswitch_0
        0x18415 -> :sswitch_1
        0x5c13d641 -> :sswitch_3
        0x6f2fbec7 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2016906
    iget-boolean v0, p0, LX/DbA;->c:Z

    if-nez v0, :cond_0

    .line 2016907
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "You\'re querying community link on not AtWork build"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2016908
    :cond_0
    iget-object v0, p0, LX/DbA;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2016909
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2016910
    invoke-virtual {p0}, LX/DbA;->a()Ljava/lang/String;

    move-result-object v0

    .line 2016911
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/DbA;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
