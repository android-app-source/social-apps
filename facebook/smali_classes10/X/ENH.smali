.class public final LX/ENH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic b:LX/1Ps;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/local/PlaceClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/local/PlaceClickListenerPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/1Ps;)V
    .locals 0

    .prologue
    .line 2112413
    iput-object p1, p0, LX/ENH;->c:Lcom/facebook/search/results/rows/sections/local/PlaceClickListenerPartDefinition;

    iput-object p2, p0, LX/ENH;->a:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object p3, p0, LX/ENH;->b:LX/1Ps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x535889f8

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2112414
    iget-object v0, p0, LX/ENH;->c:Lcom/facebook/search/results/rows/sections/local/PlaceClickListenerPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/local/PlaceClickListenerPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E1i;

    iget-object v1, p0, LX/ENH;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/ENH;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ANDROID_SEARCH_LOCAL_KWSERP_ENTITYMODULE"

    invoke-virtual {v0, v1, v3, v4}, LX/E1i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v1

    .line 2112415
    iget-object v0, p0, LX/ENH;->c:Lcom/facebook/search/results/rows/sections/local/PlaceClickListenerPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/local/PlaceClickListenerPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v1, LX/Cfl;->d:Landroid/content/Intent;

    iget-object v1, p0, LX/ENH;->b:LX/1Ps;

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2112416
    iget-object v0, p0, LX/ENH;->b:LX/1Ps;

    check-cast v0, LX/Cxe;

    iget-object v1, p0, LX/ENH;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v0, v1}, LX/Cxe;->c(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 2112417
    const v0, -0x6528deb2

    invoke-static {v5, v5, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
