.class public final LX/DQq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupReportedStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupReportedStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$GroupTopicTagsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Z

.field public F:Z

.field public G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Z

.field public I:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "photoForLauncherShortcut"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "photoForLauncherShortcut"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possibleJoinApprovalSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possibleJoinApprovalSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public P:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possiblePostPermissionSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possiblePostPermissionSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public S:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possibleVisibilitySettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possibleVisibilitySettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Z

.field public X:Z

.field public Y:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "suggestedPurpose"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "suggestedPurpose"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Z

.field public ad:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public al:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "visibilitySentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public am:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "visibilitySentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:J

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "coverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "coverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupDocsAndFiles"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupDocsAndFiles"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupForsaleAvailableStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupForsaleAvailableStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPendingMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPendingMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPendingStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPendingStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1995402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1995403
    return-void
.end method

.method public static a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DQq;
    .locals 4

    .prologue
    .line 1995517
    new-instance v0, LX/DQq;

    invoke-direct {v0}, LX/DQq;-><init>()V

    .line 1995518
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->k()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    .line 1995519
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->l()J

    move-result-wide v2

    iput-wide v2, v0, LX/DQq;->b:J

    .line 1995520
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->m()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->c:Z

    .line 1995521
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->n()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->d:Z

    .line 1995522
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->e:Z

    .line 1995523
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->o()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->f:Z

    .line 1995524
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->p()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->g:Z

    .line 1995525
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->q()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->h:Z

    .line 1995526
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->i:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1995527
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/DQq;->j:LX/15i;

    iput v1, v0, LX/DQq;->k:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1995528
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->t()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->l:Ljava/lang/String;

    .line 1995529
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->m:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 1995530
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->x()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/DQq;->n:LX/15i;

    iput v1, v0, LX/DQq;->o:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1995531
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    .line 1995532
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iput-object v2, v0, LX/DQq;->q:LX/15i;

    iput v1, v0, LX/DQq;->r:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1995533
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->A()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->s:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    .line 1995534
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->B()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    iput-object v2, v0, LX/DQq;->t:LX/15i;

    iput v1, v0, LX/DQq;->u:I

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1995535
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->C()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_4
    iput-object v2, v0, LX/DQq;->v:LX/15i;

    iput v1, v0, LX/DQq;->w:I

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 1995536
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->D()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_5
    iput-object v2, v0, LX/DQq;->x:LX/15i;

    iput v1, v0, LX/DQq;->y:I

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 1995537
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->E()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->z:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1995538
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->F()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_6
    iput-object v2, v0, LX/DQq;->A:LX/15i;

    iput v1, v0, LX/DQq;->B:I

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 1995539
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->G()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->C:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1995540
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->H()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->D:LX/0Px;

    .line 1995541
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->I()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->E:Z

    .line 1995542
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->J()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->F:Z

    .line 1995543
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->G:Ljava/lang/String;

    .line 1995544
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->L()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->H:Z

    .line 1995545
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->I:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 1995546
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->J:Ljava/lang/String;

    .line 1995547
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->K:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    .line 1995548
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_7
    iput-object v2, v0, LX/DQq;->L:LX/15i;

    iput v1, v0, LX/DQq;->M:I

    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    .line 1995549
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->P()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_8
    iput-object v2, v0, LX/DQq;->N:LX/15i;

    iput v1, v0, LX/DQq;->O:I

    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    .line 1995550
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Q()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_9
    iput-object v2, v0, LX/DQq;->P:LX/15i;

    iput v1, v0, LX/DQq;->Q:I

    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_9

    .line 1995551
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->R()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->R:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    .line 1995552
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->S()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->S:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    .line 1995553
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->T()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_a
    iput-object v2, v0, LX/DQq;->T:LX/15i;

    iput v1, v0, LX/DQq;->U:I

    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    .line 1995554
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->U()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->V:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    .line 1995555
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->V()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->W:Z

    .line 1995556
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->W()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->X:Z

    .line 1995557
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->X()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->Y:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1995558
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Y()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_b
    iput-object v2, v0, LX/DQq;->Z:LX/15i;

    iput v1, v0, LX/DQq;->aa:I

    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_b

    .line 1995559
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->ab:Ljava/lang/String;

    .line 1995560
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->aa()Z

    move-result v1

    iput-boolean v1, v0, LX/DQq;->ac:Z

    .line 1995561
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->ad:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1995562
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->ae:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1995563
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ab()Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->af:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    .line 1995564
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ac()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->ag:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 1995565
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->kx_()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->ah:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 1995566
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ky_()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->ai:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 1995567
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->aj:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 1995568
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    iput-object v1, v0, LX/DQq;->ak:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1995569
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ae()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_c
    iput-object v2, v0, LX/DQq;->al:LX/15i;

    iput v1, v0, LX/DQq;->am:I

    monitor-exit v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_c

    .line 1995570
    return-object v0

    .line 1995571
    :catchall_0
    move-exception v0

    :try_start_d
    monitor-exit v3
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    throw v0

    .line 1995572
    :catchall_1
    move-exception v0

    :try_start_e
    monitor-exit v3
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    throw v0

    .line 1995573
    :catchall_2
    move-exception v0

    :try_start_f
    monitor-exit v3
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    throw v0

    .line 1995574
    :catchall_3
    move-exception v0

    :try_start_10
    monitor-exit v3
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    throw v0

    .line 1995575
    :catchall_4
    move-exception v0

    :try_start_11
    monitor-exit v3
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    throw v0

    .line 1995576
    :catchall_5
    move-exception v0

    :try_start_12
    monitor-exit v3
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    throw v0

    .line 1995577
    :catchall_6
    move-exception v0

    :try_start_13
    monitor-exit v3
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    throw v0

    .line 1995578
    :catchall_7
    move-exception v0

    :try_start_14
    monitor-exit v3
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_7

    throw v0

    .line 1995579
    :catchall_8
    move-exception v0

    :try_start_15
    monitor-exit v3
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_8

    throw v0

    .line 1995580
    :catchall_9
    move-exception v0

    :try_start_16
    monitor-exit v3
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_9

    throw v0

    .line 1995581
    :catchall_a
    move-exception v0

    :try_start_17
    monitor-exit v3
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_a

    throw v0

    .line 1995582
    :catchall_b
    move-exception v0

    :try_start_18
    monitor-exit v3
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_b

    throw v0

    .line 1995583
    :catchall_c
    move-exception v0

    :try_start_19
    monitor-exit v3
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_c

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;
    .locals 46

    .prologue
    .line 1995404
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1995405
    move-object/from16 v0, p0

    iget-object v3, v0, LX/DQq;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1995406
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->i:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1995407
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->j:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->k:I

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v4, -0x64b842c9

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1995408
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->l:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1995409
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->m:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1995410
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->n:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->o:I

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v4, -0x1f84a18

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1995411
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1995412
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->q:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->r:I

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const v4, 0x7bbc1ce5

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1995413
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->s:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1995414
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->t:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->u:I

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    const v4, -0x18d312bf

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1995415
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->v:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->w:I

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    const v4, 0x4509b6d8

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1995416
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->x:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->y:I

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    const v4, 0x56796cf0

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1995417
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->z:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1995418
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->A:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->B:I

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    const v4, -0xf69c7a9

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1995419
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->C:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1995420
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->D:LX/0Px;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v22

    .line 1995421
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->G:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 1995422
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->I:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    .line 1995423
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->J:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 1995424
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->K:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1995425
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_7
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->L:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->M:I

    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    const v4, 0x51ecfa16

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 1995426
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_8
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->N:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->O:I

    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    const v4, -0x4a0c7f15

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 1995427
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_9
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->P:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->Q:I

    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_9

    const v4, 0x44e15961

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1995428
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->R:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1995429
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->S:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1995430
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_a
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->T:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->U:I

    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    const v4, -0x22a1d027    # -1.00064086E18f

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 1995431
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->V:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v33

    .line 1995432
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->Y:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v34

    .line 1995433
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_b
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->Z:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->aa:I

    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_b

    const v4, 0x6da0c94

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 1995434
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->ab:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    .line 1995435
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->ad:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v37

    .line 1995436
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->ae:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v38

    .line 1995437
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->af:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v39

    .line 1995438
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->ag:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v40

    .line 1995439
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->ah:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v41

    .line 1995440
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->ai:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v42

    .line 1995441
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->aj:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v43

    .line 1995442
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DQq;->ak:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v2, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v44

    .line 1995443
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_c
    move-object/from16 v0, p0

    iget-object v5, v0, LX/DQq;->al:LX/15i;

    move-object/from16 v0, p0

    iget v6, v0, LX/DQq;->am:I

    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_c

    const v4, 0x657dce4b

    invoke-static {v5, v6, v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 1995444
    const/16 v4, 0x34

    invoke-virtual {v2, v4}, LX/186;->c(I)V

    .line 1995445
    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, LX/186;->b(II)V

    .line 1995446
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/DQq;->b:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1995447
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->c:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995448
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->d:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995449
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->e:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995450
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->f:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995451
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->g:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995452
    const/4 v3, 0x7

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->h:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995453
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1995454
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1995455
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1995456
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1995457
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1995458
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1995459
    const/16 v3, 0xe

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1995460
    const/16 v3, 0xf

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1995461
    const/16 v3, 0x10

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995462
    const/16 v3, 0x11

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995463
    const/16 v3, 0x12

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995464
    const/16 v3, 0x13

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995465
    const/16 v3, 0x14

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995466
    const/16 v3, 0x15

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995467
    const/16 v3, 0x16

    move/from16 v0, v22

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995468
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->E:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995469
    const/16 v3, 0x18

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->F:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995470
    const/16 v3, 0x19

    move/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995471
    const/16 v3, 0x1a

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->H:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995472
    const/16 v3, 0x1b

    move/from16 v0, v24

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995473
    const/16 v3, 0x1c

    move/from16 v0, v25

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995474
    const/16 v3, 0x1d

    move/from16 v0, v26

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995475
    const/16 v3, 0x1e

    move/from16 v0, v27

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995476
    const/16 v3, 0x1f

    move/from16 v0, v28

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995477
    const/16 v3, 0x20

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995478
    const/16 v3, 0x21

    move/from16 v0, v30

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995479
    const/16 v3, 0x22

    move/from16 v0, v31

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995480
    const/16 v3, 0x23

    move/from16 v0, v32

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995481
    const/16 v3, 0x24

    move/from16 v0, v33

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995482
    const/16 v3, 0x25

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->W:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995483
    const/16 v3, 0x26

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->X:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995484
    const/16 v3, 0x27

    move/from16 v0, v34

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995485
    const/16 v3, 0x28

    move/from16 v0, v35

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995486
    const/16 v3, 0x29

    move/from16 v0, v36

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995487
    const/16 v3, 0x2a

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/DQq;->ac:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1995488
    const/16 v3, 0x2b

    move/from16 v0, v37

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995489
    const/16 v3, 0x2c

    move/from16 v0, v38

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995490
    const/16 v3, 0x2d

    move/from16 v0, v39

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995491
    const/16 v3, 0x2e

    move/from16 v0, v40

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995492
    const/16 v3, 0x2f

    move/from16 v0, v41

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995493
    const/16 v3, 0x30

    move/from16 v0, v42

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995494
    const/16 v3, 0x31

    move/from16 v0, v43

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995495
    const/16 v3, 0x32

    move/from16 v0, v44

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995496
    const/16 v3, 0x33

    move/from16 v0, v45

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1995497
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1995498
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1995499
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1995500
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1995501
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1995502
    new-instance v3, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-direct {v3, v2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;-><init>(LX/15i;)V

    .line 1995503
    return-object v3

    .line 1995504
    :catchall_0
    move-exception v2

    :try_start_d
    monitor-exit v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    throw v2

    .line 1995505
    :catchall_1
    move-exception v2

    :try_start_e
    monitor-exit v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    throw v2

    .line 1995506
    :catchall_2
    move-exception v2

    :try_start_f
    monitor-exit v4
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    throw v2

    .line 1995507
    :catchall_3
    move-exception v2

    :try_start_10
    monitor-exit v4
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    throw v2

    .line 1995508
    :catchall_4
    move-exception v2

    :try_start_11
    monitor-exit v4
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    throw v2

    .line 1995509
    :catchall_5
    move-exception v2

    :try_start_12
    monitor-exit v4
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    throw v2

    .line 1995510
    :catchall_6
    move-exception v2

    :try_start_13
    monitor-exit v4
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    throw v2

    .line 1995511
    :catchall_7
    move-exception v2

    :try_start_14
    monitor-exit v4
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_7

    throw v2

    .line 1995512
    :catchall_8
    move-exception v2

    :try_start_15
    monitor-exit v4
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_8

    throw v2

    .line 1995513
    :catchall_9
    move-exception v2

    :try_start_16
    monitor-exit v4
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_9

    throw v2

    .line 1995514
    :catchall_a
    move-exception v2

    :try_start_17
    monitor-exit v4
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_a

    throw v2

    .line 1995515
    :catchall_b
    move-exception v2

    :try_start_18
    monitor-exit v4
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_b

    throw v2

    .line 1995516
    :catchall_c
    move-exception v2

    :try_start_19
    monitor-exit v4
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_c

    throw v2
.end method
