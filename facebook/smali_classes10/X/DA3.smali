.class public LX/DA3;
.super LX/DA2;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field public final c:Landroid/content/ContentResolver;

.field private final d:LX/DA5;

.field public final e:LX/1Ml;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1970768
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "number"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    const-string v1, "date"

    aput-object v1, v0, v6

    const-string v1, "duration"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "numberlabel"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "numbertype"

    aput-object v2, v0, v1

    sput-object v0, LX/DA3;->a:[Ljava/lang/String;

    .line 1970769
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "number"

    aput-object v1, v0, v3

    const-string v1, "type"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "duration"

    aput-object v1, v0, v6

    const-string v1, "name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "numberlabel"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "numbertype"

    aput-object v2, v0, v1

    sput-object v0, LX/DA3;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/DA5;LX/1Ml;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1970763
    invoke-direct {p0}, LX/DA2;-><init>()V

    .line 1970764
    iput-object p1, p0, LX/DA3;->c:Landroid/content/ContentResolver;

    .line 1970765
    iput-object p2, p0, LX/DA3;->d:LX/DA5;

    .line 1970766
    iput-object p3, p0, LX/DA3;->e:LX/1Ml;

    .line 1970767
    return-void
.end method


# virtual methods
.method public final a()LX/DA1;
    .locals 1

    .prologue
    .line 1970776
    sget-object v0, LX/DA1;->CALL_LOG:LX/DA1;

    return-object v0
.end method

.method public final b()LX/DA4;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1970770
    sget-object v0, LX/DA3;->a:[Ljava/lang/String;

    const/4 v4, 0x0

    .line 1970771
    iget-object v1, p0, LX/DA3;->e:LX/1Ml;

    const-string v2, "android.permission.READ_CALL_LOG"

    invoke-virtual {v1, v2}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1970772
    :cond_0
    :goto_0
    move-object v0, v4

    .line 1970773
    return-object v0

    .line 1970774
    :cond_1
    iget-object v1, p0, LX/DA3;->c:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "_id"

    move-object v3, v0

    move-object v5, v4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1970775
    if-eqz v1, :cond_0

    invoke-static {v1, p0}, LX/DA5;->a(Landroid/database/Cursor;LX/DA2;)LX/DA4;

    move-result-object v4

    goto :goto_0
.end method
