.class public LX/DLb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/1Ck;

.field public final d:Ljava/lang/String;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/DLL;

.field public g:Ljava/lang/String;

.field public h:Z


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/1Ck;Ljava/lang/String;LX/DLL;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/DLL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1988383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1988384
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DLb;->h:Z

    .line 1988385
    iput-object p1, p0, LX/DLb;->a:LX/0tX;

    .line 1988386
    iput-object p2, p0, LX/DLb;->b:Ljava/util/concurrent/ExecutorService;

    .line 1988387
    iput-object p3, p0, LX/DLb;->c:LX/1Ck;

    .line 1988388
    iput-object p4, p0, LX/DLb;->d:Ljava/lang/String;

    .line 1988389
    iput-object p5, p0, LX/DLb;->f:LX/DLL;

    .line 1988390
    return-void
.end method
