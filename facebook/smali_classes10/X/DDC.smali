.class public LX/DDC;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DDF;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DDC",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DDF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975091
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1975092
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DDC;->b:LX/0Zi;

    .line 1975093
    iput-object p1, p0, LX/DDC;->a:LX/0Ot;

    .line 1975094
    return-void
.end method

.method public static a(LX/0QB;)LX/DDC;
    .locals 4

    .prologue
    .line 1975121
    const-class v1, LX/DDC;

    monitor-enter v1

    .line 1975122
    :try_start_0
    sget-object v0, LX/DDC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975123
    sput-object v2, LX/DDC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975124
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975125
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975126
    new-instance v3, LX/DDC;

    const/16 p0, 0x1f56

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DDC;-><init>(LX/0Ot;)V

    .line 1975127
    move-object v0, v3

    .line 1975128
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975129
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DDC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975130
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975131
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 13

    .prologue
    .line 1975095
    check-cast p1, LX/DDA;

    .line 1975096
    iget-object v0, p0, LX/DDC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DDF;

    iget-object v1, p1, LX/DDA;->a:LX/DDZ;

    iget-object v2, p1, LX/DDA;->b:LX/DDY;

    iget-object v3, p1, LX/DDA;->c:LX/1Po;

    .line 1975097
    iget-boolean v4, v2, LX/DDY;->a:Z

    move v10, v4

    .line 1975098
    iget-object v11, v1, LX/DDZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975099
    iget-object v12, v1, LX/DDZ;->b:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 1975100
    if-eqz v12, :cond_0

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    if-eqz v4, :cond_0

    if-eqz v11, :cond_0

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1975101
    :cond_0
    :goto_0
    return-void

    .line 1975102
    :cond_1
    if-nez v10, :cond_3

    .line 1975103
    const/4 v4, 0x1

    invoke-static {v4, v3, v11, v2}, LX/DDF;->a(ZLX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/DDY;)V

    .line 1975104
    iget-object v4, v0, LX/DDF;->b:LX/DWD;

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3}, LX/1Po;->c()LX/1PT;

    move-result-object v7

    invoke-static {v7}, LX/DD1;->a(LX/1PT;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, LX/DWD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1975105
    new-instance v5, LX/DDD;

    invoke-direct {v5, v0, v3, v11, v2}, LX/DDD;-><init>(LX/DDF;LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/DDY;)V

    iget-object v6, v0, LX/DDF;->c:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1975106
    :goto_1
    const-string v8, ""

    .line 1975107
    iget-object v4, v11, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1975108
    check-cast v4, LX/16h;

    invoke-static {v12, v4}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v4

    .line 1975109
    if-eqz v4, :cond_2

    .line 1975110
    invoke-virtual {v4}, LX/162;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1975111
    :cond_2
    iget-object v4, v0, LX/DDF;->g:LX/1g8;

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->j()Ljava/lang/String;

    move-result-object v7

    move v9, v10

    invoke-virtual/range {v4 .. v9}, LX/1g8;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1975112
    :cond_3
    const/4 v4, 0x0

    invoke-static {v4, v3, v11, v2}, LX/DDF;->a(ZLX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/DDY;)V

    .line 1975113
    new-instance v5, LX/4G2;

    invoke-direct {v5}, LX/4G2;-><init>()V

    iget-object v4, v0, LX/DDF;->d:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, LX/4G2;->a(Ljava/lang/String;)LX/4G2;

    move-result-object v4

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4G2;->b(Ljava/lang/String;)LX/4G2;

    move-result-object v4

    invoke-interface {v3}, LX/1Po;->c()LX/1PT;

    move-result-object v5

    invoke-static {v5}, LX/DD1;->a(LX/1PT;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4G2;->d(Ljava/lang/String;)LX/4G2;

    move-result-object v4

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4G2;->c(Ljava/lang/String;)LX/4G2;

    move-result-object v4

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->j()Ljava/lang/String;

    move-result-object v5

    .line 1975114
    const-string v6, "recommendation_key"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975115
    move-object v4, v4

    .line 1975116
    invoke-static {}, LX/DV9;->c()LX/DV6;

    move-result-object v5

    .line 1975117
    const-string v6, "input"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1975118
    iget-object v4, v0, LX/DDF;->e:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1975119
    new-instance v5, LX/DDE;

    invoke-direct {v5, v0, v3, v11, v2}, LX/DDE;-><init>(LX/DDF;LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/DDY;)V

    iget-object v6, v0, LX/DDF;->c:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_1
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1975120
    const v0, 0xc2c4e33

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1975076
    check-cast p2, LX/DDA;

    .line 1975077
    iget-object v0, p0, LX/DDC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DDF;

    iget-object v1, p2, LX/DDA;->b:LX/DDY;

    .line 1975078
    iget-object v2, v0, LX/DDF;->a:LX/2g9;

    invoke-virtual {v2, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v2

    const/16 p0, 0x101

    invoke-virtual {v2, p0}, LX/2gA;->h(I)LX/2gA;

    move-result-object p0

    .line 1975079
    iget-boolean v2, v1, LX/DDY;->a:Z

    move v2, v2

    .line 1975080
    if-eqz v2, :cond_0

    const v2, 0x7f081c3f

    :goto_0
    invoke-virtual {p0, v2}, LX/2gA;->i(I)LX/2gA;

    move-result-object p0

    .line 1975081
    iget-boolean v2, v1, LX/DDY;->a:Z

    move v2, v2

    .line 1975082
    if-eqz v2, :cond_1

    const v2, 0x7f081c40

    :goto_1
    invoke-virtual {p0, v2}, LX/2gA;->j(I)LX/2gA;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const p0, 0x7f0b1250

    invoke-interface {v2, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const/4 p0, 0x1

    const p2, 0x7f0b1253

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x3

    const p2, 0x7f0b1254

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x0

    const p2, 0x7f0b1254

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x2

    const p2, 0x7f0b1254

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    .line 1975083
    const p0, 0xc2c4e33

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1975084
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1975085
    return-object v0

    :cond_0
    const v2, 0x7f081c3d

    goto :goto_0

    :cond_1
    const v2, 0x7f081c3e

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1975086
    invoke-static {}, LX/1dS;->b()V

    .line 1975087
    iget v0, p1, LX/1dQ;->b:I

    .line 1975088
    packed-switch v0, :pswitch_data_0

    .line 1975089
    :goto_0
    return-object v1

    .line 1975090
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/DDC;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xc2c4e33
        :pswitch_0
    .end packed-switch
.end method
