.class public LX/ERG;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/ERG;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2120673
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2120674
    sget-object v0, LX/0ax;->hC:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->MOMENTS_UPSELL_PROMOTION_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2120675
    return-void
.end method

.method public static a(LX/0QB;)LX/ERG;
    .locals 3

    .prologue
    .line 2120676
    sget-object v0, LX/ERG;->a:LX/ERG;

    if-nez v0, :cond_1

    .line 2120677
    const-class v1, LX/ERG;

    monitor-enter v1

    .line 2120678
    :try_start_0
    sget-object v0, LX/ERG;->a:LX/ERG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2120679
    if-eqz v2, :cond_0

    .line 2120680
    :try_start_1
    new-instance v0, LX/ERG;

    invoke-direct {v0}, LX/ERG;-><init>()V

    .line 2120681
    move-object v0, v0

    .line 2120682
    sput-object v0, LX/ERG;->a:LX/ERG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2120683
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2120684
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2120685
    :cond_1
    sget-object v0, LX/ERG;->a:LX/ERG;

    return-object v0

    .line 2120686
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2120687
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
