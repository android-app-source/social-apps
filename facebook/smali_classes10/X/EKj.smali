.class public LX/EKj;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EKh;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EKk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2107153
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EKj;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EKk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107154
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2107155
    iput-object p1, p0, LX/EKj;->b:LX/0Ot;

    .line 2107156
    return-void
.end method

.method public static a(LX/0QB;)LX/EKj;
    .locals 4

    .prologue
    .line 2107142
    const-class v1, LX/EKj;

    monitor-enter v1

    .line 2107143
    :try_start_0
    sget-object v0, LX/EKj;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107144
    sput-object v2, LX/EKj;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107145
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107146
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107147
    new-instance v3, LX/EKj;

    const/16 p0, 0x33d2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EKj;-><init>(LX/0Ot;)V

    .line 2107148
    move-object v0, v3

    .line 2107149
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107150
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107151
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107152
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2107127
    iget-object v0, p0, LX/EKj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 2107128
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/16 v1, 0x12

    invoke-interface {v0, p2, v1}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v1

    const v2, 0x7f0a009f

    invoke-virtual {v1, v2}, LX/25Q;->i(I)LX/25Q;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b0033

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b010f

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x3

    const v3, 0x7f0b010f

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    const v2, 0x7f082299

    invoke-virtual {v1, v2}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a00a4

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    .line 2107129
    const v2, 0x2771b959

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 2107130
    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2107131
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2107132
    invoke-static {}, LX/1dS;->b()V

    .line 2107133
    iget v0, p1, LX/1dQ;->b:I

    .line 2107134
    packed-switch v0, :pswitch_data_0

    .line 2107135
    :goto_0
    return-object v2

    .line 2107136
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2107137
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 2107138
    iget-object p1, p0, LX/EKj;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/EKk;

    .line 2107139
    iget-object p2, p1, LX/EKk;->c:LX/17Y;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->jb:Ljava/lang/String;

    invoke-interface {p2, p0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    .line 2107140
    iget-object p0, p1, LX/EKk;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p0, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2107141
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2771b959
        :pswitch_0
    .end packed-switch
.end method
