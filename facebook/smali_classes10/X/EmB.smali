.class public final LX/EmB;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "LX/EmC;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2KK;


# direct methods
.method public constructor <init>(LX/2KK;)V
    .locals 0

    .prologue
    .line 2165192
    iput-object p1, p0, LX/EmB;->a:LX/2KK;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2165193
    check-cast p1, [LX/EmC;

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2165194
    const-class v1, LX/2KK;

    monitor-enter v1

    .line 2165195
    :try_start_0
    sget-boolean v0, LX/2KK;->q:Z

    if-eqz v0, :cond_0

    .line 2165196
    monitor-exit v1

    .line 2165197
    :goto_0
    return-object v3

    .line 2165198
    :cond_0
    const/4 v0, 0x1

    .line 2165199
    sput-boolean v0, LX/2KK;->q:Z

    .line 2165200
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2165201
    iget-object v0, p0, LX/EmB;->a:LX/2KK;

    const-string v1, "attempting_connection"

    invoke-static {v0, v1}, LX/2KK;->b$redex0(LX/2KK;Ljava/lang/String;)V

    .line 2165202
    aget-object v0, p1, v2

    const/4 v1, 0x0

    .line 2165203
    :try_start_1
    new-instance v4, Ljava/net/InetSocketAddress;

    iget-object v5, v0, LX/EmC;->c:Ljava/net/InetAddress;

    iget v6, v0, LX/EmC;->d:I

    invoke-direct {v4, v5, v6}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 2165204
    new-instance v4, Ljava/net/Socket;

    iget-object v5, v0, LX/EmC;->c:Ljava/net/InetAddress;

    iget v6, v0, LX/EmC;->d:I

    invoke-direct {v4, v5, v6}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    iput-object v4, v0, LX/EmC;->e:Ljava/net/Socket;

    .line 2165205
    iget-object v4, v0, LX/EmC;->e:Ljava/net/Socket;

    iget v5, v0, LX/EmC;->b:I

    mul-int/lit16 v5, v5, 0x3e8

    invoke-virtual {v4, v5}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2165206
    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2165207
    if-eqz v0, :cond_2

    .line 2165208
    aget-object v0, p1, v2

    .line 2165209
    :try_start_2
    iget-object v1, v0, LX/EmC;->e:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 2165210
    iget v1, v0, LX/EmC;->f:I

    new-array v1, v1, [B

    .line 2165211
    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2165212
    :goto_2
    move-object v0, v1

    .line 2165213
    if-eqz v0, :cond_1

    .line 2165214
    iget-object v1, p0, LX/EmB;->a:LX/2KK;

    .line 2165215
    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 2165216
    const-string v4, "data_token_read"

    .line 2165217
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "ott_wilde_tcp_device_discovery"

    invoke-direct {v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2165218
    const-string v0, "ott_device_discovery"

    .line 2165219
    iput-object v0, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2165220
    const-string v0, "event_type"

    invoke-virtual {v5, v0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2165221
    const-string v0, "token"

    invoke-virtual {v5, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2165222
    iget-object v0, v1, LX/2KK;->c:LX/0Zb;

    invoke-interface {v0, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2165223
    new-instance v4, LX/4De;

    invoke-direct {v4}, LX/4De;-><init>()V

    .line 2165224
    const-string v5, "token"

    invoke-virtual {v4, v5, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2165225
    move-object v2, v4

    .line 2165226
    new-instance v4, LX/EmD;

    invoke-direct {v4}, LX/EmD;-><init>()V

    .line 2165227
    const-string v5, "0"

    invoke-virtual {v4, v5, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2165228
    iget-object v2, v1, LX/2KK;->k:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2165229
    :cond_1
    :goto_3
    const-class v1, LX/2KK;

    monitor-enter v1

    .line 2165230
    const/4 v0, 0x0

    .line 2165231
    :try_start_3
    sput-boolean v0, LX/2KK;->q:Z

    .line 2165232
    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2165233
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2165234
    :cond_2
    iget-object v0, p0, LX/EmB;->a:LX/2KK;

    const-string v1, "failed_to_open_connection"

    invoke-static {v0, v1}, LX/2KK;->b$redex0(LX/2KK;Ljava/lang/String;)V

    goto :goto_3

    .line 2165235
    :catch_0
    goto :goto_1

    .line 2165236
    :catch_1
    goto :goto_1

    .line 2165237
    :catch_2
    move-exception v1

    .line 2165238
    sget-object v2, LX/EmC;->a:Ljava/lang/Class;

    const-string v4, "failed reading from socket"

    invoke-static {v2, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2165239
    const/4 v1, 0x0

    goto :goto_2
.end method
