.class public LX/EyU;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/EyQ;
.implements LX/EyT;


# instance fields
.field public j:Landroid/view/View;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/TextView;

.field public m:Lcom/facebook/friends/ui/SmartButtonLite;

.field public n:Lcom/facebook/friends/ui/SmartButtonLite;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2186318
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2186319
    const v0, 0x7f0306e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2186320
    const v0, 0x7f0d128c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EyU;->j:Landroid/view/View;

    .line 2186321
    const v0, 0x7f0d127d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EyU;->k:Landroid/widget/TextView;

    .line 2186322
    const v0, 0x7f0d127e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EyU;->l:Landroid/widget/TextView;

    .line 2186323
    const v0, 0x7f0d1283

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2186324
    const v0, 0x7f0d1284

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, LX/EyU;->n:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2186325
    iget-object v0, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    const p1, 0x7f0101e9

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 2186326
    iget-object v0, p0, LX/EyU;->n:Lcom/facebook/friends/ui/SmartButtonLite;

    const p1, 0x7f0101e3

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 2186327
    return-void
.end method


# virtual methods
.method public final a(LX/EyR;Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186332
    iget-object v0, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2186333
    iget-object v0, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    iget v1, p1, LX/EyR;->backgroundRes:I

    iget v2, p1, LX/EyR;->textAppearanceRes:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(II)V

    .line 2186334
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186330
    iget-object v0, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2186331
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186328
    iget-object v0, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2186329
    return-void
.end method

.method public getSubtitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2186317
    iget-object v0, p0, LX/EyU;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2186316
    iget-object v0, p0, LX/EyU;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setActionButtonContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186314
    iget-object v0, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2186315
    return-void
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186335
    iget-object v0, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186336
    return-void
.end method

.method public setFriendRequestButtonsVisible(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2186309
    iget-object v3, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186310
    iget-object v0, p0, LX/EyU;->n:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186311
    return-void

    :cond_0
    move v0, v2

    .line 2186312
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2186313
    goto :goto_1
.end method

.method public setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186307
    iget-object v0, p0, LX/EyU;->n:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186308
    return-void
.end method

.method public setNegativeButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2186305
    iget-object v0, p0, LX/EyU;->n:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 2186306
    return-void
.end method

.method public setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2186303
    iget-object v0, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186304
    return-void
.end method

.method public setShowActionButton(Z)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 2186288
    iget-object v2, p0, LX/EyU;->m:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186289
    iget-object v0, p0, LX/EyU;->n:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2186290
    return-void

    :cond_0
    move v0, v1

    .line 2186291
    goto :goto_0
.end method

.method public setSubtitleText(I)V
    .locals 1

    .prologue
    .line 2186301
    invoke-virtual {p0}, LX/EyU;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EyU;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2186302
    return-void
.end method

.method public setSubtitleText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2186296
    iget-object v0, p0, LX/EyU;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2186297
    iget-object v1, p0, LX/EyU;->l:Landroid/widget/TextView;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2186298
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 2186299
    return-void

    .line 2186300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2186292
    iget-object v0, p0, LX/EyU;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2186293
    iget-object v1, p0, LX/EyU;->k:Landroid/widget/TextView;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2186294
    return-void

    .line 2186295
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
