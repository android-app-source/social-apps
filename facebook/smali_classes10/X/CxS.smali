.class public LX/CxS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CxP;


# instance fields
.field private final a:LX/CzA;


# direct methods
.method public constructor <init>(LX/CzA;)V
    .locals 0
    .param p1    # LX/CzA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951715
    iput-object p1, p0, LX/CxS;->a:LX/CzA;

    .line 1951716
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I
    .locals 3

    .prologue
    .line 1951717
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/CxS;->a:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1951718
    iget-object v0, p0, LX/CxS;->a:LX/CzA;

    invoke-virtual {v0, v1}, LX/CzA;->b(I)LX/Cyv;

    move-result-object v0

    .line 1951719
    instance-of v2, v0, LX/Cz3;

    if-eqz v2, :cond_0

    .line 1951720
    check-cast v0, LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1951721
    :goto_1
    return v1

    .line 1951722
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1951723
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final b(LX/CzL;)I
    .locals 1

    .prologue
    .line 1951724
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 1951725
    invoke-virtual {p0, v0}, LX/CxS;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v0

    return v0
.end method
