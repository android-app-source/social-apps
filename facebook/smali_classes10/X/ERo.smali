.class public LX/ERo;
.super Landroid/database/ContentObserver;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Landroid/net/Uri;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation runtime Lcom/facebook/vault/service/ForVaultService;
    .end annotation
.end field

.field public c:LX/2TK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2121516
    const-class v0, LX/ERo;

    sput-object v0, LX/ERo;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2121521
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 2121522
    return-void
.end method


# virtual methods
.method public final deliverSelfNotifications()Z
    .locals 1

    .prologue
    .line 2121520
    const/4 v0, 0x0

    return v0
.end method

.method public final onChange(Z)V
    .locals 2

    .prologue
    .line 2121517
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Detected change from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/ERo;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2121518
    iget-object v0, p0, LX/ERo;->c:LX/2TK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2TK;->c(I)V

    .line 2121519
    return-void
.end method
