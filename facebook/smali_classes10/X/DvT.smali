.class public LX/DvT;
.super LX/Dcc;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/DvT;


# instance fields
.field private final a:LX/DvI;

.field private final b:LX/0tX;

.field private final c:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/DvI;LX/0tX;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2058555
    invoke-direct {p0}, LX/Dcc;-><init>()V

    .line 2058556
    iput-object p1, p0, LX/DvT;->c:Ljava/util/concurrent/ExecutorService;

    .line 2058557
    iput-object p2, p0, LX/DvT;->a:LX/DvI;

    .line 2058558
    iput-object p3, p0, LX/DvT;->b:LX/0tX;

    .line 2058559
    return-void
.end method

.method public static a(LX/0QB;)LX/DvT;
    .locals 6

    .prologue
    .line 2058560
    sget-object v0, LX/DvT;->d:LX/DvT;

    if-nez v0, :cond_1

    .line 2058561
    const-class v1, LX/DvT;

    monitor-enter v1

    .line 2058562
    :try_start_0
    sget-object v0, LX/DvT;->d:LX/DvT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2058563
    if-eqz v2, :cond_0

    .line 2058564
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2058565
    new-instance p0, LX/DvT;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/DvI;->a(LX/0QB;)LX/DvI;

    move-result-object v4

    check-cast v4, LX/DvI;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-direct {p0, v3, v4, v5}, LX/DvT;-><init>(Ljava/util/concurrent/ExecutorService;LX/DvI;LX/0tX;)V

    .line 2058566
    move-object v0, p0

    .line 2058567
    sput-object v0, LX/DvT;->d:LX/DvT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2058568
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2058569
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2058570
    :cond_1
    sget-object v0, LX/DvT;->d:LX/DvT;

    return-object v0

    .line 2058571
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2058572
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;",
            "IZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2058573
    new-instance v0, LX/8IA;

    invoke-direct {v0}, LX/8IA;-><init>()V

    move-object v0, v0

    .line 2058574
    const-string v1, "count"

    const-string v2, "20"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058575
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2058576
    const-string v1, "before"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058577
    :cond_0
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2058578
    const-string v1, "after"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058579
    :cond_1
    iget-object v1, p0, LX/DvT;->a:LX/DvI;

    invoke-virtual {v1, v0}, LX/DvI;->a(LX/0gW;)LX/0gW;

    .line 2058580
    iget-object v1, p0, LX/DvT;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    if-eqz p5, :cond_2

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    invoke-virtual {v2, v0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2058581
    new-instance v1, LX/DvS;

    invoke-direct {v1}, LX/DvS;-><init>()V

    iget-object v2, p0, LX/DvT;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2058582
    return-object v0

    .line 2058583
    :cond_2
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
