.class public final LX/DzC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DzG;


# direct methods
.method public constructor <init>(LX/DzG;)V
    .locals 0

    .prologue
    .line 2066494
    iput-object p1, p0, LX/DzC;->a:LX/DzG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x21c1a368

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2066495
    iget-object v0, p0, LX/DzC;->a:LX/DzG;

    iget-object v0, v0, LX/DzG;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2066496
    if-nez v0, :cond_0

    .line 2066497
    const v0, 0x567fdd30

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2066498
    :goto_0
    return-void

    .line 2066499
    :cond_0
    iget-object v2, p0, LX/DzC;->a:LX/DzG;

    const-string v3, "niem_network_settings_click"

    invoke-static {v2, v3}, LX/DzG;->a$redex0(LX/DzG;Ljava/lang/String;)V

    .line 2066500
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.settings.AIRPLANE_MODE_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2066501
    :try_start_0
    iget-object v3, p0, LX/DzC;->a:LX/DzG;

    iget-object v3, v3, LX/DzG;->q:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2066502
    :goto_1
    const v0, 0x14a9f66b

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2066503
    :catch_0
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2066504
    new-instance v2, LX/27k;

    const v3, 0x7f081766

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    .line 2066505
    const-string v0, "PlacePickerNIEM"

    .line 2066506
    iput-object v0, v2, LX/27k;->g:Ljava/lang/String;

    .line 2066507
    iget-object v0, p0, LX/DzC;->a:LX/DzG;

    iget-object v0, v0, LX/DzG;->p:LX/0kL;

    invoke-virtual {v0, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1
.end method
