.class public LX/EAH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0SI;

.field private final d:LX/01T;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/0SI;LX/01T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2085431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2085432
    iput-object p1, p0, LX/EAH;->a:LX/0tX;

    .line 2085433
    iput-object p2, p0, LX/EAH;->b:LX/1Ck;

    .line 2085434
    iput-object p3, p0, LX/EAH;->c:LX/0SI;

    .line 2085435
    iput-object p4, p0, LX/EAH;->d:LX/01T;

    .line 2085436
    return-void
.end method

.method public static a(LX/EAH;LX/0zO;)V
    .locals 2

    .prologue
    .line 2085437
    iget-object v0, p0, LX/EAH;->d:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-eq v0, v1, :cond_0

    .line 2085438
    iget-object v0, p0, LX/EAH;->c:LX/0SI;

    invoke-interface {v0}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2085439
    iput-object v0, p1, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2085440
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/EAH;
    .locals 5

    .prologue
    .line 2085441
    new-instance v4, LX/EAH;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v2

    check-cast v2, LX/0SI;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-direct {v4, v0, v1, v2, v3}, LX/EAH;-><init>(LX/0tX;LX/1Ck;LX/0SI;LX/01T;)V

    .line 2085442
    return-object v4
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2085443
    iget-object v0, p0, LX/EAH;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2085444
    return-void
.end method

.method public final a(Ljava/lang/String;ILX/EA0;Ljava/lang/String;)V
    .locals 4
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2085445
    new-instance v0, LX/EB3;

    invoke-direct {v0}, LX/EB3;-><init>()V

    move-object v0, v0

    .line 2085446
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2085447
    const-string v1, "story_count"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2085448
    const-string v1, "feed_story_render_location"

    const-string v2, "reviews_feed"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2085449
    const-string v1, "last_cursor"

    invoke-virtual {v0, v1, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2085450
    const-string v1, "action_links_location"

    const-string v2, "reviews_feed"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2085451
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2085452
    invoke-static {p0, v0}, LX/EAH;->a(LX/EAH;LX/0zO;)V

    .line 2085453
    iget-object v1, p0, LX/EAH;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2085454
    iget-object v1, p0, LX/EAH;->b:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "key_load_review_stories"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/EAF;

    invoke-direct {v3, p0, v0}, LX/EAF;-><init>(LX/EAH;LX/1Zp;)V

    new-instance v0, LX/EAG;

    invoke-direct {v0, p0, p3}, LX/EAG;-><init>(LX/EAH;LX/EA0;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2085455
    return-void
.end method
