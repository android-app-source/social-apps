.class public LX/EeS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/1wh;

.field public final b:LX/EeU;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/app/DownloadManager;

.field private final f:Landroid/os/Handler;

.field private final g:I

.field private final h:LX/Eeb;

.field private final i:LX/Eef;

.field public final j:LX/Eej;

.field public final k:LX/Eek;

.field private final l:LX/Eel;

.field private final m:LX/Een;

.field private final n:LX/1wq;

.field public o:LX/EeX;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1sX;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1sX;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private r:Z

.field private s:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final t:Landroid/content/BroadcastReceiver;

.field private final u:LX/EeO;

.field private final v:LX/EeO;

.field public final w:LX/EeO;


# direct methods
.method public constructor <init>(LX/EeX;LX/EeU;LX/1wh;Landroid/content/SharedPreferences;Landroid/content/Context;Landroid/app/DownloadManager;LX/1wq;Landroid/os/Handler;ILX/0Or;LX/0Or;LX/0Or;LX/1sZ;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EeX;",
            "LX/EeU;",
            "LX/1wh;",
            "Landroid/content/SharedPreferences;",
            "Landroid/content/Context;",
            "Landroid/app/DownloadManager;",
            "LX/1wq;",
            "Landroid/os/Handler;",
            "I",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/appupdate/ApkDiffPatcher;",
            ">;",
            "LX/1sZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2152634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152635
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, LX/EeS;->p:Ljava/util/Set;

    .line 2152636
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, LX/EeS;->q:Ljava/util/Set;

    .line 2152637
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/EeS;->r:Z

    .line 2152638
    new-instance v2, LX/EeN;

    invoke-direct {v2, p0}, LX/EeN;-><init>(LX/EeS;)V

    iput-object v2, p0, LX/EeS;->t:Landroid/content/BroadcastReceiver;

    .line 2152639
    new-instance v2, LX/EeP;

    invoke-direct {v2, p0}, LX/EeP;-><init>(LX/EeS;)V

    iput-object v2, p0, LX/EeS;->u:LX/EeO;

    .line 2152640
    new-instance v2, LX/EeQ;

    invoke-direct {v2, p0}, LX/EeQ;-><init>(LX/EeS;)V

    iput-object v2, p0, LX/EeS;->v:LX/EeO;

    .line 2152641
    new-instance v2, LX/EeR;

    invoke-direct {v2, p0}, LX/EeR;-><init>(LX/EeS;)V

    iput-object v2, p0, LX/EeS;->w:LX/EeO;

    .line 2152642
    iput-object p1, p0, LX/EeS;->o:LX/EeX;

    .line 2152643
    iput-object p2, p0, LX/EeS;->b:LX/EeU;

    .line 2152644
    iput-object p3, p0, LX/EeS;->a:LX/1wh;

    .line 2152645
    iput-object p4, p0, LX/EeS;->c:Landroid/content/SharedPreferences;

    .line 2152646
    iput-object p5, p0, LX/EeS;->d:Landroid/content/Context;

    .line 2152647
    iput-object p6, p0, LX/EeS;->e:Landroid/app/DownloadManager;

    .line 2152648
    move-object/from16 v0, p8

    iput-object v0, p0, LX/EeS;->f:Landroid/os/Handler;

    .line 2152649
    move/from16 v0, p9

    iput v0, p0, LX/EeS;->g:I

    .line 2152650
    iput-object p7, p0, LX/EeS;->n:LX/1wq;

    .line 2152651
    new-instance v2, LX/Eeb;

    invoke-direct {v2}, LX/Eeb;-><init>()V

    iput-object v2, p0, LX/EeS;->h:LX/Eeb;

    .line 2152652
    new-instance v2, LX/Eej;

    move-object/from16 v0, p10

    move-object/from16 v1, p11

    invoke-direct {v2, p6, v0, v1}, LX/Eej;-><init>(Landroid/app/DownloadManager;LX/0Or;LX/0Or;)V

    iput-object v2, p0, LX/EeS;->j:LX/Eej;

    .line 2152653
    new-instance v2, LX/Eef;

    invoke-direct {v2, p6}, LX/Eef;-><init>(Landroid/app/DownloadManager;)V

    iput-object v2, p0, LX/EeS;->i:LX/Eef;

    .line 2152654
    new-instance v2, LX/Een;

    iget-object v3, p0, LX/EeS;->a:LX/1wh;

    iget-object v4, p0, LX/EeS;->n:LX/1wq;

    invoke-direct {v2, v3, p6, v4}, LX/Een;-><init>(LX/1wh;Landroid/app/DownloadManager;LX/1wq;)V

    iput-object v2, p0, LX/EeS;->m:LX/Een;

    .line 2152655
    new-instance v2, LX/Eel;

    iget-object v3, p0, LX/EeS;->m:LX/Een;

    iget-object v4, p0, LX/EeS;->a:LX/1wh;

    move-object/from16 v0, p13

    move-object/from16 v1, p12

    invoke-direct {v2, v0, v3, v4, v1}, LX/Eel;-><init>(LX/1sZ;LX/Een;LX/1wh;LX/0Or;)V

    iput-object v2, p0, LX/EeS;->l:LX/Eel;

    .line 2152656
    new-instance v2, LX/Eek;

    iget-object v5, p0, LX/EeS;->l:LX/Eel;

    iget-object v6, p0, LX/EeS;->m:LX/Een;

    iget-object v7, p0, LX/EeS;->a:LX/1wh;

    move-object/from16 v3, p13

    move-object v4, p6

    invoke-direct/range {v2 .. v7}, LX/Eek;-><init>(LX/1sZ;Landroid/app/DownloadManager;LX/Eel;LX/Een;LX/1wh;)V

    iput-object v2, p0, LX/EeS;->k:LX/Eek;

    .line 2152657
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/EeS;LX/EeO;J)V
    .locals 4

    .prologue
    .line 2152658
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EeS;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/appupdate/AppUpdateOperation$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/appupdate/AppUpdateOperation$2;-><init>(LX/EeS;LX/EeO;)V

    const v2, 0x2ce28dd8

    invoke-static {v0, v1, p2, p3, v2}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152659
    monitor-exit p0

    return-void

    .line 2152660
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/EeS;LX/EeX;)V
    .locals 1
    .param p0    # LX/EeS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2152661
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/EeS;->b$redex0(LX/EeS;LX/EeX;)Z

    move-result v0

    .line 2152662
    if-eqz v0, :cond_0

    .line 2152663
    iget-object v0, p0, LX/EeS;->b:LX/EeU;

    invoke-virtual {v0, p1}, LX/EeU;->a(LX/EeX;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152664
    :cond_0
    monitor-exit p0

    return-void

    .line 2152665
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/EeS;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2152666
    monitor-enter p0

    :try_start_0
    const-string v1, "AppUpdateLib"

    const-string v2, "Operation failed: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, p1, v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152667
    instance-of v1, p1, LX/Eex;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, LX/Eex;

    move-object v1, v0

    .line 2152668
    iget-object v0, v1, LX/Eex;->mErrorName:Ljava/lang/String;

    move-object v1, v0

    .line 2152669
    move-object v2, v1

    .line 2152670
    :goto_0
    instance-of v1, p1, LX/Eex;

    if-eqz v1, :cond_1

    move-object v0, p1

    check-cast v0, LX/Eex;

    move-object v1, v0

    .line 2152671
    iget-object v0, v1, LX/Eex;->mCategory:Ljava/lang/String;

    move-object v1, v0

    .line 2152672
    :goto_1
    invoke-virtual {p0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    invoke-virtual {v0}, LX/EeX;->c()Lorg/json/JSONObject;

    move-result-object v0

    .line 2152673
    const-string v3, "error_name"

    invoke-static {v0, v3, v2}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2152674
    move-object v3, v0

    .line 2152675
    iget-object v4, p0, LX/EeS;->a:LX/1wh;

    invoke-virtual {v4, v2, v3, p1}, LX/1wh;->a(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/Throwable;)V

    .line 2152676
    iget-object v2, p0, LX/EeS;->a:LX/1wh;

    invoke-virtual {v2, v1, v3}, LX/1wh;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2152677
    iget-object v2, p0, LX/EeS;->a:LX/1wh;

    iget-object v3, p0, LX/EeS;->o:LX/EeX;

    iget-object v3, v3, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v4, p0, LX/EeS;->o:LX/EeX;

    invoke-virtual {v4}, LX/EeX;->d()LX/Eeh;

    move-result-object v4

    const-string v5, "task_failure"

    invoke-virtual {v2, v1, v3, v4, v5}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152678
    monitor-exit p0

    return-void

    .line 2152679
    :cond_0
    :try_start_1
    const-string v1, "unknown_appupdate_operation_failure"

    move-object v2, v1

    goto :goto_0

    .line 2152680
    :cond_1
    const-string v1, "unknown_appupdate_operation_failure"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2152681
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static declared-synchronized b$redex0(LX/EeS;LX/EeX;)Z
    .locals 2
    .param p0    # LX/EeS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2152682
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/EeS;->o:LX/EeX;

    iget-object v0, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x8

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2152683
    iput-object p1, p0, LX/EeS;->o:LX/EeX;

    .line 2152684
    invoke-direct {p0}, LX/EeS;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152685
    const/4 v0, 0x1

    .line 2152686
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2152687
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static c$redex0(LX/EeS;LX/EeX;)V
    .locals 6

    .prologue
    .line 2152688
    iget-wide v0, p1, LX/EeX;->downloadId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 2152689
    iget-object v0, p0, LX/EeS;->e:Landroid/app/DownloadManager;

    const/4 v1, 0x1

    new-array v1, v1, [J

    const/4 v2, 0x0

    iget-wide v4, p1, LX/EeX;->downloadId:J

    aput-wide v4, v1, v2

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->remove([J)I

    .line 2152690
    :cond_0
    iget-object v0, p1, LX/EeX;->localFile:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 2152691
    iget-object v0, p1, LX/EeX;->localFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2152692
    :cond_1
    return-void
.end method

.method private declared-synchronized h()V
    .locals 4

    .prologue
    .line 2152693
    monitor-enter p0

    :try_start_0
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_0

    .line 2152694
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Starting polling for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152695
    :cond_0
    iget-object v0, p0, LX/EeS;->i:LX/Eef;

    .line 2152696
    iget-object v1, v0, LX/Eef;->d:LX/EeO;

    move-object v0, v1

    .line 2152697
    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152698
    monitor-exit p0

    return-void

    .line 2152699
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized i()V
    .locals 4

    .prologue
    .line 2152572
    monitor-enter p0

    :try_start_0
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_0

    .line 2152573
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Stopping polling for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152574
    :cond_0
    iget-object v0, p0, LX/EeS;->i:LX/Eef;

    .line 2152575
    iget-object v1, v0, LX/Eef;->f:LX/EeO;

    move-object v0, v1

    .line 2152576
    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152577
    monitor-exit p0

    return-void

    .line 2152578
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized j(LX/EeS;)V
    .locals 4

    .prologue
    .line 2152700
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/EeS;->r:Z

    if-nez v0, :cond_0

    .line 2152701
    iget-object v0, p0, LX/EeS;->d:Landroid/content/Context;

    iget-object v1, p0, LX/EeS;->t:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2152702
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EeS;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152703
    :cond_0
    monitor-exit p0

    return-void

    .line 2152704
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()V
    .locals 2

    .prologue
    .line 2152705
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/EeS;->r:Z

    if-eqz v0, :cond_0

    .line 2152706
    iget-object v0, p0, LX/EeS;->d:Landroid/content/Context;

    iget-object v1, p0, LX/EeS;->t:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2152707
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EeS;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152708
    :cond_0
    monitor-exit p0

    return-void

    .line 2152709
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized l(LX/EeS;)V
    .locals 3

    .prologue
    .line 2152629
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    iget-object v0, v0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget v0, v0, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    .line 2152630
    invoke-virtual {p0}, LX/EeS;->e()LX/EeX;

    move-result-object v1

    iget-object v1, v1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v1, v1, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    .line 2152631
    iget-object v2, p0, LX/EeS;->c:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {v1}, LX/Ef3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152632
    monitor-exit p0

    return-void

    .line 2152633
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized m()V
    .locals 3

    .prologue
    .line 2152710
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EeS;->p:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sX;

    .line 2152711
    iget-object v2, p0, LX/EeS;->o:LX/EeX;

    invoke-interface {v0, p0, v2}, LX/1sX;->a(LX/EeS;LX/EeX;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2152712
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2152713
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/EeS;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sX;

    .line 2152714
    iget-object v2, p0, LX/EeS;->o:LX/EeX;

    invoke-interface {v0, p0, v2}, LX/1sX;->a(LX/EeS;LX/EeX;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2152715
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 2152600
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/EeS;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 2152601
    :goto_0
    monitor-exit p0

    return-void

    .line 2152602
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/EeS;->o:LX/EeX;

    iget-boolean v0, v0, LX/EeX;->isSelfUpdate:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EeS;->o:LX/EeX;

    iget-object v0, v0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget v0, v0, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    iget v1, p0, LX/EeS;->g:I

    if-gt v0, v1, :cond_2

    .line 2152603
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_1

    .line 2152604
    const-string v0, "Discarding operation %s, version is not newer than current (%d <= %d)."

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/EeS;->o:LX/EeX;

    iget-object v3, v3, LX/EeX;->operationUuid:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/EeS;->o:LX/EeX;

    iget-object v3, v3, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget v3, v3, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, LX/EeS;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152605
    :cond_1
    invoke-virtual {p0}, LX/EeS;->g()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2152606
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2152607
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/EeS;->o:LX/EeX;

    iget-object v0, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2152608
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_3

    .line 2152609
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Persisting "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152610
    :cond_3
    iget-object v0, p0, LX/EeS;->h:LX/Eeb;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V

    .line 2152611
    :cond_4
    :goto_1
    invoke-direct {p0}, LX/EeS;->m()V

    .line 2152612
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EeS;->s:Z

    goto :goto_0

    .line 2152613
    :cond_5
    iget-object v0, p0, LX/EeS;->o:LX/EeX;

    iget-object v0, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2152614
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_6

    .line 2152615
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Resuming download for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152616
    :cond_6
    invoke-static {p0}, LX/EeS;->j(LX/EeS;)V

    .line 2152617
    iget-object v0, p0, LX/EeS;->k:LX/Eek;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V

    goto :goto_1

    .line 2152618
    :cond_7
    iget-object v0, p0, LX/EeS;->o:LX/EeX;

    iget-object v0, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2152619
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_8

    .line 2152620
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Resuming diff patch for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152621
    :cond_8
    iget-object v0, p0, LX/EeS;->l:LX/Eel;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V

    goto :goto_1

    .line 2152622
    :cond_9
    iget-object v0, p0, LX/EeS;->o:LX/EeX;

    iget-object v0, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, LX/EeS;->o:LX/EeX;

    iget-object v0, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2152623
    :cond_a
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_b

    .line 2152624
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Resuming verification for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152625
    :cond_b
    iget-object v0, p0, LX/EeS;->m:LX/Een;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V

    goto/16 :goto_1

    .line 2152626
    :cond_c
    iget-object v0, p0, LX/EeS;->o:LX/EeX;

    iget-object v0, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2152627
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_4

    .line 2152628
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Resuming successful operation for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method

.method public final declared-synchronized a(LX/1sX;)Z
    .locals 1

    .prologue
    .line 2152595
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, LX/1sX;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2152596
    invoke-direct {p0}, LX/EeS;->h()V

    .line 2152597
    iget-object v0, p0, LX/EeS;->q:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2152598
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/EeS;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 2152599
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2152579
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LX/EeS;->o:LX/EeX;

    iget-object v2, v2, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/3CW;->c(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2152580
    sget-boolean v1, LX/EeM;->a:Z

    if-eqz v1, :cond_0

    .line 2152581
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Starting operation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152582
    :cond_0
    invoke-static {p0}, LX/EeS;->j(LX/EeS;)V

    .line 2152583
    iget-object v1, p0, LX/EeS;->j:LX/Eej;

    const-wide/16 v2, 0x0

    invoke-static {p0, v1, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V

    .line 2152584
    new-instance v1, LX/EeW;

    iget-object v2, p0, LX/EeS;->o:LX/EeX;

    invoke-direct {v1, v2}, LX/EeW;-><init>(LX/EeX;)V

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2152585
    iput-object v2, v1, LX/EeW;->e:Ljava/lang/Integer;

    .line 2152586
    move-object v1, v1

    .line 2152587
    invoke-virtual {v1}, LX/EeW;->a()LX/EeX;

    move-result-object v1

    .line 2152588
    invoke-static {p0, v1}, LX/EeS;->b$redex0(LX/EeS;LX/EeX;)Z

    .line 2152589
    iget-object v1, p0, LX/EeS;->k:LX/Eek;

    invoke-static {}, LX/Eem;->b()J

    move-result-wide v2

    .line 2152590
    iput-wide v2, v1, LX/Eek;->f:J

    .line 2152591
    iget-object v1, p0, LX/EeS;->a:LX/1wh;

    invoke-virtual {v1}, LX/1wh;->b()V

    .line 2152592
    iget-object v1, p0, LX/EeS;->a:LX/1wh;

    const-string v2, "appupdate_download_start"

    iget-object v3, p0, LX/EeS;->o:LX/EeX;

    iget-object v3, v3, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v4, p0, LX/EeS;->o:LX/EeX;

    invoke-virtual {v4}, LX/EeX;->d()LX/Eeh;

    move-result-object v4

    const-string v5, "task_start"

    invoke-virtual {v1, v2, v3, v4, v5}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152593
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 2152594
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/1sX;)Z
    .locals 2

    .prologue
    .line 2152563
    monitor-enter p0

    const/4 v0, 0x0

    .line 2152564
    :try_start_0
    iget-object v1, p0, LX/EeS;->p:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2152565
    iget-object v0, p0, LX/EeS;->p:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 2152566
    :cond_0
    iget-object v1, p0, LX/EeS;->q:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2152567
    iget-object v1, p0, LX/EeS;->q:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 2152568
    iget-object v1, p0, LX/EeS;->q:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2152569
    invoke-direct {p0}, LX/EeS;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152570
    :cond_1
    monitor-exit p0

    return v0

    .line 2152571
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2152552
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/EeS;->o:LX/EeX;

    iget-object v1, v1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x8

    invoke-static {v1, v2}, LX/3CW;->c(II)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2152553
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_0

    .line 2152554
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Restarting "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152555
    :cond_0
    invoke-static {p0}, LX/EeS;->j(LX/EeS;)V

    .line 2152556
    iget-object v0, p0, LX/EeS;->u:LX/EeO;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V

    .line 2152557
    iget-object v0, p0, LX/EeS;->k:LX/Eek;

    invoke-static {}, LX/Eem;->b()J

    move-result-wide v2

    .line 2152558
    iput-wide v2, v0, LX/Eek;->f:J

    .line 2152559
    iget-object v0, p0, LX/EeS;->a:LX/1wh;

    const-string v1, "appupdate_download_restart"

    iget-object v2, p0, LX/EeS;->o:LX/EeX;

    iget-object v2, v2, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v3, p0, LX/EeS;->o:LX/EeX;

    invoke-virtual {v3}, LX/EeX;->d()LX/Eeh;

    move-result-object v3

    const-string v4, "task_start"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152560
    const/4 v0, 0x1

    .line 2152561
    :cond_1
    monitor-exit p0

    return v0

    .line 2152562
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2152541
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/EeS;->o:LX/EeX;

    iget-object v1, v1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x8

    invoke-static {v1, v2}, LX/3CW;->c(II)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2152542
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_0

    .line 2152543
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Restarting on mobile data: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152544
    :cond_0
    invoke-static {p0}, LX/EeS;->j(LX/EeS;)V

    .line 2152545
    iget-object v0, p0, LX/EeS;->v:LX/EeO;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V

    .line 2152546
    iget-object v0, p0, LX/EeS;->k:LX/Eek;

    invoke-static {}, LX/Eem;->b()J

    move-result-wide v2

    .line 2152547
    iput-wide v2, v0, LX/Eek;->f:J

    .line 2152548
    iget-object v0, p0, LX/EeS;->a:LX/1wh;

    const-string v1, "appupdate_download_restart_on_mobile_data"

    iget-object v2, p0, LX/EeS;->o:LX/EeX;

    iget-object v2, v2, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v3, p0, LX/EeS;->o:LX/EeX;

    invoke-virtual {v3}, LX/EeX;->d()LX/Eeh;

    move-result-object v3

    const-string v4, "task_start"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152549
    const/4 v0, 0x1

    .line 2152550
    :cond_1
    monitor-exit p0

    return v0

    .line 2152551
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()LX/EeX;
    .locals 1

    .prologue
    .line 2152540
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EeS;->o:LX/EeX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 4

    .prologue
    .line 2152534
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/EeS;->o:LX/EeX;

    iget-object v0, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2152535
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_0

    .line 2152536
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Trying to complete download for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152537
    :cond_0
    iget-object v0, p0, LX/EeS;->k:LX/Eek;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152538
    :cond_1
    monitor-exit p0

    return-void

    .line 2152539
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2152520
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/EeS;->o:LX/EeX;

    iget-object v1, v1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x8

    invoke-static {v1, v2}, LX/3CW;->c(II)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2152521
    sget-boolean v0, LX/EeM;->a:Z

    if-eqz v0, :cond_0

    .line 2152522
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Discarding operation "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EeS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2152523
    :cond_0
    iget-object v0, p0, LX/EeS;->w:LX/EeO;

    const-wide/16 v2, 0x0

    invoke-static {p0, v0, v2, v3}, LX/EeS;->a$redex0(LX/EeS;LX/EeO;J)V

    .line 2152524
    invoke-direct {p0}, LX/EeS;->k()V

    .line 2152525
    new-instance v0, LX/EeW;

    iget-object v1, p0, LX/EeS;->o:LX/EeX;

    invoke-direct {v0, v1}, LX/EeW;-><init>(LX/EeX;)V

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2152526
    iput-object v1, v0, LX/EeW;->e:Ljava/lang/Integer;

    .line 2152527
    move-object v0, v0

    .line 2152528
    invoke-virtual {v0}, LX/EeW;->a()LX/EeX;

    move-result-object v0

    .line 2152529
    invoke-static {p0, v0}, LX/EeS;->b$redex0(LX/EeS;LX/EeX;)Z

    .line 2152530
    iget-object v0, p0, LX/EeS;->a:LX/1wh;

    invoke-virtual {v0}, LX/1wh;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152531
    const/4 v0, 0x1

    .line 2152532
    :cond_1
    monitor-exit p0

    return v0

    .line 2152533
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2152518
    invoke-virtual {p0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    .line 2152519
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, LX/EeS;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, LX/EeX;->operationUuid:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-static {v2}, LX/G6d;->a(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", package="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v2, v2, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", version="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget v0, v0, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
