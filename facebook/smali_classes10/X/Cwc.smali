.class public LX/Cwc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/Cwb;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:I

.field public final f:Z


# direct methods
.method public constructor <init>(LX/Cwa;)V
    .locals 1

    .prologue
    .line 1950906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950907
    iget-object v0, p1, LX/Cwa;->a:LX/Cwb;

    iput-object v0, p0, LX/Cwc;->a:LX/Cwb;

    .line 1950908
    iget-object v0, p1, LX/Cwa;->b:LX/0Px;

    iput-object v0, p0, LX/Cwc;->b:LX/0Px;

    .line 1950909
    iget-object v0, p1, LX/Cwa;->c:Ljava/lang/String;

    iput-object v0, p0, LX/Cwc;->c:Ljava/lang/String;

    .line 1950910
    iget-object v0, p1, LX/Cwa;->d:Ljava/lang/String;

    iput-object v0, p0, LX/Cwc;->d:Ljava/lang/String;

    .line 1950911
    iget v0, p1, LX/Cwa;->e:I

    iput v0, p0, LX/Cwc;->e:I

    .line 1950912
    iget-boolean v0, p1, LX/Cwa;->f:Z

    iput-boolean v0, p0, LX/Cwc;->f:Z

    .line 1950913
    return-void
.end method
