.class public final enum LX/Dt2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dt2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dt2;

.field public static final enum CLOSED:LX/Dt2;

.field public static final enum OPEN:LX/Dt2;

.field public static final enum UNKNOWN:LX/Dt2;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2051028
    new-instance v0, LX/Dt2;

    const-string v1, "OPEN"

    invoke-direct {v0, v1, v2}, LX/Dt2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dt2;->OPEN:LX/Dt2;

    .line 2051029
    new-instance v0, LX/Dt2;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v3}, LX/Dt2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dt2;->CLOSED:LX/Dt2;

    .line 2051030
    new-instance v0, LX/Dt2;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/Dt2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dt2;->UNKNOWN:LX/Dt2;

    .line 2051031
    const/4 v0, 0x3

    new-array v0, v0, [LX/Dt2;

    sget-object v1, LX/Dt2;->OPEN:LX/Dt2;

    aput-object v1, v0, v2

    sget-object v1, LX/Dt2;->CLOSED:LX/Dt2;

    aput-object v1, v0, v3

    sget-object v1, LX/Dt2;->UNKNOWN:LX/Dt2;

    aput-object v1, v0, v4

    sput-object v0, LX/Dt2;->$VALUES:[LX/Dt2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2051033
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dt2;
    .locals 1

    .prologue
    .line 2051034
    const-class v0, LX/Dt2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dt2;

    return-object v0
.end method

.method public static values()[LX/Dt2;
    .locals 1

    .prologue
    .line 2051032
    sget-object v0, LX/Dt2;->$VALUES:[LX/Dt2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dt2;

    return-object v0
.end method
