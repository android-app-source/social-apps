.class public LX/EaZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2142057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a([BI)J
    .locals 6

    .prologue
    .line 2142053
    add-int/lit8 v0, p1, 0x0

    aget-byte v0, p0, v0

    int-to-long v0, v0

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    .line 2142054
    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    int-to-long v2, v2

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    const-wide/32 v4, 0xff00

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 2142055
    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    int-to-long v2, v2

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    const-wide/32 v4, 0xff0000

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 2142056
    return-wide v0
.end method

.method public static a([B)V
    .locals 52

    .prologue
    .line 2141866
    const-wide/32 v2, 0x1fffff

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/EaZ;->a([BI)J

    move-result-wide v4

    and-long/2addr v2, v4

    .line 2141867
    const-wide/32 v4, 0x1fffff

    const/4 v6, 0x2

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/EaZ;->b([BI)J

    move-result-wide v6

    const/4 v8, 0x5

    ushr-long/2addr v6, v8

    and-long/2addr v4, v6

    .line 2141868
    const-wide/32 v6, 0x1fffff

    const/4 v8, 0x5

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/EaZ;->a([BI)J

    move-result-wide v8

    const/4 v10, 0x2

    ushr-long/2addr v8, v10

    and-long/2addr v6, v8

    .line 2141869
    const-wide/32 v8, 0x1fffff

    const/4 v10, 0x7

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/EaZ;->b([BI)J

    move-result-wide v10

    const/4 v12, 0x7

    ushr-long/2addr v10, v12

    and-long/2addr v8, v10

    .line 2141870
    const-wide/32 v10, 0x1fffff

    const/16 v12, 0xa

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/EaZ;->b([BI)J

    move-result-wide v12

    const/4 v14, 0x4

    ushr-long/2addr v12, v14

    and-long/2addr v10, v12

    .line 2141871
    const-wide/32 v12, 0x1fffff

    const/16 v14, 0xd

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/EaZ;->a([BI)J

    move-result-wide v14

    const/16 v16, 0x1

    ushr-long v14, v14, v16

    and-long/2addr v12, v14

    .line 2141872
    const-wide/32 v14, 0x1fffff

    const/16 v16, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/EaZ;->b([BI)J

    move-result-wide v16

    const/16 v18, 0x6

    ushr-long v16, v16, v18

    and-long v14, v14, v16

    .line 2141873
    const-wide/32 v16, 0x1fffff

    const/16 v18, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/EaZ;->a([BI)J

    move-result-wide v18

    const/16 v20, 0x3

    ushr-long v18, v18, v20

    and-long v16, v16, v18

    .line 2141874
    const-wide/32 v18, 0x1fffff

    const/16 v20, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/EaZ;->a([BI)J

    move-result-wide v20

    and-long v18, v18, v20

    .line 2141875
    const-wide/32 v20, 0x1fffff

    const/16 v22, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/EaZ;->b([BI)J

    move-result-wide v22

    const/16 v24, 0x5

    ushr-long v22, v22, v24

    and-long v20, v20, v22

    .line 2141876
    const-wide/32 v22, 0x1fffff

    const/16 v24, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/EaZ;->a([BI)J

    move-result-wide v24

    const/16 v26, 0x2

    ushr-long v24, v24, v26

    and-long v22, v22, v24

    .line 2141877
    const-wide/32 v24, 0x1fffff

    const/16 v26, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/EaZ;->b([BI)J

    move-result-wide v26

    const/16 v28, 0x7

    ushr-long v26, v26, v28

    and-long v24, v24, v26

    .line 2141878
    const-wide/32 v26, 0x1fffff

    const/16 v28, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/EaZ;->b([BI)J

    move-result-wide v28

    const/16 v30, 0x4

    ushr-long v28, v28, v30

    and-long v26, v26, v28

    .line 2141879
    const-wide/32 v28, 0x1fffff

    const/16 v30, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/EaZ;->a([BI)J

    move-result-wide v30

    const/16 v32, 0x1

    ushr-long v30, v30, v32

    and-long v28, v28, v30

    .line 2141880
    const-wide/32 v30, 0x1fffff

    const/16 v32, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/EaZ;->b([BI)J

    move-result-wide v32

    const/16 v34, 0x6

    ushr-long v32, v32, v34

    and-long v30, v30, v32

    .line 2141881
    const-wide/32 v32, 0x1fffff

    const/16 v34, 0x27

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/EaZ;->a([BI)J

    move-result-wide v34

    const/16 v36, 0x3

    ushr-long v34, v34, v36

    and-long v32, v32, v34

    .line 2141882
    const-wide/32 v34, 0x1fffff

    const/16 v36, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/EaZ;->a([BI)J

    move-result-wide v36

    and-long v34, v34, v36

    .line 2141883
    const-wide/32 v36, 0x1fffff

    const/16 v38, 0x2c

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/EaZ;->b([BI)J

    move-result-wide v38

    const/16 v40, 0x5

    ushr-long v38, v38, v40

    and-long v36, v36, v38

    .line 2141884
    const-wide/32 v38, 0x1fffff

    const/16 v40, 0x2f

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/EaZ;->a([BI)J

    move-result-wide v40

    const/16 v42, 0x2

    ushr-long v40, v40, v42

    and-long v38, v38, v40

    .line 2141885
    const-wide/32 v40, 0x1fffff

    const/16 v42, 0x31

    move-object/from16 v0, p0

    move/from16 v1, v42

    invoke-static {v0, v1}, LX/EaZ;->b([BI)J

    move-result-wide v42

    const/16 v44, 0x7

    ushr-long v42, v42, v44

    and-long v40, v40, v42

    .line 2141886
    const-wide/32 v42, 0x1fffff

    const/16 v44, 0x34

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-static {v0, v1}, LX/EaZ;->b([BI)J

    move-result-wide v44

    const/16 v46, 0x4

    ushr-long v44, v44, v46

    and-long v42, v42, v44

    .line 2141887
    const-wide/32 v44, 0x1fffff

    const/16 v46, 0x37

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-static {v0, v1}, LX/EaZ;->a([BI)J

    move-result-wide v46

    const/16 v48, 0x1

    ushr-long v46, v46, v48

    and-long v44, v44, v46

    .line 2141888
    const-wide/32 v46, 0x1fffff

    const/16 v48, 0x39

    move-object/from16 v0, p0

    move/from16 v1, v48

    invoke-static {v0, v1}, LX/EaZ;->b([BI)J

    move-result-wide v48

    const/16 v50, 0x6

    ushr-long v48, v48, v50

    and-long v46, v46, v48

    .line 2141889
    const/16 v48, 0x3c

    move-object/from16 v0, p0

    move/from16 v1, v48

    invoke-static {v0, v1}, LX/EaZ;->b([BI)J

    move-result-wide v48

    const/16 v50, 0x3

    ushr-long v48, v48, v50

    .line 2141890
    const-wide/32 v50, 0xa2c13

    mul-long v50, v50, v48

    add-long v24, v24, v50

    .line 2141891
    const-wide/32 v50, 0x72d18

    mul-long v50, v50, v48

    add-long v26, v26, v50

    .line 2141892
    const-wide/32 v50, 0x9fb67

    mul-long v50, v50, v48

    add-long v28, v28, v50

    .line 2141893
    const-wide/32 v50, 0xf39ad

    mul-long v50, v50, v48

    sub-long v30, v30, v50

    .line 2141894
    const-wide/32 v50, 0x215d1

    mul-long v50, v50, v48

    add-long v32, v32, v50

    .line 2141895
    const-wide/32 v50, 0xa6f7d

    mul-long v48, v48, v50

    sub-long v34, v34, v48

    .line 2141896
    const-wide/32 v48, 0xa2c13

    mul-long v48, v48, v46

    add-long v22, v22, v48

    .line 2141897
    const-wide/32 v48, 0x72d18

    mul-long v48, v48, v46

    add-long v24, v24, v48

    .line 2141898
    const-wide/32 v48, 0x9fb67

    mul-long v48, v48, v46

    add-long v26, v26, v48

    .line 2141899
    const-wide/32 v48, 0xf39ad

    mul-long v48, v48, v46

    sub-long v28, v28, v48

    .line 2141900
    const-wide/32 v48, 0x215d1

    mul-long v48, v48, v46

    add-long v30, v30, v48

    .line 2141901
    const-wide/32 v48, 0xa6f7d

    mul-long v46, v46, v48

    sub-long v32, v32, v46

    .line 2141902
    const-wide/32 v46, 0xa2c13

    mul-long v46, v46, v44

    add-long v20, v20, v46

    .line 2141903
    const-wide/32 v46, 0x72d18

    mul-long v46, v46, v44

    add-long v22, v22, v46

    .line 2141904
    const-wide/32 v46, 0x9fb67

    mul-long v46, v46, v44

    add-long v24, v24, v46

    .line 2141905
    const-wide/32 v46, 0xf39ad

    mul-long v46, v46, v44

    sub-long v26, v26, v46

    .line 2141906
    const-wide/32 v46, 0x215d1

    mul-long v46, v46, v44

    add-long v28, v28, v46

    .line 2141907
    const-wide/32 v46, 0xa6f7d

    mul-long v44, v44, v46

    sub-long v30, v30, v44

    .line 2141908
    const-wide/32 v44, 0xa2c13

    mul-long v44, v44, v42

    add-long v18, v18, v44

    .line 2141909
    const-wide/32 v44, 0x72d18

    mul-long v44, v44, v42

    add-long v20, v20, v44

    .line 2141910
    const-wide/32 v44, 0x9fb67

    mul-long v44, v44, v42

    add-long v22, v22, v44

    .line 2141911
    const-wide/32 v44, 0xf39ad

    mul-long v44, v44, v42

    sub-long v24, v24, v44

    .line 2141912
    const-wide/32 v44, 0x215d1

    mul-long v44, v44, v42

    add-long v26, v26, v44

    .line 2141913
    const-wide/32 v44, 0xa6f7d

    mul-long v42, v42, v44

    sub-long v28, v28, v42

    .line 2141914
    const-wide/32 v42, 0xa2c13

    mul-long v42, v42, v40

    add-long v16, v16, v42

    .line 2141915
    const-wide/32 v42, 0x72d18

    mul-long v42, v42, v40

    add-long v18, v18, v42

    .line 2141916
    const-wide/32 v42, 0x9fb67

    mul-long v42, v42, v40

    add-long v20, v20, v42

    .line 2141917
    const-wide/32 v42, 0xf39ad

    mul-long v42, v42, v40

    sub-long v22, v22, v42

    .line 2141918
    const-wide/32 v42, 0x215d1

    mul-long v42, v42, v40

    add-long v24, v24, v42

    .line 2141919
    const-wide/32 v42, 0xa6f7d

    mul-long v40, v40, v42

    sub-long v26, v26, v40

    .line 2141920
    const-wide/32 v40, 0xa2c13

    mul-long v40, v40, v38

    add-long v14, v14, v40

    .line 2141921
    const-wide/32 v40, 0x72d18

    mul-long v40, v40, v38

    add-long v16, v16, v40

    .line 2141922
    const-wide/32 v40, 0x9fb67

    mul-long v40, v40, v38

    add-long v18, v18, v40

    .line 2141923
    const-wide/32 v40, 0xf39ad

    mul-long v40, v40, v38

    sub-long v20, v20, v40

    .line 2141924
    const-wide/32 v40, 0x215d1

    mul-long v40, v40, v38

    add-long v22, v22, v40

    .line 2141925
    const-wide/32 v40, 0xa6f7d

    mul-long v38, v38, v40

    sub-long v24, v24, v38

    .line 2141926
    const-wide/32 v38, 0x100000

    add-long v38, v38, v14

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v16, v16, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v14, v14, v38

    .line 2141927
    const-wide/32 v38, 0x100000

    add-long v38, v38, v18

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v20, v20, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v18, v18, v38

    .line 2141928
    const-wide/32 v38, 0x100000

    add-long v38, v38, v22

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v24, v24, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v22, v22, v38

    .line 2141929
    const-wide/32 v38, 0x100000

    add-long v38, v38, v26

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v28, v28, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v26, v26, v38

    .line 2141930
    const-wide/32 v38, 0x100000

    add-long v38, v38, v30

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v32, v32, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v30, v30, v38

    .line 2141931
    const-wide/32 v38, 0x100000

    add-long v38, v38, v34

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v36, v36, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v34, v34, v38

    .line 2141932
    const-wide/32 v38, 0x100000

    add-long v38, v38, v16

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v18, v18, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v16, v16, v38

    .line 2141933
    const-wide/32 v38, 0x100000

    add-long v38, v38, v20

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v22, v22, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v20, v20, v38

    .line 2141934
    const-wide/32 v38, 0x100000

    add-long v38, v38, v24

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v26, v26, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v24, v24, v38

    .line 2141935
    const-wide/32 v38, 0x100000

    add-long v38, v38, v28

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v30, v30, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v28, v28, v38

    .line 2141936
    const-wide/32 v38, 0x100000

    add-long v38, v38, v32

    const/16 v40, 0x15

    shr-long v38, v38, v40

    add-long v34, v34, v38

    const/16 v40, 0x15

    shl-long v38, v38, v40

    sub-long v32, v32, v38

    .line 2141937
    const-wide/32 v38, 0xa2c13

    mul-long v38, v38, v36

    add-long v12, v12, v38

    .line 2141938
    const-wide/32 v38, 0x72d18

    mul-long v38, v38, v36

    add-long v14, v14, v38

    .line 2141939
    const-wide/32 v38, 0x9fb67

    mul-long v38, v38, v36

    add-long v16, v16, v38

    .line 2141940
    const-wide/32 v38, 0xf39ad

    mul-long v38, v38, v36

    sub-long v18, v18, v38

    .line 2141941
    const-wide/32 v38, 0x215d1

    mul-long v38, v38, v36

    add-long v20, v20, v38

    .line 2141942
    const-wide/32 v38, 0xa6f7d

    mul-long v36, v36, v38

    sub-long v22, v22, v36

    .line 2141943
    const-wide/32 v36, 0xa2c13

    mul-long v36, v36, v34

    add-long v10, v10, v36

    .line 2141944
    const-wide/32 v36, 0x72d18

    mul-long v36, v36, v34

    add-long v12, v12, v36

    .line 2141945
    const-wide/32 v36, 0x9fb67

    mul-long v36, v36, v34

    add-long v14, v14, v36

    .line 2141946
    const-wide/32 v36, 0xf39ad

    mul-long v36, v36, v34

    sub-long v16, v16, v36

    .line 2141947
    const-wide/32 v36, 0x215d1

    mul-long v36, v36, v34

    add-long v18, v18, v36

    .line 2141948
    const-wide/32 v36, 0xa6f7d

    mul-long v34, v34, v36

    sub-long v20, v20, v34

    .line 2141949
    const-wide/32 v34, 0xa2c13

    mul-long v34, v34, v32

    add-long v8, v8, v34

    .line 2141950
    const-wide/32 v34, 0x72d18

    mul-long v34, v34, v32

    add-long v10, v10, v34

    .line 2141951
    const-wide/32 v34, 0x9fb67

    mul-long v34, v34, v32

    add-long v12, v12, v34

    .line 2141952
    const-wide/32 v34, 0xf39ad

    mul-long v34, v34, v32

    sub-long v14, v14, v34

    .line 2141953
    const-wide/32 v34, 0x215d1

    mul-long v34, v34, v32

    add-long v16, v16, v34

    .line 2141954
    const-wide/32 v34, 0xa6f7d

    mul-long v32, v32, v34

    sub-long v18, v18, v32

    .line 2141955
    const-wide/32 v32, 0xa2c13

    mul-long v32, v32, v30

    add-long v6, v6, v32

    .line 2141956
    const-wide/32 v32, 0x72d18

    mul-long v32, v32, v30

    add-long v8, v8, v32

    .line 2141957
    const-wide/32 v32, 0x9fb67

    mul-long v32, v32, v30

    add-long v10, v10, v32

    .line 2141958
    const-wide/32 v32, 0xf39ad

    mul-long v32, v32, v30

    sub-long v12, v12, v32

    .line 2141959
    const-wide/32 v32, 0x215d1

    mul-long v32, v32, v30

    add-long v14, v14, v32

    .line 2141960
    const-wide/32 v32, 0xa6f7d

    mul-long v30, v30, v32

    sub-long v16, v16, v30

    .line 2141961
    const-wide/32 v30, 0xa2c13

    mul-long v30, v30, v28

    add-long v4, v4, v30

    .line 2141962
    const-wide/32 v30, 0x72d18

    mul-long v30, v30, v28

    add-long v6, v6, v30

    .line 2141963
    const-wide/32 v30, 0x9fb67

    mul-long v30, v30, v28

    add-long v8, v8, v30

    .line 2141964
    const-wide/32 v30, 0xf39ad

    mul-long v30, v30, v28

    sub-long v10, v10, v30

    .line 2141965
    const-wide/32 v30, 0x215d1

    mul-long v30, v30, v28

    add-long v12, v12, v30

    .line 2141966
    const-wide/32 v30, 0xa6f7d

    mul-long v28, v28, v30

    sub-long v14, v14, v28

    .line 2141967
    const-wide/32 v28, 0xa2c13

    mul-long v28, v28, v26

    add-long v2, v2, v28

    .line 2141968
    const-wide/32 v28, 0x72d18

    mul-long v28, v28, v26

    add-long v4, v4, v28

    .line 2141969
    const-wide/32 v28, 0x9fb67

    mul-long v28, v28, v26

    add-long v6, v6, v28

    .line 2141970
    const-wide/32 v28, 0xf39ad

    mul-long v28, v28, v26

    sub-long v8, v8, v28

    .line 2141971
    const-wide/32 v28, 0x215d1

    mul-long v28, v28, v26

    add-long v10, v10, v28

    .line 2141972
    const-wide/32 v28, 0xa6f7d

    mul-long v26, v26, v28

    sub-long v12, v12, v26

    .line 2141973
    const-wide/32 v26, 0x100000

    add-long v26, v26, v2

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v4, v4, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v2, v2, v26

    .line 2141974
    const-wide/32 v26, 0x100000

    add-long v26, v26, v6

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v8, v8, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v6, v6, v26

    .line 2141975
    const-wide/32 v26, 0x100000

    add-long v26, v26, v10

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v12, v12, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v10, v10, v26

    .line 2141976
    const-wide/32 v26, 0x100000

    add-long v26, v26, v14

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v16, v16, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v14, v14, v26

    .line 2141977
    const-wide/32 v26, 0x100000

    add-long v26, v26, v18

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v20, v20, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v18, v18, v26

    .line 2141978
    const-wide/32 v26, 0x100000

    add-long v26, v26, v22

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v24, v24, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v22, v22, v26

    .line 2141979
    const-wide/32 v26, 0x100000

    add-long v26, v26, v4

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v6, v6, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v4, v4, v26

    .line 2141980
    const-wide/32 v26, 0x100000

    add-long v26, v26, v8

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v10, v10, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v8, v8, v26

    .line 2141981
    const-wide/32 v26, 0x100000

    add-long v26, v26, v12

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v14, v14, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v12, v12, v26

    .line 2141982
    const-wide/32 v26, 0x100000

    add-long v26, v26, v16

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v18, v18, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v16, v16, v26

    .line 2141983
    const-wide/32 v26, 0x100000

    add-long v26, v26, v20

    const/16 v28, 0x15

    shr-long v26, v26, v28

    add-long v22, v22, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v20, v20, v26

    .line 2141984
    const-wide/32 v26, 0x100000

    add-long v26, v26, v24

    const/16 v28, 0x15

    shr-long v26, v26, v28

    const-wide/16 v28, 0x0

    add-long v28, v28, v26

    const/16 v30, 0x15

    shl-long v26, v26, v30

    sub-long v24, v24, v26

    .line 2141985
    const-wide/32 v26, 0xa2c13

    mul-long v26, v26, v28

    add-long v2, v2, v26

    .line 2141986
    const-wide/32 v26, 0x72d18

    mul-long v26, v26, v28

    add-long v4, v4, v26

    .line 2141987
    const-wide/32 v26, 0x9fb67

    mul-long v26, v26, v28

    add-long v6, v6, v26

    .line 2141988
    const-wide/32 v26, 0xf39ad

    mul-long v26, v26, v28

    sub-long v8, v8, v26

    .line 2141989
    const-wide/32 v26, 0x215d1

    mul-long v26, v26, v28

    add-long v10, v10, v26

    .line 2141990
    const-wide/32 v26, 0xa6f7d

    mul-long v26, v26, v28

    sub-long v12, v12, v26

    .line 2141991
    const/16 v26, 0x15

    shr-long v26, v2, v26

    add-long v4, v4, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v2, v2, v26

    .line 2141992
    const/16 v26, 0x15

    shr-long v26, v4, v26

    add-long v6, v6, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v4, v4, v26

    .line 2141993
    const/16 v26, 0x15

    shr-long v26, v6, v26

    add-long v8, v8, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v6, v6, v26

    .line 2141994
    const/16 v26, 0x15

    shr-long v26, v8, v26

    add-long v10, v10, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v8, v8, v26

    .line 2141995
    const/16 v26, 0x15

    shr-long v26, v10, v26

    add-long v12, v12, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v10, v10, v26

    .line 2141996
    const/16 v26, 0x15

    shr-long v26, v12, v26

    add-long v14, v14, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v12, v12, v26

    .line 2141997
    const/16 v26, 0x15

    shr-long v26, v14, v26

    add-long v16, v16, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v14, v14, v26

    .line 2141998
    const/16 v26, 0x15

    shr-long v26, v16, v26

    add-long v18, v18, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v16, v16, v26

    .line 2141999
    const/16 v26, 0x15

    shr-long v26, v18, v26

    add-long v20, v20, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v18, v18, v26

    .line 2142000
    const/16 v26, 0x15

    shr-long v26, v20, v26

    add-long v22, v22, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v20, v20, v26

    .line 2142001
    const/16 v26, 0x15

    shr-long v26, v22, v26

    add-long v24, v24, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v22, v22, v26

    .line 2142002
    const/16 v26, 0x15

    shr-long v26, v24, v26

    const-wide/16 v28, 0x0

    add-long v28, v28, v26

    const/16 v30, 0x15

    shl-long v26, v26, v30

    sub-long v24, v24, v26

    .line 2142003
    const-wide/32 v26, 0xa2c13

    mul-long v26, v26, v28

    add-long v2, v2, v26

    .line 2142004
    const-wide/32 v26, 0x72d18

    mul-long v26, v26, v28

    add-long v4, v4, v26

    .line 2142005
    const-wide/32 v26, 0x9fb67

    mul-long v26, v26, v28

    add-long v6, v6, v26

    .line 2142006
    const-wide/32 v26, 0xf39ad

    mul-long v26, v26, v28

    sub-long v8, v8, v26

    .line 2142007
    const-wide/32 v26, 0x215d1

    mul-long v26, v26, v28

    add-long v10, v10, v26

    .line 2142008
    const-wide/32 v26, 0xa6f7d

    mul-long v26, v26, v28

    sub-long v12, v12, v26

    .line 2142009
    const/16 v26, 0x15

    shr-long v26, v2, v26

    add-long v4, v4, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v2, v2, v26

    .line 2142010
    const/16 v26, 0x15

    shr-long v26, v4, v26

    add-long v6, v6, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v4, v4, v26

    .line 2142011
    const/16 v26, 0x15

    shr-long v26, v6, v26

    add-long v8, v8, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v6, v6, v26

    .line 2142012
    const/16 v26, 0x15

    shr-long v26, v8, v26

    add-long v10, v10, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v8, v8, v26

    .line 2142013
    const/16 v26, 0x15

    shr-long v26, v10, v26

    add-long v12, v12, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v10, v10, v26

    .line 2142014
    const/16 v26, 0x15

    shr-long v26, v12, v26

    add-long v14, v14, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v12, v12, v26

    .line 2142015
    const/16 v26, 0x15

    shr-long v26, v14, v26

    add-long v16, v16, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v14, v14, v26

    .line 2142016
    const/16 v26, 0x15

    shr-long v26, v16, v26

    add-long v18, v18, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v16, v16, v26

    .line 2142017
    const/16 v26, 0x15

    shr-long v26, v18, v26

    add-long v20, v20, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v18, v18, v26

    .line 2142018
    const/16 v26, 0x15

    shr-long v26, v20, v26

    add-long v22, v22, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v20, v20, v26

    .line 2142019
    const/16 v26, 0x15

    shr-long v26, v22, v26

    add-long v24, v24, v26

    const/16 v28, 0x15

    shl-long v26, v26, v28

    sub-long v22, v22, v26

    .line 2142020
    const/16 v26, 0x0

    const/16 v27, 0x0

    shr-long v28, v2, v27

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-byte v0, v0

    move/from16 v27, v0

    aput-byte v27, p0, v26

    .line 2142021
    const/16 v26, 0x1

    const/16 v27, 0x8

    shr-long v28, v2, v27

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-byte v0, v0

    move/from16 v27, v0

    aput-byte v27, p0, v26

    .line 2142022
    const/16 v26, 0x2

    const/16 v27, 0x10

    shr-long v2, v2, v27

    const/16 v27, 0x5

    shl-long v28, v4, v27

    or-long v2, v2, v28

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, p0, v26

    .line 2142023
    const/4 v2, 0x3

    const/4 v3, 0x3

    shr-long v26, v4, v3

    move-wide/from16 v0, v26

    long-to-int v3, v0

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142024
    const/4 v2, 0x4

    const/16 v3, 0xb

    shr-long v26, v4, v3

    move-wide/from16 v0, v26

    long-to-int v3, v0

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142025
    const/4 v2, 0x5

    const/16 v3, 0x13

    shr-long/2addr v4, v3

    const/4 v3, 0x2

    shl-long v26, v6, v3

    or-long v4, v4, v26

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142026
    const/4 v2, 0x6

    const/4 v3, 0x6

    shr-long v4, v6, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142027
    const/4 v2, 0x7

    const/16 v3, 0xe

    shr-long v4, v6, v3

    const/4 v3, 0x7

    shl-long v6, v8, v3

    or-long/2addr v4, v6

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142028
    const/16 v2, 0x8

    const/4 v3, 0x1

    shr-long v4, v8, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142029
    const/16 v2, 0x9

    const/16 v3, 0x9

    shr-long v4, v8, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142030
    const/16 v2, 0xa

    const/16 v3, 0x11

    shr-long v4, v8, v3

    const/4 v3, 0x4

    shl-long v6, v10, v3

    or-long/2addr v4, v6

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142031
    const/16 v2, 0xb

    const/4 v3, 0x4

    shr-long v4, v10, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142032
    const/16 v2, 0xc

    const/16 v3, 0xc

    shr-long v4, v10, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142033
    const/16 v2, 0xd

    const/16 v3, 0x14

    shr-long v4, v10, v3

    const/4 v3, 0x1

    shl-long v6, v12, v3

    or-long/2addr v4, v6

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142034
    const/16 v2, 0xe

    const/4 v3, 0x7

    shr-long v4, v12, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142035
    const/16 v2, 0xf

    const/16 v3, 0xf

    shr-long v4, v12, v3

    const/4 v3, 0x6

    shl-long v6, v14, v3

    or-long/2addr v4, v6

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142036
    const/16 v2, 0x10

    const/4 v3, 0x2

    shr-long v4, v14, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142037
    const/16 v2, 0x11

    const/16 v3, 0xa

    shr-long v4, v14, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142038
    const/16 v2, 0x12

    const/16 v3, 0x12

    shr-long v4, v14, v3

    const/4 v3, 0x3

    shl-long v6, v16, v3

    or-long/2addr v4, v6

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142039
    const/16 v2, 0x13

    const/4 v3, 0x5

    shr-long v4, v16, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142040
    const/16 v2, 0x14

    const/16 v3, 0xd

    shr-long v4, v16, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142041
    const/16 v2, 0x15

    const/4 v3, 0x0

    shr-long v4, v18, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142042
    const/16 v2, 0x16

    const/16 v3, 0x8

    shr-long v4, v18, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142043
    const/16 v2, 0x17

    const/16 v3, 0x10

    shr-long v4, v18, v3

    const/4 v3, 0x5

    shl-long v6, v20, v3

    or-long/2addr v4, v6

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142044
    const/16 v2, 0x18

    const/4 v3, 0x3

    shr-long v4, v20, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142045
    const/16 v2, 0x19

    const/16 v3, 0xb

    shr-long v4, v20, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142046
    const/16 v2, 0x1a

    const/16 v3, 0x13

    shr-long v4, v20, v3

    const/4 v3, 0x2

    shl-long v6, v22, v3

    or-long/2addr v4, v6

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142047
    const/16 v2, 0x1b

    const/4 v3, 0x6

    shr-long v4, v22, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142048
    const/16 v2, 0x1c

    const/16 v3, 0xe

    shr-long v4, v22, v3

    const/4 v3, 0x7

    shl-long v6, v24, v3

    or-long/2addr v4, v6

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142049
    const/16 v2, 0x1d

    const/4 v3, 0x1

    shr-long v4, v24, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142050
    const/16 v2, 0x1e

    const/16 v3, 0x9

    shr-long v4, v24, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142051
    const/16 v2, 0x1f

    const/16 v3, 0x11

    shr-long v4, v24, v3

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 2142052
    return-void
.end method

.method private static b([BI)J
    .locals 6

    .prologue
    .line 2141861
    add-int/lit8 v0, p1, 0x0

    aget-byte v0, p0, v0

    int-to-long v0, v0

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    .line 2141862
    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    int-to-long v2, v2

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    const-wide/32 v4, 0xff00

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 2141863
    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    int-to-long v2, v2

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    const-wide/32 v4, 0xff0000

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 2141864
    add-int/lit8 v2, p1, 0x3

    aget-byte v2, p0, v2

    int-to-long v2, v2

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    const-wide v4, 0xff000000L

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 2141865
    return-wide v0
.end method
