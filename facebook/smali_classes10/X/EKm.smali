.class public final LX/EKm;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EKn;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsCandidateInfo;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

.field public final synthetic c:LX/EKn;


# direct methods
.method public constructor <init>(LX/EKn;)V
    .locals 1

    .prologue
    .line 2107195
    iput-object p1, p0, LX/EKm;->c:LX/EKn;

    .line 2107196
    move-object v0, p1

    .line 2107197
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2107198
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2107199
    const-string v0, "SearchResultsElectionRaceComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2107200
    if-ne p0, p1, :cond_1

    .line 2107201
    :cond_0
    :goto_0
    return v0

    .line 2107202
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2107203
    goto :goto_0

    .line 2107204
    :cond_3
    check-cast p1, LX/EKm;

    .line 2107205
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2107206
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2107207
    if-eq v2, v3, :cond_0

    .line 2107208
    iget-object v2, p0, LX/EKm;->a:LX/0Px;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EKm;->a:LX/0Px;

    iget-object v3, p1, LX/EKm;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2107209
    goto :goto_0

    .line 2107210
    :cond_5
    iget-object v2, p1, LX/EKm;->a:LX/0Px;

    if-nez v2, :cond_4

    .line 2107211
    :cond_6
    iget-object v2, p0, LX/EKm;->b:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/EKm;->b:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    iget-object v3, p1, LX/EKm;->b:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2107212
    goto :goto_0

    .line 2107213
    :cond_7
    iget-object v2, p1, LX/EKm;->b:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
