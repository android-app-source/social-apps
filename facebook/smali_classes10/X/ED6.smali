.class public LX/ED6;
.super LX/3pF;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/ED5;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/ED5;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/ED5;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:LX/EG3;

.field private final f:Landroid/content/Context;

.field private final g:LX/0ad;

.field private final h:LX/ED4;

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/ED5;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

.field public k:Lcom/facebook/rtc/fragments/RtcEmptyFragment;

.field public l:Landroid/support/v4/app/Fragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2090935
    const-class v0, LX/ED6;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ED6;->a:Ljava/lang/String;

    .line 2090936
    sget-object v0, LX/ED5;->ROSTER:LX/ED5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/ED6;->b:LX/0Px;

    .line 2090937
    sget-object v0, LX/ED5;->EMPTY:LX/ED5;

    sget-object v1, LX/ED5;->ROSTER:LX/ED5;

    sget-object v2, LX/ED5;->ADD_NEW:LX/ED5;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/ED6;->c:LX/0Px;

    .line 2090938
    sget-object v0, LX/ED5;->EMPTY:LX/ED5;

    sget-object v1, LX/ED5;->ROSTER:LX/ED5;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/ED6;->d:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/EG3;Landroid/content/Context;LX/0ad;LX/0gc;LX/ED4;)V
    .locals 5
    .param p4    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/ED4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2090921
    invoke-direct {p0, p4}, LX/3pF;-><init>(LX/0gc;)V

    .line 2090922
    iput-object p1, p0, LX/ED6;->e:LX/EG3;

    .line 2090923
    iput-object p2, p0, LX/ED6;->f:Landroid/content/Context;

    .line 2090924
    iput-object p3, p0, LX/ED6;->g:LX/0ad;

    .line 2090925
    iput-object p5, p0, LX/ED6;->h:LX/ED4;

    .line 2090926
    sget-object v0, LX/ED3;->a:[I

    invoke-virtual {p5}, LX/ED4;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2090927
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2090928
    iput-object v0, p0, LX/ED6;->i:LX/0Px;

    .line 2090929
    sget-object v0, LX/ED6;->a:Ljava/lang/String;

    const-string v1, "Unimplemented mode $d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p5}, LX/ED4;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2090930
    :goto_0
    invoke-direct {p0}, LX/ED6;->g()V

    .line 2090931
    return-void

    .line 2090932
    :pswitch_0
    sget-object v0, LX/ED6;->b:LX/0Px;

    iput-object v0, p0, LX/ED6;->i:LX/0Px;

    goto :goto_0

    .line 2090933
    :pswitch_1
    iget-object v0, p0, LX/ED6;->g:LX/0ad;

    sget-short v1, LX/3Dx;->aJ:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2090934
    if-eqz v0, :cond_0

    sget-object v0, LX/ED6;->c:LX/0Px;

    :goto_1
    iput-object v0, p0, LX/ED6;->i:LX/0Px;

    goto :goto_0

    :cond_0
    sget-object v0, LX/ED6;->d:LX/0Px;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 2090914
    invoke-static {p0}, LX/ED6;->h(LX/ED6;)V

    .line 2090915
    iget-object v0, p0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    .line 2090916
    iput p1, v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->j:I

    .line 2090917
    iput p2, v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->k:I

    .line 2090918
    const/4 p0, 0x1

    iput-boolean p0, v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->m:Z

    .line 2090919
    invoke-static {v0}, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e(Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;)V

    .line 2090920
    return-void
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 2090907
    invoke-static {p0}, LX/ED6;->h(LX/ED6;)V

    .line 2090908
    iget-object v0, p0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    .line 2090909
    iput p1, v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->l:I

    .line 2090910
    const/4 p0, 0x1

    iput-boolean p0, v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->n:Z

    .line 2090911
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object p0, p0

    .line 2090912
    invoke-static {v0, p0}, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->a(Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;Landroid/view/View;)V

    .line 2090913
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 2090939
    sget-object v0, LX/ED3;->a:[I

    iget-object v1, p0, LX/ED6;->h:LX/ED4;

    invoke-virtual {v1}, LX/ED4;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2090940
    :goto_0
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2090941
    return-void

    .line 2090942
    :pswitch_0
    iget-object v0, p0, LX/ED6;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, LX/ED6;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a010e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, LX/ED6;->a(II)V

    .line 2090943
    iget-object v0, p0, LX/ED6;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {p0, v0}, LX/ED6;->e(I)V

    goto :goto_0

    .line 2090944
    :pswitch_1
    iget-object v0, p0, LX/ED6;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, LX/ED6;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a010e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, LX/ED6;->a(II)V

    .line 2090945
    iget-object v0, p0, LX/ED6;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {p0, v0}, LX/ED6;->e(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static h(LX/ED6;)V
    .locals 3

    .prologue
    .line 2090898
    iget-object v0, p0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    if-nez v0, :cond_1

    .line 2090899
    new-instance v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    invoke-direct {v0}, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;-><init>()V

    iput-object v0, p0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    .line 2090900
    iget-object v0, p0, LX/ED6;->i:LX/0Px;

    sget-object v1, LX/ED5;->ADD_NEW:LX/ED5;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090901
    iget-object v0, p0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    const/4 v1, 0x1

    .line 2090902
    iput-boolean v1, v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->o:Z

    .line 2090903
    iget-object v2, v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    if-nez v2, :cond_2

    .line 2090904
    :cond_0
    :goto_0
    invoke-direct {p0}, LX/ED6;->g()V

    .line 2090905
    :cond_1
    return-void

    .line 2090906
    :cond_2
    iget-object v2, v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    invoke-virtual {v2, v1}, LX/EEp;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2090887
    iget-object v0, p0, LX/ED6;->i:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ED5;

    .line 2090888
    sget-object v1, LX/ED3;->b:[I

    invoke-virtual {v0}, LX/ED5;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2090889
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2090890
    :pswitch_0
    iget-object v0, p0, LX/ED6;->k:Lcom/facebook/rtc/fragments/RtcEmptyFragment;

    if-nez v0, :cond_0

    .line 2090891
    new-instance v0, Lcom/facebook/rtc/fragments/RtcEmptyFragment;

    invoke-direct {v0}, Lcom/facebook/rtc/fragments/RtcEmptyFragment;-><init>()V

    iput-object v0, p0, LX/ED6;->k:Lcom/facebook/rtc/fragments/RtcEmptyFragment;

    .line 2090892
    :cond_0
    iget-object v0, p0, LX/ED6;->k:Lcom/facebook/rtc/fragments/RtcEmptyFragment;

    goto :goto_0

    .line 2090893
    :pswitch_1
    invoke-static {p0}, LX/ED6;->h(LX/ED6;)V

    .line 2090894
    iget-object v0, p0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    goto :goto_0

    .line 2090895
    :pswitch_2
    iget-object v0, p0, LX/ED6;->l:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_1

    .line 2090896
    iget-object v0, p0, LX/ED6;->e:LX/EG3;

    invoke-interface {v0}, LX/EG3;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, LX/ED6;->l:Landroid/support/v4/app/Fragment;

    .line 2090897
    :cond_1
    iget-object v0, p0, LX/ED6;->l:Landroid/support/v4/app/Fragment;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2090886
    iget-object v0, p0, LX/ED6;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 2090877
    iget-object v0, p0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 2090878
    :goto_0
    invoke-static {p0}, LX/ED6;->h(LX/ED6;)V

    .line 2090879
    iget-object v1, p0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    .line 2090880
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->b:LX/EDx;

    if-nez v2, :cond_3

    .line 2090881
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 2090882
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2090883
    :cond_1
    return-void

    .line 2090884
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2090885
    :cond_3
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    iget-object v3, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->b:LX/EDx;

    invoke-virtual {v3}, LX/EDx;->bj()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/EEp;->a(LX/0Px;)V

    goto :goto_1
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2090866
    iget-object v0, p0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    if-nez v0, :cond_0

    .line 2090867
    :goto_0
    return-void

    .line 2090868
    :cond_0
    iget-object v0, p0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    .line 2090869
    iget-object v1, v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    if-nez v1, :cond_1

    .line 2090870
    :goto_1
    goto :goto_0

    .line 2090871
    :cond_1
    iget-object v1, v0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    .line 2090872
    iget-object p0, v1, LX/EEp;->s:LX/2CH;

    .line 2090873
    iget-object v0, p0, LX/2CH;->D:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->clear()V

    .line 2090874
    invoke-virtual {p0}, LX/2CH;->b()V

    .line 2090875
    iget-object p0, v1, LX/EEp;->s:LX/2CH;

    iget-object v0, v1, LX/EEp;->p:LX/3Mg;

    invoke-virtual {p0, v0}, LX/2CH;->b(LX/3Mg;)V

    .line 2090876
    goto :goto_1
.end method
