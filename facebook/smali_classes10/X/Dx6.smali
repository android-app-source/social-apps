.class public LX/Dx6;
.super LX/Dvb;
.source ""


# instance fields
.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DvH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0Ot;LX/0Ot;LX/DwK;LX/Dw8;)V
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dux;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvg;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "LX/DvH;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/DwK;",
            "LX/Dw8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2062411
    invoke-interface {p4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Dvf;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v5, p7

    move-object v6, p5

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-direct/range {v0 .. v8}, LX/Dvb;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/Dvf;LX/0Ot;Ljava/lang/String;LX/DwK;LX/Dw8;)V

    .line 2062412
    iput-object p6, p0, LX/Dx6;->t:LX/0Ot;

    .line 2062413
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 6

    .prologue
    .line 2062409
    const-string v2, "LoadScreenImagesPhotosOf"

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-super/range {v0 .. v5}, LX/Dvb;->a(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 2062410
    return-void
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 2062402
    iget-boolean v0, p0, LX/Dvb;->m:Z

    move v0, v0

    .line 2062403
    if-nez v0, :cond_0

    .line 2062404
    :goto_0
    return-void

    .line 2062405
    :cond_0
    iget-object v0, p0, LX/Dvb;->h:LX/Dv0;

    if-nez v0, :cond_1

    .line 2062406
    iget-object v1, p0, LX/Dvb;->j:Ljava/lang/String;

    iget-object v2, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    const-string v3, "LoadScreenImagesPhotosOf"

    iget-boolean v4, p0, LX/Dvb;->o:Z

    iget-boolean v5, p0, LX/Dvb;->p:Z

    iget-boolean v6, p0, LX/Dvb;->r:Z

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, LX/Dvb;->a(Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Ljava/lang/String;ZZZ)V

    .line 2062407
    :cond_1
    iget-object v0, p0, LX/Dvb;->q:LX/DwK;

    const-string v1, "LoadImageURLs"

    invoke-virtual {v0, v1}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2062408
    iget-object v0, p0, LX/Dvb;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {p0}, LX/Dvb;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Dx4;

    invoke-direct {v2, p0}, LX/Dx4;-><init>(LX/Dx6;)V

    new-instance v3, LX/Dx5;

    invoke-direct {v3, p0}, LX/Dx5;-><init>(LX/Dx6;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2062401
    const-string v0, "fetchTaggedMediaSet_%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/DvW;
    .locals 1

    .prologue
    .line 2062400
    sget-object v0, LX/DvW;->TAGGED_MEDIA_SET:LX/DvW;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2062399
    const/4 v0, 0x0

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 2062397
    iget-boolean v0, p0, LX/Dvb;->m:Z

    move v0, v0

    .line 2062398
    return v0
.end method
