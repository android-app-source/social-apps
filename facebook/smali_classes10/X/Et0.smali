.class public LX/Et0;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Esy;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Et3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2176868
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Et0;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Et3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176869
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2176870
    iput-object p1, p0, LX/Et0;->b:LX/0Ot;

    .line 2176871
    return-void
.end method

.method public static a(LX/0QB;)LX/Et0;
    .locals 4

    .prologue
    .line 2176872
    const-class v1, LX/Et0;

    monitor-enter v1

    .line 2176873
    :try_start_0
    sget-object v0, LX/Et0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176874
    sput-object v2, LX/Et0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176875
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176876
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176877
    new-instance v3, LX/Et0;

    const/16 p0, 0x1f36

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Et0;-><init>(LX/0Ot;)V

    .line 2176878
    move-object v0, v3

    .line 2176879
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176880
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Et0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176881
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176882
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2176883
    check-cast p2, LX/Esz;

    .line 2176884
    iget-object v0, p0, LX/Et0;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Et3;

    iget-object v1, p2, LX/Esz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Esz;->b:LX/1Pm;

    .line 2176885
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2176886
    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    .line 2176887
    invoke-static {v3}, LX/Esx;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "otd_permalink"

    :goto_0
    move-object v4, v4

    .line 2176888
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2176889
    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v3}, LX/Esx;->b(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v3

    .line 2176890
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2176891
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->j()Z

    move-result v5

    .line 2176892
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->k()Z

    move-result v6

    .line 2176893
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2176894
    if-eqz v5, :cond_0

    .line 2176895
    iget-object v5, v0, LX/Et3;->f:Landroid/content/res/Resources;

    const v8, 0x7f082a1c

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2176896
    new-instance v8, LX/Esg;

    const v9, 0x7f0219c9

    const v10, 0x7f0a00e6

    .line 2176897
    const v11, -0x7a37a94e

    const/4 p0, 0x3

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v1, p0, p2

    const/4 p2, 0x1

    aput-object v2, p0, p2

    const/4 p2, 0x2

    aput-object v4, p0, p2

    invoke-static {p1, v11, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v11

    move-object v11, v11

    .line 2176898
    invoke-direct {v8, v5, v9, v10, v11}, LX/Esg;-><init>(Ljava/lang/CharSequence;IILX/1dQ;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2176899
    :cond_0
    if-eqz v6, :cond_1

    .line 2176900
    iget-object v5, v0, LX/Et3;->f:Landroid/content/res/Resources;

    const v6, 0x7f082a1b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2176901
    new-instance v6, LX/Esg;

    const v8, 0x7f020952

    const v9, 0x7f0a00e6

    .line 2176902
    const v10, 0x690b462c

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v3, v11, p0

    const/4 p0, 0x1

    aput-object v4, v11, p0

    invoke-static {p1, v10, v11}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v10

    move-object v3, v10

    .line 2176903
    invoke-direct {v6, v5, v8, v9, v3}, LX/Esg;-><init>(Ljava/lang/CharSequence;IILX/1dQ;)V

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2176904
    :cond_1
    move-object v3, v7

    .line 2176905
    iget-object v4, v0, LX/Et3;->a:LX/Esf;

    invoke-virtual {v4, p1}, LX/Esf;->c(LX/1De;)LX/Esd;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/Esd;->a(Ljava/util/List;)LX/Esd;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2176906
    return-object v0

    :cond_2
    const-string v4, "promotion"

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 2176907
    invoke-static {}, LX/1dS;->b()V

    .line 2176908
    iget v0, p1, LX/1dQ;->b:I

    .line 2176909
    sparse-switch v0, :sswitch_data_0

    .line 2176910
    :goto_0
    return-object v6

    .line 2176911
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2176912
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v2, v0, v3

    check-cast v2, Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v3, v0, v4

    check-cast v3, LX/1Pm;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v4, 0x2

    aget-object v4, v0, v4

    check-cast v4, Ljava/lang/String;

    move-object v0, p0

    .line 2176913
    iget-object v7, v0, LX/Et0;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/Et3;

    const/4 v5, 0x0

    .line 2176914
    iget-object v8, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 2176915
    check-cast v8, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v8}, LX/Esx;->b(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v9

    .line 2176916
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l()Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    move-result-object v8

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;->FRIENDVERSARY:Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    if-ne v8, p0, :cond_4

    .line 2176917
    iget-object v8, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 2176918
    check-cast v8, Lcom/facebook/graphql/model/FeedUnit;

    const/4 p0, 0x0

    .line 2176919
    iget-object p1, v7, LX/Et3;->h:LX/0Uh;

    const/16 p2, 0x4e1

    invoke-virtual {p1, p2, p0}, LX/0Uh;->a(IZ)Z

    move-result p1

    .line 2176920
    invoke-static {v8}, LX/Esx;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result p2

    if-eqz p2, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    const/4 p0, 0x1

    :cond_1
    move v8, p0

    .line 2176921
    if-eqz v8, :cond_4

    .line 2176922
    iget-object v8, v7, LX/Et3;->h:LX/0Uh;

    const/16 p0, 0x4dd

    invoke-virtual {v8, p0, v5}, LX/0Uh;->a(IZ)Z

    move-result v8

    .line 2176923
    iget-object p0, v7, LX/Et3;->i:LX/0ad;

    sget-object p1, LX/0c0;->Live:LX/0c0;

    sget-object p2, LX/0c1;->Off:LX/0c1;

    sget-short v0, LX/1Cs;->e:S

    invoke-interface {p0, p1, p2, v0, v5}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result p0

    .line 2176924
    if-nez v8, :cond_2

    if-eqz p0, :cond_3

    .line 2176925
    :cond_2
    invoke-static {v7, v2, v3, v4}, LX/Et3;->a(LX/Et3;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-interface {v8, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2176926
    :goto_1
    goto :goto_0

    .line 2176927
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2176928
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v3

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v1, v1, v4

    check-cast v1, Ljava/lang/String;

    .line 2176929
    iget-object v4, p0, LX/Et0;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Et3;

    .line 2176930
    iget-object v5, v4, LX/Et3;->b:LX/17W;

    iget-object v7, v4, LX/Et3;->e:Landroid/content/Context;

    sget-object v8, LX/0ax;->fC:Ljava/lang/String;

    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->n()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, p0

    const/4 p0, 0x1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l()Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, p0

    const/4 p0, 0x2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->t()Ljava/lang/String;

    move-result-object v2

    const-string v3, "default"

    invoke-static {v2, v3}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v9, p0

    const/4 p0, 0x3

    aput-object v1, v9, p0

    const/4 p0, 0x4

    aput-object v1, v9, p0

    invoke-static {v8, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2176931
    goto/16 :goto_0

    .line 2176932
    :cond_3
    invoke-static {v7, v9, v3, v4}, LX/Et3;->a$redex0(LX/Et3;Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;LX/1Pm;Ljava/lang/String;)V

    goto :goto_1

    .line 2176933
    :cond_4
    invoke-static {v7, v9, v3, v4}, LX/Et3;->a$redex0(LX/Et3;Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;LX/1Pm;Ljava/lang/String;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7a37a94e -> :sswitch_0
        0x690b462c -> :sswitch_1
    .end sparse-switch
.end method
