.class public LX/E4I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/reaction/ReactionUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:LX/E4E;

.field public final d:LX/2jY;

.field public e:Ljava/lang/String;

.field public f:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/E4E;LX/2jY;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/E4E;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2076464
    iput-object p1, p0, LX/E4I;->b:Ljava/lang/String;

    .line 2076465
    iput-object p2, p0, LX/E4I;->c:LX/E4E;

    .line 2076466
    iput-object p3, p0, LX/E4I;->d:LX/2jY;

    .line 2076467
    return-void
.end method

.method public static a(LX/9uc;)Z
    .locals 1
    .param p0    # LX/9uc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2076468
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/9uc;->ab()LX/5sY;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/9uc;->ab()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/E4I;LX/0us;)V
    .locals 1
    .param p0    # LX/E4I;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2076469
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/0us;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/E4I;->e:Ljava/lang/String;

    .line 2076470
    return-void

    .line 2076471
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionPaginatedSubComponents$Edges;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/9uc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2076472
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 2076473
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;

    .line 2076474
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    move-result-object v0

    .line 2076475
    invoke-static {v0}, LX/E4I;->a(LX/9uc;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2076476
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2076477
    :cond_1
    return-object v1
.end method
