.class public final LX/E59;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5A;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

.field public c:LX/174;

.field public d:LX/174;

.field public e:Z

.field public final synthetic f:LX/E5A;


# direct methods
.method public constructor <init>(LX/E5A;)V
    .locals 1

    .prologue
    .line 2077966
    iput-object p1, p0, LX/E59;->f:LX/E5A;

    .line 2077967
    move-object v0, p1

    .line 2077968
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2077969
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2077943
    const-string v0, "ReactionPlaceInfoBlurbUnitComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2077944
    if-ne p0, p1, :cond_1

    .line 2077945
    :cond_0
    :goto_0
    return v0

    .line 2077946
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2077947
    goto :goto_0

    .line 2077948
    :cond_3
    check-cast p1, LX/E59;

    .line 2077949
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2077950
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2077951
    if-eq v2, v3, :cond_0

    .line 2077952
    iget-object v2, p0, LX/E59;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E59;->a:Ljava/lang/String;

    iget-object v3, p1, LX/E59;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2077953
    goto :goto_0

    .line 2077954
    :cond_5
    iget-object v2, p1, LX/E59;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2077955
    :cond_6
    iget-object v2, p0, LX/E59;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E59;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    iget-object v3, p1, LX/E59;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2077956
    goto :goto_0

    .line 2077957
    :cond_8
    iget-object v2, p1, LX/E59;->b:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;

    if-nez v2, :cond_7

    .line 2077958
    :cond_9
    iget-object v2, p0, LX/E59;->c:LX/174;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E59;->c:LX/174;

    iget-object v3, p1, LX/E59;->c:LX/174;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2077959
    goto :goto_0

    .line 2077960
    :cond_b
    iget-object v2, p1, LX/E59;->c:LX/174;

    if-nez v2, :cond_a

    .line 2077961
    :cond_c
    iget-object v2, p0, LX/E59;->d:LX/174;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/E59;->d:LX/174;

    iget-object v3, p1, LX/E59;->d:LX/174;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2077962
    goto :goto_0

    .line 2077963
    :cond_e
    iget-object v2, p1, LX/E59;->d:LX/174;

    if-nez v2, :cond_d

    .line 2077964
    :cond_f
    iget-boolean v2, p0, LX/E59;->e:Z

    iget-boolean v3, p1, LX/E59;->e:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2077965
    goto :goto_0
.end method
