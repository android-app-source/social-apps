.class public final LX/Esw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Ljava/lang/String;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 2176652
    iput-object p1, p0, LX/Esw;->a:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iput-object p2, p0, LX/Esw;->b:Ljava/lang/String;

    iput-object p3, p0, LX/Esw;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x2

    const v0, 0x5785f86d

    invoke-static {v6, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2176653
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2176654
    iget-object v2, p0, LX/Esw;->a:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-static {v1, v2}, LX/Esx;->b(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;)Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "photos"

    aput-object v5, v3, v4

    iget-object v4, p0, LX/Esw;->b:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->a(Landroid/content/Context;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2176655
    iget-object v3, p0, LX/Esw;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2176656
    const v1, 0x34fc80fc

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
