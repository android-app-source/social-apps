.class public final LX/D9F;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/D9I;


# direct methods
.method public constructor <init>(LX/D9I;)V
    .locals 0

    .prologue
    .line 1969856
    iput-object p1, p0, LX/D9F;->a:LX/D9I;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/D9I;B)V
    .locals 0

    .prologue
    .line 1969838
    invoke-direct {p0, p1}, LX/D9F;-><init>(LX/D9I;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 1969846
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->h:LX/0wd;

    if-ne p1, v0, :cond_1

    .line 1969847
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-static {v0, v1}, LX/D9I;->setBaubleX(LX/D9I;F)V

    .line 1969848
    :cond_0
    :goto_0
    return-void

    .line 1969849
    :cond_1
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->i:LX/0wd;

    if-ne p1, v0, :cond_2

    .line 1969850
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-static {v0, v1}, LX/D9I;->setBaubleY(LX/D9I;F)V

    goto :goto_0

    .line 1969851
    :cond_2
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->j:LX/0wd;

    if-ne p1, v0, :cond_3

    .line 1969852
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->f:Landroid/view/View;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 1969853
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->f:Landroid/view/View;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0

    .line 1969854
    :cond_3
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->k:LX/0wd;

    if-ne p1, v0, :cond_0

    .line 1969855
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    invoke-static {v0}, LX/D9I;->g(LX/D9I;)V

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1969839
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->k:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->k:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1969840
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->j:LX/0wd;

    const-wide v2, 0x3fe6666666666666L    # 0.7

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1969841
    :cond_0
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    invoke-static {v0}, LX/D9I;->f(LX/D9I;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1969842
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    iget-object v0, v0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x4db524a5

    invoke-static {v0, v4, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1969843
    iget-object v0, p0, LX/D9F;->a:LX/D9I;

    .line 1969844
    iput-object v4, v0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1969845
    :cond_1
    return-void
.end method
