.class public final LX/EMX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/CzL;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2110772
    iput-object p1, p0, LX/EMX;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iput-object p2, p0, LX/EMX;->a:LX/CzL;

    iput-object p3, p0, LX/EMX;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x324c5288

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2110761
    iget-object v0, p0, LX/EMX;->a:LX/CzL;

    .line 2110762
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2110763
    check-cast v0, LX/8d1;

    .line 2110764
    iget-object v1, p0, LX/EMX;->a:LX/CzL;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->b(LX/CzL;)LX/CzL;

    move-result-object v3

    .line 2110765
    iget-object v1, p0, LX/EMX;->a:LX/CzL;

    if-ne v1, v3, :cond_0

    .line 2110766
    const v0, -0x77275acb

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2110767
    :goto_0
    return-void

    .line 2110768
    :cond_0
    iget-object v1, p0, LX/EMX;->b:LX/1Pn;

    check-cast v1, LX/CxA;

    iget-object v4, p0, LX/EMX;->a:LX/CzL;

    invoke-interface {v1, v4, v3}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2110769
    iget-object v1, p0, LX/EMX;->b:LX/1Pn;

    check-cast v1, LX/1Pq;

    invoke-interface {v1}, LX/1Pq;->iN_()V

    .line 2110770
    iget-object v1, p0, LX/EMX;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "apply_mutation_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/EMV;

    invoke-direct {v5, p0, v0}, LX/EMV;-><init>(LX/EMX;LX/8d1;)V

    new-instance v0, LX/EMW;

    invoke-direct {v0, p0, v3}, LX/EMW;-><init>(LX/EMX;LX/CzL;)V

    invoke-virtual {v1, v4, v5, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2110771
    const v0, -0x5d3c586c

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0
.end method
