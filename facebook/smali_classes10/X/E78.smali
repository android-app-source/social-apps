.class public abstract LX/E78;
.super LX/Cfk;
.source ""


# instance fields
.field public final a:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;LX/3Tx;)V
    .locals 0

    .prologue
    .line 2081161
    invoke-direct {p0, p2}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2081162
    iput-object p1, p0, LX/E78;->a:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    .line 2081163
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 1

    .prologue
    .line 2081160
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2081156
    const v0, 0x7f031173

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;

    .line 2081157
    iget-object v0, p0, LX/E78;->a:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->H()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    move-result-object v2

    iget-object v3, p0, LX/E78;->b:Ljava/lang/String;

    invoke-virtual {p0}, LX/E78;->g()I

    move-result v4

    new-instance v5, LX/E77;

    invoke-direct {v5, p0}, LX/E77;-><init>(LX/E78;)V

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILX/BeT;)V

    .line 2081158
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->setContentPaddingTop(Z)V

    .line 2081159
    return-object v1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2081155
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I
    .locals 1

    .prologue
    .line 2081152
    iput-object p1, p0, LX/E78;->b:Ljava/lang/String;

    .line 2081153
    iput-object p2, p0, LX/E78;->c:Ljava/lang/String;

    .line 2081154
    invoke-super {p0, p1, p2, p3}, LX/Cfk;->b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2081151
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->H()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Z

    move-result v0

    return v0
.end method

.method public abstract g()I
.end method
