.class public LX/Dbi;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/11S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2017433
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2017434
    const-class p1, LX/Dbi;

    invoke-static {p1, p0}, LX/Dbi;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2017435
    const p1, 0x7f030e9e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2017436
    const p1, 0x7f0d23ca

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/Dbi;->b:Landroid/widget/TextView;

    .line 2017437
    const p1, 0x7f0d23cb

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/Dbi;->c:Landroid/widget/TextView;

    .line 2017438
    const p1, 0x7f0d23cc

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, LX/Dbi;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2017439
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Dbi;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object p0

    check-cast p0, LX/11S;

    iput-object p0, p1, LX/Dbi;->a:LX/11S;

    return-void
.end method


# virtual methods
.method public setTimestamp(J)V
    .locals 7

    .prologue
    .line 2017440
    iget-object v0, p0, LX/Dbi;->c:Landroid/widget/TextView;

    iget-object v1, p0, LX/Dbi;->a:LX/11S;

    sget-object v2, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p1

    invoke-interface {v1, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2017441
    return-void
.end method
