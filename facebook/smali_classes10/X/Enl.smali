.class public abstract LX/Enl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/Eo3;

.field public c:LX/EnN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:LX/Enk;

.field public e:LX/Eno;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2167420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167421
    new-instance v0, LX/Enk;

    invoke-direct {v0, p0}, LX/Enk;-><init>(LX/Enl;)V

    iput-object v0, p0, LX/Enl;->d:LX/Enk;

    .line 2167422
    sget-object v0, LX/Eno;->UNINITIALIZED:LX/Eno;

    iput-object v0, p0, LX/Enl;->e:LX/Eno;

    .line 2167423
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/Enl;->a:Ljava/lang/String;

    .line 2167424
    new-instance v0, LX/Eo3;

    invoke-direct {v0}, LX/Eo3;-><init>()V

    iput-object v0, p0, LX/Enl;->b:LX/Eo3;

    .line 2167425
    return-void
.end method


# virtual methods
.method public final a()LX/Eo3;
    .locals 1

    .prologue
    .line 2167466
    iget-object v0, p0, LX/Enl;->b:LX/Eo3;

    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/String;
.end method

.method public final a(LX/0P1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "LX/Enn;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 2167450
    if-eqz p1, :cond_0

    .line 2167451
    invoke-virtual {p1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2167452
    iget-object v1, p0, LX/Enl;->b:LX/Eo3;

    move-object v3, v1

    .line 2167453
    new-instance v4, LX/Eo6;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Enn;

    .line 2167454
    iget-object v6, v1, LX/Enn;->a:Ljava/lang/String;

    move-object v1, v6

    .line 2167455
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v4, v5, v1, v0}, LX/Eo6;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 2167456
    :cond_0
    iget-object v0, p0, LX/Enl;->c:LX/EnN;

    if-eqz v0, :cond_1

    .line 2167457
    iget-object v0, p0, LX/Enl;->c:LX/EnN;

    .line 2167458
    iget-object v1, v0, LX/EnN;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    iget-object v1, v1, Lcom/facebook/entitycards/intent/EntityCardsFragment;->u:LX/Emc;

    .line 2167459
    if-eqz p1, :cond_1

    .line 2167460
    invoke-virtual {p1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 2167461
    iget-object v5, v1, LX/Emc;->o:LX/En9;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Enn;

    .line 2167462
    iget-object v0, v3, LX/Enn;->a:Ljava/lang/String;

    move-object v3, v0

    .line 2167463
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v3, v0}, LX/En9;->a(Ljava/lang/String;Ljava/lang/Object;)LX/Emg;

    move-result-object v3

    .line 2167464
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/Emg;->c(Ljava/lang/Object;)V

    goto :goto_1

    .line 2167465
    :cond_1
    return-void
.end method

.method public final a(LX/EnN;)V
    .locals 0
    .param p1    # LX/EnN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2167448
    iput-object p1, p0, LX/Enl;->c:LX/EnN;

    .line 2167449
    return-void
.end method

.method public abstract a(LX/Enq;)V
.end method

.method public final a(LX/Ens;LX/Eno;LX/Eno;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ens",
            "<",
            "LX/Enn;",
            "Ljava/lang/Object;",
            ">;",
            "LX/Eno;",
            "LX/Eno;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2167427
    iget-object v0, p0, LX/Enl;->c:LX/EnN;

    if-eqz v0, :cond_2

    .line 2167428
    iget-object v0, p0, LX/Enl;->c:LX/EnN;

    .line 2167429
    iget-object v1, v0, LX/EnN;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    iget-object v1, v1, Lcom/facebook/entitycards/intent/EntityCardsFragment;->u:LX/Emc;

    .line 2167430
    iget-object v2, v1, LX/Emc;->f:LX/Emx;

    sget-object v3, LX/Emv;->ADAPTER_UDPATED:LX/Emv;

    invoke-virtual {v2, v3}, LX/Emx;->a(LX/Emv;)V

    .line 2167431
    iget-object v2, v1, LX/Emc;->n:LX/Ens;

    .line 2167432
    iput-object p1, v1, LX/Emc;->n:LX/Ens;

    .line 2167433
    iget-object v3, v1, LX/Emc;->n:LX/Ens;

    .line 2167434
    invoke-virtual {v2}, LX/Ens;->b()Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v3}, LX/Ens;->b()Ljava/util/Set;

    move-result-object v5

    invoke-static {v4, v5}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v4

    .line 2167435
    invoke-virtual {v4}, LX/0Ro;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2167436
    iget-object v5, v1, LX/Emc;->g:LX/03V;

    const-string v6, "entity_cards_adapter_keys_removed_in_update"

    const-string v7, "Keys were removed on collection update! %d of %d old keys were removed."

    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-virtual {v4}, LX/0Ro;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, p0, p1

    const/4 v4, 0x1

    invoke-virtual {v2}, LX/Ens;->c()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, p0, v4

    invoke-static {v7, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2167437
    :cond_0
    invoke-virtual {v1}, LX/0gG;->kV_()V

    .line 2167438
    sget-object v2, LX/Eno;->INITIAL_ENTITIES_LOADED:LX/Eno;

    if-eq p2, v2, :cond_1

    sget-object v2, LX/Eno;->INITIAL_ENTITIES_LOADED:LX/Eno;

    if-ne p3, v2, :cond_1

    .line 2167439
    iget-object v2, v1, LX/Emc;->j:LX/Enl;

    invoke-virtual {v2}, LX/Enl;->i()I

    move-result v2

    invoke-static {v1, v2}, LX/Emc;->g(LX/Emc;I)V

    .line 2167440
    :cond_1
    iget-object v2, v1, LX/Emc;->h:LX/En0;

    iget-object v3, v1, LX/Emc;->h:LX/En0;

    .line 2167441
    iget v4, v3, LX/En0;->b:I

    move v3, v4

    .line 2167442
    invoke-virtual {v1, v3}, LX/Emc;->e(I)Ljava/lang/Object;

    move-result-object v3

    .line 2167443
    iget v4, v2, LX/En0;->b:I

    invoke-virtual {v2, v4, v3}, LX/En0;->a(ILjava/lang/Object;)V

    .line 2167444
    sget-object v1, LX/Eno;->UNINITIALIZED:LX/Eno;

    if-ne p2, v1, :cond_2

    sget-object v1, LX/Eno;->UNINITIALIZED:LX/Eno;

    if-eq p3, v1, :cond_2

    .line 2167445
    iget-object v1, v0, LX/EnN;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    iget-object v1, v1, Lcom/facebook/entitycards/intent/EntityCardsFragment;->s:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/EnN;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    iget-object v1, v1, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    if-eqz v1, :cond_2

    .line 2167446
    iget-object v1, v0, LX/EnN;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    iget-object v2, v0, LX/EnN;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    iget-object v2, v2, Lcom/facebook/entitycards/intent/EntityCardsFragment;->y:LX/Enl;

    invoke-static {v1, v2}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->a$redex0(Lcom/facebook/entitycards/intent/EntityCardsFragment;LX/Enl;)V

    .line 2167447
    :cond_2
    return-void
.end method

.method public abstract a(Ljava/lang/String;)Z
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract c(Ljava/lang/String;)Ljava/lang/Object;
.end method

.method public final d()LX/Enk;
    .locals 1

    .prologue
    .line 2167426
    iget-object v0, p0, LX/Enl;->d:LX/Enk;

    return-object v0
.end method

.method public abstract e()Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Enp;",
            ">;"
        }
    .end annotation
.end method

.method public abstract f()V
.end method

.method public abstract g()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract h()LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end method

.method public abstract i()I
.end method

.method public abstract j()LX/Eny;
.end method

.method public abstract k()Z
.end method

.method public abstract l()I
.end method
