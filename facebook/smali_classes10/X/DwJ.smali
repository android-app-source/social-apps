.class public LX/DwJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/DwJ;


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2060283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2060284
    iput v0, p0, LX/DwJ;->a:I

    .line 2060285
    iput v0, p0, LX/DwJ;->b:I

    .line 2060286
    iput v0, p0, LX/DwJ;->c:I

    .line 2060287
    return-void
.end method

.method public static a(LX/0QB;)LX/DwJ;
    .locals 3

    .prologue
    .line 2060288
    sget-object v0, LX/DwJ;->d:LX/DwJ;

    if-nez v0, :cond_1

    .line 2060289
    const-class v1, LX/DwJ;

    monitor-enter v1

    .line 2060290
    :try_start_0
    sget-object v0, LX/DwJ;->d:LX/DwJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2060291
    if-eqz v2, :cond_0

    .line 2060292
    :try_start_1
    new-instance v0, LX/DwJ;

    invoke-direct {v0}, LX/DwJ;-><init>()V

    .line 2060293
    move-object v0, v0

    .line 2060294
    sput-object v0, LX/DwJ;->d:LX/DwJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2060295
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2060296
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2060297
    :cond_1
    sget-object v0, LX/DwJ;->d:LX/DwJ;

    return-object v0

    .line 2060298
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2060299
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
