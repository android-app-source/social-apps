.class public LX/Cxf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pn;
.implements LX/1Pp;
.implements LX/1Pq;
.implements LX/1Pr;
.implements LX/1Ps;
.implements LX/Cx5;
.implements LX/CxG;
.implements LX/CxV;
.implements LX/Cxc;
.implements LX/Cxd;
.implements LX/Cxe;


# instance fields
.field private final a:LX/1QP;

.field private final b:Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

.field private final c:LX/1QR;

.field private final d:LX/1QD;

.field private final e:LX/1QF;

.field private final f:LX/1QG;

.field private final g:LX/CxH;

.field private final h:LX/CxW;

.field private final i:LX/Cy9;

.field private final j:LX/CyB;

.field private final k:LX/Cy1;

.field private final l:LX/Cx6;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;LX/CzB;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Cz2;LX/CzB;LX/CzB;LX/1Q6;Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;LX/1QC;LX/1QD;LX/1QF;LX/1QG;LX/CxI;LX/CxX;LX/CyA;LX/CyC;LX/Cy1;LX/Cx7;)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/CzB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/Cz2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/CzB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/CzB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951779
    invoke-static {p1}, LX/1Q6;->a(Landroid/content/Context;)LX/1QP;

    move-result-object v1

    iput-object v1, p0, LX/Cxf;->a:LX/1QP;

    .line 1951780
    move-object/from16 v0, p9

    iput-object v0, p0, LX/Cxf;->b:Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    .line 1951781
    invoke-static {p2}, LX/1QC;->a(Ljava/lang/Runnable;)LX/1QR;

    move-result-object v1

    iput-object v1, p0, LX/Cxf;->c:LX/1QR;

    .line 1951782
    move-object/from16 v0, p11

    iput-object v0, p0, LX/Cxf;->d:LX/1QD;

    .line 1951783
    move-object/from16 v0, p12

    iput-object v0, p0, LX/Cxf;->e:LX/1QF;

    .line 1951784
    move-object/from16 v0, p13

    iput-object v0, p0, LX/Cxf;->f:LX/1QG;

    .line 1951785
    invoke-static {p3}, LX/CxI;->a(LX/CzB;)LX/CxH;

    move-result-object v1

    iput-object v1, p0, LX/Cxf;->g:LX/CxH;

    .line 1951786
    invoke-static {p4}, LX/CxX;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;)LX/CxW;

    move-result-object v1

    iput-object v1, p0, LX/Cxf;->h:LX/CxW;

    move-object/from16 v1, p16

    move-object v2, p0

    move-object v3, p0

    move-object v4, p0

    move-object v5, p0

    move-object v6, p0

    move-object v7, p5

    .line 1951787
    invoke-virtual/range {v1 .. v7}, LX/CyA;->a(LX/Cx5;LX/1Pq;LX/CxV;LX/Cxc;LX/1Pn;LX/Cz2;)LX/Cy9;

    move-result-object v1

    iput-object v1, p0, LX/Cxf;->i:LX/Cy9;

    .line 1951788
    move-object/from16 v0, p17

    invoke-virtual {v0, p0, p0, p6}, LX/CyC;->a(LX/CxV;LX/CxG;LX/CzB;)LX/CyB;

    move-result-object v1

    iput-object v1, p0, LX/Cxf;->j:LX/CyB;

    .line 1951789
    move-object/from16 v0, p18

    iput-object v0, p0, LX/Cxf;->k:LX/Cy1;

    .line 1951790
    invoke-static {p7, p0}, LX/Cx7;->a(LX/CzB;LX/CxG;)LX/Cx6;

    move-result-object v1

    iput-object v1, p0, LX/Cxf;->l:LX/Cx6;

    .line 1951791
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1951792
    iget-object v0, p0, LX/Cxf;->g:LX/CxH;

    invoke-virtual {v0, p1}, LX/CxH;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1951777
    iget-object v0, p0, LX/Cxf;->e:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1951793
    iget-object v0, p0, LX/Cxf;->e:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1951796
    iget-object v0, p0, LX/Cxf;->b:Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1951797
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1951798
    iget-object v0, p0, LX/Cxf;->f:LX/1QG;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1QG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1951799
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 1

    .prologue
    .line 1951800
    iget-object v0, p0, LX/Cxf;->i:LX/Cy9;

    invoke-virtual {v0, p1}, LX/Cy9;->a(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 1951801
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1951802
    iget-object v0, p0, LX/Cxf;->l:LX/Cx6;

    invoke-virtual {v0, p1, p2}, LX/Cx6;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1951803
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1951804
    iget-object v0, p0, LX/Cxf;->b:Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    invoke-virtual {v0, p1}, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a(Ljava/lang/String;)V

    .line 1951805
    return-void
.end method

.method public final a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 1951806
    iget-object v0, p0, LX/Cxf;->c:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1951807
    return-void
.end method

.method public final a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1951808
    iget-object v0, p0, LX/Cxf;->c:LX/1QR;

    invoke-virtual {v0, p1}, LX/1QR;->a([Ljava/lang/Object;)V

    .line 1951809
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 1951810
    iget-object v0, p0, LX/Cxf;->e:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 1

    .prologue
    .line 1951794
    iget-object v0, p0, LX/Cxf;->i:LX/Cy9;

    invoke-virtual {v0, p1}, LX/Cy9;->b(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 1951795
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1951760
    iget-object v0, p0, LX/Cxf;->e:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->b(Ljava/lang/String;)V

    .line 1951761
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 1

    .prologue
    .line 1951762
    iget-object v0, p0, LX/Cxf;->j:LX/CyB;

    invoke-virtual {v0, p1}, LX/CyB;->c(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 1951763
    return-void
.end method

.method public final d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1951764
    iget-object v0, p0, LX/Cxf;->k:LX/Cy1;

    invoke-virtual {v0, p1}, LX/Cy1;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1951765
    iget-object v0, p0, LX/Cxf;->k:LX/Cy1;

    invoke-virtual {v0, p1}, LX/Cy1;->e(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 1951766
    iget-object v0, p0, LX/Cxf;->f:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1951767
    iget-object v0, p0, LX/Cxf;->a:LX/1QP;

    invoke-virtual {v0}, LX/1QP;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 1951768
    iget-object v0, p0, LX/Cxf;->f:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951769
    iget-object v0, p0, LX/Cxf;->f:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->i()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 1951770
    iget-object v0, p0, LX/Cxf;->f:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 1951775
    iget-object v0, p0, LX/Cxf;->c:LX/1QR;

    invoke-virtual {v0}, LX/1QR;->iN_()V

    .line 1951776
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951771
    iget-object v0, p0, LX/Cxf;->f:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->j()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1951772
    iget-object v0, p0, LX/Cxf;->f:LX/1QG;

    invoke-virtual {v0}, LX/1QG;->k()V

    .line 1951773
    return-void
.end method

.method public final t()Lcom/facebook/search/results/model/SearchResultsMutableContext;
    .locals 1

    .prologue
    .line 1951774
    iget-object v0, p0, LX/Cxf;->h:LX/CxW;

    invoke-virtual {v0}, LX/CxW;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    return-object v0
.end method
