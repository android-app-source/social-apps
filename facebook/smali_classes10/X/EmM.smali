.class public final enum LX/EmM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EmM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EmM;

.field public static final enum APP_UPDATE:LX/EmM;

.field public static final enum SIDELOADING:LX/EmM;

.field public static final enum UNKNOWN:LX/EmM;

.field public static final enum VIDEOAD:LX/EmM;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2165641
    new-instance v0, LX/EmM;

    const-string v1, "APP_UPDATE"

    invoke-direct {v0, v1, v2}, LX/EmM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EmM;->APP_UPDATE:LX/EmM;

    .line 2165642
    new-instance v0, LX/EmM;

    const-string v1, "SIDELOADING"

    invoke-direct {v0, v1, v3}, LX/EmM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EmM;->SIDELOADING:LX/EmM;

    .line 2165643
    new-instance v0, LX/EmM;

    const-string v1, "VIDEOAD"

    invoke-direct {v0, v1, v4}, LX/EmM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EmM;->VIDEOAD:LX/EmM;

    .line 2165644
    new-instance v0, LX/EmM;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, LX/EmM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EmM;->UNKNOWN:LX/EmM;

    .line 2165645
    const/4 v0, 0x4

    new-array v0, v0, [LX/EmM;

    sget-object v1, LX/EmM;->APP_UPDATE:LX/EmM;

    aput-object v1, v0, v2

    sget-object v1, LX/EmM;->SIDELOADING:LX/EmM;

    aput-object v1, v0, v3

    sget-object v1, LX/EmM;->VIDEOAD:LX/EmM;

    aput-object v1, v0, v4

    sget-object v1, LX/EmM;->UNKNOWN:LX/EmM;

    aput-object v1, v0, v5

    sput-object v0, LX/EmM;->$VALUES:[LX/EmM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2165646
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EmM;
    .locals 1

    .prologue
    .line 2165647
    const-class v0, LX/EmM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EmM;

    return-object v0
.end method

.method public static values()[LX/EmM;
    .locals 1

    .prologue
    .line 2165648
    sget-object v0, LX/EmM;->$VALUES:[LX/EmM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EmM;

    return-object v0
.end method
