.class public final LX/CpB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;LX/0Px;)V
    .locals 0

    .prologue
    .line 1936630
    iput-object p1, p0, LX/CpB;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iput-object p2, p0, LX/CpB;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, -0x1090a6a7

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1936631
    new-instance v2, LX/5OM;

    iget-object v0, p0, LX/CpB;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 1936632
    iget-object v0, p0, LX/CpB;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v2, v0}, LX/0ht;->c(Landroid/view/View;)V

    .line 1936633
    invoke-virtual {v2, v3}, LX/5OM;->a(Z)V

    .line 1936634
    new-instance v0, LX/Cp9;

    invoke-direct {v0, p0}, LX/Cp9;-><init>(LX/CpB;)V

    .line 1936635
    iput-object v0, v2, LX/0ht;->I:LX/2yQ;

    .line 1936636
    invoke-virtual {v2}, LX/5OM;->c()LX/5OG;

    move-result-object v3

    .line 1936637
    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, LX/CpB;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 1936638
    iget-object v4, p0, LX/CpB;->a:LX/0Px;

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    const v5, 0x7f020850

    invoke-virtual {v4, v5}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v4

    new-instance v5, LX/CpA;

    invoke-direct {v5, p0}, LX/CpA;-><init>(LX/CpB;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1936639
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1936640
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, LX/5OG;->a(Landroid/content/res/ColorStateList;)V

    .line 1936641
    invoke-virtual {v2}, LX/0ht;->d()V

    .line 1936642
    const v0, -0x3bd36404

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void
.end method
