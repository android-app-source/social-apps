.class public final LX/DXy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/base/fragment/FbFragment;

.field public final synthetic b:LX/DXz;


# direct methods
.method public constructor <init>(LX/DXz;Lcom/facebook/base/fragment/FbFragment;)V
    .locals 0

    .prologue
    .line 2011090
    iput-object p1, p0, LX/DXy;->b:LX/DXz;

    iput-object p2, p0, LX/DXy;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 2011075
    iget-object v0, p0, LX/DXy;->b:LX/DXz;

    iget-object v0, v0, LX/DXz;->a:LX/DYT;

    iget-object v1, p0, LX/DXy;->b:LX/DXz;

    iget-object v1, v1, LX/DXz;->c:Ljava/lang/String;

    iget-object v2, p0, LX/DXy;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    .line 2011076
    new-instance v2, LX/4Fz;

    invoke-direct {v2}, LX/4Fz;-><init>()V

    iget-object v3, v0, LX/DYT;->c:Ljava/lang/String;

    .line 2011077
    const-string p1, "actor_id"

    invoke-virtual {v2, p1, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2011078
    move-object v2, v2

    .line 2011079
    const-string v3, "group_id"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2011080
    move-object v2, v2

    .line 2011081
    const-string v3, "unknown"

    .line 2011082
    const-string p1, "source"

    invoke-virtual {v2, p1, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2011083
    move-object v2, v2

    .line 2011084
    new-instance v3, LX/DYz;

    invoke-direct {v3}, LX/DYz;-><init>()V

    move-object v3, v3

    .line 2011085
    const-string p1, "input"

    invoke-virtual {v3, p1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2011086
    iget-object v2, v0, LX/DYT;->d:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2011087
    new-instance v3, LX/DYR;

    invoke-direct {v3, v0, v1}, LX/DYR;-><init>(LX/DYT;Ljava/lang/String;)V

    iget-object p1, v0, LX/DYT;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2011088
    iget-object v0, p0, LX/DXy;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2011089
    const/4 v0, 0x1

    return v0
.end method
