.class public LX/Csn;
.super Landroid/text/method/LinkMovementMethod;
.source ""


# static fields
.field public static b:LX/Csn;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Cll;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1943214
    invoke-direct {p0}, Landroid/text/method/LinkMovementMethod;-><init>()V

    .line 1943215
    return-void
.end method

.method public static a(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)LX/Cll;
    .locals 3

    .prologue
    .line 1943216
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 1943217
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 1943218
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    .line 1943219
    invoke-virtual {p0}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1943220
    invoke-virtual {p0}, Landroid/widget/TextView;->getScrollX()I

    move-result v2

    add-int/2addr v0, v2

    .line 1943221
    invoke-virtual {p0}, Landroid/widget/TextView;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    .line 1943222
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 1943223
    invoke-virtual {v2, v1}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v1

    .line 1943224
    int-to-float v0, v0

    invoke-virtual {v2, v1, v0}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v0

    .line 1943225
    const-class v1, LX/Cll;

    invoke-interface {p1, v0, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cll;

    .line 1943226
    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)V
    .locals 2

    .prologue
    .line 1943227
    iget-object v0, p0, LX/Csn;->c:LX/Cll;

    if-eqz v0, :cond_0

    .line 1943228
    iget-object v0, p0, LX/Csn;->c:LX/Cll;

    const/4 v1, 0x0

    .line 1943229
    iput-boolean v1, v0, LX/Cll;->f:Z

    .line 1943230
    const/4 v0, 0x0

    iput-object v0, p0, LX/Csn;->c:LX/Cll;

    .line 1943231
    :cond_0
    invoke-static {p1}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    .line 1943232
    return-void
.end method

.method public final onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1943233
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1943234
    invoke-virtual {p0, p2}, LX/Csn;->a(Landroid/text/Spannable;)V

    .line 1943235
    invoke-super {p0, p1, p2, p3}, Landroid/text/method/LinkMovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1943236
    :goto_0
    return v0

    .line 1943237
    :pswitch_0
    :try_start_0
    invoke-static {p1, p2, p3}, LX/Csn;->a(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)LX/Cll;

    move-result-object v0

    .line 1943238
    if-eqz v0, :cond_0

    .line 1943239
    const/4 v1, 0x1

    .line 1943240
    iput-boolean v1, v0, LX/Cll;->f:Z

    .line 1943241
    invoke-interface {p2, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {p2, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    invoke-static {p2, v1, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 1943242
    iput-object v0, p0, LX/Csn;->c:LX/Cll;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1943243
    :cond_0
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1943244
    :catch_0
    move-exception v0

    .line 1943245
    iget-object v1, p0, LX/Csn;->a:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error while attempting to select span on touch down"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 1943246
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1943247
    move-object v0, v2

    .line 1943248
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_1

    .line 1943249
    :pswitch_1
    invoke-static {p1, p2, p3}, LX/Csn;->a(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)LX/Cll;

    move-result-object v0

    .line 1943250
    iget-object v1, p0, LX/Csn;->c:LX/Cll;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Csn;->c:LX/Cll;

    if-eq v0, v1, :cond_0

    .line 1943251
    invoke-virtual {p0, p2}, LX/Csn;->a(Landroid/text/Spannable;)V

    goto :goto_1

    .line 1943252
    :pswitch_2
    iget-object v0, p0, LX/Csn;->c:LX/Cll;

    if-eqz v0, :cond_1

    .line 1943253
    iget-object v0, p0, LX/Csn;->c:LX/Cll;

    invoke-virtual {v0, p1}, LX/Cll;->onClick(Landroid/view/View;)V

    .line 1943254
    :cond_1
    invoke-virtual {p0, p2}, LX/Csn;->a(Landroid/text/Spannable;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
