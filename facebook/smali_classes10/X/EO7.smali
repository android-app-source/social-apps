.class public LX/EO7;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EO8;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EO7",
            "<TE;>.java/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EO8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113938
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2113939
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/EO7;->b:LX/0Zi;

    .line 2113940
    iput-object p1, p0, LX/EO7;->a:LX/0Ot;

    .line 2113941
    return-void
.end method

.method public static a(LX/0QB;)LX/EO7;
    .locals 4

    .prologue
    .line 2113942
    const-class v1, LX/EO7;

    monitor-enter v1

    .line 2113943
    :try_start_0
    sget-object v0, LX/EO7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113944
    sput-object v2, LX/EO7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113945
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113946
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113947
    new-instance v3, LX/EO7;

    const/16 p0, 0x3457

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EO7;-><init>(LX/0Ot;)V

    .line 2113948
    move-object v0, v3

    .line 2113949
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113950
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EO7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113951
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113952
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2113953
    const v0, 0x3423d0ad

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2113954
    check-cast p2, LX/EO6;

    .line 2113955
    iget-object v0, p0, LX/EO7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EO8;

    .line 2113956
    iget-object v1, v0, LX/EO8;->d:LX/EPO;

    invoke-virtual {v1, p1}, LX/EPO;->c(LX/1De;)LX/EPM;

    move-result-object v1

    const p0, 0x7f0822c8

    .line 2113957
    iget-object p2, v1, LX/EPM;->a:LX/EPN;

    invoke-virtual {v1, p0}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/EPN;->a:Ljava/lang/CharSequence;

    .line 2113958
    iget-object p2, v1, LX/EPM;->d:Ljava/util/BitSet;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->set(I)V

    .line 2113959
    move-object v1, v1

    .line 2113960
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    .line 2113961
    const p0, 0x3423d0ad

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2113962
    invoke-interface {v1, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    const/16 p0, 0x8

    const p2, 0x7f0b0060

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2113963
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2113964
    invoke-static {}, LX/1dS;->b()V

    .line 2113965
    iget v0, p1, LX/1dQ;->b:I

    .line 2113966
    packed-switch v0, :pswitch_data_0

    .line 2113967
    :goto_0
    return-object v2

    .line 2113968
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2113969
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2113970
    check-cast v1, LX/EO6;

    .line 2113971
    iget-object v3, p0, LX/EO7;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EO8;

    iget-object p1, v1, LX/EO6;->a:LX/CzL;

    iget-object p2, v1, LX/EO6;->b:LX/CxV;

    invoke-virtual {v3, v0, p1, p2}, LX/EO8;->onClick(Landroid/view/View;LX/CzL;LX/CxV;)V

    .line 2113972
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3423d0ad
        :pswitch_0
    .end packed-switch
.end method
