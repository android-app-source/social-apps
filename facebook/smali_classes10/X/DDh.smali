.class public LX/DDh;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z

.field private b:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field private c:LX/Bsr;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private g:Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;

.field private h:Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1975931
    iget-boolean v0, p0, LX/DDh;->a:Z

    return v0
.end method

.method public getBlingBar()Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;
    .locals 1

    .prologue
    .line 1975952
    iget-object v0, p0, LX/DDh;->g:Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;

    return-object v0
.end method

.method public getFooter()Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;
    .locals 1

    .prologue
    .line 1975951
    iget-object v0, p0, LX/DDh;->h:Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    return-object v0
.end method

.method public getHeader()LX/Bsr;
    .locals 1

    .prologue
    .line 1975950
    iget-object v0, p0, LX/DDh;->c:LX/Bsr;

    return-object v0
.end method

.method public getPhoto()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1

    .prologue
    .line 1975949
    iget-object v0, p0, LX/DDh;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3467956

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1975945
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1975946
    const/4 v1, 0x1

    .line 1975947
    iput-boolean v1, p0, LX/DDh;->a:Z

    .line 1975948
    const/16 v1, 0x2d

    const v2, 0x293445de

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x58df586e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1975941
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1975942
    const/4 v1, 0x0

    .line 1975943
    iput-boolean v1, p0, LX/DDh;->a:Z

    .line 1975944
    const/16 v1, 0x2d

    const v2, -0x57d71b5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setPriceAndPickupNote(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1975939
    iget-object v0, p0, LX/DDh;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1975940
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1975937
    iget-object v0, p0, LX/DDh;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1975938
    return-void
.end method

.method public setWidth(I)V
    .locals 2

    .prologue
    .line 1975932
    iget-object v0, p0, LX/DDh;->b:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1975933
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1975934
    iget-object v1, p0, LX/DDh;->b:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1975935
    iget-object v0, p0, LX/DDh;->b:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->requestLayout()V

    .line 1975936
    return-void
.end method
