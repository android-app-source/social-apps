.class public LX/Cht;
.super LX/ChM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/ChM",
        "<",
        "LX/CiN;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v7/widget/RecyclerView;

.field private final b:LX/1P1;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 1928650
    invoke-direct {p0}, LX/ChM;-><init>()V

    .line 1928651
    iput-object p1, p0, LX/Cht;->a:Landroid/support/v7/widget/RecyclerView;

    .line 1928652
    iget-object v0, p0, LX/Cht;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    iput-object v0, p0, LX/Cht;->b:LX/1P1;

    .line 1928653
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/CiN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1928654
    const-class v0, LX/CiN;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 1928655
    check-cast p1, LX/CiN;

    const/4 v3, 0x0

    .line 1928656
    iget-object v0, p1, LX/CiN;->b:Landroid/view/View;

    move-object v0, v0

    .line 1928657
    iget-object v1, p1, LX/CiN;->a:LX/CiM;

    move-object v1, v1

    .line 1928658
    sget-object v2, LX/CiM;->SET_FOCUSED_VIEW:LX/CiM;

    if-ne v1, v2, :cond_1

    .line 1928659
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v2, p0, LX/Cht;->a:Landroid/support/v7/widget/RecyclerView;

    if-ne v1, v2, :cond_0

    .line 1928660
    iput-object v0, p0, LX/Cht;->c:Landroid/view/View;

    .line 1928661
    :cond_0
    :goto_0
    return-void

    .line 1928662
    :cond_1
    sget-object v2, LX/CiM;->UNSET_FOCUSED_VIEW:LX/CiM;

    if-ne v1, v2, :cond_3

    .line 1928663
    iget-object v1, p0, LX/Cht;->a:Landroid/support/v7/widget/RecyclerView;

    if-ne v0, v1, :cond_0

    .line 1928664
    iget-object v0, p0, LX/Cht;->c:Landroid/view/View;

    instance-of v0, v0, LX/Cre;

    if-eqz v0, :cond_2

    .line 1928665
    iget-object v0, p0, LX/Cht;->c:Landroid/view/View;

    check-cast v0, LX/Cre;

    sget-object v1, LX/Crd;->UNFOCUSED:LX/Crd;

    invoke-interface {v0, v1}, LX/Cre;->a(LX/Crd;)Z

    .line 1928666
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cht;->c:Landroid/view/View;

    goto :goto_0

    .line 1928667
    :cond_3
    sget-object v2, LX/CiM;->SCROLL_FOCUSED_VIEW_TO_RECT:LX/CiM;

    if-ne v1, v2, :cond_0

    .line 1928668
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v2, p0, LX/Cht;->a:Landroid/support/v7/widget/RecyclerView;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/Cht;->c:Landroid/view/View;

    if-ne v0, v1, :cond_0

    .line 1928669
    iget-object v0, p1, LX/CiN;->c:Landroid/graphics/Rect;

    move-object v0, v0

    .line 1928670
    iget-object v1, p0, LX/Cht;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    .line 1928671
    iget-object v2, p0, LX/Cht;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v2, v0

    .line 1928672
    if-eqz v1, :cond_4

    iget-object v2, p0, LX/Cht;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v2

    invoke-virtual {v2}, LX/1OR;->g()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1928673
    iget-object v0, p0, LX/Cht;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    goto :goto_0

    .line 1928674
    :cond_4
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/Cht;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v1

    invoke-virtual {v1}, LX/1OR;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1928675
    iget-object v1, p0, LX/Cht;->b:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    if-nez v1, :cond_5

    if-gez v0, :cond_5

    .line 1928676
    iget-object v1, p0, LX/Cht;->b:LX/1P1;

    invoke-virtual {v1, v3}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 1928677
    if-gez v1, :cond_0

    .line 1928678
    iget-object v2, p0, LX/Cht;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    goto/16 :goto_0

    .line 1928679
    :cond_5
    iget-object v1, p0, LX/Cht;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    goto/16 :goto_0
.end method
