.class public final LX/DG2;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

.field private final b:LX/Aox;

.field private final c:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;LX/Aox;LX/1Pr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Aox;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 1979167
    iput-object p1, p0, LX/DG2;->a:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    invoke-direct {p0}, LX/2h0;-><init>()V

    .line 1979168
    iput-object p2, p0, LX/DG2;->b:LX/Aox;

    .line 1979169
    iput-object p3, p0, LX/DG2;->c:LX/1Pr;

    .line 1979170
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1979171
    iget-object v1, p0, LX/DG2;->b:LX/Aox;

    iget-object v0, p0, LX/DG2;->b:LX/Aox;

    .line 1979172
    iget-object v2, v0, LX/Aox;->a:Ljava/lang/Boolean;

    move-object v0, v2

    .line 1979173
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, LX/Aox;->a(Z)V

    .line 1979174
    iget-object v0, p0, LX/DG2;->a:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->f:LX/03V;

    const-class v1, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1979175
    iget-object v0, p0, LX/DG2;->b:LX/Aox;

    .line 1979176
    iget-object v1, v0, LX/Aox;->a:Ljava/lang/Boolean;

    move-object v0, v1

    .line 1979177
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1979178
    iget-object v0, p0, LX/DG2;->a:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->g:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08185b

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1979179
    :goto_1
    iget-object v0, p0, LX/DG2;->c:LX/1Pr;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 1979180
    return-void

    .line 1979181
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1979182
    :cond_1
    iget-object v0, p0, LX/DG2;->a:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->g:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08185a

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1979183
    return-void
.end method
