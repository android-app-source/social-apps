.class public final LX/DP1;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DP3;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/DP3;


# direct methods
.method public constructor <init>(LX/DP3;)V
    .locals 1

    .prologue
    .line 1992952
    iput-object p1, p0, LX/DP1;->b:LX/DP3;

    .line 1992953
    move-object v0, p1

    .line 1992954
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1992955
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1992956
    const-string v0, "ApprovalBarComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1992957
    if-ne p0, p1, :cond_1

    .line 1992958
    :cond_0
    :goto_0
    return v0

    .line 1992959
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1992960
    goto :goto_0

    .line 1992961
    :cond_3
    check-cast p1, LX/DP1;

    .line 1992962
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1992963
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1992964
    if-eq v2, v3, :cond_0

    .line 1992965
    iget-object v2, p0, LX/DP1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/DP1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/DP1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1992966
    goto :goto_0

    .line 1992967
    :cond_4
    iget-object v2, p1, LX/DP1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
