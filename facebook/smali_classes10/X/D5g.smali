.class public final LX/D5g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D5f;


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V
    .locals 0

    .prologue
    .line 1963782
    iput-object p1, p0, LX/D5g;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1963783
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    .line 1963784
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1963785
    iget-object v0, p0, LX/D5g;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->k:LX/AjP;

    invoke-virtual {v0, p1, v2}, LX/AjP;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    .line 1963786
    invoke-static {p1}, LX/14w;->o(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1963787
    iget-object v0, p0, LX/D5g;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->R:Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;

    iget-object v1, p0, LX/D5g;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081a63

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedHeaderView;->setBadgeText(Ljava/lang/String;)V

    .line 1963788
    iget-object v0, p0, LX/D5g;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->D:LX/0sV;

    iget-boolean v0, v0, LX/0sV;->q:Z

    if-eqz v0, :cond_0

    .line 1963789
    iget-object v0, p0, LX/D5g;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->af:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1963790
    :cond_0
    :goto_0
    return-void

    .line 1963791
    :cond_1
    iget-object v0, p0, LX/D5g;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getVideoPrefetchVisitor(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)LX/1ly;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ly;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 1963792
    :cond_2
    instance-of v0, p1, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    if-eqz v0, :cond_0

    .line 1963793
    check-cast p1, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    .line 1963794
    iget-object v0, p0, LX/D5g;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->k:LX/AjP;

    .line 1963795
    iget-object v1, p1, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1963796
    invoke-virtual {v0, v1, v2}, LX/AjP;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    goto :goto_0
.end method
