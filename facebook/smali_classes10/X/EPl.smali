.class public final LX/EPl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/394;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/EPm;


# direct methods
.method public constructor <init>(LX/EPm;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2117497
    iput-object p1, p0, LX/EPl;->b:LX/EPm;

    iput-object p2, p0, LX/EPl;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 2

    .prologue
    .line 2117487
    iget-object v0, p0, LX/EPl;->b:LX/EPm;

    iget-object v0, v0, LX/EPm;->a:LX/CyM;

    const/4 v1, 0x1

    .line 2117488
    iput-boolean v1, v0, LX/CyM;->e:Z

    .line 2117489
    iget-object v0, p0, LX/EPl;->b:LX/EPm;

    iget-object v0, v0, LX/EPm;->a:LX/CyM;

    invoke-virtual {v0}, LX/CyM;->b()LX/2oO;

    move-result-object v0

    .line 2117490
    if-eqz v0, :cond_0

    .line 2117491
    invoke-virtual {v0}, LX/2oO;->a()V

    .line 2117492
    invoke-virtual {v0}, LX/2oO;->i()V

    .line 2117493
    :cond_0
    iget-object v0, p0, LX/EPl;->b:LX/EPm;

    iget-object v0, v0, LX/EPm;->e:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->c:LX/EPe;

    .line 2117494
    iget-object v1, v0, LX/EPe;->a:LX/1Aa;

    move-object v0, v1

    .line 2117495
    invoke-virtual {v0}, LX/1Aa;->d()V

    .line 2117496
    return-void
.end method

.method public final a(LX/04g;LX/7Jv;)V
    .locals 3

    .prologue
    .line 2117476
    iget-object v0, p0, LX/EPl;->b:LX/EPm;

    iget-object v0, v0, LX/EPm;->a:LX/CyM;

    invoke-virtual {v0}, LX/CyM;->b()LX/2oO;

    move-result-object v0

    .line 2117477
    iget-object v1, p0, LX/EPl;->b:LX/EPm;

    iget-object v1, v1, LX/EPm;->a:LX/CyM;

    iget v2, p2, LX/7Jv;->c:I

    invoke-virtual {v1, v2}, LX/CyM;->a(I)V

    .line 2117478
    if-eqz v0, :cond_0

    .line 2117479
    iget-boolean v1, p2, LX/7Jv;->b:Z

    iget-boolean v2, p2, LX/7Jv;->a:Z

    invoke-virtual {v0, v1, v2}, LX/2oO;->a(ZZ)V

    .line 2117480
    :cond_0
    iget-object v0, p0, LX/EPl;->b:LX/EPm;

    iget-object v0, v0, LX/EPm;->a:LX/CyM;

    const/4 v1, 0x0

    .line 2117481
    iput-boolean v1, v0, LX/CyM;->e:Z

    .line 2117482
    iget-object v0, p0, LX/EPl;->b:LX/EPm;

    iget-object v0, v0, LX/EPm;->e:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->c:LX/EPe;

    .line 2117483
    iget-object v1, v0, LX/EPe;->a:LX/1Aa;

    move-object v0, v1

    .line 2117484
    invoke-virtual {v0}, LX/1Aa;->f()V

    .line 2117485
    iget-object v0, p0, LX/EPl;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 2117486
    return-void
.end method
