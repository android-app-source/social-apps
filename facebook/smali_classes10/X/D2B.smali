.class public LX/D2B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/D2Q;

.field private final b:LX/7SE;

.field private c:LX/BUz;

.field private d:LX/BV0;

.field public e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

.field public f:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;


# direct methods
.method public constructor <init>(LX/D2Q;LX/BV0;LX/7SE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1958291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958292
    iput-object p1, p0, LX/D2B;->a:LX/D2Q;

    .line 1958293
    iput-object p2, p0, LX/D2B;->d:LX/BV0;

    .line 1958294
    iput-object p3, p0, LX/D2B;->b:LX/7SE;

    .line 1958295
    return-void
.end method

.method public static b(LX/D2B;)V
    .locals 1

    .prologue
    .line 1958288
    invoke-virtual {p0}, LX/D2B;->a()V

    .line 1958289
    const/4 v0, 0x0

    iput-object v0, p0, LX/D2B;->c:LX/BUz;

    .line 1958290
    return-void
.end method

.method public static c(LX/D2B;)LX/BUz;
    .locals 5

    .prologue
    .line 1958264
    iget-object v0, p0, LX/D2B;->c:LX/BUz;

    if-eqz v0, :cond_0

    .line 1958265
    iget-object v0, p0, LX/D2B;->c:LX/BUz;

    .line 1958266
    :goto_0
    return-object v0

    .line 1958267
    :cond_0
    iget-object v0, p0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1958268
    iget-object v0, p0, LX/D2B;->b:LX/7SE;

    invoke-virtual {v0}, LX/7SE;->a()LX/7SD;

    move-result-object v1

    .line 1958269
    iget-object v0, p0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1958270
    iget-object v2, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v2

    .line 1958271
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1958272
    iget-object v2, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v2

    .line 1958273
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1958274
    iget-object v0, p0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1958275
    iget-object v2, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v2

    .line 1958276
    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/7SD;->a(Landroid/net/Uri;)LX/7SD;

    .line 1958277
    :cond_1
    sget-object v0, LX/7Sv;->NONE:LX/7Sv;

    .line 1958278
    iget-object v2, p0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1958279
    iget-object v3, v2, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v2, v3

    .line 1958280
    if-eqz v2, :cond_2

    iget-object v2, p0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1958281
    iget-object v3, v2, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v2, v3

    .line 1958282
    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->shouldFlipHorizontally()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1958283
    sget-object v0, LX/7Sv;->MIRROR_HORIZONTALLY:LX/7Sv;

    .line 1958284
    :cond_2
    iget-object v2, p0, LX/D2B;->d:LX/BV0;

    iget-object v3, p0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1958285
    iget-object v4, v3, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    move-object v3, v4

    .line 1958286
    iget-object v4, p0, LX/D2B;->f:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-virtual {v1}, LX/7SD;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1, v0}, LX/BV0;->a(Landroid/net/Uri;Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;Ljava/util/List;LX/7Sv;)LX/BUz;

    move-result-object v0

    iput-object v0, p0, LX/D2B;->c:LX/BUz;

    .line 1958287
    iget-object v0, p0, LX/D2B;->c:LX/BUz;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1958296
    iget-object v0, p0, LX/D2B;->c:LX/BUz;

    if-eqz v0, :cond_0

    .line 1958297
    iget-object v0, p0, LX/D2B;->c:LX/BUz;

    invoke-virtual {v0}, LX/BUz;->a()V

    .line 1958298
    const/4 v0, 0x0

    iput-object v0, p0, LX/D2B;->c:LX/BUz;

    .line 1958299
    :cond_0
    return-void
.end method

.method public final a(ILX/D1v;F)V
    .locals 3

    .prologue
    .line 1958249
    iget-object v0, p0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1958250
    iget-object v0, p0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->h()Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1958251
    iget-object v0, p0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->i()Landroid/graphics/RectF;

    move-result-object v0

    .line 1958252
    new-instance v1, LX/D27;

    invoke-direct {v1, p0, p1, v0}, LX/D27;-><init>(LX/D2B;ILandroid/graphics/RectF;)V

    move-object v0, v1

    .line 1958253
    :goto_0
    new-instance v1, LX/D28;

    invoke-direct {v1, p0, p2, p1}, LX/D28;-><init>(LX/D2B;LX/D1v;I)V

    move-object v1, v1

    .line 1958254
    iget-object v2, p0, LX/D2B;->a:LX/D2Q;

    .line 1958255
    new-instance p0, LX/D1q;

    .line 1958256
    new-instance p1, LX/D2O;

    invoke-direct {p1, v2, v1}, LX/D2O;-><init>(LX/D2Q;LX/0TF;)V

    move-object p1, p1

    .line 1958257
    invoke-direct {p0, v0, p1}, LX/D1q;-><init>(Ljava/util/concurrent/Callable;LX/0TF;)V

    iput-object p0, v2, LX/D2Q;->c:LX/D1q;

    .line 1958258
    iget-boolean p0, v2, LX/D2Q;->e:Z

    if-nez p0, :cond_0

    .line 1958259
    const/4 p0, 0x1

    iput-boolean p0, v2, LX/D2Q;->e:Z

    .line 1958260
    invoke-static {v2}, LX/D2Q;->c(LX/D2Q;)V

    .line 1958261
    :cond_0
    return-void

    .line 1958262
    :cond_1
    new-instance v0, LX/D26;

    invoke-direct {v0, p0, p1, p3}, LX/D26;-><init>(LX/D2B;IF)V

    move-object v0, v0

    .line 1958263
    goto :goto_0
.end method

.method public final a(ILX/D1w;FI)V
    .locals 3

    .prologue
    .line 1958239
    new-instance v0, LX/D29;

    invoke-direct {v0, p0, p4, p3}, LX/D29;-><init>(LX/D2B;IF)V

    .line 1958240
    new-instance v1, LX/D2A;

    invoke-direct {v1, p0, p2, p1, p4}, LX/D2A;-><init>(LX/D2B;LX/D1w;II)V

    .line 1958241
    iget-object v2, p0, LX/D2B;->a:LX/D2Q;

    .line 1958242
    iget-object p0, v2, LX/D2Q;->d:Ljava/util/List;

    new-instance p1, LX/D1q;

    .line 1958243
    new-instance p2, LX/D2P;

    invoke-direct {p2, v2, v1}, LX/D2P;-><init>(LX/D2Q;LX/0TF;)V

    move-object p2, p2

    .line 1958244
    invoke-direct {p1, v0, p2}, LX/D1q;-><init>(Ljava/util/concurrent/Callable;LX/0TF;)V

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1958245
    iget-boolean p0, v2, LX/D2Q;->e:Z

    if-nez p0, :cond_0

    .line 1958246
    const/4 p0, 0x1

    iput-boolean p0, v2, LX/D2Q;->e:Z

    .line 1958247
    invoke-static {v2}, LX/D2Q;->b(LX/D2Q;)V

    .line 1958248
    :cond_0
    return-void
.end method
