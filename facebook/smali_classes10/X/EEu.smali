.class public final enum LX/EEu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EEu;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EEu;

.field public static final enum CONFERENCE_VIDEO:LX/EEu;

.field public static final enum CONFERENCE_VOICE:LX/EEu;

.field public static final enum DIRECT_VIDEO:LX/EEu;

.field public static final enum INSTANT_VIDEO:LX/EEu;

.field public static final enum VOICE:LX/EEu;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2094386
    new-instance v0, LX/EEu;

    const-string v1, "VOICE"

    invoke-direct {v0, v1, v2}, LX/EEu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEu;->VOICE:LX/EEu;

    .line 2094387
    new-instance v0, LX/EEu;

    const-string v1, "DIRECT_VIDEO"

    invoke-direct {v0, v1, v3}, LX/EEu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEu;->DIRECT_VIDEO:LX/EEu;

    .line 2094388
    new-instance v0, LX/EEu;

    const-string v1, "INSTANT_VIDEO"

    invoke-direct {v0, v1, v4}, LX/EEu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEu;->INSTANT_VIDEO:LX/EEu;

    .line 2094389
    new-instance v0, LX/EEu;

    const-string v1, "CONFERENCE_VOICE"

    invoke-direct {v0, v1, v5}, LX/EEu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEu;->CONFERENCE_VOICE:LX/EEu;

    .line 2094390
    new-instance v0, LX/EEu;

    const-string v1, "CONFERENCE_VIDEO"

    invoke-direct {v0, v1, v6}, LX/EEu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EEu;->CONFERENCE_VIDEO:LX/EEu;

    .line 2094391
    const/4 v0, 0x5

    new-array v0, v0, [LX/EEu;

    sget-object v1, LX/EEu;->VOICE:LX/EEu;

    aput-object v1, v0, v2

    sget-object v1, LX/EEu;->DIRECT_VIDEO:LX/EEu;

    aput-object v1, v0, v3

    sget-object v1, LX/EEu;->INSTANT_VIDEO:LX/EEu;

    aput-object v1, v0, v4

    sget-object v1, LX/EEu;->CONFERENCE_VOICE:LX/EEu;

    aput-object v1, v0, v5

    sget-object v1, LX/EEu;->CONFERENCE_VIDEO:LX/EEu;

    aput-object v1, v0, v6

    sput-object v0, LX/EEu;->$VALUES:[LX/EEu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2094385
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static isConferenceCall(LX/EEu;)Z
    .locals 1

    .prologue
    .line 2094392
    sget-object v0, LX/EEu;->CONFERENCE_VIDEO:LX/EEu;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/EEu;->CONFERENCE_VOICE:LX/EEu;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNonInstantVideoCall(LX/EEu;)Z
    .locals 1

    .prologue
    .line 2094384
    sget-object v0, LX/EEu;->CONFERENCE_VIDEO:LX/EEu;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/EEu;->DIRECT_VIDEO:LX/EEu;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/EEu;
    .locals 1

    .prologue
    .line 2094383
    const-class v0, LX/EEu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EEu;

    return-object v0
.end method

.method public static values()[LX/EEu;
    .locals 1

    .prologue
    .line 2094382
    sget-object v0, LX/EEu;->$VALUES:[LX/EEu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EEu;

    return-object v0
.end method
