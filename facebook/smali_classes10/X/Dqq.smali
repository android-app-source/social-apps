.class public LX/Dqq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Dqq;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;",
            "Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;",
            "Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;",
            "Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048693
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Dqq;->a:Ljava/util/Map;

    .line 2048694
    iget-object v0, p0, LX/Dqq;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MENU_SECTION_WITH_INDEPENDENT_ROWS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048695
    iget-object v0, p0, LX/Dqq;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MENU_SECTION_WITH_REMOVABLE_ROWS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048696
    iget-object v0, p0, LX/Dqq;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->MULTI_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048697
    iget-object v0, p0, LX/Dqq;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SETTING_PAGE_SECTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048698
    iget-object v0, p0, LX/Dqq;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->SINGLE_SELECTOR:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048699
    iget-object v0, p0, LX/Dqq;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->TOGGLE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048700
    iput-object p5, p0, LX/Dqq;->b:LX/0Ot;

    .line 2048701
    return-void
.end method

.method public static a(LX/0QB;)LX/Dqq;
    .locals 9

    .prologue
    .line 2048679
    sget-object v0, LX/Dqq;->c:LX/Dqq;

    if-nez v0, :cond_1

    .line 2048680
    const-class v1, LX/Dqq;

    monitor-enter v1

    .line 2048681
    :try_start_0
    sget-object v0, LX/Dqq;->c:LX/Dqq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2048682
    if-eqz v2, :cond_0

    .line 2048683
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2048684
    new-instance v3, LX/Dqq;

    invoke-static {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;->a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;

    invoke-static {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;->a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;

    const/16 v8, 0x6fa

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/Dqq;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsBasicSetPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsRemovableGroupPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorGroupPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTogglePartDefinition;LX/0Ot;)V

    .line 2048685
    move-object v0, v3

    .line 2048686
    sput-object v0, LX/Dqq;->c:LX/Dqq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2048687
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2048688
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2048689
    :cond_1
    sget-object v0, LX/Dqq;->c:LX/Dqq;

    return-object v0

    .line 2048690
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2048691
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
