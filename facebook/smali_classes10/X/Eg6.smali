.class public LX/Eg6;
.super LX/Efo;
.source ""


# instance fields
.field public l:LX/AK2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/7ga;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final n:Lcom/facebook/resources/ui/FbTextView;

.field public final o:LX/EgG;

.field public final p:LX/5ON;

.field public q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

.field public r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/AudienceControlData;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/Eg3;

.field private final t:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2155817
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Eg6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2155818
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2155819
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Eg6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155820
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2155821
    invoke-direct {p0, p1, p2, p3}, LX/Efo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155822
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2155823
    iput-object v0, p0, LX/Eg6;->r:LX/0Px;

    .line 2155824
    new-instance v0, LX/Eg3;

    invoke-direct {v0, p0}, LX/Eg3;-><init>(LX/Eg6;)V

    iput-object v0, p0, LX/Eg6;->s:LX/Eg3;

    .line 2155825
    new-instance v0, LX/Eg4;

    invoke-direct {v0, p0}, LX/Eg4;-><init>(LX/Eg6;)V

    iput-object v0, p0, LX/Eg6;->t:Landroid/view/View$OnClickListener;

    .line 2155826
    const-class v0, LX/Eg6;

    invoke-static {v0, p0}, LX/Eg6;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2155827
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2155828
    const v1, 0x7f03136c

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2155829
    invoke-virtual {p0}, LX/Efo;->a()V

    .line 2155830
    const v0, 0x7f0d2ce3

    invoke-virtual {p0, v0}, LX/Eg6;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Eg6;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2155831
    new-instance v0, LX/EgG;

    invoke-direct {v0}, LX/EgG;-><init>()V

    iput-object v0, p0, LX/Eg6;->o:LX/EgG;

    .line 2155832
    new-instance v0, LX/5ON;

    invoke-virtual {p0}, LX/Eg6;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5ON;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Eg6;->p:LX/5ON;

    .line 2155833
    iget-object v0, p0, LX/Eg6;->p:LX/5ON;

    .line 2155834
    iput-boolean v2, v0, LX/5OM;->e:Z

    .line 2155835
    iget-object v0, p0, LX/Eg6;->p:LX/5ON;

    invoke-virtual {v0, v2}, LX/0ht;->e(Z)V

    .line 2155836
    iget-object v0, p0, LX/Eg6;->p:LX/5ON;

    invoke-virtual {v0, v3}, LX/0ht;->c(Z)V

    .line 2155837
    iget-object v0, p0, LX/Eg6;->p:LX/5ON;

    iget-object v1, p0, LX/Eg6;->o:LX/EgG;

    .line 2155838
    iput-object v1, v0, LX/5ON;->m:Landroid/widget/ListAdapter;

    .line 2155839
    iget-object v0, p0, LX/Eg6;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Eg6;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2155840
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Eg6;

    invoke-static {p0}, LX/AK2;->a(LX/0QB;)LX/AK2;

    move-result-object v1

    check-cast v1, LX/AK2;

    invoke-static {p0}, LX/7ga;->a(LX/0QB;)LX/7ga;

    move-result-object p0

    check-cast p0, LX/7ga;

    iput-object v1, p1, LX/Eg6;->l:LX/AK2;

    iput-object p0, p1, LX/Eg6;->m:LX/7ga;

    return-void
.end method

.method public static b(LX/Eg6;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2155781
    if-nez p1, :cond_0

    .line 2155782
    iget-object v0, p0, LX/Eg6;->n:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2155783
    :goto_0
    return-void

    .line 2155784
    :cond_0
    invoke-virtual {p0}, LX/Eg6;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0161

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2155785
    iget-object v1, p0, LX/Eg6;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2155786
    iget-object v0, p0, LX/Eg6;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 9

    .prologue
    .line 2155787
    invoke-virtual {p0}, LX/Efo;->h()V

    .line 2155788
    iget-object v0, p0, LX/Eg6;->q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 2155789
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v0, v1

    .line 2155790
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;

    .line 2155791
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2155792
    iput-object v1, p0, LX/Eg6;->r:LX/0Px;

    .line 2155793
    new-instance v1, LX/Eg5;

    invoke-direct {v1, v0}, LX/Eg5;-><init>(Lcom/facebook/audience/snacks/model/SnacksMyThread;)V

    move-object v1, v1

    .line 2155794
    iget-object v2, p0, LX/Efo;->f:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    invoke-virtual {v2, v1}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->a(LX/AKy;)V

    .line 2155795
    iget-object v1, p0, LX/Efo;->e:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    iget-object v2, p0, LX/Eg6;->q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 2155796
    iget-object v3, v2, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v2, v3

    .line 2155797
    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2155798
    iget-wide v7, v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->f:J

    move-wide v4, v7

    .line 2155799
    iget-object v3, p0, LX/Eg6;->q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 2155800
    iget-object v6, v3, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v3, v6

    .line 2155801
    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v4, v5, v3}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->a(Ljava/lang/String;JLandroid/net/Uri;)V

    .line 2155802
    iget v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->e:I

    move v1, v1

    .line 2155803
    invoke-static {p0, v1}, LX/Eg6;->b(LX/Eg6;I)V

    .line 2155804
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2155805
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0, p1}, LX/Efo;->a(ZI)V

    .line 2155806
    iget-object v0, p0, LX/Eg6;->l:LX/AK2;

    invoke-virtual {p0}, LX/Eg6;->getCurrentThreadId()Ljava/lang/String;

    move-result-object v1

    .line 2155807
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, LX/AK2;->a(LX/AK2;ZLjava/lang/String;)V

    .line 2155808
    const/4 v2, 0x1

    invoke-static {v0, v2, v1}, LX/AK2;->a(LX/AK2;ZLjava/lang/String;)V

    .line 2155809
    iget-boolean v0, p0, LX/Efo;->i:Z

    if-eqz v0, :cond_0

    .line 2155810
    invoke-virtual {p0}, LX/Efo;->g()V

    .line 2155811
    :cond_0
    iget-object v0, p0, LX/Eg6;->m:LX/7ga;

    iget-object v1, p0, LX/Eg6;->q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 2155812
    iget-object v2, v1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v2

    .line 2155813
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/Eg6;->getCurrentThreadId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Efo;->h:LX/Efj;

    .line 2155814
    iget v4, v3, LX/Efj;->b:I

    move v3, v4

    .line 2155815
    invoke-virtual {p0}, LX/Eg6;->getCurrentThreadMediaType()LX/7gk;

    move-result-object v4

    sget-object v5, LX/7gZ;->SELF:LX/7gZ;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/7ga;->a(Ljava/lang/String;Ljava/lang/String;ILX/7gk;LX/7gZ;Z)V

    .line 2155816
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2155773
    iget-object v0, p0, LX/Eg6;->q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 2155774
    iget-object v3, v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v0, v3

    .line 2155775
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2155776
    if-gt v0, v2, :cond_0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, LX/Eg6;->q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 2155777
    iget-object v3, v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v0, v3

    .line 2155778
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;

    .line 2155779
    iget-object v3, v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    move-object v0, v3

    .line 2155780
    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2155769
    invoke-super {p0}, LX/Efo;->e()V

    .line 2155770
    iget-object v0, p0, LX/Eg6;->l:LX/AK2;

    iget-object v1, p0, LX/Eg6;->s:LX/Eg3;

    .line 2155771
    iget-object p0, v0, LX/AK2;->c:LX/7gh;

    invoke-virtual {p0, v1}, LX/7gh;->a(Ljava/lang/Object;)V

    .line 2155772
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 2155765
    iget-object v0, p0, LX/Eg6;->l:LX/AK2;

    iget-object v1, p0, LX/Eg6;->s:LX/Eg3;

    .line 2155766
    iget-object v2, v0, LX/AK2;->c:LX/7gh;

    invoke-virtual {v2, v1}, LX/7gh;->b(Ljava/lang/Object;)V

    .line 2155767
    invoke-super {p0}, LX/Efo;->f()V

    .line 2155768
    return-void
.end method

.method public getCurrentThreadId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2155758
    iget-object v0, p0, LX/Eg6;->q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 2155759
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v0, v1

    .line 2155760
    iget-object v1, p0, LX/Efo;->h:LX/Efj;

    .line 2155761
    iget p0, v1, LX/Efj;->b:I

    move v1, p0

    .line 2155762
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;

    .line 2155763
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2155764
    return-object v0
.end method

.method public getCurrentThreadMediaType()LX/7gk;
    .locals 2

    .prologue
    .line 2155751
    iget-object v0, p0, LX/Eg6;->q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 2155752
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v0, v1

    .line 2155753
    iget-object v1, p0, LX/Efo;->h:LX/Efj;

    .line 2155754
    iget p0, v1, LX/Efj;->b:I

    move v1, p0

    .line 2155755
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;

    .line 2155756
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyThread;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2155757
    if-nez v0, :cond_0

    sget-object v0, LX/7gk;->PHOTO:LX/7gk;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/7gk;->VIDEO:LX/7gk;

    goto :goto_0
.end method

.method public getNumberOfStoriesInCurrentUserBucket()I
    .locals 1

    .prologue
    .line 2155748
    iget-object v0, p0, LX/Eg6;->q:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 2155749
    iget-object p0, v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v0, p0

    .line 2155750
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
