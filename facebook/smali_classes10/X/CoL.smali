.class public final LX/CoL;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static A:F

.field public static B:F

.field public static C:I

.field public static D:I

.field public static E:I

.field public static F:D

.field public static G:D

.field public static H:D

.field public static I:D

.field public static J:D

.field public static K:D

.field public static L:I

.field public static M:I

.field public static N:J

.field public static O:J

.field public static P:J

.field public static Q:I

.field public static final R:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static a:F

.field public static b:F

.field public static c:F

.field public static d:D

.field public static e:D

.field public static f:D

.field public static g:D

.field public static h:F

.field public static i:I

.field public static j:F

.field public static k:F

.field public static l:F

.field public static m:D

.field public static n:D

.field public static o:I

.field public static p:I

.field public static q:F

.field public static r:F

.field public static s:F

.field public static t:I

.field public static u:F

.field public static v:I

.field public static w:I

.field public static x:F

.field public static y:F

.field public static z:F


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0xf

    const/high16 v8, 0x43c80000    # 400.0f

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide v4, 0x3fc999999999999aL    # 0.2

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    .line 1935520
    const/high16 v0, 0x40400000    # 3.0f

    sput v0, LX/CoL;->a:F

    .line 1935521
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, LX/CoL;->b:F

    .line 1935522
    const v0, 0x3a83126f    # 0.001f

    sput v0, LX/CoL;->c:F

    .line 1935523
    const-wide v0, 0x406c200000000000L    # 225.0

    sput-wide v0, LX/CoL;->d:D

    .line 1935524
    const-wide/high16 v0, 0x403d000000000000L    # 29.0

    sput-wide v0, LX/CoL;->e:D

    .line 1935525
    const-wide v0, 0x3fd999999999999aL    # 0.4

    sput-wide v0, LX/CoL;->f:D

    .line 1935526
    const-wide v0, 0x3fefef9db22d0e56L    # 0.998

    sput-wide v0, LX/CoL;->g:D

    .line 1935527
    const/high16 v0, 0x41200000    # 10.0f

    sput v0, LX/CoL;->h:F

    .line 1935528
    sput v9, LX/CoL;->i:I

    .line 1935529
    const v0, 0x3d0f5c29    # 0.035f

    sput v0, LX/CoL;->j:F

    .line 1935530
    const v0, 0x3f666666    # 0.9f

    sput v0, LX/CoL;->k:F

    .line 1935531
    const/high16 v0, 0x41000000    # 8.0f

    sput v0, LX/CoL;->l:F

    .line 1935532
    sput-wide v4, LX/CoL;->m:D

    .line 1935533
    sput-wide v4, LX/CoL;->n:D

    .line 1935534
    const/16 v0, 0x5dc

    sput v0, LX/CoL;->o:I

    .line 1935535
    const/4 v0, 0x5

    sput v0, LX/CoL;->p:I

    .line 1935536
    const v0, 0x3ecccccd    # 0.4f

    sput v0, LX/CoL;->q:F

    .line 1935537
    sput v8, LX/CoL;->r:F

    .line 1935538
    const v0, 0x3c23d70a    # 0.01f

    sput v0, LX/CoL;->s:F

    .line 1935539
    const/16 v0, 0xfa

    sput v0, LX/CoL;->t:I

    .line 1935540
    const v0, 0x3e4ccccd    # 0.2f

    sput v0, LX/CoL;->u:F

    .line 1935541
    sput v9, LX/CoL;->v:I

    .line 1935542
    const/16 v0, 0x96

    sput v0, LX/CoL;->w:I

    .line 1935543
    sput v8, LX/CoL;->x:F

    .line 1935544
    const/high16 v0, 0x42700000    # 60.0f

    sput v0, LX/CoL;->y:F

    .line 1935545
    const v0, 0x44bb8000    # 1500.0f

    sput v0, LX/CoL;->z:F

    .line 1935546
    const/high16 v0, 0x43480000    # 200.0f

    sput v0, LX/CoL;->A:F

    .line 1935547
    const v0, 0x453b8000    # 3000.0f

    sput v0, LX/CoL;->B:F

    .line 1935548
    const/4 v0, 0x4

    sput v0, LX/CoL;->C:I

    .line 1935549
    const/16 v0, 0xc

    sput v0, LX/CoL;->D:I

    .line 1935550
    const/4 v0, 0x6

    sput v0, LX/CoL;->E:I

    .line 1935551
    sput-wide v6, LX/CoL;->F:D

    .line 1935552
    sput-wide v2, LX/CoL;->G:D

    .line 1935553
    sput-wide v6, LX/CoL;->H:D

    .line 1935554
    sput-wide v2, LX/CoL;->I:D

    .line 1935555
    sput-wide v2, LX/CoL;->J:D

    .line 1935556
    sput-wide v2, LX/CoL;->K:D

    .line 1935557
    const/16 v0, 0xc8

    sput v0, LX/CoL;->L:I

    .line 1935558
    const/16 v0, 0x5dc

    sput v0, LX/CoL;->M:I

    .line 1935559
    const-wide/16 v0, 0x4e20

    sput-wide v0, LX/CoL;->N:J

    .line 1935560
    const-wide/16 v0, 0x3a98

    sput-wide v0, LX/CoL;->O:J

    .line 1935561
    const-wide/16 v0, 0xbb8

    sput-wide v0, LX/CoL;->P:J

    .line 1935562
    const/16 v0, 0xa

    sput v0, LX/CoL;->Q:I

    .line 1935563
    new-instance v0, LX/CoK;

    invoke-direct {v0}, LX/CoK;-><init>()V

    sput-object v0, LX/CoL;->R:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1935519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
