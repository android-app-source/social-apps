.class public final enum LX/EUm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EUm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EUm;

.field public static final enum RELATED_VIDEO_CHANNEL:LX/EUm;

.field public static final enum SEE_MORE:LX/EUm;

.field public static final enum VIDEO_CHANNEL:LX/EUm;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2126524
    new-instance v0, LX/EUm;

    const-string v1, "SEE_MORE"

    invoke-direct {v0, v1, v2}, LX/EUm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EUm;->SEE_MORE:LX/EUm;

    new-instance v0, LX/EUm;

    const-string v1, "VIDEO_CHANNEL"

    invoke-direct {v0, v1, v3}, LX/EUm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EUm;->VIDEO_CHANNEL:LX/EUm;

    new-instance v0, LX/EUm;

    const-string v1, "RELATED_VIDEO_CHANNEL"

    invoke-direct {v0, v1, v4}, LX/EUm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EUm;->RELATED_VIDEO_CHANNEL:LX/EUm;

    .line 2126525
    const/4 v0, 0x3

    new-array v0, v0, [LX/EUm;

    sget-object v1, LX/EUm;->SEE_MORE:LX/EUm;

    aput-object v1, v0, v2

    sget-object v1, LX/EUm;->VIDEO_CHANNEL:LX/EUm;

    aput-object v1, v0, v3

    sget-object v1, LX/EUm;->RELATED_VIDEO_CHANNEL:LX/EUm;

    aput-object v1, v0, v4

    sput-object v0, LX/EUm;->$VALUES:[LX/EUm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2126526
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EUm;
    .locals 1

    .prologue
    .line 2126527
    const-class v0, LX/EUm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EUm;

    return-object v0
.end method

.method public static values()[LX/EUm;
    .locals 1

    .prologue
    .line 2126528
    sget-object v0, LX/EUm;->$VALUES:[LX/EUm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EUm;

    return-object v0
.end method
