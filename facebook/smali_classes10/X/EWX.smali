.class public abstract LX/EWX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWW;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2130217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2130218
    return-void
.end method


# virtual methods
.method public c()LX/EZL;
    .locals 1

    .prologue
    .line 2130219
    new-instance v0, LX/EZL;

    invoke-direct {v0}, LX/EZL;-><init>()V

    return-object v0
.end method

.method public final d()LX/EWc;
    .locals 3

    .prologue
    .line 2130220
    :try_start_0
    invoke-virtual {p0}, LX/EWX;->b()I

    move-result v0

    invoke-static {v0}, LX/EWc;->b(I)LX/EWb;

    move-result-object v0

    .line 2130221
    iget-object v1, v0, LX/EWb;->a:LX/EWf;

    move-object v1, v1

    .line 2130222
    invoke-virtual {p0, v1}, LX/EWX;->a(LX/EWf;)V

    .line 2130223
    invoke-virtual {v0}, LX/EWb;->a()LX/EWc;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2130224
    :catch_0
    move-exception v0

    .line 2130225
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a ByteString threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final jZ_()[B
    .locals 3

    .prologue
    .line 2130226
    :try_start_0
    invoke-virtual {p0}, LX/EWX;->b()I

    move-result v0

    new-array v0, v0, [B

    .line 2130227
    invoke-static {v0}, LX/EWf;->a([B)LX/EWf;

    move-result-object v1

    .line 2130228
    invoke-virtual {p0, v1}, LX/EWX;->a(LX/EWf;)V

    .line 2130229
    invoke-virtual {v1}, LX/EWf;->h()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2130230
    return-object v0

    .line 2130231
    :catch_0
    move-exception v0

    .line 2130232
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
