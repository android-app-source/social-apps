.class public final enum LX/Cus;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cus;

.field public static final enum CONSUMING_PAUSE:LX/Cus;

.field public static final enum IDLE:LX/Cus;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1947194
    new-instance v0, LX/Cus;

    const-string v1, "CONSUMING_PAUSE"

    invoke-direct {v0, v1, v2}, LX/Cus;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cus;->CONSUMING_PAUSE:LX/Cus;

    .line 1947195
    new-instance v0, LX/Cus;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, LX/Cus;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cus;->IDLE:LX/Cus;

    .line 1947196
    const/4 v0, 0x2

    new-array v0, v0, [LX/Cus;

    sget-object v1, LX/Cus;->CONSUMING_PAUSE:LX/Cus;

    aput-object v1, v0, v2

    sget-object v1, LX/Cus;->IDLE:LX/Cus;

    aput-object v1, v0, v3

    sput-object v0, LX/Cus;->$VALUES:[LX/Cus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1947198
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cus;
    .locals 1

    .prologue
    .line 1947199
    const-class v0, LX/Cus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cus;

    return-object v0
.end method

.method public static values()[LX/Cus;
    .locals 1

    .prologue
    .line 1947197
    sget-object v0, LX/Cus;->$VALUES:[LX/Cus;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cus;

    return-object v0
.end method
