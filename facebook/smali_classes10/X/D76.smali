.class public final LX/D76;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/D78;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/D78;)V
    .locals 1

    .prologue
    .line 1966770
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1966771
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D76;->a:Ljava/lang/ref/WeakReference;

    .line 1966772
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    .line 1966749
    iget-object v0, p0, LX/D76;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D78;

    .line 1966750
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 1966751
    :goto_0
    return-void

    .line 1966752
    :pswitch_0
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 1966753
    iget-object v2, v0, LX/2oy;->j:LX/2pb;

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->h()I

    move-result v2

    .line 1966754
    :goto_1
    iget v4, v0, LX/D78;->f:I

    sub-int/2addr v4, v2

    int-to-long v4, v4

    .line 1966755
    iget-object v6, v0, LX/D78;->d:Landroid/widget/ProgressBar;

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v7, v0, LX/D78;->f:I

    invoke-static {v2, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v6, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1966756
    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v2, v4

    .line 1966757
    const/16 v4, 0xa

    if-ge v2, v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1966758
    :goto_2
    iget-object v4, v0, LX/D78;->b:LX/3H0;

    sget-object v5, LX/3H0;->VOD:LX/3H0;

    if-ne v4, v5, :cond_4

    invoke-virtual {v0}, LX/D78;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080de1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1966759
    :goto_3
    new-array v5, v8, [Ljava/lang/Object;

    aput-object v2, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1966760
    iget-object v3, v0, LX/D78;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1966761
    iget-object v3, v0, LX/D78;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1966762
    :cond_0
    iget-object v2, v0, LX/2oy;->j:LX/2pb;

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/2oy;->j:LX/2pb;

    .line 1966763
    iget-object v3, v2, LX/2pb;->y:LX/2qV;

    move-object v2, v3

    .line 1966764
    invoke-virtual {v2}, LX/2qV;->isPlayingState()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1966765
    iget-object v2, v0, LX/D78;->a:LX/D76;

    const-wide/16 v4, 0x2a

    invoke-virtual {v2, v8, v4, v5}, LX/D76;->sendEmptyMessageDelayed(IJ)Z

    .line 1966766
    :cond_1
    goto :goto_0

    :cond_2
    move v2, v3

    .line 1966767
    goto :goto_1

    .line 1966768
    :cond_3
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 1966769
    :cond_4
    invoke-virtual {v0}, LX/D78;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080de2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
