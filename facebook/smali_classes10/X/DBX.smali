.class public LX/DBX;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/Bni;


# instance fields
.field public A:Landroid/content/Context;

.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/7vW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/7vZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Blh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DB4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/38v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/DBA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/DB9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/DBI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/DBE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/DBH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;

.field public p:Lcom/facebook/resources/ui/FbTextView;

.field public q:Lcom/facebook/resources/ui/FbTextView;

.field public r:Lcom/facebook/resources/ui/FbTextView;

.field public s:Lcom/facebook/resources/ui/FbTextView;

.field public t:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public u:Landroid/widget/ImageView;

.field public v:I

.field private w:LX/DBW;

.field public x:Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;

.field public y:Lcom/facebook/events/model/Event;

.field public z:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1973003
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1973004
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, LX/DBX;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1973005
    const v0, 0x7f030512

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1973006
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/DBX;->setOrientation(I)V

    .line 1973007
    const v0, 0x7f020668

    invoke-virtual {p0, v0}, LX/DBX;->setBackgroundResource(I)V

    .line 1973008
    const v0, 0x7f0d0e67

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/DBX;->t:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1973009
    const v0, 0x7f0d0e68

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;

    iput-object v0, p0, LX/DBX;->o:Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;

    .line 1973010
    const v0, 0x7f0d0e69

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DBX;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 1973011
    const v0, 0x7f0d0e6a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DBX;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 1973012
    const v0, 0x7f0d0e6b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DBX;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 1973013
    const v0, 0x7f0d0e6c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/DBX;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 1973014
    const v0, 0x7f0d0e6d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/DBX;->u:Landroid/widget/ImageView;

    .line 1973015
    iget-object v0, p0, LX/DBX;->t:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1973016
    iget p1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->w:I

    move v0, p1

    .line 1973017
    iput v0, p0, LX/DBX;->v:I

    .line 1973018
    invoke-virtual {p0}, LX/DBX;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/DBX;->A:Landroid/content/Context;

    .line 1973019
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, LX/DBX;->setBackgroundResource(I)V

    .line 1973020
    return-void
.end method

.method private static a(LX/DBX;LX/BnW;)V
    .locals 2

    .prologue
    .line 1972999
    iget-object v0, p0, LX/DBX;->u:Landroid/widget/ImageView;

    iget-object v1, p1, LX/BnW;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1973000
    iget-object v0, p0, LX/DBX;->u:Landroid/widget/ImageView;

    iget-object v1, p1, LX/BnW;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1973001
    iget-object v0, p0, LX/DBX;->u:Landroid/widget/ImageView;

    iget-object v1, p1, LX/BnW;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1973002
    return-void
.end method

.method private static a(LX/DBX;Landroid/content/res/Resources;LX/6RZ;LX/7vW;LX/7vZ;LX/Blh;LX/DB4;LX/38v;LX/1Ck;LX/0wM;LX/DBA;LX/DB9;LX/DBI;LX/DBE;LX/DBH;)V
    .locals 0

    .prologue
    .line 1972998
    iput-object p1, p0, LX/DBX;->a:Landroid/content/res/Resources;

    iput-object p2, p0, LX/DBX;->b:LX/6RZ;

    iput-object p3, p0, LX/DBX;->c:LX/7vW;

    iput-object p4, p0, LX/DBX;->d:LX/7vZ;

    iput-object p5, p0, LX/DBX;->e:LX/Blh;

    iput-object p6, p0, LX/DBX;->f:LX/DB4;

    iput-object p7, p0, LX/DBX;->g:LX/38v;

    iput-object p8, p0, LX/DBX;->h:LX/1Ck;

    iput-object p9, p0, LX/DBX;->i:LX/0wM;

    iput-object p10, p0, LX/DBX;->j:LX/DBA;

    iput-object p11, p0, LX/DBX;->k:LX/DB9;

    iput-object p12, p0, LX/DBX;->l:LX/DBI;

    iput-object p13, p0, LX/DBX;->m:LX/DBE;

    iput-object p14, p0, LX/DBX;->n:LX/DBH;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, LX/DBX;

    invoke-static {v14}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {v14}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v2

    check-cast v2, LX/6RZ;

    invoke-static {v14}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object v3

    check-cast v3, LX/7vW;

    invoke-static {v14}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v4

    check-cast v4, LX/7vZ;

    invoke-static {v14}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v5

    check-cast v5, LX/Blh;

    invoke-static {v14}, LX/DB4;->b(LX/0QB;)LX/DB4;

    move-result-object v6

    check-cast v6, LX/DB4;

    const-class v7, LX/38v;

    invoke-interface {v14, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/38v;

    invoke-static {v14}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {v14}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v9

    check-cast v9, LX/0wM;

    invoke-static {v14}, LX/DBA;->b(LX/0QB;)LX/DBA;

    move-result-object v10

    check-cast v10, LX/DBA;

    invoke-static {v14}, LX/DB9;->b(LX/0QB;)LX/DB9;

    move-result-object v11

    check-cast v11, LX/DB9;

    invoke-static {v14}, LX/DBI;->b(LX/0QB;)LX/DBI;

    move-result-object v12

    check-cast v12, LX/DBI;

    invoke-static {v14}, LX/DBE;->b(LX/0QB;)LX/DBE;

    move-result-object v13

    check-cast v13, LX/DBE;

    invoke-static {v14}, LX/DBH;->b(LX/0QB;)LX/DBH;

    move-result-object v14

    check-cast v14, LX/DBH;

    invoke-static/range {v0 .. v14}, LX/DBX;->a(LX/DBX;Landroid/content/res/Resources;LX/6RZ;LX/7vW;LX/7vZ;LX/Blh;LX/DB4;LX/38v;LX/1Ck;LX/0wM;LX/DBA;LX/DB9;LX/DBI;LX/DBE;LX/DBH;)V

    return-void
.end method

.method public static a$redex0(LX/DBX;Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 1972994
    iget-object v0, p0, LX/DBX;->w:LX/DBW;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972995
    iget-object p0, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, p0

    .line 1972996
    invoke-interface {v0, v1, p1}, LX/DBW;->a(Ljava/lang/String;Lcom/facebook/events/model/Event;)V

    .line 1972997
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1972983
    if-eqz p1, :cond_0

    .line 1972984
    iget-object v0, p0, LX/DBX;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1972985
    :goto_0
    return-void

    .line 1972986
    :cond_0
    iget-object v0, p0, LX/DBX;->f:LX/DB4;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, LX/DB4;->a(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v0

    .line 1972987
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1972988
    iget-object v1, p0, LX/DBX;->s:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1972989
    iget-object v1, p0, LX/DBX;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1972990
    iget-object v1, p0, LX/DBX;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/DBX;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972991
    iget-boolean p0, v0, Lcom/facebook/events/model/Event;->z:Z

    move v0, p0

    .line 1972992
    if-eqz v0, :cond_1

    const v0, 0x7f0a00d2

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0a010e

    goto :goto_1

    .line 1972993
    :cond_2
    iget-object v0, p0, LX/DBX;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1973021
    iget-object v0, p0, LX/DBX;->p:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1973022
    iget-object p0, v1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v1, p0

    .line 1973023
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1973024
    return-void
.end method

.method private d(Z)V
    .locals 4

    .prologue
    .line 1972944
    if-eqz p1, :cond_0

    .line 1972945
    iget-object v0, p0, LX/DBX;->u:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1972946
    :goto_0
    return-void

    .line 1972947
    :cond_0
    iget-object v0, p0, LX/DBX;->u:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1972948
    iget-object v0, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1972949
    iget-object v0, p0, LX/DBX;->j:LX/DBA;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2}, LX/DBA;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 1972950
    iget-object v0, p0, LX/DBX;->k:LX/DB9;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2}, LX/DB9;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 1972951
    iget-object v0, p0, LX/DBX;->l:LX/DBI;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1972952
    iput-object v1, v0, LX/DBI;->a:Lcom/facebook/events/model/Event;

    .line 1972953
    iput-object v2, v0, LX/DBI;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1972954
    iget-object v0, p0, LX/DBX;->m:LX/DBE;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2}, LX/DBE;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 1972955
    iget-object v0, p0, LX/DBX;->n:LX/DBH;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2}, LX/DBH;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 1972956
    new-instance v0, LX/BnW;

    .line 1972957
    const v1, 0x7f020723

    .line 1972958
    iget-object v2, p0, LX/DBX;->i:LX/0wM;

    const v3, -0x808081

    invoke-virtual {v2, v1, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object v1, v2

    .line 1972959
    move-object v1, v1

    .line 1972960
    invoke-virtual {p0}, LX/DBX;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0821fa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1972961
    new-instance v3, LX/DBQ;

    invoke-direct {v3, p0}, LX/DBQ;-><init>(LX/DBX;)V

    invoke-direct {v0, v1, v2, v3}, LX/BnW;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1972962
    invoke-static {p0, v0}, LX/DBX;->a(LX/DBX;LX/BnW;)V

    goto :goto_0

    .line 1972963
    :cond_1
    iget-object v0, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    invoke-static {v0}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1972964
    iget-object v0, p0, LX/DBX;->g:LX/38v;

    invoke-virtual {v0, p0}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v0

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972965
    iget-object v2, v1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v1, v2

    .line 1972966
    iget-object v2, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    iget-object v3, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972967
    iget-object p1, v3, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v3, p1

    .line 1972968
    invoke-virtual {v0, v1, v2, v3}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v0

    .line 1972969
    invoke-static {p0, v0}, LX/DBX;->a(LX/DBX;LX/BnW;)V

    goto/16 :goto_0

    .line 1972970
    :cond_2
    const/4 v3, -0x1

    .line 1972971
    new-instance v0, LX/0wM;

    iget-object v1, p0, LX/DBX;->a:Landroid/content/res/Resources;

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 1972972
    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    .line 1972973
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v2, v1, :cond_3

    .line 1972974
    const v1, 0x7f020858

    invoke-virtual {v0, v1, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1972975
    iget-object v0, p0, LX/DBX;->u:Landroid/widget/ImageView;

    const v3, 0x7f020665

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1972976
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1972977
    :goto_1
    iget-object v3, p0, LX/DBX;->u:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1972978
    iget-object v1, p0, LX/DBX;->u:Landroid/widget/ImageView;

    new-instance v3, LX/DBN;

    invoke-direct {v3, p0, v2, v0}, LX/DBN;-><init>(LX/DBX;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1972979
    goto/16 :goto_0

    .line 1972980
    :cond_3
    const v1, 0x7f020856

    invoke-virtual {v0, v1, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1972981
    iget-object v0, p0, LX/DBX;->u:Landroid/widget/ImageView;

    const v3, 0x7f020666

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1972982
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_1
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1972929
    iget-object v0, p0, LX/DBX;->r:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1972930
    iget-object v0, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972931
    iget-object v1, v0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v0, v1

    .line 1972932
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1972933
    iget-object v0, p0, LX/DBX;->r:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972934
    iget-object p0, v1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v1, p0

    .line 1972935
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1972936
    :goto_0
    return-void

    .line 1972937
    :cond_0
    iget-object v0, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972938
    iget-object v1, v0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v0, v1

    .line 1972939
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1972940
    iget-object v0, p0, LX/DBX;->r:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972941
    iget-object p0, v1, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v1, p0

    .line 1972942
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1972943
    :cond_1
    iget-object v0, p0, LX/DBX;->r:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;ZLX/DBW;Z)V
    .locals 1

    .prologue
    .line 1972857
    iput-object p1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972858
    iput-object p2, p0, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1972859
    invoke-virtual {p0, p0}, LX/DBX;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1972860
    const/4 p1, 0x0

    .line 1972861
    iget-object v0, p0, LX/DBX;->t:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1972862
    iget p2, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->w:I

    move v0, p2

    .line 1972863
    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 1972864
    :goto_0
    if-eq v0, p3, :cond_0

    .line 1972865
    iget-object p2, p0, LX/DBX;->t:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz p3, :cond_3

    iget v0, p0, LX/DBX;->v:I

    :goto_1
    invoke-virtual {p2, p1, v0, p1, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    .line 1972866
    :cond_0
    iput-object p4, p0, LX/DBX;->w:LX/DBW;

    .line 1972867
    iget-object v0, p0, LX/DBX;->o:Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;

    iget-object p1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    iget-object p2, p0, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object p2, p2, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;)V

    .line 1972868
    invoke-direct {p0}, LX/DBX;->d()V

    .line 1972869
    iget-object v0, p0, LX/DBX;->q:Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, p0, LX/DBX;->b:LX/6RZ;

    iget-object p2, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972870
    iget-boolean p3, p2, Lcom/facebook/events/model/Event;->N:Z

    move p2, p3

    .line 1972871
    iget-object p3, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    invoke-virtual {p3}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object p3

    iget-object p4, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    invoke-virtual {p4}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object p4

    invoke-virtual {p1, p2, p3, p4}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1972872
    invoke-direct {p0}, LX/DBX;->f()V

    .line 1972873
    invoke-direct {p0, p5}, LX/DBX;->c(Z)V

    .line 1972874
    invoke-direct {p0, p5}, LX/DBX;->d(Z)V

    .line 1972875
    iget-object v0, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972876
    if-nez p5, :cond_6

    sget-object p1, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, p1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 1972877
    iget-boolean p1, v0, Lcom/facebook/events/model/Event;->H:Z

    move p1, p1

    .line 1972878
    if-eqz p1, :cond_6

    const/4 p1, 0x1

    :goto_2
    move v0, p1

    .line 1972879
    if-nez v0, :cond_4

    .line 1972880
    iget-object v0, p0, LX/DBX;->x:Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;

    if-eqz v0, :cond_1

    .line 1972881
    iget-object v0, p0, LX/DBX;->x:Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;

    const/16 p1, 0x8

    invoke-virtual {v0, p1}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->setVisibility(I)V

    .line 1972882
    :cond_1
    :goto_3
    return-void

    :cond_2
    move v0, p1

    .line 1972883
    goto :goto_0

    :cond_3
    move v0, p1

    .line 1972884
    goto :goto_1

    .line 1972885
    :cond_4
    iget-object v0, p0, LX/DBX;->x:Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;

    if-nez v0, :cond_5

    .line 1972886
    const v0, 0x7f0d0e6e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1972887
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;

    iput-object v0, p0, LX/DBX;->x:Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;

    .line 1972888
    :cond_5
    iget-object v0, p0, LX/DBX;->x:Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->setVisibility(I)V

    .line 1972889
    iget-object v0, p0, LX/DBX;->x:Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;

    iget-object p1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, p1}, Lcom/facebook/events/widget/eventrow/EventRowInlineRsvpView;->a(Lcom/facebook/events/model/Event;)V

    goto :goto_3

    :cond_6
    const/4 p1, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 8

    .prologue
    .line 1972909
    iget-object v0, p0, LX/DBX;->c:LX/7vW;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972910
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1972911
    iget-object v3, p0, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->EVENTS_LIST:Lcom/facebook/events/common/ActionMechanism;

    const/4 v5, 0x0

    move-object v2, p2

    .line 1972912
    new-instance v6, LX/4ES;

    invoke-direct {v6}, LX/4ES;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v3, v5}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)LX/4EL;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4ES;->a(LX/4EL;)LX/4ES;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/4ES;->b(Ljava/lang/String;)LX/4ES;

    move-result-object v6

    invoke-static {v2}, LX/7vW;->b(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4ES;->c(Ljava/lang/String;)LX/4ES;

    move-result-object v6

    .line 1972913
    iget-object v7, v3, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    if-eqz v7, :cond_0

    .line 1972914
    iget-object v7, v3, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4ES;->a(Ljava/util/List;)LX/4ES;

    .line 1972915
    :cond_0
    invoke-static {}, LX/7uR;->e()LX/7uI;

    move-result-object v7

    .line 1972916
    const-string p1, "input"

    invoke-virtual {v7, p1, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1972917
    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    .line 1972918
    iget-object v7, v0, LX/7vW;->b:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 1972919
    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972920
    new-instance v2, LX/7vC;

    iget-object v3, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    invoke-direct {v2, v3}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    .line 1972921
    iput-object p2, v2, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1972922
    move-object v2, v2

    .line 1972923
    invoke-virtual {v2}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v2

    .line 1972924
    iget-object v3, p0, LX/DBX;->w:LX/DBW;

    iget-object v4, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972925
    iget-object v5, v4, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1972926
    invoke-interface {v3, v4, v2}, LX/DBW;->a(Ljava/lang/String;Lcom/facebook/events/model/Event;)V

    .line 1972927
    iget-object v2, p0, LX/DBX;->h:LX/1Ck;

    new-instance v3, LX/DBO;

    invoke-direct {v3, p0, v1}, LX/DBO;-><init>(LX/DBX;Lcom/facebook/events/model/Event;)V

    invoke-virtual {v2, p0, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1972928
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 5

    .prologue
    .line 1972894
    iget-object v0, p0, LX/DBX;->d:LX/7vZ;

    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972895
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1972896
    iget-object v2, p0, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->EVENTS_LIST:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, p2, v2, v3}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1972897
    iget-object v1, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972898
    new-instance v2, LX/7vC;

    iget-object v3, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    invoke-direct {v2, v3}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    const/4 v3, 0x0

    .line 1972899
    iput-boolean v3, v2, LX/7vC;->H:Z

    .line 1972900
    move-object v2, v2

    .line 1972901
    iput-object p2, v2, LX/7vC;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1972902
    move-object v2, v2

    .line 1972903
    invoke-virtual {v2}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v2

    .line 1972904
    iget-object v3, p0, LX/DBX;->w:LX/DBW;

    iget-object v4, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972905
    iget-object p1, v4, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v4, p1

    .line 1972906
    invoke-interface {v3, v4, v2}, LX/DBW;->a(Ljava/lang/String;Lcom/facebook/events/model/Event;)V

    .line 1972907
    iget-object v2, p0, LX/DBX;->h:LX/1Ck;

    new-instance v3, LX/DBP;

    invoke-direct {v3, p0, v1}, LX/DBP;-><init>(LX/DBX;Lcom/facebook/events/model/Event;)V

    invoke-virtual {v2, p0, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1972908
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x6beab1fc

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1972890
    iget-object v1, p0, LX/DBX;->e:LX/Blh;

    iget-object v2, p0, LX/DBX;->A:Landroid/content/Context;

    iget-object v3, p0, LX/DBX;->y:Lcom/facebook/events/model/Event;

    .line 1972891
    iget-object v4, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1972892
    iget-object v4, p0, LX/DBX;->z:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {v1, v2, v3, v4}, LX/Blh;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;)V

    .line 1972893
    const v1, -0x633e1055

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
