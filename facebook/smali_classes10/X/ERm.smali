.class public final enum LX/ERm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ERm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ERm;

.field public static final enum MAX:LX/ERm;

.field public static final enum MIN:LX/ERm;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2121396
    new-instance v0, LX/ERm;

    const-string v1, "MAX"

    invoke-direct {v0, v1, v2}, LX/ERm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ERm;->MAX:LX/ERm;

    new-instance v0, LX/ERm;

    const-string v1, "MIN"

    invoke-direct {v0, v1, v3}, LX/ERm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ERm;->MIN:LX/ERm;

    .line 2121397
    const/4 v0, 0x2

    new-array v0, v0, [LX/ERm;

    sget-object v1, LX/ERm;->MAX:LX/ERm;

    aput-object v1, v0, v2

    sget-object v1, LX/ERm;->MIN:LX/ERm;

    aput-object v1, v0, v3

    sput-object v0, LX/ERm;->$VALUES:[LX/ERm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2121398
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ERm;
    .locals 1

    .prologue
    .line 2121399
    const-class v0, LX/ERm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ERm;

    return-object v0
.end method

.method public static values()[LX/ERm;
    .locals 1

    .prologue
    .line 2121400
    sget-object v0, LX/ERm;->$VALUES:[LX/ERm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ERm;

    return-object v0
.end method
