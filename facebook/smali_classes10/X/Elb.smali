.class public LX/Elb;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164515
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2164516
    return-void
.end method

.method public static a(Ljava/util/concurrent/ExecutorService;LX/El3;)LX/Ekr;
    .locals 2
    .param p0    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 2164517
    new-instance v0, LX/Ekq;

    invoke-direct {v0}, LX/Ekq;-><init>()V

    .line 2164518
    iput-object p0, v0, LX/Ekq;->b:Ljava/util/concurrent/ExecutorService;

    .line 2164519
    move-object v0, v0

    .line 2164520
    iput-object p1, v0, LX/Ekq;->a:LX/El3;

    .line 2164521
    move-object v0, v0

    .line 2164522
    const/4 v1, 0x0

    .line 2164523
    iput-boolean v1, v0, LX/Ekq;->c:Z

    .line 2164524
    move-object v0, v0

    .line 2164525
    new-instance v1, LX/Ekr;

    invoke-direct {v1, v0}, LX/Ekr;-><init>(LX/Ekq;)V

    move-object v0, v1

    .line 2164526
    return-object v0
.end method

.method public static a(LX/Ekr;LX/0Or;)LX/Ekx;
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/UserAgentString;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ekr;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/Ekx;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 2164527
    new-instance v0, LX/Eku;

    invoke-direct {v0, p0}, LX/Eku;-><init>(LX/Ekr;)V

    new-instance v1, LX/ElY;

    invoke-direct {v1, p1}, LX/ElY;-><init>(LX/0Or;)V

    .line 2164528
    iput-object v1, v0, LX/Eku;->c:LX/ElY;

    .line 2164529
    move-object v0, v0

    .line 2164530
    invoke-virtual {v0}, LX/Eku;->b()LX/Ekx;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/ElF;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 2164531
    new-instance v0, LX/Ela;

    invoke-direct {v0, p0}, LX/Ela;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-object v0
.end method

.method public static a(LX/Ekx;LX/0Or;LX/ElF;)LX/ElQ;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ekx;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/ElF;",
            ")",
            "LX/ElQ;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 2164532
    new-instance v0, LX/ElP;

    invoke-direct {v0, p0}, LX/ElP;-><init>(LX/Ekx;)V

    new-instance v1, LX/Elf;

    invoke-direct {v1}, LX/Elf;-><init>()V

    .line 2164533
    iput-object v1, v0, LX/ElP;->d:LX/Elf;

    .line 2164534
    move-object v0, v0

    .line 2164535
    iput-object p2, v0, LX/ElP;->c:LX/ElF;

    .line 2164536
    move-object v0, v0

    .line 2164537
    new-instance v1, LX/ElZ;

    invoke-direct {v1, p1}, LX/ElZ;-><init>(LX/0Or;)V

    .line 2164538
    iput-object v1, v0, LX/ElP;->b:LX/ElZ;

    .line 2164539
    move-object v0, v0

    .line 2164540
    new-instance v1, LX/ElQ;

    invoke-direct {v1, v0}, LX/ElQ;-><init>(LX/ElP;)V

    move-object v0, v1

    .line 2164541
    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2164542
    return-void
.end method
