.class public final LX/DDV;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DDW;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/DDZ;

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/DDW;


# direct methods
.method public constructor <init>(LX/DDW;)V
    .locals 1

    .prologue
    .line 1975668
    iput-object p1, p0, LX/DDV;->c:LX/DDW;

    .line 1975669
    move-object v0, p1

    .line 1975670
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1975671
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1975686
    const-string v0, "GroupsPeopleYouMayInvitePageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1975672
    if-ne p0, p1, :cond_1

    .line 1975673
    :cond_0
    :goto_0
    return v0

    .line 1975674
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1975675
    goto :goto_0

    .line 1975676
    :cond_3
    check-cast p1, LX/DDV;

    .line 1975677
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1975678
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1975679
    if-eq v2, v3, :cond_0

    .line 1975680
    iget-object v2, p0, LX/DDV;->a:LX/DDZ;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DDV;->a:LX/DDZ;

    iget-object v3, p1, LX/DDV;->a:LX/DDZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1975681
    goto :goto_0

    .line 1975682
    :cond_5
    iget-object v2, p1, LX/DDV;->a:LX/DDZ;

    if-nez v2, :cond_4

    .line 1975683
    :cond_6
    iget-object v2, p0, LX/DDV;->b:LX/1Po;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/DDV;->b:LX/1Po;

    iget-object v3, p1, LX/DDV;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1975684
    goto :goto_0

    .line 1975685
    :cond_7
    iget-object v2, p1, LX/DDV;->b:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
