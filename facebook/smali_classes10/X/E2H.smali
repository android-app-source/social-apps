.class public LX/E2H;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/E2H;


# instance fields
.field public final a:LX/E1s;


# direct methods
.method public constructor <init>(LX/E1s;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2072561
    iput-object p1, p0, LX/E2H;->a:LX/E1s;

    .line 2072562
    return-void
.end method

.method public static a(LX/0QB;)LX/E2H;
    .locals 4

    .prologue
    .line 2072563
    sget-object v0, LX/E2H;->b:LX/E2H;

    if-nez v0, :cond_1

    .line 2072564
    const-class v1, LX/E2H;

    monitor-enter v1

    .line 2072565
    :try_start_0
    sget-object v0, LX/E2H;->b:LX/E2H;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072566
    if-eqz v2, :cond_0

    .line 2072567
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072568
    new-instance p0, LX/E2H;

    invoke-static {v0}, LX/E1s;->a(LX/0QB;)LX/E1s;

    move-result-object v3

    check-cast v3, LX/E1s;

    invoke-direct {p0, v3}, LX/E2H;-><init>(LX/E1s;)V

    .line 2072569
    move-object v0, p0

    .line 2072570
    sput-object v0, LX/E2H;->b:LX/E2H;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072571
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072572
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072573
    :cond_1
    sget-object v0, LX/E2H;->b:LX/E2H;

    return-object v0

    .line 2072574
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072575
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
