.class public LX/E5i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/E5i;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078802
    return-void
.end method

.method public static a(LX/0QB;)LX/E5i;
    .locals 3

    .prologue
    .line 2078803
    sget-object v0, LX/E5i;->a:LX/E5i;

    if-nez v0, :cond_1

    .line 2078804
    const-class v1, LX/E5i;

    monitor-enter v1

    .line 2078805
    :try_start_0
    sget-object v0, LX/E5i;->a:LX/E5i;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078806
    if-eqz v2, :cond_0

    .line 2078807
    :try_start_1
    new-instance v0, LX/E5i;

    invoke-direct {v0}, LX/E5i;-><init>()V

    .line 2078808
    move-object v0, v0

    .line 2078809
    sput-object v0, LX/E5i;->a:LX/E5i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078810
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078811
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078812
    :cond_1
    sget-object v0, LX/E5i;->a:LX/E5i;

    return-object v0

    .line 2078813
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078814
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;)Landroid/graphics/Typeface;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2078815
    sget-object v0, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v1, LX/0xr;->MEDIUM:LX/0xr;

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method
