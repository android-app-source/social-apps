.class public abstract LX/CjH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "LX/CnG;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/richdocument/genesis/BlockCreator",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:I

.field public final b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 1929276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929277
    iput p1, p0, LX/CjH;->a:I

    .line 1929278
    iput p2, p0, LX/CjH;->b:I

    .line 1929279
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/view/View;)LX/CnG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")TV;"
        }
    .end annotation
.end method

.method public abstract a(LX/CnG;)LX/CnT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Lcom/facebook/richdocument/presenter/BlockPresenter;"
        }
    .end annotation
.end method

.method public a(Landroid/view/ViewGroup;)LX/Cs4;
    .locals 3

    .prologue
    .line 1929280
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1929281
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1929282
    iget v1, p0, LX/CjH;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1929283
    invoke-virtual {p0, v0}, LX/CjH;->a(Landroid/view/View;)LX/CnG;

    move-result-object v0

    .line 1929284
    invoke-virtual {p0, v0}, LX/CjH;->a(LX/CnG;)LX/CnT;

    .line 1929285
    new-instance v1, LX/Cs4;

    invoke-direct {v1, v0}, LX/Cs4;-><init>(LX/CnG;)V

    return-object v1
.end method
