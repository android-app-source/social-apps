.class public final enum LX/DiF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DiF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DiF;

.field public static final enum REQUEST:LX/DiF;

.field public static final enum TRANSFER:LX/DiF;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2031870
    new-instance v0, LX/DiF;

    const-string v1, "REQUEST"

    invoke-direct {v0, v1, v2}, LX/DiF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DiF;->REQUEST:LX/DiF;

    .line 2031871
    new-instance v0, LX/DiF;

    const-string v1, "TRANSFER"

    invoke-direct {v0, v1, v3}, LX/DiF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DiF;->TRANSFER:LX/DiF;

    .line 2031872
    const/4 v0, 0x2

    new-array v0, v0, [LX/DiF;

    sget-object v1, LX/DiF;->REQUEST:LX/DiF;

    aput-object v1, v0, v2

    sget-object v1, LX/DiF;->TRANSFER:LX/DiF;

    aput-object v1, v0, v3

    sput-object v0, LX/DiF;->$VALUES:[LX/DiF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2031873
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DiF;
    .locals 1

    .prologue
    .line 2031874
    const-class v0, LX/DiF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DiF;

    return-object v0
.end method

.method public static values()[LX/DiF;
    .locals 1

    .prologue
    .line 2031875
    sget-object v0, LX/DiF;->$VALUES:[LX/DiF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DiF;

    return-object v0
.end method
