.class public LX/ERT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/Boolean;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2120932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120933
    invoke-static {}, LX/ERT;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/ERT;->b:Ljava/lang/String;

    .line 2120934
    iput-object p1, p0, LX/ERT;->a:Ljava/lang/String;

    .line 2120935
    iput-object v1, p0, LX/ERT;->c:Ljava/lang/Boolean;

    .line 2120936
    iput-object v1, p0, LX/ERT;->d:Ljava/lang/String;

    .line 2120937
    iput-object v1, p0, LX/ERT;->e:Ljava/lang/Boolean;

    .line 2120938
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2120939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120940
    invoke-static {}, LX/ERT;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/ERT;->b:Ljava/lang/String;

    .line 2120941
    iput-object p1, p0, LX/ERT;->a:Ljava/lang/String;

    .line 2120942
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/ERT;->c:Ljava/lang/Boolean;

    .line 2120943
    iput-object p3, p0, LX/ERT;->d:Ljava/lang/String;

    .line 2120944
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/ERT;->e:Ljava/lang/Boolean;

    .line 2120945
    return-void
.end method

.method private static f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2120946
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 2120947
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 2120948
    :cond_0
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 2120949
    :goto_0
    return-object v0

    .line 2120950
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2120951
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
