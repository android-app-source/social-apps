.class public final LX/Eyn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFriendsConnectionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/FriendRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 0

    .prologue
    .line 2186550
    iput-object p1, p0, LX/Eyn;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2186574
    iget-object v0, p0, LX/Eyn;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    .line 2186575
    invoke-static {v0, p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Ljava/lang/Throwable;)V

    .line 2186576
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186551
    check-cast p1, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFriendsConnectionModel;

    const/4 v2, 0x0

    .line 2186552
    iget-object v0, p0, LX/Eyn;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->aj:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2186553
    if-nez p1, :cond_0

    .line 2186554
    :goto_0
    return-void

    .line 2186555
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2186556
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2186557
    invoke-virtual {p1}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFriendsConnectionModel;->j()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_2

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFieldsModel;

    .line 2186558
    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFieldsModel;->b()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    sget-object v9, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v9

    :try_start_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2186559
    if-eqz v7, :cond_1

    invoke-virtual {v8, v7, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFieldsModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 2186560
    invoke-virtual {v8, v7, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2186561
    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2186562
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2186563
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2186564
    :cond_2
    iget-object v0, p0, LX/Eyn;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFriendsConnectionModel;->a()I

    move-result v3

    .line 2186565
    iget-object v4, v0, LX/2iK;->p:LX/2ib;

    .line 2186566
    iget-object v5, v4, LX/2ib;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 2186567
    iget-object v5, v4, LX/2ib;->a:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2186568
    iget-object v5, v4, LX/2ib;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 2186569
    iget-object v5, v4, LX/2ib;->b:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2186570
    iput v3, v4, LX/2ib;->c:I

    .line 2186571
    invoke-virtual {v0}, LX/2iK;->m()V

    .line 2186572
    const v4, -0x7458aab7

    invoke-static {v0, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2186573
    goto/16 :goto_0
.end method
