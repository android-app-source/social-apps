.class public final LX/ECu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ECw;


# direct methods
.method public constructor <init>(LX/ECw;)V
    .locals 0

    .prologue
    .line 2090591
    iput-object p1, p0, LX/ECu;->a:LX/ECw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2090592
    iget-object v0, p0, LX/ECu;->a:LX/ECw;

    iget-object v0, v0, LX/ECw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    .line 2090593
    iget-object p1, v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/EDx;

    invoke-virtual {p1}, LX/EDx;->bh()V

    .line 2090594
    goto :goto_0

    .line 2090595
    :cond_0
    iget-object v0, p0, LX/ECu;->a:LX/ECw;

    const/4 v1, 0x0

    .line 2090596
    iput-object v1, v0, LX/ECw;->d:LX/1Zp;

    .line 2090597
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2090598
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2090599
    if-eqz p1, :cond_0

    .line 2090600
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2090601
    if-eqz v0, :cond_0

    .line 2090602
    iget-object v1, p0, LX/ECu;->a:LX/ECw;

    .line 2090603
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2090604
    check-cast v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;

    invoke-static {v1, v0}, LX/ECw;->a$redex0(LX/ECw;Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;)V

    .line 2090605
    iget-object v0, p0, LX/ECu;->a:LX/ECw;

    const/4 v1, 0x0

    .line 2090606
    iput-object v1, v0, LX/ECw;->d:LX/1Zp;

    .line 2090607
    :cond_0
    return-void
.end method
