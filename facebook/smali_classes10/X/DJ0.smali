.class public final LX/DJ0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/groupcommerce/composer/ComposerSellView;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/composer/ComposerSellView;)V
    .locals 0

    .prologue
    .line 1984637
    iput-object p1, p0, LX/DJ0;->a:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 6

    .prologue
    .line 1984638
    iget-object v0, p0, LX/DJ0;->a:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v0, v0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->n:LX/34u;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/34u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1984639
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1984640
    :cond_0
    :goto_0
    return-void

    .line 1984641
    :cond_1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1984642
    const-wide v4, 0x5af3107a3fffL

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1984643
    :goto_1
    const-string v1, "99999999999999"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    .line 1984644
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1984645
    iget-object v1, p0, LX/DJ0;->a:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1984646
    iget-object v1, p0, LX/DJ0;->a:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    .line 1984647
    invoke-static {v1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Lcom/facebook/resources/ui/FbEditText;)V

    .line 1984648
    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1984649
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1984650
    return-void
.end method
