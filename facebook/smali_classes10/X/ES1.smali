.class public final LX/ES1;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/vault/service/VaultSyncJobProcessor;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/service/VaultSyncJobProcessor;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 2121917
    iput-object p1, p0, LX/ES1;->a:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 2121918
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2121919
    iget-object v0, p0, LX/ES1;->a:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    iget-object v0, v0, Lcom/facebook/vault/service/VaultSyncJobProcessor;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/vault/service/VaultSyncJobProcessor;->d:Ljava/lang/String;

    const-string v2, "msg.what %d is not defined"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2121920
    :goto_0
    return-void

    .line 2121921
    :pswitch_0
    iget-object v0, p0, LX/ES1;->a:Lcom/facebook/vault/service/VaultSyncJobProcessor;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/facebook/vault/service/VaultSyncJobProcessor;->stopSelfResult(I)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
