.class public final LX/ELD;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/ELF;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/ELE;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2108247
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2108248
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "thumbnailUri"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/ELD;->b:[Ljava/lang/String;

    .line 2108249
    iput v3, p0, LX/ELD;->c:I

    .line 2108250
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/ELD;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/ELD;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/ELD;LX/1De;IILX/ELE;)V
    .locals 1

    .prologue
    .line 2108203
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2108204
    iput-object p4, p0, LX/ELD;->a:LX/ELE;

    .line 2108205
    iget-object v0, p0, LX/ELD;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2108206
    return-void
.end method


# virtual methods
.method public final a(LX/1n6;)LX/ELD;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n6",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/ELD;"
        }
    .end annotation

    .prologue
    .line 2108245
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    invoke-virtual {p1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    iput-object v1, v0, LX/ELE;->f:LX/1dc;

    .line 2108246
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/ELD;
    .locals 2

    .prologue
    .line 2108242
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-object p1, v0, LX/ELE;->a:Landroid/net/Uri;

    .line 2108243
    iget-object v0, p0, LX/ELD;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2108244
    return-object p0
.end method

.method public final a(Landroid/view/View$OnClickListener;)LX/ELD;
    .locals 1

    .prologue
    .line 2108240
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-object p1, v0, LX/ELE;->i:Landroid/view/View$OnClickListener;

    .line 2108241
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/ELD;
    .locals 2

    .prologue
    .line 2108237
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-object p1, v0, LX/ELE;->c:Ljava/lang/CharSequence;

    .line 2108238
    iget-object v0, p0, LX/ELD;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2108239
    return-object p0
.end method

.method public final a(Z)LX/ELD;
    .locals 1

    .prologue
    .line 2108235
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-boolean p1, v0, LX/ELE;->g:Z

    .line 2108236
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2108231
    invoke-super {p0}, LX/1X5;->a()V

    .line 2108232
    const/4 v0, 0x0

    iput-object v0, p0, LX/ELD;->a:LX/ELE;

    .line 2108233
    sget-object v0, LX/ELF;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2108234
    return-void
.end method

.method public final b(Landroid/view/View$OnClickListener;)LX/ELD;
    .locals 1

    .prologue
    .line 2108229
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-object p1, v0, LX/ELE;->k:Landroid/view/View$OnClickListener;

    .line 2108230
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)LX/ELD;
    .locals 1

    .prologue
    .line 2108227
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-object p1, v0, LX/ELE;->d:Ljava/lang/CharSequence;

    .line 2108228
    return-object p0
.end method

.method public final c(Landroid/view/View$OnClickListener;)LX/ELD;
    .locals 1

    .prologue
    .line 2108225
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-object p1, v0, LX/ELE;->l:Landroid/view/View$OnClickListener;

    .line 2108226
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)LX/ELD;
    .locals 1

    .prologue
    .line 2108223
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-object p1, v0, LX/ELE;->e:Ljava/lang/CharSequence;

    .line 2108224
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/ELF;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2108213
    iget-object v1, p0, LX/ELD;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/ELD;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/ELD;->c:I

    if-ge v1, v2, :cond_2

    .line 2108214
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2108215
    :goto_0
    iget v2, p0, LX/ELD;->c:I

    if-ge v0, v2, :cond_1

    .line 2108216
    iget-object v2, p0, LX/ELD;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2108217
    iget-object v2, p0, LX/ELD;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2108218
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2108219
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2108220
    :cond_2
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    .line 2108221
    invoke-virtual {p0}, LX/ELD;->a()V

    .line 2108222
    return-object v0
.end method

.method public final d(Landroid/view/View$OnClickListener;)LX/ELD;
    .locals 1

    .prologue
    .line 2108211
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-object p1, v0, LX/ELE;->m:Landroid/view/View$OnClickListener;

    .line 2108212
    return-object p0
.end method

.method public final d(Ljava/lang/CharSequence;)LX/ELD;
    .locals 1

    .prologue
    .line 2108209
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-object p1, v0, LX/ELE;->h:Ljava/lang/CharSequence;

    .line 2108210
    return-object p0
.end method

.method public final e(Ljava/lang/CharSequence;)LX/ELD;
    .locals 1

    .prologue
    .line 2108207
    iget-object v0, p0, LX/ELD;->a:LX/ELE;

    iput-object p1, v0, LX/ELE;->j:Ljava/lang/CharSequence;

    .line 2108208
    return-object p0
.end method
