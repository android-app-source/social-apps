.class public final LX/EIe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/EIg;


# direct methods
.method public constructor <init>(LX/EIg;)V
    .locals 0

    .prologue
    .line 2102202
    iput-object p1, p0, LX/EIe;->a:LX/EIg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x591cc392

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2102203
    iget-object v1, p0, LX/EIe;->a:LX/EIg;

    iget-object v1, v1, LX/EIg;->d:LX/EBq;

    .line 2102204
    iget-object v3, v1, LX/EBq;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    .line 2102205
    invoke-static {v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2102206
    iget-object v4, v3, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    .line 2102207
    const-string v8, "redial_button"

    iput-object v8, v4, LX/2S7;->v:Ljava/lang/String;

    .line 2102208
    iget-wide v8, v4, LX/2S7;->y:J

    iput-wide v8, v4, LX/2S7;->x:J

    .line 2102209
    iget-object v4, v3, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    invoke-virtual {v4}, LX/EDx;->aJ()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2102210
    invoke-static {v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->P(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2102211
    :cond_0
    iget-wide v4, v3, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Z:J

    const-string v6, "redial_button"

    iget-boolean v7, v3, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->S:Z

    invoke-static {v4, v5, v6, v7}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a(JLjava/lang/String;Z)Lcom/facebook/rtc/helpers/RtcCallStartParams;

    move-result-object v4

    .line 2102212
    iget-object v5, v3, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t:LX/3A0;

    invoke-virtual {v5, v4}, LX/3A0;->a(Lcom/facebook/rtc/helpers/RtcCallStartParams;)V

    .line 2102213
    invoke-static {v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2102214
    const v4, 0x7f08072b

    invoke-virtual {v3, v4}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2102215
    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->l(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2102216
    invoke-static {v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2102217
    iget-object v3, v1, LX/EBq;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static {v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ak(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2102218
    const v1, 0x6638e23c

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
