.class public LX/CuJ;
.super LX/Cts;
.source ""

# interfaces
.implements LX/Ctn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Float;",
        ">;",
        "LX/Ctn;"
    }
.end annotation


# instance fields
.field private final a:LX/CuX;

.field public final b:LX/Cud;

.field public final c:Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

.field private final d:Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

.field public final e:LX/CuK;

.field public final f:LX/CqV;

.field public g:Z

.field public h:LX/Cqt;

.field public i:Z

.field public j:I

.field private k:I

.field public l:Z


# direct methods
.method public constructor <init>(ILX/Ctg;LX/CuX;LX/Cud;Lcom/facebook/video/player/plugins/Video360HeadingPlugin;Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;LX/CuK;LX/CqV;)V
    .locals 1

    .prologue
    .line 1946469
    invoke-direct {p0, p2}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1946470
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CuJ;->g:Z

    .line 1946471
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CuJ;->i:Z

    .line 1946472
    iput p1, p0, LX/CuJ;->k:I

    .line 1946473
    iput-object p3, p0, LX/CuJ;->a:LX/CuX;

    .line 1946474
    iget-object v0, p0, LX/CuJ;->a:LX/CuX;

    .line 1946475
    iput-object p2, v0, LX/CuX;->f:LX/Ctg;

    .line 1946476
    iput-object p4, p0, LX/CuJ;->b:LX/Cud;

    .line 1946477
    iput-object p5, p0, LX/CuJ;->c:Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    .line 1946478
    iput-object p6, p0, LX/CuJ;->d:Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

    .line 1946479
    iput-object p7, p0, LX/CuJ;->e:LX/CuK;

    .line 1946480
    iput-object p8, p0, LX/CuJ;->f:LX/CqV;

    .line 1946481
    return-void
.end method

.method private d(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1946468
    iget-object v0, p0, LX/CuJ;->d:Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CuJ;->d:Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

    invoke-static {v0, p1}, LX/8ba;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1946456
    iget-boolean v0, p0, LX/CuJ;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/CuJ;->i:Z

    if-nez v0, :cond_1

    .line 1946457
    :cond_0
    :goto_0
    return-object p1

    .line 1946458
    :cond_1
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    .line 1946459
    iget-object v0, p0, LX/CuJ;->h:LX/Cqt;

    sget-object v1, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    .line 1946460
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 1946461
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 1946462
    if-eqz v0, :cond_3

    move v0, v1

    .line 1946463
    :goto_2
    invoke-virtual {v2, v0, v3}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1946464
    invoke-virtual {v2, v4, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    move-object p1, v2

    .line 1946465
    goto :goto_0

    .line 1946466
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1946467
    :cond_3
    iget v0, p0, LX/CuJ;->k:I

    int-to-float v0, v0

    sub-float/2addr v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1946448
    iget-object v0, p0, LX/CuJ;->b:LX/Cud;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CuJ;->b:LX/Cud;

    .line 1946449
    iget-object v1, v0, LX/Cud;->b:LX/Cui;

    move-object v0, v1

    .line 1946450
    sget-object v1, LX/Cui;->PAUSE_ICON:LX/Cui;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/CuJ;->b:LX/Cud;

    invoke-static {v0, p1}, LX/8ba;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1946451
    const/4 v0, 0x1

    .line 1946452
    :goto_0
    move v0, v0

    .line 1946453
    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LX/CuJ;->d(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1946454
    :cond_0
    const/4 v0, 0x0

    .line 1946455
    :goto_1
    return v0

    :cond_1
    iget-boolean v0, p0, LX/CuJ;->l:Z

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1946410
    invoke-direct {p0, p1}, LX/CuJ;->d(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1946411
    iget-object v0, p0, LX/CuJ;->d:Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1946412
    :goto_0
    return v0

    .line 1946413
    :cond_0
    invoke-direct {p0, p1}, LX/CuJ;->g(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1946414
    iget-object v2, p0, LX/CuJ;->c:Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CuJ;->c:Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    .line 1946415
    iget-object v3, v2, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    move-object v2, v3

    .line 1946416
    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CuJ;->c:Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    .line 1946417
    iget-object v3, v2, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    move-object v2, v3

    .line 1946418
    iget v3, p0, LX/CuJ;->j:I

    invoke-static {v2, v0, v3}, LX/8ba;->a(Landroid/view/View;Landroid/view/MotionEvent;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1946419
    if-eqz v2, :cond_1

    .line 1946420
    invoke-static {v0}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1946421
    invoke-virtual {v0, v6, v6}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1946422
    invoke-virtual {v0, v6, v6}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1946423
    iget-object v1, p0, LX/CuJ;->c:Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    .line 1946424
    iget-object v2, v1, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;->b:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    move-object v1, v2

    .line 1946425
    invoke-virtual {v1}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->getTouchDelegate()Landroid/view/TouchDelegate;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/TouchDelegate;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 1946426
    :cond_1
    iget-object v2, p0, LX/CuJ;->e:LX/CuK;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/CuJ;->e:LX/CuK;

    .line 1946427
    iget-object v3, v2, LX/CuK;->a:LX/Ctq;

    move-object v2, v3

    .line 1946428
    if-eqz v2, :cond_6

    iget-object v2, p0, LX/CuJ;->e:LX/CuK;

    .line 1946429
    iget-object v3, v2, LX/CuK;->a:LX/Ctq;

    move-object v2, v3

    .line 1946430
    iget-object v3, v2, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    move-object v2, v3

    .line 1946431
    if-eqz v2, :cond_6

    iget-object v2, p0, LX/CuJ;->e:LX/CuK;

    .line 1946432
    iget-object v3, v2, LX/CuK;->a:LX/Ctq;

    move-object v2, v3

    .line 1946433
    iget-object v3, v2, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    move-object v2, v3

    .line 1946434
    iget v3, p0, LX/CuJ;->j:I

    invoke-static {v2, v0, v3}, LX/8ba;->a(Landroid/view/View;Landroid/view/MotionEvent;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 1946435
    if-eqz v0, :cond_2

    .line 1946436
    iget-object v0, p0, LX/CuJ;->e:LX/CuK;

    invoke-virtual {v0}, LX/CuK;->a()V

    move v0, v1

    .line 1946437
    goto :goto_0

    .line 1946438
    :cond_2
    iget-boolean v0, p0, LX/CuJ;->g:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, LX/CuJ;->i:Z

    if-eqz v0, :cond_3

    .line 1946439
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1946440
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 1946441
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 1946442
    iget-object v4, p0, LX/CuJ;->h:LX/Cqt;

    sget-object v5, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    if-ne v4, v5, :cond_4

    .line 1946443
    neg-float v3, v3

    invoke-virtual {v0, v3, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1946444
    :goto_3
    invoke-virtual {v0, v6, v6}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    move-object p1, v0

    .line 1946445
    :cond_3
    iget-object v0, p0, LX/CuJ;->a:LX/CuX;

    invoke-virtual {v0, p1}, LX/CuX;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move v0, v1

    .line 1946446
    goto/16 :goto_0

    .line 1946447
    :cond_4
    neg-float v2, v2

    invoke-virtual {v0, v3, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method
