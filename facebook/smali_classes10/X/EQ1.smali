.class public LX/EQ1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2118105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;I)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;",
            ">;",
            "Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;",
            "I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2118106
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2118107
    const/4 v2, 0x1

    .line 2118108
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v6

    move v4, v1

    move v3, v1

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {p0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;

    .line 2118109
    invoke-static {v0, p1}, LX/EQ1;->a(Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2118110
    invoke-virtual {v5, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2118111
    add-int/lit8 v0, v3, 0x1

    move v2, v0

    move v0, v1

    .line 2118112
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 2118113
    :cond_0
    if-eqz v2, :cond_1

    .line 2118114
    invoke-virtual {v5, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2118115
    add-int/lit8 v3, v3, 0x1

    .line 2118116
    :cond_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    move v1, v3

    :goto_2
    if-ge v2, v4, :cond_2

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;

    .line 2118117
    if-eq v1, p2, :cond_2

    .line 2118118
    invoke-static {v0, p1}, LX/EQ1;->a(Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2118119
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2118120
    add-int/lit8 v0, v1, 0x1

    .line 2118121
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    .line 2118122
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    move v0, v2

    move v2, v3

    goto :goto_1
.end method

.method private static a(Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;)Z
    .locals 3

    .prologue
    .line 2118123
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 2118124
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel;->a()Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 2118125
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2118126
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 2118127
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchRecentSearchesGraphQLModels$FBRecentSearchesQueryModel$RecentSearchesModel$EdgesModel$NodeModel;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
