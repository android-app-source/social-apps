.class public LX/E1a;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/E1a;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2069924
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2069925
    sget-object v0, LX/0ax;->eh:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->PLACE_TIPS_SETTINGS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2069926
    return-void
.end method

.method public static a(LX/0QB;)LX/E1a;
    .locals 3

    .prologue
    .line 2069927
    sget-object v0, LX/E1a;->a:LX/E1a;

    if-nez v0, :cond_1

    .line 2069928
    const-class v1, LX/E1a;

    monitor-enter v1

    .line 2069929
    :try_start_0
    sget-object v0, LX/E1a;->a:LX/E1a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2069930
    if-eqz v2, :cond_0

    .line 2069931
    :try_start_1
    new-instance v0, LX/E1a;

    invoke-direct {v0}, LX/E1a;-><init>()V

    .line 2069932
    move-object v0, v0

    .line 2069933
    sput-object v0, LX/E1a;->a:LX/E1a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2069934
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2069935
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2069936
    :cond_1
    sget-object v0, LX/E1a;->a:LX/E1a;

    return-object v0

    .line 2069937
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2069938
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
