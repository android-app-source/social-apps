.class public LX/DSn;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/fig/button/FigButton;

.field public b:LX/2qr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1999856
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DSn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1999857
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1999858
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1999859
    const v0, 0x7f030821

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1999860
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/DSn;->setOrientation(I)V

    .line 1999861
    const v0, 0x7f0d154c

    invoke-virtual {p0, v0}, LX/DSn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/DSn;->a:Lcom/facebook/fig/button/FigButton;

    .line 1999862
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1999863
    const/16 v0, 0x8

    invoke-virtual {p0}, LX/DSn;->getVisibility()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 1999864
    invoke-virtual {p0, v2, v2}, LX/DSn;->setMeasuredDimension(II)V

    .line 1999865
    :goto_0
    return-void

    .line 1999866
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    goto :goto_0
.end method
