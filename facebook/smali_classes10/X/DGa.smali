.class public final LX/DGa;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/99i;

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;LX/0Px;LX/99i;I)V
    .locals 0

    .prologue
    .line 1980187
    iput-object p1, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iput-object p2, p0, LX/DGa;->a:LX/0Px;

    iput-object p3, p0, LX/DGa;->b:LX/99i;

    iput p4, p0, LX/DGa;->c:I

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method

.method private d(I)Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
    .locals 5

    .prologue
    .line 1980164
    iget-object v1, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, p0, LX/DGa;->b:LX/99i;

    iget-object v0, v0, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980165
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1980166
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    iget-object v2, p0, LX/DGa;->a:LX/0Px;

    .line 1980167
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    if-lt p1, v3, :cond_1

    .line 1980168
    invoke-static {v1, v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->a(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLStorySet;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1, v0, p1}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->b(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLStorySet;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1980169
    sget-object v3, LX/DGc;->APP_INSTALL:LX/DGc;

    .line 1980170
    :goto_0
    move-object v0, v3

    .line 1980171
    sget-object v1, LX/DGb;->a:[I

    invoke-virtual {v0}, LX/DGc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1980172
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No case to handle PageType:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1980173
    :pswitch_0
    iget-object v0, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->n:Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;

    .line 1980174
    :goto_1
    return-object v0

    .line 1980175
    :pswitch_1
    iget-object v0, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->p:Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;

    goto :goto_1

    .line 1980176
    :pswitch_2
    iget-object v0, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->l:Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;

    goto :goto_1

    .line 1980177
    :pswitch_3
    iget-object v0, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->o:Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;

    goto :goto_1

    .line 1980178
    :pswitch_4
    iget-object v0, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->m:Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;

    goto :goto_1

    .line 1980179
    :cond_0
    sget-object v3, LX/DGc;->LOADING:LX/DGc;

    goto :goto_0

    .line 1980180
    :cond_1
    invoke-virtual {v2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1980181
    invoke-static {v3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 1980182
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x4ed245b

    if-ne v3, v4, :cond_3

    .line 1980183
    iget-object v3, v1, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03R;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/03R;->asBoolean(Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1980184
    sget-object v3, LX/DGc;->VIDEO_AUTOPLAY:LX/DGc;

    goto :goto_0

    .line 1980185
    :cond_2
    sget-object v3, LX/DGc;->VIDEO:LX/DGc;

    goto :goto_0

    .line 1980186
    :cond_3
    sget-object v3, LX/DGc;->PHOTO:LX/DGc;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final synthetic a(I)LX/1RC;
    .locals 1

    .prologue
    .line 1980163
    invoke-direct {p0, p1}, LX/DGa;->d(I)Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2eI;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<",
            "LX/1Pf;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1980141
    iget-object v2, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, p0, LX/DGa;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    iget-object v0, p0, LX/DGa;->b:LX/99i;

    iget-object v0, v0, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980142
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1980143
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v2, v3, v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->a$redex0(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;ILcom/facebook/graphql/model/GraphQLStorySet;)I

    move-result v2

    move v0, v1

    .line 1980144
    :goto_0
    if-ge v0, v2, :cond_0

    .line 1980145
    invoke-direct {p0, v0}, LX/DGa;->d(I)Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;

    move-result-object v3

    new-instance v4, LX/DGm;

    iget-object v5, p0, LX/DGa;->b:LX/99i;

    iget-object v5, v5, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v6, p0, LX/DGa;->c:I

    invoke-direct {v4, v5, v0, v6, v1}, LX/DGm;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZ)V

    invoke-virtual {p1, v3, v4}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1980146
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1980147
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 5

    .prologue
    .line 1980148
    iget-object v0, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980149
    iget-object v0, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->j:LX/1K9;

    new-instance v1, LX/6Vh;

    invoke-direct {v1, p1}, LX/6Vh;-><init>(I)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1980150
    :cond_0
    iget-object v0, p0, LX/DGa;->b:LX/99i;

    iget-object v1, v0, LX/99i;->b:LX/8yy;

    iget-object v2, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, p0, LX/DGa;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    iget-object v0, p0, LX/DGa;->b:LX/99i;

    iget-object v0, v0, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980151
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1980152
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v2, v3, v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->a$redex0(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;ILcom/facebook/graphql/model/GraphQLStorySet;)I

    move-result v0

    invoke-interface {v1, p1, v0}, LX/8yy;->a(II)V

    .line 1980153
    iget-object v0, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->i:LX/2xr;

    iget-object v0, p0, LX/DGa;->b:LX/99i;

    iget-object v0, v0, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980154
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1980155
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v1, v0, p1}, LX/2xr;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1980156
    iget-object v0, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v1, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->c:LX/1LV;

    iget-object v0, p0, LX/DGa;->b:LX/99i;

    iget-object v0, v0, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980157
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1980158
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v1, v0, p1}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 1980159
    iget-object v1, p0, LX/DGa;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    iget-object v0, p0, LX/DGa;->b:LX/99i;

    iget-object v0, v0, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980160
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1980161
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v1, v0, p1}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->a$redex0(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLStorySet;I)V

    .line 1980162
    return-void
.end method
