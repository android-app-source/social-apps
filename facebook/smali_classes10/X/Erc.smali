.class public LX/Erc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8RE;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2173214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2173215
    return-void
.end method


# virtual methods
.method public final a()LX/03R;
    .locals 1

    .prologue
    .line 2173213
    sget-object v0, LX/03R;->UNSET:LX/03R;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2173212
    new-instance v0, Lcom/facebook/events/invite/InviteePickerRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/events/invite/InviteePickerRow;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Landroid/view/View;LX/621;)V
    .locals 1

    .prologue
    .line 2173196
    check-cast p1, LX/G6r;

    .line 2173197
    iget-object v0, p1, LX/G6r;->a:Landroid/widget/TextView;

    invoke-interface {p2}, LX/621;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2173198
    invoke-interface {p2}, LX/621;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, LX/G6r;->setVisibility(I)V

    .line 2173199
    return-void

    .line 2173200
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final a(Landroid/view/View;LX/8QL;Z)V
    .locals 0

    .prologue
    .line 2173210
    check-cast p1, Lcom/facebook/events/invite/InviteePickerRow;

    invoke-virtual {p1, p2, p3}, Lcom/facebook/events/invite/InviteePickerRow;->a(LX/8QL;Z)V

    .line 2173211
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2173208
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2173209
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2173207
    new-instance v0, Lcom/facebook/events/invite/InviteePickerRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/events/invite/InviteePickerRow;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final b(Landroid/view/View;LX/8QL;Z)V
    .locals 0

    .prologue
    .line 2173205
    check-cast p1, Lcom/facebook/events/invite/InviteePickerRow;

    invoke-virtual {p1, p2, p3}, Lcom/facebook/events/invite/InviteePickerRow;->a(LX/8QL;Z)V

    .line 2173206
    return-void
.end method

.method public final c(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2173203
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2173204
    const v1, 0x7f031532

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final d(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2173202
    new-instance v0, LX/G6r;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/G6r;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final e(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2173201
    new-instance v0, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
