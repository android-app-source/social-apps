.class public final LX/DFs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DFw;

.field public final synthetic b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;LX/DFw;)V
    .locals 0

    .prologue
    .line 1978909
    iput-object p1, p0, LX/DFs;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iput-object p2, p0, LX/DFs;->a:LX/DFw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x2a7b39b3

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978910
    iget-object v1, p0, LX/DFs;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v2, p0, LX/DFs;->a:LX/DFw;

    .line 1978911
    invoke-static {v2}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->f(LX/DFw;)LX/162;

    move-result-object v3

    iget-object v4, v2, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-static {v4}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;

    move-result-object v4

    .line 1978912
    invoke-static {v3}, LX/17Q;->F(LX/0lF;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1978913
    const/4 v5, 0x0

    .line 1978914
    :goto_0
    move-object v3, v5

    .line 1978915
    iget-object v4, v1, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->d:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1978916
    iget-object v1, p0, LX/DFs;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->j:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->aE:Ljava/lang/String;

    iget-object v4, p0, LX/DFs;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v5, p0, LX/DFs;->a:LX/DFw;

    iget-object v5, v5, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-static {v4, v5}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->a(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1978917
    const v1, -0x68a43641

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "place_review_ego_launched_page"

    invoke-direct {v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "tracking"

    invoke-virtual {v5, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v2, "page_id"

    invoke-virtual {v5, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v2, "native_newsfeed"

    .line 1978918
    iput-object v2, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1978919
    move-object v5, v5

    .line 1978920
    goto :goto_0
.end method
