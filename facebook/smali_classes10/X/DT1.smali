.class public final LX/DT1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupRemoveAdminMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/DT7;


# direct methods
.method public constructor <init>(LX/DT7;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2000129
    iput-object p1, p0, LX/DT1;->c:LX/DT7;

    iput-object p2, p0, LX/DT1;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DT1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2000130
    iget-object v0, p0, LX/DT1;->c:LX/DT7;

    iget-object v0, v0, LX/DT7;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DT1;->c:LX/DT7;

    iget-object v2, v2, LX/DT7;->c:Landroid/content/res/Resources;

    const v3, 0x7f082fb6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2000131
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2000132
    iget-object v0, p0, LX/DT1;->c:LX/DT7;

    iget-object v0, v0, LX/DT7;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DT1;->c:LX/DT7;

    iget-object v2, v2, LX/DT7;->c:Landroid/content/res/Resources;

    const v3, 0x7f082fb7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2000133
    iget-object v0, p0, LX/DT1;->c:LX/DT7;

    iget-object v1, p0, LX/DT1;->a:Ljava/lang/String;

    iget-object v2, p0, LX/DT1;->b:Ljava/lang/String;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->NONE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-static {v0, v1, v2, v3}, LX/DT7;->a$redex0(LX/DT7;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)V

    .line 2000134
    return-void
.end method
