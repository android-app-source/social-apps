.class public LX/DvQ;
.super LX/Dcc;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/DvI;

.field private final c:LX/0tX;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private final e:LX/DvM;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/ExecutorService;LX/DvI;LX/0tX;LX/DvM;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2058501
    invoke-direct {p0}, LX/Dcc;-><init>()V

    .line 2058502
    iput-object p1, p0, LX/DvQ;->a:Ljava/lang/String;

    .line 2058503
    iput-object p2, p0, LX/DvQ;->d:Ljava/util/concurrent/ExecutorService;

    .line 2058504
    iput-object p3, p0, LX/DvQ;->b:LX/DvI;

    .line 2058505
    iput-object p4, p0, LX/DvQ;->c:LX/0tX;

    .line 2058506
    iput-object p5, p0, LX/DvQ;->e:LX/DvM;

    .line 2058507
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;",
            "IZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2058508
    new-instance v0, LX/8I9;

    invoke-direct {v0}, LX/8I9;-><init>()V

    move-object v0, v0

    .line 2058509
    const-string v1, "node_id"

    iget-object v2, p0, LX/DvQ;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058510
    const-string v1, "count"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058511
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2058512
    const-string v1, "before"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058513
    :cond_0
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2058514
    const-string v1, "after"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058515
    :cond_1
    iget-object v1, p0, LX/DvQ;->b:LX/DvI;

    invoke-virtual {v1, v0}, LX/DvI;->a(LX/0gW;)LX/0gW;

    .line 2058516
    iget-object v1, p0, LX/DvQ;->c:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    if-eqz p5, :cond_2

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    invoke-virtual {v2, v0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2058517
    new-instance v1, LX/DvP;

    invoke-direct {v1, p0}, LX/DvP;-><init>(LX/DvQ;)V

    iget-object v2, p0, LX/DvQ;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2058518
    return-object v0

    .line 2058519
    :cond_2
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
