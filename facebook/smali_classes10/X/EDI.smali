.class public LX/EDI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/EDI;


# instance fields
.field private a:LX/2Tm;


# direct methods
.method public constructor <init>(LX/2Tm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2091219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2091220
    iput-object p1, p0, LX/EDI;->a:LX/2Tm;

    .line 2091221
    return-void
.end method

.method public static a(LX/0QB;)LX/EDI;
    .locals 4

    .prologue
    .line 2091222
    sget-object v0, LX/EDI;->b:LX/EDI;

    if-nez v0, :cond_1

    .line 2091223
    const-class v1, LX/EDI;

    monitor-enter v1

    .line 2091224
    :try_start_0
    sget-object v0, LX/EDI;->b:LX/EDI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2091225
    if-eqz v2, :cond_0

    .line 2091226
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2091227
    new-instance p0, LX/EDI;

    invoke-static {v0}, LX/2Tm;->a(LX/0QB;)LX/2Tm;

    move-result-object v3

    check-cast v3, LX/2Tm;

    invoke-direct {p0, v3}, LX/EDI;-><init>(LX/2Tm;)V

    .line 2091228
    move-object v0, p0

    .line 2091229
    sput-object v0, LX/EDI;->b:LX/EDI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2091230
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2091231
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2091232
    :cond_1
    sget-object v0, LX/EDI;->b:LX/EDI;

    return-object v0

    .line 2091233
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2091234
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2091235
    iget-object v0, p0, LX/EDI;->a:LX/2Tm;

    invoke-virtual {v0}, LX/2Tm;->b()V

    .line 2091236
    return-void
.end method
