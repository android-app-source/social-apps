.class public LX/Eel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EeO;


# instance fields
.field private final a:LX/1sZ;

.field private final b:LX/Een;

.field private final c:LX/1wh;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/appupdate/ApkDiffPatcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1sZ;LX/Een;LX/1wh;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1sZ;",
            "LX/Een;",
            "LX/1wh;",
            "LX/0Or",
            "<",
            "Lcom/facebook/appupdate/ApkDiffPatcher;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2153341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2153342
    iput-object p1, p0, LX/Eel;->a:LX/1sZ;

    .line 2153343
    iput-object p2, p0, LX/Eel;->b:LX/Een;

    .line 2153344
    iput-object p3, p0, LX/Eel;->c:LX/1wh;

    .line 2153345
    iput-object p4, p0, LX/Eel;->d:LX/0Or;

    .line 2153346
    return-void
.end method

.method private static a(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 2153347
    :try_start_0
    new-instance v0, Ljava/util/jar/JarFile;

    invoke-direct {v0, p0}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V

    .line 2153348
    invoke-virtual {v0}, Ljava/util/jar/JarFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    .line 2153349
    return-void

    .line 2153350
    :catch_0
    move-exception v0

    .line 2153351
    new-instance v1, LX/Eey;

    const-string v2, "patch_failure_ERROR_OPEN_DOWNLOADED_APK_IOEXCEPTION"

    const-string v3, "Open downloaded APK failed by IOException"

    invoke-direct {v1, v2, v0, v3}, LX/Eey;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1

    .line 2153352
    :catch_1
    move-exception v0

    .line 2153353
    new-instance v1, LX/Eey;

    const-string v2, "patch_failure_ERROR_OPEN_DOWNLOADED_APK_SECURITYEXCEPTION"

    const-string v3, "Open downloaded APK failed by SecurityException"

    invoke-direct {v1, v2, v0, v3}, LX/Eey;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1

    .line 2153354
    :catch_2
    move-exception v0

    .line 2153355
    new-instance v1, LX/Eey;

    const-string v2, "patch_failure_ERROR_OPEN_DOWNLOADED_APK_OOMERROR"

    const-string v3, "Open downloaded APK failed by OutOfMemoryError"

    invoke-direct {v1, v2, v0, v3}, LX/Eey;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a(LX/EeX;)LX/EeY;
    .locals 12

    .prologue
    const/4 v7, 0x0

    .line 2153356
    iget-object v0, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/EeX;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/EeX;->localDiffDownloadFile:Ljava/io/File;

    if-nez v0, :cond_1

    .line 2153357
    :cond_0
    new-instance v0, LX/EeY;

    invoke-direct {v0}, LX/EeY;-><init>()V

    .line 2153358
    :goto_0
    return-object v0

    .line 2153359
    :cond_1
    invoke-static {}, LX/Eem;->b()J

    move-result-wide v2

    .line 2153360
    iget-object v0, p0, LX/Eel;->c:LX/1wh;

    const-string v1, "appupdate_patch_start"

    iget-object v4, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {p1}, LX/EeX;->d()LX/Eeh;

    move-result-object v5

    const-string v6, "task_start"

    invoke-virtual {v0, v1, v4, v5, v6}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V

    .line 2153361
    iget-object v0, p0, LX/Eel;->a:LX/1sZ;

    iget-object v1, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v1, v1, Lcom/facebook/appupdate/ReleaseInfo;->packageName:Ljava/lang/String;

    .line 2153362
    :try_start_0
    iget-object v4, v0, LX/1sZ;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 2153363
    iget-object v4, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v4, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 2153364
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2153365
    :goto_1
    move-object v1, v4

    .line 2153366
    if-nez v1, :cond_2

    .line 2153367
    new-instance v0, LX/Eey;

    const-string v1, "patch_failure_ERROR_NO_CURRENT_APP_APK"

    const-string v2, "Patch Diff Failed (ERROR_NO_CURRENT_APP_APK)"

    invoke-direct {v0, v1, v7, v2}, LX/Eey;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v0

    .line 2153368
    :cond_2
    iget-object v0, p0, LX/Eel;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GTF;

    .line 2153369
    if-nez v0, :cond_3

    .line 2153370
    new-instance v0, LX/Eey;

    const-string v1, "patch_failure_APK_DIFF_PATCHER_NOT_PROVIDED"

    const-string v2, "APK Diff Patcher is null"

    invoke-direct {v0, v1, v7, v2}, LX/Eey;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v0

    .line 2153371
    :cond_3
    iget-object v4, p0, LX/Eel;->a:LX/1sZ;

    iget-wide v6, p1, LX/EeX;->downloadId:J

    .line 2153372
    invoke-static {v4}, LX/1sZ;->a(LX/1sZ;)Ljava/io/File;

    move-result-object v5

    .line 2153373
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "temp_patched_"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".apk"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v5, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2153374
    move-object v4, v8

    .line 2153375
    :try_start_1
    iget-object v5, p1, LX/EeX;->localDiffDownloadFile:Ljava/io/File;

    .line 2153376
    new-instance v6, LX/GaN;

    new-instance v7, Lcom/facebook/common/patch/core/BsdiffNativeLibrary;

    invoke-direct {v7}, Lcom/facebook/common/patch/core/BsdiffNativeLibrary;-><init>()V

    invoke-direct {v6, v7}, LX/GaN;-><init>(Lcom/facebook/common/patch/core/BsdiffNativeLibrary;)V

    .line 2153377
    invoke-virtual {v6, v1, v5, v4}, LX/GaN;->a(Ljava/io/File;Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 2153378
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Ljava/io/File;->setReadable(ZZ)Z

    .line 2153379
    iget-object v0, p1, LX/EeX;->localDiffDownloadFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2153380
    invoke-static {v4}, LX/Eel;->a(Ljava/io/File;)V

    .line 2153381
    new-instance v0, LX/EeW;

    invoke-direct {v0, p1}, LX/EeW;-><init>(LX/EeX;)V

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2153382
    iput-object v1, v0, LX/EeW;->e:Ljava/lang/Integer;

    .line 2153383
    move-object v0, v0

    .line 2153384
    iput-object v4, v0, LX/EeW;->i:Ljava/io/File;

    .line 2153385
    move-object v0, v0

    .line 2153386
    invoke-virtual {v0}, LX/EeW;->a()LX/EeX;

    move-result-object v1

    .line 2153387
    invoke-static {}, LX/Eem;->b()J

    move-result-wide v4

    .line 2153388
    iget-object v0, p0, LX/Eel;->c:LX/1wh;

    const-string v6, "appupdate_patch_successful"

    invoke-virtual {p1, v2, v3, v4, v5}, LX/EeX;->a(JJ)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v6, v2}, LX/1wh;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2153389
    iget-object v0, p0, LX/Eel;->c:LX/1wh;

    const-string v2, "appupdate_patch_successful"

    iget-object v3, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {p1}, LX/EeX;->d()LX/Eeh;

    move-result-object v4

    const-string v5, "task_success"

    invoke-virtual {v0, v2, v3, v4, v5}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V

    .line 2153390
    new-instance v0, LX/EeY;

    iget-object v2, p0, LX/Eel;->b:LX/Een;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v1, v2, v4, v5}, LX/EeY;-><init>(LX/EeX;LX/EeO;J)V

    goto/16 :goto_0

    .line 2153391
    :catch_0
    move-exception v0

    .line 2153392
    new-instance v1, LX/Eey;

    const-string v2, "patch_failure_ERROR_PATCH_DIFF_APK_FAILURE"

    const-string v3, "Patch Diff Failed (ERROR_PATCH_DIFF_APK_FAILURE)"

    invoke-direct {v1, v2, v0, v3}, LX/Eey;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1

    .line 2153393
    :catch_1
    move-exception v4

    .line 2153394
    const-string v5, "AppUpdateLib"

    const-string v6, "Could not get current application APK file path"

    invoke-static {v5, v6, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2153395
    const/4 v4, 0x0

    goto/16 :goto_1
.end method
