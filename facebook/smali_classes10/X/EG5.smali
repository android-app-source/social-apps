.class public final enum LX/EG5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EG5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EG5;

.field public static final enum Activity:LX/EG5;

.field public static final enum ChatHead:LX/EG5;

.field public static final enum Override:LX/EG5;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2096707
    new-instance v0, LX/EG5;

    const-string v1, "Override"

    invoke-direct {v0, v1, v2}, LX/EG5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EG5;->Override:LX/EG5;

    .line 2096708
    new-instance v0, LX/EG5;

    const-string v1, "Activity"

    invoke-direct {v0, v1, v3}, LX/EG5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EG5;->Activity:LX/EG5;

    .line 2096709
    new-instance v0, LX/EG5;

    const-string v1, "ChatHead"

    invoke-direct {v0, v1, v4}, LX/EG5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EG5;->ChatHead:LX/EG5;

    .line 2096710
    const/4 v0, 0x3

    new-array v0, v0, [LX/EG5;

    sget-object v1, LX/EG5;->Override:LX/EG5;

    aput-object v1, v0, v2

    sget-object v1, LX/EG5;->Activity:LX/EG5;

    aput-object v1, v0, v3

    sget-object v1, LX/EG5;->ChatHead:LX/EG5;

    aput-object v1, v0, v4

    sput-object v0, LX/EG5;->$VALUES:[LX/EG5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2096711
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EG5;
    .locals 1

    .prologue
    .line 2096712
    const-class v0, LX/EG5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EG5;

    return-object v0
.end method

.method public static values()[LX/EG5;
    .locals 1

    .prologue
    .line 2096713
    sget-object v0, LX/EG5;->$VALUES:[LX/EG5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EG5;

    return-object v0
.end method
