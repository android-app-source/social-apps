.class public LX/EkA;
.super LX/76U;
.source ""


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/17W;

.field public final e:LX/Ejx;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U8;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/3Lz;

.field public final h:LX/Ejy;

.field public final i:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public final j:LX/EkI;

.field public final k:LX/Ek6;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/17W;LX/Ejx;LX/0Ot;LX/Ek6;LX/3Lz;LX/Ejy;Landroid/content/Context;LX/78A;Ljava/lang/Runnable;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/EkI;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p8    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/78A;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/EkI;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/17W;",
            "LX/Ejx;",
            "LX/0Ot",
            "<",
            "LX/2U8;",
            ">;",
            "LX/Ek6;",
            "LX/3Lz;",
            "LX/Ejy;",
            "Landroid/content/Context;",
            "LX/78A;",
            "Ljava/lang/Runnable;",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;",
            "LX/EkI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2163088
    invoke-direct {p0, p10, p9}, LX/76U;-><init>(Ljava/lang/Runnable;LX/78A;)V

    .line 2163089
    iput-object p8, p0, LX/EkA;->b:Landroid/content/Context;

    .line 2163090
    iput-object p1, p0, LX/EkA;->c:Ljava/util/concurrent/ExecutorService;

    .line 2163091
    iput-object p2, p0, LX/EkA;->d:LX/17W;

    .line 2163092
    iput-object p3, p0, LX/EkA;->e:LX/Ejx;

    .line 2163093
    iput-object p4, p0, LX/EkA;->f:LX/0Ot;

    .line 2163094
    iput-object p5, p0, LX/EkA;->k:LX/Ek6;

    .line 2163095
    iput-object p6, p0, LX/EkA;->g:LX/3Lz;

    .line 2163096
    iput-object p7, p0, LX/EkA;->h:LX/Ejy;

    .line 2163097
    iput-object p11, p0, LX/EkA;->i:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 2163098
    iput-object p12, p0, LX/EkA;->j:LX/EkI;

    .line 2163099
    return-void
.end method


# virtual methods
.method public final f()V
    .locals 1

    .prologue
    .line 2163100
    iget-object v0, p0, LX/EkA;->j:LX/EkI;

    invoke-virtual {v0}, LX/EkI;->a()V

    .line 2163101
    return-void
.end method
