.class public final LX/Egk;
.super LX/BcN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcN",
        "<",
        "LX/Egl;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Egj;

.field private b:[Ljava/lang/String;

.field private c:I

.field public d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2157193
    invoke-direct {p0}, LX/BcN;-><init>()V

    .line 2157194
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "settingsSectionTitle"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "settingsBookmarks"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Egk;->b:[Ljava/lang/String;

    .line 2157195
    iput v3, p0, LX/Egk;->c:I

    .line 2157196
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Egk;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Egk;->d:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2157189
    invoke-super {p0}, LX/BcN;->a()V

    .line 2157190
    const/4 v0, 0x0

    iput-object v0, p0, LX/Egk;->a:LX/Egj;

    .line 2157191
    sget-object v0, LX/Egl;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2157192
    return-void
.end method

.method public final b()LX/BcO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/BcO",
            "<",
            "LX/Egl;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2157197
    iget-object v1, p0, LX/Egk;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Egk;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Egk;->c:I

    if-ge v1, v2, :cond_2

    .line 2157198
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2157199
    :goto_0
    iget v2, p0, LX/Egk;->c:I

    if-ge v0, v2, :cond_1

    .line 2157200
    iget-object v2, p0, LX/Egk;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2157201
    iget-object v2, p0, LX/Egk;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2157202
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2157203
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2157204
    :cond_2
    iget-object v0, p0, LX/Egk;->a:LX/Egj;

    .line 2157205
    invoke-virtual {p0}, LX/Egk;->a()V

    .line 2157206
    return-object v0
.end method

.method public final b(LX/BcQ;)LX/Egk;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcQ",
            "<",
            "LX/BcM;",
            ">;)",
            "LX/Egk;"
        }
    .end annotation

    .prologue
    .line 2157187
    invoke-super {p0, p1}, LX/BcN;->a(LX/BcQ;)V

    .line 2157188
    return-object p0
.end method
