.class public final LX/DkK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2034099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034100
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2034101
    if-eqz p1, :cond_0

    .line 2034102
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034103
    if-eqz v0, :cond_0

    .line 2034104
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034105
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2034106
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034107
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel$ActorModel;->j()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel$ActorModel$NativeBookingRequestModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2034108
    :cond_0
    const/4 v0, 0x0

    .line 2034109
    :goto_0
    return-object v0

    .line 2034110
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034111
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel$ActorModel;->j()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel$ActorModel$NativeBookingRequestModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsQueryModel$ActorModel$NativeBookingRequestModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
