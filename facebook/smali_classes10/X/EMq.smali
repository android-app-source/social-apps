.class public LX/EMq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2111456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2111457
    return-void
.end method

.method public static a(LX/0QB;)LX/EMq;
    .locals 3

    .prologue
    .line 2111458
    const-class v1, LX/EMq;

    monitor-enter v1

    .line 2111459
    :try_start_0
    sget-object v0, LX/EMq;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111460
    sput-object v2, LX/EMq;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111461
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111462
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2111463
    new-instance v0, LX/EMq;

    invoke-direct {v0}, LX/EMq;-><init>()V

    .line 2111464
    move-object v0, v0

    .line 2111465
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111466
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EMq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111467
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111468
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
