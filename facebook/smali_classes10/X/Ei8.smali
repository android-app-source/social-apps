.class public final LX/Ei8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;)V
    .locals 0

    .prologue
    .line 2159025
    iput-object p1, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x31dbb497

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2159026
    const-string v0, "extra_background_confirmed_contactpoint"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/Contactpoint;

    .line 2159027
    iget-object v2, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v2, v2, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->B:Lcom/facebook/growth/model/Contactpoint;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v2, v2, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->B:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v2}, Lcom/facebook/growth/model/Contactpoint;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/growth/model/Contactpoint;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2159028
    :cond_0
    const/16 v0, 0x27

    const v2, 0x261c9902

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2159029
    :goto_0
    return-void

    .line 2159030
    :cond_1
    iget-object v0, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-boolean v0, v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->D:Z

    if-eqz v0, :cond_2

    .line 2159031
    iget-object v0, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v0, v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->u:LX/EjB;

    invoke-virtual {v0}, LX/EjB;->a()V

    .line 2159032
    :cond_2
    iget-object v0, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-boolean v0, v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->E:Z

    if-eqz v0, :cond_4

    .line 2159033
    iget-object v0, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v0, v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->s:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f0833f6

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2159034
    iget-object v0, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v0, v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->q:LX/2U9;

    sget-object v2, LX/Eiw;->BACKGROUND_CONFIRM_SUCCEED:LX/Eiw;

    invoke-virtual {v0, v2, v4, v4}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159035
    iget-object v0, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v0, v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->q:LX/2U9;

    invoke-virtual {v0}, LX/2U9;->a()V

    .line 2159036
    :goto_1
    iget-object v0, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v0, v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->q:LX/2U9;

    iget-object v2, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v2, v2, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->B:Lcom/facebook/growth/model/Contactpoint;

    .line 2159037
    iget-object v3, v0, LX/2U9;->a:LX/0Zb;

    sget-object v4, LX/Eiw;->FLOW_EXIT_SINCE_BACKGROUND_CONF:LX/Eiw;

    invoke-virtual {v4}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v4

    const/4 p1, 0x1

    invoke-interface {v3, v4, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2159038
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2159039
    const-string v4, "confirmation"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159040
    const-string v4, "current_contactpoint"

    iget-object p1, v2, Lcom/facebook/growth/model/Contactpoint;->normalized:Ljava/lang/String;

    invoke-virtual {v3, v4, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159041
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2159042
    :cond_3
    iget-object v0, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    invoke-virtual {v0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->finish()V

    .line 2159043
    const v0, 0x4808455

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0

    .line 2159044
    :cond_4
    iget-object v0, p0, LX/Ei8;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v0, v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->s:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f0833f5

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    const/16 v3, 0x11

    .line 2159045
    iput v3, v2, LX/27k;->b:I

    .line 2159046
    move-object v2, v2

    .line 2159047
    invoke-virtual {v0, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_1
.end method
