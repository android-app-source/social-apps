.class public final LX/DyA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V
    .locals 0

    .prologue
    .line 2064009
    iput-object p1, p0, LX/DyA;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2064010
    iget-object v0, p0, LX/DyA;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c()V

    .line 2064011
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2064012
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2064013
    if-eqz p1, :cond_0

    .line 2064014
    iget-object v0, p0, LX/DyA;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    .line 2064015
    iput-object p1, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->z:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2064016
    iget-object v0, p0, LX/DyA;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    .line 2064017
    iget-object v1, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2064018
    iput-object v1, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->s:Ljava/lang/String;

    .line 2064019
    iget-object v0, p0, LX/DyA;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->E:LX/0SI;

    iget-object v1, p0, LX/DyA;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->z:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-interface {v0, v1}, LX/0SI;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2064020
    iget-object v0, p0, LX/DyA;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->f:LX/9bF;

    iget-object v1, p0, LX/DyA;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-static {v1}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->e(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)Z

    move-result v1

    .line 2064021
    iget-boolean p1, v0, LX/9bF;->e:Z

    if-eq v1, p1, :cond_0

    .line 2064022
    iput-boolean v1, v0, LX/9bF;->e:Z

    .line 2064023
    const p1, -0x4b27b397

    invoke-static {v0, p1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2064024
    :cond_0
    iget-object v0, p0, LX/DyA;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c()V

    .line 2064025
    return-void
.end method
