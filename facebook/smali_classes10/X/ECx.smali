.class public LX/ECx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/ECx;


# instance fields
.field private a:Z

.field public final b:Landroid/os/PowerManager;

.field public final c:LX/1r1;

.field private final d:Landroid/net/wifi/WifiManager;

.field private final e:LX/0ka;

.field private f:LX/1ql;

.field private g:LX/1ql;

.field private h:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method public constructor <init>(Landroid/os/PowerManager;LX/1r1;Landroid/net/wifi/WifiManager;LX/0ka;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2090684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2090685
    iput-object p1, p0, LX/ECx;->b:Landroid/os/PowerManager;

    .line 2090686
    iput-object p2, p0, LX/ECx;->c:LX/1r1;

    .line 2090687
    iput-object p3, p0, LX/ECx;->d:Landroid/net/wifi/WifiManager;

    .line 2090688
    iput-object p4, p0, LX/ECx;->e:LX/0ka;

    .line 2090689
    const/4 v0, 0x0

    .line 2090690
    :try_start_0
    iget-object p1, p0, LX/ECx;->c:LX/1r1;

    const/16 p2, 0x20

    const-string p3, "IncallWakeLocks"

    invoke-virtual {p1, p2, p3}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object p1

    .line 2090691
    if-eqz p1, :cond_0

    .line 2090692
    iget-object p2, p0, LX/ECx;->c:LX/1r1;

    invoke-virtual {p2, p1}, LX/1r1;->a(LX/1ql;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2090693
    const/4 v0, 0x1

    .line 2090694
    :cond_0
    :goto_0
    move v0, v0

    .line 2090695
    iput-boolean v0, p0, LX/ECx;->a:Z

    .line 2090696
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/ECx;
    .locals 7

    .prologue
    .line 2090697
    sget-object v0, LX/ECx;->i:LX/ECx;

    if-nez v0, :cond_1

    .line 2090698
    const-class v1, LX/ECx;

    monitor-enter v1

    .line 2090699
    :try_start_0
    sget-object v0, LX/ECx;->i:LX/ECx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2090700
    if-eqz v2, :cond_0

    .line 2090701
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2090702
    new-instance p0, LX/ECx;

    invoke-static {v0}, LX/0kZ;->b(LX/0QB;)Landroid/os/PowerManager;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    invoke-static {v0}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v4

    check-cast v4, LX/1r1;

    invoke-static {v0}, LX/0kt;->b(LX/0QB;)Landroid/net/wifi/WifiManager;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/WifiManager;

    invoke-static {v0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v6

    check-cast v6, LX/0ka;

    invoke-direct {p0, v3, v4, v5, v6}, LX/ECx;-><init>(Landroid/os/PowerManager;LX/1r1;Landroid/net/wifi/WifiManager;LX/0ka;)V

    .line 2090703
    move-object v0, p0

    .line 2090704
    sput-object v0, LX/ECx;->i:LX/ECx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2090705
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2090706
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2090707
    :cond_1
    sget-object v0, LX/ECx;->i:LX/ECx;

    return-object v0

    .line 2090708
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2090709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 2090710
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/ECx;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 2090711
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2090712
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/ECx;->f:LX/1ql;

    if-nez v0, :cond_2

    .line 2090713
    iget-object v0, p0, LX/ECx;->c:LX/1r1;

    const/16 v1, 0x20

    const-string v2, "IncallWakeLocks"

    invoke-virtual {v0, v1, v2}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    iput-object v0, p0, LX/ECx;->f:LX/1ql;

    .line 2090714
    iget-object v0, p0, LX/ECx;->f:LX/1ql;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1ql;->a(Z)V

    .line 2090715
    :cond_2
    iget-object v0, p0, LX/ECx;->f:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->e()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2090716
    :try_start_2
    iget-object v0, p0, LX/ECx;->f:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->c()V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2090717
    :catch_0
    const/4 v0, 0x0

    :try_start_3
    iput-boolean v0, p0, LX/ECx;->a:Z

    .line 2090718
    invoke-virtual {p0}, LX/ECx;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2090719
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 2090720
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ECx;->f:LX/1ql;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ECx;->f:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090721
    iget-object v0, p0, LX/ECx;->f:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 2090722
    :cond_0
    iget-object v0, p0, LX/ECx;->f:LX/1ql;

    if-eqz v0, :cond_1

    .line 2090723
    iget-object v0, p0, LX/ECx;->c:LX/1r1;

    iget-object v1, p0, LX/ECx;->f:LX/1ql;

    invoke-virtual {v0, v1}, LX/1r1;->a(LX/1ql;)V

    .line 2090724
    const/4 v0, 0x0

    iput-object v0, p0, LX/ECx;->f:LX/1ql;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2090725
    :cond_1
    monitor-exit p0

    return-void

    .line 2090726
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 3

    .prologue
    .line 2090727
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ECx;->g:LX/1ql;

    if-nez v0, :cond_0

    .line 2090728
    iget-object v0, p0, LX/ECx;->c:LX/1r1;

    const/4 v1, 0x1

    const-string v2, "IncallWakeLocks"

    invoke-virtual {v0, v1, v2}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    iput-object v0, p0, LX/ECx;->g:LX/1ql;

    .line 2090729
    iget-object v0, p0, LX/ECx;->g:LX/1ql;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1ql;->a(Z)V

    .line 2090730
    :cond_0
    iget-object v0, p0, LX/ECx;->g:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2090731
    iget-object v0, p0, LX/ECx;->g:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2090732
    :cond_1
    monitor-exit p0

    return-void

    .line 2090733
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 2

    .prologue
    .line 2090734
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ECx;->g:LX/1ql;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ECx;->g:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090735
    iget-object v0, p0, LX/ECx;->g:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 2090736
    :cond_0
    iget-object v0, p0, LX/ECx;->g:LX/1ql;

    if-eqz v0, :cond_1

    .line 2090737
    iget-object v0, p0, LX/ECx;->c:LX/1r1;

    iget-object v1, p0, LX/ECx;->g:LX/1ql;

    invoke-virtual {v0, v1}, LX/1r1;->a(LX/1ql;)V

    .line 2090738
    const/4 v0, 0x0

    iput-object v0, p0, LX/ECx;->g:LX/1ql;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2090739
    :cond_1
    monitor-exit p0

    return-void

    .line 2090740
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 3

    .prologue
    .line 2090741
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ECx;->e:LX/0ka;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ka;->a(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2090742
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2090743
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/ECx;->h:Landroid/net/wifi/WifiManager$WifiLock;

    if-nez v0, :cond_2

    .line 2090744
    iget-object v0, p0, LX/ECx;->d:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x3

    const-string v2, "IncallWakeLocks"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, LX/ECx;->h:Landroid/net/wifi/WifiManager$WifiLock;

    .line 2090745
    iget-object v0, p0, LX/ECx;->h:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 2090746
    :cond_2
    iget-object v0, p0, LX/ECx;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2090747
    iget-object v0, p0, LX/ECx;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2090748
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 1

    .prologue
    .line 2090749
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ECx;->h:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ECx;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090750
    iget-object v0, p0, LX/ECx;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 2090751
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/ECx;->h:Landroid/net/wifi/WifiManager$WifiLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2090752
    monitor-exit p0

    return-void

    .line 2090753
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
