.class public final LX/DMT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/events/GroupEventsBaseFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/events/GroupEventsBaseFragment;)V
    .locals 0

    .prologue
    .line 1989853
    iput-object p1, p0, LX/DMT;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1989838
    iget-object v0, p0, LX/DMT;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v0, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->j:LX/DMJ;

    .line 1989839
    if-eqz p1, :cond_0

    .line 1989840
    iput-object p1, v0, LX/DMJ;->j:LX/0Px;

    .line 1989841
    if-eqz p2, :cond_0

    .line 1989842
    const v1, 0x3d9c72d0

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1989843
    :cond_0
    iget-object v0, p0, LX/DMT;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v0, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->j:LX/DMJ;

    .line 1989844
    iget-object v1, v0, LX/DMJ;->j:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1989845
    if-nez v0, :cond_1

    .line 1989846
    iget-object v0, p0, LX/DMT;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/groups/events/GroupEventsBaseFragment;->a$redex0(Lcom/facebook/groups/events/GroupEventsBaseFragment;Z)V

    .line 1989847
    :goto_1
    return-void

    .line 1989848
    :cond_1
    iget-object v0, p0, LX/DMT;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    .line 1989849
    iget-object v1, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->d:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1989850
    iget-object v1, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->d:Landroid/view/View;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1989851
    :cond_2
    iget-object v1, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    invoke-interface {v1}, LX/DMV;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1989852
    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1989833
    iget-object v0, p0, LX/DMT;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v0, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->j:LX/DMJ;

    .line 1989834
    iget-boolean p0, v0, LX/DMJ;->k:Z

    if-eq p1, p0, :cond_0

    .line 1989835
    iput-boolean p1, v0, LX/DMJ;->k:Z

    .line 1989836
    const p0, 0x77ff0542

    invoke-static {v0, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1989837
    :cond_0
    return-void
.end method
