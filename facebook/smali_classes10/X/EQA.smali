.class public final LX/EQA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Cw5;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2SY;


# direct methods
.method public constructor <init>(LX/2SY;)V
    .locals 0

    .prologue
    .line 2118316
    iput-object p1, p0, LX/EQA;->a:LX/2SY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2118326
    iget-object v0, p0, LX/EQA;->a:LX/2SY;

    invoke-static {v0}, LX/2SY;->n(LX/2SY;)V

    .line 2118327
    iget-object v0, p0, LX/EQA;->a:LX/2SY;

    invoke-virtual {v0}, LX/2SP;->g()V

    .line 2118328
    iget-object v0, p0, LX/EQA;->a:LX/2SY;

    iget-object v0, v0, LX/2SY;->g:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_NULL_STATE_RECENT_SEARCHES_FAIL:LX/3Ql;

    invoke-virtual {v0, v1, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2118329
    iget-object v0, p0, LX/EQA;->a:LX/2SY;

    const/4 v1, 0x0

    .line 2118330
    iput-object v1, v0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2118331
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2118317
    check-cast p1, LX/Cw5;

    .line 2118318
    iget-object v0, p0, LX/EQA;->a:LX/2SY;

    invoke-static {v0}, LX/2SY;->n(LX/2SY;)V

    .line 2118319
    iget-object v0, p0, LX/EQA;->a:LX/2SY;

    invoke-virtual {v0, p1}, LX/2SP;->a(LX/Cw5;)V

    .line 2118320
    iget-object v0, p0, LX/EQA;->a:LX/2SY;

    .line 2118321
    iget-object v1, p1, LX/Cw5;->a:LX/0Px;

    move-object v1, v1

    .line 2118322
    invoke-static {v0, v1}, LX/2SY;->b(LX/2SY;LX/0Px;)V

    .line 2118323
    iget-object v0, p0, LX/EQA;->a:LX/2SY;

    const/4 v1, 0x0

    .line 2118324
    iput-object v1, v0, LX/2SY;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2118325
    return-void
.end method
