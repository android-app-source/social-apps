.class public final LX/Egu;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Egu;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Egt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Egv;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2157427
    const/4 v0, 0x0

    sput-object v0, LX/Egu;->a:LX/Egu;

    .line 2157428
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Egu;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2157406
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2157407
    new-instance v0, LX/Egv;

    invoke-direct {v0}, LX/Egv;-><init>()V

    iput-object v0, p0, LX/Egu;->c:LX/Egv;

    .line 2157408
    return-void
.end method

.method public static c(LX/1De;)LX/Egt;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2157419
    new-instance v1, LX/Egs;

    invoke-direct {v1}, LX/Egs;-><init>()V

    .line 2157420
    sget-object v2, LX/Egu;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Egt;

    .line 2157421
    if-nez v2, :cond_0

    .line 2157422
    new-instance v2, LX/Egt;

    invoke-direct {v2}, LX/Egt;-><init>()V

    .line 2157423
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Egt;->a$redex0(LX/Egt;LX/1De;IILX/Egs;)V

    .line 2157424
    move-object v1, v2

    .line 2157425
    move-object v0, v1

    .line 2157426
    return-object v0
.end method

.method public static declared-synchronized q()LX/Egu;
    .locals 2

    .prologue
    .line 2157415
    const-class v1, LX/Egu;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Egu;->a:LX/Egu;

    if-nez v0, :cond_0

    .line 2157416
    new-instance v0, LX/Egu;

    invoke-direct {v0}, LX/Egu;-><init>()V

    sput-object v0, LX/Egu;->a:LX/Egu;

    .line 2157417
    :cond_0
    sget-object v0, LX/Egu;->a:LX/Egu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2157418
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2157411
    check-cast p2, LX/Egs;

    .line 2157412
    iget-object v0, p2, LX/Egs;->a:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 p2, 0x0

    .line 2157413
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00e9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v2, v3}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0894

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0893

    invoke-interface {v2, v5, v3}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0e05ae

    invoke-static {p1, p2, v3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    sget-object v4, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v5, LX/0xr;->MEDIUM:LX/0xr;

    const/4 p0, 0x0

    invoke-static {p1, v4, v5, p0}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0892

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b0895

    invoke-interface {v1, p2, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    const v3, 0x7f0b0895

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2157414
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2157409
    invoke-static {}, LX/1dS;->b()V

    .line 2157410
    const/4 v0, 0x0

    return-object v0
.end method
