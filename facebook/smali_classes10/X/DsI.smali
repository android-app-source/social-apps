.class public final LX/DsI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/notifications/widget/NotificationsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/widget/NotificationsFragment;I)V
    .locals 0

    .prologue
    .line 2050334
    iput-object p1, p0, LX/DsI;->b:Lcom/facebook/notifications/widget/NotificationsFragment;

    iput p2, p0, LX/DsI;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2050335
    iget-object v0, p0, LX/DsI;->b:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0030

    const-string v2, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2050336
    iget-object v0, p0, LX/DsI;->b:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->L:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    .line 2050337
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2050338
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2050339
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2050340
    :goto_0
    iget-object v1, p0, LX/DsI;->b:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget v2, p0, LX/DsI;->a:I

    invoke-static {v1, v0, v2}, Lcom/facebook/notifications/widget/NotificationsFragment;->a$redex0(Lcom/facebook/notifications/widget/NotificationsFragment;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    .line 2050341
    return-void

    .line 2050342
    :cond_0
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method
