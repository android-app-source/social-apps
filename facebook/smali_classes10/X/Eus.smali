.class public LX/Eus;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/83X;


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field private final b:J

.field public final c:J

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:LX/2h7;

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public j:Z


# direct methods
.method public constructor <init>(LX/Euq;)V
    .locals 2

    .prologue
    .line 2180027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2180028
    iget-wide v0, p1, LX/Euq;->a:J

    iput-wide v0, p0, LX/Eus;->b:J

    .line 2180029
    iget-wide v0, p1, LX/Euq;->b:J

    iput-wide v0, p0, LX/Eus;->c:J

    .line 2180030
    iget-object v0, p1, LX/Euq;->c:Ljava/lang/String;

    iput-object v0, p0, LX/Eus;->d:Ljava/lang/String;

    .line 2180031
    iget-object v0, p1, LX/Euq;->d:Ljava/lang/String;

    iput-object v0, p0, LX/Eus;->e:Ljava/lang/String;

    .line 2180032
    iget v0, p1, LX/Euq;->e:I

    iput v0, p0, LX/Eus;->f:I

    .line 2180033
    iget-object v0, p1, LX/Euq;->f:LX/2h7;

    iput-object v0, p0, LX/Eus;->g:LX/2h7;

    .line 2180034
    iget-object v0, p1, LX/Euq;->g:Ljava/lang/String;

    iput-object v0, p0, LX/Eus;->h:Ljava/lang/String;

    .line 2180035
    iget-object v0, p1, LX/Euq;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/Eus;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/Eus;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2180036
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2180051
    iget-wide v0, p0, LX/Eus;->b:J

    return-wide v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 0

    .prologue
    .line 2180049
    iput-object p1, p0, LX/Eus;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2180050
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2180048
    iget-object v0, p0, LX/Eus;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 1

    .prologue
    .line 2180045
    iget-object v0, p0, LX/Eus;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/Eus;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2180046
    iput-object p1, p0, LX/Eus;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2180047
    return-void
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 2180044
    iget-object v0, p0, LX/Eus;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2180043
    iget-object v0, p0, LX/Eus;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 2180042
    iget v0, p0, LX/Eus;->f:I

    return v0
.end method

.method public final f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 2180041
    iget-object v0, p0, LX/Eus;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final g()LX/2h7;
    .locals 1

    .prologue
    .line 2180040
    iget-object v0, p0, LX/Eus;->g:LX/2h7;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2180039
    const/4 v0, 0x0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2180037
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Eus;->j:Z

    .line 2180038
    return-void
.end method
