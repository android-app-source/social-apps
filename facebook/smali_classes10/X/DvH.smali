.class public LX/DvH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DvM;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DvL;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DvJ;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DvI;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0sX;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DvM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DvL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DvJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DvI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0sX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2058141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2058142
    iput-object p1, p0, LX/DvH;->a:LX/0Ot;

    .line 2058143
    iput-object p2, p0, LX/DvH;->b:LX/0Ot;

    .line 2058144
    iput-object p3, p0, LX/DvH;->c:LX/0Ot;

    .line 2058145
    iput-object p4, p0, LX/DvH;->d:LX/0Ot;

    .line 2058146
    iput-object p5, p0, LX/DvH;->e:LX/0Ot;

    .line 2058147
    iput-object p6, p0, LX/DvH;->f:LX/0Ot;

    .line 2058148
    iput-object p7, p0, LX/DvH;->g:LX/0Ot;

    .line 2058149
    iput-object p8, p0, LX/DvH;->h:LX/0Ot;

    .line 2058150
    return-void
.end method

.method public static a(LX/0QB;)LX/DvH;
    .locals 12

    .prologue
    .line 2058130
    const-class v1, LX/DvH;

    monitor-enter v1

    .line 2058131
    :try_start_0
    sget-object v0, LX/DvH;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2058132
    sput-object v2, LX/DvH;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2058133
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2058134
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2058135
    new-instance v3, LX/DvH;

    const/16 v4, 0x140f

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xafd

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2e8f

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2e8e

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2e8d

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2e8c

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x60

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1032

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/DvH;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2058136
    move-object v0, v3

    .line 2058137
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2058138
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DvH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2058139
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2058140
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
