.class public LX/ECb;
.super Landroid/widget/ImageButton;
.source ""


# instance fields
.field public a:Landroid/content/res/ColorStateList;

.field public b:Landroid/content/res/ColorStateList;

.field public c:Landroid/graphics/drawable/Drawable;

.field public d:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2090249
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/ECb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2090250
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2090251
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/ECb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2090252
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2090253
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2090254
    sget-object v0, LX/03r;->GlyphLayerButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2090255
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, LX/ECb;->a:Landroid/content/res/ColorStateList;

    .line 2090256
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, LX/ECb;->b:Landroid/content/res/ColorStateList;

    .line 2090257
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/ECb;->c:Landroid/graphics/drawable/Drawable;

    .line 2090258
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, LX/ECb;->d:Landroid/graphics/drawable/Drawable;

    .line 2090259
    new-instance v1, LX/ECc;

    new-array v2, v4, [Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, LX/ECb;->d:Landroid/graphics/drawable/Drawable;

    aput-object v3, v2, v5

    iget-object v3, p0, LX/ECb;->c:Landroid/graphics/drawable/Drawable;

    aput-object v3, v2, v6

    new-array v3, v4, [Landroid/content/res/ColorStateList;

    iget-object v4, p0, LX/ECb;->b:Landroid/content/res/ColorStateList;

    aput-object v4, v3, v5

    iget-object v4, p0, LX/ECb;->a:Landroid/content/res/ColorStateList;

    aput-object v4, v3, v6

    invoke-direct {v1, v2, v3}, LX/ECc;-><init>([Landroid/graphics/drawable/Drawable;[Landroid/content/res/ColorStateList;)V

    .line 2090260
    invoke-virtual {p0, v1}, LX/ECb;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2090261
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2090262
    return-void
.end method
