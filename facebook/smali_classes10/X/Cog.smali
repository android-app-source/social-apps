.class public final LX/Cog;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cmm;


# instance fields
.field public final synthetic a:LX/Coj;


# direct methods
.method public constructor <init>(LX/Coj;)V
    .locals 0

    .prologue
    .line 1935946
    iput-object p1, p0, LX/Cog;->a:LX/Coj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/Cml;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1935947
    instance-of v0, p2, LX/Cn6;

    if-eqz v0, :cond_1

    .line 1935948
    iget-object v0, p0, LX/Cog;->a:LX/Coj;

    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d2a34

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1935949
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1935950
    check-cast p1, Landroid/view/ViewGroup;

    .line 1935951
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 1935952
    :goto_0
    return-void

    .line 1935953
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1935954
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1935955
    check-cast p2, LX/Cn6;

    .line 1935956
    iget-object v1, p2, LX/Cn6;->a:LX/Cmo;

    move-object v1, v1

    .line 1935957
    invoke-static {v0, v1, v0}, LX/Cn9;->a(Lcom/facebook/richdocument/view/widget/RichTextView;LX/Cmo;LX/Cmx;)V

    goto :goto_0

    .line 1935958
    :cond_1
    iget-object v0, p0, LX/Cog;->a:LX/Coj;

    iget-object v0, v0, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1935959
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1935960
    invoke-virtual {v0, v3, v3, v3, v3}, LX/CtG;->setPadding(IIII)V

    .line 1935961
    iget-object v0, p0, LX/Cog;->a:LX/Coj;

    iget-object v0, v0, LX/Coj;->b:LX/Ck0;

    iget-object v1, p0, LX/Cog;->a:LX/Coj;

    iget-object v1, v1, LX/Coi;->g:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v2, 0x7f0d011d

    const v4, 0x7f0d011d

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    goto :goto_0
.end method
