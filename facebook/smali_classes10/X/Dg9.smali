.class public final LX/Dg9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2029179
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 2029180
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2029181
    :goto_0
    return v1

    .line 2029182
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2029183
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 2029184
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2029185
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2029186
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2029187
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2029188
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2029189
    :cond_2
    const-string v6, "phones"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2029190
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2029191
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_3

    .line 2029192
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2029193
    const/4 v6, 0x0

    .line 2029194
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_b

    .line 2029195
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2029196
    :goto_3
    move v5, v6

    .line 2029197
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2029198
    :cond_3
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2029199
    goto :goto_1

    .line 2029200
    :cond_4
    const-string v6, "represented_profile"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2029201
    invoke-static {p0, p1}, LX/Dg8;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2029202
    :cond_5
    const-string v6, "structured_name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2029203
    invoke-static {p0, p1}, LX/3EF;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2029204
    :cond_6
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2029205
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2029206
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2029207
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2029208
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2029209
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 2029210
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2029211
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 2029212
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2029213
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2029214
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 2029215
    const-string v8, "primary_field"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2029216
    const/4 v7, 0x0

    .line 2029217
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_11

    .line 2029218
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2029219
    :goto_5
    move v5, v7

    .line 2029220
    goto :goto_4

    .line 2029221
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2029222
    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 2029223
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_b
    move v5, v6

    goto :goto_4

    .line 2029224
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2029225
    :cond_d
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_10

    .line 2029226
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2029227
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2029228
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_d

    if-eqz v9, :cond_d

    .line 2029229
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 2029230
    :cond_e
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_6

    .line 2029231
    :cond_f
    const-string v10, "phone"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 2029232
    invoke-static {p0, p1}, LX/6Mp;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_6

    .line 2029233
    :cond_10
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2029234
    invoke-virtual {p1, v7, v8}, LX/186;->b(II)V

    .line 2029235
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v5}, LX/186;->b(II)V

    .line 2029236
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_5

    :cond_11
    move v5, v7

    move v8, v7

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2029237
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2029238
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2029239
    if-eqz v0, :cond_0

    .line 2029240
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2029241
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2029242
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2029243
    if-eqz v0, :cond_5

    .line 2029244
    const-string v1, "phones"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2029245
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2029246
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 2029247
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 2029248
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2029249
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2029250
    if-eqz v3, :cond_3

    .line 2029251
    const-string v4, "primary_field"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2029252
    const/4 v2, 0x0

    .line 2029253
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2029254
    invoke-virtual {p0, v3, v2}, LX/15i;->g(II)I

    move-result v4

    .line 2029255
    if-eqz v4, :cond_1

    .line 2029256
    const-string v4, "__type__"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2029257
    invoke-static {p0, v3, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2029258
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2029259
    if-eqz v4, :cond_2

    .line 2029260
    const-string v2, "phone"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2029261
    invoke-static {p0, v4, p2}, LX/6Mp;->a(LX/15i;ILX/0nX;)V

    .line 2029262
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2029263
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2029264
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2029265
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2029266
    :cond_5
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2029267
    if-eqz v0, :cond_6

    .line 2029268
    const-string v1, "represented_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2029269
    invoke-static {p0, v0, p2, p3}, LX/Dg8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2029270
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2029271
    if-eqz v0, :cond_7

    .line 2029272
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2029273
    invoke-static {p0, v0, p2, p3}, LX/3EF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2029274
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2029275
    return-void
.end method
