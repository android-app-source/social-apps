.class public final LX/EAN;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/EAO;


# direct methods
.method public constructor <init>(LX/EAO;)V
    .locals 0

    .prologue
    .line 2085481
    iput-object p1, p0, LX/EAN;->b:LX/EAO;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2085482
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2085483
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2085484
    iget-object v1, p0, LX/EAN;->b:LX/EAO;

    .line 2085485
    invoke-virtual {v1, v0}, LX/1SX;->f(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 2085486
    if-eqz v1, :cond_0

    .line 2085487
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, p1, p2, v1}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 2085488
    :cond_0
    iget-object v1, p0, LX/EAN;->b:LX/EAO;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2085489
    iget-object v0, p0, LX/EAN;->b:LX/EAO;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2085490
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2085491
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2085492
    const v3, 0x7f08104c

    invoke-interface {p1, v3}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 2085493
    new-instance v4, LX/EAK;

    invoke-direct {v4, v0, v2, p2, v1}, LX/EAK;-><init>(LX/EAO;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2085494
    const v4, 0x7f020a0b

    move v4, v4

    .line 2085495
    invoke-virtual {v0, v3, v4, v2}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2085496
    :cond_1
    invoke-virtual {p0, p2}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2085497
    iget-object v0, p0, LX/EAN;->b:LX/EAO;

    .line 2085498
    invoke-virtual {v0, p1, p2, p3}, LX/1SX;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2085499
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2085500
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2085501
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2085502
    invoke-super {p0, p1}, LX/1wH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/EAN;->b:LX/EAO;

    .line 2085503
    invoke-virtual {v1, v0}, LX/1SX;->f(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 2085504
    if-nez v1, :cond_0

    iget-object v1, p0, LX/EAN;->b:LX/EAO;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
