.class public LX/EeB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2152088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152089
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2152090
    check-cast p1, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;

    .line 2152091
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2152092
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "cell"

    iget-object v2, p1, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152093
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "quiet"

    iget-boolean v0, p1, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152094
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "JSON"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2152095
    new-instance v0, LX/14N;

    const-string v1, "user_set_contact_info"

    const-string v2, "POST"

    const-string v3, "method/user.setContactInfo"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 2152096
    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2152097
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2152098
    invoke-virtual {v0}, LX/0lF;->p()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2152099
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Invalid response: boolean expected"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2152100
    :cond_0
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
