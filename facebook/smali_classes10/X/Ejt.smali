.class public final enum LX/Ejt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ejt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ejt;

.field public static final enum ADD:LX/Ejt;

.field public static final enum REMOVE:LX/Ejt;

.field public static final enum UPDATE:LX/Ejt;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2162788
    new-instance v0, LX/Ejt;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v2}, LX/Ejt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ejt;->ADD:LX/Ejt;

    .line 2162789
    new-instance v0, LX/Ejt;

    const-string v1, "UPDATE"

    invoke-direct {v0, v1, v3}, LX/Ejt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ejt;->UPDATE:LX/Ejt;

    .line 2162790
    new-instance v0, LX/Ejt;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v4}, LX/Ejt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ejt;->REMOVE:LX/Ejt;

    .line 2162791
    const/4 v0, 0x3

    new-array v0, v0, [LX/Ejt;

    sget-object v1, LX/Ejt;->ADD:LX/Ejt;

    aput-object v1, v0, v2

    sget-object v1, LX/Ejt;->UPDATE:LX/Ejt;

    aput-object v1, v0, v3

    sget-object v1, LX/Ejt;->REMOVE:LX/Ejt;

    aput-object v1, v0, v4

    sput-object v0, LX/Ejt;->$VALUES:[LX/Ejt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2162792
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ejt;
    .locals 1

    .prologue
    .line 2162793
    const-class v0, LX/Ejt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ejt;

    return-object v0
.end method

.method public static values()[LX/Ejt;
    .locals 1

    .prologue
    .line 2162794
    sget-object v0, LX/Ejt;->$VALUES:[LX/Ejt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ejt;

    return-object v0
.end method
