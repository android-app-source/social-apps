.class public LX/DOc;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/DOb;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992584
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1992585
    return-void
.end method


# virtual methods
.method public final a(LX/1Qj;)LX/DOb;
    .locals 52

    .prologue
    .line 1992586
    new-instance v2, LX/DOb;

    const/16 v3, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xbc6

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    const/16 v7, 0x236a

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lC;

    invoke-static/range {p0 .. p0}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v9

    check-cast v9, LX/1Sa;

    invoke-static/range {p0 .. p0}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v10

    check-cast v10, LX/1Sj;

    const/16 v11, 0x327a

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v12

    check-cast v12, LX/16H;

    const/16 v13, 0xde

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v14

    check-cast v14, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v15

    check-cast v15, LX/0bH;

    const/16 v16, 0x14a1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x149b

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v18

    check-cast v18, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v19

    check-cast v19, LX/17Q;

    const/16 v20, 0x2fd

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    const/16 v21, 0x15e7

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    const/16 v22, 0x12c4

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v23

    check-cast v23, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/DOl;->a(LX/0QB;)LX/DOl;

    move-result-object v24

    check-cast v24, LX/DOl;

    invoke-static/range {p0 .. p0}, LX/B0n;->a(LX/0QB;)LX/B0n;

    move-result-object v25

    check-cast v25, LX/B0n;

    invoke-static/range {p0 .. p0}, LX/1e0;->a(LX/0QB;)LX/1e0;

    move-result-object v26

    check-cast v26, LX/1e0;

    const-class v27, LX/00H;

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v27

    const/16 v28, 0x2fd7

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v28

    const/16 v29, 0x14a2

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v29

    const/16 v30, 0x31d5

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v30

    invoke-static/range {p0 .. p0}, LX/DOK;->a(LX/0QB;)LX/DOK;

    move-result-object v31

    check-cast v31, LX/DOK;

    invoke-static/range {p0 .. p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v32

    check-cast v32, LX/14w;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v33

    check-cast v33, LX/0ad;

    const/16 v34, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v34

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v35

    check-cast v35, LX/03V;

    const/16 v36, 0x1399

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v36

    invoke-static/range {p0 .. p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v38

    check-cast v38, LX/0qn;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v39

    check-cast v39, LX/0W3;

    const/16 v37, 0x31aa

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v40

    const/16 v37, 0x122d

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v41

    invoke-static/range {p0 .. p0}, LX/1Sl;->a(LX/0QB;)LX/1Sl;

    move-result-object v42

    check-cast v42, LX/1Sl;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v43

    check-cast v43, LX/0tX;

    const-class v37, LX/1Sm;

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v44

    check-cast v44, LX/1Sm;

    const/16 v37, 0x37e9

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v45

    const/16 v37, 0x132a

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v46

    const/16 v37, 0x132e

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v47

    invoke-static/range {p0 .. p0}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v48

    check-cast v48, LX/0wL;

    invoke-static/range {p0 .. p0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v49

    check-cast v49, LX/0pf;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v50

    check-cast v50, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/88k;->a(LX/0QB;)LX/88k;

    move-result-object v51

    check-cast v51, LX/88k;

    move-object/from16 v37, p1

    invoke-direct/range {v2 .. v51}, LX/DOb;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0Or;LX/0SG;LX/DOl;LX/B0n;LX/1e0;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/DOK;LX/14w;LX/0ad;LX/0Or;LX/03V;LX/0Or;LX/1Qj;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;LX/0pf;LX/0Uh;LX/88k;)V

    .line 1992587
    return-object v2
.end method
