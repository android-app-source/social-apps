.class public final enum LX/Diw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Diw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Diw;

.field public static final enum ADMIN_CANCEL:LX/Diw;

.field public static final enum ADMIN_DECLINE:LX/Diw;

.field public static final enum USER_CANCEL:LX/Diw;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2032712
    new-instance v0, LX/Diw;

    const-string v1, "ADMIN_CANCEL"

    invoke-direct {v0, v1, v2}, LX/Diw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Diw;->ADMIN_CANCEL:LX/Diw;

    .line 2032713
    new-instance v0, LX/Diw;

    const-string v1, "USER_CANCEL"

    invoke-direct {v0, v1, v3}, LX/Diw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Diw;->USER_CANCEL:LX/Diw;

    .line 2032714
    new-instance v0, LX/Diw;

    const-string v1, "ADMIN_DECLINE"

    invoke-direct {v0, v1, v4}, LX/Diw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Diw;->ADMIN_DECLINE:LX/Diw;

    .line 2032715
    const/4 v0, 0x3

    new-array v0, v0, [LX/Diw;

    sget-object v1, LX/Diw;->ADMIN_CANCEL:LX/Diw;

    aput-object v1, v0, v2

    sget-object v1, LX/Diw;->USER_CANCEL:LX/Diw;

    aput-object v1, v0, v3

    sget-object v1, LX/Diw;->ADMIN_DECLINE:LX/Diw;

    aput-object v1, v0, v4

    sput-object v0, LX/Diw;->$VALUES:[LX/Diw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2032716
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Diw;
    .locals 1

    .prologue
    .line 2032717
    const-class v0, LX/Diw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Diw;

    return-object v0
.end method

.method public static values()[LX/Diw;
    .locals 1

    .prologue
    .line 2032718
    sget-object v0, LX/Diw;->$VALUES:[LX/Diw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Diw;

    return-object v0
.end method
