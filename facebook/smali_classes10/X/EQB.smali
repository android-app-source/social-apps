.class public final LX/EQB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Cw5;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Sr;


# direct methods
.method public constructor <init>(LX/2Sr;)V
    .locals 0

    .prologue
    .line 2118336
    iput-object p1, p0, LX/EQB;->a:LX/2Sr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2118337
    iget-object v0, p0, LX/EQB;->a:LX/2Sr;

    invoke-static {v0}, LX/2Sr;->l(LX/2Sr;)V

    .line 2118338
    iget-object v0, p0, LX/EQB;->a:LX/2Sr;

    invoke-virtual {v0}, LX/2SP;->g()V

    .line 2118339
    iget-object v0, p0, LX/EQB;->a:LX/2Sr;

    iget-object v0, v0, LX/2Sr;->d:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_NULL_STATE_RECENT_SEARCHES_FAIL:LX/3Ql;

    invoke-virtual {v0, v1, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2118340
    iget-object v0, p0, LX/EQB;->a:LX/2Sr;

    const/4 v1, 0x0

    .line 2118341
    iput-object v1, v0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2118342
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2118343
    check-cast p1, LX/Cw5;

    .line 2118344
    iget-object v0, p0, LX/EQB;->a:LX/2Sr;

    invoke-static {v0}, LX/2Sr;->l(LX/2Sr;)V

    .line 2118345
    iget-object v0, p0, LX/EQB;->a:LX/2Sr;

    invoke-virtual {v0, p1}, LX/2SP;->a(LX/Cw5;)V

    .line 2118346
    iget-object v0, p0, LX/EQB;->a:LX/2Sr;

    .line 2118347
    iget-object v1, p1, LX/Cw5;->a:LX/0Px;

    move-object v1, v1

    .line 2118348
    invoke-static {v0, v1}, LX/2Sr;->b(LX/2Sr;LX/0Px;)V

    .line 2118349
    iget-object v0, p0, LX/EQB;->a:LX/2Sr;

    const/4 v1, 0x0

    .line 2118350
    iput-object v1, v0, LX/2Sr;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2118351
    return-void
.end method
