.class public final enum LX/ErX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ErX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ErX;

.field public static final enum ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

.field public static final enum ALL_CANDIDATES_SUGGESTED:LX/ErX;

.field public static final enum CONTACTS:LX/ErX;

.field public static final enum INVITE_SEARCH:LX/ErX;

.field public static final enum REVIEW:LX/ErX;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2173122
    new-instance v0, LX/ErX;

    const-string v1, "ALL_CANDIDATES_ALPHABETICAL"

    invoke-direct {v0, v1, v2}, LX/ErX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    .line 2173123
    new-instance v0, LX/ErX;

    const-string v1, "ALL_CANDIDATES_SUGGESTED"

    invoke-direct {v0, v1, v3}, LX/ErX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    .line 2173124
    new-instance v0, LX/ErX;

    const-string v1, "CONTACTS"

    invoke-direct {v0, v1, v4}, LX/ErX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ErX;->CONTACTS:LX/ErX;

    .line 2173125
    new-instance v0, LX/ErX;

    const-string v1, "INVITE_SEARCH"

    invoke-direct {v0, v1, v5}, LX/ErX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ErX;->INVITE_SEARCH:LX/ErX;

    .line 2173126
    new-instance v0, LX/ErX;

    const-string v1, "REVIEW"

    invoke-direct {v0, v1, v6}, LX/ErX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ErX;->REVIEW:LX/ErX;

    .line 2173127
    const/4 v0, 0x5

    new-array v0, v0, [LX/ErX;

    sget-object v1, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    aput-object v1, v0, v2

    sget-object v1, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    aput-object v1, v0, v3

    sget-object v1, LX/ErX;->CONTACTS:LX/ErX;

    aput-object v1, v0, v4

    sget-object v1, LX/ErX;->INVITE_SEARCH:LX/ErX;

    aput-object v1, v0, v5

    sget-object v1, LX/ErX;->REVIEW:LX/ErX;

    aput-object v1, v0, v6

    sput-object v0, LX/ErX;->$VALUES:[LX/ErX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2173128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ErX;
    .locals 1

    .prologue
    .line 2173129
    const-class v0, LX/ErX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ErX;

    return-object v0
.end method

.method public static values()[LX/ErX;
    .locals 1

    .prologue
    .line 2173130
    sget-object v0, LX/ErX;->$VALUES:[LX/ErX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ErX;

    return-object v0
.end method
