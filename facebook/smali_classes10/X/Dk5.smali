.class public final LX/Dk5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/ProfessionalservicesBookingRespondMutationsModels$NativeComponentFlowRequestStatusUpdateMutationFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dj0;

.field public final synthetic b:LX/DkN;


# direct methods
.method public constructor <init>(LX/DkN;LX/Dj0;)V
    .locals 0

    .prologue
    .line 2033936
    iput-object p1, p0, LX/Dk5;->b:LX/DkN;

    iput-object p2, p0, LX/Dk5;->a:LX/Dj0;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2033944
    iget-object v0, p0, LX/Dk5;->a:LX/Dj0;

    .line 2033945
    iget-object v1, v0, LX/Dj0;->a:LX/Dj1;

    iget-object v1, v1, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->k:LX/0kL;

    new-instance v2, LX/27k;

    iget-object p0, v0, LX/Dj0;->a:LX/Dj1;

    iget-object p0, p0, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    const p1, 0x7f08003c

    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2033946
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2033937
    iget-object v0, p0, LX/Dk5;->a:LX/Dj0;

    .line 2033938
    iget-object v1, v0, LX/Dj0;->a:LX/Dj1;

    iget-object v1, v1, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->o:Ljava/lang/String;

    const-string p0, "list"

    invoke-static {v1, p0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2033939
    iget-object v1, v0, LX/Dj0;->a:LX/Dj1;

    iget-object v1, v1, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object p0, v0, LX/Dj0;->a:LX/Dj1;

    iget-object p0, p0, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f082bb7

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {v1, p0, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2033940
    :cond_0
    iget-object v1, v0, LX/Dj0;->a:LX/Dj1;

    iget-object v1, v1, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2033941
    iget-object v1, v0, LX/Dj0;->a:LX/Dj1;

    iget-object v1, v1, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 p0, 0x2

    invoke-virtual {v1, p0}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2033942
    iget-object v1, v0, LX/Dj0;->a:LX/Dj1;

    iget-object v1, v1, LX/Dj1;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2033943
    :cond_1
    return-void
.end method
