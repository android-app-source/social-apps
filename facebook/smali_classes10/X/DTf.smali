.class public final LX/DTf;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V
    .locals 0

    .prologue
    .line 2000692
    iput-object p1, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2000667
    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->e$redex0(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V

    .line 2000668
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2000669
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2000670
    iget-object v1, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    .line 2000671
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2000672
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    .line 2000673
    iput-object v0, v1, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    .line 2000674
    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->m()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2000675
    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    .line 2000676
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2000677
    const-string v1, "group_visibility"

    iget-object v2, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    invoke-virtual {v2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->m()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000678
    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    .line 2000679
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2000680
    const-string v1, "group_url"

    iget-object v2, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    invoke-virtual {v2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000681
    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    .line 2000682
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2000683
    const-string v1, "group_admin_type"

    iget-object v2, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    invoke-virtual {v2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000684
    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    .line 2000685
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2000686
    const-string v2, "is_viewer_joined"

    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2000687
    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->e:LX/DZ7;

    iget-object v1, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v2, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->l:LX/DLO;

    invoke-interface {v0, v1, v2}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;LX/DLO;)V

    .line 2000688
    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->b(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V

    .line 2000689
    :goto_1
    return-void

    .line 2000690
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2000691
    :cond_1
    iget-object v0, p0, LX/DTf;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->e$redex0(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V

    goto :goto_1
.end method
