.class public final LX/Cr7;
.super LX/Cr5;
.source ""


# instance fields
.field private final g:I

.field private h:F

.field private i:Z


# direct methods
.method public constructor <init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;LX/Cqt;)V
    .locals 2

    .prologue
    .line 1940664
    invoke-direct/range {p0 .. p7}, LX/Cr5;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;LX/Cqt;)V

    .line 1940665
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, LX/Cr7;->h:F

    .line 1940666
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cr7;->i:Z

    .line 1940667
    invoke-interface {p2}, LX/Ctg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Cr7;->g:I

    .line 1940668
    return-void
.end method

.method public constructor <init>(LX/Ctg;)V
    .locals 2

    .prologue
    .line 1940655
    invoke-direct {p0, p1}, LX/Cr5;-><init>(LX/Ctg;)V

    .line 1940656
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, LX/Cr7;->h:F

    .line 1940657
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cr7;->i:Z

    .line 1940658
    invoke-interface {p1}, LX/Ctg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Cr7;->g:I

    .line 1940659
    return-void
.end method


# virtual methods
.method public final p()I
    .locals 3

    .prologue
    .line 1940660
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3faaaaab

    div-float v1, v0, v1

    .line 1940661
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, LX/Cr7;->h:F

    mul-float/2addr v2, v0

    iget-boolean v0, p0, LX/Cr7;->i:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/Cr7;->g:I

    :goto_0
    int-to-float v0, v0

    sub-float v0, v2, v0

    .line 1940662
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0

    .line 1940663
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
