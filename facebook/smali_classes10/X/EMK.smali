.class public LX/EMK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/CxV;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1vg;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1vg;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vg;",
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2110531
    iput-object p1, p0, LX/EMK;->a:LX/1vg;

    .line 2110532
    iput-object p2, p0, LX/EMK;->b:LX/0Ot;

    .line 2110533
    iput-object p3, p0, LX/EMK;->c:LX/0Ot;

    .line 2110534
    iput-object p4, p0, LX/EMK;->d:LX/0Ot;

    .line 2110535
    return-void
.end method

.method public static a(LX/0QB;)LX/EMK;
    .locals 7

    .prologue
    .line 2110536
    const-class v1, LX/EMK;

    monitor-enter v1

    .line 2110537
    :try_start_0
    sget-object v0, LX/EMK;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110538
    sput-object v2, LX/EMK;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110539
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110540
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110541
    new-instance v4, LX/EMK;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    const/16 v5, 0x329d

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32d4

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x12c4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, LX/EMK;-><init>(LX/1vg;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2110542
    move-object v0, v4

    .line 2110543
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110544
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EMK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110545
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110546
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/CzL;LX/CxA;Ljava/lang/String;Z)V
    .locals 11
    .param p1    # LX/CzL;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/CxA;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8dB;",
            ">;TE;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2110547
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2110548
    :cond_0
    :goto_0
    return-void

    .line 2110549
    :cond_1
    if-nez p4, :cond_2

    const/4 v2, 0x1

    .line 2110550
    :goto_1
    invoke-static {p1}, LX/CzN;->d(LX/CzL;)LX/CzL;

    move-result-object v5

    .line 2110551
    if-eq p1, v5, :cond_0

    .line 2110552
    invoke-interface {p2, p1, v5}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    move-object v0, p2

    .line 2110553
    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2110554
    iget-object v0, p0, LX/EMK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/5up;

    if-eqz v2, :cond_3

    sget-object v0, LX/5uo;->SAVE:LX/5uo;

    move-object v8, v0

    :goto_2
    const-string v9, "native_search"

    const-string v10, "toggle_button"

    new-instance v0, LX/EMJ;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p2

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, LX/EMJ;-><init>(LX/EMK;ZLjava/lang/String;LX/CxA;LX/CzL;LX/CzL;)V

    move-object v1, v7

    move-object v2, v8

    move-object v3, p3

    move-object v4, v9

    move-object v5, v10

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, LX/5up;->a(LX/5uo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    goto :goto_0

    .line 2110555
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 2110556
    :cond_3
    sget-object v0, LX/5uo;->UNSAVE:LX/5uo;

    move-object v8, v0

    goto :goto_2
.end method
