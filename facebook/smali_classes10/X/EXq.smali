.class public final LX/EXq;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EXp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EXq;",
        ">;",
        "LX/EXp;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:Ljava/lang/Object;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EXj;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EXj;",
            "LX/EXi;",
            "LX/EXh;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/EXv;

.field public f:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/EXv;",
            "LX/EXu;",
            "LX/EXt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2135408
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2135409
    const-string v0, ""

    iput-object v0, p0, LX/EXq;->b:Ljava/lang/Object;

    .line 2135410
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXq;->c:Ljava/util/List;

    .line 2135411
    sget-object v0, LX/EXv;->c:LX/EXv;

    move-object v0, v0

    .line 2135412
    iput-object v0, p0, LX/EXq;->e:LX/EXv;

    .line 2135413
    invoke-direct {p0}, LX/EXq;->m()V

    .line 2135414
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2135415
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2135416
    const-string v0, ""

    iput-object v0, p0, LX/EXq;->b:Ljava/lang/Object;

    .line 2135417
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXq;->c:Ljava/util/List;

    .line 2135418
    sget-object v0, LX/EXv;->c:LX/EXv;

    move-object v0, v0

    .line 2135419
    iput-object v0, p0, LX/EXq;->e:LX/EXv;

    .line 2135420
    invoke-direct {p0}, LX/EXq;->m()V

    .line 2135421
    return-void
.end method

.method private B()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EXj;",
            "LX/EXi;",
            "LX/EXh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2135422
    iget-object v0, p0, LX/EXq;->d:LX/EZ2;

    if-nez v0, :cond_0

    .line 2135423
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EXq;->c:Ljava/util/List;

    iget v0, p0, LX/EXq;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2135424
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2135425
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EXq;->d:LX/EZ2;

    .line 2135426
    const/4 v0, 0x0

    iput-object v0, p0, LX/EXq;->c:Ljava/util/List;

    .line 2135427
    :cond_0
    iget-object v0, p0, LX/EXq;->d:LX/EZ2;

    return-object v0

    .line 2135428
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/EXq;
    .locals 1

    .prologue
    .line 2135429
    instance-of v0, p1, LX/EXr;

    if-eqz v0, :cond_0

    .line 2135430
    check-cast p1, LX/EXr;

    invoke-virtual {p0, p1}, LX/EXq;->a(LX/EXr;)LX/EXq;

    move-result-object p0

    .line 2135431
    :goto_0
    return-object p0

    .line 2135432
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EXq;
    .locals 4

    .prologue
    .line 2135433
    const/4 v2, 0x0

    .line 2135434
    :try_start_0
    sget-object v0, LX/EXr;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXr;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2135435
    if-eqz v0, :cond_0

    .line 2135436
    invoke-virtual {p0, v0}, LX/EXq;->a(LX/EXr;)LX/EXq;

    .line 2135437
    :cond_0
    return-object p0

    .line 2135438
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2135439
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2135440
    check-cast v0, LX/EXr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2135441
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2135442
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2135443
    invoke-virtual {p0, v1}, LX/EXq;->a(LX/EXr;)LX/EXq;

    :cond_1
    throw v0

    .line 2135444
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 2135445
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2135446
    invoke-direct {p0}, LX/EXq;->B()LX/EZ2;

    .line 2135447
    iget-object v0, p0, LX/EXq;->f:LX/EZ7;

    if-nez v0, :cond_0

    .line 2135448
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/EXq;->e:LX/EXv;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2135449
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2135450
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/EXq;->f:LX/EZ7;

    .line 2135451
    const/4 v0, 0x0

    iput-object v0, p0, LX/EXq;->e:LX/EXv;

    .line 2135452
    :cond_0
    return-void
.end method

.method public static n()LX/EXq;
    .locals 1

    .prologue
    .line 2135453
    new-instance v0, LX/EXq;

    invoke-direct {v0}, LX/EXq;-><init>()V

    return-object v0
.end method

.method private u()LX/EXq;
    .locals 2

    .prologue
    .line 2135524
    invoke-static {}, LX/EXq;->n()LX/EXq;

    move-result-object v0

    invoke-direct {p0}, LX/EXq;->y()LX/EXr;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EXq;->a(LX/EXr;)LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method private x()LX/EXr;
    .locals 2

    .prologue
    .line 2135454
    invoke-direct {p0}, LX/EXq;->y()LX/EXr;

    move-result-object v0

    .line 2135455
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2135456
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2135457
    :cond_0
    return-object v0
.end method

.method private y()LX/EXr;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2135458
    new-instance v2, LX/EXr;

    invoke-direct {v2, p0}, LX/EXr;-><init>(LX/EWj;)V

    .line 2135459
    iget v3, p0, LX/EXq;->a:I

    .line 2135460
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 2135461
    :goto_0
    iget-object v1, p0, LX/EXq;->b:Ljava/lang/Object;

    .line 2135462
    iput-object v1, v2, LX/EXr;->name_:Ljava/lang/Object;

    .line 2135463
    iget-object v1, p0, LX/EXq;->d:LX/EZ2;

    if-nez v1, :cond_1

    .line 2135464
    iget v1, p0, LX/EXq;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2135465
    iget-object v1, p0, LX/EXq;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXq;->c:Ljava/util/List;

    .line 2135466
    iget v1, p0, LX/EXq;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, LX/EXq;->a:I

    .line 2135467
    :cond_0
    iget-object v1, p0, LX/EXq;->c:Ljava/util/List;

    .line 2135468
    iput-object v1, v2, LX/EXr;->method_:Ljava/util/List;

    .line 2135469
    :goto_1
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_3

    .line 2135470
    or-int/lit8 v0, v0, 0x2

    move v1, v0

    .line 2135471
    :goto_2
    iget-object v0, p0, LX/EXq;->f:LX/EZ7;

    if-nez v0, :cond_2

    .line 2135472
    iget-object v0, p0, LX/EXq;->e:LX/EXv;

    .line 2135473
    iput-object v0, v2, LX/EXr;->options_:LX/EXv;

    .line 2135474
    :goto_3
    iput v1, v2, LX/EXr;->bitField0_:I

    .line 2135475
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2135476
    return-object v2

    .line 2135477
    :cond_1
    iget-object v1, p0, LX/EXq;->d:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2135478
    iput-object v1, v2, LX/EXr;->method_:Ljava/util/List;

    .line 2135479
    goto :goto_1

    .line 2135480
    :cond_2
    iget-object v0, p0, LX/EXq;->f:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EXv;

    .line 2135481
    iput-object v0, v2, LX/EXr;->options_:LX/EXv;

    .line 2135482
    goto :goto_3

    :cond_3
    move v1, v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2135483
    invoke-direct {p0, p1}, LX/EXq;->d(LX/EWY;)LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2135484
    invoke-direct {p0, p1, p2}, LX/EXq;->d(LX/EWd;LX/EYZ;)LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EXr;)LX/EXq;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2135485
    sget-object v1, LX/EXr;->c:LX/EXr;

    move-object v1, v1

    .line 2135486
    if-ne p1, v1, :cond_0

    .line 2135487
    :goto_0
    return-object p0

    .line 2135488
    :cond_0
    const/4 v1, 0x1

    .line 2135489
    iget v2, p1, LX/EXr;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_9

    :goto_1
    move v1, v1

    .line 2135490
    if-eqz v1, :cond_1

    .line 2135491
    iget v1, p0, LX/EXq;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/EXq;->a:I

    .line 2135492
    iget-object v1, p1, LX/EXr;->name_:Ljava/lang/Object;

    iput-object v1, p0, LX/EXq;->b:Ljava/lang/Object;

    .line 2135493
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2135494
    :cond_1
    iget-object v1, p0, LX/EXq;->d:LX/EZ2;

    if-nez v1, :cond_6

    .line 2135495
    iget-object v0, p1, LX/EXr;->method_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2135496
    iget-object v0, p0, LX/EXq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2135497
    iget-object v0, p1, LX/EXr;->method_:Ljava/util/List;

    iput-object v0, p0, LX/EXq;->c:Ljava/util/List;

    .line 2135498
    iget v0, p0, LX/EXq;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, LX/EXq;->a:I

    .line 2135499
    :goto_2
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2135500
    :cond_2
    :goto_3
    invoke-virtual {p1}, LX/EXr;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2135501
    iget-object v0, p1, LX/EXr;->options_:LX/EXv;

    move-object v0, v0

    .line 2135502
    iget-object v1, p0, LX/EXq;->f:LX/EZ7;

    if-nez v1, :cond_b

    .line 2135503
    iget v1, p0, LX/EXq;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_a

    iget-object v1, p0, LX/EXq;->e:LX/EXv;

    .line 2135504
    sget-object v2, LX/EXv;->c:LX/EXv;

    move-object v2, v2

    .line 2135505
    if-eq v1, v2, :cond_a

    .line 2135506
    iget-object v1, p0, LX/EXq;->e:LX/EXv;

    invoke-static {v1}, LX/EXv;->a(LX/EXv;)LX/EXu;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/EXu;->a(LX/EXv;)LX/EXu;

    move-result-object v1

    invoke-virtual {v1}, LX/EXu;->l()LX/EXv;

    move-result-object v1

    iput-object v1, p0, LX/EXq;->e:LX/EXv;

    .line 2135507
    :goto_4
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2135508
    :goto_5
    iget v1, p0, LX/EXq;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, LX/EXq;->a:I

    .line 2135509
    :cond_3
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    .line 2135510
    :cond_4
    iget v0, p0, LX/EXq;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_5

    .line 2135511
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EXq;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EXq;->c:Ljava/util/List;

    .line 2135512
    iget v0, p0, LX/EXq;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EXq;->a:I

    .line 2135513
    :cond_5
    iget-object v0, p0, LX/EXq;->c:Ljava/util/List;

    iget-object v1, p1, LX/EXr;->method_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 2135514
    :cond_6
    iget-object v1, p1, LX/EXr;->method_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2135515
    iget-object v1, p0, LX/EXq;->d:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2135516
    iget-object v1, p0, LX/EXq;->d:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2135517
    iput-object v0, p0, LX/EXq;->d:LX/EZ2;

    .line 2135518
    iget-object v1, p1, LX/EXr;->method_:Ljava/util/List;

    iput-object v1, p0, LX/EXq;->c:Ljava/util/List;

    .line 2135519
    iget v1, p0, LX/EXq;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, LX/EXq;->a:I

    .line 2135520
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_7

    invoke-direct {p0}, LX/EXq;->B()LX/EZ2;

    move-result-object v0

    :cond_7
    iput-object v0, p0, LX/EXq;->d:LX/EZ2;

    goto/16 :goto_3

    .line 2135521
    :cond_8
    iget-object v0, p0, LX/EXq;->d:LX/EZ2;

    iget-object v1, p1, LX/EXr;->method_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto/16 :goto_3

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 2135522
    :cond_a
    iput-object v0, p0, LX/EXq;->e:LX/EXv;

    goto :goto_4

    .line 2135523
    :cond_b
    iget-object v1, p0, LX/EXq;->f:LX/EZ7;

    invoke-virtual {v1, v0}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto :goto_5
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2135389
    move v0, v1

    .line 2135390
    :goto_0
    iget-object v2, p0, LX/EXq;->d:LX/EZ2;

    if-nez v2, :cond_4

    .line 2135391
    iget-object v2, p0, LX/EXq;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2135392
    :goto_1
    move v2, v2

    .line 2135393
    if-ge v0, v2, :cond_2

    .line 2135394
    iget-object v2, p0, LX/EXq;->d:LX/EZ2;

    if-nez v2, :cond_5

    .line 2135395
    iget-object v2, p0, LX/EXq;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EXj;

    .line 2135396
    :goto_2
    move-object v2, v2

    .line 2135397
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2135398
    :cond_0
    :goto_3
    return v1

    .line 2135399
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2135400
    :cond_2
    iget v0, p0, LX/EXq;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_6

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2135401
    if-eqz v0, :cond_3

    .line 2135402
    iget-object v0, p0, LX/EXq;->f:LX/EZ7;

    if-nez v0, :cond_7

    .line 2135403
    iget-object v0, p0, LX/EXq;->e:LX/EXv;

    .line 2135404
    :goto_5
    move-object v0, v0

    .line 2135405
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2135406
    :cond_3
    const/4 v1, 0x1

    goto :goto_3

    :cond_4
    iget-object v2, p0, LX/EXq;->d:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_1

    :cond_5
    iget-object v2, p0, LX/EXq;->d:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EXj;

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    :cond_7
    iget-object v0, p0, LX/EXq;->f:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->c()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EXv;

    goto :goto_5
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2135407
    invoke-direct {p0, p1, p2}, LX/EXq;->d(LX/EWd;LX/EYZ;)LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2135375
    invoke-direct {p0}, LX/EXq;->u()LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2135376
    invoke-direct {p0, p1, p2}, LX/EXq;->d(LX/EWd;LX/EYZ;)LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2135377
    invoke-direct {p0}, LX/EXq;->u()LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2135378
    invoke-direct {p0, p1}, LX/EXq;->d(LX/EWY;)LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2135379
    invoke-direct {p0}, LX/EXq;->u()LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2135380
    sget-object v0, LX/EYC;->p:LX/EYn;

    const-class v1, LX/EXr;

    const-class v2, LX/EXq;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2135381
    sget-object v0, LX/EYC;->o:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2135382
    invoke-direct {p0}, LX/EXq;->u()LX/EXq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2135383
    invoke-direct {p0}, LX/EXq;->y()LX/EXr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2135384
    invoke-direct {p0}, LX/EXq;->x()LX/EXr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2135385
    invoke-direct {p0}, LX/EXq;->y()LX/EXr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2135386
    invoke-direct {p0}, LX/EXq;->x()LX/EXr;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2135387
    sget-object v0, LX/EXr;->c:LX/EXr;

    move-object v0, v0

    .line 2135388
    return-object v0
.end method
