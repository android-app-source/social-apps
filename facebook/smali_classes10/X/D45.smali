.class public final LX/D45;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field public final synthetic a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;


# direct methods
.method public constructor <init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V
    .locals 0

    .prologue
    .line 1961550
    iput-object p1, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;B)V
    .locals 0

    .prologue
    .line 1961571
    invoke-direct {p0, p1}, LX/D45;-><init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1961551
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1961552
    :goto_0
    return v3

    .line 1961553
    :pswitch_0
    iget-object v0, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    const-string v1, "media_play"

    invoke-static {v0, v1}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;Ljava/lang/String;)V

    .line 1961554
    iget-object v0, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    iget-object v0, v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    .line 1961555
    iput-boolean v3, v0, LX/D3y;->j:Z

    .line 1961556
    move-object v0, v0

    .line 1961557
    const/4 v1, 0x0

    .line 1961558
    iput-object v1, v0, LX/D3y;->e:Ljava/lang/CharSequence;

    .line 1961559
    iget-object v0, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-static {v0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->f(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    goto :goto_0

    .line 1961560
    :pswitch_1
    iget-object v0, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    const-string v1, "media_pause"

    invoke-static {v0, v1}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;Ljava/lang/String;)V

    .line 1961561
    iget-object v0, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    iget-object v0, v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->r:LX/D3y;

    iget-object v1, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    const v2, 0x7f0828b7

    invoke-virtual {v1, v2}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1961562
    iput-object v1, v0, LX/D3y;->e:Ljava/lang/CharSequence;

    .line 1961563
    move-object v0, v0

    .line 1961564
    const/4 v1, 0x0

    .line 1961565
    iput-boolean v1, v0, LX/D3y;->j:Z

    .line 1961566
    iget-object v0, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-static {v0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->f(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    goto :goto_0

    .line 1961567
    :pswitch_2
    iget-object v0, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    const-string v1, "media_complete"

    invoke-static {v0, v1}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;Ljava/lang/String;)V

    .line 1961568
    iget-object v0, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-static {v0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->i(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    goto :goto_0

    .line 1961569
    :pswitch_3
    iget-object v0, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    const-string v1, "media_error"

    invoke-static {v0, v1}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->b(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;Ljava/lang/String;)V

    .line 1961570
    iget-object v0, p0, LX/D45;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-static {v0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->i(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
