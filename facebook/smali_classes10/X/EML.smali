.class public final LX/EML;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8d1;

.field public final synthetic b:LX/EMN;


# direct methods
.method public constructor <init>(LX/EMN;LX/8d1;)V
    .locals 0

    .prologue
    .line 2110584
    iput-object p1, p0, LX/EML;->b:LX/EMN;

    iput-object p2, p0, LX/EML;->a:LX/8d1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2110585
    iget-object v0, p0, LX/EML;->b:LX/EMN;

    iget-object v0, v0, LX/EMN;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    iget-object v1, p0, LX/EML;->a:LX/8d1;

    invoke-interface {v1}, LX/8d1;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iget-object v2, p0, LX/EML;->a:LX/8d1;

    invoke-interface {v2}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/CyD;->a(LX/2dj;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
