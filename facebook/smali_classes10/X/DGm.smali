.class public LX/DGm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:I

.field public final d:I

.field public final e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;IIZ)V"
        }
    .end annotation

    .prologue
    .line 1980589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1980590
    iput p2, p0, LX/DGm;->c:I

    .line 1980591
    iput-object p1, p0, LX/DGm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980592
    iput p3, p0, LX/DGm;->d:I

    .line 1980593
    iput-boolean p4, p0, LX/DGm;->e:Z

    .line 1980594
    iget-object v0, p0, LX/DGm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980595
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1980596
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    .line 1980597
    iget v1, p0, LX/DGm;->c:I

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v1, p0, LX/DGm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v2, p0, LX/DGm;->c:I

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980598
    return-void

    .line 1980599
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
