.class public LX/Dxn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0zG;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2063288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2063289
    return-void
.end method

.method public static a(LX/0QB;)LX/Dxn;
    .locals 3

    .prologue
    .line 2063290
    const-class v1, LX/Dxn;

    monitor-enter v1

    .line 2063291
    :try_start_0
    sget-object v0, LX/Dxn;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2063292
    sput-object v2, LX/Dxn;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2063293
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2063294
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2063295
    new-instance v0, LX/Dxn;

    invoke-direct {v0}, LX/Dxn;-><init>()V

    .line 2063296
    move-object v0, v0

    .line 2063297
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2063298
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Dxn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2063299
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2063300
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2063301
    iget-object v0, p0, LX/Dxn;->a:LX/0h5;

    move-object v0, v0

    .line 2063302
    return-object v0
.end method
