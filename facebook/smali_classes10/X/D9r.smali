.class public LX/D9r;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/audio/DeviceHasEarpiece;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/audio/DeviceDoesNotHaveEarpiece;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1970630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970631
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/D9r;->a:Z

    .line 1970632
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/D9r;->b:Z

    .line 1970633
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/D9r;->c:Z

    .line 1970634
    return-void
.end method

.method public static a(LX/0QB;)LX/D9r;
    .locals 6

    .prologue
    .line 1970635
    new-instance v3, LX/D9r;

    invoke-static {p0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1970636
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v2, 0x52

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v1, v1

    .line 1970637
    check-cast v1, Ljava/lang/Boolean;

    .line 1970638
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    const/16 v4, 0x51

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object v2, v2

    .line 1970639
    check-cast v2, Ljava/lang/Boolean;

    invoke-direct {v3, v0, v1, v2}, LX/D9r;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1970640
    move-object v0, v3

    .line 1970641
    return-object v0
.end method
