.class public final LX/Dqk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:J

.field public final synthetic c:Ljava/lang/Runnable;

.field public final synthetic d:Lcom/facebook/notifications/service/FriendRequestNotificationService;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/service/FriendRequestNotificationService;Ljava/lang/String;JLjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 2048495
    iput-object p1, p0, LX/Dqk;->d:Lcom/facebook/notifications/service/FriendRequestNotificationService;

    iput-object p2, p0, LX/Dqk;->a:Ljava/lang/String;

    iput-wide p3, p0, LX/Dqk;->b:J

    iput-object p5, p0, LX/Dqk;->c:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 5
    .param p1    # Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2048497
    iget-object v0, p0, LX/Dqk;->d:Lcom/facebook/notifications/service/FriendRequestNotificationService;

    iget-object v0, v0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a:LX/2c4;

    iget-object v1, p0, LX/Dqk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, LX/2c4;->a(Ljava/lang/String;I)V

    .line 2048498
    if-eqz p1, :cond_0

    .line 2048499
    iget-object v0, p0, LX/Dqk;->d:Lcom/facebook/notifications/service/FriendRequestNotificationService;

    iget-object v0, v0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->b:LX/2do;

    new-instance v1, LX/2f2;

    iget-wide v2, p0, LX/Dqk;->b:J

    invoke-direct {v1, v2, v3, p1, v4}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2048500
    iget-object v0, p0, LX/Dqk;->c:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2048501
    iget-object v0, p0, LX/Dqk;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2048502
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2048503
    iget-object v0, p0, LX/Dqk;->d:Lcom/facebook/notifications/service/FriendRequestNotificationService;

    iget-object v0, v0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a:LX/2c4;

    iget-object v1, p0, LX/Dqk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, LX/2c4;->a(Ljava/lang/String;I)V

    .line 2048504
    iget-object v0, p0, LX/Dqk;->d:Lcom/facebook/notifications/service/FriendRequestNotificationService;

    iget-object v0, v0, Lcom/facebook/notifications/service/FriendRequestNotificationService;->b:LX/2do;

    new-instance v1, LX/2f2;

    iget-wide v2, p0, LX/Dqk;->b:J

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v1, v2, v3, v4, v5}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2048505
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048496
    check-cast p1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p1}, LX/Dqk;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    return-void
.end method
