.class public LX/DVn;
.super LX/B1d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B1d",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/String;LX/0tX;LX/B1b;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/B1b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2005800
    invoke-direct {p0, p1, p3, p4}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;)V

    .line 2005801
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005802
    iput-object v0, p0, LX/DVn;->e:LX/0Px;

    .line 2005803
    iput-object p2, p0, LX/DVn;->f:Ljava/lang/String;

    .line 2005804
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0gW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005795
    new-instance v0, LX/DWx;

    invoke-direct {v0}, LX/DWx;-><init>()V

    move-object v0, v0

    .line 2005796
    const-string v1, "user_id"

    iget-object v2, p0, LX/DVn;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005797
    const-string v1, "member_count_to_fetch"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2005798
    const-string v1, "afterCursor"

    iget-object v2, p0, LX/B1d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005799
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2005777
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2005778
    iget-object v0, p0, LX/DVn;->e:LX/0Px;

    invoke-virtual {v5, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2005779
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005780
    if-eqz v0, :cond_3

    .line 2005781
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005782
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2005783
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005784
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel;

    move-result-object v6

    .line 2005785
    invoke-virtual {v6}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v2, v1

    :goto_0
    if-ge v2, v8, :cond_0

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel;

    .line 2005786
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel;->a()Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {v0, v3}, LX/DVf;->a(LX/DWH;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005787
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2005788
    :cond_0
    invoke-virtual {v6}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2005789
    :goto_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iput-object v5, p0, LX/DVn;->e:LX/0Px;

    .line 2005790
    if-eqz v0, :cond_1

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iput-object v3, p0, LX/DVn;->a:Ljava/lang/String;

    .line 2005791
    if-eqz v0, :cond_2

    invoke-virtual {v2, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, LX/DVn;->b:Z

    .line 2005792
    invoke-virtual {p0}, LX/B1d;->g()V

    .line 2005793
    return-void

    :cond_2
    move v1, v4

    .line 2005794
    goto :goto_2

    :cond_3
    move v0, v1

    move-object v2, v3

    goto :goto_1
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2005776
    const-string v0, "User friends fetch failed"

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005775
    iget-object v0, p0, LX/DVn;->e:LX/0Px;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 2005772
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005773
    iput-object v0, p0, LX/DVn;->e:LX/0Px;

    .line 2005774
    return-void
.end method
