.class public LX/Dp4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile l:LX/Dp4;


# instance fields
.field private final b:LX/Dp6;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/crypto/CryptoSessionStorage;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DoJ;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

.field public final f:Lcom/facebook/messaging/tincan/outbound/Sender;

.field public final g:LX/IuF;

.field private final h:LX/2Ox;

.field private final i:LX/2P3;

.field public final j:LX/2PE;

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2043091
    const-class v0, LX/Dp4;

    sput-object v0, LX/Dp4;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/Dp6;LX/0Or;LX/0Or;Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Lcom/facebook/messaging/tincan/outbound/Sender;LX/IuF;LX/2Ox;LX/2P3;LX/2PE;LX/0Or;)V
    .locals 0
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Dp6;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/crypto/CryptoSessionStorage;",
            ">;",
            "LX/0Or",
            "<",
            "LX/DoJ;",
            ">;",
            "Lcom/facebook/messaging/tincan/inbound/MessageHandler;",
            "Lcom/facebook/messaging/tincan/outbound/Sender;",
            "Lcom/facebook/messaging/tincan/inbound/PacketIdFactory;",
            "LX/2Ox;",
            "LX/2P3;",
            "LX/2PE;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2043092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2043093
    iput-object p1, p0, LX/Dp4;->b:LX/Dp6;

    .line 2043094
    iput-object p2, p0, LX/Dp4;->c:LX/0Or;

    .line 2043095
    iput-object p3, p0, LX/Dp4;->d:LX/0Or;

    .line 2043096
    iput-object p4, p0, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    .line 2043097
    iput-object p5, p0, LX/Dp4;->f:Lcom/facebook/messaging/tincan/outbound/Sender;

    .line 2043098
    iput-object p6, p0, LX/Dp4;->g:LX/IuF;

    .line 2043099
    iput-object p7, p0, LX/Dp4;->h:LX/2Ox;

    .line 2043100
    iput-object p8, p0, LX/Dp4;->i:LX/2P3;

    .line 2043101
    iput-object p9, p0, LX/Dp4;->j:LX/2PE;

    .line 2043102
    iput-object p10, p0, LX/Dp4;->k:LX/0Or;

    .line 2043103
    return-void
.end method

.method public static a(LX/0QB;)LX/Dp4;
    .locals 14

    .prologue
    .line 2043104
    sget-object v0, LX/Dp4;->l:LX/Dp4;

    if-nez v0, :cond_1

    .line 2043105
    const-class v1, LX/Dp4;

    monitor-enter v1

    .line 2043106
    :try_start_0
    sget-object v0, LX/Dp4;->l:LX/Dp4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2043107
    if-eqz v2, :cond_0

    .line 2043108
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2043109
    new-instance v3, LX/Dp4;

    invoke-static {v0}, LX/Dp6;->a(LX/0QB;)LX/Dp6;

    move-result-object v4

    check-cast v4, LX/Dp6;

    const/16 v5, 0x2a2d

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2a0c

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-static {v0}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(LX/0QB;)Lcom/facebook/messaging/tincan/outbound/Sender;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/tincan/outbound/Sender;

    invoke-static {v0}, LX/IuF;->a(LX/0QB;)LX/IuF;

    move-result-object v9

    check-cast v9, LX/IuF;

    invoke-static {v0}, LX/2Ox;->a(LX/0QB;)LX/2Ox;

    move-result-object v10

    check-cast v10, LX/2Ox;

    invoke-static {v0}, LX/2P3;->a(LX/0QB;)LX/2P3;

    move-result-object v11

    check-cast v11, LX/2P3;

    invoke-static {v0}, LX/2PE;->a(LX/0QB;)LX/2PE;

    move-result-object v12

    check-cast v12, LX/2PE;

    const/16 v13, 0x15e7

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/Dp4;-><init>(LX/Dp6;LX/0Or;LX/0Or;Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Lcom/facebook/messaging/tincan/outbound/Sender;LX/IuF;LX/2Ox;LX/2P3;LX/2PE;LX/0Or;)V

    .line 2043110
    move-object v0, v3

    .line 2043111
    sput-object v0, LX/Dp4;->l:LX/Dp4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2043112
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2043113
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2043114
    :cond_1
    sget-object v0, LX/Dp4;->l:LX/Dp4;

    return-object v0

    .line 2043115
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2043116
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Dp4;ILX/DpN;)V
    .locals 6

    .prologue
    .line 2043117
    iget-object v0, p0, LX/Dp4;->f:Lcom/facebook/messaging/tincan/outbound/Sender;

    iget-object v1, p2, LX/DpN;->msg_from:LX/DpM;

    iget-object v2, p2, LX/DpN;->version:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p2, LX/DpN;->nonce:[B

    iget-object v2, p0, LX/Dp4;->g:LX/IuF;

    invoke-virtual {v2}, LX/IuF;->a()[B

    move-result-object v5

    move v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(LX/DpM;II[B[B)V

    .line 2043118
    return-void
.end method

.method private static a(LX/Dp4;LX/Dp5;)V
    .locals 13

    .prologue
    .line 2043119
    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->body:LX/DpO;

    invoke-virtual {v0}, LX/DpO;->g()LX/Dpa;

    move-result-object v0

    .line 2043120
    iget-object v1, v0, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2043121
    const/16 v9, 0x1b58

    .line 2043122
    iget-object v2, p1, LX/Dp5;->b:LX/DpN;

    iget-object v2, v2, LX/DpN;->msg_from:LX/DpM;

    iget-object v2, v2, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p1, LX/Dp5;->b:LX/DpN;

    iget-object v4, v4, LX/DpN;->msg_from:LX/DpM;

    iget-object v4, v4, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-static {v2, v3, v4}, LX/DoM;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2043123
    iget-object v2, p1, LX/Dp5;->b:LX/DpN;

    iget-object v2, v2, LX/DpN;->thread_fbid:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, LX/Dp4;->k:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v6, p1, LX/Dp5;->b:LX/DpN;

    iget-object v6, v6, LX/DpN;->msg_from:LX/DpM;

    iget-object v6, v6, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static/range {v2 .. v7}, LX/Dpq;->a(JJJ)J

    move-result-wide v2

    .line 2043124
    new-instance v4, LX/Eay;

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Eap;

    const/4 v5, 0x0

    invoke-direct {v3, v8, v5}, LX/Eap;-><init>(Ljava/lang/String;I)V

    invoke-direct {v4, v2, v3}, LX/Eay;-><init>(Ljava/lang/String;LX/Eap;)V

    .line 2043125
    :try_start_0
    new-instance v2, LX/Eaw;

    iget-object v3, p0, LX/Dp4;->j:LX/2PE;

    invoke-direct {v2, v3, v4}, LX/Eaw;-><init>(LX/2PE;LX/Eay;)V

    .line 2043126
    iget-object v3, v0, LX/Dpa;->serialized_salamander:[B

    .line 2043127
    new-instance v4, LX/Eav;

    const/4 v5, 0x0

    invoke-direct {v4}, LX/Eav;-><init>()V

    invoke-static {v2, v3, v4}, LX/Eaw;->a(LX/Eaw;[BLX/Eac;)[B

    move-result-object v4

    move-object v2, v4
    :try_end_0
    .catch LX/Eal; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/Eak; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/Eai; {:try_start_0 .. :try_end_0} :catch_3
    .catch LX/Ead; {:try_start_0 .. :try_end_0} :catch_2

    .line 2043128
    iget-object v3, p0, LX/Dp4;->f:Lcom/facebook/messaging/tincan/outbound/Sender;

    iget-object v4, p1, LX/Dp5;->b:LX/DpN;

    iget-object v4, v4, LX/DpN;->msg_to:LX/DpM;

    iget-object v4, v4, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, p1, LX/Dp5;->b:LX/DpN;

    iget-object v6, v6, LX/DpN;->msg_to:LX/DpM;

    iget-object v6, v6, LX/DpM;->instance_id:Ljava/lang/String;

    iget-object v7, p1, LX/Dp5;->b:LX/DpN;

    iget-object v7, v7, LX/DpN;->msg_from:LX/DpM;

    iget-object v7, v7, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iget-object v9, p1, LX/Dp5;->b:LX/DpN;

    iget-object v9, v9, LX/DpN;->msg_from:LX/DpM;

    iget-object v9, v9, LX/DpM;->instance_id:Ljava/lang/String;

    iget-object v10, p1, LX/Dp5;->b:LX/DpN;

    iget-object v10, v10, LX/DpN;->date_micros:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-object v12, p0, LX/Dp4;->g:LX/IuF;

    invoke-virtual {v12}, LX/IuF;->a()[B

    move-result-object v12

    invoke-virtual/range {v3 .. v12}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(JLjava/lang/String;JLjava/lang/String;J[B)V

    .line 2043129
    iget-object v3, p0, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-virtual {v3, p1, v2}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(LX/Dp5;[B)V

    .line 2043130
    :goto_0
    return-void

    .line 2043131
    :cond_0
    invoke-static {p0, p1, v0}, LX/Dp4;->a(LX/Dp4;LX/Dp5;LX/Dpa;)V

    goto :goto_0

    .line 2043132
    :catch_0
    move-exception v2

    .line 2043133
    iget-object v3, p1, LX/Dp5;->b:LX/DpN;

    invoke-static {p0, v9, v3}, LX/Dp4;->a(LX/Dp4;ILX/DpN;)V

    .line 2043134
    sget-object v3, LX/Dp4;->a:Ljava/lang/Class;

    const-string v4, "No crypto session found for sending message content"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2043135
    :catch_1
    move-exception v2

    .line 2043136
    :goto_1
    iget-object v3, p1, LX/Dp5;->b:LX/DpN;

    invoke-static {p0, v9, v3}, LX/Dp4;->a(LX/Dp4;ILX/DpN;)V

    .line 2043137
    sget-object v3, LX/Dp4;->a:Ljava/lang/Class;

    const-string v4, "Error decoding message format"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2043138
    :catch_2
    move-exception v2

    .line 2043139
    sget-object v3, LX/Dp4;->a:Ljava/lang/Class;

    const-string v4, "Duplicate encrypted Salamander packet received"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2043140
    :catch_3
    move-exception v2

    goto :goto_1
.end method

.method private static a(LX/Dp4;LX/Dp5;LX/Dpa;)V
    .locals 13

    .prologue
    const/16 v12, 0x1b58

    .line 2043141
    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->msg_from:LX/DpM;

    iget-object v0, v0, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p1, LX/Dp5;->b:LX/DpN;

    iget-object v2, v2, LX/DpN;->msg_from:LX/DpM;

    iget-object v2, v2, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/DoM;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2043142
    new-instance v1, LX/DoU;

    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->msg_from:LX/DpM;

    iget-object v0, v0, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->msg_from:LX/DpM;

    iget-object v4, v0, LX/DpM;->instance_id:Ljava/lang/String;

    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->msg_to:LX/DpM;

    iget-object v0, v0, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->msg_to:LX/DpM;

    iget-object v7, v0, LX/DpM;->instance_id:Ljava/lang/String;

    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->date_micros:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v10, p1, LX/Dp5;->a:Ljava/lang/String;

    invoke-direct/range {v1 .. v10}, LX/DoU;-><init>(JLjava/lang/String;JLjava/lang/String;JLjava/lang/String;)V

    .line 2043143
    iget-object v0, p0, LX/Dp4;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IuD;

    iget-object v2, p2, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v11, v2}, LX/IuD;->a(Ljava/lang/String;Z)LX/DoK;

    move-result-object v0

    .line 2043144
    :try_start_0
    iget-object v2, p2, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2043145
    iget-object v2, p2, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2043146
    const/4 v2, 0x0

    .line 2043147
    if-nez v0, :cond_6

    .line 2043148
    :cond_0
    :goto_0
    move v2, v2

    .line 2043149
    if-eqz v2, :cond_1

    .line 2043150
    iget-object v3, p0, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    iget-object v4, p1, LX/Dp5;->b:LX/DpN;

    iget-object v4, v4, LX/DpN;->msg_from:LX/DpM;

    iget-object v4, v4, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, p1, LX/Dp5;->b:LX/DpN;

    iget-object v6, v6, LX/DpN;->date_micros:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(JJ)V

    .line 2043151
    :cond_1
    :goto_1
    if-eqz v0, :cond_3

    if-nez v2, :cond_3

    .line 2043152
    iget-object v2, p0, LX/Dp4;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    iget-object v2, p2, LX/Dpa;->serialized_salamander:[B

    iget-object v3, p2, LX/Dpa;->facebook_hmac:[B

    iget-object v4, p0, LX/Dp4;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/IuD;

    iget-object v5, p2, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-static/range {v0 .. v5}, LX/DoJ;->a(LX/DoK;LX/DoU;[B[BLX/IuD;Z)[B
    :try_end_0
    .catch LX/Eak; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/Eai; {:try_start_0 .. :try_end_0} :catch_7
    .catch LX/Eaj; {:try_start_0 .. :try_end_0} :catch_8
    .catch LX/Eaq; {:try_start_0 .. :try_end_0} :catch_5
    .catch LX/DoG; {:try_start_0 .. :try_end_0} :catch_6
    .catch LX/Eah; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_4
    .catch LX/Eal; {:try_start_0 .. :try_end_0} :catch_3
    .catch LX/Ead; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 2043153
    :goto_2
    iget-object v1, p0, LX/Dp4;->f:Lcom/facebook/messaging/tincan/outbound/Sender;

    iget-object v2, p1, LX/Dp5;->b:LX/DpN;

    iget-object v2, v2, LX/DpN;->msg_to:LX/DpM;

    iget-object v2, v2, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p1, LX/Dp5;->b:LX/DpN;

    iget-object v4, v4, LX/DpN;->msg_to:LX/DpM;

    iget-object v4, v4, LX/DpM;->instance_id:Ljava/lang/String;

    iget-object v5, p1, LX/Dp5;->b:LX/DpN;

    iget-object v5, v5, LX/DpN;->msg_from:LX/DpM;

    iget-object v5, v5, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-object v7, p1, LX/Dp5;->b:LX/DpN;

    iget-object v7, v7, LX/DpN;->msg_from:LX/DpM;

    iget-object v7, v7, LX/DpM;->instance_id:Ljava/lang/String;

    iget-object v8, p1, LX/Dp5;->b:LX/DpN;

    iget-object v8, v8, LX/DpN;->date_micros:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v10, p0, LX/Dp4;->g:LX/IuF;

    invoke-virtual {v10}, LX/IuF;->a()[B

    move-result-object v10

    invoke-virtual/range {v1 .. v10}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(JLjava/lang/String;JLjava/lang/String;J[B)V

    .line 2043154
    iget-object v1, p0, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(LX/Dp5;[B)V

    .line 2043155
    :goto_3
    return-void

    .line 2043156
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 2043157
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/Dp4;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DoJ;

    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->msg_from:LX/DpM;

    iget-object v4, v0, LX/DpM;->instance_id:Ljava/lang/String;

    iget-object v6, p2, LX/Dpa;->serialized_salamander:[B

    iget-object v7, p2, LX/Dpa;->facebook_hmac:[B

    iget-object v0, p0, LX/Dp4;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/IuD;

    iget-object v0, p2, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    move-object v3, v11

    move-object v5, v1

    invoke-virtual/range {v2 .. v9}, LX/DoJ;->a(Ljava/lang/String;Ljava/lang/String;LX/DoU;[B[BLX/IuD;Z)[B

    move-result-object v0

    goto :goto_2

    .line 2043158
    :cond_4
    if-nez v0, :cond_5

    .line 2043159
    sget-object v0, LX/Dp4;->a:Ljava/lang/Class;

    const-string v1, "No local session for received message"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2043160
    const/16 v0, 0x1b58

    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    invoke-static {p0, v0, v1}, LX/Dp4;->a(LX/Dp4;ILX/DpN;)V
    :try_end_1
    .catch LX/Eak; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/Eai; {:try_start_1 .. :try_end_1} :catch_7
    .catch LX/Eaj; {:try_start_1 .. :try_end_1} :catch_8
    .catch LX/Eaq; {:try_start_1 .. :try_end_1} :catch_5
    .catch LX/DoG; {:try_start_1 .. :try_end_1} :catch_6
    .catch LX/Eah; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/Eag; {:try_start_1 .. :try_end_1} :catch_4
    .catch LX/Eal; {:try_start_1 .. :try_end_1} :catch_3
    .catch LX/Ead; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_3

    .line 2043161
    :catch_0
    move-exception v0

    .line 2043162
    :goto_4
    sget-object v1, LX/Dp4;->a:Ljava/lang/Class;

    const-string v2, "Error decrypting Salamander packet"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2043163
    const/16 v0, 0x1964

    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    invoke-static {p0, v0, v1}, LX/Dp4;->a(LX/Dp4;ILX/DpN;)V

    goto :goto_3

    .line 2043164
    :cond_5
    :try_start_2
    iget-object v2, p0, LX/Dp4;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    iget-object v3, p2, LX/Dpa;->serialized_salamander:[B

    iget-object v4, p2, LX/Dpa;->facebook_hmac:[B

    iget-object v2, p0, LX/Dp4;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/IuD;

    .line 2043165
    check-cast v0, LX/DoT;

    .line 2043166
    new-instance v5, LX/Eao;

    .line 2043167
    iget-object v6, v0, LX/DoT;->m:LX/Eap;

    move-object v6, v6

    .line 2043168
    invoke-direct {v5, v0, v6}, LX/Eao;-><init>(LX/DoS;LX/Eap;)V

    .line 2043169
    new-instance v6, LX/EbD;

    invoke-direct {v6, v3}, LX/EbD;-><init>([B)V

    .line 2043170
    new-instance v7, LX/Ean;

    const/4 v3, 0x0

    invoke-direct {v7}, LX/Ean;-><init>()V

    invoke-static {v5, v6, v7}, LX/Eao;->a(LX/Eao;LX/EbD;LX/Eac;)[B

    move-result-object v7

    move-object v5, v7

    .line 2043171
    invoke-virtual {v0}, LX/DoT;->e()V

    .line 2043172
    invoke-static {v2, v0, v1}, LX/IuD;->a(LX/IuD;LX/DoK;LX/DoU;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v6
    :try_end_2
    .catch LX/Eak; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/Eai; {:try_start_2 .. :try_end_2} :catch_7
    .catch LX/Eaj; {:try_start_2 .. :try_end_2} :catch_8
    .catch LX/Eaq; {:try_start_2 .. :try_end_2} :catch_5
    .catch LX/DoG; {:try_start_2 .. :try_end_2} :catch_6
    .catch LX/Eah; {:try_start_2 .. :try_end_2} :catch_1
    .catch LX/Eag; {:try_start_2 .. :try_end_2} :catch_4
    .catch LX/Eal; {:try_start_2 .. :try_end_2} :catch_3
    .catch LX/Ead; {:try_start_2 .. :try_end_2} :catch_2

    .line 2043173
    :try_start_3
    iget-object v7, v2, LX/IuD;->c:LX/2Ox;

    invoke-static {v0}, LX/IuD;->a(LX/DoK;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v6, v5, v4, v3}, LX/2Ox;->a(Lcom/facebook/messaging/model/messages/Message;[B[BLjava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catch LX/Eak; {:try_start_3 .. :try_end_3} :catch_0
    .catch LX/Eai; {:try_start_3 .. :try_end_3} :catch_7
    .catch LX/Eaj; {:try_start_3 .. :try_end_3} :catch_8
    .catch LX/Eaq; {:try_start_3 .. :try_end_3} :catch_5
    .catch LX/DoG; {:try_start_3 .. :try_end_3} :catch_6
    .catch LX/Eah; {:try_start_3 .. :try_end_3} :catch_1
    .catch LX/Eag; {:try_start_3 .. :try_end_3} :catch_4
    .catch LX/Eal; {:try_start_3 .. :try_end_3} :catch_3
    .catch LX/Ead; {:try_start_3 .. :try_end_3} :catch_2

    .line 2043174
    :goto_5
    :try_start_4
    move-object v0, v5
    :try_end_4
    .catch LX/Eak; {:try_start_4 .. :try_end_4} :catch_0
    .catch LX/Eai; {:try_start_4 .. :try_end_4} :catch_7
    .catch LX/Eaj; {:try_start_4 .. :try_end_4} :catch_8
    .catch LX/Eaq; {:try_start_4 .. :try_end_4} :catch_5
    .catch LX/DoG; {:try_start_4 .. :try_end_4} :catch_6
    .catch LX/Eah; {:try_start_4 .. :try_end_4} :catch_1
    .catch LX/Eag; {:try_start_4 .. :try_end_4} :catch_4
    .catch LX/Eal; {:try_start_4 .. :try_end_4} :catch_3
    .catch LX/Ead; {:try_start_4 .. :try_end_4} :catch_2

    .line 2043175
    goto/16 :goto_2

    .line 2043176
    :catch_1
    move-exception v0

    .line 2043177
    :goto_6
    sget-object v1, LX/Dp4;->a:Ljava/lang/Class;

    const-string v2, "Error in setting up local session"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2043178
    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    invoke-static {p0, v12, v0}, LX/Dp4;->a(LX/Dp4;ILX/DpN;)V

    .line 2043179
    iget-object v0, p0, LX/Dp4;->i:LX/2P3;

    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->msg_from:LX/DpM;

    iget-object v1, v1, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/2P3;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2043180
    iget-object v1, p0, LX/Dp4;->h:LX/2Ox;

    invoke-virtual {v1, v0}, LX/2Ox;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto/16 :goto_3

    .line 2043181
    :catch_2
    move-exception v0

    .line 2043182
    sget-object v1, LX/Dp4;->a:Ljava/lang/Class;

    const-string v2, "Duplicate encrypted Salamander packet received"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 2043183
    :catch_3
    move-exception v0

    goto :goto_6

    :catch_4
    move-exception v0

    goto :goto_6

    .line 2043184
    :catch_5
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v0

    goto/16 :goto_4

    :catch_7
    move-exception v0

    goto/16 :goto_4

    :catch_8
    move-exception v0

    goto/16 :goto_4

    .line 2043185
    :cond_6
    :try_start_5
    invoke-virtual {v0}, LX/DoK;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, LX/Dp5;->b:LX/DpN;

    iget-object v4, v4, LX/DpN;->msg_from:LX/DpM;

    iget-object v4, v4, LX/DpM;->instance_id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2043186
    const/4 v2, 0x1

    goto/16 :goto_0
    :try_end_5
    .catch LX/Eak; {:try_start_5 .. :try_end_5} :catch_0
    .catch LX/Eai; {:try_start_5 .. :try_end_5} :catch_7
    .catch LX/Eaj; {:try_start_5 .. :try_end_5} :catch_8
    .catch LX/Eaq; {:try_start_5 .. :try_end_5} :catch_5
    .catch LX/DoG; {:try_start_5 .. :try_end_5} :catch_6
    .catch LX/Eah; {:try_start_5 .. :try_end_5} :catch_1
    .catch LX/Eag; {:try_start_5 .. :try_end_5} :catch_4
    .catch LX/Eal; {:try_start_5 .. :try_end_5} :catch_3
    .catch LX/Ead; {:try_start_5 .. :try_end_5} :catch_2

    .line 2043187
    :catch_9
    move-exception v6

    .line 2043188
    sget-object v7, LX/IuD;->a:Ljava/lang/Class;

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/tincan/omnistore/TincanMessage;)V
    .locals 14

    .prologue
    .line 2043189
    :try_start_0
    iget-object v0, p0, LX/Dp4;->b:LX/Dp6;

    invoke-virtual {v0, p1}, LX/Dp6;->a(Lcom/facebook/messaging/tincan/omnistore/TincanMessage;)LX/Dp5;

    move-result-object v0

    .line 2043190
    iget-object v1, v0, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->nonce:[B

    if-nez v1, :cond_2

    .line 2043191
    sget-object v1, LX/Dp4;->a:Ljava/lang/Class;

    const-string v2, "Received a packet with a null sender_packet_id!"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2043192
    :goto_0
    iget-object v1, v0, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->date_micros:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 2043193
    sget-object v1, LX/Doq;->a:LX/Doq;

    move-object v1, v1

    .line 2043194
    iget-object v2, v0, LX/Dp5;->b:LX/DpN;

    iget-object v2, v2, LX/DpN;->date_micros:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, LX/Doq;->a(J)V

    .line 2043195
    :cond_0
    iget-object v1, v0, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2043196
    iget-object v1, v0, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x12c

    if-lt v1, v2, :cond_1

    .line 2043197
    iget-object v1, p0, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b(LX/Dp5;)V

    .line 2043198
    :cond_1
    :goto_1
    return-void

    .line 2043199
    :cond_2
    iget-object v1, v0, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->nonce:[B

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch LX/Dpo; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 2043200
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2043201
    sget-object v0, LX/Dp4;->a:Ljava/lang/Class;

    const-string v2, "Message processing error"

    invoke-static {v0, v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2043202
    iget v0, v1, LX/Dpo;->newVersion:I

    iget v2, v1, LX/Dpo;->currentVersion:I

    if-le v0, v2, :cond_3

    const/16 v0, 0x1c5

    :goto_2
    iget-object v1, v1, LX/Dpo;->packet:LX/DpN;

    invoke-static {p0, v0, v1}, LX/Dp4;->a(LX/Dp4;ILX/DpN;)V

    goto :goto_1

    .line 2043203
    :sswitch_0
    :try_start_1
    iget-object v1, p0, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    .line 2043204
    iget-object v6, v0, LX/Dp5;->b:LX/DpN;

    iget-object v6, v6, LX/DpN;->body:LX/DpO;

    invoke-virtual {v6}, LX/DpO;->c()LX/DpE;

    move-result-object v6

    .line 2043205
    new-instance v12, Lcom/facebook/user/model/Name;

    iget-object v7, v6, LX/DpE;->first_name:Ljava/lang/String;

    iget-object v8, v6, LX/DpE;->last_name:Ljava/lang/String;

    invoke-direct {v12, v7, v8}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2043206
    iget-object v7, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    iget-object v6, v6, LX/DpE;->user_id:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v12}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v12}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v7 .. v12}, LX/2Ox;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch LX/Dpo; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/7H0; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2043207
    goto :goto_1

    .line 2043208
    :catch_1
    move-exception v0

    .line 2043209
    :goto_3
    sget-object v1, LX/Dp4;->a:Ljava/lang/Class;

    const-string v2, "Message processing error"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2043210
    :sswitch_1
    :try_start_2
    invoke-static {p0, v0}, LX/Dp4;->a(LX/Dp4;LX/Dp5;)V

    goto :goto_1

    .line 2043211
    :catch_2
    move-exception v0

    goto :goto_3

    .line 2043212
    :sswitch_2
    iget-object v1, v0, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->body:LX/DpO;

    invoke-virtual {v1}, LX/DpO;->g()LX/Dpa;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/Dp4;->a(LX/Dp4;LX/Dp5;LX/Dpa;)V

    goto :goto_1

    .line 2043213
    :sswitch_3
    iget-object v1, p0, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    const/4 v12, 0x0

    .line 2043214
    iget-object v6, v0, LX/Dp5;->b:LX/DpN;

    iget-object v6, v6, LX/DpN;->body:LX/DpO;

    invoke-virtual {v6}, LX/DpO;->e()LX/DpV;

    move-result-object v6

    iget-object v6, v6, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    .line 2043215
    if-nez v6, :cond_4

    .line 2043216
    sget-object v6, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a:Ljava/lang/Class;

    const-string v7, "Invalid delivery-receipt payload."

    invoke-static {v6, v7}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2043217
    :goto_4
    goto :goto_1

    .line 2043218
    :sswitch_4
    iget-object v1, p0, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    const/4 v12, 0x0

    .line 2043219
    iget-object v6, v0, LX/Dp5;->b:LX/DpN;

    iget-object v6, v6, LX/DpN;->body:LX/DpO;

    invoke-virtual {v6}, LX/DpO;->e()LX/DpV;

    move-result-object v6

    iget-object v6, v6, LX/DpV;->unix_time_micros:Ljava/lang/Long;

    .line 2043220
    if-nez v6, :cond_6

    .line 2043221
    sget-object v6, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a:Ljava/lang/Class;

    const-string v7, "Invalid read-receipt payload."

    invoke-static {v6, v7}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2043222
    :goto_5
    goto/16 :goto_1

    .line 2043223
    :sswitch_5
    iget-object v1, p0, LX/Dp4;->e:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->e(LX/Dp5;)V
    :try_end_2
    .catch LX/Dpo; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/7H0; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    .line 2043224
    :cond_3
    const/16 v0, 0x1c4

    goto/16 :goto_2

    .line 2043225
    :cond_4
    :try_start_3
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    .line 2043226
    iget-object v8, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->d:LX/2P3;

    iget-object v9, v0, LX/Dp5;->b:LX/DpN;

    iget-object v9, v9, LX/DpN;->msg_from:LX/DpM;

    iget-object v9, v9, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, LX/2P3;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v8

    .line 2043227
    iget-object v9, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    invoke-virtual {v9, v8, v6, v7}, LX/2Ox;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2043228
    iget-object v6, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->k:LX/2P4;

    invoke-virtual {v6, v8, v12}, LX/2P4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v7

    .line 2043229
    iget-object v6, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-nez v6, :cond_5

    .line 2043230
    sget-object v6, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a:Ljava/lang/Class;

    const-string v7, "Thread %s not found when processing delivery receipt."

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v9, v12

    invoke-static {v6, v7, v9}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 2043231
    :cond_5
    iget-object v6, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->f:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/2Oe;

    iget-object v9, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-wide v10, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-virtual {v6, v9, v10, v11}, LX/2Oe;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2043232
    iget-object v6, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->h:LX/2Ow;

    invoke-virtual {v6, v8}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_4
    :try_end_3
    .catch LX/Dpo; {:try_start_3 .. :try_end_3} :catch_0
    .catch LX/7H0; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    .line 2043233
    :cond_6
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long v8, v6, v8

    .line 2043234
    iget-object v6, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->d:LX/2P3;

    iget-object v7, v0, LX/Dp5;->b:LX/DpN;

    iget-object v7, v7, LX/DpN;->msg_from:LX/DpM;

    iget-object v7, v7, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, LX/2P3;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v7

    .line 2043235
    iget-object v6, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    invoke-virtual {v6, v7, v8, v9}, LX/2Ox;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2043236
    iget-object v6, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->k:LX/2P4;

    invoke-virtual {v6, v7, v12}, LX/2P4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v10

    .line 2043237
    iget-object v6, v10, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-nez v6, :cond_7

    .line 2043238
    sget-object v6, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a:Ljava/lang/Class;

    const-string v8, "Thread %s not found when processing read receipt."

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v12

    invoke-static {v6, v8, v9}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 2043239
    :cond_7
    iget-object v6, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->f:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/2Oe;

    iget-object v11, v10, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-wide v12, v10, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-virtual {v6, v11, v12, v13}, LX/2Oe;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2043240
    iget-object v6, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->g:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;

    invoke-virtual {v6, v7, v8, v9}, Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2043241
    iget-object v6, v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->h:LX/2Ow;

    invoke-virtual {v6, v7}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto/16 :goto_5

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x7 -> :sswitch_2
        0x20 -> :sswitch_5
        0x32 -> :sswitch_3
        0x33 -> :sswitch_4
        0x64 -> :sswitch_0
    .end sparse-switch
.end method
