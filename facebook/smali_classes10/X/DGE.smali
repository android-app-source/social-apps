.class public LX/DGE;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        "T::",
        "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DGG;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DGE",
            "<TE;TT;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DGG;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979447
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1979448
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DGE;->b:LX/0Zi;

    .line 1979449
    iput-object p1, p0, LX/DGE;->a:LX/0Ot;

    .line 1979450
    return-void
.end method

.method public static a(LX/0QB;)LX/DGE;
    .locals 4

    .prologue
    .line 1979405
    const-class v1, LX/DGE;

    monitor-enter v1

    .line 1979406
    :try_start_0
    sget-object v0, LX/DGE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979407
    sput-object v2, LX/DGE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979408
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979409
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979410
    new-instance v3, LX/DGE;

    const/16 p0, 0x21a1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DGE;-><init>(LX/0Ot;)V

    .line 1979411
    move-object v0, v3

    .line 1979412
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979413
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DGE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979414
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1979416
    check-cast p2, LX/DGD;

    .line 1979417
    iget-object v0, p0, LX/DGE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DGG;

    iget-object v1, p2, LX/DGD;->a:LX/1Pd;

    iget-object v2, p2, LX/DGD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x0

    .line 1979418
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1979419
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1979420
    check-cast v3, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1979421
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1979422
    check-cast v4, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-static {v4}, LX/25C;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/0Px;

    move-result-object v7

    .line 1979423
    iget-object v4, v0, LX/DGG;->e:LX/1DR;

    invoke-virtual {v4}, LX/1DR;->a()I

    move-result v8

    .line 1979424
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v9

    move v4, v5

    :goto_0
    if-ge v4, v9, :cond_0

    .line 1979425
    new-instance v10, LX/DGK;

    invoke-direct {v10, v2, v4, v8, v5}, LX/DGK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZ)V

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1979426
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1979427
    :cond_0
    new-instance v4, LX/DGF;

    invoke-direct {v4, v0, v3, v7}, LX/DGF;-><init>(LX/DGG;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/0Px;)V

    .line 1979428
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v7

    .line 1979429
    iput-object v4, v7, LX/3mP;->g:LX/25K;

    .line 1979430
    move-object v4, v7

    .line 1979431
    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v7

    .line 1979432
    iput-object v7, v4, LX/3mP;->d:LX/25L;

    .line 1979433
    move-object v4, v4

    .line 1979434
    iput-object v3, v4, LX/3mP;->e:LX/0jW;

    .line 1979435
    move-object v3, v4

    .line 1979436
    iput-boolean v5, v3, LX/3mP;->a:Z

    .line 1979437
    move-object v3, v3

    .line 1979438
    const/16 v4, 0x8

    .line 1979439
    iput v4, v3, LX/3mP;->b:I

    .line 1979440
    move-object v3, v3

    .line 1979441
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v8

    .line 1979442
    iget-object v3, v0, LX/DGG;->a:LX/DGB;

    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    move-object v4, p1

    move-object v6, v2

    move-object v7, v1

    invoke-virtual/range {v3 .. v8}, LX/DGB;->a(Landroid/content/Context;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/Object;LX/25M;)LX/DGA;

    move-result-object v3

    .line 1979443
    iget-object v4, v0, LX/DGG;->b:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1979444
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1979445
    invoke-static {}, LX/1dS;->b()V

    .line 1979446
    const/4 v0, 0x0

    return-object v0
.end method
