.class public LX/EME;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EMC;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesProfilePhotoComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2110370
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EME;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesProfilePhotoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110367
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2110368
    iput-object p1, p0, LX/EME;->b:LX/0Ot;

    .line 2110369
    return-void
.end method

.method public static a(LX/0QB;)LX/EME;
    .locals 4

    .prologue
    .line 2110371
    const-class v1, LX/EME;

    monitor-enter v1

    .line 2110372
    :try_start_0
    sget-object v0, LX/EME;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110373
    sput-object v2, LX/EME;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110374
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110375
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110376
    new-instance v3, LX/EME;

    const/16 p0, 0x3410

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EME;-><init>(LX/0Ot;)V

    .line 2110377
    move-object v0, v3

    .line 2110378
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110379
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EME;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110380
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110381
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2110356
    check-cast p2, LX/EMD;

    .line 2110357
    iget-object v0, p0, LX/EME;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesProfilePhotoComponentSpec;

    iget-object v1, p2, LX/EMD;->a:LX/8d0;

    .line 2110358
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b1735

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b1736

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    iget-object v2, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesProfilePhotoComponentSpec;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    .line 2110359
    invoke-interface {v1}, LX/8d0;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object p2

    .line 2110360
    if-nez p2, :cond_0

    .line 2110361
    const/4 p2, 0x0

    .line 2110362
    :goto_0
    move-object p2, p2

    .line 2110363
    invoke-virtual {v2, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v2

    sget-object p2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesProfilePhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    sget-object p0, LX/1Up;->c:LX/1Up;

    invoke-virtual {v2, p0}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v2

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-virtual {v2, p0}, LX/1up;->c(F)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2110364
    return-object v0

    :cond_0
    invoke-interface {p2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2110365
    invoke-static {}, LX/1dS;->b()V

    .line 2110366
    const/4 v0, 0x0

    return-object v0
.end method
