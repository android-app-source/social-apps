.class public final LX/E5D;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5E;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitPlaceInfoBlurbWithBreadcrumbsComponentFragment$PlaceInfoBlurbBreadcrumbs;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLInterfaces$DefaultTimeRangeFields;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:D

.field public f:D

.field public g:Ljava/lang/String;

.field public h:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public final synthetic k:LX/E5E;


# direct methods
.method public constructor <init>(LX/E5E;)V
    .locals 1

    .prologue
    .line 2078039
    iput-object p1, p0, LX/E5D;->k:LX/E5E;

    .line 2078040
    move-object v0, p1

    .line 2078041
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2078042
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2078038
    const-string v0, "ReactionPlaceInfoBlurbWithBreadcrumbsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2078043
    if-ne p0, p1, :cond_1

    .line 2078044
    :cond_0
    :goto_0
    return v0

    .line 2078045
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2078046
    goto :goto_0

    .line 2078047
    :cond_3
    check-cast p1, LX/E5D;

    .line 2078048
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2078049
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2078050
    if-eq v2, v3, :cond_0

    .line 2078051
    iget-object v2, p0, LX/E5D;->a:LX/0Px;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E5D;->a:LX/0Px;

    iget-object v3, p1, LX/E5D;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2078052
    goto :goto_0

    .line 2078053
    :cond_5
    iget-object v2, p1, LX/E5D;->a:LX/0Px;

    if-nez v2, :cond_4

    .line 2078054
    :cond_6
    iget-object v2, p0, LX/E5D;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E5D;->b:Ljava/lang/String;

    iget-object v3, p1, LX/E5D;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2078055
    goto :goto_0

    .line 2078056
    :cond_8
    iget-object v2, p1, LX/E5D;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2078057
    :cond_9
    iget-object v2, p0, LX/E5D;->c:LX/0Px;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E5D;->c:LX/0Px;

    iget-object v3, p1, LX/E5D;->c:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2078058
    goto :goto_0

    .line 2078059
    :cond_b
    iget-object v2, p1, LX/E5D;->c:LX/0Px;

    if-nez v2, :cond_a

    .line 2078060
    :cond_c
    iget-object v2, p0, LX/E5D;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/E5D;->d:Ljava/lang/String;

    iget-object v3, p1, LX/E5D;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2078061
    goto :goto_0

    .line 2078062
    :cond_e
    iget-object v2, p1, LX/E5D;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2078063
    :cond_f
    iget-wide v2, p0, LX/E5D;->e:D

    iget-wide v4, p1, LX/E5D;->e:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_10

    move v0, v1

    .line 2078064
    goto :goto_0

    .line 2078065
    :cond_10
    iget-wide v2, p0, LX/E5D;->f:D

    iget-wide v4, p1, LX/E5D;->f:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_11

    move v0, v1

    .line 2078066
    goto :goto_0

    .line 2078067
    :cond_11
    iget-object v2, p0, LX/E5D;->g:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/E5D;->g:Ljava/lang/String;

    iget-object v3, p1, LX/E5D;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    .line 2078068
    goto/16 :goto_0

    .line 2078069
    :cond_13
    iget-object v2, p1, LX/E5D;->g:Ljava/lang/String;

    if-nez v2, :cond_12

    .line 2078070
    :cond_14
    iget-object v2, p0, LX/E5D;->h:LX/2km;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/E5D;->h:LX/2km;

    iget-object v3, p1, LX/E5D;->h:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    .line 2078071
    goto/16 :goto_0

    .line 2078072
    :cond_16
    iget-object v2, p1, LX/E5D;->h:LX/2km;

    if-nez v2, :cond_15

    .line 2078073
    :cond_17
    iget-object v2, p0, LX/E5D;->i:Ljava/lang/String;

    if-eqz v2, :cond_19

    iget-object v2, p0, LX/E5D;->i:Ljava/lang/String;

    iget-object v3, p1, LX/E5D;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_18
    move v0, v1

    .line 2078074
    goto/16 :goto_0

    .line 2078075
    :cond_19
    iget-object v2, p1, LX/E5D;->i:Ljava/lang/String;

    if-nez v2, :cond_18

    .line 2078076
    :cond_1a
    iget-object v2, p0, LX/E5D;->j:Ljava/lang/String;

    if-eqz v2, :cond_1b

    iget-object v2, p0, LX/E5D;->j:Ljava/lang/String;

    iget-object v3, p1, LX/E5D;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2078077
    goto/16 :goto_0

    .line 2078078
    :cond_1b
    iget-object v2, p1, LX/E5D;->j:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
