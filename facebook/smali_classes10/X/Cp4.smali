.class public final LX/Cp4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionViewedModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;)V
    .locals 0

    .prologue
    .line 1936514
    iput-object p1, p0, LX/Cp4;->a:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1936509
    iget-object v0, p0, LX/Cp4;->a:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sendUserViewed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error writing user viewed data"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 1936510
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1936511
    move-object v1, v1

    .line 1936512
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1936513
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1936506
    iget-object v0, p0, LX/Cp4;->a:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    const/4 v1, 0x1

    .line 1936507
    iput-boolean v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->G:Z

    .line 1936508
    return-void
.end method
