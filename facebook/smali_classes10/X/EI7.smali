.class public final enum LX/EI7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EI7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EI7;

.field public static final enum BOTTOM_RIGHT:LX/EI7;

.field public static final enum CENTER:LX/EI7;

.field public static final enum TOP_RIGHT:LX/EI7;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2100593
    new-instance v0, LX/EI7;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v2}, LX/EI7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI7;->CENTER:LX/EI7;

    .line 2100594
    new-instance v0, LX/EI7;

    const-string v1, "TOP_RIGHT"

    invoke-direct {v0, v1, v3}, LX/EI7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI7;->TOP_RIGHT:LX/EI7;

    .line 2100595
    new-instance v0, LX/EI7;

    const-string v1, "BOTTOM_RIGHT"

    invoke-direct {v0, v1, v4}, LX/EI7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI7;->BOTTOM_RIGHT:LX/EI7;

    .line 2100596
    const/4 v0, 0x3

    new-array v0, v0, [LX/EI7;

    sget-object v1, LX/EI7;->CENTER:LX/EI7;

    aput-object v1, v0, v2

    sget-object v1, LX/EI7;->TOP_RIGHT:LX/EI7;

    aput-object v1, v0, v3

    sget-object v1, LX/EI7;->BOTTOM_RIGHT:LX/EI7;

    aput-object v1, v0, v4

    sput-object v0, LX/EI7;->$VALUES:[LX/EI7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2100597
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EI7;
    .locals 1

    .prologue
    .line 2100592
    const-class v0, LX/EI7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EI7;

    return-object v0
.end method

.method public static values()[LX/EI7;
    .locals 1

    .prologue
    .line 2100591
    sget-object v0, LX/EI7;->$VALUES:[LX/EI7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EI7;

    return-object v0
.end method
