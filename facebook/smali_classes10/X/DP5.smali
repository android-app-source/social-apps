.class public LX/DP5;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Landroid/content/res/Resources;

.field private static b:LX/154;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/154;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1993056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1993057
    sput-object p1, LX/DP5;->a:Landroid/content/res/Resources;

    .line 1993058
    sput-object p2, LX/DP5;->b:LX/154;

    .line 1993059
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 1993043
    new-instance v0, Landroid/text/SpannableStringBuilder;

    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    invoke-direct {v0, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public static final a(Ljava/lang/String;I)Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 1993053
    invoke-static {p0}, LX/DP5;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1993054
    invoke-static {v0, p1}, LX/DP5;->a(Landroid/text/SpannableStringBuilder;I)V

    .line 1993055
    return-object v0
.end method

.method public static final a(Ljava/lang/String;II)Landroid/text/SpannableStringBuilder;
    .locals 4

    .prologue
    .line 1993050
    invoke-static {p0}, LX/DP5;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1993051
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int v2, p1, p2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1993052
    return-object v0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;I)V
    .locals 6

    .prologue
    .line 1993046
    if-gtz p1, :cond_1

    .line 1993047
    :cond_0
    :goto_0
    return-void

    .line 1993048
    :cond_1
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1993049
    const-string v0, " "

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    sget-object v1, LX/DP5;->a:Landroid/content/res/Resources;

    const v2, 0x7f081b5c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    sget-object v1, LX/DP5;->a:Landroid/content/res/Resources;

    const v2, 0x7f0f00cc

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, LX/DP5;->b:LX/154;

    invoke-virtual {v5, p1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/DP5;
    .locals 3

    .prologue
    .line 1993044
    new-instance v2, LX/DP5;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v1

    check-cast v1, LX/154;

    invoke-direct {v2, v0, v1}, LX/DP5;-><init>(Landroid/content/res/Resources;LX/154;)V

    .line 1993045
    return-object v2
.end method
