.class public LX/ECG;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/media/AudioManager;

.field private final c:LX/2S7;

.field public final d:LX/ECI;

.field public e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field public f:Landroid/media/AudioManager$OnAudioFocusChangeListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2089563
    const-class v0, LX/ECG;

    sput-object v0, LX/ECG;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/media/AudioManager;LX/2S7;LX/ECI;)V
    .locals 0
    .param p3    # LX/ECI;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2089577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2089578
    iput-object p1, p0, LX/ECG;->b:Landroid/media/AudioManager;

    .line 2089579
    iput-object p2, p0, LX/ECG;->c:LX/2S7;

    .line 2089580
    iput-object p3, p0, LX/ECG;->d:LX/ECI;

    .line 2089581
    return-void
.end method

.method public static a(LX/ECG;Landroid/media/AudioManager$OnAudioFocusChangeListener;II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2089575
    iget-object v1, p0, LX/ECG;->b:Landroid/media/AudioManager;

    invoke-virtual {v1, p1, p2, p3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    .line 2089576
    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/ECG;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2089572
    sget-object v0, LX/ECG;->a:Ljava/lang/Class;

    invoke-static {v0, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2089573
    iget-object v0, p0, LX/ECG;->c:LX/2S7;

    invoke-virtual {v0, p1}, LX/2S7;->a(Ljava/lang/String;)V

    .line 2089574
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 2

    .prologue
    .line 2089568
    iget-object v0, p0, LX/ECG;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v0, :cond_0

    .line 2089569
    iget-object v0, p0, LX/ECG;->b:Landroid/media/AudioManager;

    iget-object v1, p0, LX/ECG;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 2089570
    const/4 v0, 0x0

    iput-object v0, p0, LX/ECG;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 2089571
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2089564
    iget-object v0, p0, LX/ECG;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v0, :cond_0

    .line 2089565
    iget-object v0, p0, LX/ECG;->b:Landroid/media/AudioManager;

    iget-object v1, p0, LX/ECG;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 2089566
    const/4 v0, 0x0

    iput-object v0, p0, LX/ECG;->e:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 2089567
    :cond_0
    return-void
.end method
