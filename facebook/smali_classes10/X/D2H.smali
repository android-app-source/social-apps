.class public LX/D2H;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/D2J;

.field public final c:LX/Ban;

.field private final d:Z

.field public e:LX/5w5;

.field public final f:LX/1Bv;

.field private final g:LX/D2M;

.field private final h:LX/D2T;

.field private final i:LX/Bak;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Ban;ZLX/5w5;LX/D2J;LX/1Bv;LX/D2M;LX/D2T;LX/Bak;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Ban;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/5w5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/D2J;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1958515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958516
    iput-object p1, p0, LX/D2H;->a:Landroid/content/Context;

    .line 1958517
    iput-object p5, p0, LX/D2H;->b:LX/D2J;

    .line 1958518
    iput-object p2, p0, LX/D2H;->c:LX/Ban;

    .line 1958519
    iput-boolean p3, p0, LX/D2H;->d:Z

    .line 1958520
    iput-object p4, p0, LX/D2H;->e:LX/5w5;

    .line 1958521
    iput-object p6, p0, LX/D2H;->f:LX/1Bv;

    .line 1958522
    iput-object p7, p0, LX/D2H;->g:LX/D2M;

    .line 1958523
    iput-object p8, p0, LX/D2H;->h:LX/D2T;

    .line 1958524
    iput-object p9, p0, LX/D2H;->i:LX/Bak;

    .line 1958525
    return-void
.end method

.method private g()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;
    .locals 1

    .prologue
    .line 1958512
    invoke-direct {p0}, LX/D2H;->h()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1958513
    invoke-direct {p0}, LX/D2H;->h()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v0

    .line 1958514
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/D2H;->e:LX/5w5;

    invoke-interface {v0}, LX/5w5;->b()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v0

    goto :goto_0
.end method

.method private h()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1958506
    invoke-static {p0}, LX/D2H;->k(LX/D2H;)LX/D2S;

    move-result-object v0

    .line 1958507
    if-nez v0, :cond_0

    .line 1958508
    const/4 v0, 0x0

    .line 1958509
    :goto_0
    return-object v0

    .line 1958510
    :cond_0
    iget-object p0, v0, LX/D2S;->b:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-object v0, p0

    .line 1958511
    goto :goto_0
.end method

.method private j()F
    .locals 1

    .prologue
    .line 1958500
    invoke-static {p0}, LX/D2H;->k(LX/D2H;)LX/D2S;

    move-result-object v0

    .line 1958501
    if-nez v0, :cond_0

    .line 1958502
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1958503
    :goto_0
    return v0

    .line 1958504
    :cond_0
    iget p0, v0, LX/D2S;->e:F

    move v0, p0

    .line 1958505
    goto :goto_0
.end method

.method public static k(LX/D2H;)LX/D2S;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1958526
    iget-object v0, p0, LX/D2H;->b:LX/D2J;

    invoke-interface {v0}, LX/D2J;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/D2H;->d:Z

    if-nez v0, :cond_2

    .line 1958527
    :cond_0
    const/4 v0, 0x0

    .line 1958528
    :cond_1
    :goto_0
    return-object v0

    .line 1958529
    :cond_2
    iget-object v0, p0, LX/D2H;->h:LX/D2T;

    .line 1958530
    const-string v1, "uploading"

    invoke-virtual {v0, v1}, LX/D2T;->a(Ljava/lang/String;)LX/D2S;

    move-result-object v1

    move-object v0, v1

    .line 1958531
    if-nez v0, :cond_1

    .line 1958532
    iget-object v0, p0, LX/D2H;->h:LX/D2T;

    iget-object v1, p0, LX/D2H;->e:LX/5w5;

    invoke-interface {v1}, LX/5w5;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/D2T;->a(Ljava/lang/String;)LX/D2S;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1958482
    invoke-virtual {p0}, LX/D2H;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/D2H;->f:LX/1Bv;

    invoke-virtual {v0}, LX/1Bv;->a()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1958483
    if-eqz v0, :cond_0

    .line 1958484
    iget-object v0, p0, LX/D2H;->i:LX/Bak;

    iget-object v1, p0, LX/D2H;->c:LX/Ban;

    invoke-virtual {v1}, LX/Ban;->getLazyProfileVideoIcon()LX/0zw;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/Bak;->a(LX/0zw;I)V

    .line 1958485
    :goto_1
    invoke-virtual {p0}, LX/D2H;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1958486
    iget-object v0, p0, LX/D2H;->c:LX/Ban;

    invoke-virtual {v0}, LX/Ban;->getLazyProfileVideoView()LX/0zw;

    move-result-object v0

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;

    invoke-virtual {v0, v3}, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;->setVisibility(I)V

    .line 1958487
    iget-object v0, p0, LX/D2H;->c:LX/Ban;

    invoke-virtual {v0}, LX/Ban;->getLazyProfileVideoView()LX/0zw;

    move-result-object v0

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;

    invoke-direct {p0}, LX/D2H;->g()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v1

    invoke-direct {p0}, LX/D2H;->j()F

    move-result v2

    iget-object v3, p0, LX/D2H;->c:LX/Ban;

    iget-object v3, v3, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    .line 1958488
    invoke-static {p0}, LX/D2H;->k(LX/D2H;)LX/D2S;

    move-result-object v4

    .line 1958489
    if-nez v4, :cond_4

    .line 1958490
    const/4 v4, 0x0

    .line 1958491
    :goto_2
    move-object v5, v4

    .line 1958492
    move-object v4, p1

    invoke-static/range {v0 .. v5}, LX/D2K;->a(Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;FLX/7N6;Landroid/view/View$OnClickListener;Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)V

    .line 1958493
    :goto_3
    return-void

    .line 1958494
    :cond_0
    iget-object v0, p0, LX/D2H;->i:LX/Bak;

    iget-object v1, p0, LX/D2H;->c:LX/Ban;

    invoke-virtual {v1}, LX/Ban;->getLazyProfileVideoIcon()LX/0zw;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, LX/Bak;->a(LX/0zw;I)V

    goto :goto_1

    .line 1958495
    :cond_1
    iget-object v0, p0, LX/D2H;->c:LX/Ban;

    invoke-virtual {v0}, LX/Ban;->getLazyProfileVideoView()LX/0zw;

    move-result-object v0

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1958496
    iget-object v0, p0, LX/D2H;->c:LX/Ban;

    invoke-virtual {v0}, LX/Ban;->getLazyProfileVideoView()LX/0zw;

    move-result-object v0

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1958497
    :cond_2
    iget-object v0, p0, LX/D2H;->c:LX/Ban;

    invoke-virtual {v0}, LX/Ban;->getLazyProfileVideoView()LX/0zw;

    move-result-object v0

    invoke-virtual {v0}, LX/0zw;->c()V

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1958498
    :cond_4
    iget-object p0, v4, LX/D2S;->d:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v4, p0

    .line 1958499
    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1958481
    invoke-virtual {p0}, LX/D2H;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D2H;->f:LX/1Bv;

    invoke-virtual {v0}, LX/1Bv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1958480
    invoke-direct {p0}, LX/D2H;->g()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1958479
    invoke-direct {p0}, LX/D2H;->h()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1958476
    iget-object v0, p0, LX/D2H;->e:LX/5w5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D2H;->e:LX/5w5;

    invoke-interface {v0}, LX/5w5;->b()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D2H;->e:LX/5w5;

    invoke-interface {v0}, LX/5w5;->b()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D2H;->e:LX/5w5;

    invoke-interface {v0}, LX/5w5;->b()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/D2H;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1958477
    :cond_0
    :goto_0
    return-void

    .line 1958478
    :cond_1
    iget-object v0, p0, LX/D2H;->g:LX/D2M;

    iget-object v1, p0, LX/D2H;->a:Landroid/content/Context;

    iget-object v2, p0, LX/D2H;->e:LX/5w5;

    invoke-interface {v2}, LX/5w5;->b()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/D2M;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
