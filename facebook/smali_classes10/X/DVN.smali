.class public LX/DVN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DVJ;


# instance fields
.field private final a:LX/DVm;

.field private b:LX/DVl;

.field public c:LX/DVU;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z


# direct methods
.method public constructor <init>(LX/DVm;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2004973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2004974
    const-string v0, ""

    iput-object v0, p0, LX/DVN;->e:Ljava/lang/String;

    .line 2004975
    iput-object p1, p0, LX/DVN;->a:LX/DVm;

    .line 2004976
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2004977
    iget-object v0, p0, LX/DVN;->b:LX/DVl;

    if-eqz v0, :cond_0

    .line 2004978
    iget-object v0, p0, LX/DVN;->b:LX/DVl;

    invoke-virtual {v0}, LX/B1d;->e()V

    .line 2004979
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 2004980
    iget-object v0, p0, LX/DVN;->b:LX/DVl;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/DVN;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2004981
    :cond_0
    iput-object p1, p0, LX/DVN;->e:Ljava/lang/String;

    .line 2004982
    iget-object v0, p0, LX/DVN;->a:LX/DVm;

    iget-object v1, p0, LX/DVN;->d:Ljava/lang/String;

    iget-boolean v2, p0, LX/DVN;->f:Z

    new-instance v3, LX/DVM;

    invoke-direct {v3, p0}, LX/DVM;-><init>(LX/DVN;)V

    .line 2004983
    new-instance v4, LX/DVl;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v10

    check-cast v10, LX/0tX;

    move-object v7, v1

    move-object v8, p1

    move v9, v2

    move-object v11, v3

    invoke-direct/range {v4 .. v11}, LX/DVl;-><init>(LX/1Ck;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;ZLX/0tX;LX/B1b;)V

    .line 2004984
    move-object v0, v4

    .line 2004985
    iput-object v0, p0, LX/DVN;->b:LX/DVl;

    .line 2004986
    :cond_1
    iget-object v0, p0, LX/DVN;->b:LX/DVl;

    invoke-virtual {v0}, LX/B1d;->f()V

    .line 2004987
    return-void
.end method

.method public final a(Ljava/lang/String;LX/DVU;Z)V
    .locals 0

    .prologue
    .line 2004988
    iput-object p1, p0, LX/DVN;->d:Ljava/lang/String;

    .line 2004989
    iput-object p2, p0, LX/DVN;->c:LX/DVU;

    .line 2004990
    iput-boolean p3, p0, LX/DVN;->f:Z

    .line 2004991
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2004992
    iget-object v0, p0, LX/DVN;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DVN;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2004993
    iget-object v0, p0, LX/DVN;->c:LX/DVU;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
