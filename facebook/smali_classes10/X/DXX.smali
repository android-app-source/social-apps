.class public final LX/DXX;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DXY;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

.field public final synthetic b:LX/DXY;


# direct methods
.method public constructor <init>(LX/DXY;)V
    .locals 1

    .prologue
    .line 2010359
    iput-object p1, p0, LX/DXX;->b:LX/DXY;

    .line 2010360
    move-object v0, p1

    .line 2010361
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2010362
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2010363
    const-string v0, "ShareLinkRefreshComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2010364
    if-ne p0, p1, :cond_1

    .line 2010365
    :cond_0
    :goto_0
    return v0

    .line 2010366
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2010367
    goto :goto_0

    .line 2010368
    :cond_3
    check-cast p1, LX/DXX;

    .line 2010369
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2010370
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2010371
    if-eq v2, v3, :cond_0

    .line 2010372
    iget-object v2, p0, LX/DXX;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/DXX;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    iget-object v3, p1, LX/DXX;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2010373
    goto :goto_0

    .line 2010374
    :cond_4
    iget-object v2, p1, LX/DXX;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
