.class public LX/DCZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/DwF;

.field public final c:LX/1vg;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/DwF;LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1974121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1974122
    iput-object p1, p0, LX/DCZ;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1974123
    iput-object p2, p0, LX/DCZ;->b:LX/DwF;

    .line 1974124
    iput-object p3, p0, LX/DCZ;->c:LX/1vg;

    .line 1974125
    return-void
.end method

.method public static a(LX/0QB;)LX/DCZ;
    .locals 6

    .prologue
    .line 1974126
    const-class v1, LX/DCZ;

    monitor-enter v1

    .line 1974127
    :try_start_0
    sget-object v0, LX/DCZ;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1974128
    sput-object v2, LX/DCZ;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1974129
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1974130
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1974131
    new-instance p0, LX/DCZ;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/DwF;->b(LX/0QB;)LX/DwF;

    move-result-object v4

    check-cast v4, LX/DwF;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-direct {p0, v3, v4, v5}, LX/DCZ;-><init>(Lcom/facebook/content/SecureContextHelper;LX/DwF;LX/1vg;)V

    .line 1974132
    move-object v0, p0

    .line 1974133
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1974134
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DCZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1974135
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1974136
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
