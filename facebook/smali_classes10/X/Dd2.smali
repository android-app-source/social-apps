.class public final enum LX/Dd2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dd2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dd2;

.field public static final enum FACEBOOK:LX/Dd2;

.field public static final enum GAMES:LX/Dd2;

.field public static final enum MESSENGER:LX/Dd2;

.field public static final enum NO_BADGE:LX/Dd2;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2019142
    new-instance v0, LX/Dd2;

    const-string v1, "FACEBOOK"

    invoke-direct {v0, v1, v2}, LX/Dd2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dd2;->FACEBOOK:LX/Dd2;

    .line 2019143
    new-instance v0, LX/Dd2;

    const-string v1, "MESSENGER"

    invoke-direct {v0, v1, v3}, LX/Dd2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dd2;->MESSENGER:LX/Dd2;

    .line 2019144
    new-instance v0, LX/Dd2;

    const-string v1, "GAMES"

    invoke-direct {v0, v1, v4}, LX/Dd2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dd2;->GAMES:LX/Dd2;

    .line 2019145
    new-instance v0, LX/Dd2;

    const-string v1, "NO_BADGE"

    invoke-direct {v0, v1, v5}, LX/Dd2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dd2;->NO_BADGE:LX/Dd2;

    .line 2019146
    const/4 v0, 0x4

    new-array v0, v0, [LX/Dd2;

    sget-object v1, LX/Dd2;->FACEBOOK:LX/Dd2;

    aput-object v1, v0, v2

    sget-object v1, LX/Dd2;->MESSENGER:LX/Dd2;

    aput-object v1, v0, v3

    sget-object v1, LX/Dd2;->GAMES:LX/Dd2;

    aput-object v1, v0, v4

    sget-object v1, LX/Dd2;->NO_BADGE:LX/Dd2;

    aput-object v1, v0, v5

    sput-object v0, LX/Dd2;->$VALUES:[LX/Dd2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2019141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dd2;
    .locals 1

    .prologue
    .line 2019139
    const-class v0, LX/Dd2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dd2;

    return-object v0
.end method

.method public static values()[LX/Dd2;
    .locals 1

    .prologue
    .line 2019140
    sget-object v0, LX/Dd2;->$VALUES:[LX/Dd2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dd2;

    return-object v0
.end method
