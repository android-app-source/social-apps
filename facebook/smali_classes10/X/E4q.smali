.class public LX/E4q;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E4o;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E4q;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E4r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2077342
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E4q;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E4r;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077343
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2077344
    iput-object p1, p0, LX/E4q;->b:LX/0Ot;

    .line 2077345
    return-void
.end method

.method public static a(LX/0QB;)LX/E4q;
    .locals 4

    .prologue
    .line 2077346
    sget-object v0, LX/E4q;->c:LX/E4q;

    if-nez v0, :cond_1

    .line 2077347
    const-class v1, LX/E4q;

    monitor-enter v1

    .line 2077348
    :try_start_0
    sget-object v0, LX/E4q;->c:LX/E4q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077349
    if-eqz v2, :cond_0

    .line 2077350
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077351
    new-instance v3, LX/E4q;

    const/16 p0, 0x3115

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E4q;-><init>(LX/0Ot;)V

    .line 2077352
    move-object v0, v3

    .line 2077353
    sput-object v0, LX/E4q;->c:LX/E4q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077354
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077355
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077356
    :cond_1
    sget-object v0, LX/E4q;->c:LX/E4q;

    return-object v0

    .line 2077357
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077358
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2077359
    check-cast p2, LX/E4p;

    .line 2077360
    iget-object v0, p0, LX/E4q;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/E4p;->a:Landroid/text/SpannableStringBuilder;

    iget-object v1, p2, LX/E4p;->b:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    const/4 p2, 0x6

    const/4 p0, 0x1

    .line 2077361
    if-eqz v1, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_DARK_PARAGRAPH_LONG_TRUNCATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v2, :cond_0

    .line 2077362
    const v2, 0x7f0a010c

    .line 2077363
    :goto_0
    move v2, v2

    .line 2077364
    if-eqz v1, :cond_1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->LEFT_DARK_PARAGRAPH_LONG_TRUNCATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne v1, v3, :cond_1

    .line 2077365
    const v3, 0x7f0b0050

    .line 2077366
    :goto_1
    move v3, v3

    .line 2077367
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f01072b

    invoke-interface {v4, v5}, LX/1Dh;->U(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    const/high16 p0, 0x3fa00000    # 1.25f

    invoke-virtual {v5, p0}, LX/1ne;->j(F)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v2

    const/4 v3, 0x7

    const v5, 0x7f0b163a

    invoke-interface {v2, v3, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f010717

    invoke-interface {v2, p2, v3}, LX/1Di;->b(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b163a

    invoke-interface {v2, p2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2077368
    return-object v0

    :cond_0
    const v2, 0x7f0a010e

    goto :goto_0

    :cond_1
    const v3, 0x7f0b004e

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2077369
    invoke-static {}, LX/1dS;->b()V

    .line 2077370
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/E4o;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2077371
    new-instance v1, LX/E4p;

    invoke-direct {v1, p0}, LX/E4p;-><init>(LX/E4q;)V

    .line 2077372
    sget-object v2, LX/E4q;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E4o;

    .line 2077373
    if-nez v2, :cond_0

    .line 2077374
    new-instance v2, LX/E4o;

    invoke-direct {v2}, LX/E4o;-><init>()V

    .line 2077375
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/E4o;->a$redex0(LX/E4o;LX/1De;IILX/E4p;)V

    .line 2077376
    move-object v1, v2

    .line 2077377
    move-object v0, v1

    .line 2077378
    return-object v0
.end method
