.class public abstract LX/E8N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "L:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/2j3;

.field private final c:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;LX/2j3;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2082381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2082382
    const/4 v0, 0x0

    iput-object v0, p0, LX/E8N;->d:Ljava/lang/String;

    .line 2082383
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/E8N;->e:Z

    .line 2082384
    iput-boolean v1, p0, LX/E8N;->f:Z

    .line 2082385
    iput-object p1, p0, LX/E8N;->a:LX/0Sh;

    .line 2082386
    iput-object p2, p0, LX/E8N;->b:LX/2j3;

    .line 2082387
    new-instance v0, LX/E8S;

    invoke-direct {v0, p0}, LX/E8S;-><init>(LX/E8N;)V

    iput-object v0, p0, LX/E8N;->c:LX/0Ve;

    .line 2082388
    iput-object p3, p0, LX/E8N;->g:Ljava/lang/String;

    .line 2082389
    iput-object p4, p0, LX/E8N;->h:Ljava/lang/String;

    .line 2082390
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)LX/0us;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "LX/0us;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;LX/0Ve;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2082377
    iget-boolean v0, p0, LX/E8N;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/E8N;->f:Z

    if-nez v0, :cond_0

    .line 2082378
    iget-object v0, p0, LX/E8N;->d:Ljava/lang/String;

    iget-object v1, p0, LX/E8N;->c:LX/0Ve;

    invoke-virtual {p0, v0, v1}, LX/E8N;->a(Ljava/lang/String;LX/0Ve;)V

    .line 2082379
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/E8N;->f:Z

    .line 2082380
    :cond_0
    return-void
.end method

.method public abstract b(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method
