.class public LX/Ejx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2162807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2162808
    iput-object p1, p0, LX/Ejx;->a:LX/0tX;

    .line 2162809
    return-void
.end method

.method public static b(LX/0QB;)LX/Ejx;
    .locals 2

    .prologue
    .line 2162810
    new-instance v1, LX/Ejx;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/Ejx;-><init>(LX/0tX;)V

    .line 2162811
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2162812
    new-instance v0, LX/4K8;

    invoke-direct {v0}, LX/4K8;-><init>()V

    .line 2162813
    const-string v1, "country"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162814
    move-object v0, v0

    .line 2162815
    const-string v1, "contact_point"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162816
    move-object v0, v0

    .line 2162817
    const-string v1, "phone_acquisition_promo"

    .line 2162818
    const-string v2, "source"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162819
    move-object v0, v0

    .line 2162820
    const-string v1, "promo_type"

    invoke-virtual {v0, v1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162821
    move-object v0, v0

    .line 2162822
    const-string v1, "qp_id"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162823
    move-object v0, v0

    .line 2162824
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2162825
    const-string v2, "state"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2162826
    move-object v0, v0

    .line 2162827
    new-instance v1, LX/Ek1;

    invoke-direct {v1}, LX/Ek1;-><init>()V

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2162828
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2162829
    iget-object v5, v1, LX/Ek1;->a:Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2162830
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 2162831
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 2162832
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2162833
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2162834
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2162835
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2162836
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2162837
    new-instance v5, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;

    invoke-direct {v5, v4}, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;-><init>(LX/15i;)V

    .line 2162838
    move-object v1, v5

    .line 2162839
    new-instance v2, LX/Ejz;

    invoke-direct {v2}, LX/Ejz;-><init>()V

    move-object v2, v2

    .line 2162840
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2162841
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 2162842
    iget-object v1, p0, LX/Ejx;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
