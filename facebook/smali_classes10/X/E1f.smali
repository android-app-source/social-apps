.class public LX/E1f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/E1f;


# instance fields
.field private final a:LX/01T;

.field private final b:LX/E1i;

.field private final c:LX/2iz;

.field private final d:LX/1b2;

.field private final e:LX/0SI;


# direct methods
.method public constructor <init>(LX/01T;LX/E1i;LX/2iz;LX/1b2;LX/0SI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2070393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2070394
    iput-object p1, p0, LX/E1f;->a:LX/01T;

    .line 2070395
    iput-object p2, p0, LX/E1f;->b:LX/E1i;

    .line 2070396
    iput-object p3, p0, LX/E1f;->c:LX/2iz;

    .line 2070397
    iput-object p4, p0, LX/E1f;->d:LX/1b2;

    .line 2070398
    iput-object p5, p0, LX/E1f;->e:LX/0SI;

    .line 2070399
    return-void
.end method

.method public static a(LX/0QB;)LX/E1f;
    .locals 9

    .prologue
    .line 2070380
    sget-object v0, LX/E1f;->f:LX/E1f;

    if-nez v0, :cond_1

    .line 2070381
    const-class v1, LX/E1f;

    monitor-enter v1

    .line 2070382
    :try_start_0
    sget-object v0, LX/E1f;->f:LX/E1f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2070383
    if-eqz v2, :cond_0

    .line 2070384
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2070385
    new-instance v3, LX/E1f;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    invoke-static {v0}, LX/E1i;->a(LX/0QB;)LX/E1i;

    move-result-object v5

    check-cast v5, LX/E1i;

    invoke-static {v0}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v6

    check-cast v6, LX/2iz;

    invoke-static {v0}, LX/1b2;->a(LX/0QB;)LX/1b2;

    move-result-object v7

    check-cast v7, LX/1b2;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v8

    check-cast v8, LX/0SI;

    invoke-direct/range {v3 .. v8}, LX/E1f;-><init>(LX/01T;LX/E1i;LX/2iz;LX/1b2;LX/0SI;)V

    .line 2070386
    move-object v0, v3

    .line 2070387
    sput-object v0, LX/E1f;->f:LX/E1f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2070388
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2070389
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2070390
    :cond_1
    sget-object v0, LX/E1f;->f:LX/E1f;

    return-object v0

    .line 2070391
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2070392
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2070400
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v8, v7

    invoke-virtual/range {v0 .. v8}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2069970
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;
    .locals 11
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2069971
    invoke-interface {p1}, LX/9rk;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2069972
    const/4 v2, 0x0

    .line 2069973
    :cond_0
    :goto_0
    return-object v2

    .line 2069974
    :cond_1
    sget-object v2, LX/E1e;->a:[I

    invoke-interface {p1}, LX/9rk;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2069975
    const/4 v2, 0x0

    goto :goto_0

    .line 2069976
    :pswitch_0
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 2069977
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 2069978
    :cond_3
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto :goto_0

    .line 2069979
    :pswitch_1
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    .line 2069980
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 2069981
    :cond_5
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->b(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto :goto_0

    .line 2069982
    :pswitch_2
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7

    .line 2069983
    :cond_6
    const/4 v2, 0x0

    goto :goto_0

    .line 2069984
    :cond_7
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, LX/E1i;->c(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto :goto_0

    .line 2069985
    :pswitch_3
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_9

    .line 2069986
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2069987
    :cond_9
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v2, v3}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2069988
    :pswitch_4
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2069989
    :cond_a
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    iget-object v3, p0, LX/E1f;->c:LX/2iz;

    invoke-virtual {v3, p4}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1i;->a(LX/2jY;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2069990
    :cond_b
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v2, v3}, LX/E1i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2069991
    :pswitch_5
    invoke-interface {p1}, LX/9rk;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_c

    invoke-interface {p1}, LX/9rk;->T()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_c

    invoke-interface {p1}, LX/9rk;->e()LX/174;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-interface {p1}, LX/9rk;->e()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2069992
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2069993
    :cond_d
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->u()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->T()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9rk;->e()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2069994
    :pswitch_6
    invoke-interface {p1}, LX/9rk;->K()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_e

    .line 2069995
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2069996
    :cond_e
    invoke-interface {p1}, LX/9rk;->K()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/E1i;->a(Landroid/net/Uri;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2069997
    :pswitch_7
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_10

    .line 2069998
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2069999
    :cond_10
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->U()LX/0Px;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070000
    :pswitch_8
    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_11

    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 2070001
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070002
    :cond_12
    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v4

    .line 2070003
    const/4 v6, 0x0

    .line 2070004
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->hd_()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->hd_()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->b()LX/1Fb;

    move-result-object v2

    if-eqz v2, :cond_13

    .line 2070005
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->hd_()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$OwnerModel;->b()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v6

    .line 2070006
    :cond_13
    const/4 v5, 0x0

    .line 2070007
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    move-result-object v2

    if-eqz v2, :cond_14

    .line 2070008
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel$FundraiserForCharityTextModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2070009
    :cond_14
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9rk;->X()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070010
    :pswitch_9
    invoke-interface {p1}, LX/9rk;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 2070011
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070012
    :cond_15
    invoke-interface {p1}, LX/9rk;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/E1i;->b(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070013
    :pswitch_a
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_16

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2070014
    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070015
    :cond_17
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-static {p2, v0, v2}, LX/E1i;->a(Landroid/content/Context;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070016
    :pswitch_b
    const-string v3, "page/create_new_page"

    invoke-static {v3}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/E1i;->i(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070017
    :pswitch_c
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_18

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_19

    .line 2070018
    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070019
    :cond_19
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1i;->g(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070020
    :pswitch_d
    invoke-interface {p1}, LX/9rk;->x()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v2

    if-eqz v2, :cond_1a

    invoke-interface {p1}, LX/9rk;->x()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;->d()LX/2rX;

    move-result-object v2

    if-eqz v2, :cond_1a

    invoke-interface {p1}, LX/9rk;->x()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 2070021
    :cond_1a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070022
    :cond_1b
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->x()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->Y()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/E1i;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$EventSpaceModel;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070023
    :pswitch_e
    invoke-interface {p1}, LX/9rk;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v2

    if-eqz v2, :cond_1c

    invoke-interface {p1}, LX/9rk;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;->gZ_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1c

    invoke-interface {p1}, LX/9rk;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v2

    if-eqz v2, :cond_1c

    invoke-interface {p1}, LX/9rk;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 2070024
    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070025
    :cond_1d
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->y()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventMessageOnlyFriendActionFieldsModel$FriendModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/E1i;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070026
    :pswitch_f
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_1e

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1e

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 2070027
    :cond_1e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070028
    :cond_1f
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p2, v3, v4}, LX/E1i;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070029
    :pswitch_10
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_20

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_21

    .line 2070030
    :cond_20
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070031
    :cond_21
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1i;->e(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070032
    :pswitch_11
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_22

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_23

    .line 2070033
    :cond_22
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070034
    :cond_23
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1i;->d(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070035
    :pswitch_12
    invoke-interface {p1}, LX/9rk;->Q()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v2

    if-eqz v2, :cond_24

    invoke-interface {p1}, LX/9rk;->Q()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_24

    invoke-interface {p1}, LX/9rk;->Q()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;->hc_()LX/2rX;

    move-result-object v2

    if-nez v2, :cond_25

    .line 2070036
    :cond_24
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070037
    :cond_25
    invoke-interface {p1}, LX/9rk;->Q()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->m()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-result-object v3

    invoke-static {p4, v2, v3}, LX/E1i;->a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$PostTargetModel;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070038
    :pswitch_13
    iget-object v2, p0, LX/E1f;->d:LX/1b2;

    invoke-virtual {v2, p2}, LX/1b2;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    .line 2070039
    sget-object v2, LX/21D;->PAGE_FEED:LX/21D;

    sget-object v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v2, v4}, Lcom/facebook/facecast/FacecastActivity;->a(LX/21D;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Landroid/os/Bundle;

    move-result-object v2

    .line 2070040
    invoke-virtual {v3, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2070041
    new-instance v2, LX/Cfl;

    const/4 v4, 0x0

    sget-object v5, LX/Cfc;->GO_LIVE_TAP:LX/Cfc;

    invoke-direct {v2, v4, v5, v3}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2070042
    :pswitch_14
    invoke-static/range {p6 .. p6}, LX/E1i;->a(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070043
    :pswitch_15
    invoke-interface {p1}, LX/9rk;->ad()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_26

    invoke-interface {p1}, LX/9rk;->ad()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 2070044
    :cond_26
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070045
    :cond_27
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    if-nez p7, :cond_28

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->REACTION_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object p7

    :cond_28
    invoke-interface {p1}, LX/9rk;->ad()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v2, v0, v3}, LX/E1i;->a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionEventInviteFieldsModel;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070046
    :pswitch_16
    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    if-eqz v2, :cond_29

    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 2070047
    :cond_29
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070048
    :cond_2a
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->X()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070049
    :pswitch_17
    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    if-eqz v2, :cond_2b

    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 2070050
    :cond_2b
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070051
    :cond_2c
    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->X()Ljava/lang/String;

    move-result-object v3

    invoke-static {p4, v2, v3}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070052
    :pswitch_18
    invoke-interface {p1}, LX/9rk;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v2

    if-eqz v2, :cond_2d

    invoke-interface {p1}, LX/9rk;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;

    move-result-object v2

    if-eqz v2, :cond_2d

    invoke-interface {p1}, LX/9rk;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_2d

    invoke-interface {p1}, LX/9rk;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2d

    invoke-interface {p1}, LX/9rk;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel$ObjectModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 2070053
    :cond_2d
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070054
    :cond_2e
    invoke-interface {p1}, LX/9rk;->s()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->Y()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/E1i;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$ComposerInlineActivityModel;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070055
    :pswitch_19
    invoke-interface {p1}, LX/9rk;->G()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v2

    if-eqz v2, :cond_2f

    invoke-interface {p1}, LX/9rk;->G()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_30

    .line 2070056
    :cond_2f
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070057
    :cond_30
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->G()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v2, v3, v0, p3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070058
    :pswitch_1a
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->e(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070059
    :pswitch_1b
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_31

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_31

    invoke-interface {p1}, LX/9rk;->Z()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v2

    if-eqz v2, :cond_31

    invoke-interface {p1}, LX/9rk;->Z()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    move-result-object v2

    if-eqz v2, :cond_31

    invoke-interface {p1}, LX/9rk;->Z()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_32

    .line 2070060
    :cond_31
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070061
    :cond_32
    invoke-interface {p1}, LX/9rk;->Z()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenAppointmentDetailsActionFieldsModel$StatusCardModel$ComponentFlowRequestModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2070062
    invoke-static {p2, v2}, LX/E1i;->f(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070063
    :pswitch_1c
    invoke-interface {p1}, LX/9rk;->M()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_33

    invoke-interface {p1}, LX/9rk;->e()LX/174;

    move-result-object v2

    if-eqz v2, :cond_33

    invoke-interface {p1}, LX/9rk;->e()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 2070064
    :cond_33
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070065
    :cond_34
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->M()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->e()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v2, v3, v4, v0}, LX/E1i;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070066
    :pswitch_1d
    invoke-interface {p1}, LX/9rk;->F()LX/1k1;

    move-result-object v2

    if-nez v2, :cond_35

    .line 2070067
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070068
    :cond_35
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->F()LX/1k1;

    move-result-object v3

    invoke-interface {v3}, LX/1k1;->a()D

    move-result-wide v4

    invoke-interface {p1}, LX/9rk;->F()LX/1k1;

    move-result-object v3

    invoke-interface {v3}, LX/1k1;->b()D

    move-result-wide v6

    move-object v3, p2

    invoke-virtual/range {v2 .. v7}, LX/E1i;->a(Landroid/content/Context;DD)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070069
    :pswitch_1e
    invoke-static {}, LX/E1i;->a()LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070070
    :pswitch_1f
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->N()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->O()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9rk;->P()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, p2, v3, v4, v5}, LX/E1i;->a(Landroid/content/Context;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenNearbyPlacesActionFieldsModel$PlacesQueryLocationPageModel;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070071
    :pswitch_20
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_36

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_37

    .line 2070072
    :cond_36
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070073
    :cond_37
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->OPEN_PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p2, v4}, LX/E1i;->d(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    sget-object v4, LX/Cfc;->OPEN_PAGE_INFO_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070074
    :pswitch_21
    invoke-interface {p1}, LX/9rk;->F()LX/1k1;

    move-result-object v2

    if-eqz v2, :cond_38

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_38

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_39

    .line 2070075
    :cond_38
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070076
    :cond_39
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->F()LX/1k1;

    move-result-object v3

    invoke-interface {v3}, LX/1k1;->a()D

    move-result-wide v4

    invoke-interface {p1}, LX/9rk;->F()LX/1k1;

    move-result-object v3

    invoke-interface {v3}, LX/1k1;->b()D

    move-result-wide v6

    invoke-interface {p1}, LX/9rk;->z()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v10

    move-object v3, p2

    invoke-virtual/range {v2 .. v10}, LX/E1i;->a(Landroid/content/Context;DDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070077
    :pswitch_22
    invoke-interface {p1}, LX/9rk;->F()LX/1k1;

    move-result-object v2

    if-eqz v2, :cond_3a

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_3a

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3b

    .line 2070078
    :cond_3a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070079
    :cond_3b
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->F()LX/1k1;

    move-result-object v3

    invoke-interface {v3}, LX/1k1;->a()D

    move-result-wide v4

    invoke-interface {p1}, LX/9rk;->F()LX/1k1;

    move-result-object v3

    invoke-interface {v3}, LX/1k1;->b()D

    move-result-wide v6

    invoke-interface {p1}, LX/9rk;->z()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v10

    move-object v3, p2

    invoke-virtual/range {v2 .. v10}, LX/E1i;->b(Landroid/content/Context;DDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070080
    :pswitch_23
    invoke-interface {p1}, LX/9rk;->G()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v2

    if-eqz v2, :cond_3c

    invoke-interface {p1}, LX/9rk;->G()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 2070081
    :cond_3c
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070082
    :cond_3d
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->G()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPlaysActionFieldsModel$MatchPageModel;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p6

    invoke-virtual {v2, v3, v0, p3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070083
    :pswitch_24
    invoke-interface {p1}, LX/9rk;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/E1i;->i(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070084
    :pswitch_25
    invoke-interface {p1}, LX/9rk;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v2

    if-eqz v2, :cond_3e

    invoke-interface {p1}, LX/9rk;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3f

    .line 2070085
    :cond_3e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070086
    :cond_3f
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->h(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070087
    :pswitch_26
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_40

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_41

    .line 2070088
    :cond_40
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070089
    :cond_41
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->e()LX/5sY;

    move-result-object v2

    if-nez v2, :cond_42

    const/4 v7, 0x0

    .line 2070090
    :goto_1
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v6

    move-object v3, p2

    move-object/from16 v4, p6

    invoke-virtual/range {v2 .. v7}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070091
    :cond_42
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->e()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 2070092
    :pswitch_27
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_43

    invoke-interface {p1}, LX/9rk;->o()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v2

    if-eqz v2, :cond_43

    invoke-interface {p1}, LX/9rk;->o()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_44

    .line 2070093
    :cond_43
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070094
    :cond_44
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->PAGE_COMMERCE_OPEN_COLLECTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9rk;->o()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeePageCommerceProductsFieldsModel$CollectionModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p2, v4, v5}, LX/E1i;->e(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    sget-object v4, LX/Cfc;->OPEN_PAGE_COMMERCE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070095
    :pswitch_28
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_45

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_46

    .line 2070096
    :cond_45
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070097
    :cond_46
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->PAGE_COMMERCE_OPEN_STORE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    sget-object v6, LX/7iP;->PAGE:LX/7iP;

    invoke-virtual {v3, p2, v4, v5, v6}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/7iP;)LX/Cfl;

    move-result-object v3

    sget-object v4, LX/Cfc;->OPEN_PAGE_COMMERCE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070098
    :pswitch_29
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_47

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_48

    .line 2070099
    :cond_47
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070100
    :cond_48
    invoke-static/range {p5 .. p5}, LX/2s8;->i(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_49

    .line 2070101
    sget-object v2, LX/Cfc;->CREATION_TAB:LX/Cfc;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070102
    :cond_49
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070103
    :pswitch_2a
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_4a

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4b

    .line 2070104
    :cond_4a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070105
    :cond_4b
    iget-object v2, p0, LX/E1f;->e:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v6

    .line 2070106
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->e()LX/5sY;

    move-result-object v4

    invoke-interface {v4}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->c()Z

    move-result v5

    sget-object v7, LX/Cfc;->WRITE_POST_TO_PAGE_TAP:LX/Cfc;

    invoke-static/range {v2 .. v7}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/auth/viewercontext/ViewerContext;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070107
    :pswitch_2b
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_4c

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4d

    .line 2070108
    :cond_4c
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070109
    :cond_4d
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->i(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070110
    :pswitch_2c
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_4e

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4f

    .line 2070111
    :cond_4e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070112
    :cond_4f
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->j(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070113
    :pswitch_2d
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_50

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_51

    .line 2070114
    :cond_50
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070115
    :cond_51
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->k(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070116
    :pswitch_2e
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_52

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_53

    .line 2070117
    :cond_52
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070118
    :cond_53
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->x(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070119
    :pswitch_2f
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_54

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_55

    .line 2070120
    :cond_54
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070121
    :cond_55
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p2, v4}, LX/E1i;->l(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    sget-object v4, LX/Cfc;->PAGE_ADD_TAB_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070122
    :pswitch_30
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_56

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_57

    .line 2070123
    :cond_56
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070124
    :cond_57
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1i;->k(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070125
    :pswitch_31
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_58

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_59

    .line 2070126
    :cond_58
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070127
    :cond_59
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->m(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070128
    :pswitch_32
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_5a

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5b

    .line 2070129
    :cond_5a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070130
    :cond_5b
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p5 .. p5}, LX/2s8;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/Cfc;->PROMOTE_WEBSITE:LX/Cfc;

    invoke-virtual {v2, p2, v3, v4, v5}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070131
    :pswitch_33
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_5c

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5d

    .line 2070132
    :cond_5c
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070133
    :cond_5d
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->PROMOTE_PAGE:LX/Cfc;

    invoke-virtual {v2, p2, v3, v4}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070134
    :pswitch_34
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_5e

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5f

    .line 2070135
    :cond_5e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070136
    :cond_5f
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p5 .. p5}, LX/2s8;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/Cfc;->PROMOTE_WEBSITE:LX/Cfc;

    invoke-virtual {v2, p2, v3, v4, v5}, LX/E1i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070137
    :pswitch_35
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_60

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_60

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_60

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->e()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_60

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->e()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_61

    .line 2070138
    :cond_60
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070139
    :cond_61
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->e()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, LX/9rk;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->PUBLISH_PAGE_PHOTO:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v2, v6, :cond_62

    const/4 v2, 0x1

    :goto_2
    invoke-static {p4, v3, v4, v5, v2}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    :cond_62
    const/4 v2, 0x0

    goto :goto_2

    .line 2070140
    :pswitch_36
    if-nez p6, :cond_63

    .line 2070141
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070142
    :cond_63
    invoke-interface {p1}, LX/9rk;->V()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-static {v0, v2}, LX/E1i;->a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionReplacementUnitFieldsModel$ReplacementUnitModel;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070143
    :pswitch_37
    if-nez p6, :cond_64

    .line 2070144
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070145
    :cond_64
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    move-object/from16 v0, p6

    invoke-virtual {v2, v0, p3}, LX/E1i;->e(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070146
    :pswitch_38
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-virtual {v2, p2}, LX/E1i;->c(Landroid/content/Context;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070147
    :pswitch_39
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_65

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_66

    .line 2070148
    :cond_65
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070149
    :cond_66
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->C(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070150
    :pswitch_3a
    iget-object v2, p0, LX/E1f;->a:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-eq v2, v3, :cond_67

    .line 2070151
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070152
    :cond_67
    invoke-static {p4}, LX/E1i;->n(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070153
    :pswitch_3b
    if-eqz p4, :cond_68

    if-eqz p5, :cond_68

    if-nez p6, :cond_69

    .line 2070154
    :cond_68
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070155
    :cond_69
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->OG_OBJECT_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-object v4, p4

    move-object/from16 v5, p5

    move-object v6, p3

    move-object/from16 v7, p6

    invoke-virtual/range {v2 .. v7}, LX/E1i;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070156
    :pswitch_3c
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_6a

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6b

    .line 2070157
    :cond_6a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070158
    :cond_6b
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, LX/E1i;->p(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070159
    :pswitch_3d
    if-nez p6, :cond_6c

    .line 2070160
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070161
    :cond_6c
    invoke-static/range {p6 .. p6}, LX/E1i;->p(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070162
    :pswitch_3e
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_6d

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6e

    .line 2070163
    :cond_6d
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070164
    :cond_6e
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->v()Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p2, v3, v4}, LX/E1i;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070165
    :pswitch_3f
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_6f

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_70

    .line 2070166
    :cond_6f
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070167
    :cond_70
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p2, v3, v4}, LX/E1i;->g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070168
    :pswitch_40
    if-eqz p4, :cond_71

    if-eqz p5, :cond_71

    if-nez p6, :cond_72

    .line 2070169
    :cond_71
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070170
    :cond_72
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->ADMINED_PAGES_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-object v4, p4

    move-object/from16 v5, p5

    move-object v6, p3

    move-object/from16 v7, p6

    invoke-virtual/range {v2 .. v7}, LX/E1i;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070171
    :pswitch_41
    if-eqz p4, :cond_73

    if-eqz p5, :cond_73

    if-nez p6, :cond_74

    .line 2070172
    :cond_73
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070173
    :cond_74
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PROFILE_BLOCKS:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-object v4, p4

    move-object/from16 v5, p5

    move-object v6, p3

    move-object/from16 v7, p6

    invoke-virtual/range {v2 .. v7}, LX/E1i;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070174
    :pswitch_42
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_75

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_76

    .line 2070175
    :cond_75
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070176
    :cond_76
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->o(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070177
    :pswitch_43
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_77

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_78

    .line 2070178
    :cond_77
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070179
    :cond_78
    invoke-static {p4}, LX/E1i;->l(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070180
    :pswitch_44
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_79

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7a

    .line 2070181
    :cond_79
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070182
    :cond_7a
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->u(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    .line 2070183
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_JOBS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v3, v2, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070184
    :pswitch_45
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_7b

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7c

    .line 2070185
    :cond_7b
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070186
    :cond_7c
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->v(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    .line 2070187
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_OFFERS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v3, v2, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070188
    :pswitch_46
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_7d

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7e

    .line 2070189
    :cond_7d
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070190
    :cond_7e
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->x(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    .line 2070191
    invoke-static/range {p5 .. p5}, LX/2s8;->i(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2070192
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v3, v2, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070193
    :pswitch_47
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_7f

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_80

    .line 2070194
    :cond_7f
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070195
    :cond_80
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_POSTS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p2, v4, v5}, LX/E1i;->f(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070196
    :pswitch_48
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_81

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_82

    .line 2070197
    :cond_81
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070198
    :cond_82
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_VIDEO_PLAYLISTS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p2, v4}, LX/E1i;->A(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070199
    :pswitch_49
    invoke-interface {p1}, LX/9rk;->q()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_83

    .line 2070200
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070201
    :cond_83
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->q()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v3, p6

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p7

    move-object v8, p3

    invoke-virtual/range {v2 .. v8}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070202
    :pswitch_4a
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_84

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_85

    .line 2070203
    :cond_84
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070204
    :cond_85
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->y(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070205
    :pswitch_4b
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_86

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_87

    .line 2070206
    :cond_86
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070207
    :cond_87
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->z(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070208
    :pswitch_4c
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_88

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_88

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_89

    .line 2070209
    :cond_88
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070210
    :cond_89
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, p2}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)LX/Cfl;

    move-result-object v2

    .line 2070211
    invoke-static/range {p5 .. p5}, LX/2s8;->i(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2070212
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_ALL_RATINGS_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v3, v2, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070213
    :pswitch_4d
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_8a

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_8b

    .line 2070214
    :cond_8a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070215
    :cond_8b
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p2, v4}, LX/E1i;->B(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070216
    :pswitch_4e
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_8c

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_8d

    .line 2070217
    :cond_8c
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070218
    :cond_8d
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p2, v3, v4}, LX/E1i;->h(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070219
    :pswitch_4f
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_8e

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8e

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8f

    .line 2070220
    :cond_8e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070221
    :cond_8f
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, p2}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070222
    :pswitch_50
    invoke-interface {p1}, LX/9rk;->ac()Ljava/lang/String;

    move-result-object v3

    .line 2070223
    invoke-interface {p1}, LX/9rk;->hi_()LX/174;

    move-result-object v4

    .line 2070224
    invoke-interface {p1}, LX/9rk;->b()LX/174;

    move-result-object v5

    .line 2070225
    invoke-interface {p1}, LX/9rk;->c()LX/174;

    move-result-object v6

    .line 2070226
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_90

    if-eqz v4, :cond_90

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_91

    .line 2070227
    :cond_90
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070228
    :cond_91
    if-eqz p8, :cond_92

    .line 2070229
    move-object/from16 v0, p7

    move-object/from16 v1, p8

    iput-object v0, v1, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    .line 2070230
    :cond_92
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v5, :cond_93

    const/4 v5, 0x0

    :goto_3
    if-nez v6, :cond_94

    const/4 v6, 0x0

    :goto_4
    move-object/from16 v7, p8

    invoke-virtual/range {v2 .. v7}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    :cond_93
    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_94
    invoke-interface {v6}, LX/174;->a()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 2070231
    :pswitch_51
    if-eqz p4, :cond_95

    if-eqz p5, :cond_95

    if-nez p6, :cond_96

    .line 2070232
    :cond_95
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070233
    :cond_96
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->TOPIC_LIST:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-object v4, p4

    move-object/from16 v5, p5

    move-object v6, p3

    move-object/from16 v7, p6

    invoke-virtual/range {v2 .. v7}, LX/E1i;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070234
    :pswitch_52
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-virtual {v2, p2}, LX/E1i;->a(Landroid/content/Context;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070235
    :pswitch_53
    invoke-interface {p1}, LX/9rk;->E()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v2

    if-eqz v2, :cond_97

    invoke-interface {p1}, LX/9rk;->E()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_97

    invoke-interface {p1}, LX/9rk;->E()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_98

    .line 2070236
    :cond_97
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070237
    :cond_98
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->E()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeJobDetailActionFieldsModel$JobOpeningModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, LX/E1i;->c(Ljava/lang/String;Landroid/content/Context;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070238
    :pswitch_54
    invoke-interface {p1}, LX/9rk;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v2

    if-eqz v2, :cond_99

    invoke-interface {p1}, LX/9rk;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_99

    invoke-interface {p1}, LX/9rk;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    move-result-object v2

    if-eqz v2, :cond_99

    invoke-interface {p1}, LX/9rk;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_9a

    .line 2070239
    :cond_99
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070240
    :cond_9a
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->I()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSeeOfferDetailActionFieldsModel$OfferViewModel$RootShareStoryModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, p2}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070241
    :pswitch_55
    invoke-interface {p1}, LX/9rk;->ag()Ljava/lang/String;

    move-result-object v2

    .line 2070242
    iget-object v3, p0, LX/E1f;->a:LX/01T;

    sget-object v4, LX/01T;->PAA:LX/01T;

    if-ne v3, v4, :cond_9b

    .line 2070243
    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-virtual {v3, p2, v2}, LX/E1i;->g(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070244
    :cond_9b
    invoke-static {v2}, LX/E1i;->i(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070245
    :pswitch_56
    if-eqz p4, :cond_9c

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_9c

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_9d

    .line 2070246
    :cond_9c
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070247
    :cond_9d
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->s(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070248
    :pswitch_57
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_9e

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_9f

    .line 2070249
    :cond_9e
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070250
    :cond_9f
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->t(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    .line 2070251
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_PAGE_COMMUNITY_CONTENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v3, v2, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070252
    :pswitch_58
    if-eqz p4, :cond_a0

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_a0

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a0

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a1

    .line 2070253
    :cond_a0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070254
    :cond_a1
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p2, p4, v3, v4}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070255
    :pswitch_59
    if-eqz p4, :cond_a2

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_a2

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_a3

    .line 2070256
    :cond_a2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070257
    :cond_a3
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->q(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070258
    :pswitch_5a
    if-eqz p4, :cond_a4

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_a4

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_a5

    .line 2070259
    :cond_a4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070260
    :cond_a5
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->r(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070261
    :pswitch_5b
    if-eqz p4, :cond_a6

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_a6

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_a7

    .line 2070262
    :cond_a6
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070263
    :cond_a7
    invoke-static {p4}, LX/E1i;->o(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070264
    :pswitch_5c
    if-eqz p4, :cond_a8

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_a8

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_a9

    .line 2070265
    :cond_a8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070266
    :cond_a9
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, p2}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070267
    :pswitch_5d
    if-eqz p4, :cond_aa

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_aa

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_ab

    .line 2070268
    :cond_aa
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070269
    :cond_ab
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->w(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070270
    :pswitch_5e
    iget-object v2, p0, LX/E1f;->a:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-ne v2, v3, :cond_ac

    invoke-interface {p1}, LX/9rk;->ae()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v2

    if-eqz v2, :cond_ac

    invoke-interface {p1}, LX/9rk;->ae()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_ad

    .line 2070271
    :cond_ac
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070272
    :cond_ad
    invoke-interface {p1}, LX/9rk;->ae()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionSendMessageAsPageFieldsModel$ThreadKeyModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p4}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070273
    :pswitch_5f
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_ae

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_af

    .line 2070274
    :cond_ae
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070275
    :cond_af
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1i;->m(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070276
    :pswitch_60
    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    if-eqz v2, :cond_b0

    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b1

    .line 2070277
    :cond_b0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070278
    :cond_b1
    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->X()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/E1i;->c(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070279
    :pswitch_61
    invoke-interface {p1}, LX/9rk;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v2

    if-eqz v2, :cond_b2

    invoke-interface {p1}, LX/9rk;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b2

    invoke-interface {p1}, LX/9rk;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;->b()Z

    move-result v2

    if-eqz v2, :cond_b2

    invoke-interface {p1}, LX/9rk;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b3

    .line 2070280
    :cond_b2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070281
    :cond_b3
    invoke-interface {p1}, LX/9rk;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->j()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionGenericOpenGraphObjectActionFieldsModel$ActionOgObjectModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/E1i;->d(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070282
    :pswitch_62
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_b4

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b5

    .line 2070283
    :cond_b4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070284
    :cond_b5
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SHARE_EVENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v2, v4, :cond_b6

    sget-object v2, LX/Cfc;->SHARE_EVENT_TAP:LX/Cfc;

    :goto_5
    invoke-static {v3, v2}, LX/E1i;->d(Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    :cond_b6
    sget-object v2, LX/Cfc;->OPEN_COMPOSER_TAP:LX/Cfc;

    goto :goto_5

    .line 2070285
    :pswitch_63
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_b7

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b7

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b8

    .line 2070286
    :cond_b7
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070287
    :cond_b8
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v2, v3}, LX/E1i;->i(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070288
    :pswitch_64
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_b9

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_ba

    .line 2070289
    :cond_b9
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070290
    :cond_ba
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->S()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/Cfc;->SHARE_PHOTO_TO_PAGE_TAP:LX/Cfc;

    invoke-static {p2, v2, v3, v4, v5}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070291
    :pswitch_65
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_bb

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_bc

    .line 2070292
    :cond_bb
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070293
    :cond_bc
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-static {v2, v0, p4}, LX/E1i;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070294
    :pswitch_66
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p2, v3, v4}, LX/E1i;->j(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070295
    :pswitch_67
    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    if-eqz v2, :cond_bd

    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_be

    .line 2070296
    :cond_bd
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070297
    :cond_be
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->B()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionFundraiserActionFieldsModel$FundraiserInterfaceModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->X()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/E1i;->g(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070298
    :pswitch_68
    invoke-interface {p1}, LX/9rk;->A()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v2

    if-eqz v2, :cond_bf

    invoke-interface {p1}, LX/9rk;->A()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c0

    .line 2070299
    :cond_bf
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070300
    :cond_c0
    invoke-interface {p1}, LX/9rk;->t()Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    move-result-object v2

    if-nez v2, :cond_c1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;->DONATED:Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    .line 2070301
    :goto_6
    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->A()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewFundraiserSupportersActionFieldsModel$FundraiserModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, LX/E1i;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070302
    :cond_c1
    invoke-interface {p1}, LX/9rk;->t()Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    move-result-object v2

    goto :goto_6

    .line 2070303
    :pswitch_69
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_c2

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_c3

    .line 2070304
    :cond_c2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070305
    :cond_c3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->VIEW_PAGE_CHILD_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p2, v4, v5}, LX/E1i;->l(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    sget-object v4, LX/Cfc;->OPEN_PAGE_CHILD_LOCATIONS:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070306
    :pswitch_6a
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_c4

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c4

    invoke-interface {p1}, LX/9rk;->W()LX/9Zu;

    move-result-object v2

    if-eqz v2, :cond_c4

    invoke-interface {p1}, LX/9rk;->W()LX/9Zu;

    move-result-object v2

    invoke-interface {v2}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_c5

    .line 2070307
    :cond_c4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070308
    :cond_c5
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-interface {p1}, LX/9rk;->W()LX/9Zu;

    move-result-object v3

    invoke-interface {v3}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v4, v5, v3}, LX/E1i;->a(Landroid/content/Context;JLjava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070309
    :pswitch_6b
    invoke-interface {p1}, LX/9rk;->p()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v2

    if-eqz v2, :cond_c6

    invoke-interface {p1}, LX/9rk;->p()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c6

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v2

    if-eqz v2, :cond_c6

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c6

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_c7

    .line 2070310
    :cond_c6
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070311
    :cond_c7
    invoke-interface {p1}, LX/9rk;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->VIEW_COMMENT_AND_OPEN_COMPOSER:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 2070312
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->p()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->p()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel$CommentParentModel;

    move-result-object v4

    if-nez v4, :cond_c8

    const/4 v4, 0x0

    :goto_7
    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel$FeedbackModel;

    move-result-object v7

    if-nez v7, :cond_c9

    const/4 v7, 0x0

    :goto_8
    invoke-virtual/range {v2 .. v8}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    :cond_c8
    invoke-interface {p1}, LX/9rk;->p()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel$CommentParentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionViewCommentActionFieldsModel$CommentModel$CommentParentModel;->b()Ljava/lang/String;

    move-result-object v4

    goto :goto_7

    :cond_c9
    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel$FeedbackModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel$FeedbackModel;->a()Ljava/lang/String;

    move-result-object v7

    goto :goto_8

    .line 2070313
    :pswitch_6c
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p2, v3, v4}, LX/E1i;->j(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    .line 2070314
    invoke-static/range {p5 .. p5}, LX/2s8;->i(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2070315
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->VIEW_PAGE_EVENTS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v3, v2, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070316
    :pswitch_6d
    invoke-interface {p1}, LX/9rk;->D()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    if-eqz v2, :cond_ca

    invoke-interface {p1}, LX/9rk;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v2

    if-nez v2, :cond_cb

    .line 2070317
    :cond_ca
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070318
    :cond_cb
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->D()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->w()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$EventModel;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/E1i;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;LX/9rO;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070319
    :pswitch_6e
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-virtual {v2, p2}, LX/E1i;->b(Landroid/content/Context;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070320
    :pswitch_6f
    invoke-interface {p1}, LX/9rk;->C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v2

    if-eqz v2, :cond_cc

    invoke-interface {p1}, LX/9rk;->C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_cd

    .line 2070321
    :cond_cc
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070322
    :cond_cd
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1i;->f(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070323
    :pswitch_70
    invoke-interface {p1}, LX/9rk;->C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v2

    if-eqz v2, :cond_ce

    invoke-interface {p1}, LX/9rk;->C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_cf

    .line 2070324
    :cond_ce
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070325
    :cond_cf
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, LX/E1i;->a(Ljava/lang/String;Landroid/content/Context;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070326
    :pswitch_71
    invoke-interface {p1}, LX/9rk;->C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v2

    if-eqz v2, :cond_d0

    invoke-interface {p1}, LX/9rk;->C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_d1

    .line 2070327
    :cond_d0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070328
    :cond_d1
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->C()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$GroupModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, LX/E1i;->b(Ljava/lang/String;Landroid/content/Context;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070329
    :pswitch_72
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_d2

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_d2

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d3

    .line 2070330
    :cond_d2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070331
    :cond_d3
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p2, v3, v4}, LX/E1i;->k(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070332
    :pswitch_73
    invoke-interface {p1}, LX/9rk;->X()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/E1i;->h(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070333
    :pswitch_74
    invoke-interface {p1}, LX/9rk;->X()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->H()Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    move-result-object v4

    invoke-static {v3, v4}, LX/E1i;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070334
    :pswitch_75
    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v2

    if-eqz v2, :cond_d4

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_d5

    .line 2070335
    :cond_d4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070336
    :cond_d5
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/E1i;->h(Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070337
    :pswitch_76
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v2, v3}, LX/E1i;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070338
    :pswitch_77
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_d6

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d7

    .line 2070339
    :cond_d6
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070340
    :cond_d7
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->VIEW_PAGE_SERVICES:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, p2, v4, v5, v6}, LX/E1i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    sget-object v4, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070341
    :pswitch_78
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_d8

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_d9

    .line 2070342
    :cond_d8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070343
    :cond_d9
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/E1i;->j(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070344
    :pswitch_79
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_da

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_da

    invoke-interface {p1}, LX/9rk;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v2

    if-eqz v2, :cond_da

    invoke-interface {p1}, LX/9rk;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_db

    .line 2070345
    :cond_da
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070346
    :cond_db
    invoke-static/range {p5 .. p5}, LX/2s8;->i(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_dc

    .line 2070347
    sget-object v2, LX/Cfc;->ALBUM_TAP:LX/Cfc;

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->l()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenPageAlbumActionFragmentModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-static {p4, v0, v2, v3, v4}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070348
    :cond_dc
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070349
    :pswitch_7a
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_dd

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_de

    .line 2070350
    :cond_dd
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070351
    :cond_de
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, LX/E1i;->n(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070352
    :pswitch_7b
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_df

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_e0

    .line 2070353
    :cond_df
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070354
    :cond_e0
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_e1

    .line 2070355
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 2070356
    :cond_e1
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    sget-object v4, LX/Cfc;->VIEW_PROFILE_TAP:LX/Cfc;

    invoke-virtual {v2, v3, v4}, LX/E1i;->a(LX/5sc;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070357
    :sswitch_0
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v2, p2, v3, v0}, LX/E1i;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070358
    :sswitch_1
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1i;->f(Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070359
    :pswitch_7c
    if-nez p4, :cond_e2

    .line 2070360
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070361
    :cond_e2
    sget-object v2, LX/Cfc;->VIEW_PYML_TAP:LX/Cfc;

    invoke-static {p4, v2}, LX/E1i;->c(Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070362
    :pswitch_7d
    invoke-interface {p1}, LX/9rk;->aa()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;

    move-result-object v2

    .line 2070363
    if-eqz v2, :cond_e3

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_e4

    .line 2070364
    :cond_e3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070365
    :cond_e4
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel$FeedbackModel;

    move-result-object v3

    .line 2070366
    iget-object v4, p0, LX/E1f;->b:LX/E1i;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v3, :cond_e5

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$StoryModel$FeedbackModel;->a()Ljava/lang/String;

    move-result-object v2

    :goto_9
    sget-object v3, LX/Cfc;->VIEW_STORY_TAP:LX/Cfc;

    invoke-virtual {v4, v5, v6, v2, v3}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    :cond_e5
    const/4 v2, 0x0

    goto :goto_9

    .line 2070367
    :pswitch_7e
    iget-object v2, p0, LX/E1f;->b:LX/E1i;

    invoke-virtual {v2}, LX/E1i;->b()LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070368
    :pswitch_7f
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_e6

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e6

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e6

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e7

    .line 2070369
    :cond_e6
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070370
    :cond_e7
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/E1i;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070371
    :pswitch_80
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_e8

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_e9

    .line 2070372
    :cond_e8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070373
    :cond_e9
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/9rk;->S()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9rk;->n()Z

    move-result v5

    sget-object v6, LX/Cfc;->WRITE_POST_TO_PAGE_TAP:LX/Cfc;

    invoke-static {v2, v3, v4, v5, v6}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070374
    :pswitch_81
    invoke-static/range {p6 .. p6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_ea

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    if-eqz v2, :cond_ea

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_ea

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_eb

    .line 2070375
    :cond_ea
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070376
    :cond_eb
    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->k()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->WRITE_REVIEW_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {v0, v2, v3, v4}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    .line 2070377
    :pswitch_82
    invoke-static/range {p6 .. p6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_ec

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_ec

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_ec

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_ed

    .line 2070378
    :cond_ec
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2070379
    :cond_ed
    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/9rk;->J()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$PageModel;->d()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->WRITE_REVIEW_TAP:LX/Cfc;

    move-object/from16 v0, p6

    invoke-static {v0, v2, v3, v4}, LX/E1i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v2

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_62
        :pswitch_62
        :pswitch_63
        :pswitch_64
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_69
        :pswitch_6a
        :pswitch_6b
        :pswitch_6b
        :pswitch_6c
        :pswitch_6d
        :pswitch_6e
        :pswitch_6f
        :pswitch_70
        :pswitch_71
        :pswitch_72
        :pswitch_73
        :pswitch_74
        :pswitch_75
        :pswitch_76
        :pswitch_77
        :pswitch_78
        :pswitch_79
        :pswitch_7a
        :pswitch_7b
        :pswitch_7c
        :pswitch_7d
        :pswitch_7e
        :pswitch_7f
        :pswitch_80
        :pswitch_81
        :pswitch_82
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x403827a -> :sswitch_0
        0x41e065f -> :sswitch_1
    .end sparse-switch
.end method
