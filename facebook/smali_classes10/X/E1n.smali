.class public final LX/E1n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E1o;

.field public final synthetic b:LX/2kp;

.field public final synthetic c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;LX/E1o;LX/2kp;)V
    .locals 0

    .prologue
    .line 2071369
    iput-object p1, p0, LX/E1n;->c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    iput-object p2, p0, LX/E1n;->a:LX/E1o;

    iput-object p3, p0, LX/E1n;->b:LX/2kp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x2347e6d8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v9

    .line 2071370
    iget-object v0, p0, LX/E1n;->a:LX/E1o;

    iget-object v0, v0, LX/E1o;->b:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 2071371
    iget-object v0, p0, LX/E1n;->a:LX/E1o;

    iget-object v0, v0, LX/E1o;->b:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2071372
    :cond_0
    iget-object v0, p0, LX/E1n;->c:Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/common/BasicReactionActionPartDefinition;->a:LX/E1f;

    iget-object v1, p0, LX/E1n;->a:LX/E1o;

    iget-object v1, v1, LX/E1o;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v2, p0, LX/E1n;->b:LX/2kp;

    check-cast v2, LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/E1n;->a:LX/E1o;

    iget-object v3, v3, LX/E1o;->f:Ljava/lang/String;

    iget-object v4, p0, LX/E1n;->b:LX/2kp;

    invoke-interface {v4}, LX/2kp;->t()LX/2jY;

    move-result-object v4

    .line 2071373
    iget-object v5, v4, LX/2jY;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2071374
    iget-object v5, p0, LX/E1n;->b:LX/2kp;

    invoke-interface {v5}, LX/2kp;->t()LX/2jY;

    move-result-object v5

    .line 2071375
    iget-object v6, v5, LX/2jY;->b:Ljava/lang/String;

    move-object v5, v6

    .line 2071376
    iget-object v6, p0, LX/E1n;->a:LX/E1o;

    iget-object v6, v6, LX/E1o;->d:Ljava/lang/String;

    iget-object v7, p0, LX/E1n;->a:LX/E1o;

    iget-object v7, v7, LX/E1o;->e:Ljava/lang/String;

    iget-object v8, p0, LX/E1n;->b:LX/2kp;

    check-cast v8, LX/2kn;

    invoke-interface {v8}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v1

    .line 2071377
    iget-object v0, p0, LX/E1n;->b:LX/2kp;

    instance-of v0, v0, LX/2kk;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/E1n;->a:LX/E1o;

    iget-object v0, v0, LX/E1o;->h:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, v1, LX/Cfl;->d:Landroid/content/Intent;

    if-eqz v0, :cond_1

    .line 2071378
    iget-object v0, p0, LX/E1n;->a:LX/E1o;

    iget-object v0, v0, LX/E1o;->h:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v0}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v0

    .line 2071379
    if-eqz v0, :cond_1

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2071380
    iget-object v2, v1, LX/Cfl;->d:Landroid/content/Intent;

    const-string v3, "notification_id"

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/BCw;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2071381
    :cond_1
    iget-object v0, p0, LX/E1n;->a:LX/E1o;

    iget-object v0, v0, LX/E1o;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2071382
    iget-object v0, p0, LX/E1n;->b:LX/2kp;

    check-cast v0, LX/2km;

    iget-object v2, p0, LX/E1n;->a:LX/E1o;

    iget-object v2, v2, LX/E1o;->d:Ljava/lang/String;

    iget-object v3, p0, LX/E1n;->a:LX/E1o;

    iget-object v3, v3, LX/E1o;->e:Ljava/lang/String;

    iget-object v4, p0, LX/E1n;->a:LX/E1o;

    iget-object v4, v4, LX/E1o;->g:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v4, v1}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2071383
    :goto_0
    const v0, -0x5dc13987

    invoke-static {v0, v9}, LX/02F;->a(II)V

    return-void

    .line 2071384
    :cond_2
    iget-object v0, p0, LX/E1n;->b:LX/2kp;

    check-cast v0, LX/2km;

    iget-object v2, p0, LX/E1n;->a:LX/E1o;

    iget-object v2, v2, LX/E1o;->d:Ljava/lang/String;

    iget-object v3, p0, LX/E1n;->a:LX/E1o;

    iget-object v3, v3, LX/E1o;->e:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v1}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0
.end method
