.class public final LX/Env;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Enp;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/Enz;


# direct methods
.method public constructor <init>(LX/Enz;LX/0Px;)V
    .locals 0

    .prologue
    .line 2167604
    iput-object p1, p0, LX/Env;->b:LX/Enz;

    iput-object p2, p0, LX/Env;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2167605
    iget-object v0, p0, LX/Env;->b:LX/Enz;

    const/4 v1, 0x0

    .line 2167606
    iput-object v1, v0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167607
    iget-object v0, p0, LX/Env;->b:LX/Enz;

    const/4 v1, 0x1

    .line 2167608
    iput-boolean v1, v0, LX/Enz;->j:Z

    .line 2167609
    iget-object v0, p0, LX/Env;->b:LX/Enz;

    iget-object v1, p0, LX/Env;->a:LX/0Px;

    .line 2167610
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2167611
    iget-object p0, v0, LX/Enl;->b:LX/Eo3;

    move-object p0, p0

    .line 2167612
    new-instance p1, LX/Eo8;

    invoke-direct {p1, v2}, LX/Eo8;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 2167613
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2167614
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2167615
    iget-object v0, p0, LX/Env;->b:LX/Enz;

    const/4 v1, 0x0

    .line 2167616
    iput-object v1, v0, LX/Enz;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2167617
    return-void
.end method
