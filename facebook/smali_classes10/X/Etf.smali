.class public final enum LX/Etf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Etf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Etf;

.field public static final enum FETCH_REQUESTS_BADGE:LX/Etf;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2178387
    new-instance v0, LX/Etf;

    const-string v1, "FETCH_REQUESTS_BADGE"

    invoke-direct {v0, v1, v2}, LX/Etf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Etf;->FETCH_REQUESTS_BADGE:LX/Etf;

    .line 2178388
    const/4 v0, 0x1

    new-array v0, v0, [LX/Etf;

    sget-object v1, LX/Etf;->FETCH_REQUESTS_BADGE:LX/Etf;

    aput-object v1, v0, v2

    sput-object v0, LX/Etf;->$VALUES:[LX/Etf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2178389
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Etf;
    .locals 1

    .prologue
    .line 2178390
    const-class v0, LX/Etf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Etf;

    return-object v0
.end method

.method public static values()[LX/Etf;
    .locals 1

    .prologue
    .line 2178391
    sget-object v0, LX/Etf;->$VALUES:[LX/Etf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Etf;

    return-object v0
.end method
