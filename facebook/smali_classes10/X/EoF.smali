.class public LX/EoF;
.super LX/Emf;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Emf",
        "<",
        "LX/EoH;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/En7;

.field public final b:LX/En9;

.field private final c:LX/0ja;

.field public final d:LX/4mV;

.field public final e:Ljava/lang/Object;

.field public f:LX/Emf;

.field public g:Ljava/lang/Object;

.field private final h:LX/Eo3;

.field public final i:LX/Eo7;


# direct methods
.method public constructor <init>(LX/En7;Ljava/lang/Object;LX/Eo3;LX/En9;LX/0ja;Ljava/lang/Object;LX/4mV;)V
    .locals 3
    .param p1    # LX/En7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Eo3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/En9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0ja;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Object;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2168016
    invoke-direct {p0}, LX/Emf;-><init>()V

    .line 2168017
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/En7;

    iput-object v0, p0, LX/EoF;->a:LX/En7;

    .line 2168018
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eo3;

    iput-object v0, p0, LX/EoF;->h:LX/Eo3;

    .line 2168019
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/EoF;->e:Ljava/lang/Object;

    .line 2168020
    iput-object p4, p0, LX/EoF;->b:LX/En9;

    .line 2168021
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LX/EoF;->g:Ljava/lang/Object;

    .line 2168022
    iget-object v0, p0, LX/EoF;->b:LX/En9;

    check-cast p2, LX/Enn;

    .line 2168023
    iget-object v1, p2, LX/Enn;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2168024
    iget-object v2, p0, LX/EoF;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/En9;->a(Ljava/lang/String;Ljava/lang/Object;)LX/Emg;

    move-result-object v0

    iput-object v0, p0, LX/EoF;->f:LX/Emf;

    .line 2168025
    iput-object p5, p0, LX/EoF;->c:LX/0ja;

    .line 2168026
    iput-object p7, p0, LX/EoF;->d:LX/4mV;

    .line 2168027
    new-instance v0, LX/EoD;

    invoke-direct {v0, p0}, LX/EoD;-><init>(LX/EoF;)V

    iput-object v0, p0, LX/EoF;->i:LX/Eo7;

    .line 2168028
    return-void
.end method

.method public static a(LX/EoF;Ljava/lang/Class;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2168029
    iget-object v0, p0, LX/EoF;->c:LX/0ja;

    invoke-virtual {v0, p1}, LX/0ja;->a(Ljava/lang/Class;)Landroid/view/View;

    move-result-object v3

    .line 2168030
    if-eqz v3, :cond_0

    .line 2168031
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_0
    const-string v4, "Requested view of type %s but got view of type %s. Types must be identical."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v2

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v0, v4, v5}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2168032
    :cond_0
    return-object v3

    :cond_1
    move v0, v2

    .line 2168033
    goto :goto_0
.end method

.method public static a(LX/EoF;Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2168034
    if-nez p2, :cond_0

    .line 2168035
    invoke-static {p0, p1}, LX/EoF;->c(LX/EoF;Ljava/lang/Object;)LX/EoJ;

    move-result-object v0

    .line 2168036
    invoke-interface {v0, p3}, LX/EoJ;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 2168037
    const-string v0, "createView() shall not return null value!"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2168038
    :cond_0
    invoke-virtual {p2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2168039
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2168040
    iget-object v0, p0, LX/EoF;->f:LX/Emf;

    invoke-virtual {v0, p2}, LX/Eme;->a(Ljava/lang/Object;)V

    .line 2168041
    return-object p2
.end method

.method public static c(LX/EoF;Ljava/lang/Object;)LX/EoJ;
    .locals 1

    .prologue
    .line 2168042
    iget-object v0, p0, LX/EoF;->a:LX/En7;

    invoke-virtual {v0, p1}, LX/En7;->a(Ljava/lang/Object;)LX/EoJ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/EoH;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2168043
    const/4 v1, 0x1

    .line 2168044
    invoke-virtual {p1}, LX/EoH;->getCardViews()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2168045
    if-eqz v1, :cond_0

    .line 2168046
    iget-object v1, p0, LX/EoF;->f:LX/Emf;

    invoke-virtual {v1, v0}, LX/Eme;->b(Ljava/lang/Object;)V

    move v1, v2

    .line 2168047
    :cond_0
    iget-object v6, p0, LX/EoF;->c:LX/0ja;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    .line 2168048
    iget-object v8, p1, LX/EoH;->b:Lcom/facebook/widget/CustomFrameLayout;

    move-object v8, v8

    .line 2168049
    invoke-virtual {v6, v7, v0, v8}, LX/0ja;->a(Ljava/lang/Class;Landroid/view/View;LX/0h1;)V

    .line 2168050
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2168051
    :cond_1
    invoke-super {p0, p1}, LX/Emf;->b(Ljava/lang/Object;)V

    .line 2168052
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2168053
    iget-object v0, p0, LX/EoF;->h:LX/Eo3;

    .line 2168054
    iget-object v1, p0, LX/EoF;->i:LX/Eo7;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2168055
    iget-object v0, p0, LX/EoF;->h:LX/Eo3;

    .line 2168056
    iget-object v1, p0, LX/EoF;->i:LX/Eo7;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2168057
    invoke-virtual {p0}, LX/Eme;->a()LX/0am;

    move-result-object v0

    .line 2168058
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2168059
    :goto_0
    return-void

    .line 2168060
    :cond_0
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EoH;

    .line 2168061
    iput-object p0, v0, LX/EoH;->a:LX/EoF;

    .line 2168062
    iget-object v1, p0, LX/EoF;->g:Ljava/lang/Object;

    iget-object v2, p0, LX/EoF;->g:Ljava/lang/Object;

    invoke-static {p0, v2}, LX/EoF;->c(LX/EoF;Ljava/lang/Object;)LX/EoJ;

    move-result-object v2

    invoke-interface {v2}, LX/EoJ;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-static {p0, v2}, LX/EoF;->a(LX/EoF;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v2

    invoke-static {p0, v1, v2, v0}, LX/EoF;->a(LX/EoF;Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2168063
    iget-object v2, v0, LX/EoH;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomFrameLayout;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 2168064
    iget-object v2, v0, LX/EoH;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomFrameLayout;->removeAllViews()V

    .line 2168065
    :cond_1
    iget-object v2, v0, LX/EoH;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;)V

    .line 2168066
    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2168067
    check-cast p1, LX/EoH;

    invoke-virtual {p0, p1}, LX/EoF;->a(LX/EoH;)V

    return-void
.end method
