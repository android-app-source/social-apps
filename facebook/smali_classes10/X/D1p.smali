.class public LX/D1p;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/D1p;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1957406
    invoke-direct {p0}, LX/398;-><init>()V

    .line 1957407
    sget-object v0, LX/0ax;->bD:Ljava/lang/String;

    const-class v1, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 1957408
    const-string v0, "profile/video/create"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 1957409
    return-void
.end method

.method public static a(LX/0QB;)LX/D1p;
    .locals 3

    .prologue
    .line 1957410
    sget-object v0, LX/D1p;->a:LX/D1p;

    if-nez v0, :cond_1

    .line 1957411
    const-class v1, LX/D1p;

    monitor-enter v1

    .line 1957412
    :try_start_0
    sget-object v0, LX/D1p;->a:LX/D1p;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1957413
    if-eqz v2, :cond_0

    .line 1957414
    :try_start_1
    new-instance v0, LX/D1p;

    invoke-direct {v0}, LX/D1p;-><init>()V

    .line 1957415
    move-object v0, v0

    .line 1957416
    sput-object v0, LX/D1p;->a:LX/D1p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1957417
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1957418
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1957419
    :cond_1
    sget-object v0, LX/D1p;->a:LX/D1p;

    return-object v0

    .line 1957420
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1957421
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
