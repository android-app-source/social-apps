.class public LX/Dc5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Dc5;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;

.field public final c:LX/0kL;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2017861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2017862
    iput-object p1, p0, LX/Dc5;->a:LX/0tX;

    .line 2017863
    iput-object p2, p0, LX/Dc5;->b:LX/1Ck;

    .line 2017864
    iput-object p3, p0, LX/Dc5;->c:LX/0kL;

    .line 2017865
    return-void
.end method

.method public static a(LX/0QB;)LX/Dc5;
    .locals 6

    .prologue
    .line 2017866
    sget-object v0, LX/Dc5;->d:LX/Dc5;

    if-nez v0, :cond_1

    .line 2017867
    const-class v1, LX/Dc5;

    monitor-enter v1

    .line 2017868
    :try_start_0
    sget-object v0, LX/Dc5;->d:LX/Dc5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2017869
    if-eqz v2, :cond_0

    .line 2017870
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2017871
    new-instance p0, LX/Dc5;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-direct {p0, v3, v4, v5}, LX/Dc5;-><init>(LX/0tX;LX/1Ck;LX/0kL;)V

    .line 2017872
    move-object v0, p0

    .line 2017873
    sput-object v0, LX/Dc5;->d:LX/Dc5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2017874
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2017875
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2017876
    :cond_1
    sget-object v0, LX/Dc5;->d:LX/Dc5;

    return-object v0

    .line 2017877
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2017878
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/PageMenuTypeHelperMutationValues;
        .end annotation
    .end param

    .prologue
    .line 2017879
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0828fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v0

    .line 2017880
    new-instance v1, LX/Dc3;

    invoke-direct {v1, p0, v0, p1}, LX/Dc3;-><init>(LX/Dc5;LX/4BY;Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;)V

    .line 2017881
    new-instance v0, LX/4Hq;

    invoke-direct {v0}, LX/4Hq;-><init>()V

    .line 2017882
    const-string v2, "page_id"

    invoke-virtual {v0, v2, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017883
    move-object v0, v0

    .line 2017884
    const-string v2, "menu_type"

    invoke-virtual {v0, v2, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017885
    move-object v0, v0

    .line 2017886
    new-instance v2, LX/8Av;

    invoke-direct {v2}, LX/8Av;-><init>()V

    move-object v2, v2

    .line 2017887
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2017888
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2017889
    iget-object v2, p0, LX/Dc5;->b:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "task_key_mutate_menu_visibility"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Dc5;->a:LX/0tX;

    sget-object p1, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {v4, v0, p1}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v4, LX/Dc4;

    invoke-direct {v4, p0, v1}, LX/Dc4;-><init>(LX/Dc5;LX/Dc3;)V

    invoke-virtual {v2, v3, v0, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2017890
    return-void
.end method
