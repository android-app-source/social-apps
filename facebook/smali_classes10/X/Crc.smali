.class public LX/Crc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:F

.field public b:J

.field public c:[F

.field private d:J

.field public e:J

.field private f:J

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1941243
    const/high16 v0, 0x40000000    # 2.0f

    invoke-direct {p0, v0}, LX/Crc;-><init>(F)V

    .line 1941244
    return-void
.end method

.method public constructor <init>(F)V
    .locals 2

    .prologue
    .line 1941245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941246
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/Crc;->b:J

    .line 1941247
    const/4 v0, 0x0

    iput-object v0, p0, LX/Crc;->c:[F

    .line 1941248
    iput p1, p0, LX/Crc;->a:F

    .line 1941249
    return-void
.end method

.method public static a([F)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1941250
    move v0, v1

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 1941251
    aget v2, p0, v0

    const/high16 v3, 0x7fc00000    # NaNf

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 1941252
    :goto_1
    return v1

    .line 1941253
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1941254
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(J[F)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    .line 1941255
    invoke-static {p3}, LX/Crc;->a([F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1941256
    :goto_0
    return-void

    .line 1941257
    :cond_0
    iget-wide v0, p0, LX/Crc;->d:J

    .line 1941258
    iput-wide p1, p0, LX/Crc;->d:J

    .line 1941259
    iget-wide v2, p0, LX/Crc;->d:J

    sub-long v0, v2, v0

    iput-wide v0, p0, LX/Crc;->e:J

    .line 1941260
    iget-object v0, p0, LX/Crc;->c:[F

    if-eqz v0, :cond_1

    iget-wide v0, p0, LX/Crc;->e:J

    iget-wide v2, p0, LX/Crc;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 1941261
    :cond_1
    array-length v0, p3

    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v0

    iput-object v0, p0, LX/Crc;->c:[F

    .line 1941262
    iput-wide v4, p0, LX/Crc;->f:J

    .line 1941263
    const/4 v0, 0x1

    iput v0, p0, LX/Crc;->g:I

    .line 1941264
    iput-wide v4, p0, LX/Crc;->e:J

    goto :goto_0

    .line 1941265
    :cond_2
    iget-wide v0, p0, LX/Crc;->f:J

    iget-wide v2, p0, LX/Crc;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/Crc;->f:J

    .line 1941266
    iget-wide v0, p0, LX/Crc;->f:J

    long-to-float v0, v0

    iget v1, p0, LX/Crc;->g:I

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 1941267
    iget-wide v2, p0, LX/Crc;->e:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    .line 1941268
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, LX/Crc;->c:[F

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1941269
    iget-object v2, p0, LX/Crc;->c:[F

    aget v3, v2, v0

    aget v4, p3, v0

    iget-object v5, p0, LX/Crc;->c:[F

    aget v5, v5, v0

    sub-float/2addr v4, v5

    iget v5, p0, LX/Crc;->a:F

    mul-float/2addr v5, v1

    iget-wide v6, p0, LX/Crc;->e:J

    long-to-float v6, v6

    div-float/2addr v5, v6

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v2, v0

    .line 1941270
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1941271
    :cond_3
    iget v0, p0, LX/Crc;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Crc;->g:I

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1941272
    iget-object v0, p0, LX/Crc;->c:[F

    invoke-static {v0}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
