.class public final LX/DRR;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

.field public final synthetic b:LX/DRS;


# direct methods
.method public constructor <init>(LX/DRS;LX/DML;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V
    .locals 0

    .prologue
    .line 1998210
    iput-object p1, p0, LX/DRR;->b:LX/DRS;

    iput-object p3, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1998211
    new-instance v5, Landroid/text/SpannableStringBuilder;

    const-string v0, ""

    invoke-direct {v5, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1998212
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    if-eqz v0, :cond_9

    .line 1998213
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 1998214
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1998215
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1998216
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1998217
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1998218
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_8

    .line 1998219
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v7, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {v3, v0, v2, v7}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1998220
    :goto_2
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ae()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 1998221
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v0}, LX/DKD;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1998222
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ae()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v7, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$VisibilitySentenceModel$RangesModel;

    invoke-virtual {v5, v0, v2, v7}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$VisibilitySentenceModel$RangesModel;

    .line 1998223
    iget-object v2, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ae()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1998224
    invoke-virtual {v5, v2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$VisibilitySentenceModel$RangesModel;->j()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$VisibilitySentenceModel$RangesModel;->a()I

    move-result v0

    invoke-static {v1, v2, v0}, LX/DP5;->a(Ljava/lang/String;II)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    move-object v1, v0

    move-object v2, v6

    :goto_4
    move-object v0, p1

    .line 1998225
    check-cast v0, Lcom/facebook/groups/info/view/GroupBasicInfoView;

    iget-object v5, p0, LX/DRR;->b:LX/DRS;

    iget-object v5, v5, LX/DRS;->b:LX/3my;

    iget-object v6, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v6}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v5

    .line 1998226
    iget-object v6, v0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v6, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1998227
    iget-object v6, v0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v6, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1998228
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1998229
    iget-object v6, v0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v6}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v6

    check-cast v6, LX/1af;

    const v7, 0x7f020c6c

    invoke-virtual {v6, v7}, LX/1af;->b(I)V

    .line 1998230
    :goto_5
    iget-object v7, v0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->n:Landroid/widget/ImageView;

    if-eqz v5, :cond_b

    const/4 v6, 0x0

    :goto_6
    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1998231
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v0}, LX/DKD;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1998232
    new-instance v0, LX/DRQ;

    invoke-direct {v0, p0}, LX/DRQ;-><init>(LX/DRR;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1998233
    :goto_7
    return-void

    :cond_0
    move v0, v2

    .line 1998234
    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_1

    :cond_3
    move v0, v2

    goto/16 :goto_1

    .line 1998235
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1998236
    goto :goto_3

    .line 1998237
    :cond_5
    iget-object v0, p0, LX/DRR;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ae()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1998238
    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/DP5;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    move-object v1, v0

    move-object v2, v6

    goto :goto_4

    .line 1998239
    :cond_6
    invoke-virtual {p1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_7

    :cond_7
    move-object v1, v5

    move-object v2, v6

    goto :goto_4

    :cond_8
    move-object v3, v4

    goto/16 :goto_2

    :cond_9
    move-object v1, v5

    move-object v2, v4

    move-object v3, v4

    goto :goto_4

    .line 1998240
    :cond_a
    iget-object v6, v0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v6}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v6

    check-cast v6, LX/1af;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1998241
    iget-object v6, v0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/facebook/groups/info/view/GroupBasicInfoView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v7, v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_5

    .line 1998242
    :cond_b
    const/16 v6, 0x8

    goto :goto_6
.end method
