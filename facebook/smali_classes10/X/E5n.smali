.class public LX/E5n;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E5l;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E5n;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E5o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078920
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5n;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E5o;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078921
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078922
    iput-object p1, p0, LX/E5n;->b:LX/0Ot;

    .line 2078923
    return-void
.end method

.method public static a(LX/0QB;)LX/E5n;
    .locals 4

    .prologue
    .line 2078924
    sget-object v0, LX/E5n;->c:LX/E5n;

    if-nez v0, :cond_1

    .line 2078925
    const-class v1, LX/E5n;

    monitor-enter v1

    .line 2078926
    :try_start_0
    sget-object v0, LX/E5n;->c:LX/E5n;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078927
    if-eqz v2, :cond_0

    .line 2078928
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078929
    new-instance v3, LX/E5n;

    const/16 p0, 0x3135

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E5n;-><init>(LX/0Ot;)V

    .line 2078930
    move-object v0, v3

    .line 2078931
    sput-object v0, LX/E5n;->c:LX/E5n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078932
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078933
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078934
    :cond_1
    sget-object v0, LX/E5n;->c:LX/E5n;

    return-object v0

    .line 2078935
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078936
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2078937
    check-cast p2, LX/E5m;

    .line 2078938
    iget-object v0, p0, LX/E5n;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/E5m;->a:Ljava/lang/String;

    const/4 p0, 0x6

    const/4 p2, 0x2

    .line 2078939
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f010717

    invoke-interface {v1, p0, v2}, LX/1Dh;->p(II)LX/1Dh;

    move-result-object v1

    const v2, 0x7f01072b

    invoke-interface {v1, v2}, LX/1Dh;->U(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Dh;->d(Z)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v2

    const v3, 0x7f010715

    invoke-interface {v2, p0, v3}, LX/1Di;->f(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x7

    const p0, 0x7f0b1607

    invoke-interface {v2, v3, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2078940
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2078941
    invoke-static {}, LX/1dS;->b()V

    .line 2078942
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/E5l;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2078943
    new-instance v1, LX/E5m;

    invoke-direct {v1, p0}, LX/E5m;-><init>(LX/E5n;)V

    .line 2078944
    sget-object v2, LX/E5n;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E5l;

    .line 2078945
    if-nez v2, :cond_0

    .line 2078946
    new-instance v2, LX/E5l;

    invoke-direct {v2}, LX/E5l;-><init>()V

    .line 2078947
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/E5l;->a$redex0(LX/E5l;LX/1De;IILX/E5m;)V

    .line 2078948
    move-object v1, v2

    .line 2078949
    move-object v0, v1

    .line 2078950
    return-object v0
.end method
