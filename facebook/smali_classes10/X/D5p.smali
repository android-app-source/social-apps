.class public LX/D5p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/D5r;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/graphql/model/GraphQLVideo;

.field private final d:LX/1VK;

.field private final e:LX/D61;

.field private final f:LX/D4R;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;LX/D61;LX/D4R;)V
    .locals 3
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLVideo;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "LX/1VK;",
            "LX/D61;",
            "LX/D4R;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1964668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1964669
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964670
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1964671
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1964672
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1964673
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1964674
    invoke-static {v0}, LX/3ha;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1964675
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1964676
    :goto_0
    iput-object v0, p0, LX/D5p;->a:Ljava/lang/String;

    .line 1964677
    iput-object p1, p0, LX/D5p;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1964678
    iput-object p2, p0, LX/D5p;->c:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1964679
    iput-object p3, p0, LX/D5p;->d:LX/1VK;

    .line 1964680
    iput-object p4, p0, LX/D5p;->e:LX/D61;

    .line 1964681
    iput-object p5, p0, LX/D5p;->f:LX/D4R;

    .line 1964682
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1964683
    new-instance v0, LX/D5r;

    invoke-direct {v0}, LX/D5r;-><init>()V

    .line 1964684
    iget-object v1, p0, LX/D5p;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/182;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1964685
    iget-object v2, p0, LX/D5p;->d:LX/1VK;

    iget-object v3, p0, LX/D5p;->c:Lcom/facebook/graphql/model/GraphQLVideo;

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, LX/1VK;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;Ljava/lang/Integer;)LX/2oO;

    move-result-object v1

    .line 1964686
    iput-object v1, v0, LX/D5r;->a:LX/2oO;

    .line 1964687
    iget-object v1, p0, LX/D5p;->e:LX/D61;

    iget-object v2, p0, LX/D5p;->c:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v2

    .line 1964688
    new-instance v5, LX/D60;

    invoke-static {v1}, LX/04B;->a(LX/0QB;)LX/04B;

    move-result-object v8

    check-cast v8, LX/04B;

    invoke-static {v1}, LX/9Da;->b(LX/0QB;)LX/9Da;

    move-result-object v9

    check-cast v9, LX/9Da;

    invoke-static {v1}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v10

    check-cast v10, LX/1My;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    move-object v6, v2

    move-object v7, v0

    invoke-direct/range {v5 .. v11}, LX/D60;-><init>(Ljava/lang/String;LX/D5r;LX/04B;LX/9Da;LX/1My;LX/03V;)V

    .line 1964689
    move-object v1, v5

    .line 1964690
    iput-object v1, v0, LX/D5r;->b:LX/D60;

    .line 1964691
    iget-object v1, p0, LX/D5p;->f:LX/D4R;

    iget-object v2, p0, LX/D5p;->c:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v2

    .line 1964692
    new-instance v4, LX/D4Q;

    invoke-static {v1}, LX/D4i;->a(LX/0QB;)LX/D4i;

    move-result-object v3

    check-cast v3, LX/D4i;

    invoke-direct {v4, v2, v3}, LX/D4Q;-><init>(Ljava/lang/String;LX/D4i;)V

    .line 1964693
    move-object v1, v4

    .line 1964694
    iput-object v1, v0, LX/D5r;->c:LX/D4Q;

    .line 1964695
    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1964696
    iget-object v0, p0, LX/D5p;->a:Ljava/lang/String;

    return-object v0
.end method
