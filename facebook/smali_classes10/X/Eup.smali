.class public LX/Eup;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2lq;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:LX/5Oy;

.field private final b:J

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5Oy;JLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2180016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2180017
    iput-object p1, p0, LX/Eup;->a:LX/5Oy;

    .line 2180018
    iput-wide p2, p0, LX/Eup;->b:J

    .line 2180019
    iput-object p4, p0, LX/Eup;->c:Ljava/lang/String;

    .line 2180020
    iput-object p5, p0, LX/Eup;->d:Ljava/lang/String;

    .line 2180021
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2180015
    iget-wide v0, p0, LX/Eup;->b:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2180014
    iget-object v0, p0, LX/Eup;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2180011
    iget-object v0, p0, LX/Eup;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 2180013
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FriendRequestAttachmentModel does not support this operation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 2180012
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method
