.class public final LX/EEd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6e1;


# instance fields
.field public final synthetic a:LX/EEp;


# direct methods
.method public constructor <init>(LX/EEp;)V
    .locals 0

    .prologue
    .line 2094113
    iput-object p1, p0, LX/EEd;->a:LX/EEp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/dialog/MenuDialogItem;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2094114
    iget v0, p1, Lcom/facebook/messaging/dialog/MenuDialogItem;->a:I

    move v0, v0

    .line 2094115
    if-ne v0, v1, :cond_0

    .line 2094116
    iget-object v0, p1, Lcom/facebook/messaging/dialog/MenuDialogItem;->f:Landroid/os/Parcelable;

    move-object v0, v0

    .line 2094117
    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2094118
    iget-object v2, p0, LX/EEd;->a:LX/EEp;

    iget-object v2, v2, LX/EEp;->m:LX/Dpx;

    iget-object v3, p0, LX/EEd;->a:LX/EEp;

    iget-object v3, v3, LX/EEp;->n:LX/0gc;

    const/4 p1, 0x0

    .line 2094119
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2094120
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object p0

    sget-object p2, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne p0, p2, :cond_1

    const/4 p0, 0x1

    :goto_0
    invoke-static {p0}, LX/0PB;->checkArgument(Z)V

    .line 2094121
    new-instance p0, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2, p1}, Lcom/facebook/messaging/util/launchtimeline/LaunchTimelineHelper$ParsedUserData;-><init>(Ljava/lang/String;Z)V

    .line 2094122
    iget-object p1, v2, LX/Dpx;->b:LX/121;

    sget-object p2, LX/0yY;->VIEW_TIMELINE_INTERSTITIAL:LX/0yY;

    invoke-virtual {p1, p2, v3, p0}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    .line 2094123
    move v0, v1

    .line 2094124
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move p0, p1

    .line 2094125
    goto :goto_0
.end method
