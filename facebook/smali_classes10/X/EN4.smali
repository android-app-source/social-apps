.class public LX/EN4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2111868
    const v0, 0x7f0e0129

    sput v0, LX/EN4;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2111870
    return-void
.end method

.method public static a(LX/0QB;)LX/EN4;
    .locals 3

    .prologue
    .line 2111871
    const-class v1, LX/EN4;

    monitor-enter v1

    .line 2111872
    :try_start_0
    sget-object v0, LX/EN4;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111873
    sput-object v2, LX/EN4;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111874
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111875
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2111876
    new-instance v0, LX/EN4;

    invoke-direct {v0}, LX/EN4;-><init>()V

    .line 2111877
    move-object v0, v0

    .line 2111878
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111879
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EN4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111880
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111881
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
