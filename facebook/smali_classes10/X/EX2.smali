.class public final LX/EX2;
.super LX/EX1;
.source ""

# interfaces
.implements LX/EWz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EX1",
        "<",
        "LX/EX2;",
        ">;",
        "LX/EWz;"
    }
.end annotation


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EX2;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EX2;


# instance fields
.field public allowAlias_:Z

.field public bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public uninterpretedOption_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2132260
    new-instance v0, LX/EWw;

    invoke-direct {v0}, LX/EWw;-><init>()V

    sput-object v0, LX/EX2;->a:LX/EWZ;

    .line 2132261
    new-instance v0, LX/EX2;

    invoke-direct {v0}, LX/EX2;-><init>()V

    .line 2132262
    sput-object v0, LX/EX2;->c:LX/EX2;

    invoke-direct {v0}, LX/EX2;->o()V

    .line 2132263
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2132264
    invoke-direct {p0}, LX/EX1;-><init>()V

    .line 2132265
    iput-byte v0, p0, LX/EX2;->memoizedIsInitialized:B

    .line 2132266
    iput v0, p0, LX/EX2;->memoizedSerializedSize:I

    .line 2132267
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2132268
    iput-object v0, p0, LX/EX2;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v6, 0x2

    .line 2132269
    invoke-direct {p0}, LX/EX1;-><init>()V

    .line 2132270
    iput-byte v1, p0, LX/EX2;->memoizedIsInitialized:B

    .line 2132271
    iput v1, p0, LX/EX2;->memoizedSerializedSize:I

    .line 2132272
    invoke-direct {p0}, LX/EX2;->o()V

    .line 2132273
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v3

    move v1, v0

    .line 2132274
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 2132275
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v4

    .line 2132276
    sparse-switch v4, :sswitch_data_0

    .line 2132277
    invoke-virtual {p0, p1, v3, p2, v4}, LX/EX1;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 2132278
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 2132279
    goto :goto_0

    .line 2132280
    :sswitch_1
    iget v4, p0, LX/EX2;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/EX2;->bitField0_:I

    .line 2132281
    invoke-virtual {p1}, LX/EWd;->i()Z

    move-result v4

    iput-boolean v4, p0, LX/EX2;->allowAlias_:Z
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2132282
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 2132283
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2132284
    move-object v0, v0

    .line 2132285
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2132286
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_1

    .line 2132287
    iget-object v1, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    .line 2132288
    :cond_1
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EX2;->unknownFields:LX/EZQ;

    .line 2132289
    invoke-virtual {p0}, LX/EX1;->E()V

    throw v0

    .line 2132290
    :sswitch_2
    and-int/lit8 v4, v0, 0x2

    if-eq v4, v6, :cond_2

    .line 2132291
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    .line 2132292
    or-int/lit8 v0, v0, 0x2

    .line 2132293
    :cond_2
    iget-object v4, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    sget-object v5, LX/EYB;->a:LX/EWZ;

    invoke-virtual {p1, v5, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2132294
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 2132295
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2132296
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2132297
    move-object v0, v2

    .line 2132298
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2132299
    :cond_3
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 2132300
    iget-object v0, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    .line 2132301
    :cond_4
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EX2;->unknownFields:LX/EZQ;

    .line 2132302
    invoke-virtual {p0}, LX/EX1;->E()V

    .line 2132303
    return-void

    .line 2132304
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1f3a -> :sswitch_2
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWy",
            "<",
            "LX/EX2;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 2132305
    invoke-direct {p0, p1}, LX/EX1;-><init>(LX/EWy;)V

    .line 2132306
    iput-byte v0, p0, LX/EX2;->memoizedIsInitialized:B

    .line 2132307
    iput v0, p0, LX/EX2;->memoizedSerializedSize:I

    .line 2132308
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EX2;->unknownFields:LX/EZQ;

    .line 2132309
    return-void
.end method

.method public static a(LX/EX2;)LX/EX0;
    .locals 1

    .prologue
    .line 2132310
    invoke-static {}, LX/EX0;->x()LX/EX0;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EX0;->a(LX/EX2;)LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 2132322
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EX2;->allowAlias_:Z

    .line 2132323
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    .line 2132324
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2132325
    new-instance v0, LX/EX0;

    invoke-direct {v0, p1}, LX/EX0;-><init>(LX/EYd;)V

    .line 2132326
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    .line 2132327
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2132328
    invoke-virtual {p0}, LX/EX1;->G()LX/EYf;

    move-result-object v2

    .line 2132329
    iget v0, p0, LX/EX2;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2132330
    const/4 v0, 0x2

    iget-boolean v1, p0, LX/EX2;->allowAlias_:Z

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(IZ)V

    .line 2132331
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2132332
    const/16 v3, 0x3e7

    iget-object v0, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2132333
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2132334
    :cond_1
    const/high16 v0, 0x20000000

    invoke-virtual {v2, v0, p1}, LX/EYf;->a(ILX/EWf;)V

    .line 2132335
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2132336
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2132247
    iget-byte v0, p0, LX/EX2;->memoizedIsInitialized:B

    .line 2132248
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2132249
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 2132250
    :goto_1
    iget-object v3, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move v3, v3

    .line 2132251
    if-ge v0, v3, :cond_3

    .line 2132252
    iget-object v3, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EYB;

    move-object v3, v3

    .line 2132253
    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2132254
    iput-byte v1, p0, LX/EX2;->memoizedIsInitialized:B

    goto :goto_0

    .line 2132255
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2132256
    :cond_3
    invoke-virtual {p0}, LX/EX1;->F()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2132257
    iput-byte v1, p0, LX/EX2;->memoizedIsInitialized:B

    goto :goto_0

    .line 2132258
    :cond_4
    iput-byte v2, p0, LX/EX2;->memoizedIsInitialized:B

    move v1, v2

    .line 2132259
    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2132311
    iget v0, p0, LX/EX2;->memoizedSerializedSize:I

    .line 2132312
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2132313
    :goto_0
    return v0

    .line 2132314
    :cond_0
    iget v0, p0, LX/EX2;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 2132315
    const/4 v0, 0x2

    iget-boolean v2, p0, LX/EX2;->allowAlias_:Z

    invoke-static {v0, v2}, LX/EWf;->b(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v0

    .line 2132316
    :goto_2
    iget-object v0, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2132317
    const/16 v3, 0x3e7

    iget-object v0, p0, LX/EX2;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2132318
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2132319
    :cond_1
    invoke-virtual {p0}, LX/EX1;->H()I

    move-result v0

    add-int/2addr v0, v2

    .line 2132320
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2132321
    iput v0, p0, LX/EX2;->memoizedSerializedSize:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2132246
    iget-object v0, p0, LX/EX2;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2132245
    sget-object v0, LX/EYC;->z:LX/EYn;

    const-class v1, LX/EX2;

    const-class v2, LX/EX0;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EX2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2132244
    sget-object v0, LX/EX2;->a:LX/EWZ;

    return-object v0
.end method

.method public final l()LX/EX0;
    .locals 1

    .prologue
    .line 2132243
    invoke-static {p0}, LX/EX2;->a(LX/EX2;)LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2132238
    invoke-virtual {p0}, LX/EX2;->l()LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2132242
    invoke-static {}, LX/EX0;->x()LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2132241
    invoke-virtual {p0}, LX/EX2;->l()LX/EX0;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2132240
    sget-object v0, LX/EX2;->c:LX/EX2;

    return-object v0
.end method

.method public final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2132239
    invoke-super {p0}, LX/EX1;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
