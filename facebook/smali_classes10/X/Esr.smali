.class public LX/Esr;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Esp;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ess;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2176515
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Esr;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Ess;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176516
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2176517
    iput-object p1, p0, LX/Esr;->b:LX/0Ot;

    .line 2176518
    return-void
.end method

.method public static a(LX/0QB;)LX/Esr;
    .locals 4

    .prologue
    .line 2176519
    const-class v1, LX/Esr;

    monitor-enter v1

    .line 2176520
    :try_start_0
    sget-object v0, LX/Esr;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176521
    sput-object v2, LX/Esr;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176522
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176523
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176524
    new-instance v3, LX/Esr;

    const/16 p0, 0x1f31

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Esr;-><init>(LX/0Ot;)V

    .line 2176525
    move-object v0, v3

    .line 2176526
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176527
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Esr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176528
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176529
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;Landroid/view/View$OnClickListener;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Landroid/view/View$OnClickListener;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2176530
    const v0, 0x25e6fd3f

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2176531
    check-cast p2, LX/Esq;

    .line 2176532
    iget-object v0, p0, LX/Esr;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ess;

    iget-object v1, p2, LX/Esq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Esq;->b:LX/1Pm;

    .line 2176533
    iget-object v3, v0, LX/Ess;->h:LX/0Uh;

    const/16 v4, 0x4dd

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    .line 2176534
    if-eqz v3, :cond_0

    .line 2176535
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2176536
    check-cast v3, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/Esx;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2176537
    iget-object v3, v0, LX/Ess;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, v0, LX/Ess;->d:LX/C51;

    iget-object v5, v0, LX/Ess;->j:LX/1Cn;

    iget-object v6, v0, LX/Ess;->f:LX/03V;

    const-string v7, "promotion"

    iget-object v8, v0, LX/Ess;->g:LX/1Nq;

    iget-object v9, v0, LX/Ess;->e:LX/1Kf;

    iget-object v10, v0, LX/Ess;->i:LX/Es5;

    move-object v11, v1

    move-object v12, v2

    invoke-static/range {v3 .. v12}, LX/Esx;->a(Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Cn;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;LX/Es5;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 2176538
    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2176539
    new-instance v5, LX/Esg;

    iget-object v6, v0, LX/Ess;->b:Landroid/content/res/Resources;

    const v7, 0x7f080fd9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0219c9

    const v8, 0x7f0a00e6

    .line 2176540
    const v9, 0x25e6fd3f

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v3, v10, v11

    invoke-static {p1, v9, v10}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v3, v9

    .line 2176541
    invoke-direct {v5, v6, v7, v8, v3}, LX/Esg;-><init>(Ljava/lang/CharSequence;IILX/1dQ;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2176542
    iget-object v3, v0, LX/Ess;->a:LX/Esf;

    invoke-virtual {v3, p1}, LX/Esf;->c(LX/1De;)LX/Esd;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/Esd;->a(Ljava/util/List;)LX/Esd;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2176543
    return-object v0

    .line 2176544
    :cond_0
    iget-object v3, v0, LX/Ess;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, v0, LX/Ess;->d:LX/C51;

    iget-object v5, v0, LX/Ess;->j:LX/1Cn;

    iget-object v6, v0, LX/Ess;->f:LX/03V;

    const-string v7, "promotion"

    iget-object v8, v0, LX/Ess;->g:LX/1Nq;

    iget-object v9, v0, LX/Ess;->e:LX/1Kf;

    move-object v10, v1

    move-object v11, v2

    invoke-static/range {v3 .. v11}, LX/Esx;->a(Lcom/facebook/content/SecureContextHelper;LX/C51;LX/1Cn;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Landroid/view/View$OnClickListener;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2176545
    invoke-static {}, LX/1dS;->b()V

    .line 2176546
    iget v0, p1, LX/1dQ;->b:I

    .line 2176547
    packed-switch v0, :pswitch_data_0

    .line 2176548
    :goto_0
    return-object v3

    .line 2176549
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2176550
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Landroid/view/View$OnClickListener;

    .line 2176551
    iget-object p1, p0, LX/Esr;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2176552
    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2176553
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x25e6fd3f
        :pswitch_0
    .end packed-switch
.end method
