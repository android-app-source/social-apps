.class public final enum LX/Dyr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dyr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dyr;

.field public static final enum INAPPROPRIATE_CONTENT:LX/Dyr;

.field public static final enum NOT_A_PUBLIC_PLACE:LX/Dyr;

.field public static final enum REPORT_DUPLICATES:LX/Dyr;

.field public static final enum SUGGEST_EDITS:LX/Dyr;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2064861
    new-instance v0, LX/Dyr;

    const-string v1, "SUGGEST_EDITS"

    invoke-direct {v0, v1, v2}, LX/Dyr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dyr;->SUGGEST_EDITS:LX/Dyr;

    .line 2064862
    new-instance v0, LX/Dyr;

    const-string v1, "REPORT_DUPLICATES"

    invoke-direct {v0, v1, v3}, LX/Dyr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dyr;->REPORT_DUPLICATES:LX/Dyr;

    .line 2064863
    new-instance v0, LX/Dyr;

    const-string v1, "INAPPROPRIATE_CONTENT"

    invoke-direct {v0, v1, v4}, LX/Dyr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dyr;->INAPPROPRIATE_CONTENT:LX/Dyr;

    .line 2064864
    new-instance v0, LX/Dyr;

    const-string v1, "NOT_A_PUBLIC_PLACE"

    invoke-direct {v0, v1, v5}, LX/Dyr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dyr;->NOT_A_PUBLIC_PLACE:LX/Dyr;

    .line 2064865
    const/4 v0, 0x4

    new-array v0, v0, [LX/Dyr;

    sget-object v1, LX/Dyr;->SUGGEST_EDITS:LX/Dyr;

    aput-object v1, v0, v2

    sget-object v1, LX/Dyr;->REPORT_DUPLICATES:LX/Dyr;

    aput-object v1, v0, v3

    sget-object v1, LX/Dyr;->INAPPROPRIATE_CONTENT:LX/Dyr;

    aput-object v1, v0, v4

    sget-object v1, LX/Dyr;->NOT_A_PUBLIC_PLACE:LX/Dyr;

    aput-object v1, v0, v5

    sput-object v0, LX/Dyr;->$VALUES:[LX/Dyr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2064866
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dyr;
    .locals 1

    .prologue
    .line 2064867
    const-class v0, LX/Dyr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dyr;

    return-object v0
.end method

.method public static values()[LX/Dyr;
    .locals 1

    .prologue
    .line 2064868
    sget-object v0, LX/Dyr;->$VALUES:[LX/Dyr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dyr;

    return-object v0
.end method
