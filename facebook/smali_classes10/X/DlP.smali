.class public final LX/DlP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2037929
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2037930
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2037931
    :goto_0
    return v1

    .line 2037932
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2037933
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2037934
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2037935
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2037936
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2037937
    const-string v3, "catalog_items"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2037938
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2037939
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_a

    .line 2037940
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2037941
    :goto_2
    move v0, v2

    .line 2037942
    goto :goto_1

    .line 2037943
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2037944
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2037945
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2037946
    :cond_4
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_8

    .line 2037947
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2037948
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2037949
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_4

    if-eqz v6, :cond_4

    .line 2037950
    const-string v7, "count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2037951
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v3

    goto :goto_3

    .line 2037952
    :cond_5
    const-string v7, "edges"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2037953
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2037954
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_6

    .line 2037955
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_6

    .line 2037956
    const/4 v7, 0x0

    .line 2037957
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_e

    .line 2037958
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2037959
    :goto_5
    move v6, v7

    .line 2037960
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2037961
    :cond_6
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 2037962
    goto :goto_3

    .line 2037963
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2037964
    :cond_8
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2037965
    if-eqz v0, :cond_9

    .line 2037966
    invoke-virtual {p1, v2, v5, v2}, LX/186;->a(III)V

    .line 2037967
    :cond_9
    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 2037968
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_2

    :cond_a
    move v0, v2

    move v4, v2

    move v5, v2

    goto :goto_3

    .line 2037969
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2037970
    :cond_c
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_d

    .line 2037971
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2037972
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2037973
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_c

    if-eqz v8, :cond_c

    .line 2037974
    const-string v9, "node"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 2037975
    invoke-static {p0, p1}, LX/DlQ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_6

    .line 2037976
    :cond_d
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2037977
    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 2037978
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_5

    :cond_e
    move v6, v7

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2037979
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2037980
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2037981
    if-eqz v0, :cond_4

    .line 2037982
    const-string v1, "catalog_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2037983
    const/4 v1, 0x0

    .line 2037984
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2037985
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2037986
    if-eqz v1, :cond_0

    .line 2037987
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2037988
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2037989
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2037990
    if-eqz v1, :cond_3

    .line 2037991
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2037992
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2037993
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 2037994
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 2037995
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2037996
    const/4 p1, 0x0

    invoke-virtual {p0, v3, p1}, LX/15i;->g(II)I

    move-result p1

    .line 2037997
    if-eqz p1, :cond_1

    .line 2037998
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2037999
    invoke-static {p0, p1, p2, p3}, LX/DlQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2038000
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2038001
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2038002
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2038003
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2038004
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2038005
    return-void
.end method
