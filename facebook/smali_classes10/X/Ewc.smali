.class public final LX/Ewc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1vq",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 0

    .prologue
    .line 2183323
    iput-object p1, p0, LX/Ewc;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2183311
    iget-object v0, p0, LX/Ewc;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aj:LX/EuY;

    invoke-virtual {v0}, LX/EuY;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2183312
    :cond_0
    :goto_0
    return-void

    .line 2183313
    :cond_1
    iget-object v0, p0, LX/Ewc;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-static {v0, p1, p3, p4}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->a$redex0(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;LX/0Px;LX/2kM;LX/2kM;)Z

    move-result v0

    .line 2183314
    invoke-interface {p4}, LX/2kM;->b()LX/2nj;

    move-result-object v1

    .line 2183315
    iget-boolean v2, v1, LX/2nj;->d:Z

    move v1, v2

    .line 2183316
    if-nez v1, :cond_2

    .line 2183317
    iget-object v2, p0, LX/Ewc;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v2, v2, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2183318
    iget-object v2, p0, LX/Ewc;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-static {v2}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183319
    :cond_2
    if-nez v0, :cond_3

    invoke-interface {p3}, LX/2kM;->b()LX/2nj;

    move-result-object v0

    .line 2183320
    iget-boolean v2, v0, LX/2nj;->d:Z

    move v0, v2

    .line 2183321
    if-eq v1, v0, :cond_0

    .line 2183322
    :cond_3
    iget-object v0, p0, LX/Ewc;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    goto :goto_0
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2183324
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2183309
    iget-object v0, p0, LX/Ewc;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EwG;->a(Z)V

    .line 2183310
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2183307
    iget-object v0, p0, LX/Ewc;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    const-string v1, "FETCH_PYMK"

    invoke-static {v0, p4, v1}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->a$redex0(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2183308
    return-void
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2183304
    iget-object v0, p0, LX/Ewc;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EwG;->a(Z)V

    .line 2183305
    iget-object v0, p0, LX/Ewc;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->g()V

    .line 2183306
    return-void
.end method
