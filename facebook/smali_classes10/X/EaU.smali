.class public LX/EaU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2141503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(B)I
    .locals 3

    .prologue
    .line 2141504
    int-to-long v0, p0

    .line 2141505
    const/16 v2, 0x3f

    ushr-long/2addr v0, v2

    .line 2141506
    long-to-int v0, v0

    return v0
.end method

.method private static a(BB)I
    .locals 1

    .prologue
    .line 2141507
    xor-int v0, p0, p1

    .line 2141508
    add-int/lit8 v0, v0, -0x1

    .line 2141509
    ushr-int/lit8 v0, v0, 0x1f

    .line 2141510
    return v0
.end method

.method public static a(LX/EaI;[B)V
    .locals 11

    .prologue
    const/16 v10, 0x3f

    const/16 v9, 0x40

    const/4 v0, 0x0

    .line 2141511
    new-array v3, v9, [B

    .line 2141512
    new-instance v4, LX/EaC;

    invoke-direct {v4}, LX/EaC;-><init>()V

    .line 2141513
    new-instance v5, LX/EaF;

    invoke-direct {v5}, LX/EaF;-><init>()V

    .line 2141514
    new-instance v6, LX/EaO;

    invoke-direct {v6}, LX/EaO;-><init>()V

    move v1, v0

    .line 2141515
    :goto_0
    const/16 v2, 0x20

    if-ge v1, v2, :cond_0

    .line 2141516
    mul-int/lit8 v2, v1, 0x2

    add-int/lit8 v2, v2, 0x0

    aget-byte v7, p1, v1

    ushr-int/lit8 v7, v7, 0x0

    and-int/lit8 v7, v7, 0xf

    int-to-byte v7, v7

    aput-byte v7, v3, v2

    .line 2141517
    mul-int/lit8 v2, v1, 0x2

    add-int/lit8 v2, v2, 0x1

    aget-byte v7, p1, v1

    ushr-int/lit8 v7, v7, 0x4

    and-int/lit8 v7, v7, 0xf

    int-to-byte v7, v7

    aput-byte v7, v3, v2

    .line 2141518
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    move v2, v0

    .line 2141519
    :goto_1
    if-ge v2, v10, :cond_1

    .line 2141520
    aget-byte v7, v3, v2

    add-int/2addr v1, v7

    int-to-byte v1, v1

    aput-byte v1, v3, v2

    .line 2141521
    aget-byte v1, v3, v2

    add-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    .line 2141522
    shr-int/lit8 v1, v1, 0x4

    int-to-byte v1, v1

    .line 2141523
    aget-byte v7, v3, v2

    shl-int/lit8 v8, v1, 0x4

    sub-int/2addr v7, v8

    int-to-byte v7, v7

    aput-byte v7, v3, v2

    .line 2141524
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2141525
    :cond_1
    aget-byte v2, v3, v10

    add-int/2addr v1, v2

    int-to-byte v1, v1

    aput-byte v1, v3, v10

    .line 2141526
    iget-object v1, p0, LX/EaI;->a:[I

    invoke-static {v1}, LX/EZo;->a([I)V

    .line 2141527
    iget-object v1, p0, LX/EaI;->b:[I

    invoke-static {v1}, LX/EZp;->a([I)V

    .line 2141528
    iget-object v1, p0, LX/EaI;->c:[I

    invoke-static {v1}, LX/EZp;->a([I)V

    .line 2141529
    iget-object v1, p0, LX/EaI;->d:[I

    invoke-static {v1}, LX/EZo;->a([I)V

    .line 2141530
    const/4 v1, 0x1

    :goto_2
    if-ge v1, v9, :cond_2

    .line 2141531
    div-int/lit8 v2, v1, 0x2

    aget-byte v7, v3, v1

    invoke-static {v6, v2, v7}, LX/EaU;->a(LX/EaO;IB)V

    .line 2141532
    invoke-static {v4, p0, v6}, LX/EaA;->a(LX/EaC;LX/EaI;LX/EaO;)V

    invoke-static {p0, v4}, LX/EaE;->a(LX/EaI;LX/EaC;)V

    .line 2141533
    add-int/lit8 v1, v1, 0x2

    goto :goto_2

    .line 2141534
    :cond_2
    invoke-static {v4, p0}, LX/EaK;->a(LX/EaC;LX/EaI;)V

    invoke-static {v5, v4}, LX/EaD;->a(LX/EaF;LX/EaC;)V

    .line 2141535
    invoke-static {v4, v5}, LX/EaH;->a(LX/EaC;LX/EaF;)V

    invoke-static {v5, v4}, LX/EaD;->a(LX/EaF;LX/EaC;)V

    .line 2141536
    invoke-static {v4, v5}, LX/EaH;->a(LX/EaC;LX/EaF;)V

    invoke-static {v5, v4}, LX/EaD;->a(LX/EaF;LX/EaC;)V

    .line 2141537
    invoke-static {v4, v5}, LX/EaH;->a(LX/EaC;LX/EaF;)V

    invoke-static {p0, v4}, LX/EaE;->a(LX/EaI;LX/EaC;)V

    .line 2141538
    :goto_3
    if-ge v0, v9, :cond_3

    .line 2141539
    div-int/lit8 v1, v0, 0x2

    aget-byte v2, v3, v0

    invoke-static {v6, v1, v2}, LX/EaU;->a(LX/EaO;IB)V

    .line 2141540
    invoke-static {v4, p0, v6}, LX/EaA;->a(LX/EaC;LX/EaI;LX/EaO;)V

    invoke-static {p0, v4}, LX/EaE;->a(LX/EaI;LX/EaC;)V

    .line 2141541
    add-int/lit8 v0, v0, 0x2

    goto :goto_3

    .line 2141542
    :cond_3
    return-void
.end method

.method private static a(LX/EaO;IB)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x7

    .line 2141543
    if-gt p1, v7, :cond_0

    sget-object v0, LX/EaQ;->a:[[LX/EaO;

    .line 2141544
    :goto_0
    new-instance v1, LX/EaO;

    invoke-direct {v1}, LX/EaO;-><init>()V

    .line 2141545
    invoke-static {p2}, LX/EaU;->a(B)I

    move-result v2

    .line 2141546
    neg-int v3, v2

    and-int/2addr v3, p2

    shl-int/lit8 v3, v3, 0x1

    sub-int v3, p2, v3

    .line 2141547
    iget-object v4, p0, LX/EaO;->a:[I

    invoke-static {v4}, LX/EZp;->a([I)V

    .line 2141548
    iget-object v4, p0, LX/EaO;->b:[I

    invoke-static {v4}, LX/EZp;->a([I)V

    .line 2141549
    iget-object v4, p0, LX/EaO;->c:[I

    invoke-static {v4}, LX/EZo;->a([I)V

    .line 2141550
    aget-object v4, v0, p1

    const/4 v5, 0x0

    aget-object v4, v4, v5

    int-to-byte v5, v3

    invoke-static {v5, v6}, LX/EaU;->a(BB)I

    move-result v5

    invoke-static {p0, v4, v5}, LX/EaU;->a(LX/EaO;LX/EaO;I)V

    .line 2141551
    aget-object v4, v0, p1

    aget-object v4, v4, v6

    int-to-byte v5, v3

    invoke-static {v5, v8}, LX/EaU;->a(BB)I

    move-result v5

    invoke-static {p0, v4, v5}, LX/EaU;->a(LX/EaO;LX/EaO;I)V

    .line 2141552
    aget-object v4, v0, p1

    aget-object v4, v4, v8

    int-to-byte v5, v3

    invoke-static {v5, v9}, LX/EaU;->a(BB)I

    move-result v5

    invoke-static {p0, v4, v5}, LX/EaU;->a(LX/EaO;LX/EaO;I)V

    .line 2141553
    aget-object v4, v0, p1

    aget-object v4, v4, v9

    int-to-byte v5, v3

    invoke-static {v5, v10}, LX/EaU;->a(BB)I

    move-result v5

    invoke-static {p0, v4, v5}, LX/EaU;->a(LX/EaO;LX/EaO;I)V

    .line 2141554
    aget-object v4, v0, p1

    aget-object v4, v4, v10

    int-to-byte v5, v3

    const/4 v6, 0x5

    invoke-static {v5, v6}, LX/EaU;->a(BB)I

    move-result v5

    invoke-static {p0, v4, v5}, LX/EaU;->a(LX/EaO;LX/EaO;I)V

    .line 2141555
    aget-object v4, v0, p1

    const/4 v5, 0x5

    aget-object v4, v4, v5

    int-to-byte v5, v3

    const/4 v6, 0x6

    invoke-static {v5, v6}, LX/EaU;->a(BB)I

    move-result v5

    invoke-static {p0, v4, v5}, LX/EaU;->a(LX/EaO;LX/EaO;I)V

    .line 2141556
    aget-object v4, v0, p1

    const/4 v5, 0x6

    aget-object v4, v4, v5

    int-to-byte v5, v3

    invoke-static {v5, v7}, LX/EaU;->a(BB)I

    move-result v5

    invoke-static {p0, v4, v5}, LX/EaU;->a(LX/EaO;LX/EaO;I)V

    .line 2141557
    aget-object v0, v0, p1

    aget-object v0, v0, v7

    int-to-byte v3, v3

    const/16 v4, 0x8

    invoke-static {v3, v4}, LX/EaU;->a(BB)I

    move-result v3

    invoke-static {p0, v0, v3}, LX/EaU;->a(LX/EaO;LX/EaO;I)V

    .line 2141558
    iget-object v0, v1, LX/EaO;->a:[I

    iget-object v3, p0, LX/EaO;->b:[I

    invoke-static {v0, v3}, LX/EZs;->a([I[I)V

    .line 2141559
    iget-object v0, v1, LX/EaO;->b:[I

    iget-object v3, p0, LX/EaO;->a:[I

    invoke-static {v0, v3}, LX/EZs;->a([I[I)V

    .line 2141560
    iget-object v0, v1, LX/EaO;->c:[I

    iget-object v3, p0, LX/EaO;->c:[I

    invoke-static {v0, v3}, LX/Ea0;->a([I[I)V

    .line 2141561
    invoke-static {p0, v1, v2}, LX/EaU;->a(LX/EaO;LX/EaO;I)V

    .line 2141562
    return-void

    .line 2141563
    :cond_0
    const/16 v0, 0xf

    if-gt p1, v0, :cond_1

    sget-object v0, LX/EaT;->a:[[LX/EaO;

    goto/16 :goto_0

    :cond_1
    const/16 v0, 0x17

    if-gt p1, v0, :cond_2

    sget-object v0, LX/EaR;->a:[[LX/EaO;

    goto/16 :goto_0

    :cond_2
    sget-object v0, LX/EaS;->a:[[LX/EaO;

    goto/16 :goto_0
.end method

.method private static a(LX/EaO;LX/EaO;I)V
    .locals 2

    .prologue
    .line 2141564
    iget-object v0, p0, LX/EaO;->a:[I

    iget-object v1, p1, LX/EaO;->a:[I

    invoke-static {v0, v1, p2}, LX/EZr;->a([I[II)V

    .line 2141565
    iget-object v0, p0, LX/EaO;->b:[I

    iget-object v1, p1, LX/EaO;->b:[I

    invoke-static {v0, v1, p2}, LX/EZr;->a([I[II)V

    .line 2141566
    iget-object v0, p0, LX/EaO;->c:[I

    iget-object v1, p1, LX/EaO;->c:[I

    invoke-static {v0, v1, p2}, LX/EZr;->a([I[II)V

    .line 2141567
    return-void
.end method
