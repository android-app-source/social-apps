.class public LX/ErZ;
.super LX/6N2;
.source ""


# instance fields
.field private b:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;LX/3fh;)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2173142
    invoke-direct {p0, p1, p2}, LX/6N2;-><init>(Landroid/database/Cursor;LX/3fh;)V

    .line 2173143
    iput-object p1, p0, LX/ErZ;->b:Landroid/database/Cursor;

    .line 2173144
    return-void
.end method


# virtual methods
.method public final b()Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2173145
    invoke-virtual {p0}, LX/2TZ;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2173146
    new-instance v1, Landroid/util/Pair;

    iget-object v2, p0, LX/ErZ;->b:Landroid/database/Cursor;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method
