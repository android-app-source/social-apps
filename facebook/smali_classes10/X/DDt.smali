.class public final LX/DDt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1976108
    iput-object p1, p0, LX/DDt;->b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

    iput-object p2, p0, LX/DDt;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x6f69c87f

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1976109
    iget-object v0, p0, LX/DDt;->b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

    iget-object v2, v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->c:LX/1Kf;

    const/4 v3, 0x0

    iget-object v0, p0, LX/DDt;->b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->d:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    sget-object v4, LX/21D;->GROUP_FEED:LX/21D;

    const-string v5, "groupCommerceSellerActions"

    iget-object v6, p0, LX/DDt;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v0, v4, v5, v6}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(LX/21D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    const/16 v5, 0x6de

    iget-object v0, p0, LX/DDt;->b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v3, v4, v5, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1976110
    const v0, 0x1ee40633

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
