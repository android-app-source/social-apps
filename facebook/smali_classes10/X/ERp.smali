.class public final LX/ERp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4cr;


# instance fields
.field public final synthetic a:LX/ERq;

.field private b:I

.field private c:J

.field private d:Lcom/facebook/vault/provider/VaultImageProviderRow;


# direct methods
.method public constructor <init>(LX/ERq;JLcom/facebook/vault/provider/VaultImageProviderRow;)V
    .locals 2

    .prologue
    .line 2121523
    iput-object p1, p0, LX/ERp;->a:LX/ERq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121524
    const/4 v0, 0x0

    iput v0, p0, LX/ERp;->b:I

    .line 2121525
    iput-wide p2, p0, LX/ERp;->c:J

    .line 2121526
    iput-object p4, p0, LX/ERp;->d:Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2121527
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 5

    .prologue
    .line 2121528
    const-wide v0, 0x4056800000000000L    # 90.0

    long-to-double v2, p1

    mul-double/2addr v0, v2

    iget-wide v2, p0, LX/ERp;->c:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 2121529
    iget v1, p0, LX/ERp;->b:I

    if-eq v0, v1, :cond_0

    .line 2121530
    iput v0, p0, LX/ERp;->b:I

    .line 2121531
    iget v1, p0, LX/ERp;->b:I

    rem-int/lit8 v1, v1, 0xa

    if-nez v1, :cond_0

    .line 2121532
    iget-object v1, p0, LX/ERp;->a:LX/ERq;

    iget-object v1, v1, LX/ERq;->k:LX/ERr;

    iget-object v2, p0, LX/ERp;->d:Lcom/facebook/vault/provider/VaultImageProviderRow;

    invoke-virtual {v1, v2, v0}, LX/ERr;->a(Lcom/facebook/vault/provider/VaultImageProviderRow;I)V

    .line 2121533
    :cond_0
    return-void
.end method
