.class public final LX/EIS;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:LX/EIU;


# direct methods
.method public constructor <init>(LX/EIU;)V
    .locals 0

    .prologue
    .line 2101533
    iput-object p1, p0, LX/EIS;->a:LX/EIU;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(I)V
    .locals 1

    .prologue
    .line 2101534
    if-eqz p1, :cond_0

    .line 2101535
    :goto_0
    return-void

    .line 2101536
    :cond_0
    iget-object v0, p0, LX/EIS;->a:LX/EIU;

    .line 2101537
    iget-object p0, v0, LX/EIU;->N:LX/ED6;

    if-nez p0, :cond_2

    .line 2101538
    :cond_1
    :goto_1
    goto :goto_0

    .line 2101539
    :cond_2
    invoke-static {v0}, LX/EIU;->getPageType(LX/EIU;)LX/ED5;

    move-result-object p0

    .line 2101540
    if-eqz p0, :cond_1

    .line 2101541
    sget-object p1, LX/EIK;->a:[I

    invoke-virtual {p0}, LX/ED5;->ordinal()I

    move-result p0

    aget p0, p1, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_1

    .line 2101542
    :pswitch_0
    iget-object p0, v0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    sget-object p1, LX/EHD;->ROSTER_CONFERENCE:LX/EHD;

    invoke-virtual {p0, p1}, Lcom/facebook/rtc/views/RtcActionBar;->setType(LX/EHD;)V

    .line 2101543
    invoke-static {v0}, LX/EIU;->K(LX/EIU;)V

    .line 2101544
    invoke-static {v0}, LX/EIU;->Y(LX/EIU;)V

    .line 2101545
    invoke-static {v0}, LX/EIU;->X(LX/EIU;)V

    .line 2101546
    invoke-static {v0}, LX/EIU;->aa(LX/EIU;)V

    .line 2101547
    goto :goto_1

    .line 2101548
    :pswitch_1
    iget-object p0, v0, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 2101549
    iget-object p1, v0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    iget-object p0, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EDx;

    invoke-virtual {p0}, LX/EDx;->aJ()Z

    move-result p0

    if-eqz p0, :cond_3

    sget-object p0, LX/EHD;->VIDEO_CONFERENCE:LX/EHD;

    :goto_2
    invoke-virtual {p1, p0}, Lcom/facebook/rtc/views/RtcActionBar;->setType(LX/EHD;)V

    .line 2101550
    const/16 p0, 0x1388

    invoke-static {v0, p0}, LX/EIU;->d(LX/EIU;I)V

    .line 2101551
    goto :goto_1

    .line 2101552
    :cond_3
    sget-object p0, LX/EHD;->AUDIO_CONFERENCE:LX/EHD;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
