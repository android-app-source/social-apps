.class public LX/EJJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/graphql/enums/GraphQLGroupJoinState;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2103804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2103805
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EJJ;->a:Ljava/lang/String;

    .line 2103806
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->kO()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p0, LX/EJJ;->b:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2103807
    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)V
    .locals 1

    .prologue
    .line 2103808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2103809
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EJJ;->a:Ljava/lang/String;

    .line 2103810
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p0, LX/EJJ;->b:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2103811
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2103812
    iget-object v0, p0, LX/EJJ;->b:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2103803
    iget-object v0, p0, LX/EJJ;->a:Ljava/lang/String;

    return-object v0
.end method
