.class public final LX/EuF;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/EuG;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2179256
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2179257
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "friendModel"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "callerContext"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/EuF;->b:[Ljava/lang/String;

    .line 2179258
    iput v3, p0, LX/EuF;->c:I

    .line 2179259
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/EuF;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/EuF;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/EuF;LX/1De;IILcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;)V
    .locals 1

    .prologue
    .line 2179260
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2179261
    iput-object p4, p0, LX/EuF;->a:Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;

    .line 2179262
    iget-object v0, p0, LX/EuF;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2179263
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/EuF;
    .locals 2

    .prologue
    .line 2179264
    iget-object v0, p0, LX/EuF;->a:Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;

    iput-object p1, v0, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2179265
    iget-object v0, p0, LX/EuF;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2179266
    return-object p0
.end method

.method public final a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/EuF;
    .locals 2

    .prologue
    .line 2179267
    iget-object v0, p0, LX/EuF;->a:Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;

    iput-object p1, v0, Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2179268
    iget-object v0, p0, LX/EuF;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2179269
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2179270
    invoke-super {p0}, LX/1X5;->a()V

    .line 2179271
    const/4 v0, 0x0

    iput-object v0, p0, LX/EuF;->a:Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;

    .line 2179272
    sget-object v0, LX/EuG;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2179273
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/EuG;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2179274
    iget-object v1, p0, LX/EuF;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/EuF;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/EuF;->c:I

    if-ge v1, v2, :cond_2

    .line 2179275
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2179276
    :goto_0
    iget v2, p0, LX/EuF;->c:I

    if-ge v0, v2, :cond_1

    .line 2179277
    iget-object v2, p0, LX/EuF;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2179278
    iget-object v2, p0, LX/EuF;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2179279
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2179280
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2179281
    :cond_2
    iget-object v0, p0, LX/EuF;->a:Lcom/facebook/friending/center/components/FriendListProfilePicture$FriendListProfilePictureImpl;

    .line 2179282
    invoke-virtual {p0}, LX/EuF;->a()V

    .line 2179283
    return-object v0
.end method
