.class public LX/ECR;
.super LX/6LI;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3A0;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/messaging/model/threads/ThreadSummary;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2089848
    const-string v0, "OngoingGroupCallBannerNotificaiton"

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 2089849
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2089850
    iput-object v0, p0, LX/ECR;->b:LX/0Ot;

    .line 2089851
    return-void
.end method

.method public static e(LX/ECR;)V
    .locals 8

    .prologue
    .line 2089832
    iget-object v0, p0, LX/ECR;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_2

    .line 2089833
    iget-object v0, p0, LX/ECR;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->a()Z

    move-result v0

    .line 2089834
    :goto_0
    move v0, v0

    .line 2089835
    if-eqz v0, :cond_1

    .line 2089836
    iget-object v4, p0, LX/ECR;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v4, :cond_3

    .line 2089837
    iget-object v4, p0, LX/ECR;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3A0;

    iget-object v5, p0, LX/ECR;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v5, v5, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v6, v5, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 2089838
    iget-object v6, v4, LX/3A0;->d:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/EDx;

    invoke-virtual {v6}, LX/EDx;->aT()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, v4, LX/3A0;->d:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/EDx;

    invoke-virtual {v6}, LX/EDx;->ap()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, v4, LX/3A0;->d:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/EDx;

    invoke-virtual {v6}, LX/EDx;->ap()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v6, 0x1

    :goto_1
    move v4, v6

    .line 2089839
    :goto_2
    move v0, v4

    .line 2089840
    if-nez v0, :cond_1

    .line 2089841
    iget-object v0, p0, LX/ECR;->c:LX/0W3;

    sget-wide v2, LX/0X5;->hi:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    .line 2089842
    if-eqz v0, :cond_0

    .line 2089843
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 2089844
    invoke-virtual {v0, p0}, LX/6LN;->a(LX/6LI;)V

    .line 2089845
    :cond_0
    :goto_3
    return-void

    .line 2089846
    :cond_1
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 2089847
    invoke-virtual {v0, p0}, LX/6LN;->b(LX/6LI;)V

    goto :goto_3

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    :cond_4
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static i(LX/ECR;)Z
    .locals 1

    .prologue
    .line 2089852
    iget-object v0, p0, LX/ECR;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ECR;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 2089810
    new-instance v1, Lcom/facebook/common/banner/BasicBannerNotificationView;

    iget-object v0, p0, LX/ECR;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/facebook/common/banner/BasicBannerNotificationView;-><init>(Landroid/content/Context;)V

    .line 2089811
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setOrientation(I)V

    .line 2089812
    iget-object v0, p0, LX/ECR;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2089813
    invoke-static {p0}, LX/ECR;->i(LX/ECR;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080813

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2089814
    :goto_0
    iget-object v3, p0, LX/ECR;->a:Landroid/content/Context;

    const v4, 0x7f010053

    iget-object v5, p0, LX/ECR;->a:Landroid/content/Context;

    const v6, 0x7f0a019a

    invoke-static {v5, v6}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v5

    invoke-static {v3, v4, v5}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v3

    .line 2089815
    iget-object v4, p0, LX/ECR;->a:Landroid/content/Context;

    const v5, 0x1010098

    const/high16 v6, -0x1000000

    invoke-static {v4, v5, v6}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v4

    .line 2089816
    new-instance v5, LX/2M6;

    invoke-direct {v5}, LX/2M6;-><init>()V

    .line 2089817
    iput-object v0, v5, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 2089818
    move-object v0, v5

    .line 2089819
    const v5, 0x7f020f5a

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2089820
    iput-object v2, v0, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 2089821
    move-object v0, v0

    .line 2089822
    iput v4, v0, LX/2M6;->i:I

    .line 2089823
    move-object v0, v0

    .line 2089824
    iget-object v2, p0, LX/ECR;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f080723

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2M6;->a(Ljava/lang/String;)LX/2M6;

    move-result-object v0

    .line 2089825
    iput v3, v0, LX/2M6;->f:I

    .line 2089826
    move-object v0, v0

    .line 2089827
    invoke-virtual {v0}, LX/2M6;->a()LX/6LR;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 2089828
    new-instance v0, LX/ECQ;

    invoke-direct {v0, p0}, LX/ECQ;-><init>(LX/ECR;)V

    .line 2089829
    iput-object v0, v1, Lcom/facebook/common/banner/BasicBannerNotificationView;->a:LX/6LQ;

    .line 2089830
    return-object v1

    .line 2089831
    :cond_0
    const v0, 0x7f080812

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2089808
    invoke-static {p0}, LX/ECR;->e(LX/ECR;)V

    .line 2089809
    return-void
.end method
