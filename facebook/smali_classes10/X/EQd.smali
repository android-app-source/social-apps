.class public LX/EQd;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/EQd;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2118847
    const-class v0, LX/EQd;

    sput-object v0, LX/EQd;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2118848
    const-string v0, "vault"

    const/4 v1, 0x1

    new-instance v2, LX/EQc;

    invoke-direct {v2}, LX/EQc;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 2118849
    return-void
.end method

.method public static a(LX/0QB;)LX/EQd;
    .locals 3

    .prologue
    .line 2118850
    sget-object v0, LX/EQd;->b:LX/EQd;

    if-nez v0, :cond_1

    .line 2118851
    const-class v1, LX/EQd;

    monitor-enter v1

    .line 2118852
    :try_start_0
    sget-object v0, LX/EQd;->b:LX/EQd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2118853
    if-eqz v2, :cond_0

    .line 2118854
    :try_start_1
    new-instance v0, LX/EQd;

    invoke-direct {v0}, LX/EQd;-><init>()V

    .line 2118855
    move-object v0, v0

    .line 2118856
    sput-object v0, LX/EQd;->b:LX/EQd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2118857
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2118858
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2118859
    :cond_1
    sget-object v0, LX/EQd;->b:LX/EQd;

    return-object v0

    .line 2118860
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2118861
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
