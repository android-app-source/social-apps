.class public final LX/Ero;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DC2;


# instance fields
.field public final synthetic a:LX/Err;


# direct methods
.method public constructor <init>(LX/Err;)V
    .locals 0

    .prologue
    .line 2173472
    iput-object p1, p0, LX/Ero;->a:LX/Err;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2173453
    return-void
.end method

.method public final a(I)V
    .locals 10

    .prologue
    .line 2173467
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    .line 2173468
    iget-object v2, v0, LX/Err;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Fn;

    invoke-virtual {v2}, LX/1Fn;->e()D

    move-result-wide v2

    .line 2173469
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v2

    int-to-double v6, p1

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    sub-double v2, v8, v2

    mul-double/2addr v2, v6

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    move v0, v2

    .line 2173470
    iget-object v1, p0, LX/Ero;->a:LX/Err;

    invoke-static {v1, v0}, LX/Err;->b$redex0(LX/Err;I)V

    .line 2173471
    return-void
.end method

.method public final a(LX/DC6;)V
    .locals 3

    .prologue
    .line 2173473
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    invoke-static {v0}, LX/Err;->f(LX/Err;)LX/0oG;

    move-result-object v1

    .line 2173474
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173475
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    iget-object v0, v0, LX/Err;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a(LX/0oG;)V

    .line 2173476
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    sget-object v2, LX/Erp;->FAIL:LX/Erp;

    invoke-static {v0, v1, v2}, LX/Err;->a$redex0(LX/Err;LX/0oG;LX/Erp;)V

    .line 2173477
    :cond_0
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    const/4 v1, 0x0

    .line 2173478
    iput-object v1, v0, LX/Err;->t:Ljava/util/UUID;

    .line 2173479
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    invoke-static {v0, p1}, LX/Err;->a$redex0(LX/Err;LX/DC6;)V

    .line 2173480
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    invoke-static {v0}, LX/Err;->j(LX/Err;)V

    .line 2173481
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2173465
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    invoke-static {v0}, LX/Err;->d$redex0(LX/Err;)V

    .line 2173466
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 2173454
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    invoke-static {v0}, LX/Err;->f(LX/Err;)LX/0oG;

    move-result-object v1

    .line 2173455
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173456
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    iget-object v0, v0, LX/Err;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a(LX/0oG;)V

    .line 2173457
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    sget-object v2, LX/Erp;->SUCCESS:LX/Erp;

    invoke-static {v0, v1, v2}, LX/Err;->a$redex0(LX/Err;LX/0oG;LX/Erp;)V

    .line 2173458
    :cond_0
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    const/4 v1, 0x0

    .line 2173459
    iput-object v1, v0, LX/Err;->t:Ljava/util/UUID;

    .line 2173460
    iget-object v0, p0, LX/Ero;->a:LX/Err;

    .line 2173461
    iget-object v1, v0, LX/Err;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DC2;

    .line 2173462
    iget p0, v0, LX/Err;->q:I

    invoke-interface {v1, p0}, LX/DC2;->b(I)V

    goto :goto_0

    .line 2173463
    :cond_1
    invoke-static {v0}, LX/Err;->j(LX/Err;)V

    .line 2173464
    return-void
.end method
