.class public final enum LX/EmJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EmJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EmJ;

.field public static final enum DEFAULT:LX/EmJ;

.field public static final enum MOBILE:LX/EmJ;

.field public static final enum WIFI:LX/EmJ;


# instance fields
.field public final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2165621
    new-instance v0, LX/EmJ;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v5, v3}, LX/EmJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EmJ;->MOBILE:LX/EmJ;

    new-instance v0, LX/EmJ;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v3, v4}, LX/EmJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EmJ;->WIFI:LX/EmJ;

    new-instance v0, LX/EmJ;

    const-string v1, "DEFAULT"

    sget-object v2, LX/EmJ;->WIFI:LX/EmJ;

    iget v2, v2, LX/EmJ;->value:I

    invoke-direct {v0, v1, v4, v2}, LX/EmJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EmJ;->DEFAULT:LX/EmJ;

    .line 2165622
    const/4 v0, 0x3

    new-array v0, v0, [LX/EmJ;

    sget-object v1, LX/EmJ;->MOBILE:LX/EmJ;

    aput-object v1, v0, v5

    sget-object v1, LX/EmJ;->WIFI:LX/EmJ;

    aput-object v1, v0, v3

    sget-object v1, LX/EmJ;->DEFAULT:LX/EmJ;

    aput-object v1, v0, v4

    sput-object v0, LX/EmJ;->$VALUES:[LX/EmJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2165623
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/EmJ;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EmJ;
    .locals 1

    .prologue
    .line 2165624
    const-class v0, LX/EmJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EmJ;

    return-object v0
.end method

.method public static values()[LX/EmJ;
    .locals 1

    .prologue
    .line 2165625
    sget-object v0, LX/EmJ;->$VALUES:[LX/EmJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EmJ;

    return-object v0
.end method
