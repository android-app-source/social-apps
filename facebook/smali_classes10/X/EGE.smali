.class public LX/EGE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/EGE;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/EGD;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:J

.field public g:Ljava/lang/String;

.field public h:J

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/EGD;JJ)V
    .locals 9

    .prologue
    .line 2096908
    const-string v8, ""

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v8}, LX/EGE;-><init>(Ljava/lang/String;LX/EGD;JJLjava/lang/String;)V

    .line 2096909
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LX/EGD;JJLjava/lang/String;)V
    .locals 1

    .prologue
    .line 2096877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096878
    iput-object p1, p0, LX/EGE;->b:Ljava/lang/String;

    .line 2096879
    iput-object p2, p0, LX/EGE;->a:LX/EGD;

    .line 2096880
    iput-object p7, p0, LX/EGE;->c:Ljava/lang/String;

    .line 2096881
    iput-wide p3, p0, LX/EGE;->e:J

    .line 2096882
    iput-wide p5, p0, LX/EGE;->f:J

    .line 2096883
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EGE;->j:Z

    .line 2096884
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EGE;->i:Z

    .line 2096885
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2096905
    sget-object v1, LX/EGC;->a:[I

    iget-object v2, p0, LX/EGE;->a:LX/EGD;

    invoke-virtual {v2}, LX/EGD;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2096906
    :goto_0
    :pswitch_0
    return v0

    .line 2096907
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2096910
    iget-object v0, p0, LX/EGE;->a:LX/EGD;

    sget-object v1, LX/EGD;->CONNECTED:LX/EGD;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 2096898
    check-cast p1, LX/EGE;

    .line 2096899
    iget-wide v0, p0, LX/EGE;->e:J

    iget-wide v2, p1, LX/EGE;->e:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 2096900
    const/4 v0, 0x1

    .line 2096901
    :goto_0
    return v0

    .line 2096902
    :cond_0
    iget-wide v0, p0, LX/EGE;->e:J

    iget-wide v2, p1, LX/EGE;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 2096903
    const/4 v0, -0x1

    goto :goto_0

    .line 2096904
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2096891
    instance-of v2, p1, LX/EGE;

    if-nez v2, :cond_1

    .line 2096892
    :cond_0
    :goto_0
    return v0

    .line 2096893
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    .line 2096894
    goto :goto_0

    .line 2096895
    :cond_2
    check-cast p1, LX/EGE;

    .line 2096896
    iget-object v2, p0, LX/EGE;->b:Ljava/lang/String;

    iget-object v3, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/EGE;->a:LX/EGD;

    iget-object v3, p1, LX/EGE;->a:LX/EGD;

    if-ne v2, v3, :cond_0

    iget-wide v2, p0, LX/EGE;->e:J

    iget-wide v4, p1, LX/EGE;->e:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, LX/EGE;->f:J

    iget-wide v4, p1, LX/EGE;->f:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-boolean v2, p0, LX/EGE;->i:Z

    iget-boolean v3, p1, LX/EGE;->i:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, LX/EGE;->g:Ljava/lang/String;

    iget-object v3, p1, LX/EGE;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, LX/EGE;->h:J

    iget-wide v4, p1, LX/EGE;->h:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-boolean v2, p0, LX/EGE;->j:Z

    iget-boolean v3, p1, LX/EGE;->j:Z

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 2096897
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2096886
    iget-object v0, p0, LX/EGE;->a:LX/EGD;

    invoke-virtual {v0}, LX/EGD;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 2096887
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2096888
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/EGE;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2096889
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LX/EGE;->e:J

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 2096890
    return v0
.end method
