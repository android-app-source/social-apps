.class public final LX/DK9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1986701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 1986702
    if-nez p0, :cond_0

    .line 1986703
    const/4 v0, 0x0

    .line 1986704
    :goto_0
    return-object v0

    .line 1986705
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 1986706
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 1986707
    iput v1, v0, LX/2dc;->c:I

    .line 1986708
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 1986709
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1986710
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 1986711
    iput v1, v0, LX/2dc;->i:I

    .line 1986712
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 1986338
    if-nez p0, :cond_0

    .line 1986339
    const/4 v0, 0x0

    .line 1986340
    :goto_0
    return-object v0

    .line 1986341
    :cond_0
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    .line 1986342
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1986343
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1986344
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1986345
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ActorsModel;

    .line 1986346
    if-nez v0, :cond_5

    .line 1986347
    const/4 v5, 0x0

    .line 1986348
    :goto_2
    move-object v0, v5

    .line 1986349
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986350
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1986351
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1986352
    iput-object v0, v3, LX/23u;->d:LX/0Px;

    .line 1986353
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1986354
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1986355
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1986356
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;

    .line 1986357
    if-nez v0, :cond_7

    .line 1986358
    const/4 v5, 0x0

    .line 1986359
    :goto_4
    move-object v0, v5

    .line 1986360
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986361
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1986362
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1986363
    iput-object v0, v3, LX/23u;->k:LX/0Px;

    .line 1986364
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 1986365
    iput-object v0, v3, LX/23u;->m:Ljava/lang/String;

    .line 1986366
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->e()J

    move-result-wide v0

    .line 1986367
    iput-wide v0, v3, LX/23u;->v:J

    .line 1986368
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->hE_()Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;

    move-result-object v0

    .line 1986369
    if-nez v0, :cond_b

    .line 1986370
    const/4 v1, 0x0

    .line 1986371
    :goto_5
    move-object v0, v1

    .line 1986372
    iput-object v0, v3, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1986373
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->hD_()Ljava/lang/String;

    move-result-object v0

    .line 1986374
    iput-object v0, v3, LX/23u;->N:Ljava/lang/String;

    .line 1986375
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$MessageModel;

    move-result-object v0

    .line 1986376
    if-nez v0, :cond_28

    .line 1986377
    const/4 v1, 0x0

    .line 1986378
    :goto_6
    move-object v0, v1

    .line 1986379
    iput-object v0, v3, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1986380
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ToModel;

    move-result-object v0

    .line 1986381
    if-nez v0, :cond_29

    .line 1986382
    const/4 v1, 0x0

    .line 1986383
    :goto_7
    move-object v0, v1

    .line 1986384
    iput-object v0, v3, LX/23u;->aK:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1986385
    invoke-virtual {v3}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto/16 :goto_0

    .line 1986386
    :cond_5
    new-instance v5, LX/3dL;

    invoke-direct {v5}, LX/3dL;-><init>()V

    .line 1986387
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ActorsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 1986388
    iput-object v6, v5, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1986389
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ActorsModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 1986390
    iput-object v6, v5, LX/3dL;->E:Ljava/lang/String;

    .line 1986391
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ActorsModel;->d()Ljava/lang/String;

    move-result-object v6

    .line 1986392
    iput-object v6, v5, LX/3dL;->ag:Ljava/lang/String;

    .line 1986393
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ActorsModel;->e()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ActorsModel$ProfilePictureModel;

    move-result-object v6

    .line 1986394
    if-nez v6, :cond_6

    .line 1986395
    const/4 v7, 0x0

    .line 1986396
    :goto_8
    move-object v6, v7

    .line 1986397
    iput-object v6, v5, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1986398
    invoke-virtual {v5}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    goto/16 :goto_2

    .line 1986399
    :cond_6
    new-instance v7, LX/2dc;

    invoke-direct {v7}, LX/2dc;-><init>()V

    .line 1986400
    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ActorsModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1986401
    iput-object v0, v7, LX/2dc;->h:Ljava/lang/String;

    .line 1986402
    invoke-virtual {v7}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    goto :goto_8

    .line 1986403
    :cond_7
    new-instance v5, LX/39x;

    invoke-direct {v5}, LX/39x;-><init>()V

    .line 1986404
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;

    move-result-object v6

    .line 1986405
    if-nez v6, :cond_8

    .line 1986406
    const/4 v7, 0x0

    .line 1986407
    :goto_9
    move-object v6, v7

    .line 1986408
    iput-object v6, v5, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1986409
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->b()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;

    move-result-object v6

    .line 1986410
    if-nez v6, :cond_9

    .line 1986411
    const/4 v7, 0x0

    .line 1986412
    :goto_a
    move-object v6, v7

    .line 1986413
    iput-object v6, v5, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1986414
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->c()LX/0Px;

    move-result-object v6

    .line 1986415
    iput-object v6, v5, LX/39x;->p:LX/0Px;

    .line 1986416
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel;->d()Ljava/lang/String;

    move-result-object v6

    .line 1986417
    iput-object v6, v5, LX/39x;->t:Ljava/lang/String;

    .line 1986418
    invoke-virtual {v5}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    goto/16 :goto_4

    .line 1986419
    :cond_8
    new-instance v7, LX/173;

    invoke-direct {v7}, LX/173;-><init>()V

    .line 1986420
    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 1986421
    iput-object v8, v7, LX/173;->f:Ljava/lang/String;

    .line 1986422
    invoke-virtual {v7}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    goto :goto_9

    .line 1986423
    :cond_9
    new-instance v7, LX/4XB;

    invoke-direct {v7}, LX/4XB;-><init>()V

    .line 1986424
    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 1986425
    iput-object v8, v7, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1986426
    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel;->b()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v8

    .line 1986427
    if-nez v8, :cond_a

    .line 1986428
    const/4 v9, 0x0

    .line 1986429
    :goto_b
    move-object v8, v9

    .line 1986430
    iput-object v8, v7, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1986431
    invoke-virtual {v7}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    goto :goto_a

    .line 1986432
    :cond_a
    new-instance v9, LX/2dc;

    invoke-direct {v9}, LX/2dc;-><init>()V

    .line 1986433
    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->a()I

    move-result v10

    .line 1986434
    iput v10, v9, LX/2dc;->c:I

    .line 1986435
    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->b()D

    move-result-wide v11

    .line 1986436
    iput-wide v11, v9, LX/2dc;->g:D

    .line 1986437
    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->c()Ljava/lang/String;

    move-result-object v10

    .line 1986438
    iput-object v10, v9, LX/2dc;->h:Ljava/lang/String;

    .line 1986439
    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$AttachmentsModel$MediaModel$ImageModel;->d()I

    move-result v10

    .line 1986440
    iput v10, v9, LX/2dc;->i:I

    .line 1986441
    invoke-virtual {v9}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    goto :goto_b

    .line 1986442
    :cond_b
    new-instance v4, LX/3dM;

    invoke-direct {v4}, LX/3dM;-><init>()V

    .line 1986443
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->b()Z

    move-result v1

    .line 1986444
    iput-boolean v1, v4, LX/3dM;->d:Z

    .line 1986445
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->c()Z

    move-result v1

    .line 1986446
    iput-boolean v1, v4, LX/3dM;->e:Z

    .line 1986447
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->d()Z

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->c(Z)LX/3dM;

    .line 1986448
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->e()Z

    move-result v1

    .line 1986449
    iput-boolean v1, v4, LX/3dM;->g:Z

    .line 1986450
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->ac_()Z

    move-result v1

    .line 1986451
    iput-boolean v1, v4, LX/3dM;->h:Z

    .line 1986452
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->ad_()Z

    move-result v1

    .line 1986453
    iput-boolean v1, v4, LX/3dM;->i:Z

    .line 1986454
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->j()Z

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->g(Z)LX/3dM;

    .line 1986455
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->s()Z

    move-result v1

    .line 1986456
    iput-boolean v1, v4, LX/3dM;->k:Z

    .line 1986457
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->k()Z

    move-result v1

    .line 1986458
    iput-boolean v1, v4, LX/3dM;->l:Z

    .line 1986459
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 1986460
    iput-object v1, v4, LX/3dM;->p:Ljava/lang/String;

    .line 1986461
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->t()Ljava/lang/String;

    move-result-object v1

    .line 1986462
    iput-object v1, v4, LX/3dM;->r:Ljava/lang/String;

    .line 1986463
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->m()Z

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->j(Z)LX/3dM;

    .line 1986464
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->n()Ljava/lang/String;

    move-result-object v1

    .line 1986465
    iput-object v1, v4, LX/3dM;->y:Ljava/lang/String;

    .line 1986466
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v1

    .line 1986467
    if-nez v1, :cond_e

    .line 1986468
    const/4 v2, 0x0

    .line 1986469
    :goto_c
    move-object v1, v2

    .line 1986470
    iput-object v1, v4, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 1986471
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->o()Z

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->l(Z)LX/3dM;

    .line 1986472
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->p()Ljava/lang/String;

    move-result-object v1

    .line 1986473
    iput-object v1, v4, LX/3dM;->D:Ljava/lang/String;

    .line 1986474
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v1

    .line 1986475
    if-nez v1, :cond_12

    .line 1986476
    const/4 v2, 0x0

    .line 1986477
    :goto_d
    move-object v1, v2

    .line 1986478
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1986479
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->w()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v1

    .line 1986480
    if-nez v1, :cond_13

    .line 1986481
    const/4 v2, 0x0

    .line 1986482
    :goto_e
    move-object v1, v2

    .line 1986483
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 1986484
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->x()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v1

    .line 1986485
    if-nez v1, :cond_14

    .line 1986486
    const/4 v2, 0x0

    .line 1986487
    :goto_f
    move-object v1, v2

    .line 1986488
    iput-object v1, v4, LX/3dM;->I:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    .line 1986489
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->q()Ljava/lang/String;

    move-result-object v1

    .line 1986490
    iput-object v1, v4, LX/3dM;->J:Ljava/lang/String;

    .line 1986491
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v1

    .line 1986492
    if-nez v1, :cond_1e

    .line 1986493
    const/4 v2, 0x0

    .line 1986494
    :goto_10
    move-object v1, v2

    .line 1986495
    iput-object v1, v4, LX/3dM;->K:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 1986496
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->z()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 1986497
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1986498
    const/4 v1, 0x0

    move v2, v1

    :goto_11
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->z()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_c

    .line 1986499
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->z()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    .line 1986500
    if-nez v1, :cond_1f

    .line 1986501
    const/4 v6, 0x0

    .line 1986502
    :goto_12
    move-object v1, v6

    .line 1986503
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986504
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_11

    .line 1986505
    :cond_c
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1986506
    iput-object v1, v4, LX/3dM;->N:LX/0Px;

    .line 1986507
    :cond_d
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->A()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v1

    .line 1986508
    if-nez v1, :cond_20

    .line 1986509
    const/4 v2, 0x0

    .line 1986510
    :goto_13
    move-object v1, v2

    .line 1986511
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 1986512
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->B()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v1

    .line 1986513
    if-nez v1, :cond_21

    .line 1986514
    const/4 v2, 0x0

    .line 1986515
    :goto_14
    move-object v1, v2

    .line 1986516
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 1986517
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->r()LX/59N;

    move-result-object v1

    .line 1986518
    if-nez v1, :cond_26

    .line 1986519
    const/4 v2, 0x0

    .line 1986520
    :goto_15
    move-object v1, v2

    .line 1986521
    iput-object v1, v4, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1986522
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->C()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v1

    .line 1986523
    if-nez v1, :cond_27

    .line 1986524
    const/4 v2, 0x0

    .line 1986525
    :goto_16
    move-object v1, v2

    .line 1986526
    iput-object v1, v4, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 1986527
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feedback/NewsFeedFeedbackGraphQLModels$NewsFeedDefaultsFeedbackModel;->D()I

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->b(I)LX/3dM;

    .line 1986528
    invoke-virtual {v4}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    goto/16 :goto_5

    .line 1986529
    :cond_e
    new-instance v6, LX/4Wx;

    invoke-direct {v6}, LX/4Wx;-><init>()V

    .line 1986530
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_10

    .line 1986531
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1986532
    const/4 v2, 0x0

    move v5, v2

    :goto_17
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v5, v2, :cond_f

    .line 1986533
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;

    .line 1986534
    if-nez v2, :cond_11

    .line 1986535
    const/4 v8, 0x0

    .line 1986536
    :goto_18
    move-object v2, v8

    .line 1986537
    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986538
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_17

    .line 1986539
    :cond_f
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1986540
    iput-object v2, v6, LX/4Wx;->b:LX/0Px;

    .line 1986541
    :cond_10
    invoke-virtual {v6}, LX/4Wx;->a()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v2

    goto/16 :goto_c

    .line 1986542
    :cond_11
    new-instance v8, LX/3dL;

    invoke-direct {v8}, LX/3dL;-><init>()V

    .line 1986543
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    .line 1986544
    iput-object v9, v8, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1986545
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v9

    .line 1986546
    iput-object v9, v8, LX/3dL;->ag:Ljava/lang/String;

    .line 1986547
    invoke-virtual {v8}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v8

    goto :goto_18

    .line 1986548
    :cond_12
    new-instance v2, LX/3dN;

    invoke-direct {v2}, LX/3dN;-><init>()V

    .line 1986549
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a()I

    move-result v5

    .line 1986550
    iput v5, v2, LX/3dN;->b:I

    .line 1986551
    invoke-virtual {v2}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v2

    goto/16 :goto_d

    .line 1986552
    :cond_13
    new-instance v2, LX/3dO;

    invoke-direct {v2}, LX/3dO;-><init>()V

    .line 1986553
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v5

    .line 1986554
    iput v5, v2, LX/3dO;->b:I

    .line 1986555
    invoke-virtual {v2}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v2

    goto/16 :goto_e

    .line 1986556
    :cond_14
    new-instance v2, LX/4WO;

    invoke-direct {v2}, LX/4WO;-><init>()V

    .line 1986557
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;->a()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel;

    move-result-object v5

    .line 1986558
    if-nez v5, :cond_15

    .line 1986559
    const/4 v6, 0x0

    .line 1986560
    :goto_19
    move-object v5, v6

    .line 1986561
    iput-object v5, v2, LX/4WO;->b:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    .line 1986562
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;->b()LX/175;

    move-result-object v5

    .line 1986563
    if-nez v5, :cond_19

    .line 1986564
    const/4 v6, 0x0

    .line 1986565
    :goto_1a
    move-object v5, v6

    .line 1986566
    iput-object v5, v2, LX/4WO;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1986567
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;->c()Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    move-result-object v5

    .line 1986568
    iput-object v5, v2, LX/4WO;->d:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    .line 1986569
    invoke-virtual {v2}, LX/4WO;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v2

    goto/16 :goto_f

    .line 1986570
    :cond_15
    new-instance v8, LX/4WN;

    invoke-direct {v8}, LX/4WN;-><init>()V

    .line 1986571
    invoke-virtual {v5}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_17

    .line 1986572
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1986573
    const/4 v6, 0x0

    move v7, v6

    :goto_1b
    invoke-virtual {v5}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_16

    .line 1986574
    invoke-virtual {v5}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;

    .line 1986575
    if-nez v6, :cond_18

    .line 1986576
    const/4 v10, 0x0

    .line 1986577
    :goto_1c
    move-object v6, v10

    .line 1986578
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986579
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1b

    .line 1986580
    :cond_16
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1986581
    iput-object v6, v8, LX/4WN;->b:LX/0Px;

    .line 1986582
    :cond_17
    invoke-virtual {v8}, LX/4WN;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v6

    goto :goto_19

    .line 1986583
    :cond_18
    new-instance v10, LX/3dL;

    invoke-direct {v10}, LX/3dL;-><init>()V

    .line 1986584
    invoke-virtual {v6}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    .line 1986585
    iput-object v11, v10, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1986586
    invoke-virtual {v6}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v11

    .line 1986587
    iput-object v11, v10, LX/3dL;->E:Ljava/lang/String;

    .line 1986588
    invoke-virtual {v6}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v11

    .line 1986589
    iput-object v11, v10, LX/3dL;->ag:Ljava/lang/String;

    .line 1986590
    invoke-virtual {v6}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->e()LX/1Fb;

    move-result-object v11

    invoke-static {v11}, LX/DK9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    .line 1986591
    iput-object v11, v10, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1986592
    invoke-virtual {v10}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v10

    goto :goto_1c

    .line 1986593
    :cond_19
    new-instance v8, LX/173;

    invoke-direct {v8}, LX/173;-><init>()V

    .line 1986594
    invoke-interface {v5}, LX/175;->b()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_1b

    .line 1986595
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1986596
    const/4 v6, 0x0

    move v7, v6

    :goto_1d
    invoke-interface {v5}, LX/175;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_1a

    .line 1986597
    invoke-interface {v5}, LX/175;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1W5;

    .line 1986598
    if-nez v6, :cond_1c

    .line 1986599
    const/4 v10, 0x0

    .line 1986600
    :goto_1e
    move-object v6, v10

    .line 1986601
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986602
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1d

    .line 1986603
    :cond_1a
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1986604
    iput-object v6, v8, LX/173;->e:LX/0Px;

    .line 1986605
    :cond_1b
    invoke-interface {v5}, LX/175;->a()Ljava/lang/String;

    move-result-object v6

    .line 1986606
    iput-object v6, v8, LX/173;->f:Ljava/lang/String;

    .line 1986607
    invoke-virtual {v8}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    goto/16 :goto_1a

    .line 1986608
    :cond_1c
    new-instance v10, LX/4W6;

    invoke-direct {v10}, LX/4W6;-><init>()V

    .line 1986609
    invoke-interface {v6}, LX/1W5;->a()LX/171;

    move-result-object v11

    .line 1986610
    if-nez v11, :cond_1d

    .line 1986611
    const/4 v12, 0x0

    .line 1986612
    :goto_1f
    move-object v11, v12

    .line 1986613
    iput-object v11, v10, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1986614
    invoke-interface {v6}, LX/1W5;->b()I

    move-result v11

    .line 1986615
    iput v11, v10, LX/4W6;->c:I

    .line 1986616
    invoke-interface {v6}, LX/1W5;->c()I

    move-result v11

    .line 1986617
    iput v11, v10, LX/4W6;->d:I

    .line 1986618
    invoke-virtual {v10}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v10

    goto :goto_1e

    .line 1986619
    :cond_1d
    new-instance v12, LX/170;

    invoke-direct {v12}, LX/170;-><init>()V

    .line 1986620
    invoke-interface {v11}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    .line 1986621
    iput-object v13, v12, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1986622
    invoke-interface {v11}, LX/171;->c()LX/0Px;

    move-result-object v13

    .line 1986623
    iput-object v13, v12, LX/170;->b:LX/0Px;

    .line 1986624
    invoke-interface {v11}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v13

    .line 1986625
    iput-object v13, v12, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 1986626
    invoke-interface {v11}, LX/171;->e()Ljava/lang/String;

    move-result-object v13

    .line 1986627
    iput-object v13, v12, LX/170;->o:Ljava/lang/String;

    .line 1986628
    invoke-interface {v11}, LX/171;->v_()Ljava/lang/String;

    move-result-object v13

    .line 1986629
    iput-object v13, v12, LX/170;->A:Ljava/lang/String;

    .line 1986630
    invoke-interface {v11}, LX/171;->w_()Ljava/lang/String;

    move-result-object v13

    .line 1986631
    iput-object v13, v12, LX/170;->X:Ljava/lang/String;

    .line 1986632
    invoke-interface {v11}, LX/171;->j()Ljava/lang/String;

    move-result-object v13

    .line 1986633
    iput-object v13, v12, LX/170;->Y:Ljava/lang/String;

    .line 1986634
    invoke-virtual {v12}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v12

    goto :goto_1f

    .line 1986635
    :cond_1e
    new-instance v2, LX/4Ya;

    invoke-direct {v2}, LX/4Ya;-><init>()V

    .line 1986636
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a()I

    move-result v5

    .line 1986637
    iput v5, v2, LX/4Ya;->b:I

    .line 1986638
    invoke-virtual {v2}, LX/4Ya;->a()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v2

    goto/16 :goto_10

    .line 1986639
    :cond_1f
    new-instance v6, LX/4WL;

    invoke-direct {v6}, LX/4WL;-><init>()V

    .line 1986640
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;->a()I

    move-result v7

    .line 1986641
    iput v7, v6, LX/4WL;->b:I

    .line 1986642
    invoke-virtual {v6}, LX/4WL;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v6

    goto/16 :goto_12

    .line 1986643
    :cond_20
    new-instance v2, LX/4ZH;

    invoke-direct {v2}, LX/4ZH;-><init>()V

    .line 1986644
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v5

    .line 1986645
    iput v5, v2, LX/4ZH;->b:I

    .line 1986646
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b()I

    move-result v5

    .line 1986647
    iput v5, v2, LX/4ZH;->e:I

    .line 1986648
    invoke-virtual {v2}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    goto/16 :goto_13

    .line 1986649
    :cond_21
    new-instance v6, LX/3dQ;

    invoke-direct {v6}, LX/3dQ;-><init>()V

    .line 1986650
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_23

    .line 1986651
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1986652
    const/4 v2, 0x0

    move v5, v2

    :goto_20
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v5, v2, :cond_22

    .line 1986653
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    .line 1986654
    if-nez v2, :cond_24

    .line 1986655
    const/4 v8, 0x0

    .line 1986656
    :goto_21
    move-object v2, v8

    .line 1986657
    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986658
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_20

    .line 1986659
    :cond_22
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1986660
    iput-object v2, v6, LX/3dQ;->b:LX/0Px;

    .line 1986661
    :cond_23
    invoke-virtual {v6}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v2

    goto/16 :goto_14

    .line 1986662
    :cond_24
    new-instance v8, LX/4ZJ;

    invoke-direct {v8}, LX/4ZJ;-><init>()V

    .line 1986663
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;

    move-result-object v9

    .line 1986664
    if-nez v9, :cond_25

    .line 1986665
    const/4 v10, 0x0

    .line 1986666
    :goto_22
    move-object v9, v10

    .line 1986667
    iput-object v9, v8, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 1986668
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->b()I

    move-result v9

    .line 1986669
    iput v9, v8, LX/4ZJ;->c:I

    .line 1986670
    invoke-virtual {v8}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v8

    goto :goto_21

    .line 1986671
    :cond_25
    new-instance v10, LX/4WM;

    invoke-direct {v10}, LX/4WM;-><init>()V

    .line 1986672
    invoke-virtual {v9}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;->a()I

    move-result v11

    .line 1986673
    iput v11, v10, LX/4WM;->f:I

    .line 1986674
    invoke-virtual {v10}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v10

    goto :goto_22

    .line 1986675
    :cond_26
    new-instance v2, LX/4XY;

    invoke-direct {v2}, LX/4XY;-><init>()V

    .line 1986676
    invoke-interface {v1}, LX/59N;->b()Ljava/lang/String;

    move-result-object v5

    .line 1986677
    iput-object v5, v2, LX/4XY;->ag:Ljava/lang/String;

    .line 1986678
    invoke-interface {v1}, LX/59N;->c()Ljava/lang/String;

    move-result-object v5

    .line 1986679
    iput-object v5, v2, LX/4XY;->aT:Ljava/lang/String;

    .line 1986680
    invoke-interface {v1}, LX/59N;->d()LX/1Fb;

    move-result-object v5

    invoke-static {v5}, LX/DK9;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 1986681
    iput-object v5, v2, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1986682
    invoke-virtual {v2}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    goto/16 :goto_15

    .line 1986683
    :cond_27
    new-instance v2, LX/33O;

    invoke-direct {v2}, LX/33O;-><init>()V

    .line 1986684
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 1986685
    iput-object v5, v2, LX/33O;->aI:Ljava/lang/String;

    .line 1986686
    invoke-virtual {v2}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    goto/16 :goto_16

    .line 1986687
    :cond_28
    new-instance v1, LX/173;

    invoke-direct {v1}, LX/173;-><init>()V

    .line 1986688
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$MessageModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1986689
    iput-object v2, v1, LX/173;->f:Ljava/lang/String;

    .line 1986690
    invoke-virtual {v1}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    goto/16 :goto_6

    .line 1986691
    :cond_29
    new-instance v1, LX/25F;

    invoke-direct {v1}, LX/25F;-><init>()V

    .line 1986692
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ToModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 1986693
    iput-object v2, v1, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1986694
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ToModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1986695
    iput-object v2, v1, LX/25F;->C:Ljava/lang/String;

    .line 1986696
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ToModel;->d()Z

    move-result v2

    .line 1986697
    iput-boolean v2, v1, LX/25F;->H:Z

    .line 1986698
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel$ToModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 1986699
    iput-object v2, v1, LX/25F;->T:Ljava/lang/String;

    .line 1986700
    invoke-virtual {v1}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    goto/16 :goto_7
.end method
