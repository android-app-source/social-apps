.class public LX/DiT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DiR;


# instance fields
.field private a:J

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2032142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2032143
    iput-wide p1, p0, LX/DiT;->a:J

    .line 2032144
    iput-object p3, p0, LX/DiT;->b:Ljava/lang/String;

    .line 2032145
    iput-object p4, p0, LX/DiT;->c:Ljava/lang/String;

    .line 2032146
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2032147
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2032148
    const-string v1, "amount"

    iget-wide v2, p0, LX/DiT;->a:J

    invoke-virtual {v0, v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2032149
    const-string v1, "currency"

    iget-object v2, p0, LX/DiT;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2032150
    const-string v1, "type"

    iget-object v2, p0, LX/DiT;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2032151
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/DiR;)Z
    .locals 1

    .prologue
    .line 2032152
    instance-of v0, p1, LX/DiT;

    if-nez v0, :cond_0

    .line 2032153
    const/4 v0, 0x0

    .line 2032154
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
