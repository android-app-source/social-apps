.class public LX/Elm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Em6;

.field private final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/Em6;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2164722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2164723
    iput-object p1, p0, LX/Elm;->a:LX/Em6;

    .line 2164724
    iput-object p2, p0, LX/Elm;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2164725
    return-void
.end method

.method public static final a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2164726
    if-nez p0, :cond_0

    .line 2164727
    const/4 v0, 0x0

    .line 2164728
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/Elm;->b(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Elm;
    .locals 3

    .prologue
    .line 2164729
    new-instance v2, LX/Elm;

    invoke-static {p0}, LX/Em6;->b(LX/0QB;)LX/Em6;

    move-result-object v0

    check-cast v0, LX/Em6;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/Elm;-><init>(LX/Em6;Lcom/facebook/content/SecureContextHelper;)V

    .line 2164730
    return-object v2
.end method

.method public static b(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2164731
    if-nez p0, :cond_0

    .line 2164732
    const/4 v0, 0x0

    .line 2164733
    :goto_0
    return-object v0

    .line 2164734
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2164735
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method
