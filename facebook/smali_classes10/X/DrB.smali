.class public LX/DrB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/DrB;


# instance fields
.field public final b:LX/DrF;

.field public final c:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2049040
    const-class v0, LX/DrB;

    sput-object v0, LX/DrB;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/DrF;Landroid/os/Handler;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2049041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049042
    iput-object p1, p0, LX/DrB;->b:LX/DrF;

    .line 2049043
    iput-object p2, p0, LX/DrB;->c:Landroid/os/Handler;

    .line 2049044
    iput-object p3, p0, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2049045
    return-void
.end method

.method public static a(LX/0QB;)LX/DrB;
    .locals 6

    .prologue
    .line 2049046
    sget-object v0, LX/DrB;->f:LX/DrB;

    if-nez v0, :cond_1

    .line 2049047
    const-class v1, LX/DrB;

    monitor-enter v1

    .line 2049048
    :try_start_0
    sget-object v0, LX/DrB;->f:LX/DrB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2049049
    if-eqz v2, :cond_0

    .line 2049050
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2049051
    new-instance p0, LX/DrB;

    invoke-static {v0}, LX/DrF;->a(LX/0QB;)LX/DrF;

    move-result-object v3

    check-cast v3, LX/DrF;

    invoke-static {v0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5}, LX/DrB;-><init>(LX/DrF;Landroid/os/Handler;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2049052
    move-object v0, p0

    .line 2049053
    sput-object v0, LX/DrB;->f:LX/DrB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049054
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2049055
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2049056
    :cond_1
    sget-object v0, LX/DrB;->f:LX/DrB;

    return-object v0

    .line 2049057
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2049058
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/DrB;LX/BAk;LX/BAl;I)V
    .locals 10

    .prologue
    const/4 v3, -0x1

    .line 2049059
    iget-object v0, p0, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p3}, LX/DrH;->c(I)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2049060
    iput-object v0, p2, LX/BAl;->b:Ljava/lang/String;

    .line 2049061
    iget-object v0, p0, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p3}, LX/DrH;->d(I)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 2049062
    iput v0, p2, LX/BAl;->c:I

    .line 2049063
    iget-object v0, p0, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p3}, LX/DrH;->e(I)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 2049064
    iput v0, p2, LX/BAl;->a:I

    .line 2049065
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 2049066
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2049067
    iget-object v5, p2, LX/BAl;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2049068
    const/4 v7, 0x3

    invoke-virtual {v4, v7}, LX/186;->c(I)V

    .line 2049069
    iget v7, p2, LX/BAl;->a:I

    invoke-virtual {v4, v9, v7, v9}, LX/186;->a(III)V

    .line 2049070
    invoke-virtual {v4, v8, v5}, LX/186;->b(II)V

    .line 2049071
    const/4 v5, 0x2

    iget v7, p2, LX/BAl;->c:I

    invoke-virtual {v4, v5, v7, v9}, LX/186;->a(III)V

    .line 2049072
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2049073
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2049074
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2049075
    invoke-virtual {v5, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2049076
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2049077
    new-instance v5, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;

    invoke-direct {v5, v4}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;-><init>(LX/15i;)V

    .line 2049078
    move-object v0, v5

    .line 2049079
    iput-object v0, p1, LX/BAk;->c:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;

    .line 2049080
    return-void
.end method
