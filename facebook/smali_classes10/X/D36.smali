.class public final enum LX/D36;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D36;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D36;

.field public static final enum EXTERNAL:LX/D36;

.field public static final enum WEBVIEW:LX/D36;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1959524
    new-instance v0, LX/D36;

    const-string v1, "WEBVIEW"

    invoke-direct {v0, v1, v2}, LX/D36;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D36;->WEBVIEW:LX/D36;

    .line 1959525
    new-instance v0, LX/D36;

    const-string v1, "EXTERNAL"

    invoke-direct {v0, v1, v3}, LX/D36;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D36;->EXTERNAL:LX/D36;

    .line 1959526
    const/4 v0, 0x2

    new-array v0, v0, [LX/D36;

    sget-object v1, LX/D36;->WEBVIEW:LX/D36;

    aput-object v1, v0, v2

    sget-object v1, LX/D36;->EXTERNAL:LX/D36;

    aput-object v1, v0, v3

    sput-object v0, LX/D36;->$VALUES:[LX/D36;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1959523
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D36;
    .locals 1

    .prologue
    .line 1959528
    const-class v0, LX/D36;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D36;

    return-object v0
.end method

.method public static values()[LX/D36;
    .locals 1

    .prologue
    .line 1959527
    sget-object v0, LX/D36;->$VALUES:[LX/D36;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D36;

    return-object v0
.end method
