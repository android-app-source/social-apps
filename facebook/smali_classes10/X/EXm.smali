.class public final LX/EXm;
.super LX/EWy;
.source ""

# interfaces
.implements LX/EXl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWy",
        "<",
        "LX/EXn;",
        "LX/EXm;",
        ">;",
        "LX/EXl;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2135276
    invoke-direct {p0}, LX/EWy;-><init>()V

    .line 2135277
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXm;->b:Ljava/util/List;

    .line 2135278
    invoke-direct {p0}, LX/EXm;->w()V

    .line 2135279
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2135222
    invoke-direct {p0, p1}, LX/EWy;-><init>(LX/EYd;)V

    .line 2135223
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXm;->b:Ljava/util/List;

    .line 2135224
    invoke-direct {p0}, LX/EXm;->w()V

    .line 2135225
    return-void
.end method

.method private A()LX/EXn;
    .locals 2

    .prologue
    .line 2135226
    invoke-virtual {p0}, LX/EXm;->l()LX/EXn;

    move-result-object v0

    .line 2135227
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2135228
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2135229
    :cond_0
    return-object v0
.end method

.method private D()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2135214
    iget-object v1, p0, LX/EXm;->c:LX/EZ2;

    if-nez v1, :cond_0

    .line 2135215
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EXm;->b:Ljava/util/List;

    iget v3, p0, LX/EXm;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2135216
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2135217
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EXm;->c:LX/EZ2;

    .line 2135218
    const/4 v0, 0x0

    iput-object v0, p0, LX/EXm;->b:Ljava/util/List;

    .line 2135219
    :cond_0
    iget-object v0, p0, LX/EXm;->c:LX/EZ2;

    return-object v0

    .line 2135220
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/EXm;
    .locals 1

    .prologue
    .line 2135230
    instance-of v0, p1, LX/EXn;

    if-eqz v0, :cond_0

    .line 2135231
    check-cast p1, LX/EXn;

    invoke-virtual {p0, p1}, LX/EXm;->a(LX/EXn;)LX/EXm;

    move-result-object p0

    .line 2135232
    :goto_0
    return-object p0

    .line 2135233
    :cond_0
    invoke-super {p0, p1}, LX/EWy;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EXm;
    .locals 4

    .prologue
    .line 2135234
    const/4 v2, 0x0

    .line 2135235
    :try_start_0
    sget-object v0, LX/EXn;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXn;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2135236
    if-eqz v0, :cond_0

    .line 2135237
    invoke-virtual {p0, v0}, LX/EXm;->a(LX/EXn;)LX/EXm;

    .line 2135238
    :cond_0
    return-object p0

    .line 2135239
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2135240
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2135241
    check-cast v0, LX/EXn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2135242
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2135243
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2135244
    invoke-virtual {p0, v1}, LX/EXm;->a(LX/EXn;)LX/EXm;

    :cond_1
    throw v0

    .line 2135245
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 2135246
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2135247
    invoke-direct {p0}, LX/EXm;->D()LX/EZ2;

    .line 2135248
    :cond_0
    return-void
.end method

.method public static x()LX/EXm;
    .locals 1

    .prologue
    .line 2135249
    new-instance v0, LX/EXm;

    invoke-direct {v0}, LX/EXm;-><init>()V

    return-object v0
.end method

.method private y()LX/EXm;
    .locals 2

    .prologue
    .line 2135250
    invoke-static {}, LX/EXm;->x()LX/EXm;

    move-result-object v0

    invoke-virtual {p0}, LX/EXm;->l()LX/EXn;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EXm;->a(LX/EXn;)LX/EXm;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2135251
    invoke-direct {p0, p1}, LX/EXm;->d(LX/EWY;)LX/EXm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2135252
    invoke-direct {p0, p1, p2}, LX/EXm;->d(LX/EWd;LX/EYZ;)LX/EXm;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EXn;)LX/EXm;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2135253
    sget-object v1, LX/EXn;->c:LX/EXn;

    move-object v1, v1

    .line 2135254
    if-ne p1, v1, :cond_0

    .line 2135255
    :goto_0
    return-object p0

    .line 2135256
    :cond_0
    iget-object v1, p0, LX/EXm;->c:LX/EZ2;

    if-nez v1, :cond_4

    .line 2135257
    iget-object v0, p1, LX/EXn;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2135258
    iget-object v0, p0, LX/EXm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2135259
    iget-object v0, p1, LX/EXn;->uninterpretedOption_:Ljava/util/List;

    iput-object v0, p0, LX/EXm;->b:Ljava/util/List;

    .line 2135260
    iget v0, p0, LX/EXm;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, LX/EXm;->a:I

    .line 2135261
    :goto_1
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2135262
    :cond_1
    :goto_2
    invoke-virtual {p0, p1}, LX/EWy;->a(LX/EX1;)V

    .line 2135263
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    .line 2135264
    :cond_2
    iget v0, p0, LX/EXm;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    .line 2135265
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EXm;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EXm;->b:Ljava/util/List;

    .line 2135266
    iget v0, p0, LX/EXm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EXm;->a:I

    .line 2135267
    :cond_3
    iget-object v0, p0, LX/EXm;->b:Ljava/util/List;

    iget-object v1, p1, LX/EXn;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2135268
    :cond_4
    iget-object v1, p1, LX/EXn;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2135269
    iget-object v1, p0, LX/EXm;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2135270
    iget-object v1, p0, LX/EXm;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2135271
    iput-object v0, p0, LX/EXm;->c:LX/EZ2;

    .line 2135272
    iget-object v1, p1, LX/EXn;->uninterpretedOption_:Ljava/util/List;

    iput-object v1, p0, LX/EXm;->b:Ljava/util/List;

    .line 2135273
    iget v1, p0, LX/EXm;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/EXm;->a:I

    .line 2135274
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_5

    invoke-direct {p0}, LX/EXm;->D()LX/EZ2;

    move-result-object v0

    :cond_5
    iput-object v0, p0, LX/EXm;->c:LX/EZ2;

    goto :goto_2

    .line 2135275
    :cond_6
    iget-object v0, p0, LX/EXm;->c:LX/EZ2;

    iget-object v1, p1, LX/EXn;->uninterpretedOption_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_2
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2135201
    move v0, v1

    .line 2135202
    :goto_0
    iget-object v2, p0, LX/EXm;->c:LX/EZ2;

    if-nez v2, :cond_3

    .line 2135203
    iget-object v2, p0, LX/EXm;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2135204
    :goto_1
    move v2, v2

    .line 2135205
    if-ge v0, v2, :cond_2

    .line 2135206
    iget-object v2, p0, LX/EXm;->c:LX/EZ2;

    if-nez v2, :cond_4

    .line 2135207
    iget-object v2, p0, LX/EXm;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EYB;

    .line 2135208
    :goto_2
    move-object v2, v2

    .line 2135209
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2135210
    :cond_0
    :goto_3
    return v1

    .line 2135211
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2135212
    :cond_2
    invoke-virtual {p0}, LX/EWy;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2135213
    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    iget-object v2, p0, LX/EXm;->c:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_1

    :cond_4
    iget-object v2, p0, LX/EXm;->c:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EYB;

    goto :goto_2
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2135280
    invoke-direct {p0, p1, p2}, LX/EXm;->d(LX/EWd;LX/EYZ;)LX/EXm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2135221
    invoke-direct {p0}, LX/EXm;->y()LX/EXm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2135175
    invoke-direct {p0, p1, p2}, LX/EXm;->d(LX/EWd;LX/EYZ;)LX/EXm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2135176
    invoke-direct {p0}, LX/EXm;->y()LX/EXm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2135177
    invoke-direct {p0, p1}, LX/EXm;->d(LX/EWY;)LX/EXm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2135178
    invoke-direct {p0}, LX/EXm;->y()LX/EXm;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2135179
    sget-object v0, LX/EYC;->F:LX/EYn;

    const-class v1, LX/EXn;

    const-class v2, LX/EXm;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2135180
    sget-object v0, LX/EYC;->E:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2135181
    invoke-direct {p0}, LX/EXm;->y()LX/EXm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2135182
    invoke-virtual {p0}, LX/EXm;->l()LX/EXn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2135183
    invoke-direct {p0}, LX/EXm;->A()LX/EXn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2135184
    invoke-virtual {p0}, LX/EXm;->l()LX/EXn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2135185
    invoke-direct {p0}, LX/EXm;->A()LX/EXn;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EXn;
    .locals 3

    .prologue
    .line 2135186
    new-instance v0, LX/EXn;

    invoke-direct {v0, p0}, LX/EXn;-><init>(LX/EWy;)V

    .line 2135187
    iget-object v1, p0, LX/EXm;->c:LX/EZ2;

    if-nez v1, :cond_1

    .line 2135188
    iget v1, p0, LX/EXm;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2135189
    iget-object v1, p0, LX/EXm;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXm;->b:Ljava/util/List;

    .line 2135190
    iget v1, p0, LX/EXm;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/EXm;->a:I

    .line 2135191
    :cond_0
    iget-object v1, p0, LX/EXm;->b:Ljava/util/List;

    .line 2135192
    iput-object v1, v0, LX/EXn;->uninterpretedOption_:Ljava/util/List;

    .line 2135193
    :goto_0
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2135194
    return-object v0

    .line 2135195
    :cond_1
    iget-object v1, p0, LX/EXm;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2135196
    iput-object v1, v0, LX/EXn;->uninterpretedOption_:Ljava/util/List;

    .line 2135197
    goto :goto_0
.end method

.method public final synthetic m()LX/EWy;
    .locals 1

    .prologue
    .line 2135198
    invoke-direct {p0}, LX/EXm;->y()LX/EXm;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2135199
    sget-object v0, LX/EXn;->c:LX/EXn;

    move-object v0, v0

    .line 2135200
    return-object v0
.end method
