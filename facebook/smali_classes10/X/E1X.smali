.class public final enum LX/E1X;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/E1X;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/E1X;

.field public static final enum TOUCH_SETTINGS:LX/E1X;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2069825
    new-instance v0, LX/E1X;

    const-string v1, "TOUCH_SETTINGS"

    invoke-direct {v0, v1, v2}, LX/E1X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E1X;->TOUCH_SETTINGS:LX/E1X;

    .line 2069826
    const/4 v0, 0x1

    new-array v0, v0, [LX/E1X;

    sget-object v1, LX/E1X;->TOUCH_SETTINGS:LX/E1X;

    aput-object v1, v0, v2

    sput-object v0, LX/E1X;->$VALUES:[LX/E1X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2069827
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/E1X;
    .locals 1

    .prologue
    .line 2069828
    const-class v0, LX/E1X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/E1X;

    return-object v0
.end method

.method public static values()[LX/E1X;
    .locals 1

    .prologue
    .line 2069829
    sget-object v0, LX/E1X;->$VALUES:[LX/E1X;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/E1X;

    return-object v0
.end method
