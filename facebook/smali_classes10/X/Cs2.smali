.class public LX/Cs2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1941635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941636
    return-void
.end method

.method public static a(Ljava/lang/String;LX/Cs1;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x4

    .line 1941637
    invoke-static {p0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1941638
    if-nez v1, :cond_1

    .line 1941639
    :cond_0
    :goto_0
    return-object v0

    .line 1941640
    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 1941641
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1941642
    sget-object v0, LX/Cs0;->a:[I

    invoke-virtual {p1}, LX/Cs1;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 1941643
    goto :goto_0

    :pswitch_0
    move-object v0, v1

    .line 1941644
    goto :goto_0

    .line 1941645
    :pswitch_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_2

    const-string v0, "www."

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1941646
    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1941647
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
