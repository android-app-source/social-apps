.class public final LX/ELE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ELF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field public f:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Ljava/lang/CharSequence;

.field public i:Landroid/view/View$OnClickListener;

.field public j:Ljava/lang/CharSequence;

.field public k:Landroid/view/View$OnClickListener;

.field public l:Landroid/view/View$OnClickListener;

.field public m:Landroid/view/View$OnClickListener;

.field public final synthetic n:LX/ELF;


# direct methods
.method public constructor <init>(LX/ELF;)V
    .locals 1

    .prologue
    .line 2108251
    iput-object p1, p0, LX/ELE;->n:LX/ELF;

    .line 2108252
    move-object v0, p1

    .line 2108253
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2108254
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2108255
    const-string v0, "SearchResultsEntityComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2108256
    if-ne p0, p1, :cond_1

    .line 2108257
    :cond_0
    :goto_0
    return v0

    .line 2108258
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2108259
    goto :goto_0

    .line 2108260
    :cond_3
    check-cast p1, LX/ELE;

    .line 2108261
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2108262
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2108263
    if-eq v2, v3, :cond_0

    .line 2108264
    iget-object v2, p0, LX/ELE;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ELE;->a:Landroid/net/Uri;

    iget-object v3, p1, LX/ELE;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2108265
    goto :goto_0

    .line 2108266
    :cond_5
    iget-object v2, p1, LX/ELE;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 2108267
    :cond_6
    iget-object v2, p0, LX/ELE;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ELE;->b:Ljava/lang/String;

    iget-object v3, p1, LX/ELE;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2108268
    goto :goto_0

    .line 2108269
    :cond_8
    iget-object v2, p1, LX/ELE;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2108270
    :cond_9
    iget-object v2, p0, LX/ELE;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/ELE;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELE;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2108271
    goto :goto_0

    .line 2108272
    :cond_b
    iget-object v2, p1, LX/ELE;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_a

    .line 2108273
    :cond_c
    iget-object v2, p0, LX/ELE;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/ELE;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELE;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2108274
    goto :goto_0

    .line 2108275
    :cond_e
    iget-object v2, p1, LX/ELE;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_d

    .line 2108276
    :cond_f
    iget-object v2, p0, LX/ELE;->e:Ljava/lang/CharSequence;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/ELE;->e:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELE;->e:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 2108277
    goto :goto_0

    .line 2108278
    :cond_11
    iget-object v2, p1, LX/ELE;->e:Ljava/lang/CharSequence;

    if-nez v2, :cond_10

    .line 2108279
    :cond_12
    iget-object v2, p0, LX/ELE;->f:LX/1dc;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/ELE;->f:LX/1dc;

    iget-object v3, p1, LX/ELE;->f:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 2108280
    goto/16 :goto_0

    .line 2108281
    :cond_14
    iget-object v2, p1, LX/ELE;->f:LX/1dc;

    if-nez v2, :cond_13

    .line 2108282
    :cond_15
    iget-boolean v2, p0, LX/ELE;->g:Z

    iget-boolean v3, p1, LX/ELE;->g:Z

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 2108283
    goto/16 :goto_0

    .line 2108284
    :cond_16
    iget-object v2, p0, LX/ELE;->h:Ljava/lang/CharSequence;

    if-eqz v2, :cond_18

    iget-object v2, p0, LX/ELE;->h:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELE;->h:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 2108285
    goto/16 :goto_0

    .line 2108286
    :cond_18
    iget-object v2, p1, LX/ELE;->h:Ljava/lang/CharSequence;

    if-nez v2, :cond_17

    .line 2108287
    :cond_19
    iget-object v2, p0, LX/ELE;->i:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_1b

    iget-object v2, p0, LX/ELE;->i:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/ELE;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_1a
    move v0, v1

    .line 2108288
    goto/16 :goto_0

    .line 2108289
    :cond_1b
    iget-object v2, p1, LX/ELE;->i:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_1a

    .line 2108290
    :cond_1c
    iget-object v2, p0, LX/ELE;->j:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1e

    iget-object v2, p0, LX/ELE;->j:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ELE;->j:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    :cond_1d
    move v0, v1

    .line 2108291
    goto/16 :goto_0

    .line 2108292
    :cond_1e
    iget-object v2, p1, LX/ELE;->j:Ljava/lang/CharSequence;

    if-nez v2, :cond_1d

    .line 2108293
    :cond_1f
    iget-object v2, p0, LX/ELE;->k:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_21

    iget-object v2, p0, LX/ELE;->k:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/ELE;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    :cond_20
    move v0, v1

    .line 2108294
    goto/16 :goto_0

    .line 2108295
    :cond_21
    iget-object v2, p1, LX/ELE;->k:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_20

    .line 2108296
    :cond_22
    iget-object v2, p0, LX/ELE;->l:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_24

    iget-object v2, p0, LX/ELE;->l:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/ELE;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_25

    :cond_23
    move v0, v1

    .line 2108297
    goto/16 :goto_0

    .line 2108298
    :cond_24
    iget-object v2, p1, LX/ELE;->l:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_23

    .line 2108299
    :cond_25
    iget-object v2, p0, LX/ELE;->m:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_26

    iget-object v2, p0, LX/ELE;->m:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/ELE;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2108300
    goto/16 :goto_0

    .line 2108301
    :cond_26
    iget-object v2, p1, LX/ELE;->m:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
