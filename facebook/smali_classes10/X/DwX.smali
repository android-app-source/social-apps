.class public final LX/DwX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dwa;


# direct methods
.method public constructor <init>(LX/Dwa;)V
    .locals 0

    .prologue
    .line 2060567
    iput-object p1, p0, LX/DwX;->a:LX/Dwa;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2060568
    iget-object v0, p0, LX/DwX;->a:LX/Dwa;

    iget-object v0, v0, LX/Dvb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchAlbumMediaSet"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2060569
    iget-object v0, p0, LX/DwX;->a:LX/Dwa;

    const v1, 0x691063

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2060570
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2060571
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2060572
    if-eqz p1, :cond_0

    .line 2060573
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2060574
    if-nez v0, :cond_1

    .line 2060575
    :cond_0
    :goto_0
    return-void

    .line 2060576
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;

    .line 2060577
    if-eqz v0, :cond_0

    .line 2060578
    iget-object v1, p0, LX/DwX;->a:LX/Dwa;

    iget-object v1, v1, LX/Dvb;->i:LX/Dvc;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;->a:LX/0Px;

    invoke-virtual {v1, v0}, LX/Dvc;->a(LX/0Px;)V

    .line 2060579
    iget-object v0, p0, LX/DwX;->a:LX/Dwa;

    invoke-static {v0}, LX/Dwa;->s(LX/Dwa;)LX/Dv0;

    move-result-object v0

    .line 2060580
    iget-object v1, p0, LX/DwX;->a:LX/Dwa;

    if-eqz v0, :cond_2

    .line 2060581
    iget-boolean p1, v0, LX/Dv0;->c:Z

    move v0, p1

    .line 2060582
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 2060583
    :goto_1
    iput-boolean v0, v1, LX/Dwa;->m:Z

    .line 2060584
    iget-object v0, p0, LX/DwX;->a:LX/Dwa;

    const v1, -0x16b1115c

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2060585
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
