.class public final LX/Dr7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V
    .locals 0

    .prologue
    .line 2048972
    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2048973
    iput-object p1, p0, LX/Dr7;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    .line 2048974
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2048987
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2048975
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2048976
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2048977
    if-eqz v0, :cond_0

    .line 2048978
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2048979
    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2048980
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2048981
    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2048982
    :cond_0
    :goto_0
    return-void

    .line 2048983
    :cond_1
    iget-object v0, p0, LX/Dr7;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    if-eqz v0, :cond_0

    .line 2048984
    iget-object v1, p0, LX/Dr7;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    .line 2048985
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2048986
    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->a(Ljava/util/List;)V

    goto :goto_0
.end method
