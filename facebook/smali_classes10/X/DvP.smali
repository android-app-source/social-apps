.class public final LX/DvP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;",
        ">;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DvQ;


# direct methods
.method public constructor <init>(LX/DvQ;)V
    .locals 0

    .prologue
    .line 2058500
    iput-object p1, p0, LX/DvP;->a:LX/DvQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2058489
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2058490
    if-eqz p1, :cond_0

    .line 2058491
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058492
    if-nez v0, :cond_1

    .line 2058493
    :cond_0
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2058494
    :goto_0
    return-object v0

    .line 2058495
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058496
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;

    invoke-static {v0}, LX/DvM;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;)LX/0Px;

    move-result-object v1

    .line 2058497
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058498
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;

    invoke-static {v0}, LX/DvM;->b(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 2058499
    new-instance v2, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;

    invoke-direct {v2, v0, v1}, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;-><init>(Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0Px;)V

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
