.class public LX/Dy1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Dy1;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2063889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2063890
    iput-object p1, p0, LX/Dy1;->a:Landroid/content/Context;

    .line 2063891
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Dy1;->b:Ljava/util/Map;

    .line 2063892
    iget-object v0, p0, LX/Dy1;->b:Ljava/util/Map;

    const-string v1, "everyone"

    iget-object v2, p0, LX/Dy1;->a:Landroid/content/Context;

    const v3, 0x7f081278

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063893
    iget-object v0, p0, LX/Dy1;->b:Ljava/util/Map;

    const-string v1, "facebook"

    iget-object v2, p0, LX/Dy1;->a:Landroid/content/Context;

    const v3, 0x7f081279

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063894
    iget-object v0, p0, LX/Dy1;->b:Ljava/util/Map;

    const-string v1, "only_me"

    iget-object v2, p0, LX/Dy1;->a:Landroid/content/Context;

    const v3, 0x7f08127b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063895
    iget-object v0, p0, LX/Dy1;->b:Ljava/util/Map;

    const-string v1, "friends"

    iget-object v2, p0, LX/Dy1;->a:Landroid/content/Context;

    const v3, 0x7f08127a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063896
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Dy1;->c:Ljava/util/Map;

    .line 2063897
    iget-object v0, p0, LX/Dy1;->c:Ljava/util/Map;

    const-string v1, "everyone"

    iget-object v2, p0, LX/Dy1;->a:Landroid/content/Context;

    const v3, 0x7f0812d2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063898
    iget-object v0, p0, LX/Dy1;->c:Ljava/util/Map;

    const-string v1, "facebook"

    iget-object v2, p0, LX/Dy1;->a:Landroid/content/Context;

    const v3, 0x7f081279

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063899
    iget-object v0, p0, LX/Dy1;->c:Ljava/util/Map;

    const-string v1, "only_me"

    iget-object v2, p0, LX/Dy1;->a:Landroid/content/Context;

    const v3, 0x7f0812d8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063900
    iget-object v0, p0, LX/Dy1;->c:Ljava/util/Map;

    const-string v1, "friends"

    iget-object v2, p0, LX/Dy1;->a:Landroid/content/Context;

    const v3, 0x7f0812d5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063901
    return-void
.end method

.method public static a(LX/0QB;)LX/Dy1;
    .locals 4

    .prologue
    .line 2063906
    sget-object v0, LX/Dy1;->d:LX/Dy1;

    if-nez v0, :cond_1

    .line 2063907
    const-class v1, LX/Dy1;

    monitor-enter v1

    .line 2063908
    :try_start_0
    sget-object v0, LX/Dy1;->d:LX/Dy1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2063909
    if-eqz v2, :cond_0

    .line 2063910
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2063911
    new-instance p0, LX/Dy1;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/Dy1;-><init>(Landroid/content/Context;)V

    .line 2063912
    move-object v0, p0

    .line 2063913
    sput-object v0, LX/Dy1;->d:LX/Dy1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2063914
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2063915
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2063916
    :cond_1
    sget-object v0, LX/Dy1;->d:LX/Dy1;

    return-object v0

    .line 2063917
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2063918
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Dy1;LX/1Fd;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2063919
    iget-object v0, p0, LX/Dy1;->c:Ljava/util/Map;

    invoke-interface {p1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2063920
    iget-object v0, p0, LX/Dy1;->b:Ljava/util/Map;

    invoke-interface {p1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2063921
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Dy1;->a:Landroid/content/Context;

    const v1, 0x7f08127a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/Dy1;LX/1Fd;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2063903
    iget-object v0, p0, LX/Dy1;->c:Ljava/util/Map;

    invoke-interface {p1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2063904
    iget-object v0, p0, LX/Dy1;->c:Ljava/util/Map;

    invoke-interface {p1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2063905
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1oV;)Z
    .locals 2

    .prologue
    .line 2063902
    iget-object v0, p0, LX/Dy1;->b:Ljava/util/Map;

    invoke-interface {p1}, LX/1oV;->b()LX/1Fd;

    move-result-object v1

    invoke-interface {v1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
