.class public LX/EoZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/EoZ;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0jN;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0id;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0id;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0jN;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0id;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2168252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2168253
    iput-object p2, p0, LX/EoZ;->b:LX/0Or;

    .line 2168254
    iput-object p1, p0, LX/EoZ;->a:LX/0Or;

    .line 2168255
    iput-object p3, p0, LX/EoZ;->c:LX/0Or;

    .line 2168256
    iput-object p4, p0, LX/EoZ;->d:LX/0id;

    .line 2168257
    return-void
.end method

.method public static a(LX/0QB;)LX/EoZ;
    .locals 7

    .prologue
    .line 2168258
    sget-object v0, LX/EoZ;->e:LX/EoZ;

    if-nez v0, :cond_1

    .line 2168259
    const-class v1, LX/EoZ;

    monitor-enter v1

    .line 2168260
    :try_start_0
    sget-object v0, LX/EoZ;->e:LX/EoZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2168261
    if-eqz v2, :cond_0

    .line 2168262
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2168263
    new-instance v4, LX/EoZ;

    const/16 v3, 0xc49

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v3, 0x50d

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v3, 0x455

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v3

    check-cast v3, LX/0id;

    invoke-direct {v4, v5, v6, p0, v3}, LX/EoZ;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/0id;)V

    .line 2168264
    move-object v0, v4

    .line 2168265
    sput-object v0, LX/EoZ;->e:LX/EoZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2168266
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2168267
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2168268
    :cond_1
    sget-object v0, LX/EoZ;->e:LX/EoZ;

    return-object v0

    .line 2168269
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2168270
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Eon;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2168271
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/Eon;->y()LX/Eos;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/Eon;->y()LX/Eos;

    move-result-object v0

    invoke-interface {v0}, LX/Eos;->b()LX/Eor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/Eon;->y()LX/Eos;

    move-result-object v0

    invoke-interface {v0}, LX/Eos;->b()LX/Eor;

    move-result-object v0

    invoke-interface {v0}, LX/Eor;->b()LX/1Fb;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2168272
    :cond_0
    const/4 v0, 0x0

    .line 2168273
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, LX/Eon;->y()LX/Eos;

    move-result-object v0

    invoke-interface {v0}, LX/Eos;->b()LX/Eor;

    move-result-object v0

    invoke-interface {v0}, LX/Eor;->b()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/Eon;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2168274
    iget-object v0, p0, LX/EoZ;->d:LX/0id;

    const-string v1, "TimelineLauncher"

    invoke-virtual {v0, p1, v1}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2168275
    invoke-interface {p2}, LX/Eon;->c()Ljava/lang/String;

    move-result-object v1

    .line 2168276
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v4

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2168277
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2168278
    instance-of v2, p2, LX/Eop;

    if-eqz v2, :cond_0

    move-object v2, p2

    .line 2168279
    check-cast v2, LX/Eop;

    .line 2168280
    invoke-interface {v2}, LX/Eop;->w()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, LX/Eop;->w()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2168281
    invoke-interface {v2}, LX/Eop;->w()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextUserFieldsModel$TimelineContextItemsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;

    .line 2168282
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->kY_()Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->kY_()Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    if-eq v3, v4, :cond_0

    .line 2168283
    const-string v3, "timeline_context_item_type"

    invoke-virtual {v2}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->kY_()Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2168284
    :cond_0
    invoke-interface {p2}, LX/Eon;->C()LX/1Fb;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {p2}, LX/Eon;->C()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-interface {p2}, LX/Eon;->ce_()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, LX/EoZ;->a(LX/Eon;)Ljava/lang/String;

    move-result-object v4

    .line 2168285
    if-eqz p2, :cond_1

    invoke-interface {p2}, LX/Eon;->y()LX/Eos;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-static {p2}, LX/EoZ;->a(LX/Eon;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_4

    .line 2168286
    :cond_1
    const/4 v5, 0x0

    .line 2168287
    :goto_1
    move-object v5, v5

    .line 2168288
    invoke-static/range {v0 .. v5}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V

    .line 2168289
    iget-object v1, p0, LX/EoZ;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-interface {v1, p1, v6}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2168290
    if-eqz v1, :cond_3

    .line 2168291
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2168292
    iget-object v0, p0, LX/EoZ;->d:LX/0id;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0id;->q(Ljava/lang/String;)V

    .line 2168293
    iget-object v0, p0, LX/EoZ;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jN;

    invoke-virtual {v0, v1}, LX/0jN;->a(Landroid/content/Intent;)V

    .line 2168294
    iget-object v0, p0, LX/EoZ;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2168295
    iget-object v0, p0, LX/EoZ;->d:LX/0id;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->r(Ljava/lang/String;)V

    .line 2168296
    :goto_2
    return-void

    .line 2168297
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 2168298
    :cond_3
    iget-object v0, p0, LX/EoZ;->d:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    goto :goto_2

    :cond_4
    invoke-interface {p2}, LX/Eon;->y()LX/Eos;

    move-result-object v5

    invoke-interface {v5}, LX/Eos;->a()LX/1f8;

    move-result-object v5

    goto :goto_1
.end method
