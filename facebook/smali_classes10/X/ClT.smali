.class public final enum LX/ClT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ClT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ClT;

.field public static final enum AUDIO:LX/ClT;

.field public static final enum COPYRIGHT:LX/ClT;

.field public static final enum LOADING_INDICATOR:LX/ClT;

.field public static final enum LOCATION:LX/ClT;

.field public static final enum SUBTITLE:LX/ClT;

.field public static final enum TITLE:LX/ClT;

.field public static final enum UFI:LX/ClT;

.field public static final enum VIDEO_CONTROL:LX/ClT;

.field public static final enum VIDEO_SEEK_BAR:LX/ClT;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1932485
    new-instance v0, LX/ClT;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v3}, LX/ClT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClT;->TITLE:LX/ClT;

    .line 1932486
    new-instance v0, LX/ClT;

    const-string v1, "SUBTITLE"

    invoke-direct {v0, v1, v4}, LX/ClT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClT;->SUBTITLE:LX/ClT;

    .line 1932487
    new-instance v0, LX/ClT;

    const-string v1, "COPYRIGHT"

    invoke-direct {v0, v1, v5}, LX/ClT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClT;->COPYRIGHT:LX/ClT;

    .line 1932488
    new-instance v0, LX/ClT;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v6}, LX/ClT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClT;->AUDIO:LX/ClT;

    .line 1932489
    new-instance v0, LX/ClT;

    const-string v1, "LOCATION"

    invoke-direct {v0, v1, v7}, LX/ClT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClT;->LOCATION:LX/ClT;

    .line 1932490
    new-instance v0, LX/ClT;

    const-string v1, "VIDEO_CONTROL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/ClT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClT;->VIDEO_CONTROL:LX/ClT;

    .line 1932491
    new-instance v0, LX/ClT;

    const-string v1, "VIDEO_SEEK_BAR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/ClT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClT;->VIDEO_SEEK_BAR:LX/ClT;

    .line 1932492
    new-instance v0, LX/ClT;

    const-string v1, "UFI"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/ClT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClT;->UFI:LX/ClT;

    .line 1932493
    new-instance v0, LX/ClT;

    const-string v1, "LOADING_INDICATOR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/ClT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClT;->LOADING_INDICATOR:LX/ClT;

    .line 1932494
    const/16 v0, 0x9

    new-array v0, v0, [LX/ClT;

    sget-object v1, LX/ClT;->TITLE:LX/ClT;

    aput-object v1, v0, v3

    sget-object v1, LX/ClT;->SUBTITLE:LX/ClT;

    aput-object v1, v0, v4

    sget-object v1, LX/ClT;->COPYRIGHT:LX/ClT;

    aput-object v1, v0, v5

    sget-object v1, LX/ClT;->AUDIO:LX/ClT;

    aput-object v1, v0, v6

    sget-object v1, LX/ClT;->LOCATION:LX/ClT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/ClT;->VIDEO_CONTROL:LX/ClT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/ClT;->VIDEO_SEEK_BAR:LX/ClT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/ClT;->UFI:LX/ClT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/ClT;->LOADING_INDICATOR:LX/ClT;

    aput-object v2, v0, v1

    sput-object v0, LX/ClT;->$VALUES:[LX/ClT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1932495
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ClT;
    .locals 1

    .prologue
    .line 1932496
    const-class v0, LX/ClT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ClT;

    return-object v0
.end method

.method public static values()[LX/ClT;
    .locals 1

    .prologue
    .line 1932497
    sget-object v0, LX/ClT;->$VALUES:[LX/ClT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ClT;

    return-object v0
.end method
