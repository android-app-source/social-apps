.class public final LX/EDF;
.super Landroid/os/AsyncTask;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/14U;

.field public final synthetic e:Lcom/facebook/webrtc/TurnAllocatorCallback;

.field public final synthetic f:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/14U;Lcom/facebook/webrtc/TurnAllocatorCallback;)V
    .locals 0

    .prologue
    .line 2091115
    iput-object p1, p0, LX/EDF;->f:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    iput-object p2, p0, LX/EDF;->a:Ljava/lang/String;

    iput-object p3, p0, LX/EDF;->b:Ljava/lang/String;

    iput-object p4, p0, LX/EDF;->c:Ljava/lang/String;

    iput-object p5, p0, LX/EDF;->d:LX/14U;

    iput-object p6, p0, LX/EDF;->e:Lcom/facebook/webrtc/TurnAllocatorCallback;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public final doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2091116
    const/4 v1, 0x0

    .line 2091117
    :try_start_0
    iget-object v0, p0, LX/EDF;->f:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    iget-object v0, v0, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->l:LX/11H;

    iget-object v2, p0, LX/EDF;->f:Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;

    iget-object v2, v2, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->m:LX/3GG;

    new-instance v3, LX/EDU;

    iget-object v4, p0, LX/EDF;->a:Ljava/lang/String;

    iget-object v5, p0, LX/EDF;->b:Ljava/lang/String;

    const-string v6, "UTF8"

    invoke-static {v5, v6}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/EDF;->c:Ljava/lang/String;

    const-string v7, "UTF8"

    invoke-static {v6, v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, LX/EDU;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, LX/EDF;->d:LX/14U;

    sget-object v5, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->w:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3, v4, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2091118
    :goto_0
    if-eqz v0, :cond_0

    .line 2091119
    iget-object v2, p0, LX/EDF;->e:Lcom/facebook/webrtc/TurnAllocatorCallback;

    invoke-virtual {v2, v0}, Lcom/facebook/webrtc/TurnAllocatorCallback;->turnAllocationSuccess(Ljava/lang/String;)V

    .line 2091120
    :goto_1
    return-object v1

    .line 2091121
    :catch_0
    move-exception v0

    .line 2091122
    sget-object v2, Lcom/facebook/rtc/fbwebrtc/WebrtcConfigHandler;->a:Ljava/lang/Class;

    const-string v3, "failed to read turn config. partial data will be returned"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    .line 2091123
    :cond_0
    iget-object v0, p0, LX/EDF;->e:Lcom/facebook/webrtc/TurnAllocatorCallback;

    invoke-virtual {v0}, Lcom/facebook/webrtc/TurnAllocatorCallback;->turnAllocationFailure()V

    goto :goto_1
.end method
