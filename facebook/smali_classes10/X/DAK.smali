.class public final enum LX/DAK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DAK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DAK;

.field public static final enum FAIL:LX/DAK;

.field public static final enum RETRY:LX/DAK;

.field public static final enum SUCCESS:LX/DAK;

.field public static final enum UNKNOWN:LX/DAK;


# instance fields
.field public final statusCode:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1971065
    new-instance v0, LX/DAK;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2, v2}, LX/DAK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DAK;->SUCCESS:LX/DAK;

    .line 1971066
    new-instance v0, LX/DAK;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v3, v3}, LX/DAK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DAK;->FAIL:LX/DAK;

    .line 1971067
    new-instance v0, LX/DAK;

    const-string v1, "RETRY"

    invoke-direct {v0, v1, v4, v4}, LX/DAK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DAK;->RETRY:LX/DAK;

    .line 1971068
    new-instance v0, LX/DAK;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5, v5}, LX/DAK;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DAK;->UNKNOWN:LX/DAK;

    .line 1971069
    const/4 v0, 0x4

    new-array v0, v0, [LX/DAK;

    sget-object v1, LX/DAK;->SUCCESS:LX/DAK;

    aput-object v1, v0, v2

    sget-object v1, LX/DAK;->FAIL:LX/DAK;

    aput-object v1, v0, v3

    sget-object v1, LX/DAK;->RETRY:LX/DAK;

    aput-object v1, v0, v4

    sget-object v1, LX/DAK;->UNKNOWN:LX/DAK;

    aput-object v1, v0, v5

    sput-object v0, LX/DAK;->$VALUES:[LX/DAK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1971060
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1971061
    iput p3, p0, LX/DAK;->statusCode:I

    .line 1971062
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DAK;
    .locals 1

    .prologue
    .line 1971064
    const-class v0, LX/DAK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DAK;

    return-object v0
.end method

.method public static values()[LX/DAK;
    .locals 1

    .prologue
    .line 1971063
    sget-object v0, LX/DAK;->$VALUES:[LX/DAK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DAK;

    return-object v0
.end method
