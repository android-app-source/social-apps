.class public LX/Eh9;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Eh7;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EhA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2158059
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Eh9;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EhA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2158060
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2158061
    iput-object p1, p0, LX/Eh9;->b:LX/0Ot;

    .line 2158062
    return-void
.end method

.method public static a(LX/0QB;)LX/Eh9;
    .locals 4

    .prologue
    .line 2158063
    const-class v1, LX/Eh9;

    monitor-enter v1

    .line 2158064
    :try_start_0
    sget-object v0, LX/Eh9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2158065
    sput-object v2, LX/Eh9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2158066
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2158067
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2158068
    new-instance v3, LX/Eh9;

    const/16 p0, 0x1813

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Eh9;-><init>(LX/0Ot;)V

    .line 2158069
    move-object v0, v3

    .line 2158070
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2158071
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Eh9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2158072
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2158073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2158074
    check-cast p2, LX/Eh8;

    .line 2158075
    iget-object v0, p0, LX/Eh9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EhA;

    iget-object v1, p2, LX/Eh8;->a:LX/FBn;

    .line 2158076
    iget-object v2, v0, LX/EhA;->a:LX/EtW;

    invoke-virtual {v2, p1}, LX/EtW;->c(LX/1De;)LX/EtV;

    move-result-object v2

    iget-object p0, v1, LX/FBn;->d:Ljava/lang/CharSequence;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/EtV;->b(Ljava/lang/String;)LX/EtV;

    move-result-object v2

    iget p0, v1, LX/FBn;->c:I

    .line 2158077
    iget-object p2, v2, LX/EtV;->a:Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;

    invoke-virtual {v2, p0}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v0

    iput-object v0, p2, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->d:LX/1dc;

    .line 2158078
    move-object v2, v2

    .line 2158079
    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 2158080
    iget-object p0, v1, LX/FBn;->a:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    packed-switch p0, :pswitch_data_0

    .line 2158081
    const p0, 0x6e7a82b7

    const/4 v1, 0x0

    invoke-static {p1, p0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2158082
    :goto_0
    move-object p0, p0

    .line 2158083
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const p0, 0x7f020201

    invoke-interface {v2, p0}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    const p0, 0x7f0b088b

    invoke-interface {v2, p0}, LX/1Di;->t(I)LX/1Di;

    move-result-object v2

    const/4 p0, 0x1

    const p2, 0x7f0b0082

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x3

    const p2, 0x7f0b0082

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2158084
    return-object v0

    .line 2158085
    :pswitch_0
    const p0, -0x2fd9d910

    const/4 v1, 0x0

    invoke-static {p1, p0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2158086
    goto :goto_0

    .line 2158087
    :pswitch_1
    const p0, -0x2fd9d476

    const/4 v1, 0x0

    invoke-static {p1, p0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2158088
    goto :goto_0

    .line 2158089
    :pswitch_2
    const p0, 0xe9a55fd

    const/4 v1, 0x0

    invoke-static {p1, p0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2158090
    goto :goto_0

    .line 2158091
    :pswitch_3
    const p0, 0xe9a5869

    const/4 v1, 0x0

    invoke-static {p1, p0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2158092
    goto :goto_0

    .line 2158093
    :pswitch_4
    const p0, 0xe9a5a59

    const/4 v1, 0x0

    invoke-static {p1, p0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2158094
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2158095
    invoke-static {}, LX/1dS;->b()V

    .line 2158096
    iget v0, p1, LX/1dQ;->b:I

    .line 2158097
    sparse-switch v0, :sswitch_data_0

    .line 2158098
    :goto_0
    return-object v2

    .line 2158099
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2158100
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2158101
    check-cast v1, LX/Eh8;

    .line 2158102
    iget-object v3, p0, LX/Eh9;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EhA;

    iget-object v4, v1, LX/Eh8;->a:LX/FBn;

    .line 2158103
    iget-object p1, v3, LX/EhA;->k:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/2l9;

    invoke-static {}, LX/EhY;->newBuilder()LX/EhZ;

    move-result-object p0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    const-class v1, Landroid/app/Activity;

    invoke-static {p2, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/app/Activity;

    .line 2158104
    iput-object p2, p0, LX/EhZ;->a:Landroid/app/Activity;

    .line 2158105
    move-object p2, p0

    .line 2158106
    iget-object p0, v4, LX/FBn;->b:Lcom/facebook/bookmark/model/Bookmark;

    .line 2158107
    iput-object p0, p2, LX/EhZ;->b:Lcom/facebook/bookmark/model/Bookmark;

    .line 2158108
    move-object p2, p2

    .line 2158109
    iget-object p0, v4, LX/FBn;->b:Lcom/facebook/bookmark/model/Bookmark;

    iget-object p0, p0, Lcom/facebook/bookmark/model/Bookmark;->url:Ljava/lang/String;

    .line 2158110
    iput-object p0, p2, LX/EhZ;->c:Ljava/lang/String;

    .line 2158111
    move-object p2, p2

    .line 2158112
    invoke-virtual {p2}, LX/EhZ;->a()LX/EhY;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/2l9;->a(LX/EhY;)Z

    .line 2158113
    goto :goto_0

    .line 2158114
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2158115
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2158116
    check-cast v1, LX/Eh8;

    .line 2158117
    iget-object v3, p0, LX/Eh9;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v1, LX/Eh8;->b:Landroid/view/View$OnClickListener;

    .line 2158118
    if-eqz v3, :cond_0

    .line 2158119
    invoke-interface {v3, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2158120
    :cond_0
    goto :goto_0

    .line 2158121
    :sswitch_2
    check-cast p2, LX/3Ae;

    .line 2158122
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2158123
    check-cast v1, LX/Eh8;

    .line 2158124
    iget-object v3, p0, LX/Eh9;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v1, LX/Eh8;->c:Landroid/view/View$OnClickListener;

    .line 2158125
    if-eqz v3, :cond_1

    .line 2158126
    invoke-interface {v3, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2158127
    :cond_1
    goto :goto_0

    .line 2158128
    :sswitch_3
    check-cast p2, LX/3Ae;

    .line 2158129
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 2158130
    iget-object v3, p0, LX/Eh9;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EhA;

    .line 2158131
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class p1, Landroid/app/Activity;

    invoke-static {v4, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    iget-object p1, v3, LX/EhA;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0hx;

    iget-object p2, v3, LX/EhA;->d:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/10M;

    iget-object p0, v3, LX/EhA;->c:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/2lP;

    iget-object v1, v3, LX/EhA;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v4, p1, p2, p0, v1}, LX/FBm;->a(Landroid/app/Activity;LX/0hx;LX/10M;LX/2lP;Ljava/lang/String;)V

    .line 2158132
    goto/16 :goto_0

    .line 2158133
    :sswitch_4
    check-cast p2, LX/3Ae;

    .line 2158134
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 2158135
    iget-object v3, p0, LX/Eh9;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EhA;

    .line 2158136
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, v3, LX/EhA;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0hx;

    iget-object p1, v3, LX/EhA;->f:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0jh;

    iget-object p0, v3, LX/EhA;->g:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/6G2;

    invoke-static {v1, v4, p1, p0}, LX/FBm;->a(Landroid/content/Context;LX/0hx;LX/0jh;LX/6G2;)V

    .line 2158137
    goto/16 :goto_0

    .line 2158138
    :sswitch_5
    check-cast p2, LX/3Ae;

    .line 2158139
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 2158140
    iget-object v3, p0, LX/Eh9;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EhA;

    .line 2158141
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, v3, LX/EhA;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2lM;

    iget-object p1, v3, LX/EhA;->i:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/katana/urimap/IntentHandlerUtil;

    iget-object p0, v3, LX/EhA;->j:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0gh;

    invoke-static {v1, v4, p1, p0}, LX/FBm;->a(Landroid/content/Context;LX/2lM;Lcom/facebook/katana/urimap/IntentHandlerUtil;LX/0gh;)V

    .line 2158142
    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2fd9d910 -> :sswitch_1
        -0x2fd9d476 -> :sswitch_2
        0xe9a55fd -> :sswitch_3
        0xe9a5869 -> :sswitch_4
        0xe9a5a59 -> :sswitch_5
        0x6e7a82b7 -> :sswitch_0
    .end sparse-switch
.end method
