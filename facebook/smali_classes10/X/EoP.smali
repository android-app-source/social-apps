.class public LX/EoP;
.super LX/Emg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Emg",
        "<",
        "LX/EoR;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/entitycards/model/ScrollLoadError;

.field public final b:LX/Enk;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycards/model/ScrollLoadError;LX/Enk;)V
    .locals 0

    .prologue
    .line 2168149
    invoke-direct {p0}, LX/Emg;-><init>()V

    .line 2168150
    iput-object p1, p0, LX/EoP;->a:Lcom/facebook/entitycards/model/ScrollLoadError;

    .line 2168151
    iput-object p2, p0, LX/EoP;->b:LX/Enk;

    .line 2168152
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 2168153
    invoke-virtual {p0}, LX/Eme;->a()LX/0am;

    move-result-object v0

    .line 2168154
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2168155
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EoR;

    .line 2168156
    iput-object p0, v0, LX/EoR;->c:LX/EoP;

    .line 2168157
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2168158
    check-cast p1, LX/EoR;

    .line 2168159
    const/4 v0, 0x0

    .line 2168160
    iput-object v0, p1, LX/EoR;->c:LX/EoP;

    .line 2168161
    invoke-super {p0, p1}, LX/Emg;->b(Ljava/lang/Object;)V

    .line 2168162
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2168148
    return-void
.end method
