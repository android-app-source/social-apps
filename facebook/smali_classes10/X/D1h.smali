.class public final LX/D1h;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/4FK;

.field public c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:LX/4BY;

.field public g:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1957330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1957331
    const-string v0, "NOT_SET"

    iput-object v0, p0, LX/D1h;->a:Ljava/lang/String;

    .line 1957332
    const-string v0, "NOT_SET"

    iput-object v0, p0, LX/D1h;->d:Ljava/lang/String;

    .line 1957333
    const-string v0, "NOT_SET"

    iput-object v0, p0, LX/D1h;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()LX/D1i;
    .locals 2

    .prologue
    .line 1957334
    iget-object v0, p0, LX/D1h;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D1h;->b:LX/4FK;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D1h;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D1h;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D1h;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D1h;->g:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    if-nez v0, :cond_1

    .line 1957335
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "All the query arguments need to be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1957336
    :cond_1
    new-instance v0, LX/D1i;

    invoke-direct {v0, p0}, LX/D1i;-><init>(LX/D1h;)V

    return-object v0
.end method
