.class public LX/DGS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:LX/3Dm;

.field public final b:LX/C74;

.field public final c:LX/1WX;

.field public final d:LX/Anw;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/36F;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/DG6;

.field public final g:Landroid/content/Context;

.field public final h:LX/1DR;


# direct methods
.method public constructor <init>(LX/3Dm;LX/C74;LX/1WX;LX/Anw;LX/0Ot;LX/DG6;Landroid/content/Context;LX/1DR;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Dm;",
            "LX/C74;",
            "LX/1WX;",
            "LX/Anw;",
            "LX/0Ot",
            "<",
            "LX/36F;",
            ">;",
            "LX/DG6;",
            "Landroid/content/Context;",
            "LX/1DR;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1979838
    iput-object p1, p0, LX/DGS;->a:LX/3Dm;

    .line 1979839
    iput-object p2, p0, LX/DGS;->b:LX/C74;

    .line 1979840
    iput-object p3, p0, LX/DGS;->c:LX/1WX;

    .line 1979841
    iput-object p4, p0, LX/DGS;->d:LX/Anw;

    .line 1979842
    iput-object p5, p0, LX/DGS;->e:LX/0Ot;

    .line 1979843
    iput-object p6, p0, LX/DGS;->f:LX/DG6;

    .line 1979844
    iput-object p7, p0, LX/DGS;->g:Landroid/content/Context;

    .line 1979845
    iput-object p8, p0, LX/DGS;->h:LX/1DR;

    .line 1979846
    return-void
.end method

.method public static a(LX/0QB;)LX/DGS;
    .locals 12

    .prologue
    .line 1979847
    const-class v1, LX/DGS;

    monitor-enter v1

    .line 1979848
    :try_start_0
    sget-object v0, LX/DGS;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979849
    sput-object v2, LX/DGS;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979850
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979851
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979852
    new-instance v3, LX/DGS;

    invoke-static {v0}, LX/3Dm;->a(LX/0QB;)LX/3Dm;

    move-result-object v4

    check-cast v4, LX/3Dm;

    invoke-static {v0}, LX/C74;->a(LX/0QB;)LX/C74;

    move-result-object v5

    check-cast v5, LX/C74;

    invoke-static {v0}, LX/1WX;->a(LX/0QB;)LX/1WX;

    move-result-object v6

    check-cast v6, LX/1WX;

    invoke-static {v0}, LX/Anw;->a(LX/0QB;)LX/Anw;

    move-result-object v7

    check-cast v7, LX/Anw;

    const/16 v8, 0x868

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/DG6;->a(LX/0QB;)LX/DG6;

    move-result-object v9

    check-cast v9, LX/DG6;

    const-class v10, Landroid/content/Context;

    invoke-interface {v0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v11

    check-cast v11, LX/1DR;

    invoke-direct/range {v3 .. v11}, LX/DGS;-><init>(LX/3Dm;LX/C74;LX/1WX;LX/Anw;LX/0Ot;LX/DG6;Landroid/content/Context;LX/1DR;)V

    .line 1979853
    move-object v0, v3

    .line 1979854
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979855
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DGS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979856
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979857
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
