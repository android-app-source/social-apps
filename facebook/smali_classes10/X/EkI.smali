.class public LX/EkI;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/76V;


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/Runnable;

.field public B:Ljava/lang/String;

.field public C:Landroid/text/TextWatcher;

.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/7Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/Locale;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/EkB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/3Lz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Ek6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/13A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/3Rb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final l:Landroid/widget/AutoCompleteTextView;

.field private final m:Lcom/facebook/fbui/facepile/FacepileView;

.field public final n:Lcom/facebook/resources/ui/FbButton;

.field private final o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final p:Lcom/facebook/resources/ui/FbTextView;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/widget/TextView;

.field public final s:Landroid/widget/TextView;

.field public final t:Landroid/widget/TextView;

.field public final u:Landroid/widget/TextView;

.field public v:LX/7Tj;

.field private w:Z

.field private x:I

.field public y:LX/EkA;

.field public z:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2163299
    const-class v0, LX/EkI;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EkI;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    .line 2163279
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2163280
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/EkI;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-static {v0}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v4

    check-cast v4, LX/8tu;

    const-class v5, LX/7Tk;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/7Tk;

    invoke-static {v0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v6

    check-cast v6, Ljava/util/Locale;

    const-class v7, LX/EkB;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/EkB;

    invoke-static {v0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v8

    check-cast v8, LX/3Lz;

    const/16 v9, 0x15ec

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/Ek6;->a(LX/0QB;)LX/Ek6;

    move-result-object v10

    check-cast v10, LX/Ek6;

    const-class p1, LX/13A;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/13A;

    invoke-static {v0}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v0

    check-cast v0, LX/3Rb;

    iput-object v3, v2, LX/EkI;->a:LX/23P;

    iput-object v4, v2, LX/EkI;->b:LX/8tu;

    iput-object v5, v2, LX/EkI;->c:LX/7Tk;

    iput-object v6, v2, LX/EkI;->d:Ljava/util/Locale;

    iput-object v7, v2, LX/EkI;->e:LX/EkB;

    iput-object v8, v2, LX/EkI;->f:LX/3Lz;

    iput-object v9, v2, LX/EkI;->g:LX/0Or;

    iput-object v10, v2, LX/EkI;->h:LX/Ek6;

    iput-object p1, v2, LX/EkI;->i:LX/13A;

    iput-object v0, v2, LX/EkI;->j:LX/3Rb;

    .line 2163281
    const v0, 0x7f030f35

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2163282
    const v0, 0x7f0d24d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EkI;->q:Landroid/widget/TextView;

    .line 2163283
    const v0, 0x7f0d24d9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EkI;->r:Landroid/widget/TextView;

    .line 2163284
    const v0, 0x7f0d0b0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EkI;->u:Landroid/widget/TextView;

    .line 2163285
    const v0, 0x7f0d24da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EkI;->s:Landroid/widget/TextView;

    .line 2163286
    const v0, 0x7f0d24db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/EkI;->t:Landroid/widget/TextView;

    .line 2163287
    const v0, 0x7f0d24d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/EkI;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2163288
    const v0, 0x7f0d24dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/EkI;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2163289
    const v0, 0x7f0d24dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/EkI;->m:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2163290
    const v0, 0x7f0d0b11

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    .line 2163291
    const v0, 0x7f0d08a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/EkI;->n:Lcom/facebook/resources/ui/FbButton;

    .line 2163292
    iget-object v0, p0, LX/EkI;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/EkI;->B:Ljava/lang/String;

    .line 2163293
    iget-object v0, p0, LX/EkI;->B:Ljava/lang/String;

    .line 2163294
    new-instance v1, LX/7Tl;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "+"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/EkI;->f:LX/3Lz;

    invoke-virtual {v3, v0}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/Locale;

    iget-object v4, p0, LX/EkI;->d:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, LX/EkI;->d:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, LX/7Tl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v1}, LX/EkI;->a$redex0(LX/EkI;LX/7Tl;)V

    .line 2163295
    new-instance v0, LX/EkE;

    invoke-direct {v0, p0}, LX/EkE;-><init>(LX/EkI;)V

    .line 2163296
    iget-object v1, p0, LX/EkI;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2163297
    invoke-virtual {p0}, LX/EkI;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/EkI;->x:I

    .line 2163298
    return-void
.end method

.method public static a(LX/EkI;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 2163272
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2163273
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2163274
    :goto_0
    return-void

    .line 2163275
    :cond_1
    iget-object v0, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    .line 2163276
    iget-object v1, p0, LX/EkI;->a:LX/23P;

    const/4 p1, 0x0

    invoke-virtual {v1, v0, p1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    move-object v0, v1

    .line 2163277
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2163278
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2163265
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 2163266
    instance-of v1, v0, Landroid/widget/ArrayAdapter;

    if-eqz v1, :cond_0

    .line 2163267
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2163268
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2163269
    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2163270
    :goto_0
    return-void

    .line 2163271
    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/EkI;LX/7Tl;)V
    .locals 4

    .prologue
    .line 2163255
    iget-object v0, p1, LX/7Tl;->a:Ljava/lang/String;

    iput-object v0, p0, LX/EkI;->B:Ljava/lang/String;

    .line 2163256
    iget-object v0, p0, LX/EkI;->n:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/EkI;->f:LX/3Lz;

    iget-object v3, p0, LX/EkI;->B:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2163257
    iget-object v0, p0, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, LX/EkI;->C:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2163258
    new-instance v0, LX/A8g;

    iget-object v1, p1, LX/7Tl;->a:Ljava/lang/String;

    invoke-virtual {p0}, LX/EkI;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/A8g;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, LX/EkI;->C:Landroid/text/TextWatcher;

    .line 2163259
    iget-object v0, p0, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, LX/EkI;->C:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2163260
    iget-object v0, p0, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2163261
    sget-object v1, LX/1IA;->WHITESPACE:LX/1IA;

    const-string v2, "()-."

    invoke-static {v2}, LX/1IA;->anyOf(Ljava/lang/CharSequence;)LX/1IA;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1IA;->or(LX/1IA;)LX/1IA;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1IA;->removeFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2163262
    iget-object v1, p0, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    const-string v2, ""

    invoke-static {v1, v2}, LX/EkI;->a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V

    .line 2163263
    iget-object v1, p0, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    invoke-static {v1, v0}, LX/EkI;->a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V

    .line 2163264
    return-void
.end method

.method private setFacepileUrls(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2163250
    iget-object v1, p0, LX/EkI;->m:Lcom/facebook/fbui/facepile/FacepileView;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2163251
    iget-object v0, p0, LX/EkI;->m:Lcom/facebook/fbui/facepile/FacepileView;

    iget v1, p0, LX/EkI;->x:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceSize(I)V

    .line 2163252
    iget-object v0, p0, LX/EkI;->m:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceUrls(Ljava/util/List;)V

    .line 2163253
    return-void

    .line 2163254
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setSocialContext(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2163246
    iget-object v0, p0, LX/EkI;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2163247
    iget-object v1, p0, LX/EkI;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2163248
    return-void

    .line 2163249
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2163241
    iget-object v0, p0, LX/EkI;->A:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2163242
    iget-object v0, p0, LX/EkI;->A:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2163243
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EkI;->w:Z

    .line 2163244
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/EkI;->setVisibility(I)V

    .line 2163245
    return-void
.end method

.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 2163156
    iget-object v0, p0, LX/EkI;->z:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-ne v0, p1, :cond_1

    .line 2163157
    iget-boolean v0, p0, LX/EkI;->w:Z

    if-eqz v0, :cond_0

    .line 2163158
    invoke-virtual {p0, v10}, LX/EkI;->setVisibility(I)V

    .line 2163159
    :cond_0
    :goto_0
    return-void

    .line 2163160
    :cond_1
    iput-object p1, p0, LX/EkI;->z:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 2163161
    iget-object v0, p0, LX/EkI;->z:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v7

    .line 2163162
    if-nez v7, :cond_2

    .line 2163163
    invoke-virtual {p0}, LX/EkI;->a()V

    goto :goto_0

    .line 2163164
    :cond_2
    iget-object v0, p0, LX/EkI;->i:LX/13A;

    iget-object v1, p0, LX/EkI;->z:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0, v1, p2, v7, p3}, LX/13A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/78A;

    move-result-object v2

    .line 2163165
    iget-object v0, p0, LX/EkI;->e:LX/EkB;

    invoke-virtual {p0}, LX/EkI;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, LX/EkI;->A:Ljava/lang/Runnable;

    iget-object v4, p0, LX/EkI;->z:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, LX/EkB;->a(Landroid/content/Context;LX/78A;Ljava/lang/Runnable;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/EkI;)LX/EkA;

    move-result-object v0

    iput-object v0, p0, LX/EkI;->y:LX/EkA;

    .line 2163166
    iget-object v0, v7, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 2163167
    new-instance v1, LX/EkG;

    invoke-direct {v1, p0}, LX/EkG;-><init>(LX/EkI;)V

    .line 2163168
    iget-object v2, p0, LX/EkI;->s:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2163169
    iget-object v1, p0, LX/EkI;->s:Landroid/widget/TextView;

    invoke-static {p0, v0, v1}, LX/EkI;->a(LX/EkI;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/widget/TextView;)V

    .line 2163170
    iget-object v0, v7, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 2163171
    new-instance v1, LX/EkH;

    invoke-direct {v1, p0}, LX/EkH;-><init>(LX/EkI;)V

    .line 2163172
    iget-object v2, p0, LX/EkI;->t:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2163173
    iget-object v1, p0, LX/EkI;->t:Landroid/widget/TextView;

    invoke-static {p0, v0, v1}, LX/EkI;->a(LX/EkI;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/widget/TextView;)V

    .line 2163174
    iget-object v0, p0, LX/EkI;->q:Landroid/widget/TextView;

    iget-object v1, v7, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2163175
    iget-object v0, p0, LX/EkI;->r:Landroid/widget/TextView;

    iget-object v1, v7, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    const/4 v5, 0x0

    .line 2163176
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    .line 2163177
    invoke-interface {v3}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v4, Landroid/text/style/URLSpan;

    invoke-interface {v3, v5, v2, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/URLSpan;

    .line 2163178
    array-length v4, v2

    if-eqz v4, :cond_8

    .line 2163179
    aget-object v2, v2, v5

    .line 2163180
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2163181
    invoke-interface {v3, v2}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 2163182
    invoke-interface {v3, v2}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result p1

    .line 2163183
    invoke-interface {v3, v2}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result p2

    .line 2163184
    new-instance v1, LX/EkC;

    invoke-direct {v1, p0, v2}, LX/EkC;-><init>(LX/EkI;Landroid/text/style/URLSpan;)V

    .line 2163185
    invoke-virtual {v4, v1, v5, p1, p2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2163186
    invoke-virtual {v4, v2}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    .line 2163187
    move-object v2, v4

    .line 2163188
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2163189
    iget-object v2, p0, LX/EkI;->b:LX/8tu;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2163190
    :goto_1
    iget-object v0, p0, LX/EkI;->z:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2163191
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2163192
    :cond_3
    :goto_2
    iget-object v0, p0, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/EkF;

    invoke-direct {v1, p0}, LX/EkF;-><init>(LX/EkI;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2163193
    sget-object v0, LX/76S;->ANY:LX/76S;

    invoke-static {v7, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    move-result-object v0

    .line 2163194
    if-eqz v0, :cond_5

    .line 2163195
    iget-object v1, p0, LX/EkI;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2163196
    iget-object v0, p0, LX/EkI;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2163197
    :goto_3
    invoke-direct {p0, v8}, LX/EkI;->setFacepileUrls(Ljava/util/List;)V

    .line 2163198
    iget-object v0, v7, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    if-nez v0, :cond_6

    .line 2163199
    invoke-direct {p0, v8}, LX/EkI;->setSocialContext(Ljava/lang/CharSequence;)V

    .line 2163200
    :cond_4
    :goto_4
    iget-object v0, p0, LX/EkI;->y:LX/EkA;

    invoke-virtual {v0}, LX/76U;->a()V

    .line 2163201
    iput-boolean v6, p0, LX/EkI;->w:Z

    .line 2163202
    invoke-virtual {p0, v6}, LX/EkI;->setVisibility(I)V

    .line 2163203
    iget-object v0, p0, LX/EkI;->h:LX/Ek6;

    iget-object v1, p0, LX/EkI;->z:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v1, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    .line 2163204
    iput-object v1, v0, LX/Ek6;->c:Ljava/lang/String;

    .line 2163205
    iget-object v0, p0, LX/EkI;->h:LX/Ek6;

    invoke-virtual {v0}, LX/Ek6;->a()V

    .line 2163206
    iget-object v1, p0, LX/EkI;->h:LX/Ek6;

    iget-object v0, p0, LX/EkI;->z:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    const-string v2, "promo_type"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/Ek6;->a(Ljava/lang/String;)V

    .line 2163207
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "initial number"

    iget-object v0, p0, LX/EkI;->z:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    const-string v3, "phone_number"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2163208
    iget-object v1, p0, LX/EkI;->h:LX/Ek6;

    sget-object v2, LX/Ek7;->QP_IMPRESSION:LX/Ek7;

    invoke-virtual {v1, v2, v8, v0}, LX/Ek6;->a(LX/Ek7;Ljava/lang/String;LX/1rQ;)V

    goto/16 :goto_0

    .line 2163209
    :cond_5
    iget-object v0, p0, LX/EkI;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v10}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_3

    .line 2163210
    :cond_6
    iget-object v0, v7, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/EkI;->setSocialContext(Ljava/lang/CharSequence;)V

    .line 2163211
    iget-object v0, v7, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v2, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->friendIds:LX/0Px;

    .line 2163212
    invoke-static {v2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2163213
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v6

    .line 2163214
    :goto_5
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    if-ge v1, v9, :cond_7

    .line 2163215
    iget-object v4, p0, LX/EkI;->j:LX/3Rb;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget v5, p0, LX/EkI;->x:I

    iget v7, p0, LX/EkI;->x:I

    invoke-virtual {v4, v0, v5, v7}, LX/3Rb;->a(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2163216
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 2163217
    :cond_7
    invoke-direct {p0, v3}, LX/EkI;->setFacepileUrls(Ljava/util/List;)V

    goto/16 :goto_4

    .line 2163218
    :cond_8
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2163219
    :cond_9
    iget-object v1, p0, LX/EkI;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2163220
    :try_start_0
    iget-object v2, p0, LX/EkI;->f:LX/3Lz;

    invoke-virtual {v2, v0, v1}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v1

    .line 2163221
    iget-object v2, p0, LX/EkI;->f:LX/3Lz;

    invoke-virtual {v2, v1}, LX/3Lz;->getRegionCodeForNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v2

    .line 2163222
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2163223
    iput-object v2, p0, LX/EkI;->B:Ljava/lang/String;

    .line 2163224
    iget-object v3, p0, LX/EkI;->n:Lcom/facebook/resources/ui/FbButton;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2163225
    iget v5, v1, LX/4hT;->countryCode_:I

    move v5, v5

    .line 2163226
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2163227
    iget-object v3, p0, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    iget-object v4, p0, LX/EkI;->C:Landroid/text/TextWatcher;

    invoke-virtual {v3, v4}, Landroid/widget/AutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2163228
    new-instance v3, LX/A8g;

    invoke-virtual {p0}, LX/EkI;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v2, v4}, LX/A8g;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v3, p0, LX/EkI;->C:Landroid/text/TextWatcher;

    .line 2163229
    iget-object v2, p0, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    iget-object v3, p0, LX/EkI;->C:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2163230
    iget-object v2, p0, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    iget-object v3, p0, LX/EkI;->f:LX/3Lz;

    invoke-virtual {v3, v1}, LX/3Lz;->getNationalSignificantNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/EkI;->a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 2163231
    :catch_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    goto/16 :goto_2
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2163237
    iget-boolean v0, p0, LX/EkI;->w:Z

    if-eqz v0, :cond_0

    .line 2163238
    invoke-virtual {p0, v1, v1}, LX/EkI;->setMeasuredDimension(II)V

    .line 2163239
    :goto_0
    return-void

    .line 2163240
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setOnDismiss(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2163235
    iput-object p1, p0, LX/EkI;->A:Ljava/lang/Runnable;

    .line 2163236
    return-void
.end method

.method public setupErrorText(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2163232
    iget-object v0, p0, LX/EkI;->u:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2163233
    iget-object v0, p0, LX/EkI;->u:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2163234
    return-void
.end method
