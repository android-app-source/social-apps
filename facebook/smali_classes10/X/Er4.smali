.class public final LX/Er4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1OZ;


# instance fields
.field public final synthetic a:LX/Er9;


# direct methods
.method public constructor <init>(LX/Er9;)V
    .locals 0

    .prologue
    .line 2172435
    iput-object p1, p0, LX/Er4;->a:LX/Er9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 2172414
    iget-object v0, p0, LX/Er4;->a:LX/Er9;

    iget-object v0, v0, LX/Er9;->j:LX/Eqw;

    .line 2172415
    invoke-virtual {v0, p3}, LX/A8Z;->e(I)LX/A8P;

    move-result-object v2

    .line 2172416
    iget-object v1, v2, LX/A8P;->b:LX/90h;

    check-cast v1, LX/Eqx;

    iget v2, v2, LX/A8P;->a:I

    const/4 p1, 0x0

    const/4 p0, 0x1

    .line 2172417
    invoke-virtual {v1, v2}, LX/Eqx;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    .line 2172418
    instance-of p2, v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-nez p2, :cond_1

    .line 2172419
    :cond_0
    :goto_0
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2172420
    return-void

    .line 2172421
    :cond_1
    check-cast v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2172422
    invoke-virtual {v3}, LX/8QK;->a()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2172423
    invoke-virtual {v3}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object p2

    .line 2172424
    iget-object p4, v1, LX/Eqx;->d:Ljava/util/Set;

    invoke-interface {p4, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p4

    .line 2172425
    if-eqz p4, :cond_2

    .line 2172426
    iget-object p5, v1, LX/Eqx;->d:Ljava/util/Set;

    invoke-interface {p5, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2172427
    iget-object p2, v1, LX/Eqx;->j:LX/ErW;

    iget-object p5, v1, LX/Eqx;->c:LX/ErX;

    invoke-virtual {p2, p5, p0}, LX/ErW;->b(LX/ErX;I)V

    .line 2172428
    :goto_1
    iget-object p2, v1, LX/Eqx;->g:LX/EqG;

    if-nez p4, :cond_4

    :goto_2
    invoke-virtual {p2, v3, p0, p1}, LX/EqG;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;ZZ)V

    goto :goto_0

    .line 2172429
    :cond_2
    iget-object p5, v1, LX/Eqx;->d:Ljava/util/Set;

    invoke-interface {p5}, Ljava/util/Set;->size()I

    move-result p5

    iget p3, v1, LX/Eqx;->f:I

    if-lt p5, p3, :cond_3

    .line 2172430
    iget-object v3, v1, LX/Eqx;->i:LX/0kL;

    new-instance p0, LX/27k;

    const p1, 0x7f080be4

    invoke-direct {p0, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v3, p0}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2172431
    goto :goto_0

    .line 2172432
    :cond_3
    iget-object p5, v1, LX/Eqx;->d:Ljava/util/Set;

    invoke-interface {p5, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2172433
    iget-object p2, v1, LX/Eqx;->j:LX/ErW;

    iget-object p5, v1, LX/Eqx;->c:LX/ErX;

    invoke-virtual {p2, p5, p0}, LX/ErW;->a(LX/ErX;I)V

    goto :goto_1

    :cond_4
    move p0, p1

    .line 2172434
    goto :goto_2
.end method
