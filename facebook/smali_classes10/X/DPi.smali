.class public final LX/DPi;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DPj;


# direct methods
.method public constructor <init>(LX/DPj;)V
    .locals 0

    .prologue
    .line 1993664
    iput-object p1, p0, LX/DPi;->a:LX/DPj;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1993665
    iget-object v0, p0, LX/DPi;->a:LX/DPj;

    iget-object v0, v0, LX/DPj;->b:LX/DPo;

    const v1, 0x7f083049

    invoke-static {v0, v1}, LX/DPo;->a$redex0(LX/DPo;I)V

    .line 1993666
    iget-object v0, p0, LX/DPi;->a:LX/DPj;

    iget-object v0, v0, LX/DPj;->b:LX/DPo;

    iget-object v0, v0, LX/DPo;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/DPo;->a:Ljava/lang/String;

    const-string v2, "Group archive action failed."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1993667
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1993668
    iget-object v0, p0, LX/DPi;->a:LX/DPj;

    iget-object v0, v0, LX/DPj;->b:LX/DPo;

    const v1, 0x7f083047

    invoke-static {v0, v1}, LX/DPo;->a$redex0(LX/DPo;I)V

    .line 1993669
    iget-object v0, p0, LX/DPi;->a:LX/DPj;

    iget-object v0, v0, LX/DPj;->b:LX/DPo;

    iget-object v1, p0, LX/DPi;->a:LX/DPj;

    iget-object v1, v1, LX/DPj;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v0, v1}, LX/DPo;->c(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    .line 1993670
    return-void
.end method
