.class public final LX/Djx;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentCalendarEntryFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Div;

.field public final synthetic b:LX/Djz;


# direct methods
.method public constructor <init>(LX/Djz;LX/Div;)V
    .locals 0

    .prologue
    .line 2033854
    iput-object p1, p0, LX/Djx;->b:LX/Djz;

    iput-object p2, p0, LX/Djx;->a:LX/Div;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2033855
    iget-object v0, p0, LX/Djx;->a:LX/Div;

    invoke-virtual {v0}, LX/Div;->a()V

    .line 2033856
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033857
    check-cast p1, LX/0Px;

    .line 2033858
    if-nez p1, :cond_0

    .line 2033859
    iget-object v0, p0, LX/Djx;->a:LX/Div;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "result is NULL"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/Div;->a()V

    .line 2033860
    :goto_0
    return-void

    .line 2033861
    :cond_0
    iget-object v0, p0, LX/Djx;->a:LX/Div;

    .line 2033862
    iget-boolean v1, v0, LX/Div;->a:Z

    if-eqz v1, :cond_1

    .line 2033863
    iget-object v1, v0, LX/Div;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    invoke-virtual {v1}, LX/Dlg;->f()V

    .line 2033864
    iget-object v1, v0, LX/Div;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2033865
    iput-object v2, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->t:Ljava/util/List;

    .line 2033866
    iget-object v1, v0, LX/Div;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    invoke-static {v1}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->e(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;)V

    .line 2033867
    :cond_1
    iget-object v1, v0, LX/Div;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    const/4 v2, 0x0

    .line 2033868
    iput-boolean v2, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->s:Z

    .line 2033869
    iget-object v1, v0, LX/Div;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->t:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2033870
    iget-object v1, v0, LX/Div;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->o:LX/Dlg;

    .line 2033871
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/Dlg;->o:Z

    .line 2033872
    iget-object v2, v1, LX/Dlg;->k:LX/4nS;

    if-eqz v2, :cond_2

    .line 2033873
    iget-object v2, v1, LX/Dlg;->k:LX/4nS;

    invoke-virtual {v2}, LX/4nS;->b()V

    .line 2033874
    :cond_2
    iget-object v1, v0, LX/Div;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->g:LX/0TD;

    new-instance v2, LX/Dit;

    invoke-direct {v2, v0, p1}, LX/Dit;-><init>(LX/Div;LX/0Px;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2033875
    new-instance v2, LX/Diu;

    invoke-direct {v2, v0}, LX/Diu;-><init>(LX/Div;)V

    iget-object p0, v0, LX/Div;->b:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;

    iget-object p0, p0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarTabFragment;->h:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2033876
    goto :goto_0
.end method
