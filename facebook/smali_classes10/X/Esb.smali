.class public LX/Esb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/3Do;

.field public final b:LX/1zC;

.field public final c:LX/1Cn;


# direct methods
.method public constructor <init>(LX/3Do;LX/1zC;LX/1Cn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2176063
    iput-object p1, p0, LX/Esb;->a:LX/3Do;

    .line 2176064
    iput-object p2, p0, LX/Esb;->b:LX/1zC;

    .line 2176065
    iput-object p3, p0, LX/Esb;->c:LX/1Cn;

    .line 2176066
    return-void
.end method

.method public static a(LX/0QB;)LX/Esb;
    .locals 6

    .prologue
    .line 2176051
    const-class v1, LX/Esb;

    monitor-enter v1

    .line 2176052
    :try_start_0
    sget-object v0, LX/Esb;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176053
    sput-object v2, LX/Esb;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176054
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176055
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176056
    new-instance p0, LX/Esb;

    const-class v3, LX/3Do;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/3Do;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v4

    check-cast v4, LX/1zC;

    invoke-static {v0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v5

    check-cast v5, LX/1Cn;

    invoke-direct {p0, v3, v4, v5}, LX/Esb;-><init>(LX/3Do;LX/1zC;LX/1Cn;)V

    .line 2176057
    move-object v0, p0

    .line 2176058
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176059
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Esb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176060
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176061
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
