.class public final LX/DxR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DxW;


# direct methods
.method public constructor <init>(LX/DxW;)V
    .locals 0

    .prologue
    .line 2062866
    iput-object p1, p0, LX/DxR;->a:LX/DxW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x43adc9cb

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2062856
    iget-object v0, p0, LX/DxR;->a:LX/DxW;

    invoke-static {v0, p1}, LX/DxW;->a(LX/DxW;Landroid/view/View;)Landroid/app/Activity;

    move-result-object v1

    .line 2062857
    iget-object v0, p0, LX/DxR;->a:LX/DxW;

    invoke-virtual {v0}, LX/DxW;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2062858
    iget-object v0, p0, LX/DxR;->a:LX/DxW;

    .line 2062859
    invoke-static {v0, v1}, LX/DxW;->b$redex0(LX/DxW;Landroid/app/Activity;)V

    .line 2062860
    :goto_0
    const v0, 0x1394f55d

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void

    .line 2062861
    :cond_0
    iget-object v0, p0, LX/DxR;->a:LX/DxW;

    invoke-virtual {v0}, LX/DxW;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2062862
    iget-object v0, p0, LX/DxR;->a:LX/DxW;

    .line 2062863
    invoke-static {v0, v1}, LX/DxW;->c$redex0(LX/DxW;Landroid/app/Activity;)V

    .line 2062864
    goto :goto_0

    .line 2062865
    :cond_1
    iget-object v0, p0, LX/DxR;->a:LX/DxW;

    iget-object v8, v0, LX/DxW;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/DxR;->a:LX/DxW;

    iget-object v0, v0, LX/DxW;->e:LX/DwF;

    iget-object v2, p0, LX/DxR;->a:LX/DxW;

    iget-object v2, v2, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    sget-object v3, LX/8AB;->ALBUM:LX/8AB;

    sget-object v4, LX/21D;->ALBUM:LX/21D;

    iget-object v5, p0, LX/DxR;->a:LX/DxW;

    iget-object v5, v5, LX/DxW;->l:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v5}, LX/DwG;->c(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "eventAlbum"

    :goto_1
    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/DwF;->a(Landroid/app/Activity;Lcom/facebook/graphql/model/GraphQLAlbum;LX/8AB;LX/21D;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v8, v0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    const-string v5, "album"

    goto :goto_1
.end method
