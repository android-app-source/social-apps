.class public LX/EnR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/EnR;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2167095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167096
    return-void
.end method

.method public static a(LX/0Px;Ljava/lang/String;)LX/EnQ;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/EnQ;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2167069
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167070
    const/4 v10, -0x1

    .line 2167071
    new-instance v6, LX/EnQ;

    .line 2167072
    sget-object v7, LX/0Q7;->a:LX/0Px;

    move-object v7, v7

    .line 2167073
    const/4 v8, 0x0

    sget-object v9, LX/En3;->a:LX/En3;

    move v11, v10

    invoke-direct/range {v6 .. v11}, LX/EnQ;-><init>(LX/0Px;Ljava/lang/String;LX/En3;II)V

    move-object v0, v6

    .line 2167074
    :goto_0
    return-object v0

    .line 2167075
    :cond_0
    invoke-virtual {p0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 2167076
    if-ltz v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v3, "Selected ID is not in list!"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2167077
    add-int/lit8 v0, v2, -0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 2167078
    add-int/lit8 v0, v2, 0x2

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 2167079
    add-int/lit8 v0, v5, 0x1

    invoke-virtual {p0, v4, v0}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    invoke-static {v4, v0}, LX/En3;->a(ILX/0Px;)LX/En3;

    move-result-object v3

    .line 2167080
    invoke-virtual {v3}, LX/En3;->a()V

    .line 2167081
    new-instance v0, LX/EnQ;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/EnQ;-><init>(LX/0Px;Ljava/lang/String;LX/En3;II)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2167082
    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/EnR;
    .locals 3

    .prologue
    .line 2167083
    sget-object v0, LX/EnR;->a:LX/EnR;

    if-nez v0, :cond_1

    .line 2167084
    const-class v1, LX/EnR;

    monitor-enter v1

    .line 2167085
    :try_start_0
    sget-object v0, LX/EnR;->a:LX/EnR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2167086
    if-eqz v2, :cond_0

    .line 2167087
    :try_start_1
    new-instance v0, LX/EnR;

    invoke-direct {v0}, LX/EnR;-><init>()V

    .line 2167088
    move-object v0, v0

    .line 2167089
    sput-object v0, LX/EnR;->a:LX/EnR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2167090
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2167091
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2167092
    :cond_1
    sget-object v0, LX/EnR;->a:LX/EnR;

    return-object v0

    .line 2167093
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2167094
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
