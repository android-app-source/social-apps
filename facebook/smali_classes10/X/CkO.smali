.class public LX/CkO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public c:Ljava/lang/String;

.field public d:LX/Ckb;

.field public e:LX/CkR;

.field public f:Z

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/Ckb;LX/15i;I)V
    .locals 1
    .param p2    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1930925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930926
    iput-object p1, p0, LX/CkO;->d:LX/Ckb;

    .line 1930927
    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/CkO;->f:Z

    .line 1930928
    iget-boolean v0, p0, LX/CkO;->f:Z

    if-eqz v0, :cond_0

    .line 1930929
    iget-object v0, p0, LX/CkO;->d:LX/Ckb;

    iget-object v0, v0, LX/Ckb;->value:Ljava/lang/String;

    iput-object v0, p0, LX/CkO;->c:Ljava/lang/String;

    .line 1930930
    new-instance v0, LX/CkR;

    invoke-direct {v0, p2, p3}, LX/CkR;-><init>(LX/15i;I)V

    iput-object v0, p0, LX/CkO;->e:LX/CkR;

    .line 1930931
    :cond_0
    return-void

    .line 1930932
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/Ckb;Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;)V
    .locals 1
    .param p2    # Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1930933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930934
    iput-object p1, p0, LX/CkO;->d:LX/Ckb;

    .line 1930935
    if-eqz p2, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/CkO;->f:Z

    .line 1930936
    iget-boolean v0, p0, LX/CkO;->f:Z

    if-eqz v0, :cond_2

    .line 1930937
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CkO;->c:Ljava/lang/String;

    .line 1930938
    iget-object v0, p0, LX/CkO;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CkO;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1930939
    :cond_0
    iget-object v0, p0, LX/CkO;->d:LX/Ckb;

    iget-object v0, v0, LX/Ckb;->value:Ljava/lang/String;

    iput-object v0, p0, LX/CkO;->c:Ljava/lang/String;

    .line 1930940
    :cond_1
    new-instance v0, LX/CkR;

    invoke-direct {v0, p2}, LX/CkR;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;)V

    iput-object v0, p0, LX/CkO;->e:LX/CkR;

    .line 1930941
    invoke-virtual {p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CkO;->g:Ljava/lang/String;

    .line 1930942
    :cond_2
    return-void

    .line 1930943
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
