.class public final LX/DZe;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:LX/DZf;


# direct methods
.method public constructor <init>(LX/DZf;)V
    .locals 0

    .prologue
    .line 2013509
    iput-object p1, p0, LX/DZe;->a:LX/DZf;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2013510
    iget-object v0, p0, LX/DZe;->a:LX/DZf;

    iget-object v0, v0, LX/DZf;->c:LX/DZb;

    if-eqz v0, :cond_0

    .line 2013511
    iget-object v0, p0, LX/DZe;->a:LX/DZf;

    iget-object v0, v0, LX/DZf;->c:LX/DZb;

    .line 2013512
    iget-object p0, v0, LX/DZb;->a:Lcom/facebook/groups/settings/GroupSubscriptionFragment;

    invoke-static {p0}, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->b(Lcom/facebook/groups/settings/GroupSubscriptionFragment;)V

    .line 2013513
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2013514
    iget-object v0, p0, LX/DZe;->a:LX/DZf;

    iget-object v0, v0, LX/DZf;->c:LX/DZb;

    if-eqz v0, :cond_1

    .line 2013515
    iget-object v0, p0, LX/DZe;->a:LX/DZf;

    iget-object v0, v0, LX/DZf;->c:LX/DZb;

    .line 2013516
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2013517
    if-eqz p1, :cond_0

    .line 2013518
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2013519
    if-nez v1, :cond_2

    .line 2013520
    :cond_0
    iget-object v1, v0, LX/DZb;->a:Lcom/facebook/groups/settings/GroupSubscriptionFragment;

    invoke-static {v1}, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->b(Lcom/facebook/groups/settings/GroupSubscriptionFragment;)V

    .line 2013521
    :cond_1
    :goto_0
    return-void

    .line 2013522
    :cond_2
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2013523
    check-cast v1, LX/DQn;

    .line 2013524
    iget-object v2, v0, LX/DZb;->a:Lcom/facebook/groups/settings/GroupSubscriptionFragment;

    iget-object v2, v2, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->b:LX/DZ7;

    iget-object v3, v0, LX/DZb;->a:Lcom/facebook/groups/settings/GroupSubscriptionFragment;

    invoke-interface {v1}, LX/DQn;->b()Ljava/lang/String;

    move-result-object v4

    const/4 p0, 0x0

    invoke-interface {v2, v3, v4, p0}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 2013525
    iget-object v2, v0, LX/DZb;->a:Lcom/facebook/groups/settings/GroupSubscriptionFragment;

    iget-object v2, v2, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->a:LX/DZW;

    iget-object v3, v0, LX/DZb;->a:Lcom/facebook/groups/settings/GroupSubscriptionFragment;

    iget-object v3, v3, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->g:Ljava/lang/String;

    .line 2013526
    iput-object v1, v2, LX/DZW;->n:LX/DQn;

    .line 2013527
    iput-object v3, v2, LX/DZW;->o:Ljava/lang/String;

    .line 2013528
    iget-object v4, v2, LX/DZW;->n:LX/DQn;

    invoke-interface {v4}, LX/DQn;->j()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, LX/DZW;->g:Ljava/lang/String;

    .line 2013529
    iget-object v4, v2, LX/DZW;->n:LX/DQn;

    invoke-interface {v4}, LX/DQn;->kx_()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, LX/DZW;->i:Ljava/lang/String;

    .line 2013530
    iget-object v4, v2, LX/DZW;->n:LX/DQn;

    invoke-interface {v4}, LX/DQn;->ky_()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    move-result-object v4

    iput-object v4, v2, LX/DZW;->j:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 2013531
    iget-object v4, v2, LX/DZW;->n:LX/DQn;

    invoke-interface {v4}, LX/DQn;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, v2, LX/DZW;->n:LX/DQn;

    invoke-interface {v4}, LX/DQn;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v4

    :goto_1
    iput-object v4, v2, LX/DZW;->k:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2013532
    invoke-static {v2}, LX/DZW;->b(LX/DZW;)V

    .line 2013533
    iget-object v4, v2, LX/DZW;->n:LX/DQn;

    invoke-interface {v4}, LX/DQn;->e()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;->a()LX/0Px;

    move-result-object v4

    iget-object p0, v2, LX/DZW;->n:LX/DQn;

    invoke-interface {p0}, LX/DQn;->d()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;->a()LX/0Px;

    move-result-object p0

    .line 2013534
    iput-object v4, v2, LX/DZW;->l:LX/0Px;

    .line 2013535
    iput-object p0, v2, LX/DZW;->m:LX/0Px;

    .line 2013536
    invoke-static {v2}, LX/DZW;->c(LX/DZW;)V

    .line 2013537
    iget-object v1, v0, LX/DZb;->a:Lcom/facebook/groups/settings/GroupSubscriptionFragment;

    iget-object v1, v1, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->h:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, v0, LX/DZb;->a:Lcom/facebook/groups/settings/GroupSubscriptionFragment;

    iget-object v2, v2, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->a:LX/DZW;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 2013538
    :cond_3
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    goto :goto_1
.end method
