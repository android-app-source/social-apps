.class public LX/D8F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/04D;

.field private final c:LX/2oL;

.field private final d:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/13l;

.field private final f:LX/2mz;


# direct methods
.method public constructor <init>(LX/13l;Lcom/facebook/feed/rows/core/props/FeedProps;LX/04D;LX/2oL;Ljava/util/concurrent/atomic/AtomicReference;LX/2mz;)V
    .locals 0
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/04D;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/2oL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/atomic/AtomicReference;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/2mz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/13l;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/04D;",
            "LX/2oL;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;",
            "LX/2mz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1968177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1968178
    iput-object p2, p0, LX/D8F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968179
    iput-object p3, p0, LX/D8F;->b:LX/04D;

    .line 1968180
    iput-object p4, p0, LX/D8F;->c:LX/2oL;

    .line 1968181
    iput-object p5, p0, LX/D8F;->d:Ljava/util/concurrent/atomic/AtomicReference;

    .line 1968182
    iput-object p6, p0, LX/D8F;->f:LX/2mz;

    .line 1968183
    iput-object p1, p0, LX/D8F;->e:LX/13l;

    .line 1968184
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 1968185
    iget-object v0, p0, LX/D8F;->c:LX/2oL;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D8F;->c:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D8F;->c:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    .line 1968186
    iget-boolean v1, v0, LX/2oO;->t:Z

    move v0, v1

    .line 1968187
    if-eqz v0, :cond_1

    .line 1968188
    :cond_0
    :goto_0
    return-void

    .line 1968189
    :cond_1
    iget-object v0, p0, LX/D8F;->e:LX/13l;

    sget-object v1, LX/04g;->BY_NEWSFEED_OCCLUSION:LX/04g;

    invoke-virtual {v0, v1}, LX/13l;->a(LX/04g;)V

    .line 1968190
    iget-object v0, p0, LX/D8F;->c:LX/2oL;

    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    invoke-virtual {v0}, LX/2oO;->a()V

    .line 1968191
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0f8;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 1968192
    if-eqz v0, :cond_0

    .line 1968193
    invoke-interface {v0}, LX/0f8;->m()LX/0hE;

    move-result-object v0

    check-cast v0, LX/D8W;

    .line 1968194
    if-eqz v0, :cond_0

    .line 1968195
    new-instance v1, LX/D8E;

    iget-object v2, p0, LX/D8F;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1, v2}, LX/D8E;-><init>(Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-virtual {v0, v1}, LX/D8W;->b(LX/394;)LX/D8W;

    .line 1968196
    check-cast p1, LX/3FU;

    .line 1968197
    iget-object v1, p0, LX/D8F;->f:LX/2mz;

    iget-object v2, p0, LX/D8F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1}, LX/3FU;->getTransitionNode()LX/3FT;

    move-result-object v3

    invoke-interface {p1}, LX/3FU;->getSeekPosition()I

    move-result v4

    invoke-interface {p1}, LX/3FU;->getLastStartPosition()I

    move-result v5

    iget-object v6, p0, LX/D8F;->b:LX/04D;

    iget-object v7, p0, LX/D8F;->c:LX/2oL;

    invoke-virtual {v7}, LX/2oL;->c()LX/04g;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/D8W;->a(LX/2mz;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3FT;IILX/04D;LX/04g;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x446e26b7

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1968198
    instance-of v1, p1, LX/3FU;

    if-eqz v1, :cond_0

    .line 1968199
    invoke-direct {p0, p1}, LX/D8F;->a(Landroid/view/View;)V

    .line 1968200
    :cond_0
    const v1, 0x7a2a95a9

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
