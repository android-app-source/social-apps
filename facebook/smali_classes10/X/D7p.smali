.class public LX/D7p;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/2pa;


# instance fields
.field public final b:Landroid/content/Context;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation runtime Lcom/facebook/video/watchandgo/annotations/ForWatchAndGo;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D7x;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1967626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1967627
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1967628
    iput-object v0, p0, LX/D7p;->c:LX/0Ot;

    .line 1967629
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1967630
    iput-object v0, p0, LX/D7p;->d:LX/0Ot;

    .line 1967631
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1967632
    iput-object v0, p0, LX/D7p;->e:LX/0Ot;

    .line 1967633
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1967634
    iput-object v0, p0, LX/D7p;->f:LX/0Ot;

    .line 1967635
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1967636
    iput-object v0, p0, LX/D7p;->g:LX/0Ot;

    .line 1967637
    iput-object p1, p0, LX/D7p;->b:Landroid/content/Context;

    .line 1967638
    return-void
.end method

.method public static a(LX/D7p;Lcom/facebook/video/player/RichVideoPlayer;LX/D7o;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            "LX/D7o;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1967666
    iget-object v0, p1, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v2, v0

    .line 1967667
    sget-object v4, LX/04g;->BY_USER:LX/04g;

    .line 1967668
    iget-object v0, p0, LX/D7p;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13l;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v3}, LX/13l;->a(LX/04g;)V

    .line 1967669
    iget-object v0, p0, LX/D7p;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/D7y;->b:LX/0Tn;

    invoke-interface {v0, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1967670
    :goto_0
    if-eqz v0, :cond_2

    .line 1967671
    iget-object v0, p0, LX/D7p;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v9, p0, LX/D7p;->b:Landroid/content/Context;

    new-instance v0, Lcom/facebook/video/watchandgo/ipc/WatchAndGoIntentDispatcher$1;

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/facebook/video/watchandgo/ipc/WatchAndGoIntentDispatcher$1;-><init>(LX/D7p;LX/2pa;Lcom/facebook/video/player/RichVideoPlayer;LX/04g;LX/D7o;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1967672
    new-instance v1, LX/D7u;

    invoke-direct {v1, v9, v0}, LX/D7u;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 1967673
    invoke-virtual {v1}, LX/D7u;->show()V

    .line 1967674
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1967675
    goto :goto_0

    .line 1967676
    :cond_2
    invoke-static {p0}, LX/D7p;->a(LX/D7p;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    .line 1967677
    invoke-static/range {v1 .. v8}, LX/D7p;->a$redex0(LX/D7p;LX/2pa;Lcom/facebook/video/player/RichVideoPlayer;LX/04g;LX/D7o;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_1
.end method

.method public static a(LX/D7p;)Z
    .locals 4

    .prologue
    .line 1967678
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 1967679
    iget-object v0, p0, LX/D7p;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/Settings;->canDrawOverlays(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1967680
    iget-object v0, p0, LX/D7p;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D7x;

    iget-object v1, p0, LX/D7p;->b:Landroid/content/Context;

    .line 1967681
    new-instance v2, LX/31Y;

    invoke-direct {v2, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0828bd

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    const v3, 0x7f0828be

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    const v3, 0x7f0828bf

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance p0, LX/D7w;

    invoke-direct {p0, v0, v1}, LX/D7w;-><init>(LX/D7x;Landroid/content/Context;)V

    invoke-virtual {v2, v3, p0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x7f0828c0

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance p0, LX/D7v;

    invoke-direct {p0, v0}, LX/D7v;-><init>(LX/D7x;)V

    invoke-virtual {v2, v3, p0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    .line 1967682
    invoke-virtual {v2}, LX/2EJ;->show()V

    .line 1967683
    const/4 v0, 0x0

    .line 1967684
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a$redex0(LX/D7p;LX/2pa;Lcom/facebook/video/player/RichVideoPlayer;LX/04g;LX/D7o;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2pa;",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            "LX/04g;",
            "LX/D7o;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1967641
    sget-object v0, LX/D7n;->a:Ljava/lang/String;

    .line 1967642
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, LX/D7p;->b:Landroid/content/Context;

    iget-object v1, p0, LX/D7p;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1967643
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1967644
    move-object v0, v2

    .line 1967645
    invoke-static {p1}, LX/2pZ;->a(LX/2pa;)LX/2pZ;

    move-result-object v1

    invoke-virtual {v1}, LX/2pZ;->b()LX/2pa;

    move-result-object v1

    sput-object v1, LX/D7p;->a:LX/2pa;

    .line 1967646
    const-string v1, "com.facebook.katana.EXTRA_SEEK_POSITION"

    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1967647
    const-string v1, "com.facebook.katana.EXTRA_LAST_START_POSITION"

    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1967648
    const-string v1, "com.facebook.katana.EXTRA_PLAYER_ORIGIN"

    .line 1967649
    iget-object v2, p2, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v2, v2

    .line 1967650
    invoke-virtual {v2}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1967651
    const-string v1, "com.facebook.katana.EXTRA_ORIGINAL_PLAYER_TYPE"

    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v2

    iget-object v2, v2, LX/04G;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1967652
    const-string v1, "com.facebook.katana.EXTRA_EVENT_TRIGGER"

    iget-object v2, p3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1967653
    const-string v1, "com.facebook.katana.EXTRA_VIDEO_TYPE"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1967654
    const-string v1, "com.facebook.katana.EXTRA_STORY_PROPS"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1967655
    const-string v1, "com.facebook.katana.EXTRA_VIDEO_PROPS"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1967656
    const-string v1, "com.facebook.katana.EXTRA_STORY_ATTACHMENT_PROPS"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1967657
    iget-wide v2, p1, LX/2pa;->d:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    .line 1967658
    const-string v1, "com.facebook.katana.EXTRA_ASPECT_RATIO"

    iget-wide v2, p1, LX/2pa;->d:D

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1967659
    :cond_0
    iget-object v1, p0, LX/D7p;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/D7p;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 1967660
    const/4 v0, 0x1

    .line 1967661
    sput-boolean v0, LX/13l;->c:Z

    .line 1967662
    iget-object v0, p0, LX/D7p;->b:Landroid/content/Context;

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1967663
    if-eqz v0, :cond_1

    .line 1967664
    iget-object v0, p0, LX/D7p;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13l;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, LX/13l;->a(LX/04g;)V

    .line 1967665
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1967639
    sget-object v2, LX/D7o;->CHANNEL_FEED:LX/D7o;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/D7p;->a(LX/D7p;Lcom/facebook/video/player/RichVideoPlayer;LX/D7o;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1967640
    return-void
.end method
