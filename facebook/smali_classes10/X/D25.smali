.class public final LX/D25;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/share/model/ComposerAppAttribution;

.field public final synthetic c:J

.field public final synthetic d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V
    .locals 0

    .prologue
    .line 1958092
    iput-object p1, p0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    iput-object p2, p0, LX/D25;->a:Ljava/lang/String;

    iput-object p3, p0, LX/D25;->b:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-wide p4, p0, LX/D25;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1958097
    iget-object v0, p0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    iget-boolean v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->y:Z

    if-eqz v0, :cond_0

    .line 1958098
    :goto_0
    return-void

    .line 1958099
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->q:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    iget-object v1, p0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    iget-object v2, p0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->x:Landroid/net/Uri;

    new-instance v3, LX/D24;

    invoke-direct {v3, p0}, LX/D24;-><init>(LX/D25;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->b(Landroid/content/Context;Landroid/net/Uri;LX/0Vd;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1958100
    :catch_0
    move-exception v0

    .line 1958101
    iget-object v1, p0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 1958102
    invoke-static {v1, v0, v2}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->a$redex0(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1958103
    goto :goto_0
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1958095
    iget-object v0, p0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->finish()V

    .line 1958096
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1958093
    iget-object v0, p0, LX/D25;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->finish()V

    .line 1958094
    return-void
.end method
