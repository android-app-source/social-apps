.class public final LX/EcB;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EcA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EcB;",
        ">;",
        "LX/EcA;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2145668
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2145669
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcB;->c:LX/EWc;

    .line 2145670
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2145671
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2145672
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcB;->c:LX/EWc;

    .line 2145673
    return-void
.end method

.method private d(LX/EWY;)LX/EcB;
    .locals 1

    .prologue
    .line 2145674
    instance-of v0, p1, LX/EcC;

    if-eqz v0, :cond_0

    .line 2145675
    check-cast p1, LX/EcC;

    invoke-virtual {p0, p1}, LX/EcB;->a(LX/EcC;)LX/EcB;

    move-result-object p0

    .line 2145676
    :goto_0
    return-object p0

    .line 2145677
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EcB;
    .locals 4

    .prologue
    .line 2145678
    const/4 v2, 0x0

    .line 2145679
    :try_start_0
    sget-object v0, LX/EcC;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcC;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2145680
    if-eqz v0, :cond_0

    .line 2145681
    invoke-virtual {p0, v0}, LX/EcB;->a(LX/EcC;)LX/EcB;

    .line 2145682
    :cond_0
    return-object p0

    .line 2145683
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2145684
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2145685
    check-cast v0, LX/EcC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2145686
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2145687
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2145688
    invoke-virtual {p0, v1}, LX/EcB;->a(LX/EcC;)LX/EcB;

    :cond_1
    throw v0

    .line 2145689
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static u()LX/EcB;
    .locals 1

    .prologue
    .line 2145690
    new-instance v0, LX/EcB;

    invoke-direct {v0}, LX/EcB;-><init>()V

    return-object v0
.end method

.method private w()LX/EcB;
    .locals 2

    .prologue
    .line 2145691
    invoke-static {}, LX/EcB;->u()LX/EcB;

    move-result-object v0

    invoke-direct {p0}, LX/EcB;->y()LX/EcC;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcB;->a(LX/EcC;)LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/EcC;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2145692
    new-instance v2, LX/EcC;

    invoke-direct {v2, p0}, LX/EcC;-><init>(LX/EWj;)V

    .line 2145693
    iget v3, p0, LX/EcB;->a:I

    .line 2145694
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2145695
    :goto_0
    iget v1, p0, LX/EcB;->b:I

    .line 2145696
    iput v1, v2, LX/EcC;->iteration_:I

    .line 2145697
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2145698
    or-int/lit8 v0, v0, 0x2

    .line 2145699
    :cond_0
    iget-object v1, p0, LX/EcB;->c:LX/EWc;

    .line 2145700
    iput-object v1, v2, LX/EcC;->seed_:LX/EWc;

    .line 2145701
    iput v0, v2, LX/EcC;->bitField0_:I

    .line 2145702
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2145703
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2145704
    invoke-direct {p0, p1}, LX/EcB;->d(LX/EWY;)LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2145705
    invoke-direct {p0, p1, p2}, LX/EcB;->d(LX/EWd;LX/EYZ;)LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/EcB;
    .locals 1

    .prologue
    .line 2145706
    iget v0, p0, LX/EcB;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EcB;->a:I

    .line 2145707
    iput p1, p0, LX/EcB;->b:I

    .line 2145708
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145709
    return-object p0
.end method

.method public final a(LX/EWc;)LX/EcB;
    .locals 1

    .prologue
    .line 2145710
    if-nez p1, :cond_0

    .line 2145711
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2145712
    :cond_0
    iget v0, p0, LX/EcB;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EcB;->a:I

    .line 2145713
    iput-object p1, p0, LX/EcB;->c:LX/EWc;

    .line 2145714
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145715
    return-object p0
.end method

.method public final a(LX/EcC;)LX/EcB;
    .locals 2

    .prologue
    .line 2145716
    sget-object v0, LX/EcC;->c:LX/EcC;

    move-object v0, v0

    .line 2145717
    if-ne p1, v0, :cond_0

    .line 2145718
    :goto_0
    return-object p0

    .line 2145719
    :cond_0
    const/4 v0, 0x1

    .line 2145720
    iget v1, p1, LX/EcC;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    move v0, v0

    .line 2145721
    if-eqz v0, :cond_1

    .line 2145722
    iget v0, p1, LX/EcC;->iteration_:I

    move v0, v0

    .line 2145723
    invoke-virtual {p0, v0}, LX/EcB;->a(I)LX/EcB;

    .line 2145724
    :cond_1
    iget v0, p1, LX/EcC;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2145725
    if-eqz v0, :cond_2

    .line 2145726
    iget-object v0, p1, LX/EcC;->seed_:LX/EWc;

    move-object v0, v0

    .line 2145727
    invoke-virtual {p0, v0}, LX/EcB;->a(LX/EWc;)LX/EcB;

    .line 2145728
    :cond_2
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2145667
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2145649
    invoke-direct {p0, p1, p2}, LX/EcB;->d(LX/EWd;LX/EYZ;)LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2145729
    invoke-direct {p0}, LX/EcB;->w()LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2145650
    invoke-direct {p0, p1, p2}, LX/EcB;->d(LX/EWd;LX/EYZ;)LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2145651
    invoke-direct {p0}, LX/EcB;->w()LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2145652
    invoke-direct {p0, p1}, LX/EcB;->d(LX/EWY;)LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2145653
    invoke-direct {p0}, LX/EcB;->w()LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2145654
    sget-object v0, LX/Eck;->z:LX/EYn;

    const-class v1, LX/EcC;

    const-class v2, LX/EcB;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2145655
    sget-object v0, LX/Eck;->y:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2145656
    invoke-direct {p0}, LX/EcB;->w()LX/EcB;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2145657
    invoke-direct {p0}, LX/EcB;->y()LX/EcC;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2145658
    invoke-virtual {p0}, LX/EcB;->l()LX/EcC;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2145659
    invoke-direct {p0}, LX/EcB;->y()LX/EcC;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2145660
    invoke-virtual {p0}, LX/EcB;->l()LX/EcC;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EcC;
    .locals 2

    .prologue
    .line 2145661
    invoke-direct {p0}, LX/EcB;->y()LX/EcC;

    move-result-object v0

    .line 2145662
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2145663
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2145664
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145665
    sget-object v0, LX/EcC;->c:LX/EcC;

    move-object v0, v0

    .line 2145666
    return-object v0
.end method
