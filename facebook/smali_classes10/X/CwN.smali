.class public final LX/CwN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/3bj;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/NullStateModuleSuggestionUnit;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field public g:Z

.field public h:I

.field public i:LX/CwO;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1950472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/CwN;
    .locals 1

    .prologue
    .line 1950473
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3bj;->valueOf(Ljava/lang/String;)LX/3bj;

    move-result-object v0

    iput-object v0, p0, LX/CwN;->a:LX/3bj;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1950474
    :goto_0
    return-object p0

    .line 1950475
    :catch_0
    sget-object v0, LX/3bj;->unset:LX/3bj;

    iput-object v0, p0, LX/CwN;->a:LX/3bj;

    goto :goto_0
.end method

.method public final a()Lcom/facebook/search/model/NullStateModuleCollectionUnit;
    .locals 1

    .prologue
    .line 1950476
    new-instance v0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    invoke-direct {v0, p0}, Lcom/facebook/search/model/NullStateModuleCollectionUnit;-><init>(LX/CwN;)V

    return-object v0
.end method

.method public final e(Ljava/lang/String;)LX/CwN;
    .locals 1

    .prologue
    .line 1950477
    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CwO;->valueOf(Ljava/lang/String;)LX/CwO;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1950478
    :goto_0
    move-object v0, v0

    .line 1950479
    iput-object v0, p0, LX/CwN;->i:LX/CwO;

    .line 1950480
    return-object p0

    :catch_0
    sget-object v0, LX/CwO;->DISABLED:LX/CwO;

    goto :goto_0
.end method
