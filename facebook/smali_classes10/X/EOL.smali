.class public final LX/EOL;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EOM;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public final synthetic b:LX/EOM;


# direct methods
.method public constructor <init>(LX/EOM;)V
    .locals 1

    .prologue
    .line 2114344
    iput-object p1, p0, LX/EOL;->b:LX/EOM;

    .line 2114345
    move-object v0, p1

    .line 2114346
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2114347
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2114359
    const-string v0, "SearchResultsBlendedPhotoEmptyComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2114348
    if-ne p0, p1, :cond_1

    .line 2114349
    :cond_0
    :goto_0
    return v0

    .line 2114350
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2114351
    goto :goto_0

    .line 2114352
    :cond_3
    check-cast p1, LX/EOL;

    .line 2114353
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2114354
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2114355
    if-eq v2, v3, :cond_0

    .line 2114356
    iget-object v2, p0, LX/EOL;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/EOL;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/EOL;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2114357
    goto :goto_0

    .line 2114358
    :cond_4
    iget-object v2, p1, LX/EOL;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
