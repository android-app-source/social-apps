.class public LX/ECp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3A0;

.field private final b:LX/EG1;

.field private final c:Ljava/lang/String;

.field private final d:LX/0ad;

.field public final e:LX/0kL;

.field private final f:LX/2Oi;

.field public g:I


# direct methods
.method public constructor <init>(LX/3A0;LX/EG1;LX/0ad;LX/0kL;Ljava/lang/String;LX/2Oi;)V
    .locals 0
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2090379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2090380
    iput-object p1, p0, LX/ECp;->a:LX/3A0;

    .line 2090381
    iput-object p2, p0, LX/ECp;->b:LX/EG1;

    .line 2090382
    iput-object p3, p0, LX/ECp;->d:LX/0ad;

    .line 2090383
    iput-object p4, p0, LX/ECp;->e:LX/0kL;

    .line 2090384
    iput-object p5, p0, LX/ECp;->c:Ljava/lang/String;

    .line 2090385
    iput-object p6, p0, LX/ECp;->f:LX/2Oi;

    .line 2090386
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/messaging/model/threads/ThreadSummary;ZLjava/lang/String;)V
    .locals 13

    .prologue
    .line 2090387
    const v0, 0x7f0104b2

    const v1, 0x7f0e0551

    invoke-static {p1, v0, v1}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v6

    .line 2090388
    iget-object v0, p0, LX/ECp;->a:LX/3A0;

    invoke-virtual {v0}, LX/3A0;->f()I

    move-result v0

    add-int/lit8 v9, v0, -0x1

    .line 2090389
    iget-object v0, p0, LX/ECp;->d:LX/0ad;

    sget v1, LX/3Dx;->fp:I

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 2090390
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 2090391
    iget-object v1, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-gt v1, v0, :cond_2

    const/4 v0, 0x1

    move v8, v0

    .line 2090392
    :goto_0
    iget-object v2, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2090393
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/ECp;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2090394
    :cond_0
    iget-object v4, p0, LX/ECp;->f:LX/2Oi;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 2090395
    if-eqz v0, :cond_1

    .line 2090396
    iget-object v0, p0, LX/ECp;->b:LX/EG1;

    invoke-interface {v0}, LX/EG1;->a()LX/3OO;

    move-result-object v0

    .line 2090397
    invoke-virtual {v10, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2090398
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2090399
    :cond_2
    const/4 v0, 0x0

    move v8, v0

    goto :goto_0

    .line 2090400
    :cond_3
    iget-object v0, p0, LX/ECp;->b:LX/EG1;

    invoke-interface {v0}, LX/EG1;->b()LX/3LH;

    move-result-object v2

    .line 2090401
    new-instance v0, LX/31Y;

    invoke-direct {v0, v6}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0807fa

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v7

    const v11, 0x7f0807fe

    new-instance v0, LX/ECm;

    move-object v1, p0

    move-object v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    invoke-direct/range {v0 .. v6}, LX/ECm;-><init>(LX/ECp;LX/3LH;Lcom/facebook/messaging/model/threads/ThreadSummary;ZLjava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v7, v11, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0807ff

    new-instance v3, LX/ECl;

    invoke-direct {v3, p0}, LX/ECl;-><init>(LX/ECp;)V

    invoke-virtual {v0, v1, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v7

    .line 2090402
    if-eqz v8, :cond_4

    .line 2090403
    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/ECp;->g:I

    .line 2090404
    :goto_2
    new-instance v1, LX/3Ne;

    const v0, 0x7f03123f

    invoke-direct {v1, v6, v0}, LX/3Ne;-><init>(Landroid/content/Context;I)V

    .line 2090405
    const v0, 0x7f0d2ada

    invoke-virtual {v1, v0}, LX/3Ne;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2090406
    const v3, 0x7f080800

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2090407
    invoke-virtual {v1, v2}, LX/3Ne;->setAdapter(LX/3LH;)V

    .line 2090408
    new-instance v3, LX/ECn;

    move-object v4, p0

    move v5, v9

    move-object v8, v2

    invoke-direct/range {v3 .. v8}, LX/ECn;-><init>(LX/ECp;ILandroid/content/Context;LX/2EJ;LX/3LH;)V

    invoke-virtual {v1, v3}, LX/3Ne;->setOnRowClickedListener(LX/3O4;)V

    .line 2090409
    new-instance v0, LX/ECo;

    invoke-direct {v0, p0}, LX/ECo;-><init>(LX/ECp;)V

    invoke-virtual {v7, v0}, LX/2EJ;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 2090410
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3Ne;->a(LX/0Px;)V

    .line 2090411
    const/16 v9, 0xa

    const/16 v10, 0xa

    const/16 v11, 0xa

    const/16 v12, 0xa

    move-object v8, v1

    invoke-virtual/range {v7 .. v12}, LX/2EJ;->a(Landroid/view/View;IIII)V

    .line 2090412
    invoke-virtual {v7}, LX/2EJ;->show()V

    .line 2090413
    return-void

    .line 2090414
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, LX/ECp;->g:I

    goto :goto_2
.end method
