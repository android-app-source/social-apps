.class public final enum LX/DvW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DvW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DvW;

.field public static final enum ALBUM_MEDIA_SET:LX/DvW;

.field public static final enum CAMPAIGN_MEDIA_SET:LX/DvW;

.field public static final enum PAGE_ALL_PHOTOS_TAB:LX/DvW;

.field public static final enum TAGGED_MEDIA_SET:LX/DvW;

.field public static final enum UPLOADED_MEDIA_SET:LX/DvW;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2058655
    new-instance v0, LX/DvW;

    const-string v1, "TAGGED_MEDIA_SET"

    invoke-direct {v0, v1, v2}, LX/DvW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DvW;->TAGGED_MEDIA_SET:LX/DvW;

    .line 2058656
    new-instance v0, LX/DvW;

    const-string v1, "UPLOADED_MEDIA_SET"

    invoke-direct {v0, v1, v3}, LX/DvW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DvW;->UPLOADED_MEDIA_SET:LX/DvW;

    .line 2058657
    new-instance v0, LX/DvW;

    const-string v1, "ALBUM_MEDIA_SET"

    invoke-direct {v0, v1, v4}, LX/DvW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DvW;->ALBUM_MEDIA_SET:LX/DvW;

    .line 2058658
    new-instance v0, LX/DvW;

    const-string v1, "CAMPAIGN_MEDIA_SET"

    invoke-direct {v0, v1, v5}, LX/DvW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DvW;->CAMPAIGN_MEDIA_SET:LX/DvW;

    .line 2058659
    new-instance v0, LX/DvW;

    const-string v1, "PAGE_ALL_PHOTOS_TAB"

    invoke-direct {v0, v1, v6}, LX/DvW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DvW;->PAGE_ALL_PHOTOS_TAB:LX/DvW;

    .line 2058660
    const/4 v0, 0x5

    new-array v0, v0, [LX/DvW;

    sget-object v1, LX/DvW;->TAGGED_MEDIA_SET:LX/DvW;

    aput-object v1, v0, v2

    sget-object v1, LX/DvW;->UPLOADED_MEDIA_SET:LX/DvW;

    aput-object v1, v0, v3

    sget-object v1, LX/DvW;->ALBUM_MEDIA_SET:LX/DvW;

    aput-object v1, v0, v4

    sget-object v1, LX/DvW;->CAMPAIGN_MEDIA_SET:LX/DvW;

    aput-object v1, v0, v5

    sget-object v1, LX/DvW;->PAGE_ALL_PHOTOS_TAB:LX/DvW;

    aput-object v1, v0, v6

    sput-object v0, LX/DvW;->$VALUES:[LX/DvW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2058661
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DvW;
    .locals 1

    .prologue
    .line 2058662
    const-class v0, LX/DvW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DvW;

    return-object v0
.end method

.method public static values()[LX/DvW;
    .locals 1

    .prologue
    .line 2058663
    sget-object v0, LX/DvW;->$VALUES:[LX/DvW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DvW;

    return-object v0
.end method
