.class public final LX/ENk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;Landroid/net/Uri;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2113260
    iput-object p1, p0, LX/ENk;->c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;

    iput-object p2, p0, LX/ENk;->a:Landroid/net/Uri;

    iput-object p3, p0, LX/ENk;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x79e16664

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2113261
    iget-object v1, p0, LX/ENk;->c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/ENk;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, LX/ENk;->b:LX/1Pn;

    invoke-interface {v3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2113262
    const v1, -0x16366e7c

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
