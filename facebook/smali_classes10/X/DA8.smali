.class public final enum LX/DA8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DA8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DA8;

.field public static final enum UPLOAD_FAILED:LX/DA8;

.field public static final enum UPLOAD_SETTING_SET:LX/DA8;

.field public static final enum UPLOAD_STARTED:LX/DA8;

.field public static final enum UPLOAD_SUCCEEDED:LX/DA8;

.field public static final enum UPLOAD_TYPES:LX/DA8;


# instance fields
.field private final mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1970843
    new-instance v0, LX/DA8;

    const-string v1, "UPLOAD_STARTED"

    const-string v2, "contact_logs_upload_started"

    invoke-direct {v0, v1, v3, v2}, LX/DA8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DA8;->UPLOAD_STARTED:LX/DA8;

    .line 1970844
    new-instance v0, LX/DA8;

    const-string v1, "UPLOAD_SUCCEEDED"

    const-string v2, "contact_logs_upload_succeeded"

    invoke-direct {v0, v1, v4, v2}, LX/DA8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DA8;->UPLOAD_SUCCEEDED:LX/DA8;

    .line 1970845
    new-instance v0, LX/DA8;

    const-string v1, "UPLOAD_FAILED"

    const-string v2, "contact_logs_upload_failed"

    invoke-direct {v0, v1, v5, v2}, LX/DA8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DA8;->UPLOAD_FAILED:LX/DA8;

    .line 1970846
    new-instance v0, LX/DA8;

    const-string v1, "UPLOAD_SETTING_SET"

    const-string v2, "contact_logs_upload_setting_set"

    invoke-direct {v0, v1, v6, v2}, LX/DA8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DA8;->UPLOAD_SETTING_SET:LX/DA8;

    .line 1970847
    new-instance v0, LX/DA8;

    const-string v1, "UPLOAD_TYPES"

    const-string v2, "contact_logs_upload_types"

    invoke-direct {v0, v1, v7, v2}, LX/DA8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/DA8;->UPLOAD_TYPES:LX/DA8;

    .line 1970848
    const/4 v0, 0x5

    new-array v0, v0, [LX/DA8;

    sget-object v1, LX/DA8;->UPLOAD_STARTED:LX/DA8;

    aput-object v1, v0, v3

    sget-object v1, LX/DA8;->UPLOAD_SUCCEEDED:LX/DA8;

    aput-object v1, v0, v4

    sget-object v1, LX/DA8;->UPLOAD_FAILED:LX/DA8;

    aput-object v1, v0, v5

    sget-object v1, LX/DA8;->UPLOAD_SETTING_SET:LX/DA8;

    aput-object v1, v0, v6

    sget-object v1, LX/DA8;->UPLOAD_TYPES:LX/DA8;

    aput-object v1, v0, v7

    sput-object v0, LX/DA8;->$VALUES:[LX/DA8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1970849
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1970850
    iput-object p3, p0, LX/DA8;->mEventName:Ljava/lang/String;

    .line 1970851
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DA8;
    .locals 1

    .prologue
    .line 1970842
    const-class v0, LX/DA8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DA8;

    return-object v0
.end method

.method public static values()[LX/DA8;
    .locals 1

    .prologue
    .line 1970841
    sget-object v0, LX/DA8;->$VALUES:[LX/DA8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DA8;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1970840
    iget-object v0, p0, LX/DA8;->mEventName:Ljava/lang/String;

    return-object v0
.end method
