.class public LX/EKJ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final e:LX/3ag;


# instance fields
.field public a:LX/0hy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EPK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2106448
    new-instance v0, LX/EKG;

    invoke-direct {v0}, LX/EKG;-><init>()V

    sput-object v0, LX/EKJ;->e:LX/3ag;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2106449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2106450
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2106451
    iput-object v0, p0, LX/EKJ;->c:LX/0Ot;

    .line 2106452
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2106453
    iput-object v0, p0, LX/EKJ;->d:LX/0Ot;

    .line 2106454
    return-void
.end method

.method public static b(LX/0QB;)LX/EKJ;
    .locals 5

    .prologue
    .line 2106455
    new-instance v2, LX/EKJ;

    invoke-direct {v2}, LX/EKJ;-><init>()V

    .line 2106456
    invoke-static {p0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v0

    check-cast v0, LX/0hy;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x33c2

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x348e

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 2106457
    iput-object v0, v2, LX/EKJ;->a:LX/0hy;

    iput-object v1, v2, LX/EKJ;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v3, v2, LX/EKJ;->c:LX/0Ot;

    iput-object v4, v2, LX/EKJ;->d:LX/0Ot;

    .line 2106458
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2106459
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    invoke-virtual {v0, p1}, LX/89k;->e(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/89k;->f(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 2106460
    iput-object v1, v0, LX/89k;->b:Ljava/lang/String;

    .line 2106461
    move-object v0, v0

    .line 2106462
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    .line 2106463
    iput-object v1, v0, LX/89k;->c:Ljava/lang/String;

    .line 2106464
    move-object v0, v0

    .line 2106465
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 2106466
    iget-object v1, p0, LX/EKJ;->a:LX/0hy;

    invoke-interface {v1, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2106467
    if-eqz v0, :cond_0

    .line 2106468
    iget-object v1, p0, LX/EKJ;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2106469
    :cond_0
    return-void
.end method
