.class public LX/E4V;
.super Lcom/facebook/events/widget/eventcard/EventCardFooterView;
.source ""

# interfaces
.implements LX/Bni;


# instance fields
.field public j:LX/38v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/7vZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/7vW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

.field public n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

.field public o:LX/2ja;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2076801
    invoke-direct {p0, p1}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;-><init>(Landroid/content/Context;)V

    .line 2076802
    const-class p1, LX/E4V;

    invoke-static {p1, p0}, LX/E4V;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2076803
    const/16 p1, 0x10

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 2076804
    return-void
.end method

.method public static a(LX/E4V;LX/Cfc;)V
    .locals 4

    .prologue
    .line 2076805
    iget-object v0, p0, LX/E4V;->o:LX/2ja;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E4V;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E4V;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2076806
    iget-object v0, p0, LX/E4V;->o:LX/2ja;

    iget-object v1, p0, LX/E4V;->p:Ljava/lang/String;

    iget-object v2, p0, LX/E4V;->q:Ljava/lang/String;

    iget-object v3, p0, LX/E4V;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, p1, v3}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfc;Ljava/lang/String;)V

    .line 2076807
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/E4V;

    const-class v1, LX/38v;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/38v;

    invoke-static {p0}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v2

    check-cast v2, LX/7vZ;

    invoke-static {p0}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object p0

    check-cast p0, LX/7vW;

    iput-object v1, p1, LX/E4V;->j:LX/38v;

    iput-object v2, p1, LX/E4V;->k:LX/7vZ;

    iput-object p0, p1, LX/E4V;->l:LX/7vW;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 7

    .prologue
    .line 2076808
    iget-object v0, p0, LX/E4V;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    if-nez v0, :cond_0

    .line 2076809
    :goto_0
    return-void

    .line 2076810
    :cond_0
    iget-object v0, p0, LX/E4V;->l:LX/7vW;

    iget-object v1, p0, LX/E4V;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v3, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->a:Ljava/lang/String;

    :goto_1
    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v4, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->b:Ljava/lang/String;

    :goto_2
    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v5, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    :goto_3
    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v6, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    :goto_4
    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2076811
    sget-object v0, LX/E4U;->b:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2076812
    :goto_5
    goto :goto_0

    .line 2076813
    :cond_1
    const-string v3, "unknown"

    goto :goto_1

    :cond_2
    const-string v4, "unknown"

    goto :goto_2

    :cond_3
    const-string v5, "unknown"

    goto :goto_3

    :cond_4
    const-string v6, "unknown"

    goto :goto_4

    .line 2076814
    :pswitch_0
    sget-object v0, LX/Cfc;->EVENT_CARD_GOING_TAP:LX/Cfc;

    .line 2076815
    :goto_6
    invoke-static {p0, v0}, LX/E4V;->a(LX/E4V;LX/Cfc;)V

    goto :goto_5

    .line 2076816
    :pswitch_1
    sget-object v0, LX/Cfc;->EVENT_CARD_MAYBE_TAP:LX/Cfc;

    goto :goto_6

    .line 2076817
    :pswitch_2
    sget-object v0, LX/Cfc;->EVENT_CARD_NOT_GOING_TAP:LX/Cfc;

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 7

    .prologue
    .line 2076818
    iget-object v0, p0, LX/E4V;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    if-nez v0, :cond_0

    .line 2076819
    :goto_0
    return-void

    .line 2076820
    :cond_0
    iget-object v0, p0, LX/E4V;->k:LX/7vZ;

    iget-object v1, p0, LX/E4V;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v3, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->a:Ljava/lang/String;

    :goto_1
    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v4, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->b:Ljava/lang/String;

    :goto_2
    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v5, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    :goto_3
    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v6, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->d:Ljava/lang/String;

    :goto_4
    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2076821
    sget-object v0, LX/E4U;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2076822
    :goto_5
    goto :goto_0

    .line 2076823
    :cond_1
    const-string v3, "unknown"

    goto :goto_1

    :cond_2
    const-string v4, "unknown"

    goto :goto_2

    :cond_3
    const-string v5, "unknown"

    goto :goto_3

    :cond_4
    const-string v6, "unknown"

    goto :goto_4

    .line 2076824
    :pswitch_0
    sget-object v0, LX/Cfc;->EVENT_CARD_WATCHED_TAP:LX/Cfc;

    .line 2076825
    :goto_6
    invoke-static {p0, v0}, LX/E4V;->a(LX/E4V;LX/Cfc;)V

    goto :goto_5

    .line 2076826
    :pswitch_1
    sget-object v0, LX/Cfc;->EVENT_CARD_GOING_TAP:LX/Cfc;

    goto :goto_6

    .line 2076827
    :pswitch_2
    sget-object v0, LX/Cfc;->EVENT_CARD_UNWATCHED_TAP:LX/Cfc;

    goto :goto_6

    .line 2076828
    :pswitch_3
    sget-object v0, LX/Cfc;->EVENT_CARD_NOT_GOING_TAP:LX/Cfc;

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2076829
    invoke-super {p0}, Lcom/facebook/events/widget/eventcard/EventCardFooterView;->d()V

    .line 2076830
    const/4 v0, 0x0

    .line 2076831
    iput-object v0, p0, LX/E4V;->n:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 2076832
    return-void
.end method
