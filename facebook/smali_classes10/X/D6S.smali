.class public LX/D6S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final c:LX/D5f;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/D5f;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1965730
    new-instance v0, LX/D6R;

    invoke-direct {v0}, LX/D6R;-><init>()V

    sput-object v0, LX/D6S;->c:LX/D5f;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1965725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1965726
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/D6S;->a:Ljava/util/List;

    .line 1965727
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/D6S;->b:Ljava/util/Set;

    .line 1965728
    sget-object v0, LX/D6S;->c:LX/D5f;

    iput-object v0, p0, LX/D6S;->d:LX/D5f;

    .line 1965729
    return-void
.end method

.method public static a(LX/0QB;)LX/D6S;
    .locals 1

    .prologue
    .line 1965722
    new-instance v0, LX/D6S;

    invoke-direct {v0}, LX/D6S;-><init>()V

    .line 1965723
    move-object v0, v0

    .line 1965724
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1965716
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_1

    instance-of v1, p0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    if-nez v1, :cond_1

    .line 1965717
    :cond_0
    :goto_0
    return v0

    .line 1965718
    :cond_1
    instance-of v1, p0, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v1, :cond_2

    .line 1965719
    check-cast p0, Lcom/facebook/graphql/model/HideableUnit;

    .line 1965720
    invoke-interface {p0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eq v1, v2, :cond_0

    .line 1965721
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1965711
    if-nez p0, :cond_1

    .line 1965712
    :cond_0
    :goto_0
    return-object v0

    .line 1965713
    :cond_1
    invoke-static {p0}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1965714
    if-eqz v1, :cond_0

    .line 1965715
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(LX/D6S;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1965707
    invoke-static {p1}, LX/D6S;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v0

    .line 1965708
    if-nez v0, :cond_0

    .line 1965709
    :goto_0
    return-void

    .line 1965710
    :cond_0
    iget-object v1, p0, LX/D6S;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static e(LX/D6S;Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 1965688
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1965689
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1965690
    iget-object v0, p0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1965691
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1965692
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1965693
    instance-of v3, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_0

    .line 1965694
    invoke-static {v0}, LX/182;->k(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1965695
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1965696
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1965697
    :goto_1
    if-eqz v0, :cond_1

    .line 1965698
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1965699
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1965700
    :goto_2
    return v2

    .line 1965701
    :cond_0
    instance-of v0, v1, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    if-eqz v0, :cond_1

    .line 1965702
    check-cast v1, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    .line 1965703
    iget-object v0, v1, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 1965704
    goto :goto_1

    .line 1965705
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1965706
    :cond_2
    const/4 v2, -0x1

    goto :goto_2
.end method

.method public static e(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1965731
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1965732
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1965733
    const/4 v1, 0x0

    .line 1965734
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    .line 1965735
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1965736
    :goto_0
    invoke-static {v0}, LX/D6S;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1965737
    :cond_0
    instance-of v2, v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    if-eqz v2, :cond_1

    .line 1965738
    check-cast v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    .line 1965739
    iget-object v1, v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v1

    .line 1965740
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1965679
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1965680
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1965681
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 1965682
    :goto_0
    return-object p0

    .line 1965683
    :cond_0
    instance-of v1, v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    if-eqz v1, :cond_1

    .line 1965684
    check-cast v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    .line 1965685
    iget-object v1, v0, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v1

    .line 1965686
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    goto :goto_0

    .line 1965687
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1965662
    if-nez p1, :cond_1

    move-object v0, v3

    .line 1965663
    :cond_0
    :goto_0
    return-object v0

    .line 1965664
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1965665
    iget-object v0, p0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1965666
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1965667
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1965668
    instance-of v4, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_3

    .line 1965669
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1965670
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1965671
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1965672
    :cond_3
    instance-of v0, v1, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    if-eqz v0, :cond_2

    .line 1965673
    check-cast v1, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    .line 1965674
    iget-object v0, v1, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 1965675
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1965676
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1965677
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v0, v3

    .line 1965678
    goto :goto_0
.end method

.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1965661
    invoke-virtual {p0, p1}, LX/D6S;->b(I)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1965655
    iget-object v1, p0, LX/D6S;->d:LX/D5f;

    .line 1965656
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1965657
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v1, v0}, LX/D5f;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1965658
    iget-object v0, p0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1965659
    invoke-static {p0, p2}, LX/D6S;->c(LX/D6S;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1965660
    return-void
.end method

.method public final b(I)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1965654
    iget-object v0, p0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1965643
    invoke-static {p0, p1}, LX/D6S;->e(LX/D6S;Ljava/lang/String;)I

    move-result v0

    .line 1965644
    if-gez v0, :cond_1

    move-object v0, v3

    .line 1965645
    :goto_0
    return-object v0

    :cond_0
    move v0, v2

    .line 1965646
    :cond_1
    iget-object v1, p0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1965647
    add-int/lit8 v2, v0, 0x1

    .line 1965648
    iget-object v0, p0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1965649
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1965650
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 1965651
    invoke-static {v1}, LX/D6S;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1965652
    invoke-static {v0}, LX/D6S;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v3

    .line 1965653
    goto :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1965637
    iget-object v1, p0, LX/D6S;->d:LX/D5f;

    .line 1965638
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1965639
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v1, v0}, LX/D5f;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1965640
    iget-object v0, p0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1965641
    invoke-static {p0, p1}, LX/D6S;->c(LX/D6S;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1965642
    :cond_0
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1965636
    iget-object v0, p0, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
