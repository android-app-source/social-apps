.class public LX/Cwq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951145
    return-void
.end method

.method public static a(LX/0QB;)LX/Cwq;
    .locals 1

    .prologue
    .line 1951118
    new-instance v0, LX/Cwq;

    invoke-direct {v0}, LX/Cwq;-><init>()V

    .line 1951119
    move-object v0, v0

    .line 1951120
    return-object v0
.end method

.method public static a(Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;)Lcom/facebook/search/results/protocol/filters/FilterValue;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1951121
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1951122
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951123
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_FILTER:LX/3Ql;

    const-string v2, "Filter Value Missing Value "

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1951124
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1951125
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_FILTER:LX/3Ql;

    const-string v2, "Filter Value Missing Text "

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1951126
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->d()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->d()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;->b()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1951127
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->d()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;->b()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 1951128
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->d()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1951129
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->d()Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel$ValueObjectModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1951130
    :cond_2
    invoke-static {}, Lcom/facebook/search/results/protocol/filters/FilterValue;->g()LX/5ur;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 1951131
    iput-object v3, v2, LX/5ur;->b:Ljava/lang/String;

    .line 1951132
    move-object v2, v2

    .line 1951133
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 1951134
    iput-object v3, v2, LX/5ur;->a:Ljava/lang/String;

    .line 1951135
    move-object v2, v2

    .line 1951136
    iput-object v1, v2, LX/5ur;->c:Ljava/lang/String;

    .line 1951137
    move-object v1, v2

    .line 1951138
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterValueFragmentModel;->a()Z

    move-result v2

    .line 1951139
    iput-boolean v2, v1, LX/5ur;->d:Z

    .line 1951140
    move-object v1, v1

    .line 1951141
    iput-object v0, v1, LX/5ur;->e:Ljava/lang/String;

    .line 1951142
    move-object v0, v1

    .line 1951143
    invoke-virtual {v0}, LX/5ur;->f()Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method
