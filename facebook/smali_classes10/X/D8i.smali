.class public LX/D8i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/2dq;

.field private final b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

.field public final c:Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;

.field public d:Landroid/view/ViewGroup;

.field public e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;


# direct methods
.method public constructor <init>(LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1969234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1969235
    iput-object p1, p0, LX/D8i;->a:LX/2dq;

    .line 1969236
    iput-object p2, p0, LX/D8i;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 1969237
    iput-object p3, p0, LX/D8i;->c:Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;

    .line 1969238
    return-void
.end method

.method public static a(LX/0QB;)LX/D8i;
    .locals 6

    .prologue
    .line 1969223
    const-class v1, LX/D8i;

    monitor-enter v1

    .line 1969224
    :try_start_0
    sget-object v0, LX/D8i;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1969225
    sput-object v2, LX/D8i;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1969226
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1969227
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1969228
    new-instance p0, LX/D8i;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v3

    check-cast v3, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;->a(LX/0QB;)Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;

    invoke-direct {p0, v3, v4, v5}, LX/D8i;-><init>(LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/video/watchandshop/WatchAndShopProductItemPartDefinition;)V

    .line 1969229
    move-object v0, p0

    .line 1969230
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1969231
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D8i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1969232
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1969233
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(FLcom/facebook/feed/rows/core/props/FeedProps;LX/D8b;Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/D8b;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1969182
    iget-object v0, p0, LX/D8i;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1969183
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1969184
    if-nez v0, :cond_1

    .line 1969185
    :cond_0
    :goto_0
    return-void

    .line 1969186
    :cond_1
    iget-object v0, p0, LX/D8i;->d:Landroid/view/ViewGroup;

    const v1, 0x7f0d31a0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iput-object v0, p0, LX/D8i;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1969187
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c93

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 1969188
    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p4, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    .line 1969189
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1c96

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {p4, v1}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    .line 1969190
    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 1969191
    iget-object v1, p0, LX/D8i;->a:LX/2dq;

    int-to-float v2, v0

    const/high16 v3, 0x41000000    # 8.0f

    add-float/2addr v2, v3

    sget-object v3, LX/2eF;->a:LX/1Ua;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    .line 1969192
    int-to-float v0, v0

    invoke-static {p4, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 1969193
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1969194
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1969195
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 1969196
    if-nez v2, :cond_4

    move-object v2, v3

    .line 1969197
    :goto_1
    move-object v3, v2

    .line 1969198
    if-nez v3, :cond_3

    .line 1969199
    const/4 v2, 0x0

    .line 1969200
    :goto_2
    move-object v3, v2

    .line 1969201
    if-eqz v3, :cond_0

    .line 1969202
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1969203
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    .line 1969204
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 1969205
    check-cast v5, LX/0jW;

    .line 1969206
    new-instance v0, LX/2eG;

    const/4 v2, 0x0

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    .line 1969207
    instance-of v1, p3, LX/1Pu;

    if-eqz v1, :cond_2

    .line 1969208
    new-instance v1, LX/1Rb;

    invoke-direct {v1}, LX/1Rb;-><init>()V

    invoke-interface {p3, v1}, LX/1Pu;->a(LX/1Rb;)V

    .line 1969209
    :cond_2
    iget-object v1, p0, LX/D8i;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-virtual {v1, v0, p3}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/2eG;LX/1Pr;)LX/2ed;

    move-result-object v1

    .line 1969210
    iget-object v2, p0, LX/D8i;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    iget-object v3, p0, LX/D8i;->e:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v2, v0, v1, v3}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/2eG;LX/2ed;Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    goto/16 :goto_0

    :cond_3
    new-instance v2, LX/D8h;

    invoke-direct {v2, p0, v3}, LX/D8h;-><init>(LX/D8i;LX/0Px;)V

    goto :goto_2

    .line 1969211
    :cond_4
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1969212
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/1VS;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 1969213
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be()LX/0Px;

    move-result-object v5

    if-nez v5, :cond_6

    :cond_5
    move-object v2, v3

    .line 1969214
    goto :goto_1

    .line 1969215
    :cond_6
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1969216
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->be()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v4

    :goto_3
    if-ge v3, v7, :cond_7

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLVideoAnnotation;

    .line 1969217
    new-instance p1, LX/D8l;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p4

    .line 1969218
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1969219
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-direct {p1, p4, v2, v4, v0}, LX/D8l;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;II)V

    invoke-virtual {v5, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1969220
    add-int/lit8 v4, v4, 0x1

    .line 1969221
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 1969222
    :cond_7
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_1
.end method
