.class public LX/ENq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1V0;

.field public final b:LX/1vg;

.field public final c:LX/EN3;

.field public final d:LX/ENu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/ENu",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/EPK;


# direct methods
.method public constructor <init>(LX/1V0;LX/1vg;LX/EN3;LX/ENu;LX/EPK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2113583
    iput-object p1, p0, LX/ENq;->a:LX/1V0;

    .line 2113584
    iput-object p2, p0, LX/ENq;->b:LX/1vg;

    .line 2113585
    iput-object p3, p0, LX/ENq;->c:LX/EN3;

    .line 2113586
    iput-object p4, p0, LX/ENq;->d:LX/ENu;

    .line 2113587
    iput-object p5, p0, LX/ENq;->e:LX/EPK;

    .line 2113588
    return-void
.end method

.method public static a(LX/0QB;)LX/ENq;
    .locals 9

    .prologue
    .line 2113571
    const-class v1, LX/ENq;

    monitor-enter v1

    .line 2113572
    :try_start_0
    sget-object v0, LX/ENq;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113573
    sput-object v2, LX/ENq;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113574
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113575
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113576
    new-instance v3, LX/ENq;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/EN3;->a(LX/0QB;)LX/EN3;

    move-result-object v6

    check-cast v6, LX/EN3;

    invoke-static {v0}, LX/ENu;->a(LX/0QB;)LX/ENu;

    move-result-object v7

    check-cast v7, LX/ENu;

    invoke-static {v0}, LX/EPK;->b(LX/0QB;)LX/EPK;

    move-result-object v8

    check-cast v8, LX/EPK;

    invoke-direct/range {v3 .. v8}, LX/ENq;-><init>(LX/1V0;LX/1vg;LX/EN3;LX/ENu;LX/EPK;)V

    .line 2113577
    move-object v0, v3

    .line 2113578
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113579
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ENq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113580
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113581
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
