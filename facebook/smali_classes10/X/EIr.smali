.class public LX/EIr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/5zm;


# direct methods
.method public constructor <init>(LX/0SG;LX/5zm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2102456
    iput-object p1, p0, LX/EIr;->a:LX/0SG;

    .line 2102457
    iput-object p2, p0, LX/EIr;->b:LX/5zm;

    .line 2102458
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2102459
    check-cast p1, Landroid/net/Uri;

    .line 2102460
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    .line 2102461
    iput-object p1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2102462
    move-object v0, v0

    .line 2102463
    sget-object v1, LX/2MK;->AUDIO:LX/2MK;

    .line 2102464
    iput-object v1, v0, LX/5zn;->c:LX/2MK;

    .line 2102465
    move-object v0, v0

    .line 2102466
    sget-object v1, LX/5zj;->AUDIO:LX/5zj;

    .line 2102467
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 2102468
    move-object v0, v0

    .line 2102469
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2102470
    iget-object v1, p0, LX/EIr;->b:LX/5zm;

    invoke-virtual {v1, v0}, LX/5zm;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zl;

    move-result-object v0

    .line 2102471
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2102472
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2102473
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "extension"

    const-string v5, "mp4"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2102474
    iget-object v3, p0, LX/EIr;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2102475
    new-instance v4, LX/4cQ;

    invoke-direct {v4, v3, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2102476
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v3, "custom_voicemail_create"

    .line 2102477
    iput-object v3, v0, LX/14O;->b:Ljava/lang/String;

    .line 2102478
    move-object v0, v0

    .line 2102479
    const-string v3, "POST"

    .line 2102480
    iput-object v3, v0, LX/14O;->c:Ljava/lang/String;

    .line 2102481
    move-object v0, v0

    .line 2102482
    const-string v3, "me/custom_voicemails"

    .line 2102483
    iput-object v3, v0, LX/14O;->d:Ljava/lang/String;

    .line 2102484
    move-object v0, v0

    .line 2102485
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2102486
    iput-object v2, v0, LX/14O;->g:Ljava/util/List;

    .line 2102487
    move-object v0, v0

    .line 2102488
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 2102489
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 2102490
    move-object v0, v0

    .line 2102491
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 2102492
    move-object v0, v0

    .line 2102493
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2102494
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
