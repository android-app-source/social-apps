.class public final LX/Et1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/CEz;

.field public final synthetic d:LX/Et2;


# direct methods
.method public constructor <init>(LX/Et2;Landroid/view/View;Ljava/lang/String;LX/CEz;)V
    .locals 0

    .prologue
    .line 2176990
    iput-object p1, p0, LX/Et1;->d:LX/Et2;

    iput-object p2, p0, LX/Et1;->a:Landroid/view/View;

    iput-object p3, p0, LX/Et1;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Et1;->c:LX/CEz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2176991
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2176992
    const v1, 0x7f0d2f04

    if-ne v0, v1, :cond_1

    .line 2176993
    iget-object v0, p0, LX/Et1;->d:LX/Et2;

    iget-object v0, v0, LX/Et2;->f:LX/Et3;

    iget-object v1, p0, LX/Et1;->d:LX/Et2;

    iget-object v1, v1, LX/Et2;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->A()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v1

    iget-object v2, p0, LX/Et1;->d:LX/Et2;

    iget-object v2, v2, LX/Et2;->c:LX/1Pm;

    iget-object v3, p0, LX/Et1;->d:LX/Et2;

    iget-object v3, v3, LX/Et2;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, LX/Et3;->a$redex0(LX/Et3;Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;LX/1Pm;Ljava/lang/String;)V

    .line 2176994
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2176995
    :cond_1
    const v1, 0x7f0d2f05

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Et1;->d:LX/Et2;

    iget-object v0, v0, LX/Et2;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Et1;->d:LX/Et2;

    iget-object v0, v0, LX/Et2;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    if-eqz v0, :cond_0

    .line 2176996
    iget-object v0, p0, LX/Et1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2176997
    iget-object v1, p0, LX/Et1;->d:LX/Et2;

    iget-object v1, v1, LX/Et2;->f:LX/Et3;

    iget-object v1, v1, LX/Et3;->g:LX/Es5;

    iget-object v2, p0, LX/Et1;->d:LX/Et2;

    iget-object v2, v2, LX/Et2;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iget-object v3, p0, LX/Et1;->b:Ljava/lang/String;

    iget-object v4, p0, LX/Et1;->c:LX/CEz;

    invoke-virtual {v1, v2, v0, v3, v4}, LX/Es5;->a(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Landroid/app/Activity;Ljava/lang/String;LX/CEz;)V

    goto :goto_0
.end method
