.class public final LX/D5Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D5X;


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V
    .locals 0

    .prologue
    .line 1963737
    iput-object p1, p0, LX/D5Z;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/D6B;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;)V
    .locals 3

    .prologue
    .line 1963720
    iget-object v0, p0, LX/D5Z;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    .line 1963721
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->aj:LX/3Qw;

    iget-object v1, v1, LX/3Qw;->p:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1963722
    :goto_0
    iget-object v0, p0, LX/D5Z;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    .line 1963723
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ap:LX/D5D;

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->k()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->k()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->k()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1963724
    :cond_0
    :goto_1
    iget-object v0, p0, LX/D5Z;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedRootView;LX/D6B;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;Z)V

    .line 1963725
    iget-object v0, p0, LX/D5Z;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->t(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V

    .line 1963726
    return-void

    .line 1963727
    :cond_1
    const-string v1, ""

    .line 1963728
    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1963729
    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1963730
    :cond_2
    iget-object v2, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->M:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    invoke-virtual {v2, v1}, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->setTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 1963731
    :cond_3
    iget-object v1, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ap:LX/D5D;

    invoke-virtual {p2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;->k()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x1

    .line 1963732
    iput-boolean v0, v1, LX/D5D;->e:Z

    .line 1963733
    if-gt v2, v0, :cond_4

    .line 1963734
    invoke-static {v1}, LX/D5D;->h(LX/D5D;)V

    .line 1963735
    :goto_2
    goto :goto_1

    .line 1963736
    :cond_4
    invoke-static {v1}, LX/D5D;->i(LX/D5D;)V

    goto :goto_2
.end method

.method public final a(LX/D6B;Ljava/lang/String;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;)V
    .locals 1

    .prologue
    .line 1963718
    iget-object v0, p0, LX/D5Z;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-static {v0, p2, p3}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Ljava/lang/String;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;)V

    .line 1963719
    return-void
.end method
