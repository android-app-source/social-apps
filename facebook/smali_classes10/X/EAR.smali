.class public final LX/EAR;
.super LX/ChG;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/EA5;

.field public final synthetic c:LX/EAW;

.field public final synthetic d:LX/EA0;

.field public final synthetic e:LX/EAV;


# direct methods
.method public constructor <init>(LX/EAV;Ljava/lang/String;LX/EA5;LX/EAW;LX/EA0;)V
    .locals 0

    .prologue
    .line 2085545
    iput-object p1, p0, LX/EAR;->e:LX/EAV;

    iput-object p2, p0, LX/EAR;->a:Ljava/lang/String;

    iput-object p3, p0, LX/EAR;->b:LX/EA5;

    iput-object p4, p0, LX/EAR;->c:LX/EAW;

    iput-object p5, p0, LX/EAR;->d:LX/EA0;

    invoke-direct {p0}, LX/ChG;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/5tj;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 9
    .param p2    # LX/5tj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2085546
    iget-object v0, p0, LX/EAR;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2085547
    :goto_0
    return-void

    .line 2085548
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2085549
    iget-object v0, p0, LX/EAR;->e:LX/EAV;

    iget-object v1, p0, LX/EAR;->b:LX/EA5;

    iget-object v2, p0, LX/EAR;->c:LX/EAW;

    iget-object v3, p0, LX/EAR;->d:LX/EA0;

    .line 2085550
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    const-string v5, "New story argument must have an id"

    invoke-static {v4, v5}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2085551
    invoke-static {p3}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v4

    iget-object v5, v0, LX/EAV;->d:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    .line 2085552
    iput-wide v6, v4, LX/23u;->G:J

    .line 2085553
    move-object v4, v4

    .line 2085554
    invoke-virtual {v4}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 2085555
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    .line 2085556
    iget-object v6, v2, LX/EAW;->c:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    move v5, v6

    .line 2085557
    if-eqz v5, :cond_3

    .line 2085558
    invoke-virtual {v2, v4}, LX/EAW;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2085559
    :goto_2
    invoke-virtual {v1}, LX/1Qj;->iN_()V

    .line 2085560
    goto :goto_0

    .line 2085561
    :cond_1
    iget-object v0, p0, LX/EAR;->d:LX/EA0;

    .line 2085562
    invoke-virtual {v0}, LX/EA0;->f()V

    .line 2085563
    goto :goto_0

    .line 2085564
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 2085565
    :cond_3
    iget-object v5, v2, LX/EAW;->b:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2085566
    invoke-static {v2}, LX/EAW;->b(LX/EAW;)V

    .line 2085567
    invoke-static {v0, v4, v1, v2}, LX/EAV;->a(LX/EAV;Lcom/facebook/graphql/model/GraphQLStory;LX/EA5;LX/EAW;)V

    .line 2085568
    invoke-virtual {v3}, LX/EA0;->g()V

    goto :goto_2
.end method
