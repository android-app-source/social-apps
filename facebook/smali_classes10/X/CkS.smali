.class public final LX/CkS;
.super LX/CkO;
.source ""


# instance fields
.field public a:I

.field public b:LX/Ckm;

.field public h:LX/Ckm;

.field public i:LX/Ckd;


# direct methods
.method public constructor <init>(LX/Ckb;LX/15i;I)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1931004
    invoke-direct {p0, p1, p2, p3}, LX/CkO;-><init>(LX/Ckb;LX/15i;I)V

    .line 1931005
    if-nez p3, :cond_1

    .line 1931006
    :cond_0
    :goto_0
    return-void

    .line 1931007
    :cond_1
    const/4 v0, 0x7

    invoke-virtual {p2, p3, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CkY;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/CkS;->a:I

    .line 1931008
    new-instance v0, LX/Ckm;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p2, p3}, LX/Ckm;-><init>(ZLX/15i;I)V

    move-object v0, v0

    .line 1931009
    iput-object v0, p0, LX/CkS;->b:LX/Ckm;

    .line 1931010
    new-instance v0, LX/Ckm;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p2, p3}, LX/Ckm;-><init>(ZLX/15i;I)V

    move-object v0, v0

    .line 1931011
    iput-object v0, p0, LX/CkS;->h:LX/Ckm;

    .line 1931012
    const/16 v0, 0xb

    invoke-virtual {p2, p3, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1931013
    if-eqz v0, :cond_2

    const-string v1, "center"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1931014
    sget-object v0, LX/Ckd;->CENTER:LX/Ckd;

    iput-object v0, p0, LX/CkS;->i:LX/Ckd;

    goto :goto_0

    .line 1931015
    :cond_2
    if-eqz v0, :cond_0

    const-string v1, "right"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1931016
    sget-object v0, LX/Ckd;->RIGHT:LX/Ckd;

    iput-object v0, p0, LX/CkS;->i:LX/Ckd;

    goto :goto_0
.end method
