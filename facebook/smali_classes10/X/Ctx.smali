.class public LX/Ctx;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/CsO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 1

    .prologue
    .line 1945815
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1945816
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Ctx;

    invoke-static {v0}, LX/CsO;->a(LX/0QB;)LX/CsO;

    move-result-object v0

    check-cast v0, LX/CsO;

    iput-object v0, p0, LX/Ctx;->a:LX/CsO;

    .line 1945817
    return-void
.end method


# virtual methods
.method public final a(LX/Cqw;)V
    .locals 2

    .prologue
    .line 1945818
    invoke-super {p0, p1}, LX/Cts;->a(LX/Cqw;)V

    .line 1945819
    sget-object v0, LX/Cqw;->b:LX/Cqw;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/Cqw;->a:LX/Cqw;

    if-ne p1, v0, :cond_1

    .line 1945820
    :cond_0
    iget-object v0, p0, LX/Ctx;->a:LX/CsO;

    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CsO;->a(Landroid/view/View;)V

    .line 1945821
    :cond_1
    return-void
.end method

.method public final a(LX/Crd;)Z
    .locals 2

    .prologue
    .line 1945822
    sget-object v0, LX/Crd;->CLICK_MEDIA:LX/Crd;

    if-ne p1, v0, :cond_0

    .line 1945823
    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v0

    sget-object v1, LX/Cqw;->b:LX/Cqw;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1945824
    if-eqz v0, :cond_0

    .line 1945825
    iget-object v0, p0, LX/Ctx;->a:LX/CsO;

    invoke-virtual {v0}, LX/CsO;->a()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1945826
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1945827
    sget-object v1, LX/Cqw;->b:LX/Cqw;

    invoke-interface {v0, v1}, LX/Ctg;->a(LX/Cqw;)V

    .line 1945828
    iget-object v0, p0, LX/Ctx;->a:LX/CsO;

    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CsO;->a(Landroid/view/View;)V

    .line 1945829
    const/4 v0, 0x1

    .line 1945830
    :goto_1
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/Cts;->a(LX/Crd;)Z

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
