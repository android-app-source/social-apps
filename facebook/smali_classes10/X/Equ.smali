.class public LX/Equ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:LX/2RQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesTokenQueryModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesTokenQueryModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Ljava/util/List",
            "<*>;>;"
        }
    .end annotation
.end field

.field public e:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Ljava/util/List",
            "<*>;>;"
        }
    .end annotation
.end field

.field public f:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesTokenQueryModel;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;>;"
        }
    .end annotation
.end field

.field public h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesTokenQueryModel;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public m:Z

.field public n:LX/Eqg;

.field public o:LX/0v6;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public final r:LX/2RC;

.field public final s:LX/3Oq;

.field public final t:Ljava/util/concurrent/ExecutorService;

.field public final u:LX/03V;

.field public final v:LX/0tX;

.field public final w:LX/Era;

.field public final x:LX/ErU;

.field public final y:LX/0TD;

.field public final z:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2172273
    const-class v0, LX/Equ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Equ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2RC;LX/3Oq;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0tX;LX/Era;LX/ErU;LX/0TD;LX/1Ck;Ljava/lang/String;LX/Eqg;)V
    .locals 2
    .param p2    # LX/3Oq;
        .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p8    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/Eqg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2172259
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Equ;->m:Z

    .line 2172260
    iput-object p1, p0, LX/Equ;->r:LX/2RC;

    .line 2172261
    iput-object p2, p0, LX/Equ;->s:LX/3Oq;

    .line 2172262
    iput-object p3, p0, LX/Equ;->t:Ljava/util/concurrent/ExecutorService;

    .line 2172263
    iput-object p4, p0, LX/Equ;->u:LX/03V;

    .line 2172264
    iput-object p5, p0, LX/Equ;->v:LX/0tX;

    .line 2172265
    iput-object p6, p0, LX/Equ;->w:LX/Era;

    .line 2172266
    iput-object p7, p0, LX/Equ;->x:LX/ErU;

    .line 2172267
    iput-object p8, p0, LX/Equ;->y:LX/0TD;

    .line 2172268
    iput-object p9, p0, LX/Equ;->z:LX/1Ck;

    .line 2172269
    new-instance v0, LX/0v6;

    const-string v1, "EventsExtendedInviteFriendsFetcher"

    invoke-direct {v0, v1}, LX/0v6;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/Equ;->o:LX/0v6;

    .line 2172270
    iput-object p10, p0, LX/Equ;->q:Ljava/lang/String;

    .line 2172271
    iput-object p11, p0, LX/Equ;->n:LX/Eqg;

    .line 2172272
    return-void
.end method

.method public static a(LX/0Vd;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 1

    .prologue
    .line 2172253
    if-eqz p0, :cond_0

    .line 2172254
    invoke-virtual {p0}, LX/0Vd;->dispose()V

    .line 2172255
    :cond_0
    if-eqz p1, :cond_1

    .line 2172256
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2172257
    :cond_1
    return-void
.end method

.method public static b(LX/2uF;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2uF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2172248
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2172249
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v9, v0, LX/1vs;->b:I

    .line 2172250
    new-instance v0, Lcom/facebook/events/invite/EventsExtendedInviteUserToken;

    const/4 v1, 0x6

    invoke-virtual {v6, v9, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    new-instance v2, Lcom/facebook/user/model/Name;

    const/4 v3, 0x5

    invoke-virtual {v6, v9, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-virtual {v6, v9, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v6, v9, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v6, v9, v5}, LX/15i;->h(II)Z

    move-result v5

    const/4 v10, 0x1

    invoke-virtual {v6, v9, v10}, LX/15i;->h(II)Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/events/invite/EventsExtendedInviteUserToken;-><init>(Lcom/facebook/user/model/UserKey;Lcom/facebook/user/model/Name;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 2172251
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2172252
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/Equ;)LX/0zO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zO",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2172243
    new-instance v0, LX/7nu;

    invoke-direct {v0}, LX/7nu;-><init>()V

    move-object v0, v0

    .line 2172244
    const-string v1, "after_cursor"

    iget-object v2, p0, LX/Equ;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2172245
    const-string v1, "event_id"

    iget-object v2, p0, LX/Equ;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2172246
    const-string v1, "first_count"

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2172247
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/Equ;)LX/0Vd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Vd",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableContactsQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2172242
    new-instance v0, LX/Eqs;

    invoke-direct {v0, p0}, LX/Eqs;-><init>(LX/Equ;)V

    return-object v0
.end method

.method public static i(LX/Equ;)LX/0Vd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Vd",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesTokenQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2172241
    new-instance v0, LX/Eqt;

    invoke-direct {v0, p0}, LX/Eqt;-><init>(LX/Equ;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2172238
    iget-boolean v0, p0, LX/Equ;->m:Z

    if-nez v0, :cond_0

    .line 2172239
    :goto_0
    return-void

    .line 2172240
    :cond_0
    iget-object v0, p0, LX/Equ;->z:LX/1Ck;

    const-string v1, "FETCH_CONTACTS_TASK"

    new-instance v2, LX/Eqn;

    invoke-direct {v2, p0}, LX/Eqn;-><init>(LX/Equ;)V

    invoke-static {p0}, LX/Equ;->h(LX/Equ;)LX/0Vd;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method
