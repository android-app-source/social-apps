.class public final LX/DxN;
.super LX/DvC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;)V
    .locals 0

    .prologue
    .line 2062749
    iput-object p1, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    invoke-direct {p0}, LX/DvC;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 2062750
    check-cast p1, LX/DvB;

    .line 2062751
    if-eqz p1, :cond_1

    iget-object v0, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->n:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->n:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget-object v1, p1, LX/DvB;->c:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p1, LX/DvB;->d:LX/DvW;

    sget-object v1, LX/DvW;->UPLOADED_MEDIA_SET:LX/DvW;

    if-eq v0, v1, :cond_2

    .line 2062752
    :cond_1
    :goto_0
    return-void

    .line 2062753
    :cond_2
    new-instance v6, Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    sget-object v0, LX/3WA;->USER_INITIATED:LX/3WA;

    iget-object v1, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v1, v1, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v6, v0, v1}, Lcom/facebook/photos/base/photos/PhotoFetchInfo;-><init>(LX/3WA;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2062754
    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2062755
    iget-boolean v1, v0, LX/Dxa;->a:Z

    move v0, v1

    .line 2062756
    if-eqz v0, :cond_3

    .line 2062757
    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p1, LX/DvB;->b:Landroid/net/Uri;

    iget-object v5, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;Z)V

    goto :goto_0

    .line 2062758
    :cond_3
    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2062759
    iget-boolean v1, v0, LX/Dxa;->b:Z

    move v0, v1

    .line 2062760
    if-eqz v0, :cond_4

    .line 2062761
    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p1, LX/DvB;->b:Landroid/net/Uri;

    iget-object v5, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    const/4 v7, 0x1

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;Z)V

    goto :goto_0

    .line 2062762
    :cond_4
    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->o:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v0, :cond_6

    iget-object v0, p1, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_6

    .line 2062763
    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->o:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2062764
    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v1, p1, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    iget-object v2, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v3, v3, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;Landroid/support/v4/app/FragmentActivity;J)V

    goto/16 :goto_0

    .line 2062765
    :cond_5
    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->o:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2062766
    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v1, p1, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    iget-object v2, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v2, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->o:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2062767
    iget-object v3, v2, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->c:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-object v2, v3

    .line 2062768
    iget-object v3, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 2062769
    :cond_6
    iget-object v0, p0, LX/DxN;->a:Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    iget-object v1, p1, LX/DvB;->a:Ljava/lang/String;

    iget-object v2, p1, LX/DvB;->b:Landroid/net/Uri;

    .line 2062770
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    if-nez v3, :cond_8

    .line 2062771
    :cond_7
    :goto_1
    goto/16 :goto_0

    .line 2062772
    :cond_8
    invoke-virtual {v0, v1, v2}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_1
.end method
