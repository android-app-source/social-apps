.class public LX/Cnf;
.super LX/CnT;
.source ""

# interfaces
.implements LX/02k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/InlineEmailCtaBlockView;",
        "Lcom/facebook/richdocument/model/data/InlineEmailCtaBlockData;",
        ">;",
        "LX/02k;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;)V
    .locals 0

    .prologue
    .line 1934136
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934137
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 10

    .prologue
    .line 1934138
    check-cast p1, LX/CmH;

    .line 1934139
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934140
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    .line 1934141
    iget-object v1, p1, LX/CmH;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1934142
    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->F:Ljava/lang/String;

    .line 1934143
    iget-object v0, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934144
    if-eqz v0, :cond_2

    .line 1934145
    iget-object v0, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934146
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1934147
    iget-object v0, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934148
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1934149
    iget-object v0, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934150
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$ContextPageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1934151
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934152
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    .line 1934153
    iget-object v1, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v1, v1

    .line 1934154
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$ContextPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$ContextPageModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1934155
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934156
    iget-object v0, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v0

    .line 1934157
    invoke-virtual {v2, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934158
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934159
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    .line 1934160
    iget-object v1, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v1, v1

    .line 1934161
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$ContextPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$ContextPageModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1934162
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934163
    iget-object v0, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v0

    .line 1934164
    invoke-virtual {v2, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934165
    :cond_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934166
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    .line 1934167
    iget-object v1, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v1, v1

    .line 1934168
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 1934169
    iget-object v2, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v2, v2

    .line 1934170
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->et_()Ljava/lang/String;

    move-result-object v2

    .line 1934171
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f081c78

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1934172
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081c79

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1934173
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1934174
    new-instance v6, LX/Cp0;

    invoke-direct {v6, v0, v1}, LX/Cp0;-><init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;Ljava/lang/String;)V

    .line 1934175
    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v5}, Landroid/text/SpannableString;->length()I

    move-result v8

    const/16 v9, 0x21

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1934176
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a00d2

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v5}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v3, v4, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1934177
    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934178
    iget-object v4, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v3, v4

    .line 1934179
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/CtG;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1934180
    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934181
    iget-object v4, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v3, v4

    .line 1934182
    invoke-virtual {v3, v5}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934183
    iget-object v0, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934184
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$InfoFieldsDataModel;

    .line 1934185
    iget-object v1, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v1

    .line 1934186
    check-cast v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$InfoFieldsDataModel;->a()LX/0Px;

    move-result-object v0

    .line 1934187
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/8ba;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->E:Ljava/lang/String;

    .line 1934188
    iget-object v2, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934189
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1934190
    iget-object v3, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->E:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934191
    iget-object v2, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->p:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    new-instance v3, LX/Cp3;

    invoke-direct {v3, v1, v0}, LX/Cp3;-><init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;LX/0Px;)V

    invoke-virtual {v2, v3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1934192
    iget-object v0, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934193
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1934194
    iget-object v0, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934195
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1934196
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934197
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    .line 1934198
    iget-object v1, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v1, v1

    .line 1934199
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1934200
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->C:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1934201
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->C:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1934202
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->D:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1934203
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://graph.facebook.com/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->D:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/picture?width=100&height=100"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1934204
    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->C:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1934205
    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->C:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1934206
    :cond_1
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934207
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    .line 1934208
    iget-object v1, p1, LX/CmH;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v1, v1

    .line 1934209
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1934210
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934211
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1934212
    invoke-virtual {v2, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934213
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081c77

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->E:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1934214
    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->w:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934215
    iget-object v0, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v3, v0

    .line 1934216
    invoke-virtual {v3, v2}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934217
    :cond_2
    return-void
.end method
