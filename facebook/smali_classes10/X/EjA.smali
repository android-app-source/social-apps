.class public LX/EjA;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/EjA;


# direct methods
.method public constructor <init>()V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2160921
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2160922
    const-string v0, "confirmAccount/?normalized_contactpoint={%s}&contactpoint_type={%s}&conf_surface={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2160923
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&iso_country_code={%s}&for_phone_number_confirmation={%s}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&quick_promotion_id={%s}&quick_promotion_type={%s}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "normalized"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "conf_surface"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "country"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "phone_number"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "quick_promotion"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "quick_promotion_type"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ej9;

    invoke-direct {v2}, LX/Ej9;-><init>()V

    invoke-virtual {p0, v1, v2}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2160924
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&iso_country_code={%s}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "normalized"

    const-string v3, "type"

    const-string v4, "conf_surface"

    const-string v5, "country"

    invoke-static {v1, v2, v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ej9;

    invoke-direct {v2}, LX/Ej9;-><init>()V

    invoke-virtual {p0, v1, v2}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2160925
    const-string v1, "normalized"

    const-string v2, "type"

    const-string v3, "conf_surface"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/Ej9;

    invoke-direct {v1}, LX/Ej9;-><init>()V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2160926
    return-void
.end method

.method public static a(LX/0QB;)LX/EjA;
    .locals 3

    .prologue
    .line 2160927
    sget-object v0, LX/EjA;->a:LX/EjA;

    if-nez v0, :cond_1

    .line 2160928
    const-class v1, LX/EjA;

    monitor-enter v1

    .line 2160929
    :try_start_0
    sget-object v0, LX/EjA;->a:LX/EjA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2160930
    if-eqz v2, :cond_0

    .line 2160931
    :try_start_1
    new-instance v0, LX/EjA;

    invoke-direct {v0}, LX/EjA;-><init>()V

    .line 2160932
    move-object v0, v0

    .line 2160933
    sput-object v0, LX/EjA;->a:LX/EjA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2160934
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2160935
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2160936
    :cond_1
    sget-object v0, LX/EjA;->a:LX/EjA;

    return-object v0

    .line 2160937
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2160938
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
