.class public final LX/E40;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;

.field public final synthetic b:LX/2km;

.field public final synthetic c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2075705
    iput-object p1, p0, LX/E40;->d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;

    iput-object p2, p0, LX/E40;->a:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;

    iput-object p3, p0, LX/E40;->b:LX/2km;

    iput-object p4, p0, LX/E40;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x4d3c35a1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2075706
    iget-object v0, p0, LX/E40;->a:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;->c()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;

    move-result-object v0

    .line 2075707
    iget-object v2, p0, LX/E40;->d:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;

    iget-object v2, v2, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoUnitComponentPartDefinition;->d:LX/E1i;

    iget-object v3, p0, LX/E40;->a:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/E40;->a:Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitPhotoComponentFragmentModel$PhotoModel$CreationStoryModel$FeedbackModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-object v5, LX/Cfc;->PHOTO_TAP:LX/Cfc;

    invoke-virtual {v2, v3, v4, v0, v5}, LX/E1i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    .line 2075708
    iget-object v2, p0, LX/E40;->b:LX/2km;

    iget-object v3, p0, LX/E40;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075709
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v4

    .line 2075710
    iget-object v4, p0, LX/E40;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075711
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2075712
    invoke-interface {v2, v3, v4, v0}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2075713
    const v0, -0x144c465b

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2075714
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
