.class public final LX/E9t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/5tj;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/E9x;


# direct methods
.method public constructor <init>(LX/E9x;Landroid/view/View;LX/5tj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2084931
    iput-object p1, p0, LX/E9t;->e:LX/E9x;

    iput-object p2, p0, LX/E9t;->a:Landroid/view/View;

    iput-object p3, p0, LX/E9t;->b:LX/5tj;

    iput-object p4, p0, LX/E9t;->c:Ljava/lang/String;

    iput-object p5, p0, LX/E9t;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 2084926
    iget-object v0, p0, LX/E9t;->e:LX/E9x;

    iget-object v1, p0, LX/E9t;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/E9t;->b:LX/5tj;

    iget-object v3, p0, LX/E9t;->c:Ljava/lang/String;

    iget-object v4, p0, LX/E9t;->d:Ljava/lang/String;

    .line 2084927
    iget-object v5, v0, LX/E9x;->g:LX/79D;

    invoke-static {v2}, LX/BNJ;->b(LX/5ti;)Ljava/lang/String;

    move-result-object v6

    sget-object p0, LX/79C;->REVIEW_REPORT_MENU_OPTION:LX/79C;

    invoke-virtual {v5, v4, v3, v6, p0}, LX/79D;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/79C;)V

    .line 2084928
    sget-object v5, LX/0ax;->dv:Ljava/lang/String;

    invoke-interface {v2}, LX/5tj;->bh_()LX/1VU;

    move-result-object v6

    invoke-interface {v6}, LX/1VU;->k()Ljava/lang/String;

    move-result-object v6

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string p1, "page_review"

    invoke-static {v5, v6, p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2084929
    iget-object v6, v0, LX/E9x;->a:LX/17W;

    invoke-virtual {v6, v1, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2084930
    const/4 v0, 0x1

    return v0
.end method
