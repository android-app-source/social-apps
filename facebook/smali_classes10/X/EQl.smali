.class public LX/EQl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/EQl;


# instance fields
.field public final a:LX/0tX;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Landroid/content/Context;

.field public final d:LX/03V;

.field private final e:LX/1Vr;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;Landroid/content/Context;LX/03V;LX/1Vg;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2119162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2119163
    iput-object p1, p0, LX/EQl;->a:LX/0tX;

    .line 2119164
    iput-object p2, p0, LX/EQl;->b:Ljava/util/concurrent/ExecutorService;

    .line 2119165
    iput-object p3, p0, LX/EQl;->c:Landroid/content/Context;

    .line 2119166
    iput-object p4, p0, LX/EQl;->d:LX/03V;

    .line 2119167
    new-instance v0, LX/1Wx;

    invoke-direct {v0}, LX/1Wx;-><init>()V

    invoke-virtual {p5, v0}, LX/1Vg;->a(LX/1Vq;)LX/1Vr;

    move-result-object v0

    iput-object v0, p0, LX/EQl;->e:LX/1Vr;

    .line 2119168
    return-void
.end method

.method public static a(LX/0QB;)LX/EQl;
    .locals 9

    .prologue
    .line 2119169
    sget-object v0, LX/EQl;->f:LX/EQl;

    if-nez v0, :cond_1

    .line 2119170
    const-class v1, LX/EQl;

    monitor-enter v1

    .line 2119171
    :try_start_0
    sget-object v0, LX/EQl;->f:LX/EQl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2119172
    if-eqz v2, :cond_0

    .line 2119173
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2119174
    new-instance v3, LX/EQl;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    const-class v8, LX/1Vg;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1Vg;

    invoke-direct/range {v3 .. v8}, LX/EQl;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;Landroid/content/Context;LX/03V;LX/1Vg;)V

    .line 2119175
    move-object v0, v3

    .line 2119176
    sput-object v0, LX/EQl;->f:LX/EQl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2119177
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2119178
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2119179
    :cond_1
    sget-object v0, LX/EQl;->f:LX/EQl;

    return-object v0

    .line 2119180
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2119181
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;Z)Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;
    .locals 13
    .param p0    # Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 2119182
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2119183
    :cond_0
    :goto_0
    return-object v2

    .line 2119184
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v6

    .line 2119185
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kG_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    move v0, v1

    :goto_4
    if-nez v0, :cond_2

    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->c()LX/174;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->c()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_2
    move v0, v1

    :goto_5
    if-eqz v0, :cond_a

    move v0, v1

    :goto_6
    if-nez v0, :cond_0

    .line 2119186
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->n()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v8, v0, LX/1vs;->b:I

    .line 2119187
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget-object v9, v0, LX/1vs;->a:LX/15i;

    iget v10, v0, LX/1vs;->b:I

    .line 2119188
    if-eqz p1, :cond_b

    .line 2119189
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2119190
    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2119191
    :goto_7
    if-eqz p1, :cond_c

    .line 2119192
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2119193
    invoke-virtual {v3, v0, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2119194
    :goto_8
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_d

    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l()LX/1vs;

    move-result-object v3

    iget-object v11, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v11, v3, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2119195
    :goto_9
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_e

    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->l()LX/1vs;

    move-result-object v3

    iget-object v11, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v11, v3, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2119196
    :goto_a
    new-instance v11, LX/ER5;

    invoke-direct {v11}, LX/ER5;-><init>()V

    move-object v11, v11

    .line 2119197
    invoke-virtual {v7, v8, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    .line 2119198
    iput-object v7, v11, LX/ER5;->a:Ljava/lang/String;

    .line 2119199
    move-object v7, v11

    .line 2119200
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->c()LX/174;

    move-result-object v8

    invoke-interface {v8}, LX/174;->a()Ljava/lang/String;

    move-result-object v8

    .line 2119201
    iput-object v8, v7, LX/ER5;->b:Ljava/lang/String;

    .line 2119202
    move-object v7, v7

    .line 2119203
    invoke-virtual {v9, v10, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2119204
    iput-object v5, v7, LX/ER5;->c:Ljava/lang/String;

    .line 2119205
    move-object v5, v7

    .line 2119206
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kH_()LX/174;

    move-result-object v7

    if-nez v7, :cond_f

    .line 2119207
    :goto_b
    iput-object v2, v5, LX/ER5;->d:Ljava/lang/String;

    .line 2119208
    move-object v2, v5

    .line 2119209
    const/4 v8, 0x0

    .line 2119210
    if-nez v6, :cond_10

    .line 2119211
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 2119212
    :goto_c
    move-object v5, v5

    .line 2119213
    iput-object v5, v2, LX/ER5;->e:Ljava/util/List;

    .line 2119214
    move-object v2, v2

    .line 2119215
    iput-object v4, v2, LX/ER5;->f:Ljava/lang/String;

    .line 2119216
    move-object v2, v2

    .line 2119217
    iput-object v0, v2, LX/ER5;->g:Ljava/lang/String;

    .line 2119218
    move-object v0, v2

    .line 2119219
    iput-object v1, v0, LX/ER5;->h:Ljava/lang/String;

    .line 2119220
    move-object v0, v0

    .line 2119221
    iput-object v3, v0, LX/ER5;->i:Ljava/lang/String;

    .line 2119222
    move-object v0, v0

    .line 2119223
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->d()Z

    move-result v1

    .line 2119224
    iput-boolean v1, v0, LX/ER5;->j:Z

    .line 2119225
    move-object v0, v0

    .line 2119226
    new-instance v1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    invoke-direct {v1, v0}, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;-><init>(LX/ER5;)V

    move-object v2, v1

    .line 2119227
    goto/16 :goto_0

    .line 2119228
    :cond_3
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kG_()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2119229
    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2119230
    :cond_4
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kG_()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2119231
    invoke-virtual {v3, v0, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 2119232
    :cond_5
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2119233
    if-nez v0, :cond_6

    move v0, v1

    goto/16 :goto_3

    :cond_6
    move v0, v5

    goto/16 :goto_3

    .line 2119234
    :cond_7
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->n()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2119235
    invoke-virtual {v3, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_4

    .line 2119236
    :cond_8
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2119237
    if-nez v0, :cond_9

    move v0, v1

    goto/16 :goto_5

    :cond_9
    move v0, v5

    goto/16 :goto_5

    .line 2119238
    :cond_a
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2119239
    invoke-virtual {v3, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_6

    .line 2119240
    :cond_b
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kG_()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2119241
    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto/16 :goto_7

    .line 2119242
    :cond_c
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kG_()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2119243
    invoke-virtual {v3, v0, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    :cond_d
    move-object v1, v2

    .line 2119244
    goto/16 :goto_9

    :cond_e
    move-object v3, v2

    .line 2119245
    goto/16 :goto_a

    .line 2119246
    :cond_f
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->kH_()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_b

    .line 2119247
    :cond_10
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 2119248
    invoke-virtual {v6}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->k()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v9, v8

    .line 2119249
    :goto_d
    if-ge v9, v12, :cond_14

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$FacepileUsersModel;

    .line 2119250
    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$FacepileUsersModel;->b()LX/1vs;

    move-result-object v7

    iget v7, v7, LX/1vs;->b:I

    if-eqz v7, :cond_13

    .line 2119251
    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$FacepileUsersModel;->b()LX/1vs;

    move-result-object v7

    iget-object p0, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    .line 2119252
    invoke-virtual {p0, v7, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_12

    const/4 v7, 0x1

    :goto_e
    if-eqz v7, :cond_11

    .line 2119253
    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel$FacepileUsersModel;->b()LX/1vs;

    move-result-object v5

    iget-object v7, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-virtual {v7, v5, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2119254
    :cond_11
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    goto :goto_d

    :cond_12
    move v7, v8

    .line 2119255
    goto :goto_e

    :cond_13
    move v7, v8

    goto :goto_e

    .line 2119256
    :cond_14
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    goto/16 :goto_c
.end method

.method public static c(LX/EQl;)Z
    .locals 1

    .prologue
    .line 2119257
    iget-object v0, p0, LX/EQl;->e:LX/1Vr;

    invoke-virtual {v0}, LX/1Vr;->a()Z

    move-result v0

    return v0
.end method
