.class public final LX/EUb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J

.field public e:J

.field public f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2126242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2126243
    return-void
.end method

.method public constructor <init>(LX/EUc;)V
    .locals 2

    .prologue
    .line 2126244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2126245
    iget v0, p1, LX/EUc;->a:I

    iput v0, p0, LX/EUb;->a:I

    .line 2126246
    iget v0, p1, LX/EUc;->b:I

    iput v0, p0, LX/EUb;->b:I

    .line 2126247
    iget v0, p1, LX/EUc;->c:I

    iput v0, p0, LX/EUb;->c:I

    .line 2126248
    iget-wide v0, p1, LX/EUc;->d:J

    iput-wide v0, p0, LX/EUb;->d:J

    .line 2126249
    iget-wide v0, p1, LX/EUc;->e:J

    iput-wide v0, p0, LX/EUb;->e:J

    .line 2126250
    iget-object v0, p1, LX/EUc;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    iput-object v0, p0, LX/EUb;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 2126251
    return-void
.end method


# virtual methods
.method public final a()LX/EUc;
    .locals 9

    .prologue
    .line 2126252
    new-instance v0, LX/EUc;

    iget v1, p0, LX/EUb;->a:I

    iget v2, p0, LX/EUb;->b:I

    iget v3, p0, LX/EUb;->c:I

    iget-wide v4, p0, LX/EUb;->d:J

    iget-wide v6, p0, LX/EUb;->e:J

    iget-object v8, p0, LX/EUb;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    invoke-direct/range {v0 .. v8}, LX/EUc;-><init>(IIIJJLcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;)V

    return-object v0
.end method
