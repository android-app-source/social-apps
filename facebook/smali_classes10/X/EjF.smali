.class public final LX/EjF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/0Pz;

.field public final synthetic d:I

.field public final synthetic e:LX/30Z;


# direct methods
.method public constructor <init>(LX/30Z;ZLjava/lang/String;LX/0Pz;I)V
    .locals 0

    .prologue
    .line 2161049
    iput-object p1, p0, LX/EjF;->e:LX/30Z;

    iput-boolean p2, p0, LX/EjF;->a:Z

    iput-object p3, p0, LX/EjF;->b:Ljava/lang/String;

    iput-object p4, p0, LX/EjF;->c:LX/0Pz;

    iput p5, p0, LX/EjF;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 10

    .prologue
    .line 2161036
    iget-boolean v0, p0, LX/EjF;->a:Z

    if-eqz v0, :cond_0

    .line 2161037
    iget-object v0, p0, LX/EjF;->e:LX/30Z;

    iget-object v0, v0, LX/30Z;->f:LX/30e;

    invoke-virtual {v0}, LX/30e;->i()V

    .line 2161038
    :cond_0
    iget-object v0, p0, LX/EjF;->e:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    const-string v1, "create_session_fail"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, LX/EjF;->a:Z

    iget-object v4, p0, LX/EjF;->e:LX/30Z;

    iget-wide v4, v4, LX/30Z;->M:J

    iget-object v6, p0, LX/EjF;->e:LX/30Z;

    iget-object v6, v6, LX/30Z;->h:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    iget-object v8, p0, LX/EjF;->e:LX/30Z;

    iget-wide v8, v8, LX/30Z;->O:J

    sub-long/2addr v6, v8

    iget-object v8, p0, LX/EjF;->e:LX/30Z;

    iget v8, v8, LX/30Z;->N:I

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, LX/30d;->a(Ljava/lang/String;Ljava/lang/String;ZJJILjava/lang/String;)V

    .line 2161039
    iget-object v0, p0, LX/EjF;->e:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    sget-object v1, LX/Ejs;->CREATE_SESSION_FAILURE:LX/Ejs;

    invoke-virtual {v1}, LX/Ejs;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/30d;->c(Ljava/lang/String;)V

    .line 2161040
    iget-object v0, p0, LX/EjF;->e:LX/30Z;

    iget-object v0, v0, LX/30Z;->g:LX/30d;

    invoke-virtual {v0}, LX/30d;->b()V

    .line 2161041
    iget-object v0, p0, LX/EjF;->e:LX/30Z;

    const/4 v1, 0x0

    .line 2161042
    iput-boolean v1, v0, LX/30Z;->C:Z

    .line 2161043
    iget-object v0, p0, LX/EjF;->e:LX/30Z;

    iget-object v0, v0, LX/30Z;->z:LX/EjP;

    invoke-virtual {v0}, LX/EjP;->d()V

    .line 2161044
    iget-object v0, p0, LX/EjF;->e:LX/30Z;

    iget-object v0, v0, LX/30Z;->A:LX/Ejw;

    invoke-virtual {v0}, LX/2TZ;->close()V

    .line 2161045
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2161046
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2161047
    iget-object v0, p0, LX/EjF;->e:LX/30Z;

    iget-boolean v2, p0, LX/EjF;->a:Z

    iget-object v3, p0, LX/EjF;->b:Ljava/lang/String;

    iget-object v1, p0, LX/EjF;->c:LX/0Pz;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    iget v5, p0, LX/EjF;->d:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/30Z;->a(Lcom/facebook/graphql/executor/GraphQLResult;ZLjava/lang/String;Ljava/util/List;I)V

    .line 2161048
    return-void
.end method
