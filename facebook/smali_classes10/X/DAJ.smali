.class public LX/DAJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0lp;


# direct methods
.method public constructor <init>(LX/0lp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1971028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1971029
    iput-object p1, p0, LX/DAJ;->a:LX/0lp;

    .line 1971030
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 9

    .prologue
    .line 1971031
    check-cast p1, Ljava/util/ArrayList;

    .line 1971032
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/http/NameValuePair;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "target_ids"

    .line 1971033
    new-instance v6, Ljava/io/StringWriter;

    invoke-direct {v6}, Ljava/io/StringWriter;-><init>()V

    .line 1971034
    iget-object v4, p0, LX/DAJ;->a:LX/0lp;

    invoke-virtual {v4, v6}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v7

    .line 1971035
    invoke-virtual {v7}, LX/0nX;->d()V

    .line 1971036
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v8, :cond_0

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1971037
    invoke-virtual {v7, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1971038
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 1971039
    :cond_0
    invoke-virtual {v7}, LX/0nX;->e()V

    .line 1971040
    invoke-virtual {v7}, LX/0nX;->flush()V

    .line 1971041
    invoke-virtual {v6}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 1971042
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1971043
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "BeginJourneys"

    .line 1971044
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1971045
    move-object v1, v1

    .line 1971046
    const-string v2, "POST"

    .line 1971047
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1971048
    move-object v1, v1

    .line 1971049
    const-string v2, "/me/messenger_journeys"

    .line 1971050
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1971051
    move-object v1, v1

    .line 1971052
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1971053
    move-object v0, v1

    .line 1971054
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1971055
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1971056
    move-object v0, v0

    .line 1971057
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1971058
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1971059
    const/4 v0, 0x0

    return-object v0
.end method
