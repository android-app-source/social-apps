.class public LX/DKc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1987292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1987293
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1987294
    check-cast p1, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;

    .line 1987295
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1987296
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "cover"

    .line 1987297
    iget-object v3, p1, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1987298
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1987299
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "group_set_as_cover"

    .line 1987300
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1987301
    move-object v1, v1

    .line 1987302
    const-string v2, "POST"

    .line 1987303
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1987304
    move-object v1, v1

    .line 1987305
    const-string v2, "/%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 1987306
    iget-object p0, p1, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;->a:Ljava/lang/String;

    move-object p0, p0

    .line 1987307
    aput-object p0, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1987308
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1987309
    move-object v1, v1

    .line 1987310
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1987311
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1987312
    move-object v0, v1

    .line 1987313
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1987314
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1987315
    move-object v0, v0

    .line 1987316
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1987317
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1987318
    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 1987319
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1987320
    :cond_0
    new-instance v0, Lorg/apache/http/HttpException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Group Set As Cover Photo Request failed,"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
