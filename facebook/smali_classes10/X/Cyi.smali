.class public final LX/Cyi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Cyl;

.field public final synthetic b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final synthetic c:LX/Cyn;


# direct methods
.method public constructor <init>(LX/Cyn;LX/Cyl;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V
    .locals 0

    .prologue
    .line 1953487
    iput-object p1, p0, LX/Cyi;->c:LX/Cyn;

    iput-object p2, p0, LX/Cyi;->a:LX/Cyl;

    iput-object p3, p0, LX/Cyi;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1953488
    iget-object v0, p0, LX/Cyi;->a:LX/Cyl;

    invoke-virtual {v0}, LX/Cyl;->b()V

    .line 1953489
    iget-object v0, p0, LX/Cyi;->c:LX/Cyn;

    invoke-static {v0}, LX/Cyn;->k(LX/Cyn;)V

    .line 1953490
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1953491
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1953492
    iget-object v1, p0, LX/Cyi;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1953493
    if-eqz p1, :cond_0

    .line 1953494
    iget-object v2, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 1953495
    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    if-eq v2, v3, :cond_2

    .line 1953496
    :cond_0
    :goto_0
    move-object v2, p1

    .line 1953497
    move-object v3, v2

    .line 1953498
    iget-object v0, v3, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1953499
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v5

    .line 1953500
    const-string v0, "CombinedResults were null."

    invoke-static {v5, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953501
    iget-object v0, p0, LX/Cyi;->c:LX/Cyn;

    iget-object v0, v0, LX/Cyn;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cve;

    iget-object v1, p0, LX/Cyi;->b:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    const/4 v2, 0x1

    .line 1953502
    iget-object v4, v3, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    move-object v4, v4

    .line 1953503
    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/Cve;->a(LX/CwB;ZLcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;Ljava/lang/String;)V

    .line 1953504
    iget-object v0, p0, LX/Cyi;->c:LX/Cyn;

    iget-object v0, v0, LX/Cyn;->s:LX/Fdy;

    if-nez v0, :cond_1

    .line 1953505
    iget-object v0, p0, LX/Cyi;->c:LX/Cyn;

    new-instance v1, LX/Cym;

    .line 1953506
    iget-object v2, v3, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    move-object v2, v2

    .line 1953507
    invoke-direct {v1, v3, v2}, LX/Cym;-><init>(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/Cyn;->a$redex0(LX/Cyn;LX/Cym;)V

    .line 1953508
    :goto_1
    return-void

    .line 1953509
    :cond_1
    iget-object v0, p0, LX/Cyi;->c:LX/Cyn;

    iget-object v0, v0, LX/Cyn;->i:LX/CyU;

    .line 1953510
    iget-object v1, v3, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1953511
    new-instance v2, LX/Cym;

    .line 1953512
    iget-object v4, v3, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    move-object v4, v4

    .line 1953513
    invoke-direct {v2, v3, v4}, LX/Cym;-><init>(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/CyU;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 1953514
    :cond_2
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1953515
    check-cast v2, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    .line 1953516
    if-eqz v2, :cond_3

    .line 1953517
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v3

    .line 1953518
    if-eqz v3, :cond_3

    instance-of v4, v3, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    if-eqz v4, :cond_3

    .line 1953519
    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    .line 1953520
    new-instance v4, LX/8ei;

    invoke-direct {v4}, LX/8ei;-><init>()V

    .line 1953521
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->a()LX/0Px;

    move-result-object v0

    iput-object v0, v4, LX/8ei;->a:LX/0Px;

    .line 1953522
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->j()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v0

    iput-object v0, v4, LX/8ei;->b:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    .line 1953523
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, LX/8ei;->c:Ljava/lang/String;

    .line 1953524
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, LX/8ei;->d:Ljava/lang/String;

    .line 1953525
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-result-object v0

    iput-object v0, v4, LX/8ei;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    .line 1953526
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, LX/8ei;->f:Ljava/lang/String;

    .line 1953527
    move-object v4, v4

    .line 1953528
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v3

    .line 1953529
    :goto_2
    iput-object v3, v4, LX/8ei;->d:Ljava/lang/String;

    .line 1953530
    move-object v3, v4

    .line 1953531
    invoke-virtual {v3}, LX/8ei;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v3

    .line 1953532
    new-instance v4, LX/8eh;

    invoke-direct {v4}, LX/8eh;-><init>()V

    .line 1953533
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v0

    iput-object v0, v4, LX/8eh;->a:Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    .line 1953534
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->j()LX/0Px;

    move-result-object v0

    iput-object v0, v4, LX/8eh;->b:LX/0Px;

    .line 1953535
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, LX/8eh;->c:Ljava/lang/String;

    .line 1953536
    move-object v4, v4

    .line 1953537
    move-object v2, v3

    check-cast v2, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    .line 1953538
    iput-object v2, v4, LX/8eh;->a:Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    .line 1953539
    move-object v2, v4

    .line 1953540
    invoke-virtual {v2}, LX/8eh;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    move-result-object v2

    .line 1953541
    :cond_3
    invoke-static {p1}, LX/1lO;->a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1lO;

    move-result-object v3

    .line 1953542
    iput-object v2, v3, LX/1lO;->k:Ljava/lang/Object;

    .line 1953543
    move-object v2, v3

    .line 1953544
    invoke-virtual {v2}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object p1

    goto/16 :goto_0

    .line 1953545
    :cond_4
    iget-object v3, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1953546
    goto :goto_2
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1953547
    iget-object v0, p0, LX/Cyi;->c:LX/Cyn;

    iget-object v1, p0, LX/Cyi;->c:LX/Cyn;

    iget-object v1, v1, LX/Cyn;->s:LX/Fdy;

    iget-object v2, p0, LX/Cyi;->a:LX/Cyl;

    invoke-static {v0, p1, v1, v2}, LX/Cyn;->a$redex0(LX/Cyn;Ljava/lang/Throwable;LX/Fdy;LX/Cyl;)V

    .line 1953548
    return-void
.end method
