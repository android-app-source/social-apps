.class public LX/EnS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/EnU;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/EoA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/EnU;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EnU;",
            "Ljava/util/Set",
            "<",
            "LX/EoA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2167097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167098
    iput-object p1, p0, LX/EnS;->a:LX/EnU;

    .line 2167099
    iput-object p2, p0, LX/EnS;->b:Ljava/util/Set;

    .line 2167100
    return-void
.end method

.method public static a(LX/0QB;)LX/EnS;
    .locals 7

    .prologue
    .line 2167101
    const-class v1, LX/EnS;

    monitor-enter v1

    .line 2167102
    :try_start_0
    sget-object v0, LX/EnS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2167103
    sput-object v2, LX/EnS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2167104
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167105
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2167106
    new-instance v4, LX/EnS;

    invoke-static {v0}, LX/EnU;->a(LX/0QB;)LX/EnU;

    move-result-object v3

    check-cast v3, LX/EnU;

    .line 2167107
    new-instance v5, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    new-instance p0, LX/EoC;

    invoke-direct {p0, v0}, LX/EoC;-><init>(LX/0QB;)V

    invoke-direct {v5, v6, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v5, v5

    .line 2167108
    invoke-direct {v4, v3, v5}, LX/EnS;-><init>(LX/EnU;Ljava/util/Set;)V

    .line 2167109
    move-object v0, v4

    .line 2167110
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2167111
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EnS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2167112
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2167113
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/os/Bundle;)Lcom/facebook/entitycards/intent/EntityCardsParameters;
    .locals 1

    .prologue
    .line 2167114
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167115
    const-string v0, "entity_cards_fragment_parameters"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 2167116
    check-cast v0, Lcom/facebook/entitycards/intent/EntityCardsParameters;

    return-object v0
.end method

.method public static b(Landroid/os/Bundle;Landroid/os/Bundle;)LX/0Px;
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Landroid/os/Bundle;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2167117
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/EnS;->c(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167118
    invoke-static {p1}, LX/EnS;->a(Landroid/os/Bundle;)Lcom/facebook/entitycards/intent/EntityCardsParameters;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->c:LX/0Px;

    .line 2167119
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/EnS;->a(Landroid/os/Bundle;)Lcom/facebook/entitycards/intent/EntityCardsParameters;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->c:LX/0Px;

    goto :goto_0
.end method

.method public static b(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2167120
    invoke-static {p0}, LX/EnS;->a(Landroid/os/Bundle;)Lcom/facebook/entitycards/intent/EntityCardsParameters;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->f:Ljava/lang/String;

    return-object v0
.end method

.method public static c(Landroid/os/Bundle;Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2167121
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/EnS;->c(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2167122
    invoke-static {p1}, LX/EnS;->a(Landroid/os/Bundle;)Lcom/facebook/entitycards/intent/EntityCardsParameters;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->d:Ljava/lang/String;

    .line 2167123
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/EnS;->a(Landroid/os/Bundle;)Lcom/facebook/entitycards/intent/EntityCardsParameters;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/entitycards/intent/EntityCardsParameters;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method private static c(Landroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 2167124
    const-string v0, "entity_cards_fragment_parameters"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/EnS;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2167125
    iget-object v1, p0, LX/EnS;->a:LX/EnU;

    const-string v0, "entity_cards_config_extras_uuid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    .line 2167126
    iget-object p0, v1, LX/EnU;->a:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/Bundle;

    move-object v0, p0

    .line 2167127
    if-eqz v0, :cond_0

    .line 2167128
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "entity_cards_config_extras"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/EoA;
    .locals 4

    .prologue
    .line 2167129
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167130
    iget-object v0, p0, LX/EnS;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EoA;

    .line 2167131
    invoke-interface {v0}, LX/EoA;->a()Ljava/lang/String;

    move-result-object v1

    const-string v3, "EntityCardsSurfaceConfiguration.getKey should not be null"

    invoke-static {v1, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2167132
    invoke-static {p1, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2167133
    return-object v0

    .line 2167134
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "EntityCardsFragment can\'t be instantiated without a configuration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2167135
    if-nez p2, :cond_1

    .line 2167136
    invoke-static {p0, p1}, LX/EnS;->d(LX/EnS;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 2167137
    :cond_0
    :goto_0
    return-object v0

    .line 2167138
    :cond_1
    const-string v0, "entity_cards_config_extras"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 2167139
    if-nez v0, :cond_0

    invoke-static {p0, p1}, LX/EnS;->d(LX/EnS;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method
