.class public final LX/Ecd;
.super LX/EWj;
.source ""

# interfaces
.implements LX/Ecc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/Ecd;",
        ">;",
        "LX/Ecc;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2147450
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2147451
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ecd;->d:LX/EWc;

    .line 2147452
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2147402
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2147403
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ecd;->d:LX/EWc;

    .line 2147404
    return-void
.end method

.method private d(LX/EWY;)LX/Ecd;
    .locals 1

    .prologue
    .line 2147405
    instance-of v0, p1, LX/Ece;

    if-eqz v0, :cond_0

    .line 2147406
    check-cast p1, LX/Ece;

    invoke-virtual {p0, p1}, LX/Ecd;->a(LX/Ece;)LX/Ecd;

    move-result-object p0

    .line 2147407
    :goto_0
    return-object p0

    .line 2147408
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/Ecd;
    .locals 4

    .prologue
    .line 2147409
    const/4 v2, 0x0

    .line 2147410
    :try_start_0
    sget-object v0, LX/Ece;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ece;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2147411
    if-eqz v0, :cond_0

    .line 2147412
    invoke-virtual {p0, v0}, LX/Ecd;->a(LX/Ece;)LX/Ecd;

    .line 2147413
    :cond_0
    return-object p0

    .line 2147414
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2147415
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2147416
    check-cast v0, LX/Ece;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2147417
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2147418
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2147419
    invoke-virtual {p0, v1}, LX/Ecd;->a(LX/Ece;)LX/Ecd;

    :cond_1
    throw v0

    .line 2147420
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static w()LX/Ecd;
    .locals 1

    .prologue
    .line 2147421
    new-instance v0, LX/Ecd;

    invoke-direct {v0}, LX/Ecd;-><init>()V

    return-object v0
.end method

.method private x()LX/Ecd;
    .locals 2

    .prologue
    .line 2147422
    invoke-static {}, LX/Ecd;->w()LX/Ecd;

    move-result-object v0

    invoke-virtual {p0}, LX/Ecd;->m()LX/Ece;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ecd;->a(LX/Ece;)LX/Ecd;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2147423
    invoke-direct {p0, p1}, LX/Ecd;->d(LX/EWY;)LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2147424
    invoke-direct {p0, p1, p2}, LX/Ecd;->d(LX/EWd;LX/EYZ;)LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/Ecd;
    .locals 1

    .prologue
    .line 2147425
    iget v0, p0, LX/Ecd;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ecd;->a:I

    .line 2147426
    iput p1, p0, LX/Ecd;->b:I

    .line 2147427
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147428
    return-object p0
.end method

.method public final a(LX/EWc;)LX/Ecd;
    .locals 1

    .prologue
    .line 2147429
    if-nez p1, :cond_0

    .line 2147430
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2147431
    :cond_0
    iget v0, p0, LX/Ecd;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/Ecd;->a:I

    .line 2147432
    iput-object p1, p0, LX/Ecd;->d:LX/EWc;

    .line 2147433
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147434
    return-object p0
.end method

.method public final a(LX/Ece;)LX/Ecd;
    .locals 2

    .prologue
    .line 2147435
    sget-object v0, LX/Ece;->c:LX/Ece;

    move-object v0, v0

    .line 2147436
    if-ne p1, v0, :cond_0

    .line 2147437
    :goto_0
    return-object p0

    .line 2147438
    :cond_0
    invoke-virtual {p1}, LX/Ece;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2147439
    iget v0, p1, LX/Ece;->preKeyId_:I

    move v0, v0

    .line 2147440
    invoke-virtual {p0, v0}, LX/Ecd;->a(I)LX/Ecd;

    .line 2147441
    :cond_1
    iget v0, p1, LX/Ece;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2147442
    if-eqz v0, :cond_2

    .line 2147443
    iget v0, p1, LX/Ece;->signedPreKeyId_:I

    move v0, v0

    .line 2147444
    invoke-virtual {p0, v0}, LX/Ecd;->b(I)LX/Ecd;

    .line 2147445
    :cond_2
    iget v0, p1, LX/Ece;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2147446
    if-eqz v0, :cond_3

    .line 2147447
    iget-object v0, p1, LX/Ece;->baseKey_:LX/EWc;

    move-object v0, v0

    .line 2147448
    invoke-virtual {p0, v0}, LX/Ecd;->a(LX/EWc;)LX/Ecd;

    .line 2147449
    :cond_3
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2147364
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2147453
    invoke-direct {p0, p1, p2}, LX/Ecd;->d(LX/EWd;LX/EYZ;)LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2147397
    invoke-direct {p0}, LX/Ecd;->x()LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)LX/Ecd;
    .locals 1

    .prologue
    .line 2147398
    iget v0, p0, LX/Ecd;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/Ecd;->a:I

    .line 2147399
    iput p1, p0, LX/Ecd;->c:I

    .line 2147400
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147401
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2147363
    invoke-direct {p0, p1, p2}, LX/Ecd;->d(LX/EWd;LX/EYZ;)LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2147365
    invoke-direct {p0}, LX/Ecd;->x()LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2147366
    invoke-direct {p0, p1}, LX/Ecd;->d(LX/EWY;)LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2147367
    invoke-direct {p0}, LX/Ecd;->x()LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2147368
    sget-object v0, LX/Eck;->l:LX/EYn;

    const-class v1, LX/Ece;

    const-class v2, LX/Ecd;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2147369
    sget-object v0, LX/Eck;->k:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2147370
    invoke-direct {p0}, LX/Ecd;->x()LX/Ecd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2147371
    invoke-virtual {p0}, LX/Ecd;->m()LX/Ece;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2147372
    invoke-virtual {p0}, LX/Ecd;->l()LX/Ece;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2147373
    invoke-virtual {p0}, LX/Ecd;->m()LX/Ece;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2147374
    invoke-virtual {p0}, LX/Ecd;->l()LX/Ece;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/Ece;
    .locals 2

    .prologue
    .line 2147375
    invoke-virtual {p0}, LX/Ecd;->m()LX/Ece;

    move-result-object v0

    .line 2147376
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2147377
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2147378
    :cond_0
    return-object v0
.end method

.method public final m()LX/Ece;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2147379
    new-instance v2, LX/Ece;

    invoke-direct {v2, p0}, LX/Ece;-><init>(LX/EWj;)V

    .line 2147380
    iget v3, p0, LX/Ecd;->a:I

    .line 2147381
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    .line 2147382
    :goto_0
    iget v1, p0, LX/Ecd;->b:I

    .line 2147383
    iput v1, v2, LX/Ece;->preKeyId_:I

    .line 2147384
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2147385
    or-int/lit8 v0, v0, 0x2

    .line 2147386
    :cond_0
    iget v1, p0, LX/Ecd;->c:I

    .line 2147387
    iput v1, v2, LX/Ece;->signedPreKeyId_:I

    .line 2147388
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 2147389
    or-int/lit8 v0, v0, 0x4

    .line 2147390
    :cond_1
    iget-object v1, p0, LX/Ecd;->d:LX/EWc;

    .line 2147391
    iput-object v1, v2, LX/Ece;->baseKey_:LX/EWc;

    .line 2147392
    iput v0, v2, LX/Ece;->bitField0_:I

    .line 2147393
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2147394
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2147395
    sget-object v0, LX/Ece;->c:LX/Ece;

    move-object v0, v0

    .line 2147396
    return-object v0
.end method
