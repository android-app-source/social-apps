.class public final LX/DPl;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DPm;


# direct methods
.method public constructor <init>(LX/DPm;)V
    .locals 0

    .prologue
    .line 1993682
    iput-object p1, p0, LX/DPl;->a:LX/DPm;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1993679
    iget-object v0, p0, LX/DPl;->a:LX/DPm;

    iget-object v0, v0, LX/DPm;->b:LX/DPo;

    const v1, 0x7f083049

    invoke-static {v0, v1}, LX/DPo;->a$redex0(LX/DPo;I)V

    .line 1993680
    iget-object v0, p0, LX/DPl;->a:LX/DPm;

    iget-object v0, v0, LX/DPm;->b:LX/DPo;

    iget-object v0, v0, LX/DPo;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/DPo;->a:Ljava/lang/String;

    const-string v2, "Group unarchive action failed."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1993681
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1993676
    iget-object v0, p0, LX/DPl;->a:LX/DPm;

    iget-object v0, v0, LX/DPm;->b:LX/DPo;

    const v1, 0x7f083048

    invoke-static {v0, v1}, LX/DPo;->a$redex0(LX/DPo;I)V

    .line 1993677
    iget-object v0, p0, LX/DPl;->a:LX/DPm;

    iget-object v0, v0, LX/DPm;->b:LX/DPo;

    iget-object v1, p0, LX/DPl;->a:LX/DPm;

    iget-object v1, v1, LX/DPm;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v0, v1}, LX/DPo;->c(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    .line 1993678
    return-void
.end method
