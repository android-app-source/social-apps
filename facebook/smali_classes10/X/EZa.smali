.class public final enum LX/EZa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EZa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EZa;

.field public static final enum BOOLEAN:LX/EZa;

.field public static final enum BYTE_STRING:LX/EZa;

.field public static final enum DOUBLE:LX/EZa;

.field public static final enum ENUM:LX/EZa;

.field public static final enum FLOAT:LX/EZa;

.field public static final enum INT:LX/EZa;

.field public static final enum LONG:LX/EZa;

.field public static final enum MESSAGE:LX/EZa;

.field public static final enum STRING:LX/EZa;


# instance fields
.field private final defaultDefault:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2139578
    new-instance v0, LX/EZa;

    const-string v1, "INT"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, LX/EZa;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EZa;->INT:LX/EZa;

    .line 2139579
    new-instance v0, LX/EZa;

    const-string v1, "LONG"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, LX/EZa;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EZa;->LONG:LX/EZa;

    .line 2139580
    new-instance v0, LX/EZa;

    const-string v1, "FLOAT"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LX/EZa;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EZa;->FLOAT:LX/EZa;

    .line 2139581
    new-instance v0, LX/EZa;

    const-string v1, "DOUBLE"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LX/EZa;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EZa;->DOUBLE:LX/EZa;

    .line 2139582
    new-instance v0, LX/EZa;

    const-string v1, "BOOLEAN"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LX/EZa;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EZa;->BOOLEAN:LX/EZa;

    .line 2139583
    new-instance v0, LX/EZa;

    const-string v1, "STRING"

    const/4 v2, 0x5

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, LX/EZa;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EZa;->STRING:LX/EZa;

    .line 2139584
    new-instance v0, LX/EZa;

    const-string v1, "BYTE_STRING"

    const/4 v2, 0x6

    sget-object v3, LX/EWc;->a:LX/EWc;

    invoke-direct {v0, v1, v2, v3}, LX/EZa;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EZa;->BYTE_STRING:LX/EZa;

    .line 2139585
    new-instance v0, LX/EZa;

    const-string v1, "ENUM"

    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/EZa;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EZa;->ENUM:LX/EZa;

    .line 2139586
    new-instance v0, LX/EZa;

    const-string v1, "MESSAGE"

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/EZa;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EZa;->MESSAGE:LX/EZa;

    .line 2139587
    const/16 v0, 0x9

    new-array v0, v0, [LX/EZa;

    sget-object v1, LX/EZa;->INT:LX/EZa;

    aput-object v1, v0, v4

    sget-object v1, LX/EZa;->LONG:LX/EZa;

    aput-object v1, v0, v5

    sget-object v1, LX/EZa;->FLOAT:LX/EZa;

    aput-object v1, v0, v6

    sget-object v1, LX/EZa;->DOUBLE:LX/EZa;

    aput-object v1, v0, v7

    sget-object v1, LX/EZa;->BOOLEAN:LX/EZa;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/EZa;->STRING:LX/EZa;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EZa;->BYTE_STRING:LX/EZa;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EZa;->ENUM:LX/EZa;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EZa;->MESSAGE:LX/EZa;

    aput-object v2, v0, v1

    sput-object v0, LX/EZa;->$VALUES:[LX/EZa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2139572
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2139573
    iput-object p3, p0, LX/EZa;->defaultDefault:Ljava/lang/Object;

    .line 2139574
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EZa;
    .locals 1

    .prologue
    .line 2139577
    const-class v0, LX/EZa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EZa;

    return-object v0
.end method

.method public static values()[LX/EZa;
    .locals 1

    .prologue
    .line 2139576
    sget-object v0, LX/EZa;->$VALUES:[LX/EZa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EZa;

    return-object v0
.end method


# virtual methods
.method public final getDefaultDefault()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2139575
    iget-object v0, p0, LX/EZa;->defaultDefault:Ljava/lang/Object;

    return-object v0
.end method
