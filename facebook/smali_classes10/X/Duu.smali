.class public final LX/Duu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;)V
    .locals 0

    .prologue
    .line 2057830
    iput-object p1, p0, LX/Duu;->a:Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x63bcc872

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2057831
    iget-object v0, p0, LX/Duu;->a:Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;

    iget v0, v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->b:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2057832
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->z()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2057833
    :cond_0
    const v0, 0x1a59f68f

    invoke-static {v3, v3, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2057834
    :goto_0
    return-void

    .line 2057835
    :cond_1
    iget-object v1, p0, LX/Duu;->a:Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;

    iget-object v1, v1, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9bY;

    new-instance v3, LX/9bi;

    invoke-direct {v3, v0}, LX/9bi;-><init>(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V

    invoke-virtual {v1, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2057836
    const v0, -0x11873056

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0
.end method
