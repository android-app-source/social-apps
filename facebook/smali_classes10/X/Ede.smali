.class public LX/Ede;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2150038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2150039
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    .line 2150040
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    .line 2150041
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2150034
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2150035
    if-nez v0, :cond_0

    .line 2150036
    const/4 v0, 0x0

    .line 2150037
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final a(II)V
    .locals 5

    .prologue
    const/16 v2, 0x81

    const/16 v4, 0xff

    const/16 v0, 0xc0

    const/16 v1, 0xe0

    const/16 v3, 0x80

    .line 2149992
    packed-switch p2, :pswitch_data_0

    .line 2149993
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid header field!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149994
    :pswitch_1
    if-eq v3, p1, :cond_2

    if-eq v2, p1, :cond_2

    .line 2149995
    new-instance v0, LX/EdU;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Octet value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdU;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149996
    :pswitch_2
    if-eq v3, p1, :cond_2

    if-eq v2, p1, :cond_2

    .line 2149997
    new-instance v0, LX/EdU;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Octet value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdU;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149998
    :pswitch_3
    if-eq v3, p1, :cond_2

    if-eq v2, p1, :cond_2

    .line 2149999
    new-instance v0, LX/EdU;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Octet value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdU;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150000
    :pswitch_4
    if-lt p1, v3, :cond_0

    const/16 v0, 0x82

    if-le p1, v0, :cond_2

    .line 2150001
    :cond_0
    new-instance v0, LX/EdU;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Octet value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdU;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150002
    :pswitch_5
    if-lt p1, v3, :cond_1

    const/16 v0, 0x87

    if-le p1, v0, :cond_2

    .line 2150003
    :cond_1
    const/16 p1, 0x84

    .line 2150004
    :cond_2
    :goto_0
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150005
    return-void

    .line 2150006
    :pswitch_6
    if-lt p1, v3, :cond_3

    const/16 v0, 0x83

    if-le p1, v0, :cond_2

    .line 2150007
    :cond_3
    new-instance v0, LX/EdU;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Octet value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdU;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150008
    :pswitch_7
    if-lt p1, v3, :cond_4

    const/16 v0, 0x84

    if-le p1, v0, :cond_2

    .line 2150009
    :cond_4
    new-instance v0, LX/EdU;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Octet value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdU;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150010
    :pswitch_8
    if-eq v3, p1, :cond_2

    .line 2150011
    new-instance v0, LX/EdU;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Octet value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdU;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150012
    :pswitch_9
    if-lt p1, v3, :cond_5

    const/16 v0, 0x87

    if-le p1, v0, :cond_2

    .line 2150013
    :cond_5
    const/16 p1, 0x87

    goto :goto_0

    .line 2150014
    :pswitch_a
    const/16 v2, 0xc2

    if-le p1, v2, :cond_6

    if-ge p1, v1, :cond_6

    move p1, v0

    .line 2150015
    goto :goto_0

    .line 2150016
    :cond_6
    const/16 v2, 0xe3

    if-le p1, v2, :cond_7

    if-gt p1, v4, :cond_7

    move p1, v1

    .line 2150017
    goto :goto_0

    .line 2150018
    :cond_7
    if-lt p1, v3, :cond_9

    if-le p1, v3, :cond_8

    if-lt p1, v0, :cond_9

    :cond_8
    if-le p1, v4, :cond_2

    :cond_9
    move p1, v1

    .line 2150019
    goto :goto_0

    .line 2150020
    :pswitch_b
    const/16 v2, 0xc1

    if-le p1, v2, :cond_a

    if-ge p1, v1, :cond_a

    move p1, v0

    .line 2150021
    goto/16 :goto_0

    .line 2150022
    :cond_a
    const/16 v2, 0xe4

    if-le p1, v2, :cond_b

    if-gt p1, v4, :cond_b

    move p1, v1

    .line 2150023
    goto/16 :goto_0

    .line 2150024
    :cond_b
    if-lt p1, v3, :cond_d

    if-le p1, v3, :cond_c

    if-lt p1, v0, :cond_d

    :cond_c
    if-le p1, v4, :cond_2

    :cond_d
    move p1, v1

    .line 2150025
    goto/16 :goto_0

    .line 2150026
    :pswitch_c
    const/16 v2, 0xc4

    if-le p1, v2, :cond_e

    if-ge p1, v1, :cond_e

    move p1, v0

    .line 2150027
    goto/16 :goto_0

    .line 2150028
    :cond_e
    const/16 v2, 0xeb

    if-le p1, v2, :cond_f

    if-le p1, v4, :cond_11

    :cond_f
    if-lt p1, v3, :cond_11

    const/16 v2, 0x88

    if-le p1, v2, :cond_10

    if-lt p1, v0, :cond_11

    :cond_10
    if-le p1, v4, :cond_2

    :cond_11
    move p1, v1

    .line 2150029
    goto/16 :goto_0

    .line 2150030
    :pswitch_d
    const/16 v0, 0x10

    if-lt p1, v0, :cond_12

    const/16 v0, 0x13

    if-le p1, v0, :cond_2

    .line 2150031
    :cond_12
    const/16 p1, 0x12

    goto/16 :goto_0

    .line 2150032
    :pswitch_e
    if-lt p1, v3, :cond_13

    const/16 v0, 0x97

    if-le p1, v0, :cond_2

    .line 2150033
    :cond_13
    new-instance v0, LX/EdU;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Octet value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdU;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x86
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_d
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_c
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_7
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(JI)V
    .locals 3

    .prologue
    .line 2149988
    sparse-switch p3, :sswitch_data_0

    .line 2149989
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid header field!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149990
    :sswitch_0
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2149991
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x85 -> :sswitch_0
        0x87 -> :sswitch_0
        0x88 -> :sswitch_0
        0x8e -> :sswitch_0
        0x9d -> :sswitch_0
        0x9f -> :sswitch_0
        0xa1 -> :sswitch_0
        0xad -> :sswitch_0
        0xaf -> :sswitch_0
        0xb3 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(LX/EdS;I)V
    .locals 2

    .prologue
    .line 2149982
    if-nez p1, :cond_0

    .line 2149983
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2149984
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2149985
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid header field!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149986
    :sswitch_0
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2149987
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x89 -> :sswitch_0
        0x93 -> :sswitch_0
        0x96 -> :sswitch_0
        0x9a -> :sswitch_0
        0xa0 -> :sswitch_0
        0xa4 -> :sswitch_0
        0xa6 -> :sswitch_0
        0xb5 -> :sswitch_0
        0xb6 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a([BI)V
    .locals 2

    .prologue
    .line 2149976
    if-nez p1, :cond_0

    .line 2149977
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2149978
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2149979
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid header field!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149980
    :sswitch_0
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2149981
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_0
        0x84 -> :sswitch_0
        0x8a -> :sswitch_0
        0x8b -> :sswitch_0
        0x93 -> :sswitch_0
        0x98 -> :sswitch_0
        0x9e -> :sswitch_0
        0xb7 -> :sswitch_0
        0xb8 -> :sswitch_0
        0xb9 -> :sswitch_0
        0xbd -> :sswitch_0
        0xbe -> :sswitch_0
    .end sparse-switch
.end method

.method public final a([LX/EdS;I)V
    .locals 3

    .prologue
    .line 2149944
    if-nez p1, :cond_0

    .line 2149945
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2149946
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2149947
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid header field!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149948
    :sswitch_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2149949
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 2149950
    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2149951
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2149952
    :cond_1
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2149953
    return-void

    :sswitch_data_0
    .sparse-switch
        0x81 -> :sswitch_0
        0x82 -> :sswitch_0
        0x97 -> :sswitch_0
    .end sparse-switch
.end method

.method public final b(LX/EdS;I)V
    .locals 2

    .prologue
    .line 2149966
    if-nez p1, :cond_0

    .line 2149967
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2149968
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2149969
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid header field!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149970
    :sswitch_0
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2149971
    if-nez v0, :cond_1

    .line 2149972
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2149973
    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2149974
    iget-object v1, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2149975
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x81 -> :sswitch_0
        0x82 -> :sswitch_0
        0x97 -> :sswitch_0
    .end sparse-switch
.end method

.method public final b(I)[B
    .locals 1

    .prologue
    .line 2149965
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public final c(I)LX/EdS;
    .locals 1

    .prologue
    .line 2149964
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EdS;

    return-object v0
.end method

.method public final d(I)[LX/EdS;
    .locals 2

    .prologue
    .line 2149958
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2149959
    if-nez v0, :cond_0

    .line 2149960
    const/4 v0, 0x0

    .line 2149961
    :goto_0
    return-object v0

    .line 2149962
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LX/EdS;

    .line 2149963
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EdS;

    goto :goto_0
.end method

.method public final e(I)J
    .locals 2

    .prologue
    .line 2149954
    iget-object v0, p0, LX/Ede;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2149955
    if-nez v0, :cond_0

    .line 2149956
    const-wide/16 v0, -0x1

    .line 2149957
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method
