.class public LX/E14;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/9kE;

.field public final b:LX/E11;

.field public final c:LX/11H;

.field public final d:LX/E10;


# direct methods
.method public constructor <init>(LX/9kE;LX/E11;LX/11H;LX/E10;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2069338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2069339
    iput-object p1, p0, LX/E14;->a:LX/9kE;

    .line 2069340
    iput-object p2, p0, LX/E14;->b:LX/E11;

    .line 2069341
    iput-object p3, p0, LX/E14;->c:LX/11H;

    .line 2069342
    iput-object p4, p0, LX/E14;->d:LX/E10;

    .line 2069343
    return-void
.end method

.method public static b(LX/0QB;)LX/E14;
    .locals 7

    .prologue
    .line 2069344
    new-instance v4, LX/E14;

    invoke-static {p0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v0

    check-cast v0, LX/9kE;

    .line 2069345
    new-instance v1, LX/E11;

    invoke-direct {v1}, LX/E11;-><init>()V

    .line 2069346
    move-object v1, v1

    .line 2069347
    move-object v1, v1

    .line 2069348
    check-cast v1, LX/E11;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v2

    check-cast v2, LX/11H;

    .line 2069349
    new-instance v6, LX/E10;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v3

    check-cast v3, LX/0lp;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-direct {v6, v3, v5}, LX/E10;-><init>(LX/0lp;LX/0lC;)V

    .line 2069350
    move-object v3, v6

    .line 2069351
    check-cast v3, LX/E10;

    invoke-direct {v4, v0, v1, v2, v3}, LX/E14;-><init>(LX/9kE;LX/E11;LX/11H;LX/E10;)V

    .line 2069352
    return-object v4
.end method


# virtual methods
.method public final a(Lcom/facebook/places/create/network/PlaceCreationParams;LX/0TF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/create/network/PlaceCreationParams;",
            "LX/0TF",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2069353
    iget-object v0, p0, LX/E14;->a:LX/9kE;

    new-instance v1, LX/E13;

    invoke-direct {v1, p0, p1}, LX/E13;-><init>(LX/E14;Lcom/facebook/places/create/network/PlaceCreationParams;)V

    invoke-virtual {v0, v1, p2}, LX/9kE;->a(Ljava/util/concurrent/Callable;LX/0TF;)V

    .line 2069354
    return-void
.end method
