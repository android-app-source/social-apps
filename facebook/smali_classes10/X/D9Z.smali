.class public LX/D9Z;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final a:LX/0wT;


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/ImageView;

.field public final d:LX/0wd;

.field public final e:LX/0wd;

.field private final f:I

.field private final g:Landroid/graphics/PointF;

.field private final h:I

.field private i:F

.field private j:F

.field public k:LX/0wW;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1970315
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/D9Z;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1970348
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D9Z;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1970349
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1970346
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D9Z;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1970347
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const v3, 0x3f333333    # 0.7f

    const/high16 v1, 0x3f000000    # 0.5f

    .line 1970329
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1970330
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, LX/D9Z;->g:Landroid/graphics/PointF;

    .line 1970331
    iput v1, p0, LX/D9Z;->i:F

    .line 1970332
    iput v1, p0, LX/D9Z;->j:F

    .line 1970333
    const-class v0, LX/D9Z;

    invoke-static {v0, p0}, LX/D9Z;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1970334
    const v0, 0x7f030a7e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1970335
    const v0, 0x7f0d1abb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D9Z;->b:Landroid/view/View;

    .line 1970336
    const v0, 0x7f0d1abc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/D9Z;->c:Landroid/widget/ImageView;

    .line 1970337
    new-instance v0, LX/D9Y;

    invoke-direct {v0, p0}, LX/D9Y;-><init>(LX/D9Z;)V

    .line 1970338
    iget-object v1, p0, LX/D9Z;->k:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/D9Z;->a:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/D9Z;->d:LX/0wd;

    .line 1970339
    iget-object v1, p0, LX/D9Z;->k:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/D9Z;->a:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/D9Z;->e:LX/0wd;

    .line 1970340
    invoke-virtual {p0}, LX/D9Z;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D9Z;->f:I

    .line 1970341
    invoke-virtual {p0}, LX/D9Z;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D9Z;->h:I

    .line 1970342
    iget-object v0, p0, LX/D9Z;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setScaleX(F)V

    .line 1970343
    iget-object v0, p0, LX/D9Z;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setScaleY(F)V

    .line 1970344
    invoke-direct {p0}, LX/D9Z;->a()V

    .line 1970345
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 1970322
    iget-object v0, p0, LX/D9Z;->d:LX/0wd;

    iget-object v1, p0, LX/D9Z;->d:LX/0wd;

    .line 1970323
    iget-wide v4, v1, LX/0wd;->i:D

    move-wide v2, v4

    .line 1970324
    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1970325
    iget-object v0, p0, LX/D9Z;->e:LX/0wd;

    iget-object v1, p0, LX/D9Z;->e:LX/0wd;

    .line 1970326
    iget-wide v4, v1, LX/0wd;->i:D

    move-wide v2, v4

    .line 1970327
    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1970328
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/D9Z;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/D9Z;

    invoke-static {v0}, LX/D9f;->a(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, LX/D9Z;->k:LX/0wW;

    return-void
.end method

.method public static setBubbleX(LX/D9Z;F)V
    .locals 1

    .prologue
    .line 1970320
    iget-object v0, p0, LX/D9Z;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationX(F)V

    .line 1970321
    return-void
.end method

.method public static setBubbleY(LX/D9Z;F)V
    .locals 1

    .prologue
    .line 1970318
    iget-object v0, p0, LX/D9Z;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    .line 1970319
    return-void
.end method


# virtual methods
.method public getDismissBubbleCenterXInScreen()F
    .locals 2

    .prologue
    .line 1970317
    invoke-virtual {p0}, LX/D9Z;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/D9Z;->i:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDismissBubbleCenterYInScreen()F
    .locals 2

    .prologue
    .line 1970316
    invoke-virtual {p0}, LX/D9Z;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/D9Z;->j:F

    mul-float/2addr v0, v1

    return v0
.end method
