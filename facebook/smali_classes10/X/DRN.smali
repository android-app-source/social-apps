.class public final LX/DRN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1998108
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1998109
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1998110
    :goto_0
    return v1

    .line 1998111
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 1998112
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1998113
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1998114
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1998115
    const-string v7, "count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1998116
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1998117
    :cond_1
    const-string v7, "edges"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1998118
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1998119
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_2

    .line 1998120
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_2

    .line 1998121
    invoke-static {p0, p1}, LX/DRL;->b(LX/15w;LX/186;)I

    move-result v6

    .line 1998122
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1998123
    :cond_2
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1998124
    goto :goto_1

    .line 1998125
    :cond_3
    const-string v7, "page_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1998126
    invoke-static {p0, p1}, LX/DRM;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1998127
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1998128
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1998129
    if-eqz v0, :cond_6

    .line 1998130
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 1998131
    :cond_6
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1998132
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1998133
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1998134
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1998135
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1998136
    if-eqz v0, :cond_0

    .line 1998137
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1998138
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1998139
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1998140
    if-eqz v0, :cond_2

    .line 1998141
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1998142
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1998143
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1998144
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/DRL;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1998145
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1998146
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1998147
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1998148
    if-eqz v0, :cond_7

    .line 1998149
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1998150
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1998151
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1998152
    if-eqz v1, :cond_3

    .line 1998153
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1998154
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1998155
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1998156
    if-eqz v1, :cond_4

    .line 1998157
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1998158
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1998159
    :cond_4
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1998160
    if-eqz v1, :cond_5

    .line 1998161
    const-string v2, "has_previous_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1998162
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1998163
    :cond_5
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1998164
    if-eqz v1, :cond_6

    .line 1998165
    const-string v2, "start_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1998166
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1998167
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1998168
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1998169
    return-void
.end method
