.class public final LX/EwA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ewi;

.field public final synthetic b:LX/EwG;


# direct methods
.method public constructor <init>(LX/EwG;LX/Ewi;)V
    .locals 0

    .prologue
    .line 2182770
    iput-object p1, p0, LX/EwA;->b:LX/EwG;

    iput-object p2, p0, LX/EwA;->a:LX/Ewi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, -0xe30c61d

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2182771
    iget-object v0, p0, LX/EwA;->a:LX/Ewi;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v2}, LX/Eus;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2182772
    iget-object v0, p0, LX/EwA;->a:LX/Ewi;

    const/4 v2, 0x0

    .line 2182773
    iput-boolean v2, v0, LX/Eut;->f:Z

    .line 2182774
    iget-object v0, p0, LX/EwA;->b:LX/EwG;

    const v2, -0x74645569

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2182775
    iget-object v0, p0, LX/EwA;->b:LX/EwG;

    invoke-static {v0}, LX/EwG;->e(LX/EwG;)V

    .line 2182776
    iget-object v0, p0, LX/EwA;->b:LX/EwG;

    iget-object v0, v0, LX/EwG;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3UJ;

    iget-object v2, p0, LX/EwA;->a:LX/Ewi;

    invoke-virtual {v2}, LX/Eus;->a()J

    move-result-wide v2

    sget-object v4, LX/2na;->REJECT:LX/2na;

    iget-object v5, p0, LX/EwA;->b:LX/EwG;

    iget-object v6, p0, LX/EwA;->a:LX/Ewi;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {v5, v6, v7}, LX/EwG;->a$redex0(LX/EwG;LX/Ewi;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/84H;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, LX/3UJ;->a(JLX/2na;LX/84H;)V

    .line 2182777
    const v0, -0x32c7dfbb    # -1.9307016E8f

    invoke-static {v8, v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
