.class public LX/E6l;
.super LX/Cfk;
.source ""


# instance fields
.field private final a:LX/6RZ;

.field private final b:LX/E1i;


# direct methods
.method public constructor <init>(LX/6RZ;LX/E1i;LX/3Tx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080543
    invoke-direct {p0, p3}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2080544
    iput-object p1, p0, LX/E6l;->a:LX/6RZ;

    .line 2080545
    iput-object p2, p0, LX/E6l;->b:LX/E1i;

    .line 2080546
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 2

    .prologue
    .line 2080547
    iget-object v0, p0, LX/E6l;->b:LX/E1i;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v1

    invoke-interface {v1}, LX/7oY;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E1i;->c(Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 7

    .prologue
    .line 2080548
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v1

    .line 2080549
    const v0, 0x7f031103

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2080550
    const v2, 0x7f0e01f5

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2080551
    const v2, 0x7f0e01f3

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 2080552
    invoke-interface {v1}, LX/7oX;->eR_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2080553
    iget-object v2, p0, LX/E6l;->a:LX/6RZ;

    .line 2080554
    invoke-interface {v1}, LX/7oX;->eQ_()Z

    move-result v3

    invoke-interface {v1}, LX/7oX;->j()J

    move-result-wide v5

    invoke-static {v5, v6}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v4

    invoke-interface {v1}, LX/7oX;->b()J

    move-result-wide v5

    invoke-static {v5, v6}, LX/5O7;->c(J)Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2080555
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2080556
    sget-object v2, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2080557
    new-instance v3, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;

    new-instance v4, Landroid/view/ContextThemeWrapper;

    .line 2080558
    iget-object v5, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v5, v5

    .line 2080559
    const v6, 0x7f0e08f7

    invoke-direct {v4, v5, v6}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v3, v4}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;-><init>(Landroid/content/Context;)V

    .line 2080560
    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->setGravity(I)V

    .line 2080561
    iget-object v4, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v4, v4

    .line 2080562
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0099

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2080563
    new-instance v5, LX/6VC;

    invoke-direct {v5, v4, v4}, LX/6VC;-><init>(II)V

    invoke-virtual {v3, v5}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2080564
    invoke-interface {v1}, LX/7oX;->c()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v1}, LX/7oX;->j()J

    move-result-wide v5

    invoke-static {v5, v6}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v5

    const-string v6, "reaction_events"

    invoke-virtual {v3, v4, v5, v6}, Lcom/facebook/events/widget/eventrow/EventRowProfilePictureView;->a(Landroid/net/Uri;Ljava/util/Date;Ljava/lang/String;)V

    .line 2080565
    move-object v1, v3

    .line 2080566
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 2080567
    new-instance v1, LX/8sz;

    invoke-direct {v1}, LX/8sz;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2080568
    return-object v0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 2

    .prologue
    .line 2080569
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    invoke-interface {v0}, LX/7oY;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    invoke-interface {v0}, LX/7oY;->eR_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    invoke-interface {v0}, LX/7oY;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/5O7;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    invoke-interface {v0}, LX/7oY;->c()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->o()LX/7oY;

    move-result-object v0

    invoke-interface {v0}, LX/7oY;->c()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
