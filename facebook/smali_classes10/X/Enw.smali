.class public final LX/Enw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/Enp;",
        "LX/Enp;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Enz;


# direct methods
.method public constructor <init>(LX/Enz;)V
    .locals 0

    .prologue
    .line 2167618
    iput-object p1, p0, LX/Enw;->a:LX/Enz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2167619
    check-cast p1, LX/Enp;

    .line 2167620
    iget-object v0, p0, LX/Enw;->a:LX/Enz;

    .line 2167621
    iget-object v1, v0, LX/Enz;->B:LX/Emx;

    .line 2167622
    iget-object v2, v1, LX/Emx;->e:Ljava/lang/String;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2167623
    if-nez v1, :cond_0

    .line 2167624
    iget-object v1, p1, LX/Enp;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2167625
    if-eqz v1, :cond_0

    .line 2167626
    iget-object v1, v0, LX/Enz;->B:LX/Emx;

    .line 2167627
    iget-object v2, p1, LX/Enp;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2167628
    invoke-virtual {v1, v2}, LX/Emx;->b(Ljava/lang/String;)V

    .line 2167629
    :cond_0
    iget-object v1, v0, LX/Enz;->u:LX/Emq;

    sget-object v2, LX/EnB;->FINAL:LX/EnB;

    invoke-virtual {v1, v2}, LX/Emq;->a(LX/EnB;)V

    .line 2167630
    iget-object v1, v0, LX/Enz;->v:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2167631
    invoke-static {v0}, LX/Enz;->q(LX/Enz;)LX/0Px;

    move-result-object v1

    .line 2167632
    iget-object v2, p1, LX/Enp;->b:LX/0P1;

    move-object v2, v2

    .line 2167633
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v5, :cond_1

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2167634
    invoke-virtual {v2, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 2167635
    iget-object p0, v0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    invoke-static {p0}, LX/0PB;->checkArgument(Z)V

    .line 2167636
    iget-object p0, v0, LX/Enz;->h:Ljava/util/HashMap;

    invoke-virtual {p0, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167637
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2167638
    :cond_1
    :goto_2
    iget-object v1, v0, LX/Enl;->e:LX/Eno;

    move-object v1, v1

    .line 2167639
    sget-object v2, LX/Eno;->INITIAL_ENTITIES_LOADED:LX/Eno;

    .line 2167640
    iput-object v2, v0, LX/Enl;->e:LX/Eno;

    .line 2167641
    invoke-static {v0}, LX/Enz;->o(LX/Enz;)LX/Ens;

    move-result-object v2

    .line 2167642
    iget-object v3, v0, LX/Enl;->e:LX/Eno;

    move-object v3, v3

    .line 2167643
    invoke-virtual {v0, v2, v1, v3}, LX/Enl;->a(LX/Ens;LX/Eno;LX/Eno;)V

    .line 2167644
    iget-object v1, p1, LX/Enp;->a:LX/En3;

    move-object v1, v1

    .line 2167645
    invoke-static {v0, v1}, LX/Enz;->a(LX/Enz;LX/En2;)LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Enl;->a(LX/0P1;)V

    .line 2167646
    return-object p1

    .line 2167647
    :cond_2
    iget-object v1, p1, LX/Enp;->a:LX/En3;

    move-object v1, v1

    .line 2167648
    iget-object v2, p1, LX/Enp;->b:LX/0P1;

    move-object v2, v2

    .line 2167649
    invoke-static {v0, v1, v2}, LX/Enz;->a(LX/Enz;LX/En3;LX/0P1;)V

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method
