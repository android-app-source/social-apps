.class public final LX/DKG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:LX/DKH;


# direct methods
.method public constructor <init>(LX/DKH;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1986823
    iput-object p1, p0, LX/DKG;->b:LX/DKH;

    iput-object p2, p0, LX/DKG;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1986822
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1986813
    check-cast p1, Ljava/lang/String;

    .line 1986814
    iget-object v0, p0, LX/DKG;->b:LX/DKH;

    iget-object v0, v0, LX/DKH;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082f58

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1986815
    iget-object v0, p0, LX/DKG;->a:Landroid/net/Uri;

    .line 1986816
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1986817
    :cond_0
    :goto_0
    iget-object v0, p0, LX/DKG;->b:LX/DKH;

    iget-object v0, v0, LX/DKH;->b:LX/DKL;

    new-instance v1, LX/DKK;

    invoke-direct {v1, p1}, LX/DKK;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1986818
    return-void

    .line 1986819
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1986820
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1986821
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method
