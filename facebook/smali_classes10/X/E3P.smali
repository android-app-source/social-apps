.class public final LX/E3P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E2S;

.field public final synthetic b:LX/9uc;

.field public final synthetic c:LX/1Pq;

.field public final synthetic d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;LX/E2S;LX/9uc;LX/1Pq;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2074149
    iput-object p1, p0, LX/E3P;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;

    iput-object p2, p0, LX/E3P;->a:LX/E2S;

    iput-object p3, p0, LX/E3P;->b:LX/9uc;

    iput-object p4, p0, LX/E3P;->c:LX/1Pq;

    iput-object p5, p0, LX/E3P;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x47e5a64a    # -3.680001E-5f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2074150
    iget-object v1, p0, LX/E3P;->a:LX/E2S;

    sget-object v2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v1, v2}, LX/E2S;->a(LX/03R;)V

    .line 2074151
    iget-object v1, p0, LX/E3P;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;

    iget-object v1, v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->c:LX/3U3;

    iget-object v2, p0, LX/E3P;->b:LX/9uc;

    invoke-interface {v2}, LX/9uc;->af()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$CrisisModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitCrisisResponseComponentFragmentModel$CrisisModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2074152
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2074153
    new-instance v3, LX/4E0;

    invoke-direct {v3}, LX/4E0;-><init>()V

    invoke-virtual {v3, v2}, LX/4E0;->a(Ljava/lang/String;)LX/4E0;

    move-result-object v3

    .line 2074154
    new-instance v5, LX/9tv;

    invoke-direct {v5}, LX/9tv;-><init>()V

    move-object v5, v5

    .line 2074155
    const-string p1, "input"

    invoke-virtual {v5, p1, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/9tv;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2074156
    iget-object v5, v1, LX/3U3;->a:LX/0tX;

    invoke-virtual {v5, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2074157
    :cond_0
    iget-object v2, p0, LX/E3P;->c:LX/1Pq;

    iget-object v3, p0, LX/E3P;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074158
    move-object v5, v2

    check-cast v5, LX/2kk;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLSelectedActionState;->UNSELECTED:Lcom/facebook/graphql/enums/GraphQLSelectedActionState;

    invoke-static {v5, v3, p0}, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionCrisisResponseUnitComponentPartDefinition;->a(LX/2kk;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/enums/GraphQLSelectedActionState;)V

    move-object v5, v2

    .line 2074159
    check-cast v5, LX/2kk;

    sget-object p0, LX/Cfc;->CRISIS_UNMARK:LX/Cfc;

    invoke-virtual {p0}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v5, v3, p0}, LX/2kk;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V

    .line 2074160
    const/4 v5, 0x1

    new-array v5, v5, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p0, 0x0

    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    aput-object v1, v5, p0

    invoke-interface {v2, v5}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2074161
    const v1, -0x41a51290

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
