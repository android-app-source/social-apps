.class public LX/Dby;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Dby;


# instance fields
.field public final a:LX/0i4;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/9iY;


# direct methods
.method public constructor <init>(LX/0i4;Lcom/facebook/content/SecureContextHelper;LX/9iY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2017762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2017763
    iput-object p1, p0, LX/Dby;->a:LX/0i4;

    .line 2017764
    iput-object p2, p0, LX/Dby;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2017765
    iput-object p3, p0, LX/Dby;->c:LX/9iY;

    .line 2017766
    return-void
.end method

.method public static a(LX/0QB;)LX/Dby;
    .locals 6

    .prologue
    .line 2017773
    sget-object v0, LX/Dby;->d:LX/Dby;

    if-nez v0, :cond_1

    .line 2017774
    const-class v1, LX/Dby;

    monitor-enter v1

    .line 2017775
    :try_start_0
    sget-object v0, LX/Dby;->d:LX/Dby;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2017776
    if-eqz v2, :cond_0

    .line 2017777
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2017778
    new-instance p0, LX/Dby;

    const-class v3, LX/0i4;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/0i4;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/9iY;->b(LX/0QB;)LX/9iY;

    move-result-object v5

    check-cast v5, LX/9iY;

    invoke-direct {p0, v3, v4, v5}, LX/Dby;-><init>(LX/0i4;Lcom/facebook/content/SecureContextHelper;LX/9iY;)V

    .line 2017779
    move-object v0, p0

    .line 2017780
    sput-object v0, LX/Dby;->d:LX/Dby;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2017781
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2017782
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2017783
    :cond_1
    sget-object v0, LX/Dby;->d:LX/Dby;

    return-object v0

    .line 2017784
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2017785
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/Dby;Landroid/support/v4/app/Fragment;Ljava/lang/Long;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Ljava/lang/Long;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2017767
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2017768
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2017769
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2017770
    const-string v1, "extra_media_items"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2017771
    iget-object v1, p0, LX/Dby;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x6591

    invoke-interface {v1, v0, v2, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2017772
    return-void
.end method
