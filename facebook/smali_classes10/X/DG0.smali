.class public final LX/DG0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20Z;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:LX/1Pr;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:LX/Aox;

.field public final synthetic e:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;LX/Aox;)V
    .locals 0

    .prologue
    .line 1979145
    iput-object p1, p0, LX/DG0;->e:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    iput-object p2, p0, LX/DG0;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p3, p0, LX/DG0;->b:LX/1Pr;

    iput-object p4, p0, LX/DG0;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p5, p0, LX/DG0;->d:LX/Aox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/20X;)V
    .locals 8

    .prologue
    .line 1979146
    sget-object v0, LX/DG1;->a:[I

    invoke-virtual {p2}, LX/20X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1979147
    :cond_0
    :goto_0
    return-void

    .line 1979148
    :pswitch_0
    iget-object v0, p0, LX/DG0;->e:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    iget-object v1, p0, LX/DG0;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v2, p0, LX/DG0;->b:LX/1Pr;

    .line 1979149
    check-cast v2, LX/1Po;

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-static {v3}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object v3

    .line 1979150
    const-string v4, "linkSetsShareFooter"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v5

    invoke-virtual {v5}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    sget-object v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    .line 1979151
    iget-object v4, v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->c:LX/1Kf;

    const/4 v5, 0x0

    iget-object p1, v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->d:Landroid/content/Context;

    invoke-interface {v4, v5, v3, p1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1979152
    iget-object v0, p0, LX/DG0;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1979153
    iget-object v0, p0, LX/DG0;->e:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    iget-object v2, v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->h:LX/2yK;

    sget-object v3, LX/0ig;->z:LX/0ih;

    iget-object v0, p0, LX/DG0;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    const/4 v4, 0x6

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "position:"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/DG0;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v0, v4, v1}, LX/2yK;->a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;ILjava/lang/String;)V

    goto :goto_0

    .line 1979154
    :pswitch_1
    iget-object v0, p0, LX/DG0;->e:Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;

    iget-object v1, p0, LX/DG0;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/DG0;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p0, LX/DG0;->d:LX/Aox;

    iget-object v4, p0, LX/DG0;->b:LX/1Pr;

    .line 1979155
    iget-object v5, v3, LX/Aox;->a:Ljava/lang/Boolean;

    move-object v5, v5

    .line 1979156
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1979157
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, LX/Aox;->a(Z)V

    .line 1979158
    iget-object v5, v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->e:LX/5up;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v6

    const-string v7, "native_feed_article_set"

    const-string p0, "toggle_button"

    new-instance p1, LX/DG2;

    invoke-direct {p1, v0, v3, v4}, LX/DG2;-><init>(Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;LX/Aox;LX/1Pr;)V

    invoke-virtual {v5, v6, v7, p0, p1}, LX/5up;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 1979159
    :goto_1
    if-eqz v4, :cond_1

    .line 1979160
    check-cast v4, LX/1Pq;

    invoke-interface {v4}, LX/1Pq;->iN_()V

    .line 1979161
    :cond_1
    goto/16 :goto_0

    .line 1979162
    :cond_2
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, LX/Aox;->a(Z)V

    .line 1979163
    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1979164
    iget-object v7, v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->h:LX/2yK;

    sget-object p0, LX/0ig;->z:LX/0ih;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStorySet;

    const/4 p1, 0x7

    new-instance p2, Ljava/lang/StringBuilder;

    const-string v6, "position:"

    invoke-direct {p2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v6

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, p0, v5, p1, v6}, LX/2yK;->a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;ILjava/lang/String;)V

    .line 1979165
    :cond_3
    iget-object v5, v0, Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;->e:LX/5up;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v6

    const-string v7, "native_feed_article_set"

    const-string p0, "toggle_button"

    new-instance p1, LX/DG2;

    invoke-direct {p1, v0, v3, v4}, LX/DG2;-><init>(Lcom/facebook/feedplugins/storyset/rows/LinkSetsShareSaveFooterPartDefinition;LX/Aox;LX/1Pr;)V

    invoke-virtual {v5, v6, v7, p0, p1}, LX/5up;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
