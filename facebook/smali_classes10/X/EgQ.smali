.class public final LX/EgQ;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/EgS;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

.field public final synthetic c:LX/EgS;


# direct methods
.method public constructor <init>(LX/EgS;)V
    .locals 1

    .prologue
    .line 2156092
    iput-object p1, p0, LX/EgQ;->c:LX/EgS;

    .line 2156093
    move-object v0, p1

    .line 2156094
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2156095
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2156096
    if-ne p0, p1, :cond_1

    .line 2156097
    :cond_0
    :goto_0
    return v0

    .line 2156098
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2156099
    goto :goto_0

    .line 2156100
    :cond_3
    check-cast p1, LX/EgQ;

    .line 2156101
    iget-object v2, p0, LX/EgQ;->b:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/EgQ;->b:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    iget-object v3, p1, LX/EgQ;->b:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2156102
    goto :goto_0

    .line 2156103
    :cond_4
    iget-object v2, p1, LX/EgQ;->b:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
