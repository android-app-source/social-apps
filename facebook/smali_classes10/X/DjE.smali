.class public final LX/DjE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V
    .locals 0

    .prologue
    .line 2032931
    iput-object p1, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v7, 0x0

    const/4 v0, 0x1

    const v1, 0x69b6ce

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2032932
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->p:Ljava/util/Calendar;

    if-nez v0, :cond_1

    .line 2032933
    :cond_0
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082bb4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2032934
    const v0, -0x3d9205cf

    invoke-static {v3, v3, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2032935
    :goto_0
    return-void

    .line 2032936
    :cond_1
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-static {v0}, LX/Djl;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)LX/Djl;

    move-result-object v3

    .line 2032937
    iget-object v0, v3, LX/Djl;->a:Ljava/lang/Integer;

    move-object v0, v0

    .line 2032938
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    iget-object v2, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->m:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    div-long/2addr v4, v8

    cmp-long v0, v0, v4

    if-gtz v0, :cond_2

    .line 2032939
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082bb5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2032940
    const v0, 0x6d9a77f7

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto :goto_0

    .line 2032941
    :cond_2
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->j:LX/DkO;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/DkO;->a(LX/DkQ;)LX/DkN;

    move-result-object v1

    .line 2032942
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->n:LX/DkN;

    .line 2032943
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-boolean v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->a:Z

    if-nez v0, :cond_3

    .line 2032944
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->h:LX/Dih;

    iget-object v1, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->c:Ljava/lang/String;

    iget-object v2, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->o:Ljava/lang/String;

    iget-object v4, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->b:Ljava/lang/String;

    .line 2032945
    iget-object v5, v0, LX/Dih;->a:LX/0Zb;

    const-string v7, "profservices_booking_admin_schedule_appointment"

    invoke-static {v7, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "referrer"

    invoke-virtual {v7, v8, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "request_id"

    invoke-virtual {v7, v8, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v5, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032946
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->n:LX/DkN;

    iget-object v1, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->b:Ljava/lang/String;

    iget-object v2, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->e:Ljava/lang/String;

    new-instance v4, LX/DjC;

    invoke-direct {v4, p0}, LX/DjC;-><init>(LX/DjE;)V

    .line 2032947
    iget-object v5, v0, LX/DkN;->b:LX/1Ck;

    const-string v7, "admin_schedule_appointment"

    new-instance v8, LX/DkA;

    invoke-direct {v8, v0, v1, v3, v2}, LX/DkA;-><init>(LX/DkN;Ljava/lang/String;LX/Djl;Ljava/lang/String;)V

    new-instance v9, LX/DkB;

    invoke-direct {v9, v0, v4}, LX/DkB;-><init>(LX/DkN;LX/DjB;)V

    invoke-virtual {v5, v7, v8, v9}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2032948
    :goto_1
    const v0, 0x7a45778d

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2032949
    :cond_3
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-boolean v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->n:Z

    if-nez v0, :cond_4

    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2032950
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082bbb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2032951
    const v0, 0x6b5c6426

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2032952
    :cond_4
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->h:LX/Dih;

    iget-object v1, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->c:Ljava/lang/String;

    iget-object v2, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->o:Ljava/lang/String;

    .line 2032953
    iget-object v4, v0, LX/Dih;->a:LX/0Zb;

    const-string v5, "profservices_booking_admin_create_appointment"

    invoke-static {v5, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v7, "referrer"

    invoke-virtual {v5, v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032954
    iget-object v0, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->n:LX/DkN;

    iget-object v1, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->c:Ljava/lang/String;

    iget-object v2, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->d:Ljava/lang/String;

    iget-object v4, p0, LX/DjE;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    iget-object v4, v4, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->m:Ljava/lang/String;

    new-instance v5, LX/DjD;

    invoke-direct {v5, p0}, LX/DjD;-><init>(LX/DjE;)V

    invoke-virtual/range {v0 .. v5}, LX/DkN;->a(Ljava/lang/String;Ljava/lang/String;LX/Djl;Ljava/lang/String;LX/DjB;)V

    goto :goto_1
.end method
