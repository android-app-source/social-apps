.class public LX/EtI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements LX/3us;
.implements LX/5OA;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/3v0;

.field private final c:LX/EtJ;

.field private d:LX/34c;

.field public e:LX/3uE;

.field private f:LX/3Af;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3v0;LX/EtJ;)V
    .locals 0

    .prologue
    .line 2177573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2177574
    iput-object p1, p0, LX/EtI;->a:Landroid/content/Context;

    .line 2177575
    iput-object p2, p0, LX/EtI;->b:LX/3v0;

    .line 2177576
    iput-object p3, p0, LX/EtI;->c:LX/EtJ;

    .line 2177577
    invoke-virtual {p2, p0}, LX/3v0;->a(LX/3us;)V

    .line 2177578
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2177563
    iget-object v0, p0, LX/EtI;->c:LX/EtJ;

    invoke-interface {v0}, LX/EtJ;->a()LX/34c;

    move-result-object v0

    iput-object v0, p0, LX/EtI;->d:LX/34c;

    .line 2177564
    iget-object v0, p0, LX/EtI;->d:LX/34c;

    invoke-virtual {v0, p0}, LX/34c;->a(LX/5OA;)V

    .line 2177565
    iget-object v0, p0, LX/EtI;->b:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->m()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 2177566
    iget-object v4, p0, LX/EtI;->d:LX/34c;

    invoke-virtual {v4, v0}, LX/34c;->c(Landroid/view/MenuItem;)V

    .line 2177567
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2177568
    :cond_0
    new-instance v0, LX/3Af;

    iget-object v1, p0, LX/EtI;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/3Af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/EtI;->f:LX/3Af;

    .line 2177569
    iget-object v0, p0, LX/EtI;->f:LX/3Af;

    invoke-virtual {v0, p0}, LX/3Af;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2177570
    iget-object v0, p0, LX/EtI;->f:LX/3Af;

    iget-object v1, p0, LX/EtI;->d:LX/34c;

    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 2177571
    iget-object v0, p0, LX/EtI;->f:LX/3Af;

    invoke-virtual {v0}, LX/3Af;->show()V

    .line 2177572
    return-void
.end method

.method public final a(LX/3v0;Z)V
    .locals 1

    .prologue
    .line 2177558
    iget-object v0, p0, LX/EtI;->b:LX/3v0;

    if-eq p1, v0, :cond_1

    .line 2177559
    :cond_0
    :goto_0
    return-void

    .line 2177560
    :cond_1
    invoke-virtual {p0}, LX/EtI;->c()V

    .line 2177561
    iget-object v0, p0, LX/EtI;->e:LX/3uE;

    if-eqz v0, :cond_0

    .line 2177562
    iget-object v0, p0, LX/EtI;->e:LX/3uE;

    invoke-interface {v0, p1, p2}, LX/3uE;->a(LX/3v0;Z)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;LX/3v0;)V
    .locals 0

    .prologue
    .line 2177557
    return-void
.end method

.method public final a(LX/3vG;)Z
    .locals 3

    .prologue
    .line 2177536
    invoke-virtual {p1}, LX/3v0;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2177537
    new-instance v0, LX/EtI;

    iget-object v1, p0, LX/EtI;->a:Landroid/content/Context;

    iget-object v2, p0, LX/EtI;->c:LX/EtJ;

    invoke-direct {v0, v1, p1, v2}, LX/EtI;-><init>(Landroid/content/Context;LX/3v0;LX/EtJ;)V

    .line 2177538
    iget-object v1, p0, LX/EtI;->e:LX/3uE;

    .line 2177539
    iput-object v1, v0, LX/EtI;->e:LX/3uE;

    .line 2177540
    invoke-virtual {v0}, LX/EtI;->a()V

    .line 2177541
    iget-object v0, p0, LX/EtI;->e:LX/3uE;

    if-eqz v0, :cond_0

    .line 2177542
    iget-object v0, p0, LX/EtI;->e:LX/3uE;

    invoke-interface {v0, p1}, LX/3uE;->a_(LX/3v0;)Z

    .line 2177543
    :cond_0
    const/4 v0, 0x1

    .line 2177544
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2177555
    iget-object v0, p0, LX/EtI;->b:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/3v0;->a(Landroid/view/MenuItem;I)Z

    .line 2177556
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2177579
    iget-object v0, p0, LX/EtI;->d:LX/34c;

    if-eqz v0, :cond_0

    .line 2177580
    iget-object v0, p0, LX/EtI;->d:LX/34c;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2177581
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2177554
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/3v3;)Z
    .locals 1

    .prologue
    .line 2177553
    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2177550
    invoke-virtual {p0}, LX/EtI;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2177551
    iget-object v0, p0, LX/EtI;->f:LX/3Af;

    invoke-virtual {v0}, LX/3Af;->dismiss()V

    .line 2177552
    :cond_0
    return-void
.end method

.method public final c(LX/3v3;)Z
    .locals 1

    .prologue
    .line 2177549
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2177548
    iget-object v0, p0, LX/EtI;->f:LX/3Af;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EtI;->f:LX/3Af;

    invoke-virtual {v0}, LX/3Af;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 2177545
    const/4 v0, 0x0

    iput-object v0, p0, LX/EtI;->f:LX/3Af;

    .line 2177546
    iget-object v0, p0, LX/EtI;->b:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->close()V

    .line 2177547
    return-void
.end method
