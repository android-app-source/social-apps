.class public final LX/Eqt;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesTokenQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Equ;


# direct methods
.method public constructor <init>(LX/Equ;)V
    .locals 0

    .prologue
    .line 2172237
    iput-object p1, p0, LX/Eqt;->a:LX/Equ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2172214
    iget-object v0, p0, LX/Eqt;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->u:LX/03V;

    sget-object v1, LX/Equ;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch Entries Tokens"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2172215
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2172216
    move-object v1, v1

    .line 2172217
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2172218
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2172219
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesTokenQueryModel;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2172220
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 2172221
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Server returned null"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/Eqt;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2172222
    :goto_1
    return-void

    .line 2172223
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesTokenQueryModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2172224
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesTokenQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2172225
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2172226
    const v3, 0x3d76496b

    invoke-static {v2, v0, v1, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2172227
    iget-object v1, p0, LX/Eqt;->a:LX/Equ;

    iget-object v1, v1, LX/Equ;->n:LX/Eqg;

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_2
    invoke-static {v0}, LX/Equ;->b(LX/2uF;)LX/0Px;

    move-result-object v0

    const/4 v4, 0x0

    .line 2172228
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v3, v4

    :goto_3
    if-ge v3, v5, :cond_5

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2172229
    invoke-virtual {v2}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object p0

    .line 2172230
    if-eqz p0, :cond_4

    iget-object p1, v1, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object p1, p1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->F:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    iget-object p1, v1, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object p1, p1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->n:Ljava/util/ArrayList;

    if-eqz p1, :cond_3

    iget-object p1, v1, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object p1, p1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    :cond_3
    iget-object p1, v1, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object p1, p1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 2172231
    iget-object p1, v1, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object p1, p1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->a:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2172232
    iget-object p0, v1, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object p0, p0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->e:LX/EqG;

    const/4 p1, 0x1

    invoke-virtual {p0, v2, p1, v4}, LX/EqG;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;ZZ)V

    .line 2172233
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 2172234
    :cond_5
    goto :goto_1

    .line 2172235
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2172236
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2
.end method
