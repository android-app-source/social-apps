.class public final LX/DNS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "LX/44w",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedHomeStories;",
        "LX/0ta;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1991675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1991676
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1991677
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedResult;

    .line 1991678
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1991679
    new-instance v1, LX/44w;

    .line 1991680
    iget-object v2, v0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v2, v2

    .line 1991681
    iget-object p0, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, p0

    .line 1991682
    invoke-direct {v1, v2, v0}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method
