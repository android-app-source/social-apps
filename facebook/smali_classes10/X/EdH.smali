.class public LX/EdH;
.super Landroid/app/Service;
.source ""


# static fields
.field private static volatile a:I

.field private static volatile b:Z

.field private static volatile c:Landroid/os/PowerManager$WakeLock;

.field private static final d:Ljava/lang/Object;

.field public static volatile e:LX/Ed0;

.field public static volatile f:LX/Ed5;

.field public static volatile g:LX/Ed8;

.field private static volatile h:I


# instance fields
.field public i:[Ljava/util/concurrent/ExecutorService;

.field private j:I

.field private k:I

.field public l:LX/EdG;

.field private final m:Landroid/os/Handler;

.field private final n:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2149176
    const/4 v0, 0x4

    sput v0, LX/EdH;->a:I

    .line 2149177
    const/4 v0, 0x1

    sput-boolean v0, LX/EdH;->b:Z

    .line 2149178
    sput-object v1, LX/EdH;->c:Landroid/os/PowerManager$WakeLock;

    .line 2149179
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/EdH;->d:Ljava/lang/Object;

    .line 2149180
    sput-object v1, LX/EdH;->e:LX/Ed0;

    .line 2149181
    sput-object v1, LX/EdH;->f:LX/Ed5;

    .line 2149182
    sput-object v1, LX/EdH;->g:LX/Ed8;

    .line 2149183
    const/4 v0, -0x1

    sput v0, LX/EdH;->h:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2149172
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 2149173
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, LX/EdH;->i:[Ljava/util/concurrent/ExecutorService;

    .line 2149174
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/EdH;->m:Landroid/os/Handler;

    .line 2149175
    new-instance v0, Landroid_src/mmsv2/MmsService$1;

    invoke-direct {v0, p0}, Landroid_src/mmsv2/MmsService$1;-><init>(LX/EdH;)V

    iput-object v0, p0, LX/EdH;->n:Ljava/lang/Runnable;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid_src/mmsv2/MmsRequest;)V
    .locals 4

    .prologue
    .line 2149161
    sget-boolean v0, LX/EdH;->b:Z

    .line 2149162
    iput-boolean v0, p1, Landroid_src/mmsv2/MmsRequest;->e:Z

    .line 2149163
    new-instance v1, Landroid/content/Intent;

    const-class v2, LX/EdH;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2149164
    const-string v2, "request"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2149165
    const-string v2, "mypid"

    invoke-static {}, LX/EdH;->g()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2149166
    if-eqz v0, :cond_0

    .line 2149167
    invoke-static {p0}, LX/EdH;->b(Landroid/content/Context;)V

    .line 2149168
    :cond_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2149169
    if-eqz v0, :cond_1

    .line 2149170
    invoke-static {}, LX/EdH;->e()V

    .line 2149171
    :cond_1
    return-void
.end method

.method private a(Landroid_src/mmsv2/MmsRequest;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 2149058
    instance-of v0, p1, Landroid_src/mmsv2/SendRequest;

    if-eqz v0, :cond_0

    .line 2149059
    iget-object v0, p0, LX/EdH;->i:[Ljava/util/concurrent/ExecutorService;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 2149060
    :goto_0
    move-object v0, v0

    .line 2149061
    monitor-enter p0

    .line 2149062
    const v1, -0xffc8dce

    :try_start_0
    invoke-static {v0, p2, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2149063
    iget v0, p0, LX/EdH;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EdH;->j:I

    .line 2149064
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    iget-object v0, p0, LX/EdH;->i:[Ljava/util/concurrent/ExecutorService;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2149155
    sget-object v1, LX/EdH;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 2149156
    :try_start_0
    sget-object v0, LX/EdH;->c:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 2149157
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 2149158
    const/4 v2, 0x1

    const-string v3, "mmslib_wakelock"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, LX/EdH;->c:Landroid/os/PowerManager$WakeLock;

    .line 2149159
    :cond_0
    sget-object v0, LX/EdH;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 2149160
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static e()V
    .locals 3

    .prologue
    .line 2149145
    const/4 v0, 0x0

    .line 2149146
    sget-object v1, LX/EdH;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 2149147
    :try_start_0
    sget-object v2, LX/EdH;->c:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_1

    .line 2149148
    sget-object v2, LX/EdH;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2149149
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2149150
    if-eqz v0, :cond_0

    .line 2149151
    const-string v0, "MmsLib"

    const-string v1, "Releasing empty wake lock"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2149152
    :cond_0
    return-void

    .line 2149153
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2149154
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static f()V
    .locals 2

    .prologue
    .line 2149137
    sget-object v1, LX/EdH;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 2149138
    :try_start_0
    sget-object v0, LX/EdH;->c:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    sget-object v0, LX/EdH;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2149139
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2149140
    if-eqz v0, :cond_0

    .line 2149141
    const-string v0, "MmsLib"

    const-string v1, "Wake lock still held!"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2149142
    :cond_0
    return-void

    .line 2149143
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2149144
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static g()I
    .locals 1

    .prologue
    .line 2149134
    sget v0, LX/EdH;->h:I

    if-gez v0, :cond_0

    .line 2149135
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    sput v0, LX/EdH;->h:I

    .line 2149136
    :cond_0
    sget v0, LX/EdH;->h:I

    return v0
.end method

.method public static h(LX/EdH;)V
    .locals 1

    .prologue
    .line 2149128
    monitor-enter p0

    .line 2149129
    :try_start_0
    iget v0, p0, LX/EdH;->j:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/EdH;->j:I

    .line 2149130
    iget v0, p0, LX/EdH;->j:I

    if-gtz v0, :cond_0

    .line 2149131
    const/4 v0, 0x0

    iput v0, p0, LX/EdH;->j:I

    .line 2149132
    invoke-static {p0}, LX/EdH;->j(LX/EdH;)V

    .line 2149133
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 2149124
    monitor-enter p0

    .line 2149125
    :try_start_0
    iget v0, p0, LX/EdH;->j:I

    if-nez v0, :cond_0

    .line 2149126
    invoke-static {p0}, LX/EdH;->j(LX/EdH;)V

    .line 2149127
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static j(LX/EdH;)V
    .locals 5

    .prologue
    .line 2149121
    iget-object v0, p0, LX/EdH;->m:Landroid/os/Handler;

    iget-object v1, p0, LX/EdH;->n:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2149122
    iget-object v0, p0, LX/EdH;->m:Landroid/os/Handler;

    iget-object v1, p0, LX/EdH;->n:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    const v4, 0x762c2f98

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2149123
    return-void
.end method

.method public static k(LX/EdH;)V
    .locals 2

    .prologue
    .line 2149111
    const/4 v0, 0x0

    .line 2149112
    monitor-enter p0

    .line 2149113
    :try_start_0
    iget v1, p0, LX/EdH;->j:I

    if-nez v1, :cond_0

    .line 2149114
    iget v0, p0, LX/EdH;->k:I

    invoke-virtual {p0, v0}, LX/EdH;->stopSelfResult(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2149115
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2149116
    if-eqz v0, :cond_1

    .line 2149117
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2149118
    invoke-static {}, LX/EdH;->f()V

    .line 2149119
    :cond_1
    return-void

    .line 2149120
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2149110
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/16 v2, 0x24

    const v3, 0x78177c31

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2149093
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 2149094
    sget-object v2, LX/EdH;->g:LX/Ed8;

    if-nez v2, :cond_0

    .line 2149095
    new-instance v2, LX/Ed8;

    invoke-direct {v2, p0}, LX/Ed8;-><init>(Landroid/content/Context;)V

    sput-object v2, LX/EdH;->g:LX/Ed8;

    .line 2149096
    :cond_0
    sget-object v2, LX/EdH;->e:LX/Ed0;

    if-nez v2, :cond_1

    .line 2149097
    new-instance v2, LX/Ed7;

    invoke-direct {v2, p0}, LX/Ed7;-><init>(Landroid/content/Context;)V

    sput-object v2, LX/EdH;->e:LX/Ed0;

    .line 2149098
    :cond_1
    sget-object v2, LX/EdH;->f:LX/Ed5;

    if-nez v2, :cond_2

    .line 2149099
    new-instance v2, LX/Ed5;

    invoke-direct {v2, p0}, LX/Ed5;-><init>(Landroid/content/Context;)V

    sput-object v2, LX/EdH;->f:LX/Ed5;

    .line 2149100
    :cond_2
    :goto_0
    iget-object v2, p0, LX/EdH;->i:[Ljava/util/concurrent/ExecutorService;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2149101
    iget-object v2, p0, LX/EdH;->i:[Ljava/util/concurrent/ExecutorService;

    sget v3, LX/EdH;->a:I

    invoke-static {v3}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    aput-object v3, v2, v0

    .line 2149102
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2149103
    :cond_3
    new-instance v0, LX/EdG;

    invoke-direct {v0, p0}, LX/EdG;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/EdH;->l:LX/EdG;

    .line 2149104
    monitor-enter p0

    .line 2149105
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, LX/EdH;->j:I

    .line 2149106
    const/4 v0, -0x1

    iput v0, p0, LX/EdH;->k:I

    .line 2149107
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2149108
    const v0, -0x2a88e193

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void

    .line 2149109
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const v2, -0x44648b31

    invoke-static {v2, v1}, LX/02F;->d(II)V

    throw v0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x13c33ee2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2149088
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 2149089
    iget-object v2, p0, LX/EdH;->i:[Ljava/util/concurrent/ExecutorService;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2149090
    invoke-interface {v4}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 2149091
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2149092
    :cond_0
    const v0, 0x60f1ac9d

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/16 v0, 0x24

    const v1, 0x30048ac9

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2149065
    monitor-enter p0

    .line 2149066
    :try_start_0
    iput p3, p0, LX/EdH;->k:I

    .line 2149067
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2149068
    if-eqz p1, :cond_4

    .line 2149069
    const-string v0, "mypid"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2149070
    invoke-static {}, LX/EdH;->g()I

    move-result v1

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2149071
    if-eqz v0, :cond_3

    .line 2149072
    const-string v0, "request"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid_src/mmsv2/MmsRequest;

    .line 2149073
    if-eqz v0, :cond_2

    .line 2149074
    :try_start_1
    new-instance v1, Landroid_src/mmsv2/MmsService$2;

    invoke-direct {v1, p0, v0}, Landroid_src/mmsv2/MmsService$2;-><init>(LX/EdH;Landroid_src/mmsv2/MmsRequest;)V

    invoke-direct {p0, v0, v1}, LX/EdH;->a(Landroid_src/mmsv2/MmsRequest;Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    move v4, v2

    .line 2149075
    :cond_0
    :goto_1
    if-nez v4, :cond_1

    .line 2149076
    invoke-direct {p0}, LX/EdH;->i()V

    .line 2149077
    :cond_1
    const v0, 0x5cc7f988

    invoke-static {v0, v6}, LX/02F;->d(II)V

    return v9

    .line 2149078
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const v1, 0x26de9ede

    invoke-static {v1, v6}, LX/02F;->d(II)V

    throw v0

    .line 2149079
    :catch_0
    move-exception v1

    .line 2149080
    const-string v5, "MmsLib"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Executing request failed "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    move-object v5, v3

    .line 2149081
    invoke-virtual/range {v0 .. v5}, Landroid_src/mmsv2/MmsRequest;->a(Landroid/content/Context;I[BILjava/lang/String;)V

    .line 2149082
    iget-boolean v1, v0, Landroid_src/mmsv2/MmsRequest;->e:Z

    move v0, v1

    .line 2149083
    if-eqz v0, :cond_0

    .line 2149084
    invoke-static {}, LX/EdH;->e()V

    goto :goto_1

    .line 2149085
    :cond_2
    const-string v0, "MmsLib"

    const-string v1, "Empty request"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2149086
    :cond_3
    const-string v0, "MmsLib"

    const-string v1, "Got a restarted intent from previous incarnation"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2149087
    :cond_4
    const-string v0, "MmsLib"

    const-string v1, "Empty intent"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
