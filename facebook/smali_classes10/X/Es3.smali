.class public LX/Es3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2174143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2174144
    if-nez p0, :cond_0

    move-object v0, v1

    .line 2174145
    :goto_0
    return-object v0

    .line 2174146
    :cond_0
    :try_start_0
    const/4 v3, 0x0

    .line 2174147
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174148
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2174149
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v2

    .line 2174150
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2174151
    new-instance v4, LX/4Ys;

    invoke-direct {v4}, LX/4Ys;-><init>()V

    new-instance v5, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;

    invoke-direct {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;-><init>()V

    const v6, -0x11f22ab2

    .line 2174152
    iput v6, v5, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a:I

    .line 2174153
    move-object v5, v5

    .line 2174154
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 2174155
    iput-object v5, v4, LX/4Ys;->bH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2174156
    move-object v4, v4

    .line 2174157
    invoke-virtual {v4}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 2174158
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2174159
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    invoke-static {v2}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 2174160
    invoke-static {v2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    .line 2174161
    iput-object v4, v2, LX/23u;->b:LX/0Px;

    .line 2174162
    move-object v2, v2

    .line 2174163
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2174164
    invoke-static {v2, v3}, LX/0x1;->a(LX/16g;LX/162;)V

    .line 2174165
    :goto_1
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    .line 2174166
    iput-object v4, v3, LX/23u;->m:Ljava/lang/String;

    .line 2174167
    move-object v3, v3

    .line 2174168
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 2174169
    iput-object v4, v3, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2174170
    move-object v3, v3

    .line 2174171
    new-instance v4, LX/173;

    invoke-direct {v4}, LX/173;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->F()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    .line 2174172
    iput-object v5, v4, LX/173;->f:Ljava/lang/String;

    .line 2174173
    move-object v4, v4

    .line 2174174
    invoke-virtual {v4}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 2174175
    iput-object v4, v3, LX/23u;->au:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2174176
    move-object v3, v3

    .line 2174177
    iput-object v2, v3, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2174178
    move-object v2, v3

    .line 2174179
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->c()Ljava/lang/String;

    move-result-object v0

    .line 2174180
    iput-object v0, v2, LX/23u;->aM:Ljava/lang/String;

    .line 2174181
    move-object v0, v2

    .line 2174182
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2174183
    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    move-object v2, v0

    .line 2174184
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174185
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2174186
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 2174187
    :catch_0
    move-object v0, v1

    goto/16 :goto_0

    :cond_1
    move-object v2, v3

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3

    .prologue
    .line 2174188
    new-instance v1, LX/39x;

    invoke-direct {v1}, LX/39x;-><init>()V

    .line 2174189
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v0

    .line 2174190
    iput-object v0, v1, LX/39x;->q:LX/0Px;

    .line 2174191
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->p()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    .line 2174192
    iput-object v0, v1, LX/39x;->p:LX/0Px;

    .line 2174193
    new-instance v0, LX/23u;

    invoke-direct {v0}, LX/23u;-><init>()V

    invoke-interface {p0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    .line 2174194
    iput-object v2, v0, LX/23u;->m:Ljava/lang/String;

    .line 2174195
    move-object v0, v0

    .line 2174196
    iput-object p2, v0, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2174197
    move-object v0, v0

    .line 2174198
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2174199
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 2174200
    move-object v0, v0

    .line 2174201
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method
