.class public LX/ElO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zh;

.field public final b:LX/ElN;

.field public c:LX/0n9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:I


# direct methods
.method public constructor <init>(LX/0Zh;ILjava/lang/String;LX/ElN;)V
    .locals 1

    .prologue
    .line 2164318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2164319
    iput-object p1, p0, LX/ElO;->a:LX/0Zh;

    .line 2164320
    iput-object p4, p0, LX/ElO;->b:LX/ElN;

    .line 2164321
    iput p2, p0, LX/ElO;->d:I

    .line 2164322
    const-string v0, "graph"

    .line 2164323
    iput-object v0, p4, LX/ElN;->e:Ljava/lang/String;

    .line 2164324
    const-string v0, "graphql"

    .line 2164325
    iput-object v0, p4, LX/ElN;->f:Ljava/lang/String;

    .line 2164326
    const-string v0, "query_id"

    invoke-virtual {p4, v0, p3}, LX/ElN;->a(Ljava/lang/String;Ljava/lang/String;)LX/ElN;

    .line 2164327
    return-void
.end method

.method private c()LX/0n9;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2164309
    iget-object v0, p0, LX/ElO;->c:LX/0n9;

    if-nez v0, :cond_0

    .line 2164310
    iget-object v0, p0, LX/ElO;->a:LX/0Zh;

    invoke-virtual {v0}, LX/0Zh;->b()LX/0n9;

    move-result-object v0

    iput-object v0, p0, LX/ElO;->c:LX/0n9;

    .line 2164311
    :cond_0
    iget-object v0, p0, LX/ElO;->c:LX/0n9;

    return-object v0
.end method


# virtual methods
.method public final a(JLjava/lang/Number;)LX/ElO;
    .locals 3
    .param p3    # Ljava/lang/Number;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2164312
    invoke-direct {p0}, LX/ElO;->c()LX/0n9;

    move-result-object v0

    iget v1, p0, LX/ElO;->d:I

    invoke-static {v1, p1, p2}, LX/ElT;->a(IJ)Ljava/lang/String;

    move-result-object v1

    .line 2164313
    invoke-static {v0, v1, p3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2164314
    return-object p0
.end method

.method public final a(JLjava/lang/String;)LX/ElO;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2164315
    invoke-direct {p0}, LX/ElO;->c()LX/0n9;

    move-result-object v0

    iget v1, p0, LX/ElO;->d:I

    invoke-static {v1, p1, p2}, LX/ElT;->a(IJ)Ljava/lang/String;

    move-result-object v1

    .line 2164316
    invoke-static {v0, v1, p3}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2164317
    return-object p0
.end method
