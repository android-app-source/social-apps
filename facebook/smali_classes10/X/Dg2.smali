.class public final LX/Dg2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2028842
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 2028843
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028844
    :goto_0
    return v1

    .line 2028845
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 2028846
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2028847
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2028848
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 2028849
    const-string v9, "invite_clicked"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2028850
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 2028851
    :cond_1
    const-string v9, "invite_converted"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2028852
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 2028853
    :cond_2
    const-string v9, "invitee_phone_number"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2028854
    const/4 v8, 0x0

    .line 2028855
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v9, :cond_c

    .line 2028856
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028857
    :goto_2
    move v5, v8

    .line 2028858
    goto :goto_1

    .line 2028859
    :cond_3
    const-string v9, "inviter"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2028860
    invoke-static {p0, p1}, LX/Dg1;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2028861
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2028862
    :cond_5
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2028863
    if-eqz v3, :cond_6

    .line 2028864
    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 2028865
    :cond_6
    if-eqz v0, :cond_7

    .line 2028866
    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 2028867
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2028868
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2028869
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1

    .line 2028870
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2028871
    :cond_a
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_b

    .line 2028872
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2028873
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2028874
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_a

    if-eqz v9, :cond_a

    .line 2028875
    const-string v10, "display_number"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 2028876
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 2028877
    :cond_b
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2028878
    invoke-virtual {p1, v8, v5}, LX/186;->b(II)V

    .line 2028879
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_2

    :cond_c
    move v5, v8

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2028880
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2028881
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2028882
    if-eqz v0, :cond_0

    .line 2028883
    const-string v1, "invite_clicked"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028884
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2028885
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2028886
    if-eqz v0, :cond_1

    .line 2028887
    const-string v1, "invite_converted"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028888
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2028889
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2028890
    if-eqz v0, :cond_3

    .line 2028891
    const-string v1, "invitee_phone_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028892
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2028893
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2028894
    if-eqz v1, :cond_2

    .line 2028895
    const-string p3, "display_number"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028896
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2028897
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2028898
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2028899
    if-eqz v0, :cond_4

    .line 2028900
    const-string v1, "inviter"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2028901
    invoke-static {p0, v0, p2}, LX/Dg1;->a(LX/15i;ILX/0nX;)V

    .line 2028902
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2028903
    return-void
.end method
