.class public LX/CjD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/CjD;


# instance fields
.field public final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1929197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929198
    iput-object p1, p0, LX/CjD;->a:LX/0tX;

    .line 1929199
    return-void
.end method

.method public static a(LX/0QB;)LX/CjD;
    .locals 4

    .prologue
    .line 1929200
    sget-object v0, LX/CjD;->b:LX/CjD;

    if-nez v0, :cond_1

    .line 1929201
    const-class v1, LX/CjD;

    monitor-enter v1

    .line 1929202
    :try_start_0
    sget-object v0, LX/CjD;->b:LX/CjD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1929203
    if-eqz v2, :cond_0

    .line 1929204
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1929205
    new-instance p0, LX/CjD;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, LX/CjD;-><init>(LX/0tX;)V

    .line 1929206
    move-object v0, p0

    .line 1929207
    sput-object v0, LX/CjD;->b:LX/CjD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1929208
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1929209
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1929210
    :cond_1
    sget-object v0, LX/CjD;->b:LX/CjD;

    return-object v0

    .line 1929211
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1929212
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
