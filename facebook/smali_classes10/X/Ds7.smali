.class public LX/Ds7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Drs;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Ds7;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/Random;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2050067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2050068
    iput-object p1, p0, LX/Ds7;->a:Landroid/content/Context;

    .line 2050069
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/Ds7;->b:Ljava/util/Random;

    .line 2050070
    return-void
.end method

.method public static a(LX/0QB;)LX/Ds7;
    .locals 4

    .prologue
    .line 2050071
    sget-object v0, LX/Ds7;->c:LX/Ds7;

    if-nez v0, :cond_1

    .line 2050072
    const-class v1, LX/Ds7;

    monitor-enter v1

    .line 2050073
    :try_start_0
    sget-object v0, LX/Ds7;->c:LX/Ds7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2050074
    if-eqz v2, :cond_0

    .line 2050075
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2050076
    new-instance p0, LX/Ds7;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/Ds7;-><init>(Landroid/content/Context;)V

    .line 2050077
    move-object v0, p0

    .line 2050078
    sput-object v0, LX/Ds7;->c:LX/Ds7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2050079
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2050080
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2050081
    :cond_1
    sget-object v0, LX/Ds7;->c:LX/Ds7;

    return-object v0

    .line 2050082
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2050083
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/Drw;)LX/3pb;
    .locals 6

    .prologue
    .line 2050084
    iget-object v0, p1, LX/Drw;->c:Lorg/json/JSONObject;

    move-object v1, v0

    .line 2050085
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v2, v0

    .line 2050086
    iget-object v3, p0, LX/Ds7;->a:Landroid/content/Context;

    .line 2050087
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v0, v0

    .line 2050088
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, LX/Ds7;->b:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    .line 2050089
    iget-object v5, p1, LX/Drw;->d:Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;

    move-object v5, v5

    .line 2050090
    invoke-static {v2, v3, v0, v4, v5}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->c(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2050091
    new-instance v2, LX/3pX;

    const v3, 0x7f02156b

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1, v0}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v2}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 2050066
    const/4 v0, 0x0

    return v0
.end method
