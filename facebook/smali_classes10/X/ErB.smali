.class public LX/ErB;
.super LX/0gG;
.source ""


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Blb;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field private d:I

.field private e:LX/Eqe;

.field private f:LX/EqG;

.field public g:[LX/Er9;

.field private h:LX/1OX;

.field private i:LX/1OX;

.field private j:LX/0gc;

.field private k:Z

.field public l:Z

.field public m:I

.field private n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field private q:Landroid/content/Context;

.field private r:LX/ErA;


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/Eqe;LX/EqG;LX/1OX;LX/1OX;LX/0gc;Landroid/content/Context;LX/ErA;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0Or;)V
    .locals 3
    .param p1    # Ljava/util/Set;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Eqe;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/EqG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1OX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/1OX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/events/invite/EventsExtendedInviteFragment$AddContactsButtonClickListener;",
            "Lcom/facebook/events/invite/EventsExtendedInviteFriendSelectionChangedListener;",
            "LX/1OX;",
            "LX/1OX;",
            "LX/0gc;",
            "Landroid/content/Context;",
            "LX/ErA;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2172643
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2172644
    const/4 v0, -0x1

    iput v0, p0, LX/ErB;->d:I

    .line 2172645
    const v0, 0x7fffffff

    iput v0, p0, LX/ErB;->m:I

    .line 2172646
    iput-object p7, p0, LX/ErB;->q:Landroid/content/Context;

    .line 2172647
    iput-object p8, p0, LX/ErB;->r:LX/ErA;

    .line 2172648
    invoke-interface {p11}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {p9, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2172649
    const/16 v1, 0x3a6

    invoke-virtual {p10, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/ErB;->c:Z

    .line 2172650
    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/ErB;->c:Z

    if-eqz v0, :cond_1

    .line 2172651
    :cond_0
    sget-object v0, LX/Blb;->FACEBOOK:LX/Blb;

    sget-object v1, LX/Blb;->CONTACTS:LX/Blb;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/ErB;->a:LX/0Px;

    .line 2172652
    :goto_0
    iput-object p1, p0, LX/ErB;->b:Ljava/util/Set;

    .line 2172653
    iput-object p2, p0, LX/ErB;->e:LX/Eqe;

    .line 2172654
    iput-object p3, p0, LX/ErB;->f:LX/EqG;

    .line 2172655
    iput-object p4, p0, LX/ErB;->h:LX/1OX;

    .line 2172656
    iput-object p5, p0, LX/ErB;->i:LX/1OX;

    .line 2172657
    iput-object p6, p0, LX/ErB;->j:LX/0gc;

    .line 2172658
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    new-array v0, v0, [LX/Er9;

    iput-object v0, p0, LX/ErB;->g:[LX/Er9;

    .line 2172659
    return-void

    .line 2172660
    :cond_1
    sget-object v0, LX/Blb;->FACEBOOK:LX/Blb;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/ErB;->a:LX/0Px;

    goto :goto_0
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2172642
    iget-object v0, p0, LX/ErB;->q:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, LX/ErB;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Blb;

    iget v0, v0, LX/Blb;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Blb;)I
    .locals 1

    .prologue
    .line 2172639
    iget-object v0, p0, LX/ErB;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2172640
    const/4 v0, 0x0

    .line 2172641
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/ErB;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 18

    .prologue
    .line 2172636
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ErB;->g:[LX/Er9;

    aget-object v1, v1, p2

    if-nez v1, :cond_0

    .line 2172637
    move-object/from16 v0, p0

    iget-object v0, v0, LX/ErB;->g:[LX/Er9;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v1, v0, LX/ErB;->r:LX/ErA;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/ErB;->a:LX/0Px;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Blb;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/ErB;->b:Ljava/util/Set;

    move-object/from16 v0, p0

    iget v5, v0, LX/ErB;->m:I

    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/ErB;->l:Z

    move-object/from16 v0, p0

    iget-object v7, v0, LX/ErB;->o:LX/0Px;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/ErB;->p:LX/0Px;

    move-object/from16 v0, p0

    iget-boolean v9, v0, LX/ErB;->k:Z

    move-object/from16 v0, p0

    iget-object v10, v0, LX/ErB;->n:LX/0Px;

    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/ErB;->c:Z

    move-object/from16 v0, p0

    iget-object v12, v0, LX/ErB;->e:LX/Eqe;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/ErB;->f:LX/EqG;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/ErB;->h:LX/1OX;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/ErB;->i:LX/1OX;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/ErB;->j:LX/0gc;

    move-object/from16 v16, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v16}, LX/ErA;->a(Landroid/view/ViewGroup;LX/Blb;Ljava/util/Set;IZLX/0Px;LX/0Px;ZLX/0Px;ZLX/Eqe;LX/EqG;LX/1OX;LX/1OX;LX/0gc;)LX/Er9;

    move-result-object v1

    aput-object v1, v17, p2

    .line 2172638
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ErB;->g:[LX/Er9;

    aget-object v1, v1, p2

    return-object v1
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2172631
    check-cast p3, LX/Er9;

    .line 2172632
    iget-boolean p0, p3, LX/Er9;->d:Z

    if-eqz p0, :cond_0

    .line 2172633
    iget-object p0, p3, LX/Er9;->x:Landroid/view/ViewGroup;

    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2172634
    const/4 p0, 0x0

    iput-boolean p0, p3, LX/Er9;->d:Z

    .line 2172635
    :cond_0
    return-void
.end method

.method public final a(ZLX/0Px;LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2172588
    iput-boolean p1, p0, LX/ErB;->l:Z

    .line 2172589
    iput-object p2, p0, LX/ErB;->o:LX/0Px;

    .line 2172590
    iput-object p3, p0, LX/ErB;->p:LX/0Px;

    .line 2172591
    iget-object v1, p0, LX/ErB;->g:[LX/Er9;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2172592
    if-eqz v3, :cond_0

    .line 2172593
    iget-boolean v4, p0, LX/ErB;->l:Z

    iget-object v5, p0, LX/ErB;->o:LX/0Px;

    iget-object v6, p0, LX/ErB;->p:LX/0Px;

    .line 2172594
    iget-object p1, v3, LX/Er9;->h:LX/Blb;

    sget-object p2, LX/Blb;->FACEBOOK:LX/Blb;

    if-eq p1, p2, :cond_2

    .line 2172595
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2172596
    :cond_1
    return-void

    .line 2172597
    :cond_2
    iput-object v5, v3, LX/Er9;->r:LX/0Px;

    .line 2172598
    iput-object v6, v3, LX/Er9;->s:LX/0Px;

    .line 2172599
    invoke-static {v3, v4}, LX/Er9;->a(LX/Er9;Z)V

    .line 2172600
    iget-object p1, v3, LX/Er9;->m:LX/Eqx;

    invoke-virtual {p1, v6}, LX/Eqx;->a(LX/0Px;)V

    .line 2172601
    iget-object p1, v3, LX/Er9;->l:LX/Eqx;

    invoke-virtual {p1, v5}, LX/Eqx;->a(LX/0Px;)V

    goto :goto_1
.end method

.method public final a(ZLX/0Px;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2172661
    iput-boolean p1, p0, LX/ErB;->k:Z

    .line 2172662
    iput-object p2, p0, LX/ErB;->n:LX/0Px;

    .line 2172663
    iget-object v1, p0, LX/ErB;->g:[LX/Er9;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2172664
    if-eqz v3, :cond_0

    .line 2172665
    iget-boolean v4, p0, LX/ErB;->k:Z

    .line 2172666
    iget-object v5, v3, LX/Er9;->h:LX/Blb;

    sget-object p1, LX/Blb;->CONTACTS:LX/Blb;

    if-eq v5, p1, :cond_2

    .line 2172667
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2172668
    :cond_1
    return-void

    .line 2172669
    :cond_2
    iput-object p2, v3, LX/Er9;->q:LX/0Px;

    .line 2172670
    invoke-static {v3, v4}, LX/Er9;->a(LX/Er9;Z)V

    .line 2172671
    iget-object v5, v3, LX/Er9;->k:LX/Eqx;

    .line 2172672
    iget-boolean p1, v5, LX/Eqx;->e:Z

    if-eq p1, p3, :cond_3

    .line 2172673
    iput-boolean p3, v5, LX/Eqx;->e:Z

    .line 2172674
    invoke-static {v5}, LX/Eqx;->d(LX/Eqx;)I

    move-result p1

    invoke-virtual {v5, p1}, LX/1OM;->i_(I)V

    .line 2172675
    :cond_3
    iget-object v5, v3, LX/Er9;->k:LX/Eqx;

    invoke-virtual {v5, p2}, LX/Eqx;->a(LX/0Px;)V

    goto :goto_1
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2172628
    check-cast p2, LX/Er9;

    .line 2172629
    iget-object v0, p2, LX/Er9;->x:Landroid/view/ViewGroup;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2172630
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2172627
    iget-object v0, p0, LX/ErB;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2172611
    iget v0, p0, LX/ErB;->d:I

    if-eq v0, p2, :cond_0

    .line 2172612
    iput p2, p0, LX/ErB;->d:I

    move-object v0, p3

    .line 2172613
    check-cast v0, LX/Er9;

    .line 2172614
    sget-object p0, LX/Er8;->a:[I

    iget-object p2, v0, LX/Er9;->h:LX/Blb;

    invoke-virtual {p2}, LX/Blb;->ordinal()I

    move-result p2

    aget p0, p0, p2

    packed-switch p0, :pswitch_data_0

    .line 2172615
    :cond_0
    :goto_0
    check-cast p3, LX/Er9;

    .line 2172616
    iget-boolean v0, p3, LX/Er9;->d:Z

    if-nez v0, :cond_1

    .line 2172617
    iget-object v0, p3, LX/Er9;->x:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2172618
    const/4 v0, 0x1

    iput-boolean v0, p3, LX/Er9;->d:Z

    .line 2172619
    :cond_1
    invoke-virtual {p3}, LX/Er9;->c()V

    .line 2172620
    return-void

    .line 2172621
    :pswitch_0
    iget-object p0, v0, LX/Er9;->B:LX/ErW;

    sget-object p2, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    invoke-virtual {p0, p2}, LX/ErW;->a(LX/ErX;)V

    .line 2172622
    iget-object p0, v0, LX/Er9;->B:LX/ErW;

    sget-object p2, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    invoke-virtual {p0, p2}, LX/ErW;->a(LX/ErX;)V

    .line 2172623
    iget-object p0, v0, LX/Er9;->B:LX/ErW;

    sget-object p2, LX/ErX;->CONTACTS:LX/ErX;

    invoke-virtual {p0, p2}, LX/ErW;->c(LX/ErX;)V

    goto :goto_0

    .line 2172624
    :pswitch_1
    iget-object p0, v0, LX/Er9;->B:LX/ErW;

    sget-object p2, LX/ErX;->ALL_CANDIDATES_SUGGESTED:LX/ErX;

    invoke-virtual {p0, p2}, LX/ErW;->c(LX/ErX;)V

    .line 2172625
    iget-object p0, v0, LX/Er9;->B:LX/ErW;

    sget-object p2, LX/ErX;->ALL_CANDIDATES_ALPHABETICAL:LX/ErX;

    invoke-virtual {p0, p2}, LX/ErW;->c(LX/ErX;)V

    .line 2172626
    iget-object p0, v0, LX/Er9;->B:LX/ErW;

    sget-object p2, LX/ErX;->CONTACTS:LX/ErX;

    invoke-virtual {p0, p2}, LX/ErW;->a(LX/ErX;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2172610
    iget-object v0, p0, LX/ErB;->a:LX/0Px;

    sget-object v1, LX/Blb;->CONTACTS:LX/Blb;

    invoke-virtual {v0, v1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 6

    .prologue
    .line 2172603
    iget-object v1, p0, LX/ErB;->g:[LX/Er9;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2172604
    if-eqz v3, :cond_0

    .line 2172605
    iget-object v4, v3, LX/Er9;->p:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result p0

    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, p0, :cond_0

    iget-object v4, v3, LX/Er9;->p:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Eqx;

    .line 2172606
    invoke-virtual {v4}, LX/1OM;->notifyDataSetChanged()V

    .line 2172607
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 2172608
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2172609
    :cond_1
    return-void
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 2172602
    iget-object v0, p0, LX/ErB;->a:LX/0Px;

    sget-object v1, LX/Blb;->CONTACTS:LX/Blb;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
