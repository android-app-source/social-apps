.class public final LX/EkG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/EkI;


# direct methods
.method public constructor <init>(LX/EkI;)V
    .locals 0

    .prologue
    .line 2163133
    iput-object p1, p0, LX/EkG;->a:LX/EkI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7c27b473

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2163134
    iget-object v1, p0, LX/EkG;->a:LX/EkI;

    iget-object v1, v1, LX/EkI;->y:LX/EkA;

    iget-object v2, p0, LX/EkG;->a:LX/EkI;

    iget-object v2, v2, LX/EkI;->l:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/EkG;->a:LX/EkI;

    iget-object v3, v3, LX/EkI;->B:Ljava/lang/String;

    .line 2163135
    :try_start_0
    iget-object v5, v1, LX/EkA;->g:LX/3Lz;

    iget-object v6, v1, LX/EkA;->g:LX/3Lz;

    invoke-virtual {v6, v2, v3}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v6

    sget-object v7, LX/4hG;->E164:LX/4hG;

    invoke-virtual {v5, v6, v7}, LX/3Lz;->format(LX/4hT;LX/4hG;)Ljava/lang/String;
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2163136
    :goto_0
    move-object v6, v5

    .line 2163137
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v5

    const-string v7, "phone number"

    invoke-virtual {v5, v7, v6}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    .line 2163138
    iget-object v7, v1, LX/EkA;->k:LX/Ek6;

    sget-object p0, LX/Ek7;->PHONE_NUMBER_ADD_ATTEMPT:LX/Ek7;

    const-string p1, "qp"

    invoke-virtual {v7, p0, p1, v5}, LX/Ek6;->a(LX/Ek7;Ljava/lang/String;LX/1rQ;)V

    .line 2163139
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v3, :cond_0

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2163140
    :cond_0
    iget-object v5, v1, LX/EkA;->b:Landroid/content/Context;

    const v7, 0x7f083405

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2163141
    iget-object v7, v1, LX/EkA;->j:LX/EkI;

    invoke-virtual {v7, v5}, LX/EkI;->setupErrorText(Ljava/lang/String;)V

    .line 2163142
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v7

    const-string p0, "error code"

    invoke-virtual {v7, p0, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    const-string v7, "phone number"

    invoke-virtual {v5, v7, v6}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    .line 2163143
    iget-object v6, v1, LX/EkA;->k:LX/Ek6;

    sget-object v7, LX/Ek7;->INVALID_NUMBER:LX/Ek7;

    const-string p0, "qp"

    invoke-virtual {v6, v7, p0, v5}, LX/Ek6;->a(LX/Ek7;Ljava/lang/String;LX/1rQ;)V

    .line 2163144
    :goto_1
    const v1, 0x64b4105b

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2163145
    :cond_1
    iget-object v5, v1, LX/EkA;->i:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v5, v5, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->instanceLogData:LX/0P1;

    const-string v7, "promo_type"

    invoke-virtual {v5, v7}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2163146
    iget-object v7, v1, LX/EkA;->e:LX/Ejx;

    iget-object p0, v1, LX/EkA;->i:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object p0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    invoke-virtual {v7, v2, v3, p0, v5}, LX/Ejx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 2163147
    new-instance p0, LX/Ek9;

    invoke-direct {p0, v1, v6, v3, v5}, LX/Ek9;-><init>(LX/EkA;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v1, LX/EkA;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v7, p0, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    :catch_0
    const/4 v5, 0x0

    goto :goto_0
.end method
