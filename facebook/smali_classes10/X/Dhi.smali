.class public final enum LX/Dhi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dhi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dhi;

.field public static final enum REGULAR:LX/Dhi;

.field public static final enum TIME:LX/Dhi;

.field public static final enum USER_PROMPT:LX/Dhi;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2031268
    new-instance v0, LX/Dhi;

    const-string v1, "REGULAR"

    invoke-direct {v0, v1, v2}, LX/Dhi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhi;->REGULAR:LX/Dhi;

    .line 2031269
    new-instance v0, LX/Dhi;

    const-string v1, "TIME"

    invoke-direct {v0, v1, v3}, LX/Dhi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhi;->TIME:LX/Dhi;

    .line 2031270
    new-instance v0, LX/Dhi;

    const-string v1, "USER_PROMPT"

    invoke-direct {v0, v1, v4}, LX/Dhi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhi;->USER_PROMPT:LX/Dhi;

    .line 2031271
    const/4 v0, 0x3

    new-array v0, v0, [LX/Dhi;

    sget-object v1, LX/Dhi;->REGULAR:LX/Dhi;

    aput-object v1, v0, v2

    sget-object v1, LX/Dhi;->TIME:LX/Dhi;

    aput-object v1, v0, v3

    sget-object v1, LX/Dhi;->USER_PROMPT:LX/Dhi;

    aput-object v1, v0, v4

    sput-object v0, LX/Dhi;->$VALUES:[LX/Dhi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2031272
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static from(Ljava/lang/String;)LX/Dhi;
    .locals 3

    .prologue
    .line 2031273
    if-nez p0, :cond_0

    .line 2031274
    sget-object v0, LX/Dhi;->REGULAR:LX/Dhi;

    .line 2031275
    :goto_0
    return-object v0

    .line 2031276
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 2031277
    sget-object v0, LX/Dhi;->REGULAR:LX/Dhi;

    goto :goto_0

    .line 2031278
    :sswitch_0
    const-string v2, "time"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "user_prompt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    .line 2031279
    :pswitch_0
    sget-object v0, LX/Dhi;->TIME:LX/Dhi;

    goto :goto_0

    .line 2031280
    :pswitch_1
    sget-object v0, LX/Dhi;->USER_PROMPT:LX/Dhi;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0xe2d5fa8 -> :sswitch_1
        0x3652cd -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dhi;
    .locals 1

    .prologue
    .line 2031281
    const-class v0, LX/Dhi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dhi;

    return-object v0
.end method

.method public static values()[LX/Dhi;
    .locals 1

    .prologue
    .line 2031282
    sget-object v0, LX/Dhi;->$VALUES:[LX/Dhi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dhi;

    return-object v0
.end method
