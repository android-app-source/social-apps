.class public final enum LX/ClR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ClR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ClR;

.field public static final enum ABOVE:LX/ClR;

.field public static final enum BELOW:LX/ClR;

.field public static final enum BOTTOM:LX/ClR;

.field public static final enum CENTER:LX/ClR;

.field public static final enum TOP:LX/ClR;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1932455
    new-instance v0, LX/ClR;

    const-string v1, "ABOVE"

    invoke-direct {v0, v1, v2}, LX/ClR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClR;->ABOVE:LX/ClR;

    .line 1932456
    new-instance v0, LX/ClR;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, LX/ClR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClR;->TOP:LX/ClR;

    .line 1932457
    new-instance v0, LX/ClR;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v4}, LX/ClR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClR;->CENTER:LX/ClR;

    .line 1932458
    new-instance v0, LX/ClR;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v5}, LX/ClR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClR;->BOTTOM:LX/ClR;

    .line 1932459
    new-instance v0, LX/ClR;

    const-string v1, "BELOW"

    invoke-direct {v0, v1, v6}, LX/ClR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClR;->BELOW:LX/ClR;

    .line 1932460
    const/4 v0, 0x5

    new-array v0, v0, [LX/ClR;

    sget-object v1, LX/ClR;->ABOVE:LX/ClR;

    aput-object v1, v0, v2

    sget-object v1, LX/ClR;->TOP:LX/ClR;

    aput-object v1, v0, v3

    sget-object v1, LX/ClR;->CENTER:LX/ClR;

    aput-object v1, v0, v4

    sget-object v1, LX/ClR;->BOTTOM:LX/ClR;

    aput-object v1, v0, v5

    sget-object v1, LX/ClR;->BELOW:LX/ClR;

    aput-object v1, v0, v6

    sput-object v0, LX/ClR;->$VALUES:[LX/ClR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1932454
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static from(Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;)LX/ClR;
    .locals 2

    .prologue
    .line 1932461
    if-eqz p0, :cond_0

    .line 1932462
    sget-object v0, LX/ClP;->d:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1932463
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1932464
    :pswitch_0
    sget-object v0, LX/ClR;->ABOVE:LX/ClR;

    goto :goto_0

    .line 1932465
    :pswitch_1
    sget-object v0, LX/ClR;->TOP:LX/ClR;

    goto :goto_0

    .line 1932466
    :pswitch_2
    sget-object v0, LX/ClR;->CENTER:LX/ClR;

    goto :goto_0

    .line 1932467
    :pswitch_3
    sget-object v0, LX/ClR;->BOTTOM:LX/ClR;

    goto :goto_0

    .line 1932468
    :pswitch_4
    sget-object v0, LX/ClR;->BELOW:LX/ClR;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/ClR;
    .locals 1

    .prologue
    .line 1932453
    const-class v0, LX/ClR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ClR;

    return-object v0
.end method

.method public static values()[LX/ClR;
    .locals 1

    .prologue
    .line 1932452
    sget-object v0, LX/ClR;->$VALUES:[LX/ClR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ClR;

    return-object v0
.end method
