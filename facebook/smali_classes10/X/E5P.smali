.class public LX/E5P;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E5N;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E5P;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E5Q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078337
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5P;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E5Q;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078344
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078345
    iput-object p1, p0, LX/E5P;->b:LX/0Ot;

    .line 2078346
    return-void
.end method

.method public static a(LX/0QB;)LX/E5P;
    .locals 4

    .prologue
    .line 2078347
    sget-object v0, LX/E5P;->c:LX/E5P;

    if-nez v0, :cond_1

    .line 2078348
    const-class v1, LX/E5P;

    monitor-enter v1

    .line 2078349
    :try_start_0
    sget-object v0, LX/E5P;->c:LX/E5P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078350
    if-eqz v2, :cond_0

    .line 2078351
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078352
    new-instance v3, LX/E5P;

    const/16 p0, 0x3127

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E5P;-><init>(LX/0Ot;)V

    .line 2078353
    move-object v0, v3

    .line 2078354
    sput-object v0, LX/E5P;->c:LX/E5P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078355
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078356
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078357
    :cond_1
    sget-object v0, LX/E5P;->c:LX/E5P;

    return-object v0

    .line 2078358
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078359
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2078340
    check-cast p2, LX/E5O;

    .line 2078341
    iget-object v0, p0, LX/E5P;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/E5O;->a:Ljava/lang/String;

    .line 2078342
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const p0, 0x7f0a010d

    invoke-virtual {v1, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const p0, 0x7f0b0050

    invoke-virtual {v1, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v1, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    const/4 p0, 0x6

    const p2, 0x7f010718

    invoke-interface {v1, p0, p2}, LX/1Di;->f(II)LX/1Di;

    move-result-object v1

    const/4 p0, 0x1

    const p2, 0x7f0b163b

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2078343
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2078338
    invoke-static {}, LX/1dS;->b()V

    .line 2078339
    const/4 v0, 0x0

    return-object v0
.end method
