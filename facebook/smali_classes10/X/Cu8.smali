.class public final LX/Cu8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public final synthetic a:Lcom/facebook/maps/FbMapViewDelegate;

.field public final synthetic b:LX/6aM;

.field public final synthetic c:LX/CuA;


# direct methods
.method public constructor <init>(LX/CuA;Lcom/facebook/maps/FbMapViewDelegate;LX/6aM;)V
    .locals 0

    .prologue
    .line 1945998
    iput-object p1, p0, LX/Cu8;->c:LX/CuA;

    iput-object p2, p0, LX/Cu8;->a:Lcom/facebook/maps/FbMapViewDelegate;

    iput-object p3, p0, LX/Cu8;->b:LX/6aM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 8

    .prologue
    .line 1945999
    iget-object v0, p0, LX/Cu8;->c:LX/CuA;

    iget-object v0, v0, LX/CuA;->k:Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cu8;->a:Lcom/facebook/maps/FbMapViewDelegate;

    iget-object v1, p0, LX/Cu8;->c:LX/CuA;

    iget-object v1, v1, LX/CuA;->k:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1946000
    :goto_0
    return-void

    .line 1946001
    :cond_0
    iget-object v0, p0, LX/Cu8;->b:LX/6aM;

    if-eqz v0, :cond_1

    .line 1946002
    iget-object v0, p0, LX/Cu8;->b:LX/6aM;

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6aM;)V

    .line 1946003
    :cond_1
    iget-object v0, p0, LX/Cu8;->c:LX/CuA;

    iget-object v0, v0, LX/CuA;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 1946004
    new-instance v2, LX/699;

    invoke-direct {v2}, LX/699;-><init>()V

    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->a()LX/1k1;

    move-result-object v4

    invoke-interface {v4}, LX/1k1;->a()D

    move-result-wide v4

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->a()LX/1k1;

    move-result-object v6

    invoke-interface {v6}, LX/1k1;->b()D

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 1946005
    iput-object v3, v2, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 1946006
    move-object v2, v2

    .line 1946007
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1946008
    iput-object v0, v2, LX/699;->i:Ljava/lang/String;

    .line 1946009
    move-object v0, v2

    .line 1946010
    invoke-virtual {p1, v0}, LX/6al;->a(LX/699;)LX/6ax;

    goto :goto_1

    .line 1946011
    :cond_2
    new-instance v0, LX/Cu7;

    invoke-direct {v0, p0}, LX/Cu7;-><init>(LX/Cu8;)V

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6Zu;)V

    goto :goto_0
.end method
