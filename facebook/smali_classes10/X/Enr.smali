.class public final LX/Enr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final b:LX/0P2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P2",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2167504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167505
    invoke-static {}, LX/0RA;->c()Ljava/util/LinkedHashSet;

    move-result-object v0

    iput-object v0, p0, LX/Enr;->a:Ljava/util/LinkedHashSet;

    .line 2167506
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    iput-object v0, p0, LX/Enr;->b:LX/0P2;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/Ens;
    .locals 3

    .prologue
    .line 2167503
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/Ens;

    iget-object v1, p0, LX/Enr;->a:Ljava/util/LinkedHashSet;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/Enr;->b:LX/0P2;

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/Ens;-><init>(LX/0Px;LX/0P1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 2167499
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Enr;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 2167500
    iget-object v0, p0, LX/Enr;->b:LX/0P2;

    invoke-virtual {v0, p1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2167501
    monitor-exit p0

    return-void

    .line 2167502
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
