.class public final enum LX/EDG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EDG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EDG;

.field public static final enum AGC:LX/EDG;

.field public static final enum AGCMode:LX/EDG;

.field public static final enum CNG:LX/EDG;

.field public static final enum EC:LX/EDG;

.field public static final enum ECMode:LX/EDG;

.field public static final enum ExperimentalAGC:LX/EDG;

.field public static final enum HighPassFilter:LX/EDG;

.field public static final enum LAFNSMode:LX/EDG;

.field public static final enum NS:LX/EDG;

.field public static final enum NSMode:LX/EDG;

.field public static final enum NumTypes:LX/EDG;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2091127
    new-instance v0, LX/EDG;

    const-string v1, "EC"

    invoke-direct {v0, v1, v3}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->EC:LX/EDG;

    .line 2091128
    new-instance v0, LX/EDG;

    const-string v1, "AGC"

    invoke-direct {v0, v1, v4}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->AGC:LX/EDG;

    .line 2091129
    new-instance v0, LX/EDG;

    const-string v1, "NS"

    invoke-direct {v0, v1, v5}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->NS:LX/EDG;

    .line 2091130
    new-instance v0, LX/EDG;

    const-string v1, "HighPassFilter"

    invoke-direct {v0, v1, v6}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->HighPassFilter:LX/EDG;

    .line 2091131
    new-instance v0, LX/EDG;

    const-string v1, "CNG"

    invoke-direct {v0, v1, v7}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->CNG:LX/EDG;

    .line 2091132
    new-instance v0, LX/EDG;

    const-string v1, "ExperimentalAGC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->ExperimentalAGC:LX/EDG;

    .line 2091133
    new-instance v0, LX/EDG;

    const-string v1, "ECMode"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->ECMode:LX/EDG;

    .line 2091134
    new-instance v0, LX/EDG;

    const-string v1, "AGCMode"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->AGCMode:LX/EDG;

    .line 2091135
    new-instance v0, LX/EDG;

    const-string v1, "NSMode"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->NSMode:LX/EDG;

    .line 2091136
    new-instance v0, LX/EDG;

    const-string v1, "LAFNSMode"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->LAFNSMode:LX/EDG;

    .line 2091137
    new-instance v0, LX/EDG;

    const-string v1, "NumTypes"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/EDG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDG;->NumTypes:LX/EDG;

    .line 2091138
    const/16 v0, 0xb

    new-array v0, v0, [LX/EDG;

    sget-object v1, LX/EDG;->EC:LX/EDG;

    aput-object v1, v0, v3

    sget-object v1, LX/EDG;->AGC:LX/EDG;

    aput-object v1, v0, v4

    sget-object v1, LX/EDG;->NS:LX/EDG;

    aput-object v1, v0, v5

    sget-object v1, LX/EDG;->HighPassFilter:LX/EDG;

    aput-object v1, v0, v6

    sget-object v1, LX/EDG;->CNG:LX/EDG;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EDG;->ExperimentalAGC:LX/EDG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EDG;->ECMode:LX/EDG;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EDG;->AGCMode:LX/EDG;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EDG;->NSMode:LX/EDG;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/EDG;->LAFNSMode:LX/EDG;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/EDG;->NumTypes:LX/EDG;

    aput-object v2, v0, v1

    sput-object v0, LX/EDG;->$VALUES:[LX/EDG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2091124
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EDG;
    .locals 1

    .prologue
    .line 2091126
    const-class v0, LX/EDG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EDG;

    return-object v0
.end method

.method public static values()[LX/EDG;
    .locals 1

    .prologue
    .line 2091125
    sget-object v0, LX/EDG;->$VALUES:[LX/EDG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EDG;

    return-object v0
.end method
