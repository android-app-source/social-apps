.class public LX/EQj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/EQj;


# instance fields
.field public final b:LX/EQl;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/0Sh;

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Dwz;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2119062
    const-class v0, LX/EQj;

    sput-object v0, LX/EQj;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/EQl;Ljava/util/concurrent/ExecutorService;LX/0Sh;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2119037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2119038
    iput-object p1, p0, LX/EQj;->b:LX/EQl;

    .line 2119039
    iput-object p2, p0, LX/EQj;->c:Ljava/util/concurrent/ExecutorService;

    .line 2119040
    iput-object p3, p0, LX/EQj;->d:LX/0Sh;

    .line 2119041
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/EQj;->e:Ljava/util/Set;

    .line 2119042
    return-void
.end method

.method public static a(LX/0QB;)LX/EQj;
    .locals 6

    .prologue
    .line 2119049
    sget-object v0, LX/EQj;->g:LX/EQj;

    if-nez v0, :cond_1

    .line 2119050
    const-class v1, LX/EQj;

    monitor-enter v1

    .line 2119051
    :try_start_0
    sget-object v0, LX/EQj;->g:LX/EQj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2119052
    if-eqz v2, :cond_0

    .line 2119053
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2119054
    new-instance p0, LX/EQj;

    invoke-static {v0}, LX/EQl;->a(LX/0QB;)LX/EQl;

    move-result-object v3

    check-cast v3, LX/EQl;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-direct {p0, v3, v4, v5}, LX/EQj;-><init>(LX/EQl;Ljava/util/concurrent/ExecutorService;LX/0Sh;)V

    .line 2119055
    move-object v0, p0

    .line 2119056
    sput-object v0, LX/EQj;->g:LX/EQj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2119057
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2119058
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2119059
    :cond_1
    sget-object v0, LX/EQj;->g:LX/EQj;

    return-object v0

    .line 2119060
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2119061
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119063
    iget-object v0, p0, LX/EQj;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2119064
    iget-object v1, p0, LX/EQj;->f:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/EQj;->b:LX/EQl;

    iget-object v2, p0, LX/EQj;->f:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2119065
    if-eqz v2, :cond_0

    .line 2119066
    iget-boolean v3, v2, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->a:Z

    move v3, v3

    .line 2119067
    invoke-static {v1}, LX/EQl;->c(LX/EQl;)Z

    move-result v4

    if-eq v3, v4, :cond_2

    :cond_0
    const/4 v3, 0x1

    :goto_0
    move v1, v3

    .line 2119068
    if-nez v1, :cond_1

    .line 2119069
    :goto_1
    iget-object v0, p0, LX/EQj;->f:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    return-object v0

    .line 2119070
    :cond_1
    iget-object v1, p0, LX/EQj;->b:LX/EQl;

    .line 2119071
    new-instance v4, LX/EQm;

    invoke-direct {v4}, LX/EQm;-><init>()V

    move-object v4, v4

    .line 2119072
    const-string v5, "photo_count"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2119073
    const-string v5, "thumbnail_size"

    const/16 v6, 0xa0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2119074
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    move-object v4, v4

    .line 2119075
    invoke-static {v1}, LX/EQl;->c(LX/EQl;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-wide/16 v8, 0x2a30

    :goto_2
    move-wide v6, v8

    .line 2119076
    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    .line 2119077
    iget-object v5, v1, LX/EQl;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    new-instance v6, LX/EQk;

    invoke-direct {v6, v1}, LX/EQk;-><init>(LX/EQl;)V

    iget-object v7, v1, LX/EQl;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v4, v5

    .line 2119078
    move-object v1, v4

    .line 2119079
    new-instance v2, LX/EQi;

    invoke-direct {v2, p0}, LX/EQi;-><init>(LX/EQj;)V

    iget-object v3, p0, LX/EQj;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    const-wide/32 v8, 0x15180

    goto :goto_2
.end method

.method public final a(LX/Dwz;)V
    .locals 1

    .prologue
    .line 2119046
    iget-object v0, p0, LX/EQj;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2119047
    iget-object v0, p0, LX/EQj;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2119048
    return-void
.end method

.method public final b(LX/Dwz;)V
    .locals 1

    .prologue
    .line 2119043
    iget-object v0, p0, LX/EQj;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2119044
    iget-object v0, p0, LX/EQj;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2119045
    return-void
.end method
