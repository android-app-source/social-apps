.class public LX/ClG;
.super LX/1OX;
.source ""


# instance fields
.field public final a:Landroid/view/Choreographer;

.field public final b:I

.field public c:Z

.field public d:J

.field public final e:LX/ClF;

.field private final f:I

.field public final g:J

.field public h:I

.field public final i:Landroid/view/Choreographer$FrameCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/ClF;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1932214
    invoke-direct {p0}, LX/1OX;-><init>()V

    .line 1932215
    iput-boolean v4, p0, LX/ClG;->c:Z

    .line 1932216
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/ClG;->d:J

    .line 1932217
    iput v4, p0, LX/ClG;->h:I

    .line 1932218
    iput-object p2, p0, LX/ClG;->e:LX/ClF;

    .line 1932219
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-le v0, v1, :cond_0

    .line 1932220
    const-string v0, "display"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 1932221
    invoke-virtual {v0, v4}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRefreshRate()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/ClG;->f:I

    .line 1932222
    :goto_0
    const/high16 v0, 0x447a0000    # 1000.0f

    iget v1, p0, LX/ClG;->f:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1932223
    const v1, 0x49742400    # 1000000.0f

    mul-float/2addr v1, v0

    float-to-long v2, v1

    iput-wide v2, p0, LX/ClG;->g:J

    .line 1932224
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/lit8 v1, v1, 0x3

    iput v1, p0, LX/ClG;->b:I

    .line 1932225
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, LX/ClG;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x2

    iget v2, p0, LX/ClG;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1932226
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, LX/ClG;->a:Landroid/view/Choreographer;

    .line 1932227
    new-instance v0, LX/ClE;

    invoke-direct {v0, p0}, LX/ClE;-><init>(LX/ClG;)V

    iput-object v0, p0, LX/ClG;->i:Landroid/view/Choreographer$FrameCallback;

    .line 1932228
    return-void

    .line 1932229
    :cond_0
    const/16 v0, 0x3c

    iput v0, p0, LX/ClG;->f:I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1932230
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ClG;->c:Z

    .line 1932231
    iget-object v0, p0, LX/ClG;->a:Landroid/view/Choreographer;

    iget-object v1, p0, LX/ClG;->i:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1932232
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 4

    .prologue
    .line 1932233
    if-nez p2, :cond_1

    .line 1932234
    invoke-virtual {p0}, LX/ClG;->a()V

    .line 1932235
    iget-object v0, p0, LX/ClG;->e:LX/ClF;

    sget-object v1, LX/ClF;->ONLY_DROPS:LX/ClF;

    if-ne v0, v1, :cond_0

    .line 1932236
    :cond_0
    :goto_0
    return-void

    .line 1932237
    :cond_1
    iget-boolean v2, p0, LX/ClG;->c:Z

    if-nez v2, :cond_2

    .line 1932238
    const/4 v2, 0x0

    iput v2, p0, LX/ClG;->h:I

    .line 1932239
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/ClG;->d:J

    .line 1932240
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/ClG;->c:Z

    .line 1932241
    iget-object v2, p0, LX/ClG;->a:Landroid/view/Choreographer;

    iget-object v3, p0, LX/ClG;->i:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v2, v3}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1932242
    :cond_2
    goto :goto_0
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 1932243
    return-void
.end method
