.class public abstract LX/D2z;
.super LX/1SX;
.source ""


# static fields
.field public static final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/5SB;

.field public final b:LX/BPq;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1e0;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/1e4;

.field private final g:Z

.field public final h:LX/0ad;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/1Sp;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1959442
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FROM_TIMELINE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/D2z;->e:LX/0Rf;

    .line 1959443
    new-instance v0, LX/D2p;

    invoke-direct {v0}, LX/D2p;-><init>()V

    sput-object v0, LX/D2z;->f:LX/0Or;

    return-void
.end method

.method public constructor <init>(LX/5SB;LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/BPq;LX/0Or;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0SG;LX/0bH;LX/0Sh;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0Ot;LX/1Pf;Ljava/lang/Boolean;LX/0qn;LX/1e4;LX/0W3;LX/0Ot;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0wL;)V
    .locals 42
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5SB;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/BPq;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0bH;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1e0;",
            ">;",
            "LX/1Pf;",
            "Ljava/lang/Boolean;",
            "LX/0qn;",
            "Lcom/facebook/privacy/ui/PrivacyScopeResourceResolver;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1959472
    sget-object v18, LX/D2z;->f:LX/0Or;

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p14

    move-object/from16 v9, p15

    move-object/from16 v10, p16

    move-object/from16 v11, p17

    move-object/from16 v12, p18

    move-object/from16 v13, p9

    move-object/from16 v14, p22

    move-object/from16 v15, p21

    move-object/from16 v16, p7

    move-object/from16 v17, p8

    move-object/from16 v19, p10

    move-object/from16 v20, p11

    move-object/from16 v21, p13

    move-object/from16 v22, p19

    move-object/from16 v23, p20

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p30

    move-object/from16 v31, p32

    move-object/from16 v32, p34

    move-object/from16 v33, p35

    move-object/from16 v34, p42

    move-object/from16 v35, p36

    move-object/from16 v36, p37

    move-object/from16 v37, p38

    move-object/from16 v38, p39

    move-object/from16 v39, p40

    move-object/from16 v40, p41

    move-object/from16 v41, p44

    invoke-direct/range {v2 .. v41}, LX/1SX;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Pf;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;)V

    .line 1959473
    new-instance v2, LX/D2q;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/D2q;-><init>(LX/D2z;)V

    move-object/from16 v0, p0

    iput-object v2, v0, LX/D2z;->k:LX/1Sp;

    .line 1959474
    move-object/from16 v0, p33

    move-object/from16 v1, p0

    iput-object v0, v1, LX/D2z;->d:LX/1e4;

    .line 1959475
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/D2z;->a:LX/5SB;

    .line 1959476
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, LX/D2z;->b:LX/BPq;

    .line 1959477
    move-object/from16 v0, p29

    move-object/from16 v1, p0

    iput-object v0, v1, LX/D2z;->c:LX/0Ot;

    .line 1959478
    invoke-virtual/range {p31 .. p31}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/D2z;->g:Z

    .line 1959479
    move-object/from16 v0, p27

    move-object/from16 v1, p0

    iput-object v0, v1, LX/D2z;->h:LX/0ad;

    .line 1959480
    move-object/from16 v0, p42

    move-object/from16 v1, p0

    iput-object v0, v1, LX/D2z;->i:LX/0Or;

    .line 1959481
    move-object/from16 v0, p43

    move-object/from16 v1, p0

    iput-object v0, v1, LX/D2z;->j:LX/0Ot;

    .line 1959482
    const-string v2, "native_timeline"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->b(Ljava/lang/String;)V

    .line 1959483
    const-string v2, "native_story_timeline"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(Ljava/lang/String;)V

    .line 1959484
    move-object/from16 v0, p0

    iget-object v2, v0, LX/D2z;->k:LX/1Sp;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/1SX;->a(LX/1Sp;)V

    .line 1959485
    return-void
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1959467
    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1959468
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1959469
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 1959470
    goto :goto_0

    .line 1959471
    :catch_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1959462
    iget-object v0, p0, LX/D2z;->b:LX/BPq;

    new-instance v1, LX/BPV;

    iget-object v2, p0, LX/D2z;->a:LX/5SB;

    .line 1959463
    iget-object v3, v2, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v2, v3

    .line 1959464
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/BPV;-><init>(Landroid/os/ParcelUuid;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1959465
    invoke-super {p0, p1, p2}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;Landroid/content/Context;)V

    .line 1959466
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 1959454
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v2

    .line 1959455
    iget-object v6, p0, LX/D2z;->b:LX/BPq;

    new-instance v0, LX/BPT;

    iget-object v1, p0, LX/D2z;->a:LX/5SB;

    .line 1959456
    iget-object v3, v1, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v1, v3

    .line 1959457
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/BPT;-><init>(Landroid/os/ParcelUuid;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1959458
    invoke-static {p1}, LX/17E;->l(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1959459
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 1959460
    iget-object v1, p0, LX/D2z;->i:LX/0Or;

    invoke-static {v0, p3, v1}, LX/3In;->a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/0Or;)V

    .line 1959461
    :cond_0
    return-void
.end method

.method public abstract a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)Z
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1959452
    const-string v0, "native_timeline"

    invoke-virtual {p0, p1, v0, p2}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;Landroid/content/Context;)V

    .line 1959453
    return-void
.end method

.method public abstract b(Lcom/facebook/graphql/model/GraphQLStory;)Z
.end method

.method public final c(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1959449
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1959450
    new-instance v1, LX/0ju;

    invoke-direct {v1, v0}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081597

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f081578

    new-instance v3, LX/D2r;

    invoke-direct {v3, p0, p1, p2, v0}, LX/D2r;-><init>(LX/D2z;Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0815b3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1959451
    return-void
.end method

.method public d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 1

    .prologue
    .line 1959446
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1959447
    new-instance v0, LX/D2y;

    invoke-direct {v0, p0}, LX/D2y;-><init>(LX/D2z;)V

    .line 1959448
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1959445
    const/4 v0, 0x1

    return v0
.end method

.method public abstract g()LX/0wD;
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1959444
    sget-object v0, LX/04D;->USER_TIMELINE_CHEVRON:LX/04D;

    invoke-virtual {v0}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
