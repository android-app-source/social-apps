.class public LX/EUI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EVa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EVa;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2126024
    iput-object p1, p0, LX/EUI;->a:LX/0Ot;

    .line 2126025
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2126026
    iget-object v0, p0, LX/EUI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EVa;

    invoke-virtual {v0}, LX/EVa;->a()Ljava/lang/String;

    move-result-object v0

    .line 2126027
    new-instance v1, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-direct {v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;-><init>()V

    .line 2126028
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 2126029
    const-string v3, "ptr_enabled"

    const/4 p0, 0x1

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2126030
    if-eqz v0, :cond_0

    .line 2126031
    const-string v3, "reaction_session_id"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2126032
    :cond_0
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2126033
    move-object v0, v1

    .line 2126034
    return-object v0
.end method
