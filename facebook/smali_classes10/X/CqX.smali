.class public abstract LX/CqX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        "T::",
        "LX/Cqv;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/richdocument/view/transition/TransitionStrategy",
        "<TV;TT;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TT;",
            "Lcom/facebook/richdocument/view/transition/ViewLayoutStrategy",
            "<TV;TT;>;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TT;",
            "LX/CrS;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/Cqv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private e:LX/CrS;

.field private f:Z

.field public g:LX/CrK;

.field private h:LX/CrL;

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CrJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;LX/CrK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "LX/CrK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1940049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1940050
    iput-object p1, p0, LX/CqX;->a:Ljava/lang/Object;

    .line 1940051
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CqX;->b:Ljava/util/Map;

    .line 1940052
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CqX;->c:Ljava/util/Map;

    .line 1940053
    invoke-virtual {p0, p2}, LX/CqX;->a(LX/CrK;)V

    .line 1940054
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CqX;->i:Ljava/util/List;

    .line 1940055
    return-void
.end method


# virtual methods
.method public final a(LX/Cqf;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/view/transition/ViewLayoutStrategy",
            "<TV;TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1940045
    iget-object v0, p0, LX/CqX;->b:Ljava/util/Map;

    .line 1940046
    iget-object v1, p1, LX/Cqf;->h:LX/Cqw;

    move-object v1, v1

    .line 1940047
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1940048
    return-void
.end method

.method public final a(LX/CrK;)V
    .locals 3

    .prologue
    .line 1940035
    iget-object v0, p0, LX/CqX;->h:LX/CrL;

    if-eqz v0, :cond_0

    .line 1940036
    iget-object v0, p0, LX/CqX;->h:LX/CrL;

    .line 1940037
    iget-object v1, v0, LX/CrL;->b:LX/CrK;

    .line 1940038
    iget-object v2, v1, LX/CrK;->c:LX/0wd;

    invoke-virtual {v2, v0}, LX/0wd;->b(LX/0xi;)LX/0wd;

    .line 1940039
    :cond_0
    if-eqz p1, :cond_1

    .line 1940040
    iput-object p1, p0, LX/CqX;->g:LX/CrK;

    .line 1940041
    new-instance v0, LX/CrL;

    iget-object v1, p0, LX/CqX;->g:LX/CrK;

    invoke-direct {v0, v1, p0}, LX/CrL;-><init>(LX/CrK;LX/CqX;)V

    iput-object v0, p0, LX/CqX;->h:LX/CrL;

    .line 1940042
    iget-object v0, p0, LX/CqX;->g:LX/CrK;

    iget-object v1, p0, LX/CqX;->h:LX/CrL;

    .line 1940043
    iget-object v2, v0, LX/CrK;->c:LX/0wd;

    invoke-virtual {v2, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1940044
    :cond_1
    return-void
.end method

.method public final a(LX/CrS;)V
    .locals 2

    .prologue
    .line 1940027
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/CqX;->c:Ljava/util/Map;

    invoke-interface {p1}, LX/CrS;->a()LX/Cqv;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1940028
    invoke-interface {p1}, LX/CrS;->c()LX/CrS;

    move-result-object v0

    iput-object v0, p0, LX/CqX;->e:LX/CrS;

    .line 1940029
    :goto_0
    iget-object v0, p0, LX/CqX;->e:LX/CrS;

    if-eqz v0, :cond_0

    .line 1940030
    iget-object v0, p0, LX/CqX;->e:LX/CrS;

    .line 1940031
    iget-object v1, p0, LX/CqX;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CrJ;

    .line 1940032
    invoke-interface {v1, v0}, LX/CrJ;->a(LX/CrS;)V

    goto :goto_1

    .line 1940033
    :cond_0
    return-void

    .line 1940034
    :cond_1
    iput-object p1, p0, LX/CqX;->e:LX/CrS;

    goto :goto_0
.end method

.method public final b(LX/Cqv;)LX/CrS;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "LX/CrS;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1939993
    iget-object v0, p0, LX/CqX;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1939994
    iget-object v0, p0, LX/CqX;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CrS;

    .line 1939995
    :goto_0
    return-object v0

    .line 1939996
    :cond_0
    invoke-virtual {p0, p1}, LX/CqX;->d(LX/Cqv;)LX/Cqf;

    move-result-object v0

    .line 1939997
    if-eqz v0, :cond_2

    .line 1939998
    iget-boolean v2, p0, LX/CqX;->f:Z

    if-nez v2, :cond_1

    .line 1939999
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/CqX;->f:Z

    .line 1940000
    invoke-virtual {p0}, LX/CqX;->h()V

    .line 1940001
    :cond_1
    invoke-virtual {v0}, LX/Cqf;->f()LX/CrS;

    move-result-object v0

    .line 1940002
    if-eqz v0, :cond_2

    .line 1940003
    iget-object v1, p0, LX/CqX;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1940004
    :cond_2
    iget-object v0, p0, LX/CqX;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1940005
    iget-object v0, p0, LX/CqX;->c:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    move-object v0, v1

    .line 1940006
    goto :goto_0
.end method

.method public final d(LX/Cqv;)LX/Cqf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/facebook/richdocument/view/transition/ViewLayoutStrategy",
            "<TV;TT;>;"
        }
    .end annotation

    .prologue
    .line 1940026
    iget-object v0, p0, LX/CqX;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cqf;

    return-object v0
.end method

.method public d()LX/Cqv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1940025
    iget-object v0, p0, LX/CqX;->d:LX/Cqv;

    return-object v0
.end method

.method public final e()LX/Cqv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1940024
    invoke-virtual {p0}, LX/CqX;->g()LX/CrS;

    move-result-object v0

    invoke-interface {v0}, LX/CrS;->a()LX/Cqv;

    move-result-object v0

    return-object v0
.end method

.method public final e(LX/Cqv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 1940022
    iget-object v0, p0, LX/CqX;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1940023
    return-void
.end method

.method public final f()LX/Cqv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1940019
    iget-object v0, p0, LX/CqX;->g:LX/CrK;

    .line 1940020
    iget-object v1, v0, LX/CrK;->e:LX/Cqv;

    move-object v0, v1

    .line 1940021
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/CqX;->e()LX/Cqv;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final f(LX/Cqv;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 1940012
    invoke-virtual {p0, p1}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v0

    .line 1940013
    if-eqz v0, :cond_0

    .line 1940014
    iget-object v0, p0, LX/CqX;->g:LX/CrK;

    .line 1940015
    iput-object p1, v0, LX/CrK;->e:LX/Cqv;

    .line 1940016
    iget-object v1, v0, LX/CrK;->c:LX/0wd;

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v3, v4}, LX/0wd;->a(D)LX/0wd;

    .line 1940017
    iget-object v1, v0, LX/CrK;->c:LX/0wd;

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v3, v4}, LX/0wd;->b(D)LX/0wd;

    .line 1940018
    :cond_0
    return-void
.end method

.method public g()LX/CrS;
    .locals 1

    .prologue
    .line 1940007
    iget-object v0, p0, LX/CqX;->e:LX/CrS;

    if-nez v0, :cond_0

    .line 1940008
    invoke-virtual {p0}, LX/CqX;->d()LX/Cqv;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v0

    .line 1940009
    if-eqz v0, :cond_0

    .line 1940010
    invoke-interface {v0}, LX/CrS;->c()LX/CrS;

    move-result-object v0

    iput-object v0, p0, LX/CqX;->e:LX/CrS;

    .line 1940011
    :cond_0
    iget-object v0, p0, LX/CqX;->e:LX/CrS;

    return-object v0
.end method

.method public abstract h()V
.end method
