.class public LX/E5B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/E5B;


# instance fields
.field public final a:LX/E4q;

.field public final b:LX/0SG;

.field public final c:LX/E5T;


# direct methods
.method public constructor <init>(LX/E4q;LX/0SG;LX/E5T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2077999
    iput-object p1, p0, LX/E5B;->a:LX/E4q;

    .line 2078000
    iput-object p2, p0, LX/E5B;->b:LX/0SG;

    .line 2078001
    iput-object p3, p0, LX/E5B;->c:LX/E5T;

    .line 2078002
    return-void
.end method

.method public static a(LX/0QB;)LX/E5B;
    .locals 6

    .prologue
    .line 2078003
    sget-object v0, LX/E5B;->d:LX/E5B;

    if-nez v0, :cond_1

    .line 2078004
    const-class v1, LX/E5B;

    monitor-enter v1

    .line 2078005
    :try_start_0
    sget-object v0, LX/E5B;->d:LX/E5B;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078006
    if-eqz v2, :cond_0

    .line 2078007
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078008
    new-instance p0, LX/E5B;

    invoke-static {v0}, LX/E4q;->a(LX/0QB;)LX/E4q;

    move-result-object v3

    check-cast v3, LX/E4q;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/E5T;->a(LX/0QB;)LX/E5T;

    move-result-object v5

    check-cast v5, LX/E5T;

    invoke-direct {p0, v3, v4, v5}, LX/E5B;-><init>(LX/E4q;LX/0SG;LX/E5T;)V

    .line 2078009
    move-object v0, p0

    .line 2078010
    sput-object v0, LX/E5B;->d:LX/E5B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078011
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078012
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078013
    :cond_1
    sget-object v0, LX/E5B;->d:LX/E5B;

    return-object v0

    .line 2078014
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078015
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
