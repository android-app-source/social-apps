.class public final LX/Deb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2022782
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2022783
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2022784
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2022785
    invoke-static {p0, p1}, LX/Deb;->b(LX/15w;LX/186;)I

    move-result v1

    .line 2022786
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2022787
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2022776
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2022777
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2022778
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/Deb;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2022779
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2022780
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2022781
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2022788
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 2022789
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2022790
    :goto_0
    return v1

    .line 2022791
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2022792
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_6

    .line 2022793
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2022794
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2022795
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2022796
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2022797
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 2022798
    :cond_3
    const-string v5, "actors"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2022799
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2022800
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_4

    .line 2022801
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2022802
    invoke-static {p0, p1}, LX/DeZ;->b(LX/15w;LX/186;)I

    move-result v4

    .line 2022803
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2022804
    :cond_4
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2022805
    goto :goto_1

    .line 2022806
    :cond_5
    const-string v5, "text_line"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2022807
    invoke-static {p0, p1}, LX/Dea;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2022808
    :cond_6
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2022809
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2022810
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2022811
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2022812
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2022757
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2022758
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2022759
    if-eqz v0, :cond_0

    .line 2022760
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022761
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2022762
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2022763
    if-eqz v0, :cond_2

    .line 2022764
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022765
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2022766
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2022767
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/DeZ;->a(LX/15i;ILX/0nX;)V

    .line 2022768
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2022769
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2022770
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2022771
    if-eqz v0, :cond_3

    .line 2022772
    const-string v1, "text_line"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2022773
    invoke-static {p0, v0, p2}, LX/Dea;->a(LX/15i;ILX/0nX;)V

    .line 2022774
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2022775
    return-void
.end method
