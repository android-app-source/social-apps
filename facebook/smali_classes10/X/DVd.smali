.class public LX/DVd;
.super LX/B1d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B1d",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/String;LX/0tX;LX/B1b;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/B1b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2005573
    invoke-direct {p0, p1, p3, p4}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;)V

    .line 2005574
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005575
    iput-object v0, p0, LX/DVd;->e:LX/0Px;

    .line 2005576
    iput-object p2, p0, LX/DVd;->f:Ljava/lang/String;

    .line 2005577
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0gW;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005566
    invoke-static {}, LX/DWY;->a()LX/DWX;

    move-result-object v0

    .line 2005567
    const-string v1, "group_id"

    iget-object v2, p0, LX/DVd;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005568
    const-string v1, "member_count_to_fetch"

    const-string v2, "12"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005569
    const-string v1, "afterCursor"

    iget-object v2, p0, LX/B1d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005570
    const-string v1, "suggested_member_friend_sort_param"

    const-string v2, "is_viewer_friend"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005571
    const-string v1, "suggested_member_order_param"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2005572
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2005578
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2005579
    iget-object v0, p0, LX/DVd;->e:LX/0Px;

    invoke-virtual {v5, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2005580
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005581
    if-eqz v0, :cond_3

    .line 2005582
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005583
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2005584
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2005585
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel;

    move-result-object v6

    .line 2005586
    invoke-virtual {v6}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v2, v1

    :goto_0
    if-ge v2, v8, :cond_0

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;

    .line 2005587
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->a()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {v0, v3}, LX/DVf;->a(LX/DWH;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2005588
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2005589
    :cond_0
    invoke-virtual {v6}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2005590
    :goto_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iput-object v5, p0, LX/DVd;->e:LX/0Px;

    .line 2005591
    if-eqz v0, :cond_1

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iput-object v3, p0, LX/DVd;->a:Ljava/lang/String;

    .line 2005592
    if-eqz v0, :cond_2

    invoke-virtual {v2, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, LX/DVd;->b:Z

    .line 2005593
    invoke-virtual {p0}, LX/B1d;->g()V

    .line 2005594
    return-void

    :cond_2
    move v1, v4

    .line 2005595
    goto :goto_2

    :cond_3
    move v0, v1

    move-object v2, v3

    goto :goto_1
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2005565
    const-string v0, "Group alphabetical suggested member fetch failed"

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005564
    iget-object v0, p0, LX/DVd;->e:LX/0Px;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 2005561
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2005562
    iput-object v0, p0, LX/DVd;->e:LX/0Px;

    .line 2005563
    return-void
.end method
