.class public final LX/Drp;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/BAa;

.field public final synthetic c:I

.field public final synthetic d:Landroid/content/Intent;

.field public final synthetic e:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

.field public final synthetic f:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;ILX/BAa;ILandroid/content/Intent;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 0

    .prologue
    .line 2049710
    iput-object p1, p0, LX/Drp;->f:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iput p2, p0, LX/Drp;->a:I

    iput-object p3, p0, LX/Drp;->b:LX/BAa;

    iput p4, p0, LX/Drp;->c:I

    iput-object p5, p0, LX/Drp;->d:Landroid/content/Intent;

    iput-object p6, p0, LX/Drp;->e:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v6, 0x35000f

    .line 2049704
    iget-object v0, p0, LX/Drp;->f:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/Drp;->a:I

    const/16 v2, 0xd6

    invoke-interface {v0, v6, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2049705
    if-eqz p1, :cond_0

    .line 2049706
    iget-object v0, p0, LX/Drp;->b:LX/BAa;

    invoke-virtual {v0, p1}, LX/BAa;->a(Landroid/graphics/Bitmap;)LX/BAa;

    .line 2049707
    :cond_0
    iget-object v0, p0, LX/Drp;->f:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->k:LX/2c4;

    iget v1, p0, LX/Drp;->c:I

    iget-object v2, p0, LX/Drp;->b:LX/BAa;

    iget-object v3, p0, LX/Drp;->d:Landroid/content/Intent;

    sget-object v4, LX/8D4;->ACTIVITY:LX/8D4;

    iget-object v5, p0, LX/Drp;->e:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-virtual/range {v0 .. v5}, LX/2c4;->a(ILX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 2049708
    iget-object v0, p0, LX/Drp;->f:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/Drp;->a:I

    const/16 v2, 0xd5

    invoke-interface {v0, v6, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2049709
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const v6, 0x35000f

    .line 2049699
    iget-object v0, p0, LX/Drp;->f:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/Drp;->a:I

    const/16 v2, 0xd7

    invoke-interface {v0, v6, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2049700
    iget-object v0, p0, LX/Drp;->b:LX/BAa;

    const v1, 0x7f0218e4

    invoke-virtual {v0, v1}, LX/BAa;->a(I)LX/BAa;

    .line 2049701
    iget-object v0, p0, LX/Drp;->f:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->k:LX/2c4;

    iget v1, p0, LX/Drp;->c:I

    iget-object v2, p0, LX/Drp;->b:LX/BAa;

    iget-object v3, p0, LX/Drp;->d:Landroid/content/Intent;

    sget-object v4, LX/8D4;->ACTIVITY:LX/8D4;

    iget-object v5, p0, LX/Drp;->e:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-virtual/range {v0 .. v5}, LX/2c4;->a(ILX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 2049702
    iget-object v0, p0, LX/Drp;->f:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->w:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/Drp;->a:I

    const/16 v2, 0xd5

    invoke-interface {v0, v6, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2049703
    return-void
.end method
