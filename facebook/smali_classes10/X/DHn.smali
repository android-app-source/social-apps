.class public abstract LX/DHn;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/24a;
.implements LX/2fC;
.implements LX/2eZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;",
        "LX/24a;",
        "LX/2fC;",
        "LX/2eZ;"
    }
.end annotation


# instance fields
.field public a:Z

.field private final b:Lcom/facebook/components/feed/FeedComponentView;

.field private final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/ImageView;

.field private final e:Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;

.field private final f:Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field private final i:Landroid/widget/LinearLayout;

.field private final j:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/facebook/widget/CustomLinearLayout;

.field private final l:Landroid/graphics/drawable/ColorDrawable;

.field private final m:I

.field private n:Z

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 1982463
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 1982464
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DHn;->n:Z

    .line 1982465
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DHn;->o:Z

    .line 1982466
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1982467
    const v0, 0x7f0d1507

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/DHn;->j:LX/0am;

    .line 1982468
    const v0, 0x7f0d2dec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/DHn;->g:Landroid/widget/TextView;

    .line 1982469
    const v0, 0x7f0d2deb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, LX/DHn;->k:Lcom/facebook/widget/CustomLinearLayout;

    .line 1982470
    const v0, 0x7f0d2ded

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/DHn;->d:Landroid/widget/ImageView;

    .line 1982471
    iget-object v0, p0, LX/DHn;->g:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1982472
    iget-object v0, p0, LX/DHn;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 1982473
    iget-object v0, p0, LX/DHn;->g:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1982474
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DHn;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 1982475
    const v0, 0x7f0d2de8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/DHn;->i:Landroid/widget/LinearLayout;

    .line 1982476
    const v0, 0x7f0d1d87

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/feed/FeedComponentView;

    iput-object v0, p0, LX/DHn;->b:Lcom/facebook/components/feed/FeedComponentView;

    .line 1982477
    const v0, 0x7f0d1d8a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/DHn;->c:Landroid/widget/TextView;

    .line 1982478
    const v0, 0x7f0d2de3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;

    iput-object v0, p0, LX/DHn;->e:Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;

    .line 1982479
    const v0, 0x7f0d150c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;

    iput-object v0, p0, LX/DHn;->f:Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;

    .line 1982480
    const v0, 0x7f0d150d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/DHn;->h:Landroid/view/View;

    .line 1982481
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0460

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/DHn;->l:Landroid/graphics/drawable/ColorDrawable;

    .line 1982482
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0917

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/DHn;->m:I

    .line 1982483
    return-void
.end method


# virtual methods
.method public final a(IZZ)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1982484
    invoke-virtual {p0}, LX/DHn;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0917

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1982485
    invoke-virtual {p0}, LX/DHn;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 1982486
    sub-int v1, v4, v0

    .line 1982487
    iget-object v0, p0, LX/DHn;->i:Landroid/widget/LinearLayout;

    const v2, 0x7f020a3c

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1982488
    iget-object v0, p0, LX/DHn;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1982489
    if-eqz v0, :cond_0

    .line 1982490
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1982491
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 1982492
    invoke-virtual {p0}, LX/DHn;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b010d

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1982493
    :cond_0
    iput-boolean v3, p0, LX/DHn;->o:Z

    .line 1982494
    if-eqz p2, :cond_3

    move v2, v3

    .line 1982495
    :goto_0
    if-eqz p3, :cond_1

    iget-object v0, p0, LX/DHn;->j:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1982496
    iget-object v0, p0, LX/DHn;->j:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, LX/DHn;->j:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    iget-object v1, p0, LX/DHn;->j:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    invoke-virtual {v0, v2, v5, v2, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 1982497
    :cond_1
    if-eqz p2, :cond_4

    .line 1982498
    invoke-virtual {p0, p1}, LX/DHn;->setWidth(I)V

    .line 1982499
    iget-object v0, p0, LX/DHn;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v4, v4, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1982500
    iget-object v0, p0, LX/DHn;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1982501
    if-eqz v0, :cond_2

    .line 1982502
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1982503
    :cond_2
    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/DHn;->setMenuButtonActive(Z)V

    .line 1982504
    return-void

    .line 1982505
    :cond_3
    mul-int/lit8 v0, v1, -0x1

    move v2, v0

    goto :goto_0

    .line 1982506
    :cond_4
    iget-object v0, p0, LX/DHn;->j:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1982507
    iget-object v0, p0, LX/DHn;->j:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public final a(LX/0hs;)V
    .locals 0

    .prologue
    .line 1982508
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1982460
    iget-boolean v0, p0, LX/DHn;->a:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1982511
    iget-object v0, p0, LX/DHn;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1982512
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAttachmentContainer()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 1982509
    iget-object v0, p0, LX/DHn;->i:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getBlingBar()Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;
    .locals 1

    .prologue
    .line 1982510
    iget-object v0, p0, LX/DHn;->f:Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;

    return-object v0
.end method

.method public getContentText()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1982461
    iget-object v0, p0, LX/DHn;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method public getFooter()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 1982462
    iget-object v0, p0, LX/DHn;->h:Landroid/view/View;

    return-object v0
.end method

.method public getLabelAndButton()Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;
    .locals 1

    .prologue
    .line 1982447
    iget-object v0, p0, LX/DHn;->e:Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x30dcbc86

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1982427
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1982428
    const/4 v1, 0x1

    .line 1982429
    iput-boolean v1, p0, LX/DHn;->a:Z

    .line 1982430
    const/16 v1, 0x2d

    const v2, 0x2e9a354

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x13b2c6b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1982431
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1982432
    const/4 v1, 0x0

    .line 1982433
    iput-boolean v1, p0, LX/DHn;->a:Z

    .line 1982434
    const/16 v1, 0x2d

    const v2, 0x59a0f6d9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1982435
    invoke-super {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1982436
    invoke-virtual {p0}, LX/DHn;->getPaddingTop()I

    move-result v0

    .line 1982437
    invoke-virtual {p0}, LX/DHn;->getWidth()I

    move-result v1

    .line 1982438
    iget-boolean v2, p0, LX/DHn;->n:Z

    if-eqz v2, :cond_0

    .line 1982439
    iget-object v2, p0, LX/DHn;->k:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2}, Lcom/facebook/widget/CustomLinearLayout;->getBottom()I

    move-result v2

    .line 1982440
    iget-object v3, p0, LX/DHn;->l:Landroid/graphics/drawable/ColorDrawable;

    iget v4, p0, LX/DHn;->m:I

    add-int v5, v0, v2

    iget v6, p0, LX/DHn;->m:I

    sub-int v6, v1, v6

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v3, v4, v5, v6, v2}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 1982441
    iget-object v2, p0, LX/DHn;->l:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 1982442
    :cond_0
    iget-boolean v2, p0, LX/DHn;->o:Z

    if-eqz v2, :cond_1

    .line 1982443
    iget-object v2, p0, LX/DHn;->f:Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;

    invoke-virtual {v2}, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->getTop()I

    move-result v2

    .line 1982444
    iget-object v3, p0, LX/DHn;->l:Landroid/graphics/drawable/ColorDrawable;

    iget v4, p0, LX/DHn;->m:I

    add-int v5, v0, v2

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, LX/DHn;->m:I

    sub-int/2addr v1, v6

    add-int/2addr v0, v2

    invoke-virtual {v3, v4, v5, v1, v0}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 1982445
    iget-object v0, p0, LX/DHn;->l:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 1982446
    :cond_1
    return-void
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 1982448
    iget-object v1, p0, LX/DHn;->d:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1982449
    return-void

    .line 1982450
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setSocialContextText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1982451
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    .line 1982452
    :goto_0
    iget-object v2, p0, LX/DHn;->g:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1982453
    iget-object v2, p0, LX/DHn;->g:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1982454
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, LX/DHn;->n:Z

    .line 1982455
    return-void

    .line 1982456
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setWidth(I)V
    .locals 1

    .prologue
    .line 1982457
    invoke-virtual {p0}, LX/DHn;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/DHn;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v0, p1, :cond_0

    .line 1982458
    invoke-virtual {p0}, LX/DHn;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1982459
    :cond_0
    return-void
.end method
