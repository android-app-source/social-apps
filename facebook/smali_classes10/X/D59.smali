.class public LX/D59;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/0sV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1963377
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D59;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1963378
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1963379
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D59;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1963380
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1963381
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1963382
    const-class v0, LX/D59;

    invoke-static {v0, p0}, LX/D59;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1963383
    iget-object v0, p0, LX/D59;->a:LX/0sV;

    iget-boolean v0, v0, LX/0sV;->r:Z

    if-eqz v0, :cond_0

    const v0, 0x7f03026e

    .line 1963384
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1963385
    return-void

    .line 1963386
    :cond_0
    const v0, 0x7f03026d

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/D59;

    invoke-static {p0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object p0

    check-cast p0, LX/0sV;

    iput-object p0, p1, LX/D59;->a:LX/0sV;

    return-void
.end method
