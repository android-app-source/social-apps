.class public final LX/D69;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideosByVideoChannelQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Zp;

.field public final synthetic b:LX/D6B;


# direct methods
.method public constructor <init>(LX/D6B;LX/1Zp;)V
    .locals 0

    .prologue
    .line 1965167
    iput-object p1, p0, LX/D69;->b:LX/D6B;

    iput-object p2, p0, LX/D69;->a:LX/1Zp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1965168
    iget-object v0, p0, LX/D69;->b:LX/D6B;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/D6B;->b(LX/D6B;Z)V

    .line 1965169
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1965170
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1965171
    iget-object v0, p0, LX/D69;->b:LX/D6B;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/D6B;->b(LX/D6B;Z)V

    .line 1965172
    if-eqz p1, :cond_0

    .line 1965173
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1965174
    if-eqz v0, :cond_0

    .line 1965175
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1965176
    check-cast v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideosByVideoChannelQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideosByVideoChannelQueryModel;->a()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D69;->a:LX/1Zp;

    invoke-virtual {v0}, LX/1Zp;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1965177
    :cond_0
    :goto_0
    return-void

    .line 1965178
    :cond_1
    iget-object v1, p0, LX/D69;->b:LX/D6B;

    .line 1965179
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1965180
    check-cast v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideosByVideoChannelQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$FetchVideosByVideoChannelQueryModel;->a()Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;

    move-result-object v0

    .line 1965181
    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object p0

    iput-object p0, v1, LX/D6B;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1965182
    iget-object p0, v1, LX/D6B;->c:LX/D5X;

    if-eqz p0, :cond_2

    .line 1965183
    iget-object p0, v1, LX/D6B;->c:LX/D5X;

    iget-object p1, v1, LX/D6B;->d:Ljava/lang/String;

    invoke-interface {p0, v1, p1, v0}, LX/D5X;->a(LX/D6B;Ljava/lang/String;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFeedFragmentModel;)V

    .line 1965184
    :cond_2
    goto :goto_0
.end method
