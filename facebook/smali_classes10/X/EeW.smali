.class public final LX/EeW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/EeX;

.field private b:Lcom/facebook/appupdate/ReleaseInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public e:Ljava/lang/Integer;

.field public f:J

.field private g:J

.field public h:J

.field public i:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/Throwable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I

.field public m:I

.field private n:LX/Eea;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/EeX;)V
    .locals 2

    .prologue
    .line 2152813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152814
    iput-object p1, p0, LX/EeW;->a:LX/EeX;

    .line 2152815
    iget-object v0, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    iput-object v0, p0, LX/EeW;->b:Lcom/facebook/appupdate/ReleaseInfo;

    .line 2152816
    iget-boolean v0, p1, LX/EeX;->isDiffDownloadEnabled:Z

    iput-boolean v0, p0, LX/EeW;->c:Z

    .line 2152817
    iget-boolean v0, p1, LX/EeX;->isWifiOnly:Z

    iput-boolean v0, p0, LX/EeW;->d:Z

    .line 2152818
    iget-object v0, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    iput-object v0, p0, LX/EeW;->e:Ljava/lang/Integer;

    .line 2152819
    iget-wide v0, p1, LX/EeX;->downloadId:J

    iput-wide v0, p0, LX/EeW;->f:J

    .line 2152820
    iget-wide v0, p1, LX/EeX;->downloadProgress:J

    iput-wide v0, p0, LX/EeW;->g:J

    .line 2152821
    iget-wide v0, p1, LX/EeX;->downloadSize:J

    iput-wide v0, p0, LX/EeW;->h:J

    .line 2152822
    iget-object v0, p1, LX/EeX;->localFile:Ljava/io/File;

    iput-object v0, p0, LX/EeW;->i:Ljava/io/File;

    .line 2152823
    iget-object v0, p1, LX/EeX;->localDiffDownloadFile:Ljava/io/File;

    iput-object v0, p0, LX/EeW;->j:Ljava/io/File;

    .line 2152824
    iget-object v0, p1, LX/EeX;->failureReason:Ljava/lang/Throwable;

    iput-object v0, p0, LX/EeW;->k:Ljava/lang/Throwable;

    .line 2152825
    iget v0, p1, LX/EeX;->downloadManagerStatus:I

    iput v0, p0, LX/EeW;->l:I

    .line 2152826
    iget v0, p1, LX/EeX;->downloadManagerReason:I

    iput v0, p0, LX/EeW;->m:I

    .line 2152827
    iget-object v0, p1, LX/EeX;->mDownloadSpeedTracker:LX/Eea;

    iput-object v0, p0, LX/EeW;->n:LX/Eea;

    .line 2152828
    return-void
.end method


# virtual methods
.method public final a()LX/EeX;
    .locals 23

    .prologue
    .line 2152829
    new-instance v2, LX/EeX;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/EeW;->b:Lcom/facebook/appupdate/ReleaseInfo;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/EeW;->a:LX/EeX;

    iget-boolean v4, v4, LX/EeX;->isBackgroundMode:Z

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/EeW;->c:Z

    move-object/from16 v0, p0

    iget-object v6, v0, LX/EeW;->a:LX/EeX;

    iget-boolean v6, v6, LX/EeX;->isSelfUpdate:Z

    move-object/from16 v0, p0

    iget-boolean v7, v0, LX/EeW;->d:Z

    move-object/from16 v0, p0

    iget-object v8, v0, LX/EeW;->e:Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/EeW;->a:LX/EeX;

    iget-object v9, v9, LX/EeX;->operationUuid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v10, v0, LX/EeW;->f:J

    move-object/from16 v0, p0

    iget-wide v12, v0, LX/EeW;->g:J

    move-object/from16 v0, p0

    iget-wide v14, v0, LX/EeW;->h:J

    move-object/from16 v0, p0

    iget-object v0, v0, LX/EeW;->i:Ljava/io/File;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/EeW;->j:Ljava/io/File;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/EeW;->k:Ljava/lang/Throwable;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/EeW;->l:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/EeW;->m:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/EeW;->a:LX/EeX;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, LX/EeX;->extras:Ljava/util/HashMap;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/EeW;->n:LX/Eea;

    move-object/from16 v22, v0

    invoke-direct/range {v2 .. v22}, LX/EeX;-><init>(Lcom/facebook/appupdate/ReleaseInfo;ZZZZLjava/lang/Integer;Ljava/lang/String;JJJLjava/io/File;Ljava/io/File;Ljava/lang/Throwable;IILjava/util/Map;LX/Eea;)V

    return-object v2
.end method

.method public final b(J)LX/EeW;
    .locals 5

    .prologue
    .line 2152830
    iput-wide p1, p0, LX/EeW;->g:J

    .line 2152831
    iget-object v0, p0, LX/EeW;->n:LX/Eea;

    if-nez v0, :cond_0

    .line 2152832
    new-instance v0, LX/Eea;

    invoke-direct {v0}, LX/Eea;-><init>()V

    iput-object v0, p0, LX/EeW;->n:LX/Eea;

    .line 2152833
    :goto_0
    return-object p0

    .line 2152834
    :cond_0
    iget-object v0, p0, LX/EeW;->n:LX/Eea;

    iget-object v1, p0, LX/EeW;->a:LX/EeX;

    iget-wide v2, v1, LX/EeX;->downloadProgress:J

    sub-long v2, p1, v2

    invoke-virtual {v0, v2, v3}, LX/Eea;->a(J)V

    goto :goto_0
.end method

.method public final c(J)LX/EeW;
    .locals 1

    .prologue
    .line 2152835
    iput-wide p1, p0, LX/EeW;->h:J

    .line 2152836
    return-object p0
.end method
