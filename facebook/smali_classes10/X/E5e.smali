.class public LX/E5e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/E5e;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078727
    return-void
.end method

.method public static a(LX/0QB;)LX/E5e;
    .locals 3

    .prologue
    .line 2078728
    sget-object v0, LX/E5e;->a:LX/E5e;

    if-nez v0, :cond_1

    .line 2078729
    const-class v1, LX/E5e;

    monitor-enter v1

    .line 2078730
    :try_start_0
    sget-object v0, LX/E5e;->a:LX/E5e;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078731
    if-eqz v2, :cond_0

    .line 2078732
    :try_start_1
    new-instance v0, LX/E5e;

    invoke-direct {v0}, LX/E5e;-><init>()V

    .line 2078733
    move-object v0, v0

    .line 2078734
    sput-object v0, LX/E5e;->a:LX/E5e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078735
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078736
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078737
    :cond_1
    sget-object v0, LX/E5e;->a:LX/E5e;

    return-object v0

    .line 2078738
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
