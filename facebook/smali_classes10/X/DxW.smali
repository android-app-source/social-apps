.class public LX/DxW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/01T;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/DwI;

.field public final e:LX/DwF;

.field public f:Lcom/facebook/graphql/model/GraphQLAlbum;

.field public g:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

.field public h:Ljava/lang/String;

.field public i:Landroid/content/res/Resources;

.field private j:J

.field private k:Z

.field public l:Lcom/facebook/ipc/composer/intent/ComposerTargetData;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/res/Resources;LX/0Or;LX/0Ot;LX/DwI;LX/DwF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "Landroid/content/res/Resources;",
            "LX/0Or",
            "<",
            "LX/01T;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "LX/DwI;",
            "LX/DwF;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2062939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2062940
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/DxW;->j:J

    .line 2062941
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DxW;->k:Z

    .line 2062942
    iput-object p1, p0, LX/DxW;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2062943
    iput-object p2, p0, LX/DxW;->i:Landroid/content/res/Resources;

    .line 2062944
    iput-object p3, p0, LX/DxW;->b:LX/0Or;

    .line 2062945
    iput-object p4, p0, LX/DxW;->c:LX/0Ot;

    .line 2062946
    iput-object p5, p0, LX/DxW;->d:LX/DwI;

    .line 2062947
    iput-object p6, p0, LX/DxW;->e:LX/DwF;

    .line 2062948
    return-void
.end method

.method public static synthetic a(LX/DxW;Landroid/view/View;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 2062937
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class p0, Landroid/app/Activity;

    invoke-static {v0, p0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    move-object v0, v0

    .line 2062938
    return-object v0
.end method

.method private a(LX/8AB;Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 2062927
    sget-object v0, LX/DxV;->a:[I

    invoke-virtual {p1}, LX/8AB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2062928
    :goto_0
    return-void

    .line 2062929
    :pswitch_0
    sget-object v1, LX/8A9;->NONE:LX/8A9;

    .line 2062930
    const/16 v0, 0x7d1

    .line 2062931
    :goto_1
    new-instance v2, LX/8AA;

    invoke-direct {v2, p1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v2}, LX/8AA;->i()LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->j()LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->l()LX/8AA;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    .line 2062932
    invoke-static {p2, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v1

    .line 2062933
    iget-object v2, p0, LX/DxW;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    .line 2062934
    :pswitch_1
    sget-object v1, LX/8A9;->LAUNCH_PROFILE_PIC_CROPPER:LX/8A9;

    .line 2062935
    const/16 v0, 0x7d0

    .line 2062936
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/app/Activity;LX/8AB;LX/8A9;I)V
    .locals 3

    .prologue
    .line 2062921
    new-instance v0, LX/8AA;

    invoke-direct {v0, p2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 2062922
    iget-object v1, p0, LX/DxW;->l:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v1}, LX/DwG;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/DxW;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/01T;->PAA:LX/01T;

    if-ne v1, v2, :cond_0

    .line 2062923
    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    .line 2062924
    :cond_0
    invoke-static {p1, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2062925
    iget-object v1, p0, LX/DxW;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p4, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2062926
    return-void
.end method

.method public static b(LX/0QB;)LX/DxW;
    .locals 7

    .prologue
    .line 2062919
    new-instance v0, LX/DxW;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const/16 v3, 0x3df

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2b68

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/DwI;->b(LX/0QB;)LX/DwI;

    move-result-object v5

    check-cast v5, LX/DwI;

    invoke-static {p0}, LX/DwF;->b(LX/0QB;)LX/DwF;

    move-result-object v6

    check-cast v6, LX/DwF;

    invoke-direct/range {v0 .. v6}, LX/DxW;-><init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/res/Resources;LX/0Or;LX/0Ot;LX/DwI;LX/DwF;)V

    .line 2062920
    return-object v0
.end method

.method public static b$redex0(LX/DxW;Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 2062915
    iget-object v0, p0, LX/DxW;->l:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v0}, LX/DwG;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2062916
    sget-object v0, LX/8AB;->PAGE_PROFILE_PIC:LX/8AB;

    invoke-direct {p0, v0, p1}, LX/DxW;->a(LX/8AB;Landroid/app/Activity;)V

    .line 2062917
    :goto_0
    return-void

    .line 2062918
    :cond_0
    sget-object v0, LX/8AB;->PROFILEPIC:LX/8AB;

    sget-object v1, LX/8A9;->LAUNCH_PROFILE_PIC_EDIT_GALLERY:LX/8A9;

    const/16 v2, 0x3e9

    invoke-direct {p0, p1, v0, v1, v2}, LX/DxW;->a(Landroid/app/Activity;LX/8AB;LX/8A9;I)V

    goto :goto_0
.end method

.method public static c$redex0(LX/DxW;Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 2062881
    iget-object v0, p0, LX/DxW;->l:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v0}, LX/DwG;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2062882
    sget-object v0, LX/8AB;->PAGE_COVER_PHOTO:LX/8AB;

    invoke-direct {p0, v0, p1}, LX/DxW;->a(LX/8AB;Landroid/app/Activity;)V

    .line 2062883
    :goto_0
    return-void

    .line 2062884
    :cond_0
    sget-object v0, LX/8AB;->COVERPHOTO:LX/8AB;

    sget-object v1, LX/8A9;->NONE:LX/8A9;

    const/16 v2, 0x3ea

    invoke-direct {p0, p1, v0, v1, v2}, LX/DxW;->a(Landroid/app/Activity;LX/8AB;LX/8A9;I)V

    goto :goto_0
.end method

.method private static d(LX/DxW;)Z
    .locals 2

    .prologue
    .line 2062914
    iget-object v0, p0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(LX/DxW;)Z
    .locals 2

    .prologue
    .line 2062913
    iget-object v0, p0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->COVER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/app/Activity;)V
    .locals 8

    .prologue
    .line 2062902
    iput-object p1, p0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2062903
    invoke-static {p0}, LX/DxW;->d(LX/DxW;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2062904
    invoke-static {p0, p2}, LX/DxW;->b$redex0(LX/DxW;Landroid/app/Activity;)V

    .line 2062905
    :goto_0
    iget-boolean v0, p0, LX/DxW;->k:Z

    if-eqz v0, :cond_0

    .line 2062906
    iget-object v0, p0, LX/DxW;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9XE;

    iget-wide v2, p0, LX/DxW;->j:J

    const-string v4, "album_detail_view"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    .line 2062907
    :goto_1
    iget-object v0, v1, LX/9XE;->a:LX/0Zb;

    sget-object v7, LX/9XI;->EVENT_TAPPED_ADD_PHOTOS:LX/9XI;

    invoke-static {v7, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "location"

    invoke-virtual {v7, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "album_id"

    invoke-virtual {v7, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "album_name"

    invoke-virtual {v7, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v0, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2062908
    :cond_0
    return-void

    .line 2062909
    :cond_1
    invoke-static {p0}, LX/DxW;->e(LX/DxW;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2062910
    invoke-static {p0, p2}, LX/DxW;->c$redex0(LX/DxW;Landroid/app/Activity;)V

    goto :goto_0

    .line 2062911
    :cond_2
    iget-object v7, p0, LX/DxW;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/DxW;->e:LX/DwF;

    iget-object v2, p0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    sget-object v3, LX/8AB;->ALBUM:LX/8AB;

    sget-object v4, LX/21D;->ALBUM:LX/21D;

    iget-object v1, p0, LX/DxW;->l:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v1}, LX/DwG;->c(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v5, "eventAlbum"

    :goto_2
    const/4 v6, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, LX/DwF;->a(Landroid/app/Activity;Lcom/facebook/graphql/model/GraphQLAlbum;LX/8AB;LX/21D;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x7d2

    invoke-interface {v7, v0, v1, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    :cond_3
    const-string v5, "album"

    goto :goto_2

    .line 2062912
    :cond_4
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/String;JZ)V
    .locals 3
    .param p3    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2062887
    iput-object p1, p0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2062888
    iput-object p4, p0, LX/DxW;->h:Ljava/lang/String;

    .line 2062889
    iput-object p3, p0, LX/DxW;->l:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2062890
    iput-wide p5, p0, LX/DxW;->j:J

    .line 2062891
    iput-boolean p7, p0, LX/DxW;->k:Z

    .line 2062892
    iget-object v0, p0, LX/DxW;->l:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-nez v0, :cond_0

    .line 2062893
    iget-object v0, p0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {v0}, LX/DwG;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iput-object v0, p0, LX/DxW;->l:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2062894
    :cond_0
    invoke-static {p0}, LX/DxW;->e(LX/DxW;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2062895
    const v0, 0x7f0811d9

    invoke-virtual {p2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2062896
    :cond_1
    :goto_0
    new-instance v0, LX/DxQ;

    invoke-direct {v0, p0}, LX/DxQ;-><init>(LX/DxW;)V

    invoke-virtual {p2, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2062897
    return-void

    .line 2062898
    :cond_2
    invoke-static {p0}, LX/DxW;->d(LX/DxW;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2062899
    const v0, 0x7f0811d8

    invoke-virtual {p2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_0

    .line 2062900
    :cond_3
    iget-object v0, p0, LX/DxW;->d:LX/DwI;

    iget-object v1, p0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v2, p0, LX/DxW;->l:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1, v2}, LX/DwI;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2062901
    const v0, 0x7f0811d7

    invoke-virtual {p2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2062886
    iget-object v0, p0, LX/DxW;->g:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DxW;->g:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2062885
    iget-object v0, p0, LX/DxW;->g:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DxW;->g:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
