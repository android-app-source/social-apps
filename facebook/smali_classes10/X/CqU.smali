.class public final enum LX/CqU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CqU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CqU;

.field public static final enum CANVAS_MAP:LX/CqU;

.field public static final enum FEED_VIDEO:LX/CqU;

.field public static final enum SCRUBBABLE_GIFS:LX/CqU;

.field public static final enum SPHERICAL_PHOTO:LX/CqU;

.field public static final enum SPHERICAL_VIDEO:LX/CqU;

.field public static final enum UFI:LX/CqU;

.field public static final enum VIDEO_CONTROLS:LX/CqU;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1939886
    new-instance v0, LX/CqU;

    const-string v1, "UFI"

    invoke-direct {v0, v1, v3}, LX/CqU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CqU;->UFI:LX/CqU;

    .line 1939887
    new-instance v0, LX/CqU;

    const-string v1, "SCRUBBABLE_GIFS"

    invoke-direct {v0, v1, v4}, LX/CqU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CqU;->SCRUBBABLE_GIFS:LX/CqU;

    .line 1939888
    new-instance v0, LX/CqU;

    const-string v1, "VIDEO_CONTROLS"

    invoke-direct {v0, v1, v5}, LX/CqU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CqU;->VIDEO_CONTROLS:LX/CqU;

    .line 1939889
    new-instance v0, LX/CqU;

    const-string v1, "FEED_VIDEO"

    invoke-direct {v0, v1, v6}, LX/CqU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CqU;->FEED_VIDEO:LX/CqU;

    .line 1939890
    new-instance v0, LX/CqU;

    const-string v1, "CANVAS_MAP"

    invoke-direct {v0, v1, v7}, LX/CqU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CqU;->CANVAS_MAP:LX/CqU;

    .line 1939891
    new-instance v0, LX/CqU;

    const-string v1, "SPHERICAL_VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CqU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CqU;->SPHERICAL_VIDEO:LX/CqU;

    .line 1939892
    new-instance v0, LX/CqU;

    const-string v1, "SPHERICAL_PHOTO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/CqU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CqU;->SPHERICAL_PHOTO:LX/CqU;

    .line 1939893
    const/4 v0, 0x7

    new-array v0, v0, [LX/CqU;

    sget-object v1, LX/CqU;->UFI:LX/CqU;

    aput-object v1, v0, v3

    sget-object v1, LX/CqU;->SCRUBBABLE_GIFS:LX/CqU;

    aput-object v1, v0, v4

    sget-object v1, LX/CqU;->VIDEO_CONTROLS:LX/CqU;

    aput-object v1, v0, v5

    sget-object v1, LX/CqU;->FEED_VIDEO:LX/CqU;

    aput-object v1, v0, v6

    sget-object v1, LX/CqU;->CANVAS_MAP:LX/CqU;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CqU;->SPHERICAL_VIDEO:LX/CqU;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CqU;->SPHERICAL_PHOTO:LX/CqU;

    aput-object v2, v0, v1

    sput-object v0, LX/CqU;->$VALUES:[LX/CqU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1939894
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CqU;
    .locals 1

    .prologue
    .line 1939895
    const-class v0, LX/CqU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CqU;

    return-object v0
.end method

.method public static values()[LX/CqU;
    .locals 1

    .prologue
    .line 1939896
    sget-object v0, LX/CqU;->$VALUES:[LX/CqU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CqU;

    return-object v0
.end method
