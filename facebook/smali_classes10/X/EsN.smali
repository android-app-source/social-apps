.class public LX/EsN;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EsO;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EsN",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EsO;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175449
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2175450
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/EsN;->b:LX/0Zi;

    .line 2175451
    iput-object p1, p0, LX/EsN;->a:LX/0Ot;

    .line 2175452
    return-void
.end method

.method public static a(LX/0QB;)LX/EsN;
    .locals 4

    .prologue
    .line 2175453
    const-class v1, LX/EsN;

    monitor-enter v1

    .line 2175454
    :try_start_0
    sget-object v0, LX/EsN;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175455
    sput-object v2, LX/EsN;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175456
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175457
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175458
    new-instance v3, LX/EsN;

    const/16 p0, 0x1f1c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EsN;-><init>(LX/0Ot;)V

    .line 2175459
    move-object v0, v3

    .line 2175460
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175461
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EsN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175462
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175463
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2175464
    check-cast p2, LX/EsM;

    .line 2175465
    iget-object v0, p0, LX/EsN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EsO;

    iget-object v1, p2, LX/EsM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/EsM;->b:LX/1Pb;

    const/4 v7, 0x1

    .line 2175466
    invoke-static {v1}, LX/Es3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 2175467
    invoke-static {v4}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2175468
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 2175469
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2175470
    check-cast v3, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    const/4 v6, 0x0

    .line 2175471
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 2175472
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    .line 2175473
    :cond_0
    :goto_0
    move-object v3, v6

    .line 2175474
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, LX/EsO;->a:LX/1xN;

    invoke-virtual {v6, p1}, LX/1xN;->c(LX/1De;)LX/22O;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/22O;->a(LX/1Pb;)LX/22O;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/22O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22O;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    .line 2175475
    new-instance v6, LX/EsR;

    invoke-direct {v6}, LX/EsR;-><init>()V

    .line 2175476
    sget-object v8, LX/EsS;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/EsQ;

    .line 2175477
    if-nez v8, :cond_1

    .line 2175478
    new-instance v8, LX/EsQ;

    invoke-direct {v8}, LX/EsQ;-><init>()V

    .line 2175479
    :cond_1
    invoke-static {v8, p1, v5, v5, v6}, LX/EsQ;->a$redex0(LX/EsQ;LX/1De;IILX/EsR;)V

    .line 2175480
    move-object v6, v8

    .line 2175481
    move-object v5, v6

    .line 2175482
    move-object v5, v5

    .line 2175483
    iget-object v6, v5, LX/EsQ;->a:LX/EsR;

    iput-object v3, v6, LX/EsR;->a:Ljava/lang/String;

    .line 2175484
    iget-object v6, v5, LX/EsQ;->d:Ljava/util/BitSet;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/BitSet;->set(I)V

    .line 2175485
    move-object v3, v5

    .line 2175486
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Di;->c(I)LX/1Di;

    move-result-object v3

    const/4 v5, 0x2

    const v6, 0x7f0b1cf4

    invoke-interface {v3, v5, v6}, LX/1Di;->l(II)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b1cf4

    invoke-interface {v3, v7, v5}, LX/1Di;->l(II)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2175487
    return-object v0

    .line 2175488
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 2175489
    :cond_3
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 2175490
    :try_start_0
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 2175491
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0f012b

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p0, p2

    invoke-virtual {v9, v10, v8, p0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto/16 :goto_0

    .line 2175492
    :catch_0
    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2175493
    invoke-static {}, LX/1dS;->b()V

    .line 2175494
    const/4 v0, 0x0

    return-object v0
.end method
