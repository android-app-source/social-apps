.class public LX/Etb;
.super LX/98h;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/98h",
        "<",
        "Ljava/lang/Object;",
        "LX/95R;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;

.field private static volatile c:LX/Etb;


# instance fields
.field private final b:LX/EuP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2178346
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Etb;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/EuP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2178343
    invoke-direct {p0}, LX/98h;-><init>()V

    .line 2178344
    iput-object p1, p0, LX/Etb;->b:LX/EuP;

    .line 2178345
    return-void
.end method

.method public static a(LX/0QB;)LX/Etb;
    .locals 4

    .prologue
    .line 2178330
    sget-object v0, LX/Etb;->c:LX/Etb;

    if-nez v0, :cond_1

    .line 2178331
    const-class v1, LX/Etb;

    monitor-enter v1

    .line 2178332
    :try_start_0
    sget-object v0, LX/Etb;->c:LX/Etb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2178333
    if-eqz v2, :cond_0

    .line 2178334
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2178335
    new-instance p0, LX/Etb;

    invoke-static {v0}, LX/EuP;->a(LX/0QB;)LX/EuP;

    move-result-object v3

    check-cast v3, LX/EuP;

    invoke-direct {p0, v3}, LX/Etb;-><init>(LX/EuP;)V

    .line 2178336
    move-object v0, p0

    .line 2178337
    sput-object v0, LX/Etb;->c:LX/Etb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2178338
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2178339
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2178340
    :cond_1
    sget-object v0, LX/Etb;->c:LX/Etb;

    return-object v0

    .line 2178341
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2178342
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)LX/98g;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            ")",
            "LX/98g",
            "<",
            "Ljava/lang/Object;",
            "LX/95R;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2178317
    const-string v1, "fc_tab"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2178318
    const-string v1, "fc_tab"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2178319
    sget-object v2, LX/5P0;->FRIENDS:LX/5P0;

    invoke-virtual {v2}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2178320
    iget-object v1, p0, LX/Etb;->b:LX/EuP;

    invoke-virtual {v1}, LX/EuP;->c()LX/95R;

    move-result-object v1

    .line 2178321
    if-eqz v1, :cond_1

    .line 2178322
    invoke-virtual {v1}, LX/95R;->a()LX/2kW;

    move-result-object v2

    .line 2178323
    iget-object v3, v2, LX/2kW;->o:LX/2kM;

    move-object v3, v3

    .line 2178324
    invoke-interface {v3}, LX/2kM;->c()I

    move-result v3

    if-nez v3, :cond_0

    .line 2178325
    sget-object v3, LX/2nj;->a:LX/2nj;

    sget-object v4, LX/3DP;->FIRST:LX/3DP;

    const/16 v5, 0x14

    invoke-virtual {v2, v3, v4, v5, v0}, LX/2kW;->a(LX/2nj;LX/3DP;ILjava/lang/Object;)V

    .line 2178326
    :cond_0
    new-instance v0, LX/98g;

    sget-object v2, LX/Etb;->a:Ljava/lang/Object;

    invoke-direct {v0, v2, v1}, LX/98g;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2178327
    :cond_1
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2178329
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2178328
    const/4 v0, 0x1

    return v0
.end method
