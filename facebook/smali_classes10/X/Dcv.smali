.class public final enum LX/Dcv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dcv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dcv;

.field public static final enum MAXED_OUT:LX/Dcv;

.field public static final enum OPEN:LX/Dcv;

.field public static final enum RECORDING:LX/Dcv;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2019106
    new-instance v0, LX/Dcv;

    const-string v1, "RECORDING"

    invoke-direct {v0, v1, v2}, LX/Dcv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dcv;->RECORDING:LX/Dcv;

    .line 2019107
    new-instance v0, LX/Dcv;

    const-string v1, "OPEN"

    invoke-direct {v0, v1, v3}, LX/Dcv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dcv;->OPEN:LX/Dcv;

    .line 2019108
    new-instance v0, LX/Dcv;

    const-string v1, "MAXED_OUT"

    invoke-direct {v0, v1, v4}, LX/Dcv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dcv;->MAXED_OUT:LX/Dcv;

    .line 2019109
    const/4 v0, 0x3

    new-array v0, v0, [LX/Dcv;

    sget-object v1, LX/Dcv;->RECORDING:LX/Dcv;

    aput-object v1, v0, v2

    sget-object v1, LX/Dcv;->OPEN:LX/Dcv;

    aput-object v1, v0, v3

    sget-object v1, LX/Dcv;->MAXED_OUT:LX/Dcv;

    aput-object v1, v0, v4

    sput-object v0, LX/Dcv;->$VALUES:[LX/Dcv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2019110
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dcv;
    .locals 1

    .prologue
    .line 2019111
    const-class v0, LX/Dcv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dcv;

    return-object v0
.end method

.method public static values()[LX/Dcv;
    .locals 1

    .prologue
    .line 2019112
    sget-object v0, LX/Dcv;->$VALUES:[LX/Dcv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dcv;

    return-object v0
.end method
