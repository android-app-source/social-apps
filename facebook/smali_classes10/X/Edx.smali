.class public final LX/Edx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/about/AboutActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/about/AboutActivity;)V
    .locals 0

    .prologue
    .line 2151720
    iput-object p1, p0, LX/Edx;->a:Lcom/facebook/about/AboutActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x3d40c782

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2151721
    iget-object v0, p0, LX/Edx;->a:Lcom/facebook/about/AboutActivity;

    iget-object v0, v0, Lcom/facebook/about/AboutActivity;->x:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2151722
    iget-object v0, p0, LX/Edx;->a:Lcom/facebook/about/AboutActivity;

    iget-object v0, v0, Lcom/facebook/about/AboutActivity;->y:LX/0Xl;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "zero_token_request_reason"

    sget-object v4, LX/32P;->ABOUT_TITLE_CLICK:LX/32P;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2151723
    :cond_0
    const v0, -0xa5f30b5

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
