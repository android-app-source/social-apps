.class public final LX/EcU;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EcT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EcU;",
        ">;",
        "LX/EcT;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:LX/EWc;

.field private d:LX/EWc;

.field private e:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2146798
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2146799
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcU;->c:LX/EWc;

    .line 2146800
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcU;->d:LX/EWc;

    .line 2146801
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcU;->e:LX/EWc;

    .line 2146802
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2146803
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2146804
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcU;->c:LX/EWc;

    .line 2146805
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcU;->d:LX/EWc;

    .line 2146806
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcU;->e:LX/EWc;

    .line 2146807
    return-void
.end method

.method private d(LX/EWY;)LX/EcU;
    .locals 1

    .prologue
    .line 2146808
    instance-of v0, p1, LX/EcV;

    if-eqz v0, :cond_0

    .line 2146809
    check-cast p1, LX/EcV;

    invoke-virtual {p0, p1}, LX/EcU;->a(LX/EcV;)LX/EcU;

    move-result-object p0

    .line 2146810
    :goto_0
    return-object p0

    .line 2146811
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EcU;
    .locals 4

    .prologue
    .line 2146812
    const/4 v2, 0x0

    .line 2146813
    :try_start_0
    sget-object v0, LX/EcV;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcV;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2146814
    if-eqz v0, :cond_0

    .line 2146815
    invoke-virtual {p0, v0}, LX/EcU;->a(LX/EcV;)LX/EcU;

    .line 2146816
    :cond_0
    return-object p0

    .line 2146817
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2146818
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2146819
    check-cast v0, LX/EcV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2146820
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2146821
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2146822
    invoke-virtual {p0, v1}, LX/EcU;->a(LX/EcV;)LX/EcU;

    :cond_1
    throw v0

    .line 2146823
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static u()LX/EcU;
    .locals 1

    .prologue
    .line 2146824
    new-instance v0, LX/EcU;

    invoke-direct {v0}, LX/EcU;-><init>()V

    return-object v0
.end method

.method private w()LX/EcU;
    .locals 2

    .prologue
    .line 2146825
    invoke-static {}, LX/EcU;->u()LX/EcU;

    move-result-object v0

    invoke-direct {p0}, LX/EcU;->y()LX/EcV;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcU;->a(LX/EcV;)LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/EcV;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2146826
    new-instance v2, LX/EcV;

    invoke-direct {v2, p0}, LX/EcV;-><init>(LX/EWj;)V

    .line 2146827
    iget v3, p0, LX/EcU;->a:I

    .line 2146828
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 2146829
    :goto_0
    iget v1, p0, LX/EcU;->b:I

    .line 2146830
    iput v1, v2, LX/EcV;->index_:I

    .line 2146831
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2146832
    or-int/lit8 v0, v0, 0x2

    .line 2146833
    :cond_0
    iget-object v1, p0, LX/EcU;->c:LX/EWc;

    .line 2146834
    iput-object v1, v2, LX/EcV;->cipherKey_:LX/EWc;

    .line 2146835
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2146836
    or-int/lit8 v0, v0, 0x4

    .line 2146837
    :cond_1
    iget-object v1, p0, LX/EcU;->d:LX/EWc;

    .line 2146838
    iput-object v1, v2, LX/EcV;->macKey_:LX/EWc;

    .line 2146839
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 2146840
    or-int/lit8 v0, v0, 0x8

    .line 2146841
    :cond_2
    iget-object v1, p0, LX/EcU;->e:LX/EWc;

    .line 2146842
    iput-object v1, v2, LX/EcV;->iv_:LX/EWc;

    .line 2146843
    iput v0, v2, LX/EcV;->bitField0_:I

    .line 2146844
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2146845
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2146846
    invoke-direct {p0, p1}, LX/EcU;->d(LX/EWY;)LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2146847
    invoke-direct {p0, p1, p2}, LX/EcU;->d(LX/EWd;LX/EYZ;)LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/EcU;
    .locals 1

    .prologue
    .line 2146848
    iget v0, p0, LX/EcU;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EcU;->a:I

    .line 2146849
    iput p1, p0, LX/EcU;->b:I

    .line 2146850
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146851
    return-object p0
.end method

.method public final a(LX/EWc;)LX/EcU;
    .locals 1

    .prologue
    .line 2146852
    if-nez p1, :cond_0

    .line 2146853
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146854
    :cond_0
    iget v0, p0, LX/EcU;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EcU;->a:I

    .line 2146855
    iput-object p1, p0, LX/EcU;->c:LX/EWc;

    .line 2146856
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146857
    return-object p0
.end method

.method public final a(LX/EcV;)LX/EcU;
    .locals 2

    .prologue
    .line 2146858
    sget-object v0, LX/EcV;->c:LX/EcV;

    move-object v0, v0

    .line 2146859
    if-ne p1, v0, :cond_0

    .line 2146860
    :goto_0
    return-object p0

    .line 2146861
    :cond_0
    const/4 v0, 0x1

    .line 2146862
    iget v1, p1, LX/EcV;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_5

    :goto_1
    move v0, v0

    .line 2146863
    if-eqz v0, :cond_1

    .line 2146864
    iget v0, p1, LX/EcV;->index_:I

    move v0, v0

    .line 2146865
    invoke-virtual {p0, v0}, LX/EcU;->a(I)LX/EcU;

    .line 2146866
    :cond_1
    iget v0, p1, LX/EcV;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2146867
    if-eqz v0, :cond_2

    .line 2146868
    iget-object v0, p1, LX/EcV;->cipherKey_:LX/EWc;

    move-object v0, v0

    .line 2146869
    invoke-virtual {p0, v0}, LX/EcU;->a(LX/EWc;)LX/EcU;

    .line 2146870
    :cond_2
    iget v0, p1, LX/EcV;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2146871
    if-eqz v0, :cond_3

    .line 2146872
    iget-object v0, p1, LX/EcV;->macKey_:LX/EWc;

    move-object v0, v0

    .line 2146873
    invoke-virtual {p0, v0}, LX/EcU;->b(LX/EWc;)LX/EcU;

    .line 2146874
    :cond_3
    iget v0, p1, LX/EcV;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2146875
    if-eqz v0, :cond_4

    .line 2146876
    iget-object v0, p1, LX/EcV;->iv_:LX/EWc;

    move-object v0, v0

    .line 2146877
    invoke-virtual {p0, v0}, LX/EcU;->c(LX/EWc;)LX/EcU;

    .line 2146878
    :cond_4
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2146879
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2146880
    invoke-direct {p0, p1, p2}, LX/EcU;->d(LX/EWd;LX/EYZ;)LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2146769
    invoke-direct {p0}, LX/EcU;->w()LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/EWc;)LX/EcU;
    .locals 1

    .prologue
    .line 2146792
    if-nez p1, :cond_0

    .line 2146793
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146794
    :cond_0
    iget v0, p0, LX/EcU;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EcU;->a:I

    .line 2146795
    iput-object p1, p0, LX/EcU;->d:LX/EWc;

    .line 2146796
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146797
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2146768
    invoke-direct {p0, p1, p2}, LX/EcU;->d(LX/EWd;LX/EYZ;)LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2146770
    invoke-direct {p0}, LX/EcU;->w()LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2146771
    invoke-direct {p0, p1}, LX/EcU;->d(LX/EWY;)LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/EWc;)LX/EcU;
    .locals 1

    .prologue
    .line 2146772
    if-nez p1, :cond_0

    .line 2146773
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146774
    :cond_0
    iget v0, p0, LX/EcU;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EcU;->a:I

    .line 2146775
    iput-object p1, p0, LX/EcU;->e:LX/EWc;

    .line 2146776
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146777
    return-object p0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2146778
    invoke-direct {p0}, LX/EcU;->w()LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2146779
    sget-object v0, LX/Eck;->h:LX/EYn;

    const-class v1, LX/EcV;

    const-class v2, LX/EcU;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2146780
    sget-object v0, LX/Eck;->g:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2146781
    invoke-direct {p0}, LX/EcU;->w()LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2146782
    invoke-direct {p0}, LX/EcU;->y()LX/EcV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2146783
    invoke-virtual {p0}, LX/EcU;->l()LX/EcV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2146784
    invoke-direct {p0}, LX/EcU;->y()LX/EcV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2146785
    invoke-virtual {p0}, LX/EcU;->l()LX/EcV;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EcV;
    .locals 2

    .prologue
    .line 2146786
    invoke-direct {p0}, LX/EcU;->y()LX/EcV;

    move-result-object v0

    .line 2146787
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2146788
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2146789
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2146790
    sget-object v0, LX/EcV;->c:LX/EcV;

    move-object v0, v0

    .line 2146791
    return-object v0
.end method
