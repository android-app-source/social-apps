.class public LX/ECc;
.super Landroid/graphics/drawable/LayerDrawable;
.source ""


# instance fields
.field private final a:[Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>([Landroid/graphics/drawable/Drawable;[Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 2090263
    invoke-direct {p0, p1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 2090264
    iput-object p2, p0, LX/ECc;->a:[Landroid/content/res/ColorStateList;

    .line 2090265
    return-void
.end method


# virtual methods
.method public final isStateful()Z
    .locals 1

    .prologue
    .line 2090266
    const/4 v0, 0x1

    return v0
.end method

.method public final onStateChange([I)Z
    .locals 4

    .prologue
    .line 2090267
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/ECc;->a:[Landroid/content/res/ColorStateList;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2090268
    invoke-virtual {p0, v0}, LX/ECc;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, LX/ECc;->a:[Landroid/content/res/ColorStateList;

    aget-object v2, v2, v0

    iget-object v3, p0, LX/ECc;->a:[Landroid/content/res/ColorStateList;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v3

    invoke-virtual {v2, p1, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2090269
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2090270
    :cond_0
    invoke-super {p0, p1}, Landroid/graphics/drawable/LayerDrawable;->onStateChange([I)Z

    move-result v0

    return v0
.end method
