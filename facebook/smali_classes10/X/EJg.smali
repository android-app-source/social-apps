.class public final LX/EJg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/A2T;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:LX/CzL;

.field public final synthetic d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;LX/A2T;LX/1Pn;LX/CzL;)V
    .locals 0

    .prologue
    .line 2104919
    iput-object p1, p0, LX/EJg;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;

    iput-object p2, p0, LX/EJg;->a:LX/A2T;

    iput-object p3, p0, LX/EJg;->b:LX/1Pn;

    iput-object p4, p0, LX/EJg;->c:LX/CzL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x6efce557

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2104920
    iget-object v0, p0, LX/EJg;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;->c:LX/7j6;

    iget-object v1, p0, LX/EJg;->a:LX/A2T;

    invoke-interface {v1}, LX/A2T;->dW_()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/7iP;->GLOBAL_SEARCH:LX/7iP;

    invoke-virtual {v0, v1, v2}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 2104921
    iget-object v0, p0, LX/EJg;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/CvY;

    iget-object v0, p0, LX/EJg;->b:LX/1Pn;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    sget-object v10, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v11, LX/7CM;->OPEN_PRODUCT_DETAIL_PAGE:LX/7CM;

    iget-object v0, p0, LX/EJg;->b:LX/1Pn;

    check-cast v0, LX/CxP;

    iget-object v1, p0, LX/EJg;->c:LX/CzL;

    .line 2104922
    iget-object v2, v1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v2

    .line 2104923
    invoke-interface {v0, v1}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v12

    iget-object v13, p0, LX/EJg;->c:LX/CzL;

    iget-object v0, p0, LX/EJg;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/EJg;->b:LX/1Pn;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    sget-object v1, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v2, LX/7CM;->OPEN_PRODUCT_DETAIL_PAGE:LX/7CM;

    iget-object v3, p0, LX/EJg;->a:LX/A2T;

    invoke-interface {v3}, LX/A2T;->dW_()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iget-object v6, p0, LX/EJg;->c:LX/CzL;

    .line 2104924
    iget-object p0, v6, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v6, p0

    .line 2104925
    invoke-virtual {v6}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ae()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v0, v7

    move-object v1, v9

    move-object v2, v10

    move-object v3, v11

    move v4, v12

    move-object v5, v13

    invoke-virtual/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2104926
    const/4 v0, 0x2

    const/4 v1, 0x2

    const v2, -0x2a2772e9

    invoke-static {v0, v1, v2, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
