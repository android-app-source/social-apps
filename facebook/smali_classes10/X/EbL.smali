.class public final LX/EbL;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EbK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EbL;",
        ">;",
        "LX/EbK;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:LX/EWc;

.field private e:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2143494
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2143495
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbL;->d:LX/EWc;

    .line 2143496
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbL;->e:LX/EWc;

    .line 2143497
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2143498
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2143499
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbL;->d:LX/EWc;

    .line 2143500
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbL;->e:LX/EWc;

    .line 2143501
    return-void
.end method

.method private d(LX/EWY;)LX/EbL;
    .locals 1

    .prologue
    .line 2143502
    instance-of v0, p1, LX/EbM;

    if-eqz v0, :cond_0

    .line 2143503
    check-cast p1, LX/EbM;

    invoke-virtual {p0, p1}, LX/EbL;->a(LX/EbM;)LX/EbL;

    move-result-object p0

    .line 2143504
    :goto_0
    return-object p0

    .line 2143505
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EbL;
    .locals 4

    .prologue
    .line 2143506
    const/4 v2, 0x0

    .line 2143507
    :try_start_0
    sget-object v0, LX/EbM;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EbM;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2143508
    if-eqz v0, :cond_0

    .line 2143509
    invoke-virtual {p0, v0}, LX/EbL;->a(LX/EbM;)LX/EbL;

    .line 2143510
    :cond_0
    return-object p0

    .line 2143511
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2143512
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2143513
    check-cast v0, LX/EbM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2143514
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2143515
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2143516
    invoke-virtual {p0, v1}, LX/EbL;->a(LX/EbM;)LX/EbL;

    :cond_1
    throw v0

    .line 2143517
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static u()LX/EbL;
    .locals 1

    .prologue
    .line 2143518
    new-instance v0, LX/EbL;

    invoke-direct {v0}, LX/EbL;-><init>()V

    return-object v0
.end method

.method private w()LX/EbL;
    .locals 2

    .prologue
    .line 2143519
    invoke-static {}, LX/EbL;->u()LX/EbL;

    move-result-object v0

    invoke-direct {p0}, LX/EbL;->y()LX/EbM;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EbL;->a(LX/EbM;)LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/EbM;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2143446
    new-instance v2, LX/EbM;

    invoke-direct {v2, p0}, LX/EbM;-><init>(LX/EWj;)V

    .line 2143447
    iget v3, p0, LX/EbL;->a:I

    .line 2143448
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 2143449
    :goto_0
    iget v1, p0, LX/EbL;->b:I

    .line 2143450
    iput v1, v2, LX/EbM;->id_:I

    .line 2143451
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2143452
    or-int/lit8 v0, v0, 0x2

    .line 2143453
    :cond_0
    iget v1, p0, LX/EbL;->c:I

    .line 2143454
    iput v1, v2, LX/EbM;->iteration_:I

    .line 2143455
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2143456
    or-int/lit8 v0, v0, 0x4

    .line 2143457
    :cond_1
    iget-object v1, p0, LX/EbL;->d:LX/EWc;

    .line 2143458
    iput-object v1, v2, LX/EbM;->chainKey_:LX/EWc;

    .line 2143459
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 2143460
    or-int/lit8 v0, v0, 0x8

    .line 2143461
    :cond_2
    iget-object v1, p0, LX/EbL;->e:LX/EWc;

    .line 2143462
    iput-object v1, v2, LX/EbM;->signingKey_:LX/EWc;

    .line 2143463
    iput v0, v2, LX/EbM;->bitField0_:I

    .line 2143464
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2143465
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2143549
    invoke-direct {p0, p1}, LX/EbL;->d(LX/EWY;)LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2143520
    invoke-direct {p0, p1, p2}, LX/EbL;->d(LX/EWd;LX/EYZ;)LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/EbL;
    .locals 1

    .prologue
    .line 2143521
    iget v0, p0, LX/EbL;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EbL;->a:I

    .line 2143522
    iput p1, p0, LX/EbL;->b:I

    .line 2143523
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143524
    return-object p0
.end method

.method public final a(LX/EWc;)LX/EbL;
    .locals 1

    .prologue
    .line 2143525
    if-nez p1, :cond_0

    .line 2143526
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2143527
    :cond_0
    iget v0, p0, LX/EbL;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EbL;->a:I

    .line 2143528
    iput-object p1, p0, LX/EbL;->d:LX/EWc;

    .line 2143529
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143530
    return-object p0
.end method

.method public final a(LX/EbM;)LX/EbL;
    .locals 1

    .prologue
    .line 2143531
    sget-object v0, LX/EbM;->c:LX/EbM;

    move-object v0, v0

    .line 2143532
    if-ne p1, v0, :cond_0

    .line 2143533
    :goto_0
    return-object p0

    .line 2143534
    :cond_0
    invoke-virtual {p1}, LX/EbM;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2143535
    iget v0, p1, LX/EbM;->id_:I

    move v0, v0

    .line 2143536
    invoke-virtual {p0, v0}, LX/EbL;->a(I)LX/EbL;

    .line 2143537
    :cond_1
    invoke-virtual {p1}, LX/EbM;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2143538
    iget v0, p1, LX/EbM;->iteration_:I

    move v0, v0

    .line 2143539
    invoke-virtual {p0, v0}, LX/EbL;->b(I)LX/EbL;

    .line 2143540
    :cond_2
    invoke-virtual {p1}, LX/EbM;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2143541
    iget-object v0, p1, LX/EbM;->chainKey_:LX/EWc;

    move-object v0, v0

    .line 2143542
    invoke-virtual {p0, v0}, LX/EbL;->a(LX/EWc;)LX/EbL;

    .line 2143543
    :cond_3
    invoke-virtual {p1}, LX/EbM;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2143544
    iget-object v0, p1, LX/EbM;->signingKey_:LX/EWc;

    move-object v0, v0

    .line 2143545
    invoke-virtual {p0, v0}, LX/EbL;->b(LX/EWc;)LX/EbL;

    .line 2143546
    :cond_4
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2143547
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2143548
    invoke-direct {p0, p1, p2}, LX/EbL;->d(LX/EWd;LX/EYZ;)LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2143489
    invoke-direct {p0}, LX/EbL;->w()LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)LX/EbL;
    .locals 1

    .prologue
    .line 2143490
    iget v0, p0, LX/EbL;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EbL;->a:I

    .line 2143491
    iput p1, p0, LX/EbL;->c:I

    .line 2143492
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143493
    return-object p0
.end method

.method public final b(LX/EWc;)LX/EbL;
    .locals 1

    .prologue
    .line 2143471
    if-nez p1, :cond_0

    .line 2143472
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2143473
    :cond_0
    iget v0, p0, LX/EbL;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EbL;->a:I

    .line 2143474
    iput-object p1, p0, LX/EbL;->e:LX/EWc;

    .line 2143475
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143476
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2143466
    invoke-direct {p0, p1, p2}, LX/EbL;->d(LX/EWd;LX/EYZ;)LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2143467
    invoke-direct {p0}, LX/EbL;->w()LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2143468
    invoke-direct {p0, p1}, LX/EbL;->d(LX/EWY;)LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2143469
    invoke-direct {p0}, LX/EbL;->w()LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2143470
    sget-object v0, LX/EbV;->j:LX/EYn;

    const-class v1, LX/EbM;

    const-class v2, LX/EbL;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2143477
    sget-object v0, LX/EbV;->i:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2143478
    invoke-direct {p0}, LX/EbL;->w()LX/EbL;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2143479
    invoke-direct {p0}, LX/EbL;->y()LX/EbM;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2143480
    invoke-virtual {p0}, LX/EbL;->l()LX/EbM;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2143481
    invoke-direct {p0}, LX/EbL;->y()LX/EbM;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2143482
    invoke-virtual {p0}, LX/EbL;->l()LX/EbM;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EbM;
    .locals 2

    .prologue
    .line 2143483
    invoke-direct {p0}, LX/EbL;->y()LX/EbM;

    move-result-object v0

    .line 2143484
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2143485
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2143486
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2143487
    sget-object v0, LX/EbM;->c:LX/EbM;

    move-object v0, v0

    .line 2143488
    return-object v0
.end method
