.class public LX/E2D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/E2D;


# instance fields
.field private a:LX/E2B;


# direct methods
.method public constructor <init>(LX/E2B;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2072463
    iput-object p1, p0, LX/E2D;->a:LX/E2B;

    .line 2072464
    return-void
.end method

.method public static a(LX/0QB;)LX/E2D;
    .locals 4

    .prologue
    .line 2072465
    sget-object v0, LX/E2D;->b:LX/E2D;

    if-nez v0, :cond_1

    .line 2072466
    const-class v1, LX/E2D;

    monitor-enter v1

    .line 2072467
    :try_start_0
    sget-object v0, LX/E2D;->b:LX/E2D;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072468
    if-eqz v2, :cond_0

    .line 2072469
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072470
    new-instance p0, LX/E2D;

    invoke-static {v0}, LX/E2B;->a(LX/0QB;)LX/E2B;

    move-result-object v3

    check-cast v3, LX/E2B;

    invoke-direct {p0, v3}, LX/E2D;-><init>(LX/E2B;)V

    .line 2072471
    move-object v0, p0

    .line 2072472
    sput-object v0, LX/E2D;->b:LX/E2D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072473
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072474
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072475
    :cond_1
    sget-object v0, LX/E2D;->b:LX/E2D;

    return-object v0

    .line 2072476
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072477
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/E2D;LX/1De;LX/1Dh;LX/174;Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;)V
    .locals 3

    .prologue
    .line 2072478
    if-eqz p3, :cond_0

    invoke-interface {p3}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p4, :cond_1

    .line 2072479
    :cond_0
    :goto_0
    return-void

    .line 2072480
    :cond_1
    iget-object v0, p0, LX/E2D;->a:LX/E2B;

    invoke-virtual {v0, p1}, LX/E2B;->c(LX/1De;)LX/E29;

    move-result-object v0

    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2072481
    :try_start_0
    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "#"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p0

    .line 2072482
    :goto_1
    move v1, p0

    .line 2072483
    invoke-virtual {v0, v1}, LX/E29;->h(I)LX/E29;

    move-result-object v0

    invoke-interface {p3}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E29;->a(Ljava/lang/CharSequence;)LX/E29;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/E29;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;)LX/E29;

    move-result-object v0

    invoke-interface {p2, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    goto :goto_0

    :catch_0
    const p0, 0x7f0a010e

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    goto :goto_1
.end method
