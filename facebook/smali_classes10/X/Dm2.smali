.class public final LX/Dm2;
.super LX/Dm1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Dm1",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V
    .locals 0

    .prologue
    .line 2038825
    iput-object p1, p0, LX/Dm2;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    invoke-direct {p0, p2, p3, p4}, LX/Dm1;-><init>(Landroid/view/View;ZLX/0wM;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 3

    .prologue
    .line 2038826
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->l()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel;->n()Ljava/lang/String;

    move-result-object v0

    .line 2038827
    invoke-static {p1}, LX/DnS;->e(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v1

    .line 2038828
    const v2, 0x7f020964

    invoke-virtual {p0, v2, v0, v1}, LX/Dlw;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2038829
    iput-object p1, p0, LX/Dm1;->l:Ljava/lang/Object;

    .line 2038830
    return-void
.end method

.method public bridge synthetic onClick(Landroid/view/View;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2038831
    check-cast p2, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    .line 2038832
    invoke-static {p2}, LX/DnS;->g(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v0

    .line 2038833
    iget-object v1, p0, LX/Dm2;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->f:LX/Dih;

    invoke-static {p2}, LX/DnS;->h(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, LX/DnS;->i(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v3

    .line 2038834
    iget-object v4, v1, LX/Dih;->a:LX/0Zb;

    const-string v5, "profservices_booking_consumer_click_address"

    invoke-static {v5, v0}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "request_id"

    invoke-virtual {v5, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "booking_status"

    invoke-virtual {v5, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2038835
    invoke-static {p2}, LX/DnS;->e(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v1

    .line 2038836
    if-nez v1, :cond_0

    .line 2038837
    iget-object v1, p0, LX/Dm2;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    invoke-static {v1, v0}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Ljava/lang/String;)V

    .line 2038838
    :goto_0
    return-void

    .line 2038839
    :cond_0
    iget-object v0, p0, LX/Dm2;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    invoke-static {v0, p2}, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->b(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V

    goto :goto_0
.end method
