.class public final enum LX/EQG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EQG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EQG;

.field public static final enum EMPTY:LX/EQG;

.field public static final enum ERROR:LX/EQG;

.field public static final enum ERROR_LOADING_MORE:LX/EQG;

.field public static final enum LOADING:LX/EQG;

.field public static final enum LOADING_FINISHED:LX/EQG;

.field public static final enum LOADING_FINISHED_NO_RESULTS:LX/EQG;

.field public static final enum LOADING_MORE:LX/EQG;

.field public static final enum REQUEST_TIMED_OUT:LX/EQG;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2118450
    new-instance v0, LX/EQG;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/EQG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQG;->LOADING:LX/EQG;

    .line 2118451
    new-instance v0, LX/EQG;

    const-string v1, "LOADING_MORE"

    invoke-direct {v0, v1, v4}, LX/EQG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQG;->LOADING_MORE:LX/EQG;

    .line 2118452
    new-instance v0, LX/EQG;

    const-string v1, "LOADING_FINISHED_NO_RESULTS"

    invoke-direct {v0, v1, v5}, LX/EQG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQG;->LOADING_FINISHED_NO_RESULTS:LX/EQG;

    .line 2118453
    new-instance v0, LX/EQG;

    const-string v1, "LOADING_FINISHED"

    invoke-direct {v0, v1, v6}, LX/EQG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQG;->LOADING_FINISHED:LX/EQG;

    .line 2118454
    new-instance v0, LX/EQG;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v7}, LX/EQG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQG;->EMPTY:LX/EQG;

    .line 2118455
    new-instance v0, LX/EQG;

    const-string v1, "ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EQG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQG;->ERROR:LX/EQG;

    .line 2118456
    new-instance v0, LX/EQG;

    const-string v1, "ERROR_LOADING_MORE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/EQG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQG;->ERROR_LOADING_MORE:LX/EQG;

    .line 2118457
    new-instance v0, LX/EQG;

    const-string v1, "REQUEST_TIMED_OUT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/EQG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EQG;->REQUEST_TIMED_OUT:LX/EQG;

    .line 2118458
    const/16 v0, 0x8

    new-array v0, v0, [LX/EQG;

    sget-object v1, LX/EQG;->LOADING:LX/EQG;

    aput-object v1, v0, v3

    sget-object v1, LX/EQG;->LOADING_MORE:LX/EQG;

    aput-object v1, v0, v4

    sget-object v1, LX/EQG;->LOADING_FINISHED_NO_RESULTS:LX/EQG;

    aput-object v1, v0, v5

    sget-object v1, LX/EQG;->LOADING_FINISHED:LX/EQG;

    aput-object v1, v0, v6

    sget-object v1, LX/EQG;->EMPTY:LX/EQG;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EQG;->ERROR:LX/EQG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EQG;->ERROR_LOADING_MORE:LX/EQG;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EQG;->REQUEST_TIMED_OUT:LX/EQG;

    aput-object v2, v0, v1

    sput-object v0, LX/EQG;->$VALUES:[LX/EQG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2118447
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EQG;
    .locals 1

    .prologue
    .line 2118449
    const-class v0, LX/EQG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EQG;

    return-object v0
.end method

.method public static values()[LX/EQG;
    .locals 1

    .prologue
    .line 2118448
    sget-object v0, LX/EQG;->$VALUES:[LX/EQG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EQG;

    return-object v0
.end method
