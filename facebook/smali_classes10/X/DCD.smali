.class public final LX/DCD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/composer/publish/common/EditPostParams;

.field public final synthetic c:LX/DCE;


# direct methods
.method public constructor <init>(LX/DCE;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/EditPostParams;)V
    .locals 0

    .prologue
    .line 1973753
    iput-object p1, p0, LX/DCD;->c:LX/DCE;

    iput-object p2, p0, LX/DCD;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/DCD;->b:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x2ce73f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1973754
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.facebook.STREAM_PUBLISH_COMPLETE"

    if-eq v1, v2, :cond_0

    .line 1973755
    const/16 v1, 0x27

    const v2, 0x4d8df67a    # 2.97717568E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1973756
    :goto_0
    return-void

    .line 1973757
    :cond_0
    const-string v1, "extra_result"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7m7;->valueOf(Ljava/lang/String;)LX/7m7;

    move-result-object v1

    .line 1973758
    sget-object v2, LX/7m7;->SUCCESS:LX/7m7;

    if-eq v1, v2, :cond_1

    .line 1973759
    iget-object v1, p0, LX/DCD;->c:LX/DCE;

    iget-object v2, p0, LX/DCD;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1973760
    iget-object v3, v1, LX/DCE;->d:LX/0qq;

    invoke-virtual {v3, v2}, LX/0qq;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1973761
    invoke-static {v1}, LX/DCE;->b$redex0(LX/DCE;)V

    .line 1973762
    const v1, 0x5ce9f86c

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0

    .line 1973763
    :cond_1
    const-string v1, "extra_legacy_api_post_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1973764
    iget-object v2, p0, LX/DCD;->b:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/EditPostParams;->getLegacyStoryApiId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1973765
    iget-object v2, p0, LX/DCD;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    .line 1973766
    iget-object v3, p0, LX/DCD;->c:LX/DCE;

    iget-object v3, v3, LX/DCE;->d:LX/0qq;

    .line 1973767
    iget-object p1, v3, LX/0qq;->a:LX/0fz;

    invoke-virtual {p1, v2}, LX/0fz;->c(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object p1

    move-object v2, p1

    .line 1973768
    if-nez v2, :cond_2

    .line 1973769
    const v1, -0x307bd555    # -4.4347776E9f

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0

    .line 1973770
    :cond_2
    iget-object v2, p0, LX/DCD;->c:LX/DCE;

    iget-object v2, v2, LX/DCE;->d:LX/0qq;

    .line 1973771
    iget-object v3, v2, LX/0qq;->a:LX/0fz;

    .line 1973772
    iget-object v2, v3, LX/0fz;->d:LX/0qm;

    move-object v3, v2

    .line 1973773
    invoke-virtual {v3, v1}, LX/0qm;->b(Ljava/lang/String;)Z

    .line 1973774
    iget-object v1, p0, LX/DCD;->c:LX/DCE;

    invoke-static {v1}, LX/DCE;->b$redex0(LX/DCE;)V

    .line 1973775
    :cond_3
    const v1, -0x644742c4

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
