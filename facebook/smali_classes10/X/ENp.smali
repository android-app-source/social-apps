.class public LX/ENp;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ENq;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ENp",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ENq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113531
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2113532
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ENp;->b:LX/0Zi;

    .line 2113533
    iput-object p1, p0, LX/ENp;->a:LX/0Ot;

    .line 2113534
    return-void
.end method

.method public static a(LX/0QB;)LX/ENp;
    .locals 4

    .prologue
    .line 2113490
    const-class v1, LX/ENp;

    monitor-enter v1

    .line 2113491
    :try_start_0
    sget-object v0, LX/ENp;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113492
    sput-object v2, LX/ENp;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113493
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113494
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113495
    new-instance v3, LX/ENp;

    const/16 p0, 0x3453

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ENp;-><init>(LX/0Ot;)V

    .line 2113496
    move-object v0, v3

    .line 2113497
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113498
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ENp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113499
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113500
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2113503
    check-cast p2, LX/ENo;

    .line 2113504
    iget-object v0, p0, LX/ENp;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ENq;

    iget-object v1, p2, LX/ENo;->a:LX/CzL;

    iget-object v2, p2, LX/ENo;->b:LX/1Ps;

    const/4 v3, 0x1

    .line 2113505
    iget-object v4, v1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v4, v4

    .line 2113506
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ne v4, v3, :cond_1

    move v4, v3

    .line 2113507
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b0060

    invoke-interface {v5, v3, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, LX/ENq;->a:LX/1V0;

    sget-object v7, LX/EMz;->a:LX/1X6;

    iget-object v3, v0, LX/ENq;->c:LX/EN3;

    invoke-virtual {v3, p1}, LX/EN3;->c(LX/1De;)LX/EN1;

    move-result-object v3

    .line 2113508
    iget-object p0, v1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object p0, p0

    .line 2113509
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/EN1;->b(Ljava/lang/String;)LX/EN1;

    move-result-object v3

    iget-object p0, v0, LX/ENq;->b:LX/1vg;

    invoke-virtual {p0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object p0

    const p2, -0xbd984e

    invoke-virtual {p0, p2}, LX/2xv;->i(I)LX/2xv;

    move-result-object p0

    const p2, 0x7f0200bb

    invoke-virtual {p0, p2}, LX/2xv;->h(I)LX/2xv;

    move-result-object p0

    invoke-virtual {p0}, LX/1n6;->b()LX/1dc;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/EN1;->a(LX/1dc;)LX/EN1;

    move-result-object p0

    if-eqz v4, :cond_2

    iget-object p2, v0, LX/ENq;->e:LX/EPK;

    move-object v3, v2

    check-cast v3, LX/Cxi;

    invoke-virtual {p2, v1, v3}, LX/EPK;->a(LX/CzL;LX/Cxi;)Landroid/view/View$OnClickListener;

    move-result-object v3

    :goto_1
    invoke-virtual {p0, v3}, LX/EN1;->a(Landroid/view/View$OnClickListener;)LX/EN1;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-virtual {v6, p1, v2, v7, v3}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    iget-object v5, v0, LX/ENq;->d:LX/ENu;

    const/4 v6, 0x0

    .line 2113510
    new-instance v7, LX/ENs;

    invoke-direct {v7, v5}, LX/ENs;-><init>(LX/ENu;)V

    .line 2113511
    iget-object p0, v5, LX/ENu;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/ENr;

    .line 2113512
    if-nez p0, :cond_0

    .line 2113513
    new-instance p0, LX/ENr;

    invoke-direct {p0, v5}, LX/ENr;-><init>(LX/ENu;)V

    .line 2113514
    :cond_0
    invoke-static {p0, p1, v6, v6, v7}, LX/ENr;->a$redex0(LX/ENr;LX/1De;IILX/ENs;)V

    .line 2113515
    move-object v7, p0

    .line 2113516
    move-object v6, v7

    .line 2113517
    move-object v5, v6

    .line 2113518
    iget-object v6, v5, LX/ENr;->a:LX/ENs;

    iput-object v1, v6, LX/ENs;->a:LX/CzL;

    .line 2113519
    iget-object v6, v5, LX/ENr;->e:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2113520
    move-object v5, v5

    .line 2113521
    iget-object v6, v5, LX/ENr;->a:LX/ENs;

    iput-object v2, v6, LX/ENs;->b:LX/1Ps;

    .line 2113522
    iget-object v6, v5, LX/ENr;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2113523
    move-object v5, v5

    .line 2113524
    iget-object v6, v5, LX/ENr;->a:LX/ENs;

    iput-boolean v4, v6, LX/ENs;->c:Z

    .line 2113525
    iget-object v6, v5, LX/ENr;->e:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2113526
    move-object v4, v5

    .line 2113527
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, -0x1

    invoke-interface {v4, v5}, LX/1Di;->y(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2113528
    return-object v0

    .line 2113529
    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 2113530
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2113501
    invoke-static {}, LX/1dS;->b()V

    .line 2113502
    const/4 v0, 0x0

    return-object v0
.end method
