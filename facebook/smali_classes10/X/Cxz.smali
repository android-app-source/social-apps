.class public LX/Cxz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cxe;


# instance fields
.field private final a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field private final b:LX/CzE;

.field private final c:LX/CvY;

.field private final d:LX/2Sc;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;LX/CvY;LX/2Sc;)V
    .locals 0
    .param p1    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952337
    iput-object p1, p0, LX/Cxz;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1952338
    iput-object p2, p0, LX/Cxz;->b:LX/CzE;

    .line 1952339
    iput-object p3, p0, LX/Cxz;->c:LX/CvY;

    .line 1952340
    iput-object p4, p0, LX/Cxz;->d:LX/2Sc;

    .line 1952341
    return-void
.end method


# virtual methods
.method public final c(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 16

    .prologue
    .line 1952342
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 1952343
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    .line 1952344
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Cxz;->b:LX/CzE;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, LX/CzE;->b(Ljava/lang/Object;)LX/0am;

    move-result-object v9

    .line 1952345
    invoke-virtual {v9}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1952346
    move-object/from16 v0, p0

    iget-object v11, v0, LX/Cxz;->c:LX/CvY;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/Cxz;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    sget-object v13, LX/8ch;->CLICK:LX/8ch;

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v14

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v1, v0, LX/Cxz;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/Cxz;->b:LX/CzE;

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v5, v4}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v4

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v5}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v6}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v7}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->v()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->isPresent()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v7}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->v()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v9

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v10

    invoke-static/range {v1 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v1, v11

    move-object v2, v12

    move-object v3, v13

    move v4, v14

    move-object v5, v15

    invoke-virtual/range {v1 .. v6}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952347
    :goto_2
    return-void

    .line 1952348
    :cond_0
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1952349
    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    .line 1952350
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Cxz;->b:LX/CzE;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, LX/CzE;->c(Lcom/facebook/graphql/model/GraphQLNode;)LX/0am;

    move-result-object v4

    .line 1952351
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1952352
    move-object/from16 v0, p0

    iget-object v12, v0, LX/Cxz;->c:LX/CvY;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/Cxz;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    sget-object v14, LX/8ch;->CLICK:LX/8ch;

    const/4 v15, 0x0

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, LX/CvV;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/Cxz;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/Cxz;->b:LX/CzE;

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v5, v4}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v9}, LX/0am;->isPresent()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v7}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->v()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v9

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v10

    invoke-static/range {v1 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v1, v12

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    move-object v5, v11

    invoke-virtual/range {v1 .. v6}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_2

    :cond_3
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_3

    .line 1952353
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Cxz;->d:LX/2Sc;

    sget-object v2, LX/3Ql;->LOGGING_ILLEGAL_STATE:LX/3Ql;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Node = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "), module.isPresent() = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, LX/0am;->isPresent()Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", entityUnit.isPresent() = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mFeedCollection = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Cxz;->b:LX/CzE;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No feed unit found for node: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3, v4}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2
.end method
