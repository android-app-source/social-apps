.class public LX/DwF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/01T;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final c:LX/03V;

.field private final d:LX/DwI;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/auth/viewercontext/ViewerContext;LX/03V;LX/DwI;LX/0ad;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/01T;",
            ">;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/DwI;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2060180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2060181
    iput-object p1, p0, LX/DwF;->a:LX/0Or;

    .line 2060182
    iput-object p2, p0, LX/DwF;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2060183
    iput-object p3, p0, LX/DwF;->c:LX/03V;

    .line 2060184
    iput-object p4, p0, LX/DwF;->d:LX/DwI;

    .line 2060185
    iput-object p5, p0, LX/DwF;->f:LX/0ad;

    .line 2060186
    iput-object p6, p0, LX/DwF;->e:LX/0Ot;

    .line 2060187
    return-void
.end method

.method public static b(LX/0QB;)LX/DwF;
    .locals 7

    .prologue
    .line 2060188
    new-instance v0, LX/DwF;

    const/16 v1, 0x3df

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/DwI;->b(LX/0QB;)LX/DwI;

    move-result-object v4

    check-cast v4, LX/DwI;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    const/16 v6, 0x3572

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/DwF;-><init>(LX/0Or;Lcom/facebook/auth/viewercontext/ViewerContext;LX/03V;LX/DwI;LX/0ad;LX/0Ot;)V

    .line 2060189
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Lcom/facebook/graphql/model/GraphQLAlbum;LX/8AB;LX/21D;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2060190
    invoke-static {p4, p5}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    if-nez p6, :cond_7

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2060191
    invoke-static {p2}, LX/DwG;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    .line 2060192
    invoke-static {v3}, LX/DwG;->b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, LX/DwG;->c(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, LX/DwG;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2060193
    :cond_0
    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2060194
    :cond_1
    iget-object v4, p0, LX/DwF;->f:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget-object v6, LX/0c1;->Off:LX/0c1;

    sget-short v7, LX/5lr;->a:S

    invoke-interface {v4, v5, v6, v7, v2}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v4

    if-eqz v4, :cond_8

    sget-object v4, LX/21D;->ALBUM:LX/21D;

    if-ne p4, v4, :cond_8

    .line 2060195
    :goto_1
    if-eqz p2, :cond_3

    invoke-static {v3}, LX/DwG;->c(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->NORMAL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v4, :cond_2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->SHARED:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v4, :cond_2

    if-eqz v1, :cond_3

    .line 2060196
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    if-nez v1, :cond_9

    .line 2060197
    iget-object v1, p0, LX/DwF;->c:LX/03V;

    const-string v2, "add_to_album_composer_launcher_no_privacy_scope"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Album privacyScope is null, album type %s"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2060198
    :cond_3
    :goto_2
    invoke-static {v3}, LX/DwG;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v1

    .line 2060199
    if-eqz v1, :cond_4

    .line 2060200
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v2

    invoke-static {p2}, LX/DwG;->b(Lcom/facebook/graphql/model/GraphQLAlbum;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v2

    iget-object v4, p0, LX/DwF;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2060201
    :cond_4
    new-instance v2, LX/8AA;

    invoke-direct {v2, p3}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2060202
    iput-object v0, v2, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2060203
    move-object v2, v2

    .line 2060204
    iget-object v0, p0, LX/DwF;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/DwF;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2060205
    invoke-virtual {v2}, LX/8AA;->g()LX/8AA;

    .line 2060206
    invoke-virtual {v2}, LX/8AA;->h()LX/8AA;

    .line 2060207
    :cond_5
    iget-object v0, p0, LX/DwF;->d:LX/DwI;

    invoke-virtual {v0, p2, v3}, LX/DwI;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2060208
    invoke-virtual {v2}, LX/8AA;->j()LX/8AA;

    .line 2060209
    :goto_3
    if-eqz v1, :cond_6

    iget-object v0, p0, LX/DwF;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_6

    .line 2060210
    invoke-virtual {v2}, LX/8AA;->l()LX/8AA;

    .line 2060211
    :cond_6
    invoke-static {p1, v2}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_7
    move v0, v2

    .line 2060212
    goto/16 :goto_0

    :cond_8
    move v1, v2

    .line 2060213
    goto/16 :goto_1

    .line 2060214
    :cond_9
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-nez v1, :cond_a

    .line 2060215
    iget-object v1, p0, LX/DwF;->c:LX/03V;

    const-string v2, "add_to_album_composer_launcher_no_privacy_image"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Album privacyScope.iconImage is null, album type %s"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2060216
    :cond_a
    invoke-virtual {v0, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetAlbum(Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    goto/16 :goto_2

    .line 2060217
    :cond_b
    invoke-virtual {v2}, LX/8AA;->b()LX/8AA;

    goto :goto_3
.end method
