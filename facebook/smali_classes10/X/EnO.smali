.class public final LX/EnO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Enp;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/entitycards/intent/EntityCardsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V
    .locals 0

    .prologue
    .line 2166856
    iput-object p1, p0, LX/EnO;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2166857
    iget-object v0, p0, LX/EnO;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    iget-object v0, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->m:LX/Emx;

    sget-object v1, LX/Emw;->INITIAL_ENTITIES_FETCHED:LX/Emw;

    invoke-virtual {v0, v1}, LX/Emx;->c(LX/Emw;)V

    .line 2166858
    iget-object v0, p0, LX/EnO;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    const/4 v1, 0x1

    .line 2166859
    iput-boolean v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->G:Z

    .line 2166860
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2166861
    check-cast p1, LX/Enp;

    .line 2166862
    iget-object v0, p0, LX/EnO;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    iget-object v0, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->m:LX/Emx;

    sget-object v1, LX/Emw;->INITIAL_ENTITIES_FETCHED:LX/Emw;

    invoke-virtual {v0, v1}, LX/Emx;->b(LX/Emw;)V

    .line 2166863
    iget-object v0, p0, LX/EnO;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    const/4 v1, 0x1

    .line 2166864
    iput-boolean v1, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->G:Z

    .line 2166865
    iget-object v0, p0, LX/EnO;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    invoke-static {v0}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->c(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V

    .line 2166866
    iget-object v0, p0, LX/EnO;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    iget-object v0, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->x:LX/EnI;

    invoke-interface {v0}, LX/EnI;->c()V

    .line 2166867
    iget-object v0, p1, LX/Enp;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2166868
    if-eqz v0, :cond_0

    .line 2166869
    iget-object v0, p0, LX/EnO;->a:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    iget-object v0, v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;->w:LX/EnY;

    .line 2166870
    iget-object v1, p1, LX/Enp;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2166871
    invoke-interface {v0, v1}, LX/EnY;->a(Ljava/lang/String;)V

    .line 2166872
    :cond_0
    return-void
.end method
