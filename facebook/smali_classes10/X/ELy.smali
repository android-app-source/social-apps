.class public final LX/ELy;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/ELy;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ELw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/ELz;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2110043
    const/4 v0, 0x0

    sput-object v0, LX/ELy;->a:LX/ELy;

    .line 2110044
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/ELy;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2110055
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2110056
    new-instance v0, LX/ELz;

    invoke-direct {v0}, LX/ELz;-><init>()V

    iput-object v0, p0, LX/ELy;->c:LX/ELz;

    .line 2110057
    return-void
.end method

.method public static declared-synchronized q()LX/ELy;
    .locals 2

    .prologue
    .line 2110051
    const-class v1, LX/ELy;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/ELy;->a:LX/ELy;

    if-nez v0, :cond_0

    .line 2110052
    new-instance v0, LX/ELy;

    invoke-direct {v0}, LX/ELy;-><init>()V

    sput-object v0, LX/ELy;->a:LX/ELy;

    .line 2110053
    :cond_0
    sget-object v0, LX/ELy;->a:LX/ELy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2110054
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2110047
    check-cast p2, LX/ELx;

    .line 2110048
    iget-object v0, p2, LX/ELx;->a:Ljava/lang/String;

    const/4 p2, 0x2

    .line 2110049
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const p0, 0x7f0b1733

    invoke-interface {v1, p0}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v1

    const p0, 0x7f0b1744

    invoke-interface {v1, p0}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Dh;->T(I)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x4

    const p2, 0x7f0b173d

    invoke-interface {v1, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x5

    const p2, 0x7f0b173e

    invoke-interface {v1, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x3

    const p2, 0x7f0b1740

    invoke-interface {v1, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x0

    const p2, 0x7f0e012d

    invoke-static {p1, p0, p2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object p0

    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object p0

    invoke-interface {v1, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2110050
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2110045
    invoke-static {}, LX/1dS;->b()V

    .line 2110046
    const/4 v0, 0x0

    return-object v0
.end method
