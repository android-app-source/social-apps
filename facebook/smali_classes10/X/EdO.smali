.class public LX/EdO;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xff

    .line 2149302
    new-array v0, v3, [B

    sput-object v0, LX/EdO;->a:[B

    .line 2149303
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 2149304
    sget-object v1, LX/EdO;->a:[B

    const/4 v2, -0x1

    aput-byte v2, v1, v0

    .line 2149305
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2149306
    :cond_0
    const/16 v0, 0x5a

    :goto_1
    const/16 v1, 0x41

    if-lt v0, v1, :cond_1

    .line 2149307
    sget-object v1, LX/EdO;->a:[B

    add-int/lit8 v2, v0, -0x41

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 2149308
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 2149309
    :cond_1
    const/16 v0, 0x7a

    :goto_2
    const/16 v1, 0x61

    if-lt v0, v1, :cond_2

    .line 2149310
    sget-object v1, LX/EdO;->a:[B

    add-int/lit8 v2, v0, -0x61

    add-int/lit8 v2, v2, 0x1a

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 2149311
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 2149312
    :cond_2
    const/16 v0, 0x39

    :goto_3
    const/16 v1, 0x30

    if-lt v0, v1, :cond_3

    .line 2149313
    sget-object v1, LX/EdO;->a:[B

    add-int/lit8 v2, v0, -0x30

    add-int/lit8 v2, v2, 0x34

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 2149314
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 2149315
    :cond_3
    sget-object v0, LX/EdO;->a:[B

    const/16 v1, 0x2b

    const/16 v2, 0x3e

    aput-byte v2, v0, v1

    .line 2149316
    sget-object v0, LX/EdO;->a:[B

    const/16 v1, 0x2f

    const/16 v2, 0x3f

    aput-byte v2, v0, v1

    .line 2149317
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2149318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([B)[B
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/16 v10, 0x3d

    .line 2149319
    const/4 v2, 0x0

    .line 2149320
    array-length v0, p0

    new-array v5, v0, [B

    move v0, v2

    move v3, v2

    .line 2149321
    :goto_0
    array-length v4, p0

    if-ge v0, v4, :cond_2

    .line 2149322
    aget-byte v4, p0, v0

    const/4 v6, 0x1

    .line 2149323
    const/16 v7, 0x3d

    if-ne v4, v7, :cond_a

    .line 2149324
    :cond_0
    :goto_1
    move v4, v6

    .line 2149325
    if-eqz v4, :cond_1

    .line 2149326
    add-int/lit8 v4, v3, 0x1

    aget-byte v6, p0, v0

    aput-byte v6, v5, v3

    move v3, v4

    .line 2149327
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2149328
    :cond_2
    new-array v0, v3, [B

    .line 2149329
    invoke-static {v5, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2149330
    move-object v3, v0

    .line 2149331
    array-length v0, v3

    if-nez v0, :cond_4

    .line 2149332
    new-array v0, v1, [B

    .line 2149333
    :cond_3
    :goto_2
    return-object v0

    .line 2149334
    :cond_4
    array-length v0, v3

    div-int/lit8 v4, v0, 0x4

    .line 2149335
    array-length v0, v3

    .line 2149336
    :cond_5
    add-int/lit8 v2, v0, -0x1

    aget-byte v2, v3, v2

    if-ne v2, v10, :cond_6

    .line 2149337
    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_5

    .line 2149338
    new-array v0, v1, [B

    goto :goto_2

    .line 2149339
    :cond_6
    sub-int/2addr v0, v4

    new-array v0, v0, [B

    move v2, v1

    .line 2149340
    :goto_3
    if-ge v1, v4, :cond_3

    .line 2149341
    mul-int/lit8 v5, v1, 0x4

    .line 2149342
    add-int/lit8 v6, v5, 0x2

    aget-byte v6, v3, v6

    .line 2149343
    add-int/lit8 v7, v5, 0x3

    aget-byte v7, v3, v7

    .line 2149344
    sget-object v8, LX/EdO;->a:[B

    aget-byte v9, v3, v5

    aget-byte v8, v8, v9

    .line 2149345
    sget-object v9, LX/EdO;->a:[B

    add-int/lit8 v5, v5, 0x1

    aget-byte v5, v3, v5

    aget-byte v5, v9, v5

    .line 2149346
    if-eq v6, v10, :cond_8

    if-eq v7, v10, :cond_8

    .line 2149347
    sget-object v9, LX/EdO;->a:[B

    aget-byte v6, v9, v6

    .line 2149348
    sget-object v9, LX/EdO;->a:[B

    aget-byte v7, v9, v7

    .line 2149349
    shl-int/lit8 v8, v8, 0x2

    shr-int/lit8 v9, v5, 0x4

    or-int/2addr v8, v9

    int-to-byte v8, v8

    aput-byte v8, v0, v2

    .line 2149350
    add-int/lit8 v8, v2, 0x1

    and-int/lit8 v5, v5, 0xf

    shl-int/lit8 v5, v5, 0x4

    shr-int/lit8 v9, v6, 0x2

    and-int/lit8 v9, v9, 0xf

    or-int/2addr v5, v9

    int-to-byte v5, v5

    aput-byte v5, v0, v8

    .line 2149351
    add-int/lit8 v5, v2, 0x2

    shl-int/lit8 v6, v6, 0x6

    or-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v0, v5

    .line 2149352
    :cond_7
    :goto_4
    add-int/lit8 v2, v2, 0x3

    .line 2149353
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2149354
    :cond_8
    if-ne v6, v10, :cond_9

    .line 2149355
    shl-int/lit8 v6, v8, 0x2

    shr-int/lit8 v5, v5, 0x4

    or-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v0, v2

    goto :goto_4

    .line 2149356
    :cond_9
    if-ne v7, v10, :cond_7

    .line 2149357
    sget-object v7, LX/EdO;->a:[B

    aget-byte v6, v7, v6

    .line 2149358
    shl-int/lit8 v7, v8, 0x2

    shr-int/lit8 v8, v5, 0x4

    or-int/2addr v7, v8

    int-to-byte v7, v7

    aput-byte v7, v0, v2

    .line 2149359
    add-int/lit8 v7, v2, 0x1

    and-int/lit8 v5, v5, 0xf

    shl-int/lit8 v5, v5, 0x4

    shr-int/lit8 v6, v6, 0x2

    and-int/lit8 v6, v6, 0xf

    or-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v0, v7

    goto :goto_4

    .line 2149360
    :cond_a
    sget-object v7, LX/EdO;->a:[B

    aget-byte v7, v7, v4

    const/4 v8, -0x1

    if-ne v7, v8, :cond_0

    .line 2149361
    const/4 v6, 0x0

    goto/16 :goto_1
.end method
