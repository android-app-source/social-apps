.class public LX/Dpd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final facebook_hmac:[B

.field public final server_time_micros:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2046507
    new-instance v0, LX/1sv;

    const-string v1, "SendResultPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Dpd;->b:LX/1sv;

    .line 2046508
    new-instance v0, LX/1sw;

    const-string v1, "server_time_micros"

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpd;->c:LX/1sw;

    .line 2046509
    new-instance v0, LX/1sw;

    const-string v1, "facebook_hmac"

    const/16 v2, 0xb

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpd;->d:LX/1sw;

    .line 2046510
    const/4 v0, 0x1

    sput-boolean v0, LX/Dpd;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;[B)V
    .locals 0

    .prologue
    .line 2046503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2046504
    iput-object p1, p0, LX/Dpd;->server_time_micros:Ljava/lang/Long;

    .line 2046505
    iput-object p2, p0, LX/Dpd;->facebook_hmac:[B

    .line 2046506
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2046475
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2046476
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2046477
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2046478
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SendResultPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046479
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046480
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046481
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046482
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046483
    const-string v4, "server_time_micros"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046484
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046485
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046486
    iget-object v4, p0, LX/Dpd;->server_time_micros:Ljava/lang/Long;

    if-nez v4, :cond_3

    .line 2046487
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046488
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046489
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046490
    const-string v4, "facebook_hmac"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046491
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046492
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046493
    iget-object v0, p0, LX/Dpd;->facebook_hmac:[B

    if-nez v0, :cond_4

    .line 2046494
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046495
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046496
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046497
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2046498
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2046499
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2046500
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2046501
    :cond_3
    iget-object v4, p0, LX/Dpd;->server_time_micros:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2046502
    :cond_4
    iget-object v0, p0, LX/Dpd;->facebook_hmac:[B

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2046465
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2046466
    iget-object v0, p0, LX/Dpd;->server_time_micros:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2046467
    sget-object v0, LX/Dpd;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046468
    iget-object v0, p0, LX/Dpd;->server_time_micros:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2046469
    :cond_0
    iget-object v0, p0, LX/Dpd;->facebook_hmac:[B

    if-eqz v0, :cond_1

    .line 2046470
    sget-object v0, LX/Dpd;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046471
    iget-object v0, p0, LX/Dpd;->facebook_hmac:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2046472
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2046473
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2046474
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2046443
    if-nez p1, :cond_1

    .line 2046444
    :cond_0
    :goto_0
    return v0

    .line 2046445
    :cond_1
    instance-of v1, p1, LX/Dpd;

    if-eqz v1, :cond_0

    .line 2046446
    check-cast p1, LX/Dpd;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2046447
    if-nez p1, :cond_3

    .line 2046448
    :cond_2
    :goto_1
    move v0, v2

    .line 2046449
    goto :goto_0

    .line 2046450
    :cond_3
    iget-object v0, p0, LX/Dpd;->server_time_micros:Ljava/lang/Long;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2046451
    :goto_2
    iget-object v3, p1, LX/Dpd;->server_time_micros:Ljava/lang/Long;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2046452
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2046453
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046454
    iget-object v0, p0, LX/Dpd;->server_time_micros:Ljava/lang/Long;

    iget-object v3, p1, LX/Dpd;->server_time_micros:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2046455
    :cond_5
    iget-object v0, p0, LX/Dpd;->facebook_hmac:[B

    if-eqz v0, :cond_a

    move v0, v1

    .line 2046456
    :goto_4
    iget-object v3, p1, LX/Dpd;->facebook_hmac:[B

    if-eqz v3, :cond_b

    move v3, v1

    .line 2046457
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2046458
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046459
    iget-object v0, p0, LX/Dpd;->facebook_hmac:[B

    iget-object v3, p1, LX/Dpd;->facebook_hmac:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 2046460
    goto :goto_1

    :cond_8
    move v0, v2

    .line 2046461
    goto :goto_2

    :cond_9
    move v3, v2

    .line 2046462
    goto :goto_3

    :cond_a
    move v0, v2

    .line 2046463
    goto :goto_4

    :cond_b
    move v3, v2

    .line 2046464
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2046439
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2046440
    sget-boolean v0, LX/Dpd;->a:Z

    .line 2046441
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Dpd;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2046442
    return-object v0
.end method
