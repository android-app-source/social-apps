.class public LX/CrK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02k;


# static fields
.field private static final b:LX/0wT;


# instance fields
.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0wd;

.field private final d:Landroid/content/Context;

.field public e:LX/Cqv;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1940789
    sget v0, LX/CoL;->a:F

    float-to-double v0, v0

    sget v2, LX/CoL;->b:F

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/CrK;->b:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 1940791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1940792
    iput-object p1, p0, LX/CrK;->d:Landroid/content/Context;

    .line 1940793
    const-class v0, LX/CrK;

    invoke-static {v0, p0}, LX/CrK;->a(Ljava/lang/Class;LX/02k;)V

    .line 1940794
    sget-object v0, LX/CrK;->b:LX/0wT;

    .line 1940795
    iget-object v1, p0, LX/CrK;->a:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v3, v4}, LX/0wd;->c(D)LX/0wd;

    move-result-object v1

    sget v2, LX/CoL;->c:F

    float-to-double v3, v2

    .line 1940796
    iput-wide v3, v1, LX/0wd;->l:D

    .line 1940797
    move-object v1, v1

    .line 1940798
    const/4 v2, 0x0

    .line 1940799
    iput-boolean v2, v1, LX/0wd;->c:Z

    .line 1940800
    move-object v1, v1

    .line 1940801
    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/CrK;->c:LX/0wd;

    .line 1940802
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CrK;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object p0

    check-cast p0, LX/0wW;

    iput-object p0, p1, LX/CrK;->a:LX/0wW;

    return-void
.end method


# virtual methods
.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1940790
    iget-object v0, p0, LX/CrK;->d:Landroid/content/Context;

    return-object v0
.end method
