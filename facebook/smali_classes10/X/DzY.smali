.class public final LX/DzY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 0

    .prologue
    .line 2067077
    iput-object p1, p0, LX/DzY;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x248d4757

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2067078
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/DzY;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/places/create/PlaceCreationCategoryPickerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "logger_type"

    sget-object v3, LX/9kb;->PLACE_CREATION_LOGGER:LX/9kb;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "logger_params"

    iget-object v3, p0, LX/DzY;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v3, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 2067079
    iget-object v2, p0, LX/DzY;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v3, p0, LX/DzY;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v3, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v4, LX/96A;->CATEGORY_PICKER:LX/96A;

    invoke-virtual {v2, v3, v4}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    .line 2067080
    iget-object v2, p0, LX/DzY;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/DzY;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-interface {v2, v1, v5, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2067081
    const v1, 0x4293ec56

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
