.class public LX/DwC;
.super Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;
.source ""


# instance fields
.field private k:D

.field private l:D

.field private m:D

.field private n:I

.field private o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 2060068
    invoke-direct {p0, p1}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;-><init>(Landroid/content/Context;)V

    .line 2060069
    iput-wide v0, p0, LX/DwC;->k:D

    .line 2060070
    iput-wide v0, p0, LX/DwC;->l:D

    .line 2060071
    iput-wide v0, p0, LX/DwC;->m:D

    .line 2060072
    invoke-virtual {p0}, LX/DwC;->a()V

    .line 2060073
    return-void
.end method

.method private static a(LX/DwC;IIZ)Landroid/graphics/Rect;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2060074
    if-eqz p1, :cond_0

    if-nez p3, :cond_0

    .line 2060075
    int-to-double v0, p1

    iget-wide v4, p0, LX/DwC;->k:D

    iget-wide v6, p0, LX/DwC;->l:D

    add-double/2addr v4, v6

    mul-double/2addr v0, v4

    double-to-int v0, v0

    move v1, v0

    .line 2060076
    :goto_0
    if-eqz p3, :cond_1

    .line 2060077
    new-instance v0, Landroid/graphics/Rect;

    int-to-double v4, v1

    iget-wide v6, p0, LX/DwC;->m:D

    add-double/2addr v4, v6

    double-to-int v3, v4

    iget-wide v4, p0, LX/DwC;->m:D

    double-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2060078
    :goto_1
    return-object v0

    .line 2060079
    :cond_0
    if-eqz p1, :cond_2

    if-eqz p3, :cond_2

    .line 2060080
    iget-wide v0, p0, LX/DwC;->k:D

    iget-wide v4, p0, LX/DwC;->l:D

    add-double/2addr v0, v4

    double-to-int v0, v0

    move v1, v0

    goto :goto_0

    .line 2060081
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    int-to-double v2, p2

    iget-wide v4, p0, LX/DwC;->l:D

    iget-wide v6, p0, LX/DwC;->k:D

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-int v2, v2

    int-to-double v4, v1

    iget-wide v6, p0, LX/DwC;->l:D

    add-double/2addr v4, v6

    double-to-int v3, v4

    int-to-double v4, p2

    iget-wide v6, p0, LX/DwC;->l:D

    iget-wide v8, p0, LX/DwC;->k:D

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-wide v6, p0, LX/DwC;->l:D

    add-double/2addr v4, v6

    double-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method private static a(LX/DwC;IIILcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;Z)V
    .locals 6

    .prologue
    .line 2060082
    if-eqz p4, :cond_1

    iget-object v0, p4, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    if-eqz v0, :cond_1

    if-eqz p5, :cond_0

    iget-object v0, p4, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    if-nez p5, :cond_2

    iget-object v0, p4, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2060083
    :cond_1
    :goto_0
    return-void

    .line 2060084
    :cond_2
    invoke-static {p0, p2, p3, p5}, LX/DwC;->a(LX/DwC;IIZ)Landroid/graphics/Rect;

    move-result-object v1

    .line 2060085
    if-eqz p5, :cond_3

    iget-object v0, p4, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2060086
    :goto_1
    if-eqz p5, :cond_4

    const-string v5, "LoadSquareImageThumbnail"

    .line 2060087
    :goto_2
    iget-object v3, p4, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    move-object v0, p0

    move v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Landroid/graphics/Rect;Landroid/net/Uri;Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;ILjava/lang/String;)V

    goto :goto_0

    .line 2060088
    :cond_3
    iget-object v0, p4, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    .line 2060089
    :cond_4
    const-string v5, "LoadSmallImageThumbnail"

    goto :goto_2
.end method

.method private static a(LX/DwC;ILcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2060090
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2060091
    :cond_0
    :goto_0
    return-void

    .line 2060092
    :cond_1
    invoke-static {p0, p1, v1, v1}, LX/DwC;->a(LX/DwC;IIZ)Landroid/graphics/Rect;

    move-result-object v1

    .line 2060093
    iget-object v0, p2, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2060094
    if-eqz p3, :cond_2

    const-string v5, "LoadSquareImageThumbnail"

    .line 2060095
    :goto_1
    iget-object v3, p2, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    move-object v0, p0

    move v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Landroid/graphics/Rect;Landroid/net/Uri;Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;ILjava/lang/String;)V

    goto :goto_0

    .line 2060096
    :cond_2
    const-string v5, "LoadSmallImageThumbnail"

    goto :goto_1
.end method

.method private b(I)V
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/4 v3, -0x1

    const/4 v7, 0x0

    .line 2060097
    if-ne p1, v3, :cond_0

    .line 2060098
    iget-object v0, p0, LX/DwC;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    iget-object v3, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v7

    move v2, v7

    :goto_0
    if-ge v1, v4, :cond_8

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2060099
    invoke-static {p0, v2, v0, v7}, LX/DwC;->a(LX/DwC;ILcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;Z)V

    .line 2060100
    add-int/lit8 v2, v2, 0x1

    .line 2060101
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2060102
    :cond_0
    if-nez p1, :cond_3

    move v10, v8

    .line 2060103
    :goto_1
    if-eqz v10, :cond_4

    .line 2060104
    :goto_2
    if-eqz v10, :cond_5

    const/4 v0, 0x2

    move v6, v0

    .line 2060105
    :goto_3
    iget-object v0, p0, LX/DwC;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    iget-object v11, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v9, v7

    move v1, v7

    :goto_4
    if-ge v9, v12, :cond_8

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2060106
    if-ne v1, p1, :cond_6

    move v5, v8

    .line 2060107
    :goto_5
    if-eqz v5, :cond_7

    move v2, v1

    :goto_6
    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/DwC;->a(LX/DwC;IIILcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;Z)V

    .line 2060108
    if-ne v1, p1, :cond_1

    if-eqz v10, :cond_2

    .line 2060109
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 2060110
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 2060111
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_4

    :cond_3
    move v10, v7

    .line 2060112
    goto :goto_1

    :cond_4
    move v3, v7

    .line 2060113
    goto :goto_2

    :cond_5
    move v6, v7

    .line 2060114
    goto :goto_3

    :cond_6
    move v5, v7

    .line 2060115
    goto :goto_5

    :cond_7
    move v2, v6

    .line 2060116
    goto :goto_6

    .line 2060117
    :cond_8
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2060118
    invoke-super {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a()V

    .line 2060119
    invoke-virtual {p0}, LX/DwC;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2060120
    invoke-virtual {p0}, LX/DwC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0b48

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, LX/DwC;->k:D

    .line 2060121
    int-to-double v0, v0

    iget-wide v2, p0, LX/DwC;->k:D

    invoke-virtual {p0}, LX/DwC;->getNumOfItems()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    invoke-virtual {p0}, LX/DwC;->getNumOfItems()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, LX/DwC;->l:D

    .line 2060122
    iget-wide v0, p0, LX/DwC;->l:D

    invoke-virtual {p0}, LX/DwC;->getNumOfItems()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iget-wide v2, p0, LX/DwC;->k:D

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/DwC;->m:D

    .line 2060123
    return-void
.end method

.method public final a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V
    .locals 2
    .param p8    # LX/Dw7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2060124
    invoke-super/range {p0 .. p8}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V

    .line 2060125
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2060126
    :cond_0
    :goto_0
    return-void

    .line 2060127
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c()V

    .line 2060128
    iput-object p1, p0, LX/DwC;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    .line 2060129
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2060130
    if-eqz v0, :cond_0

    .line 2060131
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 2060132
    iget-object p2, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result p2

    const/4 p3, 0x1

    if-ne p2, p3, :cond_2

    move v0, v1

    .line 2060133
    :goto_1
    move v0, v0

    .line 2060134
    iput v0, p0, LX/DwC;->n:I

    .line 2060135
    invoke-virtual {p0}, LX/DwC;->forceLayout()V

    .line 2060136
    iget v0, p0, LX/DwC;->n:I

    invoke-direct {p0, v0}, LX/DwC;->b(I)V

    .line 2060137
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->b()V

    goto :goto_0

    .line 2060138
    :cond_2
    iget-object p4, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {p4}, LX/0Px;->size()I

    move-result p5

    move p3, v0

    move p2, v0

    :goto_2
    if-ge p3, p5, :cond_4

    invoke-virtual {p4, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2060139
    iget-boolean v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    if-eqz v0, :cond_3

    move v0, p2

    .line 2060140
    goto :goto_1

    .line 2060141
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 2060142
    add-int/lit8 v0, p3, 0x1

    move p3, v0

    goto :goto_2

    :cond_4
    move v0, v1

    .line 2060143
    goto :goto_1
.end method

.method public final getNumOfItems()I
    .locals 1

    .prologue
    .line 2060144
    const/4 v0, 0x3

    return v0
.end method

.method public getRowHeight()I
    .locals 2

    .prologue
    .line 2060145
    iget-object v0, p0, LX/DwC;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DwC;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    if-nez v0, :cond_1

    .line 2060146
    :cond_0
    const/4 v0, 0x0

    .line 2060147
    :goto_0
    return v0

    .line 2060148
    :cond_1
    iget v0, p0, LX/DwC;->n:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 2060149
    iget-wide v0, p0, LX/DwC;->l:D

    double-to-int v0, v0

    goto :goto_0

    .line 2060150
    :cond_2
    iget-wide v0, p0, LX/DwC;->m:D

    double-to-int v0, v0

    goto :goto_0
.end method
