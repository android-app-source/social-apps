.class public LX/EsH;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EsF;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2174661
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EsH;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174662
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2174663
    iput-object p1, p0, LX/EsH;->b:LX/0Ot;

    .line 2174664
    return-void
.end method

.method public static a(LX/0QB;)LX/EsH;
    .locals 4

    .prologue
    .line 2174665
    const-class v1, LX/EsH;

    monitor-enter v1

    .line 2174666
    :try_start_0
    sget-object v0, LX/EsH;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174667
    sput-object v2, LX/EsH;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174668
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174669
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174670
    new-instance v3, LX/EsH;

    const/16 p0, 0x1f0a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EsH;-><init>(LX/0Ot;)V

    .line 2174671
    move-object v0, v3

    .line 2174672
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174673
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EsH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174674
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174675
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2174676
    check-cast p2, LX/EsG;

    .line 2174677
    iget-object v0, p0, LX/EsH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentSpec;

    iget-object v1, p2, LX/EsG;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p0, 0x6

    const/4 v7, 0x0

    .line 2174678
    if-eqz v1, :cond_0

    .line 2174679
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2174680
    if-eqz v2, :cond_0

    invoke-static {v1}, LX/C4e;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2174681
    :cond_0
    const/4 v2, 0x0

    .line 2174682
    :goto_0
    move-object v0, v2

    .line 2174683
    return-object v0

    .line 2174684
    :cond_1
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2174685
    check-cast v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-static {v2}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    .line 2174686
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    .line 2174687
    invoke-static {v2}, LX/6X0;->a(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;)Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;

    move-result-object v2

    .line 2174688
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2174689
    :cond_2
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2174690
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    const v6, 0x7f0b010f

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentSpec;->b:LX/8yV;

    invoke-virtual {v5, p1}, LX/8yV;->c(LX/1De;)LX/8yU;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6}, LX/8yU;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/8yU;->a(Ljava/util/List;)LX/8yU;

    move-result-object v5

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v5, v2}, LX/8yU;->m(I)LX/8yU;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/8yU;->q(I)LX/8yU;

    move-result-object v2

    .line 2174691
    iget-object v5, v2, LX/8yU;->a:Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    iput v7, v5, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->d:I

    .line 2174692
    move-object v2, v2

    .line 2174693
    const v5, 0x7f0b1d04

    invoke-virtual {v2, v5}, LX/8yU;->l(I)LX/8yU;

    move-result-object v2

    const v5, 0x7f0b1d05

    invoke-virtual {v2, v5}, LX/8yU;->h(I)LX/8yU;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v5, 0x7f0b0060

    invoke-interface {v2, p0, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004c

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a010e

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    const v4, 0x3f99999a    # 1.2f

    invoke-virtual {v3, v4}, LX/1ne;->j(F)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/16 v4, 0x41

    invoke-interface {v3, p0, v4}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x1

    const/16 v5, 0xa

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x3

    const/4 v5, 0x4

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_0

    .line 2174694
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaign;->j()Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillBirthdayCampaignPostingActorsConnection;->a()LX/0Px;

    move-result-object v2

    .line 2174695
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2174696
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result p2

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, p2, :cond_5

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLUser;

    .line 2174697
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2174698
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {v4}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2174699
    :cond_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 2174700
    :cond_5
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v2, v4

    .line 2174701
    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2174702
    invoke-static {}, LX/1dS;->b()V

    .line 2174703
    const/4 v0, 0x0

    return-object v0
.end method
