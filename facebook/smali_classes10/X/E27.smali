.class public final LX/E27;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E28;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/9qL;

.field public final synthetic b:LX/E28;


# direct methods
.method public constructor <init>(LX/E28;)V
    .locals 1

    .prologue
    .line 2072261
    iput-object p1, p0, LX/E27;->b:LX/E28;

    .line 2072262
    move-object v0, p1

    .line 2072263
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2072264
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2072265
    const-string v0, "ReactionCoreTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2072266
    if-ne p0, p1, :cond_1

    .line 2072267
    :cond_0
    :goto_0
    return v0

    .line 2072268
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2072269
    goto :goto_0

    .line 2072270
    :cond_3
    check-cast p1, LX/E27;

    .line 2072271
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2072272
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2072273
    if-eq v2, v3, :cond_0

    .line 2072274
    iget-object v2, p0, LX/E27;->a:LX/9qL;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/E27;->a:LX/9qL;

    iget-object v3, p1, LX/E27;->a:LX/9qL;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2072275
    goto :goto_0

    .line 2072276
    :cond_4
    iget-object v2, p1, LX/E27;->a:LX/9qL;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
