.class public LX/E6W;
.super LX/1OM;
.source ""

# interfaces
.implements LX/E6V;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "LX/1a1;",
        ">",
        "LX/1OM",
        "<TVH;>;",
        "LX/E6V;"
    }
.end annotation


# instance fields
.field private final a:LX/Cft;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Cft",
            "<TVH;>;"
        }
    .end annotation
.end field

.field private final b:LX/0o8;

.field private final c:LX/E8O;

.field private final d:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/Cft;LX/0o8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/E8P;)V
    .locals 6
    .param p1    # LX/Cft;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0o8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cft",
            "<TVH;>;",
            "LX/0o8;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/E8P;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080272
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2080273
    iput-object p1, p0, LX/E6W;->a:LX/Cft;

    .line 2080274
    iput-object p2, p0, LX/E6W;->b:LX/0o8;

    .line 2080275
    iput-object p3, p0, LX/E6W;->f:Ljava/lang/String;

    .line 2080276
    iput-object p4, p0, LX/E6W;->d:Ljava/lang/String;

    .line 2080277
    iput-object p5, p0, LX/E6W;->g:Ljava/lang/String;

    move-object v0, p6

    move-object v1, p1

    move-object v2, p5

    move-object v3, p0

    move-object v4, p3

    move-object v5, p4

    .line 2080278
    invoke-virtual/range {v0 .. v5}, LX/E8P;->a(LX/Cft;Ljava/lang/String;LX/E6V;Ljava/lang/String;Ljava/lang/String;)LX/E8O;

    move-result-object v0

    iput-object v0, p0, LX/E6W;->c:LX/E8O;

    .line 2080279
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/E6W;->e:Z

    .line 2080280
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2080281
    int-to-long v0, p1

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)TVH;"
        }
    .end annotation

    .prologue
    .line 2080282
    iget-boolean v0, p0, LX/E6W;->e:Z

    if-nez v0, :cond_0

    .line 2080283
    iget-object v0, p0, LX/E6W;->a:LX/Cft;

    new-instance v1, LX/E6U;

    invoke-direct {v1}, LX/E6U;-><init>()V

    iget-object v3, p0, LX/E6W;->b:LX/0o8;

    iget-object v4, p0, LX/E6W;->f:Ljava/lang/String;

    iget-object v5, p0, LX/E6W;->d:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, LX/Cfk;->a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V

    .line 2080284
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/E6W;->e:Z

    .line 2080285
    :cond_0
    iget-object v0, p0, LX/E6W;->a:LX/Cft;

    invoke-virtual {v0}, LX/Cft;->g()LX/1a1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;I)V"
        }
    .end annotation

    .prologue
    .line 2080286
    iget-object v0, p0, LX/E6W;->c:LX/E8O;

    .line 2080287
    iget-object v1, v0, LX/E8O;->b:Ljava/util/List;

    move-object v0, v1

    .line 2080288
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    .line 2080289
    iget-object v1, p0, LX/E6W;->a:LX/Cft;

    invoke-virtual {v1, p1, v0}, LX/Cft;->a(LX/1a1;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)V

    .line 2080290
    iget-object v1, p0, LX/E6W;->a:LX/Cft;

    iget-object v2, p0, LX/E6W;->g:Ljava/lang/String;

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    .line 2080291
    new-instance p0, LX/Cfs;

    invoke-direct {p0, v1, v0, v2}, LX/Cfs;-><init>(LX/Cft;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080292
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2080293
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2080294
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2080295
    iget-object v0, p0, LX/E6W;->c:LX/E8O;

    invoke-virtual {v0}, LX/E8N;->b()V

    .line 2080296
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2080297
    iget-object v0, p0, LX/E6W;->c:LX/E8O;

    .line 2080298
    iget-object p0, v0, LX/E8O;->b:Ljava/util/List;

    move-object v0, p0

    .line 2080299
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
