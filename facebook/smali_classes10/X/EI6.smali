.class public final enum LX/EI6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EI6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EI6;

.field public static final enum ACCEPT:LX/EI6;

.field public static final enum DECLINE:LX/EI6;

.field public static final enum MUTE:LX/EI6;

.field public static final enum NEW_VOICEMAIL_BUTTONS:LX/EI6;

.field public static final enum SWITCH_CAMERA:LX/EI6;

.field public static final enum TOGGLE_VIDEO:LX/EI6;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2100581
    new-instance v0, LX/EI6;

    const-string v1, "ACCEPT"

    invoke-direct {v0, v1, v3}, LX/EI6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI6;->ACCEPT:LX/EI6;

    .line 2100582
    new-instance v0, LX/EI6;

    const-string v1, "TOGGLE_VIDEO"

    invoke-direct {v0, v1, v4}, LX/EI6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI6;->TOGGLE_VIDEO:LX/EI6;

    .line 2100583
    new-instance v0, LX/EI6;

    const-string v1, "MUTE"

    invoke-direct {v0, v1, v5}, LX/EI6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI6;->MUTE:LX/EI6;

    .line 2100584
    new-instance v0, LX/EI6;

    const-string v1, "SWITCH_CAMERA"

    invoke-direct {v0, v1, v6}, LX/EI6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI6;->SWITCH_CAMERA:LX/EI6;

    .line 2100585
    new-instance v0, LX/EI6;

    const-string v1, "DECLINE"

    invoke-direct {v0, v1, v7}, LX/EI6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI6;->DECLINE:LX/EI6;

    .line 2100586
    new-instance v0, LX/EI6;

    const-string v1, "NEW_VOICEMAIL_BUTTONS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EI6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI6;->NEW_VOICEMAIL_BUTTONS:LX/EI6;

    .line 2100587
    const/4 v0, 0x6

    new-array v0, v0, [LX/EI6;

    sget-object v1, LX/EI6;->ACCEPT:LX/EI6;

    aput-object v1, v0, v3

    sget-object v1, LX/EI6;->TOGGLE_VIDEO:LX/EI6;

    aput-object v1, v0, v4

    sget-object v1, LX/EI6;->MUTE:LX/EI6;

    aput-object v1, v0, v5

    sget-object v1, LX/EI6;->SWITCH_CAMERA:LX/EI6;

    aput-object v1, v0, v6

    sget-object v1, LX/EI6;->DECLINE:LX/EI6;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EI6;->NEW_VOICEMAIL_BUTTONS:LX/EI6;

    aput-object v2, v0, v1

    sput-object v0, LX/EI6;->$VALUES:[LX/EI6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2100588
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EI6;
    .locals 1

    .prologue
    .line 2100589
    const-class v0, LX/EI6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EI6;

    return-object v0
.end method

.method public static values()[LX/EI6;
    .locals 1

    .prologue
    .line 2100590
    sget-object v0, LX/EI6;->$VALUES:[LX/EI6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EI6;

    return-object v0
.end method
