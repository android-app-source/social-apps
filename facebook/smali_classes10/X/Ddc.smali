.class public LX/Ddc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/Ddc;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2019931
    iget-object v0, p0, LX/Ddc;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2019932
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2019933
    :goto_0
    return-void

    .line 2019934
    :cond_0
    const-string v1, "omni_m_suggestion"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "suggestion_mode"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Ddc;
    .locals 2

    .prologue
    .line 2019926
    new-instance v1, LX/Ddc;

    invoke-direct {v1}, LX/Ddc;-><init>()V

    .line 2019927
    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2019928
    iput-object v0, v1, LX/Ddc;->a:LX/0Zb;

    .line 2019929
    return-object v1
.end method
