.class public LX/DH4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field public static final b:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field public static final c:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1VD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1981247
    const v0, 0x7f0b00e8

    sput v0, LX/DH4;->a:I

    .line 1981248
    const v0, 0x7f0b00dd

    sput v0, LX/DH4;->b:I

    .line 1981249
    const v0, 0x7f0b00d9

    sput v0, LX/DH4;->c:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1VD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1981251
    iput-object p1, p0, LX/DH4;->d:LX/0Ot;

    .line 1981252
    return-void
.end method

.method public static a(LX/0QB;)LX/DH4;
    .locals 4

    .prologue
    .line 1981253
    const-class v1, LX/DH4;

    monitor-enter v1

    .line 1981254
    :try_start_0
    sget-object v0, LX/DH4;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981255
    sput-object v2, LX/DH4;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981256
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981257
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981258
    new-instance v3, LX/DH4;

    const/16 p0, 0x71f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DH4;-><init>(LX/0Ot;)V

    .line 1981259
    move-object v0, v3

    .line 1981260
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981261
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DH4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981262
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981263
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
