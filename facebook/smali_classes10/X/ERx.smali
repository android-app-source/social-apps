.class public LX/ERx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/18V;

.field public final b:LX/ERN;


# direct methods
.method public constructor <init>(LX/18V;LX/ERN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2121842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121843
    iput-object p1, p0, LX/ERx;->a:LX/18V;

    .line 2121844
    iput-object p2, p0, LX/ERx;->b:LX/ERN;

    .line 2121845
    return-void
.end method

.method public static a(LX/0QB;)LX/ERx;
    .locals 5

    .prologue
    .line 2121846
    const-class v1, LX/ERx;

    monitor-enter v1

    .line 2121847
    :try_start_0
    sget-object v0, LX/ERx;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2121848
    sput-object v2, LX/ERx;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2121849
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2121850
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2121851
    new-instance p0, LX/ERx;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    .line 2121852
    new-instance v4, LX/ERN;

    invoke-direct {v4}, LX/ERN;-><init>()V

    .line 2121853
    move-object v4, v4

    .line 2121854
    move-object v4, v4

    .line 2121855
    check-cast v4, LX/ERN;

    invoke-direct {p0, v3, v4}, LX/ERx;-><init>(LX/18V;LX/ERN;)V

    .line 2121856
    move-object v0, p0

    .line 2121857
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2121858
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ERx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2121859
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2121860
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2121861
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2121862
    const-string v1, "fetch_blacklisted_sync_paths"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2121863
    iget-object v0, p0, LX/ERx;->a:LX/18V;

    iget-object v1, p0, LX/ERx;->b:LX/ERN;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2121864
    if-nez v0, :cond_1

    .line 2121865
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2121866
    :goto_0
    move-object v0, v0

    .line 2121867
    return-object v0

    .line 2121868
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
