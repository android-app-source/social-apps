.class public LX/DDF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/2g9;

.field public final b:LX/DWD;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0tX;

.field public final f:LX/0kL;

.field public final g:LX/1g8;


# direct methods
.method public constructor <init>(LX/2g9;LX/DWD;Ljava/util/concurrent/Executor;LX/0Or;LX/0tX;LX/0kL;LX/1g8;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2g9;",
            "LX/DWD;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0tX;",
            "LX/0kL;",
            "LX/1g8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1975208
    iput-object p1, p0, LX/DDF;->a:LX/2g9;

    .line 1975209
    iput-object p2, p0, LX/DDF;->b:LX/DWD;

    .line 1975210
    iput-object p3, p0, LX/DDF;->c:Ljava/util/concurrent/Executor;

    .line 1975211
    iput-object p4, p0, LX/DDF;->d:LX/0Or;

    .line 1975212
    iput-object p5, p0, LX/DDF;->e:LX/0tX;

    .line 1975213
    iput-object p6, p0, LX/DDF;->f:LX/0kL;

    .line 1975214
    iput-object p7, p0, LX/DDF;->g:LX/1g8;

    .line 1975215
    return-void
.end method

.method public static a(LX/0QB;)LX/DDF;
    .locals 11

    .prologue
    .line 1975196
    const-class v1, LX/DDF;

    monitor-enter v1

    .line 1975197
    :try_start_0
    sget-object v0, LX/DDF;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975198
    sput-object v2, LX/DDF;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975199
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975200
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975201
    new-instance v3, LX/DDF;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v4

    check-cast v4, LX/2g9;

    invoke-static {v0}, LX/DWD;->b(LX/0QB;)LX/DWD;

    move-result-object v5

    check-cast v5, LX/DWD;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    const/16 v7, 0x15e7

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-static {v0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v10

    check-cast v10, LX/1g8;

    invoke-direct/range {v3 .. v10}, LX/DDF;-><init>(LX/2g9;LX/DWD;Ljava/util/concurrent/Executor;LX/0Or;LX/0tX;LX/0kL;LX/1g8;)V

    .line 1975202
    move-object v0, v3

    .line 1975203
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975204
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DDF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975205
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975206
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(ZLX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;LX/DDY;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;",
            ">;",
            "LX/DDY;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1975189
    iput-boolean p0, p3, LX/DDY;->a:Z

    .line 1975190
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1975191
    check-cast v0, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;

    .line 1975192
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1975193
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;->F_()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    invoke-static {v0, v2, v3}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 1975194
    check-cast p1, LX/1Pq;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-interface {p1, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1975195
    return-void
.end method
