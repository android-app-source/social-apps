.class public final LX/EfB;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:LX/EfL;


# direct methods
.method public constructor <init>(LX/EfL;)V
    .locals 0

    .prologue
    .line 2154011
    iput-object p1, p0, LX/EfB;->a:LX/EfL;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2154012
    invoke-super {p0, p1, p2, p3}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 2154013
    iget-object v1, p0, LX/EfB;->a:LX/EfL;

    iget-object v1, v1, LX/EfL;->e:Lcom/facebook/audience/direct/ui/BackstageReplyThreadRecyclerViewAdapter;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 2154014
    iget-object v1, p0, LX/EfB;->a:LX/EfL;

    iget-object v1, v1, LX/EfL;->u:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2154015
    :cond_0
    :goto_0
    return-void

    .line 2154016
    :cond_1
    iget-object v1, p0, LX/EfB;->a:LX/EfL;

    iget-boolean v1, v1, LX/EfL;->F:Z

    if-nez v1, :cond_0

    .line 2154017
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollOffset()I

    move-result v1

    int-to-float v1, v1

    .line 2154018
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollExtent()I

    move-result v2

    int-to-float v2, v2

    .line 2154019
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollRange()I

    move-result v3

    int-to-float v3, v3

    .line 2154020
    cmpl-float v4, v1, v0

    if-nez v4, :cond_2

    .line 2154021
    :goto_1
    iget-object v1, p0, LX/EfB;->a:LX/EfL;

    iget-object v1, v1, LX/EfL;->u:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 2154022
    :cond_2
    add-float v0, v1, v2

    div-float/2addr v0, v3

    .line 2154023
    const v1, 0x3faaaaab

    const/high16 v2, 0x3e800000    # 0.25f

    sub-float v2, v0, v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    move v0, v1

    .line 2154024
    goto :goto_1
.end method
