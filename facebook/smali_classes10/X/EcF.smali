.class public final LX/EcF;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EcE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EcF;",
        ">;",
        "LX/EcE;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:LX/EWc;

.field private c:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2145846
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2145847
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcF;->b:LX/EWc;

    .line 2145848
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcF;->c:LX/EWc;

    .line 2145849
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2145850
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2145851
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcF;->b:LX/EWc;

    .line 2145852
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcF;->c:LX/EWc;

    .line 2145853
    return-void
.end method

.method private d(LX/EWY;)LX/EcF;
    .locals 1

    .prologue
    .line 2145854
    instance-of v0, p1, LX/EcG;

    if-eqz v0, :cond_0

    .line 2145855
    check-cast p1, LX/EcG;

    invoke-virtual {p0, p1}, LX/EcF;->a(LX/EcG;)LX/EcF;

    move-result-object p0

    .line 2145856
    :goto_0
    return-object p0

    .line 2145857
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EcF;
    .locals 4

    .prologue
    .line 2145858
    const/4 v2, 0x0

    .line 2145859
    :try_start_0
    sget-object v0, LX/EcG;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcG;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2145860
    if-eqz v0, :cond_0

    .line 2145861
    invoke-virtual {p0, v0}, LX/EcF;->a(LX/EcG;)LX/EcF;

    .line 2145862
    :cond_0
    return-object p0

    .line 2145863
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2145864
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2145865
    check-cast v0, LX/EcG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2145866
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2145867
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2145868
    invoke-virtual {p0, v1}, LX/EcF;->a(LX/EcG;)LX/EcF;

    :cond_1
    throw v0

    .line 2145869
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static w()LX/EcF;
    .locals 1

    .prologue
    .line 2145870
    new-instance v0, LX/EcF;

    invoke-direct {v0}, LX/EcF;-><init>()V

    return-object v0
.end method

.method private x()LX/EcF;
    .locals 2

    .prologue
    .line 2145871
    invoke-static {}, LX/EcF;->w()LX/EcF;

    move-result-object v0

    invoke-virtual {p0}, LX/EcF;->m()LX/EcG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcF;->a(LX/EcG;)LX/EcF;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2145872
    invoke-direct {p0, p1}, LX/EcF;->d(LX/EWY;)LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2145873
    invoke-direct {p0, p1, p2}, LX/EcF;->d(LX/EWd;LX/EYZ;)LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EWc;)LX/EcF;
    .locals 1

    .prologue
    .line 2145874
    if-nez p1, :cond_0

    .line 2145875
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2145876
    :cond_0
    iget v0, p0, LX/EcF;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EcF;->a:I

    .line 2145877
    iput-object p1, p0, LX/EcF;->b:LX/EWc;

    .line 2145878
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145879
    return-object p0
.end method

.method public final a(LX/EcG;)LX/EcF;
    .locals 2

    .prologue
    .line 2145880
    sget-object v0, LX/EcG;->c:LX/EcG;

    move-object v0, v0

    .line 2145881
    if-ne p1, v0, :cond_0

    .line 2145882
    :goto_0
    return-object p0

    .line 2145883
    :cond_0
    const/4 v0, 0x1

    .line 2145884
    iget v1, p1, LX/EcG;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    move v0, v0

    .line 2145885
    if-eqz v0, :cond_1

    .line 2145886
    iget-object v0, p1, LX/EcG;->public_:LX/EWc;

    move-object v0, v0

    .line 2145887
    invoke-virtual {p0, v0}, LX/EcF;->a(LX/EWc;)LX/EcF;

    .line 2145888
    :cond_1
    iget v0, p1, LX/EcG;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2145889
    if-eqz v0, :cond_2

    .line 2145890
    iget-object v0, p1, LX/EcG;->private_:LX/EWc;

    move-object v0, v0

    .line 2145891
    invoke-virtual {p0, v0}, LX/EcF;->b(LX/EWc;)LX/EcF;

    .line 2145892
    :cond_2
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2145893
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2145894
    invoke-direct {p0, p1, p2}, LX/EcF;->d(LX/EWd;LX/EYZ;)LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2145895
    invoke-direct {p0}, LX/EcF;->x()LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/EWc;)LX/EcF;
    .locals 1

    .prologue
    .line 2145812
    if-nez p1, :cond_0

    .line 2145813
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2145814
    :cond_0
    iget v0, p0, LX/EcF;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EcF;->a:I

    .line 2145815
    iput-object p1, p0, LX/EcF;->c:LX/EWc;

    .line 2145816
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145817
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2145845
    invoke-direct {p0, p1, p2}, LX/EcF;->d(LX/EWd;LX/EYZ;)LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2145811
    invoke-direct {p0}, LX/EcF;->x()LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2145818
    invoke-direct {p0, p1}, LX/EcF;->d(LX/EWY;)LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2145819
    invoke-direct {p0}, LX/EcF;->x()LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2145820
    sget-object v0, LX/Eck;->B:LX/EYn;

    const-class v1, LX/EcG;

    const-class v2, LX/EcF;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2145821
    sget-object v0, LX/Eck;->A:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2145822
    invoke-direct {p0}, LX/EcF;->x()LX/EcF;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2145823
    invoke-virtual {p0}, LX/EcF;->m()LX/EcG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2145824
    invoke-virtual {p0}, LX/EcF;->l()LX/EcG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2145825
    invoke-virtual {p0}, LX/EcF;->m()LX/EcG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2145826
    invoke-virtual {p0}, LX/EcF;->l()LX/EcG;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EcG;
    .locals 2

    .prologue
    .line 2145827
    invoke-virtual {p0}, LX/EcF;->m()LX/EcG;

    move-result-object v0

    .line 2145828
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2145829
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2145830
    :cond_0
    return-object v0
.end method

.method public final m()LX/EcG;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2145831
    new-instance v2, LX/EcG;

    invoke-direct {v2, p0}, LX/EcG;-><init>(LX/EWj;)V

    .line 2145832
    iget v3, p0, LX/EcF;->a:I

    .line 2145833
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2145834
    :goto_0
    iget-object v1, p0, LX/EcF;->b:LX/EWc;

    .line 2145835
    iput-object v1, v2, LX/EcG;->public_:LX/EWc;

    .line 2145836
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2145837
    or-int/lit8 v0, v0, 0x2

    .line 2145838
    :cond_0
    iget-object v1, p0, LX/EcF;->c:LX/EWc;

    .line 2145839
    iput-object v1, v2, LX/EcG;->private_:LX/EWc;

    .line 2145840
    iput v0, v2, LX/EcG;->bitField0_:I

    .line 2145841
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2145842
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145843
    sget-object v0, LX/EcG;->c:LX/EcG;

    move-object v0, v0

    .line 2145844
    return-object v0
.end method
