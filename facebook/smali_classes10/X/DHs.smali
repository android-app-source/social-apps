.class public final enum LX/DHs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DHs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DHs;

.field public static final enum ALL_FRIENDS:LX/DHs;

.field public static final enum FRIENDS_WITH_NEW_POSTS:LX/DHs;

.field public static final enum MUTUAL_FRIENDS:LX/DHs;

.field public static final enum PYMK:LX/DHs;

.field public static final enum RECENTLY_ADDED_FRIENDS:LX/DHs;

.field public static final enum SUGGESTIONS:LX/DHs;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1982585
    new-instance v0, LX/DHs;

    const-string v1, "ALL_FRIENDS"

    invoke-direct {v0, v1, v3}, LX/DHs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHs;->ALL_FRIENDS:LX/DHs;

    .line 1982586
    new-instance v0, LX/DHs;

    const-string v1, "MUTUAL_FRIENDS"

    invoke-direct {v0, v1, v4}, LX/DHs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHs;->MUTUAL_FRIENDS:LX/DHs;

    .line 1982587
    new-instance v0, LX/DHs;

    const-string v1, "PYMK"

    invoke-direct {v0, v1, v5}, LX/DHs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHs;->PYMK:LX/DHs;

    .line 1982588
    new-instance v0, LX/DHs;

    const-string v1, "RECENTLY_ADDED_FRIENDS"

    invoke-direct {v0, v1, v6}, LX/DHs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHs;->RECENTLY_ADDED_FRIENDS:LX/DHs;

    .line 1982589
    new-instance v0, LX/DHs;

    const-string v1, "SUGGESTIONS"

    invoke-direct {v0, v1, v7}, LX/DHs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHs;->SUGGESTIONS:LX/DHs;

    .line 1982590
    new-instance v0, LX/DHs;

    const-string v1, "FRIENDS_WITH_NEW_POSTS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/DHs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHs;->FRIENDS_WITH_NEW_POSTS:LX/DHs;

    .line 1982591
    const/4 v0, 0x6

    new-array v0, v0, [LX/DHs;

    sget-object v1, LX/DHs;->ALL_FRIENDS:LX/DHs;

    aput-object v1, v0, v3

    sget-object v1, LX/DHs;->MUTUAL_FRIENDS:LX/DHs;

    aput-object v1, v0, v4

    sget-object v1, LX/DHs;->PYMK:LX/DHs;

    aput-object v1, v0, v5

    sget-object v1, LX/DHs;->RECENTLY_ADDED_FRIENDS:LX/DHs;

    aput-object v1, v0, v6

    sget-object v1, LX/DHs;->SUGGESTIONS:LX/DHs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DHs;->FRIENDS_WITH_NEW_POSTS:LX/DHs;

    aput-object v2, v0, v1

    sput-object v0, LX/DHs;->$VALUES:[LX/DHs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1982592
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/DHs;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1982593
    invoke-static {}, LX/DHs;->values()[LX/DHs;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1982594
    invoke-virtual {v0}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1982595
    :goto_1
    return-object v0

    .line 1982596
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1982597
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/DHs;
    .locals 1

    .prologue
    .line 1982598
    const-class v0, LX/DHs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DHs;

    return-object v0
.end method

.method public static values()[LX/DHs;
    .locals 1

    .prologue
    .line 1982599
    sget-object v0, LX/DHs;->$VALUES:[LX/DHs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DHs;

    return-object v0
.end method
