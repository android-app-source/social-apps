.class public final LX/DY1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2011102
    iput-object p1, p0, LX/DY1;->c:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iput-object p2, p0, LX/DY1;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DY1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x34ddee5c    # -1.0621348E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2011103
    iget-object v1, p0, LX/DY1;->c:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v1, v1, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->k:LX/DYU;

    iget-object v2, p0, LX/DY1;->a:Ljava/lang/String;

    iget-object v3, p0, LX/DY1;->b:Ljava/lang/String;

    .line 2011104
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2011105
    sget-object v6, LX/0ax;->C:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2011106
    const-string p0, "group_feed_id"

    invoke-virtual {v5, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2011107
    const-string p0, "group_feed_title"

    invoke-virtual {v5, p0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2011108
    iget-object p0, v1, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    iget-object p0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->f:LX/17W;

    iget-object p1, v1, LX/DYU;->a:Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, p1, v6, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2011109
    const v1, 0x2928121a

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
