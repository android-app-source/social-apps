.class public LX/Eea;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public mAverageBytesPerSecond:F

.field private mLastChangeWaitTime:J

.field private mLastProgressUpdate:J

.field private mLastProgressUpdateWithChange:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2152997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152998
    invoke-static {p0}, LX/Eea;->b(LX/Eea;)V

    .line 2152999
    return-void
.end method

.method private static b(LX/Eea;)V
    .locals 2

    .prologue
    .line 2152992
    const/4 v0, 0x0

    iput v0, p0, LX/Eea;->mAverageBytesPerSecond:F

    .line 2152993
    invoke-static {}, LX/Eea;->c()J

    move-result-wide v0

    iput-wide v0, p0, LX/Eea;->mLastProgressUpdate:J

    .line 2152994
    iget-wide v0, p0, LX/Eea;->mLastProgressUpdate:J

    iput-wide v0, p0, LX/Eea;->mLastProgressUpdateWithChange:J

    .line 2152995
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Eea;->mLastChangeWaitTime:J

    .line 2152996
    return-void
.end method

.method private static c()J
    .locals 2

    .prologue
    .line 2152973
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final declared-synchronized a(J)V
    .locals 9

    .prologue
    .line 2152974
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/Eea;->mLastProgressUpdate:J

    .line 2152975
    iget-wide v2, p0, LX/Eea;->mLastProgressUpdateWithChange:J

    .line 2152976
    invoke-static {}, LX/Eea;->c()J

    move-result-wide v4

    .line 2152977
    cmp-long v6, v4, v0

    if-ltz v6, :cond_0

    cmp-long v6, v4, v2

    if-gez v6, :cond_2

    .line 2152978
    :cond_0
    invoke-static {p0}, LX/Eea;->b(LX/Eea;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152979
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 2152980
    :cond_2
    sub-long v0, v4, v0

    .line 2152981
    sub-long v2, v4, v2

    .line 2152982
    const-wide/16 v6, 0x0

    cmp-long v6, p1, v6

    if-lez v6, :cond_3

    .line 2152983
    :try_start_1
    iput-wide v4, p0, LX/Eea;->mLastProgressUpdate:J

    .line 2152984
    iput-wide v4, p0, LX/Eea;->mLastProgressUpdateWithChange:J

    .line 2152985
    const-wide/16 v0, 0x3e8

    add-long/2addr v0, v2

    const-wide/16 v4, 0x7530

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/Eea;->mLastChangeWaitTime:J

    .line 2152986
    :goto_1
    long-to-float v0, p1

    long-to-float v1, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    div-float/2addr v0, v1

    .line 2152987
    iget v1, p0, LX/Eea;->mAverageBytesPerSecond:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_4

    :goto_2
    iput v0, p0, LX/Eea;->mAverageBytesPerSecond:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2152988
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2152989
    :cond_3
    :try_start_2
    iget-wide v6, p0, LX/Eea;->mLastChangeWaitTime:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    .line 2152990
    iput-wide v4, p0, LX/Eea;->mLastProgressUpdate:J

    goto :goto_1

    .line 2152991
    :cond_4
    const/high16 v1, 0x3f400000    # 0.75f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3e800000    # 0.25f

    iget v2, p0, LX/Eea;->mAverageBytesPerSecond:F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_2
.end method
