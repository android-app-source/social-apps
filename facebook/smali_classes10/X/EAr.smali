.class public final LX/EAr;
.super LX/ChC;
.source ""


# instance fields
.field public final synthetic a:LX/E9N;

.field public final synthetic b:LX/EA4;

.field public final synthetic c:LX/E9T;

.field public final synthetic d:LX/EAs;


# direct methods
.method public constructor <init>(LX/EAs;LX/E9N;LX/EA4;LX/E9T;)V
    .locals 0

    .prologue
    .line 2085936
    iput-object p1, p0, LX/EAr;->d:LX/EAs;

    iput-object p2, p0, LX/EAr;->a:LX/E9N;

    iput-object p3, p0, LX/EAr;->b:LX/EA4;

    iput-object p4, p0, LX/EAr;->c:LX/E9T;

    invoke-direct {p0}, LX/ChC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;)V
    .locals 4
    .param p2    # Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2085937
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;->bi_()LX/55r;

    move-result-object v0

    invoke-static {v0}, LX/BNJ;->b(LX/55r;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2085938
    :cond_0
    iget-object v0, p0, LX/EAr;->a:LX/E9N;

    invoke-virtual {v0, p1}, LX/E9N;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2085939
    iget-object v0, p0, LX/EAr;->b:LX/EA4;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EA4;->a(I)V

    .line 2085940
    :cond_1
    :goto_0
    iget-object v0, p0, LX/EAr;->c:LX/E9T;

    invoke-interface {v0}, LX/E9T;->f()V

    .line 2085941
    return-void

    .line 2085942
    :cond_2
    iget-object v0, p0, LX/EAr;->a:LX/E9N;

    .line 2085943
    iget-object v1, v0, LX/E9N;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2085944
    iget-object v1, v0, LX/E9N;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;

    .line 2085945
    iget-object v2, v0, LX/E9N;->b:Ljava/util/List;

    iget-object v3, v0, LX/E9N;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v2, v1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2085946
    :goto_1
    iget-object v1, v0, LX/E9N;->c:Ljava/util/Map;

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2085947
    goto :goto_0

    .line 2085948
    :cond_3
    iget-object v1, v0, LX/E9N;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1
.end method
