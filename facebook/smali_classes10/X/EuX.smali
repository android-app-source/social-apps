.class public final LX/EuX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EuY;


# direct methods
.method public constructor <init>(LX/EuY;)V
    .locals 0

    .prologue
    .line 2179665
    iput-object p1, p0, LX/EuX;->a:LX/EuY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2179666
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2179667
    if-eqz p1, :cond_0

    .line 2179668
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179669
    if-eqz v0, :cond_0

    .line 2179670
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179671
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel$FriendingPossibilitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2179672
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179673
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel$FriendingPossibilitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel$FriendingPossibilitiesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2179674
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2179675
    :goto_0
    return-object v0

    .line 2179676
    :cond_1
    iget-object v1, p0, LX/EuX;->a:LX/EuY;

    .line 2179677
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179678
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel$FriendingPossibilitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel$FriendingPossibilitiesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/EuY;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2179679
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179680
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel$FriendingPossibilitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterFetchRequestsQueryModel$FriendingPossibilitiesModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
