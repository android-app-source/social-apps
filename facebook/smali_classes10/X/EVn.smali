.class public final enum LX/EVn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EVn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EVn;

.field public static final enum HIDDEN:LX/EVn;

.field public static final enum HIDING:LX/EVn;

.field public static final enum REVEALING:LX/EVn;

.field public static final enum SHOWN:LX/EVn;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2128972
    new-instance v0, LX/EVn;

    const-string v1, "REVEALING"

    invoke-direct {v0, v1, v2}, LX/EVn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EVn;->REVEALING:LX/EVn;

    .line 2128973
    new-instance v0, LX/EVn;

    const-string v1, "HIDING"

    invoke-direct {v0, v1, v3}, LX/EVn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EVn;->HIDING:LX/EVn;

    .line 2128974
    new-instance v0, LX/EVn;

    const-string v1, "SHOWN"

    invoke-direct {v0, v1, v4}, LX/EVn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EVn;->SHOWN:LX/EVn;

    .line 2128975
    new-instance v0, LX/EVn;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v5}, LX/EVn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EVn;->HIDDEN:LX/EVn;

    .line 2128976
    const/4 v0, 0x4

    new-array v0, v0, [LX/EVn;

    sget-object v1, LX/EVn;->REVEALING:LX/EVn;

    aput-object v1, v0, v2

    sget-object v1, LX/EVn;->HIDING:LX/EVn;

    aput-object v1, v0, v3

    sget-object v1, LX/EVn;->SHOWN:LX/EVn;

    aput-object v1, v0, v4

    sget-object v1, LX/EVn;->HIDDEN:LX/EVn;

    aput-object v1, v0, v5

    sput-object v0, LX/EVn;->$VALUES:[LX/EVn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2128977
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EVn;
    .locals 1

    .prologue
    .line 2128978
    const-class v0, LX/EVn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EVn;

    return-object v0
.end method

.method public static values()[LX/EVn;
    .locals 1

    .prologue
    .line 2128979
    sget-object v0, LX/EVn;->$VALUES:[LX/EVn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EVn;

    return-object v0
.end method
