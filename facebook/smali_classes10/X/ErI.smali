.class public LX/ErI;
.super LX/7HO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HO",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/8i9;


# direct methods
.method public constructor <init>(LX/8i9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172766
    invoke-direct {p0}, LX/7HO;-><init>()V

    .line 2172767
    iput-object p1, p0, LX/ErI;->a:LX/8i9;

    .line 2172768
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/7Hi;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2172769
    check-cast p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2172770
    invoke-virtual {p1}, LX/8QL;->j()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2172771
    const/4 v0, 0x0

    .line 2172772
    :goto_0
    return v0

    .line 2172773
    :cond_0
    iget-object v0, p0, LX/ErI;->a:LX/8i9;

    invoke-virtual {v0, p3}, LX/8i9;->a(Ljava/lang/String;)LX/8i4;

    move-result-object v0

    .line 2172774
    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    .line 2172775
    invoke-virtual {v0, v1}, LX/8i4;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
