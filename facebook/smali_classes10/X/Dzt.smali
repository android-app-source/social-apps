.class public LX/Dzt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/DzP;

.field public final c:LX/E00;

.field public final d:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/DzP;LX/E00;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2067538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2067539
    iput-object p1, p0, LX/Dzt;->a:Landroid/content/Context;

    .line 2067540
    iput-object p2, p0, LX/Dzt;->b:LX/DzP;

    .line 2067541
    iput-object p3, p0, LX/Dzt;->c:LX/E00;

    .line 2067542
    iput-object p4, p0, LX/Dzt;->d:LX/0Uh;

    .line 2067543
    return-void
.end method

.method public static a(LX/Dzl;ILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2067544
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 2067545
    iget-object v0, p0, LX/Dzl;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v1, p0, LX/Dzl;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v1, v1, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v2, LX/96A;->FORM:LX/96A;

    invoke-virtual {v0, v1, v2}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    .line 2067546
    :cond_0
    :goto_0
    return-void

    .line 2067547
    :cond_1
    const-string v0, "continue_place_creation"

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2067548
    invoke-virtual {p0}, LX/Dzl;->b()V

    goto :goto_0

    .line 2067549
    :cond_2
    const-string v0, "select_existing_place"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067550
    const-string v0, "select_existing_place"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2067551
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2067552
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2067553
    iget-object v1, p0, LX/Dzl;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    sget-object p1, LX/96A;->DEDUPER:LX/96A;

    invoke-static {v1, v2, v3, v0, p1}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->a$redex0(Lcom/facebook/places/create/NewPlaceCreationFormFragment;JLjava/lang/String;LX/96A;)V

    .line 2067554
    goto :goto_0
.end method
