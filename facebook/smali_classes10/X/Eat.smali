.class public LX/Eat;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final a:[B


# direct methods
.method public constructor <init>([B)V
    .locals 0

    .prologue
    .line 2142646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142647
    iput-object p1, p0, LX/Eat;->a:[B

    .line 2142648
    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2142644
    new-array v0, v3, [B

    const/4 v1, 0x5

    aput-byte v1, v0, v2

    .line 2142645
    const/4 v1, 0x2

    new-array v1, v1, [[B

    aput-object v0, v1, v2

    iget-object v0, p0, LX/Eat;->a:[B

    aput-object v0, v1, v3

    invoke-static {v1}, LX/Eco;->a([[B)[B

    move-result-object v0

    return-object v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 2142650
    check-cast p1, LX/Eat;

    .line 2142651
    new-instance v0, Ljava/math/BigInteger;

    iget-object v1, p0, LX/Eat;->a:[B

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>([B)V

    new-instance v1, Ljava/math/BigInteger;

    check-cast p1, LX/Eat;

    iget-object v2, p1, LX/Eat;->a:[B

    invoke-direct {v1, v2}, Ljava/math/BigInteger;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2142652
    if-nez p1, :cond_1

    .line 2142653
    :cond_0
    :goto_0
    return v0

    .line 2142654
    :cond_1
    instance-of v1, p1, LX/Eat;

    if-eqz v1, :cond_0

    .line 2142655
    check-cast p1, LX/Eat;

    .line 2142656
    iget-object v0, p0, LX/Eat;->a:[B

    iget-object v1, p1, LX/Eat;->a:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2142649
    iget-object v0, p0, LX/Eat;->a:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    return v0
.end method
