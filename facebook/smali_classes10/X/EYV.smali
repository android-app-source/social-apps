.class public final LX/EYV;
.super LX/EWV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWV",
        "<",
        "LX/EYV;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/EYF;

.field private b:LX/EYc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/EZQ;


# direct methods
.method public constructor <init>(LX/EYF;)V
    .locals 1

    .prologue
    .line 2137419
    invoke-direct {p0}, LX/EWV;-><init>()V

    .line 2137420
    iput-object p1, p0, LX/EYV;->a:LX/EYF;

    .line 2137421
    invoke-static {}, LX/EYc;->a()LX/EYc;

    move-result-object v0

    iput-object v0, p0, LX/EYV;->b:LX/EYc;

    .line 2137422
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2137423
    iput-object v0, p0, LX/EYV;->c:LX/EZQ;

    .line 2137424
    return-void
.end method

.method private d(LX/EZQ;)LX/EYV;
    .locals 1

    .prologue
    .line 2137452
    iget-object v0, p0, LX/EYV;->c:LX/EZQ;

    invoke-static {v0}, LX/EZQ;->a(LX/EZQ;)LX/EZM;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZM;->a(LX/EZQ;)LX/EZM;

    move-result-object v0

    invoke-virtual {v0}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EYV;->c:LX/EZQ;

    .line 2137453
    return-object p0
.end method

.method private e(LX/EYP;)V
    .locals 2

    .prologue
    .line 2137454
    iget-object v0, p1, LX/EYP;->h:LX/EYF;

    move-object v0, v0

    .line 2137455
    iget-object v1, p0, LX/EYV;->a:LX/EYF;

    if-eq v0, v1, :cond_0

    .line 2137456
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FieldDescriptor does not match message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137457
    :cond_0
    return-void
.end method

.method private l()LX/EYW;
    .locals 5

    .prologue
    .line 2137458
    invoke-virtual {p0}, LX/EYV;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2137459
    new-instance v0, LX/EYW;

    iget-object v1, p0, LX/EYV;->a:LX/EYF;

    iget-object v2, p0, LX/EYV;->b:LX/EYc;

    iget-object v3, p0, LX/EYV;->c:LX/EZQ;

    invoke-direct {v0, v1, v2, v3}, LX/EYW;-><init>(LX/EYF;LX/EYc;LX/EZQ;)V

    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2137460
    :cond_0
    invoke-virtual {p0}, LX/EYV;->d()LX/EYW;

    move-result-object v0

    return-object v0
.end method

.method private m()LX/EYV;
    .locals 3

    .prologue
    .line 2137461
    new-instance v0, LX/EYV;

    iget-object v1, p0, LX/EYV;->a:LX/EYF;

    invoke-direct {v0, v1}, LX/EYV;-><init>(LX/EYF;)V

    .line 2137462
    iget-object v1, v0, LX/EYV;->b:LX/EYc;

    iget-object v2, p0, LX/EYV;->b:LX/EYc;

    invoke-virtual {v1, v2}, LX/EYc;->a(LX/EYc;)V

    .line 2137463
    iget-object v1, p0, LX/EYV;->c:LX/EZQ;

    invoke-direct {v0, v1}, LX/EYV;->d(LX/EZQ;)LX/EYV;

    .line 2137464
    return-object v0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 2137465
    iget-object v0, p0, LX/EYV;->b:LX/EYc;

    .line 2137466
    iget-boolean v1, v0, LX/EYc;->b:Z

    move v0, v1

    .line 2137467
    if-eqz v0, :cond_0

    .line 2137468
    iget-object v0, p0, LX/EYV;->b:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->e()LX/EYc;

    move-result-object v0

    iput-object v0, p0, LX/EYV;->b:LX/EYc;

    .line 2137469
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/EYP;Ljava/lang/Object;)LX/EWU;
    .locals 1

    .prologue
    .line 2137470
    invoke-direct {p0, p1}, LX/EYV;->e(LX/EYP;)V

    .line 2137471
    invoke-direct {p0}, LX/EYV;->o()V

    .line 2137472
    iget-object v0, p0, LX/EYV;->b:LX/EYc;

    invoke-virtual {v0, p1, p2}, LX/EYc;->b(LX/EYP;Ljava/lang/Object;)V

    .line 2137473
    return-object p0
.end method

.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2137474
    invoke-virtual {p0, p1}, LX/EYV;->d(LX/EWY;)LX/EYV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EZQ;)LX/EWV;
    .locals 1

    .prologue
    .line 2137475
    invoke-direct {p0, p1}, LX/EYV;->d(LX/EZQ;)LX/EYV;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2137476
    iget-object v0, p0, LX/EYV;->a:LX/EYF;

    iget-object v1, p0, LX/EYV;->b:LX/EYc;

    invoke-static {v0, v1}, LX/EYW;->b(LX/EYF;LX/EYc;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/EYP;)Z
    .locals 1

    .prologue
    .line 2137477
    invoke-direct {p0, p1}, LX/EYV;->e(LX/EYP;)V

    .line 2137478
    iget-object v0, p0, LX/EYV;->b:LX/EYc;

    invoke-virtual {v0, p1}, LX/EYc;->a(LX/EYP;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/EYP;Ljava/lang/Object;)LX/EWU;
    .locals 1

    .prologue
    .line 2137479
    invoke-direct {p0, p1}, LX/EYV;->e(LX/EYP;)V

    .line 2137480
    invoke-direct {p0}, LX/EYV;->o()V

    .line 2137481
    iget-object v0, p0, LX/EYV;->b:LX/EYc;

    invoke-virtual {v0, p1, p2}, LX/EYc;->a(LX/EYP;Ljava/lang/Object;)V

    .line 2137482
    return-object p0
.end method

.method public final b(LX/EZQ;)LX/EWU;
    .locals 1

    .prologue
    .line 2137483
    iput-object p1, p0, LX/EYV;->c:LX/EZQ;

    .line 2137484
    return-object p0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2137451
    invoke-direct {p0}, LX/EYV;->m()LX/EYV;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/EYP;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2137485
    invoke-direct {p0, p1}, LX/EYV;->e(LX/EYP;)V

    .line 2137486
    iget-object v0, p0, LX/EYV;->b:LX/EYc;

    invoke-virtual {v0, p1}, LX/EYc;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    .line 2137487
    if-nez v0, :cond_0

    .line 2137488
    invoke-virtual {p1}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v0, v1, :cond_1

    .line 2137489
    invoke-virtual {p1}, LX/EYP;->t()LX/EYF;

    move-result-object v0

    invoke-static {v0}, LX/EYW;->a(LX/EYF;)LX/EYW;

    move-result-object v0

    .line 2137490
    :cond_0
    :goto_0
    return-object v0

    .line 2137491
    :cond_1
    invoke-virtual {p1}, LX/EYP;->p()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2137450
    invoke-direct {p0}, LX/EYV;->m()LX/EYV;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2137449
    invoke-virtual {p0, p1}, LX/EYV;->d(LX/EWY;)LX/EYV;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/EYP;)LX/EWU;
    .locals 2

    .prologue
    .line 2137445
    invoke-direct {p0, p1}, LX/EYV;->e(LX/EYP;)V

    .line 2137446
    invoke-virtual {p1}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->MESSAGE:LX/EYN;

    if-eq v0, v1, :cond_0

    .line 2137447
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "newBuilderForField is only valid for fields with message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137448
    :cond_0
    new-instance v0, LX/EYV;

    invoke-virtual {p1}, LX/EYP;->t()LX/EYF;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EYV;-><init>(LX/EYF;)V

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2137444
    invoke-direct {p0}, LX/EYV;->m()LX/EYV;

    move-result-object v0

    return-object v0
.end method

.method public final d(LX/EWY;)LX/EYV;
    .locals 2

    .prologue
    .line 2137436
    instance-of v0, p1, LX/EYW;

    if-eqz v0, :cond_1

    .line 2137437
    check-cast p1, LX/EYW;

    .line 2137438
    iget-object v0, p1, LX/EYW;->a:LX/EYF;

    iget-object v1, p0, LX/EYV;->a:LX/EYF;

    if-eq v0, v1, :cond_0

    .line 2137439
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mergeFrom(Message) can only merge messages of the same type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2137440
    :cond_0
    invoke-direct {p0}, LX/EYV;->o()V

    .line 2137441
    iget-object v0, p0, LX/EYV;->b:LX/EYc;

    iget-object v1, p1, LX/EYW;->b:LX/EYc;

    invoke-virtual {v0, v1}, LX/EYc;->a(LX/EYc;)V

    .line 2137442
    iget-object v0, p1, LX/EYW;->c:LX/EZQ;

    invoke-direct {p0, v0}, LX/EYV;->d(LX/EZQ;)LX/EYV;

    .line 2137443
    :goto_0
    return-object p0

    :cond_1
    invoke-super {p0, p1}, LX/EWV;->a(LX/EWY;)LX/EWV;

    move-result-object v0

    check-cast v0, LX/EYV;

    move-object p0, v0

    goto :goto_0
.end method

.method public final d()LX/EYW;
    .locals 5

    .prologue
    .line 2137433
    iget-object v0, p0, LX/EYV;->b:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->c()V

    .line 2137434
    new-instance v0, LX/EYW;

    iget-object v1, p0, LX/EYV;->a:LX/EYF;

    iget-object v2, p0, LX/EYV;->b:LX/EYc;

    iget-object v3, p0, LX/EYV;->c:LX/EZQ;

    invoke-direct {v0, v1, v2, v3}, LX/EYW;-><init>(LX/EYF;LX/EYc;LX/EZQ;)V

    .line 2137435
    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2137432
    iget-object v0, p0, LX/EYV;->a:LX/EYF;

    return-object v0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2137431
    iget-object v0, p0, LX/EYV;->c:LX/EZQ;

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2137430
    invoke-virtual {p0}, LX/EYV;->d()LX/EYW;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2137429
    invoke-direct {p0}, LX/EYV;->l()LX/EYW;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2137428
    invoke-virtual {p0}, LX/EYV;->d()LX/EYW;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2137427
    invoke-direct {p0}, LX/EYV;->l()LX/EYW;

    move-result-object v0

    return-object v0
.end method

.method public final kb_()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2137426
    iget-object v0, p0, LX/EYV;->b:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->f()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2137425
    iget-object v0, p0, LX/EYV;->a:LX/EYF;

    invoke-static {v0}, LX/EYW;->a(LX/EYF;)LX/EYW;

    move-result-object v0

    return-object v0
.end method
