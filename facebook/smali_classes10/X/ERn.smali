.class public final LX/ERn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2TH;


# direct methods
.method public constructor <init>(LX/2TH;)V
    .locals 0

    .prologue
    .line 2121401
    iput-object p1, p0, LX/ERn;->a:LX/2TH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2121402
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error fetching blacklisted sync paths: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2121403
    iget-object v0, p0, LX/ERn;->a:LX/2TH;

    iget-object v0, v0, LX/2TH;->i:LX/03V;

    const-string v1, "Error fetching blacklisted sync paths"

    const-string v2, "Error fetching blacklisted sync paths: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2121404
    iget-object v0, p0, LX/ERn;->a:LX/2TH;

    invoke-static {v0}, LX/2TH;->e(LX/2TH;)V

    .line 2121405
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2121406
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2121407
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 2121408
    iget-object v1, p0, LX/ERn;->a:LX/2TH;

    iget-object v1, v1, LX/2TH;->f:LX/2TJ;

    .line 2121409
    iget-object v2, v1, LX/2TJ;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/2TR;->j:LX/0Tn;

    invoke-interface {v2, v3, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v3, LX/2TR;->k:LX/0Tn;

    iget-object v4, v1, LX/2TJ;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2121410
    iget-object v0, p0, LX/ERn;->a:LX/2TH;

    invoke-static {v0}, LX/2TH;->e(LX/2TH;)V

    .line 2121411
    return-void
.end method
