.class public LX/DpA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;


# instance fields
.field public final download_fbid:Ljava/lang/Long;

.field public final download_hash:[B

.field public final download_mac:Ljava/lang/String;

.field public final download_size_bytes:Ljava/lang/Long;

.field public final file_mime_type:Ljava/lang/String;

.field public final image_metadata:LX/DpI;

.field public final secret_key:[B

.field public final suggested_file_name:Ljava/lang/String;

.field public final thumbnail_data:[B

.field public final video_metadata:LX/Dpp;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/16 v4, 0xa

    const/16 v3, 0xb

    .line 2043534
    new-instance v0, LX/1sv;

    const-string v1, "AttachmentInfo"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpA;->b:LX/1sv;

    .line 2043535
    new-instance v0, LX/1sw;

    const-string v1, "secret_key"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpA;->c:LX/1sw;

    .line 2043536
    new-instance v0, LX/1sw;

    const-string v1, "download_fbid"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpA;->d:LX/1sw;

    .line 2043537
    new-instance v0, LX/1sw;

    const-string v1, "download_size_bytes"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpA;->e:LX/1sw;

    .line 2043538
    new-instance v0, LX/1sw;

    const-string v1, "download_hash"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpA;->f:LX/1sw;

    .line 2043539
    new-instance v0, LX/1sw;

    const-string v1, "suggested_file_name"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpA;->g:LX/1sw;

    .line 2043540
    new-instance v0, LX/1sw;

    const-string v1, "file_mime_type"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpA;->h:LX/1sw;

    .line 2043541
    new-instance v0, LX/1sw;

    const-string v1, "thumbnail_data"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpA;->i:LX/1sw;

    .line 2043542
    new-instance v0, LX/1sw;

    const-string v1, "image_metadata"

    invoke-direct {v0, v1, v5, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpA;->j:LX/1sw;

    .line 2043543
    new-instance v0, LX/1sw;

    const-string v1, "video_metadata"

    invoke-direct {v0, v1, v5, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpA;->k:LX/1sw;

    .line 2043544
    new-instance v0, LX/1sw;

    const-string v1, "download_mac"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpA;->l:LX/1sw;

    .line 2043545
    const/4 v0, 0x1

    sput-boolean v0, LX/DpA;->a:Z

    return-void
.end method

.method public constructor <init>([BLjava/lang/Long;Ljava/lang/Long;[BLjava/lang/String;Ljava/lang/String;[BLX/DpI;LX/Dpp;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2043546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2043547
    iput-object p1, p0, LX/DpA;->secret_key:[B

    .line 2043548
    iput-object p2, p0, LX/DpA;->download_fbid:Ljava/lang/Long;

    .line 2043549
    iput-object p3, p0, LX/DpA;->download_size_bytes:Ljava/lang/Long;

    .line 2043550
    iput-object p4, p0, LX/DpA;->download_hash:[B

    .line 2043551
    iput-object p5, p0, LX/DpA;->suggested_file_name:Ljava/lang/String;

    .line 2043552
    iput-object p6, p0, LX/DpA;->file_mime_type:Ljava/lang/String;

    .line 2043553
    iput-object p7, p0, LX/DpA;->thumbnail_data:[B

    .line 2043554
    iput-object p8, p0, LX/DpA;->image_metadata:LX/DpI;

    .line 2043555
    iput-object p9, p0, LX/DpA;->video_metadata:LX/Dpp;

    .line 2043556
    iput-object p10, p0, LX/DpA;->download_mac:Ljava/lang/String;

    .line 2043557
    return-void
.end method

.method public static b(LX/1su;)LX/DpA;
    .locals 14

    .prologue
    .line 2043558
    const/4 v1, 0x0

    .line 2043559
    const/4 v2, 0x0

    .line 2043560
    const/4 v3, 0x0

    .line 2043561
    const/4 v4, 0x0

    .line 2043562
    const/4 v5, 0x0

    .line 2043563
    const/4 v6, 0x0

    .line 2043564
    const/4 v7, 0x0

    .line 2043565
    const/4 v8, 0x0

    .line 2043566
    const/4 v9, 0x0

    .line 2043567
    const/4 v10, 0x0

    .line 2043568
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 2043569
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 2043570
    iget-byte v11, v0, LX/1sw;->b:B

    if-eqz v11, :cond_d

    .line 2043571
    iget-short v11, v0, LX/1sw;->c:S

    packed-switch v11, :pswitch_data_0

    .line 2043572
    :pswitch_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2043573
    :pswitch_1
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xb

    if-ne v11, v12, :cond_0

    .line 2043574
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v1

    goto :goto_0

    .line 2043575
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2043576
    :pswitch_2
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xa

    if-ne v11, v12, :cond_1

    .line 2043577
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    .line 2043578
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2043579
    :pswitch_3
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xa

    if-ne v11, v12, :cond_2

    .line 2043580
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 2043581
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2043582
    :pswitch_4
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xb

    if-ne v11, v12, :cond_3

    .line 2043583
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v4

    goto :goto_0

    .line 2043584
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2043585
    :pswitch_5
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xb

    if-ne v11, v12, :cond_4

    .line 2043586
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 2043587
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2043588
    :pswitch_6
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xb

    if-ne v11, v12, :cond_5

    .line 2043589
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 2043590
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2043591
    :pswitch_7
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xb

    if-ne v11, v12, :cond_6

    .line 2043592
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v7

    goto/16 :goto_0

    .line 2043593
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2043594
    :pswitch_8
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xc

    if-ne v11, v12, :cond_a

    .line 2043595
    const/4 v0, 0x0

    const/16 v13, 0x8

    .line 2043596
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v8, v0

    .line 2043597
    :goto_1
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v11

    .line 2043598
    iget-byte v12, v11, LX/1sw;->b:B

    if-eqz v12, :cond_9

    .line 2043599
    iget-short v12, v11, LX/1sw;->c:S

    packed-switch v12, :pswitch_data_1

    .line 2043600
    iget-byte v11, v11, LX/1sw;->b:B

    invoke-static {p0, v11}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2043601
    :pswitch_9
    iget-byte v12, v11, LX/1sw;->b:B

    if-ne v12, v13, :cond_7

    .line 2043602
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_1

    .line 2043603
    :cond_7
    iget-byte v11, v11, LX/1sw;->b:B

    invoke-static {p0, v11}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2043604
    :pswitch_a
    iget-byte v12, v11, LX/1sw;->b:B

    if-ne v12, v13, :cond_8

    .line 2043605
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 2043606
    :cond_8
    iget-byte v11, v11, LX/1sw;->b:B

    invoke-static {p0, v11}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2043607
    :cond_9
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2043608
    new-instance v11, LX/DpI;

    invoke-direct {v11, v8, v0}, LX/DpI;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2043609
    move-object v8, v11

    .line 2043610
    goto/16 :goto_0

    .line 2043611
    :cond_a
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2043612
    :pswitch_b
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xc

    if-ne v11, v12, :cond_b

    .line 2043613
    invoke-static {p0}, LX/Dpp;->b(LX/1su;)LX/Dpp;

    move-result-object v9

    goto/16 :goto_0

    .line 2043614
    :cond_b
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2043615
    :pswitch_c
    iget-byte v11, v0, LX/1sw;->b:B

    const/16 v12, 0xb

    if-ne v11, v12, :cond_c

    .line 2043616
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2043617
    :cond_c
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2043618
    :cond_d
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2043619
    new-instance v0, LX/DpA;

    invoke-direct/range {v0 .. v10}, LX/DpA;-><init>([BLjava/lang/Long;Ljava/lang/Long;[BLjava/lang/String;Ljava/lang/String;[BLX/DpI;LX/Dpp;Ljava/lang/String;)V

    .line 2043620
    return-object v0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_c
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x80

    .line 2043436
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2043437
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v3, v0

    .line 2043438
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 2043439
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "AttachmentInfo"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2043440
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043441
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043442
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043443
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043444
    const-string v1, "secret_key"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043445
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043446
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043447
    iget-object v1, p0, LX/DpA;->secret_key:[B

    if-nez v1, :cond_4

    .line 2043448
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043449
    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043450
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043451
    const-string v1, "download_fbid"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043452
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043453
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043454
    iget-object v1, p0, LX/DpA;->download_fbid:Ljava/lang/Long;

    if-nez v1, :cond_5

    .line 2043455
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043456
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043457
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043458
    const-string v1, "download_size_bytes"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043459
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043460
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043461
    iget-object v1, p0, LX/DpA;->download_size_bytes:Ljava/lang/Long;

    if-nez v1, :cond_6

    .line 2043462
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043463
    :goto_5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043464
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043465
    const-string v1, "download_hash"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043466
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043467
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043468
    iget-object v1, p0, LX/DpA;->download_hash:[B

    if-nez v1, :cond_7

    .line 2043469
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043470
    :goto_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043471
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043472
    const-string v1, "suggested_file_name"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043473
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043474
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043475
    iget-object v1, p0, LX/DpA;->suggested_file_name:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 2043476
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043477
    :goto_7
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043478
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043479
    const-string v1, "file_mime_type"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043480
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043481
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043482
    iget-object v1, p0, LX/DpA;->file_mime_type:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 2043483
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043484
    :goto_8
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043485
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043486
    const-string v1, "thumbnail_data"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043487
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043488
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043489
    iget-object v1, p0, LX/DpA;->thumbnail_data:[B

    if-nez v1, :cond_a

    .line 2043490
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043491
    :cond_0
    :goto_9
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043492
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043493
    const-string v1, "image_metadata"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043494
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043495
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043496
    iget-object v1, p0, LX/DpA;->image_metadata:LX/DpI;

    if-nez v1, :cond_e

    .line 2043497
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043498
    :goto_a
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043499
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043500
    const-string v1, "video_metadata"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043501
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043502
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043503
    iget-object v1, p0, LX/DpA;->video_metadata:LX/Dpp;

    if-nez v1, :cond_f

    .line 2043504
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043505
    :goto_b
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043506
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043507
    const-string v1, "download_mac"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043508
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043509
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043510
    iget-object v0, p0, LX/DpA;->download_mac:Ljava/lang/String;

    if-nez v0, :cond_10

    .line 2043511
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043512
    :goto_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043513
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043514
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2043515
    :cond_1
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2043516
    :cond_2
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2043517
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 2043518
    :cond_4
    iget-object v1, p0, LX/DpA;->secret_key:[B

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2043519
    :cond_5
    iget-object v1, p0, LX/DpA;->download_fbid:Ljava/lang/Long;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2043520
    :cond_6
    iget-object v1, p0, LX/DpA;->download_size_bytes:Ljava/lang/Long;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2043521
    :cond_7
    iget-object v1, p0, LX/DpA;->download_hash:[B

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2043522
    :cond_8
    iget-object v1, p0, LX/DpA;->suggested_file_name:Ljava/lang/String;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2043523
    :cond_9
    iget-object v1, p0, LX/DpA;->file_mime_type:Ljava/lang/String;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 2043524
    :cond_a
    iget-object v1, p0, LX/DpA;->thumbnail_data:[B

    array-length v1, v1

    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 2043525
    const/4 v1, 0x0

    move v2, v1

    :goto_d
    if-ge v2, v6, :cond_d

    .line 2043526
    if-eqz v2, :cond_b

    const-string v1, " "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043527
    :cond_b
    iget-object v1, p0, LX/DpA;->thumbnail_data:[B

    aget-byte v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v7, 0x1

    if-le v1, v7, :cond_c

    iget-object v1, p0, LX/DpA;->thumbnail_data:[B

    aget-byte v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v7, p0, LX/DpA;->thumbnail_data:[B

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    :goto_e
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043528
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_d

    .line 2043529
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "0"

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, LX/DpA;->thumbnail_data:[B

    aget-byte v7, v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_e

    .line 2043530
    :cond_d
    iget-object v1, p0, LX/DpA;->thumbnail_data:[B

    array-length v1, v1

    if-le v1, v8, :cond_0

    const-string v1, " ..."

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 2043531
    :cond_e
    iget-object v1, p0, LX/DpA;->image_metadata:LX/DpI;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 2043532
    :cond_f
    iget-object v1, p0, LX/DpA;->video_metadata:LX/Dpp;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v1, v2, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 2043533
    :cond_10
    iget-object v0, p0, LX/DpA;->download_mac:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2043402
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2043403
    iget-object v0, p0, LX/DpA;->secret_key:[B

    if-eqz v0, :cond_0

    .line 2043404
    sget-object v0, LX/DpA;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043405
    iget-object v0, p0, LX/DpA;->secret_key:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2043406
    :cond_0
    iget-object v0, p0, LX/DpA;->download_fbid:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2043407
    sget-object v0, LX/DpA;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043408
    iget-object v0, p0, LX/DpA;->download_fbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2043409
    :cond_1
    iget-object v0, p0, LX/DpA;->download_size_bytes:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2043410
    sget-object v0, LX/DpA;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043411
    iget-object v0, p0, LX/DpA;->download_size_bytes:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2043412
    :cond_2
    iget-object v0, p0, LX/DpA;->download_hash:[B

    if-eqz v0, :cond_3

    .line 2043413
    sget-object v0, LX/DpA;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043414
    iget-object v0, p0, LX/DpA;->download_hash:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2043415
    :cond_3
    iget-object v0, p0, LX/DpA;->suggested_file_name:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2043416
    sget-object v0, LX/DpA;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043417
    iget-object v0, p0, LX/DpA;->suggested_file_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2043418
    :cond_4
    iget-object v0, p0, LX/DpA;->file_mime_type:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2043419
    sget-object v0, LX/DpA;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043420
    iget-object v0, p0, LX/DpA;->file_mime_type:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2043421
    :cond_5
    iget-object v0, p0, LX/DpA;->thumbnail_data:[B

    if-eqz v0, :cond_6

    .line 2043422
    sget-object v0, LX/DpA;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043423
    iget-object v0, p0, LX/DpA;->thumbnail_data:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2043424
    :cond_6
    iget-object v0, p0, LX/DpA;->image_metadata:LX/DpI;

    if-eqz v0, :cond_7

    .line 2043425
    sget-object v0, LX/DpA;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043426
    iget-object v0, p0, LX/DpA;->image_metadata:LX/DpI;

    invoke-virtual {v0, p1}, LX/DpI;->a(LX/1su;)V

    .line 2043427
    :cond_7
    iget-object v0, p0, LX/DpA;->video_metadata:LX/Dpp;

    if-eqz v0, :cond_8

    .line 2043428
    sget-object v0, LX/DpA;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043429
    iget-object v0, p0, LX/DpA;->video_metadata:LX/Dpp;

    invoke-virtual {v0, p1}, LX/Dpp;->a(LX/1su;)V

    .line 2043430
    :cond_8
    iget-object v0, p0, LX/DpA;->download_mac:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 2043431
    sget-object v0, LX/DpA;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043432
    iget-object v0, p0, LX/DpA;->download_mac:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2043433
    :cond_9
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2043434
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2043435
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2043320
    if-nez p1, :cond_1

    .line 2043321
    :cond_0
    :goto_0
    return v0

    .line 2043322
    :cond_1
    instance-of v1, p1, LX/DpA;

    if-eqz v1, :cond_0

    .line 2043323
    check-cast p1, LX/DpA;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2043324
    if-nez p1, :cond_3

    .line 2043325
    :cond_2
    :goto_1
    move v0, v2

    .line 2043326
    goto :goto_0

    .line 2043327
    :cond_3
    iget-object v0, p0, LX/DpA;->secret_key:[B

    if-eqz v0, :cond_18

    move v0, v1

    .line 2043328
    :goto_2
    iget-object v3, p1, LX/DpA;->secret_key:[B

    if-eqz v3, :cond_19

    move v3, v1

    .line 2043329
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2043330
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043331
    iget-object v0, p0, LX/DpA;->secret_key:[B

    iget-object v3, p1, LX/DpA;->secret_key:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043332
    :cond_5
    iget-object v0, p0, LX/DpA;->download_fbid:Ljava/lang/Long;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 2043333
    :goto_4
    iget-object v3, p1, LX/DpA;->download_fbid:Ljava/lang/Long;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 2043334
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2043335
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043336
    iget-object v0, p0, LX/DpA;->download_fbid:Ljava/lang/Long;

    iget-object v3, p1, LX/DpA;->download_fbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043337
    :cond_7
    iget-object v0, p0, LX/DpA;->download_size_bytes:Ljava/lang/Long;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 2043338
    :goto_6
    iget-object v3, p1, LX/DpA;->download_size_bytes:Ljava/lang/Long;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 2043339
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2043340
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043341
    iget-object v0, p0, LX/DpA;->download_size_bytes:Ljava/lang/Long;

    iget-object v3, p1, LX/DpA;->download_size_bytes:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043342
    :cond_9
    iget-object v0, p0, LX/DpA;->download_hash:[B

    if-eqz v0, :cond_1e

    move v0, v1

    .line 2043343
    :goto_8
    iget-object v3, p1, LX/DpA;->download_hash:[B

    if-eqz v3, :cond_1f

    move v3, v1

    .line 2043344
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2043345
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043346
    iget-object v0, p0, LX/DpA;->download_hash:[B

    iget-object v3, p1, LX/DpA;->download_hash:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043347
    :cond_b
    iget-object v0, p0, LX/DpA;->suggested_file_name:Ljava/lang/String;

    if-eqz v0, :cond_20

    move v0, v1

    .line 2043348
    :goto_a
    iget-object v3, p1, LX/DpA;->suggested_file_name:Ljava/lang/String;

    if-eqz v3, :cond_21

    move v3, v1

    .line 2043349
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2043350
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043351
    iget-object v0, p0, LX/DpA;->suggested_file_name:Ljava/lang/String;

    iget-object v3, p1, LX/DpA;->suggested_file_name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043352
    :cond_d
    iget-object v0, p0, LX/DpA;->file_mime_type:Ljava/lang/String;

    if-eqz v0, :cond_22

    move v0, v1

    .line 2043353
    :goto_c
    iget-object v3, p1, LX/DpA;->file_mime_type:Ljava/lang/String;

    if-eqz v3, :cond_23

    move v3, v1

    .line 2043354
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 2043355
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043356
    iget-object v0, p0, LX/DpA;->file_mime_type:Ljava/lang/String;

    iget-object v3, p1, LX/DpA;->file_mime_type:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043357
    :cond_f
    iget-object v0, p0, LX/DpA;->thumbnail_data:[B

    if-eqz v0, :cond_24

    move v0, v1

    .line 2043358
    :goto_e
    iget-object v3, p1, LX/DpA;->thumbnail_data:[B

    if-eqz v3, :cond_25

    move v3, v1

    .line 2043359
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 2043360
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043361
    iget-object v0, p0, LX/DpA;->thumbnail_data:[B

    iget-object v3, p1, LX/DpA;->thumbnail_data:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043362
    :cond_11
    iget-object v0, p0, LX/DpA;->image_metadata:LX/DpI;

    if-eqz v0, :cond_26

    move v0, v1

    .line 2043363
    :goto_10
    iget-object v3, p1, LX/DpA;->image_metadata:LX/DpI;

    if-eqz v3, :cond_27

    move v3, v1

    .line 2043364
    :goto_11
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 2043365
    :cond_12
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043366
    iget-object v0, p0, LX/DpA;->image_metadata:LX/DpI;

    iget-object v3, p1, LX/DpA;->image_metadata:LX/DpI;

    invoke-virtual {v0, v3}, LX/DpI;->a(LX/DpI;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043367
    :cond_13
    iget-object v0, p0, LX/DpA;->video_metadata:LX/Dpp;

    if-eqz v0, :cond_28

    move v0, v1

    .line 2043368
    :goto_12
    iget-object v3, p1, LX/DpA;->video_metadata:LX/Dpp;

    if-eqz v3, :cond_29

    move v3, v1

    .line 2043369
    :goto_13
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 2043370
    :cond_14
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043371
    iget-object v0, p0, LX/DpA;->video_metadata:LX/Dpp;

    iget-object v3, p1, LX/DpA;->video_metadata:LX/Dpp;

    invoke-virtual {v0, v3}, LX/Dpp;->a(LX/Dpp;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043372
    :cond_15
    iget-object v0, p0, LX/DpA;->download_mac:Ljava/lang/String;

    if-eqz v0, :cond_2a

    move v0, v1

    .line 2043373
    :goto_14
    iget-object v3, p1, LX/DpA;->download_mac:Ljava/lang/String;

    if-eqz v3, :cond_2b

    move v3, v1

    .line 2043374
    :goto_15
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 2043375
    :cond_16
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043376
    iget-object v0, p0, LX/DpA;->download_mac:Ljava/lang/String;

    iget-object v3, p1, LX/DpA;->download_mac:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_17
    move v2, v1

    .line 2043377
    goto/16 :goto_1

    :cond_18
    move v0, v2

    .line 2043378
    goto/16 :goto_2

    :cond_19
    move v3, v2

    .line 2043379
    goto/16 :goto_3

    :cond_1a
    move v0, v2

    .line 2043380
    goto/16 :goto_4

    :cond_1b
    move v3, v2

    .line 2043381
    goto/16 :goto_5

    :cond_1c
    move v0, v2

    .line 2043382
    goto/16 :goto_6

    :cond_1d
    move v3, v2

    .line 2043383
    goto/16 :goto_7

    :cond_1e
    move v0, v2

    .line 2043384
    goto/16 :goto_8

    :cond_1f
    move v3, v2

    .line 2043385
    goto/16 :goto_9

    :cond_20
    move v0, v2

    .line 2043386
    goto/16 :goto_a

    :cond_21
    move v3, v2

    .line 2043387
    goto/16 :goto_b

    :cond_22
    move v0, v2

    .line 2043388
    goto/16 :goto_c

    :cond_23
    move v3, v2

    .line 2043389
    goto/16 :goto_d

    :cond_24
    move v0, v2

    .line 2043390
    goto/16 :goto_e

    :cond_25
    move v3, v2

    .line 2043391
    goto/16 :goto_f

    :cond_26
    move v0, v2

    .line 2043392
    goto :goto_10

    :cond_27
    move v3, v2

    .line 2043393
    goto :goto_11

    :cond_28
    move v0, v2

    .line 2043394
    goto :goto_12

    :cond_29
    move v3, v2

    .line 2043395
    goto :goto_13

    :cond_2a
    move v0, v2

    .line 2043396
    goto :goto_14

    :cond_2b
    move v3, v2

    .line 2043397
    goto :goto_15
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2043401
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2043398
    sget-boolean v0, LX/DpA;->a:Z

    .line 2043399
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpA;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2043400
    return-object v0
.end method
