.class public final LX/DVt;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;)V
    .locals 0

    .prologue
    .line 2005874
    iput-object p1, p0, LX/DVt;->a:Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 2005875
    iget-object v0, p0, LX/DVt;->a:Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2005876
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2005877
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2005878
    const-string v2, "groups_custom_invite_message"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2005879
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2005880
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2005881
    iget-object v1, p0, LX/DVt;->a:Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2005882
    :cond_0
    iget-object v0, p0, LX/DVt;->a:Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2005883
    return-void
.end method
