.class public LX/CrG;
.super LX/Cqj;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 1940747
    invoke-direct {p0, p1, p2}, LX/Cqj;-><init>(LX/Ctg;LX/CrK;)V

    .line 1940748
    const-class v0, LX/CrG;

    invoke-static {v0, p0}, LX/CrG;->a(Ljava/lang/Class;LX/02k;)V

    .line 1940749
    new-instance v0, LX/CrF;

    sget-object v1, LX/Cqw;->a:LX/Cqw;

    .line 1940750
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940751
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/CrF;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940752
    new-instance v0, LX/CrF;

    sget-object v1, LX/Cqw;->b:LX/Cqw;

    .line 1940753
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940754
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_VIEWPORT:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/CrF;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940755
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CrG;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object p0

    check-cast p0, LX/Cju;

    iput-object p0, p1, LX/CrG;->a:LX/Cju;

    return-void
.end method


# virtual methods
.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1940756
    iget-object v0, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1940757
    check-cast v0, LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 1940758
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1940759
    :goto_0
    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1940760
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    .line 1940761
    :cond_0
    iget-object v1, p0, LX/CrG;->a:LX/Cju;

    const v2, 0x7f0d0122

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    .line 1940762
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p0, v1, v2}, LX/Cqi;->a(II)V

    .line 1940763
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/Cqi;->b(II)V

    .line 1940764
    return-void
.end method
