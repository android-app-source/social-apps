.class public final LX/Dx9;
.super LX/DvC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)V
    .locals 0

    .prologue
    .line 2062423
    iput-object p1, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    invoke-direct {p0}, LX/DvC;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 2062424
    check-cast p1, LX/DvB;

    .line 2062425
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/DvB;->d:LX/DvW;

    sget-object v1, LX/DvW;->TAGGED_MEDIA_SET:LX/DvW;

    if-eq v0, v1, :cond_1

    .line 2062426
    :cond_0
    :goto_0
    return-void

    .line 2062427
    :cond_1
    new-instance v6, Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    sget-object v0, LX/3WA;->USER_INITIATED:LX/3WA;

    iget-object v1, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v1, v1, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->o:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v6, v0, v1}, Lcom/facebook/photos/base/photos/PhotoFetchInfo;-><init>(LX/3WA;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2062428
    iget-object v0, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->n:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v0, :cond_3

    iget-object v0, p1, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_3

    .line 2062429
    iget-object v0, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->n:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2062430
    iget-object v0, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v1, p1, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    iget-object v2, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v3, v3, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;Landroid/support/v4/app/FragmentActivity;J)V

    goto :goto_0

    .line 2062431
    :cond_2
    iget-object v0, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->n:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2062432
    iget-object v0, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v1, p1, LX/DvB;->e:Lcom/facebook/graphql/model/GraphQLPhoto;

    iget-object v2, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v2, v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->n:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2062433
    iget-object v3, v2, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->c:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-object v2, v3

    .line 2062434
    iget-object v3, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(Lcom/facebook/graphql/model/GraphQLPhoto;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Landroid/app/Activity;)V

    goto :goto_0

    .line 2062435
    :cond_3
    iget-object v0, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2062436
    iget-boolean v1, v0, LX/Dxa;->a:Z

    move v0, v1

    .line 2062437
    if-eqz v0, :cond_4

    .line 2062438
    iget-object v0, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p1, LX/DvB;->b:Landroid/net/Uri;

    iget-object v5, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;Z)V

    goto/16 :goto_0

    .line 2062439
    :cond_4
    iget-object v0, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2062440
    iget-boolean v1, v0, LX/Dxa;->b:Z

    move v0, v1

    .line 2062441
    if-eqz v0, :cond_5

    .line 2062442
    iget-object v0, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v0, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p1, LX/DvB;->b:Landroid/net/Uri;

    iget-object v5, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    const/4 v7, 0x1

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/net/Uri;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/base/photos/PhotoFetchInfo;Z)V

    goto/16 :goto_0

    .line 2062443
    :cond_5
    iget-object v0, p0, LX/Dx9;->a:Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    iget-object v1, p1, LX/DvB;->a:Ljava/lang/String;

    iget-object v2, p1, LX/DvB;->b:Landroid/net/Uri;

    invoke-static {v0, v1, v2}, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->a$redex0(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0
.end method
