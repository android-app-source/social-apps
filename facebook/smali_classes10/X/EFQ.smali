.class public final LX/EFQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2095832
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2095833
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2095834
    :goto_0
    return v1

    .line 2095835
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2095836
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2095837
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2095838
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2095839
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2095840
    const-string v4, "mask_effect"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2095841
    invoke-static {p0, p1}, LX/EFO;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2095842
    :cond_2
    const-string v4, "thumbnail_image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2095843
    invoke-static {p0, p1}, LX/EFP;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2095844
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2095845
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2095846
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2095847
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2095848
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2095849
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2095850
    if-eqz v0, :cond_0

    .line 2095851
    const-string v1, "mask_effect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2095852
    invoke-static {p0, v0, p2, p3}, LX/EFO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2095853
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2095854
    if-eqz v0, :cond_1

    .line 2095855
    const-string v1, "thumbnail_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2095856
    invoke-static {p0, v0, p2}, LX/EFP;->a(LX/15i;ILX/0nX;)V

    .line 2095857
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2095858
    return-void
.end method
