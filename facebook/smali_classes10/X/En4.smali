.class public LX/En4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2166615
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/En2;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2166616
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move v0, v1

    .line 2166617
    :goto_0
    invoke-virtual {p0}, LX/En2;->c()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2166618
    invoke-virtual {p0, v0}, LX/En2;->b(I)Ljava/lang/Object;

    move-result-object v5

    .line 2166619
    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_1
    const-string v6, "Duplicate value %s detected in array!"

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v5, v7, v1

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2166620
    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2166621
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 2166622
    goto :goto_1

    .line 2166623
    :cond_1
    return-void
.end method
