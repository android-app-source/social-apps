.class public final LX/Dqv;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4ok;

.field public final synthetic b:LX/Dqy;


# direct methods
.method public constructor <init>(LX/Dqy;LX/4ok;)V
    .locals 0

    .prologue
    .line 2048789
    iput-object p1, p0, LX/Dqv;->b:LX/Dqy;

    iput-object p2, p0, LX/Dqv;->a:LX/4ok;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2048778
    iget-object v1, p0, LX/Dqv;->a:LX/4ok;

    iget-object v0, p0, LX/Dqv;->a:LX/4ok;

    invoke-virtual {v0}, LX/4ok;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, LX/4ok;->setChecked(Z)V

    .line 2048779
    return-void

    .line 2048780
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2048781
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2048782
    iget-object v1, p0, LX/Dqv;->a:LX/4ok;

    .line 2048783
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2048784
    if-eqz v0, :cond_0

    .line 2048785
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2048786
    check-cast v0, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;->a()Z

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, LX/4ok;->setChecked(Z)V

    .line 2048787
    return-void

    .line 2048788
    :cond_0
    iget-object v0, p0, LX/Dqv;->a:LX/4ok;

    invoke-virtual {v0}, LX/4ok;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
