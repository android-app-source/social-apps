.class public final enum LX/D7e;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D7e;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D7e;

.field public static final enum BACKGROUNDED:LX/D7e;

.field public static final enum ENDED:LX/D7e;

.field public static final enum FOREGROUNDED:LX/D7e;

.field public static final enum PAUSED:LX/D7e;

.field public static final enum RESUMED:LX/D7e;

.field public static final enum STARTED:LX/D7e;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1967416
    new-instance v0, LX/D7e;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v3}, LX/D7e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D7e;->STARTED:LX/D7e;

    new-instance v0, LX/D7e;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, LX/D7e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D7e;->PAUSED:LX/D7e;

    new-instance v0, LX/D7e;

    const-string v1, "RESUMED"

    invoke-direct {v0, v1, v5}, LX/D7e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D7e;->RESUMED:LX/D7e;

    new-instance v0, LX/D7e;

    const-string v1, "ENDED"

    invoke-direct {v0, v1, v6}, LX/D7e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D7e;->ENDED:LX/D7e;

    new-instance v0, LX/D7e;

    const-string v1, "BACKGROUNDED"

    invoke-direct {v0, v1, v7}, LX/D7e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D7e;->BACKGROUNDED:LX/D7e;

    new-instance v0, LX/D7e;

    const-string v1, "FOREGROUNDED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/D7e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D7e;->FOREGROUNDED:LX/D7e;

    .line 1967417
    const/4 v0, 0x6

    new-array v0, v0, [LX/D7e;

    sget-object v1, LX/D7e;->STARTED:LX/D7e;

    aput-object v1, v0, v3

    sget-object v1, LX/D7e;->PAUSED:LX/D7e;

    aput-object v1, v0, v4

    sget-object v1, LX/D7e;->RESUMED:LX/D7e;

    aput-object v1, v0, v5

    sget-object v1, LX/D7e;->ENDED:LX/D7e;

    aput-object v1, v0, v6

    sget-object v1, LX/D7e;->BACKGROUNDED:LX/D7e;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/D7e;->FOREGROUNDED:LX/D7e;

    aput-object v2, v0, v1

    sput-object v0, LX/D7e;->$VALUES:[LX/D7e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1967415
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D7e;
    .locals 1

    .prologue
    .line 1967413
    const-class v0, LX/D7e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D7e;

    return-object v0
.end method

.method public static values()[LX/D7e;
    .locals 1

    .prologue
    .line 1967414
    sget-object v0, LX/D7e;->$VALUES:[LX/D7e;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D7e;

    return-object v0
.end method
