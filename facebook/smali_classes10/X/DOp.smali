.class public LX/DOp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1DS;


# direct methods
.method public constructor <init>(LX/1DS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1992815
    iput-object p1, p0, LX/DOp;->a:LX/1DS;

    .line 1992816
    return-void
.end method

.method public static a(LX/0QB;)LX/DOp;
    .locals 1

    .prologue
    .line 1992805
    invoke-static {p0}, LX/DOp;->b(LX/0QB;)LX/DOp;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DOp;
    .locals 2

    .prologue
    .line 1992812
    new-instance v1, LX/DOp;

    invoke-static {p0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v0

    check-cast v0, LX/1DS;

    invoke-direct {v1, v0}, LX/DOp;-><init>(LX/1DS;)V

    .line 1992813
    return-object v1
.end method


# virtual methods
.method public final a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pf;",
            ">(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<**-TE;>;>;TE;",
            "LX/DNR;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 1992806
    new-instance v0, LX/DOo;

    invoke-direct {v0, p0, p4}, LX/DOo;-><init>(LX/DOp;LX/DNR;)V

    .line 1992807
    iget-object v1, p0, LX/DOp;->a:LX/1DS;

    invoke-virtual {v1, p2, p1}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ql;->a(LX/99g;)LX/1Ql;

    move-result-object v0

    .line 1992808
    iput-object p3, v0, LX/1Ql;->f:LX/1PW;

    .line 1992809
    move-object v0, v0

    .line 1992810
    move-object v0, v0

    .line 1992811
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    return-object v0
.end method
