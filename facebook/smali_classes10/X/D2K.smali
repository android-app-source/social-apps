.class public LX/D2K;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1958537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;FLX/7N6;Landroid/view/View$OnClickListener;Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)V
    .locals 8
    .param p5    # Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1958538
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1958539
    new-instance v2, LX/2oE;

    invoke-direct {v2}, LX/2oE;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1958540
    iput-object v3, v2, LX/2oE;->a:Landroid/net/Uri;

    .line 1958541
    move-object v2, v2

    .line 1958542
    sget-object v3, LX/097;->FROM_STREAM:LX/097;

    .line 1958543
    iput-object v3, v2, LX/2oE;->e:LX/097;

    .line 1958544
    move-object v3, v2

    .line 1958545
    if-eqz p5, :cond_2

    invoke-virtual {p5}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->shouldFlipHorizontally()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    .line 1958546
    :goto_0
    iput-object v2, v3, LX/2oE;->g:LX/2oF;

    .line 1958547
    move-object v2, v3

    .line 1958548
    invoke-virtual {v2}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v2

    .line 1958549
    new-instance v3, LX/2oH;

    invoke-direct {v3}, LX/2oH;-><init>()V

    invoke-virtual {v3, v2}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 1958550
    iput-object v3, v2, LX/2oH;->b:Ljava/lang/String;

    .line 1958551
    move-object v2, v2

    .line 1958552
    const/4 v3, 0x1

    .line 1958553
    iput-boolean v3, v2, LX/2oH;->g:Z

    .line 1958554
    move-object v2, v2

    .line 1958555
    invoke-virtual {v2}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v4

    .line 1958556
    const/4 v3, 0x0

    .line 1958557
    const/4 v2, -0x1

    .line 1958558
    if-eqz p5, :cond_0

    .line 1958559
    invoke-virtual {p5}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v5

    .line 1958560
    if-eqz v5, :cond_0

    iget-boolean v6, v5, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->isTrimSpecified:Z

    if-eqz v6, :cond_0

    .line 1958561
    iget v3, v5, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    .line 1958562
    iget v2, v5, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    .line 1958563
    :cond_0
    new-instance v5, LX/2pZ;

    invoke-direct {v5}, LX/2pZ;-><init>()V

    .line 1958564
    iput-object v4, v5, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1958565
    move-object v4, v5

    .line 1958566
    const-string v5, "TrimStartPosition"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v3

    const-string v4, "TrimEndPosition"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v2

    float-to-double v4, p2

    .line 1958567
    iput-wide v4, v2, LX/2pZ;->e:D

    .line 1958568
    move-object v2, v2

    .line 1958569
    invoke-virtual {v2}, LX/2pZ;->b()LX/2pa;

    move-result-object v2

    move-object v0, v2

    .line 1958570
    invoke-virtual {p0, p4}, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1958571
    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;->m:LX/7MX;

    move-object v1, v1

    .line 1958572
    iput-object p3, v1, LX/7MX;->a:LX/7N6;

    .line 1958573
    const/4 v2, 0x3

    iput v2, v1, LX/7MX;->b:I

    .line 1958574
    iget-object v2, v1, LX/7MX;->a:LX/7N6;

    invoke-interface {v2}, LX/7N6;->c()V

    .line 1958575
    iget-object v2, v1, LX/2oy;->j:LX/2pb;

    if-eqz v2, :cond_1

    .line 1958576
    iget-object v2, v1, LX/2oy;->j:LX/2pb;

    .line 1958577
    iget-object p3, v2, LX/2pb;->y:LX/2qV;

    move-object v2, p3

    .line 1958578
    invoke-static {v1, v2}, LX/7MX;->a$redex0(LX/7MX;LX/2qV;)V

    .line 1958579
    :cond_1
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1958580
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1958581
    return-void

    .line 1958582
    :cond_2
    sget-object v2, LX/2oF;->NONE:LX/2oF;

    goto :goto_0
.end method
