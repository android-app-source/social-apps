.class public LX/DqG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/DqG;


# instance fields
.field private final a:LX/0Sh;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/3Ax;

.field private final d:LX/1rp;

.field private final e:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field private final f:LX/03V;

.field private final g:LX/1rU;


# direct methods
.method public constructor <init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/3Ax;LX/1rp;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/03V;LX/1rU;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048171
    iput-object p1, p0, LX/DqG;->a:LX/0Sh;

    .line 2048172
    iput-object p2, p0, LX/DqG;->b:Ljava/util/concurrent/ExecutorService;

    .line 2048173
    iput-object p3, p0, LX/DqG;->c:LX/3Ax;

    .line 2048174
    iput-object p4, p0, LX/DqG;->d:LX/1rp;

    .line 2048175
    iput-object p5, p0, LX/DqG;->e:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 2048176
    iput-object p6, p0, LX/DqG;->f:LX/03V;

    .line 2048177
    iput-object p7, p0, LX/DqG;->g:LX/1rU;

    .line 2048178
    return-void
.end method

.method public static a(LX/0QB;)LX/DqG;
    .locals 11

    .prologue
    .line 2048179
    sget-object v0, LX/DqG;->h:LX/DqG;

    if-nez v0, :cond_1

    .line 2048180
    const-class v1, LX/DqG;

    monitor-enter v1

    .line 2048181
    :try_start_0
    sget-object v0, LX/DqG;->h:LX/DqG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2048182
    if-eqz v2, :cond_0

    .line 2048183
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2048184
    new-instance v3, LX/DqG;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/3Ax;->a(LX/0QB;)LX/3Ax;

    move-result-object v6

    check-cast v6, LX/3Ax;

    invoke-static {v0}, LX/1rp;->a(LX/0QB;)LX/1rp;

    move-result-object v7

    check-cast v7, LX/1rp;

    invoke-static {v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v8

    check-cast v8, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v10

    check-cast v10, LX/1rU;

    invoke-direct/range {v3 .. v10}, LX/DqG;-><init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/3Ax;LX/1rp;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/03V;LX/1rU;)V

    .line 2048185
    move-object v0, v3

    .line 2048186
    sput-object v0, LX/DqG;->h:LX/DqG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2048187
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2048188
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2048189
    :cond_1
    sget-object v0, LX/DqG;->h:LX/DqG;

    return-object v0

    .line 2048190
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2048191
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/DqQ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2048192
    iget-object v0, p0, LX/DqG;->a:LX/0Sh;

    const-string v1, "Trying to access db from ui thread"

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 2048193
    iget-object v0, p0, LX/DqG;->c:LX/3Ax;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2048194
    const-string v1, "push_notifications"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/3B0;->e:LX/0U1;

    .line 2048195
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2048196
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " DESC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2048197
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2048198
    if-eqz v1, :cond_5

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2048199
    :cond_0
    invoke-static {v1}, LX/DqQ;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 2048200
    iget-object v4, p0, LX/DqG;->g:LX/1rU;

    invoke-virtual {v4}, LX/1rU;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2048201
    iget-object v4, p0, LX/DqG;->d:LX/1rp;

    invoke-virtual {v4}, LX/1rp;->a()LX/2kW;

    move-result-object v4

    invoke-static {v4, v0}, LX/3Cu;->b(LX/2kW;Ljava/lang/String;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    .line 2048202
    if-nez v0, :cond_3

    move-object v0, v2

    .line 2048203
    :goto_0
    new-instance v4, LX/DqQ;

    invoke-direct {v4, v1, v0}, LX/DqQ;-><init>(Landroid/database/Cursor;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2048204
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2048205
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 2048206
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2048207
    :cond_2
    :goto_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 2048208
    :cond_3
    :try_start_1
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 2048209
    :cond_4
    iget-object v4, p0, LX/DqG;->e:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v4, v0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 2048210
    :cond_5
    if-eqz v1, :cond_1

    .line 2048211
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2048212
    :catch_0
    move-exception v0

    .line 2048213
    :try_start_2
    iget-object v2, p0, LX/DqG;->f:LX/03V;

    const-string v4, "LockscreenUtil_unseen_push_notif_fetch_error"

    const-string v5, "Error fetching unseen push notifications"

    invoke-virtual {v2, v4, v5, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2048214
    if-eqz v1, :cond_2

    .line 2048215
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 2048216
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_6

    .line 2048217
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2048218
    iget-object v0, p0, LX/DqG;->a:LX/0Sh;

    const-string v1, "Trying to access db on ui thread"

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 2048219
    iget-object v0, p0, LX/DqG;->c:LX/3Ax;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2048220
    const-string v1, "push_notifications"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2048221
    return-void
.end method
