.class public final LX/EYQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/EXV;

.field public final b:[LX/EYF;

.field public final c:[LX/EYL;

.field public final d:[LX/EYS;

.field public final e:[LX/EYP;

.field private final f:[LX/EYQ;

.field public final g:[LX/EYQ;

.field public final h:LX/EYJ;


# direct methods
.method private constructor <init>(LX/EXV;[LX/EYQ;LX/EYJ;)V
    .locals 8

    .prologue
    .line 2137291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137292
    iput-object p3, p0, LX/EYQ;->h:LX/EYJ;

    .line 2137293
    iput-object p1, p0, LX/EYQ;->a:LX/EXV;

    .line 2137294
    invoke-virtual {p2}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EYQ;

    iput-object v0, p0, LX/EYQ;->f:[LX/EYQ;

    .line 2137295
    invoke-virtual {p1}, LX/EXV;->o()I

    move-result v0

    new-array v0, v0, [LX/EYQ;

    iput-object v0, p0, LX/EYQ;->g:[LX/EYQ;

    .line 2137296
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, LX/EXV;->o()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2137297
    invoke-virtual {p1, v0}, LX/EXV;->b(I)I

    move-result v1

    .line 2137298
    if-ltz v1, :cond_0

    iget-object v2, p0, LX/EYQ;->f:[LX/EYQ;

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 2137299
    :cond_0
    new-instance v0, LX/EYK;

    const-string v1, "Invalid public dependency index."

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYQ;Ljava/lang/String;)V

    throw v0

    .line 2137300
    :cond_1
    iget-object v1, p0, LX/EYQ;->g:[LX/EYQ;

    iget-object v2, p0, LX/EYQ;->f:[LX/EYQ;

    invoke-virtual {p1, v0}, LX/EXV;->b(I)I

    move-result v3

    aget-object v2, v2, v3

    aput-object v2, v1, v0

    .line 2137301
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2137302
    :cond_2
    invoke-virtual {p0}, LX/EYQ;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0, p0}, LX/EYJ;->a(Ljava/lang/String;LX/EYQ;)V

    .line 2137303
    invoke-virtual {p1}, LX/EXV;->p()I

    move-result v0

    new-array v0, v0, [LX/EYF;

    iput-object v0, p0, LX/EYQ;->b:[LX/EYF;

    .line 2137304
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {p1}, LX/EXV;->p()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 2137305
    iget-object v6, p0, LX/EYQ;->b:[LX/EYF;

    new-instance v0, LX/EYF;

    invoke-virtual {p1, v4}, LX/EXV;->c(I)LX/EWr;

    move-result-object v1

    const/4 v3, 0x0

    move-object v2, p0

    invoke-direct/range {v0 .. v4}, LX/EYF;-><init>(LX/EWr;LX/EYQ;LX/EYF;I)V

    aput-object v0, v6, v4

    .line 2137306
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2137307
    :cond_3
    invoke-virtual {p1}, LX/EXV;->q()I

    move-result v0

    new-array v0, v0, [LX/EYL;

    iput-object v0, p0, LX/EYQ;->c:[LX/EYL;

    .line 2137308
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {p1}, LX/EXV;->q()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 2137309
    iget-object v6, p0, LX/EYQ;->c:[LX/EYL;

    new-instance v0, LX/EYL;

    invoke-virtual {p1, v4}, LX/EXV;->d(I)LX/EWv;

    move-result-object v1

    const/4 v3, 0x0

    move-object v2, p0

    invoke-direct/range {v0 .. v4}, LX/EYL;-><init>(LX/EWv;LX/EYQ;LX/EYF;I)V

    aput-object v0, v6, v4

    .line 2137310
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2137311
    :cond_4
    invoke-virtual {p1}, LX/EXV;->r()I

    move-result v0

    new-array v0, v0, [LX/EYS;

    iput-object v0, p0, LX/EYQ;->d:[LX/EYS;

    .line 2137312
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p1}, LX/EXV;->r()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 2137313
    iget-object v1, p0, LX/EYQ;->d:[LX/EYS;

    new-instance v2, LX/EYS;

    invoke-virtual {p1, v0}, LX/EXV;->e(I)LX/EXr;

    move-result-object v3

    invoke-direct {v2, v3, p0, v0}, LX/EYS;-><init>(LX/EXr;LX/EYQ;I)V

    aput-object v2, v1, v0

    .line 2137314
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2137315
    :cond_5
    invoke-virtual {p1}, LX/EXV;->w()I

    move-result v0

    new-array v0, v0, [LX/EYP;

    iput-object v0, p0, LX/EYQ;->e:[LX/EYP;

    .line 2137316
    const/4 v4, 0x0

    :goto_4
    invoke-virtual {p1}, LX/EXV;->w()I

    move-result v0

    if-ge v4, v0, :cond_6

    .line 2137317
    iget-object v7, p0, LX/EYQ;->e:[LX/EYP;

    new-instance v0, LX/EYP;

    invoke-virtual {p1, v4}, LX/EXV;->f(I)LX/EXL;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, LX/EYP;-><init>(LX/EXL;LX/EYQ;LX/EYF;IZB)V

    aput-object v0, v7, v4

    .line 2137318
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 2137319
    :cond_6
    return-void
.end method

.method private static a(LX/EXV;[LX/EYQ;)LX/EYQ;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2137268
    new-instance v0, LX/EYJ;

    invoke-direct {v0, p1}, LX/EYJ;-><init>([LX/EYQ;)V

    .line 2137269
    new-instance v2, LX/EYQ;

    invoke-direct {v2, p0, p1, v0}, LX/EYQ;-><init>(LX/EXV;[LX/EYQ;LX/EYJ;)V

    .line 2137270
    array-length v0, p1

    invoke-virtual {p0}, LX/EXV;->n()I

    move-result v3

    if-eq v0, v3, :cond_0

    .line 2137271
    new-instance v0, LX/EYK;

    const-string v3, "Dependencies passed to FileDescriptor.buildFrom() don\'t match those listed in the FileDescriptorProto."

    invoke-direct {v0, v2, v3}, LX/EYK;-><init>(LX/EYQ;Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    .line 2137272
    :goto_0
    invoke-virtual {p0}, LX/EXV;->n()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 2137273
    aget-object v3, p1, v0

    invoke-virtual {v3}, LX/EYQ;->b()Ljava/lang/String;

    move-result-object v3

    .line 2137274
    iget-object v1, p0, LX/EXV;->dependency_:LX/EYv;

    invoke-interface {v1, v0}, LX/EYv;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    .line 2137275
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2137276
    new-instance v0, LX/EYK;

    const-string v3, "Dependencies passed to FileDescriptor.buildFrom() don\'t match those listed in the FileDescriptorProto."

    invoke-direct {v0, v2, v3}, LX/EYK;-><init>(LX/EYQ;Ljava/lang/String;)V

    throw v0

    .line 2137277
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2137278
    :cond_2
    const/4 v0, 0x0

    .line 2137279
    iget-object v3, v2, LX/EYQ;->b:[LX/EYF;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 2137280
    invoke-static {v5}, LX/EYF;->j(LX/EYF;)V

    .line 2137281
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2137282
    :cond_3
    iget-object v3, v2, LX/EYQ;->d:[LX/EYS;

    array-length v4, v3

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 2137283
    iget-object v7, v5, LX/EYS;->e:[LX/EYR;

    array-length p0, v7

    const/4 v6, 0x0

    :goto_3
    if-ge v6, p0, :cond_4

    aget-object p1, v7, v6

    .line 2137284
    invoke-static {p1}, LX/EYR;->e(LX/EYR;)V

    .line 2137285
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 2137286
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2137287
    :cond_5
    iget-object v1, v2, LX/EYQ;->e:[LX/EYP;

    array-length v3, v1

    :goto_4
    if-ge v0, v3, :cond_6

    aget-object v4, v1, v0

    .line 2137288
    invoke-static {v4}, LX/EYP;->x(LX/EYP;)V

    .line 2137289
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2137290
    :cond_6
    return-object v2
.end method

.method public static a([Ljava/lang/String;[LX/EYQ;LX/EWg;)V
    .locals 6

    .prologue
    .line 2137320
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2137321
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 2137322
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2137323
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2137324
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ISO-8859-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2137325
    :try_start_1
    sget-object v1, LX/EXV;->a:LX/EWZ;

    invoke-virtual {v1, v0}, LX/EWZ;->a([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EXV;

    move-object v1, v1
    :try_end_1
    .catch LX/EYr; {:try_start_1 .. :try_end_1} :catch_1

    .line 2137326
    :try_start_2
    invoke-static {v1, p1}, LX/EYQ;->a(LX/EXV;[LX/EYQ;)LX/EYQ;
    :try_end_2
    .catch LX/EYK; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    .line 2137327
    invoke-interface {p2, v1}, LX/EWg;->a(LX/EYQ;)LX/EYa;

    move-result-object v2

    .line 2137328
    if-eqz v2, :cond_5

    .line 2137329
    :try_start_3
    sget-object v3, LX/EXV;->a:LX/EWZ;

    .line 2137330
    invoke-static {v3, v0, v2}, LX/EWZ;->b(LX/EWZ;[BLX/EYZ;)LX/EWW;

    move-result-object v4

    move-object v3, v4

    .line 2137331
    check-cast v3, LX/EXV;

    move-object v0, v3
    :try_end_3
    .catch LX/EYr; {:try_start_3 .. :try_end_3} :catch_3

    .line 2137332
    const/4 v3, 0x0

    .line 2137333
    iput-object v0, v1, LX/EYQ;->a:LX/EXV;

    move v2, v3

    .line 2137334
    :goto_1
    iget-object v4, v1, LX/EYQ;->b:[LX/EYF;

    array-length v4, v4

    if-ge v2, v4, :cond_1

    .line 2137335
    iget-object v4, v1, LX/EYQ;->b:[LX/EYF;

    aget-object v4, v4, v2

    invoke-virtual {v0, v2}, LX/EXV;->c(I)LX/EWr;

    move-result-object v5

    invoke-static {v4, v5}, LX/EYF;->a$redex0(LX/EYF;LX/EWr;)V

    .line 2137336
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v2, v3

    .line 2137337
    :goto_2
    iget-object v4, v1, LX/EYQ;->c:[LX/EYL;

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 2137338
    iget-object v4, v1, LX/EYQ;->c:[LX/EYL;

    aget-object v4, v4, v2

    invoke-virtual {v0, v2}, LX/EXV;->d(I)LX/EWv;

    move-result-object v5

    invoke-static {v4, v5}, LX/EYL;->a$redex0(LX/EYL;LX/EWv;)V

    .line 2137339
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v2, v3

    .line 2137340
    :goto_3
    iget-object v4, v1, LX/EYQ;->d:[LX/EYS;

    array-length v4, v4

    if-ge v2, v4, :cond_4

    .line 2137341
    iget-object v4, v1, LX/EYQ;->d:[LX/EYS;

    aget-object v4, v4, v2

    invoke-virtual {v0, v2}, LX/EXV;->e(I)LX/EXr;

    move-result-object v5

    .line 2137342
    iput-object v5, v4, LX/EYS;->b:LX/EXr;

    .line 2137343
    const/4 p0, 0x0

    :goto_4
    iget-object p1, v4, LX/EYS;->e:[LX/EYR;

    array-length p1, p1

    if-ge p0, p1, :cond_3

    .line 2137344
    iget-object p1, v4, LX/EYS;->e:[LX/EYR;

    aget-object p1, p1, p0

    invoke-virtual {v5, p0}, LX/EXr;->a(I)LX/EXj;

    move-result-object p2

    .line 2137345
    iput-object p2, p1, LX/EYR;->b:LX/EXj;

    .line 2137346
    add-int/lit8 p0, p0, 0x1

    goto :goto_4

    .line 2137347
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2137348
    :cond_4
    :goto_5
    iget-object v2, v1, LX/EYQ;->e:[LX/EYP;

    array-length v2, v2

    if-ge v3, v2, :cond_5

    .line 2137349
    iget-object v2, v1, LX/EYQ;->e:[LX/EYP;

    aget-object v2, v2, v3

    invoke-virtual {v0, v3}, LX/EXV;->f(I)LX/EXL;

    move-result-object v4

    .line 2137350
    iput-object v4, v2, LX/EYP;->c:LX/EXL;

    .line 2137351
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 2137352
    :cond_5
    return-void

    .line 2137353
    :catch_0
    move-exception v0

    .line 2137354
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Standard encoding ISO-8859-1 not supported by JVM."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2137355
    :catch_1
    move-exception v0

    .line 2137356
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Failed to parse protocol buffer descriptor for generated code."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2137357
    :catch_2
    move-exception v0

    .line 2137358
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid embedded descriptor for \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/EXV;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\"."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 2137359
    :catch_3
    move-exception v0

    .line 2137360
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Failed to parse protocol buffer descriptor for generated code."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137267
    iget-object v0, p0, LX/EYQ;->a:LX/EXV;

    invoke-virtual {v0}, LX/EXV;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2137255
    iget-object v0, p0, LX/EYQ;->a:LX/EXV;

    .line 2137256
    iget-object v1, v0, LX/EXV;->package_:Ljava/lang/Object;

    .line 2137257
    instance-of p0, v1, Ljava/lang/String;

    if-eqz p0, :cond_0

    .line 2137258
    check-cast v1, Ljava/lang/String;

    .line 2137259
    :goto_0
    move-object v0, v1

    .line 2137260
    return-object v0

    .line 2137261
    :cond_0
    check-cast v1, LX/EWc;

    .line 2137262
    invoke-virtual {v1}, LX/EWc;->e()Ljava/lang/String;

    move-result-object p0

    .line 2137263
    invoke-virtual {v1}, LX/EWc;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2137264
    iput-object p0, v0, LX/EXV;->package_:Ljava/lang/Object;

    :cond_1
    move-object v1, p0

    .line 2137265
    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/EYF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2137266
    iget-object v0, p0, LX/EYQ;->b:[LX/EYF;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
