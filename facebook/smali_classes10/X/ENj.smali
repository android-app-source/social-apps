.class public final LX/ENj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:LX/1Ps;

.field public final synthetic c:LX/CzL;

.field public final synthetic d:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;Landroid/net/Uri;LX/1Ps;LX/CzL;)V
    .locals 0

    .prologue
    .line 2113253
    iput-object p1, p0, LX/ENj;->d:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;

    iput-object p2, p0, LX/ENj;->a:Landroid/net/Uri;

    iput-object p3, p0, LX/ENj;->b:LX/1Ps;

    iput-object p4, p0, LX/ENj;->c:LX/CzL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v0, 0x1

    const v1, 0x5235acb3

    invoke-static {v11, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2113254
    new-instance v7, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2113255
    iget-object v0, p0, LX/ENj;->a:Landroid/net/Uri;

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2113256
    iget-object v0, p0, LX/ENj;->d:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->h:LX/CvY;

    iget-object v1, p0, LX/ENj;->b:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->OPEN_LINK:LX/8ch;

    iget-object v3, p0, LX/ENj;->b:LX/1Ps;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/ENj;->c:LX/CzL;

    invoke-interface {v3, v4}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, LX/ENj;->c:LX/CzL;

    .line 2113257
    iget-object v5, p0, LX/ENj;->b:LX/1Ps;

    check-cast v5, LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v8

    iget-object v5, p0, LX/ENj;->b:LX/1Ps;

    check-cast v5, LX/CxP;

    iget-object v9, p0, LX/ENj;->c:LX/CzL;

    invoke-interface {v5, v9}, LX/CxP;->b(LX/CzL;)I

    move-result v5

    sget-object v9, LX/8ch;->OPEN_LINK:LX/8ch;

    const/4 v10, 0x0

    invoke-static {v8, v5, v9, v10}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/8ch;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2113258
    iget-object v0, p0, LX/ENj;->d:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;

    iget-object v1, v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;->g:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/ENj;->b:LX/1Ps;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v1, v7, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2113259
    const v0, 0x3d4ea2a1

    invoke-static {v11, v11, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
