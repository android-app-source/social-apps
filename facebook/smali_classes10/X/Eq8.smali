.class public LX/Eq8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Eq8;


# instance fields
.field public a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/events/invite/EventInviteeToken;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0Or;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2171180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2171181
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Eq8;->c:Z

    .line 2171182
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/Eq8;->a:Ljava/util/LinkedList;

    .line 2171183
    return-void
.end method

.method public static a(LX/0QB;)LX/Eq8;
    .locals 5

    .prologue
    .line 2171184
    sget-object v0, LX/Eq8;->d:LX/Eq8;

    if-nez v0, :cond_1

    .line 2171185
    const-class v1, LX/Eq8;

    monitor-enter v1

    .line 2171186
    :try_start_0
    sget-object v0, LX/Eq8;->d:LX/Eq8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2171187
    if-eqz v2, :cond_0

    .line 2171188
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2171189
    new-instance v4, LX/Eq8;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0xc

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/Eq8;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0Or;)V

    .line 2171190
    move-object v0, v4

    .line 2171191
    sput-object v0, LX/Eq8;->d:LX/Eq8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2171192
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2171193
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2171194
    :cond_1
    sget-object v0, LX/Eq8;->d:LX/Eq8;

    return-object v0

    .line 2171195
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2171196
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
