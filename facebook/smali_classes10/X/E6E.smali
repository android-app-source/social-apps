.class public LX/E6E;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/E1f;


# direct methods
.method public constructor <init>(LX/E1f;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2079809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2079810
    iput-object p1, p0, LX/E6E;->a:LX/E1f;

    .line 2079811
    return-void
.end method

.method public static b(LX/0QB;)LX/E6E;
    .locals 2

    .prologue
    .line 2079812
    new-instance v1, LX/E6E;

    invoke-static {p0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v0

    check-cast v0, LX/E1f;

    invoke-direct {v1, v0}, LX/E6E;-><init>(LX/E1f;)V

    .line 2079813
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Landroid/content/Context;LX/3U9;LX/2kp;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14
    .param p1    # Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2079814
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2079815
    :cond_0
    :goto_0
    return-void

    .line 2079816
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 2079817
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ai()LX/0Px;

    move-result-object v12

    invoke-virtual {v12}, LX/0Px;->size()I

    move-result v13

    const/4 v3, 0x0

    move v10, v3

    :goto_1
    if-ge v10, v13, :cond_3

    invoke-virtual {v12, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/9rk;

    .line 2079818
    invoke-interface {v4}, LX/9rk;->e()LX/174;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, LX/9rk;->e()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2079819
    iget-object v3, p0, LX/E6E;->a:LX/E1f;

    invoke-interface {v4}, LX/9rk;->e()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {p6 .. p6}, LX/2kp;->t()LX/2jY;

    move-result-object v5

    invoke-virtual {v5}, LX/2jY;->f()Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {p6 .. p6}, LX/2kp;->t()LX/2jY;

    move-result-object v5

    invoke-virtual {v5}, LX/2jY;->v()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v5, p4

    move-object/from16 v9, p7

    invoke-virtual/range {v3 .. v9}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    .line 2079820
    if-eqz v3, :cond_2

    .line 2079821
    new-instance v5, LX/E6A;

    invoke-direct {v5, v4, v3}, LX/E6A;-><init>(LX/9rk;LX/Cfl;)V

    invoke-virtual {v11, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2079822
    :cond_2
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    goto :goto_1

    .line 2079823
    :cond_3
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 2079824
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2079825
    new-instance v9, LX/3Af;

    move-object/from16 v0, p4

    invoke-direct {v9, v0}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2079826
    new-instance v3, LX/E6D;

    move-object/from16 v4, p4

    move-object/from16 v6, p2

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v3 .. v8}, LX/E6D;-><init>(Landroid/content/Context;LX/0Px;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v3}, LX/3Af;->a(LX/1OM;)V

    .line 2079827
    invoke-virtual {v9}, LX/3Af;->show()V

    .line 2079828
    invoke-interface/range {p5 .. p5}, LX/3U9;->n()LX/2ja;

    move-result-object v3

    sget-object v4, LX/Cfc;->OPEN_VERTICAL_ACTION_SHEET_TAP:LX/Cfc;

    move-object/from16 v0, p7

    move-object/from16 v1, p8

    move-object/from16 v2, p3

    invoke-virtual {v3, v0, v1, v2, v4}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    goto/16 :goto_0
.end method
