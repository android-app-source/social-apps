.class public final LX/Cj3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Ve",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CH5;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field public final d:J

.field private e:Lcom/facebook/graphql/executor/GraphQLResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(LX/CH5;Ljava/lang/String;JLX/0Ve;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1929027
    iput-object p1, p0, LX/Cj3;->a:LX/CH5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929028
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LX/Cj3;->b:Ljava/util/List;

    .line 1929029
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cj3;->f:Z

    .line 1929030
    iput-object p2, p0, LX/Cj3;->c:Ljava/lang/String;

    .line 1929031
    iput-wide p3, p0, LX/Cj3;->d:J

    .line 1929032
    iget-object v0, p0, LX/Cj3;->b:Ljava/util/List;

    invoke-interface {v0, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1929033
    return-void
.end method


# virtual methods
.method public final a(LX/0Ve;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1929018
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/0Vf;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1929019
    :cond_0
    :goto_0
    return-void

    .line 1929020
    :cond_1
    monitor-enter p0

    .line 1929021
    :try_start_0
    iget-object v0, p0, LX/Cj3;->e:Lcom/facebook/graphql/executor/GraphQLResult;

    if-eqz v0, :cond_2

    .line 1929022
    iget-object v0, p0, LX/Cj3;->e:Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-interface {p1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1929023
    :goto_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1929024
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/Cj3;->g:Ljava/lang/Throwable;

    if-eqz v0, :cond_3

    .line 1929025
    iget-object v0, p0, LX/Cj3;->g:Ljava/lang/Throwable;

    invoke-interface {p1, v0}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1929026
    :cond_3
    iget-object v0, p0, LX/Cj3;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized dispose()V
    .locals 1

    .prologue
    .line 1929015
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/Cj3;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929016
    monitor-exit p0

    return-void

    .line 1929017
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 1929034
    iget-boolean v0, p0, LX/Cj3;->f:Z

    return v0
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1929004
    monitor-enter p0

    .line 1929005
    :try_start_0
    iput-object p1, p0, LX/Cj3;->g:Ljava/lang/Throwable;

    .line 1929006
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/Cj3;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1929007
    iget-object v1, p0, LX/Cj3;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1929008
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929009
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ve;

    .line 1929010
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1929011
    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1929012
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1929013
    :cond_1
    iget-object v0, p0, LX/Cj3;->a:LX/CH5;

    iget-object v1, p0, LX/Cj3;->c:Ljava/lang/String;

    invoke-static {v0, v1}, LX/CH5;->a$redex0(LX/CH5;Ljava/lang/String;)V

    .line 1929014
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1928992
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1928993
    monitor-enter p0

    .line 1928994
    :try_start_0
    iput-object p1, p0, LX/Cj3;->e:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1928995
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/Cj3;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1928996
    iget-object v1, p0, LX/Cj3;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1928997
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1928998
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ve;

    .line 1928999
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1929000
    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0

    .line 1929001
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1929002
    :cond_1
    iget-object v0, p0, LX/Cj3;->a:LX/CH5;

    iget-object v1, p0, LX/Cj3;->c:Ljava/lang/String;

    invoke-static {v0, v1}, LX/CH5;->a$redex0(LX/CH5;Ljava/lang/String;)V

    .line 1929003
    return-void
.end method
