.class public final enum LX/Cwd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cwd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cwd;

.field private static final DEFAULT_TABS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Cwd;",
            ">;"
        }
    .end annotation
.end field

.field private static final FLIPPED_TABS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Cwd;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum GLOBAL:LX/Cwd;

.field public static final enum SCOPED:LX/Cwd;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1950926
    new-instance v0, LX/Cwd;

    const-string v1, "GLOBAL"

    invoke-direct {v0, v1, v2}, LX/Cwd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwd;->GLOBAL:LX/Cwd;

    .line 1950927
    new-instance v0, LX/Cwd;

    const-string v1, "SCOPED"

    invoke-direct {v0, v1, v3}, LX/Cwd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cwd;->SCOPED:LX/Cwd;

    .line 1950928
    const/4 v0, 0x2

    new-array v0, v0, [LX/Cwd;

    sget-object v1, LX/Cwd;->GLOBAL:LX/Cwd;

    aput-object v1, v0, v2

    sget-object v1, LX/Cwd;->SCOPED:LX/Cwd;

    aput-object v1, v0, v3

    sput-object v0, LX/Cwd;->$VALUES:[LX/Cwd;

    .line 1950929
    sget-object v0, LX/Cwd;->GLOBAL:LX/Cwd;

    sget-object v1, LX/Cwd;->SCOPED:LX/Cwd;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Cwd;->DEFAULT_TABS:LX/0Px;

    .line 1950930
    sget-object v0, LX/Cwd;->SCOPED:LX/Cwd;

    sget-object v1, LX/Cwd;->GLOBAL:LX/Cwd;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Cwd;->FLIPPED_TABS:LX/0Px;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1950925
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getTabs(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            ")",
            "LX/0Px",
            "<",
            "LX/Cwd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1950917
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1950918
    sget-object v1, LX/103;->VIDEO:LX/103;

    if-eq v0, v1, :cond_0

    .line 1950919
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1950920
    sget-object v1, LX/103;->MARKETPLACE:LX/103;

    if-eq v0, v1, :cond_0

    .line 1950921
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1950922
    sget-object v1, LX/103;->COMMERCE:LX/103;

    if-ne v0, v1, :cond_1

    .line 1950923
    :cond_0
    sget-object v0, LX/Cwd;->FLIPPED_TABS:LX/0Px;

    .line 1950924
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/Cwd;->DEFAULT_TABS:LX/0Px;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cwd;
    .locals 1

    .prologue
    .line 1950916
    const-class v0, LX/Cwd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cwd;

    return-object v0
.end method

.method public static values()[LX/Cwd;
    .locals 1

    .prologue
    .line 1950914
    sget-object v0, LX/Cwd;->$VALUES:[LX/Cwd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cwd;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950915
    invoke-virtual {p0}, LX/Cwd;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
