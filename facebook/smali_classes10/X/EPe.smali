.class public LX/EPe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1Aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Aa",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7zZ;LX/1AZ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2117283
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2, p1, v0, v1}, LX/1AZ;->a(LX/1AX;ZZ)LX/1Aa;

    move-result-object v0

    iput-object v0, p0, LX/EPe;->a:LX/1Aa;

    .line 2117284
    return-void
.end method

.method public static a(LX/0QB;)LX/EPe;
    .locals 5

    .prologue
    .line 2117285
    const-class v1, LX/EPe;

    monitor-enter v1

    .line 2117286
    :try_start_0
    sget-object v0, LX/EPe;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117287
    sput-object v2, LX/EPe;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117288
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117289
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117290
    new-instance p0, LX/EPe;

    invoke-static {v0}, LX/7zZ;->b(LX/0QB;)LX/7zZ;

    move-result-object v3

    check-cast v3, LX/7zZ;

    const-class v4, LX/1AZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1AZ;

    invoke-direct {p0, v3, v4}, LX/EPe;-><init>(LX/7zZ;LX/1AZ;)V

    .line 2117291
    move-object v0, p0

    .line 2117292
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117293
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EPe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117294
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117295
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
