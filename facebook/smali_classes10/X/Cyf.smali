.class public LX/Cyf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/7B0;

.field private final c:LX/CvK;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private final e:Ljava/util/concurrent/Executor;

.field private final f:LX/Cve;

.field private final g:LX/CyY;

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CyS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cvk;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/7B0;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;LX/Cve;LX/CyY;LX/CvK;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1953442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1953443
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953444
    iput-object v0, p0, LX/Cyf;->h:LX/0Ot;

    .line 1953445
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953446
    iput-object v0, p0, LX/Cyf;->i:LX/0Ot;

    .line 1953447
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953448
    iput-object v0, p0, LX/Cyf;->j:LX/0Ot;

    .line 1953449
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953450
    iput-object v0, p0, LX/Cyf;->k:LX/0Ot;

    .line 1953451
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1953452
    iput-object v0, p0, LX/Cyf;->l:LX/0Ot;

    .line 1953453
    iput-object p1, p0, LX/Cyf;->a:LX/0tX;

    .line 1953454
    iput-object p2, p0, LX/Cyf;->b:LX/7B0;

    .line 1953455
    iput-object p3, p0, LX/Cyf;->d:Ljava/util/concurrent/ExecutorService;

    .line 1953456
    iput-object p4, p0, LX/Cyf;->e:Ljava/util/concurrent/Executor;

    .line 1953457
    iput-object p5, p0, LX/Cyf;->f:LX/Cve;

    .line 1953458
    iput-object p6, p0, LX/Cyf;->g:LX/CyY;

    .line 1953459
    iput-object p7, p0, LX/Cyf;->c:LX/CvK;

    .line 1953460
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1953466
    const-string v0, "content"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1953467
    iget-object v0, p0, LX/Cyf;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v2, LX/100;->aw:I

    invoke-direct {p0}, LX/Cyf;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v1, 0xa

    :cond_0
    invoke-interface {v0, v2, v1}, LX/0ad;->a(II)I

    move-result v0

    mul-int/lit8 v1, v0, 0x3c

    .line 1953468
    :cond_1
    :goto_0
    return v1

    .line 1953469
    :cond_2
    const-string v0, "news_v2"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1953470
    iget-object v0, p0, LX/Cyf;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v2, LX/100;->aF:I

    invoke-direct {p0}, LX/Cyf;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v1, 0x3

    :cond_3
    invoke-interface {v0, v2, v1}, LX/0ad;->a(II)I

    move-result v0

    mul-int/lit8 v1, v0, 0x3c

    goto :goto_0
.end method

.method private static a(LX/Cyf;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;LX/0Px;Ljava/lang/Integer;ZZLX/0Px;)LX/0gW;
    .locals 13
    .param p1    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "Ljava/lang/Integer;",
            "ZZ",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0gW",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1953461
    invoke-static {}, LX/8ee;->a()LX/8ed;

    move-result-object v2

    .line 1953462
    iget-object v0, p0, LX/Cyf;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/100;->aD:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 1953463
    const/16 p6, 0x1

    move/from16 v9, p6

    .line 1953464
    :goto_0
    iget-object v0, p0, LX/Cyf;->g:LX/CyY;

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v1

    iget-object v10, v1, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->q()LX/8ci;

    move-result-object v12

    move-object v1, p1

    move-object v4, p2

    move-object/from16 v5, p7

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    invoke-virtual/range {v0 .. v12}, LX/CyY;->a(LX/CwB;LX/0gW;ZLjava/lang/String;LX/0Px;LX/0Px;Ljava/lang/Integer;ZZLjava/lang/String;Ljava/lang/String;LX/8ci;)V

    .line 1953465
    return-object v2

    :cond_0
    move/from16 v9, p6

    goto :goto_0
.end method

.method private static a(LX/0gW;LX/0zO;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
            ">;",
            "LX/0zO",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1953438
    if-gtz p2, :cond_0

    .line 1953439
    :goto_0
    return-void

    .line 1953440
    :cond_0
    const-string v0, "end_cursor"

    const-string v1, "end_cursor"

    sget-object v2, LX/4Zz;->FIRST:LX/4Zz;

    sget-object v3, LX/4a0;->SKIP:LX/4a0;

    invoke-virtual {p1, v1, v2, v3}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0gW;->a(Ljava/lang/String;LX/4a1;)LX/0gW;

    goto :goto_0
.end method

.method public static a()[I
    .locals 1

    .prologue
    .line 1953441
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method public static c(LX/Cyf;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0rl;LX/0Px;Ljava/util/List;LX/0Px;)LX/0zi;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "LX/0rl",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
            ">;>;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0zi;"
        }
    .end annotation

    .prologue
    .line 1953408
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    .line 1953409
    invoke-static {v2}, LX/CyW;->fromDisplayStyle(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/CyW;

    move-result-object v2

    .line 1953410
    new-instance v13, LX/0v6;

    invoke-virtual {v2}, LX/CyW;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v13, v2}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 1953411
    iget-object v2, p0, LX/Cyf;->c:LX/CvK;

    invoke-virtual {v2, v13}, LX/CvK;->a(LX/0v6;)V

    .line 1953412
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v5, p3

    move-object/from16 v9, p5

    invoke-static/range {v2 .. v9}, LX/Cyf;->a(LX/Cyf;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;LX/0Px;Ljava/lang/Integer;ZZLX/0Px;)LX/0gW;

    move-result-object v2

    .line 1953413
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 1953414
    invoke-virtual {p0}, LX/Cyf;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1953415
    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jA_()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, LX/Cyf;->a(Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    new-instance v4, LX/Cye;

    invoke-direct {v4}, LX/Cye;-><init>()V

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zT;)LX/0zO;

    .line 1953416
    :cond_0
    invoke-virtual {v13, v2}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v3

    .line 1953417
    invoke-virtual {v2}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1953418
    const/4 v4, 0x0

    .line 1953419
    invoke-static {}, LX/Cyf;->a()[I

    move-result-object v14

    .line 1953420
    const/4 v2, 0x0

    move v10, v2

    move-object v11, v3

    move-object v12, v4

    :goto_0
    array-length v2, v14

    if-ge v10, v2, :cond_3

    .line 1953421
    aget v2, v14, v10

    .line 1953422
    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    if-nez v10, :cond_2

    const/4 v8, 0x1

    :goto_1
    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v5, p3

    move-object/from16 v9, p5

    invoke-static/range {v2 .. v9}, LX/Cyf;->a(LX/Cyf;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;LX/0Px;Ljava/lang/Integer;ZZLX/0Px;)LX/0gW;

    move-result-object v2

    .line 1953423
    invoke-static {v2, v12, v10}, LX/Cyf;->a(LX/0gW;LX/0zO;I)V

    .line 1953424
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 1953425
    invoke-virtual {p0}, LX/Cyf;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1953426
    sget-object v2, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jA_()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, LX/Cyf;->a(Ljava/lang/String;)I

    move-result v3

    int-to-long v6, v3

    invoke-virtual {v2, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v2

    new-instance v3, LX/Cye;

    invoke-direct {v3}, LX/Cye;-><init>()V

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zT;)LX/0zO;

    .line 1953427
    :cond_1
    invoke-virtual {v13, v4}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v2

    .line 1953428
    invoke-virtual {v4}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1953429
    invoke-virtual {v11, v2}, LX/0zX;->a(LX/0zX;)LX/0zX;

    move-result-object v3

    .line 1953430
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    move-object v11, v3

    move-object v12, v4

    goto :goto_0

    .line 1953431
    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    .line 1953432
    :cond_3
    iget-object v2, p0, LX/Cyf;->e:Ljava/util/concurrent/Executor;

    invoke-virtual {v11, v2}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, LX/0zX;->a(LX/0rl;)LX/0zi;

    move-result-object v2

    .line 1953433
    iget-object v3, p0, LX/Cyf;->f:LX/Cve;

    const/4 v4, 0x1

    const-string v5, "2"

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v3, v0, v4, v1, v5}, LX/Cve;->a(LX/CwB;ZLjava/lang/Iterable;Ljava/lang/String;)V

    .line 1953434
    invoke-virtual {p0}, LX/Cyf;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1953435
    iget-object v3, p0, LX/Cyf;->a:LX/0tX;

    iget-object v4, p0, LX/Cyf;->d:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v3, v13, v4}, LX/0tX;->a(LX/0v6;Ljava/util/concurrent/ExecutorService;)V

    .line 1953436
    :goto_2
    return-object v2

    .line 1953437
    :cond_4
    iget-object v3, p0, LX/Cyf;->a:LX/0tX;

    iget-object v4, p0, LX/Cyf;->d:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v3, v13, v4}, LX/0tX;->b(LX/0v6;Ljava/util/concurrent/ExecutorService;)V

    goto :goto_2
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 1953407
    iget-object v0, p0, LX/Cyf;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/2SU;->l:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;LX/0Px;Ljava/lang/String;LX/Cyd;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "Ljava/lang/String;",
            "LX/Cyd;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1953401
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, v5

    move-object v7, v4

    invoke-static/range {v0 .. v7}, LX/Cyf;->a(LX/Cyf;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;LX/0Px;Ljava/lang/Integer;ZZLX/0Px;)LX/0gW;

    move-result-object v0

    .line 1953402
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 1953403
    iget-object v1, p0, LX/Cyf;->b:LX/7B0;

    sget-object v2, LX/7Az;->SERP:LX/7Az;

    invoke-virtual {v1, v2, v0}, LX/7B0;->a(LX/7Az;LX/0zO;)V

    .line 1953404
    iget-object v1, p0, LX/Cyf;->c:LX/CvK;

    invoke-virtual {v1, v0}, LX/CvK;->a(LX/0zO;)V

    .line 1953405
    iget-object v1, p0, LX/Cyf;->f:LX/Cve;

    sget-object v2, LX/Cyd;->LOAD:LX/Cyd;

    if-ne p5, v2, :cond_0

    const/4 v5, 0x1

    :cond_0
    const-string v2, "2"

    invoke-virtual {v1, p1, v5, p4, v2}, LX/Cve;->a(LX/CwB;ZLjava/lang/String;Ljava/lang/String;)V

    .line 1953406
    iget-object v1, p0, LX/Cyf;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0rl;LX/0Px;Ljava/util/List;LX/0Px;)Ljava/util/concurrent/Future;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "LX/0rl",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
            ">;>;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/concurrent/Future",
            "<",
            "LX/0zi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1953399
    iget-object v7, p0, LX/Cyf;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v0, LX/Cyb;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/Cyb;-><init>(LX/Cyf;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0rl;LX/0Px;Ljava/util/List;LX/0Px;)V

    const v1, -0x271f2c1f

    invoke-static {v7, v0, v1}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 1953400
    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1953398
    iget-object v0, p0, LX/Cyf;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/100;->aI:S

    invoke-direct {p0}, LX/Cyf;->d()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 1953397
    iget-object v0, p0, LX/Cyf;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/100;->aC:S

    invoke-direct {p0}, LX/Cyf;->d()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
