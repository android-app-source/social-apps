.class public final LX/CrF;
.super LX/Cqf;
.source ""


# instance fields
.field public g:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V
    .locals 2

    .prologue
    .line 1940744
    invoke-direct/range {p0 .. p6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    .line 1940745
    invoke-interface {p2}, LX/Ctg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/CrF;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v0

    check-cast v0, LX/Cju;

    iput-object v0, p0, LX/CrF;->g:LX/Cju;

    .line 1940746
    return-void
.end method


# virtual methods
.method public final g()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1940722
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1940723
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 1940724
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 1940725
    :goto_0
    invoke-virtual {p0}, LX/Cqf;->k()F

    move-result v3

    .line 1940726
    invoke-virtual {p0}, LX/Cqf;->l()F

    move-result v4

    .line 1940727
    iget-object v5, p0, LX/Cqf;->b:LX/Cqd;

    sget-object v6, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    if-ne v5, v6, :cond_1

    .line 1940728
    cmpl-float v3, v4, v3

    if-ltz v3, :cond_0

    .line 1940729
    int-to-float v0, v2

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v3, v1

    .line 1940730
    :goto_1
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x0

    invoke-direct {v4, v3, v1, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1940731
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/CrW;

    invoke-direct {v1, v4}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v1}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940732
    return-void

    .line 1940733
    :cond_0
    int-to-float v3, v0

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 1940734
    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    move v7, v3

    move v3, v2

    move v2, v7

    goto :goto_1

    :cond_1
    move v3, v1

    goto :goto_1

    :cond_2
    move v0, v1

    move v2, v1

    goto :goto_0
.end method

.method public final h()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1940735
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cqf;->a(Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 1940736
    iget-object v0, p0, LX/Cqf;->c:LX/Cqe;

    sget-object v2, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    if-ne v0, v2, :cond_1

    .line 1940737
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {v1}, LX/CrW;->e()I

    move-result v2

    invoke-virtual {v1}, LX/CrW;->f()I

    move-result v3

    invoke-direct {v0, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1940738
    :goto_0
    if-eqz v0, :cond_0

    .line 1940739
    invoke-static {v1, v0}, LX/Cqf;->a(LX/CrW;Landroid/graphics/Rect;)V

    .line 1940740
    :cond_0
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v1

    new-instance v2, LX/CrW;

    invoke-direct {v2, v0}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v1, v2}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940741
    invoke-virtual {p0}, LX/Cqf;->n()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    new-instance v1, LX/CrP;

    iget-object v2, p0, LX/Cqf;->a:LX/Cqt;

    invoke-virtual {v2}, LX/Cqt;->getDegree()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v1, v2}, LX/CrP;-><init>(F)V

    invoke-virtual {p0, v0, v1}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 1940742
    return-void

    .line 1940743
    :cond_1
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method
