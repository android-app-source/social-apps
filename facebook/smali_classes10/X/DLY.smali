.class public final LX/DLY;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DLb;


# direct methods
.method public constructor <init>(LX/DLb;)V
    .locals 0

    .prologue
    .line 1988334
    iput-object p1, p0, LX/DLY;->a:LX/DLb;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1988335
    iget-object v0, p0, LX/DLY;->a:LX/DLb;

    iget-object v0, v0, LX/DLb;->f:LX/DLL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/DLL;->a(Z)V

    .line 1988336
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1988337
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1988338
    if-eqz p1, :cond_0

    .line 1988339
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1988340
    if-eqz v0, :cond_0

    .line 1988341
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1988342
    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    .line 1988343
    :goto_1
    return-void

    .line 1988344
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1988345
    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1988346
    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1988347
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1988348
    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    move-result-object v3

    .line 1988349
    invoke-virtual {v3}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1988350
    iget-object v5, p0, LX/DLY;->a:LX/DLb;

    invoke-virtual {v4, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 1988351
    iput-object v6, v5, LX/DLb;->g:Ljava/lang/String;

    .line 1988352
    iget-object v5, p0, LX/DLY;->a:LX/DLb;

    iget-object v5, v5, LX/DLb;->f:LX/DLL;

    invoke-virtual {v5, v1}, LX/DLL;->a(Z)V

    .line 1988353
    iget-object v5, p0, LX/DLY;->a:LX/DLb;

    invoke-virtual {v4, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/DLY;->a:LX/DLb;

    iget-object v0, v0, LX/DLb;->g:Ljava/lang/String;

    if-nez v0, :cond_5

    :cond_4
    move v0, v2

    .line 1988354
    :goto_2
    iput-boolean v0, v5, LX/DLb;->h:Z

    .line 1988355
    iget-object v0, p0, LX/DLY;->a:LX/DLb;

    iget-object v0, v0, LX/DLb;->e:LX/0Px;

    if-nez v0, :cond_6

    .line 1988356
    iget-object v0, p0, LX/DLY;->a:LX/DLb;

    invoke-virtual {v3}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;->a()LX/0Px;

    move-result-object v1

    .line 1988357
    iput-object v1, v0, LX/DLb;->e:LX/0Px;

    .line 1988358
    :goto_3
    iget-object v0, p0, LX/DLY;->a:LX/DLb;

    iget-object v0, v0, LX/DLb;->f:LX/DLL;

    iget-object v1, p0, LX/DLY;->a:LX/DLb;

    iget-object v1, v1, LX/DLb;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/DLL;->a(LX/0Px;)V

    .line 1988359
    iget-object v0, p0, LX/DLY;->a:LX/DLb;

    iget-object v0, v0, LX/DLb;->f:LX/DLL;

    iget-object v1, p0, LX/DLY;->a:LX/DLb;

    iget-object v1, v1, LX/DLb;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/DLL;->b(Z)V

    goto :goto_1

    .line 1988360
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    move v0, v1

    .line 1988361
    goto :goto_2

    .line 1988362
    :cond_6
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1988363
    iget-object v1, p0, LX/DLY;->a:LX/DLb;

    iget-object v2, p0, LX/DLY;->a:LX/DLb;

    iget-object v2, v2, LX/DLb;->e:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v3}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1988364
    iput-object v0, v1, LX/DLb;->e:LX/0Px;

    .line 1988365
    goto :goto_3
.end method
