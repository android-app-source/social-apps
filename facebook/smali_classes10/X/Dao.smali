.class public final LX/Dao;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;)V
    .locals 0

    .prologue
    .line 2016316
    iput-object p1, p0, LX/Dao;->a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2016317
    iget-object v0, p0, LX/Dao;->a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    iget-object v0, v0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2016318
    iget-object v0, p0, LX/Dao;->a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    iget-object v0, v0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->b:LX/17W;

    iget-object v1, p0, LX/Dao;->a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-virtual {v1}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Dao;->a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    iget-object v2, v2, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2016319
    :cond_0
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2016320
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2016321
    iget-object v0, p0, LX/Dao;->a:Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    iget-object v0, v0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a05f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2016322
    return-void
.end method
