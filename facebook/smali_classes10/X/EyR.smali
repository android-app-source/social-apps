.class public final enum LX/EyR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EyR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EyR;

.field public static final enum FIG_PRIMARY:LX/EyR;

.field public static final enum FIG_SECONDARY:LX/EyR;

.field public static final enum PRIMARY:LX/EyR;

.field public static final enum SECONDARY:LX/EyR;


# instance fields
.field public final backgroundRes:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final textAppearanceRes:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2186077
    new-instance v0, LX/EyR;

    const-string v1, "PRIMARY"

    const v2, 0x7f0207a0

    const v3, 0x7f0e0207

    invoke-direct {v0, v1, v4, v2, v3}, LX/EyR;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EyR;->PRIMARY:LX/EyR;

    .line 2186078
    new-instance v0, LX/EyR;

    const-string v1, "SECONDARY"

    const v2, 0x7f0201fe

    const v3, 0x7f0e020b

    invoke-direct {v0, v1, v5, v2, v3}, LX/EyR;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EyR;->SECONDARY:LX/EyR;

    .line 2186079
    new-instance v0, LX/EyR;

    const-string v1, "FIG_PRIMARY"

    const v2, 0x7f020b08

    const v3, 0x7f0e011f

    invoke-direct {v0, v1, v6, v2, v3}, LX/EyR;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EyR;->FIG_PRIMARY:LX/EyR;

    .line 2186080
    new-instance v0, LX/EyR;

    const-string v1, "FIG_SECONDARY"

    const v2, 0x7f020b0b

    const v3, 0x7f0e0123

    invoke-direct {v0, v1, v7, v2, v3}, LX/EyR;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EyR;->FIG_SECONDARY:LX/EyR;

    .line 2186081
    const/4 v0, 0x4

    new-array v0, v0, [LX/EyR;

    sget-object v1, LX/EyR;->PRIMARY:LX/EyR;

    aput-object v1, v0, v4

    sget-object v1, LX/EyR;->SECONDARY:LX/EyR;

    aput-object v1, v0, v5

    sget-object v1, LX/EyR;->FIG_PRIMARY:LX/EyR;

    aput-object v1, v0, v6

    sget-object v1, LX/EyR;->FIG_SECONDARY:LX/EyR;

    aput-object v1, v0, v7

    sput-object v0, LX/EyR;->$VALUES:[LX/EyR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2186082
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2186083
    iput p3, p0, LX/EyR;->backgroundRes:I

    .line 2186084
    iput p4, p0, LX/EyR;->textAppearanceRes:I

    .line 2186085
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EyR;
    .locals 1

    .prologue
    .line 2186086
    const-class v0, LX/EyR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EyR;

    return-object v0
.end method

.method public static values()[LX/EyR;
    .locals 1

    .prologue
    .line 2186087
    sget-object v0, LX/EyR;->$VALUES:[LX/EyR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EyR;

    return-object v0
.end method
