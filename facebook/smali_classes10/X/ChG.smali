.class public abstract LX/ChG;
.super LX/Ch6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Ch6",
        "<",
        "LX/ChF;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1927833
    invoke-direct {p0}, LX/Ch6;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/ChF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1927834
    const-class v0, LX/ChF;

    return-object v0
.end method

.method public abstract a(Ljava/lang/String;LX/5tj;Lcom/facebook/graphql/model/GraphQLStory;)V
    .param p2    # LX/5tj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1927828
    check-cast p1, LX/ChF;

    .line 1927829
    iget v0, p1, LX/ChF;->d:I

    packed-switch v0, :pswitch_data_0

    .line 1927830
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No event type matches: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, LX/ChF;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1927831
    :pswitch_0
    iget-object v1, p1, LX/ChF;->a:Ljava/lang/String;

    iget-object v2, p1, LX/ChF;->b:LX/5tj;

    iget-object v0, p1, LX/ChF;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, v1, v2, v0}, LX/ChG;->a(Ljava/lang/String;LX/5tj;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1927832
    :pswitch_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
