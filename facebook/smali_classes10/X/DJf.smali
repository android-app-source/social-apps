.class public final enum LX/DJf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DJf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DJf;

.field public static final enum BLACK:LX/DJf;

.field public static final enum BLUE:LX/DJf;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1985676
    new-instance v0, LX/DJf;

    const-string v1, "BLUE"

    invoke-direct {v0, v1, v2}, LX/DJf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DJf;->BLUE:LX/DJf;

    .line 1985677
    new-instance v0, LX/DJf;

    const-string v1, "BLACK"

    invoke-direct {v0, v1, v3}, LX/DJf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DJf;->BLACK:LX/DJf;

    .line 1985678
    const/4 v0, 0x2

    new-array v0, v0, [LX/DJf;

    sget-object v1, LX/DJf;->BLUE:LX/DJf;

    aput-object v1, v0, v2

    sget-object v1, LX/DJf;->BLACK:LX/DJf;

    aput-object v1, v0, v3

    sput-object v0, LX/DJf;->$VALUES:[LX/DJf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1985680
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DJf;
    .locals 1

    .prologue
    .line 1985681
    const-class v0, LX/DJf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DJf;

    return-object v0
.end method

.method public static values()[LX/DJf;
    .locals 1

    .prologue
    .line 1985679
    sget-object v0, LX/DJf;->$VALUES:[LX/DJf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DJf;

    return-object v0
.end method
