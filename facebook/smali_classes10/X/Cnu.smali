.class public LX/Cnu;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/MapBlockView;",
        "Lcom/facebook/richdocument/model/data/MapBlockData;",
        ">;",
        "Lcom/facebook/richdocument/presenter/BlockPresenter$OnRecycledListener;"
    }
.end annotation


# instance fields
.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CpI;)V
    .locals 2

    .prologue
    .line 1934566
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934567
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, LX/Cnu;

    const/16 v0, 0x31dc

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object p1, p0, LX/Cnu;->d:LX/0Ot;

    .line 1934568
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 7

    .prologue
    .line 1934569
    check-cast p1, LX/CmS;

    .line 1934570
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934571
    check-cast v0, LX/CpI;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1934572
    iget-object v0, p1, LX/CmS;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    move-object v0, v0

    .line 1934573
    sget-object v1, LX/Cnt;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1934574
    const/4 v1, 0x1

    :goto_0
    move v2, v1

    .line 1934575
    iget-object v0, p1, LX/CmS;->c:LX/0Px;

    move-object v3, v0

    .line 1934576
    iget v0, p1, LX/CmS;->a:I

    move v0, v0

    .line 1934577
    if-gtz v0, :cond_0

    .line 1934578
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b126d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    .line 1934579
    :goto_1
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934580
    check-cast v0, LX/CpI;

    const/4 v6, 0x0

    .line 1934581
    sget-object v4, LX/Cqw;->a:LX/Cqw;

    invoke-virtual {v0, v4}, LX/Cos;->a(LX/Cqw;)V

    .line 1934582
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v5, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1934583
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v4, v6, v5, v1, v6}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a(Ljava/lang/String;IILjava/lang/String;)V

    .line 1934584
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->setVisibility(I)V

    .line 1934585
    const-class v4, LX/CuA;

    invoke-virtual {v0, v4}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v4

    check-cast v4, LX/CuA;

    .line 1934586
    invoke-virtual {v4, v2, v3}, LX/CuA;->a(ILjava/util/List;)V

    .line 1934587
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v0

    .line 1934588
    iget-object v0, p0, LX/Cnu;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 1934589
    iget-object v2, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v2

    .line 1934590
    invoke-interface {p1}, LX/Clr;->o()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, p1, v0, v2}, LX/Co1;->a(LX/CnG;LX/Clq;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;Landroid/os/Bundle;)V

    .line 1934591
    return-void

    :cond_0
    move v1, v0

    goto :goto_1

    .line 1934592
    :pswitch_0
    const/4 v1, 0x4

    goto :goto_0

    .line 1934593
    :pswitch_1
    const/4 v1, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
