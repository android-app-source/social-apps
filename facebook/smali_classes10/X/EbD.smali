.class public LX/EbD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Eb9;


# instance fields
.field public final a:I

.field public final b:LX/Eat;

.field public final c:I

.field private final d:I

.field public final e:[B

.field private final f:[B


# direct methods
.method public constructor <init>(ILjavax/crypto/spec/SecretKeySpec;LX/Eat;II[BLX/Eae;LX/Eae;)V
    .locals 7

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2143105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2143106
    new-array v0, v5, [B

    invoke-static {p1, v3}, LX/Eco;->a(II)B

    move-result v1

    aput-byte v1, v0, v4

    .line 2143107
    invoke-static {}, LX/EbT;->u()LX/EbT;

    move-result-object v1

    invoke-virtual {p3}, LX/Eat;->a()[B

    move-result-object v2

    invoke-static {v2}, LX/EWc;->a([B)LX/EWc;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EbT;->a(LX/EWc;)LX/EbT;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/EbT;->a(I)LX/EbT;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/EbT;->b(I)LX/EbT;

    move-result-object v1

    invoke-static {p6}, LX/EWc;->a([B)LX/EWc;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EbT;->b(LX/EWc;)LX/EbT;

    move-result-object v1

    invoke-virtual {v1}, LX/EbT;->l()LX/EbU;

    move-result-object v1

    invoke-virtual {v1}, LX/EWX;->jZ_()[B

    move-result-object v1

    .line 2143108
    new-array v2, v6, [[B

    aput-object v0, v2, v4

    aput-object v1, v2, v5

    invoke-static {v2}, LX/Eco;->a([[B)[B

    move-result-object v2

    invoke-static {p1, p7, p8, p2, v2}, LX/EbD;->a(ILX/Eae;LX/Eae;Ljavax/crypto/spec/SecretKeySpec;[B)[B

    move-result-object v2

    .line 2143109
    new-array v3, v3, [[B

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    aput-object v2, v3, v6

    invoke-static {v3}, LX/Eco;->a([[B)[B

    move-result-object v0

    iput-object v0, p0, LX/EbD;->f:[B

    .line 2143110
    iput-object p3, p0, LX/EbD;->b:LX/Eat;

    .line 2143111
    iput p4, p0, LX/EbD;->c:I

    .line 2143112
    iput p5, p0, LX/EbD;->d:I

    .line 2143113
    iput-object p6, p0, LX/EbD;->e:[B

    .line 2143114
    iput p1, p0, LX/EbD;->a:I

    .line 2143115
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2143116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2143117
    const/4 v0, 0x1

    :try_start_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v1, v1, -0x8

    const/16 v2, 0x8

    invoke-static {p1, v0, v1, v2}, LX/Eco;->a([BIII)[[B

    move-result-object v0

    .line 2143118
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    .line 2143119
    const/4 v2, 0x1

    aget-object v0, v0, v2

    .line 2143120
    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v2

    if-gt v2, v3, :cond_0

    .line 2143121
    new-instance v0, LX/Eak;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Legacy message: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eak;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2143122
    :catch_0
    move-exception v0

    .line 2143123
    :goto_0
    new-instance v1, LX/Eai;

    invoke-direct {v1, v0}, LX/Eai;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2143124
    :cond_0
    :try_start_1
    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v2

    const/4 v3, 0x3

    if-le v2, v3, :cond_1

    .line 2143125
    new-instance v0, LX/Eai;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2143126
    :catch_1
    move-exception v0

    goto :goto_0

    .line 2143127
    :cond_1
    sget-object v2, LX/EbU;->a:LX/EWZ;

    invoke-virtual {v2, v0}, LX/EWZ;->a([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EbU;

    move-object v0, v2

    .line 2143128
    invoke-virtual {v0}, LX/EbU;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, LX/EbU;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, LX/EbU;->k()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2143129
    :cond_2
    new-instance v0, LX/Eai;

    const-string v1, "Incomplete message."

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2143130
    :catch_2
    move-exception v0

    goto :goto_0

    .line 2143131
    :cond_3
    iput-object p1, p0, LX/EbD;->f:[B

    .line 2143132
    iget-object v2, v0, LX/EbU;->ratchetKey_:LX/EWc;

    move-object v2, v2

    .line 2143133
    invoke-virtual {v2}, LX/EWc;->d()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v2

    iput-object v2, p0, LX/EbD;->b:LX/Eat;

    .line 2143134
    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v1

    iput v1, p0, LX/EbD;->a:I

    .line 2143135
    iget v1, v0, LX/EbU;->counter_:I

    move v1, v1

    .line 2143136
    iput v1, p0, LX/EbD;->c:I

    .line 2143137
    iget v1, v0, LX/EbU;->previousCounter_:I

    move v1, v1

    .line 2143138
    iput v1, p0, LX/EbD;->d:I

    .line 2143139
    iget-object v1, v0, LX/EbU;->ciphertext_:LX/EWc;

    move-object v0, v1

    .line 2143140
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    iput-object v0, p0, LX/EbD;->e:[B
    :try_end_1
    .catch LX/EYr; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/Eag; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2143141
    return-void
.end method

.method private static a(ILX/Eae;LX/Eae;Ljavax/crypto/spec/SecretKeySpec;[B)[B
    .locals 2

    .prologue
    .line 2143142
    :try_start_0
    const-string v0, "HmacSHA256"

    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v0

    .line 2143143
    invoke-virtual {v0, p3}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 2143144
    const/4 v1, 0x3

    if-lt p0, v1, :cond_0

    .line 2143145
    iget-object v1, p1, LX/Eae;->a:LX/Eat;

    move-object v1, v1

    .line 2143146
    invoke-virtual {v1}, LX/Eat;->a()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->update([B)V

    .line 2143147
    iget-object v1, p2, LX/Eae;->a:LX/Eat;

    move-object v1, v1

    .line 2143148
    invoke-virtual {v1}, LX/Eat;->a()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->update([B)V

    .line 2143149
    :cond_0
    invoke-virtual {v0, p4}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v0

    .line 2143150
    const/16 v1, 0x8

    const/4 p1, 0x0

    .line 2143151
    new-array p0, v1, [B

    .line 2143152
    invoke-static {v0, p1, p0, p1, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2143153
    move-object v0, p0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2143154
    return-object v0

    .line 2143155
    :catch_0
    move-exception v0

    .line 2143156
    :goto_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 2143157
    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/Eae;LX/Eae;Ljavax/crypto/spec/SecretKeySpec;)V
    .locals 3

    .prologue
    .line 2143158
    iget-object v0, p0, LX/EbD;->f:[B

    iget-object v1, p0, LX/EbD;->f:[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x8

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, LX/Eco;->a([BII)[[B

    move-result-object v0

    .line 2143159
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {p1, p2, p3, p4, v1}, LX/EbD;->a(ILX/Eae;LX/Eae;Ljavax/crypto/spec/SecretKeySpec;[B)[B

    move-result-object v1

    .line 2143160
    const/4 v2, 0x1

    aget-object v0, v0, v2

    .line 2143161
    invoke-static {v1, v0}, Ljava/security/MessageDigest;->isEqual([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2143162
    new-instance v0, LX/Eai;

    const-string v1, "Bad Mac!"

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2143163
    :cond_0
    return-void
.end method

.method public final a()[B
    .locals 1

    .prologue
    .line 2143164
    iget-object v0, p0, LX/EbD;->f:[B

    return-object v0
.end method
