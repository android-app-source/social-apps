.class public final LX/E0N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/home/HomeActivity;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/home/HomeActivity;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2068221
    iput-object p1, p0, LX/E0N;->a:Lcom/facebook/places/create/home/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2068222
    iput-object p2, p0, LX/E0N;->b:Landroid/view/View;

    .line 2068223
    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 2068224
    iget-object v0, p0, LX/E0N;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    .line 2068225
    const v1, 0x7f0d162b

    if-ne v0, v1, :cond_1

    .line 2068226
    if-eqz p2, :cond_3

    .line 2068227
    iget-object v0, p0, LX/E0N;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068228
    iget-object p0, v0, LX/E0T;->a:LX/0Zb;

    const-string p2, "home_%s_name_tapped"

    invoke-static {v0, p2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p0, p2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068229
    :cond_0
    :goto_0
    return-void

    .line 2068230
    :cond_1
    const v1, 0x7f0d1631

    if-ne v0, v1, :cond_2

    .line 2068231
    if-eqz p2, :cond_4

    .line 2068232
    iget-object v0, p0, LX/E0N;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068233
    iget-object p0, v0, LX/E0T;->a:LX/0Zb;

    const-string p2, "home_%s_address_tapped"

    invoke-static {v0, p2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p0, p2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068234
    :goto_1
    goto :goto_0

    .line 2068235
    :cond_2
    const v1, 0x7f0d1630

    if-ne v0, v1, :cond_0

    .line 2068236
    if-eqz p2, :cond_5

    .line 2068237
    iget-object v0, p0, LX/E0N;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068238
    iget-object p0, v0, LX/E0T;->a:LX/0Zb;

    const-string p2, "home_%s_neighborhood_tapped"

    invoke-static {v0, p2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p0, p2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068239
    :goto_2
    goto :goto_0

    .line 2068240
    :cond_3
    iget-object v0, p0, LX/E0N;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068241
    iget-object p0, v0, LX/E0T;->a:LX/0Zb;

    const-string p2, "home_%s_name_updated"

    invoke-static {v0, p2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p0, p2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068242
    goto :goto_0

    .line 2068243
    :cond_4
    iget-object v0, p0, LX/E0N;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068244
    iget-object p0, v0, LX/E0T;->a:LX/0Zb;

    const-string p2, "home_%s_address_updated"

    invoke-static {v0, p2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p0, p2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068245
    goto :goto_1

    .line 2068246
    :cond_5
    iget-object v0, p0, LX/E0N;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068247
    iget-object p0, v0, LX/E0T;->a:LX/0Zb;

    const-string p2, "home_%s_neighborhood_updated"

    invoke-static {v0, p2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p0, p2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068248
    goto :goto_2
.end method
