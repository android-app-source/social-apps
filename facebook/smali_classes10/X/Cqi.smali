.class public abstract LX/Cqi;
.super LX/CqX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        "T::",
        "LX/Cqv;",
        ">",
        "LX/CqX",
        "<TV;TT;>;"
    }
.end annotation


# instance fields
.field public a:Landroid/graphics/Rect;

.field public b:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Ljava/lang/Object;LX/CrK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "LX/CrK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1940350
    invoke-direct {p0, p1, p2}, LX/CqX;-><init>(Ljava/lang/Object;LX/CrK;)V

    .line 1940351
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1940352
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/Cqi;->a:Landroid/graphics/Rect;

    .line 1940353
    return-void
.end method

.method public final a(LX/Cqv;Landroid/graphics/Rect;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/graphics/Rect;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1940354
    invoke-virtual {p0, p1}, LX/CqX;->d(LX/Cqv;)LX/Cqf;

    move-result-object v0

    .line 1940355
    if-eqz v0, :cond_0

    .line 1940356
    iput-object p2, v0, LX/Cqf;->o:Landroid/graphics/Rect;

    .line 1940357
    :cond_0
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1940358
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/Cqi;->b:Landroid/graphics/Rect;

    .line 1940359
    return-void
.end method

.method public final r()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1940360
    iget-object v0, p0, LX/Cqi;->b:Landroid/graphics/Rect;

    move-object v0, v0

    .line 1940361
    return-object v0
.end method
