.class public final LX/DQU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/info/GroupInfoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoFragment;)V
    .locals 0

    .prologue
    .line 1994377
    iput-object p1, p0, LX/DQU;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1994378
    iget-object v0, p0, LX/DQU;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/groups/info/GroupInfoFragment;->c(Lcom/facebook/groups/info/GroupInfoFragment;Z)V

    .line 1994379
    iget-object v0, p0, LX/DQU;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v0, v0, Lcom/facebook/groups/info/GroupInfoFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081b54

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1994380
    iget-object v0, p0, LX/DQU;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v0, v0, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    if-nez v0, :cond_0

    .line 1994381
    iget-object v0, p0, LX/DQU;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/facebook/groups/info/GroupInfoFragment;->a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Z)V

    .line 1994382
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1994383
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1994384
    iget-object v0, p0, LX/DQU;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/groups/info/GroupInfoFragment;->c(Lcom/facebook/groups/info/GroupInfoFragment;Z)V

    .line 1994385
    iget-object v1, p0, LX/DQU;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    .line 1994386
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1994387
    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/facebook/groups/info/GroupInfoFragment;->a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Z)V

    .line 1994388
    return-void
.end method
