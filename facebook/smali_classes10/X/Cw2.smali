.class public LX/Cw2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field private final b:Ljava/lang/Integer;

.field private final c:Ljava/lang/String;

.field private final d:LX/Cw4;

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:LX/162;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/Integer;Ljava/lang/String;LX/Cw4;Ljava/lang/String;Ljava/lang/String;LX/162;Ljava/lang/String;)V
    .locals 0
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/162;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1949848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949849
    iput-object p1, p0, LX/Cw2;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1949850
    iput-object p2, p0, LX/Cw2;->b:Ljava/lang/Integer;

    .line 1949851
    iput-object p3, p0, LX/Cw2;->c:Ljava/lang/String;

    .line 1949852
    iput-object p4, p0, LX/Cw2;->d:LX/Cw4;

    .line 1949853
    iput-object p5, p0, LX/Cw2;->e:Ljava/lang/String;

    .line 1949854
    iput-object p6, p0, LX/Cw2;->f:Ljava/lang/String;

    .line 1949855
    iput-object p7, p0, LX/Cw2;->g:LX/162;

    .line 1949856
    iput-object p8, p0, LX/Cw2;->h:Ljava/lang/String;

    .line 1949857
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1949858
    iget-object v0, p0, LX/Cw2;->d:LX/Cw4;

    invoke-virtual {v0, p1}, LX/Cw4;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949859
    const-string v0, "results_module_role"

    iget-object v1, p0, LX/Cw2;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "items_count"

    iget-object v2, p0, LX/Cw2;->b:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "results_module_subtype"

    iget-object v2, p0, LX/Cw2;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "results_module_result_type"

    iget-object v2, p0, LX/Cw2;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "results_module_semantic"

    iget-object v2, p0, LX/Cw2;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "entity_ids"

    iget-object v2, p0, LX/Cw2;->g:LX/162;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "results_module_type"

    iget-object v2, p0, LX/Cw2;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949860
    return-object p1
.end method
