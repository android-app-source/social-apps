.class public LX/Cx8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cx5;


# instance fields
.field private final a:LX/CzE;


# direct methods
.method public constructor <init>(LX/CzE;)V
    .locals 0
    .param p1    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951450
    iput-object p1, p0, LX/Cx8;->a:LX/CzE;

    .line 1951451
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1951452
    instance-of v0, p1, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_1

    instance-of v0, p2, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v0, :cond_1

    .line 1951453
    iget-object v0, p0, LX/Cx8;->a:LX/CzE;

    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v0, p1, p2}, LX/CzE;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1951454
    :cond_0
    :goto_0
    return-void

    .line 1951455
    :cond_1
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLNode;

    if-nez v0, :cond_2

    instance-of v0, p2, Lcom/facebook/graphql/model/GraphQLNode;

    if-nez v0, :cond_2

    .line 1951456
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to replace item of unsupported type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1951457
    :cond_2
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    .line 1951458
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    .line 1951459
    iget-object v0, p0, LX/Cx8;->a:LX/CzE;

    invoke-virtual {v0, p1}, LX/CzE;->b(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1951460
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1951461
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-static {v0, p1, p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;Ljava/lang/Object;Ljava/lang/Object;)Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-result-object v2

    .line 1951462
    iget-object v3, p0, LX/Cx8;->a:LX/CzE;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v3, v0, v2}, LX/CzE;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_0

    .line 1951463
    :cond_3
    iget-object v0, p0, LX/Cx8;->a:LX/CzE;

    invoke-virtual {v0, p1}, LX/CzE;->c(Lcom/facebook/graphql/model/GraphQLNode;)LX/0am;

    move-result-object v1

    .line 1951464
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951465
    new-instance v2, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    .line 1951466
    iget-object p1, v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->d:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-object v0, p1

    .line 1951467
    invoke-direct {v2, p2, v3, v0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;-><init>(Lcom/facebook/graphql/model/GraphQLNode;LX/0am;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)V

    .line 1951468
    iget-object v3, p0, LX/Cx8;->a:LX/CzE;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v3, v0, v2}, LX/CzE;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_0
.end method
