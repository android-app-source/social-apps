.class public final LX/DIz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groupcommerce/composer/ComposerSellView;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/composer/ComposerSellView;)V
    .locals 0

    .prologue
    .line 1984627
    iput-object p1, p0, LX/DIz;->a:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 1984628
    iget-object v0, p0, LX/DIz;->a:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v0, v0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->p:Ljava/lang/String;

    const-string v1, "Currency code must be set."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1984629
    check-cast p1, Lcom/facebook/resources/ui/FbEditText;

    .line 1984630
    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1984631
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1984632
    :goto_0
    return-void

    .line 1984633
    :cond_0
    iget-object v1, p0, LX/DIz;->a:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/ComposerSellView;->n:LX/34u;

    invoke-virtual {v1, v0}, LX/34u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1984634
    if-eqz p2, :cond_1

    .line 1984635
    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1984636
    :cond_1
    iget-object v1, p0, LX/DIz;->a:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-static {v1, v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->b(Lcom/facebook/groupcommerce/composer/ComposerSellView;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
