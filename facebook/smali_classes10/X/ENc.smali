.class public final LX/ENc;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ENd;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataInterfaces$SearchResultsFlexibleContextMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Ps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/ENd;


# direct methods
.method public constructor <init>(LX/ENd;)V
    .locals 1

    .prologue
    .line 2113048
    iput-object p1, p0, LX/ENc;->c:LX/ENd;

    .line 2113049
    move-object v0, p1

    .line 2113050
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2113051
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2113052
    const-string v0, "SearchResultsFlexibleNewsContextPhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2113053
    if-ne p0, p1, :cond_1

    .line 2113054
    :cond_0
    :goto_0
    return v0

    .line 2113055
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2113056
    goto :goto_0

    .line 2113057
    :cond_3
    check-cast p1, LX/ENc;

    .line 2113058
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2113059
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2113060
    if-eq v2, v3, :cond_0

    .line 2113061
    iget-object v2, p0, LX/ENc;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ENc;->a:LX/CzL;

    iget-object v3, p1, LX/ENc;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2113062
    goto :goto_0

    .line 2113063
    :cond_5
    iget-object v2, p1, LX/ENc;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2113064
    :cond_6
    iget-object v2, p0, LX/ENc;->b:LX/1Ps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/ENc;->b:LX/1Ps;

    iget-object v3, p1, LX/ENc;->b:LX/1Ps;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2113065
    goto :goto_0

    .line 2113066
    :cond_7
    iget-object v2, p1, LX/ENc;->b:LX/1Ps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
