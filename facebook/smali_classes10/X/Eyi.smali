.class public final LX/Eyi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/FriendRequest;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2il;


# direct methods
.method public constructor <init>(LX/2il;)V
    .locals 0

    .prologue
    .line 2186448
    iput-object p1, p0, LX/Eyi;->a:LX/2il;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2186472
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186449
    check-cast p1, Ljava/util/List;

    const/4 v2, 0x0

    .line 2186450
    if-nez p1, :cond_1

    .line 2186451
    :cond_0
    :goto_0
    return-void

    .line 2186452
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    .line 2186453
    iget-object v4, p0, LX/Eyi;->a:LX/2il;

    iget-object v4, v4, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v4, v4, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    .line 2186454
    iget-object v5, v0, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2186455
    iget-object v6, v4, LX/2iK;->j:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/friends/model/FriendRequest;

    move-object v4, v6

    .line 2186456
    if-eqz v4, :cond_4

    .line 2186457
    iget-object v5, v0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v5, v5

    .line 2186458
    iget-object v6, v4, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v6, v6

    .line 2186459
    if-eq v5, v6, :cond_4

    .line 2186460
    iget-object v1, v4, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v1, v1

    .line 2186461
    sget-object v5, LX/2lu;->NEEDS_RESPONSE:LX/2lu;

    if-eq v1, v5, :cond_2

    .line 2186462
    iget-object v0, p0, LX/Eyi;->a:LX/2il;

    iget-object v0, v0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0, v2}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->a$redex0(Lcom/facebook/friending/jewel/FriendRequestsFragment;Z)V

    goto :goto_0

    .line 2186463
    :cond_2
    iget-object v1, v0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v0, v1

    .line 2186464
    iput-object v0, v4, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    .line 2186465
    const/4 v1, 0x1

    move v0, v1

    :goto_2
    move v1, v0

    .line 2186466
    goto :goto_1

    .line 2186467
    :cond_3
    if-eqz v1, :cond_0

    .line 2186468
    iget-object v0, p0, LX/Eyi;->a:LX/2il;

    iget-object v0, v0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->z(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    .line 2186469
    iget-object v0, p0, LX/Eyi;->a:LX/2il;

    iget-object v0, v0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/FriendRequestsFragment;->al:LX/2iK;

    const v1, -0x26a66d4d

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2186470
    iget-object v0, p0, LX/Eyi;->a:LX/2il;

    iget-object v0, v0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2186471
    iget-object v0, p0, LX/Eyi;->a:LX/2il;

    iget-object v0, v0, LX/2il;->a:Lcom/facebook/friending/jewel/FriendRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->m(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method
