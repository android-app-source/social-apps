.class public final LX/EwR;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 0

    .prologue
    .line 2183183
    iput-object p1, p0, LX/EwR;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2183184
    check-cast p1, LX/2f2;

    .line 2183185
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eqz v0, :cond_0

    iget-boolean v0, p1, LX/2f2;->c:Z

    if-eqz v0, :cond_1

    .line 2183186
    :cond_0
    :goto_0
    return-void

    .line 2183187
    :cond_1
    iget-object v0, p0, LX/EwR;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->z:LX/Ewf;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EwR;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->z:LX/Ewf;

    invoke-virtual {v0}, LX/Eup;->a()J

    move-result-wide v0

    iget-wide v2, p1, LX/2f2;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget-object v0, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v1, :cond_2

    .line 2183188
    iget-object v0, p0, LX/EwR;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    const/4 v1, 0x0

    .line 2183189
    iput-object v1, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->z:LX/Ewf;

    .line 2183190
    iget-object v0, p0, LX/EwR;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    const v1, -0x7415a4ba

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2183191
    :cond_2
    iget-object v0, p0, LX/EwR;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    iget-wide v2, p1, LX/2f2;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/83X;

    .line 2183192
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iget-object v2, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v1, v2, :cond_0

    .line 2183193
    instance-of v1, v0, LX/Eut;

    if-eqz v1, :cond_4

    move-object v1, v0

    .line 2183194
    check-cast v1, LX/Eut;

    const/4 v2, 0x0

    .line 2183195
    iput-boolean v2, v1, LX/Eut;->f:Z

    .line 2183196
    iget-object v1, p0, LX/EwR;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->r:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    if-eqz v1, :cond_3

    .line 2183197
    iget-object v1, p0, LX/EwR;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->r:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    invoke-virtual {v1}, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->d()V

    .line 2183198
    :cond_3
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-interface {v0, v1}, LX/2np;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2183199
    iget-object v0, p0, LX/EwR;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    const v1, 0xdaeca03

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2183200
    :cond_4
    instance-of v1, v0, LX/Ewo;

    if-eqz v1, :cond_3

    .line 2183201
    iget-object v1, p0, LX/EwR;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    if-eqz v1, :cond_3

    goto :goto_0
.end method
