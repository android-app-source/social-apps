.class public final LX/E5G;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5H;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Landroid/net/Uri;

.field public e:I

.field public f:Landroid/net/Uri;


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2078142
    const-string v0, "ReactionPlacePhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2078143
    if-ne p0, p1, :cond_1

    .line 2078144
    :cond_0
    :goto_0
    return v0

    .line 2078145
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2078146
    goto :goto_0

    .line 2078147
    :cond_3
    check-cast p1, LX/E5G;

    .line 2078148
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2078149
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2078150
    if-eq v2, v3, :cond_0

    .line 2078151
    iget-object v2, p0, LX/E5G;->a:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E5G;->a:Ljava/util/List;

    iget-object v3, p1, LX/E5G;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2078152
    goto :goto_0

    .line 2078153
    :cond_5
    iget-object v2, p1, LX/E5G;->a:Ljava/util/List;

    if-nez v2, :cond_4

    .line 2078154
    :cond_6
    iget-object v2, p0, LX/E5G;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E5G;->b:Ljava/lang/String;

    iget-object v3, p1, LX/E5G;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2078155
    goto :goto_0

    .line 2078156
    :cond_8
    iget-object v2, p1, LX/E5G;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2078157
    :cond_9
    iget-object v2, p0, LX/E5G;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E5G;->c:Ljava/lang/String;

    iget-object v3, p1, LX/E5G;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2078158
    goto :goto_0

    .line 2078159
    :cond_b
    iget-object v2, p1, LX/E5G;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2078160
    :cond_c
    iget-object v2, p0, LX/E5G;->d:Landroid/net/Uri;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/E5G;->d:Landroid/net/Uri;

    iget-object v3, p1, LX/E5G;->d:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2078161
    goto :goto_0

    .line 2078162
    :cond_e
    iget-object v2, p1, LX/E5G;->d:Landroid/net/Uri;

    if-nez v2, :cond_d

    .line 2078163
    :cond_f
    iget v2, p0, LX/E5G;->e:I

    iget v3, p1, LX/E5G;->e:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 2078164
    goto :goto_0

    .line 2078165
    :cond_10
    iget-object v2, p0, LX/E5G;->f:Landroid/net/Uri;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/E5G;->f:Landroid/net/Uri;

    iget-object v3, p1, LX/E5G;->f:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2078166
    goto :goto_0

    .line 2078167
    :cond_11
    iget-object v2, p1, LX/E5G;->f:Landroid/net/Uri;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
