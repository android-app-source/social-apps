.class public final LX/D0h;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/D0i;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:LX/1dQ;

.field public c:I

.field public d:LX/1dQ;

.field public final synthetic e:LX/D0i;


# direct methods
.method public constructor <init>(LX/D0i;)V
    .locals 1

    .prologue
    .line 1955824
    iput-object p1, p0, LX/D0h;->e:LX/D0i;

    .line 1955825
    move-object v0, p1

    .line 1955826
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1955827
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1955823
    const-string v0, "DTIButtonBarComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1955805
    if-ne p0, p1, :cond_1

    .line 1955806
    :cond_0
    :goto_0
    return v0

    .line 1955807
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1955808
    goto :goto_0

    .line 1955809
    :cond_3
    check-cast p1, LX/D0h;

    .line 1955810
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1955811
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1955812
    if-eq v2, v3, :cond_0

    .line 1955813
    iget v2, p0, LX/D0h;->a:I

    iget v3, p1, LX/D0h;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1955814
    goto :goto_0

    .line 1955815
    :cond_4
    iget-object v2, p0, LX/D0h;->b:LX/1dQ;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/D0h;->b:LX/1dQ;

    iget-object v3, p1, LX/D0h;->b:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1955816
    goto :goto_0

    .line 1955817
    :cond_6
    iget-object v2, p1, LX/D0h;->b:LX/1dQ;

    if-nez v2, :cond_5

    .line 1955818
    :cond_7
    iget v2, p0, LX/D0h;->c:I

    iget v3, p1, LX/D0h;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1955819
    goto :goto_0

    .line 1955820
    :cond_8
    iget-object v2, p0, LX/D0h;->d:LX/1dQ;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/D0h;->d:LX/1dQ;

    iget-object v3, p1, LX/D0h;->d:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1955821
    goto :goto_0

    .line 1955822
    :cond_9
    iget-object v2, p1, LX/D0h;->d:LX/1dQ;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
