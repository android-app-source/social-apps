.class public final LX/EpD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EpE;


# direct methods
.method public constructor <init>(LX/EpE;)V
    .locals 0

    .prologue
    .line 2170077
    iput-object p1, p0, LX/EpD;->a:LX/EpE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2170078
    iget-object v0, p0, LX/EpD;->a:LX/EpE;

    iget-object v0, v0, LX/EpE;->f:LX/2hZ;

    iget-object v1, p0, LX/EpD;->a:LX/EpE;

    iget-object v1, v1, LX/EpE;->g:Landroid/content/Context;

    .line 2170079
    const v2, 0x7f080039

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v2, v3, v1}, LX/2hZ;->a(Ljava/lang/Throwable;ILandroid/content/DialogInterface$OnClickListener;Landroid/content/Context;)V

    .line 2170080
    iget-object v0, p0, LX/EpD;->a:LX/EpE;

    iget-object v0, v0, LX/EpE;->e:LX/2do;

    new-instance v1, LX/2f2;

    iget-object v2, p0, LX/EpD;->a:LX/EpE;

    iget-wide v2, v2, LX/EpE;->b:J

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2170081
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170082
    iget-object v0, p0, LX/EpD;->a:LX/EpE;

    iget-object v0, v0, LX/EpE;->e:LX/2do;

    new-instance v1, LX/2f2;

    iget-object v2, p0, LX/EpD;->a:LX/EpE;

    iget-wide v2, v2, LX/EpE;->b:J

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2170083
    return-void
.end method
