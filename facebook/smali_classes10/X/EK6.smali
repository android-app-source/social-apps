.class public final LX/EK6;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EK7;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Ljava/lang/CharSequence;

.field public f:Ljava/lang/CharSequence;

.field public g:I

.field public h:I

.field public i:I

.field public j:Ljava/lang/String;

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:Landroid/view/View$OnClickListener;

.field public final synthetic p:LX/EK7;


# direct methods
.method public constructor <init>(LX/EK7;)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 2105965
    iput-object p1, p0, LX/EK6;->p:LX/EK7;

    .line 2105966
    move-object v0, p1

    .line 2105967
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2105968
    iput v1, p0, LX/EK6;->d:I

    .line 2105969
    const/4 v0, -0x1

    iput v0, p0, LX/EK6;->g:I

    .line 2105970
    iput v1, p0, LX/EK6;->h:I

    .line 2105971
    iput v1, p0, LX/EK6;->i:I

    .line 2105972
    sget v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->a:I

    iput v0, p0, LX/EK6;->k:I

    .line 2105973
    sget v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->b:I

    iput v0, p0, LX/EK6;->l:I

    .line 2105974
    sget v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->c:I

    iput v0, p0, LX/EK6;->m:I

    .line 2105975
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2105976
    const-string v0, "SearchResultsImageBlockLayoutRowComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2105977
    if-ne p0, p1, :cond_1

    .line 2105978
    :cond_0
    :goto_0
    return v0

    .line 2105979
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2105980
    goto :goto_0

    .line 2105981
    :cond_3
    check-cast p1, LX/EK6;

    .line 2105982
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2105983
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2105984
    if-eq v2, v3, :cond_0

    .line 2105985
    iget-object v2, p0, LX/EK6;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EK6;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/EK6;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2105986
    goto :goto_0

    .line 2105987
    :cond_5
    iget-object v2, p1, LX/EK6;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 2105988
    :cond_6
    iget-object v2, p0, LX/EK6;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EK6;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/EK6;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2105989
    goto :goto_0

    .line 2105990
    :cond_8
    iget-object v2, p1, LX/EK6;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 2105991
    :cond_9
    iget-object v2, p0, LX/EK6;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/EK6;->c:Ljava/lang/String;

    iget-object v3, p1, LX/EK6;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2105992
    goto :goto_0

    .line 2105993
    :cond_b
    iget-object v2, p1, LX/EK6;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2105994
    :cond_c
    iget v2, p0, LX/EK6;->d:I

    iget v3, p1, LX/EK6;->d:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 2105995
    goto :goto_0

    .line 2105996
    :cond_d
    iget-object v2, p0, LX/EK6;->e:Ljava/lang/CharSequence;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/EK6;->e:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/EK6;->e:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 2105997
    goto :goto_0

    .line 2105998
    :cond_f
    iget-object v2, p1, LX/EK6;->e:Ljava/lang/CharSequence;

    if-nez v2, :cond_e

    .line 2105999
    :cond_10
    iget-object v2, p0, LX/EK6;->f:Ljava/lang/CharSequence;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/EK6;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/EK6;->f:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 2106000
    goto :goto_0

    .line 2106001
    :cond_12
    iget-object v2, p1, LX/EK6;->f:Ljava/lang/CharSequence;

    if-nez v2, :cond_11

    .line 2106002
    :cond_13
    iget v2, p0, LX/EK6;->g:I

    iget v3, p1, LX/EK6;->g:I

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 2106003
    goto/16 :goto_0

    .line 2106004
    :cond_14
    iget v2, p0, LX/EK6;->h:I

    iget v3, p1, LX/EK6;->h:I

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 2106005
    goto/16 :goto_0

    .line 2106006
    :cond_15
    iget v2, p0, LX/EK6;->i:I

    iget v3, p1, LX/EK6;->i:I

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 2106007
    goto/16 :goto_0

    .line 2106008
    :cond_16
    iget-object v2, p0, LX/EK6;->j:Ljava/lang/String;

    if-eqz v2, :cond_18

    iget-object v2, p0, LX/EK6;->j:Ljava/lang/String;

    iget-object v3, p1, LX/EK6;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 2106009
    goto/16 :goto_0

    .line 2106010
    :cond_18
    iget-object v2, p1, LX/EK6;->j:Ljava/lang/String;

    if-nez v2, :cond_17

    .line 2106011
    :cond_19
    iget v2, p0, LX/EK6;->k:I

    iget v3, p1, LX/EK6;->k:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 2106012
    goto/16 :goto_0

    .line 2106013
    :cond_1a
    iget v2, p0, LX/EK6;->l:I

    iget v3, p1, LX/EK6;->l:I

    if-eq v2, v3, :cond_1b

    move v0, v1

    .line 2106014
    goto/16 :goto_0

    .line 2106015
    :cond_1b
    iget v2, p0, LX/EK6;->m:I

    iget v3, p1, LX/EK6;->m:I

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 2106016
    goto/16 :goto_0

    .line 2106017
    :cond_1c
    iget v2, p0, LX/EK6;->n:I

    iget v3, p1, LX/EK6;->n:I

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 2106018
    goto/16 :goto_0

    .line 2106019
    :cond_1d
    iget-object v2, p0, LX/EK6;->o:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_1e

    iget-object v2, p0, LX/EK6;->o:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/EK6;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2106020
    goto/16 :goto_0

    .line 2106021
    :cond_1e
    iget-object v2, p1, LX/EK6;->o:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
