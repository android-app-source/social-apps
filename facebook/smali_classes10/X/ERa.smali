.class public LX/ERa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/ERb;",
        "LX/ERc;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2121104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121105
    iput-object p1, p0, LX/ERa;->a:LX/03V;

    .line 2121106
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 2121107
    check-cast p1, LX/ERb;

    .line 2121108
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2121109
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "client_image_hash"

    .line 2121110
    iget-object v3, p1, LX/ERb;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2121111
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2121112
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "date_taken"

    .line 2121113
    iget-object v6, p1, LX/ERb;->b:Ljava/lang/String;

    invoke-static {v6}, LX/EQg;->b(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v6

    iget-object v6, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2121114
    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    move-object v3, v6

    .line 2121115
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2121116
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_oid"

    .line 2121117
    iget-object v3, p1, LX/ERb;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2121118
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2121119
    iget-wide v6, p1, LX/ERb;->d:J

    move-wide v2, v6

    .line 2121120
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 2121121
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "existing_fbid"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2121122
    :cond_0
    iget-object v1, p1, LX/ERb;->a:Ljava/io/File;

    move-object v1, v1

    .line 2121123
    new-instance v2, LX/4d5;

    const-string v3, "image/jpeg"

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 2121124
    iget-object v5, p1, LX/ERb;->e:LX/4cr;

    move-object v5, v5

    .line 2121125
    invoke-direct {v2, v1, v3, v4, v5}, LX/4d5;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;LX/4cr;)V

    .line 2121126
    new-instance v1, LX/4cQ;

    const-string v3, "source"

    invoke-direct {v1, v3, v2}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 2121127
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "vaultImageUpload"

    .line 2121128
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2121129
    move-object v2, v2

    .line 2121130
    const-string v3, "POST"

    .line 2121131
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2121132
    move-object v2, v2

    .line 2121133
    const-string v3, "me/vaultimages"

    .line 2121134
    iput-object v3, v2, LX/14O;->d:Ljava/lang/String;

    .line 2121135
    move-object v2, v2

    .line 2121136
    iput-object v0, v2, LX/14O;->g:Ljava/util/List;

    .line 2121137
    move-object v0, v2

    .line 2121138
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 2121139
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 2121140
    move-object v0, v0

    .line 2121141
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2121142
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 2121143
    move-object v0, v0

    .line 2121144
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2121145
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2121146
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v0

    .line 2121147
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 2121148
    iget-object v2, p0, LX/ERa;->a:LX/03V;

    const-string v3, "vault_image_upload_api missing id"

    const-string v4, "missing id in response: %s"

    .line 2121149
    iget-object v5, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v5, v5

    .line 2121150
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2121151
    :cond_0
    new-instance v2, LX/ERc;

    invoke-direct {v2, v0, v1}, LX/ERc;-><init>(J)V

    return-object v2
.end method
