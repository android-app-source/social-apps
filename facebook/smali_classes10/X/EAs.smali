.class public LX/EAs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/EAs;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/E9N;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/79D;

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/E9P;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/E9Y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/79D;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/E9N;",
            ">;",
            "LX/79D;",
            "LX/0Or",
            "<",
            "LX/E9P;",
            ">;",
            "LX/0Or",
            "<",
            "LX/E9Y;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2085949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2085950
    iput-object p1, p0, LX/EAs;->a:LX/0Or;

    .line 2085951
    iput-object p2, p0, LX/EAs;->b:LX/79D;

    .line 2085952
    iput-object p3, p0, LX/EAs;->c:LX/0Or;

    .line 2085953
    iput-object p4, p0, LX/EAs;->d:LX/0Or;

    .line 2085954
    return-void
.end method

.method public static a(LX/0QB;)LX/EAs;
    .locals 7

    .prologue
    .line 2085955
    sget-object v0, LX/EAs;->e:LX/EAs;

    if-nez v0, :cond_1

    .line 2085956
    const-class v1, LX/EAs;

    monitor-enter v1

    .line 2085957
    :try_start_0
    sget-object v0, LX/EAs;->e:LX/EAs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2085958
    if-eqz v2, :cond_0

    .line 2085959
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2085960
    new-instance v4, LX/EAs;

    const/16 v3, 0x31af

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/79D;->a(LX/0QB;)LX/79D;

    move-result-object v3

    check-cast v3, LX/79D;

    const/16 v6, 0x31b0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x31b4

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v5, v3, v6, p0}, LX/EAs;-><init>(LX/0Or;LX/79D;LX/0Or;LX/0Or;)V

    .line 2085961
    move-object v0, v4

    .line 2085962
    sput-object v0, LX/EAs;->e:LX/EAs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2085963
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2085964
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2085965
    :cond_1
    sget-object v0, LX/EAs;->e:LX/EAs;

    return-object v0

    .line 2085966
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2085967
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
