.class public final LX/El9;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:LX/El7;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;LX/El7;)V
    .locals 0

    .prologue
    .line 2164086
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 2164087
    iput-object p1, p0, LX/El9;->a:Ljava/io/InputStream;

    .line 2164088
    iput-object p2, p0, LX/El9;->b:LX/El7;

    .line 2164089
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 2164083
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2164084
    iget-object v0, p0, LX/El9;->b:LX/El7;

    invoke-virtual {v0}, LX/El7;->b()V

    .line 2164085
    :cond_0
    return p1
.end method

.method private a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 1

    .prologue
    .line 2164081
    iget-object v0, p0, LX/El9;->b:LX/El7;

    invoke-virtual {v0, p1}, LX/El7;->a(Ljava/io/IOException;)V

    .line 2164082
    throw p1
.end method


# virtual methods
.method public final available()I
    .locals 1

    .prologue
    .line 2164080
    iget-object v0, p0, LX/El9;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 2164076
    :try_start_0
    iget-object v0, p0, LX/El9;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2164077
    iget-object v0, p0, LX/El9;->b:LX/El7;

    invoke-virtual {v0}, LX/El7;->a()V

    .line 2164078
    return-void

    .line 2164079
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/El9;->b:LX/El7;

    invoke-virtual {v1}, LX/El7;->a()V

    throw v0
.end method

.method public final mark(I)V
    .locals 1

    .prologue
    .line 2164090
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 2164075
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 1

    .prologue
    .line 2164072
    :try_start_0
    iget-object v0, p0, LX/El9;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    invoke-direct {p0, v0}, LX/El9;->a(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 2164073
    :catch_0
    move-exception v0

    .line 2164074
    invoke-direct {p0, v0}, LX/El9;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public final read([B)I
    .locals 1

    .prologue
    .line 2164069
    :try_start_0
    iget-object v0, p0, LX/El9;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    invoke-direct {p0, v0}, LX/El9;->a(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 2164070
    :catch_0
    move-exception v0

    .line 2164071
    invoke-direct {p0, v0}, LX/El9;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 2164066
    :try_start_0
    iget-object v0, p0, LX/El9;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    invoke-direct {p0, v0}, LX/El9;->a(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 2164067
    :catch_0
    move-exception v0

    .line 2164068
    invoke-direct {p0, v0}, LX/El9;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public final declared-synchronized reset()V
    .locals 1

    .prologue
    .line 2164065
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
