.class public LX/DNJ;
.super LX/1La;
.source ""

# interfaces
.implements LX/DN9;


# instance fields
.field private A:Landroid/view/View;

.field public B:LX/2iI;

.field public C:LX/DNB;

.field public D:LX/0Yb;

.field private E:Ljava/lang/String;

.field public F:LX/DNR;

.field private G:Z

.field public H:Z

.field public I:Z

.field private final J:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public K:Z

.field public L:Z

.field private M:Z

.field private N:Z

.field public final a:LX/DCI;

.field public final b:LX/DCL;

.field public final c:LX/DCE;

.field public final d:LX/DCC;

.field public final e:LX/DCO;

.field public final f:LX/DNI;

.field public final g:LX/DCQ;

.field private final h:LX/0kb;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/1Db;

.field public final k:LX/1CY;

.field public final l:LX/1Kt;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2cm;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/1LV;

.field public final o:LX/1B1;

.field public final p:LX/0bH;

.field private final q:LX/0ad;

.field private final r:LX/88l;

.field public s:LX/DNU;

.field public t:LX/1Cw;

.field public u:LX/1Cw;

.field public v:LX/1Qq;

.field public w:LX/DRf;

.field private x:LX/1Pf;

.field public y:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private z:Lcom/facebook/feed/banner/GenericNotificationBanner;


# direct methods
.method public constructor <init>(LX/0Or;LX/1B1;LX/0kb;LX/DCC;LX/DCE;LX/0bH;LX/DCI;LX/1LV;LX/DCL;LX/DCO;LX/DCQ;LX/1CY;LX/0Ot;LX/1Db;LX/0Ot;LX/0ad;LX/1Kt;LX/88l;)V
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/groups/feed/controller/annotations/IsGroupsFeedVPVLoggingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1B1;",
            "LX/0kb;",
            "LX/DCC;",
            "LX/DCE;",
            "LX/0bH;",
            "LX/DCI;",
            "LX/1LV;",
            "LX/DCL;",
            "LX/DCO;",
            "LX/DCQ;",
            "LX/1CY;",
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;",
            "LX/1Db;",
            "LX/0Ot",
            "<",
            "LX/2cm;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            "LX/88l;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1991204
    invoke-direct {p0}, LX/1La;-><init>()V

    .line 1991205
    const/4 v1, 0x0

    iput-object v1, p0, LX/DNJ;->E:Ljava/lang/String;

    .line 1991206
    iput-object p4, p0, LX/DNJ;->d:LX/DCC;

    .line 1991207
    move-object/from16 v0, p13

    iput-object v0, p0, LX/DNJ;->i:LX/0Ot;

    .line 1991208
    iput-object p5, p0, LX/DNJ;->c:LX/DCE;

    .line 1991209
    iput-object p2, p0, LX/DNJ;->o:LX/1B1;

    .line 1991210
    iput-object p6, p0, LX/DNJ;->p:LX/0bH;

    .line 1991211
    iput-object p8, p0, LX/DNJ;->n:LX/1LV;

    .line 1991212
    iput-object p11, p0, LX/DNJ;->g:LX/DCQ;

    .line 1991213
    iput-object p12, p0, LX/DNJ;->k:LX/1CY;

    .line 1991214
    iput-object p7, p0, LX/DNJ;->a:LX/DCI;

    .line 1991215
    iput-object p3, p0, LX/DNJ;->h:LX/0kb;

    .line 1991216
    move-object/from16 v0, p14

    iput-object v0, p0, LX/DNJ;->j:LX/1Db;

    .line 1991217
    iput-object p9, p0, LX/DNJ;->b:LX/DCL;

    .line 1991218
    move-object/from16 v0, p15

    iput-object v0, p0, LX/DNJ;->m:LX/0Ot;

    .line 1991219
    move-object/from16 v0, p17

    iput-object v0, p0, LX/DNJ;->l:LX/1Kt;

    .line 1991220
    iput-object p10, p0, LX/DNJ;->e:LX/DCO;

    .line 1991221
    iput-object p1, p0, LX/DNJ;->J:LX/0Or;

    .line 1991222
    new-instance v1, LX/DNI;

    invoke-direct {v1, p0}, LX/DNI;-><init>(LX/DNJ;)V

    iput-object v1, p0, LX/DNJ;->f:LX/DNI;

    .line 1991223
    move-object/from16 v0, p16

    iput-object v0, p0, LX/DNJ;->q:LX/0ad;

    .line 1991224
    move-object/from16 v0, p18

    iput-object v0, p0, LX/DNJ;->r:LX/88l;

    .line 1991225
    return-void
.end method

.method public static a(LX/0QB;)LX/DNJ;
    .locals 1

    .prologue
    .line 1991226
    invoke-static {p0}, LX/DNJ;->b(LX/0QB;)LX/DNJ;

    move-result-object v0

    return-object v0
.end method

.method private a(ZZ)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1991227
    iget-boolean v0, p0, LX/DNJ;->G:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNJ;->A:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1991228
    iget-object v0, p0, LX/DNJ;->A:Landroid/view/View;

    const v3, 0x7f0d0d65

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1991229
    if-eqz v3, :cond_0

    .line 1991230
    iget-object v0, p0, LX/DNJ;->A:Landroid/view/View;

    const v4, 0x7f0d01f9

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1991231
    iget-object v0, p0, LX/DNJ;->A:Landroid/view/View;

    const v5, 0x7f0d01f8

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1991232
    if-eqz p2, :cond_1

    iget-object v0, p0, LX/DNJ;->h:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1991233
    if-eqz p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1991234
    if-nez p1, :cond_3

    if-nez p2, :cond_3

    :goto_2
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1991235
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1991236
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1991237
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1991238
    goto :goto_2
.end method

.method public static b(LX/0QB;)LX/DNJ;
    .locals 21

    .prologue
    .line 1991239
    new-instance v2, LX/DNJ;

    const/16 v3, 0x31d

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/1B1;->a(LX/0QB;)LX/1B1;

    move-result-object v4

    check-cast v4, LX/1B1;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/DCC;->a(LX/0QB;)LX/DCC;

    move-result-object v6

    check-cast v6, LX/DCC;

    invoke-static/range {p0 .. p0}, LX/DCE;->a(LX/0QB;)LX/DCE;

    move-result-object v7

    check-cast v7, LX/DCE;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v8

    check-cast v8, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/DCI;->a(LX/0QB;)LX/DCI;

    move-result-object v9

    check-cast v9, LX/DCI;

    invoke-static/range {p0 .. p0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v10

    check-cast v10, LX/1LV;

    invoke-static/range {p0 .. p0}, LX/DCL;->a(LX/0QB;)LX/DCL;

    move-result-object v11

    check-cast v11, LX/DCL;

    invoke-static/range {p0 .. p0}, LX/DCO;->a(LX/0QB;)LX/DCO;

    move-result-object v12

    check-cast v12, LX/DCO;

    invoke-static/range {p0 .. p0}, LX/DCQ;->a(LX/0QB;)LX/DCQ;

    move-result-object v13

    check-cast v13, LX/DCQ;

    invoke-static/range {p0 .. p0}, LX/1CY;->a(LX/0QB;)LX/1CY;

    move-result-object v14

    check-cast v14, LX/1CY;

    const/16 v15, 0x1a75

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v16

    check-cast v16, LX/1Db;

    const/16 v17, 0xf4d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v18

    check-cast v18, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/23N;->a(LX/0QB;)LX/23N;

    move-result-object v19

    check-cast v19, LX/1Kt;

    invoke-static/range {p0 .. p0}, LX/88l;->a(LX/0QB;)LX/88l;

    move-result-object v20

    check-cast v20, LX/88l;

    invoke-direct/range {v2 .. v20}, LX/DNJ;-><init>(LX/0Or;LX/1B1;LX/0kb;LX/DCC;LX/DCE;LX/0bH;LX/DCI;LX/1LV;LX/DCL;LX/DCO;LX/DCQ;LX/1CY;LX/0Ot;LX/1Db;LX/0Ot;LX/0ad;LX/1Kt;LX/88l;)V

    .line 1991240
    return-object v2
.end method

.method public static l(LX/DNJ;)Z
    .locals 2

    .prologue
    .line 1991241
    iget-object v0, p0, LX/DNJ;->J:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static o(LX/DNJ;)V
    .locals 3

    .prologue
    .line 1991242
    iget-object v0, p0, LX/DNJ;->C:LX/DNB;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNJ;->z:Lcom/facebook/feed/banner/GenericNotificationBanner;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNJ;->C:LX/DNB;

    iget-object v1, p0, LX/DNJ;->h:LX/0kb;

    iget-object v2, p0, LX/DNJ;->z:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-interface {v0, v1, v2}, LX/DNB;->a(LX/0kb;Lcom/facebook/feed/banner/GenericNotificationBanner;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1991243
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    if-eqz v0, :cond_0

    .line 1991244
    iget-object v0, p0, LX/DNJ;->h:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    .line 1991245
    if-eqz v0, :cond_1

    .line 1991246
    iget-object v0, p0, LX/DNJ;->z:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {v0}, LX/4nk;->a()V

    .line 1991247
    :cond_0
    :goto_0
    return-void

    .line 1991248
    :cond_1
    iget-object v0, p0, LX/DNJ;->z:Lcom/facebook/feed/banner/GenericNotificationBanner;

    sget-object v1, LX/DBa;->NO_CONNECTION:LX/DBa;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1991249
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1991250
    :goto_0
    return-void

    .line 1991251
    :cond_0
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 1991252
    :sswitch_0
    iget-object v0, p0, LX/DNJ;->c:LX/DCE;

    invoke-virtual {v0, p3}, LX/DCE;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 1991253
    :sswitch_1
    iget-object v0, p0, LX/DNJ;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cm;

    invoke-virtual {v0, p3}, LX/2cm;->a(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x6de -> :sswitch_0
        0x138a -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1991254
    invoke-super {p0, p1, p2, p3}, LX/1La;->a(Landroid/support/v4/app/Fragment;Landroid/view/View;Landroid/os/Bundle;)V

    .line 1991255
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DNJ;->H:Z

    .line 1991256
    return-void
.end method

.method public final a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;ZLX/0fu;ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1991257
    iput-boolean p8, p0, LX/DNJ;->M:Z

    .line 1991258
    iput-boolean v3, p0, LX/DNJ;->H:Z

    .line 1991259
    iput-boolean v2, p0, LX/DNJ;->N:Z

    .line 1991260
    iput-object p2, p0, LX/DNJ;->F:LX/DNR;

    .line 1991261
    iput-boolean p6, p0, LX/DNJ;->G:Z

    .line 1991262
    invoke-virtual {p4, p0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 1991263
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1991264
    iget-object v1, p0, LX/DNJ;->D:LX/0Yb;

    if-nez v1, :cond_0

    .line 1991265
    new-instance v1, LX/0Xj;

    invoke-direct {v1, v0}, LX/0Xj;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, LX/0Xk;->a()LX/0YX;

    move-result-object v1

    const-string p2, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance p4, LX/DNH;

    invoke-direct {p4, p0}, LX/DNH;-><init>(LX/DNJ;)V

    invoke-interface {v1, p2, p4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, p0, LX/DNJ;->D:LX/0Yb;

    .line 1991266
    iget-object v1, p0, LX/DNJ;->D:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 1991267
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0830e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/DNJ;->E:Ljava/lang/String;

    .line 1991268
    iput-object p5, p0, LX/DNJ;->C:LX/DNB;

    .line 1991269
    iput-object p1, p0, LX/DNJ;->A:Landroid/view/View;

    .line 1991270
    iget-object v0, p0, LX/DNJ;->A:Landroid/view/View;

    const v1, 0x7f0d0595

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, LX/DNJ;->y:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 1991271
    iget-object v0, p0, LX/DNJ;->n:LX/1LV;

    const-string v1, "group_feed"

    invoke-virtual {v0, v1}, LX/1LV;->a(Ljava/lang/String;)V

    .line 1991272
    invoke-static {p0}, LX/DNJ;->l(LX/DNJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1991273
    iget-object v0, p0, LX/DNJ;->l:LX/1Kt;

    iget-object v1, p0, LX/DNJ;->n:LX/1LV;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 1991274
    :cond_1
    iget-object v0, p0, LX/DNJ;->A:Landroid/view/View;

    const v1, 0x7f0d01f9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1991275
    new-instance v1, LX/2iI;

    invoke-direct {v1, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v1, p0, LX/DNJ;->B:LX/2iI;

    .line 1991276
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    invoke-virtual {v0, v2}, LX/2iI;->b(I)V

    .line 1991277
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    invoke-virtual {v0}, LX/2iI;->k()V

    .line 1991278
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    invoke-virtual {v0, v2}, LX/2iI;->b(Z)V

    .line 1991279
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    invoke-virtual {v0, v3}, LX/2iI;->d(Z)V

    .line 1991280
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    const v1, 0x1020004

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2iI;->f(Landroid/view/View;)V

    .line 1991281
    if-eqz p7, :cond_2

    .line 1991282
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    invoke-virtual {v0, p7}, LX/2iI;->b(LX/0fu;)V

    .line 1991283
    :cond_2
    iget-object v0, p0, LX/DNJ;->C:LX/DNB;

    invoke-interface {v0, p1}, LX/DNB;->a(Landroid/view/View;)V

    .line 1991284
    const v0, 0x7f0d08bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/banner/GenericNotificationBanner;

    iput-object v0, p0, LX/DNJ;->z:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 1991285
    invoke-static {p0}, LX/DNJ;->o(LX/DNJ;)V

    .line 1991286
    iget-object v0, p0, LX/DNJ;->C:LX/DNB;

    invoke-interface {v0}, LX/DNB;->d()LX/1Cv;

    move-result-object v0

    iput-object v0, p0, LX/DNJ;->t:LX/1Cw;

    .line 1991287
    iget-object v0, p0, LX/DNJ;->C:LX/DNB;

    iget-object v1, p0, LX/DNJ;->B:LX/2iI;

    invoke-interface {v0, v1}, LX/DNB;->a(LX/0g8;)LX/1Pf;

    move-result-object v0

    iput-object v0, p0, LX/DNJ;->x:LX/1Pf;

    .line 1991288
    iget-object v0, p0, LX/DNJ;->C:LX/DNB;

    iget-object v1, p0, LX/DNJ;->x:LX/1Pf;

    invoke-interface {v0, p3, v1}, LX/DNB;->a(LX/0g1;LX/1Pf;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/DNJ;->v:LX/1Qq;

    .line 1991289
    if-eqz p9, :cond_3

    .line 1991290
    iget-object v0, p0, LX/DNJ;->C:LX/DNB;

    invoke-interface {v0}, LX/DNB;->e()LX/DRf;

    move-result-object v0

    iput-object v0, p0, LX/DNJ;->w:LX/DRf;

    .line 1991291
    :cond_3
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    new-instance v1, LX/DND;

    invoke-direct {v1, p0}, LX/DND;-><init>(LX/DNJ;)V

    invoke-virtual {v0, v1}, LX/2iI;->b(LX/0fx;)V

    .line 1991292
    iget-object v0, p0, LX/DNJ;->t:LX/1Cw;

    if-eqz v0, :cond_4

    .line 1991293
    iget-object v0, p0, LX/DNJ;->t:LX/1Cw;

    iput-object v0, p0, LX/DNJ;->u:LX/1Cw;

    .line 1991294
    :cond_4
    if-eqz p9, :cond_5

    iget-object v0, p0, LX/DNJ;->w:LX/DRf;

    if-nez v0, :cond_5

    .line 1991295
    iget-object v0, p0, LX/DNJ;->C:LX/DNB;

    invoke-interface {v0}, LX/DNB;->e()LX/DRf;

    move-result-object v0

    iput-object v0, p0, LX/DNJ;->w:LX/DRf;

    .line 1991296
    :cond_5
    iget-object v0, p0, LX/DNJ;->u:LX/1Cw;

    if-eqz v0, :cond_7

    .line 1991297
    if-nez p9, :cond_6

    .line 1991298
    new-instance v0, LX/DNV;

    iget-object v1, p0, LX/DNJ;->u:LX/1Cw;

    iget-object v2, p0, LX/DNJ;->v:LX/1Qq;

    invoke-direct {v0, v1, v2}, LX/DNV;-><init>(LX/1Cw;LX/1Cw;)V

    iput-object v0, p0, LX/DNJ;->s:LX/DNU;

    .line 1991299
    :goto_0
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    iget-object v1, p0, LX/DNJ;->s:LX/DNU;

    invoke-virtual {v0, v1}, LX/2iI;->a(Landroid/widget/ListAdapter;)V

    .line 1991300
    :goto_1
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    iget-object v1, p0, LX/DNJ;->j:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2iI;->a(LX/1St;)V

    .line 1991301
    iget-object v0, p0, LX/DNJ;->y:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, LX/DNE;

    invoke-direct {v1, p0}, LX/DNE;-><init>(LX/DNJ;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 1991302
    new-instance v0, LX/DNF;

    invoke-direct {v0, p0}, LX/DNF;-><init>(LX/DNJ;)V

    .line 1991303
    iget-object v1, p0, LX/DNJ;->a:LX/DCI;

    .line 1991304
    iget-object v2, p3, LX/0fz;->a:LX/0qq;

    move-object v2, v2

    .line 1991305
    invoke-virtual {v1, v2, v0}, LX/DCI;->a(LX/0qq;LX/0g4;)V

    .line 1991306
    iget-object v1, p0, LX/DNJ;->b:LX/DCL;

    .line 1991307
    iget-object v2, p3, LX/0fz;->a:LX/0qq;

    move-object v2, v2

    .line 1991308
    invoke-virtual {v1, v2, v0}, LX/DCL;->a(LX/0qq;LX/0g4;)V

    .line 1991309
    iget-object v1, p0, LX/DNJ;->c:LX/DCE;

    .line 1991310
    iget-object v2, p3, LX/0fz;->a:LX/0qq;

    move-object v2, v2

    .line 1991311
    invoke-virtual {v1, v2, v0}, LX/DCE;->a(LX/0qq;LX/0g4;)V

    .line 1991312
    iget-object v1, p0, LX/DNJ;->d:LX/DCC;

    .line 1991313
    iget-object v2, p3, LX/0fz;->a:LX/0qq;

    move-object v2, v2

    .line 1991314
    invoke-virtual {v1, v2, v0}, LX/DCC;->a(LX/0qq;LX/0g4;)V

    .line 1991315
    iget-object v1, p0, LX/DNJ;->e:LX/DCO;

    .line 1991316
    iget-object v2, p3, LX/0fz;->a:LX/0qq;

    move-object v2, v2

    .line 1991317
    invoke-virtual {v1, v2, v0}, LX/DCO;->a(LX/0qq;LX/0g4;)V

    .line 1991318
    iget-object v1, p0, LX/DNJ;->g:LX/DCQ;

    .line 1991319
    iget-object v2, p3, LX/0fz;->a:LX/0qq;

    move-object v2, v2

    .line 1991320
    invoke-virtual {v1, v2, v0}, LX/DCQ;->a(LX/0qq;LX/0g4;)V

    .line 1991321
    iget-object v0, p0, LX/DNJ;->o:LX/1B1;

    iget-object v1, p0, LX/DNJ;->f:LX/DNI;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 1991322
    iget-object v0, p0, LX/DNJ;->o:LX/1B1;

    iget-object v1, p0, LX/DNJ;->p:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 1991323
    new-instance v0, LX/DNG;

    invoke-direct {v0, p0}, LX/DNG;-><init>(LX/DNJ;)V

    .line 1991324
    iget-object v1, p0, LX/DNJ;->k:LX/1CY;

    invoke-virtual {v1, p3, v0}, LX/1CY;->a(LX/0fz;LX/0g4;)V

    .line 1991325
    iget-object v0, p0, LX/DNJ;->F:LX/DNR;

    invoke-virtual {v0}, LX/DNR;->c()V

    .line 1991326
    return-void

    .line 1991327
    :cond_6
    new-instance v0, LX/DNV;

    iget-object v1, p0, LX/DNJ;->u:LX/1Cw;

    iget-object v2, p0, LX/DNJ;->w:LX/DRf;

    invoke-direct {v0, v1, v2}, LX/DNV;-><init>(LX/1Cw;LX/1Cw;)V

    iput-object v0, p0, LX/DNJ;->s:LX/DNU;

    goto :goto_0

    .line 1991328
    :cond_7
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    iget-object v1, p0, LX/DNJ;->v:LX/1Qq;

    invoke-virtual {v0, v1}, LX/2iI;->a(Landroid/widget/ListAdapter;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1991329
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 1991330
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DNJ;->v:LX/1Qq;

    if-eqz v0, :cond_0

    .line 1991331
    new-instance v0, LX/5Mj;

    iget-object v1, p0, LX/DNJ;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/5Mk;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/5Mj;-><init>(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;LX/5Mk;)V

    .line 1991332
    iget-object v1, v0, LX/5Mj;->f:LX/5Mk;

    move-object v1, v1

    .line 1991333
    iget-object v2, p0, LX/DNJ;->v:LX/1Qq;

    invoke-virtual {v1, v2, v0}, LX/5Mk;->a(Ljava/lang/Object;LX/5Mj;)V

    .line 1991334
    :cond_0
    return-void
.end method

.method public final b(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1991201
    invoke-super {p0, p1}, LX/1La;->b(Landroid/support/v4/app/Fragment;)V

    .line 1991202
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DNJ;->H:Z

    .line 1991203
    return-void
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1991174
    iget-object v0, p0, LX/DNJ;->F:LX/DNR;

    invoke-virtual {v0}, LX/DNR;->l()Z

    move-result v0

    iput-boolean v0, p0, LX/DNJ;->I:Z

    .line 1991175
    iget-object v3, p0, LX/DNJ;->C:LX/DNB;

    iget-boolean v4, p0, LX/DNJ;->I:Z

    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    invoke-virtual {v0}, LX/2iI;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v4, v0}, LX/DNB;->a(ZZ)V

    .line 1991176
    iget-object v0, p0, LX/DNJ;->F:LX/DNR;

    invoke-virtual {v0}, LX/DNR;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1991177
    if-nez p1, :cond_0

    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    invoke-virtual {v0}, LX/2iI;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1991178
    invoke-direct {p0, v2, v1}, LX/DNJ;->a(ZZ)V

    .line 1991179
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 1991180
    goto :goto_0

    .line 1991181
    :cond_2
    iget-object v0, p0, LX/DNJ;->y:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1991182
    if-eqz v0, :cond_3

    .line 1991183
    iget-object v0, p0, LX/DNJ;->y:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 1991184
    :cond_3
    const/4 v4, 0x0

    .line 1991185
    move v3, v4

    :goto_3
    iget-object v0, p0, LX/DNJ;->v:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 1991186
    iget-object v0, p0, LX/DNJ;->v:LX/1Qq;

    invoke-interface {v0, v3}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    invoke-virtual {v0}, LX/1Rk;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1991187
    instance-of p1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz p1, :cond_8

    .line 1991188
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object p1, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, p1, :cond_8

    .line 1991189
    const/4 v4, 0x1

    .line 1991190
    :cond_4
    move v0, v4

    .line 1991191
    if-nez v0, :cond_5

    .line 1991192
    invoke-direct {p0, v1, v2}, LX/DNJ;->a(ZZ)V

    goto :goto_1

    .line 1991193
    :cond_5
    invoke-direct {p0, v2, v2}, LX/DNJ;->a(ZZ)V

    .line 1991194
    iget-object v0, p0, LX/DNJ;->u:LX/1Cw;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/DNJ;->M:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/DNJ;->N:Z

    if-nez v0, :cond_0

    .line 1991195
    iput-boolean v1, p0, LX/DNJ;->N:Z

    .line 1991196
    iget-object v0, p0, LX/DNJ;->r:LX/88l;

    invoke-virtual {v0}, LX/88l;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/DNJ;->q:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-short v4, LX/88j;->d:S

    invoke-interface {v0, v3, v4, v1}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1991197
    :goto_4
    if-eqz v1, :cond_0

    .line 1991198
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    new-instance v1, Lcom/facebook/groups/feed/controller/GroupsFeedController$1;

    invoke-direct {v1, p0}, Lcom/facebook/groups/feed/controller/GroupsFeedController$1;-><init>(LX/DNJ;)V

    invoke-virtual {v0, v1}, LX/2iI;->a(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_6
    move v1, v2

    .line 1991199
    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    .line 1991200
    :cond_8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3
.end method

.method public final c(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 1991117
    invoke-static {p0}, LX/DNJ;->l(LX/DNJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1991118
    iget-object v0, p0, LX/DNJ;->l:LX/1Kt;

    iget-object v1, p0, LX/DNJ;->B:LX/2iI;

    invoke-virtual {v0, v1}, LX/1Kt;->c(LX/0g8;)V

    .line 1991119
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    .line 1991167
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/DNJ;->w:LX/DRf;

    if-nez v0, :cond_0

    .line 1991168
    iget-object v0, p0, LX/DNJ;->C:LX/DNB;

    invoke-interface {v0}, LX/DNB;->e()LX/DRf;

    move-result-object v0

    iput-object v0, p0, LX/DNJ;->w:LX/DRf;

    .line 1991169
    :cond_0
    iget-object v0, p0, LX/DNJ;->u:LX/1Cw;

    if-eqz v0, :cond_1

    .line 1991170
    if-nez p1, :cond_2

    .line 1991171
    iget-object v0, p0, LX/DNJ;->s:LX/DNU;

    iget-object v1, p0, LX/DNJ;->u:LX/1Cw;

    iget-object v2, p0, LX/DNJ;->v:LX/1Qq;

    invoke-virtual {v0, v1, v2}, LX/DNU;->a(LX/1Cw;LX/1Cw;)V

    .line 1991172
    :cond_1
    :goto_0
    return-void

    .line 1991173
    :cond_2
    iget-object v0, p0, LX/DNJ;->s:LX/DNU;

    iget-object v1, p0, LX/DNJ;->u:LX/1Cw;

    iget-object v2, p0, LX/DNJ;->w:LX/DRf;

    invoke-virtual {v0, v1, v2}, LX/DNU;->a(LX/1Cw;LX/1Cw;)V

    goto :goto_0
.end method

.method public final d(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 1991160
    invoke-static {p0}, LX/DNJ;->l(LX/DNJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1991161
    iget-object v0, p0, LX/DNJ;->l:LX/1Kt;

    const/4 v1, 0x1

    iget-object v2, p0, LX/DNJ;->B:LX/2iI;

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 1991162
    :cond_0
    iget-object v0, p0, LX/DNJ;->v:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 1991163
    iget-object v0, p0, LX/DNJ;->w:LX/DRf;

    if-eqz v0, :cond_1

    .line 1991164
    iget-object v0, p0, LX/DNJ;->w:LX/DRf;

    const v1, 0x211fc2d4

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1991165
    :cond_1
    iget-object v0, p0, LX/DNJ;->k:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->d()V

    .line 1991166
    return-void
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 1991158
    iget-object v0, p0, LX/DNJ;->F:LX/DNR;

    invoke-virtual {v0, p1}, LX/DNR;->a(Z)V

    .line 1991159
    return-void
.end method

.method public final e(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 1991140
    const/4 p1, 0x0

    .line 1991141
    iget-object v0, p0, LX/DNJ;->a:LX/DCI;

    invoke-virtual {v0}, LX/DCI;->a()V

    .line 1991142
    iget-object v0, p0, LX/DNJ;->b:LX/DCL;

    invoke-virtual {v0}, LX/DCL;->a()V

    .line 1991143
    iget-object v0, p0, LX/DNJ;->c:LX/DCE;

    invoke-virtual {v0}, LX/DCE;->a()V

    .line 1991144
    iget-object v0, p0, LX/DNJ;->d:LX/DCC;

    invoke-virtual {v0}, LX/DCC;->a()V

    .line 1991145
    iget-object v0, p0, LX/DNJ;->e:LX/DCO;

    invoke-virtual {v0}, LX/DCO;->a()V

    .line 1991146
    iget-object v0, p0, LX/DNJ;->o:LX/1B1;

    iget-object v1, p0, LX/DNJ;->p:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 1991147
    iget-object v0, p0, LX/DNJ;->k:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->e()V

    .line 1991148
    iget-object v0, p0, LX/DNJ;->g:LX/DCQ;

    invoke-virtual {v0}, LX/DCQ;->a()V

    .line 1991149
    iget-object v0, p0, LX/DNJ;->s:LX/DNU;

    if-eqz v0, :cond_0

    .line 1991150
    iget-object v0, p0, LX/DNJ;->s:LX/DNU;

    invoke-virtual {v0}, LX/DNU;->a()V

    .line 1991151
    :cond_0
    iget-object v0, p0, LX/DNJ;->y:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_1

    .line 1991152
    iget-object v0, p0, LX/DNJ;->y:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 1991153
    iput-object p1, p0, LX/DNJ;->y:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 1991154
    :cond_1
    iget-object v0, p0, LX/DNJ;->D:LX/0Yb;

    if-eqz v0, :cond_2

    .line 1991155
    iget-object v0, p0, LX/DNJ;->D:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1991156
    iput-object p1, p0, LX/DNJ;->D:LX/0Yb;

    .line 1991157
    :cond_2
    return-void
.end method

.method public final f()Lcom/facebook/widget/listview/BetterListView;
    .locals 1

    .prologue
    .line 1991139
    iget-object v0, p0, LX/DNJ;->B:LX/2iI;

    invoke-virtual {v0}, LX/2iI;->c()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v0

    return-object v0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1991133
    iget-object v0, p0, LX/DNJ;->h:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1991134
    iget-object v0, p0, LX/DNJ;->A:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1991135
    iget-object v0, p0, LX/DNJ;->A:Landroid/view/View;

    const v1, 0x7f0d08bd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/4nk;

    .line 1991136
    if-eqz v0, :cond_0

    .line 1991137
    iget-object v1, p0, LX/DNJ;->E:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/4nk;->a(Ljava/lang/String;Z)V

    .line 1991138
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 1991123
    iget-object v0, p0, LX/DNJ;->v:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 1991124
    iget-object v0, p0, LX/DNJ;->w:LX/DRf;

    if-eqz v0, :cond_0

    .line 1991125
    iget-object v0, p0, LX/DNJ;->w:LX/DRf;

    const v1, 0x2a76936

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1991126
    :cond_0
    iget-object v0, p0, LX/DNJ;->F:LX/DNR;

    .line 1991127
    iget-object v1, v0, LX/DNR;->c:LX/0fz;

    move-object v0, v1

    .line 1991128
    iget-object v1, p0, LX/DNJ;->k:LX/1CY;

    .line 1991129
    iget-object v2, v0, LX/0fz;->d:LX/0qm;

    move-object v2, v2

    .line 1991130
    invoke-virtual {v2}, LX/0qm;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1CY;->a(Ljava/lang/Iterable;)V

    .line 1991131
    iget-object v1, p0, LX/DNJ;->k:LX/1CY;

    invoke-virtual {v1, v0}, LX/1CY;->a(Ljava/lang/Iterable;)V

    .line 1991132
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1991121
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DNJ;->d(Z)V

    .line 1991122
    return-void
.end method

.method public final w_(I)I
    .locals 1

    .prologue
    .line 1991120
    iget-object v0, p0, LX/DNJ;->v:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->h_(I)I

    move-result v0

    return v0
.end method
