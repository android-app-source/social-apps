.class public final LX/DLr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OriginalPostModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1989099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v1, 0x0

    const/4 v11, 0x0

    .line 1989100
    new-instance v0, LX/186;

    const/16 v2, 0x80

    invoke-direct {v0, v2}, LX/186;-><init>(I)V

    .line 1989101
    iget-object v2, p0, LX/DLr;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1989102
    iget-object v2, p0, LX/DLr;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1989103
    iget-object v2, p0, LX/DLr;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1989104
    iget-object v2, p0, LX/DLr;->e:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OriginalPostModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1989105
    iget-object v2, p0, LX/DLr;->f:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel$OwnerModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1989106
    const/4 v2, 0x6

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1989107
    iget-wide v2, p0, LX/DLr;->a:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1989108
    invoke-virtual {v0, v12, v6}, LX/186;->b(II)V

    .line 1989109
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1989110
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1989111
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1989112
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1989113
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    .line 1989114
    invoke-virtual {v0, v2}, LX/186;->d(I)V

    .line 1989115
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1989116
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1989117
    new-instance v0, LX/15i;

    move-object v1, v2

    move-object v2, v11

    move-object v3, v11

    move v4, v12

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1989118
    new-instance v1, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-direct {v1, v0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;-><init>(LX/15i;)V

    .line 1989119
    return-object v1
.end method
