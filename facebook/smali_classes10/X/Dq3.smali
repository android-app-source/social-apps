.class public final LX/Dq3;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/notifications/protocol/NotifOptionRowsMutationModels$NotifOptionActionMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/33W;

.field private final b:LX/DqD;


# direct methods
.method public constructor <init>(LX/33W;LX/DqD;)V
    .locals 0

    .prologue
    .line 2047815
    iput-object p1, p0, LX/Dq3;->a:LX/33W;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2047816
    iput-object p2, p0, LX/Dq3;->b:LX/DqD;

    .line 2047817
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2047818
    iget-object v0, p0, LX/Dq3;->b:LX/DqD;

    .line 2047819
    iget-object p1, v0, LX/DqD;->a:LX/Dq2;

    move-object v0, p1

    .line 2047820
    if-eqz v0, :cond_0

    .line 2047821
    iget-object v0, p0, LX/Dq3;->b:LX/DqD;

    .line 2047822
    iget-object p0, v0, LX/DqD;->a:LX/Dq2;

    move-object v0, p0

    .line 2047823
    invoke-interface {v0}, LX/Dq2;->a()V

    .line 2047824
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2047825
    iget-object v0, p0, LX/Dq3;->b:LX/DqD;

    .line 2047826
    iget-object v1, v0, LX/DqD;->f:LX/2nq;

    move-object v0, v1

    .line 2047827
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dq3;->b:LX/DqD;

    .line 2047828
    iget-object v1, v0, LX/DqD;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2047829
    if-eqz v0, :cond_0

    .line 2047830
    iget-object v0, p0, LX/Dq3;->a:LX/33W;

    iget-object v6, v0, LX/33W;->g:LX/33Z;

    new-instance v0, LX/CSA;

    iget-object v1, p0, LX/Dq3;->b:LX/DqD;

    .line 2047831
    iget-object v2, v1, LX/DqD;->f:LX/2nq;

    move-object v1, v2

    .line 2047832
    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iget-object v2, p0, LX/Dq3;->b:LX/DqD;

    .line 2047833
    iget-object v3, v2, LX/DqD;->g:Ljava/lang/String;

    move-object v2, v3

    .line 2047834
    iget-object v3, p0, LX/Dq3;->b:LX/DqD;

    .line 2047835
    iget-object v4, v3, LX/DqD;->i:Ljava/lang/String;

    move-object v3, v4

    .line 2047836
    iget-object v4, p0, LX/Dq3;->b:LX/DqD;

    .line 2047837
    iget-object v5, v4, LX/DqD;->j:Ljava/lang/String;

    move-object v4, v5

    .line 2047838
    iget-object v5, p0, LX/Dq3;->b:LX/DqD;

    .line 2047839
    iget-object p1, v5, LX/DqD;->d:Ljava/lang/String;

    move-object v5, p1

    .line 2047840
    invoke-direct/range {v0 .. v5}, LX/CSA;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 2047841
    :cond_0
    iget-object v0, p0, LX/Dq3;->b:LX/DqD;

    .line 2047842
    iget-object v1, v0, LX/DqD;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2047843
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2047844
    iget-object v0, p0, LX/Dq3;->a:LX/33W;

    iget-object v0, v0, LX/33W;->g:LX/33Z;

    new-instance v1, LX/CS8;

    iget-object v2, p0, LX/Dq3;->b:LX/DqD;

    .line 2047845
    iget-object v3, v2, LX/DqD;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2047846
    invoke-direct {v1, v2}, LX/CS8;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2047847
    :cond_1
    return-void
.end method
