.class public final LX/EP8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/CxP;

.field public final synthetic c:LX/EPA;


# direct methods
.method public constructor <init>(LX/EPA;LX/CzL;LX/CxP;)V
    .locals 0

    .prologue
    .line 2115983
    iput-object p1, p0, LX/EP8;->c:LX/EPA;

    iput-object p2, p0, LX/EP8;->a:LX/CzL;

    iput-object p3, p0, LX/EP8;->b:LX/CxP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x136b1d44

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2115984
    iget-object v0, p0, LX/EP8;->a:LX/CzL;

    .line 2115985
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2115986
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->r()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v3

    .line 2115987
    iget-object v0, p0, LX/EP8;->b:LX/CxP;

    check-cast v0, LX/Cxh;

    iget-object v1, p0, LX/EP8;->a:LX/CzL;

    iget-object v2, p0, LX/EP8;->a:LX/CzL;

    .line 2115988
    iget v4, v2, LX/CzL;->b:I

    move v2, v4

    .line 2115989
    invoke-virtual {v1, v3, v2}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Cxh;->c(LX/CzL;)V

    .line 2115990
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2115991
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2115992
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2115993
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2115994
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EP8;->c:LX/EPA;

    iget-object v0, v0, LX/EPA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EJ5;

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->O()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bi()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, LX/EP8;->b:LX/CxP;

    check-cast v6, LX/CxV;

    invoke-virtual/range {v0 .. v6}, LX/EJ5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CxV;)V

    .line 2115995
    const v0, 0xfd169db

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move-object v4, v5

    goto :goto_0
.end method
