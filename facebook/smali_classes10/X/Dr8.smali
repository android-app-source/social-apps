.class public LX/Dr8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Dr8;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0rq;

.field public final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Ck;LX/0rq;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048989
    iput-object p3, p0, LX/Dr8;->a:LX/0tX;

    .line 2048990
    iput-object p2, p0, LX/Dr8;->b:LX/0rq;

    .line 2048991
    iput-object p1, p0, LX/Dr8;->c:LX/1Ck;

    .line 2048992
    return-void
.end method

.method public static a(LX/0QB;)LX/Dr8;
    .locals 6

    .prologue
    .line 2048993
    sget-object v0, LX/Dr8;->d:LX/Dr8;

    if-nez v0, :cond_1

    .line 2048994
    const-class v1, LX/Dr8;

    monitor-enter v1

    .line 2048995
    :try_start_0
    sget-object v0, LX/Dr8;->d:LX/Dr8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2048996
    if-eqz v2, :cond_0

    .line 2048997
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2048998
    new-instance p0, LX/Dr8;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v4

    check-cast v4, LX/0rq;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-direct {p0, v3, v4, v5}, LX/Dr8;-><init>(LX/1Ck;LX/0rq;LX/0tX;)V

    .line 2048999
    move-object v0, p0

    .line 2049000
    sput-object v0, LX/Dr8;->d:LX/Dr8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049001
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2049002
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2049003
    :cond_1
    sget-object v0, LX/Dr8;->d:LX/Dr8;

    return-object v0

    .line 2049004
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2049005
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
