.class public final LX/EZQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWW;


# static fields
.field public static final a:LX/EZQ;

.field private static final c:LX/EZP;


# instance fields
.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/EZO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2139390
    new-instance v0, LX/EZQ;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EZQ;-><init>(Ljava/util/Map;)V

    sput-object v0, LX/EZQ;->a:LX/EZQ;

    .line 2139391
    new-instance v0, LX/EZP;

    invoke-direct {v0}, LX/EZP;-><init>()V

    sput-object v0, LX/EZQ;->c:LX/EZP;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2139477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/EZO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2139474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2139475
    iput-object p1, p0, LX/EZQ;->b:Ljava/util/Map;

    .line 2139476
    return-void
.end method

.method public static a(LX/EZQ;)LX/EZM;
    .locals 1

    .prologue
    .line 2139473
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EZM;->a(LX/EZQ;)LX/EZM;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/EWf;)V
    .locals 7

    .prologue
    .line 2139457
    iget-object v0, p0, LX/EZQ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2139458
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EZO;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2139459
    iget-object v3, v1, LX/EZO;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 2139460
    invoke-virtual {p1, v0, v5, v6}, LX/EWf;->a(IJ)V

    goto :goto_1

    .line 2139461
    :cond_0
    iget-object v3, v1, LX/EZO;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2139462
    const/4 v5, 0x5

    invoke-virtual {p1, v0, v5}, LX/EWf;->i(II)V

    .line 2139463
    invoke-static {p1, v3}, LX/EWf;->o(LX/EWf;I)V

    .line 2139464
    goto :goto_2

    .line 2139465
    :cond_1
    iget-object v3, v1, LX/EZO;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 2139466
    invoke-virtual {p1, v0, v5, v6}, LX/EWf;->c(IJ)V

    goto :goto_3

    .line 2139467
    :cond_2
    iget-object v3, v1, LX/EZO;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EWc;

    .line 2139468
    invoke-virtual {p1, v0, v3}, LX/EWf;->a(ILX/EWc;)V

    goto :goto_4

    .line 2139469
    :cond_3
    iget-object v3, v1, LX/EZO;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EZQ;

    .line 2139470
    invoke-virtual {p1, v0, v3}, LX/EWf;->a(ILX/EWW;)V

    goto :goto_5

    .line 2139471
    :cond_4
    goto/16 :goto_0

    .line 2139472
    :cond_5
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2139456
    const/4 v0, 0x1

    return v0
.end method

.method public final b()I
    .locals 10

    .prologue
    .line 2139429
    const/4 v0, 0x0

    .line 2139430
    iget-object v1, p0, LX/EZQ;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2139431
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EZO;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2139432
    const/4 v4, 0x0

    .line 2139433
    iget-object v5, v1, LX/EZO;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v5, v4

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 2139434
    invoke-static {v0, v8, v9}, LX/EWf;->d(IJ)I

    move-result v4

    add-int/2addr v4, v5

    move v5, v4

    .line 2139435
    goto :goto_1

    .line 2139436
    :cond_0
    iget-object v4, v1, LX/EZO;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    .line 2139437
    invoke-static {v0}, LX/EWf;->k(I)I

    move-result v7

    .line 2139438
    const/4 v8, 0x4

    move v8, v8

    .line 2139439
    add-int/2addr v7, v8

    move v4, v7

    .line 2139440
    add-int/2addr v5, v4

    .line 2139441
    goto :goto_2

    .line 2139442
    :cond_1
    iget-object v4, v1, LX/EZO;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 2139443
    invoke-static {v0, v8, v9}, LX/EWf;->f(IJ)I

    move-result v4

    add-int/2addr v5, v4

    .line 2139444
    goto :goto_3

    .line 2139445
    :cond_2
    iget-object v4, v1, LX/EZO;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EWc;

    .line 2139446
    invoke-static {v0, v4}, LX/EWf;->c(ILX/EWc;)I

    move-result v4

    add-int/2addr v5, v4

    .line 2139447
    goto :goto_4

    .line 2139448
    :cond_3
    iget-object v4, v1, LX/EZO;->f:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EZQ;

    .line 2139449
    invoke-static {v0}, LX/EWf;->k(I)I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    invoke-static {v4}, LX/EWf;->c(LX/EWW;)I

    move-result v8

    add-int/2addr v7, v8

    move v4, v7

    .line 2139450
    add-int/2addr v5, v4

    .line 2139451
    goto :goto_5

    .line 2139452
    :cond_4
    move v0, v5

    .line 2139453
    add-int/2addr v0, v2

    move v2, v0

    .line 2139454
    goto/16 :goto_0

    .line 2139455
    :cond_5
    return v2
.end method

.method public final b(LX/EWf;)V
    .locals 4

    .prologue
    .line 2139423
    iget-object v0, p0, LX/EZQ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2139424
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EZO;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2139425
    iget-object v3, v1, LX/EZO;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EWc;

    .line 2139426
    invoke-virtual {p1, v0, v3}, LX/EWf;->b(ILX/EWc;)V

    goto :goto_1

    .line 2139427
    :cond_0
    goto :goto_0

    .line 2139428
    :cond_1
    return-void
.end method

.method public final d()LX/EWc;
    .locals 3

    .prologue
    .line 2139417
    :try_start_0
    invoke-virtual {p0}, LX/EZQ;->b()I

    move-result v0

    invoke-static {v0}, LX/EWc;->b(I)LX/EWb;

    move-result-object v0

    .line 2139418
    iget-object v1, v0, LX/EWb;->a:LX/EWf;

    move-object v1, v1

    .line 2139419
    invoke-virtual {p0, v1}, LX/EZQ;->a(LX/EWf;)V

    .line 2139420
    invoke-virtual {v0}, LX/EWb;->a()LX/EWc;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2139421
    :catch_0
    move-exception v0

    .line 2139422
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a ByteString threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2139415
    if-ne p0, p1, :cond_1

    .line 2139416
    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, LX/EZQ;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/EZQ;->b:Ljava/util/Map;

    check-cast p1, LX/EZQ;

    iget-object v2, p1, LX/EZQ;->b:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 7

    .prologue
    .line 2139403
    const/4 v0, 0x0

    .line 2139404
    iget-object v1, p0, LX/EZQ;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2139405
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EZO;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2139406
    const/4 v4, 0x0

    .line 2139407
    iget-object v5, v1, LX/EZO;->e:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v5, v4

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EWc;

    .line 2139408
    const/4 p0, 0x1

    invoke-static {p0}, LX/EWf;->k(I)I

    move-result p0

    mul-int/lit8 p0, p0, 0x2

    const/4 v1, 0x2

    invoke-static {v1, v0}, LX/EWf;->g(II)I

    move-result v1

    add-int/2addr p0, v1

    const/4 v1, 0x3

    invoke-static {v1, v4}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr p0, v1

    move v4, p0

    .line 2139409
    add-int/2addr v4, v5

    move v5, v4

    .line 2139410
    goto :goto_1

    .line 2139411
    :cond_0
    move v0, v5

    .line 2139412
    add-int/2addr v0, v2

    move v2, v0

    .line 2139413
    goto :goto_0

    .line 2139414
    :cond_1
    return v2
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2139402
    iget-object v0, p0, LX/EZQ;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()LX/EWZ;
    .locals 1

    .prologue
    .line 2139401
    sget-object v0, LX/EZQ;->c:LX/EZP;

    return-object v0
.end method

.method public final jZ_()[B
    .locals 3

    .prologue
    .line 2139394
    :try_start_0
    invoke-virtual {p0}, LX/EZQ;->b()I

    move-result v0

    new-array v0, v0, [B

    .line 2139395
    invoke-static {v0}, LX/EWf;->a([B)LX/EWf;

    move-result-object v1

    .line 2139396
    invoke-virtual {p0, v1}, LX/EZQ;->a(LX/EWf;)V

    .line 2139397
    invoke-virtual {v1}, LX/EWf;->h()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2139398
    return-object v0

    .line 2139399
    :catch_0
    move-exception v0

    .line 2139400
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2139393
    invoke-static {p0}, LX/EZK;->a(LX/EZQ;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()LX/EWR;
    .locals 1

    .prologue
    .line 2139392
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EZM;->a(LX/EZQ;)LX/EZM;

    move-result-object v0

    return-object v0
.end method
