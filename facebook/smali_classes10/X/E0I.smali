.class public final LX/E0I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/home/HomeActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 2068185
    iput-object p1, p0, LX/E0I;->a:Lcom/facebook/places/create/home/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x2aa00def

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2068186
    iget-object v1, p0, LX/E0I;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068187
    iget-object v2, v1, LX/E0T;->a:LX/0Zb;

    const-string p1, "home_%s_camera_icon_tapped"

    invoke-static {v1, p1}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v2, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068188
    iget-object v1, p0, LX/E0I;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->i:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 2068189
    iget-object v1, p0, LX/E0I;->a:Lcom/facebook/places/create/home/HomeActivity;

    .line 2068190
    invoke-virtual {v1}, Lcom/facebook/places/create/home/HomeActivity;->p()V

    .line 2068191
    invoke-virtual {v1}, Lcom/facebook/places/create/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance p0, LX/8AA;

    sget-object p1, LX/8AB;->PLACES_HOME:LX/8AB;

    invoke-direct {p0, p1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {p0}, LX/8AA;->i()LX/8AA;

    move-result-object p0

    invoke-virtual {p0}, LX/8AA;->j()LX/8AA;

    move-result-object p0

    sget-object p1, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {p0, p1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object p0

    invoke-static {v2, p0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v2

    .line 2068192
    iget-object p0, v1, Lcom/facebook/places/create/home/HomeActivity;->u:Lcom/facebook/content/SecureContextHelper;

    const/16 p1, 0xb

    invoke-interface {p0, v2, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2068193
    :goto_0
    const v1, -0x4496cc07

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2068194
    :cond_0
    iget-object v1, p0, LX/E0I;->a:Lcom/facebook/places/create/home/HomeActivity;

    invoke-static {v1}, Lcom/facebook/places/create/home/HomeActivity;->w(Lcom/facebook/places/create/home/HomeActivity;)V

    goto :goto_0
.end method
