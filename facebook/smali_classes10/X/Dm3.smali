.class public final LX/Dm3;
.super LX/Dm1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Dm1",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Landroid/view/View;ZLX/0wM;)V
    .locals 0

    .prologue
    .line 2038840
    iput-object p1, p0, LX/Dm3;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    invoke-direct {p0, p2, p3, p4}, LX/Dm1;-><init>(Landroid/view/View;ZLX/0wM;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 3

    .prologue
    .line 2038848
    invoke-static {p1}, LX/DnS;->f(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;

    move-result-object v0

    .line 2038849
    iput-object v0, p0, LX/Dm1;->l:Ljava/lang/Object;

    .line 2038850
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2038851
    const v1, 0x7f020956

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, LX/Dlw;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2038852
    return-void
.end method

.method public bridge synthetic onClick(Landroid/view/View;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2038841
    check-cast p2, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;

    .line 2038842
    iget-object v0, p0, LX/Dm3;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->f:LX/Dih;

    iget-object v1, p0, LX/Dm3;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v1}, LX/DnS;->g(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Dm3;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v2}, LX/DnS;->h(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Dm3;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->n:Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;

    invoke-static {v3}, LX/DnS;->i(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v3

    .line 2038843
    iget-object v4, v0, LX/Dih;->a:LX/0Zb;

    const-string v5, "profservices_booking_consumer_click_call"

    invoke-static {v5, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "request_id"

    invoke-virtual {v5, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "booking_status"

    invoke-virtual {v5, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2038844
    invoke-virtual {p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$PageModel$AllPhonesModel$PhoneNumberModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2038845
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tel:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2038846
    iget-object v1, p0, LX/Dm3;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Dm3;->l:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2038847
    return-void
.end method
