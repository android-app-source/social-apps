.class public LX/DBA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/events/model/Event;

.field private b:Lcom/facebook/events/common/EventAnalyticsParams;

.field private c:Landroid/content/Context;

.field private d:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1972340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1972341
    iput-object p1, p0, LX/DBA;->c:Landroid/content/Context;

    .line 1972342
    iput-object p2, p0, LX/DBA;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1972343
    return-void
.end method

.method public static a(LX/0QB;)LX/DBA;
    .locals 1

    .prologue
    .line 1972327
    invoke-static {p0}, LX/DBA;->b(LX/0QB;)LX/DBA;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/DBA;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;ZZ)V
    .locals 4
    .param p1    # Lcom/facebook/events/common/ActionMechanism;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1972330
    iget-object v0, p0, LX/DBA;->a:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DBA;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v0, :cond_1

    .line 1972331
    :cond_0
    :goto_0
    return-void

    .line 1972332
    :cond_1
    iget-object v0, p0, LX/DBA;->c:Landroid/content/Context;

    iget-object v1, p0, LX/DBA;->a:Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/DBA;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-static {v0, v1, p2, v2, p1}, Lcom/facebook/events/create/EventEditNikumanActivity;->a(Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;

    move-result-object v1

    .line 1972333
    if-eqz p3, :cond_2

    .line 1972334
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1972335
    :cond_2
    if-eqz p4, :cond_3

    .line 1972336
    iget-object v0, p0, LX/DBA;->c:Landroid/content/Context;

    const-class v2, Landroid/app/Activity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1972337
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1972338
    iget-object v2, p0, LX/DBA;->d:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x6b

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    .line 1972339
    :cond_3
    iget-object v0, p0, LX/DBA;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DBA;->c:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/DBA;
    .locals 3

    .prologue
    .line 1972344
    new-instance v2, LX/DBA;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/DBA;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 1972345
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/events/common/ActionMechanism;Z)V
    .locals 2

    .prologue
    .line 1972328
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, LX/DBA;->a(LX/DBA;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;ZZ)V

    .line 1972329
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 0

    .prologue
    .line 1972324
    iput-object p1, p0, LX/DBA;->a:Lcom/facebook/events/model/Event;

    .line 1972325
    iput-object p2, p0, LX/DBA;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1972326
    return-void
.end method
