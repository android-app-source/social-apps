.class public final LX/DMj;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/events/GroupEventsTabFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/events/GroupEventsTabFragment;)V
    .locals 0

    .prologue
    .line 1990133
    iput-object p1, p0, LX/DMj;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1990134
    iget-object v0, p0, LX/DMj;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    iget-object v0, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/groups/events/GroupEventsTabFragment;->h:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to fetch group name information for group :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/DMj;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    iget-object v3, v3, Lcom/facebook/groups/events/GroupEventsTabFragment;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1990135
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1990136
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1990137
    iget-object v1, p0, LX/DMj;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    .line 1990138
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1990139
    check-cast v0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    .line 1990140
    iput-object v0, v1, Lcom/facebook/groups/events/GroupEventsTabFragment;->j:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    .line 1990141
    iget-object v0, p0, LX/DMj;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    iget-object v0, v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->d:LX/DZ8;

    iget-object v1, p0, LX/DMj;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    iget-object v2, p0, LX/DMj;->a:Lcom/facebook/groups/events/GroupEventsTabFragment;

    iget-object v2, v2, Lcom/facebook/groups/events/GroupEventsTabFragment;->m:LX/DLO;

    invoke-virtual {v0, v1, v2}, LX/DZ8;->a(Lcom/facebook/base/fragment/FbFragment;LX/DLO;)V

    .line 1990142
    return-void
.end method
