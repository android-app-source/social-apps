.class public LX/Eoc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Sg;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0rq;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0tX;

.field public final d:LX/BS3;

.field public final e:LX/EpA;

.field public final f:LX/7H7;

.field private final g:LX/0ad;

.field public final h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sg;LX/0Or;LX/0tX;LX/BS3;LX/EpA;LX/7H7;LX/0ad;LX/0am;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0
    .param p8    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/0Or",
            "<",
            "LX/0rq;",
            ">;",
            "LX/0tX;",
            "LX/BS3;",
            "LX/EpA;",
            "LX/7H7;",
            "LX/0ad;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2168313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2168314
    iput-object p1, p0, LX/Eoc;->a:LX/0Sg;

    .line 2168315
    iput-object p2, p0, LX/Eoc;->b:LX/0Or;

    .line 2168316
    iput-object p3, p0, LX/Eoc;->c:LX/0tX;

    .line 2168317
    iput-object p4, p0, LX/Eoc;->d:LX/BS3;

    .line 2168318
    iput-object p5, p0, LX/Eoc;->e:LX/EpA;

    .line 2168319
    iput-object p6, p0, LX/Eoc;->f:LX/7H7;

    .line 2168320
    iput-object p7, p0, LX/Eoc;->g:LX/0ad;

    .line 2168321
    iput-object p8, p0, LX/Eoc;->h:LX/0am;

    .line 2168322
    iput-object p9, p0, LX/Eoc;->i:Lcom/facebook/common/callercontext/CallerContext;

    .line 2168323
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;>;"
        }
    .end annotation

    .prologue
    .line 2168324
    new-instance v5, LX/0v6;

    const-string v3, "DefaultPersonCard"

    invoke-direct {v5, v3}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2168325
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2168326
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_0

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2168327
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v8

    .line 2168328
    if-nez v8, :cond_1

    .line 2168329
    sget-object v8, LX/0wB;->a:LX/0wC;

    move-object v9, v8

    .line 2168330
    :goto_1
    iget-object v8, p0, LX/Eoc;->b:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0rq;

    .line 2168331
    new-instance v10, LX/Eol;

    invoke-direct {v10}, LX/Eol;-><init>()V

    move-object v10, v10

    .line 2168332
    const-string v11, "id"

    invoke-virtual {v10, v11, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v11

    const-string v12, "context_items_num"

    const-string v13, "3"

    invoke-virtual {v11, v12, v13}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v11

    const-string v12, "icon_scale"

    invoke-virtual {v11, v12, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v9

    const-string v11, "context_item_image_size"

    iget-object v12, p0, LX/Eoc;->e:LX/EpA;

    invoke-virtual {v12}, LX/EpA;->b()I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v11

    const-string v12, "context_item_type"

    iget-object v9, p0, LX/Eoc;->h:LX/0am;

    invoke-virtual {v9}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v11, v12, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v11, "cover_image_portrait_size"

    iget-object v12, p0, LX/Eoc;->f:LX/7H7;

    invoke-virtual {v12}, LX/7H7;->b()I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v11, "media_type"

    invoke-virtual {v8}, LX/0rq;->a()LX/0wF;

    move-result-object v12

    invoke-virtual {v9, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v9

    const-string v11, "profile_pic_media_type"

    invoke-virtual {v8}, LX/0rq;->b()LX/0wF;

    move-result-object v8

    invoke-virtual {v9, v11, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v8

    const-string v9, "profile_image_size"

    iget-object v11, p0, LX/Eoc;->e:LX/EpA;

    invoke-virtual {v11}, LX/EpA;->b()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2168333
    iget-object v8, p0, LX/Eoc;->d:LX/BS3;

    invoke-virtual {v8, v10}, LX/BS3;->a(LX/0gW;)V

    .line 2168334
    invoke-static {v10}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    sget-object v9, LX/0zS;->a:LX/0zS;

    invoke-virtual {v8, v9}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v8

    iget-object v9, p0, LX/Eoc;->i:Lcom/facebook/common/callercontext/CallerContext;

    .line 2168335
    iput-object v9, v8, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2168336
    move-object v8, v8

    .line 2168337
    sget-object v9, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v8, v9}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v8

    const-wide/16 v10, 0xe10

    invoke-virtual {v8, v10, v11}, LX/0zO;->a(J)LX/0zO;

    move-result-object v8

    const/4 v9, 0x1

    .line 2168338
    iput-boolean v9, v8, LX/0zO;->p:Z

    .line 2168339
    move-object v8, v8

    .line 2168340
    move-object v3, v8

    .line 2168341
    invoke-virtual {v5, v3}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2168342
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_0

    .line 2168343
    :cond_0
    iget-object v3, p0, LX/Eoc;->c:LX/0tX;

    invoke-virtual {v3, v5}, LX/0tX;->a(LX/0v6;)V

    .line 2168344
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 2168345
    invoke-static {v6}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/Eob;

    invoke-direct {v5, p0, v3}, LX/Eob;-><init>(LX/Eoc;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2168346
    move-object v0, v3

    .line 2168347
    iget-object v1, p0, LX/Eoc;->a:LX/0Sg;

    invoke-virtual {v1, v0}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2168348
    new-instance v1, LX/Eoa;

    invoke-direct {v1, p0}, LX/Eoa;-><init>(LX/Eoc;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v9, v8

    goto/16 :goto_1
.end method
