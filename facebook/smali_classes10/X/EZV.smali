.class public enum LX/EZV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EZV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EZV;

.field public static final enum BOOL:LX/EZV;

.field public static final enum BYTES:LX/EZV;

.field public static final enum DOUBLE:LX/EZV;

.field public static final enum ENUM:LX/EZV;

.field public static final enum FIXED32:LX/EZV;

.field public static final enum FIXED64:LX/EZV;

.field public static final enum FLOAT:LX/EZV;

.field public static final enum GROUP:LX/EZV;

.field public static final enum INT32:LX/EZV;

.field public static final enum INT64:LX/EZV;

.field public static final enum MESSAGE:LX/EZV;

.field public static final enum SFIXED32:LX/EZV;

.field public static final enum SFIXED64:LX/EZV;

.field public static final enum SINT32:LX/EZV;

.field public static final enum SINT64:LX/EZV;

.field public static final enum STRING:LX/EZV;

.field public static final enum UINT32:LX/EZV;

.field public static final enum UINT64:LX/EZV;


# instance fields
.field private final javaType:LX/EZa;

.field private final wireType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2139545
    new-instance v0, LX/EZV;

    const-string v1, "DOUBLE"

    sget-object v2, LX/EZa;->DOUBLE:LX/EZa;

    invoke-direct {v0, v1, v4, v2, v5}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->DOUBLE:LX/EZV;

    .line 2139546
    new-instance v0, LX/EZV;

    const-string v1, "FLOAT"

    sget-object v2, LX/EZa;->FLOAT:LX/EZa;

    invoke-direct {v0, v1, v5, v2, v7}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->FLOAT:LX/EZV;

    .line 2139547
    new-instance v0, LX/EZV;

    const-string v1, "INT64"

    sget-object v2, LX/EZa;->LONG:LX/EZa;

    invoke-direct {v0, v1, v6, v2, v4}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->INT64:LX/EZV;

    .line 2139548
    new-instance v0, LX/EZV;

    const-string v1, "UINT64"

    sget-object v2, LX/EZa;->LONG:LX/EZa;

    invoke-direct {v0, v1, v8, v2, v4}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->UINT64:LX/EZV;

    .line 2139549
    new-instance v0, LX/EZV;

    const-string v1, "INT32"

    const/4 v2, 0x4

    sget-object v3, LX/EZa;->INT:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v4}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->INT32:LX/EZV;

    .line 2139550
    new-instance v0, LX/EZV;

    const-string v1, "FIXED64"

    sget-object v2, LX/EZa;->LONG:LX/EZa;

    invoke-direct {v0, v1, v7, v2, v5}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->FIXED64:LX/EZV;

    .line 2139551
    new-instance v0, LX/EZV;

    const-string v1, "FIXED32"

    const/4 v2, 0x6

    sget-object v3, LX/EZa;->INT:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v7}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->FIXED32:LX/EZV;

    .line 2139552
    new-instance v0, LX/EZV;

    const-string v1, "BOOL"

    const/4 v2, 0x7

    sget-object v3, LX/EZa;->BOOLEAN:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v4}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->BOOL:LX/EZV;

    .line 2139553
    new-instance v0, LX/EZW;

    const-string v1, "STRING"

    const/16 v2, 0x8

    sget-object v3, LX/EZa;->STRING:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v6}, LX/EZW;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->STRING:LX/EZV;

    .line 2139554
    new-instance v0, LX/EZX;

    const-string v1, "GROUP"

    const/16 v2, 0x9

    sget-object v3, LX/EZa;->MESSAGE:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v8}, LX/EZX;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->GROUP:LX/EZV;

    .line 2139555
    new-instance v0, LX/EZY;

    const-string v1, "MESSAGE"

    const/16 v2, 0xa

    sget-object v3, LX/EZa;->MESSAGE:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v6}, LX/EZY;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->MESSAGE:LX/EZV;

    .line 2139556
    new-instance v0, LX/EZZ;

    const-string v1, "BYTES"

    const/16 v2, 0xb

    sget-object v3, LX/EZa;->BYTE_STRING:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v6}, LX/EZZ;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->BYTES:LX/EZV;

    .line 2139557
    new-instance v0, LX/EZV;

    const-string v1, "UINT32"

    const/16 v2, 0xc

    sget-object v3, LX/EZa;->INT:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v4}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->UINT32:LX/EZV;

    .line 2139558
    new-instance v0, LX/EZV;

    const-string v1, "ENUM"

    const/16 v2, 0xd

    sget-object v3, LX/EZa;->ENUM:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v4}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->ENUM:LX/EZV;

    .line 2139559
    new-instance v0, LX/EZV;

    const-string v1, "SFIXED32"

    const/16 v2, 0xe

    sget-object v3, LX/EZa;->INT:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v7}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->SFIXED32:LX/EZV;

    .line 2139560
    new-instance v0, LX/EZV;

    const-string v1, "SFIXED64"

    const/16 v2, 0xf

    sget-object v3, LX/EZa;->LONG:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v5}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->SFIXED64:LX/EZV;

    .line 2139561
    new-instance v0, LX/EZV;

    const-string v1, "SINT32"

    const/16 v2, 0x10

    sget-object v3, LX/EZa;->INT:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v4}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->SINT32:LX/EZV;

    .line 2139562
    new-instance v0, LX/EZV;

    const-string v1, "SINT64"

    const/16 v2, 0x11

    sget-object v3, LX/EZa;->LONG:LX/EZa;

    invoke-direct {v0, v1, v2, v3, v4}, LX/EZV;-><init>(Ljava/lang/String;ILX/EZa;I)V

    sput-object v0, LX/EZV;->SINT64:LX/EZV;

    .line 2139563
    const/16 v0, 0x12

    new-array v0, v0, [LX/EZV;

    sget-object v1, LX/EZV;->DOUBLE:LX/EZV;

    aput-object v1, v0, v4

    sget-object v1, LX/EZV;->FLOAT:LX/EZV;

    aput-object v1, v0, v5

    sget-object v1, LX/EZV;->INT64:LX/EZV;

    aput-object v1, v0, v6

    sget-object v1, LX/EZV;->UINT64:LX/EZV;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, LX/EZV;->INT32:LX/EZV;

    aput-object v2, v0, v1

    sget-object v1, LX/EZV;->FIXED64:LX/EZV;

    aput-object v1, v0, v7

    const/4 v1, 0x6

    sget-object v2, LX/EZV;->FIXED32:LX/EZV;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EZV;->BOOL:LX/EZV;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EZV;->STRING:LX/EZV;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/EZV;->GROUP:LX/EZV;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/EZV;->MESSAGE:LX/EZV;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/EZV;->BYTES:LX/EZV;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/EZV;->UINT32:LX/EZV;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/EZV;->ENUM:LX/EZV;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/EZV;->SFIXED32:LX/EZV;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/EZV;->SFIXED64:LX/EZV;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/EZV;->SINT32:LX/EZV;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/EZV;->SINT64:LX/EZV;

    aput-object v2, v0, v1

    sput-object v0, LX/EZV;->$VALUES:[LX/EZV;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILX/EZa;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EZa;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2139540
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2139541
    iput-object p3, p0, LX/EZV;->javaType:LX/EZa;

    .line 2139542
    iput p4, p0, LX/EZV;->wireType:I

    .line 2139543
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EZV;
    .locals 1

    .prologue
    .line 2139538
    const-class v0, LX/EZV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EZV;

    return-object v0
.end method

.method public static values()[LX/EZV;
    .locals 1

    .prologue
    .line 2139544
    sget-object v0, LX/EZV;->$VALUES:[LX/EZV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EZV;

    return-object v0
.end method


# virtual methods
.method public getJavaType()LX/EZa;
    .locals 1

    .prologue
    .line 2139539
    iget-object v0, p0, LX/EZV;->javaType:LX/EZa;

    return-object v0
.end method

.method public getWireType()I
    .locals 1

    .prologue
    .line 2139537
    iget v0, p0, LX/EZV;->wireType:I

    return v0
.end method

.method public isPackable()Z
    .locals 1

    .prologue
    .line 2139536
    const/4 v0, 0x1

    return v0
.end method
