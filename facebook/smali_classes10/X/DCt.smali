.class public final LX/DCt;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3mc;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public final synthetic d:LX/3mc;


# direct methods
.method public constructor <init>(LX/3mc;)V
    .locals 1

    .prologue
    .line 1974648
    iput-object p1, p0, LX/DCt;->d:LX/3mc;

    .line 1974649
    move-object v0, p1

    .line 1974650
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1974651
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1974652
    const-string v0, "GroupsYouShouldJoinCoverItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1974653
    if-ne p0, p1, :cond_1

    .line 1974654
    :cond_0
    :goto_0
    return v0

    .line 1974655
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1974656
    goto :goto_0

    .line 1974657
    :cond_3
    check-cast p1, LX/DCt;

    .line 1974658
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1974659
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1974660
    if-eq v2, v3, :cond_0

    .line 1974661
    iget v2, p0, LX/DCt;->a:I

    iget v3, p1, LX/DCt;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1974662
    goto :goto_0

    .line 1974663
    :cond_4
    iget-object v2, p0, LX/DCt;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/DCt;->b:Ljava/lang/String;

    iget-object v3, p1, LX/DCt;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1974664
    goto :goto_0

    .line 1974665
    :cond_6
    iget-object v2, p1, LX/DCt;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1974666
    :cond_7
    iget-object v2, p0, LX/DCt;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DCt;->c:Ljava/lang/String;

    iget-object v3, p1, LX/DCt;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1974667
    goto :goto_0

    .line 1974668
    :cond_8
    iget-object v2, p1, LX/DCt;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
