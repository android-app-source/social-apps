.class public LX/ENh;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ENi;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ENh",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ENi;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113183
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2113184
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ENh;->b:LX/0Zi;

    .line 2113185
    iput-object p1, p0, LX/ENh;->a:LX/0Ot;

    .line 2113186
    return-void
.end method

.method public static a(LX/0QB;)LX/ENh;
    .locals 4

    .prologue
    .line 2113187
    const-class v1, LX/ENh;

    monitor-enter v1

    .line 2113188
    :try_start_0
    sget-object v0, LX/ENh;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113189
    sput-object v2, LX/ENh;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113190
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113191
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113192
    new-instance v3, LX/ENh;

    const/16 p0, 0x344a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ENh;-><init>(LX/0Ot;)V

    .line 2113193
    move-object v0, v3

    .line 2113194
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113195
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ENh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113196
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113197
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2113198
    check-cast p2, LX/ENg;

    .line 2113199
    iget-object v0, p0, LX/ENh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ENi;

    iget-object v1, p2, LX/ENg;->a:LX/CzL;

    iget-object v2, p2, LX/ENg;->c:Ljava/lang/CharSequence;

    iget v3, p2, LX/ENg;->d:I

    invoke-virtual {v0, p1, v1, v2, v3}, LX/ENi;->a(LX/1De;LX/CzL;Ljava/lang/CharSequence;I)LX/1Dg;

    move-result-object v0

    .line 2113200
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2113201
    invoke-static {}, LX/1dS;->b()V

    .line 2113202
    iget v0, p1, LX/1dQ;->b:I

    .line 2113203
    packed-switch v0, :pswitch_data_0

    .line 2113204
    :goto_0
    return-object v2

    .line 2113205
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2113206
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2113207
    check-cast v1, LX/ENg;

    .line 2113208
    iget-object v3, p0, LX/ENh;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ENi;

    iget-object p1, v1, LX/ENg;->a:LX/CzL;

    iget-object p2, v1, LX/ENg;->b:LX/1Ps;

    invoke-virtual {v3, p1, p2}, LX/ENi;->a(LX/CzL;LX/1Ps;)V

    .line 2113209
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x34d9eef4
        :pswitch_0
    .end packed-switch
.end method
