.class public LX/EuE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Eto;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Eu4;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2do;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2179233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179234
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179235
    iput-object v0, p0, LX/EuE;->a:LX/0Ot;

    .line 2179236
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179237
    iput-object v0, p0, LX/EuE;->b:LX/0Ot;

    .line 2179238
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179239
    iput-object v0, p0, LX/EuE;->c:LX/0Ot;

    .line 2179240
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2179241
    iput-object v0, p0, LX/EuE;->d:LX/0Ot;

    .line 2179242
    return-void
.end method

.method public static a(LX/0QB;)LX/EuE;
    .locals 7

    .prologue
    .line 2179243
    const-class v1, LX/EuE;

    monitor-enter v1

    .line 2179244
    :try_start_0
    sget-object v0, LX/EuE;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2179245
    sput-object v2, LX/EuE;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2179246
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2179247
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2179248
    new-instance v3, LX/EuE;

    invoke-direct {v3}, LX/EuE;-><init>()V

    .line 2179249
    const/16 v4, 0x2224

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x222a

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xa71

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0xa7c

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2179250
    iput-object v4, v3, LX/EuE;->a:LX/0Ot;

    iput-object v5, v3, LX/EuE;->b:LX/0Ot;

    iput-object v6, v3, LX/EuE;->c:LX/0Ot;

    iput-object p0, v3, LX/EuE;->d:LX/0Ot;

    .line 2179251
    move-object v0, v3

    .line 2179252
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2179253
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EuE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2179254
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2179255
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
