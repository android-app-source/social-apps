.class public LX/Cw1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Cw3;

.field private final b:J

.field private final c:J

.field private final d:J

.field private final e:Z


# direct methods
.method public constructor <init>(LX/Cw3;IIIZ)V
    .locals 2

    .prologue
    .line 1949832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949833
    iput-object p1, p0, LX/Cw1;->a:LX/Cw3;

    .line 1949834
    int-to-long v0, p2

    iput-wide v0, p0, LX/Cw1;->b:J

    .line 1949835
    int-to-long v0, p3

    iput-wide v0, p0, LX/Cw1;->c:J

    .line 1949836
    int-to-long v0, p4

    iput-wide v0, p0, LX/Cw1;->d:J

    .line 1949837
    iput-boolean p5, p0, LX/Cw1;->e:Z

    .line 1949838
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 6

    .prologue
    .line 1949839
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/Cvz;->RESULTS_LOAD:LX/Cvz;

    iget-object v1, v1, LX/Cvz;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "browse"

    .line 1949840
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1949841
    move-object v0, v0

    .line 1949842
    iget-object v1, p0, LX/Cw1;->a:LX/Cw3;

    .line 1949843
    const-string v2, "typeahead_sid"

    iget-object v3, v1, LX/Cw3;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "candidate_results_sid"

    iget-object v4, v1, LX/Cw3;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "serp_sid"

    iget-object v4, v1, LX/Cw3;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "source"

    iget-object v4, v1, LX/Cw3;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "referrer_surface"

    iget-object v4, v1, LX/Cw3;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "tab_name"

    iget-object v4, v1, LX/Cw3;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "browse_sid"

    iget-object v4, v1, LX/Cw3;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949844
    iget-object v2, v1, LX/Cw3;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1949845
    const-string v2, "module_source"

    iget-object v3, v1, LX/Cw3;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949846
    :cond_0
    const-string v1, "page_number"

    iget-wide v2, p0, LX/Cw1;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "results_count"

    iget-wide v4, p0, LX/Cw1;->c:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "retry_count"

    iget-wide v4, p0, LX/Cw1;->d:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "was_serp_empty"

    iget-boolean v3, p0, LX/Cw1;->e:Z

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949847
    return-object v0
.end method
