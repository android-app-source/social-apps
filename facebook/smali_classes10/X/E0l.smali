.class public final LX/E0l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/home/HomeUpdateParams;

.field public final synthetic b:LX/E0o;


# direct methods
.method public constructor <init>(LX/E0o;Lcom/facebook/places/create/home/HomeUpdateParams;)V
    .locals 0

    .prologue
    .line 2069006
    iput-object p1, p0, LX/E0l;->b:LX/E0o;

    iput-object p2, p0, LX/E0l;->a:Lcom/facebook/places/create/home/HomeUpdateParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2069007
    :try_start_0
    iget-object v0, p0, LX/E0l;->b:LX/E0o;

    iget-object v0, v0, LX/E0o;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/E0l;->b:LX/E0o;

    iget-object v1, v1, LX/E0o;->c:LX/E0j;

    iget-object v2, p0, LX/E0l;->a:Lcom/facebook/places/create/home/HomeUpdateParams;

    invoke-virtual {v0, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2069008
    :catch_0
    move-exception v0

    .line 2069009
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 2069010
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x96c

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 2069011
    new-instance v0, LX/E0n;

    invoke-direct {v0}, LX/E0n;-><init>()V

    throw v0

    .line 2069012
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v1

    int-to-long v2, v1

    const-wide/16 v4, 0x96d

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 2069013
    new-instance v0, LX/E0m;

    invoke-direct {v0}, LX/E0m;-><init>()V

    throw v0

    .line 2069014
    :cond_1
    throw v0
.end method
