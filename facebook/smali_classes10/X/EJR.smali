.class public final LX/EJR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/EJT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;)V
    .locals 0

    .prologue
    .line 2104029
    iput-object p1, p0, LX/EJR;->a:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2104030
    const/4 v1, 0x1

    .line 2104031
    new-instance v0, LX/EJT;

    invoke-direct {v0, v1, v1}, LX/EJT;-><init>(ZZ)V

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2104032
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/EJR;->a:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    .line 2104033
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v1, v2

    .line 2104034
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/EJR;->a:Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
