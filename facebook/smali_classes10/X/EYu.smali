.class public LX/EYu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/EWW;

.field private final b:LX/EYZ;

.field public c:LX/EWc;

.field public volatile d:LX/EWW;

.field public volatile e:Z


# direct methods
.method public constructor <init>(LX/EWW;LX/EYZ;LX/EWc;)V
    .locals 1

    .prologue
    .line 2138170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2138171
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EYu;->e:Z

    .line 2138172
    iput-object p1, p0, LX/EYu;->a:LX/EWW;

    .line 2138173
    iput-object p2, p0, LX/EYu;->b:LX/EYZ;

    .line 2138174
    iput-object p3, p0, LX/EYu;->c:LX/EWc;

    .line 2138175
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2138159
    iget-object v0, p0, LX/EYu;->d:LX/EWW;

    if-eqz v0, :cond_0

    .line 2138160
    :goto_0
    return-void

    .line 2138161
    :cond_0
    monitor-enter p0

    .line 2138162
    :try_start_0
    iget-object v0, p0, LX/EYu;->d:LX/EWW;

    if-eqz v0, :cond_1

    .line 2138163
    monitor-exit p0

    goto :goto_0

    .line 2138164
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2138165
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/EYu;->c:LX/EWc;

    if-eqz v0, :cond_2

    .line 2138166
    iget-object v0, p0, LX/EYu;->a:LX/EWW;

    invoke-interface {v0}, LX/EWW;->i()LX/EWZ;

    move-result-object v0

    iget-object v1, p0, LX/EYu;->c:LX/EWc;

    iget-object v2, p0, LX/EYu;->b:LX/EYZ;

    .line 2138167
    invoke-static {v0, v1, v2}, LX/EWZ;->d(LX/EWZ;LX/EWc;LX/EYZ;)LX/EWW;

    move-result-object v3

    move-object v0, v3

    .line 2138168
    check-cast v0, LX/EWW;

    iput-object v0, p0, LX/EYu;->d:LX/EWW;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2138169
    :cond_2
    :goto_1
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    goto :goto_1
.end method


# virtual methods
.method public final a()LX/EWW;
    .locals 1

    .prologue
    .line 2138157
    invoke-direct {p0}, LX/EYu;->d()V

    .line 2138158
    iget-object v0, p0, LX/EYu;->d:LX/EWW;

    return-object v0
.end method

.method public final c()LX/EWc;
    .locals 1

    .prologue
    .line 2138141
    iget-boolean v0, p0, LX/EYu;->e:Z

    if-nez v0, :cond_0

    .line 2138142
    iget-object v0, p0, LX/EYu;->c:LX/EWc;

    .line 2138143
    :goto_0
    return-object v0

    .line 2138144
    :cond_0
    monitor-enter p0

    .line 2138145
    :try_start_0
    iget-boolean v0, p0, LX/EYu;->e:Z

    if-nez v0, :cond_1

    .line 2138146
    iget-object v0, p0, LX/EYu;->c:LX/EWc;

    monitor-exit p0

    goto :goto_0

    .line 2138147
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2138148
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/EYu;->d:LX/EWW;

    invoke-interface {v0}, LX/EWW;->d()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EYu;->c:LX/EWc;

    .line 2138149
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EYu;->e:Z

    .line 2138150
    iget-object v0, p0, LX/EYu;->c:LX/EWc;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2138155
    invoke-direct {p0}, LX/EYu;->d()V

    .line 2138156
    iget-object v0, p0, LX/EYu;->d:LX/EWW;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2138153
    invoke-direct {p0}, LX/EYu;->d()V

    .line 2138154
    iget-object v0, p0, LX/EYu;->d:LX/EWW;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2138151
    invoke-direct {p0}, LX/EYu;->d()V

    .line 2138152
    iget-object v0, p0, LX/EYu;->d:LX/EWW;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
