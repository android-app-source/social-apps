.class public LX/DD1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1974821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1974822
    iput-object p1, p0, LX/DD1;->a:Landroid/content/res/Resources;

    .line 1974823
    iput-object p2, p0, LX/DD1;->b:LX/0Ot;

    .line 1974824
    return-void
.end method

.method public static a(LX/1PT;)Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
    .end annotation

    .prologue
    .line 1974825
    invoke-static {p0}, LX/DD1;->c(LX/1PT;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1974826
    const-string v0, "mobile_group_feed_pymi"

    .line 1974827
    :goto_0
    return-object v0

    .line 1974828
    :cond_0
    invoke-static {p0}, LX/DD1;->b(LX/1PT;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1974829
    const-string v0, "mobile_newsfeed_pymi"

    goto :goto_0

    .line 1974830
    :cond_1
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/DD1;
    .locals 3

    .prologue
    .line 1974831
    new-instance v1, LX/DD1;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const/16 v2, 0x2eb

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/DD1;-><init>(Landroid/content/res/Resources;LX/0Ot;)V

    .line 1974832
    return-object v1
.end method

.method public static b(LX/1PT;)Z
    .locals 2

    .prologue
    .line 1974833
    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    invoke-virtual {v0}, LX/1Qt;->ordinal()I

    move-result v0

    sget-object v1, LX/1Qt;->FEED:LX/1Qt;

    invoke-virtual {v1}, LX/1Qt;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/1PT;)Z
    .locals 2

    .prologue
    .line 1974834
    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    invoke-virtual {v0}, LX/1Qt;->ordinal()I

    move-result v0

    sget-object v1, LX/1Qt;->GROUPS:LX/1Qt;

    invoke-virtual {v1}, LX/1Qt;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
