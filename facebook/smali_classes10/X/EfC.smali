.class public final LX/EfC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AKt;


# instance fields
.field public final synthetic a:LX/EfL;


# direct methods
.method public constructor <init>(LX/EfL;)V
    .locals 0

    .prologue
    .line 2154058
    iput-object p1, p0, LX/EfC;->a:LX/EfL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2154025
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    invoke-virtual {v0}, LX/EfL;->d()V

    .line 2154026
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->y:LX/EfA;

    if-eqz v0, :cond_0

    .line 2154027
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->k:LX/0he;

    .line 2154028
    sget-object v1, LX/7gU;->SWIPE_DOWN:LX/7gU;

    iput-object v1, v0, LX/0he;->i:LX/7gU;

    .line 2154029
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->y:LX/EfA;

    invoke-virtual {v0}, LX/EfA;->d()V

    .line 2154030
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->y:LX/EfA;

    const/4 v1, 0x0

    const v2, 0x7f04002f

    .line 2154031
    iget-object p0, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->overridePendingTransition(II)V

    .line 2154032
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 2154034
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    invoke-static {v0}, LX/EfL;->g(LX/EfL;)Z

    move-result v0

    .line 2154035
    if-nez v0, :cond_0

    .line 2154036
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->y:LX/EfA;

    if-eqz v0, :cond_0

    .line 2154037
    iget-object v1, p0, LX/EfC;->a:LX/EfL;

    iget-object v1, v1, LX/EfL;->o:Lcom/facebook/audience/ui/ReplyRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/audience/ui/ReplyRecyclerView;->getWidth()I

    move-result v1

    .line 2154038
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    .line 2154039
    div-int/lit8 v0, v1, 0x3

    if-ge v2, v0, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2154040
    move v0, v2

    .line 2154041
    if-eqz v0, :cond_1

    .line 2154042
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->k:LX/0he;

    .line 2154043
    sget-object v1, LX/7gU;->CLICK_LEFT:LX/7gU;

    iput-object v1, v0, LX/0he;->i:LX/7gU;

    .line 2154044
    sget-object v1, LX/0hf;->THREAD:LX/0hf;

    iput-object v1, v0, LX/0he;->j:LX/0hf;

    .line 2154045
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->y:LX/EfA;

    .line 2154046
    iget-object v1, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v1, v1, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 2154047
    if-ltz v1, :cond_0

    .line 2154048
    iget-object v2, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v2, v2, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2154049
    :cond_0
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 2154050
    :cond_1
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->k:LX/0he;

    .line 2154051
    sget-object v1, LX/7gU;->CLICK_RIGHT:LX/7gU;

    iput-object v1, v0, LX/0he;->i:LX/7gU;

    .line 2154052
    sget-object v1, LX/0hf;->THREAD:LX/0hf;

    iput-object v1, v0, LX/0he;->j:LX/0hf;

    .line 2154053
    iget-object v0, p0, LX/EfC;->a:LX/EfL;

    iget-object v0, v0, LX/EfL;->y:LX/EfA;

    .line 2154054
    iget-object v1, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v1, v1, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 2154055
    iget-object v2, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v2, v2, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->j:LX/Efh;

    invoke-virtual {v2}, LX/0gG;->b()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2154056
    iget-object v2, v0, LX/EfA;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v2, v2, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2154057
    :cond_2
    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 2154033
    return-void
.end method
