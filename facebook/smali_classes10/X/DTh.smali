.class public LX/DTh;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/DRo;",
        "LX/DTg;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DTh;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2000740
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 2000741
    return-void
.end method

.method public static a(LX/0QB;)LX/DTh;
    .locals 3

    .prologue
    .line 2000742
    sget-object v0, LX/DTh;->a:LX/DTh;

    if-nez v0, :cond_1

    .line 2000743
    const-class v1, LX/DTh;

    monitor-enter v1

    .line 2000744
    :try_start_0
    sget-object v0, LX/DTh;->a:LX/DTh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2000745
    if-eqz v2, :cond_0

    .line 2000746
    :try_start_1
    new-instance v0, LX/DTh;

    invoke-direct {v0}, LX/DTh;-><init>()V

    .line 2000747
    move-object v0, v0

    .line 2000748
    sput-object v0, LX/DTh;->a:LX/DTh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2000749
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2000750
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2000751
    :cond_1
    sget-object v0, LX/DTh;->a:LX/DTh;

    return-object v0

    .line 2000752
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2000753
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
