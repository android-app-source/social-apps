.class public final LX/E5c;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5d;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public final synthetic c:LX/E5d;


# direct methods
.method public constructor <init>(LX/E5d;)V
    .locals 1

    .prologue
    .line 2078682
    iput-object p1, p0, LX/E5c;->c:LX/E5d;

    .line 2078683
    move-object v0, p1

    .line 2078684
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2078685
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2078686
    const-string v0, "ReactionSingleActionFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2078687
    if-ne p0, p1, :cond_1

    .line 2078688
    :cond_0
    :goto_0
    return v0

    .line 2078689
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2078690
    goto :goto_0

    .line 2078691
    :cond_3
    check-cast p1, LX/E5c;

    .line 2078692
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2078693
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2078694
    if-eq v2, v3, :cond_0

    .line 2078695
    iget-object v2, p0, LX/E5c;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E5c;->a:Ljava/lang/String;

    iget-object v3, p1, LX/E5c;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2078696
    goto :goto_0

    .line 2078697
    :cond_5
    iget-object v2, p1, LX/E5c;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2078698
    :cond_6
    iget-boolean v2, p0, LX/E5c;->b:Z

    iget-boolean v3, p1, LX/E5c;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2078699
    goto :goto_0
.end method
