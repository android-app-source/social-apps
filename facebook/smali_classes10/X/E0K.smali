.class public final LX/E0K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/home/HomeActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 2068207
    iput-object p1, p0, LX/E0K;->a:Lcom/facebook/places/create/home/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2068208
    iget-object v0, p0, LX/E0K;->a:Lcom/facebook/places/create/home/HomeActivity;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068209
    iget-object v1, v0, LX/E0T;->a:LX/0Zb;

    const-string p1, "home_%s_update_camera_icon_tapped"

    invoke-static {v0, p1}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v1, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068210
    iget-object v0, p0, LX/E0K;->a:Lcom/facebook/places/create/home/HomeActivity;

    .line 2068211
    invoke-virtual {v0}, Lcom/facebook/places/create/home/HomeActivity;->p()V

    .line 2068212
    invoke-virtual {v0}, Lcom/facebook/places/create/home/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance p0, LX/8AA;

    sget-object p1, LX/8AB;->PLACES_HOME:LX/8AB;

    invoke-direct {p0, p1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {p0}, LX/8AA;->i()LX/8AA;

    move-result-object p0

    invoke-virtual {p0}, LX/8AA;->j()LX/8AA;

    move-result-object p0

    sget-object p1, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {p0, p1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v1

    .line 2068213
    iget-object p0, v0, Lcom/facebook/places/create/home/HomeActivity;->u:Lcom/facebook/content/SecureContextHelper;

    const/16 p1, 0xb

    invoke-interface {p0, v1, p1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2068214
    const/4 v0, 0x1

    return v0
.end method
