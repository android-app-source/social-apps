.class public final LX/Dj8;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V
    .locals 0

    .prologue
    .line 2032893
    iput-object p1, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2032894
    iget-object v0, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    iget-object v1, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)V

    .line 2032895
    iget-object v0, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2032896
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2032868
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2032869
    if-eqz p1, :cond_3

    .line 2032870
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2032871
    if-eqz v0, :cond_3

    .line 2032872
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2032873
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2032874
    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_5

    .line 2032875
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2032876
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2032877
    const-class v4, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_7

    .line 2032878
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2032879
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2032880
    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    .line 2032881
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2032882
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel$ServicesCardModel$ProductCatalogModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2032883
    invoke-virtual {v3, v0, v2}, LX/15i;->j(II)I

    move-result v0

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    if-eqz v2, :cond_1

    .line 2032884
    iget-object v0, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v2, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-static {v2}, LX/Djn;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)LX/Djn;

    move-result-object v2

    .line 2032885
    iput-boolean v1, v2, LX/Djn;->n:Z

    .line 2032886
    move-object v1, v2

    .line 2032887
    invoke-virtual {v1}, LX/Djn;->a()Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    move-result-object v1

    .line 2032888
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    .line 2032889
    :cond_1
    iget-object v0, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    iget-object v1, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)V

    .line 2032890
    iget-object v0, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/Dj8;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2032891
    return-void

    :cond_2
    move v0, v2

    .line 2032892
    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_2
.end method
