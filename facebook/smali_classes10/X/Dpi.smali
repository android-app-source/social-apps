.class public LX/Dpi;
.super LX/6kT;
.source ""


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xc

    .line 2047047
    const/4 v0, 0x1

    sput-boolean v0, LX/Dpi;->a:Z

    .line 2047048
    new-instance v0, LX/1sv;

    const-string v1, "StoredProcedureResponseBody"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Dpi;->b:LX/1sv;

    .line 2047049
    new-instance v0, LX/1sw;

    const-string v1, "register_response_payload"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpi;->c:LX/1sw;

    .line 2047050
    new-instance v0, LX/1sw;

    const-string v1, "lookup_response_payload"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpi;->d:LX/1sw;

    .line 2047051
    new-instance v0, LX/1sw;

    const-string v1, "send_result_payload"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpi;->e:LX/1sw;

    .line 2047052
    new-instance v0, LX/1sw;

    const-string v1, "create_thread_response_payload"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpi;->f:LX/1sw;

    .line 2047053
    new-instance v0, LX/1sw;

    const-string v1, "batch_lookup_response_payload"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpi;->g:LX/1sw;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2047045
    invoke-direct {p0}, LX/6kT;-><init>()V

    .line 2047046
    return-void
.end method


# virtual methods
.method public final a(LX/1su;LX/1sw;)Ljava/lang/Object;
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 2046973
    iget-short v1, p2, LX/1sw;->c:S

    packed-switch v1, :pswitch_data_0

    .line 2046974
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    .line 2046975
    :goto_0
    return-object v0

    .line 2046976
    :pswitch_0
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/Dpi;->c:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_2

    .line 2046977
    const/4 v0, 0x0

    .line 2046978
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    .line 2046979
    :goto_1
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v1

    .line 2046980
    iget-byte v2, v1, LX/1sw;->b:B

    if-eqz v2, :cond_1

    .line 2046981
    iget-short v2, v1, LX/1sw;->c:S

    packed-switch v2, :pswitch_data_1

    .line 2046982
    iget-byte v1, v1, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2046983
    :pswitch_1
    iget-byte v2, v1, LX/1sw;->b:B

    const/4 p0, 0x2

    if-ne v2, p0, :cond_0

    .line 2046984
    invoke-virtual {p1}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 2046985
    :cond_0
    iget-byte v1, v1, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_1

    .line 2046986
    :cond_1
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2046987
    new-instance v1, LX/DpX;

    invoke-direct {v1, v0}, LX/DpX;-><init>(Ljava/lang/Boolean;)V

    .line 2046988
    move-object v0, v1

    .line 2046989
    goto :goto_0

    .line 2046990
    :cond_2
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046991
    :pswitch_2
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/Dpi;->d:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_3

    .line 2046992
    invoke-static {p1}, LX/DpK;->b(LX/1su;)LX/DpK;

    move-result-object v0

    goto :goto_0

    .line 2046993
    :cond_3
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2046994
    :pswitch_3
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/Dpi;->e:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_7

    .line 2046995
    const/4 v3, 0x0

    .line 2046996
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    move-object v4, v3

    .line 2046997
    :goto_2
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v5

    .line 2046998
    iget-byte v6, v5, LX/1sw;->b:B

    if-eqz v6, :cond_6

    .line 2046999
    iget-short v6, v5, LX/1sw;->c:S

    packed-switch v6, :pswitch_data_2

    .line 2047000
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 2047001
    :pswitch_4
    iget-byte v6, v5, LX/1sw;->b:B

    const/16 v7, 0xa

    if-ne v6, v7, :cond_4

    .line 2047002
    invoke-virtual {p1}, LX/1su;->n()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_2

    .line 2047003
    :cond_4
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 2047004
    :pswitch_5
    iget-byte v6, v5, LX/1sw;->b:B

    const/16 v7, 0xb

    if-ne v6, v7, :cond_5

    .line 2047005
    invoke-virtual {p1}, LX/1su;->q()[B

    move-result-object v3

    goto :goto_2

    .line 2047006
    :cond_5
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_2

    .line 2047007
    :cond_6
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2047008
    new-instance v5, LX/Dpd;

    invoke-direct {v5, v4, v3}, LX/Dpd;-><init>(Ljava/lang/Long;[B)V

    .line 2047009
    move-object v0, v5

    .line 2047010
    goto/16 :goto_0

    .line 2047011
    :cond_7
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2047012
    :pswitch_6
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/Dpi;->f:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_8

    .line 2047013
    invoke-static {p1}, LX/DpH;->b(LX/1su;)LX/DpH;

    move-result-object v0

    goto/16 :goto_0

    .line 2047014
    :cond_8
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2047015
    :pswitch_7
    iget-byte v1, p2, LX/1sw;->b:B

    sget-object v2, LX/Dpi;->g:LX/1sw;

    iget-byte v2, v2, LX/1sw;->b:B

    if-ne v1, v2, :cond_11

    .line 2047016
    const/4 v4, 0x0

    .line 2047017
    const/4 v3, 0x0

    .line 2047018
    invoke-virtual {p1}, LX/1su;->r()LX/1sv;

    .line 2047019
    :goto_3
    invoke-virtual {p1}, LX/1su;->f()LX/1sw;

    move-result-object v5

    .line 2047020
    iget-byte v6, v5, LX/1sw;->b:B

    if-eqz v6, :cond_10

    .line 2047021
    iget-short v6, v5, LX/1sw;->c:S

    packed-switch v6, :pswitch_data_3

    .line 2047022
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_3

    .line 2047023
    :pswitch_8
    iget-byte v6, v5, LX/1sw;->b:B

    const/16 v7, 0xd

    if-ne v6, v7, :cond_f

    .line 2047024
    invoke-virtual {p1}, LX/1su;->g()LX/7H3;

    move-result-object v7

    .line 2047025
    new-instance v6, Ljava/util/HashMap;

    iget v3, v7, LX/7H3;->c:I

    mul-int/lit8 v3, v3, 0x2

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-direct {v6, v3}, Ljava/util/HashMap;-><init>(I)V

    move v3, v4

    .line 2047026
    :goto_4
    iget v5, v7, LX/7H3;->c:I

    if-gez v5, :cond_b

    invoke-static {}, LX/1su;->s()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 2047027
    :cond_9
    invoke-virtual {p1}, LX/1su;->n()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 2047028
    invoke-virtual {p1}, LX/1su;->h()LX/1u3;

    move-result-object v9

    .line 2047029
    new-instance v10, Ljava/util/ArrayList;

    iget v5, v9, LX/1u3;->b:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-direct {v10, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v5, v4

    .line 2047030
    :goto_5
    iget v11, v9, LX/1u3;->b:I

    if-gez v11, :cond_d

    invoke-static {}, LX/1su;->t()Z

    move-result v11

    if-eqz v11, :cond_e

    .line 2047031
    :cond_a
    invoke-static {p1}, LX/DpK;->b(LX/1su;)LX/DpK;

    move-result-object v11

    .line 2047032
    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2047033
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 2047034
    :cond_b
    iget v5, v7, LX/7H3;->c:I

    if-lt v3, v5, :cond_9

    :cond_c
    move-object v3, v6

    .line 2047035
    goto :goto_3

    .line 2047036
    :cond_d
    iget v11, v9, LX/1u3;->b:I

    if-lt v5, v11, :cond_a

    .line 2047037
    :cond_e
    invoke-interface {v6, v8, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2047038
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 2047039
    :cond_f
    iget-byte v5, v5, LX/1sw;->b:B

    invoke-static {p1, v5}, LX/3ae;->a(LX/1su;B)V

    goto :goto_3

    .line 2047040
    :cond_10
    invoke-virtual {p1}, LX/1su;->e()V

    .line 2047041
    new-instance v4, LX/DpC;

    invoke-direct {v4, v3}, LX/DpC;-><init>(Ljava/util/Map;)V

    .line 2047042
    move-object v0, v4

    .line 2047043
    goto/16 :goto_0

    .line 2047044
    :cond_11
    iget-byte v1, p2, LX/1sw;->b:B

    invoke-static {p1, v1}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_8
    .end packed-switch
.end method

.method public final a(IZ)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2046910
    if-eqz p2, :cond_8

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2046911
    :goto_0
    if-eqz p2, :cond_9

    const-string v0, "\n"

    move-object v3, v0

    .line 2046912
    :goto_1
    if-eqz p2, :cond_a

    const-string v0, " "

    .line 2046913
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "StoredProcedureResponseBody"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046914
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046915
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046916
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046917
    const/4 v1, 0x1

    .line 2046918
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2046919
    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 2046920
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046921
    const-string v1, "register_response_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046922
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046923
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046924
    invoke-virtual {p0}, LX/Dpi;->c()LX/DpX;

    move-result-object v1

    if-nez v1, :cond_b

    .line 2046925
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 2046926
    :cond_0
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2046927
    const/4 v7, 0x3

    if-ne v6, v7, :cond_2

    .line 2046928
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046929
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046930
    const-string v1, "lookup_response_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046931
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046932
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046933
    invoke-virtual {p0}, LX/Dpi;->d()LX/DpK;

    move-result-object v1

    if-nez v1, :cond_c

    .line 2046934
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 2046935
    :cond_2
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2046936
    const/4 v7, 0x4

    if-ne v6, v7, :cond_4

    .line 2046937
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046938
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046939
    const-string v1, "send_result_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046940
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046941
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046942
    invoke-virtual {p0}, LX/Dpi;->e()LX/Dpd;

    move-result-object v1

    if-nez v1, :cond_d

    .line 2046943
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 2046944
    :cond_4
    iget v6, p0, LX/6kT;->setField_:I

    move v6, v6

    .line 2046945
    const/4 v7, 0x5

    if-ne v6, v7, :cond_10

    .line 2046946
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046947
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046948
    const-string v1, "create_thread_response_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046949
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046950
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046951
    invoke-virtual {p0}, LX/Dpi;->f()LX/DpH;

    move-result-object v1

    if-nez v1, :cond_e

    .line 2046952
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046953
    :goto_6
    iget v1, p0, LX/6kT;->setField_:I

    move v1, v1

    .line 2046954
    const/4 v6, 0x6

    if-ne v1, v6, :cond_7

    .line 2046955
    if-nez v2, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046956
    :cond_6
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046957
    const-string v1, "batch_lookup_response_payload"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046958
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046959
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046960
    invoke-virtual {p0}, LX/Dpi;->g()LX/DpC;

    move-result-object v0

    if-nez v0, :cond_f

    .line 2046961
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046962
    :cond_7
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046963
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046964
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2046965
    :cond_8
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2046966
    :cond_9
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2046967
    :cond_a
    const-string v0, ""

    goto/16 :goto_2

    .line 2046968
    :cond_b
    invoke-virtual {p0}, LX/Dpi;->c()LX/DpX;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2046969
    :cond_c
    invoke-virtual {p0}, LX/Dpi;->d()LX/DpK;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2046970
    :cond_d
    invoke-virtual {p0}, LX/Dpi;->e()LX/Dpd;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2046971
    :cond_e
    invoke-virtual {p0}, LX/Dpi;->f()LX/DpH;

    move-result-object v1

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2046972
    :cond_f
    invoke-virtual {p0}, LX/Dpi;->g()LX/DpC;

    move-result-object v0

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_10
    move v2, v1

    goto/16 :goto_6
.end method

.method public final a(LX/1su;S)V
    .locals 3

    .prologue
    .line 2046892
    packed-switch p2, :pswitch_data_0

    .line 2046893
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot write union with unknown field "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2046894
    :pswitch_0
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046895
    check-cast v0, LX/DpX;

    .line 2046896
    invoke-virtual {v0, p1}, LX/DpX;->a(LX/1su;)V

    .line 2046897
    :goto_0
    return-void

    .line 2046898
    :pswitch_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046899
    check-cast v0, LX/DpK;

    .line 2046900
    invoke-virtual {v0, p1}, LX/DpK;->a(LX/1su;)V

    goto :goto_0

    .line 2046901
    :pswitch_2
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046902
    check-cast v0, LX/Dpd;

    .line 2046903
    invoke-virtual {v0, p1}, LX/Dpd;->a(LX/1su;)V

    goto :goto_0

    .line 2046904
    :pswitch_3
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046905
    check-cast v0, LX/DpH;

    .line 2046906
    invoke-virtual {v0, p1}, LX/DpH;->a(LX/1su;)V

    goto :goto_0

    .line 2046907
    :pswitch_4
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046908
    check-cast v0, LX/DpC;

    .line 2046909
    invoke-virtual {v0, p1}, LX/DpC;->a(LX/1su;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/Dpi;)Z
    .locals 2

    .prologue
    .line 2046882
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046883
    iget v1, p1, LX/6kT;->setField_:I

    move v1, v1

    .line 2046884
    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_1

    .line 2046885
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046886
    check-cast v0, [B

    check-cast v0, [B

    .line 2046887
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 2046888
    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2046889
    :cond_1
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046890
    iget-object v1, p1, LX/6kT;->value_:Ljava/lang/Object;

    move-object v1, v1

    .line 2046891
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)LX/1sw;
    .locals 3

    .prologue
    .line 2046867
    packed-switch p1, :pswitch_data_0

    .line 2046868
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown field id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2046869
    :pswitch_0
    sget-object v0, LX/Dpi;->c:LX/1sw;

    .line 2046870
    :goto_0
    return-object v0

    .line 2046871
    :pswitch_1
    sget-object v0, LX/Dpi;->d:LX/1sw;

    goto :goto_0

    .line 2046872
    :pswitch_2
    sget-object v0, LX/Dpi;->e:LX/1sw;

    goto :goto_0

    .line 2046873
    :pswitch_3
    sget-object v0, LX/Dpi;->f:LX/1sw;

    goto :goto_0

    .line 2046874
    :pswitch_4
    sget-object v0, LX/Dpi;->g:LX/1sw;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final c()LX/DpX;
    .locals 3

    .prologue
    .line 2047054
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2047055
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2047056
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2047057
    check-cast v0, LX/DpX;

    return-object v0

    .line 2047058
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'register_response_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2047059
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2047060
    invoke-virtual {p0, v2}, LX/Dpi;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()LX/DpK;
    .locals 3

    .prologue
    .line 2046875
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046876
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 2046877
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046878
    check-cast v0, LX/DpK;

    return-object v0

    .line 2046879
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'lookup_response_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046880
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2046881
    invoke-virtual {p0, v2}, LX/Dpi;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final e()LX/Dpd;
    .locals 3

    .prologue
    .line 2046860
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046861
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 2046862
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046863
    check-cast v0, LX/Dpd;

    return-object v0

    .line 2046864
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'send_result_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046865
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2046866
    invoke-virtual {p0, v2}, LX/Dpi;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2046857
    instance-of v0, p1, LX/Dpi;

    if-eqz v0, :cond_0

    .line 2046858
    check-cast p1, LX/Dpi;

    invoke-virtual {p0, p1}, LX/Dpi;->a(LX/Dpi;)Z

    move-result v0

    .line 2046859
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()LX/DpH;
    .locals 3

    .prologue
    .line 2046850
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046851
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 2046852
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046853
    check-cast v0, LX/DpH;

    return-object v0

    .line 2046854
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'create_thread_response_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046855
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2046856
    invoke-virtual {p0, v2}, LX/Dpi;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final g()LX/DpC;
    .locals 3

    .prologue
    .line 2046843
    iget v0, p0, LX/6kT;->setField_:I

    move v0, v0

    .line 2046844
    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 2046845
    iget-object v0, p0, LX/6kT;->value_:Ljava/lang/Object;

    move-object v0, v0

    .line 2046846
    check-cast v0, LX/DpC;

    return-object v0

    .line 2046847
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get field \'batch_lookup_response_payload\' because union is currently set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046848
    iget v2, p0, LX/6kT;->setField_:I

    move v2, v2

    .line 2046849
    invoke-virtual {p0, v2}, LX/Dpi;->b(I)LX/1sw;

    move-result-object v2

    iget-object v2, v2, LX/1sw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2046842
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2046839
    sget-boolean v0, LX/Dpi;->a:Z

    .line 2046840
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Dpi;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2046841
    return-object v0
.end method
