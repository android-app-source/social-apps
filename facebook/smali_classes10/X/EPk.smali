.class public final LX/EPk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D6L;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/Cxj;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;LX/CzL;LX/Cxj;)V
    .locals 0

    .prologue
    .line 2117438
    iput-object p1, p0, LX/EPk;->c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    iput-object p2, p0, LX/EPk;->a:LX/CzL;

    iput-object p3, p0, LX/EPk;->b:LX/Cxj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2117439
    iget-object v0, p0, LX/EPk;->a:LX/CzL;

    .line 2117440
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2117441
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2117442
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2117443
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2117444
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    move-object v2, v0

    .line 2117445
    :goto_0
    iget-object v0, p0, LX/EPk;->c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v3, p0, LX/EPk;->a:LX/CzL;

    iget-object v1, p0, LX/EPk;->b:LX/Cxj;

    check-cast v1, LX/CxP;

    invoke-static {v0, v3, v2, v1}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a(LX/CvY;LX/CzL;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/CxP;)V

    .line 2117446
    return-void

    .line 2117447
    :cond_0
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_0
.end method
