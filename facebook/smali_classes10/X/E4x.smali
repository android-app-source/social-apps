.class public final LX/E4x;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E4y;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public d:Ljava/lang/String;

.field public e:Landroid/net/Uri;

.field public f:LX/1X1;

.field public g:LX/4Ab;

.field public h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

.field public i:Ljava/lang/String;

.field public j:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

.field public final synthetic n:LX/E4y;


# direct methods
.method public constructor <init>(LX/E4y;)V
    .locals 1

    .prologue
    .line 2077565
    iput-object p1, p0, LX/E4x;->n:LX/E4y;

    .line 2077566
    move-object v0, p1

    .line 2077567
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2077568
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2077569
    const-string v0, "ReactionImageBlockComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2077570
    if-ne p0, p1, :cond_1

    .line 2077571
    :cond_0
    :goto_0
    return v0

    .line 2077572
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2077573
    goto :goto_0

    .line 2077574
    :cond_3
    check-cast p1, LX/E4x;

    .line 2077575
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2077576
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2077577
    if-eq v2, v3, :cond_0

    .line 2077578
    iget-object v2, p0, LX/E4x;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E4x;->a:Landroid/net/Uri;

    iget-object v3, p1, LX/E4x;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2077579
    goto :goto_0

    .line 2077580
    :cond_5
    iget-object v2, p1, LX/E4x;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 2077581
    :cond_6
    iget-object v2, p0, LX/E4x;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E4x;->b:Ljava/lang/String;

    iget-object v3, p1, LX/E4x;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2077582
    goto :goto_0

    .line 2077583
    :cond_8
    iget-object v2, p1, LX/E4x;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2077584
    :cond_9
    iget-object v2, p0, LX/E4x;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E4x;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iget-object v3, p1, LX/E4x;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2077585
    goto :goto_0

    .line 2077586
    :cond_b
    iget-object v2, p1, LX/E4x;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-nez v2, :cond_a

    .line 2077587
    :cond_c
    iget-object v2, p0, LX/E4x;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/E4x;->d:Ljava/lang/String;

    iget-object v3, p1, LX/E4x;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2077588
    goto :goto_0

    .line 2077589
    :cond_e
    iget-object v2, p1, LX/E4x;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2077590
    :cond_f
    iget-object v2, p0, LX/E4x;->e:Landroid/net/Uri;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/E4x;->e:Landroid/net/Uri;

    iget-object v3, p1, LX/E4x;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 2077591
    goto :goto_0

    .line 2077592
    :cond_11
    iget-object v2, p1, LX/E4x;->e:Landroid/net/Uri;

    if-nez v2, :cond_10

    .line 2077593
    :cond_12
    iget-object v2, p0, LX/E4x;->f:LX/1X1;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/E4x;->f:LX/1X1;

    iget-object v3, p1, LX/E4x;->f:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 2077594
    goto/16 :goto_0

    .line 2077595
    :cond_14
    iget-object v2, p1, LX/E4x;->f:LX/1X1;

    if-nez v2, :cond_13

    .line 2077596
    :cond_15
    iget-object v2, p0, LX/E4x;->g:LX/4Ab;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/E4x;->g:LX/4Ab;

    iget-object v3, p1, LX/E4x;->g:LX/4Ab;

    invoke-virtual {v2, v3}, LX/4Ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 2077597
    goto/16 :goto_0

    .line 2077598
    :cond_17
    iget-object v2, p1, LX/E4x;->g:LX/4Ab;

    if-nez v2, :cond_16

    .line 2077599
    :cond_18
    iget-object v2, p0, LX/E4x;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-eqz v2, :cond_1a

    iget-object v2, p0, LX/E4x;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p1, LX/E4x;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    .line 2077600
    goto/16 :goto_0

    .line 2077601
    :cond_1a
    iget-object v2, p1, LX/E4x;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-nez v2, :cond_19

    .line 2077602
    :cond_1b
    iget-object v2, p0, LX/E4x;->i:Ljava/lang/String;

    if-eqz v2, :cond_1d

    iget-object v2, p0, LX/E4x;->i:Ljava/lang/String;

    iget-object v3, p1, LX/E4x;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    :cond_1c
    move v0, v1

    .line 2077603
    goto/16 :goto_0

    .line 2077604
    :cond_1d
    iget-object v2, p1, LX/E4x;->i:Ljava/lang/String;

    if-nez v2, :cond_1c

    .line 2077605
    :cond_1e
    iget-object v2, p0, LX/E4x;->j:LX/2km;

    if-eqz v2, :cond_20

    iget-object v2, p0, LX/E4x;->j:LX/2km;

    iget-object v3, p1, LX/E4x;->j:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    :cond_1f
    move v0, v1

    .line 2077606
    goto/16 :goto_0

    .line 2077607
    :cond_20
    iget-object v2, p1, LX/E4x;->j:LX/2km;

    if-nez v2, :cond_1f

    .line 2077608
    :cond_21
    iget-object v2, p0, LX/E4x;->k:Ljava/lang/String;

    if-eqz v2, :cond_23

    iget-object v2, p0, LX/E4x;->k:Ljava/lang/String;

    iget-object v3, p1, LX/E4x;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_24

    :cond_22
    move v0, v1

    .line 2077609
    goto/16 :goto_0

    .line 2077610
    :cond_23
    iget-object v2, p1, LX/E4x;->k:Ljava/lang/String;

    if-nez v2, :cond_22

    .line 2077611
    :cond_24
    iget-object v2, p0, LX/E4x;->l:Ljava/lang/String;

    if-eqz v2, :cond_26

    iget-object v2, p0, LX/E4x;->l:Ljava/lang/String;

    iget-object v3, p1, LX/E4x;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_27

    :cond_25
    move v0, v1

    .line 2077612
    goto/16 :goto_0

    .line 2077613
    :cond_26
    iget-object v2, p1, LX/E4x;->l:Ljava/lang/String;

    if-nez v2, :cond_25

    .line 2077614
    :cond_27
    iget-object v2, p0, LX/E4x;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-eqz v2, :cond_28

    iget-object v2, p0, LX/E4x;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p1, LX/E4x;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2077615
    goto/16 :goto_0

    .line 2077616
    :cond_28
    iget-object v2, p1, LX/E4x;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2077617
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/E4x;

    .line 2077618
    iget-object v1, v0, LX/E4x;->f:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/E4x;->f:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/E4x;->f:LX/1X1;

    .line 2077619
    return-object v0

    .line 2077620
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
