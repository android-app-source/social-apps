.class public LX/Dbe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Dbe;


# instance fields
.field public final a:LX/0TD;

.field public final b:LX/0tX;

.field public final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0TD;LX/0tX;LX/1Ck;)V
    .locals 0
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2017337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2017338
    iput-object p1, p0, LX/Dbe;->a:LX/0TD;

    .line 2017339
    iput-object p2, p0, LX/Dbe;->b:LX/0tX;

    .line 2017340
    iput-object p3, p0, LX/Dbe;->c:LX/1Ck;

    .line 2017341
    return-void
.end method

.method public static a(LX/0QB;)LX/Dbe;
    .locals 6

    .prologue
    .line 2017342
    sget-object v0, LX/Dbe;->d:LX/Dbe;

    if-nez v0, :cond_1

    .line 2017343
    const-class v1, LX/Dbe;

    monitor-enter v1

    .line 2017344
    :try_start_0
    sget-object v0, LX/Dbe;->d:LX/Dbe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2017345
    if-eqz v2, :cond_0

    .line 2017346
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2017347
    new-instance p0, LX/Dbe;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-direct {p0, v3, v4, v5}, LX/Dbe;-><init>(LX/0TD;LX/0tX;LX/1Ck;)V

    .line 2017348
    move-object v0, p0

    .line 2017349
    sput-object v0, LX/Dbe;->d:LX/Dbe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2017350
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2017351
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2017352
    :cond_1
    sget-object v0, LX/Dbe;->d:LX/Dbe;

    return-object v0

    .line 2017353
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2017354
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
