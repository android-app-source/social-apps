.class public LX/EWC;
.super LX/EQV;
.source ""


# static fields
.field private static final a:LX/0wT;

.field private static final b:LX/EW9;


# instance fields
.field private A:I

.field private B:LX/0Sy;

.field public C:LX/0So;

.field private D:Ljava/lang/Runnable;

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field public I:D

.field private J:LX/0wW;

.field public K:LX/EVz;

.field private L:LX/EW9;

.field public M:Landroid/graphics/drawable/Drawable;

.field public N:Landroid/widget/AbsListView$OnScrollListener;

.field public O:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private P:Landroid/widget/AdapterView$OnItemLongClickListener;

.field public Q:Landroid/widget/AdapterView$OnItemClickListener;

.field private R:Landroid/widget/AdapterView$OnItemClickListener;

.field private final S:Landroid/widget/AbsListView$OnScrollListener;

.field private final c:LX/1OB;

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field private h:I

.field private i:I

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public k:J

.field public l:Z

.field private m:I

.field public n:Z

.field private o:I

.field public p:Z

.field public q:I

.field public r:J

.field public s:Z

.field private t:LX/EWB;

.field public u:LX/EWA;

.field public v:Z

.field public w:Z

.field public x:LX/0wd;

.field public y:Z

.field private z:LX/0xi;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2129655
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    const-wide/high16 v2, 0x400c000000000000L    # 3.5

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/EWC;->a:LX/0wT;

    .line 2129656
    sget-object v0, LX/EW9;->CLONE_BITMAP:LX/EW9;

    sput-object v0, LX/EWC;->b:LX/EW9;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 2129572
    invoke-direct {p0, p1}, LX/EQV;-><init>(Landroid/content/Context;)V

    .line 2129573
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, LX/EWC;->c:LX/1OB;

    .line 2129574
    iput v2, p0, LX/EWC;->d:I

    .line 2129575
    iput v2, p0, LX/EWC;->e:I

    .line 2129576
    iput v3, p0, LX/EWC;->f:I

    .line 2129577
    iput v3, p0, LX/EWC;->g:I

    .line 2129578
    iput v3, p0, LX/EWC;->h:I

    .line 2129579
    iput v3, p0, LX/EWC;->i:I

    .line 2129580
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/EWC;->j:Ljava/util/List;

    .line 2129581
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/EWC;->k:J

    .line 2129582
    iput-boolean v2, p0, LX/EWC;->l:Z

    .line 2129583
    iput v3, p0, LX/EWC;->m:I

    .line 2129584
    iput v2, p0, LX/EWC;->o:I

    .line 2129585
    iput-boolean v2, p0, LX/EWC;->p:Z

    .line 2129586
    iput v2, p0, LX/EWC;->q:I

    .line 2129587
    iput-boolean v2, p0, LX/EWC;->s:Z

    .line 2129588
    iput-boolean v2, p0, LX/EWC;->y:Z

    .line 2129589
    iput v2, p0, LX/EWC;->A:I

    .line 2129590
    const-wide v0, 0x3ff199999999999aL    # 1.1

    iput-wide v0, p0, LX/EWC;->I:D

    .line 2129591
    sget-object v0, LX/EWC;->b:LX/EW9;

    iput-object v0, p0, LX/EWC;->L:LX/EW9;

    .line 2129592
    new-instance v0, LX/EW1;

    invoke-direct {v0, p0}, LX/EW1;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->P:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 2129593
    new-instance v0, LX/EW2;

    invoke-direct {v0, p0}, LX/EW2;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->R:Landroid/widget/AdapterView$OnItemClickListener;

    .line 2129594
    new-instance v0, LX/EW7;

    invoke-direct {v0, p0}, LX/EW7;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->S:Landroid/widget/AbsListView$OnScrollListener;

    .line 2129595
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EWC;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2129596
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 2129597
    invoke-direct {p0, p1, p2}, LX/EQV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2129598
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, LX/EWC;->c:LX/1OB;

    .line 2129599
    iput v2, p0, LX/EWC;->d:I

    .line 2129600
    iput v2, p0, LX/EWC;->e:I

    .line 2129601
    iput v3, p0, LX/EWC;->f:I

    .line 2129602
    iput v3, p0, LX/EWC;->g:I

    .line 2129603
    iput v3, p0, LX/EWC;->h:I

    .line 2129604
    iput v3, p0, LX/EWC;->i:I

    .line 2129605
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/EWC;->j:Ljava/util/List;

    .line 2129606
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/EWC;->k:J

    .line 2129607
    iput-boolean v2, p0, LX/EWC;->l:Z

    .line 2129608
    iput v3, p0, LX/EWC;->m:I

    .line 2129609
    iput v2, p0, LX/EWC;->o:I

    .line 2129610
    iput-boolean v2, p0, LX/EWC;->p:Z

    .line 2129611
    iput v2, p0, LX/EWC;->q:I

    .line 2129612
    iput-boolean v2, p0, LX/EWC;->s:Z

    .line 2129613
    iput-boolean v2, p0, LX/EWC;->y:Z

    .line 2129614
    iput v2, p0, LX/EWC;->A:I

    .line 2129615
    const-wide v0, 0x3ff199999999999aL    # 1.1

    iput-wide v0, p0, LX/EWC;->I:D

    .line 2129616
    sget-object v0, LX/EWC;->b:LX/EW9;

    iput-object v0, p0, LX/EWC;->L:LX/EW9;

    .line 2129617
    new-instance v0, LX/EW1;

    invoke-direct {v0, p0}, LX/EW1;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->P:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 2129618
    new-instance v0, LX/EW2;

    invoke-direct {v0, p0}, LX/EW2;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->R:Landroid/widget/AdapterView$OnItemClickListener;

    .line 2129619
    new-instance v0, LX/EW7;

    invoke-direct {v0, p0}, LX/EW7;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->S:Landroid/widget/AbsListView$OnScrollListener;

    .line 2129620
    invoke-direct {p0, p1, p2}, LX/EWC;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2129621
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 2129622
    invoke-direct {p0, p1, p2, p3}, LX/EQV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2129623
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, LX/EWC;->c:LX/1OB;

    .line 2129624
    iput v2, p0, LX/EWC;->d:I

    .line 2129625
    iput v2, p0, LX/EWC;->e:I

    .line 2129626
    iput v3, p0, LX/EWC;->f:I

    .line 2129627
    iput v3, p0, LX/EWC;->g:I

    .line 2129628
    iput v3, p0, LX/EWC;->h:I

    .line 2129629
    iput v3, p0, LX/EWC;->i:I

    .line 2129630
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/EWC;->j:Ljava/util/List;

    .line 2129631
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/EWC;->k:J

    .line 2129632
    iput-boolean v2, p0, LX/EWC;->l:Z

    .line 2129633
    iput v3, p0, LX/EWC;->m:I

    .line 2129634
    iput v2, p0, LX/EWC;->o:I

    .line 2129635
    iput-boolean v2, p0, LX/EWC;->p:Z

    .line 2129636
    iput v2, p0, LX/EWC;->q:I

    .line 2129637
    iput-boolean v2, p0, LX/EWC;->s:Z

    .line 2129638
    iput-boolean v2, p0, LX/EWC;->y:Z

    .line 2129639
    iput v2, p0, LX/EWC;->A:I

    .line 2129640
    const-wide v0, 0x3ff199999999999aL    # 1.1

    iput-wide v0, p0, LX/EWC;->I:D

    .line 2129641
    sget-object v0, LX/EWC;->b:LX/EW9;

    iput-object v0, p0, LX/EWC;->L:LX/EW9;

    .line 2129642
    new-instance v0, LX/EW1;

    invoke-direct {v0, p0}, LX/EW1;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->P:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 2129643
    new-instance v0, LX/EW2;

    invoke-direct {v0, p0}, LX/EW2;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->R:Landroid/widget/AdapterView$OnItemClickListener;

    .line 2129644
    new-instance v0, LX/EW7;

    invoke-direct {v0, p0}, LX/EW7;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->S:Landroid/widget/AbsListView$OnScrollListener;

    .line 2129645
    invoke-direct {p0, p1, p2}, LX/EWC;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2129646
    return-void
.end method

.method private a(I)J
    .locals 2

    .prologue
    .line 2129647
    invoke-virtual {p0}, LX/EWC;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2129648
    const-string v0, "translationX"

    new-array v1, v3, [F

    aput p1, v1, v4

    aput p2, v1, v5

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2129649
    const-string v1, "translationY"

    new-array v2, v3, [F

    aput p3, v2, v4

    aput p4, v2, v5

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 2129650
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2129651
    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 2129652
    return-object v2
.end method

.method private a(II)V
    .locals 0

    .prologue
    .line 2129653
    invoke-static {p0}, LX/EWC;->getAdapterInterface(LX/EWC;)LX/EVv;

    .line 2129654
    return-void
.end method

.method private a(LX/0Sy;LX/0So;LX/0wW;)V
    .locals 0
    .param p2    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedAwakeTimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2129566
    iput-object p1, p0, LX/EWC;->B:LX/0Sy;

    .line 2129567
    iput-object p2, p0, LX/EWC;->C:LX/0So;

    .line 2129568
    iput-object p3, p0, LX/EWC;->J:LX/0wW;

    .line 2129569
    return-void
.end method

.method public static a(LX/EWC;J)V
    .locals 7

    .prologue
    .line 2129657
    invoke-direct {p0, p1, p2}, LX/EWC;->b(J)I

    move-result v1

    .line 2129658
    invoke-virtual {p0}, LX/EWC;->getFirstVisiblePosition()I

    move-result v0

    :goto_0
    invoke-virtual {p0}, LX/EWC;->getLastVisiblePosition()I

    move-result v2

    if-gt v0, v2, :cond_1

    .line 2129659
    if-eq v1, v0, :cond_0

    .line 2129660
    iget-object v2, p0, LX/EWC;->j:Ljava/util/List;

    invoke-direct {p0, v0}, LX/EWC;->a(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2129661
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2129662
    :cond_1
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 2129663
    const-class v0, LX/EWC;

    invoke-static {v0, p0}, LX/EWC;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2129664
    if-eqz p2, :cond_0

    .line 2129665
    sget-object v0, LX/03r;->DragSortGridView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2129666
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, LX/EWC;->E:I

    .line 2129667
    const/16 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, LX/EWC;->F:I

    .line 2129668
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2129669
    :cond_0
    new-instance v0, Lcom/facebook/widget/dragsortgridview/DragSortGridView$3;

    invoke-direct {v0, p0}, Lcom/facebook/widget/dragsortgridview/DragSortGridView$3;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->D:Ljava/lang/Runnable;

    .line 2129670
    iget-object v0, p0, LX/EWC;->J:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/EWC;->x:LX/0wd;

    .line 2129671
    iget-object v0, p0, LX/EWC;->x:LX/0wd;

    sget-object v1, LX/EWC;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 2129672
    new-instance v0, LX/EW3;

    invoke-direct {v0, p0}, LX/EW3;-><init>(LX/EWC;)V

    iput-object v0, p0, LX/EWC;->z:LX/0xi;

    .line 2129673
    iget-object v0, p0, LX/EWC;->S:Landroid/widget/AbsListView$OnScrollListener;

    invoke-super {p0, v0}, LX/EQV;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2129674
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2129675
    const/high16 v1, 0x41000000    # 8.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/EWC;->o:I

    .line 2129676
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/EWC;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, LX/EWC;

    invoke-static {v2}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v0

    check-cast v0, LX/0Sy;

    invoke-static {v2}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-static {v2}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v2

    check-cast v2, LX/0wW;

    invoke-direct {p0, v0, v1, v2}, LX/EWC;->a(LX/0Sy;LX/0So;LX/0wW;)V

    return-void
.end method

.method private static a(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2

    .prologue
    .line 2129570
    iget v0, p0, Landroid/graphics/Point;->y:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/graphics/Rect;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2129677
    invoke-virtual {p0}, LX/EWC;->computeVerticalScrollOffset()I

    move-result v2

    .line 2129678
    invoke-virtual {p0}, LX/EWC;->getHeight()I

    move-result v3

    .line 2129679
    invoke-virtual {p0}, LX/EWC;->computeVerticalScrollExtent()I

    move-result v4

    .line 2129680
    invoke-virtual {p0}, LX/EWC;->computeVerticalScrollRange()I

    move-result v5

    .line 2129681
    iget v6, p1, Landroid/graphics/Rect;->top:I

    .line 2129682
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    .line 2129683
    if-gtz v6, :cond_0

    if-lez v2, :cond_0

    .line 2129684
    iget v2, p0, LX/EWC;->o:I

    neg-int v2, v2

    invoke-virtual {p0, v2, v1}, LX/EWC;->smoothScrollBy(II)V

    .line 2129685
    :goto_0
    return v0

    .line 2129686
    :cond_0
    add-int/2addr v6, v7

    if-lt v6, v3, :cond_1

    add-int/2addr v2, v4

    if-ge v2, v5, :cond_1

    .line 2129687
    iget v2, p0, LX/EWC;->o:I

    invoke-virtual {p0, v2, v1}, LX/EWC;->smoothScrollBy(II)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2129688
    goto :goto_0
.end method

.method public static a$redex0(LX/EWC;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2129689
    iget-object v0, p0, LX/EWC;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2129690
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/EWC;->k:J

    .line 2129691
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2129692
    iget-object v0, p0, LX/EWC;->K:LX/EVz;

    invoke-interface {v0}, LX/EVz;->b()V

    .line 2129693
    invoke-virtual {p0}, LX/EWC;->invalidate()V

    .line 2129694
    return-void
.end method

.method private b(J)I
    .locals 1

    .prologue
    .line 2129714
    invoke-direct {p0, p1, p2}, LX/EWC;->c(J)Landroid/view/View;

    move-result-object v0

    .line 2129715
    if-nez v0, :cond_0

    .line 2129716
    const/4 v0, -0x1

    .line 2129717
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, LX/EWC;->getPositionForView(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method private b()LX/EVz;
    .locals 3

    .prologue
    .line 2129768
    new-instance v1, LX/EW4;

    invoke-direct {v1, p0}, LX/EW4;-><init>(LX/EWC;)V

    .line 2129769
    sget-object v0, LX/EW8;->a:[I

    iget-object v2, p0, LX/EWC;->L:LX/EW9;

    invoke-virtual {v2}, LX/EW9;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2129770
    new-instance v0, LX/EW0;

    invoke-direct {v0, v1}, LX/EW0;-><init>(LX/EW4;)V

    :goto_0
    return-object v0

    .line 2129771
    :pswitch_0
    new-instance v0, LX/EWG;

    invoke-direct {v0, v1}, LX/EWG;-><init>(LX/EW4;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private b(Landroid/view/View;)Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 2129763
    invoke-virtual {p0, p1}, LX/EWC;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 2129764
    invoke-virtual {p0}, LX/EWC;->getNumColumns()I

    move-result v1

    .line 2129765
    rem-int v2, v0, v1

    .line 2129766
    div-int/2addr v0, v1

    .line 2129767
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v1
.end method

.method public static b(LX/EWC;I)V
    .locals 4

    .prologue
    .line 2129756
    iget v0, p0, LX/EWC;->A:I

    if-eq p1, v0, :cond_0

    .line 2129757
    iput p1, p0, LX/EWC;->A:I

    .line 2129758
    if-nez p1, :cond_1

    .line 2129759
    iget-object v0, p0, LX/EWC;->B:LX/0Sy;

    invoke-virtual {v0, p0}, LX/0Sy;->b(Landroid/view/View;)V

    .line 2129760
    :cond_0
    :goto_0
    return-void

    .line 2129761
    :cond_1
    iget-object v0, p0, LX/EWC;->B:LX/0Sy;

    invoke-virtual {v0, p0}, LX/0Sy;->a(Landroid/view/View;)V

    .line 2129762
    iget-object v0, p0, LX/EWC;->D:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {p0, v0, v2, v3}, LX/EWC;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public static b(LX/EWC;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2129731
    if-le p2, p1, :cond_0

    const/4 v0, 0x1

    .line 2129732
    :goto_0
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v1

    .line 2129733
    if-eqz v0, :cond_2

    .line 2129734
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2129735
    :goto_1
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 2129736
    invoke-direct {p0, v0}, LX/EWC;->a(I)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, LX/EWC;->c(J)Landroid/view/View;

    move-result-object v2

    .line 2129737
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0}, LX/EWC;->getNumColumns()I

    move-result v4

    rem-int/2addr v3, v4

    if-nez v3, :cond_1

    .line 2129738
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {p0}, LX/EWC;->getNumColumns()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-static {v2, v3, v5, v4, v5}, LX/EWC;->a(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2129739
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2129740
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2129741
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-static {v2, v3, v5, v5, v5}, LX/EWC;->a(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2129742
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2129743
    :goto_3
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-le v0, v2, :cond_4

    .line 2129744
    invoke-direct {p0, v0}, LX/EWC;->a(I)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, LX/EWC;->c(J)Landroid/view/View;

    move-result-object v2

    .line 2129745
    invoke-virtual {p0}, LX/EWC;->getNumColumns()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p0}, LX/EWC;->getNumColumns()I

    move-result v4

    rem-int/2addr v3, v4

    if-nez v3, :cond_3

    .line 2129746
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p0}, LX/EWC;->getNumColumns()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-static {v2, v3, v5, v4, v5}, LX/EWC;->a(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2129747
    :goto_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 2129748
    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-static {v2, v3, v5, v5, v5}, LX/EWC;->a(Landroid/view/View;FFFF)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2129749
    :cond_4
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2129750
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 2129751
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2129752
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2129753
    new-instance v1, LX/EW6;

    invoke-direct {v1, p0}, LX/EW6;-><init>(LX/EWC;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2129754
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 2129755
    return-void
.end method

.method private static b(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2

    .prologue
    .line 2129730
    iget v0, p0, Landroid/graphics/Point;->y:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/view/View;)F
    .locals 2

    .prologue
    .line 2129729
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public static synthetic c(LX/EWC;I)I
    .locals 1

    .prologue
    .line 2129728
    iget v0, p0, LX/EWC;->e:I

    sub-int/2addr v0, p1

    iput v0, p0, LX/EWC;->e:I

    return v0
.end method

.method private c(J)Landroid/view/View;
    .locals 7

    .prologue
    .line 2129718
    invoke-virtual {p0}, LX/EWC;->getFirstVisiblePosition()I

    move-result v2

    .line 2129719
    invoke-virtual {p0}, LX/EWC;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 2129720
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/EWC;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2129721
    invoke-virtual {p0, v0}, LX/EWC;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2129722
    add-int v4, v2, v0

    .line 2129723
    invoke-interface {v3, v4}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    .line 2129724
    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    move-object v0, v1

    .line 2129725
    :goto_1
    return-object v0

    .line 2129726
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2129727
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static c(LX/EWC;)V
    .locals 1

    .prologue
    .line 2129695
    iget-object v0, p0, LX/EWC;->K:LX/EVz;

    invoke-interface {v0}, LX/EVz;->d()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, v0}, LX/EWC;->a(Landroid/graphics/Rect;)Z

    move-result v0

    iput-boolean v0, p0, LX/EWC;->n:Z

    .line 2129696
    return-void
.end method

.method private static c(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2

    .prologue
    .line 2129713
    iget v0, p0, Landroid/graphics/Point;->y:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Landroid/view/View;)F
    .locals 2

    .prologue
    .line 2129712
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public static synthetic d(LX/EWC;I)I
    .locals 1

    .prologue
    .line 2129711
    iget v0, p0, LX/EWC;->d:I

    sub-int/2addr v0, p1

    iput v0, p0, LX/EWC;->d:I

    return v0
.end method

.method public static d(LX/EWC;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2129698
    iget-wide v0, p0, LX/EWC;->k:J

    invoke-direct {p0, v0, v1}, LX/EWC;->c(J)Landroid/view/View;

    move-result-object v0

    .line 2129699
    iget-boolean v1, p0, LX/EWC;->l:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, LX/EWC;->p:Z

    if-eqz v1, :cond_2

    .line 2129700
    :cond_0
    iput-boolean v2, p0, LX/EWC;->l:Z

    .line 2129701
    iput-boolean v2, p0, LX/EWC;->p:Z

    .line 2129702
    iput-boolean v2, p0, LX/EWC;->n:Z

    .line 2129703
    const/4 v1, -0x1

    iput v1, p0, LX/EWC;->m:I

    .line 2129704
    iget-object v1, p0, LX/EWC;->x:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    .line 2129705
    iput-boolean v3, p0, LX/EWC;->y:Z

    .line 2129706
    iget v1, p0, LX/EWC;->q:I

    if-eqz v1, :cond_1

    .line 2129707
    iput-boolean v3, p0, LX/EWC;->p:Z

    .line 2129708
    :goto_0
    return-void

    .line 2129709
    :cond_1
    iget-object v1, p0, LX/EWC;->K:LX/EVz;

    invoke-interface {v1, v0}, LX/EVz;->b(Landroid/view/View;)V

    goto :goto_0

    .line 2129710
    :cond_2
    invoke-direct {p0}, LX/EWC;->f()V

    goto :goto_0
.end method

.method private static d(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2

    .prologue
    .line 2129697
    iget v0, p0, Landroid/graphics/Point;->y:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic e(LX/EWC;I)I
    .locals 1

    .prologue
    .line 2129571
    iget v0, p0, LX/EWC;->d:I

    add-int/2addr v0, p1

    iput v0, p0, LX/EWC;->d:I

    return v0
.end method

.method public static e(LX/EWC;)V
    .locals 1

    .prologue
    .line 2129472
    iget-boolean v0, p0, LX/EWC;->v:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/EWC;->w:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/EWC;->setEnabled(Z)V

    .line 2129473
    return-void

    .line 2129474
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2

    .prologue
    .line 2129471
    iget v0, p0, Landroid/graphics/Point;->y:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->x:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic f(LX/EWC;I)I
    .locals 1

    .prologue
    .line 2129470
    iget v0, p0, LX/EWC;->e:I

    add-int/2addr v0, p1

    iput v0, p0, LX/EWC;->e:I

    return v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2129401
    iget-wide v0, p0, LX/EWC;->k:J

    invoke-direct {p0, v0, v1}, LX/EWC;->c(J)Landroid/view/View;

    move-result-object v0

    .line 2129402
    iget-boolean v1, p0, LX/EWC;->l:Z

    if-eqz v1, :cond_0

    .line 2129403
    invoke-static {p0, v0}, LX/EWC;->a$redex0(LX/EWC;Landroid/view/View;)V

    .line 2129404
    :cond_0
    iput-boolean v2, p0, LX/EWC;->l:Z

    .line 2129405
    iput-boolean v2, p0, LX/EWC;->n:Z

    .line 2129406
    const/4 v0, -0x1

    iput v0, p0, LX/EWC;->m:I

    .line 2129407
    return-void
.end method

.method private static f(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2

    .prologue
    .line 2129469
    iget v0, p0, Landroid/graphics/Point;->y:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->x:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/EWC;)V
    .locals 20

    .prologue
    .line 2129434
    move-object/from16 v0, p0

    iget v4, v0, LX/EWC;->h:I

    move-object/from16 v0, p0

    iget v5, v0, LX/EWC;->g:I

    sub-int v7, v4, v5

    .line 2129435
    move-object/from16 v0, p0

    iget v4, v0, LX/EWC;->i:I

    move-object/from16 v0, p0

    iget v5, v0, LX/EWC;->f:I

    sub-int v8, v4, v5

    .line 2129436
    move-object/from16 v0, p0

    iget-object v4, v0, LX/EWC;->K:LX/EVz;

    invoke-interface {v4}, LX/EVz;->c()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, LX/EWC;->d:I

    add-int/2addr v4, v5

    add-int v12, v4, v7

    .line 2129437
    move-object/from16 v0, p0

    iget-object v4, v0, LX/EWC;->K:LX/EVz;

    invoke-interface {v4}, LX/EVz;->c()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, LX/EWC;->e:I

    add-int/2addr v4, v5

    add-int v13, v4, v8

    .line 2129438
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/EWC;->k:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, LX/EWC;->c(J)Landroid/view/View;

    move-result-object v14

    .line 2129439
    const/4 v11, 0x0

    .line 2129440
    const/4 v6, 0x0

    .line 2129441
    const/4 v5, 0x0

    .line 2129442
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, LX/EWC;->b(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v15

    .line 2129443
    move-object/from16 v0, p0

    iget-object v4, v0, LX/EWC;->j:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 2129444
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, LX/EWC;->c(J)Landroid/view/View;

    move-result-object v10

    .line 2129445
    if-eqz v10, :cond_d

    .line 2129446
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, LX/EWC;->b(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v4

    .line 2129447
    invoke-static {v4, v15}, LX/EWC;->d(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v9

    if-ge v12, v9, :cond_0

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v9

    if-gt v13, v9, :cond_7

    :cond_0
    invoke-static {v4, v15}, LX/EWC;->c(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v9

    if-ge v12, v9, :cond_1

    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v9

    if-lt v13, v9, :cond_7

    :cond_1
    invoke-static {v4, v15}, LX/EWC;->b(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v9

    if-le v12, v9, :cond_2

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v9

    if-gt v13, v9, :cond_7

    :cond_2
    invoke-static {v4, v15}, LX/EWC;->a(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v9

    if-le v12, v9, :cond_3

    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v9

    if-lt v13, v9, :cond_7

    :cond_3
    invoke-static {v4, v15}, LX/EWC;->e(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v9

    if-lt v12, v9, :cond_7

    :cond_4
    invoke-static {v4, v15}, LX/EWC;->f(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v9

    if-gt v12, v9, :cond_7

    :cond_5
    invoke-static {v4, v15}, LX/EWC;->g(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v9

    if-gt v13, v9, :cond_7

    :cond_6
    invoke-static {v4, v15}, LX/EWC;->h(Landroid/graphics/Point;Landroid/graphics/Point;)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v4

    if-ge v13, v4, :cond_d

    .line 2129448
    :cond_7
    invoke-static {v10}, LX/EWC;->c(Landroid/view/View;)F

    move-result v4

    invoke-static {v14}, LX/EWC;->c(Landroid/view/View;)F

    move-result v9

    sub-float/2addr v4, v9

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 2129449
    invoke-static {v10}, LX/EWC;->d(Landroid/view/View;)F

    move-result v4

    invoke-static {v14}, LX/EWC;->d(Landroid/view/View;)F

    move-result v17

    sub-float v4, v4, v17

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 2129450
    cmpl-float v17, v9, v6

    if-ltz v17, :cond_d

    cmpl-float v17, v4, v5

    if-ltz v17, :cond_d

    move v5, v9

    move-object v6, v10

    :goto_1
    move-object v11, v6

    move v6, v5

    move v5, v4

    .line 2129451
    goto/16 :goto_0

    .line 2129452
    :cond_8
    if-eqz v11, :cond_a

    .line 2129453
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LX/EWC;->getPositionForView(Landroid/view/View;)I

    move-result v9

    .line 2129454
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, LX/EWC;->getPositionForView(Landroid/view/View;)I

    move-result v10

    .line 2129455
    const/4 v4, -0x1

    if-eq v10, v4, :cond_9

    invoke-static/range {p0 .. p0}, LX/EWC;->getAdapterInterface(LX/EWC;)LX/EVv;

    move-result-object v4

    invoke-virtual {v4}, LX/EVv;->a()I

    move-result v4

    if-lt v10, v4, :cond_b

    .line 2129456
    :cond_9
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/EWC;->k:J

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, LX/EWC;->a(LX/EWC;J)V

    .line 2129457
    :cond_a
    :goto_2
    return-void

    .line 2129458
    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10}, LX/EWC;->a(II)V

    .line 2129459
    move-object/from16 v0, p0

    iget v4, v0, LX/EWC;->h:I

    move-object/from16 v0, p0

    iput v4, v0, LX/EWC;->g:I

    .line 2129460
    move-object/from16 v0, p0

    iget v4, v0, LX/EWC;->i:I

    move-object/from16 v0, p0

    iput v4, v0, LX/EWC;->f:I

    .line 2129461
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2129462
    const/4 v4, 0x4

    invoke-virtual {v11, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2129463
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/EWC;->k:J

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, LX/EWC;->a(LX/EWC;J)V

    .line 2129464
    invoke-virtual/range {p0 .. p0}, LX/EWC;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v6

    .line 2129465
    if-eqz v6, :cond_c

    .line 2129466
    new-instance v4, LX/EW5;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v10}, LX/EW5;-><init>(LX/EWC;Landroid/view/ViewTreeObserver;IIII)V

    invoke-virtual {v6, v4}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_2

    .line 2129467
    :cond_c
    move-object/from16 v0, p0

    iget v4, v0, LX/EWC;->d:I

    add-int/2addr v4, v7

    move-object/from16 v0, p0

    iput v4, v0, LX/EWC;->d:I

    .line 2129468
    move-object/from16 v0, p0

    iget v4, v0, LX/EWC;->e:I

    add-int/2addr v4, v8

    move-object/from16 v0, p0

    iput v4, v0, LX/EWC;->e:I

    goto :goto_2

    :cond_d
    move v4, v5

    move v5, v6

    move-object v6, v11

    goto :goto_1
.end method

.method private static g(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2

    .prologue
    .line 2129433
    iget v0, p0, Landroid/graphics/Point;->y:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->x:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getAdapterInterface(LX/EWC;)LX/EVv;
    .locals 2

    .prologue
    .line 2129429
    invoke-virtual {p0}, LX/EWC;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 2129430
    :goto_0
    instance-of v1, v0, Landroid/widget/WrapperListAdapter;

    if-eqz v1, :cond_0

    .line 2129431
    check-cast v0, Landroid/widget/WrapperListAdapter;

    invoke-interface {v0}, Landroid/widget/WrapperListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    goto :goto_0

    .line 2129432
    :cond_0
    check-cast v0, LX/EVv;

    return-object v0
.end method

.method private static h(Landroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 2

    .prologue
    .line 2129428
    iget v0, p0, Landroid/graphics/Point;->y:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->x:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h$redex0(LX/EWC;)V
    .locals 6

    .prologue
    .line 2129420
    iget-object v0, p0, LX/EWC;->C:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 2129421
    iget v2, p0, LX/EWC;->A:I

    if-eqz v2, :cond_0

    .line 2129422
    iget-wide v2, p0, LX/EWC;->r:J

    const-wide/16 v4, 0xbb8

    add-long/2addr v2, v4

    .line 2129423
    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    .line 2129424
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/EWC;->b(LX/EWC;I)V

    .line 2129425
    :cond_0
    :goto_0
    return-void

    .line 2129426
    :cond_1
    sub-long v0, v2, v0

    .line 2129427
    iget-object v2, p0, LX/EWC;->D:Ljava/lang/Runnable;

    invoke-virtual {p0, v2, v0, v1}, LX/EWC;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2129417
    invoke-super {p0, p1}, LX/EQV;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2129418
    iget-object v0, p0, LX/EWC;->K:LX/EVz;

    invoke-interface {v0, p1}, LX/EVz;->a(Landroid/graphics/Canvas;)V

    .line 2129419
    return-void
.end method

.method public getHoverBehavior()LX/EW9;
    .locals 1

    .prologue
    .line 2129416
    iget-object v0, p0, LX/EWC;->L:LX/EW9;

    return-object v0
.end method

.method public getRequestedHorizontalSpacing()I
    .locals 1

    .prologue
    .line 2129415
    iget v0, p0, LX/EWC;->H:I

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4fe321ea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2129411
    invoke-super {p0}, LX/EQV;->onAttachedToWindow()V

    .line 2129412
    iget-object v1, p0, LX/EWC;->x:LX/0wd;

    iget-object v2, p0, LX/EWC;->z:LX/0xi;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2129413
    invoke-direct {p0}, LX/EWC;->b()LX/EVz;

    move-result-object v1

    iput-object v1, p0, LX/EWC;->K:LX/EVz;

    .line 2129414
    const/16 v1, 0x2d

    const v2, -0x1af7ef12

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x64739215

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2129408
    invoke-super {p0}, LX/EQV;->onDetachedFromWindow()V

    .line 2129409
    iget-object v1, p0, LX/EWC;->x:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->k()LX/0wd;

    .line 2129410
    const/16 v1, 0x2d

    const v2, 0x543e5359

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2129475
    invoke-super {p0, p1}, LX/EQV;->onDraw(Landroid/graphics/Canvas;)V

    .line 2129476
    iget-object v0, p0, LX/EWC;->c:LX/1OB;

    invoke-virtual {v0}, LX/1OB;->a()V

    .line 2129477
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2129478
    invoke-super {p0, p1, p2}, LX/EQV;->onMeasure(II)V

    .line 2129479
    iget v0, p0, LX/EWC;->G:I

    invoke-virtual {p0}, LX/EWC;->getMeasuredWidth()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/EWC;->E:I

    if-lez v0, :cond_0

    .line 2129480
    invoke-virtual {p0}, LX/EWC;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, LX/EWC;->getPaddingBottom()I

    move-result v1

    invoke-virtual {p0, v2, v0, v2, v1}, LX/EWC;->setPadding(IIII)V

    .line 2129481
    iget v0, p0, LX/EWC;->F:I

    invoke-virtual {p0, v0}, LX/EWC;->setColumnWidth(I)V

    .line 2129482
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, LX/EWC;->setNumColumns(I)V

    .line 2129483
    invoke-virtual {p0, v2}, LX/EWC;->setStretchMode(I)V

    .line 2129484
    invoke-super {p0, p1, p2}, LX/EQV;->onMeasure(II)V

    .line 2129485
    invoke-virtual {p0}, LX/EWC;->getNumColumns()I

    move-result v0

    iget v1, p0, LX/EWC;->E:I

    if-ge v0, v1, :cond_1

    .line 2129486
    iget v0, p0, LX/EWC;->E:I

    invoke-virtual {p0, v0}, LX/EWC;->setNumColumns(I)V

    .line 2129487
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/EWC;->setStretchMode(I)V

    .line 2129488
    invoke-super {p0, p1, p2}, LX/EQV;->onMeasure(II)V

    .line 2129489
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/EWC;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, LX/EWC;->G:I

    .line 2129490
    return-void

    .line 2129491
    :cond_1
    iget v0, p0, LX/EWC;->G:I

    invoke-virtual {p0}, LX/EWC;->getMeasuredWidth()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2129492
    invoke-virtual {p0}, LX/EWC;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, LX/EWC;->F:I

    invoke-virtual {p0}, LX/EWC;->getNumColumns()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-virtual {p0}, LX/EWC;->getRequestedHorizontalSpacing()I

    move-result v2

    invoke-virtual {p0}, LX/EWC;->getNumColumns()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 2129493
    div-int/lit8 v0, v0, 0x2

    .line 2129494
    invoke-virtual {p0}, LX/EWC;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, LX/EWC;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0, v0, v1, v0, v2}, LX/EWC;->setPadding(IIII)V

    .line 2129495
    invoke-super {p0, p1, p2}, LX/EQV;->onMeasure(II)V

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x2

    const v2, 0x4f4d3be1

    invoke-static {v1, v6, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2129496
    iget-boolean v2, p0, LX/EWC;->l:Z

    .line 2129497
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    packed-switch v3, :pswitch_data_0

    .line 2129498
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, LX/EQV;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x2f53b8c1

    invoke-static {v2, v1}, LX/02F;->a(II)V

    :goto_1
    return v0

    .line 2129499
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, LX/EWC;->f:I

    .line 2129500
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, LX/EWC;->g:I

    .line 2129501
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    iput v2, p0, LX/EWC;->m:I

    .line 2129502
    iget-boolean v2, p0, LX/EWC;->s:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, LX/EWC;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2129503
    invoke-virtual {p0}, LX/EWC;->layoutChildren()V

    .line 2129504
    iput v0, p0, LX/EWC;->d:I

    .line 2129505
    iput v0, p0, LX/EWC;->e:I

    .line 2129506
    iget v2, p0, LX/EWC;->f:I

    iget v3, p0, LX/EWC;->g:I

    invoke-virtual {p0, v2, v3}, LX/EWC;->pointToPosition(II)I

    move-result v2

    .line 2129507
    invoke-virtual {p0}, LX/EWC;->getFirstVisiblePosition()I

    move-result v3

    sub-int v3, v2, v3

    .line 2129508
    invoke-virtual {p0, v3}, LX/EWC;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2129509
    if-nez v3, :cond_1

    .line 2129510
    const v2, -0x192902b7

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_1

    .line 2129511
    :cond_1
    invoke-virtual {p0}, LX/EWC;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    iput-wide v4, p0, LX/EWC;->k:J

    .line 2129512
    iget-object v0, p0, LX/EWC;->K:LX/EVz;

    invoke-interface {v0, v3}, LX/EVz;->a(Landroid/view/View;)V

    .line 2129513
    const/4 v0, 0x4

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2129514
    iput-boolean v6, p0, LX/EWC;->l:Z

    .line 2129515
    iget-wide v2, p0, LX/EWC;->k:J

    invoke-static {p0, v2, v3}, LX/EWC;->a(LX/EWC;J)V

    goto :goto_0

    .line 2129516
    :cond_2
    invoke-virtual {p0}, LX/EWC;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2129517
    const v2, -0x823b35d

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_1

    .line 2129518
    :pswitch_2
    iget v2, p0, LX/EWC;->m:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 2129519
    iget v2, p0, LX/EWC;->m:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    .line 2129520
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, LX/EWC;->h:I

    .line 2129521
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, LX/EWC;->i:I

    .line 2129522
    iget v2, p0, LX/EWC;->h:I

    iget v3, p0, LX/EWC;->g:I

    sub-int/2addr v2, v3

    .line 2129523
    iget v3, p0, LX/EWC;->i:I

    iget v4, p0, LX/EWC;->f:I

    sub-int/2addr v3, v4

    .line 2129524
    iget-boolean v4, p0, LX/EWC;->l:Z

    if-eqz v4, :cond_0

    .line 2129525
    iget-object v4, p0, LX/EWC;->K:LX/EVz;

    iget v5, p0, LX/EWC;->e:I

    add-int/2addr v3, v5

    iget v5, p0, LX/EWC;->d:I

    add-int/2addr v2, v5

    invoke-interface {v4, v3, v2}, LX/EVz;->a(II)V

    .line 2129526
    invoke-virtual {p0}, LX/EWC;->invalidate()V

    .line 2129527
    invoke-static {p0}, LX/EWC;->g(LX/EWC;)V

    .line 2129528
    iput-boolean v0, p0, LX/EWC;->n:Z

    .line 2129529
    invoke-static {p0}, LX/EWC;->c(LX/EWC;)V

    .line 2129530
    const v2, 0x35bfb8ac

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto/16 :goto_1

    .line 2129531
    :pswitch_3
    invoke-static {p0}, LX/EWC;->d(LX/EWC;)V

    .line 2129532
    iget-object v0, p0, LX/EWC;->t:LX/EWB;

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    goto/16 :goto_0

    .line 2129533
    :pswitch_4
    invoke-direct {p0}, LX/EWC;->f()V

    .line 2129534
    iget-object v0, p0, LX/EWC;->t:LX/EWB;

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    goto/16 :goto_0

    .line 2129535
    :pswitch_5
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 2129536
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 2129537
    iget v2, p0, LX/EWC;->m:I

    if-ne v0, v2, :cond_0

    .line 2129538
    invoke-static {p0}, LX/EWC;->d(LX/EWC;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public setColumnWidthCompat(I)V
    .locals 0

    .prologue
    .line 2129539
    iput p1, p0, LX/EWC;->F:I

    .line 2129540
    return-void
.end method

.method public setDraggedItemBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2129541
    iput-object p1, p0, LX/EWC;->M:Landroid/graphics/drawable/Drawable;

    .line 2129542
    return-void
.end method

.method public setHorizontalSpacing(I)V
    .locals 0

    .prologue
    .line 2129543
    iput p1, p0, LX/EWC;->H:I

    .line 2129544
    invoke-super {p0, p1}, LX/EQV;->setHorizontalSpacing(I)V

    .line 2129545
    return-void
.end method

.method public setHoverBehavior(LX/EW9;)V
    .locals 1

    .prologue
    .line 2129546
    iput-object p1, p0, LX/EWC;->L:LX/EW9;

    .line 2129547
    invoke-direct {p0}, LX/EWC;->b()LX/EVz;

    move-result-object v0

    iput-object v0, p0, LX/EWC;->K:LX/EVz;

    .line 2129548
    return-void
.end method

.method public setHoverScaleFactor(D)V
    .locals 1

    .prologue
    .line 2129549
    iput-wide p1, p0, LX/EWC;->I:D

    .line 2129550
    return-void
.end method

.method public setOnAnimateListener(LX/EWA;)V
    .locals 0

    .prologue
    .line 2129551
    iput-object p1, p0, LX/EWC;->u:LX/EWA;

    .line 2129552
    return-void
.end method

.method public setOnDrawListenerTo(LX/0fu;)V
    .locals 1

    .prologue
    .line 2129553
    iget-object v0, p0, LX/EWC;->c:LX/1OB;

    invoke-virtual {v0, p1}, LX/1OB;->b(LX/0fu;)V

    .line 2129554
    return-void
.end method

.method public setOnDropListener(LX/EWB;)V
    .locals 0

    .prologue
    .line 2129555
    iput-object p1, p0, LX/EWC;->t:LX/EWB;

    .line 2129556
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .prologue
    .line 2129557
    iput-object p1, p0, LX/EWC;->Q:Landroid/widget/AdapterView$OnItemClickListener;

    .line 2129558
    iget-object v0, p0, LX/EWC;->R:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-super {p0, v0}, LX/EQV;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2129559
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 1

    .prologue
    .line 2129560
    iput-object p1, p0, LX/EWC;->O:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 2129561
    iget-object v0, p0, LX/EWC;->P:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-super {p0, v0}, LX/EQV;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 2129562
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 2129563
    iput-object p1, p0, LX/EWC;->N:Landroid/widget/AbsListView$OnScrollListener;

    .line 2129564
    iget-object v0, p0, LX/EWC;->S:Landroid/widget/AbsListView$OnScrollListener;

    invoke-super {p0, v0}, LX/EQV;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2129565
    return-void
.end method
