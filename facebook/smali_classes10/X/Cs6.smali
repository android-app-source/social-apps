.class public final LX/Cs6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/CnQ;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/Cs6;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1941830
    new-instance v0, LX/Cs6;

    invoke-direct {v0}, LX/Cs6;-><init>()V

    sput-object v0, LX/Cs6;->a:LX/Cs6;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1941831
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 1941832
    check-cast p1, LX/CnQ;

    check-cast p2, LX/CnQ;

    .line 1941833
    invoke-interface {p1}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v0

    .line 1941834
    invoke-interface {p2}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v1

    .line 1941835
    iget-object v2, v0, LX/ClU;->a:LX/ClT;

    move-object v2, v2

    .line 1941836
    iget-object v3, v1, LX/ClU;->a:LX/ClT;

    move-object v3, v3

    .line 1941837
    if-ne v2, v3, :cond_0

    .line 1941838
    iget-object v2, v0, LX/ClU;->e:LX/ClR;

    move-object v0, v2

    .line 1941839
    iget-object v2, v1, LX/ClU;->e:LX/ClR;

    move-object v1, v2

    .line 1941840
    invoke-virtual {v0, v1}, LX/ClR;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    .line 1941841
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v2, v3}, LX/ClT;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_0
.end method
