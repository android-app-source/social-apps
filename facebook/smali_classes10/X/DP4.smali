.class public LX/DP4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DOl;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2g9;


# direct methods
.method public constructor <init>(LX/0Ot;LX/2g9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DOl;",
            ">;",
            "LX/2g9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1993028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1993029
    iput-object p1, p0, LX/DP4;->a:LX/0Ot;

    .line 1993030
    iput-object p2, p0, LX/DP4;->b:LX/2g9;

    .line 1993031
    return-void
.end method

.method public static a(LX/0QB;)LX/DP4;
    .locals 5

    .prologue
    .line 1993032
    const-class v1, LX/DP4;

    monitor-enter v1

    .line 1993033
    :try_start_0
    sget-object v0, LX/DP4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1993034
    sput-object v2, LX/DP4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1993035
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1993036
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1993037
    new-instance v4, LX/DP4;

    const/16 v3, 0x240c

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v3

    check-cast v3, LX/2g9;

    invoke-direct {v4, p0, v3}, LX/DP4;-><init>(LX/0Ot;LX/2g9;)V

    .line 1993038
    move-object v0, v4

    .line 1993039
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1993040
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DP4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1993041
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1993042
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
