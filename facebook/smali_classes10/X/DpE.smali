.class public LX/DpE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final first_name:Ljava/lang/String;

.field public final last_name:Ljava/lang/String;

.field public final user_id:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xb

    .line 2043830
    new-instance v0, LX/1sv;

    const-string v1, "CallerIDPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpE;->b:LX/1sv;

    .line 2043831
    new-instance v0, LX/1sw;

    const-string v1, "user_id"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpE;->c:LX/1sw;

    .line 2043832
    new-instance v0, LX/1sw;

    const-string v1, "first_name"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpE;->d:LX/1sw;

    .line 2043833
    new-instance v0, LX/1sw;

    const-string v1, "last_name"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpE;->e:LX/1sw;

    .line 2043834
    const/4 v0, 0x1

    sput-boolean v0, LX/DpE;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2043825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2043826
    iput-object p1, p0, LX/DpE;->user_id:Ljava/lang/Long;

    .line 2043827
    iput-object p2, p0, LX/DpE;->first_name:Ljava/lang/String;

    .line 2043828
    iput-object p3, p0, LX/DpE;->last_name:Ljava/lang/String;

    .line 2043829
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2043789
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2043790
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2043791
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2043792
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CallerIDPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2043793
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043794
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043795
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043796
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043797
    const-string v4, "user_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043798
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043799
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043800
    iget-object v4, p0, LX/DpE;->user_id:Ljava/lang/Long;

    if-nez v4, :cond_3

    .line 2043801
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043802
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043803
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043804
    const-string v4, "first_name"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043805
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043806
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043807
    iget-object v4, p0, LX/DpE;->first_name:Ljava/lang/String;

    if-nez v4, :cond_4

    .line 2043808
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043809
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043810
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043811
    const-string v4, "last_name"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043812
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043813
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043814
    iget-object v0, p0, LX/DpE;->last_name:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 2043815
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043816
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043817
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043818
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2043819
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2043820
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2043821
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2043822
    :cond_3
    iget-object v4, p0, LX/DpE;->user_id:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2043823
    :cond_4
    iget-object v4, p0, LX/DpE;->first_name:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 2043824
    :cond_5
    iget-object v0, p0, LX/DpE;->last_name:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2043776
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2043777
    iget-object v0, p0, LX/DpE;->user_id:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2043778
    sget-object v0, LX/DpE;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043779
    iget-object v0, p0, LX/DpE;->user_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2043780
    :cond_0
    iget-object v0, p0, LX/DpE;->first_name:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2043781
    sget-object v0, LX/DpE;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043782
    iget-object v0, p0, LX/DpE;->first_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2043783
    :cond_1
    iget-object v0, p0, LX/DpE;->last_name:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2043784
    sget-object v0, LX/DpE;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2043785
    iget-object v0, p0, LX/DpE;->last_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2043786
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2043787
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2043788
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2043744
    if-nez p1, :cond_1

    .line 2043745
    :cond_0
    :goto_0
    return v0

    .line 2043746
    :cond_1
    instance-of v1, p1, LX/DpE;

    if-eqz v1, :cond_0

    .line 2043747
    check-cast p1, LX/DpE;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2043748
    if-nez p1, :cond_3

    .line 2043749
    :cond_2
    :goto_1
    move v0, v2

    .line 2043750
    goto :goto_0

    .line 2043751
    :cond_3
    iget-object v0, p0, LX/DpE;->user_id:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2043752
    :goto_2
    iget-object v3, p1, LX/DpE;->user_id:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2043753
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2043754
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043755
    iget-object v0, p0, LX/DpE;->user_id:Ljava/lang/Long;

    iget-object v3, p1, LX/DpE;->user_id:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043756
    :cond_5
    iget-object v0, p0, LX/DpE;->first_name:Ljava/lang/String;

    if-eqz v0, :cond_c

    move v0, v1

    .line 2043757
    :goto_4
    iget-object v3, p1, LX/DpE;->first_name:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v3, v1

    .line 2043758
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2043759
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043760
    iget-object v0, p0, LX/DpE;->first_name:Ljava/lang/String;

    iget-object v3, p1, LX/DpE;->first_name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2043761
    :cond_7
    iget-object v0, p0, LX/DpE;->last_name:Ljava/lang/String;

    if-eqz v0, :cond_e

    move v0, v1

    .line 2043762
    :goto_6
    iget-object v3, p1, LX/DpE;->last_name:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v3, v1

    .line 2043763
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2043764
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2043765
    iget-object v0, p0, LX/DpE;->last_name:Ljava/lang/String;

    iget-object v3, p1, LX/DpE;->last_name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 2043766
    goto :goto_1

    :cond_a
    move v0, v2

    .line 2043767
    goto :goto_2

    :cond_b
    move v3, v2

    .line 2043768
    goto :goto_3

    :cond_c
    move v0, v2

    .line 2043769
    goto :goto_4

    :cond_d
    move v3, v2

    .line 2043770
    goto :goto_5

    :cond_e
    move v0, v2

    .line 2043771
    goto :goto_6

    :cond_f
    move v3, v2

    .line 2043772
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2043743
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2043773
    sget-boolean v0, LX/DpE;->a:Z

    .line 2043774
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpE;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2043775
    return-object v0
.end method
