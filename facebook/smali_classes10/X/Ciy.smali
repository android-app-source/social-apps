.class public LX/Ciy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/Chv;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Cit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Chv;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1928922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1928923
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ciy;->b:Ljava/util/List;

    .line 1928924
    iput-object p1, p0, LX/Ciy;->a:LX/Chv;

    .line 1928925
    return-void
.end method

.method public static a(LX/0QB;)LX/Ciy;
    .locals 4

    .prologue
    .line 1928926
    const-class v1, LX/Ciy;

    monitor-enter v1

    .line 1928927
    :try_start_0
    sget-object v0, LX/Ciy;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1928928
    sput-object v2, LX/Ciy;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1928929
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1928930
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1928931
    new-instance p0, LX/Ciy;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v3

    check-cast v3, LX/Chv;

    invoke-direct {p0, v3}, LX/Ciy;-><init>(LX/Chv;)V

    .line 1928932
    move-object v0, p0

    .line 1928933
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1928934
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ciy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1928935
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1928936
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1928937
    iget-object v0, p0, LX/Ciy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cit;

    .line 1928938
    invoke-interface {v0}, LX/Cit;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1928939
    const/4 v0, 0x0

    .line 1928940
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
