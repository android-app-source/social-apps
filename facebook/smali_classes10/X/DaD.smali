.class public final LX/DaD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V
    .locals 0

    .prologue
    .line 2014641
    iput-object p1, p0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x5d6bb4e7

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2014642
    iget-object v1, p0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    invoke-static {v1}, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->d$redex0(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V

    .line 2014643
    iget-object v1, p0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    const/4 v3, -0x1

    .line 2014644
    iget-object v2, v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->w:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2014645
    const v2, 0x7f081b63

    .line 2014646
    :goto_0
    if-eq v2, v3, :cond_3

    .line 2014647
    iget-object v3, v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->l:LX/0kL;

    new-instance v4, LX/27k;

    invoke-direct {v4, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v3, v4}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2014648
    const/4 v2, 0x0

    .line 2014649
    :goto_1
    move v1, v2

    .line 2014650
    if-eqz v1, :cond_1

    .line 2014651
    iget-object v1, p0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v1, v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->a:LX/1g8;

    iget-object v2, p0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v2, v2, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->u:Ljava/lang/String;

    .line 2014652
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "group_side_conversation_started"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "group_mall_side_conversation"

    .line 2014653
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2014654
    move-object v3, v3

    .line 2014655
    const-string v4, "group_id"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2014656
    iget-object v4, v1, LX/1g8;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2014657
    invoke-static {v1, v3}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2014658
    iget-object v1, p0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    const/4 v4, 0x0

    .line 2014659
    iget-object v2, v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->B:Landroid/support/v4/app/DialogFragment;

    if-nez v2, :cond_0

    .line 2014660
    const v2, 0x7f081b60

    const/4 v3, 0x1

    invoke-static {v2, v3, v4, v4}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->B:Landroid/support/v4/app/DialogFragment;

    .line 2014661
    :cond_0
    iget-object v2, v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->B:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2014662
    iget-object v1, p0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v1, v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->h:LX/1Ck;

    sget-object v2, LX/DaL;->START_SIDE_CONVERSATION:LX/DaL;

    new-instance v3, LX/Da9;

    invoke-direct {v3, p0}, LX/Da9;-><init>(LX/DaD;)V

    new-instance v4, LX/DaA;

    invoke-direct {v4, p0}, LX/DaA;-><init>(LX/DaD;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2014663
    :cond_1
    const v1, 0x6efe4a96

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2014664
    :cond_2
    iget-object v2, v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->C:Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    invoke-virtual {v2}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->e()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v4, 0x2

    if-ge v2, v4, :cond_4

    .line 2014665
    const v2, 0x7f081b64

    goto :goto_0

    .line 2014666
    :cond_3
    const/4 v2, 0x1

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_0
.end method
