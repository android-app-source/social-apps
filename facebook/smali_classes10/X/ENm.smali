.class public LX/ENm;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/widget/ImageView;

.field public b:Lcom/facebook/widget/text/BetterTextView;

.field public c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2113441
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2113442
    const v0, 0x7f030c12

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2113443
    const v0, 0x7f0d1dca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/ENm;->a:Landroid/widget/ImageView;

    .line 2113444
    const v0, 0x7f0d1dcc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/ENm;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2113445
    const v0, 0x7f0d1dcd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/ENm;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2113446
    iget-object v0, p0, LX/ENm;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/ENm;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f021125

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2113447
    iget-object v0, p0, LX/ENm;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, LX/ENm;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f08227f

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2113448
    return-void
.end method


# virtual methods
.method public getQueryText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2113440
    iget-object v0, p0, LX/ENm;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
