.class public final LX/Dnp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2041096
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2041097
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2041098
    :goto_0
    return v1

    .line 2041099
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2041100
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2041101
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2041102
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2041103
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2041104
    const-string v6, "associated_users"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2041105
    invoke-static {p0, p1}, LX/5dJ;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2041106
    :cond_2
    const-string v6, "suggested_reason"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2041107
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2041108
    :cond_3
    const-string v6, "suggested_thread"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2041109
    invoke-static {p0, p1}, LX/Dno;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2041110
    :cond_4
    const-string v6, "tracking_data"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2041111
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2041112
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2041113
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2041114
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2041115
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2041116
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2041117
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2041118
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2041119
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2041120
    if-eqz v0, :cond_0

    .line 2041121
    const-string v1, "associated_users"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2041122
    invoke-static {p0, v0, p2, p3}, LX/5dJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2041123
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2041124
    if-eqz v0, :cond_1

    .line 2041125
    const-string v1, "suggested_reason"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2041126
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2041127
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2041128
    if-eqz v0, :cond_2

    .line 2041129
    const-string v1, "suggested_thread"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2041130
    invoke-static {p0, v0, p2, p3}, LX/Dno;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2041131
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2041132
    if-eqz v0, :cond_3

    .line 2041133
    const-string v1, "tracking_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2041134
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2041135
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2041136
    return-void
.end method
