.class public final LX/Cqh;
.super LX/Cqf;
.source ""


# instance fields
.field public final synthetic g:LX/Cql;


# direct methods
.method public constructor <init>(LX/Cql;LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V
    .locals 7

    .prologue
    .line 1940336
    iput-object p1, p0, LX/Cqh;->g:LX/Cql;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    return-void
.end method


# virtual methods
.method public final g()V
    .locals 5

    .prologue
    .line 1940337
    invoke-virtual {p0}, LX/Cqf;->k()F

    move-result v0

    .line 1940338
    invoke-virtual {p0}, LX/Cqf;->l()F

    move-result v1

    .line 1940339
    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v1, LX/CoL;->q:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1940340
    invoke-super {p0}, LX/Cqf;->g()V

    .line 1940341
    :goto_0
    return-void

    .line 1940342
    :cond_0
    iget-object v0, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v0, v0

    .line 1940343
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 1940344
    iget-object v1, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v1, v1

    .line 1940345
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 1940346
    int-to-float v2, v1

    invoke-virtual {p0}, LX/Cqf;->l()F

    move-result v3

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1940347
    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 1940348
    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    add-int/2addr v2, v0

    add-int/lit8 v1, v1, 0x0

    invoke-direct {v3, v0, v4, v2, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1940349
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/CrW;

    invoke-direct {v1, v3}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v1}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    goto :goto_0
.end method
