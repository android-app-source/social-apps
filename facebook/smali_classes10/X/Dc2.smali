.class public final enum LX/Dc2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dc2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dc2;

.field public static final enum LINK_MENU:LX/Dc2;

.field public static final enum PHOTO_MENU:LX/Dc2;

.field public static final enum STRUCTURED_MENU:LX/Dc2;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2017797
    new-instance v0, LX/Dc2;

    const-string v1, "PHOTO_MENU"

    invoke-direct {v0, v1, v2}, LX/Dc2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dc2;->PHOTO_MENU:LX/Dc2;

    .line 2017798
    new-instance v0, LX/Dc2;

    const-string v1, "STRUCTURED_MENU"

    invoke-direct {v0, v1, v3}, LX/Dc2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dc2;->STRUCTURED_MENU:LX/Dc2;

    .line 2017799
    new-instance v0, LX/Dc2;

    const-string v1, "LINK_MENU"

    invoke-direct {v0, v1, v4}, LX/Dc2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dc2;->LINK_MENU:LX/Dc2;

    .line 2017800
    const/4 v0, 0x3

    new-array v0, v0, [LX/Dc2;

    sget-object v1, LX/Dc2;->PHOTO_MENU:LX/Dc2;

    aput-object v1, v0, v2

    sget-object v1, LX/Dc2;->STRUCTURED_MENU:LX/Dc2;

    aput-object v1, v0, v3

    sget-object v1, LX/Dc2;->LINK_MENU:LX/Dc2;

    aput-object v1, v0, v4

    sput-object v0, LX/Dc2;->$VALUES:[LX/Dc2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2017796
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dc2;
    .locals 1

    .prologue
    .line 2017795
    const-class v0, LX/Dc2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dc2;

    return-object v0
.end method

.method public static values()[LX/Dc2;
    .locals 1

    .prologue
    .line 2017794
    sget-object v0, LX/Dc2;->$VALUES:[LX/Dc2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dc2;

    return-object v0
.end method
