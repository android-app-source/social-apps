.class public final LX/CkE;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "LX/Cjt;",
        "LX/CkF;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/CkJ;


# direct methods
.method public constructor <init>(LX/CkJ;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1930480
    iput-object p1, p0, LX/CkE;->this$0:LX/CkJ;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 1930481
    new-instance v4, LX/Ck5;

    invoke-direct {v4, p0}, LX/Ck5;-><init>(LX/CkE;)V

    .line 1930482
    sget-object v6, LX/Cjt;->TEXT_KICKER:LX/Cjt;

    new-instance v0, LX/CkI;

    iget-object v1, p0, LX/CkE;->this$0:LX/CkJ;

    sget v2, LX/CkJ;->d:I

    sget v3, LX/CkJ;->c:I

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LX/CkI;-><init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V

    invoke-virtual {p0, v6, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930483
    sget-object v6, LX/Cjt;->TEXT_TITLE:LX/Cjt;

    new-instance v0, LX/CkI;

    iget-object v1, p0, LX/CkE;->this$0:LX/CkJ;

    sget v2, LX/CkJ;->d:I

    sget v3, LX/CkJ;->d:I

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LX/CkI;-><init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V

    invoke-virtual {p0, v6, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930484
    sget-object v0, LX/Cjt;->TEXT_SUBTITLE:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->c:I

    sget v4, LX/CkJ;->d:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930485
    sget-object v0, LX/Cjt;->TEXT_BYLINE:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->d:I

    sget v4, LX/CkJ;->f:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930486
    sget-object v0, LX/Cjt;->TEXT_AUTHOR_PIC:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->d:I

    sget v4, LX/CkJ;->f:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930487
    sget-object v0, LX/Cjt;->TEXT_BODY:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->d:I

    sget v4, LX/CkJ;->d:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930488
    new-instance v0, LX/CkI;

    iget-object v1, p0, LX/CkE;->this$0:LX/CkJ;

    sget v2, LX/CkJ;->g:I

    sget v3, LX/CkJ;->d:I

    new-instance v4, LX/Ck6;

    invoke-direct {v4, p0}, LX/Ck6;-><init>(LX/CkE;)V

    new-instance v5, LX/Ck7;

    invoke-direct {v5, p0}, LX/Ck7;-><init>(LX/CkE;)V

    invoke-direct/range {v0 .. v5}, LX/CkI;-><init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V

    .line 1930489
    sget-object v1, LX/Cjt;->TEXT_H1:LX/Cjt;

    invoke-virtual {p0, v1, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930490
    sget-object v1, LX/Cjt;->TEXT_H2:LX/Cjt;

    invoke-virtual {p0, v1, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930491
    sget-object v0, LX/Cjt;->TEXT_BLOCK_QUOTE:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->g:I

    sget v4, LX/CkJ;->g:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930492
    new-instance v0, LX/CkI;

    iget-object v1, p0, LX/CkE;->this$0:LX/CkJ;

    sget v2, LX/CkJ;->g:I

    sget v3, LX/CkJ;->g:I

    new-instance v4, LX/Ck8;

    invoke-direct {v4, p0}, LX/Ck8;-><init>(LX/CkE;)V

    new-instance v5, LX/Ck9;

    invoke-direct {v5, p0}, LX/Ck9;-><init>(LX/CkE;)V

    invoke-direct/range {v0 .. v5}, LX/CkI;-><init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V

    .line 1930493
    sget-object v1, LX/Cjt;->TEXT_PULL_QUOTE:LX/Cjt;

    invoke-virtual {p0, v1, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930494
    sget-object v1, LX/Cjt;->TEXT_PULL_QUOTE_ATTRIBUTION:LX/Cjt;

    invoke-virtual {p0, v1, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930495
    sget-object v0, LX/Cjt;->TEXT_CODE:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    invoke-direct {v1, v2, v7, v7}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930496
    new-instance v0, LX/CkI;

    iget-object v1, p0, LX/CkE;->this$0:LX/CkJ;

    sget v2, LX/CkJ;->c:I

    sget v3, LX/CkJ;->c:I

    new-instance v5, LX/CkA;

    invoke-direct {v5, p0}, LX/CkA;-><init>(LX/CkE;)V

    move-object v4, v8

    invoke-direct/range {v0 .. v5}, LX/CkI;-><init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V

    .line 1930497
    sget-object v1, LX/Cjt;->TEXT_CAPTION_SMALL:LX/Cjt;

    invoke-virtual {p0, v1, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930498
    sget-object v1, LX/Cjt;->TEXT_CAPTION_MEDIUM:LX/Cjt;

    invoke-virtual {p0, v1, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930499
    new-instance v0, LX/CkI;

    iget-object v1, p0, LX/CkE;->this$0:LX/CkJ;

    sget v2, LX/CkJ;->d:I

    sget v3, LX/CkJ;->c:I

    new-instance v5, LX/CkB;

    invoke-direct {v5, p0}, LX/CkB;-><init>(LX/CkE;)V

    move-object v4, v8

    invoke-direct/range {v0 .. v5}, LX/CkI;-><init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V

    .line 1930500
    sget-object v1, LX/Cjt;->TEXT_CAPTION_LARGE:LX/Cjt;

    invoke-virtual {p0, v1, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930501
    sget-object v1, LX/Cjt;->TEXT_CAPTION_XLARGE:LX/Cjt;

    invoke-virtual {p0, v1, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930502
    sget-object v0, LX/Cjt;->TEXT_CAPTION_CREDIT:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->a:I

    sget v4, LX/CkJ;->c:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930503
    sget-object v0, LX/Cjt;->TEXT_ELEMENT_UFI:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->c:I

    sget v4, LX/CkJ;->f:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930504
    new-instance v0, LX/CkI;

    iget-object v1, p0, LX/CkE;->this$0:LX/CkJ;

    sget v2, LX/CkJ;->d:I

    sget v3, LX/CkJ;->d:I

    new-instance v4, LX/CkC;

    invoke-direct {v4, p0}, LX/CkC;-><init>(LX/CkE;)V

    new-instance v5, LX/CkD;

    invoke-direct {v5, p0}, LX/CkD;-><init>(LX/CkE;)V

    invoke-direct/range {v0 .. v5}, LX/CkI;-><init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V

    .line 1930505
    sget-object v1, LX/Cjt;->TEXT_BULLETED_LIST:LX/Cjt;

    invoke-virtual {p0, v1, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930506
    sget-object v1, LX/Cjt;->TEXT_NUMBERED_LIST:LX/Cjt;

    invoke-virtual {p0, v1, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930507
    sget-object v0, LX/Cjt;->TEXT_END_CREDITS_BAR:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->d:I

    sget v4, LX/CkJ;->c:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930508
    sget-object v0, LX/Cjt;->TEXT_END_CREDITS:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->c:I

    sget v4, LX/CkJ;->d:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930509
    sget-object v0, LX/Cjt;->MEDIA_WITH_ABOVE_AND_BELOW_CAPTION:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->f:I

    sget v4, LX/CkJ;->c:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930510
    sget-object v6, LX/Cjt;->MEDIA_WITH_ABOVE_CAPTION:LX/Cjt;

    new-instance v0, LX/CkI;

    iget-object v1, p0, LX/CkE;->this$0:LX/CkJ;

    sget v2, LX/CkJ;->f:I

    sget v3, LX/CkJ;->f:I

    new-instance v5, LX/Ck1;

    invoke-direct {v5, p0}, LX/Ck1;-><init>(LX/CkE;)V

    move-object v4, v8

    invoke-direct/range {v0 .. v5}, LX/CkI;-><init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V

    invoke-virtual {p0, v6, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930511
    sget-object v6, LX/Cjt;->MEDIA_WITH_BELOW_CAPTION:LX/Cjt;

    new-instance v0, LX/CkI;

    iget-object v1, p0, LX/CkE;->this$0:LX/CkJ;

    sget v2, LX/CkJ;->f:I

    sget v3, LX/CkJ;->f:I

    new-instance v4, LX/Ck2;

    invoke-direct {v4, p0}, LX/Ck2;-><init>(LX/CkE;)V

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, LX/CkI;-><init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V

    invoke-virtual {p0, v6, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930512
    sget-object v6, LX/Cjt;->MEDIA_WITHOUT_ABOVE_OR_BELOW_CAPTION:LX/Cjt;

    new-instance v0, LX/CkI;

    iget-object v1, p0, LX/CkE;->this$0:LX/CkJ;

    sget v2, LX/CkJ;->f:I

    sget v3, LX/CkJ;->f:I

    new-instance v4, LX/Ck3;

    invoke-direct {v4, p0}, LX/Ck3;-><init>(LX/CkE;)V

    new-instance v5, LX/Ck4;

    invoke-direct {v5, p0}, LX/Ck4;-><init>(LX/CkE;)V

    invoke-direct/range {v0 .. v5}, LX/CkI;-><init>(LX/CkJ;IILjava/util/Map;Ljava/util/Map;)V

    invoke-virtual {p0, v6, v0}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930513
    sget-object v0, LX/Cjt;->AD_WITH_CAPTION:LX/Cjt;

    new-instance v1, LX/CkG;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, LX/CkG;-><init>(LX/CkJ;Z)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930514
    sget-object v0, LX/Cjt;->AD_WITHOUT_CAPTION:LX/Cjt;

    new-instance v1, LX/CkG;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    invoke-direct {v1, v2, v7}, LX/CkG;-><init>(LX/CkJ;Z)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930515
    sget-object v0, LX/Cjt;->MAP_WITH_CAPTION:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->f:I

    sget v4, LX/CkJ;->c:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930516
    sget-object v0, LX/Cjt;->MAP_WITHOUT_CAPTION:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->f:I

    sget v4, LX/CkJ;->f:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930517
    sget-object v0, LX/Cjt;->HTML_WITH_CAPTION:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->f:I

    sget v4, LX/CkJ;->c:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930518
    sget-object v0, LX/Cjt;->HTML_WITHOUT_CAPTION:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->f:I

    sget v4, LX/CkJ;->f:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930519
    sget-object v0, LX/Cjt;->SOCIAL_EMBED_WITH_CAPTION:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->f:I

    sget v4, LX/CkJ;->c:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930520
    sget-object v0, LX/Cjt;->SOCIAL_EMBED_WITHOUT_CAPTION:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->f:I

    sget v4, LX/CkJ;->f:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930521
    sget-object v0, LX/Cjt;->RELATED_ARTICLE_CELL:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->b:I

    sget v4, LX/CkJ;->c:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930522
    sget-object v0, LX/Cjt;->AUTHORS_CONTRIBUTORS_HEADER:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->g:I

    sget v4, LX/CkJ;->a:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930523
    sget-object v0, LX/Cjt;->RELATED_ARTICLES_HEADER:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->b:I

    invoke-direct {v1, v2, v7, v3}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930524
    sget-object v0, LX/Cjt;->INLINE_RELATED_ARTICLES_HEADER:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->f:I

    sget v4, LX/CkJ;->a:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930525
    sget-object v0, LX/Cjt;->INLINE_RELATED_ARTICLES_FOOTER:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->c:I

    sget v4, LX/CkJ;->f:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930526
    sget-object v0, LX/Cjt;->SHARE_BUTTON:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->e:I

    sget v4, LX/CkJ;->e:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930527
    sget-object v0, LX/Cjt;->VIDEO_SEEK_BAR:LX/Cjt;

    new-instance v1, LX/CkH;

    iget-object v2, p0, LX/CkE;->this$0:LX/CkJ;

    sget v3, LX/CkJ;->b:I

    sget v4, LX/CkJ;->c:I

    invoke-direct {v1, v2, v3, v4}, LX/CkH;-><init>(LX/CkJ;II)V

    invoke-virtual {p0, v0, v1}, LX/CkE;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930528
    return-void
.end method
