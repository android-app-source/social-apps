.class public LX/D4u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/D4u;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/D33;

.field public final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/D33;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1963035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1963036
    iput-object p1, p0, LX/D4u;->a:Landroid/content/res/Resources;

    .line 1963037
    iput-object p2, p0, LX/D4u;->b:LX/D33;

    .line 1963038
    iput-object p3, p0, LX/D4u;->c:Landroid/content/Context;

    .line 1963039
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;)LX/D4s;
    .locals 6
    .param p0    # Lcom/facebook/graphql/model/GraphQLActor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1963040
    if-nez p0, :cond_1

    .line 1963041
    :cond_0
    :goto_0
    return-object v1

    .line 1963042
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1963043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1963044
    invoke-static {p0}, LX/1xl;->f(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v3

    .line 1963045
    if-eqz v3, :cond_0

    .line 1963046
    new-instance v4, LX/D4r;

    invoke-direct {v4}, LX/D4r;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    .line 1963047
    iput-object v5, v4, LX/D4r;->a:Ljava/lang/String;

    .line 1963048
    move-object v4, v4

    .line 1963049
    iput-object p1, v4, LX/D4r;->b:Ljava/lang/String;

    .line 1963050
    move-object v4, v4

    .line 1963051
    if-eqz v0, :cond_3

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    .line 1963052
    :goto_1
    iput-object v0, v4, LX/D4r;->c:Ljava/lang/String;

    .line 1963053
    move-object v0, v4

    .line 1963054
    if-eqz v2, :cond_2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 1963055
    :cond_2
    iput-object v1, v0, LX/D4r;->d:Ljava/lang/String;

    .line 1963056
    move-object v0, v0

    .line 1963057
    iput-object v3, v0, LX/D4r;->f:Ljava/lang/String;

    .line 1963058
    move-object v0, v0

    .line 1963059
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v1

    .line 1963060
    iput-boolean v1, v0, LX/D4r;->g:Z

    .line 1963061
    move-object v0, v0

    .line 1963062
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v1

    .line 1963063
    iput-boolean v1, v0, LX/D4r;->h:Z

    .line 1963064
    move-object v0, v0

    .line 1963065
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v1

    .line 1963066
    iput-boolean v1, v0, LX/D4r;->i:Z

    .line 1963067
    move-object v0, v0

    .line 1963068
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v1

    .line 1963069
    iput-boolean v1, v0, LX/D4r;->j:Z

    .line 1963070
    move-object v0, v0

    .line 1963071
    iput-object p0, v0, LX/D4r;->n:LX/0jT;

    .line 1963072
    move-object v0, v0

    .line 1963073
    invoke-virtual {v0}, LX/D4r;->a()LX/D4s;

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/D4u;
    .locals 8

    .prologue
    .line 1963074
    sget-object v0, LX/D4u;->d:LX/D4u;

    if-nez v0, :cond_1

    .line 1963075
    const-class v1, LX/D4u;

    monitor-enter v1

    .line 1963076
    :try_start_0
    sget-object v0, LX/D4u;->d:LX/D4u;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1963077
    if-eqz v2, :cond_0

    .line 1963078
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1963079
    new-instance v6, LX/D4u;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    .line 1963080
    new-instance p0, LX/D33;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {p0, v4, v5, v7}, LX/D33;-><init>(LX/1Ck;LX/0tX;Ljava/lang/String;)V

    .line 1963081
    move-object v4, p0

    .line 1963082
    check-cast v4, LX/D33;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {v6, v3, v4, v5}, LX/D4u;-><init>(Landroid/content/res/Resources;LX/D33;Landroid/content/Context;)V

    .line 1963083
    move-object v0, v6

    .line 1963084
    sput-object v0, LX/D4u;->d:LX/D4u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1963085
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1963086
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1963087
    :cond_1
    sget-object v0, LX/D4u;->d:LX/D4u;

    return-object v0

    .line 1963088
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1963089
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
