.class public LX/DCL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0bH;

.field public final b:LX/967;

.field private c:LX/DCK;

.field public d:LX/0qq;

.field public e:LX/0g4;


# direct methods
.method public constructor <init>(LX/0bH;LX/967;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1973939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1973940
    iput-object p1, p0, LX/DCL;->a:LX/0bH;

    .line 1973941
    iput-object p2, p0, LX/DCL;->b:LX/967;

    .line 1973942
    return-void
.end method

.method public static a(LX/0QB;)LX/DCL;
    .locals 3

    .prologue
    .line 1973943
    new-instance v2, LX/DCL;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v0

    check-cast v0, LX/0bH;

    invoke-static {p0}, LX/967;->b(LX/0QB;)LX/967;

    move-result-object v1

    check-cast v1, LX/967;

    invoke-direct {v2, v0, v1}, LX/DCL;-><init>(LX/0bH;LX/967;)V

    .line 1973944
    move-object v0, v2

    .line 1973945
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1973936
    iget-object v0, p0, LX/DCL;->c:LX/DCK;

    if-eqz v0, :cond_0

    .line 1973937
    iget-object v0, p0, LX/DCL;->a:LX/0bH;

    iget-object v1, p0, LX/DCL;->c:LX/DCK;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1973938
    :cond_0
    return-void
.end method

.method public final a(LX/0qq;LX/0g4;)V
    .locals 2
    .param p2    # LX/0g4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1973931
    new-instance v0, LX/DCK;

    invoke-direct {v0, p0}, LX/DCK;-><init>(LX/DCL;)V

    iput-object v0, p0, LX/DCL;->c:LX/DCK;

    .line 1973932
    iget-object v0, p0, LX/DCL;->a:LX/0bH;

    iget-object v1, p0, LX/DCL;->c:LX/DCK;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1973933
    iput-object p1, p0, LX/DCL;->d:LX/0qq;

    .line 1973934
    iput-object p2, p0, LX/DCL;->e:LX/0g4;

    .line 1973935
    return-void
.end method
