.class public LX/DxD;
.super Lcom/facebook/widget/listview/BetterListView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2062566
    invoke-direct {p0, p1}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;)V

    .line 2062567
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    .line 2062568
    invoke-virtual {p0}, LX/DxD;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0a04ba

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2062569
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2062570
    invoke-virtual {p0, v0}, LX/DxD;->setDrawingCacheBackgroundColor(I)V

    .line 2062571
    invoke-virtual {p0, p1}, LX/DxD;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2062572
    invoke-virtual {p0, p1}, LX/DxD;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 2062573
    invoke-virtual {p0, p1}, LX/DxD;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2062574
    invoke-virtual {p0, p1}, LX/DxD;->setOverscrollFooter(Landroid/graphics/drawable/Drawable;)V

    .line 2062575
    invoke-virtual {p0}, LX/DxD;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b0b48

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/DxD;->setDividerHeight(I)V

    .line 2062576
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DxD;->setCacheColorHint(I)V

    .line 2062577
    new-instance v0, LX/DxC;

    invoke-direct {v0, p0}, LX/DxC;-><init>(LX/DxD;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2062578
    return-void
.end method
