.class public final LX/DfP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2027157
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2027158
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2027159
    :goto_0
    return v1

    .line 2027160
    :cond_0
    const-string v8, "show_app_badge"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2027161
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v5, v3

    move v3, v2

    .line 2027162
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 2027163
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2027164
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2027165
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2027166
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2027167
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2027168
    :cond_2
    const-string v8, "show_highlight_badge"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2027169
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2027170
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2027171
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2027172
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2027173
    if-eqz v3, :cond_5

    .line 2027174
    invoke-virtual {p1, v2, v5}, LX/186;->a(IZ)V

    .line 2027175
    :cond_5
    if-eqz v0, :cond_6

    .line 2027176
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 2027177
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 2027178
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2027179
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2027180
    if-eqz v0, :cond_0

    .line 2027181
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2027182
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2027183
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2027184
    if-eqz v0, :cond_1

    .line 2027185
    const-string v1, "show_app_badge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2027186
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2027187
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2027188
    if-eqz v0, :cond_2

    .line 2027189
    const-string v1, "show_highlight_badge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2027190
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2027191
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2027192
    return-void
.end method
