.class public final LX/EoX;
.super LX/En6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/En6",
        "<",
        "Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EoY;


# direct methods
.method public constructor <init>(LX/EoY;)V
    .locals 0

    .prologue
    .line 2168216
    iput-object p1, p0, LX/EoX;->a:LX/EoY;

    invoke-direct {p0}, LX/En6;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/Emj;Landroid/os/Bundle;)LX/Emg;
    .locals 10

    .prologue
    .line 2168217
    const/4 v2, 0x0

    .line 2168218
    const/4 v1, 0x0

    .line 2168219
    if-eqz p7, :cond_0

    .line 2168220
    const-string v1, "extra_friending_location_name"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2168221
    const-string v1, "extra_friend_request_make_ref"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2168222
    :cond_0
    const/4 v8, 0x0

    .line 2168223
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2168224
    invoke-static {v2}, LX/2h7;->valueOf(Ljava/lang/String;)LX/2h7;

    move-result-object v8

    .line 2168225
    :cond_1
    const/4 v9, 0x0

    .line 2168226
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2168227
    invoke-static {v1}, LX/5P2;->valueOf(Ljava/lang/String;)LX/5P2;

    move-result-object v9

    .line 2168228
    :cond_2
    iget-object v1, p0, LX/EoX;->a:LX/EoY;

    iget-object v1, v1, LX/EoY;->b:LX/Eok;

    move-object v2, p1

    move-object/from16 v3, p6

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-virtual/range {v1 .. v9}, LX/Eok;->a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;LX/Emj;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/2h7;LX/5P2;)LX/Eoj;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/Emj;Landroid/os/Bundle;)LX/Emg;
    .locals 8

    .prologue
    .line 2168215
    move-object v1, p1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, LX/EoX;->a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/Emj;Landroid/os/Bundle;)LX/Emg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)LX/EoJ;
    .locals 1

    .prologue
    .line 2168214
    iget-object v0, p0, LX/EoX;->a:LX/EoY;

    iget-object v0, v0, LX/EoY;->a:LX/EoJ;

    return-object v0
.end method
