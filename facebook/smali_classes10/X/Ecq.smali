.class public LX/Ecq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2148027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Z)I
    .locals 2

    .prologue
    .line 2148028
    :try_start_0
    const-string v0, "SHA1PRNG"

    invoke-static {v0}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v0

    .line 2148029
    if-eqz p0, :cond_0

    const v1, 0x7ffffffe

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2148030
    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x3ffc

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextInt(I)I
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2148031
    :catch_0
    move-exception v0

    .line 2148032
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static a(LX/Eaf;I)LX/Ebk;
    .locals 6

    .prologue
    .line 2148033
    invoke-static {}, LX/Ear;->a()LX/Eau;

    move-result-object v4

    .line 2148034
    iget-object v0, p0, LX/Eaf;->b:LX/Eas;

    move-object v0, v0

    .line 2148035
    iget-object v1, v4, LX/Eau;->a:LX/Eat;

    move-object v1, v1

    .line 2148036
    invoke-virtual {v1}, LX/Eat;->a()[B

    move-result-object v1

    invoke-static {v0, v1}, LX/Ear;->a(LX/Eas;[B)[B

    move-result-object v5

    .line 2148037
    new-instance v0, LX/Ebk;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move v1, p1

    invoke-direct/range {v0 .. v5}, LX/Ebk;-><init>(IJLX/Eau;[B)V

    return-object v0
.end method

.method public static a(II)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "LX/Ebg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2148038
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 2148039
    add-int/lit8 v2, p0, -0x1

    .line 2148040
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 2148041
    new-instance v3, LX/Ebg;

    add-int v4, v2, v0

    sget v5, LX/2PB;->a:I

    add-int/lit8 v5, v5, -0x1

    rem-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    invoke-static {}, LX/Ear;->a()LX/Eau;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/Ebg;-><init>(ILX/Eau;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2148042
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2148043
    :cond_0
    return-object v1
.end method

.method public static d()[B
    .locals 2

    .prologue
    .line 2148044
    const/16 v0, 0x20

    :try_start_0
    new-array v0, v0, [B

    .line 2148045
    const-string v1, "SHA1PRNG"

    invoke-static {v1}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2148046
    return-object v0

    .line 2148047
    :catch_0
    move-exception v0

    .line 2148048
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static e()I
    .locals 2

    .prologue
    .line 2148049
    :try_start_0
    const-string v0, "SHA1PRNG"

    invoke-static {v0}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v0

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextInt(I)I
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 2148050
    :catch_0
    move-exception v0

    .line 2148051
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method
