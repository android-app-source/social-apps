.class public LX/CxH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CxG;


# instance fields
.field private final a:LX/CzB;


# direct methods
.method public constructor <init>(LX/CzB;)V
    .locals 0
    .param p1    # LX/CzB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951638
    iput-object p1, p0, LX/CxH;->a:LX/CzB;

    .line 1951639
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1951631
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v0, :cond_0

    .line 1951632
    iget-object v0, p0, LX/CxH;->a:LX/CzB;

    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0, p1}, LX/CzB;->b(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v0

    .line 1951633
    :goto_0
    return v0

    .line 1951634
    :cond_0
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    if-eqz v0, :cond_1

    .line 1951635
    iget-object v0, p0, LX/CxH;->a:LX/CzB;

    check-cast p1, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    invoke-virtual {v0, p1}, LX/CzB;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;)I

    move-result v0

    goto :goto_0

    .line 1951636
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method
