.class public final LX/DXI;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DXJ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

.field public final synthetic b:LX/DXJ;


# direct methods
.method public constructor <init>(LX/DXJ;)V
    .locals 1

    .prologue
    .line 2009998
    iput-object p1, p0, LX/DXI;->b:LX/DXJ;

    .line 2009999
    move-object v0, p1

    .line 2010000
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2010001
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2010002
    const-string v0, "GroupShareLinkComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2010003
    if-ne p0, p1, :cond_1

    .line 2010004
    :cond_0
    :goto_0
    return v0

    .line 2010005
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2010006
    goto :goto_0

    .line 2010007
    :cond_3
    check-cast p1, LX/DXI;

    .line 2010008
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2010009
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2010010
    if-eq v2, v3, :cond_0

    .line 2010011
    iget-object v2, p0, LX/DXI;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/DXI;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    iget-object v3, p1, LX/DXI;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2010012
    goto :goto_0

    .line 2010013
    :cond_4
    iget-object v2, p1, LX/DXI;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
