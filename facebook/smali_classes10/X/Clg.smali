.class public final synthetic LX/Clg;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I

.field public static final synthetic d:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1932725
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->values()[Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Clg;->d:[I

    :try_start_0
    sget-object v0, LX/Clg;->d:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->INLINE:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1a

    :goto_0
    :try_start_1
    sget-object v0, LX/Clg;->d:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->BLOCK:Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_19

    .line 1932726
    :goto_1
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;->values()[Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Clg;->c:[I

    :try_start_2
    sget-object v0, LX/Clg;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;->LEFT:Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_18

    :goto_2
    :try_start_3
    sget-object v0, LX/Clg;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;->CENTER:Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_17

    :goto_3
    :try_start_4
    sget-object v0, LX/Clg;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;->RIGHT:Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_16

    .line 1932727
    :goto_4
    invoke-static {}, LX/ClT;->values()[LX/ClT;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Clg;->b:[I

    :try_start_5
    sget-object v0, LX/Clg;->b:[I

    sget-object v1, LX/ClT;->TITLE:LX/ClT;

    invoke-virtual {v1}, LX/ClT;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_15

    :goto_5
    :try_start_6
    sget-object v0, LX/Clg;->b:[I

    sget-object v1, LX/ClT;->SUBTITLE:LX/ClT;

    invoke-virtual {v1}, LX/ClT;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_14

    :goto_6
    :try_start_7
    sget-object v0, LX/Clg;->b:[I

    sget-object v1, LX/ClT;->COPYRIGHT:LX/ClT;

    invoke-virtual {v1}, LX/ClT;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_13

    .line 1932728
    :goto_7
    invoke-static {}, LX/Cjt;->values()[LX/Cjt;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Clg;->a:[I

    :try_start_8
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_KICKER:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_12

    :goto_8
    :try_start_9
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_TITLE:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_11

    :goto_9
    :try_start_a
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_SUBTITLE:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_10

    :goto_a
    :try_start_b
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_H1:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_f

    :goto_b
    :try_start_c
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_H2:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_e

    :goto_c
    :try_start_d
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_BLOCK_QUOTE:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :goto_d
    :try_start_e
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_PULL_QUOTE:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_c

    :goto_e
    :try_start_f
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_PULL_QUOTE_ATTRIBUTION:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_b

    :goto_f
    :try_start_10
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_END_CREDITS:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_a

    :goto_10
    :try_start_11
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_BODY:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_9

    :goto_11
    :try_start_12
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->RELATED_ARTICLE_CELL:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_8

    :goto_12
    :try_start_13
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_BYLINE:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_7

    :goto_13
    :try_start_14
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_CAPTION_CREDIT:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_6

    :goto_14
    :try_start_15
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_CAPTION_SMALL:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_5

    :goto_15
    :try_start_16
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_CAPTION_MEDIUM:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_4

    :goto_16
    :try_start_17
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_CAPTION_LARGE:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_3

    :goto_17
    :try_start_18
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->TEXT_CAPTION_XLARGE:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_2

    :goto_18
    :try_start_19
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->NONE:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_1

    :goto_19
    :try_start_1a
    sget-object v0, LX/Clg;->a:[I

    sget-object v1, LX/Cjt;->UNKNOWN:LX/Cjt;

    invoke-virtual {v1}, LX/Cjt;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_0

    :goto_1a
    return-void

    :catch_0
    goto :goto_1a

    :catch_1
    goto :goto_19

    :catch_2
    goto :goto_18

    :catch_3
    goto :goto_17

    :catch_4
    goto :goto_16

    :catch_5
    goto :goto_15

    :catch_6
    goto :goto_14

    :catch_7
    goto :goto_13

    :catch_8
    goto :goto_12

    :catch_9
    goto :goto_11

    :catch_a
    goto/16 :goto_10

    :catch_b
    goto/16 :goto_f

    :catch_c
    goto/16 :goto_e

    :catch_d
    goto/16 :goto_d

    :catch_e
    goto/16 :goto_c

    :catch_f
    goto/16 :goto_b

    :catch_10
    goto/16 :goto_a

    :catch_11
    goto/16 :goto_9

    :catch_12
    goto/16 :goto_8

    :catch_13
    goto/16 :goto_7

    :catch_14
    goto/16 :goto_6

    :catch_15
    goto/16 :goto_5

    :catch_16
    goto/16 :goto_4

    :catch_17
    goto/16 :goto_3

    :catch_18
    goto/16 :goto_2

    :catch_19
    goto/16 :goto_1

    :catch_1a
    goto/16 :goto_0
.end method
