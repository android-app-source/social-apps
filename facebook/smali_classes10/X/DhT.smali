.class public final LX/DhT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/messaging/montage/model/art/ArtAsset;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2030979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2030981
    const-class v0, LX/DhV;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DhV;

    .line 2030982
    sget-object v1, LX/DhU;->a:[I

    invoke-virtual {v0}, LX/DhV;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2030983
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid art asset type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/DhV;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2030984
    :pswitch_0
    new-instance v0, Lcom/facebook/messaging/montage/model/art/ImageAsset;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/montage/model/art/ImageAsset;-><init>(Landroid/os/Parcel;)V

    .line 2030985
    :goto_0
    return-object v0

    .line 2030986
    :pswitch_1
    new-instance v0, Lcom/facebook/messaging/montage/model/art/StickerAsset;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/montage/model/art/StickerAsset;-><init>(Landroid/os/Parcel;)V

    goto :goto_0

    .line 2030987
    :pswitch_2
    new-instance v0, Lcom/facebook/messaging/montage/model/art/TextAsset;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/montage/model/art/TextAsset;-><init>(Landroid/os/Parcel;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2030980
    new-array v0, p1, [Lcom/facebook/messaging/montage/model/art/ArtAsset;

    return-object v0
.end method
