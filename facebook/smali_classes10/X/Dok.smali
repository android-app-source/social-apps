.class public LX/Dok;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/crypto/keychain/KeyChain;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final g:Ljava/lang/Object;


# instance fields
.field public final b:Ljava/security/SecureRandom;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1IW;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/Doe;

.field private f:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042452
    const-class v0, LX/Dok;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Dok;->a:Ljava/lang/String;

    .line 2042453
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Dok;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Doe;LX/1Hr;LX/0Ot;LX/0Or;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Doe;",
            "LX/1Hr;",
            "LX/0Ot",
            "<",
            "LX/1IW;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042447
    iput-object p1, p0, LX/Dok;->e:LX/Doe;

    .line 2042448
    invoke-static {}, LX/1Hr;->a()Ljava/security/SecureRandom;

    move-result-object v0

    iput-object v0, p0, LX/Dok;->b:Ljava/security/SecureRandom;

    .line 2042449
    iput-object p3, p0, LX/Dok;->c:LX/0Ot;

    .line 2042450
    iput-object p4, p0, LX/Dok;->d:LX/0Or;

    .line 2042451
    return-void
.end method

.method public static a(LX/0QB;)LX/Dok;
    .locals 10

    .prologue
    .line 2042414
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2042415
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2042416
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2042417
    if-nez v1, :cond_0

    .line 2042418
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2042419
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2042420
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2042421
    sget-object v1, LX/Dok;->g:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2042422
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2042423
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2042424
    :cond_1
    if-nez v1, :cond_4

    .line 2042425
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2042426
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2042427
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2042428
    new-instance v8, LX/Dok;

    .line 2042429
    new-instance v1, LX/Doe;

    const/16 v7, 0xdc6

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct {v1, v7}, LX/Doe;-><init>(LX/0Or;)V

    .line 2042430
    move-object v1, v1

    .line 2042431
    check-cast v1, LX/Doe;

    invoke-static {v0}, LX/1Hr;->a(LX/0QB;)LX/1Hr;

    move-result-object v7

    check-cast v7, LX/1Hr;

    const/16 v9, 0x488

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v8, v1, v7, v9, p0}, LX/Dok;-><init>(LX/Doe;LX/1Hr;LX/0Ot;LX/0Or;)V

    .line 2042432
    move-object v1, v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2042433
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2042434
    if-nez v1, :cond_2

    .line 2042435
    sget-object v0, LX/Dok;->g:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dok;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2042436
    :goto_1
    if-eqz v0, :cond_3

    .line 2042437
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042438
    :goto_3
    check-cast v0, LX/Dok;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2042439
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2042440
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2042441
    :catchall_1
    move-exception v0

    .line 2042442
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042443
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2042444
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2042445
    :cond_2
    :try_start_8
    sget-object v0, LX/Dok;->g:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dok;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private b([BLX/1Hb;)V
    .locals 3

    .prologue
    .line 2042406
    :try_start_0
    iget-object v0, p0, LX/Dok;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1IW;

    invoke-virtual {v0, p1, p2}, LX/1IW;->a([BLX/1Hb;)[B

    move-result-object v0

    .line 2042407
    iget-object v1, p0, LX/Dok;->e:LX/Doe;

    sget-object v2, LX/Doo;->d:LX/2PC;

    invoke-virtual {v1, v2, v0}, LX/Dod;->a(LX/2PC;[B)V

    .line 2042408
    iget-object v0, p0, LX/Dok;->e:LX/Doe;

    sget-object v1, LX/Doo;->c:LX/2PC;

    invoke-virtual {v0, v1}, LX/Dod;->c(LX/2PC;)V
    :try_end_0
    .catch LX/48k; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2042409
    return-void

    .line 2042410
    :catch_0
    move-exception v0

    .line 2042411
    :goto_0
    sget-object v1, LX/Dok;->a:Ljava/lang/String;

    const-string v2, "Failed to encrypt master key for local storage"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2042412
    new-instance v1, LX/48g;

    const-string v2, "Encryption failed"

    invoke-direct {v1, v2, v0}, LX/48g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2042413
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 2

    .prologue
    .line 2042401
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Dok;->f:[B

    if-eqz v0, :cond_0

    .line 2042402
    iget-object v0, p0, LX/Dok;->f:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 2042403
    const/4 v0, 0x0

    iput-object v0, p0, LX/Dok;->f:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2042404
    :cond_0
    monitor-exit p0

    return-void

    .line 2042405
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()[B
    .locals 5

    .prologue
    .line 2042379
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Dok;->f:[B

    if-eqz v0, :cond_0

    .line 2042380
    iget-object v0, p0, LX/Dok;->f:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2042381
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2042382
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Dok;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1IW;

    .line 2042383
    invoke-virtual {p0}, LX/Dok;->c()LX/1Hb;

    move-result-object v1

    .line 2042384
    iget-object v2, p0, LX/Dok;->e:LX/Doe;

    sget-object v3, LX/Doo;->d:LX/2PC;

    invoke-virtual {v2, v3}, LX/Dod;->b(LX/2PC;)[B

    move-result-object v2

    .line 2042385
    if-eqz v2, :cond_1

    .line 2042386
    invoke-virtual {p0, v2, v1}, LX/Dok;->a([BLX/1Hb;)[B

    move-result-object v0

    iput-object v0, p0, LX/Dok;->f:[B

    .line 2042387
    iget-object v0, p0, LX/Dok;->f:[B

    goto :goto_0

    .line 2042388
    :cond_1
    iget-object v2, p0, LX/Dok;->e:LX/Doe;

    sget-object v3, LX/Doo;->c:LX/2PC;

    invoke-virtual {v2, v3}, LX/Dod;->b(LX/2PC;)[B

    move-result-object v2

    .line 2042389
    if-eqz v2, :cond_4

    .line 2042390
    iput-object v2, p0, LX/Dok;->f:[B

    .line 2042391
    :cond_2
    :goto_1
    invoke-virtual {v0}, LX/1IW;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2042392
    iget-object v0, p0, LX/Dok;->f:[B

    invoke-direct {p0, v0, v1}, LX/Dok;->b([BLX/1Hb;)V

    .line 2042393
    :cond_3
    iget-object v0, p0, LX/Dok;->f:[B

    goto :goto_0

    .line 2042394
    :cond_4
    sget-object v3, LX/2Oy;->a:LX/1Hy;

    iget v3, v3, LX/1Hy;->keyLength:I

    new-array v3, v3, [B

    .line 2042395
    iget-object v4, p0, LX/Dok;->b:Ljava/security/SecureRandom;

    invoke-virtual {v4, v3}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 2042396
    move-object v3, v3

    .line 2042397
    iput-object v3, p0, LX/Dok;->f:[B

    .line 2042398
    invoke-virtual {v0}, LX/1IW;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2042399
    iget-object v3, p0, LX/Dok;->e:LX/Doe;

    sget-object v4, LX/Doo;->c:LX/2PC;

    invoke-virtual {v3, v4, v2}, LX/Dod;->a(LX/2PC;[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2042400
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a([BLX/1Hb;)[B
    .locals 3

    .prologue
    .line 2042374
    :try_start_0
    iget-object v0, p0, LX/Dok;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1IW;

    invoke-virtual {v0, p1, p2}, LX/1IW;->b([BLX/1Hb;)[B
    :try_end_0
    .catch LX/48k; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2042375
    :catch_0
    move-exception v0

    .line 2042376
    :goto_0
    sget-object v1, LX/Dok;->a:Ljava/lang/String;

    const-string v2, "Failed to decrypt master key from local storage"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2042377
    new-instance v1, LX/48g;

    const-string v2, "Decryption failed"

    invoke-direct {v1, v2, v0}, LX/48g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2042378
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public final b()[B
    .locals 2

    .prologue
    .line 2042361
    sget-object v0, LX/2Oy;->a:LX/1Hy;

    iget v0, v0, LX/1Hy;->ivLength:I

    new-array v0, v0, [B

    .line 2042362
    iget-object v1, p0, LX/Dok;->b:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 2042363
    return-object v0
.end method

.method public final c()LX/1Hb;
    .locals 3

    .prologue
    .line 2042366
    iget-object v0, p0, LX/Dok;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2042367
    if-nez v0, :cond_0

    .line 2042368
    sget-object v0, LX/Dok;->a:Ljava/lang/String;

    const-string v1, "getCipherKey called when user not logged in"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042369
    const-string v0, "0"

    .line 2042370
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UserMasterKey."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1Hb;->a(Ljava/lang/String;)LX/1Hb;

    move-result-object v0

    return-object v0

    .line 2042371
    :cond_0
    iget-object v0, p0, LX/Dok;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2042372
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2042373
    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 0

    .prologue
    .line 2042364
    invoke-direct {p0}, LX/Dok;->e()V

    .line 2042365
    return-void
.end method
