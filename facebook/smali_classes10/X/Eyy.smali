.class public final LX/Eyy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;)V
    .locals 0

    .prologue
    .line 2186722
    iput-object p1, p0, LX/Eyy;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2186723
    iget-object v0, p0, LX/Eyy;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    .line 2186724
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 2186725
    if-nez v0, :cond_0

    .line 2186726
    :goto_0
    return-void

    .line 2186727
    :cond_0
    iget-object v0, p0, LX/Eyy;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2186728
    const/4 v0, 0x2

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    .line 2186729
    iget-object v0, p0, LX/Eyy;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->v:LX/0P1;

    iget-object v2, p0, LX/Eyy;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v2, v2, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->r:LX/Ez3;

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ez2;

    iget-object v0, v0, LX/Ez2;->a:LX/BS8;

    invoke-virtual {v0, v1}, LX/BS8;->getLocationOnScreen([I)V

    .line 2186730
    aget v2, v1, v7

    .line 2186731
    iget-object v0, p0, LX/Eyy;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->v:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ez2;

    .line 2186732
    iget-object v4, v0, LX/Ez2;->a:LX/BS8;

    invoke-virtual {v4, v1}, LX/BS8;->getLocationOnScreen([I)V

    .line 2186733
    aget v4, v1, v7

    .line 2186734
    iget-object v5, p0, LX/Eyy;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b01cd

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    add-int/2addr v5, v2

    sub-int v4, v5, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v0, LX/Ez2;->b:LX/0am;

    goto :goto_1

    .line 2186735
    :cond_1
    iget-object v0, p0, LX/Eyy;->a:Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;

    iget-object v0, v0, Lcom/facebook/friending/jewel/UnifiedPublisherSproutFragment;->s:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
