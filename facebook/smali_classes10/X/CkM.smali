.class public LX/CkM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/CkM;


# instance fields
.field public final a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "Ljava/lang/String;",
            "LX/CkL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1930891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930892
    new-instance v0, LX/0aq;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    iput-object v0, p0, LX/CkM;->a:LX/0aq;

    .line 1930893
    return-void
.end method

.method public static a(LX/0QB;)LX/CkM;
    .locals 3

    .prologue
    .line 1930894
    sget-object v0, LX/CkM;->b:LX/CkM;

    if-nez v0, :cond_1

    .line 1930895
    const-class v1, LX/CkM;

    monitor-enter v1

    .line 1930896
    :try_start_0
    sget-object v0, LX/CkM;->b:LX/CkM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1930897
    if-eqz v2, :cond_0

    .line 1930898
    :try_start_1
    new-instance v0, LX/CkM;

    invoke-direct {v0}, LX/CkM;-><init>()V

    .line 1930899
    move-object v0, v0

    .line 1930900
    sput-object v0, LX/CkM;->b:LX/CkM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1930901
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1930902
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1930903
    :cond_1
    sget-object v0, LX/CkM;->b:LX/CkM;

    return-object v0

    .line 1930904
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1930905
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
