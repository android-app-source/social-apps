.class public final LX/DqX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DqY;


# direct methods
.method public constructor <init>(LX/DqY;)V
    .locals 0

    .prologue
    .line 2048416
    iput-object p1, p0, LX/DqX;->a:LX/DqY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2048417
    iget-object v0, p0, LX/DqX;->a:LX/DqY;

    iget-object v0, v0, LX/DqY;->c:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "NUX status reset complete, but server fetch failed."

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2048418
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2048419
    iget-object v0, p0, LX/DqX;->a:LX/DqY;

    iget-object v0, v0, LX/DqY;->c:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "NUX status fetched"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2048420
    return-void
.end method
