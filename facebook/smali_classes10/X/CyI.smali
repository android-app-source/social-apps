.class public final enum LX/CyI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CyI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CyI;

.field public static final enum APPS:LX/CyI;

.field public static final enum BLENDED_POSTS:LX/CyI;

.field public static final enum BLENDED_VIDEOS:LX/CyI;

.field public static final enum EVENTS:LX/CyI;

.field public static final enum GROUPS:LX/CyI;

.field public static final I18N_TABS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CyI;",
            ">;"
        }
    .end annotation
.end field

.field public static final I18N_TOP_ENTITIES_TABS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CyI;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum LATEST:LX/CyI;

.field public static final enum MARKETPLACE:LX/CyI;

.field public static final enum PAGES:LX/CyI;

.field public static final enum PEOPLE:LX/CyI;

.field public static final enum PHOTOS:LX/CyI;

.field public static final enum PLACES:LX/CyI;

.field public static final enum POSTS:LX/CyI;

.field public static final SECONDARY_PREFER_ENTITY_TABS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CyI;",
            ">;"
        }
    .end annotation
.end field

.field public static final SECONDARY_TABS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CyI;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum TOP:LX/CyI;

.field public static final enum TOP_ENTITIES:LX/CyI;

.field public static final enum TOP_REACTION:LX/CyI;

.field public static final enum VIDEOS:LX/CyI;

.field public static final enum VIDEO_CHANNELS:LX/CyI;

.field public static final VIDEO_HOME_TABS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CyI;",
            ">;"
        }
    .end annotation
.end field

.field public static final WORK_TABS:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CyI;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final displayStyle:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1952593
    new-instance v0, LX/CyI;

    const-string v1, "TOP"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v4, v2}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->TOP:LX/CyI;

    .line 1952594
    new-instance v0, LX/CyI;

    const-string v1, "TOP_REACTION"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v5, v2}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->TOP_REACTION:LX/CyI;

    .line 1952595
    new-instance v0, LX/CyI;

    const-string v1, "TOP_ENTITIES"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v6, v2}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->TOP_ENTITIES:LX/CyI;

    .line 1952596
    new-instance v0, LX/CyI;

    const-string v1, "LATEST"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LATEST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v7, v2}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->LATEST:LX/CyI;

    .line 1952597
    new-instance v0, LX/CyI;

    const-string v1, "MARKETPLACE"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v8, v2}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->MARKETPLACE:LX/CyI;

    .line 1952598
    new-instance v0, LX/CyI;

    const-string v1, "VIDEOS"

    const/4 v2, 0x5

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->VIDEOS:LX/CyI;

    .line 1952599
    new-instance v0, LX/CyI;

    const-string v1, "BLENDED_VIDEOS"

    const/4 v2, 0x6

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->BLENDED_VIDEOS:LX/CyI;

    .line 1952600
    new-instance v0, LX/CyI;

    const-string v1, "VIDEO_CHANNELS"

    const/4 v2, 0x7

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->VIDEO_CHANNELS:LX/CyI;

    .line 1952601
    new-instance v0, LX/CyI;

    const-string v1, "PEOPLE"

    const/16 v2, 0x8

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->PEOPLE:LX/CyI;

    .line 1952602
    new-instance v0, LX/CyI;

    const-string v1, "PHOTOS"

    const/16 v2, 0x9

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->PHOTOS:LX/CyI;

    .line 1952603
    new-instance v0, LX/CyI;

    const-string v1, "POSTS"

    const/16 v2, 0xa

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->POSTS:LX/CyI;

    .line 1952604
    new-instance v0, LX/CyI;

    const-string v1, "BLENDED_POSTS"

    const/16 v2, 0xb

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->BLENDED_POSTS:LX/CyI;

    .line 1952605
    new-instance v0, LX/CyI;

    const-string v1, "PAGES"

    const/16 v2, 0xc

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->PAGES:LX/CyI;

    .line 1952606
    new-instance v0, LX/CyI;

    const-string v1, "PLACES"

    const/16 v2, 0xd

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->PLACES:LX/CyI;

    .line 1952607
    new-instance v0, LX/CyI;

    const-string v1, "GROUPS"

    const/16 v2, 0xe

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->GROUPS:LX/CyI;

    .line 1952608
    new-instance v0, LX/CyI;

    const-string v1, "APPS"

    const/16 v2, 0xf

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->APPS:LX/CyI;

    .line 1952609
    new-instance v0, LX/CyI;

    const-string v1, "EVENTS"

    const/16 v2, 0x10

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v0, v1, v2, v3}, LX/CyI;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    sput-object v0, LX/CyI;->EVENTS:LX/CyI;

    .line 1952610
    const/16 v0, 0x11

    new-array v0, v0, [LX/CyI;

    sget-object v1, LX/CyI;->TOP:LX/CyI;

    aput-object v1, v0, v4

    sget-object v1, LX/CyI;->TOP_REACTION:LX/CyI;

    aput-object v1, v0, v5

    sget-object v1, LX/CyI;->TOP_ENTITIES:LX/CyI;

    aput-object v1, v0, v6

    sget-object v1, LX/CyI;->LATEST:LX/CyI;

    aput-object v1, v0, v7

    sget-object v1, LX/CyI;->MARKETPLACE:LX/CyI;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/CyI;->VIDEOS:LX/CyI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CyI;->BLENDED_VIDEOS:LX/CyI;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CyI;->VIDEO_CHANNELS:LX/CyI;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/CyI;->PEOPLE:LX/CyI;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/CyI;->PHOTOS:LX/CyI;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/CyI;->POSTS:LX/CyI;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/CyI;->BLENDED_POSTS:LX/CyI;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/CyI;->PAGES:LX/CyI;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/CyI;->PLACES:LX/CyI;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/CyI;->GROUPS:LX/CyI;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/CyI;->APPS:LX/CyI;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/CyI;->EVENTS:LX/CyI;

    aput-object v2, v0, v1

    sput-object v0, LX/CyI;->$VALUES:[LX/CyI;

    .line 1952611
    sget-object v0, LX/CyI;->BLENDED_POSTS:LX/CyI;

    sget-object v1, LX/CyI;->LATEST:LX/CyI;

    sget-object v2, LX/CyI;->PEOPLE:LX/CyI;

    sget-object v3, LX/CyI;->PHOTOS:LX/CyI;

    sget-object v4, LX/CyI;->VIDEOS:LX/CyI;

    sget-object v5, LX/CyI;->MARKETPLACE:LX/CyI;

    sget-object v6, LX/CyI;->PAGES:LX/CyI;

    sget-object v7, LX/CyI;->PLACES:LX/CyI;

    sget-object v8, LX/CyI;->GROUPS:LX/CyI;

    sget-object v9, LX/CyI;->APPS:LX/CyI;

    sget-object v10, LX/CyI;->EVENTS:LX/CyI;

    invoke-static/range {v0 .. v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/CyI;->SECONDARY_TABS:LX/0Px;

    .line 1952612
    sget-object v0, LX/CyI;->PEOPLE:LX/CyI;

    sget-object v1, LX/CyI;->PAGES:LX/CyI;

    sget-object v2, LX/CyI;->PHOTOS:LX/CyI;

    sget-object v3, LX/CyI;->VIDEOS:LX/CyI;

    sget-object v4, LX/CyI;->MARKETPLACE:LX/CyI;

    sget-object v5, LX/CyI;->LATEST:LX/CyI;

    sget-object v6, LX/CyI;->PLACES:LX/CyI;

    sget-object v7, LX/CyI;->GROUPS:LX/CyI;

    sget-object v8, LX/CyI;->APPS:LX/CyI;

    sget-object v9, LX/CyI;->EVENTS:LX/CyI;

    invoke-static/range {v0 .. v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/CyI;->SECONDARY_PREFER_ENTITY_TABS:LX/0Px;

    .line 1952613
    sget-object v0, LX/CyI;->BLENDED_VIDEOS:LX/CyI;

    sget-object v1, LX/CyI;->VIDEO_CHANNELS:LX/CyI;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/CyI;->VIDEO_HOME_TABS:LX/0Px;

    .line 1952614
    sget-object v0, LX/CyI;->TOP:LX/CyI;

    sget-object v1, LX/CyI;->PEOPLE:LX/CyI;

    sget-object v2, LX/CyI;->PHOTOS:LX/CyI;

    sget-object v3, LX/CyI;->GROUPS:LX/CyI;

    sget-object v4, LX/CyI;->EVENTS:LX/CyI;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/CyI;->WORK_TABS:LX/0Px;

    .line 1952615
    sget-object v0, LX/CyI;->TOP:LX/CyI;

    sget-object v1, LX/CyI;->PEOPLE:LX/CyI;

    sget-object v2, LX/CyI;->PHOTOS:LX/CyI;

    sget-object v3, LX/CyI;->PAGES:LX/CyI;

    sget-object v4, LX/CyI;->GROUPS:LX/CyI;

    sget-object v5, LX/CyI;->EVENTS:LX/CyI;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/CyI;->I18N_TABS:LX/0Px;

    .line 1952616
    sget-object v0, LX/CyI;->TOP_ENTITIES:LX/CyI;

    sget-object v1, LX/CyI;->PEOPLE:LX/CyI;

    sget-object v2, LX/CyI;->PAGES:LX/CyI;

    sget-object v3, LX/CyI;->GROUPS:LX/CyI;

    sget-object v4, LX/CyI;->EVENTS:LX/CyI;

    sget-object v5, LX/CyI;->POSTS:LX/CyI;

    sget-object v6, LX/CyI;->PHOTOS:LX/CyI;

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/CyI;->I18N_TOP_ENTITIES_TABS:LX/0Px;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1952617
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1952618
    iput-object p3, p0, LX/CyI;->displayStyle:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1952619
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CyI;
    .locals 1

    .prologue
    .line 1952620
    const-class v0, LX/CyI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CyI;

    return-object v0
.end method

.method public static values()[LX/CyI;
    .locals 1

    .prologue
    .line 1952621
    sget-object v0, LX/CyI;->$VALUES:[LX/CyI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CyI;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1952622
    iget-object v0, p0, LX/CyI;->displayStyle:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0}, LX/7CN;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
