.class public final LX/EXe;
.super LX/EWy;
.source ""

# interfaces
.implements LX/EXd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWy",
        "<",
        "LX/EXf;",
        "LX/EXe;",
        ">;",
        "LX/EXd;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Z

.field public c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2134705
    invoke-direct {p0}, LX/EWy;-><init>()V

    .line 2134706
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXe;->d:Ljava/util/List;

    .line 2134707
    invoke-direct {p0}, LX/EXe;->w()V

    .line 2134708
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2134709
    invoke-direct {p0, p1}, LX/EWy;-><init>(LX/EYd;)V

    .line 2134710
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXe;->d:Ljava/util/List;

    .line 2134711
    invoke-direct {p0}, LX/EXe;->w()V

    .line 2134712
    return-void
.end method

.method private A()LX/EXf;
    .locals 2

    .prologue
    .line 2134700
    invoke-virtual {p0}, LX/EXe;->l()LX/EXf;

    move-result-object v0

    .line 2134701
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2134702
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2134703
    :cond_0
    return-object v0
.end method

.method private D()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2134713
    iget-object v0, p0, LX/EXe;->e:LX/EZ2;

    if-nez v0, :cond_0

    .line 2134714
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EXe;->d:Ljava/util/List;

    iget v0, p0, LX/EXe;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2134715
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2134716
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EXe;->e:LX/EZ2;

    .line 2134717
    const/4 v0, 0x0

    iput-object v0, p0, LX/EXe;->d:Ljava/util/List;

    .line 2134718
    :cond_0
    iget-object v0, p0, LX/EXe;->e:LX/EZ2;

    return-object v0

    .line 2134719
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/EXe;
    .locals 1

    .prologue
    .line 2134720
    instance-of v0, p1, LX/EXf;

    if-eqz v0, :cond_0

    .line 2134721
    check-cast p1, LX/EXf;

    invoke-virtual {p0, p1}, LX/EXe;->a(LX/EXf;)LX/EXe;

    move-result-object p0

    .line 2134722
    :goto_0
    return-object p0

    .line 2134723
    :cond_0
    invoke-super {p0, p1}, LX/EWy;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EXe;
    .locals 4

    .prologue
    .line 2134724
    const/4 v2, 0x0

    .line 2134725
    :try_start_0
    sget-object v0, LX/EXf;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXf;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2134726
    if-eqz v0, :cond_0

    .line 2134727
    invoke-virtual {p0, v0}, LX/EXe;->a(LX/EXf;)LX/EXe;

    .line 2134728
    :cond_0
    return-object p0

    .line 2134729
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2134730
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2134731
    check-cast v0, LX/EXf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2134732
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2134733
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2134734
    invoke-virtual {p0, v1}, LX/EXe;->a(LX/EXf;)LX/EXe;

    :cond_1
    throw v0

    .line 2134735
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 2134736
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2134737
    invoke-direct {p0}, LX/EXe;->D()LX/EZ2;

    .line 2134738
    :cond_0
    return-void
.end method

.method public static x()LX/EXe;
    .locals 1

    .prologue
    .line 2134792
    new-instance v0, LX/EXe;

    invoke-direct {v0}, LX/EXe;-><init>()V

    return-object v0
.end method

.method private y()LX/EXe;
    .locals 2

    .prologue
    .line 2134739
    invoke-static {}, LX/EXe;->x()LX/EXe;

    move-result-object v0

    invoke-virtual {p0}, LX/EXe;->l()LX/EXf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EXe;->a(LX/EXf;)LX/EXe;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2134791
    invoke-direct {p0, p1}, LX/EXe;->d(LX/EWY;)LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2134790
    invoke-direct {p0, p1, p2}, LX/EXe;->d(LX/EWd;LX/EYZ;)LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EXf;)LX/EXe;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2134754
    sget-object v1, LX/EXf;->c:LX/EXf;

    move-object v1, v1

    .line 2134755
    if-ne p1, v1, :cond_0

    .line 2134756
    :goto_0
    return-object p0

    .line 2134757
    :cond_0
    const/4 v1, 0x1

    .line 2134758
    iget v2, p1, LX/EXf;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_9

    :goto_1
    move v1, v1

    .line 2134759
    if-eqz v1, :cond_1

    .line 2134760
    iget-boolean v1, p1, LX/EXf;->messageSetWireFormat_:Z

    move v1, v1

    .line 2134761
    iget v2, p0, LX/EXe;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/EXe;->a:I

    .line 2134762
    iput-boolean v1, p0, LX/EXe;->b:Z

    .line 2134763
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134764
    :cond_1
    iget v1, p1, LX/EXf;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_a

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2134765
    if-eqz v1, :cond_2

    .line 2134766
    iget-boolean v1, p1, LX/EXf;->noStandardDescriptorAccessor_:Z

    move v1, v1

    .line 2134767
    iget v2, p0, LX/EXe;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, LX/EXe;->a:I

    .line 2134768
    iput-boolean v1, p0, LX/EXe;->c:Z

    .line 2134769
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134770
    :cond_2
    iget-object v1, p0, LX/EXe;->e:LX/EZ2;

    if-nez v1, :cond_6

    .line 2134771
    iget-object v0, p1, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2134772
    iget-object v0, p0, LX/EXe;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2134773
    iget-object v0, p1, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    iput-object v0, p0, LX/EXe;->d:Ljava/util/List;

    .line 2134774
    iget v0, p0, LX/EXe;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, LX/EXe;->a:I

    .line 2134775
    :goto_3
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2134776
    :cond_3
    :goto_4
    invoke-virtual {p0, p1}, LX/EWy;->a(LX/EX1;)V

    .line 2134777
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    .line 2134778
    :cond_4
    iget v0, p0, LX/EXe;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_5

    .line 2134779
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EXe;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EXe;->d:Ljava/util/List;

    .line 2134780
    iget v0, p0, LX/EXe;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EXe;->a:I

    .line 2134781
    :cond_5
    iget-object v0, p0, LX/EXe;->d:Ljava/util/List;

    iget-object v1, p1, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 2134782
    :cond_6
    iget-object v1, p1, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2134783
    iget-object v1, p0, LX/EXe;->e:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2134784
    iget-object v1, p0, LX/EXe;->e:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2134785
    iput-object v0, p0, LX/EXe;->e:LX/EZ2;

    .line 2134786
    iget-object v1, p1, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    iput-object v1, p0, LX/EXe;->d:Ljava/util/List;

    .line 2134787
    iget v1, p0, LX/EXe;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, LX/EXe;->a:I

    .line 2134788
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_7

    invoke-direct {p0}, LX/EXe;->D()LX/EZ2;

    move-result-object v0

    :cond_7
    iput-object v0, p0, LX/EXe;->e:LX/EZ2;

    goto :goto_4

    .line 2134789
    :cond_8
    iget-object v0, p0, LX/EXe;->e:LX/EZ2;

    iget-object v1, p1, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_4

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_2
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2134741
    move v0, v1

    .line 2134742
    :goto_0
    iget-object v2, p0, LX/EXe;->e:LX/EZ2;

    if-nez v2, :cond_3

    .line 2134743
    iget-object v2, p0, LX/EXe;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2134744
    :goto_1
    move v2, v2

    .line 2134745
    if-ge v0, v2, :cond_2

    .line 2134746
    iget-object v2, p0, LX/EXe;->e:LX/EZ2;

    if-nez v2, :cond_4

    .line 2134747
    iget-object v2, p0, LX/EXe;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EYB;

    .line 2134748
    :goto_2
    move-object v2, v2

    .line 2134749
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2134750
    :cond_0
    :goto_3
    return v1

    .line 2134751
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2134752
    :cond_2
    invoke-virtual {p0}, LX/EWy;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2134753
    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    iget-object v2, p0, LX/EXe;->e:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_1

    :cond_4
    iget-object v2, p0, LX/EXe;->e:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EYB;

    goto :goto_2
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2134740
    invoke-direct {p0, p1, p2}, LX/EXe;->d(LX/EWd;LX/EYZ;)LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2134704
    invoke-direct {p0}, LX/EXe;->y()LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2134665
    invoke-direct {p0, p1, p2}, LX/EXe;->d(LX/EWd;LX/EYZ;)LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2134699
    invoke-direct {p0}, LX/EXe;->y()LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2134666
    invoke-direct {p0, p1}, LX/EXe;->d(LX/EWY;)LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2134667
    invoke-direct {p0}, LX/EXe;->y()LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2134668
    sget-object v0, LX/EYC;->v:LX/EYn;

    const-class v1, LX/EXf;

    const-class v2, LX/EXe;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2134669
    sget-object v0, LX/EYC;->u:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2134670
    invoke-direct {p0}, LX/EXe;->y()LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2134671
    invoke-virtual {p0}, LX/EXe;->l()LX/EXf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2134672
    invoke-direct {p0}, LX/EXe;->A()LX/EXf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2134673
    invoke-virtual {p0}, LX/EXe;->l()LX/EXf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2134674
    invoke-direct {p0}, LX/EXe;->A()LX/EXf;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EXf;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2134675
    new-instance v2, LX/EXf;

    invoke-direct {v2, p0}, LX/EXf;-><init>(LX/EWy;)V

    .line 2134676
    iget v3, p0, LX/EXe;->a:I

    .line 2134677
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 2134678
    :goto_0
    iget-boolean v1, p0, LX/EXe;->b:Z

    .line 2134679
    iput-boolean v1, v2, LX/EXf;->messageSetWireFormat_:Z

    .line 2134680
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2134681
    or-int/lit8 v0, v0, 0x2

    .line 2134682
    :cond_0
    iget-boolean v1, p0, LX/EXe;->c:Z

    .line 2134683
    iput-boolean v1, v2, LX/EXf;->noStandardDescriptorAccessor_:Z

    .line 2134684
    iget-object v1, p0, LX/EXe;->e:LX/EZ2;

    if-nez v1, :cond_2

    .line 2134685
    iget v1, p0, LX/EXe;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    .line 2134686
    iget-object v1, p0, LX/EXe;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXe;->d:Ljava/util/List;

    .line 2134687
    iget v1, p0, LX/EXe;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, LX/EXe;->a:I

    .line 2134688
    :cond_1
    iget-object v1, p0, LX/EXe;->d:Ljava/util/List;

    .line 2134689
    iput-object v1, v2, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    .line 2134690
    :goto_1
    iput v0, v2, LX/EXf;->bitField0_:I

    .line 2134691
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2134692
    return-object v2

    .line 2134693
    :cond_2
    iget-object v1, p0, LX/EXe;->e:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2134694
    iput-object v1, v2, LX/EXf;->uninterpretedOption_:Ljava/util/List;

    .line 2134695
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic m()LX/EWy;
    .locals 1

    .prologue
    .line 2134696
    invoke-direct {p0}, LX/EXe;->y()LX/EXe;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2134697
    sget-object v0, LX/EXf;->c:LX/EXf;

    move-object v0, v0

    .line 2134698
    return-object v0
.end method
