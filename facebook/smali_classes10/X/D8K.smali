.class public LX/D8K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin$DismissVideoCallback;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/D7g;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1968299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1968300
    const/4 v0, 0x1

    iput v0, p0, LX/D8K;->f:I

    .line 1968301
    iput-object p1, p0, LX/D8K;->a:Landroid/content/Context;

    .line 1968302
    return-void
.end method

.method public static a(LX/0QB;)LX/D8K;
    .locals 4

    .prologue
    .line 1968230
    const-class v1, LX/D8K;

    monitor-enter v1

    .line 1968231
    :try_start_0
    sget-object v0, LX/D8K;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1968232
    sput-object v2, LX/D8K;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1968233
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1968234
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1968235
    new-instance p0, LX/D8K;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/D8K;-><init>(Landroid/content/Context;)V

    .line 1968236
    move-object v0, p0

    .line 1968237
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1968238
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D8K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1968239
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1968240
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/D8K;)V
    .locals 7

    .prologue
    .line 1968277
    invoke-direct {p0}, LX/D8K;->g()Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    move-result-object v1

    .line 1968278
    invoke-direct {p0}, LX/D8K;->f()Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    move-result-object v2

    .line 1968279
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 1968280
    :cond_0
    :goto_0
    return-void

    .line 1968281
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D8K;->g:Z

    .line 1968282
    iget-object v0, p0, LX/D8K;->a:Landroid/content/Context;

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1968283
    if-eqz v0, :cond_2

    .line 1968284
    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1968285
    :cond_2
    iget-object v0, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->g:LX/2pa;

    move-object v0, v0

    .line 1968286
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v3, :cond_3

    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968287
    iget-object v4, v3, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v3, v4

    .line 1968288
    if-nez v3, :cond_5

    .line 1968289
    :cond_3
    const/4 v3, 0x0

    .line 1968290
    :goto_1
    move-object v3, v3

    .line 1968291
    iget-object v4, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v4, :cond_6

    const/4 v4, 0x0

    :goto_2
    move v4, v4

    .line 1968292
    iget-object v5, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v5, :cond_7

    const/4 v5, 0x0

    :goto_3
    move v5, v5

    .line 1968293
    iget-object v6, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1968294
    iget-object v6, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object p0, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v6, p0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1968295
    :cond_4
    iget-object v6, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v6}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1968296
    invoke-virtual {v2, v0, v4, v5, v3}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->a(LX/2pa;IILX/2oi;)V

    goto :goto_0

    :cond_5
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968297
    iget-object v4, v3, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v3, v4

    .line 1968298
    invoke-virtual {v3}, LX/2pb;->f()LX/2oi;

    move-result-object v3

    goto :goto_1

    :cond_6
    iget-object v4, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    goto :goto_2

    :cond_7
    iget-object v5, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v5

    goto :goto_3
.end method

.method public static e(LX/D8K;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1968252
    invoke-direct {p0}, LX/D8K;->g()Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    move-result-object v1

    .line 1968253
    invoke-direct {p0}, LX/D8K;->f()Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    move-result-object v0

    .line 1968254
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 1968255
    :cond_0
    :goto_0
    return-void

    .line 1968256
    :cond_1
    iput-boolean v2, p0, LX/D8K;->g:Z

    .line 1968257
    iput-boolean v2, p0, LX/D8K;->h:Z

    .line 1968258
    invoke-virtual {v0}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->a()V

    .line 1968259
    invoke-virtual {v0}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->getCurrentPositionMs()I

    move-result v2

    .line 1968260
    invoke-virtual {v0}, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;->getVideoResolution()LX/2oi;

    move-result-object v0

    .line 1968261
    if-nez v0, :cond_2

    .line 1968262
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    .line 1968263
    :cond_2
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->g:LX/2pa;

    if-nez v3, :cond_4

    .line 1968264
    :goto_1
    iget-object v0, p0, LX/D8K;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/D8K;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1968265
    iget-object v0, p0, LX/D8K;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D8U;

    invoke-virtual {v1, v0}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->setupDismissPlayerButton(LX/D8U;)V

    .line 1968266
    :cond_3
    iget-object v0, p0, LX/D8K;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D8K;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1968267
    iget-object v0, p0, LX/D8K;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D7g;

    invoke-interface {v0}, LX/D7g;->j()V

    goto :goto_0

    .line 1968268
    :cond_4
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

    iget-object v4, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v5, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->g:LX/2pa;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 1968269
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->h:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1, v3}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->setupPlayerLayout(Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1968270
    invoke-static {v1}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f(Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;)V

    .line 1968271
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->i:LX/0QK;

    invoke-virtual {v1, v3}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->setupFullscreenButtonClickHandler(LX/0QK;)V

    .line 1968272
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v4, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->g:LX/2pa;

    invoke-virtual {v3, v4}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1968273
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v4, 0x0

    sget-object v5, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1968274
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v4, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v3, v0, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oi;LX/04g;)V

    .line 1968275
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v4, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1968276
    iget-object v3, v1, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->f:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v4, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v3, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_1
.end method

.method private f()Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1968251
    iget-object v0, p0, LX/D8K;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/D8K;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;

    goto :goto_0
.end method

.method private g()Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1968250
    iget-object v0, p0, LX/D8K;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/D8K;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/D7g;)V
    .locals 1
    .param p1    # LX/D7g;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1968247
    if-nez p1, :cond_0

    .line 1968248
    :goto_0
    return-void

    .line 1968249
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D8K;->e:Ljava/lang/ref/WeakReference;

    goto :goto_0
.end method

.method public final a(LX/D8U;)V
    .locals 1

    .prologue
    .line 1968245
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D8K;->d:Ljava/lang/ref/WeakReference;

    .line 1968246
    return-void
.end method

.method public final a(Lcom/facebook/video/watchandmore/WatchAndMoreFullScreenVideoPlayer;)V
    .locals 1

    .prologue
    .line 1968243
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D8K;->c:Ljava/lang/ref/WeakReference;

    .line 1968244
    return-void
.end method

.method public final a(Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;)V
    .locals 1

    .prologue
    .line 1968241
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D8K;->b:Ljava/lang/ref/WeakReference;

    .line 1968242
    return-void
.end method
