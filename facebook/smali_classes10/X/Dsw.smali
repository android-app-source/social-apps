.class public LX/Dsw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2050968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2050969
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2050952
    check-cast p1, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;

    .line 2050953
    const-string v0, "Params cannot be null!"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2050954
    iget-object v0, p1, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2050955
    const-string v1, "InviteeId can\'t be null!"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2050956
    iget-object v0, p1, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2050957
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2050958
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2050959
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "invitee_id"

    .line 2050960
    iget-object v2, p1, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2050961
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2050962
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2050963
    iget-object v1, p1, Lcom/facebook/pages/common/friendinviter/protocol/SendPageLikeInviteMethod$Params;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2050964
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/invited"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2050965
    new-instance v0, LX/14N;

    const-string v1, "SendPageLikeInviteMethod"

    const-string v2, "POST"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2050966
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2050967
    const/4 v0, 0x0

    return-object v0
.end method
