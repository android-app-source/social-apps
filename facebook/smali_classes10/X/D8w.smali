.class public final LX/D8w;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/D8z;


# direct methods
.method public constructor <init>(LX/D8z;)V
    .locals 0

    .prologue
    .line 1969530
    iput-object p1, p0, LX/D8w;->a:LX/D8z;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 14

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    const v8, 0x3f8ccccd    # 1.1f

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 1969531
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    .line 1969532
    sget-object v6, LX/D8v;->a:[I

    iget-object v7, p0, LX/D8w;->a:LX/D8z;

    iget-object v7, v7, LX/D8z;->p:LX/D8y;

    invoke-virtual {v7}, LX/D8y;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    move v6, v10

    move v7, v12

    .line 1969533
    :goto_0
    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1969534
    iget-object v1, p0, LX/D8w;->a:LX/D8z;

    iget-object v1, v1, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {v1, v12}, Landroid/view/View;->setTranslationY(F)V

    .line 1969535
    iget-object v1, p0, LX/D8w;->a:LX/D8z;

    iget-object v1, v1, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setRotation(F)V

    .line 1969536
    iget-object v1, p0, LX/D8w;->a:LX/D8z;

    iget-object v1, v1, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setScaleX(F)V

    .line 1969537
    iget-object v1, p0, LX/D8w;->a:LX/D8z;

    iget-object v1, v1, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setScaleY(F)V

    .line 1969538
    iget-object v1, p0, LX/D8w;->a:LX/D8z;

    iget-object v1, v1, LX/D8z;->e:Landroid/view/View;

    invoke-virtual {v1, v10}, Landroid/view/View;->setAlpha(F)V

    .line 1969539
    iget-object v1, p0, LX/D8w;->a:LX/D8z;

    iget-object v1, v1, LX/D8z;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->setAlpha(F)V

    .line 1969540
    return-void

    .line 1969541
    :pswitch_0
    iget-object v6, p0, LX/D8w;->a:LX/D8z;

    invoke-virtual {v6}, LX/D8z;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v8

    float-to-double v6, v6

    move-wide v8, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v6

    double-to-float v12, v6

    .line 1969542
    const-wide/high16 v6, 0x4039000000000000L    # 25.0

    move-wide v8, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v6

    double-to-float v6, v6

    move v7, v6

    move v6, v10

    .line 1969543
    goto :goto_0

    .line 1969544
    :pswitch_1
    iget-object v6, p0, LX/D8w;->a:LX/D8z;

    invoke-virtual {v6}, LX/D8z;->getHeight()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    mul-float/2addr v6, v8

    float-to-double v6, v6

    move-wide v8, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v6

    double-to-float v12, v6

    .line 1969545
    const-wide/high16 v6, -0x3fc7000000000000L    # -25.0

    move-wide v8, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v6

    double-to-float v6, v6

    move v7, v6

    move v6, v10

    .line 1969546
    goto :goto_0

    .line 1969547
    :pswitch_2
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    move-wide v6, v0

    move-wide v8, v2

    invoke-static/range {v6 .. v11}, LX/0xw;->a(DDD)D

    move-result-wide v6

    double-to-float v6, v6

    .line 1969548
    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v8

    double-to-float v10, v8

    move v7, v12

    .line 1969549
    goto :goto_0

    .line 1969550
    :pswitch_3
    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v6

    double-to-float v6, v6

    move v7, v12

    move v13, v6

    move v6, v10

    move v10, v13

    .line 1969551
    goto :goto_0

    .line 1969552
    :pswitch_4
    iget-object v6, p0, LX/D8w;->a:LX/D8z;

    invoke-virtual {v6}, LX/D8z;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v8

    float-to-double v6, v6

    move-wide v8, v2

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v6

    double-to-float v6, v6

    move v7, v12

    move v12, v6

    move v6, v10

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(LX/0wd;)V
    .locals 6

    .prologue
    .line 1969553
    iget-wide v4, p1, LX/0wd;->i:D

    move-wide v0, v4

    .line 1969554
    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 1969555
    iget-object v0, p0, LX/D8w;->a:LX/D8z;

    invoke-static {v0}, LX/D8z;->j$redex0(LX/D8z;)V

    .line 1969556
    :cond_0
    :goto_0
    return-void

    .line 1969557
    :cond_1
    iget-wide v4, p1, LX/0wd;->i:D

    move-wide v0, v4

    .line 1969558
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1969559
    iget-object v0, p0, LX/D8w;->a:LX/D8z;

    invoke-static {v0}, LX/D8z;->k(LX/D8z;)V

    goto :goto_0
.end method

.method public final c(LX/0wd;)V
    .locals 6

    .prologue
    .line 1969560
    iget-wide v4, p1, LX/0wd;->i:D

    move-wide v0, v4

    .line 1969561
    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 1969562
    iget-object v0, p0, LX/D8w;->a:LX/D8z;

    invoke-static {v0}, LX/D8z;->h$redex0(LX/D8z;)V

    .line 1969563
    :cond_0
    :goto_0
    return-void

    .line 1969564
    :cond_1
    iget-wide v4, p1, LX/0wd;->i:D

    move-wide v0, v4

    .line 1969565
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1969566
    iget-object v0, p0, LX/D8w;->a:LX/D8z;

    invoke-static {v0}, LX/D8z;->i$redex0(LX/D8z;)V

    goto :goto_0
.end method
