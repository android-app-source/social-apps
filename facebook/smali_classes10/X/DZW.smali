.class public LX/DZW;
.super LX/1Cv;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;

.field public b:LX/DZH;

.field public c:Landroid/view/View$OnClickListener;

.field public d:Landroid/view/View$OnClickListener;

.field public e:LX/DZN;

.field private f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

.field public i:Ljava/lang/String;

.field public j:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

.field public k:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataInterfaces$GroupSubscriptionData$PossibleSubscriptionLevels$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataInterfaces$GroupSubscriptionData$PossiblePushSubscriptionLevels$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/DQn;

.field public o:Ljava/lang/String;

.field private final p:LX/DQQ;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/DZH;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2013436
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2013437
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2013438
    iput-object v0, p0, LX/DZW;->f:LX/0Px;

    .line 2013439
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    iput-object v0, p0, LX/DZW;->h:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 2013440
    new-instance v0, LX/DZM;

    invoke-direct {v0, p0}, LX/DZM;-><init>(LX/DZW;)V

    iput-object v0, p0, LX/DZW;->p:LX/DQQ;

    .line 2013441
    iput-object p1, p0, LX/DZW;->a:Landroid/content/res/Resources;

    .line 2013442
    iput-object p2, p0, LX/DZW;->b:LX/DZH;

    .line 2013443
    iget-object v0, p0, LX/DZW;->b:LX/DZH;

    iget-object v1, p0, LX/DZW;->p:LX/DQQ;

    .line 2013444
    iput-object v1, v0, LX/DZH;->h:LX/DQQ;

    .line 2013445
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2013446
    iput-object v0, p0, LX/DZW;->l:LX/0Px;

    .line 2013447
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2013448
    iput-object v0, p0, LX/DZW;->m:LX/0Px;

    .line 2013449
    new-instance v0, LX/DZO;

    invoke-direct {v0, p0}, LX/DZO;-><init>(LX/DZW;)V

    iput-object v0, p0, LX/DZW;->e:LX/DZN;

    .line 2013450
    new-instance v0, LX/DZP;

    invoke-direct {v0, p0}, LX/DZP;-><init>(LX/DZW;)V

    iput-object v0, p0, LX/DZW;->c:Landroid/view/View$OnClickListener;

    .line 2013451
    new-instance v0, LX/DZQ;

    invoke-direct {v0, p0}, LX/DZQ;-><init>(LX/DZW;)V

    iput-object v0, p0, LX/DZW;->d:Landroid/view/View$OnClickListener;

    .line 2013452
    invoke-static {p0}, LX/DZW;->c(LX/DZW;)V

    .line 2013453
    return-void
.end method

.method public static a(LX/DZW;Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;)LX/DMC;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataInterfaces$GroupSubscriptionData$PossiblePushSubscriptionLevels$Edges;",
            ")",
            "LX/DMC",
            "<",
            "LX/Daz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2013435
    new-instance v0, LX/DZK;

    sget-object v1, LX/DZa;->b:LX/DML;

    invoke-direct {v0, p0, v1, p1}, LX/DZK;-><init>(LX/DZW;LX/DML;Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;)V

    return-object v0
.end method

.method public static b(LX/DZW;)V
    .locals 2

    .prologue
    .line 2013381
    sget-object v0, LX/DZL;->a:[I

    iget-object v1, p0, LX/DZW;->g:Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2013382
    :cond_0
    :goto_0
    return-void

    .line 2013383
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    iput-object v0, p0, LX/DZW;->h:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    goto :goto_0

    .line 2013384
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->HIGHLIGHTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    iput-object v0, p0, LX/DZW;->h:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 2013385
    iget-object v0, p0, LX/DZW;->i:Ljava/lang/String;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2013386
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->HIGHLIGHTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/DZW;->i:Ljava/lang/String;

    goto :goto_0

    .line 2013387
    :pswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    iput-object v0, p0, LX/DZW;->h:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 2013388
    iget-object v0, p0, LX/DZW;->i:Ljava/lang/String;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/DZW;->i:Ljava/lang/String;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->HIGHLIGHTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2013389
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/DZW;->i:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static c(LX/DZW;)V
    .locals 6

    .prologue
    .line 2013400
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2013401
    iput-object v0, p0, LX/DZW;->f:LX/0Px;

    .line 2013402
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2013403
    new-instance v1, LX/DZT;

    sget-object v2, LX/DZa;->a:LX/DML;

    invoke-direct {v1, p0, v2}, LX/DZT;-><init>(LX/DZW;LX/DML;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013404
    invoke-static {}, LX/DZW;->d()LX/DMB;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013405
    iget-object v1, p0, LX/DZW;->l:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v1, p0, LX/DZW;->l:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel$EdgesModel;

    .line 2013406
    new-instance v4, LX/DZU;

    sget-object v5, LX/DZa;->b:LX/DML;

    invoke-direct {v4, p0, v5, v1}, LX/DZU;-><init>(LX/DZW;LX/DML;Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel$EdgesModel;)V

    invoke-virtual {v0, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013407
    invoke-static {}, LX/DZW;->d()LX/DMB;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013408
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2013409
    :cond_0
    new-instance v1, LX/DZV;

    sget-object v2, LX/DZa;->a:LX/DML;

    invoke-direct {v1, p0, v2}, LX/DZV;-><init>(LX/DZW;LX/DML;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013410
    invoke-static {}, LX/DZW;->d()LX/DMB;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013411
    iget-object v1, p0, LX/DZW;->m:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    iget-object v1, p0, LX/DZW;->m:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;

    .line 2013412
    sget-object v4, LX/DZL;->b:[I

    iget-object v5, p0, LX/DZW;->h:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2013413
    :cond_1
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2013414
    :pswitch_0
    invoke-static {p0, v1}, LX/DZW;->a(LX/DZW;Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;)LX/DMC;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013415
    invoke-static {}, LX/DZW;->d()LX/DMB;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2013416
    :pswitch_1
    invoke-virtual {v1}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    if-eq v4, v5, :cond_1

    .line 2013417
    invoke-static {p0, v1}, LX/DZW;->a(LX/DZW;Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;)LX/DMC;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013418
    invoke-static {}, LX/DZW;->d()LX/DMB;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2013419
    :pswitch_2
    invoke-virtual {v1}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    if-eq v4, v5, :cond_1

    invoke-virtual {v1}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->HIGHLIGHTS:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    if-eq v4, v5, :cond_1

    .line 2013420
    invoke-static {p0, v1}, LX/DZW;->a(LX/DZW;Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel$EdgesModel;)LX/DMC;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013421
    invoke-static {}, LX/DZW;->d()LX/DMB;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2013422
    :cond_2
    new-instance v1, LX/DZJ;

    sget-object v2, LX/DZa;->c:LX/DML;

    invoke-direct {v1, p0, v2}, LX/DZJ;-><init>(LX/DZW;LX/DML;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013423
    new-instance v1, LX/DZR;

    sget-object v2, LX/DZa;->a:LX/DML;

    invoke-direct {v1, p0, v2}, LX/DZR;-><init>(LX/DZW;LX/DML;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013424
    invoke-static {}, LX/DZW;->d()LX/DMB;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013425
    iget-object v1, p0, LX/DZW;->j:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    if-eqz v1, :cond_5

    .line 2013426
    iget-object v1, p0, LX/DZW;->j:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->ON:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    if-ne v1, v2, :cond_5

    .line 2013427
    iget-object v1, p0, LX/DZW;->k:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/DZW;->k:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v1, v2, :cond_4

    .line 2013428
    :cond_3
    const v1, 0x7f082fdb

    .line 2013429
    :goto_3
    new-instance v2, LX/DZS;

    sget-object v3, LX/DZa;->d:LX/DML;

    invoke-direct {v2, p0, v3, v1}, LX/DZS;-><init>(LX/DZW;LX/DML;I)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2013430
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/DZW;->f:LX/0Px;

    .line 2013431
    const v0, -0x28b38200

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2013432
    return-void

    .line 2013433
    :cond_4
    const v1, 0x7f082fda

    goto :goto_3

    .line 2013434
    :cond_5
    const v1, 0x7f082fd9

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static d()LX/DMB;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/DMB",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2013399
    new-instance v0, LX/DaP;

    sget-object v1, LX/DZa;->e:LX/DML;

    invoke-direct {v0, v1}, LX/DaP;-><init>(LX/DML;)V

    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2013398
    sget-object v0, LX/DZa;->f:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-interface {v0, p2}, LX/DML;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2013395
    check-cast p2, LX/DMB;

    .line 2013396
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 2013397
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2013394
    iget-object v0, p0, LX/DZW;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2013393
    iget-object v0, p0, LX/DZW;->f:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2013392
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2013391
    sget-object v1, LX/DZa;->f:LX/0Px;

    iget-object v0, p0, LX/DZW;->f:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2013390
    sget-object v0, LX/DZa;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
