.class public LX/EUF;
.super LX/ESt;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1ly;


# direct methods
.method public constructor <init>(Ljava/util/List;LX/1ly;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1ly;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1ly;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125941
    invoke-direct {p0}, LX/ESt;-><init>()V

    .line 2125942
    iput-object p1, p0, LX/EUF;->a:Ljava/util/List;

    .line 2125943
    iput-object p2, p0, LX/EUF;->b:LX/1ly;

    .line 2125944
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 2125927
    invoke-static {p1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2125928
    if-nez v0, :cond_1

    .line 2125929
    :cond_0
    :goto_0
    return-void

    .line 2125930
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v1, v2, :cond_2

    .line 2125931
    iget-object v0, p0, LX/EUF;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2125932
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_0

    .line 2125933
    :cond_3
    iget-object v0, p0, LX/EUF;->b:LX/1ly;

    invoke-virtual {v0, p1}, LX/1ly;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 2125939
    iget-object v0, p0, LX/EUF;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2125940
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 2125934
    invoke-static {p1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2125935
    if-nez v0, :cond_1

    .line 2125936
    :cond_0
    :goto_0
    return-void

    .line 2125937
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_0

    .line 2125938
    iget-object v0, p0, LX/EUF;->b:LX/1ly;

    invoke-virtual {v0, p1}, LX/1ly;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method
