.class public final LX/EYB;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EY5;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EYB;


# instance fields
.field public aggregateValue_:Ljava/lang/Object;

.field public bitField0_:I

.field public doubleValue_:D

.field public identifierValue_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public name_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYA;",
            ">;"
        }
    .end annotation
.end field

.field public negativeIntValue_:J

.field public positiveIntValue_:J

.field public stringValue_:LX/EWc;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2136757
    new-instance v0, LX/EY4;

    invoke-direct {v0}, LX/EY4;-><init>()V

    sput-object v0, LX/EYB;->a:LX/EWZ;

    .line 2136758
    new-instance v0, LX/EYB;

    invoke-direct {v0}, LX/EYB;-><init>()V

    .line 2136759
    sput-object v0, LX/EYB;->c:LX/EYB;

    invoke-direct {v0}, LX/EYB;->B()V

    .line 2136760
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2136752
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2136753
    iput-byte v0, p0, LX/EYB;->memoizedIsInitialized:B

    .line 2136754
    iput v0, p0, LX/EYB;->memoizedSerializedSize:I

    .line 2136755
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2136756
    iput-object v0, p0, LX/EYB;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    .line 2136707
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2136708
    iput-byte v1, p0, LX/EYB;->memoizedIsInitialized:B

    .line 2136709
    iput v1, p0, LX/EYB;->memoizedSerializedSize:I

    .line 2136710
    invoke-direct {p0}, LX/EYB;->B()V

    .line 2136711
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v3

    move v1, v0

    .line 2136712
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 2136713
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v4

    .line 2136714
    sparse-switch v4, :sswitch_data_0

    .line 2136715
    invoke-virtual {p0, p1, v3, p2, v4}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 2136716
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 2136717
    goto :goto_0

    .line 2136718
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 2136719
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, LX/EYB;->name_:Ljava/util/List;

    .line 2136720
    or-int/lit8 v1, v1, 0x1

    .line 2136721
    :cond_1
    iget-object v4, p0, LX/EYB;->name_:Ljava/util/List;

    sget-object v5, LX/EYA;->a:LX/EWZ;

    invoke-virtual {p1, v5, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2136722
    :catch_0
    move-exception v0

    .line 2136723
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2136724
    move-object v0, v0

    .line 2136725
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2136726
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 2136727
    iget-object v1, p0, LX/EYB;->name_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EYB;->name_:Ljava/util/List;

    .line 2136728
    :cond_2
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EYB;->unknownFields:LX/EZQ;

    .line 2136729
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2136730
    :sswitch_2
    :try_start_2
    iget v4, p0, LX/EYB;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/EYB;->bitField0_:I

    .line 2136731
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v4

    iput-object v4, p0, LX/EYB;->identifierValue_:Ljava/lang/Object;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2136732
    :catch_1
    move-exception v0

    .line 2136733
    :try_start_3
    new-instance v4, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2136734
    iput-object p0, v4, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2136735
    move-object v0, v4

    .line 2136736
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2136737
    :sswitch_3
    :try_start_4
    iget v4, p0, LX/EYB;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, LX/EYB;->bitField0_:I

    .line 2136738
    invoke-virtual {p1}, LX/EWd;->d()J

    move-result-wide v4

    iput-wide v4, p0, LX/EYB;->positiveIntValue_:J

    goto :goto_0

    .line 2136739
    :sswitch_4
    iget v4, p0, LX/EYB;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, LX/EYB;->bitField0_:I

    .line 2136740
    invoke-virtual {p1}, LX/EWd;->e()J

    move-result-wide v4

    iput-wide v4, p0, LX/EYB;->negativeIntValue_:J

    goto :goto_0

    .line 2136741
    :sswitch_5
    iget v4, p0, LX/EYB;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, LX/EYB;->bitField0_:I

    .line 2136742
    invoke-virtual {p1}, LX/EWd;->b()D

    move-result-wide v4

    iput-wide v4, p0, LX/EYB;->doubleValue_:D

    goto/16 :goto_0

    .line 2136743
    :sswitch_6
    iget v4, p0, LX/EYB;->bitField0_:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, LX/EYB;->bitField0_:I

    .line 2136744
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v4

    iput-object v4, p0, LX/EYB;->stringValue_:LX/EWc;

    goto/16 :goto_0

    .line 2136745
    :sswitch_7
    iget v4, p0, LX/EYB;->bitField0_:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, LX/EYB;->bitField0_:I

    .line 2136746
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v4

    iput-object v4, p0, LX/EYB;->aggregateValue_:Ljava/lang/Object;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2136747
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 2136748
    iget-object v0, p0, LX/EYB;->name_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EYB;->name_:Ljava/util/List;

    .line 2136749
    :cond_4
    invoke-virtual {v3}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EYB;->unknownFields:LX/EZQ;

    .line 2136750
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2136751
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x31 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2136702
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2136703
    iput-byte v1, p0, LX/EYB;->memoizedIsInitialized:B

    .line 2136704
    iput v1, p0, LX/EYB;->memoizedSerializedSize:I

    .line 2136705
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EYB;->unknownFields:LX/EZQ;

    .line 2136706
    return-void
.end method

.method private A()LX/EWc;
    .locals 2

    .prologue
    .line 2136697
    iget-object v0, p0, LX/EYB;->aggregateValue_:Ljava/lang/Object;

    .line 2136698
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2136699
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2136700
    iput-object v0, p0, LX/EYB;->aggregateValue_:Ljava/lang/Object;

    .line 2136701
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private B()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2136689
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EYB;->name_:Ljava/util/List;

    .line 2136690
    const-string v0, ""

    iput-object v0, p0, LX/EYB;->identifierValue_:Ljava/lang/Object;

    .line 2136691
    iput-wide v2, p0, LX/EYB;->positiveIntValue_:J

    .line 2136692
    iput-wide v2, p0, LX/EYB;->negativeIntValue_:J

    .line 2136693
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EYB;->doubleValue_:D

    .line 2136694
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EYB;->stringValue_:LX/EWc;

    .line 2136695
    const-string v0, ""

    iput-object v0, p0, LX/EYB;->aggregateValue_:Ljava/lang/Object;

    .line 2136696
    return-void
.end method

.method private static d(LX/EYB;)LX/EY6;
    .locals 1

    .prologue
    .line 2136688
    invoke-static {}, LX/EY6;->n()LX/EY6;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EY6;->a(LX/EYB;)LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method private z()LX/EWc;
    .locals 2

    .prologue
    .line 2136683
    iget-object v0, p0, LX/EYB;->identifierValue_:Ljava/lang/Object;

    .line 2136684
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2136685
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2136686
    iput-object v0, p0, LX/EYB;->identifierValue_:Ljava/lang/Object;

    .line 2136687
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2136681
    new-instance v0, LX/EY6;

    invoke-direct {v0, p1}, LX/EY6;-><init>(LX/EYd;)V

    .line 2136682
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 2136761
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2136762
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EYB;->name_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2136763
    iget-object v0, p0, LX/EYB;->name_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v2, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2136764
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2136765
    :cond_0
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2136766
    const/4 v0, 0x3

    invoke-direct {p0}, LX/EYB;->z()LX/EWc;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2136767
    :cond_1
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 2136768
    iget-wide v0, p0, LX/EYB;->positiveIntValue_:J

    invoke-virtual {p1, v3, v0, v1}, LX/EWf;->a(IJ)V

    .line 2136769
    :cond_2
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 2136770
    const/4 v0, 0x5

    iget-wide v2, p0, LX/EYB;->negativeIntValue_:J

    .line 2136771
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/EWf;->i(II)V

    .line 2136772
    invoke-static {p1, v2, v3}, LX/EWf;->i(LX/EWf;J)V

    .line 2136773
    :cond_3
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 2136774
    const/4 v0, 0x6

    iget-wide v2, p0, LX/EYB;->doubleValue_:D

    .line 2136775
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/EWf;->i(II)V

    .line 2136776
    invoke-virtual {p1, v2, v3}, LX/EWf;->a(D)V

    .line 2136777
    :cond_4
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 2136778
    const/4 v0, 0x7

    iget-object v1, p0, LX/EYB;->stringValue_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2136779
    :cond_5
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 2136780
    invoke-direct {p0}, LX/EYB;->A()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2136781
    :cond_6
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2136782
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2136670
    iget-byte v0, p0, LX/EYB;->memoizedIsInitialized:B

    .line 2136671
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2136672
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 2136673
    :goto_1
    iget-object v3, p0, LX/EYB;->name_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move v3, v3

    .line 2136674
    if-ge v0, v3, :cond_3

    .line 2136675
    iget-object v3, p0, LX/EYB;->name_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EYA;

    move-object v3, v3

    .line 2136676
    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2136677
    iput-byte v1, p0, LX/EYB;->memoizedIsInitialized:B

    goto :goto_0

    .line 2136678
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2136679
    :cond_3
    iput-byte v2, p0, LX/EYB;->memoizedIsInitialized:B

    move v1, v2

    .line 2136680
    goto :goto_0
.end method

.method public final b()I
    .locals 8

    .prologue
    const/16 v6, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 2136644
    iget v1, p0, LX/EYB;->memoizedSerializedSize:I

    .line 2136645
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 2136646
    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 2136647
    :goto_1
    iget-object v0, p0, LX/EYB;->name_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2136648
    iget-object v0, p0, LX/EYB;->name_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2136649
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2136650
    :cond_1
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2136651
    const/4 v0, 0x3

    invoke-direct {p0}, LX/EYB;->z()LX/EWc;

    move-result-object v1

    invoke-static {v0, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2136652
    :cond_2
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_3

    .line 2136653
    iget-wide v0, p0, LX/EYB;->positiveIntValue_:J

    invoke-static {v4, v0, v1}, LX/EWf;->d(IJ)I

    move-result v0

    add-int/2addr v2, v0

    .line 2136654
    :cond_3
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_4

    .line 2136655
    const/4 v0, 0x5

    iget-wide v4, p0, LX/EYB;->negativeIntValue_:J

    .line 2136656
    invoke-static {v0}, LX/EWf;->k(I)I

    move-result v1

    invoke-static {v4, v5}, LX/EWf;->j(J)I

    move-result v3

    add-int/2addr v1, v3

    move v0, v1

    .line 2136657
    add-int/2addr v2, v0

    .line 2136658
    :cond_4
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_5

    .line 2136659
    const/4 v0, 0x6

    .line 2136660
    invoke-static {v0}, LX/EWf;->k(I)I

    move-result v1

    .line 2136661
    const/16 v3, 0x8

    move v3, v3

    .line 2136662
    add-int/2addr v1, v3

    move v0, v1

    .line 2136663
    add-int/2addr v2, v0

    .line 2136664
    :cond_5
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 2136665
    const/4 v0, 0x7

    iget-object v1, p0, LX/EYB;->stringValue_:LX/EWc;

    invoke-static {v0, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2136666
    :cond_6
    iget v0, p0, LX/EYB;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 2136667
    invoke-direct {p0}, LX/EYB;->A()LX/EWc;

    move-result-object v0

    invoke-static {v6, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2136668
    :cond_7
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2136669
    iput v0, p0, LX/EYB;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2136643
    iget-object v0, p0, LX/EYB;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2136642
    sget-object v0, LX/EYC;->H:LX/EYn;

    const-class v1, LX/EYB;

    const-class v2, LX/EY6;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2136641
    sget-object v0, LX/EYB;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2136640
    invoke-static {p0}, LX/EYB;->d(LX/EYB;)LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2136639
    invoke-static {}, LX/EY6;->n()LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2136638
    invoke-static {p0}, LX/EYB;->d(LX/EYB;)LX/EY6;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2136637
    sget-object v0, LX/EYB;->c:LX/EYB;

    return-object v0
.end method
