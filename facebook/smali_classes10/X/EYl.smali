.class public final LX/EYl;
.super LX/EYk;
.source ""


# instance fields
.field private h:Ljava/lang/reflect/Method;

.field private i:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(LX/EYP;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYP;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "LX/EWp;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "LX/EWj;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2138029
    invoke-direct {p0, p2, p3, p4}, LX/EYk;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 2138030
    iget-object v0, p0, LX/EYk;->a:Ljava/lang/Class;

    const-string v1, "valueOf"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, LX/EYM;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/EYl;->h:Ljava/lang/reflect/Method;

    .line 2138031
    iget-object v0, p0, LX/EYk;->a:Ljava/lang/Class;

    const-string v1, "getValueDescriptor"

    new-array v2, v4, [Ljava/lang/Class;

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/EYl;->i:Ljava/lang/reflect/Method;

    .line 2138032
    return-void
.end method


# virtual methods
.method public final a(LX/EWj;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2138033
    iget-object v0, p0, LX/EYl;->i:Ljava/lang/reflect/Method;

    invoke-super {p0, p1}, LX/EYk;->a(LX/EWj;)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EWp;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2138034
    iget-object v0, p0, LX/EYl;->i:Ljava/lang/reflect/Method;

    invoke-super {p0, p1}, LX/EYk;->a(LX/EWp;)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EWj;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2138035
    iget-object v0, p0, LX/EYl;->h:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-super {p0, p1, v0}, LX/EYk;->a(LX/EWj;Ljava/lang/Object;)V

    .line 2138036
    return-void
.end method
