.class public final LX/EM6;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EM7;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CxA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/entities/SearchResultsPageModuleInterfaces$SearchResultsPageModule;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/EM7;


# direct methods
.method public constructor <init>(LX/EM7;)V
    .locals 1

    .prologue
    .line 2110165
    iput-object p1, p0, LX/EM6;->c:LX/EM7;

    .line 2110166
    move-object v0, p1

    .line 2110167
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2110168
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2110183
    const-string v0, "SearchResultsRelatedPagesHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2110169
    if-ne p0, p1, :cond_1

    .line 2110170
    :cond_0
    :goto_0
    return v0

    .line 2110171
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2110172
    goto :goto_0

    .line 2110173
    :cond_3
    check-cast p1, LX/EM6;

    .line 2110174
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2110175
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2110176
    if-eq v2, v3, :cond_0

    .line 2110177
    iget-object v2, p0, LX/EM6;->a:LX/CxA;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EM6;->a:LX/CxA;

    iget-object v3, p1, LX/EM6;->a:LX/CxA;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2110178
    goto :goto_0

    .line 2110179
    :cond_5
    iget-object v2, p1, LX/EM6;->a:LX/CxA;

    if-nez v2, :cond_4

    .line 2110180
    :cond_6
    iget-object v2, p0, LX/EM6;->b:LX/CzL;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/EM6;->b:LX/CzL;

    iget-object v3, p1, LX/EM6;->b:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2110181
    goto :goto_0

    .line 2110182
    :cond_7
    iget-object v2, p1, LX/EM6;->b:LX/CzL;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
