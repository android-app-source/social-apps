.class public final enum LX/DRe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DRe;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DRe;

.field public static final enum COUNT:LX/DRe;

.field public static final enum PROGRESS:LX/DRe;

.field public static final enum UNIT:LX/DRe;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1998460
    new-instance v0, LX/DRe;

    const-string v1, "PROGRESS"

    invoke-direct {v0, v1, v2}, LX/DRe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DRe;->PROGRESS:LX/DRe;

    .line 1998461
    new-instance v0, LX/DRe;

    const-string v1, "UNIT"

    invoke-direct {v0, v1, v3}, LX/DRe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DRe;->UNIT:LX/DRe;

    .line 1998462
    new-instance v0, LX/DRe;

    const-string v1, "COUNT"

    invoke-direct {v0, v1, v4}, LX/DRe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DRe;->COUNT:LX/DRe;

    .line 1998463
    const/4 v0, 0x3

    new-array v0, v0, [LX/DRe;

    sget-object v1, LX/DRe;->PROGRESS:LX/DRe;

    aput-object v1, v0, v2

    sget-object v1, LX/DRe;->UNIT:LX/DRe;

    aput-object v1, v0, v3

    sget-object v1, LX/DRe;->COUNT:LX/DRe;

    aput-object v1, v0, v4

    sput-object v0, LX/DRe;->$VALUES:[LX/DRe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1998464
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DRe;
    .locals 1

    .prologue
    .line 1998459
    const-class v0, LX/DRe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DRe;

    return-object v0
.end method

.method public static values()[LX/DRe;
    .locals 1

    .prologue
    .line 1998458
    sget-object v0, LX/DRe;->$VALUES:[LX/DRe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DRe;

    return-object v0
.end method
