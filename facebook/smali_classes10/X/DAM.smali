.class public LX/DAM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DAI;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/DA3;

.field private final d:LX/DA6;

.field private final e:LX/DA7;

.field public final f:LX/DAA;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DAF;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DAJ;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/DAL;

.field private final j:LX/2UL;

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/DA3;LX/DA6;LX/DA7;LX/DAA;LX/0Ot;LX/0Ot;LX/DAL;LX/2UL;LX/0Or;)V
    .locals 0
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/contactlogs/annotation/IsContactLogsUploadMethodEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DAI;",
            ">;",
            "LX/DA3;",
            "LX/DA6;",
            "LX/DA7;",
            "LX/DAA;",
            "LX/0Ot",
            "<",
            "LX/DAF;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DAJ;",
            ">;",
            "LX/DAL;",
            "LX/2UL;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1971100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1971101
    iput-object p1, p0, LX/DAM;->a:LX/0Or;

    .line 1971102
    iput-object p2, p0, LX/DAM;->b:LX/0Ot;

    .line 1971103
    iput-object p3, p0, LX/DAM;->c:LX/DA3;

    .line 1971104
    iput-object p4, p0, LX/DAM;->d:LX/DA6;

    .line 1971105
    iput-object p5, p0, LX/DAM;->e:LX/DA7;

    .line 1971106
    iput-object p6, p0, LX/DAM;->f:LX/DAA;

    .line 1971107
    iput-object p9, p0, LX/DAM;->i:LX/DAL;

    .line 1971108
    iput-object p7, p0, LX/DAM;->g:LX/0Ot;

    .line 1971109
    iput-object p8, p0, LX/DAM;->h:LX/0Ot;

    .line 1971110
    iput-object p10, p0, LX/DAM;->j:LX/2UL;

    .line 1971111
    iput-object p11, p0, LX/DAM;->k:LX/0Or;

    .line 1971112
    return-void
.end method

.method public static a(LX/DA4;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DA4;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/contactlogs/data/ContactLogMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1971113
    if-nez p0, :cond_0

    .line 1971114
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1971115
    :goto_0
    return-object v0

    .line 1971116
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1971117
    :goto_1
    :try_start_0
    invoke-virtual {p0}, LX/0Rr;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1971118
    invoke-virtual {p0}, LX/0Rr;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1971119
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LX/DA4;->c()V

    throw v0

    :cond_1
    invoke-virtual {p0}, LX/DA4;->c()V

    .line 1971120
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/DAM;
    .locals 15

    .prologue
    .line 1971121
    const-class v1, LX/DAM;

    monitor-enter v1

    .line 1971122
    :try_start_0
    sget-object v0, LX/DAM;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1971123
    sput-object v2, LX/DAM;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1971124
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1971125
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1971126
    new-instance v3, LX/DAM;

    const/16 v4, 0xb83

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1a06

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 1971127
    new-instance v9, LX/DA3;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v6

    check-cast v6, Landroid/content/ContentResolver;

    const-class v7, LX/DA5;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/DA5;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v8

    check-cast v8, LX/1Ml;

    invoke-direct {v9, v6, v7, v8}, LX/DA3;-><init>(Landroid/content/ContentResolver;LX/DA5;LX/1Ml;)V

    .line 1971128
    move-object v6, v9

    .line 1971129
    check-cast v6, LX/DA3;

    .line 1971130
    new-instance v10, LX/DA6;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v7

    check-cast v7, Landroid/content/ContentResolver;

    const-class v8, LX/DA5;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/DA5;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v9

    check-cast v9, LX/1Ml;

    invoke-direct {v10, v7, v8, v9}, LX/DA6;-><init>(Landroid/content/ContentResolver;LX/DA5;LX/1Ml;)V

    .line 1971131
    move-object v7, v10

    .line 1971132
    check-cast v7, LX/DA6;

    .line 1971133
    new-instance v11, LX/DA7;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v8

    check-cast v8, Landroid/content/ContentResolver;

    const-class v9, LX/DA5;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/DA5;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v10

    check-cast v10, LX/1Ml;

    invoke-direct {v11, v8, v9, v10}, LX/DA7;-><init>(Landroid/content/ContentResolver;LX/DA5;LX/1Ml;)V

    .line 1971134
    move-object v8, v11

    .line 1971135
    check-cast v8, LX/DA7;

    .line 1971136
    new-instance v11, LX/DAA;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v9

    check-cast v9, Landroid/content/ContentResolver;

    const-class v10, LX/2UU;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/2UU;

    invoke-direct {v11, v9, v10}, LX/DAA;-><init>(Landroid/content/ContentResolver;LX/2UU;)V

    .line 1971137
    move-object v9, v11

    .line 1971138
    check-cast v9, LX/DAA;

    const/16 v10, 0x1a05

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1a07

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    .line 1971139
    new-instance v13, LX/DAL;

    .line 1971140
    new-instance v14, LX/DAE;

    invoke-static {v0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v12

    check-cast v12, LX/0lp;

    invoke-direct {v14, v12}, LX/DAE;-><init>(LX/0lp;)V

    .line 1971141
    move-object v12, v14

    .line 1971142
    check-cast v12, LX/DAE;

    invoke-direct {v13, v12}, LX/DAL;-><init>(LX/DAE;)V

    .line 1971143
    move-object v12, v13

    .line 1971144
    check-cast v12, LX/DAL;

    invoke-static {v0}, LX/2UL;->a(LX/0QB;)LX/2UL;

    move-result-object v13

    check-cast v13, LX/2UL;

    const/16 v14, 0x1476

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, LX/DAM;-><init>(LX/0Or;LX/0Ot;LX/DA3;LX/DA6;LX/DA7;LX/DAA;LX/0Ot;LX/0Ot;LX/DAL;LX/2UL;LX/0Or;)V

    .line 1971145
    move-object v0, v3

    .line 1971146
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1971147
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DAM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1971148
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1971149
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 1971150
    iget-object v0, p0, LX/DAM;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1971151
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Contact logs upload method has been disabled."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1971152
    :goto_0
    return-object v0

    .line 1971153
    :cond_0
    iget-object v0, p0, LX/DAM;->c:LX/DA3;

    invoke-virtual {v0}, LX/DA3;->b()LX/DA4;

    move-result-object v0

    iget-object v1, p0, LX/DAM;->e:LX/DA7;

    invoke-virtual {v1}, LX/DA7;->b()LX/DA4;

    move-result-object v1

    iget-object v2, p0, LX/DAM;->d:LX/DA6;

    invoke-virtual {v2}, LX/DA6;->b()LX/DA4;

    move-result-object v2

    .line 1971154
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    .line 1971155
    invoke-static {v0}, LX/DAM;->a(LX/DA4;)LX/0Px;

    move-result-object v4

    .line 1971156
    const-string v5, "call_logs"

    invoke-virtual {v3, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1971157
    invoke-static {v1}, LX/DAM;->a(LX/DA4;)LX/0Px;

    move-result-object v4

    .line 1971158
    const-string v5, "sms_logs"

    invoke-virtual {v3, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1971159
    invoke-static {v2}, LX/DAM;->a(LX/DA4;)LX/0Px;

    move-result-object v4

    .line 1971160
    const-string v5, "mms_logs"

    invoke-virtual {v3, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1971161
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    move-object v8, v3

    .line 1971162
    iget-object v1, p0, LX/DAM;->j:LX/2UL;

    const-string v0, "call_logs"

    invoke-virtual {v8, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    int-to-long v2, v0

    const-string v0, "sms_logs"

    invoke-virtual {v8, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    int-to-long v4, v0

    const-string v0, "mms_logs"

    invoke-virtual {v8, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    int-to-long v6, v0

    .line 1971163
    iget-object v0, v1, LX/2UL;->a:LX/0Zb;

    sget-object v9, LX/DA8;->UPLOAD_TYPES:LX/DA8;

    invoke-static {v9}, LX/2UL;->b(LX/DA8;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "call"

    invoke-virtual {v9, v10, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "sms"

    invoke-virtual {v9, v10, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "mms"

    invoke-virtual {v9, v10, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    invoke-interface {v0, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1971164
    iget-object v0, p0, LX/DAM;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/DAM;->i:LX/DAL;

    .line 1971165
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 1971166
    invoke-virtual {v0, v1, v8, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DAK;

    .line 1971167
    invoke-virtual {v0}, LX/DAK;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 1971168
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1971169
    const-string v1, "set_contact_logs_upload_setting"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1971170
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v0

    .line 1971171
    iget-object v0, p0, LX/DAM;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/DAM;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    const-string v3, "set_contact_logs_upload_setting_param_key"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, LX/DAH;

    .line 1971172
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 1971173
    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 1971174
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1971175
    move-object v0, v0

    .line 1971176
    :goto_0
    return-object v0

    .line 1971177
    :cond_0
    const-string v1, "upload_contact_logs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1971178
    invoke-direct {p0, p1}, LX/DAM;->b(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1971179
    :cond_1
    const-string v1, "match_top_sms_contacts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1971180
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1971181
    const-string v1, "matchTopSmsContactsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/contactlogs/protocol/MatchTopSMSContactsParams;

    .line 1971182
    iget-object v1, p0, LX/DAM;->f:LX/DAA;

    .line 1971183
    iget-object v2, v0, Lcom/facebook/contactlogs/protocol/MatchTopSMSContactsParams;->a:LX/0Px;

    move-object v0, v2

    .line 1971184
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1971185
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DAC;

    .line 1971186
    iget v6, v2, LX/DAC;->d:I

    move v2, v6

    .line 1971187
    const/4 v6, -0x1

    if-eq v2, v6, :cond_2

    .line 1971188
    invoke-static {v1, v2}, LX/DAA;->a(LX/DAA;I)Lcom/facebook/contacts/model/PhonebookContact;

    move-result-object v2

    .line 1971189
    if-eqz v2, :cond_2

    .line 1971190
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1971191
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1971192
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 1971193
    iget-object v0, p0, LX/DAM;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/DAM;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    .line 1971194
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 1971195
    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1971196
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 1971197
    goto :goto_0

    .line 1971198
    :cond_4
    const-string v1, "begin_journeys"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1971199
    iget-object v0, p0, LX/DAM;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/DAM;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    .line 1971200
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 1971201
    const-string v3, "target_ids"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1971202
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 1971203
    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 1971204
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1971205
    move-object v0, v0

    .line 1971206
    goto/16 :goto_0

    .line 1971207
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
