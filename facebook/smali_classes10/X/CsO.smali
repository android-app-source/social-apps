.class public LX/CsO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cre;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1942711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1942712
    return-void
.end method

.method public static a(LX/0QB;)LX/CsO;
    .locals 3

    .prologue
    .line 1942692
    const-class v1, LX/CsO;

    monitor-enter v1

    .line 1942693
    :try_start_0
    sget-object v0, LX/CsO;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1942694
    sput-object v2, LX/CsO;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1942695
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1942696
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1942697
    new-instance v0, LX/CsO;

    invoke-direct {v0}, LX/CsO;-><init>()V

    .line 1942698
    move-object v0, v0

    .line 1942699
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1942700
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CsO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1942701
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1942702
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 1942713
    iget-object v0, p0, LX/CsO;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/CsO;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1942707
    if-nez p1, :cond_0

    .line 1942708
    const/4 v0, 0x0

    iput-object v0, p0, LX/CsO;->a:Ljava/lang/ref/WeakReference;

    .line 1942709
    :goto_0
    return-void

    .line 1942710
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/CsO;->a:Ljava/lang/ref/WeakReference;

    goto :goto_0
.end method

.method public final a(LX/Crd;)Z
    .locals 2

    .prologue
    .line 1942703
    invoke-virtual {p0}, LX/CsO;->a()Landroid/view/View;

    move-result-object v0

    .line 1942704
    instance-of v1, v0, LX/Cre;

    if-eqz v1, :cond_0

    .line 1942705
    check-cast v0, LX/Cre;

    invoke-interface {v0, p1}, LX/Cre;->a(LX/Crd;)Z

    move-result v0

    .line 1942706
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
