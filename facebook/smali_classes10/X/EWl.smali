.class public final LX/EWl;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EWk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EWl;",
        ">;",
        "LX/EWk;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:Ljava/lang/Object;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EXL;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EXL;",
            "LX/EXD;",
            "LX/EXC;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EXL;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EXL;",
            "LX/EXD;",
            "LX/EXC;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EWr;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EWr;",
            "LX/EWl;",
            "LX/EWk;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EWv;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EWv;",
            "LX/EWu;",
            "LX/EWt;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EWq;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EWq;",
            "LX/EWo;",
            "LX/EWn;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/EXf;

.field public n:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/EXf;",
            "LX/EXe;",
            "LX/EXd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2131182
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2131183
    const-string v0, ""

    iput-object v0, p0, LX/EWl;->b:Ljava/lang/Object;

    .line 2131184
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWl;->c:Ljava/util/List;

    .line 2131185
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWl;->e:Ljava/util/List;

    .line 2131186
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWl;->g:Ljava/util/List;

    .line 2131187
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWl;->i:Ljava/util/List;

    .line 2131188
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWl;->k:Ljava/util/List;

    .line 2131189
    sget-object v0, LX/EXf;->c:LX/EXf;

    move-object v0, v0

    .line 2131190
    iput-object v0, p0, LX/EWl;->m:LX/EXf;

    .line 2131191
    invoke-direct {p0}, LX/EWl;->m()V

    .line 2131192
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2131193
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2131194
    const-string v0, ""

    iput-object v0, p0, LX/EWl;->b:Ljava/lang/Object;

    .line 2131195
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWl;->c:Ljava/util/List;

    .line 2131196
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWl;->e:Ljava/util/List;

    .line 2131197
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWl;->g:Ljava/util/List;

    .line 2131198
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWl;->i:Ljava/util/List;

    .line 2131199
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EWl;->k:Ljava/util/List;

    .line 2131200
    sget-object v0, LX/EXf;->c:LX/EXf;

    move-object v0, v0

    .line 2131201
    iput-object v0, p0, LX/EWl;->m:LX/EXf;

    .line 2131202
    invoke-direct {p0}, LX/EWl;->m()V

    .line 2131203
    return-void
.end method

.method private B()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EXL;",
            "LX/EXD;",
            "LX/EXC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131204
    iget-object v0, p0, LX/EWl;->d:LX/EZ2;

    if-nez v0, :cond_0

    .line 2131205
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EWl;->c:Ljava/util/List;

    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2131206
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2131207
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EWl;->d:LX/EZ2;

    .line 2131208
    const/4 v0, 0x0

    iput-object v0, p0, LX/EWl;->c:Ljava/util/List;

    .line 2131209
    :cond_0
    iget-object v0, p0, LX/EWl;->d:LX/EZ2;

    return-object v0

    .line 2131210
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private E()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EXL;",
            "LX/EXD;",
            "LX/EXC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131211
    iget-object v0, p0, LX/EWl;->f:LX/EZ2;

    if-nez v0, :cond_0

    .line 2131212
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EWl;->e:Ljava/util/List;

    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2131213
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2131214
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EWl;->f:LX/EZ2;

    .line 2131215
    const/4 v0, 0x0

    iput-object v0, p0, LX/EWl;->e:Ljava/util/List;

    .line 2131216
    :cond_0
    iget-object v0, p0, LX/EWl;->f:LX/EZ2;

    return-object v0

    .line 2131217
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private H()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EWr;",
            "LX/EWl;",
            "LX/EWk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131218
    iget-object v0, p0, LX/EWl;->h:LX/EZ2;

    if-nez v0, :cond_0

    .line 2131219
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EWl;->g:Ljava/util/List;

    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2131220
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2131221
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EWl;->h:LX/EZ2;

    .line 2131222
    const/4 v0, 0x0

    iput-object v0, p0, LX/EWl;->g:Ljava/util/List;

    .line 2131223
    :cond_0
    iget-object v0, p0, LX/EWl;->h:LX/EZ2;

    return-object v0

    .line 2131224
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private K()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EWv;",
            "LX/EWu;",
            "LX/EWt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131225
    iget-object v0, p0, LX/EWl;->j:LX/EZ2;

    if-nez v0, :cond_0

    .line 2131226
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EWl;->i:Ljava/util/List;

    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2131227
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2131228
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EWl;->j:LX/EZ2;

    .line 2131229
    const/4 v0, 0x0

    iput-object v0, p0, LX/EWl;->i:Ljava/util/List;

    .line 2131230
    :cond_0
    iget-object v0, p0, LX/EWl;->j:LX/EZ2;

    return-object v0

    .line 2131231
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private M()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EWq;",
            "LX/EWo;",
            "LX/EWn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131232
    iget-object v0, p0, LX/EWl;->l:LX/EZ2;

    if-nez v0, :cond_0

    .line 2131233
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EWl;->k:Ljava/util/List;

    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2131234
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2131235
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EWl;->l:LX/EZ2;

    .line 2131236
    const/4 v0, 0x0

    iput-object v0, p0, LX/EWl;->k:Ljava/util/List;

    .line 2131237
    :cond_0
    iget-object v0, p0, LX/EWl;->l:LX/EZ2;

    return-object v0

    .line 2131238
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/EWl;
    .locals 1

    .prologue
    .line 2131331
    instance-of v0, p1, LX/EWr;

    if-eqz v0, :cond_0

    .line 2131332
    check-cast p1, LX/EWr;

    invoke-virtual {p0, p1}, LX/EWl;->a(LX/EWr;)LX/EWl;

    move-result-object p0

    .line 2131333
    :goto_0
    return-object p0

    .line 2131334
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EWl;
    .locals 4

    .prologue
    .line 2131239
    const/4 v2, 0x0

    .line 2131240
    :try_start_0
    sget-object v0, LX/EWr;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWr;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2131241
    if-eqz v0, :cond_0

    .line 2131242
    invoke-virtual {p0, v0}, LX/EWl;->a(LX/EWr;)LX/EWl;

    .line 2131243
    :cond_0
    return-object p0

    .line 2131244
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2131245
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2131246
    check-cast v0, LX/EWr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2131247
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2131248
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2131249
    invoke-virtual {p0, v1}, LX/EWl;->a(LX/EWr;)LX/EWl;

    :cond_1
    throw v0

    .line 2131250
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 2131251
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2131252
    invoke-direct {p0}, LX/EWl;->B()LX/EZ2;

    .line 2131253
    invoke-direct {p0}, LX/EWl;->E()LX/EZ2;

    .line 2131254
    invoke-direct {p0}, LX/EWl;->H()LX/EZ2;

    .line 2131255
    invoke-direct {p0}, LX/EWl;->K()LX/EZ2;

    .line 2131256
    invoke-direct {p0}, LX/EWl;->M()LX/EZ2;

    .line 2131257
    iget-object v0, p0, LX/EWl;->n:LX/EZ7;

    if-nez v0, :cond_0

    .line 2131258
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/EWl;->m:LX/EXf;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2131259
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2131260
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/EWl;->n:LX/EZ7;

    .line 2131261
    const/4 v0, 0x0

    iput-object v0, p0, LX/EWl;->m:LX/EXf;

    .line 2131262
    :cond_0
    return-void
.end method

.method public static n()LX/EWl;
    .locals 1

    .prologue
    .line 2131263
    new-instance v0, LX/EWl;

    invoke-direct {v0}, LX/EWl;-><init>()V

    return-object v0
.end method

.method private u()LX/EWl;
    .locals 2

    .prologue
    .line 2131264
    invoke-static {}, LX/EWl;->n()LX/EWl;

    move-result-object v0

    invoke-direct {p0}, LX/EWl;->y()LX/EWr;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EWl;->a(LX/EWr;)LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method private x()LX/EWr;
    .locals 2

    .prologue
    .line 2131265
    invoke-direct {p0}, LX/EWl;->y()LX/EWr;

    move-result-object v0

    .line 2131266
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2131267
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2131268
    :cond_0
    return-object v0
.end method

.method private y()LX/EWr;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2131269
    new-instance v2, LX/EWr;

    invoke-direct {v2, p0}, LX/EWr;-><init>(LX/EWj;)V

    .line 2131270
    iget v3, p0, LX/EWl;->a:I

    .line 2131271
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    .line 2131272
    :goto_0
    iget-object v1, p0, LX/EWl;->b:Ljava/lang/Object;

    .line 2131273
    iput-object v1, v2, LX/EWr;->name_:Ljava/lang/Object;

    .line 2131274
    iget-object v1, p0, LX/EWl;->d:LX/EZ2;

    if-nez v1, :cond_5

    .line 2131275
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2131276
    iget-object v1, p0, LX/EWl;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EWl;->c:Ljava/util/List;

    .line 2131277
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, LX/EWl;->a:I

    .line 2131278
    :cond_0
    iget-object v1, p0, LX/EWl;->c:Ljava/util/List;

    .line 2131279
    iput-object v1, v2, LX/EWr;->field_:Ljava/util/List;

    .line 2131280
    :goto_1
    iget-object v1, p0, LX/EWl;->f:LX/EZ2;

    if-nez v1, :cond_6

    .line 2131281
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2131282
    iget-object v1, p0, LX/EWl;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EWl;->e:Ljava/util/List;

    .line 2131283
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, LX/EWl;->a:I

    .line 2131284
    :cond_1
    iget-object v1, p0, LX/EWl;->e:Ljava/util/List;

    .line 2131285
    iput-object v1, v2, LX/EWr;->extension_:Ljava/util/List;

    .line 2131286
    :goto_2
    iget-object v1, p0, LX/EWl;->h:LX/EZ2;

    if-nez v1, :cond_7

    .line 2131287
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2131288
    iget-object v1, p0, LX/EWl;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EWl;->g:Ljava/util/List;

    .line 2131289
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, LX/EWl;->a:I

    .line 2131290
    :cond_2
    iget-object v1, p0, LX/EWl;->g:Ljava/util/List;

    .line 2131291
    iput-object v1, v2, LX/EWr;->nestedType_:Ljava/util/List;

    .line 2131292
    :goto_3
    iget-object v1, p0, LX/EWl;->j:LX/EZ2;

    if-nez v1, :cond_8

    .line 2131293
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 2131294
    iget-object v1, p0, LX/EWl;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EWl;->i:Ljava/util/List;

    .line 2131295
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, LX/EWl;->a:I

    .line 2131296
    :cond_3
    iget-object v1, p0, LX/EWl;->i:Ljava/util/List;

    .line 2131297
    iput-object v1, v2, LX/EWr;->enumType_:Ljava/util/List;

    .line 2131298
    :goto_4
    iget-object v1, p0, LX/EWl;->l:LX/EZ2;

    if-nez v1, :cond_9

    .line 2131299
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 2131300
    iget-object v1, p0, LX/EWl;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EWl;->k:Ljava/util/List;

    .line 2131301
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, LX/EWl;->a:I

    .line 2131302
    :cond_4
    iget-object v1, p0, LX/EWl;->k:Ljava/util/List;

    .line 2131303
    iput-object v1, v2, LX/EWr;->extensionRange_:Ljava/util/List;

    .line 2131304
    :goto_5
    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_b

    .line 2131305
    or-int/lit8 v0, v0, 0x2

    move v1, v0

    .line 2131306
    :goto_6
    iget-object v0, p0, LX/EWl;->n:LX/EZ7;

    if-nez v0, :cond_a

    .line 2131307
    iget-object v0, p0, LX/EWl;->m:LX/EXf;

    .line 2131308
    iput-object v0, v2, LX/EWr;->options_:LX/EXf;

    .line 2131309
    :goto_7
    iput v1, v2, LX/EWr;->bitField0_:I

    .line 2131310
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2131311
    return-object v2

    .line 2131312
    :cond_5
    iget-object v1, p0, LX/EWl;->d:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2131313
    iput-object v1, v2, LX/EWr;->field_:Ljava/util/List;

    .line 2131314
    goto/16 :goto_1

    .line 2131315
    :cond_6
    iget-object v1, p0, LX/EWl;->f:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2131316
    iput-object v1, v2, LX/EWr;->extension_:Ljava/util/List;

    .line 2131317
    goto/16 :goto_2

    .line 2131318
    :cond_7
    iget-object v1, p0, LX/EWl;->h:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2131319
    iput-object v1, v2, LX/EWr;->nestedType_:Ljava/util/List;

    .line 2131320
    goto :goto_3

    .line 2131321
    :cond_8
    iget-object v1, p0, LX/EWl;->j:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2131322
    iput-object v1, v2, LX/EWr;->enumType_:Ljava/util/List;

    .line 2131323
    goto :goto_4

    .line 2131324
    :cond_9
    iget-object v1, p0, LX/EWl;->l:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2131325
    iput-object v1, v2, LX/EWr;->extensionRange_:Ljava/util/List;

    .line 2131326
    goto :goto_5

    .line 2131327
    :cond_a
    iget-object v0, p0, LX/EWl;->n:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EXf;

    .line 2131328
    iput-object v0, v2, LX/EWr;->options_:LX/EXf;

    .line 2131329
    goto :goto_7

    :cond_b
    move v1, v0

    goto :goto_6

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2131181
    invoke-direct {p0, p1}, LX/EWl;->d(LX/EWY;)LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2131330
    invoke-direct {p0, p1, p2}, LX/EWl;->d(LX/EWd;LX/EYZ;)LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EWr;)LX/EWl;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2131010
    sget-object v0, LX/EWr;->c:LX/EWr;

    move-object v0, v0

    .line 2131011
    if-ne p1, v0, :cond_0

    .line 2131012
    :goto_0
    return-object p0

    .line 2131013
    :cond_0
    const/4 v0, 0x1

    .line 2131014
    iget v2, p1, LX/EWr;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_21

    :goto_1
    move v0, v0

    .line 2131015
    if-eqz v0, :cond_1

    .line 2131016
    iget v0, p0, LX/EWl;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EWl;->a:I

    .line 2131017
    iget-object v0, p1, LX/EWr;->name_:Ljava/lang/Object;

    iput-object v0, p0, LX/EWl;->b:Ljava/lang/Object;

    .line 2131018
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2131019
    :cond_1
    iget-object v0, p0, LX/EWl;->d:LX/EZ2;

    if-nez v0, :cond_a

    .line 2131020
    iget-object v0, p1, LX/EWr;->field_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2131021
    iget-object v0, p0, LX/EWl;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2131022
    iget-object v0, p1, LX/EWr;->field_:Ljava/util/List;

    iput-object v0, p0, LX/EWl;->c:Ljava/util/List;

    .line 2131023
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, LX/EWl;->a:I

    .line 2131024
    :goto_2
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2131025
    :cond_2
    :goto_3
    iget-object v0, p0, LX/EWl;->f:LX/EZ2;

    if-nez v0, :cond_f

    .line 2131026
    iget-object v0, p1, LX/EWr;->extension_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2131027
    iget-object v0, p0, LX/EWl;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2131028
    iget-object v0, p1, LX/EWr;->extension_:Ljava/util/List;

    iput-object v0, p0, LX/EWl;->e:Ljava/util/List;

    .line 2131029
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, LX/EWl;->a:I

    .line 2131030
    :goto_4
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2131031
    :cond_3
    :goto_5
    iget-object v0, p0, LX/EWl;->h:LX/EZ2;

    if-nez v0, :cond_14

    .line 2131032
    iget-object v0, p1, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2131033
    iget-object v0, p0, LX/EWl;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2131034
    iget-object v0, p1, LX/EWr;->nestedType_:Ljava/util/List;

    iput-object v0, p0, LX/EWl;->g:Ljava/util/List;

    .line 2131035
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, LX/EWl;->a:I

    .line 2131036
    :goto_6
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2131037
    :cond_4
    :goto_7
    iget-object v0, p0, LX/EWl;->j:LX/EZ2;

    if-nez v0, :cond_19

    .line 2131038
    iget-object v0, p1, LX/EWr;->enumType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2131039
    iget-object v0, p0, LX/EWl;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2131040
    iget-object v0, p1, LX/EWr;->enumType_:Ljava/util/List;

    iput-object v0, p0, LX/EWl;->i:Ljava/util/List;

    .line 2131041
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, LX/EWl;->a:I

    .line 2131042
    :goto_8
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2131043
    :cond_5
    :goto_9
    iget-object v0, p0, LX/EWl;->l:LX/EZ2;

    if-nez v0, :cond_1e

    .line 2131044
    iget-object v0, p1, LX/EWr;->extensionRange_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2131045
    iget-object v0, p0, LX/EWl;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 2131046
    iget-object v0, p1, LX/EWr;->extensionRange_:Ljava/util/List;

    iput-object v0, p0, LX/EWl;->k:Ljava/util/List;

    .line 2131047
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, LX/EWl;->a:I

    .line 2131048
    :goto_a
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2131049
    :cond_6
    :goto_b
    invoke-virtual {p1}, LX/EWr;->q()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2131050
    iget-object v0, p1, LX/EWr;->options_:LX/EXf;

    move-object v0, v0

    .line 2131051
    iget-object v1, p0, LX/EWl;->n:LX/EZ7;

    if-nez v1, :cond_23

    .line 2131052
    iget v1, p0, LX/EWl;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_22

    iget-object v1, p0, LX/EWl;->m:LX/EXf;

    .line 2131053
    sget-object v2, LX/EXf;->c:LX/EXf;

    move-object v2, v2

    .line 2131054
    if-eq v1, v2, :cond_22

    .line 2131055
    iget-object v1, p0, LX/EWl;->m:LX/EXf;

    invoke-static {v1}, LX/EXf;->a(LX/EXf;)LX/EXe;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/EXe;->a(LX/EXf;)LX/EXe;

    move-result-object v1

    invoke-virtual {v1}, LX/EXe;->l()LX/EXf;

    move-result-object v1

    iput-object v1, p0, LX/EWl;->m:LX/EXf;

    .line 2131056
    :goto_c
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2131057
    :goto_d
    iget v1, p0, LX/EWl;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, LX/EWl;->a:I

    .line 2131058
    :cond_7
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto/16 :goto_0

    .line 2131059
    :cond_8
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_9

    .line 2131060
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, LX/EWl;->c:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EWl;->c:Ljava/util/List;

    .line 2131061
    iget v0, p0, LX/EWl;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EWl;->a:I

    .line 2131062
    :cond_9
    iget-object v0, p0, LX/EWl;->c:Ljava/util/List;

    iget-object v2, p1, LX/EWr;->field_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 2131063
    :cond_a
    iget-object v0, p1, LX/EWr;->field_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2131064
    iget-object v0, p0, LX/EWl;->d:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2131065
    iget-object v0, p0, LX/EWl;->d:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->b()V

    .line 2131066
    iput-object v1, p0, LX/EWl;->d:LX/EZ2;

    .line 2131067
    iget-object v0, p1, LX/EWr;->field_:Ljava/util/List;

    iput-object v0, p0, LX/EWl;->c:Ljava/util/List;

    .line 2131068
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, LX/EWl;->a:I

    .line 2131069
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_b

    invoke-direct {p0}, LX/EWl;->B()LX/EZ2;

    move-result-object v0

    :goto_e
    iput-object v0, p0, LX/EWl;->d:LX/EZ2;

    goto/16 :goto_3

    :cond_b
    move-object v0, v1

    goto :goto_e

    .line 2131070
    :cond_c
    iget-object v0, p0, LX/EWl;->d:LX/EZ2;

    iget-object v2, p1, LX/EWr;->field_:Ljava/util/List;

    invoke-virtual {v0, v2}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto/16 :goto_3

    .line 2131071
    :cond_d
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-eq v0, v2, :cond_e

    .line 2131072
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, LX/EWl;->e:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EWl;->e:Ljava/util/List;

    .line 2131073
    iget v0, p0, LX/EWl;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EWl;->a:I

    .line 2131074
    :cond_e
    iget-object v0, p0, LX/EWl;->e:Ljava/util/List;

    iget-object v2, p1, LX/EWr;->extension_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    .line 2131075
    :cond_f
    iget-object v0, p1, LX/EWr;->extension_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2131076
    iget-object v0, p0, LX/EWl;->f:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->d()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2131077
    iget-object v0, p0, LX/EWl;->f:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->b()V

    .line 2131078
    iput-object v1, p0, LX/EWl;->f:LX/EZ2;

    .line 2131079
    iget-object v0, p1, LX/EWr;->extension_:Ljava/util/List;

    iput-object v0, p0, LX/EWl;->e:Ljava/util/List;

    .line 2131080
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, LX/EWl;->a:I

    .line 2131081
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_10

    invoke-direct {p0}, LX/EWl;->E()LX/EZ2;

    move-result-object v0

    :goto_f
    iput-object v0, p0, LX/EWl;->f:LX/EZ2;

    goto/16 :goto_5

    :cond_10
    move-object v0, v1

    goto :goto_f

    .line 2131082
    :cond_11
    iget-object v0, p0, LX/EWl;->f:LX/EZ2;

    iget-object v2, p1, LX/EWr;->extension_:Ljava/util/List;

    invoke-virtual {v0, v2}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto/16 :goto_5

    .line 2131083
    :cond_12
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-eq v0, v2, :cond_13

    .line 2131084
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, LX/EWl;->g:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EWl;->g:Ljava/util/List;

    .line 2131085
    iget v0, p0, LX/EWl;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EWl;->a:I

    .line 2131086
    :cond_13
    iget-object v0, p0, LX/EWl;->g:Ljava/util/List;

    iget-object v2, p1, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    .line 2131087
    :cond_14
    iget-object v0, p1, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2131088
    iget-object v0, p0, LX/EWl;->h:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->d()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2131089
    iget-object v0, p0, LX/EWl;->h:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->b()V

    .line 2131090
    iput-object v1, p0, LX/EWl;->h:LX/EZ2;

    .line 2131091
    iget-object v0, p1, LX/EWr;->nestedType_:Ljava/util/List;

    iput-object v0, p0, LX/EWl;->g:Ljava/util/List;

    .line 2131092
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, LX/EWl;->a:I

    .line 2131093
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_15

    invoke-direct {p0}, LX/EWl;->H()LX/EZ2;

    move-result-object v0

    :goto_10
    iput-object v0, p0, LX/EWl;->h:LX/EZ2;

    goto/16 :goto_7

    :cond_15
    move-object v0, v1

    goto :goto_10

    .line 2131094
    :cond_16
    iget-object v0, p0, LX/EWl;->h:LX/EZ2;

    iget-object v2, p1, LX/EWr;->nestedType_:Ljava/util/List;

    invoke-virtual {v0, v2}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto/16 :goto_7

    .line 2131095
    :cond_17
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-eq v0, v2, :cond_18

    .line 2131096
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, LX/EWl;->i:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EWl;->i:Ljava/util/List;

    .line 2131097
    iget v0, p0, LX/EWl;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/EWl;->a:I

    .line 2131098
    :cond_18
    iget-object v0, p0, LX/EWl;->i:Ljava/util/List;

    iget-object v2, p1, LX/EWr;->enumType_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    .line 2131099
    :cond_19
    iget-object v0, p1, LX/EWr;->enumType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2131100
    iget-object v0, p0, LX/EWl;->j:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->d()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 2131101
    iget-object v0, p0, LX/EWl;->j:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->b()V

    .line 2131102
    iput-object v1, p0, LX/EWl;->j:LX/EZ2;

    .line 2131103
    iget-object v0, p1, LX/EWr;->enumType_:Ljava/util/List;

    iput-object v0, p0, LX/EWl;->i:Ljava/util/List;

    .line 2131104
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, LX/EWl;->a:I

    .line 2131105
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_1a

    invoke-direct {p0}, LX/EWl;->K()LX/EZ2;

    move-result-object v0

    :goto_11
    iput-object v0, p0, LX/EWl;->j:LX/EZ2;

    goto/16 :goto_9

    :cond_1a
    move-object v0, v1

    goto :goto_11

    .line 2131106
    :cond_1b
    iget-object v0, p0, LX/EWl;->j:LX/EZ2;

    iget-object v2, p1, LX/EWr;->enumType_:Ljava/util/List;

    invoke-virtual {v0, v2}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto/16 :goto_9

    .line 2131107
    :cond_1c
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_1d

    .line 2131108
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EWl;->k:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EWl;->k:Ljava/util/List;

    .line 2131109
    iget v0, p0, LX/EWl;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, LX/EWl;->a:I

    .line 2131110
    :cond_1d
    iget-object v0, p0, LX/EWl;->k:Ljava/util/List;

    iget-object v1, p1, LX/EWr;->extensionRange_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a

    .line 2131111
    :cond_1e
    iget-object v0, p1, LX/EWr;->extensionRange_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2131112
    iget-object v0, p0, LX/EWl;->l:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->d()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 2131113
    iget-object v0, p0, LX/EWl;->l:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->b()V

    .line 2131114
    iput-object v1, p0, LX/EWl;->l:LX/EZ2;

    .line 2131115
    iget-object v0, p1, LX/EWr;->extensionRange_:Ljava/util/List;

    iput-object v0, p0, LX/EWl;->k:Ljava/util/List;

    .line 2131116
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, LX/EWl;->a:I

    .line 2131117
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_1f

    invoke-direct {p0}, LX/EWl;->M()LX/EZ2;

    move-result-object v1

    :cond_1f
    iput-object v1, p0, LX/EWl;->l:LX/EZ2;

    goto/16 :goto_b

    .line 2131118
    :cond_20
    iget-object v0, p0, LX/EWl;->l:LX/EZ2;

    iget-object v1, p1, LX/EWr;->extensionRange_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto/16 :goto_b

    :cond_21
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2131119
    :cond_22
    iput-object v0, p0, LX/EWl;->m:LX/EXf;

    goto/16 :goto_c

    .line 2131120
    :cond_23
    iget-object v1, p0, LX/EWl;->n:LX/EZ7;

    invoke-virtual {v1, v0}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto/16 :goto_d
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2131121
    move v0, v1

    .line 2131122
    :goto_0
    iget-object v2, p0, LX/EWl;->d:LX/EZ2;

    if-nez v2, :cond_7

    .line 2131123
    iget-object v2, p0, LX/EWl;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2131124
    :goto_1
    move v2, v2

    .line 2131125
    if-ge v0, v2, :cond_2

    .line 2131126
    iget-object v2, p0, LX/EWl;->d:LX/EZ2;

    if-nez v2, :cond_8

    .line 2131127
    iget-object v2, p0, LX/EWl;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EXL;

    .line 2131128
    :goto_2
    move-object v2, v2

    .line 2131129
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2131130
    :cond_0
    :goto_3
    return v1

    .line 2131131
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2131132
    :goto_4
    iget-object v2, p0, LX/EWl;->f:LX/EZ2;

    if-nez v2, :cond_9

    .line 2131133
    iget-object v2, p0, LX/EWl;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2131134
    :goto_5
    move v2, v2

    .line 2131135
    if-ge v0, v2, :cond_3

    .line 2131136
    iget-object v2, p0, LX/EWl;->f:LX/EZ2;

    if-nez v2, :cond_a

    .line 2131137
    iget-object v2, p0, LX/EWl;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EXL;

    .line 2131138
    :goto_6
    move-object v2, v2

    .line 2131139
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2131140
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_3
    move v0, v1

    .line 2131141
    :goto_7
    iget-object v2, p0, LX/EWl;->h:LX/EZ2;

    if-nez v2, :cond_b

    .line 2131142
    iget-object v2, p0, LX/EWl;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2131143
    :goto_8
    move v2, v2

    .line 2131144
    if-ge v0, v2, :cond_4

    .line 2131145
    iget-object v2, p0, LX/EWl;->h:LX/EZ2;

    if-nez v2, :cond_c

    .line 2131146
    iget-object v2, p0, LX/EWl;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EWr;

    .line 2131147
    :goto_9
    move-object v2, v2

    .line 2131148
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2131149
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_4
    move v0, v1

    .line 2131150
    :goto_a
    iget-object v2, p0, LX/EWl;->j:LX/EZ2;

    if-nez v2, :cond_d

    .line 2131151
    iget-object v2, p0, LX/EWl;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2131152
    :goto_b
    move v2, v2

    .line 2131153
    if-ge v0, v2, :cond_5

    .line 2131154
    iget-object v2, p0, LX/EWl;->j:LX/EZ2;

    if-nez v2, :cond_e

    .line 2131155
    iget-object v2, p0, LX/EWl;->i:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EWv;

    .line 2131156
    :goto_c
    move-object v2, v2

    .line 2131157
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2131158
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 2131159
    :cond_5
    iget v0, p0, LX/EWl;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_f

    const/4 v0, 0x1

    :goto_d
    move v0, v0

    .line 2131160
    if-eqz v0, :cond_6

    .line 2131161
    iget-object v0, p0, LX/EWl;->n:LX/EZ7;

    if-nez v0, :cond_10

    .line 2131162
    iget-object v0, p0, LX/EWl;->m:LX/EXf;

    .line 2131163
    :goto_e
    move-object v0, v0

    .line 2131164
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2131165
    :cond_6
    const/4 v1, 0x1

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, LX/EWl;->d:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto/16 :goto_1

    :cond_8
    iget-object v2, p0, LX/EWl;->d:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EXL;

    goto/16 :goto_2

    :cond_9
    iget-object v2, p0, LX/EWl;->f:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto/16 :goto_5

    :cond_a
    iget-object v2, p0, LX/EWl;->f:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EXL;

    goto/16 :goto_6

    :cond_b
    iget-object v2, p0, LX/EWl;->h:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, LX/EWl;->h:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EWr;

    goto :goto_9

    :cond_d
    iget-object v2, p0, LX/EWl;->j:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_b

    :cond_e
    iget-object v2, p0, LX/EWl;->j:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EWv;

    goto :goto_c

    :cond_f
    const/4 v0, 0x0

    goto :goto_d

    :cond_10
    iget-object v0, p0, LX/EWl;->n:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->c()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EXf;

    goto :goto_e
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2131166
    invoke-direct {p0, p1, p2}, LX/EWl;->d(LX/EWd;LX/EYZ;)LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2131167
    invoke-direct {p0}, LX/EWl;->u()LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2131168
    invoke-direct {p0, p1, p2}, LX/EWl;->d(LX/EWd;LX/EYZ;)LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2131169
    invoke-direct {p0}, LX/EWl;->u()LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2131170
    invoke-direct {p0, p1}, LX/EWl;->d(LX/EWY;)LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2131171
    invoke-direct {p0}, LX/EWl;->u()LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2131172
    sget-object v0, LX/EYC;->f:LX/EYn;

    const-class v1, LX/EWr;

    const-class v2, LX/EWl;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2131173
    sget-object v0, LX/EYC;->e:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2131174
    invoke-direct {p0}, LX/EWl;->u()LX/EWl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2131175
    invoke-direct {p0}, LX/EWl;->y()LX/EWr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2131176
    invoke-direct {p0}, LX/EWl;->x()LX/EWr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2131177
    invoke-direct {p0}, LX/EWl;->y()LX/EWr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2131178
    invoke-direct {p0}, LX/EWl;->x()LX/EWr;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2131179
    sget-object v0, LX/EWr;->c:LX/EWr;

    move-object v0, v0

    .line 2131180
    return-object v0
.end method
