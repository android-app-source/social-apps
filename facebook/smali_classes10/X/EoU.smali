.class public LX/EoU;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2168195
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2168196
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EoU;->a:Z

    .line 2168197
    const v0, 0x7f030498

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2168198
    const v0, 0x7f020634

    invoke-virtual {p0, v0}, LX/EoU;->setBackgroundResource(I)V

    .line 2168199
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2168208
    iget-boolean v0, p0, LX/EoU;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x14d19f40

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2168204
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2168205
    const/4 v1, 0x1

    .line 2168206
    iput-boolean v1, p0, LX/EoU;->a:Z

    .line 2168207
    const/16 v1, 0x2d

    const v2, 0x735abf18

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x165e2227

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2168200
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2168201
    const/4 v1, 0x0

    .line 2168202
    iput-boolean v1, p0, LX/EoU;->a:Z

    .line 2168203
    const/16 v1, 0x2d

    const v2, -0x392bfa39

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
