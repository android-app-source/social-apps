.class public final LX/DGD;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DGE;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/DGE;


# direct methods
.method public constructor <init>(LX/DGE;)V
    .locals 1

    .prologue
    .line 1979401
    iput-object p1, p0, LX/DGD;->c:LX/DGE;

    .line 1979402
    move-object v0, p1

    .line 1979403
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1979404
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1979400
    const-string v0, "NetEgoStorySetHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1979386
    if-ne p0, p1, :cond_1

    .line 1979387
    :cond_0
    :goto_0
    return v0

    .line 1979388
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1979389
    goto :goto_0

    .line 1979390
    :cond_3
    check-cast p1, LX/DGD;

    .line 1979391
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1979392
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1979393
    if-eq v2, v3, :cond_0

    .line 1979394
    iget-object v2, p0, LX/DGD;->a:LX/1Pd;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DGD;->a:LX/1Pd;

    iget-object v3, p1, LX/DGD;->a:LX/1Pd;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1979395
    goto :goto_0

    .line 1979396
    :cond_5
    iget-object v2, p1, LX/DGD;->a:LX/1Pd;

    if-nez v2, :cond_4

    .line 1979397
    :cond_6
    iget-object v2, p0, LX/DGD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/DGD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/DGD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1979398
    goto :goto_0

    .line 1979399
    :cond_7
    iget-object v2, p1, LX/DGD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
