.class public abstract LX/EkV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AU7;


# instance fields
.field public final a:[Ljava/lang/Object;

.field public final b:LX/1Se;

.field public final c:LX/EkU;

.field public final d:[Ljava/lang/String;

.field public final e:I

.field public final f:[Ljava/lang/String;

.field public final g:[I

.field public final h:Ljava/lang/String;

.field public final i:I

.field public final j:LX/EkZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Se;LX/Ekf;[Ljava/lang/Object;LX/EkZ;)V
    .locals 2
    .param p4    # LX/EkZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x5

    .line 2163559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163560
    iput-object p1, p0, LX/EkV;->b:LX/1Se;

    .line 2163561
    iput-object p4, p0, LX/EkV;->j:LX/EkZ;

    .line 2163562
    invoke-virtual {p2, p3}, LX/Ekf;->a([Ljava/lang/Object;)LX/EkU;

    move-result-object v0

    iput-object v0, p0, LX/EkV;->c:LX/EkU;

    .line 2163563
    const/4 v0, 0x2

    aget-object v0, p3, v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, LX/EkV;->d:[Ljava/lang/String;

    .line 2163564
    const/4 v0, 0x3

    aget-object v0, p3, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/EkV;->e:I

    .line 2163565
    iget v0, p0, LX/EkV;->e:I

    if-eq v0, v1, :cond_0

    .line 2163566
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2163567
    :cond_0
    const/4 v0, 0x4

    aget-object v0, p3, v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, LX/EkV;->f:[Ljava/lang/String;

    .line 2163568
    aget-object v0, p3, v1

    check-cast v0, [I

    check-cast v0, [I

    iput-object v0, p0, LX/EkV;->g:[I

    .line 2163569
    const/4 v0, 0x6

    aget-object v0, p3, v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/EkV;->h:Ljava/lang/String;

    .line 2163570
    const/4 v0, 0x7

    aget-object v0, p3, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/EkV;->i:I

    .line 2163571
    iget-object v0, p0, LX/EkV;->d:[Ljava/lang/String;

    array-length v0, v0

    iget v1, p0, LX/EkV;->i:I

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/EkV;->a:[Ljava/lang/Object;

    .line 2163572
    return-void
.end method


# virtual methods
.method public a(I)LX/AU7;
    .locals 2

    .prologue
    .line 2163557
    iget-object v0, p0, LX/EkV;->a:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 2163558
    return-object p0
.end method

.method public a(ILjava/lang/Boolean;)LX/AU7;
    .locals 1

    .prologue
    .line 2163555
    iget-object v0, p0, LX/EkV;->a:[Ljava/lang/Object;

    aput-object p2, v0, p1

    .line 2163556
    return-object p0
.end method

.method public a(ILjava/lang/Double;)LX/AU7;
    .locals 1

    .prologue
    .line 2163553
    iget-object v0, p0, LX/EkV;->a:[Ljava/lang/Object;

    aput-object p2, v0, p1

    .line 2163554
    return-object p0
.end method

.method public a(ILjava/lang/Integer;)LX/AU7;
    .locals 1

    .prologue
    .line 2163551
    iget-object v0, p0, LX/EkV;->a:[Ljava/lang/Object;

    aput-object p2, v0, p1

    .line 2163552
    return-object p0
.end method

.method public a(ILjava/lang/Long;)LX/AU7;
    .locals 1

    .prologue
    .line 2163549
    iget-object v0, p0, LX/EkV;->a:[Ljava/lang/Object;

    aput-object p2, v0, p1

    .line 2163550
    return-object p0
.end method

.method public a(ILjava/lang/String;)LX/AU7;
    .locals 1

    .prologue
    .line 2163547
    iget-object v0, p0, LX/EkV;->a:[Ljava/lang/Object;

    aput-object p2, v0, p1

    .line 2163548
    return-object p0
.end method
