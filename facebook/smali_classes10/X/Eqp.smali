.class public final LX/Eqp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Equ;


# direct methods
.method public constructor <init>(LX/Equ;)V
    .locals 0

    .prologue
    .line 2172071
    iput-object p1, p0, LX/Eqp;->a:LX/Equ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2172072
    iget-object v1, p0, LX/Eqp;->a:LX/Equ;

    iget-object v1, v1, LX/Equ;->d:LX/0Vd;

    iget-object v2, p0, LX/Eqp;->a:LX/Equ;

    iget-object v2, v2, LX/Equ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2172073
    invoke-static {v1, v2}, LX/Equ;->a(LX/0Vd;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2172074
    iget-object v0, p0, LX/Eqp;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->n:LX/Eqg;

    invoke-virtual {v0}, LX/Eqg;->a()V

    .line 2172075
    iget-object v0, p0, LX/Eqp;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->x:LX/ErU;

    invoke-virtual {v0}, LX/ErU;->b()V

    .line 2172076
    iget-object v0, p0, LX/Eqp;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->u:LX/03V;

    sget-object v1, LX/Equ;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch facebook friends"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2172077
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2172078
    move-object v1, v1

    .line 2172079
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2172080
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2172081
    const/4 v4, 0x0

    .line 2172082
    :try_start_0
    iget-object v0, p0, LX/Eqp;->a:LX/Equ;

    iget-object v0, v0, LX/Equ;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    const v1, 0x3c624d63

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2172083
    :try_start_1
    iget-object v1, p0, LX/Eqp;->a:LX/Equ;

    iget-object v1, v1, LX/Equ;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    const v2, 0x288f9631

    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;

    .line 2172084
    if-nez v1, :cond_0

    .line 2172085
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Restrictions model is null"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 2172086
    :catch_0
    move-exception v0

    .line 2172087
    iget-object v1, p0, LX/Eqp;->a:LX/Equ;

    iget-object v1, v1, LX/Equ;->u:LX/03V;

    sget-object v2, LX/Equ;->a:Ljava/lang/String;

    const-string v3, "Failed to fetch restrictions for facebook friends"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 2172088
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2172089
    move-object v0, v2

    .line 2172090
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 2172091
    :goto_0
    return-void

    .line 2172092
    :catch_1
    move-exception v0

    .line 2172093
    :goto_1
    invoke-virtual {p0, v0}, LX/Eqp;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2172094
    :cond_0
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v5

    .line 2172095
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v6

    .line 2172096
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2172097
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v3, v4

    .line 2172098
    :goto_2
    if-ge v3, v8, :cond_4

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;

    .line 2172099
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 2172100
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    move-result-object v9

    .line 2172101
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-eq v9, p1, :cond_1

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-eq v9, p1, :cond_1

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-eq v9, p1, :cond_1

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->REMOVED:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-eq v9, p1, :cond_1

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-ne v9, p1, :cond_9

    :cond_1
    const/4 p1, 0x1

    :goto_3
    move v9, p1

    .line 2172102
    if-eqz v9, :cond_3

    .line 2172103
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2172104
    :cond_2
    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2172105
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    move-result-object v9

    .line 2172106
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;->INELIGIBLE_FOR_EVENT:Lcom/facebook/graphql/enums/GraphQLEventInviteeStatusType;

    if-ne v9, p1, :cond_a

    const/4 p1, 0x1

    :goto_5
    move v9, p1

    .line 2172107
    if-eqz v9, :cond_2

    .line 2172108
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel$UninvitableFriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_4

    .line 2172109
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_8

    .line 2172110
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSpecificUninvitableFriendsAndInviteeLimitModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2172111
    invoke-virtual {v2, v1, v4}, LX/15i;->j(II)I

    move-result v1

    .line 2172112
    :goto_6
    iget-object v2, p0, LX/Eqp;->a:LX/Equ;

    iget-object v2, v2, LX/Equ;->n:LX/Eqg;

    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v3

    invoke-virtual {v6}, LX/0cA;->b()LX/0Rf;

    move-result-object v4

    .line 2172113
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2172114
    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v5

    invoke-virtual {v5}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    :goto_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2172115
    invoke-virtual {v5}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v6

    .line 2172116
    invoke-virtual {v3, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 2172117
    invoke-virtual {v4, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    const/4 v6, 0x1

    .line 2172118
    :goto_8
    iput-boolean v6, v5, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    .line 2172119
    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    .line 2172120
    :cond_6
    const/4 v6, 0x0

    goto :goto_8

    .line 2172121
    :cond_7
    invoke-static {v2, v1}, LX/Eqg;->a(LX/Eqg;I)V

    .line 2172122
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 2172123
    iget-object v6, v2, LX/Eqg;->b:LX/0Px;

    if-nez v6, :cond_b

    .line 2172124
    iput-object v5, v2, LX/Eqg;->b:LX/0Px;

    .line 2172125
    :goto_9
    goto/16 :goto_0

    .line 2172126
    :cond_8
    const v1, 0x7fffffff

    goto :goto_6

    .line 2172127
    :catch_2
    move-exception v0

    goto/16 :goto_1

    :cond_9
    const/4 p1, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 p1, 0x0

    goto :goto_5

    .line 2172128
    :cond_b
    invoke-static {v2, v5}, LX/Eqg;->b(LX/Eqg;LX/0Px;)V

    .line 2172129
    iget-object v5, v2, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v5, v5, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    iget-object v6, v2, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v6, v6, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->s:LX/0Px;

    const/4 v7, 0x0

    .line 2172130
    iput-boolean v7, v5, LX/ErB;->l:Z

    .line 2172131
    iput-object v6, v5, LX/ErB;->o:LX/0Px;

    .line 2172132
    iget-object v8, v5, LX/ErB;->g:[LX/Er9;

    array-length v9, v8

    :goto_a
    if-ge v7, v9, :cond_d

    aget-object v2, v8, v7

    .line 2172133
    if-eqz v2, :cond_c

    .line 2172134
    iget-object v0, v5, LX/ErB;->o:LX/0Px;

    .line 2172135
    iget-object v3, v2, LX/Er9;->h:LX/Blb;

    sget-object v6, LX/Blb;->FACEBOOK:LX/Blb;

    if-eq v3, v6, :cond_e

    .line 2172136
    :cond_c
    :goto_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_a

    .line 2172137
    :cond_d
    goto :goto_9

    .line 2172138
    :cond_e
    iput-object v0, v2, LX/Er9;->r:LX/0Px;

    .line 2172139
    iget-object v3, v2, LX/Er9;->l:LX/Eqx;

    invoke-virtual {v3, v0}, LX/Eqx;->a(LX/0Px;)V

    goto :goto_b
.end method
