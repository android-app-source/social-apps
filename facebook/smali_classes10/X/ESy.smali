.class public LX/ESy;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Tn;

.field public static final b:LX/0Tn;


# instance fields
.field public c:LX/0xX;

.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public e:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2123869
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "video_home_cached_latw"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2123870
    sput-object v0, LX/ESy;->a:LX/0Tn;

    const-string v1, "last_cached_latw_fetch_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/ESy;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0xX;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2123871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2123872
    iput-object p1, p0, LX/ESy;->c:LX/0xX;

    .line 2123873
    iput-object p2, p0, LX/ESy;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2123874
    iput-object p3, p0, LX/ESy;->e:LX/0SG;

    .line 2123875
    return-void
.end method

.method public static b(LX/0QB;)LX/ESy;
    .locals 4

    .prologue
    .line 2123876
    new-instance v3, LX/ESy;

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v0

    check-cast v0, LX/0xX;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-direct {v3, v0, v1, v2}, LX/ESy;-><init>(LX/0xX;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    .line 2123877
    return-object v3
.end method
