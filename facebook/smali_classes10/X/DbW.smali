.class public final LX/DbW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DbX;


# direct methods
.method public constructor <init>(LX/DbX;)V
    .locals 0

    .prologue
    .line 2017152
    iput-object p1, p0, LX/DbW;->a:LX/DbX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x769a2e38

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017153
    iget-object v1, p0, LX/DbW;->a:LX/DbX;

    iget-object v1, v1, LX/DbX;->c:LX/DbM;

    iget-object v2, p0, LX/DbW;->a:LX/DbX;

    iget-object v2, v2, LX/DbX;->g:Ljava/lang/String;

    iget-object v3, p0, LX/DbW;->a:LX/DbX;

    iget-object v3, v3, LX/DbX;->j:Ljava/lang/String;

    .line 2017154
    iget-object v4, v1, LX/DbM;->a:LX/0Zb;

    const-string p1, "menu_viewer_see_more_food_photos_tap"

    invoke-static {v2, p1, v3}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v4, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2017155
    iget-object v1, p0, LX/DbW;->a:LX/DbX;

    iget-object v1, v1, LX/DbX;->e:LX/17Y;

    iget-object v2, p0, LX/DbW;->a:LX/DbX;

    iget-object v2, v2, LX/DbX;->k:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    invoke-virtual {v2}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->eD:Ljava/lang/String;

    iget-object v4, p0, LX/DbW;->a:LX/DbX;

    iget-object v4, v4, LX/DbX;->j:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "local_content_entry_point"

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->FOOD_PHOTOS_OF_PLACE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    .line 2017156
    iget-object v2, p0, LX/DbW;->a:LX/DbX;

    iget-object v2, v2, LX/DbX;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/DbW;->a:LX/DbX;

    iget-object v3, v3, LX/DbX;->k:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    invoke-virtual {v3}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2017157
    const v1, -0x6e0e4bab

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
