.class public final LX/DUC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 2001844
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 2001845
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2001846
    :goto_0
    return v6

    .line 2001847
    :cond_0
    const-string v11, "added_time"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2001848
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v1

    .line 2001849
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_5

    .line 2001850
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2001851
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2001852
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2001853
    const-string v11, "added_by"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2001854
    invoke-static {p0, p1}, LX/DUB;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2001855
    :cond_2
    const-string v11, "admin_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2001856
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 2001857
    :cond_3
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2001858
    invoke-static {p0, p1}, LX/Daw;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2001859
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2001860
    :cond_5
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2001861
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 2001862
    if-eqz v0, :cond_6

    move-object v0, p1

    .line 2001863
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2001864
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2001865
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2001866
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_7
    move v0, v6

    move v7, v6

    move v8, v6

    move-wide v2, v4

    move v9, v6

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x2

    .line 2001825
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2001826
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2001827
    if-eqz v0, :cond_0

    .line 2001828
    const-string v1, "added_by"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001829
    invoke-static {p0, v0, p2}, LX/DUB;->a(LX/15i;ILX/0nX;)V

    .line 2001830
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2001831
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 2001832
    const-string v2, "added_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001833
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2001834
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2001835
    if-eqz v0, :cond_2

    .line 2001836
    const-string v0, "admin_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001837
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2001838
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2001839
    if-eqz v0, :cond_3

    .line 2001840
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2001841
    invoke-static {p0, v0, p2, p3}, LX/Daw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2001842
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2001843
    return-void
.end method
