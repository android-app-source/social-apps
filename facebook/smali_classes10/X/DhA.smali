.class public LX/DhA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DhB;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/compactdisk/StoreManagerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/compactdisk/DiskCache;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2030666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030667
    return-void
.end method

.method public static a(LX/DhA;)Lcom/facebook/compactdisk/DiskCache;
    .locals 7

    .prologue
    .line 2030668
    iget-object v0, p0, LX/DhA;->c:Lcom/facebook/compactdisk/DiskCache;

    if-nez v0, :cond_0

    .line 2030669
    iget-object v0, p0, LX/DhA;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/compactdisk/StoreManagerFactory;

    .line 2030670
    iget-object v1, p0, LX/DhA;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    .line 2030671
    const/4 v6, 0x1

    .line 2030672
    new-instance v2, Lcom/facebook/compactdisk/EvictionConfig;

    invoke-direct {v2}, Lcom/facebook/compactdisk/EvictionConfig;-><init>()V

    const-wide/32 v4, 0x500000

    invoke-virtual {v2, v4, v5}, Lcom/facebook/compactdisk/EvictionConfig;->maxSize(J)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/facebook/compactdisk/EvictionConfig;->strictEnforcement(Z)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v2

    .line 2030673
    new-instance v3, Lcom/facebook/compactdisk/StalePruningConfig;

    invoke-direct {v3}, Lcom/facebook/compactdisk/StalePruningConfig;-><init>()V

    const-wide/32 v4, 0x24ea00

    invoke-virtual {v3, v4, v5}, Lcom/facebook/compactdisk/StalePruningConfig;->a(J)Lcom/facebook/compactdisk/StalePruningConfig;

    move-result-object v3

    .line 2030674
    new-instance v4, Lcom/facebook/compactdisk/ManagedConfig;

    invoke-direct {v4}, Lcom/facebook/compactdisk/ManagedConfig;-><init>()V

    invoke-virtual {v4, v3}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/StalePruningConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/EvictionConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v2

    .line 2030675
    new-instance v3, Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-direct {v3}, Lcom/facebook/compactdisk/DiskCacheConfig;-><init>()V

    sget-object v4, Lcom/facebook/compactdisk/DiskArea;->DOCUMENTS:Lcom/facebook/compactdisk/DiskArea;

    invoke-virtual {v3, v4}, Lcom/facebook/compactdisk/DiskCacheConfig;->diskArea(Lcom/facebook/compactdisk/DiskArea;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v3

    const-wide/16 v4, 0x1e

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/compactdisk/DiskCacheConfig;->maxCapacity(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v3

    const-string v4, "StyleTransferCache"

    invoke-virtual {v3, v4}, Lcom/facebook/compactdisk/DiskCacheConfig;->name(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/facebook/compactdisk/DiskCacheConfig;->sessionScoped(Z)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/facebook/compactdisk/DiskCacheConfig;->subConfig(Lcom/facebook/compactdisk/SubConfig;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    const-wide/16 v4, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/compactdisk/DiskCacheConfig;->version(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v2

    move-object v1, v2

    .line 2030676
    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/StoreManager;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object v0

    iput-object v0, p0, LX/DhA;->c:Lcom/facebook/compactdisk/DiskCache;

    .line 2030677
    :cond_0
    iget-object v0, p0, LX/DhA;->c:Lcom/facebook/compactdisk/DiskCache;

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2030678
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "InitNet"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DhA;
    .locals 3

    .prologue
    .line 2030679
    new-instance v0, LX/DhA;

    invoke-direct {v0}, LX/DhA;-><init>()V

    .line 2030680
    const/16 v1, 0x2839

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x39f

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 2030681
    iput-object v1, v0, LX/DhA;->a:LX/0Or;

    iput-object v2, v0, LX/DhA;->b:LX/0Or;

    .line 2030682
    return-object v0
.end method

.method public static b(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2030683
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "PredictNet"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2030684
    invoke-static {p0}, LX/DhA;->a(LX/DhA;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2030685
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 2030686
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    :cond_1
    return-object v0

    .line 2030687
    :cond_2
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
