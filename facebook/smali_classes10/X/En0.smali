.class public LX/En0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Emy;

.field public b:I

.field private c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(LX/Emy;)V
    .locals 0
    .param p1    # LX/Emy;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2166559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166560
    iput-object p1, p0, LX/En0;->a:LX/Emy;

    .line 2166561
    return-void
.end method

.method private a(I)LX/Enq;
    .locals 1

    .prologue
    .line 2166562
    iget v0, p0, LX/En0;->b:I

    if-le v0, p1, :cond_0

    sget-object v0, LX/Enq;->LEFT:LX/Enq;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Enq;->RIGHT:LX/Enq;

    goto :goto_0
.end method

.method private static d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2166531
    instance-of v0, p0, Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    move v0, v0

    .line 2166532
    if-nez v0, :cond_0

    .line 2166533
    instance-of v0, p0, Lcom/facebook/entitycards/model/ScrollLoadError;

    move v0, v0

    .line 2166534
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 2166535
    iget v0, p0, LX/En0;->b:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, LX/En0;->c:Ljava/lang/Object;

    if-ne v0, p2, :cond_0

    .line 2166536
    :goto_0
    return-void

    .line 2166537
    :cond_0
    iget-object v0, p0, LX/En0;->c:Ljava/lang/Object;

    .line 2166538
    instance-of v1, v0, Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    move v0, v1

    .line 2166539
    if-nez v0, :cond_2

    .line 2166540
    instance-of v0, p2, Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    move v0, v0

    .line 2166541
    if-eqz v0, :cond_2

    .line 2166542
    iget-object v0, p0, LX/En0;->a:LX/Emy;

    invoke-direct {p0, p1}, LX/En0;->a(I)LX/Enq;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Emy;->a(LX/Enq;)V

    .line 2166543
    :cond_1
    :goto_1
    iput-object p2, p0, LX/En0;->c:Ljava/lang/Object;

    .line 2166544
    iput p1, p0, LX/En0;->b:I

    goto :goto_0

    .line 2166545
    :cond_2
    iget-object v0, p0, LX/En0;->c:Ljava/lang/Object;

    .line 2166546
    instance-of v1, v0, Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    move v0, v1

    .line 2166547
    if-eqz v0, :cond_4

    .line 2166548
    instance-of v0, p2, Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    move v0, v0

    .line 2166549
    if-nez v0, :cond_4

    .line 2166550
    instance-of v0, p2, Lcom/facebook/entitycards/model/ScrollLoadError;

    move v0, v0

    .line 2166551
    if-nez v0, :cond_3

    .line 2166552
    iget-object v0, p0, LX/En0;->a:LX/Emy;

    invoke-direct {p0, p1}, LX/En0;->a(I)LX/Enq;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Emy;->b(LX/Enq;)V

    goto :goto_1

    .line 2166553
    :cond_3
    iget-object v0, p0, LX/En0;->a:LX/Emy;

    .line 2166554
    const v1, 0x100003

    const-string v2, "ec_card_scroll_wait_time"

    invoke-static {v0, v1, v2}, LX/Emy;->c(LX/Emy;ILjava/lang/String;)V

    .line 2166555
    goto :goto_1

    .line 2166556
    :cond_4
    iget-object v0, p0, LX/En0;->c:Ljava/lang/Object;

    invoke-static {v0}, LX/En0;->d(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, LX/En0;->d(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2166557
    iget-object v0, p0, LX/En0;->a:LX/Emy;

    invoke-direct {p0, p1}, LX/En0;->a(I)LX/Enq;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Emy;->a(LX/Enq;)V

    .line 2166558
    iget-object v0, p0, LX/En0;->a:LX/Emy;

    invoke-direct {p0, p1}, LX/En0;->a(I)LX/Enq;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Emy;->b(LX/Enq;)V

    goto :goto_1
.end method
