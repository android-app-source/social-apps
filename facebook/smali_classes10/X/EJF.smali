.class public LX/EJF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EJD;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EJH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2103697
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EJF;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EJH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103671
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2103672
    iput-object p1, p0, LX/EJF;->b:LX/0Ot;

    .line 2103673
    return-void
.end method

.method public static a(LX/0QB;)LX/EJF;
    .locals 4

    .prologue
    .line 2103686
    const-class v1, LX/EJF;

    monitor-enter v1

    .line 2103687
    :try_start_0
    sget-object v0, LX/EJF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103688
    sput-object v2, LX/EJF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103689
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103690
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103691
    new-instance v3, LX/EJF;

    const/16 p0, 0x338a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EJF;-><init>(LX/0Ot;)V

    .line 2103692
    move-object v0, v3

    .line 2103693
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103694
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EJF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103695
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103696
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2103676
    check-cast p2, LX/EJE;

    .line 2103677
    iget-object v0, p0, LX/EJF;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EJH;

    iget-object v1, p2, LX/EJE;->a:Ljava/lang/CharSequence;

    iget-object v2, p2, LX/EJE;->b:Ljava/lang/CharSequence;

    const/16 p2, 0x3c

    const/4 p0, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x7

    const/4 v7, 0x6

    .line 2103678
    iget-object v3, v0, LX/EJH;->a:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v4, 0x7f0217c2

    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const v4, 0x7f0a00d1

    invoke-virtual {v3, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 2103679
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const/16 v5, 0xa

    invoke-interface {v4, v8, v5}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const/16 v5, 0xe

    invoke-interface {v4, v7, v5}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00d5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-interface {v4, v5}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v8, v7}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2103680
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2103681
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0058

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00a8

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    sget-object v5, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v4, v5}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v8, v7}, LX/1Di;->h(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2103682
    :cond_0
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2103683
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0050

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00a4

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    sget-object v5, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v4, v5}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v8, v7}, LX/1Di;->h(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2103684
    :cond_1
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2103685
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2103674
    invoke-static {}, LX/1dS;->b()V

    .line 2103675
    const/4 v0, 0x0

    return-object v0
.end method
