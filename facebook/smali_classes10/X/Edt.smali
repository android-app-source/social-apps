.class public final LX/Edt;
.super LX/Edp;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Edp",
        "<",
        "Landroid/net/Uri;",
        "LX/Edu;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Landroid/content/UriMatcher;

.field private static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/Edt;


# instance fields
.field public final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x4

    const/4 v4, 0x2

    .line 2151593
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 2151594
    sput-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151595
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const-string v2, "#"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151596
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const-string v2, "inbox"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151597
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const-string v2, "inbox/#"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151598
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const-string v2, "sent"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151599
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const-string v2, "sent/#"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151600
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const-string v2, "drafts"

    invoke-virtual {v0, v1, v2, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151601
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const-string v2, "drafts/#"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151602
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const-string v2, "outbox"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151603
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms"

    const-string v2, "outbox/#"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151604
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "conversations"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151605
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    const-string v1, "mms-sms"

    const-string v2, "conversations/#"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2151606
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 2151607
    sput-object v0, LX/Edt;->b:Landroid/util/SparseArray;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151608
    sget-object v0, LX/Edt;->b:Landroid/util/SparseArray;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151609
    sget-object v0, LX/Edt;->b:Landroid/util/SparseArray;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v8, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151610
    sget-object v0, LX/Edt;->b:Landroid/util/SparseArray;

    const/16 v1, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2151611
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2151704
    invoke-direct {p0}, LX/Edp;-><init>()V

    .line 2151705
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Edt;->d:Landroid/util/SparseArray;

    .line 2151706
    new-instance v0, LX/01J;

    invoke-direct {v0}, LX/01J;-><init>()V

    iput-object v0, p0, LX/Edt;->e:LX/01J;

    .line 2151707
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Edt;->f:Ljava/util/HashSet;

    .line 2151708
    return-void
.end method

.method public static final declared-synchronized b()LX/Edt;
    .locals 2

    .prologue
    .line 2151700
    const-class v1, LX/Edt;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Edt;->c:LX/Edt;

    if-nez v0, :cond_0

    .line 2151701
    new-instance v0, LX/Edt;

    invoke-direct {v0}, LX/Edt;-><init>()V

    sput-object v0, LX/Edt;->c:LX/Edt;

    .line 2151702
    :cond_0
    sget-object v0, LX/Edt;->c:LX/Edt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2151703
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(LX/Edt;Landroid/net/Uri;LX/Edu;)V
    .locals 6

    .prologue
    .line 2151694
    iget-object v0, p0, LX/Edt;->e:LX/01J;

    .line 2151695
    iget-wide v4, p2, LX/Edu;->c:J

    move-wide v2, v4

    .line 2151696
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 2151697
    if-eqz v0, :cond_0

    .line 2151698
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2151699
    :cond_0
    return-void
.end method

.method private c(Landroid/net/Uri;)LX/Edu;
    .locals 1

    .prologue
    .line 2151688
    iget-object v0, p0, LX/Edt;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2151689
    invoke-super {p0, p1}, LX/Edp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Edu;

    .line 2151690
    if-eqz v0, :cond_0

    .line 2151691
    invoke-static {p0, p1, v0}, LX/Edt;->b(LX/Edt;Landroid/net/Uri;LX/Edu;)V

    .line 2151692
    invoke-static {p0, p1, v0}, LX/Edt;->c(LX/Edt;Landroid/net/Uri;LX/Edu;)V

    .line 2151693
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/Edt;Landroid/net/Uri;LX/Edu;)V
    .locals 4

    .prologue
    .line 2151682
    iget-object v0, p0, LX/Edt;->e:LX/01J;

    .line 2151683
    iget v1, p2, LX/Edu;->b:I

    move v1, v1

    .line 2151684
    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 2151685
    if-eqz v0, :cond_0

    .line 2151686
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2151687
    :cond_0
    return-void
.end method

.method private static d(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 2151676
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 2151677
    packed-switch v0, :pswitch_data_0

    .line 2151678
    :pswitch_0
    const/4 p0, 0x0

    .line 2151679
    :goto_0
    :pswitch_1
    return-object p0

    .line 2151680
    :pswitch_2
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 2151681
    sget-object v1, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 2151670
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, LX/Edp;->a()V

    .line 2151671
    iget-object v0, p0, LX/Edt;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 2151672
    iget-object v0, p0, LX/Edt;->e:LX/01J;

    invoke-virtual {v0}, LX/01J;->clear()V

    .line 2151673
    iget-object v0, p0, LX/Edt;->f:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2151674
    monitor-exit p0

    return-void

    .line 2151675
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/net/Uri;Z)V
    .locals 1

    .prologue
    .line 2151665
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 2151666
    :try_start_0
    iget-object v0, p0, LX/Edt;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2151667
    :goto_0
    monitor-exit p0

    return-void

    .line 2151668
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Edt;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2151669
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 2151664
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Edt;->f:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/net/Uri;LX/Edu;)Z
    .locals 7

    .prologue
    .line 2151645
    monitor-enter p0

    .line 2151646
    :try_start_0
    iget v0, p2, LX/Edu;->b:I

    move v1, v0

    .line 2151647
    iget-object v0, p0, LX/Edt;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 2151648
    if-nez v0, :cond_2

    .line 2151649
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2151650
    iget-object v2, p0, LX/Edt;->d:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v1, v0

    .line 2151651
    :goto_0
    iget-wide v5, p2, LX/Edu;->c:J

    move-wide v2, v5

    .line 2151652
    iget-object v0, p0, LX/Edt;->e:LX/01J;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 2151653
    if-nez v0, :cond_0

    .line 2151654
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2151655
    iget-object v4, p0, LX/Edt;->e:LX/01J;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v2, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2151656
    :cond_0
    invoke-static {p1}, LX/Edt;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 2151657
    invoke-super {p0, v2, p2}, LX/Edp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    .line 2151658
    if-eqz v3, :cond_1

    .line 2151659
    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2151660
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2151661
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Edt;->a(Landroid/net/Uri;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2151662
    monitor-exit p0

    return v3

    .line 2151663
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2151644
    check-cast p1, Landroid/net/Uri;

    check-cast p2, LX/Edu;

    invoke-virtual {p0, p1, p2}, LX/Edt;->a(Landroid/net/Uri;LX/Edu;)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized b(Landroid/net/Uri;)LX/Edu;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2151613
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/Edt;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2151614
    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 2151615
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2151616
    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1}, LX/Edt;->c(Landroid/net/Uri;)LX/Edu;

    move-result-object v0

    goto :goto_0

    .line 2151617
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 2151618
    sget-object v1, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Edt;->c(Landroid/net/Uri;)LX/Edu;

    move-result-object v0

    goto :goto_0

    .line 2151619
    :pswitch_2
    invoke-virtual {p0}, LX/Edp;->a()V

    move-object v0, v1

    .line 2151620
    goto :goto_0

    .line 2151621
    :pswitch_3
    sget-object v2, LX/Edt;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2151622
    if-eqz v0, :cond_1

    .line 2151623
    iget-object v2, p0, LX/Edt;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashSet;

    .line 2151624
    iget-object v3, p0, LX/Edt;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 2151625
    if-eqz v2, :cond_1

    .line 2151626
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 2151627
    iget-object v3, p0, LX/Edt;->f:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2151628
    invoke-super {p0, v2}, LX/Edp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Edu;

    .line 2151629
    if-eqz v3, :cond_0

    .line 2151630
    invoke-static {p0, v2, v3}, LX/Edt;->b(LX/Edt;Landroid/net/Uri;LX/Edu;)V

    goto :goto_1

    .line 2151631
    :cond_1
    move-object v0, v1

    .line 2151632
    goto :goto_0

    .line 2151633
    :pswitch_4
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 2151634
    iget-object v0, p0, LX/Edt;->e:LX/01J;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 2151635
    if-eqz v0, :cond_3

    .line 2151636
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2151637
    iget-object v4, p0, LX/Edt;->f:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2151638
    invoke-super {p0, v0}, LX/Edp;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Edu;

    .line 2151639
    if-eqz v4, :cond_2

    .line 2151640
    invoke-static {p0, v0, v4}, LX/Edt;->c(LX/Edt;Landroid/net/Uri;LX/Edu;)V

    goto :goto_2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2151641
    :cond_3
    move-object v0, v1

    .line 2151642
    goto/16 :goto_0

    .line 2151643
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2151612
    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, LX/Edt;->b(Landroid/net/Uri;)LX/Edu;

    move-result-object v0

    return-object v0
.end method
