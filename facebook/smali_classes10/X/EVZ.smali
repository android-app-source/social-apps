.class public final LX/EVZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CfG;


# instance fields
.field public final synthetic a:LX/EVa;


# direct methods
.method public constructor <init>(LX/EVa;)V
    .locals 0

    .prologue
    .line 2128397
    iput-object p1, p0, LX/EVZ;->a:LX/EVa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2128398
    return-void
.end method


# virtual methods
.method public final a(LX/9qT;)V
    .locals 0

    .prologue
    .line 2128370
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 0
    .param p2    # Lcom/facebook/composer/publish/common/PendingStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2128396
    return-void
.end method

.method public final kJ_()Z
    .locals 1

    .prologue
    .line 2128395
    const/4 v0, 0x1

    return v0
.end method

.method public final kK_()V
    .locals 5

    .prologue
    .line 2128389
    iget-object v0, p0, LX/EVZ;->a:LX/EVa;

    iget-object v0, v0, LX/EVa;->k:LX/2jY;

    if-eqz v0, :cond_0

    .line 2128390
    iget-object v0, p0, LX/EVZ;->a:LX/EVa;

    invoke-static {v0}, LX/EVa;->e(LX/EVa;)LX/0ho;

    .line 2128391
    sget-object v0, LX/EVa;->c:Ljava/lang/String;

    const-string v1, "prefetchVideoHomeData prefetch error sessionId = %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/EVZ;->a:LX/EVa;

    iget-object v4, v4, LX/EVa;->k:LX/2jY;

    .line 2128392
    iget-object p0, v4, LX/2jY;->a:Ljava/lang/String;

    move-object v4, p0

    .line 2128393
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2128394
    :cond_0
    return-void
.end method

.method public final kL_()V
    .locals 5

    .prologue
    .line 2128372
    iget-object v0, p0, LX/EVZ;->a:LX/EVa;

    iget-object v0, v0, LX/EVa;->k:LX/2jY;

    if-eqz v0, :cond_2

    .line 2128373
    iget-object v0, p0, LX/EVZ;->a:LX/EVa;

    invoke-static {v0}, LX/EVa;->e(LX/EVa;)LX/0ho;

    move-result-object v0

    .line 2128374
    if-eqz v0, :cond_0

    .line 2128375
    iget-object v1, p0, LX/EVZ;->a:LX/EVa;

    iget-object v2, p0, LX/EVZ;->a:LX/EVa;

    iget-object v2, v2, LX/EVa;->k:LX/2jY;

    invoke-static {v1, v2}, LX/EVa;->a$redex0(LX/EVa;LX/2jY;)I

    move-result v1

    invoke-interface {v0, v1}, LX/0ho;->a(I)V

    .line 2128376
    :cond_0
    iget-object v0, p0, LX/EVZ;->a:LX/EVa;

    .line 2128377
    iget-object v1, v0, LX/EVa;->k:LX/2jY;

    if-eqz v1, :cond_1

    .line 2128378
    iget-object v1, v0, LX/EVa;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2xj;

    invoke-virtual {v1}, LX/2xj;->i()Z

    move-result v1

    move v1, v1

    .line 2128379
    if-eqz v1, :cond_3

    .line 2128380
    :cond_1
    :goto_0
    iget-object v0, p0, LX/EVZ;->a:LX/EVa;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EVa;->a(Z)V

    .line 2128381
    iget-object v0, p0, LX/EVZ;->a:LX/EVa;

    iget-object v1, p0, LX/EVZ;->a:LX/EVa;

    iget-object v1, v1, LX/EVa;->s:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 2128382
    iput-wide v2, v0, LX/EVa;->A:J

    .line 2128383
    iget-object v0, p0, LX/EVZ;->a:LX/EVa;

    iget-object v0, v0, LX/EVa;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2128384
    iget-object v0, p0, LX/EVZ;->a:LX/EVa;

    iget-object v0, v0, LX/EVa;->m:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "prefetchVideoHomeData finished succesfully"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2128385
    :cond_2
    return-void

    .line 2128386
    :cond_3
    sget-object v2, LX/379;->VIDEO_HOME_OCCLUSION:LX/379;

    .line 2128387
    iget-object v1, v0, LX/EVa;->k:LX/2jY;

    invoke-virtual {v1}, LX/2jY;->o()LX/0Px;

    move-result-object v3

    .line 2128388
    iget-object v1, v0, LX/EVa;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4, v2}, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->b(LX/0Px;ILX/379;)V

    goto :goto_0
.end method

.method public final kU_()V
    .locals 0

    .prologue
    .line 2128371
    return-void
.end method
