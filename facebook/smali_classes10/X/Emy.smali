.class public LX/Emy;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0Yj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2166489
    const-class v0, LX/Emy;

    sput-object v0, LX/Emy;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2166490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166491
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2166492
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2166493
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Emy;->f:Ljava/util/Map;

    .line 2166494
    iput-object p1, p0, LX/Emy;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2166495
    iput-object p2, p0, LX/Emy;->c:Ljava/lang/String;

    .line 2166496
    iput-object p3, p0, LX/Emy;->d:Ljava/lang/String;

    .line 2166497
    iput-object p4, p0, LX/Emy;->e:Ljava/lang/String;

    .line 2166498
    return-void

    :cond_0
    move v0, v2

    .line 2166499
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2166500
    goto :goto_1
.end method

.method public static a(LX/Emy;ILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2166501
    iget-object v0, p0, LX/Emy;->f:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yj;

    .line 2166502
    if-nez v0, :cond_1

    .line 2166503
    new-instance v0, LX/0Yj;

    invoke-direct {v0, p1, p2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    const-string v1, "entity_cards"

    .line 2166504
    iput-object v1, v0, LX/0Yj;->e:Ljava/lang/String;

    .line 2166505
    move-object v0, v0

    .line 2166506
    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "entity_cards"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yj;->b()LX/0Yj;

    move-result-object v0

    const-string v1, "surface"

    iget-object v2, p0, LX/Emy;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0Yj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 2166507
    if-eqz v4, :cond_0

    iget-object v0, v1, LX/0Yj;->l:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 2166508
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, v1, LX/0Yj;->l:Ljava/util/Map;

    .line 2166509
    :cond_0
    iget-object v0, v1, LX/0Yj;->l:Ljava/util/Map;

    move-object v2, v0

    .line 2166510
    const-string v0, "instance_id"

    iget-object v3, p0, LX/Emy;->c:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166511
    const-string v0, "surface"

    iget-object v3, p0, LX/Emy;->d:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166512
    const-string v3, "surface_source_id"

    iget-object v0, p0, LX/Emy;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Emy;->e:Ljava/lang/String;

    :goto_0
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166513
    iget-object v0, p0, LX/Emy;->f:Ljava/util/Map;

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 2166514
    :cond_1
    iget-object v1, p0, LX/Emy;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 2166515
    return-void

    .line 2166516
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public static b(LX/Emy;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 2166517
    iget-object v0, p0, LX/Emy;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "entity_cards"

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2166518
    return-void
.end method

.method public static c(LX/Emy;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 2166519
    iget-object v0, p0, LX/Emy;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "entity_cards"

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;Ljava/lang/String;)V

    .line 2166520
    return-void
.end method


# virtual methods
.method public final a(LX/Enq;)V
    .locals 2

    .prologue
    .line 2166521
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StartEntityCardsScrollWaitTime_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/Enq;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2166522
    const v0, 0x100003

    const-string v1, "ec_card_scroll_wait_time"

    invoke-static {p0, v0, v1}, LX/Emy;->a(LX/Emy;ILjava/lang/String;)V

    .line 2166523
    return-void
.end method

.method public final b(LX/Enq;)V
    .locals 2

    .prologue
    .line 2166524
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StopEntityCardsScrollWaitTime_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/Enq;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2166525
    const v0, 0x100003

    const-string v1, "ec_card_scroll_wait_time"

    invoke-static {p0, v0, v1}, LX/Emy;->b(LX/Emy;ILjava/lang/String;)V

    .line 2166526
    return-void
.end method
