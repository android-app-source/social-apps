.class public final LX/DzZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 0

    .prologue
    .line 2067082
    iput-object p1, p0, LX/DzZ;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x2e0e05c5

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2067083
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/DzZ;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/places/create/PlaceCreationCityPickerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2067084
    const-string v2, "current_location"

    iget-object v3, p0, LX/DzZ;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v3, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->s:Landroid/location/Location;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2067085
    const-string v2, "crowdsourcing_context"

    iget-object v3, p0, LX/DzZ;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    .line 2067086
    iget-object v4, v3, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v4

    .line 2067087
    const-string v4, "crowdsourcing_context"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2067088
    iget-object v2, p0, LX/DzZ;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v3, p0, LX/DzZ;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v3, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v4, LX/96A;->CITY_PICKER:LX/96A;

    invoke-virtual {v2, v3, v4}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    .line 2067089
    iget-object v2, p0, LX/DzZ;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->b:Lcom/facebook/content/SecureContextHelper;

    const/4 v3, 0x3

    iget-object v4, p0, LX/DzZ;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-interface {v2, v1, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2067090
    const v1, -0x2d699772    # -3.22999615E11f

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
