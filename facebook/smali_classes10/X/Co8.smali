.class public LX/Co8;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "LX/Cob;",
        "LX/Cm3;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Cob;)V
    .locals 2

    .prologue
    .line 1935008
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1935009
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, LX/Co8;

    const/16 v0, 0x31dc

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object p1, p0, LX/Co8;->d:LX/0Ot;

    .line 1935010
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 3

    .prologue
    .line 1935011
    check-cast p1, LX/Cm3;

    .line 1935012
    invoke-static {p1}, LX/Co1;->a(LX/Clu;)Landroid/os/Bundle;

    move-result-object v1

    .line 1935013
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935014
    check-cast v0, LX/Cob;

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1935015
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935016
    check-cast v0, LX/Cob;

    invoke-interface {p1}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v1

    invoke-interface {p1}, LX/Cm3;->a()LX/Clo;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/Cob;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;LX/Clo;)V

    .line 1935017
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935018
    check-cast v0, LX/Cob;

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Cob;->a(Ljava/lang/String;)V

    .line 1935019
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v0

    .line 1935020
    iget-object v0, p0, LX/Co8;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 1935021
    iget-object v2, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v2

    .line 1935022
    invoke-interface {p1}, LX/Clr;->o()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, p1, v0, v2}, LX/Co1;->a(LX/CnG;LX/Clq;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;Landroid/os/Bundle;)V

    .line 1935023
    return-void
.end method
