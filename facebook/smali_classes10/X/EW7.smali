.class public final LX/EW7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:LX/EWC;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(LX/EWC;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2129387
    iput-object p1, p0, LX/EW7;->a:LX/EWC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129388
    iput v0, p0, LX/EW7;->b:I

    .line 2129389
    iput v0, p0, LX/EW7;->c:I

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2129381
    iget v0, p0, LX/EW7;->e:I

    if-lez v0, :cond_0

    iget v0, p0, LX/EW7;->f:I

    if-nez v0, :cond_0

    .line 2129382
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-boolean v0, v0, LX/EWC;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-boolean v0, v0, LX/EWC;->n:Z

    if-eqz v0, :cond_1

    .line 2129383
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    invoke-static {v0}, LX/EWC;->c(LX/EWC;)V

    .line 2129384
    :cond_0
    :goto_0
    return-void

    .line 2129385
    :cond_1
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-boolean v0, v0, LX/EWC;->p:Z

    if-eqz v0, :cond_0

    .line 2129386
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    invoke-static {v0}, LX/EWC;->d(LX/EWC;)V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2129390
    iget v0, p0, LX/EW7;->d:I

    iget v1, p0, LX/EW7;->b:I

    if-eq v0, v1, :cond_0

    .line 2129391
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-boolean v0, v0, LX/EWC;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-wide v0, v0, LX/EWC;->k:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2129392
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-object v1, p0, LX/EW7;->a:LX/EWC;

    iget-wide v2, v1, LX/EWC;->k:J

    invoke-static {v0, v2, v3}, LX/EWC;->a(LX/EWC;J)V

    .line 2129393
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    invoke-static {v0}, LX/EWC;->g(LX/EWC;)V

    .line 2129394
    :cond_0
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2129374
    iget v0, p0, LX/EW7;->d:I

    iget v1, p0, LX/EW7;->e:I

    add-int/2addr v0, v1

    .line 2129375
    iget v1, p0, LX/EW7;->b:I

    iget v2, p0, LX/EW7;->c:I

    add-int/2addr v1, v2

    .line 2129376
    if-eq v0, v1, :cond_0

    .line 2129377
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-boolean v0, v0, LX/EWC;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-wide v0, v0, LX/EWC;->k:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2129378
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-object v1, p0, LX/EW7;->a:LX/EWC;

    iget-wide v2, v1, LX/EWC;->k:J

    invoke-static {v0, v2, v3}, LX/EWC;->a(LX/EWC;J)V

    .line 2129379
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    invoke-static {v0}, LX/EWC;->g(LX/EWC;)V

    .line 2129380
    :cond_0
    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const/4 v4, -0x1

    .line 2129351
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-object v1, p0, LX/EW7;->a:LX/EWC;

    iget-object v1, v1, LX/EWC;->C:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 2129352
    iput-wide v2, v0, LX/EWC;->r:J

    .line 2129353
    iput p2, p0, LX/EW7;->d:I

    .line 2129354
    iput p3, p0, LX/EW7;->e:I

    .line 2129355
    iget v0, p0, LX/EW7;->b:I

    if-ne v0, v4, :cond_1

    iget v0, p0, LX/EW7;->d:I

    :goto_0
    iput v0, p0, LX/EW7;->b:I

    .line 2129356
    iget v0, p0, LX/EW7;->c:I

    if-ne v0, v4, :cond_2

    iget v0, p0, LX/EW7;->e:I

    :goto_1
    iput v0, p0, LX/EW7;->c:I

    .line 2129357
    invoke-direct {p0}, LX/EW7;->b()V

    .line 2129358
    invoke-direct {p0}, LX/EW7;->c()V

    .line 2129359
    iget v0, p0, LX/EW7;->d:I

    iput v0, p0, LX/EW7;->b:I

    .line 2129360
    iget v0, p0, LX/EW7;->e:I

    iput v0, p0, LX/EW7;->c:I

    .line 2129361
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->N:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 2129362
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->N:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 2129363
    :cond_0
    return-void

    .line 2129364
    :cond_1
    iget v0, p0, LX/EW7;->b:I

    goto :goto_0

    .line 2129365
    :cond_2
    iget v0, p0, LX/EW7;->c:I

    goto :goto_1
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 2129366
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    invoke-static {v0, p2}, LX/EWC;->b(LX/EWC;I)V

    .line 2129367
    iput p2, p0, LX/EW7;->f:I

    .line 2129368
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    .line 2129369
    iput p2, v0, LX/EWC;->q:I

    .line 2129370
    invoke-direct {p0}, LX/EW7;->a()V

    .line 2129371
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->N:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 2129372
    iget-object v0, p0, LX/EW7;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->N:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 2129373
    :cond_0
    return-void
.end method
