.class public final LX/EGN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:LX/EGe;


# direct methods
.method public constructor <init>(LX/EGe;ZZ)V
    .locals 0

    .prologue
    .line 2097165
    iput-object p1, p0, LX/EGN;->c:LX/EGe;

    iput-boolean p2, p0, LX/EGN;->a:Z

    iput-boolean p3, p0, LX/EGN;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 5

    .prologue
    .line 2097166
    iget-boolean v0, p0, LX/EGN;->a:Z

    if-nez v0, :cond_0

    .line 2097167
    iget-object v0, p0, LX/EGN;->c:LX/EGe;

    invoke-static {v0}, LX/EGe;->av(LX/EGe;)V

    .line 2097168
    :cond_0
    iget-object v0, p0, LX/EGN;->c:LX/EGe;

    iget-object v0, v0, LX/EGe;->D:LX/EI9;

    if-nez v0, :cond_2

    .line 2097169
    :cond_1
    :goto_0
    return-void

    .line 2097170
    :cond_2
    iget-boolean v0, p0, LX/EGN;->b:Z

    if-eqz v0, :cond_3

    .line 2097171
    iget-object v0, p0, LX/EGN;->c:LX/EGe;

    iget-object v0, v0, LX/EGe;->D:LX/EI9;

    iget-boolean v1, p0, LX/EGN;->b:Z

    const/4 v2, 0x1

    iget-object v3, p0, LX/EGN;->c:LX/EGe;

    invoke-static {v3}, LX/EGe;->W(LX/EGe;)Z

    move-result v3

    iget-object v4, p0, LX/EGN;->c:LX/EGe;

    invoke-static {v4}, LX/EGe;->V(LX/EGe;)Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/EI9;->a(ZZZZ)V

    .line 2097172
    :cond_3
    iget-boolean v0, p0, LX/EGN;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/EGN;->c:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-eqz v0, :cond_1

    .line 2097173
    iget-object v0, p0, LX/EGN;->c:LX/EGe;

    iget-object v0, v0, LX/EGe;->W:LX/EHn;

    iget-object v1, p0, LX/EGN;->c:LX/EGe;

    invoke-virtual {v1}, LX/EGe;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021783

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EHn;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2097174
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2097175
    iget-object v0, p0, LX/EGN;->c:LX/EGe;

    invoke-static {v0}, LX/EGe;->ah(LX/EGe;)V

    .line 2097176
    iget-boolean v0, p0, LX/EGN;->a:Z

    if-eqz v0, :cond_0

    .line 2097177
    iget-object v0, p0, LX/EGN;->c:LX/EGe;

    invoke-static {v0}, LX/EGe;->av(LX/EGe;)V

    .line 2097178
    :goto_0
    return-void

    .line 2097179
    :cond_0
    iget-object v0, p0, LX/EGN;->c:LX/EGe;

    iget-object v0, v0, LX/EGe;->W:LX/EHn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EHn;->setBackgroundResource(I)V

    goto :goto_0
.end method
