.class public LX/En7;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/3AL;


# instance fields
.field private final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/EoJ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/En6",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2166682
    new-instance v0, LX/3AL;

    const/4 v1, 0x6

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/3AL;-><init>(II)V

    sput-object v0, LX/En7;->a:LX/3AL;

    return-void
.end method

.method public constructor <init>(LX/0ja;LX/Enm;LX/EoO;)V
    .locals 6
    .param p1    # LX/0ja;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Enm;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2166654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166655
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2166656
    invoke-virtual {p3}, LX/EoO;->b()LX/0P1;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2166657
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 2166658
    invoke-virtual {p3}, LX/EoO;->a()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EoJ;

    .line 2166659
    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2166660
    invoke-interface {v0}, LX/EoJ;->a()Ljava/lang/Class;

    move-result-object v0

    sget-object v4, LX/En7;->a:LX/3AL;

    sget-object v5, LX/0ja;->c:LX/3AM;

    invoke-virtual {p1, v0, v4, v5}, LX/0ja;->a(Ljava/lang/Class;LX/3AL;LX/3AM;)V

    goto :goto_0

    .line 2166661
    :cond_0
    invoke-interface {p2}, LX/Enm;->b()LX/0P1;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2166662
    invoke-interface {p2}, LX/Enm;->a()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EoJ;

    .line 2166663
    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2166664
    invoke-interface {v0}, LX/EoJ;->a()Ljava/lang/Class;

    move-result-object v0

    sget-object v4, LX/En7;->a:LX/3AL;

    sget-object v5, LX/0ja;->c:LX/3AM;

    invoke-virtual {p1, v0, v4, v5}, LX/0ja;->a(Ljava/lang/Class;LX/3AL;LX/3AM;)V

    goto :goto_1

    .line 2166665
    :cond_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/En7;->c:LX/0P1;

    .line 2166666
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/En7;->b:LX/0Rf;

    .line 2166667
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/Emj;Landroid/os/Bundle;)LX/Emg;
    .locals 8

    .prologue
    .line 2166675
    const/4 v1, 0x0

    .line 2166676
    iget-object v0, p0, LX/En7;->c:LX/0P1;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/En6;

    .line 2166677
    if-eqz v0, :cond_1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    .line 2166678
    invoke-virtual/range {v0 .. v7}, LX/En6;->a(Ljava/lang/Object;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/Emj;Landroid/os/Bundle;)LX/Emg;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emg;

    .line 2166679
    :goto_0
    if-nez v0, :cond_0

    .line 2166680
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EntityCardsController does not recognize a presenter for model "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2166681
    :cond_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)LX/EoJ;
    .locals 3

    .prologue
    .line 2166668
    const/4 v1, 0x0

    .line 2166669
    iget-object v0, p0, LX/En7;->c:LX/0P1;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/En6;

    .line 2166670
    if-eqz v0, :cond_1

    .line 2166671
    invoke-virtual {v0, p1}, LX/En6;->a(Ljava/lang/Object;)LX/EoJ;

    move-result-object v0

    .line 2166672
    :goto_0
    if-nez v0, :cond_0

    .line 2166673
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EntityCardsController does not recognize a type for model "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2166674
    :cond_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
