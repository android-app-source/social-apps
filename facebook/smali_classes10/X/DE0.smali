.class public LX/DE0;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DE1;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DE0",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DE1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976187
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1976188
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DE0;->b:LX/0Zi;

    .line 1976189
    iput-object p1, p0, LX/DE0;->a:LX/0Ot;

    .line 1976190
    return-void
.end method

.method public static a(LX/0QB;)LX/DE0;
    .locals 4

    .prologue
    .line 1976191
    const-class v1, LX/DE0;

    monitor-enter v1

    .line 1976192
    :try_start_0
    sget-object v0, LX/DE0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976193
    sput-object v2, LX/DE0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976194
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976195
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976196
    new-instance v3, LX/DE0;

    const/16 p0, 0x1faf

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DE0;-><init>(LX/0Ot;)V

    .line 1976197
    move-object v0, v3

    .line 1976198
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976199
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DE0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976200
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976201
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 11

    .prologue
    .line 1976202
    check-cast p1, LX/DDy;

    .line 1976203
    iget-object v0, p0, LX/DE0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DE1;

    iget-object v1, p1, LX/DDy;->a:LX/DEI;

    iget-object v2, p1, LX/DDy;->b:LX/1Po;

    .line 1976204
    iget-object v3, v1, LX/DEI;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    iget-object v4, v1, LX/DEI;->a:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v5

    invoke-interface {v5}, LX/1PT;->a()LX/1Qt;

    move-result-object v5

    iget-object v6, v0, LX/DE1;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/17W;

    iget-object v7, v0, LX/DE1;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    iget-object v8, v0, LX/DE1;->c:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/17Q;

    iget-object v9, v0, LX/DE1;->d:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0Zb;

    iget-boolean v10, v0, LX/DE1;->f:Z

    invoke-static/range {v3 .. v10}, LX/DES;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;LX/1Qt;LX/17W;Landroid/content/Context;LX/17Q;LX/0Zb;Z)V

    .line 1976205
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1976206
    const v0, 0x63c7d41f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1976207
    iget-object v0, p0, LX/DE0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DE1;

    const/4 p2, 0x3

    const/4 p0, 0x0

    const/4 v4, 0x2

    .line 1976208
    iget-object v1, v0, LX/DE1;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1vg;

    invoke-virtual {v1, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v1

    const v2, 0x7f020973

    invoke-virtual {v1, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v1

    const v2, -0x6e685d

    invoke-virtual {v1, v2}, LX/2xv;->i(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    .line 1976209
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b0f54

    invoke-interface {v1, v2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b0f50

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    .line 1976210
    const v2, 0x63c7d41f

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1976211
    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, p0}, LX/1Di;->y(I)LX/1Di;

    move-result-object v1

    const v2, 0x7f081864

    invoke-interface {v1, v2}, LX/1Di;->A(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, p0, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v4, v4}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1, p2, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1976212
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1976213
    invoke-static {}, LX/1dS;->b()V

    .line 1976214
    iget v0, p1, LX/1dQ;->b:I

    .line 1976215
    packed-switch v0, :pswitch_data_0

    .line 1976216
    :goto_0
    return-object v1

    .line 1976217
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/DE0;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x63c7d41f
        :pswitch_0
    .end packed-switch
.end method
