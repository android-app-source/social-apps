.class public final LX/CkR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/Ckc;

.field public b:F

.field public c:I

.field public d:Ljava/lang/String;

.field public e:LX/Ckf;

.field public f:F

.field public g:I

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:LX/CkQ;

.field public k:Z

.field public l:LX/CkQ;


# direct methods
.method public constructor <init>(LX/15i;I)V
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/16 v5, 0xd

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1930953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930954
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CkN;->a(Ljava/lang/String;)LX/CkN;

    move-result-object v3

    .line 1930955
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CkN;->a(Ljava/lang/String;)LX/CkN;

    move-result-object v4

    .line 1930956
    invoke-virtual {p1, p2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/CkR;->i:Z

    .line 1930957
    invoke-virtual {p1, p2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, LX/CkR;->k:Z

    .line 1930958
    iget-object v0, v3, LX/CkN;->a:Ljava/lang/String;

    invoke-static {v0}, LX/Ckc;->fromString(Ljava/lang/String;)LX/Ckc;

    move-result-object v0

    iput-object v0, p0, LX/CkR;->a:LX/Ckc;

    .line 1930959
    iget-object v0, v3, LX/CkN;->c:Ljava/lang/String;

    invoke-static {v0}, LX/CkY;->a(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, LX/CkR;->b:F

    .line 1930960
    iget-object v0, p0, LX/CkR;->a:LX/Ckc;

    invoke-static {v0}, LX/Ckc;->hasElementArgument(LX/Ckc;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1930961
    iget-object v0, v3, LX/CkN;->b:Ljava/lang/String;

    iput-object v0, p0, LX/CkR;->d:Ljava/lang/String;

    .line 1930962
    :cond_1
    :goto_1
    iget-object v0, v4, LX/CkN;->a:Ljava/lang/String;

    invoke-static {v0}, LX/Ckf;->fromString(Ljava/lang/String;)LX/Ckf;

    move-result-object v0

    iput-object v0, p0, LX/CkR;->e:LX/Ckf;

    .line 1930963
    iget-object v0, v4, LX/CkN;->c:Ljava/lang/String;

    invoke-static {v0}, LX/CkY;->a(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, LX/CkR;->f:F

    .line 1930964
    iget-object v0, p0, LX/CkR;->e:LX/Ckf;

    invoke-static {v0}, LX/Ckf;->hasElementArgument(LX/Ckf;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1930965
    iget-object v0, v4, LX/CkN;->b:Ljava/lang/String;

    iput-object v0, p0, LX/CkR;->h:Ljava/lang/String;

    .line 1930966
    :cond_2
    :goto_2
    iget-boolean v0, p0, LX/CkR;->i:Z

    if-eqz v0, :cond_3

    .line 1930967
    invoke-virtual {p1, p2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CkN;->a(Ljava/lang/String;)LX/CkN;

    move-result-object v0

    .line 1930968
    new-instance v1, LX/CkQ;

    iget-object v2, v0, LX/CkN;->a:Ljava/lang/String;

    invoke-static {v2}, LX/CkZ;->fromString(Ljava/lang/String;)LX/CkZ;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/CkQ;-><init>(LX/CkZ;LX/CkN;)V

    iput-object v1, p0, LX/CkR;->j:LX/CkQ;

    .line 1930969
    :cond_3
    iget-boolean v0, p0, LX/CkR;->k:Z

    if-eqz v0, :cond_4

    .line 1930970
    invoke-virtual {p1, p2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CkN;->a(Ljava/lang/String;)LX/CkN;

    move-result-object v0

    .line 1930971
    new-instance v1, LX/CkQ;

    iget-object v2, v0, LX/CkN;->a:Ljava/lang/String;

    invoke-static {v2}, LX/CkZ;->fromString(Ljava/lang/String;)LX/CkZ;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/CkQ;-><init>(LX/CkZ;LX/CkN;)V

    iput-object v1, p0, LX/CkR;->l:LX/CkQ;

    .line 1930972
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 1930973
    goto :goto_0

    .line 1930974
    :cond_6
    iget-object v0, p0, LX/CkR;->a:LX/Ckc;

    invoke-static {v0}, LX/Ckc;->hasGridArgument(LX/Ckc;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1930975
    iget-object v0, v3, LX/CkN;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/CkR;->c:I

    goto :goto_1

    .line 1930976
    :cond_7
    iget-object v0, p0, LX/CkR;->e:LX/Ckf;

    invoke-static {v0}, LX/Ckf;->hasGridArgument(LX/Ckf;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1930977
    iget-object v0, v4, LX/CkN;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/CkR;->g:I

    goto :goto_2
.end method

.method public constructor <init>(Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1930978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930979
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CkN;->a(Ljava/lang/String;)LX/CkN;

    move-result-object v3

    .line 1930980
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CkN;->a(Ljava/lang/String;)LX/CkN;

    move-result-object v4

    .line 1930981
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/CkR;->i:Z

    .line 1930982
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    :goto_1
    iput-boolean v1, p0, LX/CkR;->k:Z

    .line 1930983
    iget-object v0, v3, LX/CkN;->a:Ljava/lang/String;

    invoke-static {v0}, LX/Ckc;->fromString(Ljava/lang/String;)LX/Ckc;

    move-result-object v0

    iput-object v0, p0, LX/CkR;->a:LX/Ckc;

    .line 1930984
    iget-object v0, v3, LX/CkN;->c:Ljava/lang/String;

    invoke-static {v0}, LX/CkY;->a(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, LX/CkR;->b:F

    .line 1930985
    iget-object v0, p0, LX/CkR;->a:LX/Ckc;

    invoke-static {v0}, LX/Ckc;->hasElementArgument(LX/Ckc;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1930986
    iget-object v0, v3, LX/CkN;->b:Ljava/lang/String;

    iput-object v0, p0, LX/CkR;->d:Ljava/lang/String;

    .line 1930987
    :cond_0
    :goto_2
    iget-object v0, v4, LX/CkN;->a:Ljava/lang/String;

    invoke-static {v0}, LX/Ckf;->fromString(Ljava/lang/String;)LX/Ckf;

    move-result-object v0

    iput-object v0, p0, LX/CkR;->e:LX/Ckf;

    .line 1930988
    iget-object v0, v4, LX/CkN;->c:Ljava/lang/String;

    invoke-static {v0}, LX/CkY;->a(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, LX/CkR;->f:F

    .line 1930989
    iget-object v0, p0, LX/CkR;->e:LX/Ckf;

    invoke-static {v0}, LX/Ckf;->hasElementArgument(LX/Ckf;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1930990
    iget-object v0, v4, LX/CkN;->b:Ljava/lang/String;

    iput-object v0, p0, LX/CkR;->h:Ljava/lang/String;

    .line 1930991
    :cond_1
    :goto_3
    iget-boolean v0, p0, LX/CkR;->i:Z

    if-eqz v0, :cond_2

    .line 1930992
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CkN;->a(Ljava/lang/String;)LX/CkN;

    move-result-object v0

    .line 1930993
    new-instance v1, LX/CkQ;

    iget-object v2, v0, LX/CkN;->a:Ljava/lang/String;

    invoke-static {v2}, LX/CkZ;->fromString(Ljava/lang/String;)LX/CkZ;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/CkQ;-><init>(LX/CkZ;LX/CkN;)V

    iput-object v1, p0, LX/CkR;->j:LX/CkQ;

    .line 1930994
    :cond_2
    iget-boolean v0, p0, LX/CkR;->k:Z

    if-eqz v0, :cond_3

    .line 1930995
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CkN;->a(Ljava/lang/String;)LX/CkN;

    move-result-object v0

    .line 1930996
    new-instance v1, LX/CkQ;

    iget-object v2, v0, LX/CkN;->a:Ljava/lang/String;

    invoke-static {v2}, LX/CkZ;->fromString(Ljava/lang/String;)LX/CkZ;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/CkQ;-><init>(LX/CkZ;LX/CkN;)V

    iput-object v1, p0, LX/CkR;->l:LX/CkQ;

    .line 1930997
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 1930998
    goto :goto_0

    :cond_5
    move v1, v2

    .line 1930999
    goto :goto_1

    .line 1931000
    :cond_6
    iget-object v0, p0, LX/CkR;->a:LX/Ckc;

    invoke-static {v0}, LX/Ckc;->hasGridArgument(LX/Ckc;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1931001
    iget-object v0, v3, LX/CkN;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/CkR;->c:I

    goto :goto_2

    .line 1931002
    :cond_7
    iget-object v0, p0, LX/CkR;->e:LX/Ckf;

    invoke-static {v0}, LX/Ckf;->hasGridArgument(LX/Ckf;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1931003
    iget-object v0, v4, LX/CkN;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/CkR;->g:I

    goto :goto_3
.end method
