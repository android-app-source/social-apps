.class public LX/Ed5;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "LX/Ecx;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2148355
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "mmsc"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mmsproxy"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mmsport"

    aput-object v2, v0, v1

    sput-object v0, LX/Ed5;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2148356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2148357
    iput-object p1, p0, LX/Ed5;->b:Landroid/content/Context;

    .line 2148358
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Ed5;->c:Landroid/util/SparseArray;

    .line 2148359
    return-void
.end method

.method private static a(LX/Ed5;Landroid/net/Uri;ZLjava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 2148314
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loading APNs from system, checkCurrent="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " apnName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148315
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2148316
    if-eqz p2, :cond_0

    .line 2148317
    const-string v0, "current IS NOT NULL"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148318
    :cond_0
    invoke-static {p3}, LX/Ed5;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2148319
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2148320
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 2148321
    const-string v0, " AND "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148322
    :cond_1
    const-string v0, "apn=?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148323
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v7, v4, v0

    .line 2148324
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/Ed5;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, LX/Ed5;->a:[Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2148325
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_4

    .line 2148326
    :cond_2
    if-eqz v0, :cond_3

    .line 2148327
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2148328
    :cond_3
    const-string v1, "MmsLib"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Query "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " with apn "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " and "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_5

    const-string v0, "checking CURRENT"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " returned empty"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 2148329
    :cond_4
    :goto_2
    return-object v0

    .line 2148330
    :cond_5
    const-string v0, "not checking CURRENT"
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 2148331
    :catch_0
    move-exception v0

    .line 2148332
    const-string v1, "MmsLib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "APN table query exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 2148333
    goto :goto_2

    .line 2148334
    :catch_1
    move-exception v0

    .line 2148335
    const-string v1, "MmsLib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Platform restricts APN table access: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2148336
    throw v0

    :cond_6
    move-object v4, v6

    goto/16 :goto_0
.end method

.method public static b(LX/Ed5;ILjava/lang/String;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/Ecx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2148337
    invoke-static {}, LX/EdL;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 2148338
    sget-object v0, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/subId/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 2148339
    :goto_0
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, v1, v0, p2}, LX/Ed5;->a(LX/Ed5;Landroid/net/Uri;ZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2148340
    if-nez v0, :cond_0

    .line 2148341
    const/4 v0, 0x0

    invoke-static {p0, v1, v0, p2}, LX/Ed5;->a(LX/Ed5;Landroid/net/Uri;ZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2148342
    if-nez v0, :cond_0

    .line 2148343
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/Ed5;->a(LX/Ed5;Landroid/net/Uri;ZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2148344
    if-nez v0, :cond_0

    .line 2148345
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/Ed5;->a(LX/Ed5;Landroid/net/Uri;ZLjava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2148346
    :cond_0
    if-nez v0, :cond_2

    .line 2148347
    :goto_1
    return-void

    .line 2148348
    :cond_1
    sget-object v0, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    move-object v1, v0

    goto :goto_0

    .line 2148349
    :cond_2
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2148350
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/Ed3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Ed3;

    move-result-object v1

    .line 2148351
    if-eqz v1, :cond_3

    .line 2148352
    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2148353
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1

    .line 2148354
    :catch_0
    goto :goto_1
.end method

.method public static c(LX/Ed5;ILjava/lang/String;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/Ecx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2148298
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loading APNs from resources, apnName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148299
    iget-object v0, p0, LX/Ed5;->b:Landroid/content/Context;

    invoke-static {v0, p1}, LX/EdL;->a(Landroid/content/Context;I)[I

    move-result-object v0

    .line 2148300
    const/4 v1, 0x0

    aget v1, v0, v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    aget v1, v0, v1

    if-nez v1, :cond_1

    .line 2148301
    const-string v0, "MmsLib"

    const-string v1, "Can not get valid mcc/mnc from system"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2148302
    :cond_0
    :goto_0
    return-void

    .line 2148303
    :cond_1
    const/4 v1, 0x0

    .line 2148304
    :try_start_0
    iget-object v2, p0, LX/Ed5;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f060000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 2148305
    new-instance v2, LX/Ecz;

    new-instance v3, LX/Ed2;

    invoke-direct {v3, p0, v0, p2, p3}, LX/Ed2;-><init>(LX/Ed5;[ILjava/lang/String;Ljava/util/List;)V

    invoke-direct {v2, v1, v3}, LX/Ecz;-><init>(Lorg/xmlpull/v1/XmlPullParser;LX/Ed2;)V

    invoke-virtual {v2}, LX/Ecy;->c()V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2148306
    if-eqz v1, :cond_0

    .line 2148307
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_0

    .line 2148308
    :catch_0
    move-exception v0

    .line 2148309
    :try_start_1
    const-string v2, "MmsLib"

    const-string v3, "Can not get apns.xml "

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2148310
    if-eqz v1, :cond_0

    .line 2148311
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_0

    .line 2148312
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 2148313
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_2
    throw v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2148297
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    .line 2148283
    if-nez p0, :cond_1

    .line 2148284
    const/4 p0, 0x0

    .line 2148285
    :cond_0
    :goto_0
    return-object p0

    .line 2148286
    :cond_1
    const-string v0, "\\."

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2148287
    array-length v0, v1

    if-ne v0, v5, :cond_0

    .line 2148288
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v0, 0x10

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2148289
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_3

    .line 2148290
    :try_start_0
    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v3, v4, :cond_0

    .line 2148291
    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2148292
    if-ge v0, v4, :cond_2

    .line 2148293
    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2148294
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2148295
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 2148296
    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/Ecx;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2148263
    const/4 v0, -0x1

    invoke-static {v0}, LX/EdL;->b(I)I

    move-result v4

    .line 2148264
    monitor-enter p0

    .line 2148265
    :try_start_0
    iget-object v0, p0, LX/Ed5;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2148266
    if-nez v0, :cond_3

    .line 2148267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2148268
    iget-object v1, p0, LX/Ed5;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2148269
    invoke-static {p0, v4, p1, v0}, LX/Ed5;->b(LX/Ed5;ILjava/lang/String;Ljava/util/List;)V

    .line 2148270
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 2148271
    :cond_0
    :goto_0
    move v1, v2

    .line 2148272
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2148273
    if-eqz v1, :cond_1

    .line 2148274
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Loaded "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " APNs"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2148275
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2148276
    iget-object v1, p0, LX/Ed5;->b:Landroid/content/Context;

    invoke-static {v1, v4}, LX/EdL;->a(Landroid/content/Context;I)[I

    move-result-object v1

    .line 2148277
    const-string v4, "MmsLib"

    const-string v5, "Failed to load APN for %s. MCC: %d MNC: %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v3

    aget v3, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    const/4 v3, 0x2

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v3

    invoke-static {v4, v5, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2148278
    :cond_2
    return-object v0

    .line 2148279
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move v1, v3

    goto :goto_1

    .line 2148280
    :cond_4
    invoke-static {p0, v4, p1, v0}, LX/Ed5;->c(LX/Ed5;ILjava/lang/String;Ljava/util/List;)V

    .line 2148281
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 2148282
    const/4 v1, 0x0

    invoke-static {p0, v4, v1, v0}, LX/Ed5;->c(LX/Ed5;ILjava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method
