.class public final LX/EwH;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 0

    .prologue
    .line 2183042
    iput-object p1, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2183043
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    const-string v1, "FETCH_REQUESTS"

    invoke-static {v0, p1, v1}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->a$redex0(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2183044
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 2183045
    check-cast p1, LX/0Px;

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 2183046
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->f()V

    .line 2183047
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->B:Z

    if-nez v0, :cond_0

    .line 2183048
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    const/4 v1, 0x1

    .line 2183049
    iput-boolean v1, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->B:Z

    .line 2183050
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->A:LX/Eun;

    if-eqz v0, :cond_0

    .line 2183051
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->A:LX/Eun;

    invoke-virtual {v0}, LX/Eun;->a()V

    .line 2183052
    :cond_0
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    if-eqz v0, :cond_5

    .line 2183053
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->O:LX/2kW;

    .line 2183054
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 2183055
    invoke-interface {v0}, LX/2kM;->b()LX/2nj;

    move-result-object v0

    .line 2183056
    iget-boolean v1, v0, LX/2nj;->d:Z

    move v0, v1

    .line 2183057
    :goto_0
    iget-object v1, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aj:LX/EuY;

    invoke-virtual {v1}, LX/EuY;->a()Z

    move-result v1

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    .line 2183058
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    invoke-virtual {v0, v6}, LX/EwG;->a(Z)V

    .line 2183059
    :cond_1
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2183060
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2183061
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    move v5, v6

    :goto_1
    if-ge v5, v7, :cond_8

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;

    .line 2183062
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;->j()Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;

    move-result-object v4

    .line 2183063
    invoke-virtual {v4}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2183064
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 2183065
    iget-object v1, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2183066
    invoke-virtual {v4}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v10

    .line 2183067
    invoke-virtual {v4}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->n()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_6

    invoke-virtual {v4}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->n()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 2183068
    :goto_2
    new-instance v1, LX/Ewh;

    invoke-direct {v1}, LX/Ewh;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v11

    .line 2183069
    iput-object v11, v1, LX/Euq;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2183070
    move-object v1, v1

    .line 2183071
    check-cast v1, LX/Ewh;

    .line 2183072
    iput-wide v8, v1, LX/Euq;->a:J

    .line 2183073
    move-object v1, v1

    .line 2183074
    check-cast v1, LX/Ewh;

    invoke-virtual {v4}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendsCenterRequestNodeModel;->l()Ljava/lang/String;

    move-result-object v4

    .line 2183075
    iput-object v4, v1, LX/Euq;->d:Ljava/lang/String;

    .line 2183076
    move-object v1, v1

    .line 2183077
    check-cast v1, LX/Ewh;

    if-eqz v10, :cond_7

    invoke-virtual {v10}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2183078
    :goto_3
    iput-object v4, v1, LX/Euq;->c:Ljava/lang/String;

    .line 2183079
    move-object v1, v1

    .line 2183080
    check-cast v1, LX/Ewh;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;->k()LX/0Px;

    move-result-object v10

    .line 2183081
    invoke-virtual {v10}, LX/0Px;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_9

    const/4 v11, 0x0

    :goto_4
    move-object v11, v11

    .line 2183082
    move-object v4, v11

    .line 2183083
    iput-object v4, v1, LX/Eur;->c:Ljava/lang/String;

    .line 2183084
    move-object v1, v1

    .line 2183085
    check-cast v1, LX/Ewh;

    sget-object v4, LX/2h7;->FRIENDS_CENTER_REQUESTS:LX/2h7;

    .line 2183086
    iput-object v4, v1, LX/Euq;->f:LX/2h7;

    .line 2183087
    move-object v1, v1

    .line 2183088
    check-cast v1, LX/Ewh;

    .line 2183089
    iput-object v2, v1, LX/Eur;->d:Ljava/lang/String;

    .line 2183090
    move-object v1, v1

    .line 2183091
    check-cast v1, LX/Ewh;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;->a()Z

    move-result v0

    .line 2183092
    iput-boolean v0, v1, LX/Eur;->b:Z

    .line 2183093
    move-object v0, v1

    .line 2183094
    check-cast v0, LX/Ewh;

    invoke-virtual {v0}, LX/Ewh;->c()LX/Ewi;

    move-result-object v0

    .line 2183095
    iget-object v1, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2183096
    iget-object v1, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->x:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2183097
    :cond_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_1

    .line 2183098
    :cond_5
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    invoke-virtual {v0}, LX/Eui;->a()Z

    move-result v0

    goto/16 :goto_0

    :cond_6
    move-object v2, v3

    .line 2183099
    goto :goto_2

    :cond_7
    move-object v4, v3

    .line 2183100
    goto :goto_3

    .line 2183101
    :cond_8
    iget-object v0, p0, LX/EwH;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-static {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183102
    return-void

    :cond_9
    const/4 v11, 0x0

    invoke-virtual {v10, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel$SuggestersModel;

    invoke-virtual {v11}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel$SuggestersModel;->a()Ljava/lang/String;

    move-result-object v11

    goto :goto_4
.end method
