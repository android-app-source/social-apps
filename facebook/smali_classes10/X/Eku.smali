.class public final LX/Eku;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Ekr;

.field public final b:LX/Ekn;

.field public c:LX/ElY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/Eko;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ekr;)V
    .locals 2

    .prologue
    .line 2163833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163834
    if-nez p1, :cond_0

    .line 2163835
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "crudoNet cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163836
    :cond_0
    iput-object p1, p0, LX/Eku;->a:LX/Ekr;

    .line 2163837
    new-instance v0, LX/Ekn;

    invoke-direct {v0}, LX/Ekn;-><init>()V

    iput-object v0, p0, LX/Eku;->b:LX/Ekn;

    .line 2163838
    return-void
.end method

.method public constructor <init>(LX/Ekx;)V
    .locals 2

    .prologue
    .line 2163840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163841
    iget-object v0, p1, LX/Ekx;->a:LX/Ekr;

    iput-object v0, p0, LX/Eku;->a:LX/Ekr;

    .line 2163842
    new-instance v0, LX/Ekn;

    iget-object v1, p1, LX/Ekx;->b:LX/Ekl;

    invoke-direct {v0, v1}, LX/Ekn;-><init>(LX/Ekl;)V

    iput-object v0, p0, LX/Eku;->b:LX/Ekn;

    .line 2163843
    iget-object v0, p1, LX/Ekx;->d:LX/ElY;

    iput-object v0, p0, LX/Eku;->c:LX/ElY;

    .line 2163844
    iget-object v0, p1, LX/Ekx;->e:LX/Eko;

    iput-object v0, p0, LX/Eku;->d:LX/Eko;

    .line 2163845
    return-void
.end method


# virtual methods
.method public final b()LX/Ekx;
    .locals 2

    .prologue
    .line 2163839
    new-instance v0, LX/Ekx;

    invoke-direct {v0, p0}, LX/Ekx;-><init>(LX/Eku;)V

    return-object v0
.end method
