.class public LX/E1y;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E1w;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E1y;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E1z;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2072036
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E1y;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E1z;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072033
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2072034
    iput-object p1, p0, LX/E1y;->b:LX/0Ot;

    .line 2072035
    return-void
.end method

.method public static a(LX/0QB;)LX/E1y;
    .locals 4

    .prologue
    .line 2072020
    sget-object v0, LX/E1y;->c:LX/E1y;

    if-nez v0, :cond_1

    .line 2072021
    const-class v1, LX/E1y;

    monitor-enter v1

    .line 2072022
    :try_start_0
    sget-object v0, LX/E1y;->c:LX/E1y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072023
    if-eqz v2, :cond_0

    .line 2072024
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072025
    new-instance v3, LX/E1y;

    const/16 p0, 0x30b1

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E1y;-><init>(LX/0Ot;)V

    .line 2072026
    move-object v0, v3

    .line 2072027
    sput-object v0, LX/E1y;->c:LX/E1y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072028
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072029
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072030
    :cond_1
    sget-object v0, LX/E1y;->c:LX/E1y;

    return-object v0

    .line 2072031
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2071986
    check-cast p2, LX/E1x;

    .line 2071987
    iget-object v0, p0, LX/E1y;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p2, LX/E1x;->a:LX/1X1;

    iget-object v2, p2, LX/E1x;->b:Ljava/lang/String;

    iget-object v3, p2, LX/E1x;->c:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    iget-object v4, p2, LX/E1x;->d:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    iget-object v5, p2, LX/E1x;->e:Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-object v0, p1

    const/4 p2, 0x3

    const/4 p1, 0x2

    const/4 p0, 0x1

    const/4 v8, 0x0

    .line 2071988
    invoke-static {v0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v1}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v6

    .line 2071989
    if-eqz v5, :cond_0

    .line 2071990
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v7

    invoke-static {v7}, LX/CgW;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;)I

    move-result v7

    invoke-interface {v6, p0, v7}, LX/1Dh;->u(II)LX/1Dh;

    .line 2071991
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v7

    invoke-static {v7}, LX/CgW;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;)I

    move-result v7

    invoke-interface {v6, p1, v7}, LX/1Dh;->u(II)LX/1Dh;

    .line 2071992
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v7

    invoke-static {v7}, LX/CgW;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;)I

    move-result v7

    invoke-interface {v6, p2, v7}, LX/1Dh;->u(II)LX/1Dh;

    .line 2071993
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v7

    invoke-static {v7}, LX/CgW;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;)I

    move-result v7

    invoke-interface {v6, v8, v7}, LX/1Dh;->u(II)LX/1Dh;

    .line 2071994
    :cond_0
    if-eqz v4, :cond_1

    .line 2071995
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v7

    invoke-static {v7}, LX/E1z;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;)I

    move-result v7

    invoke-interface {v6, p0, v7}, LX/1Dh;->q(II)LX/1Dh;

    .line 2071996
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v7

    invoke-static {v7}, LX/E1z;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;)I

    move-result v7

    invoke-interface {v6, p1, v7}, LX/1Dh;->q(II)LX/1Dh;

    .line 2071997
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v7

    invoke-static {v7}, LX/E1z;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;)I

    move-result v7

    invoke-interface {v6, p2, v7}, LX/1Dh;->q(II)LX/1Dh;

    .line 2071998
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;

    move-result-object v7

    invoke-static {v7}, LX/E1z;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentMargin;)I

    move-result v7

    invoke-interface {v6, v8, v7}, LX/1Dh;->q(II)LX/1Dh;

    .line 2071999
    :cond_1
    if-eqz v3, :cond_3

    .line 2072000
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->b()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->d()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->c()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;->a()Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_2
    const/4 v7, 0x1

    :goto_0
    move v7, v7

    .line 2072001
    if-eqz v7, :cond_3

    .line 2072002
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v7

    invoke-virtual {v0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v8, v3}, LX/E1z;->a(Landroid/content/res/Resources;Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    .line 2072003
    :cond_3
    invoke-static {v0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    .line 2072004
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 2072005
    :try_start_0
    new-instance v8, Ljava/lang/StringBuilder;

    const-string p0, "#"

    invoke-direct {v8, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v7, v8}, LX/1Dh;->W(I)LX/1Dh;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2072006
    :goto_1
    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2072007
    return-object v0

    .line 2072008
    :catch_0
    const v8, 0x7f01072b

    invoke-interface {v7, v8}, LX/1Dh;->U(I)LX/1Dh;

    goto :goto_1

    .line 2072009
    :cond_4
    const v8, 0x7f01072b

    invoke-interface {v7, v8}, LX/1Dh;->U(I)LX/1Dh;

    goto :goto_1

    :cond_5
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2072018
    invoke-static {}, LX/1dS;->b()V

    .line 2072019
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/E1w;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2072010
    new-instance v1, LX/E1x;

    invoke-direct {v1, p0}, LX/E1x;-><init>(LX/E1y;)V

    .line 2072011
    sget-object v2, LX/E1y;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E1w;

    .line 2072012
    if-nez v2, :cond_0

    .line 2072013
    new-instance v2, LX/E1w;

    invoke-direct {v2}, LX/E1w;-><init>()V

    .line 2072014
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/E1w;->a$redex0(LX/E1w;LX/1De;IILX/E1x;)V

    .line 2072015
    move-object v1, v2

    .line 2072016
    move-object v0, v1

    .line 2072017
    return-object v0
.end method
