.class public final LX/E3R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Pn;

.field public final synthetic b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;LX/1Pn;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2074579
    iput-object p1, p0, LX/E3R;->c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionExpandableUnitComponentPromptPartDefinition;

    iput-object p2, p0, LX/E3R;->a:LX/1Pn;

    iput-object p3, p0, LX/E3R;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const v0, 0x7431d5d1

    invoke-static {v7, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2074580
    iget-object v0, p0, LX/E3R;->a:LX/1Pn;

    check-cast v0, LX/1Pr;

    new-instance v2, LX/E2Z;

    iget-object v3, p0, LX/E3R;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074581
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v3, v4

    .line 2074582
    invoke-direct {v2, v3}, LX/E2Z;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/E3R;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v2, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2a;

    .line 2074583
    iput-boolean v6, v0, LX/E2a;->a:Z

    .line 2074584
    iget-object v0, p0, LX/E3R;->a:LX/1Pn;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    iget-object v2, p0, LX/E3R;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074585
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2074586
    iget-object v3, p0, LX/E3R;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074587
    iget-object v4, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2074588
    iget-object v4, p0, LX/E3R;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074589
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v5

    .line 2074590
    invoke-interface {v4}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/Cfc;->INLINE_EXPANSION_TAP:LX/Cfc;

    invoke-virtual {v0, v2, v3, v4, v5}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    .line 2074591
    iget-object v0, p0, LX/E3R;->a:LX/1Pn;

    check-cast v0, LX/1Pq;

    new-array v2, v6, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    iget-object v4, p0, LX/E3R;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2074592
    const v0, 0x60e18705    # 1.3000771E20f

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
