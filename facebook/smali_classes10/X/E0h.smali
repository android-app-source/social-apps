.class public final enum LX/E0h;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/E0h;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/E0h;

.field public static final enum HOME_CITY_CHANGE_DENIED_ERROR:LX/E0h;

.field public static final enum HOME_FETCH_ERROR:LX/E0h;

.field public static final enum HOME_NAME_CHANGE_DENIED_ERROR:LX/E0h;

.field public static final enum HOME_UPDATE_ERROR:LX/E0h;

.field public static final enum NO_ERROR:LX/E0h;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2068777
    new-instance v0, LX/E0h;

    const-string v1, "NO_ERROR"

    invoke-direct {v0, v1, v2}, LX/E0h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0h;->NO_ERROR:LX/E0h;

    .line 2068778
    new-instance v0, LX/E0h;

    const-string v1, "HOME_FETCH_ERROR"

    invoke-direct {v0, v1, v3}, LX/E0h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0h;->HOME_FETCH_ERROR:LX/E0h;

    .line 2068779
    new-instance v0, LX/E0h;

    const-string v1, "HOME_NAME_CHANGE_DENIED_ERROR"

    invoke-direct {v0, v1, v4}, LX/E0h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0h;->HOME_NAME_CHANGE_DENIED_ERROR:LX/E0h;

    .line 2068780
    new-instance v0, LX/E0h;

    const-string v1, "HOME_CITY_CHANGE_DENIED_ERROR"

    invoke-direct {v0, v1, v5}, LX/E0h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0h;->HOME_CITY_CHANGE_DENIED_ERROR:LX/E0h;

    .line 2068781
    new-instance v0, LX/E0h;

    const-string v1, "HOME_UPDATE_ERROR"

    invoke-direct {v0, v1, v6}, LX/E0h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0h;->HOME_UPDATE_ERROR:LX/E0h;

    .line 2068782
    const/4 v0, 0x5

    new-array v0, v0, [LX/E0h;

    sget-object v1, LX/E0h;->NO_ERROR:LX/E0h;

    aput-object v1, v0, v2

    sget-object v1, LX/E0h;->HOME_FETCH_ERROR:LX/E0h;

    aput-object v1, v0, v3

    sget-object v1, LX/E0h;->HOME_NAME_CHANGE_DENIED_ERROR:LX/E0h;

    aput-object v1, v0, v4

    sget-object v1, LX/E0h;->HOME_CITY_CHANGE_DENIED_ERROR:LX/E0h;

    aput-object v1, v0, v5

    sget-object v1, LX/E0h;->HOME_UPDATE_ERROR:LX/E0h;

    aput-object v1, v0, v6

    sput-object v0, LX/E0h;->$VALUES:[LX/E0h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2068783
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/E0h;
    .locals 1

    .prologue
    .line 2068784
    const-class v0, LX/E0h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/E0h;

    return-object v0
.end method

.method public static values()[LX/E0h;
    .locals 1

    .prologue
    .line 2068785
    sget-object v0, LX/E0h;->$VALUES:[LX/E0h;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/E0h;

    return-object v0
.end method
