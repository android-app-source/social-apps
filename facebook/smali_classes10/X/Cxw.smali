.class public LX/Cxw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cxh;


# instance fields
.field private final a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field private final b:LX/CzA;

.field private final c:LX/CvY;

.field private final d:LX/2Sc;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;LX/CvY;LX/2Sc;)V
    .locals 0
    .param p1    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/CzA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952323
    iput-object p1, p0, LX/Cxw;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1952324
    iput-object p2, p0, LX/Cxw;->b:LX/CzA;

    .line 1952325
    iput-object p3, p0, LX/Cxw;->c:LX/CvY;

    .line 1952326
    iput-object p4, p0, LX/Cxw;->d:LX/2Sc;

    .line 1952327
    return-void
.end method


# virtual methods
.method public final a(LX/CzL;LX/0P1;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<-",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node$ModuleResults$Edges$EdgesNode;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1952313
    invoke-virtual {p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v7

    .line 1952314
    const/4 v8, 0x0

    .line 1952315
    invoke-virtual {p1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v0

    .line 1952316
    instance-of v1, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    if-eqz v1, :cond_0

    .line 1952317
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v8

    .line 1952318
    :goto_0
    iget-object v10, p0, LX/Cxw;->c:LX/CvY;

    iget-object v11, p0, LX/Cxw;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    sget-object v12, LX/8ch;->CLICK:LX/8ch;

    invoke-virtual {p1}, LX/CzL;->d()I

    move-result v13

    iget-object v0, p0, LX/Cxw;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ax()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ae()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Cxw;->b:LX/CzA;

    new-instance v4, LX/Cz3;

    invoke-virtual {p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v5

    invoke-direct {v4, v5}, LX/Cz3;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V

    invoke-virtual {v3, v4}, LX/CzA;->a(LX/Cyv;)I

    move-result v3

    invoke-virtual {p1}, LX/CzL;->d()I

    move-result v4

    invoke-virtual {p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v5

    invoke-static {v5}, LX/8eM;->j(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v5

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->X()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-lez v6, :cond_1

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->X()LX/0Px;

    move-result-object v6

    const/4 v9, 0x0

    invoke-virtual {v6, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_1
    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v7

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v9, p2

    invoke-static/range {v0 .. v9}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v0, v10

    move-object v1, v11

    move-object v2, v12

    move v3, v13

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952319
    return-void

    .line 1952320
    :cond_0
    iget-object v0, p0, LX/Cxw;->d:LX/2Sc;

    sget-object v1, LX/3Ql;->UNABLE_TO_LOG_ITEM_NAVIGATION:LX/3Ql;

    const-string v2, "Unable to cast item to EdgesNode for navigation logging"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->b(LX/3Ql;Ljava/lang/String;)V

    goto :goto_0

    .line 1952321
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)V
    .locals 2

    .prologue
    .line 1952328
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 1952329
    if-eqz v0, :cond_0

    .line 1952330
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Logging item navigation for a SearchResultsCollectionUnitItem is not supported in SearchResultsEnvironment"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1952331
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 12

    .prologue
    .line 1952298
    if-nez p1, :cond_0

    .line 1952299
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 1952300
    if-eqz v0, :cond_2

    .line 1952301
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Logging item navigation to a null story"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1952302
    :cond_0
    const/4 v1, 0x0

    .line 1952303
    iget-object v0, p0, LX/Cxw;->b:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->e()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cz3;

    .line 1952304
    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    .line 1952305
    if-eqz v0, :cond_1

    .line 1952306
    invoke-static {v0}, LX/8eM;->k(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1952307
    if-eqz v3, :cond_1

    .line 1952308
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    .line 1952309
    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v4, v0

    .line 1952310
    :goto_0
    if-eqz v4, :cond_2

    .line 1952311
    iget-object v7, p0, LX/Cxw;->c:LX/CvY;

    iget-object v8, p0, LX/Cxw;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    sget-object v9, LX/8ch;->CLICK:LX/8ch;

    iget-object v0, p0, LX/Cxw;->b:LX/CzA;

    invoke-virtual {v0, v4}, LX/CzA;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v10

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v11

    iget-object v0, p0, LX/Cxw;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->k()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ae()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, LX/Cxw;->b:LX/CzA;

    invoke-virtual {v5, v4}, LX/CzA;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;ILjava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v0, v7

    move-object v1, v8

    move-object v2, v9

    move v3, v10

    move-object v4, v11

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952312
    :cond_2
    return-void

    :cond_3
    move-object v4, v1

    goto :goto_0
.end method

.method public final c(LX/CzL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<-",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node$ModuleResults$Edges$EdgesNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1952295
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1952296
    invoke-virtual {p0, p1, v0}, LX/Cxw;->a(LX/CzL;LX/0P1;)V

    .line 1952297
    return-void
.end method
