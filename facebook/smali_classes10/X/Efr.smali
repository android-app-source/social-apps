.class public final LX/Efr;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;)V
    .locals 0

    .prologue
    .line 2155426
    iput-object p1, p0, LX/Efr;->a:Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 1

    .prologue
    .line 2155427
    iget-object v0, p0, LX/Efr;->a:Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    .line 2155428
    iput p1, v0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->e:I

    .line 2155429
    return-void
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 2155430
    iget-object v0, p0, LX/Efr;->a:Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    iget-object v0, v0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->g:LX/Efq;

    .line 2155431
    const/4 p0, 0x0

    cmpl-float p0, p2, p0

    if-nez p0, :cond_1

    .line 2155432
    iget-object p0, v0, LX/Efq;->e:Landroid/util/SparseArray;

    invoke-virtual {p0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Efp;

    .line 2155433
    if-eqz p0, :cond_0

    .line 2155434
    invoke-virtual {p0}, LX/Efp;->a()V

    .line 2155435
    :cond_0
    :goto_0
    return-void

    .line 2155436
    :cond_1
    iget-object p0, v0, LX/Efq;->e:Landroid/util/SparseArray;

    invoke-virtual {p0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Efp;

    .line 2155437
    if-eqz p0, :cond_2

    .line 2155438
    invoke-virtual {p0}, LX/Efp;->b()V

    .line 2155439
    :cond_2
    add-int/lit8 p0, p1, 0x1

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result p3

    if-ge p0, p3, :cond_0

    .line 2155440
    iget-object p0, v0, LX/Efq;->e:Landroid/util/SparseArray;

    add-int/lit8 p3, p1, 0x1

    invoke-virtual {p0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Efp;

    .line 2155441
    if-eqz p0, :cond_0

    .line 2155442
    invoke-virtual {p0}, LX/Efp;->b()V

    goto :goto_0
.end method
