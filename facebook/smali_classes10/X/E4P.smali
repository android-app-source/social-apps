.class public LX/E4P;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E4P;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E4R;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E4P",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E4R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2076611
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2076612
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/E4P;->b:LX/0Zi;

    .line 2076613
    iput-object p1, p0, LX/E4P;->a:LX/0Ot;

    .line 2076614
    return-void
.end method

.method public static a(LX/0QB;)LX/E4P;
    .locals 4

    .prologue
    .line 2076624
    sget-object v0, LX/E4P;->c:LX/E4P;

    if-nez v0, :cond_1

    .line 2076625
    const-class v1, LX/E4P;

    monitor-enter v1

    .line 2076626
    :try_start_0
    sget-object v0, LX/E4P;->c:LX/E4P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2076627
    if-eqz v2, :cond_0

    .line 2076628
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2076629
    new-instance v3, LX/E4P;

    const/16 p0, 0x310b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E4P;-><init>(LX/0Ot;)V

    .line 2076630
    move-object v0, v3

    .line 2076631
    sput-object v0, LX/E4P;->c:LX/E4P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2076632
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2076633
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2076634
    :cond_1
    sget-object v0, LX/E4P;->c:LX/E4P;

    return-object v0

    .line 2076635
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2076636
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2076637
    check-cast p2, LX/E4O;

    .line 2076638
    iget-object v0, p0, LX/E4P;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/E4O;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v1, p2, LX/E4O;->b:LX/E2S;

    const/4 v4, 0x2

    .line 2076639
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f02079a

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b163d

    invoke-interface {v2, v4, v3}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b163a

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x7

    const v4, 0x7f0b163c

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const/4 v4, 0x0

    .line 2076640
    iget-object p0, v1, LX/E2S;->a:LX/03R;

    move-object p0, p0

    .line 2076641
    sget-object p2, LX/03R;->YES:LX/03R;

    if-ne p0, p2, :cond_1

    .line 2076642
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->b()LX/174;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->b()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 2076643
    :cond_0
    :goto_0
    move-object v4, v4

    .line 2076644
    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a09f4

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004d

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    .line 2076645
    const v3, -0x14443588

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2076646
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2076647
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2076615
    invoke-static {}, LX/1dS;->b()V

    .line 2076616
    iget v0, p1, LX/1dQ;->b:I

    .line 2076617
    packed-switch v0, :pswitch_data_0

    .line 2076618
    :goto_0
    return-object v2

    .line 2076619
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2076620
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2076621
    check-cast v1, LX/E4O;

    .line 2076622
    iget-object v3, p0, LX/E4P;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/E4R;

    iget-object v4, v1, LX/E4O;->c:LX/1Pq;

    iget-object v5, v1, LX/E4O;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object p1, v1, LX/E4O;->b:LX/E2S;

    iget-object p2, v1, LX/E4O;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v3, v4, v5, p1, p2}, LX/E4R;->a(LX/1Pq;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/E2S;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2076623
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x14443588
        :pswitch_0
    .end packed-switch
.end method
