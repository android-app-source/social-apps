.class public final LX/DeI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/font/FontLoader;

.field private final b:LX/DeE;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/font/FontLoader;LX/DeE;)V
    .locals 0

    .prologue
    .line 2021262
    iput-object p1, p0, LX/DeI;->a:Lcom/facebook/messaging/font/FontLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2021263
    iput-object p2, p0, LX/DeI;->b:LX/DeE;

    .line 2021264
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2021265
    iget-object v0, p0, LX/DeI;->a:Lcom/facebook/messaging/font/FontLoader;

    iget-object v0, v0, Lcom/facebook/messaging/font/FontLoader;->e:LX/DeD;

    iget-object v1, p0, LX/DeI;->b:LX/DeE;

    invoke-virtual {v0, v1, p1}, LX/2Vx;->a(LX/2WG;Ljava/io/InputStream;)LX/1gI;

    move-result-object v0

    check-cast v0, LX/1gH;

    .line 2021266
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 2021267
    iget-object v1, v0, LX/1gH;->a:Ljava/io/File;

    move-object v0, v1

    .line 2021268
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2021269
    :try_start_0
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/io/File;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2021270
    const/4 v1, 0x1

    .line 2021271
    :goto_0
    move v1, v1

    .line 2021272
    if-nez v1, :cond_1

    .line 2021273
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid font file: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/DeI;->b:LX/DeE;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2021274
    iget-object v1, p0, LX/DeI;->a:Lcom/facebook/messaging/font/FontLoader;

    iget-object v1, v1, Lcom/facebook/messaging/font/FontLoader;->f:LX/03V;

    sget-object v2, Lcom/facebook/messaging/font/FontLoader;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2021275
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 2021276
    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    :catch_0
    const/4 v1, 0x0

    goto :goto_0
.end method
