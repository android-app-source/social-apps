.class public final LX/ExZ;
.super LX/BcN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcN",
        "<",
        "LX/Exc;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Exa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Exc",
            "<TTUserInfo;>.FriendsCenterSuggestionsSectionImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Exc;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Exc;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2184975
    iput-object p1, p0, LX/ExZ;->b:LX/Exc;

    invoke-direct {p0}, LX/BcN;-><init>()V

    .line 2184976
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "connectionControllerLease"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/ExZ;->c:[Ljava/lang/String;

    .line 2184977
    iput v3, p0, LX/ExZ;->d:I

    .line 2184978
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/ExZ;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/ExZ;->e:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2184979
    invoke-super {p0}, LX/BcN;->a()V

    .line 2184980
    const/4 v0, 0x0

    iput-object v0, p0, LX/ExZ;->a:LX/Exa;

    .line 2184981
    iget-object v0, p0, LX/ExZ;->b:LX/Exc;

    iget-object v0, v0, LX/Exc;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2184982
    return-void
.end method

.method public final b()LX/BcO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/BcO",
            "<",
            "LX/Exc;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2184983
    iget-object v1, p0, LX/ExZ;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/ExZ;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/ExZ;->d:I

    if-ge v1, v2, :cond_2

    .line 2184984
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2184985
    :goto_0
    iget v2, p0, LX/ExZ;->d:I

    if-ge v0, v2, :cond_1

    .line 2184986
    iget-object v2, p0, LX/ExZ;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2184987
    iget-object v2, p0, LX/ExZ;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2184988
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2184989
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2184990
    :cond_2
    iget-object v0, p0, LX/ExZ;->a:LX/Exa;

    .line 2184991
    invoke-virtual {p0}, LX/ExZ;->a()V

    .line 2184992
    return-object v0
.end method

.method public final b(LX/BcQ;)LX/ExZ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcQ",
            "<",
            "LX/BcM;",
            ">;)",
            "LX/Exc",
            "<TTUserInfo;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2184993
    invoke-super {p0, p1}, LX/BcN;->a(LX/BcQ;)V

    .line 2184994
    return-object p0
.end method
