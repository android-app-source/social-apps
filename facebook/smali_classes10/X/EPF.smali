.class public LX/EPF;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/support/v4/app/FragmentActivity;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 2116319
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_BEHIND_THE_SCENE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CELEBRITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CELEBRITIES_MENTION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_COMMENTARY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const/16 v6, 0x10

    new-array v6, v6, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const/4 v7, 0x0

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_EXPERIENTIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_FEATURED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_GOVERNMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_HOW_TO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_LOCATION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_MINUTIAE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RECENT_TOP:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/4 v7, 0x7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RECIPES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/16 v7, 0x8

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RELATED_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/16 v7, 0x9

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_REVIEWS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/16 v7, 0xa

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_REVIEWS_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/16 v7, 0xb

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/16 v7, 0xc

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/16 v7, 0xd

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/16 v7, 0xe

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_VITAL_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    const/16 v7, 0xf

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_SET:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/EPF;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2116320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2116321
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116322
    iput-object v0, p0, LX/EPF;->b:LX/0Ot;

    .line 2116323
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116324
    iput-object v0, p0, LX/EPF;->c:LX/0Ot;

    .line 2116325
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116326
    iput-object v0, p0, LX/EPF;->d:LX/0Ot;

    .line 2116327
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116328
    iput-object v0, p0, LX/EPF;->e:LX/0Ot;

    .line 2116329
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116330
    iput-object v0, p0, LX/EPF;->f:LX/0Ot;

    .line 2116331
    return-void
.end method
