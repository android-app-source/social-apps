.class public final LX/DMa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DMe;


# direct methods
.method public constructor <init>(LX/DMe;)V
    .locals 0

    .prologue
    .line 1989983
    iput-object p1, p0, LX/DMa;->a:LX/DMe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1989984
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1989985
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1989986
    if-nez p1, :cond_0

    .line 1989987
    :goto_0
    return-void

    .line 1989988
    :cond_0
    iget-object v0, p0, LX/DMa;->a:LX/DMe;

    iget-object v1, v0, LX/DMe;->i:LX/DMT;

    .line 1989989
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1989990
    check-cast v0, LX/7oa;

    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 1989991
    iget-object v2, v1, LX/DMT;->a:Lcom/facebook/groups/events/GroupEventsBaseFragment;

    iget-object v2, v2, Lcom/facebook/groups/events/GroupEventsBaseFragment;->j:LX/DMJ;

    .line 1989992
    if-nez v0, :cond_1

    .line 1989993
    :goto_1
    goto :goto_0

    .line 1989994
    :cond_1
    new-instance p0, LX/0Pz;

    invoke-direct {p0}, LX/0Pz;-><init>()V

    .line 1989995
    iget-object v3, v2, LX/DMJ;->j:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result p1

    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, p1, :cond_3

    iget-object v3, v2, LX/DMJ;->j:LX/0Px;

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/events/model/Event;

    .line 1989996
    invoke-virtual {v3, v0}, Lcom/facebook/events/model/Event;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1989997
    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1989998
    :goto_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 1989999
    :cond_2
    invoke-virtual {p0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1990000
    :cond_3
    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    iput-object v3, v2, LX/DMJ;->j:LX/0Px;

    .line 1990001
    const v3, -0x62dbc94d

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_1
.end method
