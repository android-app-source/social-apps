.class public LX/Dcd;
.super LX/Dcc;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Dcd;


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:LX/0tX;

.field private final c:LX/DvI;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/DvI;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018627
    invoke-direct {p0}, LX/Dcc;-><init>()V

    .line 2018628
    iput-object p1, p0, LX/Dcd;->a:Ljava/util/concurrent/ExecutorService;

    .line 2018629
    iput-object p2, p0, LX/Dcd;->b:LX/0tX;

    .line 2018630
    iput-object p3, p0, LX/Dcd;->c:LX/DvI;

    .line 2018631
    return-void
.end method

.method public static a(LX/0QB;)LX/Dcd;
    .locals 6

    .prologue
    .line 2018632
    sget-object v0, LX/Dcd;->d:LX/Dcd;

    if-nez v0, :cond_1

    .line 2018633
    const-class v1, LX/Dcd;

    monitor-enter v1

    .line 2018634
    :try_start_0
    sget-object v0, LX/Dcd;->d:LX/Dcd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2018635
    if-eqz v2, :cond_0

    .line 2018636
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2018637
    new-instance p0, LX/Dcd;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/DvI;->a(LX/0QB;)LX/DvI;

    move-result-object v5

    check-cast v5, LX/DvI;

    invoke-direct {p0, v3, v4, v5}, LX/Dcd;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/DvI;)V

    .line 2018638
    move-object v0, p0

    .line 2018639
    sput-object v0, LX/Dcd;->d:LX/Dcd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2018640
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2018641
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2018642
    :cond_1
    sget-object v0, LX/Dcd;->d:LX/Dcd;

    return-object v0

    .line 2018643
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2018644
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;",
            "IZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2018645
    new-instance v0, LX/8AM;

    invoke-direct {v0}, LX/8AM;-><init>()V

    move-object v2, v0

    .line 2018646
    check-cast p3, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;

    .line 2018647
    iget-object v0, p3, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->d:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    move-object v3, v0

    .line 2018648
    if-eqz v3, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-virtual {v0, v3}, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2018649
    :goto_0
    const-string v4, "page_id"

    .line 2018650
    iget-object v1, p3, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->a:Ljava/lang/String;

    move-object v5, v1

    .line 2018651
    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "category"

    .line 2018652
    iget-object v1, p3, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;->c:Ljava/lang/String;

    move-object v6, v1

    .line 2018653
    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "entry_point"

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->name()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v4, v5, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v3, "beforeCursor"

    invoke-virtual {v0, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v3, "afterCursor"

    invoke-virtual {v0, v3, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v3, "count"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2018654
    iget-object v0, p0, LX/Dcd;->c:LX/DvI;

    invoke-virtual {v0, v2}, LX/DvI;->a(LX/0gW;)LX/0gW;

    .line 2018655
    iget-object v3, p0, LX/Dcd;->b:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    if-eqz p5, :cond_2

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_2
    invoke-virtual {v2, v0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2018656
    new-instance v2, LX/Dcb;

    invoke-direct {v2}, LX/Dcb;-><init>()V

    iget-object v1, p0, LX/Dcd;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 2018657
    goto :goto_0

    .line 2018658
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2018659
    :cond_2
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_2
.end method
