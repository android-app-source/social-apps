.class public LX/CsG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:LX/Chi;

.field public final c:LX/Ckw;

.field public final d:LX/CoV;

.field private final e:LX/03V;

.field public final f:Landroid/content/Context;

.field public final g:Landroid/text/style/BackgroundColorSpan;

.field private final h:Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;

.field private final i:F

.field private final j:I

.field public k:LX/Csm;

.field private l:F

.field private m:F

.field public n:Z

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:Z

.field public r:Z

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/widget/TextView;LX/Chi;LX/Ckw;LX/CoV;LX/03V;)V
    .locals 3
    .param p1    # Landroid/widget/TextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1942359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1942360
    iput-object p1, p0, LX/CsG;->a:Landroid/widget/TextView;

    .line 1942361
    iget-object v0, p0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/CsG;->f:Landroid/content/Context;

    .line 1942362
    iput-object p2, p0, LX/CsG;->b:LX/Chi;

    .line 1942363
    iput-object p3, p0, LX/CsG;->c:LX/Ckw;

    .line 1942364
    iput-object p4, p0, LX/CsG;->d:LX/CoV;

    .line 1942365
    iput-object p5, p0, LX/CsG;->e:LX/03V;

    .line 1942366
    iget-object v0, p0, LX/CsG;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3f99999a    # 1.2f

    div-float/2addr v0, v1

    iput v0, p0, LX/CsG;->i:F

    .line 1942367
    const/4 v0, 0x1

    const/high16 v1, 0x40a00000    # 5.0f

    iget-object v2, p0, LX/CsG;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/CsG;->j:I

    .line 1942368
    new-instance v0, Landroid/text/style/BackgroundColorSpan;

    iget-object v1, p0, LX/CsG;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a061f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    iput-object v0, p0, LX/CsG;->g:Landroid/text/style/BackgroundColorSpan;

    .line 1942369
    new-instance v0, Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;

    invoke-direct {v0, p0}, Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;-><init>(LX/CsG;)V

    iput-object v0, p0, LX/CsG;->h:Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;

    .line 1942370
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1942411
    iget-object v0, p0, LX/CsG;->a:Landroid/widget/TextView;

    iget-object v1, p0, LX/CsG;->h:Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1942412
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CsG;->p:Z

    .line 1942413
    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    .line 1942414
    iget-boolean v1, v0, LX/0ht;->r:Z

    move v0, v1

    .line 1942415
    if-eqz v0, :cond_0

    .line 1942416
    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1942417
    :cond_0
    return-void
.end method

.method public static c(LX/CsG;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1942418
    new-instance v0, LX/Csm;

    iget-object v1, p0, LX/CsG;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Csm;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/CsG;->k:LX/Csm;

    .line 1942419
    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    new-instance v1, LX/CsD;

    invoke-direct {v1, p0}, LX/CsD;-><init>(LX/CsG;)V

    .line 1942420
    iget-object v2, v0, LX/Csm;->m:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1942421
    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    new-instance v1, LX/CsE;

    invoke-direct {v1, p0}, LX/CsE;-><init>(LX/CsG;)V

    .line 1942422
    iget-object v2, v0, LX/Csm;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1942423
    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    new-instance v1, LX/CsF;

    invoke-direct {v1, p0}, LX/CsF;-><init>(LX/CsG;)V

    .line 1942424
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 1942425
    iget-boolean v0, p0, LX/CsG;->n:Z

    if-eqz v0, :cond_0

    .line 1942426
    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    iget-object v1, p0, LX/CsG;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081c5f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Csm;->a(Ljava/lang/String;)V

    .line 1942427
    iget v0, p0, LX/CsG;->m:F

    float-to-int v0, v0

    .line 1942428
    iget-object v1, p0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1942429
    iget-object v1, p0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getScrollY()I

    move-result v1

    add-int/2addr v0, v1

    .line 1942430
    iget-object v1, p0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1942431
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v0

    .line 1942432
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineTop(I)I

    move-result v0

    iget v1, p0, LX/CsG;->j:I

    add-int/2addr v0, v1

    .line 1942433
    iget-object v1, p0, LX/CsG;->k:LX/Csm;

    iget v2, p0, LX/CsG;->l:F

    float-to-int v2, v2

    iget-boolean v3, p0, LX/CsG;->n:Z

    invoke-virtual {v1, v2, v0, v3}, LX/Csm;->a(IIZ)V

    .line 1942434
    :goto_0
    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    iget-object v1, p0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1942435
    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1942436
    iget-object v0, p0, LX/CsG;->c:LX/Ckw;

    const-string v1, "long_pressed_text"

    invoke-virtual {v0, v1}, LX/Ckw;->a(Ljava/lang/String;)V

    .line 1942437
    iput-boolean v4, p0, LX/CsG;->r:Z

    iput-boolean v4, p0, LX/CsG;->q:Z

    .line 1942438
    return-void

    .line 1942439
    :cond_0
    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    iget-object v1, p0, LX/CsG;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081c5e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Csm;->a(Ljava/lang/String;)V

    .line 1942440
    iget-object v0, p0, LX/CsG;->k:LX/Csm;

    iget v1, p0, LX/CsG;->l:F

    float-to-int v1, v1

    iget-boolean v2, p0, LX/CsG;->n:Z

    invoke-virtual {v0, v1, v4, v2}, LX/Csm;->a(IIZ)V

    .line 1942441
    iget-object v0, p0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableString;

    .line 1942442
    iget-object v1, p0, LX/CsG;->g:Landroid/text/style/BackgroundColorSpan;

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v5, 0x21

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1942443
    iget-object v1, p0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1942444
    goto :goto_0
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1942371
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1942372
    packed-switch v0, :pswitch_data_0

    .line 1942373
    :cond_0
    :goto_0
    iget-object v0, p0, LX/CsG;->a:Landroid/widget/TextView;

    iget-object v1, p0, LX/CsG;->h:Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_1
    move v0, v2

    .line 1942374
    :goto_2
    return v0

    .line 1942375
    :pswitch_0
    iput-boolean v2, p0, LX/CsG;->n:Z

    .line 1942376
    iput-boolean v2, p0, LX/CsG;->s:Z

    .line 1942377
    const/4 v0, 0x0

    iput-object v0, p0, LX/CsG;->o:Ljava/lang/String;

    .line 1942378
    iget-object v0, p0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    instance-of v0, v0, LX/Csn;

    if-eqz v0, :cond_2

    .line 1942379
    :try_start_0
    iget-object v3, p0, LX/CsG;->a:Landroid/widget/TextView;

    iget-object v0, p0, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    invoke-static {v3, v0, p2}, LX/Csn;->a(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)LX/Cll;

    move-result-object v0

    .line 1942380
    if-eqz v0, :cond_2

    instance-of v3, v0, LX/Clm;

    if-eqz v3, :cond_2

    .line 1942381
    const/4 v3, 0x1

    iput-boolean v3, p0, LX/CsG;->n:Z

    .line 1942382
    check-cast v0, LX/Clm;

    .line 1942383
    iget-object v3, v0, LX/Cll;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->F()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-object v3, v0, LX/Cll;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->F()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, v0, LX/Cll;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->F()Ljava/lang/String;

    move-result-object v3

    :goto_3
    move-object v0, v3

    .line 1942384
    iput-object v0, p0, LX/CsG;->o:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1942385
    :cond_2
    :goto_4
    iget-object v0, p0, LX/CsG;->h:Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;

    .line 1942386
    iget-object v3, v0, Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;->a:LX/CsG;

    iget-object v3, v3, LX/CsG;->a:Landroid/widget/TextView;

    instance-of v3, v3, LX/CtG;

    if-eqz v3, :cond_3

    .line 1942387
    iget-object v3, v0, Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;->a:LX/CsG;

    iget-object v3, v3, LX/CsG;->a:Landroid/widget/TextView;

    check-cast v3, LX/CtG;

    invoke-virtual {v3}, LX/CtG;->getWindowAttachmentCount()I

    move-result v3

    iput v3, v0, Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;->b:I

    .line 1942388
    :cond_3
    iget-object v0, p0, LX/CsG;->a:Landroid/widget/TextView;

    iget-object v3, p0, LX/CsG;->h:Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1942389
    iget-object v0, p0, LX/CsG;->a:Landroid/widget/TextView;

    iget-object v3, p0, LX/CsG;->h:Lcom/facebook/richdocument/view/widget/CopyPasteTouchHandler$CheckForExtendedLongPress;

    const-wide/16 v4, 0x2bc

    invoke-virtual {v0, v3, v4, v5}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1942390
    iput-boolean v1, p0, LX/CsG;->p:Z

    .line 1942391
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LX/CsG;->l:F

    .line 1942392
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LX/CsG;->m:F

    goto :goto_1

    .line 1942393
    :catch_0
    move-exception v0

    .line 1942394
    iget-object v3, p0, LX/CsG;->e:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error while attempting to detect text long-press for article: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, LX/CsG;->b:LX/Chi;

    .line 1942395
    iget-object p1, v6, LX/Chi;->c:Ljava/lang/String;

    move-object v6, p1

    .line 1942396
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v4

    .line 1942397
    iput-object v0, v4, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1942398
    move-object v0, v4

    .line 1942399
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_4

    .line 1942400
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, LX/CsG;->l:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1942401
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v3, p0, LX/CsG;->m:F

    sub-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1942402
    iget v3, p0, LX/CsG;->i:F

    cmpl-float v0, v0, v3

    if-gtz v0, :cond_4

    iget v0, p0, LX/CsG;->i:F

    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    .line 1942403
    :cond_4
    invoke-direct {p0}, LX/CsG;->b()V

    goto/16 :goto_1

    .line 1942404
    :pswitch_2
    iput-boolean v1, p0, LX/CsG;->s:Z

    .line 1942405
    iget-boolean v0, p0, LX/CsG;->p:Z

    if-eqz v0, :cond_6

    .line 1942406
    invoke-direct {p0}, LX/CsG;->b()V

    .line 1942407
    :cond_5
    :pswitch_3
    iget-boolean v0, p0, LX/CsG;->s:Z

    if-nez v0, :cond_0

    .line 1942408
    invoke-direct {p0}, LX/CsG;->b()V

    goto/16 :goto_0

    .line 1942409
    :cond_6
    iget-boolean v0, p0, LX/CsG;->n:Z

    if-eqz v0, :cond_5

    move v0, v1

    .line 1942410
    goto/16 :goto_2

    :cond_7
    iget-object v3, v0, LX/Cll;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;->G()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
