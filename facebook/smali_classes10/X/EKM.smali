.class public LX/EKM;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EKK;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EKN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2106508
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EKM;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EKN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106526
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2106527
    iput-object p1, p0, LX/EKM;->b:LX/0Ot;

    .line 2106528
    return-void
.end method

.method public static a(LX/0QB;)LX/EKM;
    .locals 4

    .prologue
    .line 2106529
    const-class v1, LX/EKM;

    monitor-enter v1

    .line 2106530
    :try_start_0
    sget-object v0, LX/EKM;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106531
    sput-object v2, LX/EKM;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106532
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106533
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106534
    new-instance v3, LX/EKM;

    const/16 p0, 0x33c7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EKM;-><init>(LX/0Ot;)V

    .line 2106535
    move-object v0, v3

    .line 2106536
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106537
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106538
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106539
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2106511
    check-cast p2, LX/EKL;

    .line 2106512
    iget-object v0, p0, LX/EKM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EKN;

    iget-object v1, p2, LX/EKL;->a:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    const/16 v10, 0x10

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v4, 0x0

    .line 2106513
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08229d

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->e()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2106514
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->c()Z

    move-result v6

    .line 2106515
    if-eqz v6, :cond_2

    const v2, 0x7f0a0098

    .line 2106516
    :goto_0
    sget-object v3, LX/EKU;->a:LX/0P1;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2106517
    if-eqz v3, :cond_5

    :goto_1
    move v3, v3

    .line 2106518
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v9}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v8}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0b172a

    invoke-interface {v7, v8}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0b172b

    invoke-interface {v7, v8}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v7

    if-eqz v6, :cond_3

    :goto_2
    invoke-interface {v7, v3}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v7, 0x7f0b0050

    invoke-virtual {v5, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v5}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    if-eqz v6, :cond_4

    const/16 v2, 0x16

    :goto_3
    invoke-interface {v5, v4, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    if-eqz v6, :cond_0

    const/4 v4, 0x6

    :cond_0
    invoke-interface {v2, v9, v4}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2106519
    if-eqz v6, :cond_1

    .line 2106520
    iget-object v3, v0, LX/EKN;->a:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v4, 0x7f0207dc

    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const v4, 0x7f0a0097

    invoke-virtual {v3, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 2106521
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v10}, LX/1Di;->j(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v10}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2106522
    :cond_1
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2106523
    return-object v0

    .line 2106524
    :cond_2
    const v2, 0x7f0a00ab

    goto/16 :goto_0

    .line 2106525
    :cond_3
    const/4 v3, -0x1

    goto :goto_2

    :cond_4
    move v2, v4

    goto :goto_3

    :cond_5
    const v3, -0x5c318f

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2106509
    invoke-static {}, LX/1dS;->b()V

    .line 2106510
    const/4 v0, 0x0

    return-object v0
.end method
