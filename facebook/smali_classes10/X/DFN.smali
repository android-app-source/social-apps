.class public LX/DFN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/DFE;

.field private final b:LX/1DR;

.field public final c:LX/0ad;

.field public final d:LX/2e2;

.field public final e:LX/3mL;

.field public final f:LX/1LV;


# direct methods
.method public constructor <init>(LX/DFE;LX/1DR;LX/0ad;LX/2e2;LX/3mL;LX/1LV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1978086
    iput-object p1, p0, LX/DFN;->a:LX/DFE;

    .line 1978087
    iput-object p2, p0, LX/DFN;->b:LX/1DR;

    .line 1978088
    iput-object p3, p0, LX/DFN;->c:LX/0ad;

    .line 1978089
    iput-object p4, p0, LX/DFN;->d:LX/2e2;

    .line 1978090
    iput-object p5, p0, LX/DFN;->e:LX/3mL;

    .line 1978091
    iput-object p6, p0, LX/DFN;->f:LX/1LV;

    .line 1978092
    return-void
.end method

.method public static a(LX/0QB;)LX/DFN;
    .locals 10

    .prologue
    .line 1978093
    const-class v1, LX/DFN;

    monitor-enter v1

    .line 1978094
    :try_start_0
    sget-object v0, LX/DFN;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978095
    sput-object v2, LX/DFN;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978096
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978097
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978098
    new-instance v3, LX/DFN;

    const-class v4, LX/DFE;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DFE;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v5

    check-cast v5, LX/1DR;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v0}, LX/2e2;->a(LX/0QB;)LX/2e2;

    move-result-object v7

    check-cast v7, LX/2e2;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v8

    check-cast v8, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v9

    check-cast v9, LX/1LV;

    invoke-direct/range {v3 .. v9}, LX/DFN;-><init>(LX/DFE;LX/1DR;LX/0ad;LX/2e2;LX/3mL;LX/1LV;)V

    .line 1978099
    move-object v0, v3

    .line 1978100
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978101
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978102
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978103
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
