.class public final LX/EL8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:I

.field public final e:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(IIILandroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2107986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2107987
    iput p1, p0, LX/EL8;->a:I

    .line 2107988
    iput p2, p0, LX/EL8;->b:I

    .line 2107989
    iput-object p4, p0, LX/EL8;->c:Landroid/view/View$OnClickListener;

    .line 2107990
    iput p3, p0, LX/EL8;->d:I

    .line 2107991
    iput-object v0, p0, LX/EL8;->e:Ljava/lang/Integer;

    .line 2107992
    iput-object v0, p0, LX/EL8;->f:Ljava/lang/Integer;

    .line 2107993
    return-void
.end method

.method public constructor <init>(IILandroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2107970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2107971
    iput p1, p0, LX/EL8;->a:I

    .line 2107972
    const v0, -0x6e685d

    iput v0, p0, LX/EL8;->b:I

    .line 2107973
    iput-object p3, p0, LX/EL8;->c:Landroid/view/View$OnClickListener;

    .line 2107974
    iput p2, p0, LX/EL8;->d:I

    .line 2107975
    iput-object v1, p0, LX/EL8;->e:Ljava/lang/Integer;

    .line 2107976
    iput-object v1, p0, LX/EL8;->f:Ljava/lang/Integer;

    .line 2107977
    return-void
.end method

.method public constructor <init>(IILandroid/view/View$OnClickListener;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 2107978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2107979
    iput p1, p0, LX/EL8;->a:I

    .line 2107980
    const v0, -0x6e685d

    iput v0, p0, LX/EL8;->b:I

    .line 2107981
    iput-object p3, p0, LX/EL8;->c:Landroid/view/View$OnClickListener;

    .line 2107982
    iput p2, p0, LX/EL8;->d:I

    .line 2107983
    iput-object p4, p0, LX/EL8;->e:Ljava/lang/Integer;

    .line 2107984
    iput-object p5, p0, LX/EL8;->f:Ljava/lang/Integer;

    .line 2107985
    return-void
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2107967
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 2107968
    :cond_0
    const/4 v0, 0x1

    .line 2107969
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2107963
    instance-of v1, p1, LX/EL8;

    if-nez v1, :cond_1

    .line 2107964
    :cond_0
    :goto_0
    return v0

    .line 2107965
    :cond_1
    check-cast p1, LX/EL8;

    .line 2107966
    iget v1, p0, LX/EL8;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, LX/EL8;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/EL8;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EL8;->c:Landroid/view/View$OnClickListener;

    iget-object v2, p1, LX/EL8;->c:Landroid/view/View$OnClickListener;

    invoke-static {v1, v2}, LX/EL8;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LX/EL8;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, LX/EL8;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/EL8;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2107962
    const/4 v0, 0x0

    return v0
.end method
