.class public LX/ESw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2123752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2123753
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionPaginatedSubComponents;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2123729
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2123730
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;

    .line 2123731
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2123732
    new-instance v5, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitSubComponentModel;

    move-result-object v0

    invoke-direct {v5, v0, p0, p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2123733
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2123734
    :cond_1
    return-object v2
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLInterfaces$ReactionUnitComponent;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2123754
    invoke-static {p2}, LX/Cfu;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)LX/0Px;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/ESw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2123755
    invoke-static {p2}, LX/Cfu;->a(LX/9uc;)LX/0Px;

    move-result-object v1

    invoke-static {p0, p1, v1}, LX/ESw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2123756
    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .param p2    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<+",
            "LX/9uc;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2123746
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2123747
    if-nez p2, :cond_0

    move-object v0, v1

    .line 2123748
    :goto_0
    return-object v0

    .line 2123749
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2123750
    new-instance v3, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-direct {v3, v0, p0, p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 2123751
    goto :goto_0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 5

    .prologue
    .line 2123735
    new-instance v1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-direct {v1, p2, p0, p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2123736
    invoke-static {p2}, LX/Cfu;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)LX/0Px;

    move-result-object v0

    .line 2123737
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2123738
    :goto_0
    return-object v0

    .line 2123739
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v2

    .line 2123740
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2123741
    new-instance v4, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-direct {v4, v0, p0, p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/ETQ;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    goto :goto_1

    .line 2123742
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->as()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedComponentFragmentModel$PaginatedComponentsModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->b()LX/0us;

    move-result-object v0

    .line 2123743
    iput-object v0, v1, Lcom/facebook/video/videohome/data/VideoHomeItem;->b:LX/0us;

    .line 2123744
    move-object v0, v1

    .line 2123745
    goto :goto_0
.end method
