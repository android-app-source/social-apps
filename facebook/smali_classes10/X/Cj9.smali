.class public LX/Cj9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Cj9;


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1929115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929116
    iput-object p1, p0, LX/Cj9;->a:LX/0tX;

    .line 1929117
    return-void
.end method

.method public static a(LX/0QB;)LX/Cj9;
    .locals 4

    .prologue
    .line 1929118
    sget-object v0, LX/Cj9;->b:LX/Cj9;

    if-nez v0, :cond_1

    .line 1929119
    const-class v1, LX/Cj9;

    monitor-enter v1

    .line 1929120
    :try_start_0
    sget-object v0, LX/Cj9;->b:LX/Cj9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1929121
    if-eqz v2, :cond_0

    .line 1929122
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1929123
    new-instance p0, LX/Cj9;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, LX/Cj9;-><init>(LX/0tX;)V

    .line 1929124
    move-object v0, p0

    .line 1929125
    sput-object v0, LX/Cj9;->b:LX/Cj9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1929126
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1929127
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1929128
    :cond_1
    sget-object v0, LX/Cj9;->b:LX/Cj9;

    return-object v0

    .line 1929129
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1929130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionViewedModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1929131
    new-instance v0, LX/4GR;

    invoke-direct {v0}, LX/4GR;-><init>()V

    .line 1929132
    const-string v1, "actor_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1929133
    invoke-virtual {v0, p2}, LX/4GR;->c(Ljava/lang/String;)LX/4GR;

    .line 1929134
    const-string v1, "INLINE_CTA"

    invoke-virtual {v0, v1}, LX/4GR;->d(Ljava/lang/String;)LX/4GR;

    .line 1929135
    invoke-virtual {v0, p3}, LX/4GR;->b(Ljava/lang/String;)LX/4GR;

    .line 1929136
    invoke-static {}, LX/8bB;->a()LX/8bA;

    move-result-object v1

    .line 1929137
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1929138
    iget-object v0, p0, LX/Cj9;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1929139
    new-instance v1, LX/Cj8;

    invoke-direct {v1, p0}, LX/Cj8;-><init>(LX/Cj9;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionAcceptedModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1929140
    new-instance v0, LX/4GQ;

    invoke-direct {v0}, LX/4GQ;-><init>()V

    .line 1929141
    const-string v1, "actor_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1929142
    invoke-virtual {v0, p2}, LX/4GQ;->c(Ljava/lang/String;)LX/4GQ;

    .line 1929143
    const-string v1, "INLINE_CTA"

    invoke-virtual {v0, v1}, LX/4GQ;->d(Ljava/lang/String;)LX/4GQ;

    .line 1929144
    invoke-virtual {v0, p3}, LX/4GQ;->b(Ljava/lang/String;)LX/4GQ;

    .line 1929145
    new-instance v1, LX/4K5;

    invoke-direct {v1}, LX/4K5;-><init>()V

    .line 1929146
    const-string v2, "email"

    invoke-virtual {v1, v2}, LX/4K5;->a(Ljava/lang/String;)LX/4K5;

    .line 1929147
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1929148
    invoke-interface {v2, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1929149
    invoke-virtual {v1, v2}, LX/4K5;->a(Ljava/util/List;)LX/4K5;

    .line 1929150
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1929151
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1929152
    invoke-virtual {v0, v2}, LX/4GQ;->a(Ljava/util/List;)LX/4GQ;

    .line 1929153
    invoke-static {}, LX/8bB;->b()LX/8b9;

    move-result-object v1

    .line 1929154
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1929155
    iget-object v0, p0, LX/Cj9;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1929156
    new-instance v1, LX/Cj7;

    invoke-direct {v1, p0}, LX/Cj7;-><init>(LX/Cj9;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
