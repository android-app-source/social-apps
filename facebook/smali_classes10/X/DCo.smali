.class public final LX/DCo;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public final synthetic c:LX/3mN;


# direct methods
.method public constructor <init>(LX/3mN;Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 0

    .prologue
    .line 1974552
    iput-object p1, p0, LX/DCo;->c:LX/3mN;

    iput-object p2, p0, LX/DCo;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    iput-object p3, p0, LX/DCo;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1974589
    iget-object v0, p0, LX/DCo;->c:LX/3mN;

    iget-object v0, v0, LX/3mN;->a:Ljava/util/Set;

    iget-object v1, p0, LX/DCo;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1974590
    iget-object v0, p0, LX/DCo;->c:LX/3mN;

    iget-object v0, v0, LX/3mN;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x640001

    const-string v2, "PaginatedGysjFeedUnitTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 1974591
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 1974553
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    .line 1974554
    iget-object v0, p0, LX/DCo;->c:LX/3mN;

    iget-object v0, v0, LX/3mN;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x640001

    const-string v2, "PaginatedGysjFeedUnitTTI"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 1974555
    iget-object v0, p0, LX/DCo;->c:LX/3mN;

    iget-object v0, v0, LX/3mN;->a:Ljava/util/Set;

    iget-object v1, p0, LX/DCo;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1974556
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1974557
    :goto_0
    return-void

    .line 1974558
    :cond_0
    iget-object v0, p0, LX/DCo;->c:LX/3mN;

    iget-object v0, v0, LX/3mN;->e:LX/189;

    iget-object v1, p0, LX/DCo;->a:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    .line 1974559
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1974560
    invoke-static {v1}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)LX/0Px;

    move-result-object v6

    invoke-virtual {v8, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1974561
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1974562
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v6, 0x0

    move v7, v6

    :goto_1
    if-ge v7, v10, :cond_2

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;

    .line 1974563
    invoke-static {v6}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsEdge;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1974564
    invoke-virtual {v8, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1974565
    :cond_1
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 1974566
    :cond_2
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1974567
    invoke-static {v1}, LX/4Xj;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;)LX/4Xj;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v8

    invoke-static {v8}, LX/4Xk;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;)LX/4Xk;

    move-result-object v8

    .line 1974568
    iput-object v6, v8, LX/4Xk;->b:LX/0Px;

    .line 1974569
    move-object v6, v8

    .line 1974570
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v8

    .line 1974571
    iput-object v8, v6, LX/4Xk;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1974572
    move-object v6, v6

    .line 1974573
    invoke-virtual {v6}, LX/4Xk;->a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    move-result-object v6

    .line 1974574
    iput-object v6, v7, LX/4Xj;->b:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnitGroupsConnection;

    .line 1974575
    move-object v6, v7

    .line 1974576
    iget-object v7, v0, LX/189;->i:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    .line 1974577
    iput-wide v8, v6, LX/4Xj;->e:J

    .line 1974578
    move-object v6, v6

    .line 1974579
    invoke-virtual {v6}, LX/4Xj;->a()Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    move-result-object v6

    .line 1974580
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->I_()I

    move-result v7

    invoke-static {v6, v7}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 1974581
    const/4 v7, 0x0

    invoke-static {v6, v7}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;LX/0Rf;)V

    .line 1974582
    move-object v1, v6

    .line 1974583
    const/4 v0, 0x0

    .line 1974584
    if-eqz v1, :cond_3

    .line 1974585
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1974586
    :cond_3
    iget-object v2, p0, LX/DCo;->c:LX/3mN;

    iget-object v2, v2, LX/3mN;->d:LX/03V;

    const-class v3, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsYouShouldJoinFeedUnit;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/DCo;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1974587
    iget-object v0, p0, LX/DCo;->c:LX/3mN;

    iget-object v0, v0, LX/3mN;->j:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v1}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1974588
    iget-object v0, p0, LX/DCo;->c:LX/3mN;

    iget-object v0, v0, LX/3mN;->j:LX/0bH;

    new-instance v2, LX/1Nf;

    invoke-direct {v2, v1}, LX/1Nf;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    goto/16 :goto_0
.end method
