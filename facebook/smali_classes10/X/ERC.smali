.class public final LX/ERC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

.field public final synthetic b:LX/1Vr;

.field public final synthetic c:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;LX/1Vr;)V
    .locals 0

    .prologue
    .line 2120554
    iput-object p1, p0, LX/ERC;->c:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    iput-object p2, p0, LX/ERC;->a:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    iput-object p3, p0, LX/ERC;->b:LX/1Vr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x2a5f00c3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2120544
    iget-object v1, p0, LX/ERC;->a:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    .line 2120545
    iget-object v2, v1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->g:Ljava/lang/String;

    move-object v1, v2

    .line 2120546
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2120547
    iget-object v1, p0, LX/ERC;->c:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    iget-object v1, v1, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->c:LX/EQh;

    .line 2120548
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "shoebox_moments_interstitial_accept"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2120549
    iget-object v3, v1, LX/EQh;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2120550
    iget-object v1, p0, LX/ERC;->b:LX/1Vr;

    iget-object v2, p0, LX/ERC;->a:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    .line 2120551
    iget-object v3, v2, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->g:Ljava/lang/String;

    move-object v2, v3

    .line 2120552
    iget-object v3, p0, LX/ERC;->c:Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    iget-object v3, v3, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v3}, LX/1Vr;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 2120553
    :cond_0
    const v1, -0x306c12a3

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
