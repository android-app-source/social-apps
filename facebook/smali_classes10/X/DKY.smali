.class public final LX/DKY;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1987200
    iput-object p1, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iput-object p2, p0, LX/DKY;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1987198
    iget-object v0, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iget-object v0, v0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1987199
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1987190
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1987191
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1987192
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->a()LX/0Px;

    move-result-object v0

    .line 1987193
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iget-object v0, v0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iget-boolean v0, v0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->g:Z

    if-eqz v0, :cond_1

    .line 1987194
    :cond_0
    iget-object v0, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iget-object v0, v0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->i:LX/DKa;

    sget-object v1, LX/DKF;->UPLOADING_COVER_PHOTO:LX/DKF;

    iget-object v2, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iget-object v2, v2, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->c:LX/F4e;

    invoke-static {v0, v1, v2}, LX/DKa;->a$redex0(LX/DKa;LX/DKF;LX/F4e;)V

    .line 1987195
    iget-object v0, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iget-object v0, v0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->i:LX/DKa;

    iget-object v1, p0, LX/DKY;->a:Ljava/lang/String;

    iget-object v2, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iget-object v2, v2, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->h:Landroid/net/Uri;

    iget-object v3, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iget-object v3, v3, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v4, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iget-object v4, v4, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->c:LX/F4e;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/DKa;->a(Ljava/lang/String;Landroid/net/Uri;Lcom/google/common/util/concurrent/SettableFuture;LX/F4e;Z)V

    .line 1987196
    :goto_0
    return-void

    .line 1987197
    :cond_1
    iget-object v0, p0, LX/DKY;->b:Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;

    iget-object v0, v0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Create group failure: Couldn\'t invite any group members"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
