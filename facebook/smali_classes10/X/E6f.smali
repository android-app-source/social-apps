.class public LX/E6f;
.super LX/Cft;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cft",
        "<",
        "LX/E97;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/E1i;


# direct methods
.method public constructor <init>(LX/3Tx;LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080450
    invoke-direct {p0, p1}, LX/Cft;-><init>(LX/3Tx;)V

    .line 2080451
    iput-object p2, p0, LX/E6f;->a:LX/E1i;

    .line 2080452
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 5

    .prologue
    .line 2080449
    iget-object v0, p0, LX/E6f;->a:LX/E1i;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->X()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->X()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;->c()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/Cfc;->PROFILE_TAP:LX/Cfc;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/E1i;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1a1;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)V
    .locals 5

    .prologue
    .line 2080453
    check-cast p1, LX/E97;

    .line 2080454
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2080455
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->X()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2080456
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->X()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 2080457
    const v1, 0x7f0a010a

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2080458
    const v1, 0x7f0d2436

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2080459
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2080460
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->X()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;->d()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2080461
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->gS_()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->gS_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2080462
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->gS_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2080463
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2080464
    new-instance v0, LX/E6e;

    invoke-direct {v0, p0, v2, v3}, LX/E6e;-><init>(LX/E6f;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080465
    :goto_0
    return-void

    .line 2080466
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2080448
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->X()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->X()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;->d()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->X()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;->d()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->X()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->X()Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/attachments/ReactionAttachmentsGraphQLModels$ReactionStoryAdminPageAttachmentFragmentModel$StoryAdminPageModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/1a1;
    .locals 2

    .prologue
    .line 2080445
    const v0, 0x7f030ed5

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2080446
    new-instance v1, LX/8sz;

    invoke-direct {v1}, LX/8sz;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2080447
    new-instance v1, LX/E97;

    invoke-direct {v1, v0}, LX/E97;-><init>(Landroid/view/View;)V

    return-object v1
.end method
