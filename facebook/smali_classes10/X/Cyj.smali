.class public final LX/Cyj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CyR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/CyR",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final synthetic b:LX/Cyl;

.field public final synthetic c:LX/Cyn;


# direct methods
.method public constructor <init>(LX/Cyn;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Cyl;)V
    .locals 0

    .prologue
    .line 1953549
    iput-object p1, p0, LX/Cyj;->c:LX/Cyn;

    iput-object p2, p0, LX/Cyj;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iput-object p3, p0, LX/Cyj;->b:LX/Cyl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1953550
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x1

    .line 1953551
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1953552
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v0

    .line 1953553
    const-string v1, "CombinedResults were null."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1953554
    iget-object v0, p0, LX/Cyj;->c:LX/Cyn;

    iget-object v0, v0, LX/Cyn;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cve;

    iget-object v1, p0, LX/Cyj;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1953555
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    move-object v4, v3

    .line 1953556
    iget-object v3, p0, LX/Cyj;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v5

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, LX/Cve;->a(LX/CwB;ZLcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;Ljava/lang/String;)V

    .line 1953557
    iget-object v0, p0, LX/Cyj;->c:LX/Cyn;

    new-instance v1, LX/Cym;

    .line 1953558
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->i:Ljava/lang/String;

    move-object v3, v3

    .line 1953559
    invoke-direct {v1, p1, v3}, LX/Cym;-><init>(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, LX/Cyn;->a$redex0(LX/Cyn;LX/Cym;Z)V

    .line 1953560
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1953561
    iget-object v0, p0, LX/Cyj;->c:LX/Cyn;

    iget-object v1, p0, LX/Cyj;->c:LX/Cyn;

    iget-object v1, v1, LX/Cyn;->s:LX/Fdy;

    iget-object v2, p0, LX/Cyj;->b:LX/Cyl;

    invoke-static {v0, p1, v1, v2}, LX/Cyn;->a$redex0(LX/Cyn;Ljava/lang/Throwable;LX/Fdy;LX/Cyl;)V

    .line 1953562
    return-void
.end method
