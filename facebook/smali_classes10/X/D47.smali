.class public LX/D47;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/widget/RemoteViews;

.field public final c:Landroid/widget/RemoteViews;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1961823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1961824
    iput-object p1, p0, LX/D47;->a:Landroid/content/Context;

    .line 1961825
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030393

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, LX/D47;->b:Landroid/widget/RemoteViews;

    .line 1961826
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030392

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, LX/D47;->c:Landroid/widget/RemoteViews;

    .line 1961827
    iget-object v0, p0, LX/D47;->b:Landroid/widget/RemoteViews;

    invoke-direct {p0, v0}, LX/D47;->a(Landroid/widget/RemoteViews;)V

    .line 1961828
    iget-object v0, p0, LX/D47;->c:Landroid/widget/RemoteViews;

    invoke-direct {p0, v0}, LX/D47;->a(Landroid/widget/RemoteViews;)V

    .line 1961829
    return-void
.end method

.method private a(Landroid/widget/RemoteViews;)V
    .locals 3

    .prologue
    .line 1961830
    const v0, 0x7f0d0b73

    const v1, 0x7f021b29

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 1961831
    const v0, 0x7f0d0b73

    iget-object v1, p0, LX/D47;->a:Landroid/content/Context;

    invoke-static {v1}, LX/D3x;->b(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1961832
    const v0, 0x7f0d0b6e

    const v1, 0x7f020342

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 1961833
    const v0, 0x7f0d0b6b

    iget-object v1, p0, LX/D47;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0828b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1961834
    const v0, 0x7f0d0b6c

    iget-object v1, p0, LX/D47;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0828ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1961835
    const v0, 0x7f0d0b74

    iget-object v1, p0, LX/D47;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0828bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1961836
    return-void
.end method
