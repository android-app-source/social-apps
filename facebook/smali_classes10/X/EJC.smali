.class public final LX/EJC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;LX/CzL;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2103573
    iput-object p1, p0, LX/EJC;->c:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

    iput-object p2, p0, LX/EJC;->a:LX/CzL;

    iput-object p3, p0, LX/EJC;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1780671c

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2103574
    iget-object v0, p0, LX/EJC;->a:LX/CzL;

    .line 2103575
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2103576
    check-cast v0, LX/A2F;

    invoke-interface {v0}, LX/A2F;->bn()Ljava/lang/String;

    move-result-object v0

    .line 2103577
    if-nez v0, :cond_0

    .line 2103578
    const v0, 0xc43b63a

    invoke-static {v2, v2, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2103579
    :goto_0
    return-void

    .line 2103580
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 2103581
    iget-object v0, p0, LX/EJC;->c:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2103582
    iget-object v0, p0, LX/EJC;->c:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v1, p0, LX/EJC;->b:LX/1Pn;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->OPEN_LINK:LX/8ch;

    iget-object v3, p0, LX/EJC;->b:LX/1Pn;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/EJC;->a:LX/CzL;

    .line 2103583
    iget-object v5, v4, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v4, v5

    .line 2103584
    invoke-interface {v3, v4}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v3

    iget-object v4, p0, LX/EJC;->a:LX/CzL;

    iget-object v5, p0, LX/EJC;->c:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

    iget-object v5, v5, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v5, p0, LX/EJC;->b:LX/1Pn;

    check-cast v5, LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v7

    iget-object v5, p0, LX/EJC;->b:LX/1Pn;

    check-cast v5, LX/CxP;

    iget-object v8, p0, LX/EJC;->a:LX/CzL;

    .line 2103585
    iget-object p1, v8, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v8, p1

    .line 2103586
    invoke-interface {v5, v8}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v8

    iget-object v5, p0, LX/EJC;->a:LX/CzL;

    .line 2103587
    iget-object p0, v5, LX/CzL;->d:LX/0am;

    move-object v5, p0

    .line 2103588
    invoke-virtual {v5}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2103589
    sget-object v9, LX/CvJ;->ITEM_TAPPED:LX/CvJ;

    invoke-static {v9, v7}, LX/CvY;->a(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "tapped_result_position"

    invoke-virtual {v9, p0, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "action"

    sget-object p1, LX/8ch;->OPEN_LINK:LX/8ch;

    invoke-virtual {v9, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "results_module_role"

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WEATHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v9, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string p0, "results_module_extra_logging"

    invoke-virtual {v9, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    move-object v5, v9

    .line 2103590
    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2103591
    const v0, -0x5d400c18

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method
