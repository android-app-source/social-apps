.class public LX/Co9;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/optional/Image360PhotoBlockView;",
        "LX/Clw;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/1Fb;

.field public e:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalSphericalPhoto;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/K1w;)V
    .locals 5

    .prologue
    .line 1935031
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1935032
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p0, LX/Co9;

    const-class v2, Landroid/content/Context;

    invoke-interface {v4, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v4}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v3

    check-cast v3, LX/Ckw;

    const/16 p1, 0x31dc

    invoke-static {v4, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    const/16 v0, 0x3226

    invoke-static {v4, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    iput-object v2, p0, LX/Co9;->e:Landroid/content/Context;

    iput-object v3, p0, LX/Co9;->f:LX/Ckw;

    iput-object p1, p0, LX/Co9;->g:LX/0Ot;

    iput-object v4, p0, LX/Co9;->h:LX/0Ot;

    .line 1935033
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 11

    .prologue
    .line 1935034
    check-cast p1, LX/Clw;

    const/4 v2, 0x0

    .line 1935035
    const-string v0, "SphericalPhotoBlockPresenter.bind"

    const v1, -0xdc2d0bc

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1935036
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935037
    check-cast v0, LX/K1w;

    invoke-static {p1}, LX/Co1;->a(LX/Clu;)Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1935038
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1935039
    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Co9;->i:Ljava/lang/String;

    .line 1935040
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Co9;->j:Ljava/lang/String;

    .line 1935041
    :goto_0
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1935042
    const-string v0, "SphericalPhotoBlockPresenter.bind#getImgFromSection"

    const v1, 0x3b465674

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1935043
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->l()Ljava/lang/String;

    .line 1935044
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    iput-object v0, p0, LX/Co9;->d:LX/1Fb;

    .line 1935045
    invoke-interface {p1}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v4

    .line 1935046
    const v0, -0x46174a37

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1935047
    iget-object v0, p0, LX/Co9;->d:LX/1Fb;

    if-eqz v0, :cond_2

    .line 1935048
    const-string v0, "SphericalPhotoBlockPresenter.bind#setImage"

    const v1, 0x6397a3fb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1935049
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1935050
    check-cast v0, LX/K1w;

    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v1

    invoke-interface {v1}, LX/8Yr;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Co9;->d:LX/1Fb;

    invoke-interface {v2}, LX/1Fb;->c()I

    move-result v2

    iget-object v3, p0, LX/Co9;->d:LX/1Fb;

    invoke-interface {v3}, LX/1Fb;->a()I

    move-result v3

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/Co9;->h:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/K29;

    iget-object v7, p0, LX/Co9;->e:Landroid/content/Context;

    invoke-virtual {v6, v7, p1}, LX/K29;->a(Landroid/content/Context;LX/Clw;)Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    move-result-object v6

    .line 1935051
    iput-object v1, v0, LX/K1w;->e:Ljava/lang/String;

    .line 1935052
    iput-object v5, v0, LX/K1w;->k:Ljava/lang/String;

    .line 1935053
    iput-object v4, v0, LX/K1w;->n:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1935054
    invoke-static {v4}, LX/CoV;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Cqw;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/Cos;->a(LX/Cqw;)V

    .line 1935055
    iput v2, v0, LX/K1w;->l:I

    .line 1935056
    iput v3, v0, LX/K1w;->m:I

    .line 1935057
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1935058
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v7

    check-cast v7, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;

    iget v8, v0, LX/K1w;->l:I

    iget v9, v0, LX/K1w;->m:I

    iget-object v10, v0, LX/K1w;->e:Ljava/lang/String;

    invoke-virtual {v7, v8, v9, v10, v6}, Lcom/facebook/richdocument/optional/impl/RichDocument360PhotoView;->a(IILjava/lang/String;Lcom/facebook/spherical/photo/model/SphericalPhotoParams;)V

    .line 1935059
    :cond_0
    const-class v7, LX/Cu1;

    invoke-virtual {v0, v7}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object v7

    check-cast v7, LX/Cu1;

    .line 1935060
    if-eqz v7, :cond_1

    .line 1935061
    iput-object v5, v7, LX/Cu1;->b:Ljava/lang/String;

    .line 1935062
    :cond_1
    const v0, -0x473affae

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1935063
    :cond_2
    new-instance v0, Lcom/facebook/richdocument/presenter/SphericalPhotoBlockPresenter$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/richdocument/presenter/SphericalPhotoBlockPresenter$1;-><init>(LX/Co9;LX/Clw;)V

    .line 1935064
    invoke-interface {p1}, LX/Cls;->l()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1935065
    invoke-virtual {p0, v0}, LX/CnT;->a(Ljava/lang/Runnable;)V

    .line 1935066
    :cond_3
    :goto_1
    const v0, 0xac56436

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1935067
    return-void

    .line 1935068
    :cond_4
    iput-object v2, p0, LX/Co9;->i:Ljava/lang/String;

    .line 1935069
    iput-object v2, p0, LX/Co9;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 1935070
    :cond_5
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1935071
    invoke-super {p0, p1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 1935072
    iget-object v0, p0, LX/Co9;->f:LX/Ckw;

    iget-object v1, p0, LX/Co9;->i:Ljava/lang/String;

    iget-object v2, p0, LX/Co9;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/Ckw;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1935073
    return-void
.end method
