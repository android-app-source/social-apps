.class public final LX/D6H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3J0;
.implements LX/394;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1YQ;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/atomic/AtomicReference;LX/1YQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;",
            "LX/1YQ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1965440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1965441
    iput-object p1, p0, LX/D6H;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 1965442
    iput-object p2, p0, LX/D6H;->b:LX/1YQ;

    .line 1965443
    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 0

    .prologue
    .line 1965444
    return-void
.end method

.method public final a(LX/04g;LX/7Jv;)V
    .locals 0

    .prologue
    .line 1965438
    invoke-virtual {p0, p2}, LX/D6H;->a(LX/7Jv;)V

    .line 1965439
    return-void
.end method

.method public final a(LX/7Jv;)V
    .locals 3

    .prologue
    .line 1965433
    iget-object v0, p0, LX/D6H;->b:LX/1YQ;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/1YQ;->a(LX/2oO;Z)V

    .line 1965434
    iget-object v0, p0, LX/D6H;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3J0;

    .line 1965435
    if-nez v0, :cond_0

    .line 1965436
    :goto_0
    return-void

    .line 1965437
    :cond_0
    invoke-interface {v0, p1}, LX/3J0;->a(LX/7Jv;)V

    goto :goto_0
.end method
