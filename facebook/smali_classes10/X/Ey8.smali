.class public final LX/Ey8;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;)V
    .locals 0

    .prologue
    .line 2185795
    iput-object p1, p0, LX/Ey8;->a:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2185796
    check-cast p1, LX/2f2;

    .line 2185797
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-nez v0, :cond_1

    .line 2185798
    :cond_0
    :goto_0
    return-void

    .line 2185799
    :cond_1
    iget-object v0, p0, LX/Ey8;->a:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-wide v2, p1, LX/2f2;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(J)I

    move-result v0

    .line 2185800
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2185801
    iget-object v1, p0, LX/Ey8;->a:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-object v1, v1, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eus;

    .line 2185802
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2185803
    invoke-virtual {v0}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    if-eq v2, v1, :cond_0

    .line 2185804
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/Eus;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185805
    invoke-virtual {v0, v1}, LX/Eus;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185806
    iget-object v0, p0, LX/Ey8;->a:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method
