.class public LX/Co2;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/PullQuoteAttributionBlockView;",
        "Lcom/facebook/richdocument/model/data/PullQuoteAttributionBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/CpX;)V
    .locals 0

    .prologue
    .line 1934788
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934789
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 11

    .prologue
    .line 1934768
    check-cast p1, LX/CmZ;

    .line 1934769
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934770
    check-cast v0, LX/CpX;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1934771
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934772
    check-cast v0, LX/CpX;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1934773
    new-instance v2, LX/Cle;

    invoke-direct {v2, v1}, LX/Cle;-><init>(Landroid/content/Context;)V

    .line 1934774
    iget-object v3, p1, LX/CmZ;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-object v3, v3

    .line 1934775
    sget-object v4, LX/Clb;->PULL_QUOTE_ATTRIBUTION:LX/Clb;

    .line 1934776
    if-nez v3, :cond_0

    .line 1934777
    :goto_0
    move-object v2, v2

    .line 1934778
    invoke-virtual {v2}, LX/Cle;->a()LX/Clf;

    move-result-object v2

    move-object v1, v2

    .line 1934779
    iget-object v2, v0, LX/CpX;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934780
    iget-object v0, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v0

    .line 1934781
    invoke-virtual {v2, v1}, LX/CtG;->setText(LX/Clf;)V

    .line 1934782
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934783
    check-cast v0, LX/CpX;

    invoke-interface {p1}, LX/Clr;->lw_()LX/Cml;

    move-result-object v1

    invoke-interface {v0, v1}, LX/CnG;->a(LX/Cml;)V

    .line 1934784
    return-void

    .line 1934785
    :cond_0
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v7, v5

    .line 1934786
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v8, v5

    .line 1934787
    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->b()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    move-object v5, v2

    move-object v10, v4

    invoke-static/range {v5 .. v10}, LX/Cle;->a(LX/Cle;Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;LX/Clb;)V

    goto :goto_0
.end method
