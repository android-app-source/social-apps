.class public LX/EnP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/EnP;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2167034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167035
    return-void
.end method

.method public static a(LX/0QB;)LX/EnP;
    .locals 3

    .prologue
    .line 2167036
    sget-object v0, LX/EnP;->a:LX/EnP;

    if-nez v0, :cond_1

    .line 2167037
    const-class v1, LX/EnP;

    monitor-enter v1

    .line 2167038
    :try_start_0
    sget-object v0, LX/EnP;->a:LX/EnP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2167039
    if-eqz v2, :cond_0

    .line 2167040
    :try_start_1
    new-instance v0, LX/EnP;

    invoke-direct {v0}, LX/EnP;-><init>()V

    .line 2167041
    move-object v0, v0

    .line 2167042
    sput-object v0, LX/EnP;->a:LX/EnP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2167043
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2167044
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2167045
    :cond_1
    sget-object v0, LX/EnP;->a:LX/EnP;

    return-object v0

    .line 2167046
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2167047
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;)Lcom/facebook/entitycards/intent/EntityCardsFragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2167048
    invoke-static {p0}, LX/4tq;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v0

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 2167049
    const-string v1, "chromeless:content:fragment:tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2167050
    check-cast v0, Lcom/facebook/entitycards/intent/EntityCardsFragment;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2167051
    invoke-static {p0}, LX/4tq;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v0

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 2167052
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2167053
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2167054
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2167055
    new-instance v2, Lcom/facebook/entitycards/intent/EntityCardsFragment;

    invoke-direct {v2}, Lcom/facebook/entitycards/intent/EntityCardsFragment;-><init>()V

    .line 2167056
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2167057
    move-object v1, v2

    .line 2167058
    invoke-static {p0}, LX/18w;->b(Landroid/content/Context;)I

    move-result v2

    const-string v3, "chromeless:content:fragment:tag"

    invoke-virtual {v0, v2, v1, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 2167059
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 2167060
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2167061
    return-void
.end method
