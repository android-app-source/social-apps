.class public final LX/EVt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21M;


# instance fields
.field public final synthetic a:Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

.field private final b:LX/21M;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;LX/21M;)V
    .locals 0

    .prologue
    .line 2129032
    iput-object p1, p0, LX/EVt;->a:Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129033
    iput-object p2, p0, LX/EVt;->b:LX/21M;

    .line 2129034
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1zt;LX/0Ve;)V
    .locals 5

    .prologue
    .line 2129035
    iget-object v0, p0, LX/EVt;->b:LX/21M;

    if-eqz v0, :cond_0

    .line 2129036
    iget-object v0, p0, LX/EVt;->b:LX/21M;

    invoke-interface {v0, p1, p2, p3}, LX/21M;->a(Landroid/view/View;LX/1zt;LX/0Ve;)V

    .line 2129037
    :cond_0
    iget-object v0, p0, LX/EVt;->a:Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    iget-object v0, v0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v1, p0, LX/EVt;->a:Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    iget-object v1, v1, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v1

    invoke-static {v0, v1, p2}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;ILX/1zt;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    move-result-object v1

    .line 2129038
    iget-object v2, p0, LX/EVt;->a:Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    iget-object v0, p0, LX/EVt;->a:Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    iget-object v0, v0, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20j;

    iget-object v3, p0, LX/EVt;->a:Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    iget-object v3, v3, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->h:LX/20i;

    invoke-virtual {v3}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    iget-object v4, p0, LX/EVt;->a:Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    iget-object v4, v4, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v1

    invoke-virtual {v0, v3, v4, v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 2129039
    iput-object v0, v2, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->n:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2129040
    iget-object v0, p0, LX/EVt;->a:Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;

    .line 2129041
    invoke-static {v0}, Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;->b$redex0(Lcom/facebook/video/videohome/views/VideoHomeUnitFeedbackView;)V

    .line 2129042
    return-void
.end method
