.class public LX/DFR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/2ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2ew",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2ew;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1978170
    iput-object p1, p0, LX/DFR;->a:LX/2ew;

    .line 1978171
    return-void
.end method

.method public static a(LX/0QB;)LX/DFR;
    .locals 4

    .prologue
    .line 1978172
    const-class v1, LX/DFR;

    monitor-enter v1

    .line 1978173
    :try_start_0
    sget-object v0, LX/DFR;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978174
    sput-object v2, LX/DFR;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978175
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978176
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978177
    new-instance p0, LX/DFR;

    invoke-static {v0}, LX/2ew;->a(LX/0QB;)LX/2ew;

    move-result-object v3

    check-cast v3, LX/2ew;

    invoke-direct {p0, v3}, LX/DFR;-><init>(LX/2ew;)V

    .line 1978178
    move-object v0, p0

    .line 1978179
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978180
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978181
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
