.class public LX/Drf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dre;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2049558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049559
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Drf;->a:Ljava/util/Set;

    .line 2049560
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Drf;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2049561
    if-eqz p2, :cond_0

    .line 2049562
    iget-object v0, p0, LX/Drf;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2049563
    :goto_0
    return-void

    .line 2049564
    :cond_0
    iget-object v0, p0, LX/Drf;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2049565
    iget-object v0, p0, LX/Drf;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
