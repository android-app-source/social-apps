.class public final LX/Dq5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/2nq;

.field public final synthetic d:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

.field public final synthetic e:LX/3D3;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:I

.field public final synthetic h:LX/3D2;


# direct methods
.method public constructor <init>(LX/3D2;Landroid/content/Context;Landroid/view/View;LX/2nq;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;LX/3D3;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2047852
    iput-object p1, p0, LX/Dq5;->h:LX/3D2;

    iput-object p2, p0, LX/Dq5;->a:Landroid/content/Context;

    iput-object p3, p0, LX/Dq5;->b:Landroid/view/View;

    iput-object p4, p0, LX/Dq5;->c:LX/2nq;

    iput-object p5, p0, LX/Dq5;->d:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    iput-object p6, p0, LX/Dq5;->e:LX/3D3;

    iput-object p7, p0, LX/Dq5;->f:Ljava/lang/String;

    iput p8, p0, LX/Dq5;->g:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 14

    .prologue
    .line 2047853
    iget-object v0, p0, LX/Dq5;->h:LX/3D2;

    iget-object v0, v0, LX/3D2;->b:LX/33W;

    iget-object v1, p0, LX/Dq5;->a:Landroid/content/Context;

    iget-object v2, p0, LX/Dq5;->b:Landroid/view/View;

    iget-object v3, p0, LX/Dq5;->c:LX/2nq;

    iget-object v4, p0, LX/Dq5;->d:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    iget-object v5, p0, LX/Dq5;->e:LX/3D3;

    iget-object v6, p0, LX/Dq5;->f:Ljava/lang/String;

    iget v7, p0, LX/Dq5;->g:I

    .line 2047854
    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v8

    .line 2047855
    sget-object v9, LX/Dq1;->a:[I

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->ordinal()I

    move-result v8

    aget v8, v9, v8

    packed-switch v8, :pswitch_data_0

    .line 2047856
    :goto_0
    :pswitch_0
    iget-object v8, v0, LX/33W;->f:LX/33Y;

    const-string v9, "inline_action_selected"

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->c()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v3}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v12

    move-object v11, v6

    move v13, v7

    invoke-virtual/range {v8 .. v13}, LX/33Y;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;I)V

    .line 2047857
    const/4 v0, 0x1

    return v0

    .line 2047858
    :pswitch_1
    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object v9

    .line 2047859
    if-eqz v9, :cond_0

    invoke-interface {v9}, LX/BCS;->j()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    const/4 v10, 0x1

    if-eq v8, v10, :cond_1

    .line 2047860
    :cond_0
    :goto_1
    goto :goto_0

    .line 2047861
    :pswitch_2
    iget-object v8, v0, LX/33W;->n:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/79m;

    invoke-virtual {v8, v2}, LX/79m;->a(Landroid/view/View;)V

    .line 2047862
    invoke-static {v0, v1}, LX/33W;->a(LX/33W;Landroid/content/Context;)V

    .line 2047863
    :goto_2
    :pswitch_3
    invoke-static {v0, v3, v4, v5}, LX/33W;->a(LX/33W;LX/2nq;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;LX/3D3;)V

    goto :goto_0

    .line 2047864
    :pswitch_4
    iget-object v8, v0, LX/33W;->n:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/79m;

    invoke-virtual {v8}, LX/79m;->a()V

    goto :goto_2

    .line 2047865
    :cond_1
    invoke-interface {v9}, LX/BCS;->j()LX/0Px;

    move-result-object v8

    const/4 v10, 0x0

    invoke-virtual {v8, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/BCR;

    .line 2047866
    invoke-interface {v8}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 2047867
    invoke-interface {v8}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v8

    .line 2047868
    sget-object v10, LX/Dq1;->b:[I

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->ordinal()I

    move-result v8

    aget v8, v10, v8

    packed-switch v8, :pswitch_data_1

    goto :goto_1

    .line 2047869
    :pswitch_5
    const/4 v8, 0x0

    invoke-static {v0, v1, v9, v3, v8}, LX/33W;->a(LX/33W;Landroid/content/Context;LX/BCS;LX/2nq;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch
.end method
