.class public final LX/Ex7;
.super LX/2hO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2184134
    iput-object p1, p0, LX/Ex7;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-direct {p0}, LX/2hO;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2184135
    check-cast p1, LX/2iB;

    .line 2184136
    if-nez p1, :cond_1

    .line 2184137
    :cond_0
    :goto_0
    return-void

    .line 2184138
    :cond_1
    iget-object v0, p0, LX/Ex7;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    if-eqz v0, :cond_2

    .line 2184139
    iget-object v0, p0, LX/Ex7;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->v:LX/2kW;

    new-instance v1, LX/Ex6;

    invoke-direct {v1, p0, p1}, LX/Ex6;-><init>(LX/Ex7;LX/2iB;)V

    iget-wide v2, p1, LX/2iB;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2kW;->a(LX/0Rl;Ljava/lang/String;)V

    .line 2184140
    :goto_1
    iget-object v0, p0, LX/Ex7;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-static {v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->p(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    goto :goto_0

    .line 2184141
    :cond_2
    iget-object v0, p0, LX/Ex7;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->h:Ljava/util/LinkedHashMap;

    iget-wide v2, p1, LX/2iB;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2184142
    iget-object v0, p0, LX/Ex7;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->h:Ljava/util/LinkedHashMap;

    iget-wide v2, p1, LX/2iB;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2184143
    iget-object v0, p0, LX/Ex7;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    instance-of v0, v0, LX/Exw;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2184144
    iget-object v0, p0, LX/Ex7;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    check-cast v0, LX/Exw;

    iget-wide v2, p1, LX/2iB;->a:J

    .line 2184145
    invoke-virtual {v0, v2, v3}, LX/Exw;->a(J)I

    move-result v1

    .line 2184146
    const/4 v4, -0x1

    if-eq v1, v4, :cond_3

    .line 2184147
    iget-object v4, v0, LX/Exw;->c:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2184148
    const v1, -0x7fc61058

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2184149
    :cond_3
    goto :goto_1
.end method
