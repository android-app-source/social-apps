.class public LX/DcA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dc9;


# static fields
.field private static final a:I


# instance fields
.field private final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2017974
    sget-object v0, LX/DcG;->LINK_MENU:LX/DcG;

    invoke-virtual {v0}, LX/DcG;->ordinal()I

    move-result v0

    sput v0, LX/DcA;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2017975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2017976
    iput-object p1, p0, LX/DcA;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2017977
    return-void
.end method


# virtual methods
.method public final a()LX/DcG;
    .locals 1

    .prologue
    .line 2017978
    sget-object v0, LX/DcG;->LINK_MENU:LX/DcG;

    return-object v0
.end method

.method public final a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V
    .locals 4
    .param p4    # Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2017953
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2017954
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2017955
    const-string v1, "extra_menu_type"

    sget-object v2, LX/Dc2;->LINK_MENU:LX/Dc2;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2017956
    const p3, 0x23392750

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2017957
    if-nez p4, :cond_0

    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    if-eqz v1, :cond_4

    .line 2017958
    const/4 v1, 0x0

    .line 2017959
    :goto_2
    move-object v1, v1

    .line 2017960
    const-string v2, "extra_link_menu_url"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2017961
    iget-object v1, p0, LX/DcA;->b:Lcom/facebook/content/SecureContextHelper;

    sget v2, LX/DcA;->a:I

    invoke-interface {v1, v0, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2017962
    return-void

    .line 2017963
    :cond_0
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2017964
    if-nez v2, :cond_1

    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_0

    .line 2017965
    :cond_2
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-static {v2, v1, v3, p3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v1

    .line 2017966
    if-eqz v1, :cond_3

    invoke-static {v1}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    :goto_3
    invoke-virtual {v1}, LX/39O;->a()Z

    move-result v1

    goto :goto_1

    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto :goto_3

    .line 2017967
    :cond_4
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-static {v2, v1, v3, p3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {v1}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    :goto_4
    invoke-virtual {v1, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto :goto_4
.end method

.method public final a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 2017971
    sget v0, LX/DcA;->a:I

    if-ne p3, v0, :cond_0

    const/4 v0, -0x1

    if-ne p4, v0, :cond_0

    .line 2017972
    invoke-virtual {p1}, Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;->c()V

    .line 2017973
    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 2017970
    sget v0, LX/DcA;->a:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/15i;I)Z
    .locals 2
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "isVisibleMenu"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2017969
    if-eqz p2, :cond_0

    invoke-virtual {p1, p2, v0}, LX/15i;->h(II)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b(LX/15i;I)Z
    .locals 1
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "shouldShowInManagementScreen"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2017968
    const/4 v0, 0x1

    return v0
.end method
