.class public final LX/DPy;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Landroid/support/v7/widget/GridLayout;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

.field public final synthetic c:Lcom/facebook/groups/info/GroupInfoAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;LX/0Px;Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;)V
    .locals 0

    .prologue
    .line 1993963
    iput-object p1, p0, LX/DPy;->c:Lcom/facebook/groups/info/GroupInfoAdapter;

    iput-object p3, p0, LX/DPy;->a:LX/0Px;

    iput-object p4, p0, LX/DPy;->b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 1993964
    check-cast p1, Landroid/support/v7/widget/GridLayout;

    const/4 v1, 0x0

    .line 1993965
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1993966
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1993967
    iget-object v0, p0, LX/DPy;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    iget-object v0, p0, LX/DPy;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;

    .line 1993968
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1993969
    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1993970
    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1993971
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1993972
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1993973
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1993974
    invoke-virtual {p1}, Landroid/support/v7/widget/GridLayout;->getColumnCount()I

    move-result v2

    invoke-virtual {p1}, Landroid/support/v7/widget/GridLayout;->getRowCount()I

    move-result v5

    mul-int/2addr v2, v5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 1993975
    :goto_1
    invoke-virtual {p1}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v2

    if-ge v2, v5, :cond_2

    .line 1993976
    const v2, 0x7f030814

    const/4 v6, 0x1

    invoke-virtual {v0, v2, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_1

    .line 1993977
    :cond_2
    :goto_2
    invoke-virtual {p1}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v0

    if-le v0, v5, :cond_3

    .line 1993978
    invoke-virtual {p1}, Landroid/support/v7/widget/GridLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/GridLayout;->removeView(Landroid/view/View;)V

    goto :goto_2

    :cond_3
    move v2, v1

    .line 1993979
    :goto_3
    if-ge v2, v5, :cond_5

    .line 1993980
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    .line 1993981
    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1993982
    invoke-virtual {p1, v2}, Landroid/support/v7/widget/GridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1993983
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v1, 0x0

    :goto_4
    sget-object v6, Lcom/facebook/groups/info/GroupInfoAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1993984
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1993985
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1993986
    :cond_4
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_4

    .line 1993987
    :cond_5
    iget-object v0, p0, LX/DPy;->c:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v1, p0, LX/DPy;->b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    invoke-static {v0, v1, v3, v4, v5}, Lcom/facebook/groups/info/GroupInfoAdapter;->a$redex0(Lcom/facebook/groups/info/GroupInfoAdapter;Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;Ljava/util/List;Ljava/util/List;I)V

    .line 1993988
    return-void
.end method
