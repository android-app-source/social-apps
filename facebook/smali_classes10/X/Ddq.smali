.class public final LX/Ddq;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactsYouMayKnowQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2020231
    const-class v1, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactsYouMayKnowQueryModel;

    const v0, -0x5a090ce5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "ContactsYouMayKnowQuery"

    const-string v6, "30ae26d9285af44855af1756974f7285"

    const-string v7, "viewer"

    const-string v8, "10155069963396729"

    const-string v9, "10155259086526729"

    .line 2020232
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2020233
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2020234
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2020235
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2020236
    sparse-switch v0, :sswitch_data_0

    .line 2020237
    :goto_0
    return-object p1

    .line 2020238
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2020239
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2020240
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6e761353 -> :sswitch_2
        -0x476da071 -> :sswitch_1
        -0xedb418c -> :sswitch_0
    .end sparse-switch
.end method
