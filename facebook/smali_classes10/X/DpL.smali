.class public LX/DpL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# instance fields
.field public final identity_key:[B

.field public final msg_to:LX/DpM;

.field public final pre_key_with_id:LX/DpU;

.field public final signed_pre_key_with_id:LX/Dpf;

.field public final suggested_codename:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/16 v3, 0xc

    .line 2044332
    new-instance v0, LX/1sv;

    const-string v1, "LookupResult"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpL;->b:LX/1sv;

    .line 2044333
    new-instance v0, LX/1sw;

    const-string v1, "msg_to"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpL;->c:LX/1sw;

    .line 2044334
    new-instance v0, LX/1sw;

    const-string v1, "suggested_codename"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpL;->d:LX/1sw;

    .line 2044335
    new-instance v0, LX/1sw;

    const-string v1, "identity_key"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpL;->e:LX/1sw;

    .line 2044336
    new-instance v0, LX/1sw;

    const-string v1, "signed_pre_key_with_id"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpL;->f:LX/1sw;

    .line 2044337
    new-instance v0, LX/1sw;

    const-string v1, "pre_key_with_id"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpL;->g:LX/1sw;

    .line 2044338
    const/4 v0, 0x1

    sput-boolean v0, LX/DpL;->a:Z

    return-void
.end method

.method private constructor <init>(LX/DpM;Ljava/lang/String;[BLX/Dpf;LX/DpU;)V
    .locals 0

    .prologue
    .line 2044460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2044461
    iput-object p1, p0, LX/DpL;->msg_to:LX/DpM;

    .line 2044462
    iput-object p2, p0, LX/DpL;->suggested_codename:Ljava/lang/String;

    .line 2044463
    iput-object p3, p0, LX/DpL;->identity_key:[B

    .line 2044464
    iput-object p4, p0, LX/DpL;->signed_pre_key_with_id:LX/Dpf;

    .line 2044465
    iput-object p5, p0, LX/DpL;->pre_key_with_id:LX/DpU;

    .line 2044466
    return-void
.end method

.method public static b(LX/1su;)LX/DpL;
    .locals 9

    .prologue
    const/16 v8, 0xb

    const/16 v7, 0xc

    const/4 v5, 0x0

    .line 2044437
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v4, v5

    move-object v3, v5

    move-object v2, v5

    move-object v1, v5

    .line 2044438
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 2044439
    iget-byte v6, v0, LX/1sw;->b:B

    if-eqz v6, :cond_5

    .line 2044440
    iget-short v6, v0, LX/1sw;->c:S

    packed-switch v6, :pswitch_data_0

    .line 2044441
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044442
    :pswitch_0
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v7, :cond_0

    .line 2044443
    invoke-static {p0}, LX/DpM;->b(LX/1su;)LX/DpM;

    move-result-object v1

    goto :goto_0

    .line 2044444
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044445
    :pswitch_1
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v8, :cond_1

    .line 2044446
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2044447
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044448
    :pswitch_2
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v8, :cond_2

    .line 2044449
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v3

    goto :goto_0

    .line 2044450
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044451
    :pswitch_3
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v7, :cond_3

    .line 2044452
    invoke-static {p0}, LX/Dpf;->b(LX/1su;)LX/Dpf;

    move-result-object v4

    goto :goto_0

    .line 2044453
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044454
    :pswitch_4
    iget-byte v6, v0, LX/1sw;->b:B

    if-ne v6, v7, :cond_4

    .line 2044455
    invoke-static {p0}, LX/DpU;->b(LX/1su;)LX/DpU;

    move-result-object v5

    goto :goto_0

    .line 2044456
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2044457
    :cond_5
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2044458
    new-instance v0, LX/DpL;

    invoke-direct/range {v0 .. v5}, LX/DpL;-><init>(LX/DpM;Ljava/lang/String;[BLX/Dpf;LX/DpU;)V

    .line 2044459
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2044385
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2044386
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2044387
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2044388
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "LookupResult"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2044389
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044390
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044391
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044392
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044393
    const-string v4, "msg_to"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044394
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044395
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044396
    iget-object v4, p0, LX/DpL;->msg_to:LX/DpM;

    if-nez v4, :cond_3

    .line 2044397
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044398
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044399
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044400
    const-string v4, "suggested_codename"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044401
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044402
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044403
    iget-object v4, p0, LX/DpL;->suggested_codename:Ljava/lang/String;

    if-nez v4, :cond_4

    .line 2044404
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044405
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044406
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044407
    const-string v4, "identity_key"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044408
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044409
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044410
    iget-object v4, p0, LX/DpL;->identity_key:[B

    if-nez v4, :cond_5

    .line 2044411
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044412
    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044413
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044414
    const-string v4, "signed_pre_key_with_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044415
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044416
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044417
    iget-object v4, p0, LX/DpL;->signed_pre_key_with_id:LX/Dpf;

    if-nez v4, :cond_6

    .line 2044418
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044419
    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044420
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044421
    const-string v4, "pre_key_with_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044422
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044423
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044424
    iget-object v0, p0, LX/DpL;->pre_key_with_id:LX/DpU;

    if-nez v0, :cond_7

    .line 2044425
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044426
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044427
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044428
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2044429
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2044430
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2044431
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2044432
    :cond_3
    iget-object v4, p0, LX/DpL;->msg_to:LX/DpM;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2044433
    :cond_4
    iget-object v4, p0, LX/DpL;->suggested_codename:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2044434
    :cond_5
    iget-object v4, p0, LX/DpL;->identity_key:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2044435
    :cond_6
    iget-object v4, p0, LX/DpL;->signed_pre_key_with_id:LX/Dpf;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2044436
    :cond_7
    iget-object v0, p0, LX/DpL;->pre_key_with_id:LX/DpU;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2044467
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2044468
    iget-object v0, p0, LX/DpL;->msg_to:LX/DpM;

    if-eqz v0, :cond_0

    .line 2044469
    sget-object v0, LX/DpL;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044470
    iget-object v0, p0, LX/DpL;->msg_to:LX/DpM;

    invoke-virtual {v0, p1}, LX/DpM;->a(LX/1su;)V

    .line 2044471
    :cond_0
    iget-object v0, p0, LX/DpL;->suggested_codename:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2044472
    sget-object v0, LX/DpL;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044473
    iget-object v0, p0, LX/DpL;->suggested_codename:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2044474
    :cond_1
    iget-object v0, p0, LX/DpL;->identity_key:[B

    if-eqz v0, :cond_2

    .line 2044475
    sget-object v0, LX/DpL;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044476
    iget-object v0, p0, LX/DpL;->identity_key:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2044477
    :cond_2
    iget-object v0, p0, LX/DpL;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v0, :cond_3

    .line 2044478
    sget-object v0, LX/DpL;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044479
    iget-object v0, p0, LX/DpL;->signed_pre_key_with_id:LX/Dpf;

    invoke-virtual {v0, p1}, LX/Dpf;->a(LX/1su;)V

    .line 2044480
    :cond_3
    iget-object v0, p0, LX/DpL;->pre_key_with_id:LX/DpU;

    if-eqz v0, :cond_4

    .line 2044481
    sget-object v0, LX/DpL;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044482
    iget-object v0, p0, LX/DpL;->pre_key_with_id:LX/DpU;

    invoke-virtual {v0, p1}, LX/DpU;->a(LX/1su;)V

    .line 2044483
    :cond_4
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2044484
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2044485
    return-void
.end method

.method public final a(LX/DpL;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2044347
    if-nez p1, :cond_1

    .line 2044348
    :cond_0
    :goto_0
    return v2

    .line 2044349
    :cond_1
    iget-object v0, p0, LX/DpL;->msg_to:LX/DpM;

    if-eqz v0, :cond_c

    move v0, v1

    .line 2044350
    :goto_1
    iget-object v3, p1, LX/DpL;->msg_to:LX/DpM;

    if-eqz v3, :cond_d

    move v3, v1

    .line 2044351
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 2044352
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2044353
    iget-object v0, p0, LX/DpL;->msg_to:LX/DpM;

    iget-object v3, p1, LX/DpL;->msg_to:LX/DpM;

    invoke-virtual {v0, v3}, LX/DpM;->a(LX/DpM;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2044354
    :cond_3
    iget-object v0, p0, LX/DpL;->suggested_codename:Ljava/lang/String;

    if-eqz v0, :cond_e

    move v0, v1

    .line 2044355
    :goto_3
    iget-object v3, p1, LX/DpL;->suggested_codename:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v3, v1

    .line 2044356
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2044357
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2044358
    iget-object v0, p0, LX/DpL;->suggested_codename:Ljava/lang/String;

    iget-object v3, p1, LX/DpL;->suggested_codename:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2044359
    :cond_5
    iget-object v0, p0, LX/DpL;->identity_key:[B

    if-eqz v0, :cond_10

    move v0, v1

    .line 2044360
    :goto_5
    iget-object v3, p1, LX/DpL;->identity_key:[B

    if-eqz v3, :cond_11

    move v3, v1

    .line 2044361
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2044362
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2044363
    iget-object v0, p0, LX/DpL;->identity_key:[B

    iget-object v3, p1, LX/DpL;->identity_key:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2044364
    :cond_7
    iget-object v0, p0, LX/DpL;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v0, :cond_12

    move v0, v1

    .line 2044365
    :goto_7
    iget-object v3, p1, LX/DpL;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v3, :cond_13

    move v3, v1

    .line 2044366
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2044367
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2044368
    iget-object v0, p0, LX/DpL;->signed_pre_key_with_id:LX/Dpf;

    iget-object v3, p1, LX/DpL;->signed_pre_key_with_id:LX/Dpf;

    invoke-virtual {v0, v3}, LX/Dpf;->a(LX/Dpf;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2044369
    :cond_9
    iget-object v0, p0, LX/DpL;->pre_key_with_id:LX/DpU;

    if-eqz v0, :cond_14

    move v0, v1

    .line 2044370
    :goto_9
    iget-object v3, p1, LX/DpL;->pre_key_with_id:LX/DpU;

    if-eqz v3, :cond_15

    move v3, v1

    .line 2044371
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2044372
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2044373
    iget-object v0, p0, LX/DpL;->pre_key_with_id:LX/DpU;

    iget-object v3, p1, LX/DpL;->pre_key_with_id:LX/DpU;

    invoke-virtual {v0, v3}, LX/DpU;->a(LX/DpU;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_b
    move v2, v1

    .line 2044374
    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 2044375
    goto/16 :goto_1

    :cond_d
    move v3, v2

    .line 2044376
    goto/16 :goto_2

    :cond_e
    move v0, v2

    .line 2044377
    goto :goto_3

    :cond_f
    move v3, v2

    .line 2044378
    goto :goto_4

    :cond_10
    move v0, v2

    .line 2044379
    goto :goto_5

    :cond_11
    move v3, v2

    .line 2044380
    goto :goto_6

    :cond_12
    move v0, v2

    .line 2044381
    goto :goto_7

    :cond_13
    move v3, v2

    .line 2044382
    goto :goto_8

    :cond_14
    move v0, v2

    .line 2044383
    goto :goto_9

    :cond_15
    move v3, v2

    .line 2044384
    goto :goto_a
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2044343
    if-nez p1, :cond_1

    .line 2044344
    :cond_0
    :goto_0
    return v0

    .line 2044345
    :cond_1
    instance-of v1, p1, LX/DpL;

    if-eqz v1, :cond_0

    .line 2044346
    check-cast p1, LX/DpL;

    invoke-virtual {p0, p1}, LX/DpL;->a(LX/DpL;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2044342
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2044339
    sget-boolean v0, LX/DpL;->a:Z

    .line 2044340
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpL;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2044341
    return-object v0
.end method
