.class public LX/EVq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/636;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 2129009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129010
    iput-object p1, p0, LX/EVq;->a:Landroid/widget/TextView;

    .line 2129011
    iput-object p2, p0, LX/EVq;->b:Landroid/widget/TextView;

    .line 2129012
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 2129013
    iget-object v0, p0, LX/EVq;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    .line 2129014
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 2129015
    :cond_0
    :goto_0
    move v0, v1

    .line 2129016
    if-eqz v0, :cond_1

    .line 2129017
    iget-object v0, p0, LX/EVq;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2129018
    iget-object v0, p0, LX/EVq;->a:Landroid/widget/TextView;

    iget-object v1, p0, LX/EVq;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, LX/EVq;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, LX/EVq;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, LX/EVq;->a:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2129019
    :goto_1
    return-void

    .line 2129020
    :cond_1
    iget-object v0, p0, LX/EVq;->b:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2129021
    const/4 v0, 0x0

    return v0
.end method
