.class public LX/Dii;
.super LX/6LI;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field public b:Lcom/facebook/messaging/model/threads/ThreadSummary;

.field private c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;

.field private d:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

.field private e:LX/2Ou;

.field private f:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2Ou;LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2032473
    const-string v0, "BookingRequestsBannerNotification"

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 2032474
    const v0, 0x7f010593

    const v1, 0x7f0e0a87

    invoke-static {p1, v0, v1}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/Dii;->a:Landroid/content/Context;

    .line 2032475
    iput-object p2, p0, LX/Dii;->e:LX/2Ou;

    .line 2032476
    iput-object p3, p0, LX/Dii;->f:LX/0Uh;

    .line 2032477
    return-void
.end method

.method public static f(LX/Dii;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2032478
    iget-object v1, p0, LX/Dii;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_1

    invoke-direct {p0}, LX/Dii;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Dii;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;

    if-eqz v1, :cond_1

    :cond_0
    invoke-direct {p0}, LX/Dii;->g()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/Dii;->d:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    if-nez v1, :cond_2

    .line 2032479
    :cond_1
    :goto_0
    return-void

    .line 2032480
    :cond_2
    iget-object v1, p0, LX/Dii;->e:LX/2Ou;

    iget-object v2, p0, LX/Dii;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v1, v2}, LX/2Ou;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v1

    .line 2032481
    invoke-direct {p0}, LX/Dii;->g()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2032482
    iget-object v2, p0, LX/Dii;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;

    iget-object v3, p0, LX/Dii;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_3

    :goto_1
    iget-object v1, p0, LX/Dii;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->V:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2032483
    :cond_4
    iget-object v2, p0, LX/Dii;->d:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iget-object v3, p0, LX/Dii;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_5

    :goto_2
    iget-object v1, p0, LX/Dii;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->V:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private g()Z
    .locals 3

    .prologue
    .line 2032484
    iget-object v0, p0, LX/Dii;->f:LX/0Uh;

    const/16 v1, 0x61e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2032485
    iget-object v0, p0, LX/Dii;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2032486
    invoke-direct {p0}, LX/Dii;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2032487
    const v1, 0x7f0301b8

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;

    iput-object v0, p0, LX/Dii;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;

    .line 2032488
    invoke-static {p0}, LX/Dii;->f(LX/Dii;)V

    .line 2032489
    iget-object v0, p0, LX/Dii;->c:Lcom/facebook/messaging/professionalservices/booking/ui/BookingNotificationBannerView;

    .line 2032490
    :goto_0
    return-object v0

    .line 2032491
    :cond_0
    const v1, 0x7f0301b2

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    iput-object v0, p0, LX/Dii;->d:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    .line 2032492
    invoke-static {p0}, LX/Dii;->f(LX/Dii;)V

    .line 2032493
    iget-object v0, p0, LX/Dii;->d:Lcom/facebook/messaging/professionalservices/booking/ui/BookingAppointmentNotificationBannerView;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2032494
    iget-object v0, p0, LX/Dii;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dii;->b:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->V:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    if-eqz v0, :cond_0

    .line 2032495
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 2032496
    invoke-virtual {v0, p0}, LX/6LN;->a(LX/6LI;)V

    .line 2032497
    invoke-static {p0}, LX/Dii;->f(LX/Dii;)V

    .line 2032498
    :goto_0
    return-void

    .line 2032499
    :cond_0
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 2032500
    invoke-virtual {v0, p0}, LX/6LN;->b(LX/6LI;)V

    goto :goto_0
.end method
