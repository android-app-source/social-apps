.class public final LX/EJe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2104801
    iput-object p1, p0, LX/EJe;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

    iput-object p2, p0, LX/EJe;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    iput-object p3, p0, LX/EJe;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x5ea47f2b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2104802
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    iget-object v1, p0, LX/EJe;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2104803
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v1, v2

    .line 2104804
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2104805
    iget-object v0, p0, LX/EJe;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v2, p0, LX/EJe;->b:LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2104806
    iget-object v0, p0, LX/EJe;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/CvY;

    iget-object v0, p0, LX/EJe;->b:LX/1Pn;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    sget-object v10, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v11, LX/7CM;->MESSAGE_SELLER:LX/7CM;

    iget-object v0, p0, LX/EJe;->b:LX/1Pn;

    check-cast v0, LX/CxG;

    iget-object v1, p0, LX/EJe;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-interface {v0, v1}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v12

    iget-object v13, p0, LX/EJe;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    iget-object v0, p0, LX/EJe;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/EJe;->b:LX/1Pn;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    sget-object v1, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v2, LX/7CM;->MESSAGE_SELLER:LX/7CM;

    iget-object v3, p0, LX/EJe;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2104807
    iget-object v4, v3, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v3, v4

    .line 2104808
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iget-object v5, p0, LX/EJe;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2104809
    iget-object v6, v5, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v5, v6

    .line 2104810
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v0, v7

    move-object v1, v9

    move-object v2, v10

    move-object v3, v11

    move v4, v12

    move-object v5, v13

    invoke-virtual/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2104811
    const/4 v0, 0x2

    const/4 v1, 0x2

    const v2, 0x33de8bff

    invoke-static {v0, v1, v2, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
