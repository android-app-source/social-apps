.class public final LX/EuC;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EuD;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

.field public b:Z

.field public c:LX/2h7;

.field public final synthetic d:LX/EuD;


# direct methods
.method public constructor <init>(LX/EuD;)V
    .locals 1

    .prologue
    .line 2179162
    iput-object p1, p0, LX/EuC;->d:LX/EuD;

    .line 2179163
    move-object v0, p1

    .line 2179164
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2179165
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2179166
    const-string v0, "FriendListContentText"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2179167
    if-ne p0, p1, :cond_1

    .line 2179168
    :cond_0
    :goto_0
    return v0

    .line 2179169
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2179170
    goto :goto_0

    .line 2179171
    :cond_3
    check-cast p1, LX/EuC;

    .line 2179172
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2179173
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2179174
    if-eq v2, v3, :cond_0

    .line 2179175
    iget-object v2, p0, LX/EuC;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EuC;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    iget-object v3, p1, LX/EuC;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2179176
    goto :goto_0

    .line 2179177
    :cond_5
    iget-object v2, p1, LX/EuC;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    if-nez v2, :cond_4

    .line 2179178
    :cond_6
    iget-boolean v2, p0, LX/EuC;->b:Z

    iget-boolean v3, p1, LX/EuC;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2179179
    goto :goto_0

    .line 2179180
    :cond_7
    iget-object v2, p0, LX/EuC;->c:LX/2h7;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EuC;->c:LX/2h7;

    iget-object v3, p1, LX/EuC;->c:LX/2h7;

    invoke-virtual {v2, v3}, LX/2h7;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2179181
    goto :goto_0

    .line 2179182
    :cond_8
    iget-object v2, p1, LX/EuC;->c:LX/2h7;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
