.class public final LX/E76;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

.field public final synthetic b:Lcom/facebook/fbui/glyph/GlyphView;

.field public final synthetic c:Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Lcom/facebook/fbui/glyph/GlyphView;)V
    .locals 0

    .prologue
    .line 2081072
    iput-object p1, p0, LX/E76;->c:Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;

    iput-object p2, p0, LX/E76;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    iput-object p3, p0, LX/E76;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    const v1, -0x169cb60d

    invoke-static {v0, v8, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v10

    .line 2081073
    iget-object v0, p0, LX/E76;->c:Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;

    iget-object v0, v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->e:Ljava/util/HashMap;

    iget-object v1, p0, LX/E76;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v1

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 2081074
    if-nez v5, :cond_1

    move v2, v8

    .line 2081075
    :goto_0
    iget-object v0, p0, LX/E76;->c:Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;

    iget-object v0, v0, Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;->d:LX/961;

    iget-object v1, p0, LX/E76;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->M()LX/5sc;

    move-result-object v1

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    const-string v4, "reaction_dialog"

    new-instance v9, LX/E75;

    invoke-direct {v9, p0, v2, v5}, LX/E75;-><init>(LX/E76;ZZ)V

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v9}, LX/961;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;ZLX/1L9;)V

    .line 2081076
    if-eqz v2, :cond_0

    .line 2081077
    iget-object v0, p0, LX/E76;->c:Lcom/facebook/reaction/ui/attachment/handler/ReactionPageYouMayLikeHscrollHandler;

    .line 2081078
    iget-object v1, v0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 2081079
    iget-object v2, v0, LX/E6d;->b:LX/E6b;

    invoke-virtual {v2}, LX/E6b;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 2081080
    iget-object v2, v0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2081081
    :cond_0
    const v0, 0x1be2c4af

    invoke-static {v0, v10}, LX/02F;->a(II)V

    return-void

    .line 2081082
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
