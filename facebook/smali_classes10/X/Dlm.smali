.class public final enum LX/Dlm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dlm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dlm;

.field public static final enum BLUE_BACKGROUND:LX/Dlm;

.field public static final enum GREEN_BACKGROUND:LX/Dlm;

.field public static final enum ORANGE_BACKGROUND:LX/Dlm;

.field public static final enum RED_BACKGROUND:LX/Dlm;

.field public static final enum RED_TITLE:LX/Dlm;

.field public static final enum WHITE_BACKGROUND:LX/Dlm;


# instance fields
.field public final backgroundColorResId:I

.field public final photoTintColorResId:I

.field public final subtitleColorResId:I

.field public final titleColorResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 2038655
    new-instance v0, LX/Dlm;

    const-string v1, "BLUE_BACKGROUND"

    const v3, 0x7f0a07f7

    const v4, 0x7f0a07f9

    const v5, 0x7f0a07fb

    const v6, 0x7f0a07f9

    invoke-direct/range {v0 .. v6}, LX/Dlm;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, LX/Dlm;->BLUE_BACKGROUND:LX/Dlm;

    .line 2038656
    new-instance v3, LX/Dlm;

    const-string v4, "GREEN_BACKGROUND"

    const v6, 0x7f0a07f0

    const v7, 0x7f0a07f9

    const v8, 0x7f0a07fb

    const v9, 0x7f0a07f9

    move v5, v10

    invoke-direct/range {v3 .. v9}, LX/Dlm;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, LX/Dlm;->GREEN_BACKGROUND:LX/Dlm;

    .line 2038657
    new-instance v3, LX/Dlm;

    const-string v4, "ORANGE_BACKGROUND"

    const v6, 0x7f0a07f1

    const v7, 0x7f0a07f9

    const v8, 0x7f0a07fb

    const v9, 0x7f0a07f9

    move v5, v11

    invoke-direct/range {v3 .. v9}, LX/Dlm;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, LX/Dlm;->ORANGE_BACKGROUND:LX/Dlm;

    .line 2038658
    new-instance v3, LX/Dlm;

    const-string v4, "RED_BACKGROUND"

    const v6, 0x7f0a07f2

    const v7, 0x7f0a07f9

    const v8, 0x7f0a07fb

    const v9, 0x7f0a07f9

    move v5, v12

    invoke-direct/range {v3 .. v9}, LX/Dlm;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, LX/Dlm;->RED_BACKGROUND:LX/Dlm;

    .line 2038659
    new-instance v3, LX/Dlm;

    const-string v4, "WHITE_BACKGROUND"

    const v6, 0x7f0a07f9

    const v7, 0x7f0a07f8

    const v8, 0x7f0a07fa

    const v9, 0x7f0a07f8

    move v5, v13

    invoke-direct/range {v3 .. v9}, LX/Dlm;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, LX/Dlm;->WHITE_BACKGROUND:LX/Dlm;

    .line 2038660
    new-instance v3, LX/Dlm;

    const-string v4, "RED_TITLE"

    const/4 v5, 0x5

    const v6, 0x7f0a07f9

    const v7, 0x7f0a07f2

    const v8, 0x7f0a07fa

    const v9, 0x7f0a07f8

    invoke-direct/range {v3 .. v9}, LX/Dlm;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, LX/Dlm;->RED_TITLE:LX/Dlm;

    .line 2038661
    const/4 v0, 0x6

    new-array v0, v0, [LX/Dlm;

    sget-object v1, LX/Dlm;->BLUE_BACKGROUND:LX/Dlm;

    aput-object v1, v0, v2

    sget-object v1, LX/Dlm;->GREEN_BACKGROUND:LX/Dlm;

    aput-object v1, v0, v10

    sget-object v1, LX/Dlm;->ORANGE_BACKGROUND:LX/Dlm;

    aput-object v1, v0, v11

    sget-object v1, LX/Dlm;->RED_BACKGROUND:LX/Dlm;

    aput-object v1, v0, v12

    sget-object v1, LX/Dlm;->WHITE_BACKGROUND:LX/Dlm;

    aput-object v1, v0, v13

    const/4 v1, 0x5

    sget-object v2, LX/Dlm;->RED_TITLE:LX/Dlm;

    aput-object v2, v0, v1

    sput-object v0, LX/Dlm;->$VALUES:[LX/Dlm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII)V"
        }
    .end annotation

    .prologue
    .line 2038662
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2038663
    iput p3, p0, LX/Dlm;->backgroundColorResId:I

    .line 2038664
    iput p4, p0, LX/Dlm;->titleColorResId:I

    .line 2038665
    iput p5, p0, LX/Dlm;->subtitleColorResId:I

    .line 2038666
    iput p6, p0, LX/Dlm;->photoTintColorResId:I

    .line 2038667
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dlm;
    .locals 1

    .prologue
    .line 2038668
    const-class v0, LX/Dlm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dlm;

    return-object v0
.end method

.method public static values()[LX/Dlm;
    .locals 1

    .prologue
    .line 2038669
    sget-object v0, LX/Dlm;->$VALUES:[LX/Dlm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dlm;

    return-object v0
.end method
