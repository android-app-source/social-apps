.class public final LX/Eqg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/EventsExtendedInviteFragment;)V
    .locals 0

    .prologue
    .line 2171753
    iput-object p1, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/Eqg;I)V
    .locals 7

    .prologue
    .line 2171754
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->q:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_2

    .line 2171755
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    .line 2171756
    iput p1, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->q:I

    .line 2171757
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->g:LX/Er0;

    if-eqz v0, :cond_0

    .line 2171758
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->g:LX/Er0;

    iget-object v1, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget v1, v1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->q:I

    .line 2171759
    iput v1, v0, LX/Er0;->d:I

    .line 2171760
    :cond_0
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    iget-object v1, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget v1, v1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->q:I

    .line 2171761
    iput v1, v0, LX/ErB;->m:I

    .line 2171762
    iget-object v3, v0, LX/ErB;->g:[LX/Er9;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2171763
    if-eqz v5, :cond_1

    .line 2171764
    iget v6, v0, LX/ErB;->m:I

    .line 2171765
    iget-object p0, v5, LX/Er9;->p:LX/0Px;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    const/4 p0, 0x0

    move p1, p0

    :goto_1
    if-ge p1, v1, :cond_1

    iget-object p0, v5, LX/Er9;->p:LX/0Px;

    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Eqx;

    .line 2171766
    iput v6, p0, LX/Eqx;->f:I

    .line 2171767
    add-int/lit8 p0, p1, 0x1

    move p1, p0

    goto :goto_1

    .line 2171768
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2171769
    :cond_2
    return-void
.end method

.method public static b(LX/Eqg;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2171770
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    .line 2171771
    iput-object p1, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->s:LX/0Px;

    .line 2171772
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->i:LX/ErM;

    if-eqz v0, :cond_0

    .line 2171773
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->i:LX/ErM;

    iget-object v1, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v1, v1, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->s:LX/0Px;

    invoke-virtual {v0, v1}, LX/ErM;->a(LX/0Px;)V

    .line 2171774
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2171775
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->D:LX/ErU;

    invoke-virtual {v0}, LX/ErU;->b()V

    .line 2171776
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->s:LX/0Px;

    invoke-static {p0, v0}, LX/Eqg;->b(LX/Eqg;LX/0Px;)V

    .line 2171777
    iget-object v0, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->h:LX/ErB;

    const/4 v1, 0x1

    iget-object v2, p0, LX/Eqg;->a:Lcom/facebook/events/invite/EventsExtendedInviteFragment;

    iget-object v2, v2, Lcom/facebook/events/invite/EventsExtendedInviteFragment;->s:LX/0Px;

    .line 2171778
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2171779
    invoke-virtual {v0, v1, v2, v3}, LX/ErB;->a(ZLX/0Px;LX/0Px;)V

    .line 2171780
    return-void
.end method
