.class public final LX/EiB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;)V
    .locals 0

    .prologue
    .line 2159058
    iput-object p1, p0, LX/EiB;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2159059
    iget-object v0, p0, LX/EiB;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v0, v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->q:LX/2U9;

    iget-object v1, p0, LX/EiB;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v1, v1, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->B:Lcom/facebook/growth/model/Contactpoint;

    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2159060
    iget-object v2, v0, LX/2U9;->a:LX/0Zb;

    sget-object p1, LX/Eiw;->LOGOUT_CLICK:LX/Eiw;

    invoke-virtual {p1}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    invoke-interface {v2, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2159061
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2159062
    const-string p1, "confirmation"

    invoke-virtual {v2, p1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159063
    const-string p1, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159064
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2159065
    :cond_0
    iget-object v0, p0, LX/EiB;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    iget-object v0, v0, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->r:LX/GvB;

    iget-object v1, p0, LX/EiB;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    invoke-virtual {v0, v1}, LX/GvB;->a(Landroid/content/Context;)V

    .line 2159066
    iget-object v0, p0, LX/EiB;->a:Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    invoke-virtual {v0}, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;->finish()V

    .line 2159067
    return-void
.end method
