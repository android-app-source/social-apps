.class public LX/E5d;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E5b;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/E5d;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E5e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078725
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5d;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E5e;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078722
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078723
    iput-object p1, p0, LX/E5d;->b:LX/0Ot;

    .line 2078724
    return-void
.end method

.method public static a(LX/0QB;)LX/E5d;
    .locals 4

    .prologue
    .line 2078709
    sget-object v0, LX/E5d;->c:LX/E5d;

    if-nez v0, :cond_1

    .line 2078710
    const-class v1, LX/E5d;

    monitor-enter v1

    .line 2078711
    :try_start_0
    sget-object v0, LX/E5d;->c:LX/E5d;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078712
    if-eqz v2, :cond_0

    .line 2078713
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078714
    new-instance v3, LX/E5d;

    const/16 p0, 0x312f

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E5d;-><init>(LX/0Ot;)V

    .line 2078715
    move-object v0, v3

    .line 2078716
    sput-object v0, LX/E5d;->c:LX/E5d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078717
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078718
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078719
    :cond_1
    sget-object v0, LX/E5d;->c:LX/E5d;

    return-object v0

    .line 2078720
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078721
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2078702
    check-cast p2, LX/E5c;

    .line 2078703
    iget-object v0, p0, LX/E5d;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/E5c;->a:Ljava/lang/String;

    iget-boolean v1, p2, LX/E5c;->b:Z

    const/4 v5, 0x2

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    .line 2078704
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    if-eqz v1, :cond_0

    move v2, v3

    :goto_0
    invoke-interface {v5, v2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x6

    const p0, 0x7f010717

    invoke-interface {v2, v5, p0}, LX/1Dh;->p(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->c(Z)LX/1Dh;

    move-result-object v2

    const v5, 0x7f01072b

    invoke-interface {v2, v5}, LX/1Dh;->U(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const p0, 0x7f01071b

    .line 2078705
    iget-object p2, v2, LX/1ne;->a:LX/1nb;

    const/4 p1, 0x0

    invoke-virtual {v2, p0, p1}, LX/1Dp;->b(II)I

    move-result p1

    iput p1, p2, LX/1nb;->n:I

    .line 2078706
    move-object v2, v2

    .line 2078707
    const p0, 0x7f0a0a53

    invoke-virtual {v2, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    :goto_1
    invoke-interface {p0, v2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v2

    const/16 v4, 0x8

    const p0, 0x7f0b163a

    invoke-interface {v2, v4, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Di;->a(Z)LX/1Di;

    move-result-object v2

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2078708
    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2078700
    invoke-static {}, LX/1dS;->b()V

    .line 2078701
    const/4 v0, 0x0

    return-object v0
.end method
