.class public LX/DCc;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

.field public final c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1974256
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1974257
    const/4 v0, -0x1

    iput v0, p0, LX/DCc;->d:I

    .line 1974258
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DCc;->e:Z

    .line 1974259
    const v0, 0x7f030b9a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1974260
    const v0, 0x7f0d1cc1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iput-object v0, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 1974261
    new-instance v0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    invoke-virtual {p0}, LX/DCc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/DCc;->b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    .line 1974262
    iget-object v0, p0, LX/DCc;->b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    const v1, 0x7f0d00fe

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->setId(I)V

    .line 1974263
    iget-object v0, p0, LX/DCc;->b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    invoke-virtual {p0, v0}, LX/DCc;->addView(Landroid/view/View;)V

    .line 1974264
    return-void
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1974251
    iget-object v0, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {p0}, LX/DCc;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, LX/DCc;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->layout(IIII)V

    .line 1974252
    iget v0, p0, LX/DCc;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1974253
    iget-object v0, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget v1, p0, LX/DCc;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 1974254
    iget-object v1, p0, LX/DCc;->b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    iget-object v2, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingLeft()I

    move-result v2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v3

    iget-object v3, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v3}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingTop()I

    move-result v3

    iget v4, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iget-object v4, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingLeft()I

    move-result v4

    iget v5, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    iget-object v5, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v5}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getPaddingTop()I

    move-result v5

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->layout(IIII)V

    .line 1974255
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1974229
    iget-object v0, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->measure(II)V

    .line 1974230
    iget v0, p0, LX/DCc;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1974231
    iget-object v0, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget v1, p0, LX/DCc;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 1974232
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1974233
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1974234
    iget-object v2, p0, LX/DCc;->b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->measure(II)V

    .line 1974235
    :cond_0
    iget-boolean v0, p0, LX/DCc;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DCc;->a:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->d()I

    move-result v0

    .line 1974236
    :goto_0
    iget-object v1, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, LX/DCc;->setMeasuredDimension(II)V

    .line 1974237
    return-void

    .line 1974238
    :cond_1
    iget-object v0, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method public setDialtoneEnabled(Z)V
    .locals 1

    .prologue
    .line 1974246
    iput-boolean p1, p0, LX/DCc;->e:Z

    .line 1974247
    iget-object v0, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 1974248
    iput-boolean p1, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->v:Z

    .line 1974249
    invoke-virtual {p0}, LX/DCc;->invalidate()V

    .line 1974250
    return-void
.end method

.method public setVideoIndex(I)V
    .locals 2

    .prologue
    .line 1974239
    iput p1, p0, LX/DCc;->d:I

    .line 1974240
    iget-object v0, p0, LX/DCc;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 1974241
    iput p1, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->x:I

    .line 1974242
    iget v0, p0, LX/DCc;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1974243
    iget-object v0, p0, LX/DCc;->b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->setVisibility(I)V

    .line 1974244
    :goto_0
    return-void

    .line 1974245
    :cond_0
    iget-object v0, p0, LX/DCc;->b:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->setVisibility(I)V

    goto :goto_0
.end method
