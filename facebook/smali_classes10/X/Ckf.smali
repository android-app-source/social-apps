.class public final enum LX/Ckf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ckf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ckf;

.field public static final enum ABOVE:LX/Ckf;

.field public static final enum BASE_ON_GRID_LINE:LX/Ckf;

.field public static final enum BELOW:LX/Ckf;

.field public static final enum BOTTOM:LX/Ckf;

.field public static final enum BOTTOM_FLUSH:LX/Ckf;

.field public static final enum CENTERED_IN:LX/Ckf;

.field public static final enum CENTERED_IN_GRID_LINE:LX/Ckf;

.field public static final enum TOP:LX/Ckf;

.field public static final enum TOP_FLUSH:LX/Ckf;

.field private static final sTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Ckf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1931206
    new-instance v0, LX/Ckf;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, LX/Ckf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckf;->TOP:LX/Ckf;

    .line 1931207
    new-instance v0, LX/Ckf;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v4}, LX/Ckf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckf;->BOTTOM:LX/Ckf;

    .line 1931208
    new-instance v0, LX/Ckf;

    const-string v1, "TOP_FLUSH"

    invoke-direct {v0, v1, v5}, LX/Ckf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckf;->TOP_FLUSH:LX/Ckf;

    .line 1931209
    new-instance v0, LX/Ckf;

    const-string v1, "BOTTOM_FLUSH"

    invoke-direct {v0, v1, v6}, LX/Ckf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckf;->BOTTOM_FLUSH:LX/Ckf;

    .line 1931210
    new-instance v0, LX/Ckf;

    const-string v1, "ABOVE"

    invoke-direct {v0, v1, v7}, LX/Ckf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckf;->ABOVE:LX/Ckf;

    .line 1931211
    new-instance v0, LX/Ckf;

    const-string v1, "BELOW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Ckf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckf;->BELOW:LX/Ckf;

    .line 1931212
    new-instance v0, LX/Ckf;

    const-string v1, "BASE_ON_GRID_LINE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Ckf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckf;->BASE_ON_GRID_LINE:LX/Ckf;

    .line 1931213
    new-instance v0, LX/Ckf;

    const-string v1, "CENTERED_IN_GRID_LINE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Ckf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckf;->CENTERED_IN_GRID_LINE:LX/Ckf;

    .line 1931214
    new-instance v0, LX/Ckf;

    const-string v1, "CENTERED_IN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Ckf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckf;->CENTERED_IN:LX/Ckf;

    .line 1931215
    const/16 v0, 0x9

    new-array v0, v0, [LX/Ckf;

    sget-object v1, LX/Ckf;->TOP:LX/Ckf;

    aput-object v1, v0, v3

    sget-object v1, LX/Ckf;->BOTTOM:LX/Ckf;

    aput-object v1, v0, v4

    sget-object v1, LX/Ckf;->TOP_FLUSH:LX/Ckf;

    aput-object v1, v0, v5

    sget-object v1, LX/Ckf;->BOTTOM_FLUSH:LX/Ckf;

    aput-object v1, v0, v6

    sget-object v1, LX/Ckf;->ABOVE:LX/Ckf;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Ckf;->BELOW:LX/Ckf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Ckf;->BASE_ON_GRID_LINE:LX/Ckf;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Ckf;->CENTERED_IN_GRID_LINE:LX/Ckf;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Ckf;->CENTERED_IN:LX/Ckf;

    aput-object v2, v0, v1

    sput-object v0, LX/Ckf;->$VALUES:[LX/Ckf;

    .line 1931216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1931217
    sput-object v0, LX/Ckf;->sTypeMap:Ljava/util/Map;

    const-string v1, "top"

    sget-object v2, LX/Ckf;->TOP:LX/Ckf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931218
    sget-object v0, LX/Ckf;->sTypeMap:Ljava/util/Map;

    const-string v1, "bottom"

    sget-object v2, LX/Ckf;->BOTTOM:LX/Ckf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931219
    sget-object v0, LX/Ckf;->sTypeMap:Ljava/util/Map;

    const-string v1, "top_flush"

    sget-object v2, LX/Ckf;->TOP_FLUSH:LX/Ckf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931220
    sget-object v0, LX/Ckf;->sTypeMap:Ljava/util/Map;

    const-string v1, "bottom_flush"

    sget-object v2, LX/Ckf;->BOTTOM_FLUSH:LX/Ckf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931221
    sget-object v0, LX/Ckf;->sTypeMap:Ljava/util/Map;

    const-string v1, "above"

    sget-object v2, LX/Ckf;->ABOVE:LX/Ckf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931222
    sget-object v0, LX/Ckf;->sTypeMap:Ljava/util/Map;

    const-string v1, "below"

    sget-object v2, LX/Ckf;->BELOW:LX/Ckf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931223
    sget-object v0, LX/Ckf;->sTypeMap:Ljava/util/Map;

    const-string v1, "base_on_gridline"

    sget-object v2, LX/Ckf;->BASE_ON_GRID_LINE:LX/Ckf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931224
    sget-object v0, LX/Ckf;->sTypeMap:Ljava/util/Map;

    const-string v1, "centered_in_gridline"

    sget-object v2, LX/Ckf;->CENTERED_IN_GRID_LINE:LX/Ckf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931225
    sget-object v0, LX/Ckf;->sTypeMap:Ljava/util/Map;

    const-string v1, "centered_in"

    sget-object v2, LX/Ckf;->CENTERED_IN:LX/Ckf;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931226
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1931235
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/Ckf;
    .locals 1

    .prologue
    .line 1931230
    if-nez p0, :cond_1

    .line 1931231
    sget-object v0, LX/Ckf;->TOP:LX/Ckf;

    .line 1931232
    :cond_0
    :goto_0
    return-object v0

    .line 1931233
    :cond_1
    sget-object v0, LX/Ckf;->sTypeMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckf;

    .line 1931234
    if-nez v0, :cond_0

    sget-object v0, LX/Ckf;->TOP:LX/Ckf;

    goto :goto_0
.end method

.method public static hasElementArgument(LX/Ckf;)Z
    .locals 1

    .prologue
    .line 1931236
    sget-object v0, LX/Ckf;->ABOVE:LX/Ckf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/Ckf;->BELOW:LX/Ckf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/Ckf;->CENTERED_IN:LX/Ckf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasGridArgument(LX/Ckf;)Z
    .locals 1

    .prologue
    .line 1931229
    sget-object v0, LX/Ckf;->BASE_ON_GRID_LINE:LX/Ckf;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/Ckf;->CENTERED_IN_GRID_LINE:LX/Ckf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ckf;
    .locals 1

    .prologue
    .line 1931228
    const-class v0, LX/Ckf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ckf;

    return-object v0
.end method

.method public static values()[LX/Ckf;
    .locals 1

    .prologue
    .line 1931227
    sget-object v0, LX/Ckf;->$VALUES:[LX/Ckf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ckf;

    return-object v0
.end method
