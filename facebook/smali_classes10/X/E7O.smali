.class public LX/E7O;
.super LX/Cfk;
.source ""


# direct methods
.method public constructor <init>(LX/3Tx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081480
    invoke-direct {p0, p1}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2081481
    return-void
.end method

.method private static a(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2081491
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;

    .line 2081492
    invoke-virtual {v0, p2}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setTitle(Ljava/lang/String;)V

    .line 2081493
    invoke-virtual {v0, p3}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setLabel(Ljava/lang/String;)V

    .line 2081494
    const v1, 0x7f0e08f8

    invoke-virtual {v0, v1}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setTitleTextAppearance(I)V

    .line 2081495
    const v1, 0x7f0e08f9

    invoke-virtual {v0, v1}, Lcom/facebook/reaction/feed/rows/ui/ReactionTitleAndLabelView;->setLabelTextAppearance(I)V

    .line 2081496
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 1

    .prologue
    .line 2081497
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2081483
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->y()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    .line 2081484
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->x()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 2081485
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->T()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    .line 2081486
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->S()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    .line 2081487
    const v4, 0x7f031117

    invoke-virtual {p0, v4}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v4

    .line 2081488
    const v5, 0x7f0d2887

    invoke-static {v4, v5, v0, v1}, LX/E7O;->a(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    .line 2081489
    const v0, 0x7f0d2889

    invoke-static {v4, v0, v2, v3}, LX/E7O;->a(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    .line 2081490
    return-object v4
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2081482
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->y()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->y()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->x()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->x()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->T()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->T()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->S()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->S()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
