.class public final enum LX/ETK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ETK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ETK;

.field public static final enum EMPTY:LX/ETK;

.field public static final enum MULTIPLE:LX/ETK;

.field public static final enum SINGLE:LX/ETK;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2124685
    new-instance v0, LX/ETK;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v2}, LX/ETK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ETK;->EMPTY:LX/ETK;

    .line 2124686
    new-instance v0, LX/ETK;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v3}, LX/ETK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ETK;->SINGLE:LX/ETK;

    .line 2124687
    new-instance v0, LX/ETK;

    const-string v1, "MULTIPLE"

    invoke-direct {v0, v1, v4}, LX/ETK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ETK;->MULTIPLE:LX/ETK;

    .line 2124688
    const/4 v0, 0x3

    new-array v0, v0, [LX/ETK;

    sget-object v1, LX/ETK;->EMPTY:LX/ETK;

    aput-object v1, v0, v2

    sget-object v1, LX/ETK;->SINGLE:LX/ETK;

    aput-object v1, v0, v3

    sget-object v1, LX/ETK;->MULTIPLE:LX/ETK;

    aput-object v1, v0, v4

    sput-object v0, LX/ETK;->$VALUES:[LX/ETK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2124689
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ETK;
    .locals 1

    .prologue
    .line 2124690
    const-class v0, LX/ETK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ETK;

    return-object v0
.end method

.method public static values()[LX/ETK;
    .locals 1

    .prologue
    .line 2124691
    sget-object v0, LX/ETK;->$VALUES:[LX/ETK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ETK;

    return-object v0
.end method
