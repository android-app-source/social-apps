.class public LX/Cqp;
.super LX/Cqj;
.source ""


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 1940466
    invoke-direct {p0, p1, p2}, LX/Cqj;-><init>(LX/Ctg;LX/CrK;)V

    .line 1940467
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cqp;->a:Z

    .line 1940468
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->a:LX/Cqw;

    .line 1940469
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 1940470
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940471
    new-instance v0, LX/Cqn;

    sget-object v2, LX/Cqw;->b:LX/Cqw;

    .line 1940472
    iget-object v1, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v3, v1

    .line 1940473
    check-cast v3, LX/Ctg;

    sget-object v4, LX/Cqd;->MEDIA_FULLSCREEN:LX/Cqd;

    sget-object v5, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v6, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/Cqn;-><init>(LX/Cqp;LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 1940474
    return-void
.end method


# virtual methods
.method public final h()V
    .locals 2

    .prologue
    .line 1940476
    invoke-super {p0}, LX/Cqj;->h()V

    .line 1940477
    invoke-virtual {p0}, LX/CqX;->d()LX/Cqv;

    move-result-object v0

    sget-object v1, LX/Cqw;->a:LX/Cqw;

    if-eq v0, v1, :cond_0

    .line 1940478
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    invoke-virtual {p0, v0}, LX/CqX;->e(LX/Cqv;)V

    .line 1940479
    :cond_0
    return-void
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 1940475
    iget-boolean v0, p0, LX/Cqp;->a:Z

    return v0
.end method
