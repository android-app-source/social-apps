.class public LX/ELF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ELD;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2108344
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/ELF;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108345
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2108346
    iput-object p1, p0, LX/ELF;->b:LX/0Ot;

    .line 2108347
    return-void
.end method

.method public static a(LX/0QB;)LX/ELF;
    .locals 4

    .prologue
    .line 2108331
    const-class v1, LX/ELF;

    monitor-enter v1

    .line 2108332
    :try_start_0
    sget-object v0, LX/ELF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2108333
    sput-object v2, LX/ELF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2108334
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2108335
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2108336
    new-instance v3, LX/ELF;

    const/16 p0, 0x33eb

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ELF;-><init>(LX/0Ot;)V

    .line 2108337
    move-object v0, v3

    .line 2108338
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2108339
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ELF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2108340
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2108341
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2108342
    const v0, -0x7105b598

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2108343
    const v0, -0x71058aba

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2108302
    check-cast p2, LX/ELE;

    .line 2108303
    iget-object v0, p0, LX/ELF;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;

    iget-object v2, p2, LX/ELE;->a:Landroid/net/Uri;

    iget-object v3, p2, LX/ELE;->b:Ljava/lang/String;

    iget-object v4, p2, LX/ELE;->c:Ljava/lang/CharSequence;

    iget-object v5, p2, LX/ELE;->d:Ljava/lang/CharSequence;

    iget-object v6, p2, LX/ELE;->e:Ljava/lang/CharSequence;

    iget-object v7, p2, LX/ELE;->f:LX/1dc;

    iget-boolean v8, p2, LX/ELE;->g:Z

    iget-object v9, p2, LX/ELE;->h:Ljava/lang/CharSequence;

    iget-object v10, p2, LX/ELE;->i:Landroid/view/View$OnClickListener;

    iget-object v11, p2, LX/ELE;->j:Ljava/lang/CharSequence;

    iget-object v12, p2, LX/ELE;->k:Landroid/view/View$OnClickListener;

    move-object v1, p1

    invoke-virtual/range {v0 .. v12}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->a(LX/1De;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/1dc;ZLjava/lang/CharSequence;Landroid/view/View$OnClickListener;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)LX/1Dg;

    move-result-object v0

    .line 2108304
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2108305
    invoke-static {}, LX/1dS;->b()V

    .line 2108306
    iget v0, p1, LX/1dQ;->b:I

    .line 2108307
    sparse-switch v0, :sswitch_data_0

    .line 2108308
    :goto_0
    return-object v2

    .line 2108309
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2108310
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2108311
    check-cast v1, LX/ELE;

    .line 2108312
    iget-object p1, p0, LX/ELF;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/ELE;->l:Landroid/view/View$OnClickListener;

    .line 2108313
    if-eqz p1, :cond_0

    .line 2108314
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2108315
    :cond_0
    goto :goto_0

    .line 2108316
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2108317
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2108318
    check-cast v1, LX/ELE;

    .line 2108319
    iget-object p1, p0, LX/ELF;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/ELE;->m:Landroid/view/View$OnClickListener;

    .line 2108320
    if-eqz p1, :cond_1

    .line 2108321
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2108322
    :cond_1
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7105b598 -> :sswitch_0
        -0x71058aba -> :sswitch_1
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/ELD;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2108323
    new-instance v1, LX/ELE;

    invoke-direct {v1, p0}, LX/ELE;-><init>(LX/ELF;)V

    .line 2108324
    sget-object v2, LX/ELF;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ELD;

    .line 2108325
    if-nez v2, :cond_0

    .line 2108326
    new-instance v2, LX/ELD;

    invoke-direct {v2}, LX/ELD;-><init>()V

    .line 2108327
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/ELD;->a$redex0(LX/ELD;LX/1De;IILX/ELE;)V

    .line 2108328
    move-object v1, v2

    .line 2108329
    move-object v0, v1

    .line 2108330
    return-object v0
.end method
