.class public final LX/EWE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Landroid/graphics/Rect;

.field public final synthetic b:LX/EWG;


# direct methods
.method public constructor <init>(LX/EWG;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 2129776
    iput-object p1, p0, LX/EWE;->b:LX/EWG;

    iput-object p2, p0, LX/EWE;->a:Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 2129777
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    .line 2129778
    const/high16 v0, 0x3f800000    # 1.0f

    sub-double v4, v2, v8

    iget-object v1, p0, LX/EWE;->b:LX/EWG;

    iget-wide v6, v1, LX/EWG;->a:D

    sub-double/2addr v6, v8

    div-double/2addr v4, v6

    double-to-float v1, v4

    sub-float/2addr v0, v1

    .line 2129779
    iget-object v4, p0, LX/EWE;->a:Landroid/graphics/Rect;

    iget-object v5, p0, LX/EWE;->b:LX/EWG;

    iget-object v5, v5, LX/EWG;->f:Landroid/graphics/Rect;

    .line 2129780
    new-instance v6, Landroid/graphics/Rect;

    iget v7, v4, Landroid/graphics/Rect;->left:I

    iget v8, v5, Landroid/graphics/Rect;->left:I

    invoke-static {v7, v8, v0}, LX/EWG;->a(IIF)I

    move-result v7

    iget v8, v4, Landroid/graphics/Rect;->top:I

    iget v9, v5, Landroid/graphics/Rect;->top:I

    invoke-static {v8, v9, v0}, LX/EWG;->a(IIF)I

    move-result v8

    iget v9, v4, Landroid/graphics/Rect;->right:I

    iget p1, v5, Landroid/graphics/Rect;->right:I

    invoke-static {v9, p1, v0}, LX/EWG;->a(IIF)I

    move-result v9

    iget p1, v4, Landroid/graphics/Rect;->bottom:I

    iget v1, v5, Landroid/graphics/Rect;->bottom:I

    invoke-static {p1, v1, v0}, LX/EWG;->a(IIF)I

    move-result p1

    invoke-direct {v6, v7, v8, v9, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v6, v6

    .line 2129781
    move-object v0, v6

    .line 2129782
    iget-object v1, p0, LX/EWE;->b:LX/EWG;

    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-static {v1, v4, v0}, LX/EWG;->b(LX/EWG;II)V

    .line 2129783
    iget-object v0, p0, LX/EWE;->b:LX/EWG;

    iget-object v0, v0, LX/EWG;->d:Landroid/view/View;

    check-cast v0, LX/DaW;

    invoke-interface {v0, v2, v3}, LX/DaW;->a(D)V

    .line 2129784
    iget-object v0, p0, LX/EWE;->b:LX/EWG;

    iget-object v0, v0, LX/EWG;->g:LX/EW4;

    invoke-virtual {v0}, LX/EW4;->a()V

    .line 2129785
    return-void
.end method
