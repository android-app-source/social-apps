.class public LX/ErC;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/ErB;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172676
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2172677
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Set;LX/Eqe;LX/EqG;LX/1OX;LX/1OX;LX/0gc;)LX/ErB;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/events/invite/EventsExtendedInviteFragment$AddContactsButtonClickListener;",
            "Lcom/facebook/events/invite/EventsExtendedInviteFriendSelectionChangedListener;",
            "LX/1OX;",
            "LX/1OX;",
            "LX/0gc;",
            ")",
            "LX/ErB;"
        }
    .end annotation

    .prologue
    .line 2172678
    new-instance v0, LX/ErB;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    const-class v1, LX/ErA;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/ErA;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    const/16 v1, 0x15e7

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v11}, LX/ErB;-><init>(Ljava/util/Set;LX/Eqe;LX/EqG;LX/1OX;LX/1OX;LX/0gc;Landroid/content/Context;LX/ErA;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0Or;)V

    .line 2172679
    return-object v0
.end method
