.class public LX/Cta;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/CtX;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CtX;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/ClK;

.field public d:LX/CtZ;


# direct methods
.method public constructor <init>(LX/ClK;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1945069
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1945070
    new-instance v0, Ljava/util/PriorityQueue;

    const/16 v1, 0xa

    new-instance v2, LX/CtW;

    invoke-direct {v2}, LX/CtW;-><init>()V

    invoke-direct {v0, v1, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v0, p0, LX/Cta;->a:Ljava/util/Queue;

    .line 1945071
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Cta;->b:Ljava/util/List;

    .line 1945072
    sget-object v0, LX/CtZ;->PAUSED:LX/CtZ;

    iput-object v0, p0, LX/Cta;->d:LX/CtZ;

    .line 1945073
    iput-object p1, p0, LX/Cta;->c:LX/ClK;

    .line 1945074
    return-void
.end method

.method public static a(LX/0QB;)LX/Cta;
    .locals 4

    .prologue
    .line 1945075
    const-class v1, LX/Cta;

    monitor-enter v1

    .line 1945076
    :try_start_0
    sget-object v0, LX/Cta;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1945077
    sput-object v2, LX/Cta;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1945078
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1945079
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1945080
    new-instance p0, LX/Cta;

    invoke-static {v0}, LX/ClK;->a(LX/0QB;)LX/ClK;

    move-result-object v3

    check-cast v3, LX/ClK;

    invoke-direct {p0, v3}, LX/Cta;-><init>(LX/ClK;)V

    .line 1945081
    move-object v0, p0

    .line 1945082
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1945083
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Cta;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1945084
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1945085
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/Cta;LX/CtX;)V
    .locals 1

    .prologue
    .line 1945086
    iget-object v0, p0, LX/Cta;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 1945087
    iget-object v0, p0, LX/Cta;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1945088
    invoke-static {p0}, LX/Cta;->d(LX/Cta;)V

    .line 1945089
    return-void
.end method

.method public static declared-synchronized b(LX/Cta;LX/CtX;)V
    .locals 1

    .prologue
    .line 1945090
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cta;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1945091
    monitor-exit p0

    return-void

    .line 1945092
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(LX/CtX;)V
    .locals 6

    .prologue
    .line 1945093
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CsX;

    new-instance v1, LX/CtV;

    invoke-direct {v1, p0, p1}, LX/CtV;-><init>(LX/Cta;LX/CtX;)V

    invoke-virtual {v0, v1}, LX/CsV;->a(LX/Csq;)V

    .line 1945094
    iget-object v1, p0, LX/Cta;->c:LX/ClK;

    iget-object v0, p1, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CsX;

    .line 1945095
    iget-object v2, v0, LX/CsW;->c:LX/Ckw;

    move-object v0, v2

    .line 1945096
    iget-object v2, p1, LX/CtX;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/ClK;->b(LX/Ckw;Ljava/lang/String;)V

    .line 1945097
    iget-object v0, p1, LX/CtX;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1945098
    iget-object v0, p1, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CsX;

    iget-object v1, p1, LX/CtX;->d:Ljava/lang/String;

    iget-object v2, p1, LX/CtX;->c:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/CsX;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1945099
    :goto_0
    monitor-exit p0

    return-void

    .line 1945100
    :cond_0
    :try_start_1
    iget-object v0, p1, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CsX;

    iget-object v1, p1, LX/CtX;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CsX;->loadUrl(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1945101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized d(LX/Cta;)V
    .locals 3

    .prologue
    .line 1945102
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Cta;->d:LX/CtZ;

    sget-object v1, LX/CtZ;->PAUSED:LX/CtZ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 1945103
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1945104
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/Cta;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CtX;

    .line 1945105
    if-nez v0, :cond_4

    .line 1945106
    :cond_2
    :goto_1
    iget-object v0, p0, LX/Cta;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CtX;

    .line 1945107
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/Cta;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_3

    iget-object v1, v0, LX/CtX;->f:LX/CtY;

    sget-object v2, LX/CtY;->BYPASS_LIMITS:LX/CtY;

    if-ne v1, v2, :cond_0

    .line 1945108
    :cond_3
    iget-object v1, p0, LX/Cta;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 1945109
    invoke-direct {p0, v0}, LX/Cta;->c(LX/CtX;)V

    .line 1945110
    iget-object v1, v0, LX/CtX;->f:LX/CtY;

    sget-object v2, LX/CtY;->BYPASS_LIMITS:LX/CtY;

    if-eq v1, v2, :cond_0

    .line 1945111
    iget-object v1, p0, LX/Cta;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1945112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1945113
    :cond_4
    iget-object v1, v0, LX/CtX;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CsX;

    .line 1945114
    if-nez v1, :cond_2

    .line 1945115
    iget-object v1, p0, LX/Cta;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 1945116
    if-nez v0, :cond_1

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/CsX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CtY;)V
    .locals 6

    .prologue
    .line 1945117
    invoke-static {p3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1945118
    :cond_0
    :goto_0
    return-void

    .line 1945119
    :cond_1
    iget-object v0, p0, LX/Cta;->c:LX/ClK;

    .line 1945120
    iget-object v1, p1, LX/CsW;->c:LX/Ckw;

    move-object v1, v1

    .line 1945121
    invoke-virtual {v0, v1, p2}, LX/ClK;->a(LX/Ckw;Ljava/lang/String;)V

    .line 1945122
    new-instance v0, LX/CtX;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/CtX;-><init>(LX/CsX;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CtY;)V

    .line 1945123
    invoke-static {p0, v0}, LX/Cta;->a$redex0(LX/Cta;LX/CtX;)V

    .line 1945124
    iget-object v1, p0, LX/Cta;->a:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1945125
    invoke-static {p0}, LX/Cta;->d(LX/Cta;)V

    goto :goto_0
.end method
