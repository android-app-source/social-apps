.class public LX/Cw3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V
    .locals 1

    .prologue
    .line 1949861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949862
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v0, v0

    .line 1949863
    iget-object v0, v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    iput-object v0, p0, LX/Cw3;->a:Ljava/lang/String;

    .line 1949864
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v0, v0

    .line 1949865
    iget-object v0, v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    iput-object v0, p0, LX/Cw3;->b:Ljava/lang/String;

    .line 1949866
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1949867
    iput-object v0, p0, LX/Cw3;->c:Ljava/lang/String;

    .line 1949868
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u:Ljava/lang/String;

    move-object v0, v0

    .line 1949869
    iput-object v0, p0, LX/Cw3;->d:Ljava/lang/String;

    .line 1949870
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v0, v0

    .line 1949871
    invoke-virtual {v0}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cw3;->e:Ljava/lang/String;

    .line 1949872
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v0, v0

    .line 1949873
    if-eqz v0, :cond_0

    .line 1949874
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v0, v0

    .line 1949875
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/Cw3;->f:Ljava/lang/String;

    .line 1949876
    const-string v0, "referrer_placeholder"

    iput-object v0, p0, LX/Cw3;->g:Ljava/lang/String;

    .line 1949877
    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    invoke-static {v0}, LX/7CN;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cw3;->h:Ljava/lang/String;

    .line 1949878
    return-void

    .line 1949879
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
