.class public abstract LX/Cte;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements LX/Cq9;


# instance fields
.field public a:LX/Crk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8bU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:I

.field private final e:LX/Cs7;

.field private final f:Landroid/view/View;

.field public final g:Landroid/graphics/Paint;

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Ctd;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:I

.field public k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1945270
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Cte;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1945271
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1945268
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Cte;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945269
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1945253
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945254
    new-instance v0, LX/Cs7;

    invoke-direct {v0}, LX/Cs7;-><init>()V

    iput-object v0, p0, LX/Cte;->e:LX/Cs7;

    .line 1945255
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/Cte;->g:Landroid/graphics/Paint;

    .line 1945256
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Cte;->h:Ljava/util/Map;

    .line 1945257
    const/4 v0, 0x0

    iput v0, p0, LX/Cte;->k:I

    .line 1945258
    const-class v0, LX/Cte;

    invoke-static {v0, p0}, LX/Cte;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1945259
    invoke-static {}, LX/473;->a()I

    move-result v0

    iput v0, p0, LX/Cte;->d:I

    .line 1945260
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Cte;->f:Landroid/view/View;

    .line 1945261
    if-eqz p2, :cond_0

    .line 1945262
    sget-object v0, LX/03r;->MediaFrame:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1945263
    const/16 v1, 0x1

    sget p3, LX/CoL;->t:I

    invoke-virtual {v0, v1, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/Cte;->j:I

    .line 1945264
    const/16 v1, 0x0

    const/high16 p3, -0x1000000

    invoke-virtual {v0, v1, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1945265
    iget-object p3, p0, LX/Cte;->g:Landroid/graphics/Paint;

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1945266
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1945267
    :cond_0
    return-void
.end method

.method public static varargs a(LX/Cte;Landroid/graphics/Rect;LX/Ctd;[LX/ClR;)V
    .locals 7

    .prologue
    .line 1945220
    iget-object v0, p0, LX/Cte;->h:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1945221
    iget-object v0, p0, LX/Cte;->h:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1945222
    :cond_0
    iget-object v0, p0, LX/Cte;->e:LX/Cs7;

    invoke-virtual {v0, p3}, LX/Cs7;->a([LX/ClR;)Ljava/util/List;

    move-result-object v0

    .line 1945223
    const/4 v1, 0x0

    .line 1945224
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CnQ;

    .line 1945225
    invoke-interface {v1}, LX/CnQ;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 1945226
    invoke-interface {v1}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/Cte;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    .line 1945227
    if-eqz v4, :cond_1

    .line 1945228
    if-nez v2, :cond_2

    .line 1945229
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    move-object v2, v1

    goto :goto_0

    .line 1945230
    :cond_2
    invoke-virtual {v2, v4}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 1945231
    :cond_3
    if-eqz v2, :cond_4

    .line 1945232
    invoke-virtual {v2, p1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    .line 1945233
    :cond_4
    move-object v1, v2

    .line 1945234
    if-eqz v1, :cond_6

    .line 1945235
    iget-object v2, p0, LX/Cte;->h:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1945236
    iget-object v2, p0, LX/Cte;->h:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1945237
    :goto_1
    move-object v2, v2

    .line 1945238
    iget v3, v1, Landroid/graphics/Rect;->left:I

    iget v4, p0, LX/Cte;->j:I

    sub-int/2addr v3, v4

    iget v4, v1, Landroid/graphics/Rect;->top:I

    iget v5, p0, LX/Cte;->j:I

    sub-int/2addr v4, v5

    iget v5, v1, Landroid/graphics/Rect;->right:I

    iget v6, p0, LX/Cte;->j:I

    add-int/2addr v5, v6

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget v6, p0, LX/Cte;->j:I

    add-int/2addr v1, v6

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/view/View;->layout(IIII)V

    .line 1945239
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1945240
    const/4 v1, 0x0

    .line 1945241
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v1

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CnQ;

    .line 1945242
    invoke-interface {v1}, LX/CnQ;->d()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v1}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_8

    .line 1945243
    invoke-interface {v1}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/Cte;->b(Landroid/view/View;)Ljava/lang/Float;

    move-result-object v1

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v1, v5}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 1945244
    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    move v1, v3

    :goto_3
    move v3, v1

    .line 1945245
    goto :goto_2

    .line 1945246
    :cond_5
    sget v1, LX/CoL;->u:F

    mul-float/2addr v1, v3

    move v0, v1

    .line 1945247
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v2, v0}, LX/Cte;->a(Landroid/view/View;Ljava/lang/Float;)V

    .line 1945248
    :cond_6
    return-void

    .line 1945249
    :cond_7
    new-instance v2, LX/Ctc;

    invoke-virtual {p0}, LX/Cte;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/Cte;->c:LX/8bU;

    iget-object v5, p0, LX/Cte;->b:LX/0Uh;

    const/16 v6, 0x3ea

    const/4 p1, 0x0

    invoke-virtual {v5, v6, p1}, LX/0Uh;->a(IZ)Z

    move-result v5

    invoke-direct {v2, v3, v4, v5}, LX/Ctc;-><init>(Landroid/content/Context;LX/8bU;Z)V

    .line 1945250
    sget v3, LX/CoL;->u:F

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    .line 1945251
    iget-object v3, p0, LX/Cte;->h:Ljava/util/Map;

    invoke-interface {v3, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1945252
    iget v3, p0, LX/Cte;->k:I

    invoke-virtual {p0}, Lcom/facebook/widget/CustomViewGroup;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {p0, v2, v3, v4, v5}, LX/Cte;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto/16 :goto_1

    :cond_8
    move v1, v3

    goto :goto_3
.end method

.method private static a(Landroid/view/View;Ljava/lang/Float;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1945212
    if-eqz p1, :cond_1

    .line 1945213
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1945214
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sget v1, LX/CoL;->s:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 1945215
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1945216
    :goto_0
    return-void

    .line 1945217
    :cond_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1945218
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1945219
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cte;

    invoke-static {p0}, LX/Crk;->a(LX/0QB;)LX/Crk;

    move-result-object v1

    check-cast v1, LX/Crk;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {p0}, LX/8bU;->a(LX/0QB;)LX/8bU;

    move-result-object p0

    check-cast p0, LX/8bU;

    iput-object v1, p1, LX/Cte;->a:LX/Crk;

    iput-object v2, p1, LX/Cte;->b:LX/0Uh;

    iput-object p0, p1, LX/Cte;->c:LX/8bU;

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/view/View;)Landroid/graphics/Rect;
.end method

.method public a(LX/CnQ;)V
    .locals 2

    .prologue
    .line 1945201
    if-eqz p1, :cond_0

    .line 1945202
    iget-object v0, p0, LX/Cte;->e:LX/Cs7;

    .line 1945203
    iget-object v1, v0, LX/Cs7;->a:Ljava/util/TreeSet;

    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 1945204
    invoke-interface {p1}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cte;->addView(Landroid/view/View;)V

    .line 1945205
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/Cte;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1945206
    invoke-virtual {p0, v0}, LX/Cte;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1945207
    instance-of v1, v1, LX/CnQ;

    if-eqz v1, :cond_1

    .line 1945208
    iput v0, p0, LX/Cte;->k:I

    .line 1945209
    :cond_0
    :goto_1
    return-void

    .line 1945210
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1945211
    :cond_2
    invoke-virtual {p0}, LX/Cte;->getChildCount()I

    move-result v0

    iput v0, p0, LX/Cte;->k:I

    goto :goto_1
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1945195
    invoke-virtual {p0}, LX/Cte;->getOverlayView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cte;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1945196
    if-eqz v0, :cond_0

    .line 1945197
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1945198
    iget-object v1, p0, LX/Cte;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1945199
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1945200
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1945193
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move v5, v3

    invoke-super/range {v0 .. v5}, Lcom/facebook/widget/CustomViewGroup;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1945194
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    .line 1945191
    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget v2, p2, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    move-object v0, p0

    move-object v5, p1

    invoke-super/range {v0 .. v5}, Lcom/facebook/widget/CustomViewGroup;->layoutChild(IIIILandroid/view/View;)V

    .line 1945192
    return-void
.end method

.method public abstract b(Landroid/view/View;)Ljava/lang/Float;
.end method

.method public b()V
    .locals 6

    .prologue
    .line 1945172
    iget-object v0, p0, LX/Cte;->e:LX/Cs7;

    .line 1945173
    iget-object v1, v0, LX/Cs7;->a:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->clear()V

    .line 1945174
    iget-object v0, p0, LX/Cte;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1945175
    invoke-virtual {p0}, LX/Cte;->getChildCount()I

    move-result v0

    .line 1945176
    if-lez v0, :cond_4

    .line 1945177
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_4

    .line 1945178
    invoke-virtual {p0, v1}, LX/Cte;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1945179
    instance-of v2, v0, LX/CnQ;

    if-nez v2, :cond_0

    instance-of v2, v0, LX/Ctc;

    if-eqz v2, :cond_3

    .line 1945180
    :cond_0
    instance-of v2, v0, LX/CnP;

    if-eqz v2, :cond_2

    .line 1945181
    iget-object v2, p0, LX/Cte;->a:LX/Crk;

    check-cast v0, LX/CnP;

    .line 1945182
    iget-object v3, v2, LX/Crk;->d:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Crh;

    .line 1945183
    iget-object v5, v3, LX/Crh;->b:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    move v5, v5

    .line 1945184
    if-eqz v5, :cond_1

    .line 1945185
    iget-object v4, v3, LX/Crh;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1945186
    invoke-interface {v0}, LX/CnP;->b()V

    .line 1945187
    iget-object v4, v3, LX/Crh;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1945188
    :cond_2
    :goto_1
    invoke-virtual {p0, v1}, LX/Cte;->removeViewAt(I)V

    .line 1945189
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1945190
    :cond_4
    return-void

    :cond_5
    goto :goto_1

    :cond_6
    goto :goto_1
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 1945169
    invoke-virtual {p0, p1}, LX/Cte;->a(Landroid/graphics/Canvas;)V

    .line 1945170
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1945171
    return-void
.end method

.method public getAnnotationViews()LX/Cs7;
    .locals 1

    .prologue
    .line 1945168
    iget-object v0, p0, LX/Cte;->e:LX/Cs7;

    return-object v0
.end method

.method public abstract getOverlayBounds()Landroid/graphics/Rect;
.end method

.method public getOverlayShadowBounds()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1945167
    invoke-virtual {p0}, LX/Cte;->getOverlayBounds()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getOverlayView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1945166
    iget-object v0, p0, LX/Cte;->f:Landroid/view/View;

    return-object v0
.end method

.method public jl_()Z
    .locals 1

    .prologue
    .line 1945165
    iget-boolean v0, p0, LX/Cte;->i:Z

    return v0
.end method

.method public onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 1945149
    invoke-virtual {p0}, LX/Cte;->getOverlayBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 1945150
    iget-object v0, p0, LX/Cte;->e:LX/Cs7;

    invoke-virtual {v0}, LX/Cs7;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1945151
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v3

    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/Cte;->b(Landroid/view/View;)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v3, v4}, LX/Cte;->a(Landroid/view/View;Ljava/lang/Float;)V

    .line 1945152
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/Cte;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    .line 1945153
    if-eqz v3, :cond_0

    .line 1945154
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result p1

    invoke-virtual {v1, v4, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    .line 1945155
    invoke-interface {v0, v4}, LX/CnQ;->setIsOverlay(Z)V

    .line 1945156
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4, v3}, LX/Cte;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1945157
    goto :goto_0

    .line 1945158
    :cond_1
    invoke-virtual {p0}, LX/Cte;->jl_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1945159
    const/4 p2, 0x2

    const/4 p1, 0x1

    const/4 v4, 0x0

    .line 1945160
    invoke-virtual {p0}, LX/Cte;->getOverlayShadowBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1945161
    sget-object v1, LX/Ctd;->TOP:LX/Ctd;

    new-array v2, p2, [LX/ClR;

    sget-object v3, LX/ClR;->ABOVE:LX/ClR;

    aput-object v3, v2, v4

    sget-object v3, LX/ClR;->TOP:LX/ClR;

    aput-object v3, v2, p1

    invoke-static {p0, v0, v1, v2}, LX/Cte;->a(LX/Cte;Landroid/graphics/Rect;LX/Ctd;[LX/ClR;)V

    .line 1945162
    sget-object v1, LX/Ctd;->CENTER:LX/Ctd;

    new-array v2, p1, [LX/ClR;

    sget-object v3, LX/ClR;->CENTER:LX/ClR;

    aput-object v3, v2, v4

    invoke-static {p0, v0, v1, v2}, LX/Cte;->a(LX/Cte;Landroid/graphics/Rect;LX/Ctd;[LX/ClR;)V

    .line 1945163
    sget-object v1, LX/Ctd;->BOTTOM:LX/Ctd;

    new-array v2, p2, [LX/ClR;

    sget-object v3, LX/ClR;->BOTTOM:LX/ClR;

    aput-object v3, v2, v4

    sget-object v3, LX/ClR;->BELOW:LX/ClR;

    aput-object v3, v2, p1

    invoke-static {p0, v0, v1, v2}, LX/Cte;->a(LX/Cte;Landroid/graphics/Rect;LX/Ctd;[LX/ClR;)V

    .line 1945164
    :cond_2
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1945144
    invoke-virtual {p0, p0}, LX/Cte;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1945145
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1945146
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1945147
    invoke-super {p0, v1, v0}, Lcom/facebook/widget/CustomViewGroup;->onMeasure(II)V

    .line 1945148
    return-void
.end method

.method public setOverlayBackgroundColor(I)V
    .locals 1

    .prologue
    .line 1945142
    iget-object v0, p0, LX/Cte;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1945143
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1945141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/Cte;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
