.class public abstract LX/Cts;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02k;
.implements LX/Ctr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/02k;",
        "LX/Ctr",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:LX/Ctg;


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 0

    .prologue
    .line 1945751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1945752
    iput-object p1, p0, LX/Cts;->a:LX/Ctg;

    .line 1945753
    return-void
.end method

.method public static a(LX/CrS;Landroid/view/View;)LX/CrW;
    .locals 2

    .prologue
    .line 1945750
    sget-object v0, LX/CrQ;->RECT:LX/CrQ;

    const-class v1, LX/CrW;

    invoke-interface {p0, p1, v0, v1}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v0

    check-cast v0, LX/CrW;

    return-object v0
.end method


# virtual methods
.method public a(LX/Cqw;)V
    .locals 0

    .prologue
    .line 1945749
    return-void
.end method

.method public a(LX/CrS;)V
    .locals 0

    .prologue
    .line 1945748
    return-void
.end method

.method public a(LX/Ctr;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1945747
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 1945746
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1945744
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    invoke-interface {v0, p0, p1}, LX/Ctf;->a(LX/Ctr;Ljava/lang/Object;)V

    .line 1945745
    return-void
.end method

.method public a(LX/Crd;)Z
    .locals 1

    .prologue
    .line 1945734
    const/4 v0, 0x0

    return v0
.end method

.method public b(LX/CrS;)V
    .locals 0

    .prologue
    .line 1945743
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1945754
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 1945742
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 1945741
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 1945740
    return-void
.end method

.method public final g()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 1945739
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1945738
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;
    .locals 1

    .prologue
    .line 1945737
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    return-object v0
.end method

.method public final i()Landroid/view/View;
    .locals 1

    .prologue
    .line 1945736
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/Cqw;
    .locals 1

    .prologue
    .line 1945735
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    invoke-virtual {v0}, LX/CqX;->f()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    return-object v0
.end method
