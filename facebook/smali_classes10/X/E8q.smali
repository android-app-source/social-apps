.class public LX/E8q;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bi8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dt5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

.field public d:Landroid/view/View;

.field public e:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

.field public f:Landroid/animation/AnimatorSet;

.field public g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

.field public h:Landroid/support/v4/app/Fragment;

.field public i:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field public j:LX/2ja;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2083478
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2083479
    const-class v0, LX/E8q;

    invoke-static {v0, p0}, LX/E8q;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2083480
    const v0, 0x7f031145

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2083481
    return-void
.end method

.method private static a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;)Lcom/facebook/graphql/model/GraphQLRating;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083515
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_0

    .line 2083516
    const/4 v0, 0x0

    .line 2083517
    :goto_0
    return-object v0

    .line 2083518
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->n()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2083519
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->n()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2083520
    new-instance v4, LX/4YW;

    invoke-direct {v4}, LX/4YW;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5}, LX/15i;->j(II)I

    move-result v0

    .line 2083521
    iput v0, v4, LX/4YW;->b:I

    .line 2083522
    move-object v0, v4

    .line 2083523
    const/4 v1, 0x1

    invoke-virtual {v3, v2, v1}, LX/15i;->l(II)D

    move-result-wide v2

    .line 2083524
    iput-wide v2, v0, LX/4YW;->d:D

    .line 2083525
    move-object v0, v0

    .line 2083526
    invoke-virtual {v0}, LX/4YW;->a()Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/E8q;

    const/16 v2, 0x1aba

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x2c39

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v2, p1, LX/E8q;->a:LX/0Ot;

    iput-object v1, p1, LX/E8q;->b:LX/0Ot;

    return-void
.end method

.method private static setContextItems(LX/E8q;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x0

    .line 2083503
    iget-object v0, p0, LX/E8q;->e:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/E8q;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0118

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2083504
    new-instance v1, LX/Bi6;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/E8q;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;)Lcom/facebook/graphql/model/GraphQLRating;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/Bi6;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLRating;)V

    .line 2083505
    iget-object v0, p0, LX/E8q;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bi8;

    .line 2083506
    iget-object v2, p0, LX/E8q;->e:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    invoke-virtual {v2, v0}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->setAdapter(LX/Bi8;)V

    .line 2083507
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->d()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v2

    sget-object v3, LX/Bi3;->PLACE_TIPS:LX/Bi3;

    invoke-virtual {v0, v2, v3, v1}, LX/Bi8;->a(Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;LX/Bi3;LX/Bi6;)V

    .line 2083508
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2083509
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v10}, LX/15i;->h(II)Z

    move-result v9

    .line 2083510
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v10

    .line 2083511
    :goto_0
    new-instance v1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->ar_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->j()LX/1k1;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;->b()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v7

    move-object v8, v5

    invoke-direct/range {v1 .. v10}, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;-><init>(JLjava/lang/String;Ljava/lang/String;LX/1k1;Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;Lcom/facebook/auth/viewercontext/ViewerContext;ZZ)V

    .line 2083512
    iget-object v0, p0, LX/E8q;->e:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    new-instance v2, LX/E8p;

    invoke-direct {v2, p0, v1}, LX/E8p;-><init>(LX/E8q;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V

    .line 2083513
    iput-object v2, v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->g:LX/Bi2;

    .line 2083514
    return-void

    :cond_0
    move v9, v10

    goto :goto_0
.end method


# virtual methods
.method public final a(ZLcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 13

    .prologue
    .line 2083482
    iget-object v0, p0, LX/E8q;->d:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2083483
    const v0, 0x7f0d28d5

    invoke-virtual {p0, v0}, LX/E8q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2083484
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/E8q;->d:Landroid/view/View;

    .line 2083485
    iget-object v0, p0, LX/E8q;->d:Landroid/view/View;

    const v1, 0x7f0d28d3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    iput-object v0, p0, LX/E8q;->e:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    .line 2083486
    iget-object v0, p0, LX/E8q;->d:Landroid/view/View;

    const v1, 0x7f0d28d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iput-object v0, p0, LX/E8q;->c:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    .line 2083487
    iget-object v0, p0, LX/E8q;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    invoke-static {p0, v0}, LX/E8q;->setContextItems(LX/E8q;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;)V

    .line 2083488
    iget-object v0, p0, LX/E8q;->c:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;

    iget-object v1, p0, LX/E8q;->g:Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;

    iget-object v2, p0, LX/E8q;->h:Landroid/support/v4/app/Fragment;

    iget-object v3, p0, LX/E8q;->i:Ljava/lang/String;

    iget-object v4, p0, LX/E8q;->j:LX/2ja;

    move v5, p1

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderActionButtonsView;->a(Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsWithPlaceTipsInfoModel;Landroid/support/v4/app/Fragment;Ljava/lang/String;LX/2ja;ZLcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2083489
    :cond_0
    const/4 v7, 0x0

    .line 2083490
    iget-object v8, p0, LX/E8q;->f:Landroid/animation/AnimatorSet;

    if-eqz v8, :cond_1

    .line 2083491
    iget-object v8, p0, LX/E8q;->f:Landroid/animation/AnimatorSet;

    invoke-virtual {v8}, Landroid/animation/AnimatorSet;->end()V

    .line 2083492
    :cond_1
    iget-object v8, p0, LX/E8q;->d:Landroid/view/View;

    invoke-virtual {v8, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2083493
    :goto_0
    iget-object v8, p0, LX/E8q;->e:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    invoke-virtual {v8}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->getChildCount()I

    move-result v8

    if-ge v7, v8, :cond_2

    .line 2083494
    iget-object v8, p0, LX/E8q;->e:Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    invoke-virtual {v8, v7}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 2083495
    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2083496
    invoke-virtual {p0}, LX/E8q;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f050001

    invoke-static {v9, v10}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v9

    .line 2083497
    new-instance v10, LX/E8o;

    invoke-direct {v10, p0, v8}, LX/E8o;-><init>(LX/E8q;Landroid/view/View;)V

    invoke-virtual {v9, v10}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2083498
    invoke-virtual {v9, v8}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 2083499
    mul-int/lit8 v8, v7, 0x1e

    int-to-long v11, v8

    invoke-virtual {v9, v11, v12}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 2083500
    invoke-virtual {v9}, Landroid/animation/Animator;->start()V

    .line 2083501
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 2083502
    :cond_2
    return-void
.end method
