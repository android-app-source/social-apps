.class public LX/Es5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Es5;


# instance fields
.field private final a:LX/B9y;

.field private final b:LX/1Cn;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/B9y;LX/1Cn;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B9y;",
            "LX/1Cn;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2174449
    iput-object p1, p0, LX/Es5;->a:LX/B9y;

    .line 2174450
    iput-object p2, p0, LX/Es5;->b:LX/1Cn;

    .line 2174451
    iput-object p3, p0, LX/Es5;->c:LX/0Or;

    .line 2174452
    iput-object p4, p0, LX/Es5;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2174453
    return-void
.end method

.method public static a(LX/0QB;)LX/Es5;
    .locals 7

    .prologue
    .line 2174454
    sget-object v0, LX/Es5;->e:LX/Es5;

    if-nez v0, :cond_1

    .line 2174455
    const-class v1, LX/Es5;

    monitor-enter v1

    .line 2174456
    :try_start_0
    sget-object v0, LX/Es5;->e:LX/Es5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2174457
    if-eqz v2, :cond_0

    .line 2174458
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2174459
    new-instance v6, LX/Es5;

    invoke-static {v0}, LX/B9y;->a(LX/0QB;)LX/B9y;

    move-result-object v3

    check-cast v3, LX/B9y;

    invoke-static {v0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v4

    check-cast v4, LX/1Cn;

    const/16 v5, 0xbc6

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v6, v3, v4, p0, v5}, LX/Es5;-><init>(LX/B9y;LX/1Cn;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V

    .line 2174460
    move-object v0, v6

    .line 2174461
    sput-object v0, LX/Es5;->e:LX/Es5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174462
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2174463
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2174464
    :cond_1
    sget-object v0, LX/Es5;->e:LX/Es5;

    return-object v0

    .line 2174465
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2174466
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Landroid/app/Activity;Ljava/lang/String;LX/CEz;)V
    .locals 14

    .prologue
    .line 2174467
    iget-object v2, p0, LX/Es5;->b:LX/1Cn;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v2, v3, v0, v1}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;LX/CEz;)V

    .line 2174468
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    .line 2174469
    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2174470
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2174471
    iget-object v2, p0, LX/Es5;->a:LX/B9y;

    invoke-virtual {v2}, LX/B9y;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2174472
    sget-object v2, LX/0ax;->ai:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2174473
    iget-object v2, p0, LX/Es5;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    move-object/from16 v0, p2

    invoke-interface {v2, v0, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2174474
    if-eqz v2, :cond_0

    .line 2174475
    iget-object v3, p0, LX/Es5;->d:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p2

    invoke-interface {v3, v2, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2174476
    :cond_0
    :goto_2
    return-void

    .line 2174477
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 2174478
    :cond_2
    const/4 v2, 0x0

    move-object v3, v2

    goto :goto_1

    .line 2174479
    :cond_3
    iget-object v2, p0, LX/Es5;->a:LX/B9y;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->B()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->t()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    :goto_4
    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    :goto_5
    invoke-static {v3}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v9

    invoke-static {v3}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, LX/CEz;->getAnalyticsName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "throwback"

    const/4 v13, 0x1

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v13}, LX/B9y;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    goto :goto_3

    :cond_5
    const/4 v6, 0x0

    goto :goto_4

    :cond_6
    const/4 v8, 0x0

    goto :goto_5
.end method
