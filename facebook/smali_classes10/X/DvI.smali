.class public LX/DvI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static d:I

.field public static e:I

.field public static f:I

.field public static g:I

.field public static h:I

.field public static i:I

.field public static j:I

.field private static k:I

.field private static l:I

.field private static volatile m:LX/DvI;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0rq;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0se;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2058151
    sput v0, LX/DvI;->d:I

    .line 2058152
    sput v0, LX/DvI;->e:I

    .line 2058153
    sput v0, LX/DvI;->f:I

    .line 2058154
    sput v0, LX/DvI;->g:I

    .line 2058155
    sput v0, LX/DvI;->h:I

    .line 2058156
    sput v0, LX/DvI;->i:I

    .line 2058157
    sput v0, LX/DvI;->j:I

    .line 2058158
    sput v0, LX/DvI;->k:I

    .line 2058159
    sput v0, LX/DvI;->l:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0rq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0se;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2058160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2058161
    iput-object p1, p0, LX/DvI;->a:LX/0Ot;

    .line 2058162
    iput-object p2, p0, LX/DvI;->b:LX/0Ot;

    .line 2058163
    iput-object p3, p0, LX/DvI;->c:LX/0Ot;

    .line 2058164
    iget-object v0, p0, LX/DvI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2058165
    iget-object v0, p0, LX/DvI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v2, 0x7f0b0b48

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2058166
    mul-int/lit8 v2, v0, 0x4

    sub-int v2, v1, v2

    div-int/lit8 v2, v2, 0x3

    sput v2, LX/DvI;->d:I

    .line 2058167
    mul-int/lit8 v2, v0, 0x3

    sub-int v2, v1, v2

    sget v3, LX/DvI;->d:I

    sub-int/2addr v2, v3

    sput v2, LX/DvI;->e:I

    .line 2058168
    mul-int/lit8 v2, v0, 0x2

    sub-int v2, v1, v2

    sput v2, LX/DvI;->h:I

    .line 2058169
    sget v2, LX/DvI;->e:I

    sput v2, LX/DvI;->i:I

    .line 2058170
    sget v2, LX/DvI;->e:I

    sput v2, LX/DvI;->f:I

    .line 2058171
    sget v2, LX/DvI;->d:I

    mul-int/lit8 v2, v2, 0x3

    mul-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    sput v2, LX/DvI;->g:I

    .line 2058172
    sget v2, LX/DvI;->f:I

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x2

    sput v2, LX/DvI;->j:I

    .line 2058173
    mul-int/lit8 v2, v0, 0x3

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    sput v1, LX/DvI;->k:I

    .line 2058174
    sget v1, LX/DvI;->k:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sput v0, LX/DvI;->l:I

    .line 2058175
    return-void
.end method

.method public static a(LX/0QB;)LX/DvI;
    .locals 6

    .prologue
    .line 2058176
    sget-object v0, LX/DvI;->m:LX/DvI;

    if-nez v0, :cond_1

    .line 2058177
    const-class v1, LX/DvI;

    monitor-enter v1

    .line 2058178
    :try_start_0
    sget-object v0, LX/DvI;->m:LX/DvI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2058179
    if-eqz v2, :cond_0

    .line 2058180
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2058181
    new-instance v3, LX/DvI;

    const/16 v4, 0x1b

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xb24

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xf27

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/DvI;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2058182
    move-object v0, v3

    .line 2058183
    sput-object v0, LX/DvI;->m:LX/DvI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2058184
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2058185
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2058186
    :cond_1
    sget-object v0, LX/DvI;->m:LX/DvI;

    return-object v0

    .line 2058187
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2058188
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0gW;)LX/0gW;
    .locals 2

    .prologue
    .line 2058189
    iget-object v0, p0, LX/DvI;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0se;

    iget-object v1, p0, LX/DvI;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0rq;

    invoke-virtual {v1}, LX/0rq;->a()LX/0wF;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2058190
    const-string v0, "image_thumbnail_width"

    sget v1, LX/DvI;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058191
    const-string v0, "image_large_thumbnail_width"

    sget v1, LX/DvI;->e:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058192
    const-string v0, "image_portrait_width"

    sget v1, LX/DvI;->f:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058193
    const-string v0, "image_portrait_height"

    sget v1, LX/DvI;->g:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058194
    const-string v0, "image_landscape_width"

    sget v1, LX/DvI;->h:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058195
    const-string v0, "image_landscape_height"

    sget v1, LX/DvI;->i:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058196
    const-string v0, "large_portrait_height"

    sget v1, LX/DvI;->j:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058197
    const-string v0, "narrow_landscape_height"

    sget v1, LX/DvI;->k:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058198
    const-string v0, "narrow_portrait_height"

    sget v1, LX/DvI;->l:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058199
    return-object p1
.end method
