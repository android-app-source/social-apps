.class public LX/Cs3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static l:LX/0Xm;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0WJ;

.field public final d:LX/0Or;
    .annotation runtime Lcom/facebook/config/server/IsRedirectToSandboxEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0lC;

.field public final f:LX/0Uh;

.field private final g:LX/01T;

.field private final h:LX/0WV;

.field public final i:LX/Ckw;

.field private final j:LX/Cju;

.field private final k:LX/8bZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1941778
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->YOUTUBE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->VINE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, LX/Cs3;->a:Ljava/util/EnumSet;

    return-void
.end method

.method public constructor <init>(LX/0WJ;LX/0Or;LX/0lC;LX/0Uh;LX/0W3;LX/01T;LX/0WV;LX/Ckw;LX/Cju;LX/8bZ;)V
    .locals 3
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/config/server/IsRedirectToSandboxEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0lC;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/01T;",
            "LX/0WV;",
            "LX/Ckw;",
            "LX/Cju;",
            "LX/8bZ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1941765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941766
    iput-object p1, p0, LX/Cs3;->c:LX/0WJ;

    .line 1941767
    iput-object p2, p0, LX/Cs3;->d:LX/0Or;

    .line 1941768
    iput-object p3, p0, LX/Cs3;->e:LX/0lC;

    .line 1941769
    iput-object p4, p0, LX/Cs3;->f:LX/0Uh;

    .line 1941770
    iput-object p7, p0, LX/Cs3;->h:LX/0WV;

    .line 1941771
    iput-object p6, p0, LX/Cs3;->g:LX/01T;

    .line 1941772
    iput-object p8, p0, LX/Cs3;->i:LX/Ckw;

    .line 1941773
    iput-object p9, p0, LX/Cs3;->j:LX/Cju;

    .line 1941774
    iput-object p10, p0, LX/Cs3;->k:LX/8bZ;

    .line 1941775
    sget-wide v0, LX/0X5;->fp:J

    const-string v2, ""

    invoke-interface {p5, v0, v1, v2}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1941776
    const/16 v1, 0x2c

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Cs3;->b:Ljava/util/List;

    .line 1941777
    return-void
.end method

.method public static a(LX/0QB;)LX/Cs3;
    .locals 14

    .prologue
    .line 1941648
    const-class v1, LX/Cs3;

    monitor-enter v1

    .line 1941649
    :try_start_0
    sget-object v0, LX/Cs3;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1941650
    sput-object v2, LX/Cs3;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1941651
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941652
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1941653
    new-instance v3, LX/Cs3;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    const/16 v5, 0x1473

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v6

    check-cast v6, LX/0lC;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v9

    check-cast v9, LX/01T;

    invoke-static {v0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v10

    check-cast v10, LX/0WV;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v11

    check-cast v11, LX/Ckw;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v12

    check-cast v12, LX/Cju;

    invoke-static {v0}, LX/8bZ;->b(LX/0QB;)LX/8bZ;

    move-result-object v13

    check-cast v13, LX/8bZ;

    invoke-direct/range {v3 .. v13}, LX/Cs3;-><init>(LX/0WJ;LX/0Or;LX/0lC;LX/0Uh;LX/0W3;LX/01T;LX/0WV;LX/Ckw;LX/Cju;LX/8bZ;)V

    .line 1941654
    move-object v0, v3

    .line 1941655
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1941656
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Cs3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1941657
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1941658
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 1941762
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1941763
    const-string v1, "Referer"

    const-string v2, "http://m.facebook.com"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1941764
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1941757
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941758
    const-string v0, ""

    .line 1941759
    :goto_0
    return-object v0

    .line 1941760
    :cond_0
    invoke-static {p0}, LX/0YN;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1941761
    const-string v1, "/"

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)Z
    .locals 1

    .prologue
    .line 1941754
    sget-object v0, LX/Cs3;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1941755
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->HTML_INTERACTIVE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->SOCIAL_EMBED:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne p0, v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1941756
    if-eqz v0, :cond_2

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->FULL_BLEED:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->AUTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    if-ne p1, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 1941738
    if-eqz p0, :cond_0

    .line 1941739
    :try_start_0
    invoke-virtual {p0}, Landroid/webkit/WebView;->stopLoading()V

    .line 1941740
    invoke-virtual {p0}, Landroid/webkit/WebView;->resumeTimers()V

    .line 1941741
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setTag(Ljava/lang/Object;)V

    .line 1941742
    invoke-virtual {p0}, Landroid/webkit/WebView;->clearHistory()V

    .line 1941743
    invoke-virtual {p0}, Landroid/webkit/WebView;->removeAllViews()V

    .line 1941744
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1941745
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 1941746
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1941747
    invoke-virtual {p0}, Landroid/webkit/WebView;->clearView()V

    .line 1941748
    invoke-virtual {p0}, Landroid/webkit/WebView;->onPause()V

    .line 1941749
    invoke-virtual {p0}, Landroid/webkit/WebView;->destroy()V

    .line 1941750
    invoke-virtual {p0}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1941751
    invoke-virtual {p0}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1941752
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1941753
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static c(Landroid/webkit/WebView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1941729
    if-eqz p0, :cond_0

    .line 1941730
    invoke-virtual {p0}, Landroid/webkit/WebView;->stopLoading()V

    .line 1941731
    invoke-virtual {p0}, Landroid/webkit/WebView;->clearHistory()V

    .line 1941732
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setInitialScale(I)V

    .line 1941733
    invoke-virtual {p0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 1941734
    invoke-virtual {p0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1941735
    invoke-virtual {p0, v1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1941736
    const-string v0, "about:blank"

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 1941737
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;ZLcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;)I
    .locals 4

    .prologue
    .line 1941719
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1941720
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1941721
    iget-object v1, p0, LX/Cs3;->j:LX/Cju;

    const v3, 0x7f0d0120

    invoke-interface {v1, v3}, LX/Cju;->c(I)I

    move-result v1

    .line 1941722
    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    .line 1941723
    if-eqz p2, :cond_1

    .line 1941724
    :goto_0
    iget-object v1, p0, LX/Cs3;->k:LX/8bZ;

    invoke-virtual {v1}, LX/8bZ;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Cs3;->k:LX/8bZ;

    invoke-virtual {v1}, LX/8bZ;->c()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1941725
    :cond_0
    int-to-float v0, v0

    iget v1, v2, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 1941726
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 1941727
    goto :goto_0

    .line 1941728
    :cond_2
    iget-object v0, p0, LX/Cs3;->k:LX/8bZ;

    sget-object v1, LX/8bY;->DP:LX/8bY;

    invoke-virtual {v0, p3, v1}, LX/8bZ;->a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;LX/8bY;)I

    move-result v0

    goto :goto_1
.end method

.method public final a(Landroid/webkit/WebView;)V
    .locals 12

    .prologue
    const/16 v5, 0x15

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1941668
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 1941669
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 1941670
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 1941671
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 1941672
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 1941673
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 1941674
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 1941675
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 1941676
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 1941677
    invoke-static {v0}, LX/0G0;->a(Landroid/webkit/WebSettings;)V

    .line 1941678
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v5, :cond_0

    .line 1941679
    iget-object v1, p0, LX/Cs3;->f:LX/0Uh;

    const/16 v2, 0x3e6

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v6}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v1, v1

    .line 1941680
    if-eqz v1, :cond_0

    .line 1941681
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    .line 1941682
    :cond_0
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 1941683
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 1941684
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 1941685
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "appcache"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    .line 1941686
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "databases"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDatabasePath(Ljava/lang/String;)V

    .line 1941687
    invoke-virtual {p1, v3}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 1941688
    invoke-virtual {p1, v3}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 1941689
    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v1

    .line 1941690
    iget-object v2, p0, LX/Cs3;->g:LX/01T;

    iget-object v3, p0, LX/Cs3;->h:LX/0WV;

    .line 1941691
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1941692
    const-string v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1941693
    const-string v7, "%s/%s;%s/%s;"

    const-string v8, "FBIA"

    invoke-virtual {v2}, LX/01T;->name()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/Cs3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "FBAV"

    invoke-virtual {v3}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, LX/Cs3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v8, v9, v10, v11}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1941694
    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1941695
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    .line 1941696
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1941697
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 1941698
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_1

    .line 1941699
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 1941700
    invoke-virtual {v0, p1, v4}, Landroid/webkit/CookieManager;->setAcceptThirdPartyCookies(Landroid/webkit/WebView;Z)V

    .line 1941701
    :cond_1
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1941702
    iget-object v1, p0, LX/Cs3;->c:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1941703
    :goto_0
    return-void

    .line 1941704
    :cond_2
    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 1941705
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v3

    .line 1941706
    iget-object v1, p0, LX/Cs3;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "http://%s/"

    .line 1941707
    :goto_1
    invoke-static {v0, v1}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1941708
    iget-object v1, p0, LX/Cs3;->c:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 1941709
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1941710
    if-eqz v1, :cond_4

    .line 1941711
    iget-object v2, p0, LX/Cs3;->e:LX/0lC;

    invoke-static {v2, v1}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v5

    .line 1941712
    if-eqz v5, :cond_4

    .line 1941713
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v6, :cond_4

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/credentials/SessionCookie;

    .line 1941714
    invoke-virtual {v1}, Lcom/facebook/auth/credentials/SessionCookie;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1941715
    invoke-virtual {v3, v4, v1}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 1941716
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1941717
    :cond_3
    const-string v1, "https://%s/"

    goto :goto_1

    .line 1941718
    :cond_4
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/CookieSyncManager;->sync()V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)Z
    .locals 5

    .prologue
    .line 1941659
    iget-object v0, p0, LX/Cs3;->b:Ljava/util/List;

    const/4 v2, 0x0

    .line 1941660
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    move v1, v2

    .line 1941661
    :goto_0
    move v0, v1

    .line 1941662
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1941663
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 1941664
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1941665
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_3

    invoke-virtual {v3, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1941666
    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    move v1, v2

    .line 1941667
    goto :goto_0
.end method
