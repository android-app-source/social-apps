.class public LX/Cng;
.super LX/CnT;
.source ""

# interfaces
.implements LX/02k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/InlineEmailCtaExperimentBlockView;",
        "Lcom/facebook/richdocument/model/data/InlineEmailCtaExperimentBlockData;",
        ">;",
        "LX/02k;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;)V
    .locals 0

    .prologue
    .line 1934288
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934289
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 13

    .prologue
    .line 1934218
    check-cast p1, LX/CmI;

    .line 1934219
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934220
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    .line 1934221
    iget-object v1, p1, LX/CmI;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1934222
    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->C:Ljava/lang/String;

    .line 1934223
    iget-object v0, p1, LX/CmI;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934224
    if-eqz v0, :cond_2

    .line 1934225
    iget-object v0, p1, LX/CmI;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934226
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1934227
    iget-object v0, p1, LX/CmI;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934228
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1934229
    iget-object v0, p1, LX/CmI;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934230
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v1

    .line 1934231
    iget-object v0, p1, LX/CmI;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v0, v0

    .line 1934232
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    move-result-object v2

    .line 1934233
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$ContextPageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1934234
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934235
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$ContextPageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$ContextPageModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 1934236
    iget-object v4, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934237
    iget-object v0, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, v0

    .line 1934238
    invoke-virtual {v4, v3}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934239
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934240
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$ContextPageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$ContextPageModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 1934241
    iget-object v4, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934242
    iget-object v0, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, v0

    .line 1934243
    invoke-virtual {v4, v3}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934244
    :cond_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934245
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->et_()Ljava/lang/String;

    move-result-object v4

    const/4 v12, 0x0

    .line 1934246
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f081c78

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1934247
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f081c79

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1934248
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1934249
    new-instance v8, LX/Cp8;

    invoke-direct {v8, v0, v3}, LX/Cp8;-><init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;Ljava/lang/String;)V

    .line 1934250
    invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7}, Landroid/text/SpannableString;->length()I

    move-result v10

    const/16 v11, 0x21

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1934251
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a062d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-direct {v8, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7}, Landroid/text/SpannableString;->length()I

    move-result v10

    invoke-virtual {v7, v8, v9, v10, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1934252
    new-instance v8, Landroid/text/style/UnderlineSpan;

    invoke-direct {v8}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v7}, Landroid/text/SpannableString;->length()I

    move-result v6

    invoke-virtual {v7, v8, v5, v6, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1934253
    iget-object v5, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934254
    iget-object v6, v5, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v5, v6

    .line 1934255
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/CtG;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1934256
    iget-object v5, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934257
    iget-object v6, v5, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v5, v6

    .line 1934258
    invoke-virtual {v5, v7}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934259
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->b()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$InfoFieldsDataModel;

    .line 1934260
    iget-object v1, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v1

    .line 1934261
    check-cast v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$InfoFieldsDataModel;->a()LX/0Px;

    move-result-object v0

    .line 1934262
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1934263
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1934264
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934265
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$ProfilePhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1934266
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->z:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1934267
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->z:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1934268
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->A:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1934269
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://graph.facebook.com/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->A:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/picture?width=100&height=100"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1934270
    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->z:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1934271
    :cond_1
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934272
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    .line 1934273
    iget-object v1, p1, LX/CmI;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-object v1, v1

    .line 1934274
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1934275
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934276
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1934277
    invoke-virtual {v2, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934278
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081c77

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->B:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1934279
    iget-object v3, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934280
    iget-object v0, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v3, v0

    .line 1934281
    invoke-virtual {v3, v2}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934282
    :cond_2
    return-void

    .line 1934283
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/8ba;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->B:Ljava/lang/String;

    .line 1934284
    iget-object v3, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1934285
    iget-object v4, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v3, v4

    .line 1934286
    iget-object v4, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->B:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1934287
    iget-object v3, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    new-instance v4, LX/CpB;

    invoke-direct {v4, v1, v0}, LX/CpB;-><init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;LX/0Px;)V

    invoke-virtual {v3, v4}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method
