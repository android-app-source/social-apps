.class public LX/ETR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2125022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/ETQ;Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2125034
    const/4 v2, 0x0

    move v3, v0

    .line 2125035
    :goto_0
    invoke-virtual {p0}, LX/ETQ;->size()I

    move-result v1

    if-ge v3, v1, :cond_1

    .line 2125036
    invoke-virtual {p0, v3}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v1

    .line 2125037
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v4

    .line 2125038
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2125039
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2125040
    :goto_1
    return v0

    .line 2125041
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 2125042
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_0

    .line 2125043
    :cond_1
    const/4 v0, -0x1

    goto :goto_1

    :cond_2
    move-object v1, v2

    goto :goto_2
.end method

.method public static a(LX/ETQ;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 2
    .param p2    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, -0x1

    .line 2125044
    if-nez p2, :cond_1

    .line 2125045
    :cond_0
    :goto_0
    return v0

    .line 2125046
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    .line 2125047
    if-eqz v1, :cond_0

    .line 2125048
    invoke-virtual {p0, p1}, LX/ETQ;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2125049
    invoke-static {v0, v1}, LX/ETR;->a(Ljava/util/Collection;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljava/util/Collection;Ljava/lang/String;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 2125023
    const/4 v0, 0x0

    .line 2125024
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125025
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2125026
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v0

    invoke-static {v0, p1}, LX/ETR;->a(Ljava/util/Collection;Ljava/lang/String;)I

    move-result v1

    .line 2125027
    :cond_0
    :goto_1
    return v1

    .line 2125028
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2125029
    if-eqz v0, :cond_3

    .line 2125030
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2125031
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 2125032
    goto :goto_0

    .line 2125033
    :cond_2
    const/4 v1, -0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
