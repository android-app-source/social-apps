.class public abstract LX/DvY;
.super LX/1Cv;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/DvZ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:LX/DvX;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2058731
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2058732
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/DvY;->a:Ljava/util/List;

    .line 2058733
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/DvY;->b:Ljava/util/List;

    .line 2058734
    new-instance v0, LX/DvX;

    invoke-direct {v0, p0}, LX/DvX;-><init>(LX/DvY;)V

    iput-object v0, p0, LX/DvY;->c:LX/DvX;

    .line 2058735
    return-void
.end method

.method private a(I)LX/DvZ;
    .locals 4

    .prologue
    .line 2058724
    const/4 v0, 0x0

    .line 2058725
    iget-object v1, p0, LX/DvY;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DvZ;

    .line 2058726
    invoke-virtual {v0}, LX/DvZ;->getCount()I

    move-result v3

    add-int/2addr v3, v1

    if-le v3, p1, :cond_0

    .line 2058727
    :goto_1
    return-object v0

    .line 2058728
    :cond_0
    invoke-virtual {v0}, LX/DvZ;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 2058729
    goto :goto_0

    .line 2058730
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(LX/DvZ;)I
    .locals 3

    .prologue
    .line 2058716
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2058717
    const/4 v0, 0x0

    .line 2058718
    iget-object v1, p0, LX/DvY;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DvZ;

    .line 2058719
    if-ne v0, p1, :cond_0

    .line 2058720
    return v1

    .line 2058721
    :cond_0
    invoke-virtual {v0}, LX/DvZ;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 2058722
    goto :goto_0

    .line 2058723
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to get absolute starting position of current adapter"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2058669
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2058715
    return-void
.end method

.method public final a(LX/DvZ;)V
    .locals 5

    .prologue
    .line 2058705
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2058706
    iget-object v0, p0, LX/DvY;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2058707
    invoke-virtual {p1}, LX/DvZ;->a()LX/0Px;

    move-result-object v2

    .line 2058708
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 2058709
    iget-object v4, p0, LX/DvY;->b:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2058710
    iget-object v4, p0, LX/DvY;->b:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2058711
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2058712
    :cond_1
    iget-object v0, p0, LX/DvY;->c:LX/DvX;

    invoke-virtual {p1, v0}, LX/DvZ;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2058713
    const v0, -0x7b6006f7

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2058714
    return-void
.end method

.method public final getCount()I
    .locals 3

    .prologue
    .line 2058700
    const/4 v0, 0x0

    .line 2058701
    iget-object v1, p0, LX/DvY;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DvZ;

    .line 2058702
    invoke-virtual {v0}, LX/DvZ;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 2058703
    goto :goto_0

    .line 2058704
    :cond_0
    return v1
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2058693
    invoke-direct {p0, p1}, LX/DvY;->a(I)LX/DvZ;

    move-result-object v0

    .line 2058694
    if-nez v0, :cond_0

    .line 2058695
    const/4 v0, 0x0

    .line 2058696
    :goto_0
    return-object v0

    .line 2058697
    :cond_0
    invoke-direct {p0, v0}, LX/DvY;->b(LX/DvZ;)I

    move-result v1

    .line 2058698
    sub-int v1, p1, v1

    .line 2058699
    invoke-virtual {v0, v1}, LX/DvZ;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2058686
    invoke-direct {p0, p1}, LX/DvY;->a(I)LX/DvZ;

    move-result-object v0

    .line 2058687
    if-nez v0, :cond_0

    .line 2058688
    const-wide/16 v0, 0x0

    .line 2058689
    :goto_0
    return-wide v0

    .line 2058690
    :cond_0
    invoke-direct {p0, v0}, LX/DvY;->b(LX/DvZ;)I

    move-result v1

    .line 2058691
    sub-int v1, p1, v1

    .line 2058692
    invoke-virtual {v0, v1}, LX/DvZ;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2058678
    invoke-direct {p0, p1}, LX/DvY;->a(I)LX/DvZ;

    move-result-object v0

    .line 2058679
    if-nez v0, :cond_0

    .line 2058680
    const/4 v0, 0x0

    .line 2058681
    :goto_0
    return v0

    .line 2058682
    :cond_0
    invoke-direct {p0, v0}, LX/DvY;->b(LX/DvZ;)I

    move-result v1

    .line 2058683
    sub-int v1, p1, v1

    .line 2058684
    invoke-virtual {v0, v1}, LX/DvZ;->a(I)Ljava/lang/Class;

    move-result-object v0

    .line 2058685
    iget-object v1, p0, LX/DvY;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2058671
    invoke-direct {p0, p1}, LX/DvY;->a(I)LX/DvZ;

    move-result-object v0

    .line 2058672
    if-nez v0, :cond_0

    .line 2058673
    const/4 v0, 0x0

    .line 2058674
    :goto_0
    return-object v0

    .line 2058675
    :cond_0
    invoke-direct {p0, v0}, LX/DvY;->b(LX/DvZ;)I

    move-result v1

    .line 2058676
    sub-int v1, p1, v1

    .line 2058677
    invoke-virtual {v0, v1, p2, p3}, LX/DvZ;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 2

    .prologue
    .line 2058670
    const/4 v0, 0x1

    iget-object v1, p0, LX/DvY;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method
