.class public LX/Een;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EeO;


# instance fields
.field private final a:LX/1wh;

.field private final b:Landroid/app/DownloadManager;

.field private final c:LX/1wq;


# direct methods
.method public constructor <init>(LX/1wh;Landroid/app/DownloadManager;LX/1wq;)V
    .locals 0

    .prologue
    .line 2153412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2153413
    iput-object p1, p0, LX/Een;->a:LX/1wh;

    .line 2153414
    iput-object p2, p0, LX/Een;->b:Landroid/app/DownloadManager;

    .line 2153415
    iput-object p3, p0, LX/Een;->c:LX/1wq;

    .line 2153416
    return-void
.end method

.method private static b(LX/EeX;)V
    .locals 4

    .prologue
    .line 2153417
    :try_start_0
    new-instance v0, Ljava/util/jar/JarFile;

    iget-object v1, p0, LX/EeX;->localFile:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V

    .line 2153418
    invoke-virtual {v0}, Ljava/util/jar/JarFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    .line 2153419
    return-void

    .line 2153420
    :catch_0
    move-exception v0

    .line 2153421
    new-instance v1, LX/Ef1;

    const-string v2, "verify_failure_ERROR_OPEN_DOWNLOADED_APK_IOEXCEPTION"

    const-string v3, "Open downloaded APK failed by IOException"

    invoke-direct {v1, v2, v0, v3}, LX/Ef1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1

    .line 2153422
    :catch_1
    move-exception v0

    .line 2153423
    new-instance v1, LX/Ef1;

    const-string v2, "verify_failure_ERROR_OPEN_DOWNLOADED_APK_SECURITYEXCEPTION"

    const-string v3, "Open downloaded APK failed by SecurityException"

    invoke-direct {v1, v2, v0, v3}, LX/Ef1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1

    .line 2153424
    :catch_2
    move-exception v0

    .line 2153425
    new-instance v1, LX/Ef1;

    const-string v2, "verify_failure_ERROR_OPEN_DOWNLOADED_APK_OOMERROR"

    const-string v3, "Open downloaded APK failed by OutOfMemoryError"

    invoke-direct {v1, v2, v0, v3}, LX/Ef1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a(LX/EeX;)LX/EeY;
    .locals 8

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x0

    .line 2153426
    iget-object v0, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v6}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2153427
    new-instance v0, LX/EeY;

    invoke-direct {v0}, LX/EeY;-><init>()V

    .line 2153428
    :goto_0
    return-object v0

    .line 2153429
    :cond_0
    iget-object v0, p0, LX/Een;->a:LX/1wh;

    const-string v1, "appupdate_verify_download_start"

    iget-object v2, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {p1}, LX/EeX;->d()LX/Eeh;

    move-result-object v3

    const-string v4, "task_start"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V

    .line 2153430
    iget-object v0, p0, LX/Een;->b:Landroid/app/DownloadManager;

    const/4 v1, 0x1

    new-array v1, v1, [J

    iget-wide v2, p1, LX/EeX;->downloadId:J

    aput-wide v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->remove([J)I

    .line 2153431
    invoke-virtual {p1}, LX/EeX;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2153432
    invoke-static {p1}, LX/Een;->b(LX/EeX;)V

    .line 2153433
    :cond_1
    iget-object v0, p0, LX/Een;->c:LX/1wq;

    invoke-interface {v0, p1}, LX/1wq;->a(LX/EeX;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2153434
    const-string v0, "Signature of installed app does not match newly downloaded apk."

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/EeM;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2153435
    new-instance v0, LX/EeW;

    invoke-direct {v0, p1}, LX/EeW;-><init>(LX/EeX;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2153436
    iput-object v1, v0, LX/EeW;->e:Ljava/lang/Integer;

    .line 2153437
    move-object v0, v0

    .line 2153438
    invoke-virtual {v0}, LX/EeW;->a()LX/EeX;

    move-result-object v1

    .line 2153439
    new-instance v0, LX/EeY;

    invoke-direct {v0, v1}, LX/EeY;-><init>(LX/EeX;)V

    goto :goto_0

    .line 2153440
    :cond_2
    new-instance v0, LX/EeW;

    invoke-direct {v0, p1}, LX/EeW;-><init>(LX/EeX;)V

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2153441
    iput-object v1, v0, LX/EeW;->e:Ljava/lang/Integer;

    .line 2153442
    move-object v0, v0

    .line 2153443
    const-wide/16 v2, -0x1

    .line 2153444
    iput-wide v2, v0, LX/EeW;->f:J

    .line 2153445
    move-object v0, v0

    .line 2153446
    invoke-virtual {v0}, LX/EeW;->a()LX/EeX;

    move-result-object v1

    .line 2153447
    iget-object v0, p0, LX/Een;->a:LX/1wh;

    const-string v2, "appupdate_verify_download_successful"

    invoke-virtual {p1}, LX/EeX;->c()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/1wh;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2153448
    iget-object v0, p0, LX/Een;->a:LX/1wh;

    const-string v2, "appupdate_verify_download_successful"

    iget-object v3, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {p1}, LX/EeX;->d()LX/Eeh;

    move-result-object v4

    const-string v5, "task_success"

    invoke-virtual {v0, v2, v3, v4, v5}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V

    .line 2153449
    new-instance v0, LX/EeY;

    invoke-direct {v0, v1}, LX/EeY;-><init>(LX/EeX;)V

    goto :goto_0
.end method
