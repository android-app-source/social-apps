.class public LX/D85;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2mz;


# instance fields
.field public final a:LX/B8t;

.field public b:Landroid/content/Context;

.field public c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

.field public d:LX/B6l;

.field public e:LX/D7g;

.field public f:LX/D83;

.field public g:Landroid/view/ViewGroup;

.field public h:LX/D8S;


# direct methods
.method public constructor <init>(LX/B8t;LX/B6l;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1967985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1967986
    iput-object p1, p0, LX/D85;->a:LX/B8t;

    .line 1967987
    iput-object p2, p0, LX/D85;->d:LX/B6l;

    .line 1967988
    return-void
.end method

.method public static h(LX/D85;)V
    .locals 2

    .prologue
    .line 1967982
    iget-object v0, p0, LX/D85;->h:LX/D8S;

    if-nez v0, :cond_0

    .line 1967983
    :goto_0
    return-void

    .line 1967984
    :cond_0
    iget-object v0, p0, LX/D85;->h:LX/D8S;

    iget-object v1, p0, LX/D85;->e:LX/D7g;

    invoke-interface {v1}, LX/D7g;->b()F

    move-result v1

    invoke-virtual {v0, v1}, LX/D8S;->a(F)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1967981
    const/4 v0, 0x0

    return v0
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1967953
    iget-object v1, p0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    if-nez v1, :cond_0

    .line 1967954
    :goto_0
    iget-object v1, p0, LX/D85;->f:LX/D83;

    if-nez v1, :cond_3

    .line 1967955
    :goto_1
    iput-object v0, p0, LX/D85;->b:Landroid/content/Context;

    .line 1967956
    iput-object v0, p0, LX/D85;->e:LX/D7g;

    .line 1967957
    iput-object v0, p0, LX/D85;->g:Landroid/view/ViewGroup;

    .line 1967958
    iput-object v0, p0, LX/D85;->h:LX/D8S;

    .line 1967959
    return-void

    .line 1967960
    :cond_0
    iget-object v1, p0, LX/D85;->b:Landroid/content/Context;

    const-class v2, LX/0ew;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ew;

    .line 1967961
    if-eqz v1, :cond_2

    .line 1967962
    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iget-object v2, p0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-virtual {v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1967963
    iget-object v1, p0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    const/4 v3, 0x0

    .line 1967964
    iget-object v2, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_1

    .line 1967965
    iget-object v2, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1967966
    iget-object v2, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1967967
    :cond_1
    iput-object v3, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->h:LX/0hB;

    .line 1967968
    iput-object v3, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->i:LX/B7W;

    .line 1967969
    iput-object v3, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->d:Landroid/widget/LinearLayout;

    .line 1967970
    iput-object v3, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->a:LX/0gc;

    .line 1967971
    iput-object v3, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->b:LX/B6H;

    .line 1967972
    iput-object v3, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->c:Landroid/view/ViewGroup;

    .line 1967973
    iput-object v3, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->e:LX/D8V;

    .line 1967974
    iput-object v3, v1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->g:Landroid/widget/LinearLayout;

    .line 1967975
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    goto :goto_0

    .line 1967976
    :cond_3
    iget-object v1, p0, LX/D85;->f:LX/D83;

    invoke-virtual {v1}, LX/D83;->a()Z

    .line 1967977
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_4

    .line 1967978
    iget-object v1, p0, LX/D85;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, LX/D85;->f:LX/D83;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1967979
    :goto_2
    const/4 v1, 0x0

    iput-object v1, p0, LX/D85;->f:LX/D83;

    goto :goto_1

    .line 1967980
    :cond_4
    iget-object v1, p0, LX/D85;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, LX/D85;->f:LX/D83;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_2
.end method

.method public final a(ILandroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D8b;LX/D8V;ILX/D8S;)V
    .locals 1
    .param p9    # LX/D8S;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/D8b;",
            "Lcom/facebook/video/watchandmore/core/OnExitWatchAndMoreListener;",
            "I",
            "Lcom/facebook/video/watchandmore/core/WatchAndMoreContentAnimationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1967905
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/D85;->b:Landroid/content/Context;

    .line 1967906
    iget-object v0, p5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1967907
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {p5, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1967908
    iput-object p2, p0, LX/D85;->g:Landroid/view/ViewGroup;

    .line 1967909
    iput-object p9, p0, LX/D85;->h:LX/D8S;

    .line 1967910
    iget-object p1, p0, LX/D85;->b:Landroid/content/Context;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1967911
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1967912
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1967913
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1967914
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1967915
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const p2, 0x46a1c4a4

    invoke-static {p1, p2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p1

    .line 1967916
    if-nez p1, :cond_0

    .line 1967917
    :goto_0
    iget-object p1, p5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1967918
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1967919
    iget-object p2, p0, LX/D85;->d:LX/B6l;

    invoke-static {p5}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object p3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result p1

    invoke-static {v0}, LX/2sb;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result p4

    invoke-virtual {p2, p3, p1, p4}, LX/B6l;->a(LX/0lF;ZI)V

    .line 1967920
    iget-object p1, p0, LX/D85;->d:LX/B6l;

    const-string p2, "cta_lead_gen_open_popover"

    invoke-virtual {p1, p2}, LX/B6l;->a(Ljava/lang/String;)V

    .line 1967921
    return-void

    .line 1967922
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object p2

    .line 1967923
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->l()Ljava/lang/String;

    move-result-object p1

    .line 1967924
    iget-object p3, p0, LX/D85;->a:LX/B8t;

    invoke-virtual {p3, p1}, LX/B8t;->a(Ljava/lang/String;)Z

    move-result p3

    .line 1967925
    iget-object p1, p0, LX/D85;->b:Landroid/content/Context;

    const-class p4, LX/0ew;

    invoke-static {p1, p4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/0ew;

    .line 1967926
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1967927
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->j()Z

    move-result p2

    if-nez p2, :cond_2

    :cond_1
    if-eqz p3, :cond_4

    .line 1967928
    :cond_2
    sget-object p2, LX/B76;->SUCCESS:LX/B76;

    invoke-static {v0, p2}, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/B76;)Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    move-result-object p2

    .line 1967929
    :goto_1
    invoke-interface {p1}, LX/0ew;->iC_()LX/0gc;

    move-result-object p1

    .line 1967930
    new-instance p3, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    invoke-direct {p3}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;-><init>()V

    .line 1967931
    iput-object p2, p3, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->b:LX/B6H;

    .line 1967932
    move-object p4, p3

    .line 1967933
    iput-object p1, p4, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->a:LX/0gc;

    .line 1967934
    move-object p4, p4

    .line 1967935
    iput p8, p4, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->f:I

    .line 1967936
    move-object p4, p4

    .line 1967937
    iput-object p7, p4, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->e:LX/D8V;

    .line 1967938
    move-object p1, p3

    .line 1967939
    iput-object p1, p0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967940
    iget-object p1, p0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    .line 1967941
    iget-object p2, p1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->a:LX/0gc;

    invoke-virtual {p2}, LX/0gc;->a()LX/0hH;

    move-result-object p2

    const p3, 0x7f0d3199

    invoke-virtual {p2, p3, p1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object p2

    invoke-virtual {p2}, LX/0hH;->b()I

    .line 1967942
    iget-object p2, p1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->a:LX/0gc;

    invoke-virtual {p2}, LX/0gc;->b()Z

    .line 1967943
    iget-object p2, p1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->b:LX/B6H;

    if-eqz p2, :cond_3

    .line 1967944
    iget-object p2, p1, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->b:LX/B6H;

    invoke-virtual {p1, p2}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->a(LX/B6H;)V

    .line 1967945
    :cond_3
    new-instance p1, LX/D83;

    iget-object p2, p0, LX/D85;->b:Landroid/content/Context;

    invoke-virtual {p0}, LX/D85;->e()LX/D7g;

    move-result-object p3

    neg-int p4, p8

    invoke-direct {p1, p0, p2, p3, p4}, LX/D83;-><init>(LX/D85;Landroid/content/Context;LX/D7g;I)V

    iput-object p1, p0, LX/D85;->f:LX/D83;

    .line 1967946
    iget-object p1, p0, LX/D85;->g:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    iget-object p2, p0, LX/D85;->f:LX/D83;

    invoke-virtual {p1, p2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto/16 :goto_0

    .line 1967947
    :cond_4
    new-instance p2, LX/D82;

    invoke-direct {p2, p0, p8}, LX/D82;-><init>(LX/D85;I)V

    .line 1967948
    invoke-static {v0}, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    move-result-object p3

    .line 1967949
    iput-object p2, p3, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->D:LX/D82;

    .line 1967950
    move-object p3, p3

    .line 1967951
    move-object p2, p3

    .line 1967952
    goto :goto_1
.end method

.method public final a(Landroid/content/res/Configuration;I)V
    .locals 1

    .prologue
    .line 1967902
    iget-object v0, p0, LX/D85;->f:LX/D83;

    if-nez v0, :cond_0

    .line 1967903
    :goto_0
    return-void

    .line 1967904
    :cond_0
    iget-object v0, p0, LX/D85;->f:LX/D83;

    invoke-virtual {v0}, LX/D83;->a()Z

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1967884
    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1967901
    const/4 v0, 0x1

    return v0
.end method

.method public final c()LX/D8g;
    .locals 1

    .prologue
    .line 1967900
    sget-object v0, LX/D8g;->WATCH_AND_LEADGEN:LX/D8g;

    return-object v0
.end method

.method public final d()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1967888
    iget-object v1, p0, LX/D85;->b:Landroid/content/Context;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/D85;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1967889
    :cond_0
    :goto_0
    return v0

    .line 1967890
    :cond_1
    iget-object v1, p0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/D85;->c:Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;

    const/4 v2, 0x1

    .line 1967891
    invoke-static {v1}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->m(Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;)LX/B6H;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-static {v1}, Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;->m(Lcom/facebook/video/watchandleadgen/WatchAndLeadGenMultiPageFragment;)LX/B6H;

    move-result-object v3

    invoke-interface {v3}, LX/B6H;->c()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1967892
    :goto_1
    move v1, v2

    .line 1967893
    if-eqz v1, :cond_0

    .line 1967894
    iget-object v0, p0, LX/D85;->e:LX/D7g;

    if-eqz v0, :cond_2

    .line 1967895
    iget-object v0, p0, LX/D85;->e:LX/D7g;

    invoke-interface {v0}, LX/D7g;->j()V

    .line 1967896
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1967897
    :cond_3
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    invoke-virtual {v3}, LX/0gc;->f()I

    move-result v3

    if-le v3, v2, :cond_4

    .line 1967898
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    invoke-virtual {v3}, LX/0gc;->d()V

    goto :goto_1

    .line 1967899
    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final e()LX/D7g;
    .locals 2

    .prologue
    .line 1967885
    iget-object v0, p0, LX/D85;->e:LX/D7g;

    if-nez v0, :cond_0

    .line 1967886
    new-instance v0, LX/D84;

    invoke-direct {v0, p0}, LX/D84;-><init>(LX/D85;)V

    iput-object v0, p0, LX/D85;->e:LX/D7g;

    .line 1967887
    :cond_0
    iget-object v0, p0, LX/D85;->e:LX/D7g;

    return-object v0
.end method
