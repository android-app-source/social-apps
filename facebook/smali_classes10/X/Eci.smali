.class public final LX/Eci;
.super LX/EWj;
.source ""

# interfaces
.implements LX/Ech;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/Eci;",
        ">;",
        "LX/Ech;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:LX/EWc;

.field private d:LX/EWc;

.field private e:LX/EWc;

.field private f:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2147759
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2147760
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eci;->c:LX/EWc;

    .line 2147761
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eci;->d:LX/EWc;

    .line 2147762
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eci;->e:LX/EWc;

    .line 2147763
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2147764
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2147765
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eci;->c:LX/EWc;

    .line 2147766
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eci;->d:LX/EWc;

    .line 2147767
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Eci;->e:LX/EWc;

    .line 2147768
    return-void
.end method

.method private d(LX/EWY;)LX/Eci;
    .locals 1

    .prologue
    .line 2147769
    instance-of v0, p1, LX/Ecj;

    if-eqz v0, :cond_0

    .line 2147770
    check-cast p1, LX/Ecj;

    invoke-virtual {p0, p1}, LX/Eci;->a(LX/Ecj;)LX/Eci;

    move-result-object p0

    .line 2147771
    :goto_0
    return-object p0

    .line 2147772
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/Eci;
    .locals 4

    .prologue
    .line 2147773
    const/4 v2, 0x0

    .line 2147774
    :try_start_0
    sget-object v0, LX/Ecj;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ecj;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2147775
    if-eqz v0, :cond_0

    .line 2147776
    invoke-virtual {p0, v0}, LX/Eci;->a(LX/Ecj;)LX/Eci;

    .line 2147777
    :cond_0
    return-object p0

    .line 2147778
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2147779
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2147780
    check-cast v0, LX/Ecj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2147781
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2147782
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2147783
    invoke-virtual {p0, v1}, LX/Eci;->a(LX/Ecj;)LX/Eci;

    :cond_1
    throw v0

    .line 2147784
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static u()LX/Eci;
    .locals 1

    .prologue
    .line 2147785
    new-instance v0, LX/Eci;

    invoke-direct {v0}, LX/Eci;-><init>()V

    return-object v0
.end method

.method private w()LX/Eci;
    .locals 2

    .prologue
    .line 2147786
    invoke-static {}, LX/Eci;->u()LX/Eci;

    move-result-object v0

    invoke-direct {p0}, LX/Eci;->y()LX/Ecj;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Eci;->a(LX/Ecj;)LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/Ecj;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2147787
    new-instance v2, LX/Ecj;

    invoke-direct {v2, p0}, LX/Ecj;-><init>(LX/EWj;)V

    .line 2147788
    iget v3, p0, LX/Eci;->a:I

    .line 2147789
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    .line 2147790
    :goto_0
    iget v1, p0, LX/Eci;->b:I

    .line 2147791
    iput v1, v2, LX/Ecj;->id_:I

    .line 2147792
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2147793
    or-int/lit8 v0, v0, 0x2

    .line 2147794
    :cond_0
    iget-object v1, p0, LX/Eci;->c:LX/EWc;

    .line 2147795
    iput-object v1, v2, LX/Ecj;->publicKey_:LX/EWc;

    .line 2147796
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2147797
    or-int/lit8 v0, v0, 0x4

    .line 2147798
    :cond_1
    iget-object v1, p0, LX/Eci;->d:LX/EWc;

    .line 2147799
    iput-object v1, v2, LX/Ecj;->privateKey_:LX/EWc;

    .line 2147800
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2147801
    or-int/lit8 v0, v0, 0x8

    .line 2147802
    :cond_2
    iget-object v1, p0, LX/Eci;->e:LX/EWc;

    .line 2147803
    iput-object v1, v2, LX/Ecj;->signature_:LX/EWc;

    .line 2147804
    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    .line 2147805
    or-int/lit8 v0, v0, 0x10

    .line 2147806
    :cond_3
    iget-wide v4, p0, LX/Eci;->f:J

    .line 2147807
    iput-wide v4, v2, LX/Ecj;->timestamp_:J

    .line 2147808
    iput v0, v2, LX/Ecj;->bitField0_:I

    .line 2147809
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2147810
    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2147811
    invoke-direct {p0, p1}, LX/Eci;->d(LX/EWY;)LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2147812
    invoke-direct {p0, p1, p2}, LX/Eci;->d(LX/EWd;LX/EYZ;)LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/Eci;
    .locals 1

    .prologue
    .line 2147813
    iget v0, p0, LX/Eci;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Eci;->a:I

    .line 2147814
    iput p1, p0, LX/Eci;->b:I

    .line 2147815
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147816
    return-object p0
.end method

.method public final a(J)LX/Eci;
    .locals 1

    .prologue
    .line 2147817
    iget v0, p0, LX/Eci;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/Eci;->a:I

    .line 2147818
    iput-wide p1, p0, LX/Eci;->f:J

    .line 2147819
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147820
    return-object p0
.end method

.method public final a(LX/EWc;)LX/Eci;
    .locals 1

    .prologue
    .line 2147821
    if-nez p1, :cond_0

    .line 2147822
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2147823
    :cond_0
    iget v0, p0, LX/Eci;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/Eci;->a:I

    .line 2147824
    iput-object p1, p0, LX/Eci;->c:LX/EWc;

    .line 2147825
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147826
    return-object p0
.end method

.method public final a(LX/Ecj;)LX/Eci;
    .locals 4

    .prologue
    .line 2147827
    sget-object v0, LX/Ecj;->c:LX/Ecj;

    move-object v0, v0

    .line 2147828
    if-ne p1, v0, :cond_0

    .line 2147829
    :goto_0
    return-object p0

    .line 2147830
    :cond_0
    const/4 v0, 0x1

    .line 2147831
    iget v1, p1, LX/Ecj;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_6

    :goto_1
    move v0, v0

    .line 2147832
    if-eqz v0, :cond_1

    .line 2147833
    iget v0, p1, LX/Ecj;->id_:I

    move v0, v0

    .line 2147834
    invoke-virtual {p0, v0}, LX/Eci;->a(I)LX/Eci;

    .line 2147835
    :cond_1
    iget v0, p1, LX/Ecj;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2147836
    if-eqz v0, :cond_2

    .line 2147837
    iget-object v0, p1, LX/Ecj;->publicKey_:LX/EWc;

    move-object v0, v0

    .line 2147838
    invoke-virtual {p0, v0}, LX/Eci;->a(LX/EWc;)LX/Eci;

    .line 2147839
    :cond_2
    iget v0, p1, LX/Ecj;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2147840
    if-eqz v0, :cond_3

    .line 2147841
    iget-object v0, p1, LX/Ecj;->privateKey_:LX/EWc;

    move-object v0, v0

    .line 2147842
    invoke-virtual {p0, v0}, LX/Eci;->b(LX/EWc;)LX/Eci;

    .line 2147843
    :cond_3
    iget v0, p1, LX/Ecj;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2147844
    if-eqz v0, :cond_4

    .line 2147845
    iget-object v0, p1, LX/Ecj;->signature_:LX/EWc;

    move-object v0, v0

    .line 2147846
    invoke-virtual {p0, v0}, LX/Eci;->c(LX/EWc;)LX/Eci;

    .line 2147847
    :cond_4
    iget v0, p1, LX/Ecj;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 2147848
    if-eqz v0, :cond_5

    .line 2147849
    iget-wide v2, p1, LX/Ecj;->timestamp_:J

    move-wide v0, v2

    .line 2147850
    invoke-virtual {p0, v0, v1}, LX/Eci;->a(J)LX/Eci;

    .line 2147851
    :cond_5
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    :cond_a
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2147734
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2147758
    invoke-direct {p0, p1, p2}, LX/Eci;->d(LX/EWd;LX/EYZ;)LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2147852
    invoke-direct {p0}, LX/Eci;->w()LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/EWc;)LX/Eci;
    .locals 1

    .prologue
    .line 2147728
    if-nez p1, :cond_0

    .line 2147729
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2147730
    :cond_0
    iget v0, p0, LX/Eci;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/Eci;->a:I

    .line 2147731
    iput-object p1, p0, LX/Eci;->d:LX/EWc;

    .line 2147732
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147733
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2147735
    invoke-direct {p0, p1, p2}, LX/Eci;->d(LX/EWd;LX/EYZ;)LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2147736
    invoke-direct {p0}, LX/Eci;->w()LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2147737
    invoke-direct {p0, p1}, LX/Eci;->d(LX/EWY;)LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/EWc;)LX/Eci;
    .locals 1

    .prologue
    .line 2147738
    if-nez p1, :cond_0

    .line 2147739
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2147740
    :cond_0
    iget v0, p0, LX/Eci;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/Eci;->a:I

    .line 2147741
    iput-object p1, p0, LX/Eci;->e:LX/EWc;

    .line 2147742
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2147743
    return-object p0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2147744
    invoke-direct {p0}, LX/Eci;->w()LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2147745
    sget-object v0, LX/Eck;->r:LX/EYn;

    const-class v1, LX/Ecj;

    const-class v2, LX/Eci;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2147746
    sget-object v0, LX/Eck;->q:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2147747
    invoke-direct {p0}, LX/Eci;->w()LX/Eci;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2147748
    invoke-direct {p0}, LX/Eci;->y()LX/Ecj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2147749
    invoke-virtual {p0}, LX/Eci;->l()LX/Ecj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2147750
    invoke-direct {p0}, LX/Eci;->y()LX/Ecj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2147751
    invoke-virtual {p0}, LX/Eci;->l()LX/Ecj;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/Ecj;
    .locals 2

    .prologue
    .line 2147752
    invoke-direct {p0}, LX/Eci;->y()LX/Ecj;

    move-result-object v0

    .line 2147753
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2147754
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2147755
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2147756
    sget-object v0, LX/Ecj;->c:LX/Ecj;

    move-object v0, v0

    .line 2147757
    return-object v0
.end method
