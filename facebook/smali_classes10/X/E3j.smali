.class public final LX/E3j;
.super LX/37T;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;

.field private final b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field private final c:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2075143
    iput-object p1, p0, LX/E3j;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;

    invoke-direct {p0}, LX/37T;-><init>()V

    .line 2075144
    iput-object p2, p0, LX/E3j;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075145
    new-instance v0, LX/E3i;

    invoke-direct {v0, p1, p2, p3}, LX/E3i;-><init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHeaderWithVerifiedBadgeComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Landroid/content/Context;)V

    iput-object v0, p0, LX/E3j;->c:LX/1KL;

    .line 2075146
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 1

    .prologue
    .line 2075142
    const/4 v0, 0x0

    return v0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2075141
    iget-object v0, p0, LX/E3j;->c:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2075138
    iget-object v0, p0, LX/E3j;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075139
    iget-object p0, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, p0

    .line 2075140
    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-static {v0}, LX/9JZ;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 2075137
    iget-object v0, p0, LX/E3j;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    return-object v0
.end method
