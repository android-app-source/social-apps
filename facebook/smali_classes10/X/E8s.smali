.class public LX/E8s;
.super Landroid/view/View;
.source ""

# interfaces
.implements LX/0hY;


# instance fields
.field public a:Landroid/view/View;

.field public b:Z

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2083530
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2083531
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/E8s;->c:Z

    .line 2083532
    iput-object p1, p0, LX/E8s;->a:Landroid/view/View;

    .line 2083533
    return-void
.end method


# virtual methods
.method public final a(LX/31M;II)Z
    .locals 1

    .prologue
    .line 2083534
    iget-boolean v0, p0, LX/E8s;->c:Z

    return v0
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2083535
    iget-boolean v0, p0, LX/E8s;->b:Z

    if-eqz v0, :cond_0

    .line 2083536
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/E8s;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2083537
    :cond_0
    iget-object v0, p0, LX/E8s;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2083538
    iget-object v0, p0, LX/E8s;->a:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E8s;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    invoke-virtual {v0}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->getContentViewHeight()I

    move-result v0

    .line 2083539
    :goto_0
    iget-object v1, p0, LX/E8s;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, LX/E8s;->setMeasuredDimension(II)V

    .line 2083540
    return-void

    .line 2083541
    :cond_0
    iget-object v0, p0, LX/E8s;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method
