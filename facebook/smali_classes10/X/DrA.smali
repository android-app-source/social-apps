.class public final LX/DrA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DrB;


# direct methods
.method public constructor <init>(LX/DrB;)V
    .locals 0

    .prologue
    .line 2049011
    iput-object p1, p0, LX/DrA;->a:LX/DrB;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2049012
    sget-object v0, LX/DrB;->a:Ljava/lang/Class;

    const-string v1, "Failed to fetch the notification bucket settings from the server - "

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2049013
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2049014
    check-cast p1, Ljava/util/List;

    .line 2049015
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2049016
    :cond_0
    :goto_0
    return-void

    .line 2049017
    :cond_1
    iget-object v0, p0, LX/DrA;->a:LX/DrB;

    iget-object v0, v0, LX/DrB;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsController$PersistanceRunnable;

    iget-object v2, p0, LX/DrA;->a:LX/DrB;

    invoke-direct {v1, v2, p1}, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsController$PersistanceRunnable;-><init>(LX/DrB;Ljava/util/List;)V

    const v2, -0x71c9f027

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
