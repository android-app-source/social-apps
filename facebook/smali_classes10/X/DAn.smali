.class public LX/DAn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/3OQ;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/DAp;


# direct methods
.method public constructor <init>(Ljava/util/Collection;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1971458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1971459
    new-instance v0, LX/DAp;

    invoke-direct {v0, p1, p2}, LX/DAp;-><init>(Ljava/util/Collection;Z)V

    iput-object v0, p0, LX/DAn;->a:LX/DAp;

    .line 1971460
    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 1971461
    check-cast p1, LX/3OQ;

    check-cast p2, LX/3OQ;

    .line 1971462
    new-instance v1, LX/DAm;

    invoke-direct {v1, p1}, LX/DAm;-><init>(LX/3OQ;)V

    .line 1971463
    new-instance v2, LX/DAm;

    invoke-direct {v2, p2}, LX/DAm;-><init>(LX/3OQ;)V

    .line 1971464
    iget-object v0, v2, LX/DAm;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v3, v1, LX/DAm;->a:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    .line 1971465
    if-eqz v0, :cond_0

    .line 1971466
    :goto_0
    return v0

    .line 1971467
    :cond_0
    iget-boolean v0, v1, LX/DAm;->c:Z

    if-eqz v0, :cond_1

    iget-boolean v0, v2, LX/DAm;->c:Z

    if-eqz v0, :cond_1

    .line 1971468
    const/4 v0, 0x0

    goto :goto_0

    .line 1971469
    :cond_1
    iget-boolean v0, v1, LX/DAm;->b:Z

    if-eqz v0, :cond_2

    iget-boolean v0, v2, LX/DAm;->b:Z

    if-eqz v0, :cond_2

    .line 1971470
    iget-object v0, p0, LX/DAn;->a:LX/DAp;

    check-cast p1, LX/3OO;

    .line 1971471
    iget-object v1, p1, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v1, v1

    .line 1971472
    check-cast p2, LX/3OO;

    .line 1971473
    iget-object v2, p2, LX/3OO;->a:Lcom/facebook/user/model/User;

    move-object v2, v2

    .line 1971474
    invoke-virtual {v0, v1, v2}, LX/DAp;->a(Lcom/facebook/user/model/User;Lcom/facebook/user/model/User;)I

    move-result v0

    goto :goto_0

    .line 1971475
    :cond_2
    iget-boolean v0, v1, LX/DAm;->b:Z

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
