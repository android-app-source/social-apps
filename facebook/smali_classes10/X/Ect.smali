.class public final LX/Ect;
.super LX/Ecs;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Ecs",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/Ect;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2148079
    new-instance v0, LX/Ect;

    invoke-direct {v0}, LX/Ect;-><init>()V

    sput-object v0, LX/Ect;->a:LX/Ect;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2148080
    invoke-direct {p0}, LX/Ecs;-><init>()V

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2148077
    sget-object v0, LX/Ect;->a:LX/Ect;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2148078
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2148076
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "value is absent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2148075
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2148074
    const v0, 0x598df91c

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2148073
    const-string v0, "Optional.absent()"

    return-object v0
.end method
