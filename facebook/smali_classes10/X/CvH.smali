.class public abstract LX/CvH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1947976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/search/model/TypeaheadUnit;LX/0Px;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 1947977
    if-nez p1, :cond_0

    move v0, v3

    .line 1947978
    :goto_0
    return v0

    .line 1947979
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/model/TypeaheadUnit;->m()Z

    move-result v1

    const-string v2, "We should not look for the position of a non-result row"

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    move v1, v0

    move v2, v0

    .line 1947980
    :goto_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1947981
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 1947982
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->m()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1947983
    add-int/lit8 v2, v2, 0x1

    .line 1947984
    :cond_1
    if-ne v0, p0, :cond_2

    .line 1947985
    sub-int v0, v1, v2

    goto :goto_0

    .line 1947986
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v3

    .line 1947987
    goto :goto_0
.end method

.method public static a(LX/0m9;Lcom/facebook/search/model/EntityTypeaheadUnit;)V
    .locals 3

    .prologue
    .line 1947988
    const-string v0, "connected_state"

    invoke-virtual {p1}, Lcom/facebook/search/model/EntityTypeaheadUnit;->K()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1947989
    const-string v0, "semantic"

    .line 1947990
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1947991
    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1947992
    const-string v0, "text"

    .line 1947993
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1947994
    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1947995
    const-string v0, "result_style_list"

    .line 1947996
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 1947997
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1947998
    const-string v0, "type"

    .line 1947999
    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 1948000
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1948001
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)LX/162;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1948002
    new-instance v3, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v1}, LX/162;-><init>(LX/0mC;)V

    .line 1948003
    if-nez p1, :cond_0

    move-object v0, v3

    .line 1948004
    :goto_0
    return-object v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 1948005
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1948006
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 1948007
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->m()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1948008
    add-int/lit8 v2, v2, 0x1

    .line 1948009
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1948010
    :cond_1
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 1948011
    const-string v5, "index_in_group"

    sub-int v6, v1, v2

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1948012
    const-string v5, "is_scoped"

    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->E()Z

    move-result v6

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1948013
    invoke-virtual {p0, v4, v0}, LX/CvH;->a(LX/0m9;Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 1948014
    invoke-virtual {v3, v4}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_2

    :cond_2
    move-object v0, v3

    .line 1948015
    goto :goto_0
.end method

.method public abstract a(LX/0m9;Lcom/facebook/search/model/TypeaheadUnit;)V
.end method
