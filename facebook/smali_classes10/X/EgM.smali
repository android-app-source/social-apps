.class public final LX/EgM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bookmark/client/BookmarkClient;


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/client/BookmarkClient;)V
    .locals 0

    .prologue
    .line 2156040
    iput-object p1, p0, LX/EgM;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2156041
    iget-object v1, p0, LX/EgM;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    monitor-enter v1

    .line 2156042
    :try_start_0
    iget-object v0, p0, LX/EgM;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    const/4 v2, 0x0

    .line 2156043
    iput-object v2, v0, Lcom/facebook/bookmark/client/BookmarkClient;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2156044
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2156045
    iget-object v0, p0, LX/EgM;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    const-string v1, "syncWithServer"

    invoke-static {v0, p1, v1}, Lcom/facebook/bookmark/client/BookmarkClient;->a$redex0(Lcom/facebook/bookmark/client/BookmarkClient;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2156046
    return-void

    .line 2156047
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2156048
    iget-object v1, p0, LX/EgM;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    monitor-enter v1

    .line 2156049
    :try_start_0
    iget-object v0, p0, LX/EgM;->a:Lcom/facebook/bookmark/client/BookmarkClient;

    const/4 v2, 0x0

    .line 2156050
    iput-object v2, v0, Lcom/facebook/bookmark/client/BookmarkClient;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2156051
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
