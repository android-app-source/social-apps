.class public final LX/EhR;
.super LX/2lH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2lH",
        "<",
        "LX/EhS;",
        "Lcom/facebook/bookmark/model/Bookmark;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)V
    .locals 2

    .prologue
    .line 2158423
    iput-object p1, p0, LX/EhR;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    .line 2158424
    const v0, 0x7f0301c6

    iget-object v1, p1, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b:Landroid/view/LayoutInflater;

    invoke-direct {p0, p2, v0, p3, v1}, LX/2lH;-><init>(LX/2lb;ILjava/lang/Object;Landroid/view/LayoutInflater;)V

    .line 2158425
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2158440
    move-object v0, p1

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f0e01ee

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    move-object v0, p1

    .line 2158441
    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f0e01ef

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 2158442
    new-instance v0, LX/EhS;

    invoke-direct {v0, p1}, LX/EhS;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2158426
    check-cast p1, LX/EhS;

    .line 2158427
    iget-object v0, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f020b5e

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2158428
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2158429
    :try_start_0
    iget-object v1, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2158430
    :cond_0
    :goto_0
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    const-string v1, "page"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2158431
    iget-object v0, p0, LX/EhR;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    iget-object v0, v0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    const v1, 0x7f082a71

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2158432
    :goto_1
    iget-object v2, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2158433
    iget-object v0, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2158434
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2158435
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ". "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2158436
    iget-object v0, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2158437
    return-void

    .line 2158438
    :catch_0
    iget-object v0, p1, LX/EhS;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    goto :goto_0

    .line 2158439
    :cond_1
    iget-object v0, p0, LX/EhR;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    iget-object v0, v0, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a:Landroid/app/Activity;

    const v1, 0x7f082a70

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method
