.class public final LX/D4p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Lg;
.implements LX/7Li;
.implements LX/7Lk;


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)V
    .locals 0

    .prologue
    .line 1962733
    iput-object p1, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;B)V
    .locals 0

    .prologue
    .line 1962766
    invoke-direct {p0, p1}, LX/D4p;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)V

    return-void
.end method

.method private a(LX/04g;LX/0QK;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/04g;",
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 1962734
    const/4 v0, 0x0

    .line 1962735
    if-eqz p2, :cond_0

    .line 1962736
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->getCurrentVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1962737
    :cond_0
    iget-object v1, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a:LX/D5t;

    invoke-virtual {v1, v0}, LX/D5t;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2pa;

    move-result-object v6

    .line 1962738
    if-nez v6, :cond_1

    .line 1962739
    const/4 v0, 0x0

    .line 1962740
    :goto_0
    return v0

    .line 1962741
    :cond_1
    sget-object v1, LX/04g;->BY_USER_SWIPE:LX/04g;

    if-ne p1, v1, :cond_2

    .line 1962742
    sget-object p1, LX/04g;->BY_USER:LX/04g;

    .line 1962743
    :cond_2
    iget-object v1, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1962744
    iget-object v1, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-static {v1}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->b(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)V

    .line 1962745
    iget-object v1, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1962746
    iget-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v1, v2

    .line 1962747
    if-eqz v1, :cond_4

    iget-object v1, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1962748
    iget-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v1, v2

    .line 1962749
    invoke-virtual {v1}, LX/2pb;->f()LX/2oi;

    move-result-object v1

    .line 1962750
    :goto_1
    iget-object v2, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    .line 1962751
    invoke-static {v2, v6}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;LX/2pa;)V

    .line 1962752
    iget-object v2, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    .line 1962753
    iput-object v6, v2, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    .line 1962754
    iget-object v2, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    .line 1962755
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1962756
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1962757
    invoke-static {v2, v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->a$redex0(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1962758
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v6}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1962759
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oi;LX/04g;)V

    .line 1962760
    invoke-direct {p0, p1}, LX/D4p;->d(LX/04g;)V

    .line 1962761
    sget-object v0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    if-ne v1, v0, :cond_3

    .line 1962762
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->b:LX/1C2;

    iget-object v1, v6, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, v6, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v3, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v3, v3, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->n:LX/04D;

    sget-object v4, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    iget-object v5, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v5, v5, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v5

    iget-object v6, v6, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v0 .. v6}, LX/1C2;->a(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;IZ)LX/1C2;

    .line 1962763
    :cond_3
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1962764
    const/4 v0, 0x1

    goto :goto_0

    .line 1962765
    :cond_4
    sget-object v1, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    goto :goto_1
.end method

.method private d(LX/04g;)V
    .locals 8

    .prologue
    .line 1962730
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1962731
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->b:LX/1C2;

    iget-object v1, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v2, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v4, v4, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    iget-object v5, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v5, v5, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    iget-object v5, v5, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v6, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v6, v6, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->n:LX/04D;

    iget-object v7, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v7, v7, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->m:LX/2pa;

    iget-object v7, v7, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1962732
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1962728
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->o:LX/0QK;

    const-string v1, "setNextStoryFinder(..) was not called"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1962729
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1962726
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->p:LX/0QK;

    const-string v1, "setPreviousStoryFinder(..) was not called"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1962727
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1962767
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 1962705
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->l:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1962706
    invoke-direct {p0, p1}, LX/D4p;->d(LX/04g;)V

    .line 1962707
    return-void
.end method

.method public final b(LX/04g;)V
    .locals 2

    .prologue
    .line 1962708
    invoke-direct {p0}, LX/D4p;->e()V

    .line 1962709
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->o:LX/0QK;

    invoke-direct {p0, p1, v0}, LX/D4p;->a(LX/04g;LX/0QK;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->r:Ljava/util/Map;

    if-eqz v0, :cond_0

    sget-object v0, LX/04g;->BY_USER_SWIPE:LX/04g;

    if-ne p1, v0, :cond_0

    .line 1962710
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->i(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)I

    .line 1962711
    :cond_0
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->c:LX/D4G;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/D4G;->a(Z)V

    .line 1962712
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1962713
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->q:LX/0QK;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->o:LX/0QK;

    if-nez v0, :cond_1

    .line 1962714
    :cond_0
    const/4 v0, 0x0

    .line 1962715
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->q:LX/0QK;

    iget-object v1, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->o:LX/0QK;

    iget-object v2, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->getCurrentVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final c(LX/04g;)V
    .locals 2

    .prologue
    .line 1962716
    invoke-direct {p0}, LX/D4p;->f()V

    .line 1962717
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->p:LX/0QK;

    invoke-direct {p0, p1, v0}, LX/D4p;->a(LX/04g;LX/0QK;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->r:Ljava/util/Map;

    if-eqz v0, :cond_0

    sget-object v0, LX/04g;->BY_USER_SWIPE:LX/04g;

    if-ne p1, v0, :cond_0

    .line 1962718
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->i(Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;)I

    .line 1962719
    :cond_0
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->c:LX/D4G;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/D4G;->b(Z)V

    .line 1962720
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1962724
    invoke-direct {p0}, LX/D4p;->e()V

    .line 1962725
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->o:LX/0QK;

    iget-object v1, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->getCurrentVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1962721
    invoke-direct {p0}, LX/D4p;->f()V

    .line 1962722
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->p:LX/0QK;

    iget-object v1, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->getCurrentVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()LX/1SX;
    .locals 1

    .prologue
    .line 1962723
    iget-object v0, p0, LX/D4p;->a:Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedFullscreenPlayer;->j:LX/1SX;

    return-object v0
.end method
