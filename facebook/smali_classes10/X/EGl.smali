.class public final LX/EGl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rtc/views/CountdownView;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/views/CountdownView;)V
    .locals 0

    .prologue
    .line 2098394
    iput-object p1, p0, LX/EGl;->a:Lcom/facebook/rtc/views/CountdownView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 2098395
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v1

    sub-float/2addr v0, v1

    .line 2098396
    iget-object v1, p0, LX/EGl;->a:Lcom/facebook/rtc/views/CountdownView;

    iget-object v1, v1, Lcom/facebook/rtc/views/CountdownView;->b:Lcom/facebook/widget/FacebookProgressCircleView;

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, v0

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(D)V

    .line 2098397
    iget-object v1, p0, LX/EGl;->a:Lcom/facebook/rtc/views/CountdownView;

    iget-object v2, p0, LX/EGl;->a:Lcom/facebook/rtc/views/CountdownView;

    iget v2, v2, Lcom/facebook/rtc/views/CountdownView;->f:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    invoke-virtual {v1, v0}, Lcom/facebook/rtc/views/CountdownView;->a(I)V

    .line 2098398
    return-void
.end method
