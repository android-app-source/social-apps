.class public final LX/E3t;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;

.field private final b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field private final c:LX/3U7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final d:LX/E2c;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/3U7;LX/E2c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "LX/E2c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2075550
    iput-object p1, p0, LX/E3t;->a:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPaginatedHScrollUnitComponentPartDefinition;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2075551
    iput-object p2, p0, LX/E3t;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075552
    iput-object p3, p0, LX/E3t;->c:LX/3U7;

    .line 2075553
    iput-object p4, p0, LX/E3t;->d:LX/E2c;

    .line 2075554
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2075555
    iget-object v0, p0, LX/E3t;->d:LX/E2c;

    const/4 v1, 0x0

    .line 2075556
    iput-boolean v1, v0, LX/E2c;->e:Z

    .line 2075557
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2075558
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2075559
    iget-object v0, p0, LX/E3t;->d:LX/E2c;

    const/4 v1, 0x0

    .line 2075560
    iput-boolean v1, v0, LX/E2c;->e:Z

    .line 2075561
    if-eqz p1, :cond_0

    .line 2075562
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2075563
    if-eqz v0, :cond_0

    .line 2075564
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2075565
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2075566
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2075567
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    .line 2075568
    iget-object v1, p0, LX/E3t;->c:LX/3U7;

    iget-object v2, p0, LX/E3t;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v1, v0, v2}, LX/3U7;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2075569
    :cond_0
    return-void
.end method
