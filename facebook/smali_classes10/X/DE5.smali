.class public LX/DE5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/DEA;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/17Y;

.field public final d:Z


# direct methods
.method public constructor <init>(LX/DEA;Lcom/facebook/content/SecureContextHelper;LX/17Y;Ljava/lang/Boolean;)V
    .locals 1
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1976336
    iput-object p1, p0, LX/DE5;->a:LX/DEA;

    .line 1976337
    iput-object p2, p0, LX/DE5;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1976338
    iput-object p3, p0, LX/DE5;->c:LX/17Y;

    .line 1976339
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/DE5;->d:Z

    .line 1976340
    return-void
.end method

.method public static a(LX/0QB;)LX/DE5;
    .locals 7

    .prologue
    .line 1976341
    const-class v1, LX/DE5;

    monitor-enter v1

    .line 1976342
    :try_start_0
    sget-object v0, LX/DE5;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976343
    sput-object v2, LX/DE5;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976344
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976345
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976346
    new-instance p0, LX/DE5;

    invoke-static {v0}, LX/DEA;->a(LX/0QB;)LX/DEA;

    move-result-object v3

    check-cast v3, LX/DEA;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-direct {p0, v3, v4, v5, v6}, LX/DE5;-><init>(LX/DEA;Lcom/facebook/content/SecureContextHelper;LX/17Y;Ljava/lang/Boolean;)V

    .line 1976347
    move-object v0, p0

    .line 1976348
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976349
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DE5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976350
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976351
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
