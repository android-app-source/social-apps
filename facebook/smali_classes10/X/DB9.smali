.class public LX/DB9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/events/model/Event;

.field private b:Lcom/facebook/events/common/EventAnalyticsParams;

.field public c:LX/4BY;

.field public final d:Landroid/content/Context;

.field public final e:Landroid/content/ContentResolver;

.field public final f:LX/Bky;

.field private final g:LX/1Ck;

.field public final h:LX/Bl6;

.field public final i:LX/0TD;

.field private final j:LX/7vP;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/ContentResolver;LX/Bky;LX/1Ck;LX/Bl6;LX/0TD;LX/7vP;)V
    .locals 0
    .param p6    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1972288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1972289
    iput-object p1, p0, LX/DB9;->d:Landroid/content/Context;

    .line 1972290
    iput-object p2, p0, LX/DB9;->e:Landroid/content/ContentResolver;

    .line 1972291
    iput-object p3, p0, LX/DB9;->f:LX/Bky;

    .line 1972292
    iput-object p4, p0, LX/DB9;->g:LX/1Ck;

    .line 1972293
    iput-object p5, p0, LX/DB9;->h:LX/Bl6;

    .line 1972294
    iput-object p6, p0, LX/DB9;->i:LX/0TD;

    .line 1972295
    iput-object p7, p0, LX/DB9;->j:LX/7vP;

    .line 1972296
    return-void
.end method

.method public static b(LX/0QB;)LX/DB9;
    .locals 8

    .prologue
    .line 1972297
    new-instance v0, LX/DB9;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v2

    check-cast v2, Landroid/content/ContentResolver;

    invoke-static {p0}, LX/Bky;->b(LX/0QB;)LX/Bky;

    move-result-object v3

    check-cast v3, LX/Bky;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v5

    check-cast v5, LX/Bl6;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {p0}, LX/7vP;->b(LX/0QB;)LX/7vP;

    move-result-object v7

    check-cast v7, LX/7vP;

    invoke-direct/range {v0 .. v7}, LX/DB9;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;LX/Bky;LX/1Ck;LX/Bl6;LX/0TD;LX/7vP;)V

    .line 1972298
    return-object v0
.end method

.method public static b(LX/DB9;Lcom/facebook/events/common/ActionMechanism;LX/DB8;)V
    .locals 5
    .param p1    # Lcom/facebook/events/common/ActionMechanism;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1972299
    iget-object v0, p0, LX/DB9;->a:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_0

    .line 1972300
    :goto_0
    return-void

    .line 1972301
    :cond_0
    iget-object v0, p0, LX/DB9;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz v0, :cond_1

    if-nez p1, :cond_2

    .line 1972302
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "EventActionContext or ActionMechanism not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1972303
    :cond_2
    const/4 v3, 0x0

    .line 1972304
    new-instance v0, LX/4BY;

    iget-object v1, p0, LX/DB9;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/DB9;->c:LX/4BY;

    .line 1972305
    iget-object v0, p0, LX/DB9;->c:LX/4BY;

    .line 1972306
    iput v3, v0, LX/4BY;->d:I

    .line 1972307
    iget-object v0, p0, LX/DB9;->c:LX/4BY;

    iget-object v1, p0, LX/DB9;->d:Landroid/content/Context;

    const v2, 0x7f082ae9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1972308
    iget-object v0, p0, LX/DB9;->c:LX/4BY;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/4BY;->a(Z)V

    .line 1972309
    iget-object v0, p0, LX/DB9;->c:LX/4BY;

    invoke-virtual {v0, v3}, LX/4BY;->setCancelable(Z)V

    .line 1972310
    iget-object v0, p0, LX/DB9;->c:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->show()V

    .line 1972311
    iget-object v0, p0, LX/DB9;->j:LX/7vP;

    iget-object v1, p0, LX/DB9;->a:Lcom/facebook/events/model/Event;

    .line 1972312
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1972313
    iget-object v2, p0, LX/DB9;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {p1}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/7vP;->a(Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1972314
    new-instance v1, LX/DB7;

    invoke-direct {v1, p0, p2}, LX/DB7;-><init>(LX/DB9;LX/DB8;)V

    .line 1972315
    iget-object v2, p0, LX/DB9;->g:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tasks-deleteEvent:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/DB9;->a:Lcom/facebook/events/model/Event;

    .line 1972316
    iget-object p0, v4, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v4, p0

    .line 1972317
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/common/ActionMechanism;)V
    .locals 4

    .prologue
    .line 1972318
    const/4 v0, 0x0

    .line 1972319
    new-instance v1, LX/31Y;

    iget-object v2, p0, LX/DB9;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v2, 0x7f082f2d

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f082f2e

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f082f2f    # 1.8102E38f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f082f30

    new-instance v3, LX/DB6;

    invoke-direct {v3, p0, p1, v0}, LX/DB6;-><init>(LX/DB9;Lcom/facebook/events/common/ActionMechanism;LX/DB8;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 1972320
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 0

    .prologue
    .line 1972321
    iput-object p1, p0, LX/DB9;->a:Lcom/facebook/events/model/Event;

    .line 1972322
    iput-object p2, p0, LX/DB9;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1972323
    return-void
.end method
