.class public final LX/Dbl;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V
    .locals 0

    .prologue
    .line 2017518
    iput-object p1, p0, LX/Dbl;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 13

    .prologue
    .line 2017519
    iget-object v0, p0, LX/Dbl;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    iget-object v0, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->c:LX/DbM;

    iget-object v1, p0, LX/Dbl;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    iget-wide v2, v1, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Dbl;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    iget-object v2, v2, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget-object v3, p0, LX/Dbl;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    const/4 v4, 0x0

    .line 2017520
    iget-object v5, v3, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result p1

    move v6, v4

    move v5, v4

    :goto_0
    if-ge v6, p1, :cond_0

    iget-object v4, v3, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;

    .line 2017521
    iget-object p2, v4, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->d:Ljava/lang/String;

    move-object v4, p2

    .line 2017522
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2017523
    add-int/lit8 v4, v5, 0x1

    .line 2017524
    :goto_1
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    goto :goto_0

    .line 2017525
    :cond_0
    move v3, v5

    .line 2017526
    iget-object v4, v0, LX/DbM;->a:LX/0Zb;

    const-string v5, "upload_photo_menu_upload_button_click"

    invoke-static {v5, v1}, LX/DbM;->e(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "photo_labels_count"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "photos_total_count"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2017527
    iget-object v0, p0, LX/Dbl;->a:Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;

    .line 2017528
    iget-object v4, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 2017529
    :goto_2
    return-void

    :cond_1
    move v4, v5

    goto :goto_1

    .line 2017530
    :cond_2
    iget-object v5, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->i:LX/8LV;

    .line 2017531
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2017532
    iget-object v4, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v4, 0x0

    move v6, v4

    :goto_3
    if-ge v6, v8, :cond_3

    iget-object v4, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;

    .line 2017533
    iget-object v9, v4, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->a:Lcom/facebook/ipc/media/MediaItem;

    move-object v4, v9

    .line 2017534
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2017535
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_3

    .line 2017536
    :cond_3
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v6, v4

    .line 2017537
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2017538
    iget-object v4, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v4, 0x0

    move v7, v4

    :goto_4
    if-ge v7, v9, :cond_5

    iget-object v4, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;

    .line 2017539
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 2017540
    iget-object v11, v4, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->d:Ljava/lang/String;

    move-object v11, v11

    .line 2017541
    invoke-static {v11}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 2017542
    const-string v11, "caption"

    .line 2017543
    iget-object v12, v4, Lcom/facebook/localcontent/menus/PhotoMenuUploadItemModel;->d:Ljava/lang/String;

    move-object v4, v12

    .line 2017544
    invoke-virtual {v10, v11, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017545
    :cond_4
    invoke-virtual {v8, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2017546
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_4

    .line 2017547
    :cond_5
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v7, v4

    .line 2017548
    iget-wide v8, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->q:J

    iget-object v10, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->r:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v5 .. v11}, LX/8LV;->a(LX/0Px;LX/0Px;JLcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v4

    .line 2017549
    iget-object v5, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->h:LX/1EZ;

    invoke-virtual {v5, v4}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 2017550
    iget-object v4, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->d:LX/CXj;

    new-instance v5, LX/CXy;

    iget-wide v6, v0, Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;->q:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v5, v6}, LX/CXy;-><init>(Ljava/lang/Long;)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2017551
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2017552
    new-instance v4, LX/31Y;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0828e2

    invoke-virtual {v4, v5}, LX/0ju;->a(I)LX/0ju;

    move-result-object v4

    const v5, 0x7f0828e3

    invoke-virtual {v4, v5}, LX/0ju;->b(I)LX/0ju;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v4

    const v5, 0x7f080036

    new-instance v6, LX/Dbn;

    invoke-direct {v6, v0}, LX/Dbn;-><init>(Lcom/facebook/localcontent/menus/PhotoMenuUploadFragment;)V

    invoke-virtual {v4, v5, v6}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    invoke-virtual {v4}, LX/0ju;->a()LX/2EJ;

    move-result-object v4

    invoke-virtual {v4}, LX/2EJ;->show()V

    .line 2017553
    goto/16 :goto_2
.end method
