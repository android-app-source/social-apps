.class public LX/D4i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:LX/D4g;

.field public final b:Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

.field public final c:LX/D4h;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/D4Q;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/player/RichVideoPlayer;",
            ">;"
        }
    .end annotation
.end field

.field public g:Landroid/animation/ValueAnimator;

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1962484
    new-instance v0, LX/D4g;

    invoke-direct {v0, p0}, LX/D4g;-><init>(LX/D4i;)V

    iput-object v0, p0, LX/D4i;->a:LX/D4g;

    .line 1962485
    new-instance v0, Lcom/facebook/video/channelfeed/ChannelFeedFocusDimmingManager$1;

    const v1, 0x3f333333    # 0.7f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, p0, v1, v2}, Lcom/facebook/video/channelfeed/ChannelFeedFocusDimmingManager$1;-><init>(LX/D4i;FF)V

    iput-object v0, p0, LX/D4i;->b:Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;

    .line 1962486
    new-instance v0, LX/D4h;

    invoke-direct {v0, p0}, LX/D4h;-><init>(LX/D4i;)V

    iput-object v0, p0, LX/D4i;->c:LX/D4h;

    .line 1962487
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/D4i;->d:Ljava/util/Set;

    .line 1962488
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, LX/D4i;->g:Landroid/animation/ValueAnimator;

    .line 1962489
    iget-object v0, p0, LX/D4i;->g:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1962490
    return-void
.end method

.method public static a(LX/0QB;)LX/D4i;
    .locals 3

    .prologue
    .line 1962491
    const-class v1, LX/D4i;

    monitor-enter v1

    .line 1962492
    :try_start_0
    sget-object v0, LX/D4i;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1962493
    sput-object v2, LX/D4i;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1962494
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1962495
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1962496
    new-instance v0, LX/D4i;

    invoke-direct {v0}, LX/D4i;-><init>()V

    .line 1962497
    move-object v0, v0

    .line 1962498
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1962499
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D4i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1962500
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1962501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c$redex0(LX/D4i;)V
    .locals 4

    .prologue
    .line 1962502
    iget-object v0, p0, LX/D4i;->f:Ljava/lang/ref/WeakReference;

    .line 1962503
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1962504
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getPlaybackPercentage()F

    move-result v1

    const v2, 0x3f333333    # 0.7f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 1962505
    if-eqz v0, :cond_0

    .line 1962506
    iget-object v0, p0, LX/D4i;->a:LX/D4g;

    const/4 v1, 0x1

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, LX/D4g;->sendEmptyMessageDelayed(IJ)Z

    .line 1962507
    :cond_0
    return-void

    .line 1962508
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1962509
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static e(LX/D4i;)V
    .locals 6

    .prologue
    .line 1962510
    invoke-static {p0}, LX/D4i;->g(LX/D4i;)LX/D4Q;

    move-result-object v0

    .line 1962511
    if-eqz v0, :cond_1

    .line 1962512
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1962513
    iget-object v1, v0, LX/D4Q;->d:LX/D4P;

    sget-object v4, LX/D4P;->IMMERSED:LX/D4P;

    if-ne v1, v4, :cond_4

    iget-object v1, v0, LX/D4Q;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 1962514
    :goto_0
    iget-object v4, v0, LX/D4Q;->d:LX/D4P;

    sget-object v5, LX/D4P;->IMMERSED:LX/D4P;

    if-eq v4, v5, :cond_0

    iget-object v4, v0, LX/D4Q;->d:LX/D4P;

    sget-object v5, LX/D4P;->FOCUS_DIMMED:LX/D4P;

    if-ne v4, v5, :cond_5

    :cond_0
    move v4, v2

    .line 1962515
    :goto_1
    if-nez v1, :cond_6

    if-eqz v4, :cond_6

    :goto_2
    move v1, v2

    .line 1962516
    if-nez v1, :cond_3

    .line 1962517
    :cond_1
    :goto_3
    iget-boolean v0, p0, LX/D4i;->h:Z

    if-nez v0, :cond_2

    .line 1962518
    :goto_4
    return-void

    .line 1962519
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D4i;->h:Z

    .line 1962520
    iget-object v0, p0, LX/D4i;->d:Ljava/util/Set;

    iget-object v1, p0, LX/D4i;->g:Landroid/animation/ValueAnimator;

    const/16 v2, 0x12c

    .line 1962521
    const v3, 0x3da3d70a    # 0.08f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v3, v4, v2}, LX/D4T;->a(Ljava/util/Collection;Landroid/animation/ValueAnimator;FFI)V

    .line 1962522
    goto :goto_4

    .line 1962523
    :cond_3
    const v1, 0x3da3d70a    # 0.08f

    iput v1, v0, LX/D4Q;->f:F

    .line 1962524
    sget-object v1, LX/D4P;->UNDIMMED:LX/D4P;

    invoke-static {v0, v1}, LX/D4Q;->a(LX/D4Q;LX/D4P;)V

    .line 1962525
    invoke-static {v0}, LX/D4Q;->k(LX/D4Q;)V

    goto :goto_3

    :cond_4
    move v1, v3

    .line 1962526
    goto :goto_0

    :cond_5
    move v4, v3

    .line 1962527
    goto :goto_1

    :cond_6
    move v2, v3

    .line 1962528
    goto :goto_2
.end method

.method public static g(LX/D4i;)LX/D4Q;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1962529
    iget-object v0, p0, LX/D4i;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D4i;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D4Q;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1962530
    iget-object v0, p0, LX/D4i;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1962531
    iget-boolean v0, p0, LX/D4i;->h:Z

    if-eqz v0, :cond_0

    .line 1962532
    const v0, 0x3da3d70a    # 0.08f

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1962533
    :cond_0
    return-void
.end method
