.class public final LX/DVv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWx;


# instance fields
.field public final synthetic a:Landroid/support/v4/app/Fragment;

.field public final synthetic b:LX/DVw;


# direct methods
.method public constructor <init>(LX/DVw;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 2005933
    iput-object p1, p0, LX/DVv;->b:LX/DVw;

    iput-object p2, p0, LX/DVv;->a:Landroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2005926
    iget-object v0, p0, LX/DVv;->b:LX/DVw;

    iget-object v1, p0, LX/DVv;->a:Landroid/support/v4/app/Fragment;

    .line 2005927
    iget-object v2, v0, LX/DVw;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 2005928
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 2005929
    const-string v3, "target_fragment"

    sget-object p0, LX/0cQ;->GROUPS_CUSTOM_INVITE_FRAGMENT:LX/0cQ;

    invoke-virtual {p0}, LX/0cQ;->ordinal()I

    move-result p0

    invoke-virtual {v2, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2005930
    const-string v3, "groups_custom_invite_message"

    iget-object p0, v0, LX/DVw;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2005931
    iget-object v3, v0, LX/DVw;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x457f

    invoke-interface {v3, v2, p0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2005932
    return-void
.end method
