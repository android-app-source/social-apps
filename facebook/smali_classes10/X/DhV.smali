.class public final enum LX/DhV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DhV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DhV;

.field public static final enum IMAGE:LX/DhV;

.field public static final enum STICKER:LX/DhV;

.field public static final enum TEXT:LX/DhV;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2030989
    new-instance v0, LX/DhV;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v2}, LX/DhV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DhV;->IMAGE:LX/DhV;

    .line 2030990
    new-instance v0, LX/DhV;

    const-string v1, "STICKER"

    invoke-direct {v0, v1, v3}, LX/DhV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DhV;->STICKER:LX/DhV;

    .line 2030991
    new-instance v0, LX/DhV;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v4}, LX/DhV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DhV;->TEXT:LX/DhV;

    .line 2030992
    const/4 v0, 0x3

    new-array v0, v0, [LX/DhV;

    sget-object v1, LX/DhV;->IMAGE:LX/DhV;

    aput-object v1, v0, v2

    sget-object v1, LX/DhV;->STICKER:LX/DhV;

    aput-object v1, v0, v3

    sget-object v1, LX/DhV;->TEXT:LX/DhV;

    aput-object v1, v0, v4

    sput-object v0, LX/DhV;->$VALUES:[LX/DhV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2030993
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DhV;
    .locals 1

    .prologue
    .line 2030994
    const-class v0, LX/DhV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DhV;

    return-object v0
.end method

.method public static values()[LX/DhV;
    .locals 1

    .prologue
    .line 2030995
    sget-object v0, LX/DhV;->$VALUES:[LX/DhV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DhV;

    return-object v0
.end method
