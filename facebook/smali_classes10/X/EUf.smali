.class public LX/EUf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BwV;
.implements LX/7Lk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lk;",
        ":",
        "LX/BwV;",
        ">",
        "Ljava/lang/Object;",
        "LX/BwV;",
        "LX/7Lk;"
    }
.end annotation


# instance fields
.field private final a:LX/1SX;

.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Pf;LX/0wD;Ljava/lang/String;LX/But;LX/Bux;LX/0xX;)V
    .locals 1
    .param p1    # LX/1Pf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0wD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2126365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2126366
    sget-object v0, LX/1vy;->VH_LIVE_NOTIFICATIONS_MENU:LX/1vy;

    invoke-virtual {p6, v0}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126367
    invoke-virtual {p5, p1, p2, p3}, LX/Bux;->a(LX/1Pf;LX/0wD;Ljava/lang/String;)LX/Buw;

    move-result-object v0

    .line 2126368
    :goto_0
    move-object v0, v0

    .line 2126369
    iput-object v0, p0, LX/EUf;->a:LX/1SX;

    .line 2126370
    return-void

    :cond_0
    invoke-virtual {p4, p1, p2, p3}, LX/But;->a(LX/1Pf;LX/0wD;Ljava/lang/String;)LX/Bus;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7Lk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 2126371
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2126372
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/EUf;->b:Ljava/lang/ref/WeakReference;

    .line 2126373
    return-void
.end method

.method public final kM_()LX/Bwd;
    .locals 1

    .prologue
    .line 2126374
    iget-object v0, p0, LX/EUf;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    .line 2126375
    const/4 v0, 0x0

    .line 2126376
    :goto_0
    move-object v0, v0

    .line 2126377
    if-nez v0, :cond_0

    .line 2126378
    const/4 v0, 0x0

    .line 2126379
    :goto_1
    return-object v0

    :cond_0
    check-cast v0, LX/BwV;

    invoke-interface {v0}, LX/BwV;->kM_()LX/Bwd;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, LX/EUf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Lk;

    goto :goto_0
.end method

.method public final n()LX/1SX;
    .locals 1

    .prologue
    .line 2126380
    iget-object v0, p0, LX/EUf;->a:LX/1SX;

    return-object v0
.end method
