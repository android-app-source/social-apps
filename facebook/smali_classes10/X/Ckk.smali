.class public final enum LX/Ckk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ckk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ckk;

.field public static final enum StyleLowercase:LX/Ckk;

.field public static final enum StyleNone:LX/Ckk;

.field public static final enum StyleUppercase:LX/Ckk;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1931282
    new-instance v0, LX/Ckk;

    const-string v1, "StyleNone"

    invoke-direct {v0, v1, v2}, LX/Ckk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckk;->StyleNone:LX/Ckk;

    .line 1931283
    new-instance v0, LX/Ckk;

    const-string v1, "StyleUppercase"

    invoke-direct {v0, v1, v3}, LX/Ckk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckk;->StyleUppercase:LX/Ckk;

    .line 1931284
    new-instance v0, LX/Ckk;

    const-string v1, "StyleLowercase"

    invoke-direct {v0, v1, v4}, LX/Ckk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ckk;->StyleLowercase:LX/Ckk;

    .line 1931285
    const/4 v0, 0x3

    new-array v0, v0, [LX/Ckk;

    sget-object v1, LX/Ckk;->StyleNone:LX/Ckk;

    aput-object v1, v0, v2

    sget-object v1, LX/Ckk;->StyleUppercase:LX/Ckk;

    aput-object v1, v0, v3

    sget-object v1, LX/Ckk;->StyleLowercase:LX/Ckk;

    aput-object v1, v0, v4

    sput-object v0, LX/Ckk;->$VALUES:[LX/Ckk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1931286
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ckk;
    .locals 1

    .prologue
    .line 1931287
    const-class v0, LX/Ckk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ckk;

    return-object v0
.end method

.method public static values()[LX/Ckk;
    .locals 1

    .prologue
    .line 1931288
    sget-object v0, LX/Ckk;->$VALUES:[LX/Ckk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ckk;

    return-object v0
.end method
