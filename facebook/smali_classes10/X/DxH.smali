.class public final LX/DxH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/DxJ;


# direct methods
.method public constructor <init>(LX/DxJ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2062652
    iput-object p1, p0, LX/DxH;->b:LX/DxJ;

    iput-object p2, p0, LX/DxH;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 15

    .prologue
    .line 2062653
    iget-object v0, p0, LX/DxH;->b:LX/DxJ;

    iget-object v0, v0, LX/DxJ;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DvH;

    iget-object v1, p0, LX/DxH;->b:LX/DxJ;

    iget-object v1, v1, LX/DxJ;->u:LX/Dcc;

    iget-object v2, p0, LX/DxH;->b:LX/DxJ;

    iget-object v2, v2, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget-object v3, p0, LX/DxH;->b:LX/DxJ;

    iget-object v3, v3, LX/Dvb;->a:LX/Dvf;

    const/4 v4, 0x0

    iget-object v5, p0, LX/DxH;->a:Ljava/lang/String;

    iget-object v6, p0, LX/DxH;->b:LX/DxJ;

    .line 2062654
    invoke-virtual {v6}, LX/Dvb;->b()Z

    move-result v7

    move v6, v7

    .line 2062655
    if-eqz v6, :cond_1

    const/16 v6, 0x1e

    :goto_0
    iget-object v7, p0, LX/DxH;->b:LX/DxJ;

    .line 2062656
    invoke-virtual {v7}, LX/Dvb;->i()Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    move-result-object v8

    move-object v7, v8

    .line 2062657
    iget-object v8, p0, LX/DxH;->b:LX/DxJ;

    const/4 v9, 0x1

    .line 2062658
    iget-boolean p0, v8, LX/Dvb;->o:Z

    if-eqz p0, :cond_2

    .line 2062659
    :cond_0
    :goto_1
    move v8, v9

    .line 2062660
    move-object v9, v1

    move-object v10, v4

    move-object v11, v5

    move-object v12, v2

    move v13, v6

    move v14, v8

    invoke-virtual/range {v9 .. v14}, LX/Dcc;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v11

    .line 2062661
    iget-object v9, v0, LX/DvH;->e:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/DvF;

    iget-object v10, v0, LX/DvH;->a:LX/0Ot;

    invoke-interface {v10}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Executor;

    invoke-static {v11, v9, v7, v10}, LX/DvG;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/DvF;Ljava/lang/Object;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v11

    .line 2062662
    iget-object v9, v0, LX/DvH;->d:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/DvF;

    new-instance v12, LX/DvK;

    sget-object v10, LX/DvW;->UPLOADED_MEDIA_SET:LX/DvW;

    invoke-direct {v12, v2, v10, v3}, LX/DvK;-><init>(Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;LX/Dvf;)V

    iget-object v10, v0, LX/DvH;->a:LX/0Ot;

    invoke-interface {v10}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Executor;

    invoke-static {v11, v9, v12, v10}, LX/DvG;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/DvF;Ljava/lang/Object;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 2062663
    move-object v0, v9

    .line 2062664
    return-object v0

    :cond_1
    const/16 v6, 0xc

    goto :goto_0

    .line 2062665
    :cond_2
    iget-boolean p0, v8, LX/DxJ;->x:Z

    if-eqz p0, :cond_0

    iget-boolean p0, v8, LX/Dvb;->o:Z

    if-nez p0, :cond_0

    .line 2062666
    const/4 v9, 0x0

    goto :goto_1
.end method
