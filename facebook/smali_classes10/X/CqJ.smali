.class public final LX/CqJ;
.super LX/1Rm;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/CqK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CqK;Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    .prologue
    .line 1939544
    invoke-direct {p0, p2}, LX/1Rm;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 1939545
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/CqJ;->a:Ljava/lang/ref/WeakReference;

    .line 1939546
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 1

    .prologue
    .line 1939547
    iget-object v0, p0, LX/CqJ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1939548
    iget-object v0, p0, LX/CqJ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqK;

    invoke-static {v0}, LX/CqK;->b$redex0(LX/CqK;)V

    .line 1939549
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1939550
    iget-object v0, p0, LX/CqJ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1939551
    iget-object v0, p0, LX/CqJ;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqK;

    const/4 p0, 0x0

    .line 1939552
    iget-object v1, v0, LX/CqK;->f:LX/CqE;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/CqK;->f:LX/CqE;

    invoke-interface {v1}, LX/CqE;->i()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, v0, LX/CqK;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_1
    const/4 v1, 0x1

    .line 1939553
    :goto_0
    if-nez v1, :cond_2

    .line 1939554
    iput-object p0, v0, LX/CqK;->f:LX/CqE;

    .line 1939555
    iput-object p0, v0, LX/CqK;->g:LX/CqJ;

    .line 1939556
    :cond_2
    move v0, v1

    .line 1939557
    :goto_1
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1939558
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method
