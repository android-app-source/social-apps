.class public LX/ENa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static final b:LX/1Ua;

.field public static final c:LX/1X6;

.field public static final d:LX/1X6;

.field private static j:LX/0Xm;


# instance fields
.field public final e:LX/1V0;

.field public final f:LX/0Uh;

.field public final g:LX/ENV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/ENV",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final h:LX/ENd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/ENd",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final i:LX/ENh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/ENh",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2112967
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 2112968
    iput v1, v0, LX/1UY;->b:F

    .line 2112969
    move-object v0, v0

    .line 2112970
    const/high16 v1, -0x3fc00000    # -3.0f

    .line 2112971
    iput v1, v0, LX/1UY;->c:F

    .line 2112972
    move-object v0, v0

    .line 2112973
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, LX/ENa;->a:LX/1Ua;

    .line 2112974
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3ec00000    # -12.0f

    .line 2112975
    iput v1, v0, LX/1UY;->c:F

    .line 2112976
    move-object v0, v0

    .line 2112977
    const/high16 v1, -0x3ee00000    # -10.0f

    .line 2112978
    iput v1, v0, LX/1UY;->d:F

    .line 2112979
    move-object v0, v0

    .line 2112980
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, LX/ENa;->b:LX/1Ua;

    .line 2112981
    new-instance v0, LX/1X6;

    sget-object v1, LX/ENa;->a:LX/1Ua;

    sget-object v2, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v3, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, LX/ENa;->c:LX/1X6;

    .line 2112982
    new-instance v0, LX/1X6;

    sget-object v1, LX/ENa;->b:LX/1Ua;

    sget-object v2, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v0, v3, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, LX/ENa;->d:LX/1X6;

    return-void
.end method

.method public constructor <init>(LX/1V0;LX/0Uh;LX/ENV;LX/ENd;LX/ENh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2112984
    iput-object p4, p0, LX/ENa;->h:LX/ENd;

    .line 2112985
    iput-object p5, p0, LX/ENa;->i:LX/ENh;

    .line 2112986
    iput-object p3, p0, LX/ENa;->g:LX/ENV;

    .line 2112987
    iput-object p1, p0, LX/ENa;->e:LX/1V0;

    .line 2112988
    iput-object p2, p0, LX/ENa;->f:LX/0Uh;

    .line 2112989
    return-void
.end method

.method public static a(LX/0QB;)LX/ENa;
    .locals 9

    .prologue
    .line 2112990
    const-class v1, LX/ENa;

    monitor-enter v1

    .line 2112991
    :try_start_0
    sget-object v0, LX/ENa;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112992
    sput-object v2, LX/ENa;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112993
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112994
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112995
    new-instance v3, LX/ENa;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/ENV;->a(LX/0QB;)LX/ENV;

    move-result-object v6

    check-cast v6, LX/ENV;

    invoke-static {v0}, LX/ENd;->a(LX/0QB;)LX/ENd;

    move-result-object v7

    check-cast v7, LX/ENd;

    invoke-static {v0}, LX/ENh;->a(LX/0QB;)LX/ENh;

    move-result-object v8

    check-cast v8, LX/ENh;

    invoke-direct/range {v3 .. v8}, LX/ENa;-><init>(LX/1V0;LX/0Uh;LX/ENV;LX/ENd;LX/ENh;)V

    .line 2112996
    move-object v0, v3

    .line 2112997
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112998
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ENa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112999
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113000
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
