.class public final LX/EPq;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;LX/0Px;)V
    .locals 0

    .prologue
    .line 2117825
    iput-object p1, p0, LX/EPq;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;

    iput-object p2, p0, LX/EPq;->a:LX/0Px;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2117819
    iget-object v0, p0, LX/EPq;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, LX/EPq;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CzL;

    .line 2117820
    invoke-virtual {v0}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v1, v4, :cond_0

    iget-object v1, p0, LX/EPq;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    :goto_1
    invoke-virtual {p1, v1, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2117821
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2117822
    :cond_0
    iget-object v1, p0, LX/EPq;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    goto :goto_1

    .line 2117823
    :cond_1
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 2117824
    return-void
.end method
