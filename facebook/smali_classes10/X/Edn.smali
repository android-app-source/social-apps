.class public LX/Edn;
.super LX/EdV;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    .line 2151440
    invoke-direct {p0}, LX/EdV;-><init>()V

    .line 2151441
    const/16 v0, 0x80

    :try_start_0
    invoke-virtual {p0, v0}, LX/EdM;->a(I)V

    .line 2151442
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, LX/EdM;->b(I)V

    .line 2151443
    const-string v0, "application/vnd.wap.multipart.related"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 2151444
    iget-object v1, p0, LX/EdM;->a:LX/Ede;

    const/16 v2, 0x84

    invoke-virtual {v1, v0, v2}, LX/Ede;->a([BI)V

    .line 2151445
    new-instance v0, LX/EdS;

    const-string v1, "insert-address-token"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdS;-><init>([B)V

    invoke-virtual {p0, v0}, LX/EdM;->a(LX/EdS;)V

    .line 2151446
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "T"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2151447
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    move-object v0, v3

    .line 2151448
    iget-object v1, p0, LX/EdM;->a:LX/Ede;

    const/16 v2, 0x98

    invoke-virtual {v1, v0, v2}, LX/Ede;->a([BI)V
    :try_end_0
    .catch LX/EdU; {:try_start_0 .. :try_end_0} :catch_0

    .line 2151449
    return-void

    .line 2151450
    :catch_0
    move-exception v0

    .line 2151451
    const-string v1, "SendReq"

    const-string v2, "Unexpected InvalidHeaderValueException."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2151452
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>(LX/Ede;LX/EdY;)V
    .locals 0

    .prologue
    .line 2151453
    invoke-direct {p0, p1, p2}, LX/EdV;-><init>(LX/Ede;LX/EdY;)V

    .line 2151454
    return-void
.end method


# virtual methods
.method public final e()J
    .locals 2

    .prologue
    .line 2151455
    iget-object v0, p0, LX/EdM;->a:LX/Ede;

    const/16 v1, 0x8e

    invoke-virtual {v0, v1}, LX/Ede;->e(I)J

    move-result-wide v0

    return-wide v0
.end method
