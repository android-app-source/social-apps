.class public final LX/D73;
.super Landroid/os/CountDownTimer;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;J)V
    .locals 2

    .prologue
    .line 1966725
    iput-object p1, p0, LX/D73;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    .line 1966726
    const-wide/16 v0, 0x1f4

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 1966727
    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 1

    .prologue
    .line 1966728
    iget-object v0, p0, LX/D73;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    .line 1966729
    iget-object p0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->b(Landroid/view/View;)V

    .line 1966730
    iget-object p0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->q:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->b(Landroid/view/View;)V

    .line 1966731
    iget-object p0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p0}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->b(Landroid/view/View;)V

    .line 1966732
    return-void
.end method

.method public final onTick(J)V
    .locals 3

    .prologue
    .line 1966733
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 1966734
    iget-object v1, p0, LX/D73;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/D73;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1966735
    iget-object v1, p0, LX/D73;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1966736
    :cond_0
    return-void
.end method
