.class public final LX/ENz;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EO0;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:LX/ENv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2113792
    invoke-static {}, LX/EO0;->q()LX/EO0;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2113793
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2113791
    const-string v0, "SearchResultsOpinionSearchQueryNameButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2113794
    if-ne p0, p1, :cond_1

    .line 2113795
    :cond_0
    :goto_0
    return v0

    .line 2113796
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2113797
    goto :goto_0

    .line 2113798
    :cond_3
    check-cast p1, LX/ENz;

    .line 2113799
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2113800
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2113801
    if-eq v2, v3, :cond_0

    .line 2113802
    iget-object v2, p0, LX/ENz;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ENz;->a:Ljava/lang/String;

    iget-object v3, p1, LX/ENz;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2113803
    goto :goto_0

    .line 2113804
    :cond_5
    iget-object v2, p1, LX/ENz;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2113805
    :cond_6
    iget-boolean v2, p0, LX/ENz;->b:Z

    iget-boolean v3, p1, LX/ENz;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2113806
    goto :goto_0

    .line 2113807
    :cond_7
    iget-object v2, p0, LX/ENz;->c:LX/ENv;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ENz;->c:LX/ENv;

    iget-object v3, p1, LX/ENz;->c:LX/ENv;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2113808
    goto :goto_0

    .line 2113809
    :cond_8
    iget-object v2, p1, LX/ENz;->c:LX/ENv;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
