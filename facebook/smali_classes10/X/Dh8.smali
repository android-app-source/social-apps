.class public LX/Dh8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

.field public final b:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;Ljava/io/File;Ljava/io/File;)V
    .locals 1
    .param p2    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2030656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2030657
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    iput-object v0, p0, LX/Dh8;->a:Lcom/facebook/messaging/montage/composer/styletransfer/StyleTransferKey;

    .line 2030658
    iput-object p2, p0, LX/Dh8;->b:Ljava/io/File;

    .line 2030659
    iput-object p3, p0, LX/Dh8;->c:Ljava/io/File;

    .line 2030660
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2030655
    iget-object v0, p0, LX/Dh8;->b:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dh8;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2030654
    iget-object v0, p0, LX/Dh8;->c:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dh8;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
