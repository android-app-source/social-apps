.class public LX/D1g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/4BY;

.field public final b:LX/1Ck;

.field public final c:LX/0tX;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1957315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1957316
    iput-object p1, p0, LX/D1g;->b:LX/1Ck;

    .line 1957317
    iput-object p2, p0, LX/D1g;->c:LX/0tX;

    .line 1957318
    return-void
.end method

.method public static a(LX/0QB;)LX/D1g;
    .locals 5

    .prologue
    .line 1957319
    const-class v1, LX/D1g;

    monitor-enter v1

    .line 1957320
    :try_start_0
    sget-object v0, LX/D1g;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1957321
    sput-object v2, LX/D1g;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1957322
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1957323
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1957324
    new-instance p0, LX/D1g;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {p0, v3, v4}, LX/D1g;-><init>(LX/1Ck;LX/0tX;)V

    .line 1957325
    move-object v0, p0

    .line 1957326
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1957327
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D1g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1957328
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1957329
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/D1g;)V
    .locals 1

    .prologue
    .line 1957310
    iget-object v0, p0, LX/D1g;->a:LX/4BY;

    if-eqz v0, :cond_1

    .line 1957311
    iget-object v0, p0, LX/D1g;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1957312
    iget-object v0, p0, LX/D1g;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1957313
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/D1g;->a:LX/4BY;

    .line 1957314
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1957307
    invoke-static {p0}, LX/D1g;->b(LX/D1g;)V

    .line 1957308
    iget-object v0, p0, LX/D1g;->b:LX/1Ck;

    sget-object v1, LX/D1f;->FETCH_LOCATIONS_TASK:LX/D1f;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1957309
    return-void
.end method
