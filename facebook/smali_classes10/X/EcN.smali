.class public final LX/EcN;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EcM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EcN;",
        ">;",
        "LX/EcM;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:LX/EWc;

.field private c:LX/EWc;

.field public d:LX/EcR;

.field public e:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/EcR;",
            "LX/EcQ;",
            "LX/EcP;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EcV;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EcV;",
            "LX/EcU;",
            "LX/EcT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2146497
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2146498
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcN;->b:LX/EWc;

    .line 2146499
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcN;->c:LX/EWc;

    .line 2146500
    sget-object v0, LX/EcR;->c:LX/EcR;

    move-object v0, v0

    .line 2146501
    iput-object v0, p0, LX/EcN;->d:LX/EcR;

    .line 2146502
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EcN;->f:Ljava/util/List;

    .line 2146503
    invoke-direct {p0}, LX/EcN;->x()V

    .line 2146504
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2146485
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2146486
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcN;->b:LX/EWc;

    .line 2146487
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcN;->c:LX/EWc;

    .line 2146488
    sget-object v0, LX/EcR;->c:LX/EcR;

    move-object v0, v0

    .line 2146489
    iput-object v0, p0, LX/EcN;->d:LX/EcR;

    .line 2146490
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EcN;->f:Ljava/util/List;

    .line 2146491
    invoke-direct {p0}, LX/EcN;->x()V

    .line 2146492
    return-void
.end method

.method public static C(LX/EcN;)V
    .locals 2

    .prologue
    .line 2146493
    iget v0, p0, LX/EcN;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 2146494
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EcN;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EcN;->f:Ljava/util/List;

    .line 2146495
    iget v0, p0, LX/EcN;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EcN;->a:I

    .line 2146496
    :cond_0
    return-void
.end method

.method private D()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EcV;",
            "LX/EcU;",
            "LX/EcT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2146505
    iget-object v0, p0, LX/EcN;->g:LX/EZ2;

    if-nez v0, :cond_0

    .line 2146506
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EcN;->f:Ljava/util/List;

    iget v0, p0, LX/EcN;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2146507
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2146508
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EcN;->g:LX/EZ2;

    .line 2146509
    const/4 v0, 0x0

    iput-object v0, p0, LX/EcN;->f:Ljava/util/List;

    .line 2146510
    :cond_0
    iget-object v0, p0, LX/EcN;->g:LX/EZ2;

    return-object v0

    .line 2146511
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/EcN;
    .locals 1

    .prologue
    .line 2146512
    instance-of v0, p1, LX/EcW;

    if-eqz v0, :cond_0

    .line 2146513
    check-cast p1, LX/EcW;

    invoke-virtual {p0, p1}, LX/EcN;->a(LX/EcW;)LX/EcN;

    move-result-object p0

    .line 2146514
    :goto_0
    return-object p0

    .line 2146515
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EcN;
    .locals 4

    .prologue
    .line 2146516
    const/4 v2, 0x0

    .line 2146517
    :try_start_0
    sget-object v0, LX/EcW;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EcW;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2146518
    if-eqz v0, :cond_0

    .line 2146519
    invoke-virtual {p0, v0}, LX/EcN;->a(LX/EcW;)LX/EcN;

    .line 2146520
    :cond_0
    return-object p0

    .line 2146521
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2146522
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2146523
    check-cast v0, LX/EcW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2146524
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2146525
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2146526
    invoke-virtual {p0, v1}, LX/EcN;->a(LX/EcW;)LX/EcN;

    :cond_1
    throw v0

    .line 2146527
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private x()V
    .locals 4

    .prologue
    .line 2146528
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_1

    .line 2146529
    iget-object v0, p0, LX/EcN;->e:LX/EZ7;

    if-nez v0, :cond_0

    .line 2146530
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/EcN;->d:LX/EcR;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2146531
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2146532
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/EcN;->e:LX/EZ7;

    .line 2146533
    const/4 v0, 0x0

    iput-object v0, p0, LX/EcN;->d:LX/EcR;

    .line 2146534
    :cond_0
    invoke-direct {p0}, LX/EcN;->D()LX/EZ2;

    .line 2146535
    :cond_1
    return-void
.end method

.method public static y()LX/EcN;
    .locals 1

    .prologue
    .line 2146602
    new-instance v0, LX/EcN;

    invoke-direct {v0}, LX/EcN;-><init>()V

    return-object v0
.end method

.method private z()LX/EcN;
    .locals 2

    .prologue
    .line 2146536
    invoke-static {}, LX/EcN;->y()LX/EcN;

    move-result-object v0

    invoke-virtual {p0}, LX/EcN;->m()LX/EcW;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcN;->a(LX/EcW;)LX/EcN;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2146537
    invoke-direct {p0, p1}, LX/EcN;->d(LX/EWY;)LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2146538
    invoke-direct {p0, p1, p2}, LX/EcN;->d(LX/EWd;LX/EYZ;)LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EWc;)LX/EcN;
    .locals 1

    .prologue
    .line 2146539
    if-nez p1, :cond_0

    .line 2146540
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146541
    :cond_0
    iget v0, p0, LX/EcN;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EcN;->a:I

    .line 2146542
    iput-object p1, p0, LX/EcN;->b:LX/EWc;

    .line 2146543
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146544
    return-object p0
.end method

.method public final a(LX/EcR;)LX/EcN;
    .locals 1

    .prologue
    .line 2146545
    iget-object v0, p0, LX/EcN;->e:LX/EZ7;

    if-nez v0, :cond_1

    .line 2146546
    if-nez p1, :cond_0

    .line 2146547
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146548
    :cond_0
    iput-object p1, p0, LX/EcN;->d:LX/EcR;

    .line 2146549
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146550
    :goto_0
    iget v0, p0, LX/EcN;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EcN;->a:I

    .line 2146551
    return-object p0

    .line 2146552
    :cond_1
    iget-object v0, p0, LX/EcN;->e:LX/EZ7;

    invoke-virtual {v0, p1}, LX/EZ7;->a(LX/EWp;)LX/EZ7;

    goto :goto_0
.end method

.method public final a(LX/EcV;)LX/EcN;
    .locals 1

    .prologue
    .line 2146553
    iget-object v0, p0, LX/EcN;->g:LX/EZ2;

    if-nez v0, :cond_1

    .line 2146554
    if-nez p1, :cond_0

    .line 2146555
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146556
    :cond_0
    invoke-static {p0}, LX/EcN;->C(LX/EcN;)V

    .line 2146557
    iget-object v0, p0, LX/EcN;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2146558
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146559
    :goto_0
    return-object p0

    .line 2146560
    :cond_1
    iget-object v0, p0, LX/EcN;->g:LX/EZ2;

    invoke-virtual {v0, p1}, LX/EZ2;->a(LX/EWp;)LX/EZ2;

    goto :goto_0
.end method

.method public final a(LX/EcW;)LX/EcN;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2146561
    sget-object v1, LX/EcW;->c:LX/EcW;

    move-object v1, v1

    .line 2146562
    if-ne p1, v1, :cond_0

    .line 2146563
    :goto_0
    return-object p0

    .line 2146564
    :cond_0
    const/4 v1, 0x1

    .line 2146565
    iget v2, p1, LX/EcW;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_9

    :goto_1
    move v1, v1

    .line 2146566
    if-eqz v1, :cond_1

    .line 2146567
    iget-object v1, p1, LX/EcW;->senderRatchetKey_:LX/EWc;

    move-object v1, v1

    .line 2146568
    invoke-virtual {p0, v1}, LX/EcN;->a(LX/EWc;)LX/EcN;

    .line 2146569
    :cond_1
    iget v1, p1, LX/EcW;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_a

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2146570
    if-eqz v1, :cond_2

    .line 2146571
    iget-object v1, p1, LX/EcW;->senderRatchetKeyPrivate_:LX/EWc;

    move-object v1, v1

    .line 2146572
    invoke-virtual {p0, v1}, LX/EcN;->b(LX/EWc;)LX/EcN;

    .line 2146573
    :cond_2
    iget v1, p1, LX/EcW;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_b

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 2146574
    if-eqz v1, :cond_3

    .line 2146575
    iget-object v1, p1, LX/EcW;->chainKey_:LX/EcR;

    move-object v1, v1

    .line 2146576
    iget-object v2, p0, LX/EcN;->e:LX/EZ7;

    if-nez v2, :cond_d

    .line 2146577
    iget v2, p0, LX/EcN;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    iget-object v2, p0, LX/EcN;->d:LX/EcR;

    .line 2146578
    sget-object v3, LX/EcR;->c:LX/EcR;

    move-object v3, v3

    .line 2146579
    if-eq v2, v3, :cond_c

    .line 2146580
    iget-object v2, p0, LX/EcN;->d:LX/EcR;

    invoke-static {v2}, LX/EcR;->a(LX/EcR;)LX/EcQ;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/EcQ;->a(LX/EcR;)LX/EcQ;

    move-result-object v2

    invoke-virtual {v2}, LX/EcQ;->m()LX/EcR;

    move-result-object v2

    iput-object v2, p0, LX/EcN;->d:LX/EcR;

    .line 2146581
    :goto_4
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146582
    :goto_5
    iget v2, p0, LX/EcN;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, LX/EcN;->a:I

    .line 2146583
    :cond_3
    iget-object v1, p0, LX/EcN;->g:LX/EZ2;

    if-nez v1, :cond_6

    .line 2146584
    iget-object v0, p1, LX/EcW;->messageKeys_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2146585
    iget-object v0, p0, LX/EcN;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2146586
    iget-object v0, p1, LX/EcW;->messageKeys_:Ljava/util/List;

    iput-object v0, p0, LX/EcN;->f:Ljava/util/List;

    .line 2146587
    iget v0, p0, LX/EcN;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, LX/EcN;->a:I

    .line 2146588
    :goto_6
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146589
    :cond_4
    :goto_7
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto/16 :goto_0

    .line 2146590
    :cond_5
    invoke-static {p0}, LX/EcN;->C(LX/EcN;)V

    .line 2146591
    iget-object v0, p0, LX/EcN;->f:Ljava/util/List;

    iget-object v1, p1, LX/EcW;->messageKeys_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    .line 2146592
    :cond_6
    iget-object v1, p1, LX/EcW;->messageKeys_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2146593
    iget-object v1, p0, LX/EcN;->g:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2146594
    iget-object v1, p0, LX/EcN;->g:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2146595
    iput-object v0, p0, LX/EcN;->g:LX/EZ2;

    .line 2146596
    iget-object v1, p1, LX/EcW;->messageKeys_:Ljava/util/List;

    iput-object v1, p0, LX/EcN;->f:Ljava/util/List;

    .line 2146597
    iget v1, p0, LX/EcN;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, LX/EcN;->a:I

    .line 2146598
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_7

    invoke-direct {p0}, LX/EcN;->D()LX/EZ2;

    move-result-object v0

    :cond_7
    iput-object v0, p0, LX/EcN;->g:LX/EZ2;

    goto :goto_7

    .line 2146599
    :cond_8
    iget-object v0, p0, LX/EcN;->g:LX/EZ2;

    iget-object v1, p1, LX/EcW;->messageKeys_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_7

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 2146600
    :cond_c
    iput-object v1, p0, LX/EcN;->d:LX/EcR;

    goto :goto_4

    .line 2146601
    :cond_d
    iget-object v2, p0, LX/EcN;->e:LX/EZ7;

    invoke-virtual {v2, v1}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto :goto_5
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2146483
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2146484
    invoke-direct {p0, p1, p2}, LX/EcN;->d(LX/EWd;LX/EYZ;)LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2146430
    invoke-direct {p0}, LX/EcN;->z()LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/EWc;)LX/EcN;
    .locals 1

    .prologue
    .line 2146435
    if-nez p1, :cond_0

    .line 2146436
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146437
    :cond_0
    iget v0, p0, LX/EcN;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EcN;->a:I

    .line 2146438
    iput-object p1, p0, LX/EcN;->c:LX/EWc;

    .line 2146439
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146440
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2146441
    invoke-direct {p0, p1, p2}, LX/EcN;->d(LX/EWd;LX/EYZ;)LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2146442
    invoke-direct {p0}, LX/EcN;->z()LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2146443
    invoke-direct {p0, p1}, LX/EcN;->d(LX/EWY;)LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2146444
    invoke-direct {p0}, LX/EcN;->z()LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2146445
    sget-object v0, LX/Eck;->d:LX/EYn;

    const-class v1, LX/EcW;

    const-class v2, LX/EcN;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2146446
    sget-object v0, LX/Eck;->c:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2146447
    invoke-direct {p0}, LX/EcN;->z()LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2146448
    invoke-virtual {p0}, LX/EcN;->m()LX/EcW;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2146449
    invoke-virtual {p0}, LX/EcN;->l()LX/EcW;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2146450
    invoke-virtual {p0}, LX/EcN;->m()LX/EcW;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2146451
    invoke-virtual {p0}, LX/EcN;->l()LX/EcW;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EcW;
    .locals 2

    .prologue
    .line 2146431
    invoke-virtual {p0}, LX/EcN;->m()LX/EcW;

    move-result-object v0

    .line 2146432
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2146433
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2146434
    :cond_0
    return-object v0
.end method

.method public final m()LX/EcW;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2146452
    new-instance v2, LX/EcW;

    invoke-direct {v2, p0}, LX/EcW;-><init>(LX/EWj;)V

    .line 2146453
    iget v3, p0, LX/EcN;->a:I

    .line 2146454
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 2146455
    :goto_0
    iget-object v1, p0, LX/EcN;->b:LX/EWc;

    .line 2146456
    iput-object v1, v2, LX/EcW;->senderRatchetKey_:LX/EWc;

    .line 2146457
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2146458
    or-int/lit8 v0, v0, 0x2

    .line 2146459
    :cond_0
    iget-object v1, p0, LX/EcN;->c:LX/EWc;

    .line 2146460
    iput-object v1, v2, LX/EcW;->senderRatchetKeyPrivate_:LX/EWc;

    .line 2146461
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_4

    .line 2146462
    or-int/lit8 v0, v0, 0x4

    move v1, v0

    .line 2146463
    :goto_1
    iget-object v0, p0, LX/EcN;->e:LX/EZ7;

    if-nez v0, :cond_2

    .line 2146464
    iget-object v0, p0, LX/EcN;->d:LX/EcR;

    .line 2146465
    iput-object v0, v2, LX/EcW;->chainKey_:LX/EcR;

    .line 2146466
    :goto_2
    iget-object v0, p0, LX/EcN;->g:LX/EZ2;

    if-nez v0, :cond_3

    .line 2146467
    iget v0, p0, LX/EcN;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    .line 2146468
    iget-object v0, p0, LX/EcN;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EcN;->f:Ljava/util/List;

    .line 2146469
    iget v0, p0, LX/EcN;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, LX/EcN;->a:I

    .line 2146470
    :cond_1
    iget-object v0, p0, LX/EcN;->f:Ljava/util/List;

    .line 2146471
    iput-object v0, v2, LX/EcW;->messageKeys_:Ljava/util/List;

    .line 2146472
    :goto_3
    iput v1, v2, LX/EcW;->bitField0_:I

    .line 2146473
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2146474
    return-object v2

    .line 2146475
    :cond_2
    iget-object v0, p0, LX/EcN;->e:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EcR;

    .line 2146476
    iput-object v0, v2, LX/EcW;->chainKey_:LX/EcR;

    .line 2146477
    goto :goto_2

    .line 2146478
    :cond_3
    iget-object v0, p0, LX/EcN;->g:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v0

    .line 2146479
    iput-object v0, v2, LX/EcW;->messageKeys_:Ljava/util/List;

    .line 2146480
    goto :goto_3

    :cond_4
    move v1, v0

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2146481
    sget-object v0, LX/EcW;->c:LX/EcW;

    move-object v0, v0

    .line 2146482
    return-object v0
.end method
