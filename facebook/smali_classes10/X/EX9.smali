.class public final LX/EX9;
.super LX/EWy;
.source ""

# interfaces
.implements LX/EX8;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWy",
        "<",
        "LX/EXA;",
        "LX/EX9;",
        ">;",
        "LX/EX8;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2132595
    invoke-direct {p0}, LX/EWy;-><init>()V

    .line 2132596
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EX9;->b:Ljava/util/List;

    .line 2132597
    invoke-direct {p0}, LX/EX9;->w()V

    .line 2132598
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2132599
    invoke-direct {p0, p1}, LX/EWy;-><init>(LX/EYd;)V

    .line 2132600
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EX9;->b:Ljava/util/List;

    .line 2132601
    invoke-direct {p0}, LX/EX9;->w()V

    .line 2132602
    return-void
.end method

.method private A()LX/EXA;
    .locals 2

    .prologue
    .line 2132603
    invoke-virtual {p0}, LX/EX9;->l()LX/EXA;

    move-result-object v0

    .line 2132604
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2132605
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2132606
    :cond_0
    return-object v0
.end method

.method private D()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2132607
    iget-object v1, p0, LX/EX9;->c:LX/EZ2;

    if-nez v1, :cond_0

    .line 2132608
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EX9;->b:Ljava/util/List;

    iget v3, p0, LX/EX9;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2132609
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2132610
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EX9;->c:LX/EZ2;

    .line 2132611
    const/4 v0, 0x0

    iput-object v0, p0, LX/EX9;->b:Ljava/util/List;

    .line 2132612
    :cond_0
    iget-object v0, p0, LX/EX9;->c:LX/EZ2;

    return-object v0

    .line 2132613
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/EX9;
    .locals 1

    .prologue
    .line 2132614
    instance-of v0, p1, LX/EXA;

    if-eqz v0, :cond_0

    .line 2132615
    check-cast p1, LX/EXA;

    invoke-virtual {p0, p1}, LX/EX9;->a(LX/EXA;)LX/EX9;

    move-result-object p0

    .line 2132616
    :goto_0
    return-object p0

    .line 2132617
    :cond_0
    invoke-super {p0, p1}, LX/EWy;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EX9;
    .locals 4

    .prologue
    .line 2132618
    const/4 v2, 0x0

    .line 2132619
    :try_start_0
    sget-object v0, LX/EXA;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXA;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2132620
    if-eqz v0, :cond_0

    .line 2132621
    invoke-virtual {p0, v0}, LX/EX9;->a(LX/EXA;)LX/EX9;

    .line 2132622
    :cond_0
    return-object p0

    .line 2132623
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2132624
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2132625
    check-cast v0, LX/EXA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2132626
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2132627
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2132628
    invoke-virtual {p0, v1}, LX/EX9;->a(LX/EXA;)LX/EX9;

    :cond_1
    throw v0

    .line 2132629
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 2132630
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2132631
    invoke-direct {p0}, LX/EX9;->D()LX/EZ2;

    .line 2132632
    :cond_0
    return-void
.end method

.method public static x()LX/EX9;
    .locals 1

    .prologue
    .line 2132633
    new-instance v0, LX/EX9;

    invoke-direct {v0}, LX/EX9;-><init>()V

    return-object v0
.end method

.method private y()LX/EX9;
    .locals 2

    .prologue
    .line 2132634
    invoke-static {}, LX/EX9;->x()LX/EX9;

    move-result-object v0

    invoke-virtual {p0}, LX/EX9;->l()LX/EXA;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EX9;->a(LX/EXA;)LX/EX9;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2132635
    invoke-direct {p0, p1}, LX/EX9;->d(LX/EWY;)LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2132636
    invoke-direct {p0, p1, p2}, LX/EX9;->d(LX/EWd;LX/EYZ;)LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EXA;)LX/EX9;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2132637
    sget-object v1, LX/EXA;->c:LX/EXA;

    move-object v1, v1

    .line 2132638
    if-ne p1, v1, :cond_0

    .line 2132639
    :goto_0
    return-object p0

    .line 2132640
    :cond_0
    iget-object v1, p0, LX/EX9;->c:LX/EZ2;

    if-nez v1, :cond_4

    .line 2132641
    iget-object v0, p1, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2132642
    iget-object v0, p0, LX/EX9;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2132643
    iget-object v0, p1, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    iput-object v0, p0, LX/EX9;->b:Ljava/util/List;

    .line 2132644
    iget v0, p0, LX/EX9;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, LX/EX9;->a:I

    .line 2132645
    :goto_1
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2132646
    :cond_1
    :goto_2
    invoke-virtual {p0, p1}, LX/EWy;->a(LX/EX1;)V

    .line 2132647
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    .line 2132648
    :cond_2
    iget v0, p0, LX/EX9;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    .line 2132649
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EX9;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EX9;->b:Ljava/util/List;

    .line 2132650
    iget v0, p0, LX/EX9;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EX9;->a:I

    .line 2132651
    :cond_3
    iget-object v0, p0, LX/EX9;->b:Ljava/util/List;

    iget-object v1, p1, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2132652
    :cond_4
    iget-object v1, p1, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2132653
    iget-object v1, p0, LX/EX9;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2132654
    iget-object v1, p0, LX/EX9;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2132655
    iput-object v0, p0, LX/EX9;->c:LX/EZ2;

    .line 2132656
    iget-object v1, p1, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    iput-object v1, p0, LX/EX9;->b:Ljava/util/List;

    .line 2132657
    iget v1, p0, LX/EX9;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/EX9;->a:I

    .line 2132658
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_5

    invoke-direct {p0}, LX/EX9;->D()LX/EZ2;

    move-result-object v0

    :cond_5
    iput-object v0, p0, LX/EX9;->c:LX/EZ2;

    goto :goto_2

    .line 2132659
    :cond_6
    iget-object v0, p0, LX/EX9;->c:LX/EZ2;

    iget-object v1, p1, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_2
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2132660
    move v0, v1

    .line 2132661
    :goto_0
    iget-object v2, p0, LX/EX9;->c:LX/EZ2;

    if-nez v2, :cond_3

    .line 2132662
    iget-object v2, p0, LX/EX9;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2132663
    :goto_1
    move v2, v2

    .line 2132664
    if-ge v0, v2, :cond_2

    .line 2132665
    iget-object v2, p0, LX/EX9;->c:LX/EZ2;

    if-nez v2, :cond_4

    .line 2132666
    iget-object v2, p0, LX/EX9;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EYB;

    .line 2132667
    :goto_2
    move-object v2, v2

    .line 2132668
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2132669
    :cond_0
    :goto_3
    return v1

    .line 2132670
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2132671
    :cond_2
    invoke-virtual {p0}, LX/EWy;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2132672
    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    iget-object v2, p0, LX/EX9;->c:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_1

    :cond_4
    iget-object v2, p0, LX/EX9;->c:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EYB;

    goto :goto_2
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2132593
    invoke-direct {p0, p1, p2}, LX/EX9;->d(LX/EWd;LX/EYZ;)LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2132594
    invoke-direct {p0}, LX/EX9;->y()LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2132567
    invoke-direct {p0, p1, p2}, LX/EX9;->d(LX/EWd;LX/EYZ;)LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2132568
    invoke-direct {p0}, LX/EX9;->y()LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2132569
    invoke-direct {p0, p1}, LX/EX9;->d(LX/EWY;)LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2132570
    invoke-direct {p0}, LX/EX9;->y()LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2132571
    sget-object v0, LX/EYC;->B:LX/EYn;

    const-class v1, LX/EXA;

    const-class v2, LX/EX9;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2132572
    sget-object v0, LX/EYC;->A:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2132573
    invoke-direct {p0}, LX/EX9;->y()LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2132574
    invoke-virtual {p0}, LX/EX9;->l()LX/EXA;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2132575
    invoke-direct {p0}, LX/EX9;->A()LX/EXA;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2132576
    invoke-virtual {p0}, LX/EX9;->l()LX/EXA;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2132577
    invoke-direct {p0}, LX/EX9;->A()LX/EXA;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EXA;
    .locals 3

    .prologue
    .line 2132578
    new-instance v0, LX/EXA;

    invoke-direct {v0, p0}, LX/EXA;-><init>(LX/EWy;)V

    .line 2132579
    iget-object v1, p0, LX/EX9;->c:LX/EZ2;

    if-nez v1, :cond_1

    .line 2132580
    iget v1, p0, LX/EX9;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2132581
    iget-object v1, p0, LX/EX9;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EX9;->b:Ljava/util/List;

    .line 2132582
    iget v1, p0, LX/EX9;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/EX9;->a:I

    .line 2132583
    :cond_0
    iget-object v1, p0, LX/EX9;->b:Ljava/util/List;

    .line 2132584
    iput-object v1, v0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    .line 2132585
    :goto_0
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2132586
    return-object v0

    .line 2132587
    :cond_1
    iget-object v1, p0, LX/EX9;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2132588
    iput-object v1, v0, LX/EXA;->uninterpretedOption_:Ljava/util/List;

    .line 2132589
    goto :goto_0
.end method

.method public final synthetic m()LX/EWy;
    .locals 1

    .prologue
    .line 2132590
    invoke-direct {p0}, LX/EX9;->y()LX/EX9;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2132591
    sget-object v0, LX/EXA;->c:LX/EXA;

    move-object v0, v0

    .line 2132592
    return-object v0
.end method
