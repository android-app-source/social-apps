.class public final LX/Cp2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Cp3;


# direct methods
.method public constructor <init>(LX/Cp3;)V
    .locals 0

    .prologue
    .line 1936482
    iput-object p1, p0, LX/Cp2;->a:LX/Cp3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1936483
    iget-object v0, p0, LX/Cp2;->a:LX/Cp3;

    iget-object v0, v0, LX/Cp3;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/8ba;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1936484
    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->E:Ljava/lang/String;

    .line 1936485
    iget-object v0, p0, LX/Cp2;->a:LX/Cp3;

    iget-object v0, v0, LX/Cp3;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v0, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936486
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936487
    iget-object v1, p0, LX/Cp2;->a:LX/Cp3;

    iget-object v1, v1, LX/Cp3;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->E:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1936488
    iget-object v0, p0, LX/Cp2;->a:LX/Cp3;

    iget-object v0, v0, LX/Cp3;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081c77

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/Cp2;->a:LX/Cp3;

    iget-object v1, v1, LX/Cp3;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->E:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1936489
    iget-object v1, p0, LX/Cp2;->a:LX/Cp3;

    iget-object v1, v1, LX/Cp3;->b:Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    iget-object v1, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->w:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936490
    iget-object p0, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, p0

    .line 1936491
    invoke-virtual {v1, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1936492
    const/4 v0, 0x1

    return v0
.end method
