.class public LX/EOC;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EOE;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EOC",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EOE;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2114138
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2114139
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/EOC;->b:LX/0Zi;

    .line 2114140
    iput-object p1, p0, LX/EOC;->a:LX/0Ot;

    .line 2114141
    return-void
.end method

.method public static a(LX/0QB;)LX/EOC;
    .locals 4

    .prologue
    .line 2114127
    const-class v1, LX/EOC;

    monitor-enter v1

    .line 2114128
    :try_start_0
    sget-object v0, LX/EOC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114129
    sput-object v2, LX/EOC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114130
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114131
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114132
    new-instance v3, LX/EOC;

    const/16 p0, 0x3459

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EOC;-><init>(LX/0Ot;)V

    .line 2114133
    move-object v0, v3

    .line 2114134
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114135
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EOC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114136
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114137
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)V
    .locals 3

    .prologue
    .line 2114078
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 2114079
    if-nez v0, :cond_0

    .line 2114080
    :goto_0
    return-void

    .line 2114081
    :cond_0
    check-cast v0, LX/EOA;

    .line 2114082
    new-instance v1, LX/EOB;

    iget-object v2, v0, LX/EOA;->e:LX/EOC;

    invoke-direct {v1, v2}, LX/EOB;-><init>(LX/EOC;)V

    move-object v0, v1

    .line 2114083
    invoke-virtual {p0, v0}, LX/1De;->a(LX/48B;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2114088
    check-cast p2, LX/EOA;

    .line 2114089
    iget-object v0, p0, LX/EOC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EOE;

    iget-object v2, p2, LX/EOA;->a:LX/C33;

    iget-object v3, p2, LX/EOA;->b:LX/1Ps;

    iget-boolean v4, p2, LX/EOA;->c:Z

    iget-boolean v5, p2, LX/EOA;->d:Z

    move-object v1, p1

    .line 2114090
    iget-object v6, v0, LX/EOE;->c:LX/0ad;

    sget v7, LX/100;->z:I

    const/4 v8, 0x4

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    .line 2114091
    iget-object v7, v0, LX/EOE;->c:LX/0ad;

    sget-short v8, LX/100;->A:S

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v7

    if-nez v7, :cond_0

    if-eqz v5, :cond_1

    .line 2114092
    :cond_0
    const v6, 0x7fffffff

    .line 2114093
    :cond_1
    iget-object v7, v0, LX/EOE;->c:LX/0ad;

    sget-short v8, LX/100;->B:S

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v8

    .line 2114094
    iget-object v7, v0, LX/EOE;->c:LX/0ad;

    sget-short v9, LX/100;->C:S

    const/4 v10, 0x0

    invoke-interface {v7, v9, v10}, LX/0ad;->a(SZ)Z

    move-result v9

    .line 2114095
    const/4 v7, 0x0

    .line 2114096
    if-eqz v4, :cond_2

    .line 2114097
    new-instance v10, LX/EOD;

    invoke-direct {v10, v0, v2, v1}, LX/EOD;-><init>(LX/EOE;LX/C33;LX/1De;)V

    .line 2114098
    new-instance v7, LX/C33;

    iget-object p0, v2, LX/C33;->b:LX/C34;

    invoke-virtual {v2}, LX/C33;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p1

    iget-object p2, v2, LX/C33;->d:Ljava/lang/String;

    invoke-direct {v7, v10, p0, p1, p2}, LX/C33;-><init>(Landroid/view/View$OnClickListener;LX/C34;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 2114099
    :cond_2
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    .line 2114100
    const p0, -0xeb3933e

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v1, p1, p2

    invoke-static {v1, p0, p1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2114101
    invoke-interface {v10, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v10

    iget-object p0, v0, LX/EOE;->b:LX/C3U;

    const/4 p1, 0x0

    .line 2114102
    new-instance p2, LX/C3T;

    invoke-direct {p2, p0}, LX/C3T;-><init>(LX/C3U;)V

    .line 2114103
    iget-object v4, p0, LX/C3U;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C3S;

    .line 2114104
    if-nez v4, :cond_3

    .line 2114105
    new-instance v4, LX/C3S;

    invoke-direct {v4, p0}, LX/C3S;-><init>(LX/C3U;)V

    .line 2114106
    :cond_3
    invoke-static {v4, v1, p1, p1, p2}, LX/C3S;->a$redex0(LX/C3S;LX/1De;IILX/C3T;)V

    .line 2114107
    move-object p2, v4

    .line 2114108
    move-object p1, p2

    .line 2114109
    move-object p0, p1

    .line 2114110
    if-nez v7, :cond_4

    move-object v7, v2

    .line 2114111
    :cond_4
    iget-object p1, p0, LX/C3S;->a:LX/C3T;

    iput-object v7, p1, LX/C3T;->a:LX/C33;

    .line 2114112
    iget-object p1, p0, LX/C3S;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/BitSet;->set(I)V

    .line 2114113
    move-object v7, p0

    .line 2114114
    iget-object p0, v7, LX/C3S;->a:LX/C3T;

    iput-object v3, p0, LX/C3T;->b:LX/1Ps;

    .line 2114115
    iget-object p0, v7, LX/C3S;->e:Ljava/util/BitSet;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2114116
    move-object v7, v7

    .line 2114117
    iget-object p0, v7, LX/C3S;->a:LX/C3T;

    iput v6, p0, LX/C3T;->c:I

    .line 2114118
    move-object v6, v7

    .line 2114119
    iget-object v7, v6, LX/C3S;->a:LX/C3T;

    iput-boolean v9, v7, LX/C3T;->d:Z

    .line 2114120
    move-object v6, v6

    .line 2114121
    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v7, 0x3

    const v9, 0x7f0b0060

    invoke-interface {v6, v7, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    invoke-interface {v10, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-virtual {v2}, LX/C33;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 2114122
    iget-object v9, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v9

    .line 2114123
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v6, v9, v10}, LX/1WY;->a(Lcom/facebook/graphql/model/GraphQLStory;ZZ)Z

    move-result v6

    if-eqz v6, :cond_5

    if-eqz v8, :cond_5

    iget-object v6, v0, LX/EOE;->a:LX/1WX;

    invoke-virtual {v6, v1}, LX/1WX;->c(LX/1De;)LX/1XJ;

    move-result-object v8

    invoke-virtual {v2}, LX/C33;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 2114124
    iget-object v9, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v9

    .line 2114125
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v8, v6}, LX/1XJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1XJ;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v8, 0x6

    const v9, 0x7f0b0060

    invoke-interface {v6, v8, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    const/4 v8, 0x3

    const v9, 0x7f0b0062

    invoke-interface {v6, v8, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    :goto_0
    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2114126
    return-object v0

    :cond_5
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2114142
    invoke-static {}, LX/1dS;->b()V

    .line 2114143
    iget v0, p1, LX/1dQ;->b:I

    .line 2114144
    packed-switch v0, :pswitch_data_0

    .line 2114145
    :goto_0
    return-object v3

    .line 2114146
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2114147
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/1De;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 2114148
    check-cast v2, LX/EOA;

    .line 2114149
    iget-object p1, p0, LX/EOC;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v2, LX/EOA;->a:LX/C33;

    iget-boolean p2, v2, LX/EOA;->c:Z

    .line 2114150
    iget-object p0, p1, LX/C33;->a:Landroid/view/View$OnClickListener;

    if-eqz p0, :cond_0

    .line 2114151
    iget-object p0, p1, LX/C33;->a:Landroid/view/View$OnClickListener;

    invoke-interface {p0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2114152
    :cond_0
    if-eqz p2, :cond_1

    .line 2114153
    invoke-static {v0}, LX/EOC;->d(LX/1De;)V

    .line 2114154
    :cond_1
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0xeb3933e
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 2114084
    check-cast p1, LX/EOA;

    .line 2114085
    check-cast p2, LX/EOA;

    .line 2114086
    iget-boolean v0, p1, LX/EOA;->d:Z

    iput-boolean v0, p2, LX/EOA;->d:Z

    .line 2114087
    return-void
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 2

    .prologue
    .line 2114069
    check-cast p2, LX/EOA;

    .line 2114070
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 2114071
    iget-object v0, p0, LX/EOC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2114072
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2114073
    iput-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2114074
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2114075
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/EOA;->d:Z

    .line 2114076
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 2114077
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2114068
    const/4 v0, 0x1

    return v0
.end method
