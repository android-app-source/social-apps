.class public final LX/EOP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EOQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsBlendedPhotoPublicModuleInterfaces$SearchResultsBlendedPhotoPublicModule;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/EOS;

.field public c:I

.field public final synthetic d:LX/EOQ;


# direct methods
.method public constructor <init>(LX/EOQ;)V
    .locals 1

    .prologue
    .line 2114474
    iput-object p1, p0, LX/EOP;->d:LX/EOQ;

    .line 2114475
    move-object v0, p1

    .line 2114476
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2114477
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2114494
    const-string v0, "SearchResultsBlendedPhotoPublicGridComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2114478
    if-ne p0, p1, :cond_1

    .line 2114479
    :cond_0
    :goto_0
    return v0

    .line 2114480
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2114481
    goto :goto_0

    .line 2114482
    :cond_3
    check-cast p1, LX/EOP;

    .line 2114483
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2114484
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2114485
    if-eq v2, v3, :cond_0

    .line 2114486
    iget-object v2, p0, LX/EOP;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EOP;->a:LX/CzL;

    iget-object v3, p1, LX/EOP;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2114487
    goto :goto_0

    .line 2114488
    :cond_5
    iget-object v2, p1, LX/EOP;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2114489
    :cond_6
    iget-object v2, p0, LX/EOP;->b:LX/EOS;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/EOP;->b:LX/EOS;

    iget-object v3, p1, LX/EOP;->b:LX/EOS;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2114490
    goto :goto_0

    .line 2114491
    :cond_8
    iget-object v2, p1, LX/EOP;->b:LX/EOS;

    if-nez v2, :cond_7

    .line 2114492
    :cond_9
    iget v2, p0, LX/EOP;->c:I

    iget v3, p1, LX/EOP;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2114493
    goto :goto_0
.end method
