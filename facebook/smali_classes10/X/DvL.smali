.class public LX/DvL;
.super LX/DvF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DvF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "LX/DvK;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/DvL;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvd;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dvd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2058286
    invoke-direct {p0}, LX/DvF;-><init>()V

    .line 2058287
    iput-object p1, p0, LX/DvL;->a:LX/0Ot;

    .line 2058288
    iput-object p2, p0, LX/DvL;->b:LX/0Ot;

    .line 2058289
    return-void
.end method

.method public static a(LX/0QB;)LX/DvL;
    .locals 5

    .prologue
    .line 2058258
    sget-object v0, LX/DvL;->c:LX/DvL;

    if-nez v0, :cond_1

    .line 2058259
    const-class v1, LX/DvL;

    monitor-enter v1

    .line 2058260
    :try_start_0
    sget-object v0, LX/DvL;->c:LX/DvL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2058261
    if-eqz v2, :cond_0

    .line 2058262
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2058263
    new-instance v3, LX/DvL;

    const/16 v4, 0x2e93

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x2e94

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/DvL;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2058264
    move-object v0, v3

    .line 2058265
    sput-object v0, LX/DvL;->c:LX/DvL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2058266
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2058267
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2058268
    :cond_1
    sget-object v0, LX/DvL;->c:LX/DvL;

    return-object v0

    .line 2058269
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2058270
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2058271
    check-cast p1, LX/DvK;

    check-cast p2, Lcom/facebook/fbservice/service/OperationResult;

    .line 2058272
    iget-boolean v0, p2, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2058273
    if-nez v0, :cond_0

    .line 2058274
    :goto_0
    return-object p2

    .line 2058275
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;

    .line 2058276
    iget-object v0, v6, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    if-eqz v0, :cond_1

    iget-object v0, v6, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2058277
    :cond_1
    iget-boolean v0, p2, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2058278
    if-eqz v0, :cond_2

    .line 2058279
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2058280
    new-instance v1, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;

    invoke-direct {v1, v0}, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;-><init>(LX/0Px;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object p2

    goto :goto_0

    .line 2058281
    :cond_2
    new-instance v0, LX/Dvm;

    iget-object v1, p1, LX/DvK;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget v2, p1, LX/DvK;->b:I

    iget v3, p1, LX/DvK;->c:I

    iget-object v4, p1, LX/DvK;->d:LX/DvW;

    iget-object v5, v6, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/Dvm;-><init>(Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IILX/DvW;Z)V

    .line 2058282
    iget-object v1, p1, LX/DvK;->e:LX/Dvf;

    if-eqz v1, :cond_3

    iget-object v1, p1, LX/DvK;->e:LX/Dvf;

    move-object v2, v1

    .line 2058283
    :goto_1
    iget-object v1, p0, LX/DvL;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dvd;

    iget-object v3, v6, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, LX/Dvd;->a(LX/Dvm;LX/Dvf;LX/0Px;Z)LX/0Px;

    move-result-object v0

    .line 2058284
    new-instance v1, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;

    invoke-direct {v1, v0}, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;-><init>(LX/0Px;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object p2

    goto :goto_0

    .line 2058285
    :cond_3
    iget-object v1, p0, LX/DvL;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dvf;

    move-object v2, v1

    goto :goto_1
.end method
