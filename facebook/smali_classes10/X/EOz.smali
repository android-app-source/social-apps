.class public final LX/EOz;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EP0;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/A3T;

.field public final synthetic b:LX/EP0;


# direct methods
.method public constructor <init>(LX/EP0;)V
    .locals 1

    .prologue
    .line 2115608
    iput-object p1, p0, LX/EOz;->b:LX/EP0;

    .line 2115609
    move-object v0, p1

    .line 2115610
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2115611
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2115612
    const-string v0, "PulseContextSourceComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2115613
    if-ne p0, p1, :cond_1

    .line 2115614
    :cond_0
    :goto_0
    return v0

    .line 2115615
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2115616
    goto :goto_0

    .line 2115617
    :cond_3
    check-cast p1, LX/EOz;

    .line 2115618
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2115619
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2115620
    if-eq v2, v3, :cond_0

    .line 2115621
    iget-object v2, p0, LX/EOz;->a:LX/A3T;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/EOz;->a:LX/A3T;

    iget-object v3, p1, LX/EOz;->a:LX/A3T;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2115622
    goto :goto_0

    .line 2115623
    :cond_4
    iget-object v2, p1, LX/EOz;->a:LX/A3T;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
