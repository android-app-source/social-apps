.class public LX/Enc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EnI;


# instance fields
.field private a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private final b:Lcom/facebook/entitycards/intent/EntityCardsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycards/intent/EntityCardsFragment;)V
    .locals 0

    .prologue
    .line 2167307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167308
    iput-object p1, p0, LX/Enc;->b:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    .line 2167309
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewStub;)Landroid/support/v4/view/ViewPager;
    .locals 2

    .prologue
    .line 2167304
    const v0, 0x7f03048c

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2167305
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, LX/Enc;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2167306
    iget-object v0, p0, LX/Enc;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f0d0d8b

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method public final a()Landroid/view/View$OnTouchListener;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2167295
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(ZLX/Emm;)V
    .locals 1

    .prologue
    .line 2167302
    iget-object v0, p0, LX/Enc;->b:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    invoke-virtual {v0, p2}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->a(LX/Emm;)V

    .line 2167303
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2167299
    iget-object v0, p0, LX/Enc;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2167300
    iget-object v0, p0, LX/Enc;->b:Lcom/facebook/entitycards/intent/EntityCardsFragment;

    invoke-virtual {v0}, Lcom/facebook/entitycards/intent/EntityCardsFragment;->b()V

    .line 2167301
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2167297
    iget-object v0, p0, LX/Enc;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2167298
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2167296
    return-void
.end method
