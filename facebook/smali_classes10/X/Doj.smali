.class public LX/Doj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/crypto/keychain/KeyChain;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1IW;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/security/SecureRandom;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042359
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Doj;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Hr;LX/0Ot;LX/0Or;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/1Hr;",
            "LX/0Ot",
            "<",
            "LX/1IW;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042354
    iput-object p1, p0, LX/Doj;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2042355
    iput-object p3, p0, LX/Doj;->b:LX/0Ot;

    .line 2042356
    iput-object p4, p0, LX/Doj;->c:LX/0Or;

    .line 2042357
    invoke-static {}, LX/1Hr;->a()Ljava/security/SecureRandom;

    move-result-object v0

    iput-object v0, p0, LX/Doj;->d:Ljava/security/SecureRandom;

    .line 2042358
    return-void
.end method

.method public static a(LX/0QB;)LX/Doj;
    .locals 10

    .prologue
    .line 2042324
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2042325
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2042326
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2042327
    if-nez v1, :cond_0

    .line 2042328
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2042329
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2042330
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2042331
    sget-object v1, LX/Doj;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2042332
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2042333
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2042334
    :cond_1
    if-nez v1, :cond_4

    .line 2042335
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2042336
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2042337
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2042338
    new-instance v8, LX/Doj;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1Hr;->a(LX/0QB;)LX/1Hr;

    move-result-object v7

    check-cast v7, LX/1Hr;

    const/16 v9, 0x488

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v8, v1, v7, v9, p0}, LX/Doj;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Hr;LX/0Ot;LX/0Or;)V

    .line 2042339
    move-object v1, v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2042340
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2042341
    if-nez v1, :cond_2

    .line 2042342
    sget-object v0, LX/Doj;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doj;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2042343
    :goto_1
    if-eqz v0, :cond_3

    .line 2042344
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042345
    :goto_3
    check-cast v0, LX/Doj;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2042346
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2042347
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2042348
    :catchall_1
    move-exception v0

    .line 2042349
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2042350
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2042351
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2042352
    :cond_2
    :try_start_8
    sget-object v0, LX/Doj;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Doj;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Ljava/lang/String;)[B
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2042320
    sget-object v0, LX/Don;->a:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2042321
    iget-object v2, p0, LX/Doj;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2042322
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2042323
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/Doj;->b(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method private a([BLX/1Hb;)[B
    .locals 3

    .prologue
    .line 2042315
    :try_start_0
    iget-object v0, p0, LX/Doj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1IW;

    invoke-virtual {v0, p1, p2}, LX/1IW;->b([BLX/1Hb;)[B
    :try_end_0
    .catch LX/48k; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/48f; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2042316
    :catch_0
    move-exception v0

    .line 2042317
    :goto_0
    const-string v1, "LegacyMasterKeyChain"

    const-string v2, "Failed to decrypt master key from local storage"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2042318
    new-instance v1, LX/48g;

    const-string v2, "Decryption failed"

    invoke-direct {v1, v2, v0}, LX/48g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2042319
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 2042360
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/Doj;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2042309
    iget-object v0, p0, LX/Doj;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2042310
    if-nez v0, :cond_0

    .line 2042311
    const/4 v0, 0x0

    .line 2042312
    :goto_0
    return-object v0

    .line 2042313
    :cond_0
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2042314
    goto :goto_0
.end method


# virtual methods
.method public final a()[B
    .locals 4

    .prologue
    .line 2042297
    invoke-static {p0}, LX/Doj;->f(LX/Doj;)Ljava/lang/String;

    move-result-object v0

    .line 2042298
    if-nez v0, :cond_0

    .line 2042299
    const-string v0, "LegacyMasterKeyChain"

    const-string v1, "getCipherKey called when user not logged in"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042300
    new-instance v0, LX/48g;

    const-string v1, "Legacy key chain only available with logged-in user"

    invoke-direct {v0, v1}, LX/48g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2042301
    :cond_0
    invoke-direct {p0, v0}, LX/Doj;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 2042302
    if-eqz v1, :cond_2

    .line 2042303
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UserMasterKey."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1Hb;->a(Ljava/lang/String;)LX/1Hb;

    move-result-object v0

    .line 2042304
    invoke-direct {p0, v1, v0}, LX/Doj;->a([BLX/1Hb;)[B

    move-result-object v0

    .line 2042305
    :cond_1
    return-object v0

    .line 2042306
    :cond_2
    invoke-virtual {p0}, LX/Doj;->d()[B

    move-result-object v0

    .line 2042307
    if-nez v0, :cond_1

    .line 2042308
    new-instance v0, LX/Doi;

    const-string v1, "No legacy keys found"

    invoke-direct {v0, v1}, LX/Doi;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()[B
    .locals 2

    .prologue
    .line 2042294
    sget-object v0, LX/Doc;->a:LX/1Hy;

    iget v0, v0, LX/1Hy;->ivLength:I

    new-array v0, v0, [B

    .line 2042295
    iget-object v1, p0, LX/Doj;->d:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 2042296
    return-object v0
.end method

.method public final d()[B
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2042291
    iget-object v1, p0, LX/Doj;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Don;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2042292
    if-nez v1, :cond_0

    .line 2042293
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v1}, LX/Doj;->b(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public final e()[B
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2042286
    invoke-static {p0}, LX/Doj;->f(LX/Doj;)Ljava/lang/String;

    move-result-object v0

    .line 2042287
    if-nez v0, :cond_0

    .line 2042288
    const-string v0, "LegacyMasterKeyChain"

    const-string v1, "getRawUserMasterKey called when user not logged in"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042289
    new-instance v0, LX/48g;

    const-string v1, "Legacy key chain only available with logged-in user"

    invoke-direct {v0, v1}, LX/48g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2042290
    :cond_0
    invoke-direct {p0, v0}, LX/Doj;->a(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method
