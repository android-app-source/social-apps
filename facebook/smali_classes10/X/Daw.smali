.class public final LX/Daw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2016602
    const/4 v10, 0x0

    .line 2016603
    const/4 v9, 0x0

    .line 2016604
    const/4 v8, 0x0

    .line 2016605
    const/4 v7, 0x0

    .line 2016606
    const/4 v6, 0x0

    .line 2016607
    const/4 v5, 0x0

    .line 2016608
    const/4 v4, 0x0

    .line 2016609
    const/4 v3, 0x0

    .line 2016610
    const/4 v2, 0x0

    .line 2016611
    const/4 v1, 0x0

    .line 2016612
    const/4 v0, 0x0

    .line 2016613
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 2016614
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2016615
    const/4 v0, 0x0

    .line 2016616
    :goto_0
    return v0

    .line 2016617
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2016618
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 2016619
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2016620
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2016621
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2016622
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2016623
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 2016624
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2016625
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2016626
    :cond_4
    const-string v12, "is_viewer_coworker"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2016627
    const/4 v2, 0x1

    .line 2016628
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 2016629
    :cond_5
    const-string v12, "is_viewer_friend"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2016630
    const/4 v1, 0x1

    .line 2016631
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 2016632
    :cond_6
    const-string v12, "is_work_user"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2016633
    const/4 v0, 0x1

    .line 2016634
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 2016635
    :cond_7
    const-string v12, "last_active_messages_status"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2016636
    invoke-static {p0, p1}, LX/Dau;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2016637
    :cond_8
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 2016638
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 2016639
    :cond_9
    const-string v12, "profile_picture"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2016640
    invoke-static {p0, p1}, LX/Dav;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2016641
    :cond_a
    const/16 v11, 0x8

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2016642
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 2016643
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 2016644
    if-eqz v2, :cond_b

    .line 2016645
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8}, LX/186;->a(IZ)V

    .line 2016646
    :cond_b
    if-eqz v1, :cond_c

    .line 2016647
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 2016648
    :cond_c
    if-eqz v0, :cond_d

    .line 2016649
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 2016650
    :cond_d
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2016651
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2016652
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2016653
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2016654
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2016655
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2016656
    if-eqz v0, :cond_0

    .line 2016657
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016658
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2016659
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2016660
    if-eqz v0, :cond_1

    .line 2016661
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016662
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2016663
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2016664
    if-eqz v0, :cond_2

    .line 2016665
    const-string v1, "is_viewer_coworker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016666
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2016667
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2016668
    if-eqz v0, :cond_3

    .line 2016669
    const-string v1, "is_viewer_friend"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016670
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2016671
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2016672
    if-eqz v0, :cond_4

    .line 2016673
    const-string v1, "is_work_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016674
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2016675
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2016676
    if-eqz v0, :cond_5

    .line 2016677
    const-string v1, "last_active_messages_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016678
    invoke-static {p0, v0, p2}, LX/Dau;->a(LX/15i;ILX/0nX;)V

    .line 2016679
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2016680
    if-eqz v0, :cond_6

    .line 2016681
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016682
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2016683
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2016684
    if-eqz v0, :cond_7

    .line 2016685
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016686
    invoke-static {p0, v0, p2}, LX/Dav;->a(LX/15i;ILX/0nX;)V

    .line 2016687
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2016688
    return-void
.end method
