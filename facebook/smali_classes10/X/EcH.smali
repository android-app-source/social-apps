.class public final LX/EcH;
.super LX/EWp;
.source ""

# interfaces
.implements LX/Ec3;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcH;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EcH;


# instance fields
.field public bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public senderChainKey_:LX/Ec8;

.field public senderKeyId_:I

.field public senderMessageKeys_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EcC;",
            ">;"
        }
    .end annotation
.end field

.field public senderSigningKey_:LX/EcG;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2146074
    new-instance v0, LX/Ec2;

    invoke-direct {v0}, LX/Ec2;-><init>()V

    sput-object v0, LX/EcH;->a:LX/EWZ;

    .line 2146075
    new-instance v0, LX/EcH;

    invoke-direct {v0}, LX/EcH;-><init>()V

    .line 2146076
    sput-object v0, LX/EcH;->c:LX/EcH;

    invoke-direct {v0}, LX/EcH;->z()V

    .line 2146077
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2146069
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2146070
    iput-byte v0, p0, LX/EcH;->memoizedIsInitialized:B

    .line 2146071
    iput v0, p0, LX/EcH;->memoizedSerializedSize:I

    .line 2146072
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2146073
    iput-object v0, p0, LX/EcH;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/16 v7, 0x8

    .line 2146016
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2146017
    iput-byte v1, p0, LX/EcH;->memoizedIsInitialized:B

    .line 2146018
    iput v1, p0, LX/EcH;->memoizedSerializedSize:I

    .line 2146019
    invoke-direct {p0}, LX/EcH;->z()V

    .line 2146020
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v6

    move v4, v0

    move v1, v0

    .line 2146021
    :goto_0
    if-nez v4, :cond_3

    .line 2146022
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2146023
    sparse-switch v0, :sswitch_data_0

    .line 2146024
    invoke-virtual {p0, p1, v6, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_8

    move v4, v5

    .line 2146025
    goto :goto_0

    :sswitch_0
    move v4, v5

    .line 2146026
    goto :goto_0

    .line 2146027
    :sswitch_1
    iget v0, p0, LX/EcH;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EcH;->bitField0_:I

    .line 2146028
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v0

    iput v0, p0, LX/EcH;->senderKeyId_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2146029
    :catch_0
    move-exception v0

    .line 2146030
    :goto_1
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2146031
    move-object v0, v0

    .line 2146032
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2146033
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_0

    .line 2146034
    iget-object v1, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    .line 2146035
    :cond_0
    invoke-virtual {v6}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EcH;->unknownFields:LX/EZQ;

    .line 2146036
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2146037
    :sswitch_2
    :try_start_2
    iget v0, p0, LX/EcH;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_7

    .line 2146038
    iget-object v0, p0, LX/EcH;->senderChainKey_:LX/Ec8;

    invoke-virtual {v0}, LX/Ec8;->o()LX/Ec7;

    move-result-object v0

    move-object v2, v0

    .line 2146039
    :goto_3
    sget-object v0, LX/Ec8;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/Ec8;

    iput-object v0, p0, LX/EcH;->senderChainKey_:LX/Ec8;

    .line 2146040
    if-eqz v2, :cond_1

    .line 2146041
    iget-object v0, p0, LX/EcH;->senderChainKey_:LX/Ec8;

    invoke-virtual {v2, v0}, LX/Ec7;->a(LX/Ec8;)LX/Ec7;

    .line 2146042
    invoke-virtual {v2}, LX/Ec7;->m()LX/Ec8;

    move-result-object v0

    iput-object v0, p0, LX/EcH;->senderChainKey_:LX/Ec8;

    .line 2146043
    :cond_1
    iget v0, p0, LX/EcH;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EcH;->bitField0_:I
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2146044
    :catch_1
    move-exception v0

    .line 2146045
    :goto_4
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2146046
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2146047
    move-object v0, v2

    .line 2146048
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2146049
    :sswitch_3
    :try_start_4
    iget v0, p0, LX/EcH;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_6

    .line 2146050
    iget-object v0, p0, LX/EcH;->senderSigningKey_:LX/EcG;

    invoke-virtual {v0}, LX/EcG;->o()LX/EcF;

    move-result-object v0

    move-object v2, v0

    .line 2146051
    :goto_5
    sget-object v0, LX/EcG;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EcG;

    iput-object v0, p0, LX/EcH;->senderSigningKey_:LX/EcG;

    .line 2146052
    if-eqz v2, :cond_2

    .line 2146053
    iget-object v0, p0, LX/EcH;->senderSigningKey_:LX/EcG;

    invoke-virtual {v2, v0}, LX/EcF;->a(LX/EcG;)LX/EcF;

    .line 2146054
    invoke-virtual {v2}, LX/EcF;->m()LX/EcG;

    move-result-object v0

    iput-object v0, p0, LX/EcH;->senderSigningKey_:LX/EcG;

    .line 2146055
    :cond_2
    iget v0, p0, LX/EcH;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EcH;->bitField0_:I

    goto/16 :goto_0

    .line 2146056
    :sswitch_4
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v7, :cond_5

    .line 2146057
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2146058
    or-int/lit8 v0, v1, 0x8

    .line 2146059
    :goto_6
    :try_start_5
    iget-object v1, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    sget-object v2, LX/EcC;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch LX/EYr; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_7
    move v1, v0

    .line 2146060
    goto/16 :goto_0

    .line 2146061
    :cond_3
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_4

    .line 2146062
    iget-object v0, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    .line 2146063
    :cond_4
    invoke-virtual {v6}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcH;->unknownFields:LX/EZQ;

    .line 2146064
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2146065
    return-void

    .line 2146066
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_2

    .line 2146067
    :catch_2
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_4

    .line 2146068
    :catch_3
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_1

    :cond_5
    move v0, v1

    goto :goto_6

    :cond_6
    move-object v2, v3

    goto :goto_5

    :cond_7
    move-object v2, v3

    goto/16 :goto_3

    :cond_8
    move v0, v1

    goto :goto_7

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2146011
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2146012
    iput-byte v1, p0, LX/EcH;->memoizedIsInitialized:B

    .line 2146013
    iput v1, p0, LX/EcH;->memoizedSerializedSize:I

    .line 2146014
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcH;->unknownFields:LX/EZQ;

    .line 2146015
    return-void
.end method

.method private z()V
    .locals 1

    .prologue
    .line 2146004
    const/4 v0, 0x0

    iput v0, p0, LX/EcH;->senderKeyId_:I

    .line 2146005
    sget-object v0, LX/Ec8;->c:LX/Ec8;

    move-object v0, v0

    .line 2146006
    iput-object v0, p0, LX/EcH;->senderChainKey_:LX/Ec8;

    .line 2146007
    sget-object v0, LX/EcG;->c:LX/EcG;

    move-object v0, v0

    .line 2146008
    iput-object v0, p0, LX/EcH;->senderSigningKey_:LX/EcG;

    .line 2146009
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    .line 2146010
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2146002
    new-instance v0, LX/Ec4;

    invoke-direct {v0, p1}, LX/Ec4;-><init>(LX/EYd;)V

    .line 2146003
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2145990
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2145991
    iget v0, p0, LX/EcH;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2145992
    iget v0, p0, LX/EcH;->senderKeyId_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2145993
    :cond_0
    iget v0, p0, LX/EcH;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2145994
    iget-object v0, p0, LX/EcH;->senderChainKey_:LX/Ec8;

    invoke-virtual {p1, v2, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2145995
    :cond_1
    iget v0, p0, LX/EcH;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2145996
    const/4 v0, 0x3

    iget-object v1, p0, LX/EcH;->senderSigningKey_:LX/EcG;

    invoke-virtual {p1, v0, v1}, LX/EWf;->b(ILX/EWW;)V

    .line 2145997
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2145998
    iget-object v0, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2145999
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2146000
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2146001
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2145985
    iget-byte v1, p0, LX/EcH;->memoizedIsInitialized:B

    .line 2145986
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2145987
    :goto_0
    return v0

    .line 2145988
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2145989
    :cond_1
    iput-byte v0, p0, LX/EcH;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2146078
    iget v0, p0, LX/EcH;->memoizedSerializedSize:I

    .line 2146079
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2146080
    :goto_0
    return v0

    .line 2146081
    :cond_0
    iget v0, p0, LX/EcH;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 2146082
    iget v0, p0, LX/EcH;->senderKeyId_:I

    invoke-static {v3, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2146083
    :goto_1
    iget v2, p0, LX/EcH;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2146084
    iget-object v2, p0, LX/EcH;->senderChainKey_:LX/Ec8;

    invoke-static {v4, v2}, LX/EWf;->e(ILX/EWW;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2146085
    :cond_1
    iget v2, p0, LX/EcH;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 2146086
    const/4 v2, 0x3

    iget-object v3, p0, LX/EcH;->senderSigningKey_:LX/EcG;

    invoke-static {v2, v3}, LX/EWf;->e(ILX/EWW;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    move v2, v0

    .line 2146087
    :goto_2
    iget-object v0, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2146088
    iget-object v0, p0, LX/EcH;->senderMessageKeys_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v5, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2146089
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2146090
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2146091
    iput v0, p0, LX/EcH;->memoizedSerializedSize:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2145984
    iget-object v0, p0, LX/EcH;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2145983
    sget-object v0, LX/Eck;->v:LX/EYn;

    const-class v1, LX/EcH;

    const-class v2, LX/Ec4;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2145982
    sget-object v0, LX/EcH;->a:LX/EWZ;

    return-object v0
.end method

.method public final r()LX/Ec4;
    .locals 1

    .prologue
    .line 2145980
    invoke-static {}, LX/Ec4;->x()LX/Ec4;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/Ec4;->a(LX/EcH;)LX/Ec4;

    move-result-object v0

    move-object v0, v0

    .line 2145981
    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2145979
    invoke-virtual {p0}, LX/EcH;->r()LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2145978
    invoke-static {}, LX/Ec4;->x()LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2145977
    invoke-virtual {p0}, LX/EcH;->r()LX/Ec4;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145976
    sget-object v0, LX/EcH;->c:LX/EcH;

    return-object v0
.end method
