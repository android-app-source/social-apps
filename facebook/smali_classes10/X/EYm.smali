.class public final LX/EYm;
.super LX/EYk;
.source ""


# instance fields
.field public final h:Ljava/lang/reflect/Method;

.field private final i:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(LX/EYP;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EYP;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "LX/EWp;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "LX/EWj;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2138037
    invoke-direct {p0, p2, p3, p4}, LX/EYk;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 2138038
    iget-object v0, p0, LX/EYk;->a:Ljava/lang/Class;

    const-string v1, "newBuilder"

    new-array v2, v3, [Ljava/lang/Class;

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/EYm;->h:Ljava/lang/reflect/Method;

    .line 2138039
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "get"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Builder"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Class;

    invoke-static {p4, v0, v1}, LX/EWp;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, LX/EYm;->i:Ljava/lang/reflect/Method;

    .line 2138040
    return-void
.end method


# virtual methods
.method public final a(LX/EWj;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2138041
    iget-object v0, p0, LX/EYk;->a:Ljava/lang/Class;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2138042
    :goto_0
    move-object v0, p2

    .line 2138043
    invoke-super {p0, p1, v0}, LX/EYk;->a(LX/EWj;Ljava/lang/Object;)V

    .line 2138044
    return-void

    :cond_0
    iget-object v0, p0, LX/EYm;->h:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWU;

    check-cast p2, LX/EWY;

    invoke-interface {v0, p2}, LX/EWU;->c(LX/EWY;)LX/EWU;

    move-result-object v0

    invoke-interface {v0}, LX/EWU;->h()LX/EWY;

    move-result-object p2

    goto :goto_0
.end method

.method public final newBuilder()LX/EWU;
    .locals 3

    .prologue
    .line 2138045
    iget-object v0, p0, LX/EYm;->h:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LX/EWp;->b(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWU;

    return-object v0
.end method
