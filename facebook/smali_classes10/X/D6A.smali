.class public final LX/D6A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:LX/D6B;


# direct methods
.method public constructor <init>(LX/D6B;)V
    .locals 0

    .prologue
    .line 1965198
    iput-object p1, p0, LX/D6A;->a:LX/D6B;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 1965185
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 10

    .prologue
    .line 1965186
    iget-object v0, p0, LX/D6A;->a:LX/D6B;

    const/4 v3, 0x0

    .line 1965187
    iget-object v4, v0, LX/D6B;->j:LX/1Qq;

    if-eqz v4, :cond_0

    iget-object v4, v0, LX/D6B;->j:LX/1Qq;

    invoke-interface {v4}, LX/1Qr;->d()I

    move-result v4

    iget v5, v0, LX/D6B;->p:I

    if-ge v4, v5, :cond_0

    if-lez p3, :cond_0

    if-lez p4, :cond_0

    iget-object v4, v0, LX/D6B;->d:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1965188
    iget v6, v0, LX/D6B;->s:I

    const/4 v7, 0x5

    if-gt v6, v7, :cond_3

    iget-object v6, v0, LX/D6B;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    iget-wide v8, v0, LX/D6B;->t:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0xbb8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_3

    const/4 v6, 0x1

    :goto_0
    move v4, v6

    .line 1965189
    if-nez v4, :cond_2

    .line 1965190
    :cond_0
    :goto_1
    move v0, v3

    .line 1965191
    if-eqz v0, :cond_1

    .line 1965192
    iget-object v0, p0, LX/D6A;->a:LX/D6B;

    iget-object v1, p0, LX/D6A;->a:LX/D6B;

    iget-object v2, p0, LX/D6A;->a:LX/D6B;

    iget v2, v2, LX/D6B;->o:I

    invoke-static {v1, v2}, LX/D6B;->b(LX/D6B;I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/D6B;->a(I)Z

    .line 1965193
    :cond_1
    return-void

    .line 1965194
    :cond_2
    add-int v4, p2, p3

    .line 1965195
    iget-object v5, v0, LX/D6B;->j:LX/1Qq;

    invoke-interface {v5, v4}, LX/1Qr;->h_(I)I

    move-result v4

    .line 1965196
    iget-object v5, v0, LX/D6B;->j:LX/1Qq;

    invoke-interface {v5}, LX/1Qr;->d()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    .line 1965197
    sub-int v4, v5, v4

    iget v5, v0, LX/D6B;->q:I

    if-gt v4, v5, :cond_0

    const/4 v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    goto :goto_0
.end method
