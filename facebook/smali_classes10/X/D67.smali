.class public final LX/D67;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Zp;

.field public final synthetic b:LX/D6B;


# direct methods
.method public constructor <init>(LX/D6B;LX/1Zp;)V
    .locals 0

    .prologue
    .line 1965141
    iput-object p1, p0, LX/D67;->b:LX/D6B;

    iput-object p2, p0, LX/D67;->a:LX/1Zp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1965142
    iget-object v0, p0, LX/D67;->b:LX/D6B;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/D6B;->b(LX/D6B;Z)V

    .line 1965143
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1965144
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1965145
    iget-object v0, p0, LX/D67;->b:LX/D6B;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/D6B;->b(LX/D6B;Z)V

    .line 1965146
    if-eqz p1, :cond_0

    .line 1965147
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1965148
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D67;->a:LX/1Zp;

    invoke-virtual {v0}, LX/1Zp;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1965149
    :cond_0
    :goto_0
    return-void

    .line 1965150
    :cond_1
    iget-object v1, p0, LX/D67;->b:LX/D6B;

    .line 1965151
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1965152
    check-cast v0, Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;

    invoke-static {v1, v0}, LX/D6B;->a$redex0(LX/D6B;Lcom/facebook/video/channelfeed/protocol/FetchVideoChannelQueryModels$ChannelFeedVideoChannelFragmentModel;)V

    goto :goto_0
.end method
