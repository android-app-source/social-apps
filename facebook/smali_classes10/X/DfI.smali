.class public final LX/DfI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 53

    .prologue
    .line 2026608
    const/16 v47, 0x0

    .line 2026609
    const/16 v46, 0x0

    .line 2026610
    const/16 v45, 0x0

    .line 2026611
    const/16 v44, 0x0

    .line 2026612
    const/16 v43, 0x0

    .line 2026613
    const/16 v42, 0x0

    .line 2026614
    const/16 v41, 0x0

    .line 2026615
    const/16 v40, 0x0

    .line 2026616
    const/16 v39, 0x0

    .line 2026617
    const/16 v38, 0x0

    .line 2026618
    const/16 v37, 0x0

    .line 2026619
    const/16 v36, 0x0

    .line 2026620
    const/16 v35, 0x0

    .line 2026621
    const/16 v34, 0x0

    .line 2026622
    const/16 v33, 0x0

    .line 2026623
    const/16 v32, 0x0

    .line 2026624
    const/16 v31, 0x0

    .line 2026625
    const/16 v30, 0x0

    .line 2026626
    const/16 v29, 0x0

    .line 2026627
    const/16 v28, 0x0

    .line 2026628
    const/16 v27, 0x0

    .line 2026629
    const/16 v26, 0x0

    .line 2026630
    const/16 v21, 0x0

    .line 2026631
    const-wide/16 v24, 0x0

    .line 2026632
    const-wide/16 v22, 0x0

    .line 2026633
    const/16 v20, 0x0

    .line 2026634
    const/16 v19, 0x0

    .line 2026635
    const/16 v18, 0x0

    .line 2026636
    const/16 v17, 0x0

    .line 2026637
    const/16 v16, 0x0

    .line 2026638
    const/4 v15, 0x0

    .line 2026639
    const/4 v14, 0x0

    .line 2026640
    const/4 v13, 0x0

    .line 2026641
    const/4 v12, 0x0

    .line 2026642
    const/4 v11, 0x0

    .line 2026643
    const/4 v10, 0x0

    .line 2026644
    const/4 v9, 0x0

    .line 2026645
    const/4 v8, 0x0

    .line 2026646
    const/4 v7, 0x0

    .line 2026647
    const/4 v6, 0x0

    .line 2026648
    const/4 v5, 0x0

    .line 2026649
    const/4 v4, 0x0

    .line 2026650
    const/4 v3, 0x0

    .line 2026651
    const/4 v2, 0x0

    .line 2026652
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v48

    sget-object v49, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v48

    move-object/from16 v1, v49

    if-eq v0, v1, :cond_2f

    .line 2026653
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2026654
    const/4 v2, 0x0

    .line 2026655
    :goto_0
    return v2

    .line 2026656
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_29

    .line 2026657
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2026658
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2026659
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 2026660
    const-string v6, "__type__"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "__typename"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2026661
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v50, v2

    goto :goto_1

    .line 2026662
    :cond_2
    const-string v6, "action_text"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2026663
    invoke-static/range {p0 .. p1}, LX/Df1;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v49, v2

    goto :goto_1

    .line 2026664
    :cond_3
    const-string v6, "ad_client_token"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2026665
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v48, v2

    goto :goto_1

    .line 2026666
    :cond_4
    const-string v6, "ad_display_info"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2026667
    invoke-static/range {p0 .. p1}, LX/CJx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v47, v2

    goto :goto_1

    .line 2026668
    :cond_5
    const-string v6, "ad_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2026669
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v46, v2

    goto :goto_1

    .line 2026670
    :cond_6
    const-string v6, "ad_preference_uri"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2026671
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v45, v2

    goto/16 :goto_1

    .line 2026672
    :cond_7
    const-string v6, "ad_properties"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2026673
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v44, v2

    goto/16 :goto_1

    .line 2026674
    :cond_8
    const-string v6, "call_to_action_url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2026675
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v43, v2

    goto/16 :goto_1

    .line 2026676
    :cond_9
    const-string v6, "candidate"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2026677
    invoke-static/range {p0 .. p1}, LX/Ddu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v42, v2

    goto/16 :goto_1

    .line 2026678
    :cond_a
    const-string v6, "contact"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2026679
    invoke-static/range {p0 .. p1}, LX/Dg9;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v41, v2

    goto/16 :goto_1

    .line 2026680
    :cond_b
    const-string v6, "description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2026681
    invoke-static/range {p0 .. p1}, LX/Df2;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 2026682
    :cond_c
    const-string v6, "hidden_authors"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2026683
    invoke-static/range {p0 .. p1}, LX/DfB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v39, v2

    goto/16 :goto_1

    .line 2026684
    :cond_d
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2026685
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v38, v2

    goto/16 :goto_1

    .line 2026686
    :cond_e
    const-string v6, "item"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 2026687
    invoke-static/range {p0 .. p1}, LX/DeY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 2026688
    :cond_f
    const-string v6, "item_description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 2026689
    invoke-static/range {p0 .. p1}, LX/De3;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v36, v2

    goto/16 :goto_1

    .line 2026690
    :cond_10
    const-string v6, "item_description_icon"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 2026691
    invoke-static/range {p0 .. p1}, LX/De2;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 2026692
    :cond_11
    const-string v6, "item_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 2026693
    invoke-static/range {p0 .. p1}, LX/DfG;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 2026694
    :cond_12
    const-string v6, "item_thread"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 2026695
    invoke-static/range {p0 .. p1}, LX/De6;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 2026696
    :cond_13
    const-string v6, "item_user"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 2026697
    invoke-static/range {p0 .. p1}, LX/De7;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 2026698
    :cond_14
    const-string v6, "link"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 2026699
    invoke-static/range {p0 .. p1}, LX/5Sn;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 2026700
    :cond_15
    const-string v6, "mcs_item_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 2026701
    invoke-static/range {p0 .. p1}, LX/De8;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 2026702
    :cond_16
    const-string v6, "messenger_room_suggestion"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 2026703
    invoke-static/range {p0 .. p1}, LX/Dnp;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 2026704
    :cond_17
    const-string v6, "nux_messages"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 2026705
    invoke-static/range {p0 .. p1}, LX/DfD;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 2026706
    :cond_18
    const-string v6, "offline_score"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 2026707
    const/4 v2, 0x1

    .line 2026708
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 2026709
    :cond_19
    const-string v6, "online_score"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 2026710
    const/4 v2, 0x1

    .line 2026711
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v26, v6

    goto/16 :goto_1

    .line 2026712
    :cond_1a
    const-string v6, "page"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 2026713
    invoke-static/range {p0 .. p1}, LX/DfH;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 2026714
    :cond_1b
    const-string v6, "pymm_icon_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 2026715
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLMessengerPYMMIconType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerPYMMIconType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 2026716
    :cond_1c
    const-string v6, "reason"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 2026717
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInbox2MessageThreadReason;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 2026718
    :cond_1d
    const-string v6, "section"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 2026719
    invoke-static/range {p0 .. p1}, LX/DfC;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 2026720
    :cond_1e
    const-string v6, "show_presence"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1f

    .line 2026721
    const/4 v2, 0x1

    .line 2026722
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v10, v2

    move/from16 v21, v6

    goto/16 :goto_1

    .line 2026723
    :cond_1f
    const-string v6, "snippet"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_20

    .line 2026724
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 2026725
    :cond_20
    const-string v6, "story_feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_21

    .line 2026726
    invoke-static/range {p0 .. p1}, LX/Df8;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 2026727
    :cond_21
    const-string v6, "summary"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_22

    .line 2026728
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 2026729
    :cond_22
    const-string v6, "thread"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_23

    .line 2026730
    invoke-static/range {p0 .. p1}, LX/DfO;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 2026731
    :cond_23
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_24

    .line 2026732
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 2026733
    :cond_24
    const-string v6, "total_count"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_25

    .line 2026734
    const/4 v2, 0x1

    .line 2026735
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v9, v2

    move v15, v6

    goto/16 :goto_1

    .line 2026736
    :cond_25
    const-string v6, "unread_count"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_26

    .line 2026737
    const/4 v2, 0x1

    .line 2026738
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v14, v6

    goto/16 :goto_1

    .line 2026739
    :cond_26
    const-string v6, "user"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_27

    .line 2026740
    invoke-static/range {p0 .. p1}, LX/DfT;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2026741
    :cond_27
    const-string v6, "user_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 2026742
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2026743
    :cond_28
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2026744
    :cond_29
    const/16 v2, 0x27

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2026745
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026746
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026747
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026748
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026749
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026750
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026751
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026752
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026753
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026754
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026755
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026756
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026757
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026758
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026759
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026760
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026761
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026762
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026763
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026764
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026765
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026766
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026767
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026768
    if-eqz v3, :cond_2a

    .line 2026769
    const/16 v3, 0x17

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2026770
    :cond_2a
    if-eqz v11, :cond_2b

    .line 2026771
    const/16 v3, 0x18

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2026772
    :cond_2b
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026773
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026774
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026775
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026776
    if-eqz v10, :cond_2c

    .line 2026777
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2026778
    :cond_2c
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026779
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026780
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026781
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026782
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2026783
    if-eqz v9, :cond_2d

    .line 2026784
    const/16 v2, 0x23

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, LX/186;->a(III)V

    .line 2026785
    :cond_2d
    if-eqz v8, :cond_2e

    .line 2026786
    const/16 v2, 0x24

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 2026787
    :cond_2e
    const/16 v2, 0x25

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2026788
    const/16 v2, 0x26

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2026789
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_2f
    move/from16 v48, v45

    move/from16 v49, v46

    move/from16 v50, v47

    move/from16 v45, v42

    move/from16 v46, v43

    move/from16 v47, v44

    move/from16 v42, v39

    move/from16 v43, v40

    move/from16 v44, v41

    move/from16 v39, v36

    move/from16 v40, v37

    move/from16 v41, v38

    move/from16 v36, v33

    move/from16 v37, v34

    move/from16 v38, v35

    move/from16 v33, v30

    move/from16 v34, v31

    move/from16 v35, v32

    move/from16 v30, v27

    move/from16 v31, v28

    move/from16 v32, v29

    move/from16 v28, v21

    move/from16 v29, v26

    move-wide/from16 v26, v22

    move/from16 v21, v16

    move/from16 v22, v17

    move/from16 v23, v18

    move/from16 v16, v11

    move/from16 v17, v12

    move/from16 v18, v13

    move v11, v5

    move v13, v8

    move v12, v7

    move v8, v2

    move/from16 v51, v14

    move v14, v9

    move v9, v3

    move v3, v6

    move/from16 v52, v10

    move v10, v4

    move-wide/from16 v4, v24

    move/from16 v24, v19

    move/from16 v25, v20

    move/from16 v19, v51

    move/from16 v20, v15

    move/from16 v15, v52

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const/16 v7, 0x1b

    const/16 v6, 0x1a

    const/4 v2, 0x6

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 2026449
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2026450
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2026451
    if-eqz v0, :cond_0

    .line 2026452
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026453
    invoke-static {p0, p1, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2026454
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026455
    if-eqz v0, :cond_1

    .line 2026456
    const-string v1, "action_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026457
    invoke-static {p0, v0, p2}, LX/Df1;->a(LX/15i;ILX/0nX;)V

    .line 2026458
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026459
    if-eqz v0, :cond_2

    .line 2026460
    const-string v1, "ad_client_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026461
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026462
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026463
    if-eqz v0, :cond_3

    .line 2026464
    const-string v1, "ad_display_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026465
    invoke-static {p0, v0, p2, p3}, LX/CJx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026466
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026467
    if-eqz v0, :cond_4

    .line 2026468
    const-string v1, "ad_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026469
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026470
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026471
    if-eqz v0, :cond_5

    .line 2026472
    const-string v1, "ad_preference_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026473
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026474
    :cond_5
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2026475
    if-eqz v0, :cond_6

    .line 2026476
    const-string v0, "ad_properties"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026477
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2026478
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026479
    if-eqz v0, :cond_7

    .line 2026480
    const-string v1, "call_to_action_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026481
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026482
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026483
    if-eqz v0, :cond_8

    .line 2026484
    const-string v1, "candidate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026485
    invoke-static {p0, v0, p2, p3}, LX/Ddu;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026486
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026487
    if-eqz v0, :cond_9

    .line 2026488
    const-string v1, "contact"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026489
    invoke-static {p0, v0, p2, p3}, LX/Dg9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026490
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026491
    if-eqz v0, :cond_a

    .line 2026492
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026493
    invoke-static {p0, v0, p2}, LX/Df2;->a(LX/15i;ILX/0nX;)V

    .line 2026494
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026495
    if-eqz v0, :cond_b

    .line 2026496
    const-string v1, "hidden_authors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026497
    invoke-static {p0, v0, p2, p3}, LX/DfB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026498
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026499
    if-eqz v0, :cond_c

    .line 2026500
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026501
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026502
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026503
    if-eqz v0, :cond_d

    .line 2026504
    const-string v1, "item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026505
    invoke-static {p0, v0, p2, p3}, LX/DeY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026506
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026507
    if-eqz v0, :cond_e

    .line 2026508
    const-string v1, "item_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026509
    invoke-static {p0, v0, p2}, LX/De3;->a(LX/15i;ILX/0nX;)V

    .line 2026510
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026511
    if-eqz v0, :cond_f

    .line 2026512
    const-string v1, "item_description_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026513
    invoke-static {p0, v0, p2}, LX/De2;->a(LX/15i;ILX/0nX;)V

    .line 2026514
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026515
    if-eqz v0, :cond_10

    .line 2026516
    const-string v1, "item_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026517
    invoke-static {p0, v0, p2}, LX/DfG;->a(LX/15i;ILX/0nX;)V

    .line 2026518
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026519
    if-eqz v0, :cond_11

    .line 2026520
    const-string v1, "item_thread"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026521
    invoke-static {p0, v0, p2, p3}, LX/De6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026522
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026523
    if-eqz v0, :cond_12

    .line 2026524
    const-string v1, "item_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026525
    invoke-static {p0, v0, p2}, LX/De7;->a(LX/15i;ILX/0nX;)V

    .line 2026526
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026527
    if-eqz v0, :cond_13

    .line 2026528
    const-string v1, "link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026529
    invoke-static {p0, v0, p2, p3}, LX/5Sn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026530
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026531
    if-eqz v0, :cond_14

    .line 2026532
    const-string v1, "mcs_item_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026533
    invoke-static {p0, v0, p2}, LX/De8;->a(LX/15i;ILX/0nX;)V

    .line 2026534
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026535
    if-eqz v0, :cond_15

    .line 2026536
    const-string v1, "messenger_room_suggestion"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026537
    invoke-static {p0, v0, p2, p3}, LX/Dnp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026538
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026539
    if-eqz v0, :cond_16

    .line 2026540
    const-string v1, "nux_messages"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026541
    invoke-static {p0, v0, p2, p3}, LX/DfD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026542
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2026543
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_17

    .line 2026544
    const-string v2, "offline_score"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026545
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2026546
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2026547
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_18

    .line 2026548
    const-string v2, "online_score"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026549
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2026550
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026551
    if-eqz v0, :cond_19

    .line 2026552
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026553
    invoke-static {p0, v0, p2, p3}, LX/DfH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026554
    :cond_19
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 2026555
    if-eqz v0, :cond_1a

    .line 2026556
    const-string v0, "pymm_icon_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026557
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026558
    :cond_1a
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 2026559
    if-eqz v0, :cond_1b

    .line 2026560
    const-string v0, "reason"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026561
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026562
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026563
    if-eqz v0, :cond_1c

    .line 2026564
    const-string v1, "section"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026565
    invoke-static {p0, v0, p2, p3}, LX/DfC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026566
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2026567
    if-eqz v0, :cond_1d

    .line 2026568
    const-string v1, "show_presence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026569
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2026570
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026571
    if-eqz v0, :cond_1e

    .line 2026572
    const-string v1, "snippet"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026573
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026574
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026575
    if-eqz v0, :cond_1f

    .line 2026576
    const-string v1, "story_feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026577
    invoke-static {p0, v0, p2, p3}, LX/Df8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026578
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026579
    if-eqz v0, :cond_20

    .line 2026580
    const-string v1, "summary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026581
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026582
    :cond_20
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026583
    if-eqz v0, :cond_21

    .line 2026584
    const-string v1, "thread"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026585
    invoke-static {p0, v0, p2, p3}, LX/DfO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026586
    :cond_21
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026587
    if-eqz v0, :cond_22

    .line 2026588
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026589
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026590
    :cond_22
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2026591
    if-eqz v0, :cond_23

    .line 2026592
    const-string v1, "total_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026593
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2026594
    :cond_23
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2026595
    if-eqz v0, :cond_24

    .line 2026596
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026597
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2026598
    :cond_24
    const/16 v0, 0x25

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026599
    if-eqz v0, :cond_25

    .line 2026600
    const-string v1, "user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026601
    invoke-static {p0, v0, p2, p3}, LX/DfT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026602
    :cond_25
    const/16 v0, 0x26

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026603
    if-eqz v0, :cond_26

    .line 2026604
    const-string v1, "user_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026605
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026606
    :cond_26
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2026607
    return-void
.end method
