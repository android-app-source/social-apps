.class public final LX/DsO;
.super LX/CSB;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/widget/NotificationsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/widget/NotificationsFragment;)V
    .locals 0

    .prologue
    .line 2050398
    iput-object p1, p0, LX/DsO;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    invoke-direct {p0}, LX/CSB;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2050399
    check-cast p1, LX/CSA;

    .line 2050400
    iget-object v0, p0, LX/DsO;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    if-nez v0, :cond_1

    .line 2050401
    :cond_0
    :goto_0
    return-void

    .line 2050402
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, LX/DsO;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v1, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {v1}, LX/DsH;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2050403
    iget-object v1, p0, LX/DsO;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v1, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {v1, v0}, LX/DsH;->b(I)LX/2nq;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/DsO;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v1, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {v1, v0}, LX/DsH;->b(I)LX/2nq;

    move-result-object v1

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/DsO;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v1, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {v1, v0}, LX/DsH;->b(I)LX/2nq;

    move-result-object v1

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/DsO;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v1, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    invoke-virtual {v1, v0}, LX/DsH;->b(I)LX/2nq;

    move-result-object v1

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, LX/CSA;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2050404
    iget-object v1, p0, LX/DsO;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    iget-object v1, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->T:LX/DsH;

    iget-object v2, p1, LX/CSA;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/DsH;->a(ILjava/lang/String;)V

    .line 2050405
    iget-object v1, p0, LX/DsO;->a:Lcom/facebook/notifications/widget/NotificationsFragment;

    .line 2050406
    new-instance v3, Landroid/text/SpannableString;

    iget-object v2, p1, LX/CSA;->d:Ljava/lang/String;

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2050407
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    iget-object v4, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->Y:Landroid/content/Context;

    const v5, 0x7f0a00d5

    invoke-static {v4, v5}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v4

    invoke-direct {v2, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v4, 0x0

    iget-object v5, p1, LX/CSA;->d:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 p0, 0x11

    invoke-interface {v3, v2, v4, v5, p0}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2050408
    iget-object v2, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->V:LX/3TO;

    check-cast v2, Landroid/view/View;

    iget-object v4, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->R:LX/1rx;

    invoke-virtual {v4}, LX/1rx;->h()I

    move-result v4

    invoke-static {v2, v3, v4}, LX/3oW;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;

    move-result-object v2

    .line 2050409
    iget-object v3, p1, LX/CSA;->c:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2050410
    iget-object v3, v1, Lcom/facebook/notifications/widget/NotificationsFragment;->Y:Landroid/content/Context;

    const v4, 0x7f0a00d2

    invoke-static {v3, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v2, v3}, LX/3oW;->a(I)LX/3oW;

    move-result-object v3

    const v4, 0x7f08116d

    new-instance v5, LX/DsM;

    invoke-direct {v5, v1, v0, p1}, LX/DsM;-><init>(Lcom/facebook/notifications/widget/NotificationsFragment;ILX/CSA;)V

    invoke-virtual {v3, v4, v5}, LX/3oW;->a(ILandroid/view/View$OnClickListener;)LX/3oW;

    .line 2050411
    :cond_2
    iget-object v3, v2, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    move-object v3, v3

    .line 2050412
    const v4, 0x7f0a00f7

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2050413
    invoke-virtual {v2}, LX/3oW;->b()V

    .line 2050414
    goto/16 :goto_0

    .line 2050415
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method
