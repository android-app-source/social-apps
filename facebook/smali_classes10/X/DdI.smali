.class public final LX/DdI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/messaging/model/messages/Message;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2019424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 5

    .prologue
    .line 2019425
    check-cast p1, Lcom/facebook/messaging/model/messages/Message;

    check-cast p2, Lcom/facebook/messaging/model/messages/Message;

    .line 2019426
    invoke-static {p1}, LX/2Mk;->b(Lcom/facebook/messaging/model/messages/Message;)J

    move-result-wide v0

    .line 2019427
    invoke-static {p2}, LX/2Mk;->b(Lcom/facebook/messaging/model/messages/Message;)J

    move-result-wide v2

    .line 2019428
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 2019429
    const/4 v0, -0x1

    .line 2019430
    :goto_0
    return v0

    .line 2019431
    :cond_0
    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 2019432
    const/4 v0, 0x1

    goto :goto_0

    .line 2019433
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
