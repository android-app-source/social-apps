.class public LX/DEt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/DEt;


# instance fields
.field public final a:LX/0iA;

.field public final b:LX/2hf;


# direct methods
.method public constructor <init>(LX/0iA;LX/2hf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1977475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1977476
    iput-object p1, p0, LX/DEt;->a:LX/0iA;

    .line 1977477
    iput-object p2, p0, LX/DEt;->b:LX/2hf;

    .line 1977478
    return-void
.end method

.method public static a(LX/0QB;)LX/DEt;
    .locals 5

    .prologue
    .line 1977479
    sget-object v0, LX/DEt;->c:LX/DEt;

    if-nez v0, :cond_1

    .line 1977480
    const-class v1, LX/DEt;

    monitor-enter v1

    .line 1977481
    :try_start_0
    sget-object v0, LX/DEt;->c:LX/DEt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1977482
    if-eqz v2, :cond_0

    .line 1977483
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1977484
    new-instance p0, LX/DEt;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-static {v0}, LX/2hf;->a(LX/0QB;)LX/2hf;

    move-result-object v4

    check-cast v4, LX/2hf;

    invoke-direct {p0, v3, v4}, LX/DEt;-><init>(LX/0iA;LX/2hf;)V

    .line 1977485
    move-object v0, p0

    .line 1977486
    sput-object v0, LX/DEt;->c:LX/DEt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1977487
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1977488
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1977489
    :cond_1
    sget-object v0, LX/DEt;->c:LX/DEt;

    return-object v0

    .line 1977490
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1977491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
