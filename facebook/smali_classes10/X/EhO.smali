.class public final LX/EhO;
.super LX/2lH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2lH",
        "<",
        "LX/EhT;",
        "Lcom/facebook/bookmark/model/Bookmark;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)V
    .locals 2

    .prologue
    .line 2158369
    iput-object p1, p0, LX/EhO;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    .line 2158370
    const v0, 0x7f03024d

    iget-object v1, p1, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b:Landroid/view/LayoutInflater;

    invoke-direct {p0, p2, v0, p3, v1}, LX/2lH;-><init>(LX/2lb;ILjava/lang/Object;Landroid/view/LayoutInflater;)V

    .line 2158371
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2158372
    move-object v0, p1

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2158373
    const v1, 0x7f0e01ee

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2158374
    new-instance v0, LX/EhT;

    invoke-direct {v0, p1}, LX/EhT;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2158375
    check-cast p1, LX/EhT;

    const/4 v1, 0x0

    .line 2158376
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 2158377
    :cond_0
    :goto_0
    return-void

    .line 2158378
    :cond_1
    iget-object v2, p0, LX/EhO;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    invoke-virtual {v2, v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a(Lcom/facebook/bookmark/model/Bookmark;)I

    move-result v0

    .line 2158379
    if-lez v0, :cond_2

    .line 2158380
    iget-object v2, p1, LX/EhT;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2158381
    iget-object v1, p1, LX/EhT;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2158382
    :goto_1
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v0, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v2, 0x8bb78869L

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 2158383
    iget-object v0, p1, LX/EhT;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f0d01d9

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setId(I)V

    .line 2158384
    :goto_2
    iget-object v1, p1, LX/EhT;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2158385
    iget-object v1, p0, LX/EhO;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v2, p1, LX/EhT;->b:Landroid/widget/TextView;

    invoke-static {v1, v0, v2}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a$redex0(Lcom/facebook/bookmark/ui/BaseViewItemFactory;Lcom/facebook/bookmark/model/Bookmark;Landroid/widget/TextView;)V

    .line 2158386
    iget-object v1, p0, LX/EhO;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v2, p1, LX/EhT;->b:Landroid/widget/TextView;

    invoke-static {v1, v0, v2}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b$redex0(Lcom/facebook/bookmark/ui/BaseViewItemFactory;Lcom/facebook/bookmark/model/Bookmark;Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    .line 2158387
    iget-object v1, p1, LX/EhT;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2158388
    iget-object v0, p0, LX/EhO;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    invoke-static {v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a$redex0(Lcom/facebook/bookmark/ui/BaseViewItemFactory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2158389
    iget-object v0, p1, LX/EhT;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-static {v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b(Lcom/facebook/fbui/widget/contentview/ContentView;)V

    goto :goto_0

    .line 2158390
    :cond_2
    iget-object v2, p1, LX/EhT;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2158391
    iget-object v0, p1, LX/EhT;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 2158392
    :cond_3
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    .line 2158393
    :cond_4
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v0, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    const-wide v2, 0x378ae22b932d7L

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 2158394
    iget-object v0, p1, LX/EhT;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f0d01da

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setId(I)V

    goto :goto_2

    .line 2158395
    :cond_5
    iget-object v1, p1, LX/EhT;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v2, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    long-to-int v0, v2

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setId(I)V

    goto :goto_2
.end method
