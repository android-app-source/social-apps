.class public final LX/DdO;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/service/model/MarkThreadsParams;

.field public final synthetic b:LX/DdP;


# direct methods
.method public constructor <init>(LX/DdP;Lcom/facebook/messaging/service/model/MarkThreadsParams;)V
    .locals 0

    .prologue
    .line 2019463
    iput-object p1, p0, LX/DdO;->b:LX/DdP;

    iput-object p2, p0, LX/DdO;->a:Lcom/facebook/messaging/service/model/MarkThreadsParams;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2019464
    iget-object v0, p0, LX/DdO;->b:LX/DdP;

    iget-object v1, p0, LX/DdO;->a:Lcom/facebook/messaging/service/model/MarkThreadsParams;

    .line 2019465
    iget-object p0, v0, LX/DdP;->c:Ljava/util/Set;

    iget-object p1, v1, Lcom/facebook/messaging/service/model/MarkThreadsParams;->d:LX/0Px;

    invoke-interface {p0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 2019466
    iget-object p0, v0, LX/DdP;->a:LX/2Ow;

    iget-object p1, v1, Lcom/facebook/messaging/service/model/MarkThreadsParams;->d:LX/0Px;

    invoke-virtual {p0, p1}, LX/2Ow;->a(LX/0Px;)V

    .line 2019467
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2019468
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2019469
    iget-object v0, p0, LX/DdO;->b:LX/DdP;

    iget-object v1, p0, LX/DdO;->a:Lcom/facebook/messaging/service/model/MarkThreadsParams;

    .line 2019470
    iget-object p0, v0, LX/DdP;->c:Ljava/util/Set;

    iget-object p1, v1, Lcom/facebook/messaging/service/model/MarkThreadsParams;->d:LX/0Px;

    invoke-interface {p0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 2019471
    return-void
.end method
