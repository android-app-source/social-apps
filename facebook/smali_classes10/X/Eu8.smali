.class public final LX/Eu8;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Eu9;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2179105
    invoke-static {}, LX/Eu9;->q()LX/Eu9;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2179106
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2179104
    const-string v0, "FriendListButtonText"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2179089
    if-ne p0, p1, :cond_1

    .line 2179090
    :cond_0
    :goto_0
    return v0

    .line 2179091
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2179092
    goto :goto_0

    .line 2179093
    :cond_3
    check-cast p1, LX/Eu8;

    .line 2179094
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2179095
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2179096
    if-eq v2, v3, :cond_0

    .line 2179097
    iget-object v2, p0, LX/Eu8;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Eu8;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Eu8;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2179098
    goto :goto_0

    .line 2179099
    :cond_5
    iget-object v2, p1, LX/Eu8;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 2179100
    :cond_6
    iget v2, p0, LX/Eu8;->b:I

    iget v3, p1, LX/Eu8;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2179101
    goto :goto_0

    .line 2179102
    :cond_7
    iget v2, p0, LX/Eu8;->c:I

    iget v3, p1, LX/Eu8;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2179103
    goto :goto_0
.end method
