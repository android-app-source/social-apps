.class public LX/Duk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/01T;

.field private b:Landroid/media/AudioManager;

.field private c:LX/0Uo;

.field private d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public e:LX/3RX;

.field public f:LX/3RZ;


# direct methods
.method public constructor <init>(LX/01T;Landroid/media/AudioManager;LX/0Uo;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3RX;LX/3RZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2057584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057585
    iput-object p1, p0, LX/Duk;->a:LX/01T;

    .line 2057586
    iput-object p2, p0, LX/Duk;->b:Landroid/media/AudioManager;

    .line 2057587
    iput-object p3, p0, LX/Duk;->c:LX/0Uo;

    .line 2057588
    iput-object p4, p0, LX/Duk;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2057589
    iput-object p5, p0, LX/Duk;->e:LX/3RX;

    .line 2057590
    iput-object p6, p0, LX/Duk;->f:LX/3RZ;

    .line 2057591
    return-void
.end method

.method public static a(LX/0QB;)LX/Duk;
    .locals 1

    .prologue
    .line 2057592
    invoke-static {p0}, LX/Duk;->b(LX/0QB;)LX/Duk;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Duk;
    .locals 7

    .prologue
    .line 2057593
    new-instance v0, LX/Duk;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v1

    check-cast v1, LX/01T;

    invoke-static {p0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v3

    check-cast v3, LX/0Uo;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/8iw;->b(LX/0QB;)LX/8iw;

    move-result-object v5

    check-cast v5, LX/3RX;

    invoke-static {p0}, LX/3RZ;->b(LX/0QB;)LX/3RZ;

    move-result-object v6

    check-cast v6, LX/3RZ;

    invoke-direct/range {v0 .. v6}, LX/Duk;-><init>(LX/01T;Landroid/media/AudioManager;LX/0Uo;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3RX;LX/3RZ;)V

    .line 2057594
    return-object v0
.end method

.method public static d(LX/Duk;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2057595
    iget-object v1, p0, LX/Duk;->a:LX/01T;

    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/Duk;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Duj;->c:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Duk;->c:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Duk;->b:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Duk;->b:Landroid/media/AudioManager;

    iget-object v2, p0, LX/Duk;->f:LX/3RZ;

    invoke-virtual {v2}, LX/3RZ;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
