.class public final enum LX/Dhy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dhy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dhy;

.field public static final enum CONFERENCE:LX/Dhy;

.field public static final enum P2P:LX/Dhy;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2031516
    new-instance v0, LX/Dhy;

    const-string v1, "P2P"

    invoke-direct {v0, v1, v2}, LX/Dhy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhy;->P2P:LX/Dhy;

    .line 2031517
    new-instance v0, LX/Dhy;

    const-string v1, "CONFERENCE"

    invoke-direct {v0, v1, v3}, LX/Dhy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dhy;->CONFERENCE:LX/Dhy;

    .line 2031518
    const/4 v0, 0x2

    new-array v0, v0, [LX/Dhy;

    sget-object v1, LX/Dhy;->P2P:LX/Dhy;

    aput-object v1, v0, v2

    sget-object v1, LX/Dhy;->CONFERENCE:LX/Dhy;

    aput-object v1, v0, v3

    sput-object v0, LX/Dhy;->$VALUES:[LX/Dhy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2031519
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dhy;
    .locals 1

    .prologue
    .line 2031520
    const-class v0, LX/Dhy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dhy;

    return-object v0
.end method

.method public static values()[LX/Dhy;
    .locals 1

    .prologue
    .line 2031521
    sget-object v0, LX/Dhy;->$VALUES:[LX/Dhy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dhy;

    return-object v0
.end method
