.class public LX/CsW;
.super LX/CsV;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:LX/Ckw;

.field public f:LX/ClK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1942884
    invoke-direct {p0, p1}, LX/CsV;-><init>(Landroid/content/Context;)V

    .line 1942885
    const-class p1, LX/CsW;

    invoke-static {p1, p0}, LX/CsW;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1942886
    new-instance p1, LX/Csr;

    invoke-direct {p1, p0}, LX/Csr;-><init>(LX/CsW;)V

    invoke-virtual {p0, p1}, LX/CsV;->a(LX/Csq;)V

    .line 1942887
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CsW;

    invoke-static {p0}, LX/ClK;->a(LX/0QB;)LX/ClK;

    move-result-object p0

    check-cast p0, LX/ClK;

    iput-object p0, p1, LX/CsW;->f:LX/ClK;

    return-void
.end method


# virtual methods
.method public getBlockId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1942883
    iget-object v0, p0, LX/CsW;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1942888
    invoke-super {p0, p1, p2, p3}, LX/CsV;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1942889
    iget-object v0, p0, LX/CsW;->f:LX/ClK;

    iget-object v1, p0, LX/CsW;->c:LX/Ckw;

    iget-object v2, p0, LX/CsW;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/ClK;->b(LX/Ckw;Ljava/lang/String;)V

    .line 1942890
    return-void
.end method

.method public final loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1942880
    invoke-super/range {p0 .. p5}, LX/CsV;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1942881
    iget-object v0, p0, LX/CsW;->f:LX/ClK;

    iget-object v1, p0, LX/CsW;->c:LX/Ckw;

    iget-object v2, p0, LX/CsW;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/ClK;->b(LX/Ckw;Ljava/lang/String;)V

    .line 1942882
    return-void
.end method

.method public final loadUrl(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1942877
    invoke-super {p0, p1}, LX/CsV;->loadUrl(Ljava/lang/String;)V

    .line 1942878
    iget-object v0, p0, LX/CsW;->f:LX/ClK;

    iget-object v1, p0, LX/CsW;->c:LX/Ckw;

    iget-object v2, p0, LX/CsW;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/ClK;->b(LX/Ckw;Ljava/lang/String;)V

    .line 1942879
    return-void
.end method

.method public final loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1942874
    invoke-super {p0, p1, p2}, LX/CsV;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 1942875
    iget-object v0, p0, LX/CsW;->f:LX/ClK;

    iget-object v1, p0, LX/CsW;->c:LX/Ckw;

    iget-object v2, p0, LX/CsW;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/ClK;->b(LX/Ckw;Ljava/lang/String;)V

    .line 1942876
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1942865
    invoke-super {p0, p1}, LX/CsV;->onDraw(Landroid/graphics/Canvas;)V

    .line 1942866
    iget-boolean v0, p0, LX/CsW;->b:Z

    if-eqz v0, :cond_1

    .line 1942867
    iget-object v0, p0, LX/CsW;->f:LX/ClK;

    iget-object v1, p0, LX/CsW;->c:LX/Ckw;

    iget-object v2, p0, LX/CsW;->a:Ljava/lang/String;

    .line 1942868
    iget-object v3, v0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 1942869
    if-eqz v3, :cond_0

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1942870
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CsW;->b:Z

    .line 1942871
    :cond_1
    return-void

    .line 1942872
    :cond_2
    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ClJ;

    .line 1942873
    iget-object v4, v0, LX/ClK;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v5

    iput-wide v5, v3, LX/ClJ;->j:J

    goto :goto_0
.end method
