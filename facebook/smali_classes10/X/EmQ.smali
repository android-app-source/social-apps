.class public LX/EmQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/EmQ;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/EmO;

.field private final c:Landroid/app/DownloadManager;

.field private final d:LX/0V8;

.field private final e:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final f:LX/0en;

.field public final g:Ljava/util/concurrent/ExecutorService;

.field public final h:LX/EmX;

.field private final i:LX/EmR;

.field private final j:LX/0Zb;

.field private k:LX/0Yd;

.field public l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "LX/EmN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/app/DownloadManager;LX/EmO;LX/0V8;LX/0en;Ljava/util/concurrent/ExecutorService;LX/EmX;LX/EmR;LX/0Zb;)V
    .locals 1
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2165892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2165893
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/EmQ;->l:Ljava/util/HashMap;

    .line 2165894
    iput-object p1, p0, LX/EmQ;->a:Landroid/content/Context;

    .line 2165895
    iput-object p3, p0, LX/EmQ;->c:Landroid/app/DownloadManager;

    .line 2165896
    iput-object p4, p0, LX/EmQ;->b:LX/EmO;

    .line 2165897
    iput-object p5, p0, LX/EmQ;->d:LX/0V8;

    .line 2165898
    iput-object p2, p0, LX/EmQ;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2165899
    iput-object p6, p0, LX/EmQ;->f:LX/0en;

    .line 2165900
    iput-object p7, p0, LX/EmQ;->g:Ljava/util/concurrent/ExecutorService;

    .line 2165901
    iput-object p8, p0, LX/EmQ;->h:LX/EmX;

    .line 2165902
    iput-object p9, p0, LX/EmQ;->i:LX/EmR;

    .line 2165903
    iput-object p10, p0, LX/EmQ;->j:LX/0Zb;

    .line 2165904
    return-void
.end method

.method public static a(LX/0QB;)LX/EmQ;
    .locals 14

    .prologue
    .line 2165875
    sget-object v0, LX/EmQ;->m:LX/EmQ;

    if-nez v0, :cond_1

    .line 2165876
    const-class v1, LX/EmQ;

    monitor-enter v1

    .line 2165877
    :try_start_0
    sget-object v0, LX/EmQ;->m:LX/EmQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2165878
    if-eqz v2, :cond_0

    .line 2165879
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2165880
    new-instance v3, LX/EmQ;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v0}, LX/2S1;->b(LX/0QB;)Landroid/app/DownloadManager;

    move-result-object v6

    check-cast v6, Landroid/app/DownloadManager;

    invoke-static {v0}, LX/EmO;->a(LX/0QB;)LX/EmO;

    move-result-object v7

    check-cast v7, LX/EmO;

    invoke-static {v0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v8

    check-cast v8, LX/0V8;

    invoke-static {v0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v9

    check-cast v9, LX/0en;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/EmX;->a(LX/0QB;)LX/EmX;

    move-result-object v11

    check-cast v11, LX/EmX;

    .line 2165881
    new-instance v12, LX/EmR;

    invoke-direct {v12}, LX/EmR;-><init>()V

    .line 2165882
    move-object v12, v12

    .line 2165883
    move-object v12, v12

    .line 2165884
    check-cast v12, LX/EmR;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v13

    check-cast v13, LX/0Zb;

    invoke-direct/range {v3 .. v13}, LX/EmQ;-><init>(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/app/DownloadManager;LX/EmO;LX/0V8;LX/0en;Ljava/util/concurrent/ExecutorService;LX/EmX;LX/EmR;LX/0Zb;)V

    .line 2165885
    move-object v0, v3

    .line 2165886
    sput-object v0, LX/EmQ;->m:LX/EmQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2165887
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2165888
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2165889
    :cond_1
    sget-object v0, LX/EmQ;->m:LX/EmQ;

    return-object v0

    .line 2165890
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2165891
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/database/Cursor;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 2165841
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2165842
    const-string v1, "status"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 2165843
    sparse-switch v1, :sswitch_data_0

    .line 2165844
    const-string v2, "status"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UNKNOWN("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2165845
    :goto_0
    const-string v1, "reason"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 2165846
    const-string v2, "status_reason"

    .line 2165847
    sparse-switch v1, :sswitch_data_1

    .line 2165848
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UNKNOWN("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    move-object v1, v3

    .line 2165849
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2165850
    const-string v1, "last_modified_timestamp"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2165851
    const-string v1, "last_modified"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2165852
    const-string v1, "bytes_so_far"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2165853
    const-string v1, "bytes_so_far"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2165854
    const-string v1, "total_size"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2165855
    const-string v1, "total_bytes"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2165856
    return-object v0

    .line 2165857
    :sswitch_0
    const-string v1, "status"

    const-string v2, "STATUS_FAILED"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2165858
    :sswitch_1
    const-string v1, "status"

    const-string v2, "STATUS_PAUSED"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2165859
    :sswitch_2
    const-string v1, "status"

    const-string v2, "STATUS_PENDING"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2165860
    :sswitch_3
    const-string v1, "status"

    const-string v2, "STATUS_RUNNING"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2165861
    :sswitch_4
    const-string v1, "status"

    const-string v2, "STATUS_SUCCESSFUL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2165862
    :sswitch_5
    const-string v3, "ERROR_CANNOT_RESUME"

    goto :goto_1

    .line 2165863
    :sswitch_6
    const-string v3, "ERROR_DEVICE_NOT_FOUND"

    goto :goto_1

    .line 2165864
    :sswitch_7
    const-string v3, "ERROR_FILE_ALREADY_EXISTS"

    goto :goto_1

    .line 2165865
    :sswitch_8
    const-string v3, "ERROR_FILE_ERROR"

    goto :goto_1

    .line 2165866
    :sswitch_9
    const-string v3, "ERROR_HTTP_DATA_ERROR"

    goto :goto_1

    .line 2165867
    :sswitch_a
    const-string v3, "ERROR_INSUFFICIENT_SPACE"

    goto :goto_1

    .line 2165868
    :sswitch_b
    const-string v3, "ERROR_TOO_MANY_REDIRECTS"

    goto :goto_1

    .line 2165869
    :sswitch_c
    const-string v3, "ERROR_UNHANDLED_HTTP_CODE"

    goto :goto_1

    .line 2165870
    :sswitch_d
    const-string v3, "ERROR_UNKNOWN"

    goto/16 :goto_1

    .line 2165871
    :sswitch_e
    const-string v3, "PAUSED_QUEUED_FOR_WIFI"

    goto/16 :goto_1

    .line 2165872
    :sswitch_f
    const-string v3, "PAUSED_UNKNOWN"

    goto/16 :goto_1

    .line 2165873
    :sswitch_10
    const-string v3, "PAUSED_WAITING_FOR_NETWORK"

    goto/16 :goto_1

    .line 2165874
    :sswitch_11
    const-string v3, "PAUSED_WAITING_TO_RETRY"

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x4 -> :sswitch_1
        0x8 -> :sswitch_4
        0x10 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_10
        0x3 -> :sswitch_e
        0x4 -> :sswitch_f
        0x3e8 -> :sswitch_d
        0x3e9 -> :sswitch_8
        0x3ea -> :sswitch_c
        0x3ec -> :sswitch_9
        0x3ed -> :sswitch_b
        0x3ee -> :sswitch_a
        0x3ef -> :sswitch_6
        0x3f0 -> :sswitch_5
        0x3f1 -> :sswitch_7
    .end sparse-switch
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 2165835
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2165836
    const-string v1, "com_facebook_downloader"

    .line 2165837
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2165838
    invoke-virtual {v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2165839
    iget-object v1, p0, LX/EmQ;->j:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2165840
    return-void
.end method

.method private a(JLandroid/app/DownloadManager$Request;LX/EmM;Ljava/lang/String;Z)Z
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2165819
    iget-object v0, p0, LX/EmQ;->d:LX/0V8;

    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v0, v1}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v0

    .line 2165820
    const-wide/16 v2, 0x2

    mul-long/2addr v2, p1

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 2165821
    iget-object v0, p0, LX/EmQ;->h:LX/EmX;

    new-instance v1, LX/EmW;

    sget-object v4, LX/EmV;->ERROR_NO_FREE_SPACE_TO_DOWNLOAD:LX/EmV;

    const/4 v5, 0x0

    move-object v2, p4

    move-object v3, p5

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, LX/EmW;-><init>(LX/EmM;Ljava/lang/String;LX/EmV;Ljava/lang/Exception;J)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2165822
    const/4 v0, 0x0

    .line 2165823
    :goto_0
    return v0

    .line 2165824
    :cond_0
    iget-object v2, p0, LX/EmQ;->i:LX/EmR;

    invoke-virtual {v2, p4}, LX/EmR;->a(LX/EmM;)J

    move-result-wide v2

    .line 2165825
    const-wide/16 v4, 0x3

    mul-long/2addr v4, p1

    cmp-long v4, v0, v4

    if-ltz v4, :cond_1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 2165826
    const/4 v0, 0x1

    goto :goto_0

    .line 2165827
    :cond_1
    iget-object v0, p0, LX/EmQ;->i:LX/EmR;

    invoke-virtual {v0, p4}, LX/EmR;->b(LX/EmM;)J

    move-result-wide v0

    .line 2165828
    cmp-long v2, p1, v0

    if-lez v2, :cond_2

    move-wide v0, p1

    .line 2165829
    :cond_2
    if-eqz p6, :cond_3

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mounted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/EmQ;->d:LX/0V8;

    sget-object v3, LX/0VA;->EXTERNAL:LX/0VA;

    invoke-virtual {v2, v3, v0, v1}, LX/0V8;->a(LX/0VA;J)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2165830
    iget-object v0, p0, LX/EmQ;->a:Landroid/content/Context;

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v0, v1, v2}, Landroid/app/DownloadManager$Request;->setDestinationInExternalFilesDir(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 2165831
    iget-object v0, p0, LX/EmQ;->h:LX/EmX;

    new-instance v1, LX/Ema;

    sget-object v2, LX/EmZ;->DOWNLOAD_AT_EXTERNAL_DESTINATION:LX/EmZ;

    const/4 v3, 0x0

    invoke-direct {v1, p4, p5, v2, v3}, LX/Ema;-><init>(LX/EmM;Ljava/lang/String;LX/EmZ;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2165832
    const/4 v0, 0x1

    goto :goto_0

    .line 2165833
    :cond_3
    iget-object v0, p0, LX/EmQ;->h:LX/EmX;

    new-instance v1, LX/EmW;

    sget-object v4, LX/EmV;->ERROR_NO_FREE_SPACE_TO_DOWNLOAD:LX/EmV;

    const/4 v5, 0x0

    move-object v2, p4

    move-object v3, p5

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, LX/EmW;-><init>(LX/EmM;Ljava/lang/String;LX/EmV;Ljava/lang/Exception;J)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2165834
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/EmQ;J)LX/EmM;
    .locals 3

    .prologue
    .line 2165689
    iget-object v0, p0, LX/EmQ;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2165690
    iget-object v0, p0, LX/EmQ;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EmN;

    .line 2165691
    if-eqz v0, :cond_0

    .line 2165692
    iget-object v1, v0, LX/EmN;->a:LX/EmM;

    move-object v0, v1

    .line 2165693
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/EmM;->UNKNOWN:LX/EmM;

    goto :goto_0
.end method

.method private b(JLX/EmM;Ljava/lang/String;)V
    .locals 9
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2165813
    sget-object v0, LX/EmM;->UNKNOWN:LX/EmM;

    if-ne p3, v0, :cond_0

    .line 2165814
    invoke-static {p0, p1, p2}, LX/EmQ;->a$redex0(LX/EmQ;J)LX/EmM;

    move-result-object v2

    .line 2165815
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/EmQ;->c:Landroid/app/DownloadManager;

    const/4 v1, 0x1

    new-array v1, v1, [J

    const/4 v3, 0x0

    aput-wide p1, v1, v3

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->remove([J)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2165816
    :goto_1
    return-void

    .line 2165817
    :catch_0
    move-exception v5

    .line 2165818
    iget-object v0, p0, LX/EmQ;->h:LX/EmX;

    new-instance v1, LX/EmW;

    sget-object v4, LX/EmV;->ERROR_FAILED_TO_REMOVE_DOWNLOAD_ID:LX/EmV;

    move-object v3, p4

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, LX/EmW;-><init>(LX/EmM;Ljava/lang/String;LX/EmV;Ljava/lang/Exception;J)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    :cond_0
    move-object v2, p3

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/EmN;)J
    .locals 13

    .prologue
    const/4 v2, 0x0

    const-wide/16 v8, -0x1

    .line 2165757
    iget-object v0, p1, LX/EmN;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2165758
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 2165759
    const/4 v0, 0x0

    .line 2165760
    invoke-virtual {v10}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v3, "https"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2165761
    invoke-virtual {v10}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    const-string v3, ".facebook.com"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    move v1, v1

    .line 2165762
    if-eqz v1, :cond_2

    .line 2165763
    iget-object v0, p0, LX/EmQ;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-nez v0, :cond_1

    move-wide v0, v8

    .line 2165764
    :cond_0
    :goto_0
    return-wide v0

    .line 2165765
    :cond_1
    iget-object v0, p0, LX/EmQ;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2165766
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2165767
    :cond_2
    iget-object v1, p0, LX/EmQ;->h:LX/EmX;

    new-instance v3, LX/EmU;

    .line 2165768
    iget-object v4, p1, LX/EmN;->a:LX/EmM;

    move-object v4, v4

    .line 2165769
    iget-object v5, p1, LX/EmN;->g:Ljava/lang/String;

    move-object v5, v5

    .line 2165770
    invoke-direct {v3, v4, v5}, LX/EmU;-><init>(LX/EmM;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2165771
    new-instance v1, Landroid/app/DownloadManager$Request;

    invoke-direct {v1, v10}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    move-object v4, v1

    .line 2165772
    iget-boolean v1, p1, LX/EmN;->e:Z

    move v1, v1

    .line 2165773
    if-nez v1, :cond_b

    .line 2165774
    iget-object v1, p1, LX/EmN;->c:LX/EmK;

    move-object v3, v1

    .line 2165775
    iget v1, v3, LX/EmK;->a:I

    sget-object v5, LX/EmJ;->MOBILE:LX/EmJ;

    iget v5, v5, LX/EmJ;->value:I

    and-int/2addr v1, v5

    sget-object v5, LX/EmJ;->MOBILE:LX/EmJ;

    iget v5, v5, LX/EmJ;->value:I

    if-ne v1, v5, :cond_c

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2165776
    if-eqz v1, :cond_a

    .line 2165777
    const/4 v1, 0x1

    .line 2165778
    :goto_2
    iget v5, v3, LX/EmK;->a:I

    sget-object v6, LX/EmJ;->WIFI:LX/EmJ;

    iget v6, v6, LX/EmJ;->value:I

    and-int/2addr v5, v6

    sget-object v6, LX/EmJ;->WIFI:LX/EmJ;

    iget v6, v6, LX/EmJ;->value:I

    if-ne v5, v6, :cond_d

    const/4 v5, 0x1

    :goto_3
    move v3, v5

    .line 2165779
    if-eqz v3, :cond_3

    .line 2165780
    or-int/lit8 v1, v1, 0x2

    .line 2165781
    :cond_3
    const-string v3, "android.permission.DOWNLOAD_WITHOUT_NOTIFICATION"

    .line 2165782
    iget-object v5, p0, LX/EmQ;->a:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_e

    const/4 v5, 0x1

    :goto_4
    move v3, v5

    .line 2165783
    if-eqz v3, :cond_4

    .line 2165784
    const/4 v3, 0x2

    invoke-virtual {v4, v3}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 2165785
    :cond_4
    invoke-virtual {v4, v2}, Landroid/app/DownloadManager$Request;->setAllowedOverRoaming(Z)Landroid/app/DownloadManager$Request;

    .line 2165786
    :goto_5
    invoke-virtual {v4, v2}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    .line 2165787
    if-eqz v1, :cond_5

    .line 2165788
    invoke-virtual {v4, v1}, Landroid/app/DownloadManager$Request;->setAllowedNetworkTypes(I)Landroid/app/DownloadManager$Request;

    .line 2165789
    :cond_5
    const-string v1, "Accept"

    const-string v2, "application/octet-stream"

    invoke-virtual {v4, v1, v2}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 2165790
    if-eqz v0, :cond_6

    .line 2165791
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OAuth "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 2165792
    :cond_6
    iget-object v0, p1, LX/EmN;->h:Ljava/lang/String;

    move-object v0, v0

    .line 2165793
    if-eqz v0, :cond_7

    .line 2165794
    const-string v1, "User-Agent"

    invoke-virtual {v4, v1, v0}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 2165795
    :cond_7
    iget-object v0, p1, LX/EmN;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2165796
    if-eqz v0, :cond_8

    .line 2165797
    iget-object v0, p1, LX/EmN;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2165798
    invoke-virtual {v4, v0}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 2165799
    :cond_8
    iget-wide v11, p1, LX/EmN;->f:J

    move-wide v2, v11

    .line 2165800
    iget-object v0, p1, LX/EmN;->a:LX/EmM;

    move-object v5, v0

    .line 2165801
    iget-object v0, p1, LX/EmN;->g:Ljava/lang/String;

    move-object v6, v0

    .line 2165802
    iget-boolean v0, p1, LX/EmN;->i:Z

    move v7, v0

    .line 2165803
    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LX/EmQ;->a(JLandroid/app/DownloadManager$Request;LX/EmM;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_9

    move-wide v0, v8

    .line 2165804
    goto/16 :goto_0

    .line 2165805
    :cond_9
    iget-object v0, p0, LX/EmQ;->h:LX/EmX;

    new-instance v1, LX/Ema;

    .line 2165806
    iget-object v2, p1, LX/EmN;->a:LX/EmM;

    move-object v2, v2

    .line 2165807
    iget-object v3, p1, LX/EmN;->g:Ljava/lang/String;

    move-object v3, v3

    .line 2165808
    sget-object v5, LX/EmZ;->QUEUE_DOWNLOAD:LX/EmZ;

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v2, v3, v5, v6}, LX/Ema;-><init>(LX/EmM;Ljava/lang/String;LX/EmZ;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2165809
    :try_start_0
    iget-object v0, p0, LX/EmQ;->c:Landroid/app/DownloadManager;

    invoke-virtual {v0, v4}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2165810
    :goto_6
    cmp-long v2, v0, v8

    if-eqz v2, :cond_0

    .line 2165811
    iget-object v2, p0, LX/EmQ;->l:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2165812
    :catch_0
    move-wide v0, v8

    goto :goto_6

    :cond_a
    move v1, v2

    goto/16 :goto_2

    :cond_b
    move v1, v2

    goto/16 :goto_5

    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_4
.end method

.method public final a(Ljava/lang/String;JLX/EmM;Ljava/lang/String;)Ljava/io/File;
    .locals 6
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2165736
    sget-object v0, LX/EmM;->UNKNOWN:LX/EmM;

    if-ne p4, v0, :cond_0

    .line 2165737
    invoke-static {p0, p2, p3}, LX/EmQ;->a$redex0(LX/EmQ;J)LX/EmM;

    move-result-object p4

    .line 2165738
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2165739
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2165740
    if-eqz v0, :cond_4

    .line 2165741
    const-string v2, "file"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2165742
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    .line 2165743
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 2165744
    const-string v3, ""

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2165745
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2165746
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    .line 2165747
    :goto_0
    move v2, v2

    .line 2165748
    if-eqz v2, :cond_2

    .line 2165749
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0en;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    :cond_1
    :goto_1
    move-object v1, v0

    .line 2165750
    :goto_2
    return-object v1

    .line 2165751
    :cond_2
    :try_start_0
    iget-object v0, p0, LX/EmQ;->c:Landroid/app/DownloadManager;

    invoke-virtual {v0, p2, p3}, Landroid/app/DownloadManager;->openDownloadedFile(J)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2165752
    iget-object v2, p0, LX/EmQ;->a:Landroid/content/Context;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, LX/0en;->a(Landroid/content/Context;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 2165753
    if-eqz v0, :cond_1

    .line 2165754
    invoke-direct {p0, p2, p3, p4, p5}, LX/EmQ;->b(JLX/EmM;Ljava/lang/String;)V

    .line 2165755
    iget-object v2, p0, LX/EmQ;->h:LX/EmX;

    new-instance v3, LX/Ema;

    sget-object v4, LX/EmZ;->CREATED_FILE:LX/EmZ;

    invoke-direct {v3, p4, p5, v4, v1}, LX/Ema;-><init>(LX/EmM;Ljava/lang/String;LX/EmZ;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    .line 2165756
    :catch_0
    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 2165731
    iget-object v0, p0, LX/EmQ;->k:LX/0Yd;

    if-nez v0, :cond_0

    .line 2165732
    new-instance v0, LX/EmP;

    invoke-direct {v0, p0}, LX/EmP;-><init>(LX/EmQ;)V

    .line 2165733
    new-instance v1, LX/0Yd;

    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    const-string v3, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v2, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, LX/EmQ;->k:LX/0Yd;

    .line 2165734
    iget-object v0, p0, LX/EmQ;->a:Landroid/content/Context;

    iget-object v1, p0, LX/EmQ;->k:LX/0Yd;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2165735
    :cond_0
    return-void
.end method

.method public final a(JLX/EmM;Ljava/lang/String;)V
    .locals 11
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2165694
    new-instance v1, Landroid/app/DownloadManager$Query;

    invoke-direct {v1}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 2165695
    const/4 v2, 0x1

    new-array v2, v2, [J

    const/4 v3, 0x0

    aput-wide p1, v2, v3

    invoke-virtual {v1, v2}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 2165696
    sget-object v2, LX/EmM;->UNKNOWN:LX/EmM;

    if-ne p3, v2, :cond_0

    .line 2165697
    invoke-static {p0, p1, p2}, LX/EmQ;->a$redex0(LX/EmQ;J)LX/EmM;

    move-result-object p3

    .line 2165698
    :cond_0
    :try_start_0
    iget-object v2, p0, LX/EmQ;->c:Landroid/app/DownloadManager;

    invoke-virtual {v2, v1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 2165699
    if-eqz v0, :cond_1

    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2165700
    :cond_1
    const-string v1, "download_complete_cursor_empty"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, LX/EmQ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2165701
    iget-object v8, p0, LX/EmQ;->h:LX/EmX;

    new-instance v1, LX/EmW;

    invoke-static {p0, p1, p2}, LX/EmQ;->a$redex0(LX/EmQ;J)LX/EmM;

    move-result-object v2

    sget-object v4, LX/EmV;->ERROR_EMPTY_DOWNLOAD_MANAGER_CURSOR:LX/EmV;

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    move-object v3, p4

    invoke-direct/range {v1 .. v7}, LX/EmW;-><init>(LX/EmM;Ljava/lang/String;LX/EmV;Ljava/lang/Exception;J)V

    invoke-virtual {v8, v1}, LX/0b4;->a(LX/0b7;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2165702
    :goto_0
    if-eqz v0, :cond_2

    .line 2165703
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2165704
    :cond_2
    iget-object v0, p0, LX/EmQ;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2165705
    iget-object v0, p0, LX/EmQ;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2165706
    :cond_3
    :goto_1
    return-void

    .line 2165707
    :cond_4
    :try_start_2
    const-string v1, "status"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 2165708
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 2165709
    const-string v2, "reason"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 2165710
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 2165711
    const-string v2, "local_uri"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 2165712
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2165713
    sparse-switch v1, :sswitch_data_0

    .line 2165714
    const-string v1, "download_complete_status_other"

    invoke-static {v0}, LX/EmQ;->a(Landroid/database/Cursor;)Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, v1, v2}, LX/EmQ;->a(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2165715
    :catch_0
    move-exception v5

    .line 2165716
    :try_start_3
    const-string v1, "download_complete_exception"

    invoke-static {v0}, LX/EmQ;->a(Landroid/database/Cursor;)Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, v1, v2}, LX/EmQ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2165717
    iget-object v8, p0, LX/EmQ;->h:LX/EmX;

    new-instance v1, LX/EmW;

    sget-object v4, LX/EmV;->ERROR_DOWNLOAD_MANAGER_COMPLETION_EXCEPTION:LX/EmV;

    move-object v2, p3

    move-object v3, p4

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, LX/EmW;-><init>(LX/EmM;Ljava/lang/String;LX/EmV;Ljava/lang/Exception;J)V

    invoke-virtual {v8, v1}, LX/0b4;->a(LX/0b7;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2165718
    if-eqz v0, :cond_5

    .line 2165719
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2165720
    :cond_5
    iget-object v0, p0, LX/EmQ;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2165721
    iget-object v0, p0, LX/EmQ;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2165722
    :sswitch_0
    :try_start_4
    const-string v1, "download_complete_successful"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, LX/EmQ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2165723
    iget-object v8, p0, LX/EmQ;->h:LX/EmX;

    new-instance v1, LX/Emb;

    move-object v2, p3

    move-object v3, p4

    move-wide v4, p1

    invoke-direct/range {v1 .. v7}, LX/Emb;-><init>(LX/EmM;Ljava/lang/String;JLjava/lang/String;I)V

    invoke-virtual {v8, v1}, LX/0b4;->a(LX/0b7;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2165724
    :catchall_0
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_2
    if-eqz v1, :cond_6

    .line 2165725
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2165726
    :cond_6
    iget-object v1, p0, LX/EmQ;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2165727
    iget-object v1, p0, LX/EmQ;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    throw v0

    .line 2165728
    :sswitch_1
    :try_start_5
    const-string v1, "download_complete_failed"

    invoke-static {v0}, LX/EmQ;->a(Landroid/database/Cursor;)Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, v1, v2}, LX/EmQ;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2165729
    iget-object v8, p0, LX/EmQ;->h:LX/EmX;

    new-instance v1, LX/EmW;

    sget-object v4, LX/EmV;->ERROR_DOWNLOAD_MANAGER_FAILURE:LX/EmV;

    const/4 v5, 0x0

    int-to-long v6, v7

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v1 .. v7}, LX/EmW;-><init>(LX/EmM;Ljava/lang/String;LX/EmV;Ljava/lang/Exception;J)V

    invoke-virtual {v8, v1}, LX/0b4;->a(LX/0b7;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 2165730
    :catchall_1
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method
