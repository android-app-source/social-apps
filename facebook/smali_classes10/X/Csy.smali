.class public LX/Csy;
.super Lcom/facebook/video/player/plugins/CoverImagePlugin;
.source ""


# instance fields
.field public f:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/BA0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final p:LX/2oW;

.field private final q:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2oW;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1943641
    invoke-direct {p0, p1, p3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1943642
    const-class v0, LX/Csy;

    invoke-static {v0, p0}, LX/Csy;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1943643
    iput-object p2, p0, LX/Csy;->p:LX/2oW;

    .line 1943644
    iput-object p3, p0, LX/Csy;->q:Lcom/facebook/common/callercontext/CallerContext;

    .line 1943645
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Csy;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/BA0;->a(LX/0QB;)LX/BA0;

    move-result-object p0

    check-cast p0, LX/BA0;

    iput-object v1, p1, LX/Csy;->f:LX/1Ad;

    iput-object v2, p1, LX/Csy;->n:Ljava/util/concurrent/Executor;

    iput-object p0, p1, LX/Csy;->o:LX/BA0;

    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    .line 1943646
    invoke-super {p0, p1, p2}, Lcom/facebook/video/player/plugins/CoverImagePlugin;->a(LX/2pa;Z)V

    .line 1943647
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "CoverImageParamsKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1943648
    instance-of v1, v0, LX/Csx;

    if-eqz v1, :cond_0

    .line 1943649
    check-cast v0, LX/Csx;

    .line 1943650
    iget-object v1, p0, LX/Csy;->f:LX/1Ad;

    iget-object v2, p0, LX/Csy;->q:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    iget-object v2, v0, LX/Csx;->d:LX/1bf;

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    .line 1943651
    iget-object v2, p0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v2, v2

    .line 1943652
    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    new-instance v2, LX/Csv;

    invoke-direct {v2, p0}, LX/Csv;-><init>(LX/Csy;)V

    invoke-virtual {v1, v2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1943653
    iget-object v2, p0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v2, v2

    .line 1943654
    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1943655
    iget-object v1, v0, LX/Csx;->a:Ljava/lang/String;

    iget v2, v0, LX/Csx;->b:I

    iget v0, v0, LX/Csx;->c:I

    .line 1943656
    if-nez v1, :cond_1

    .line 1943657
    :cond_0
    :goto_0
    return-void

    .line 1943658
    :cond_1
    iget-object v3, p0, LX/Csy;->o:LX/BA0;

    int-to-float p1, v2

    int-to-float p2, v0

    div-float/2addr p1, p2

    invoke-virtual {v3, v1, p1}, LX/BA0;->a(Ljava/lang/String;F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance p1, LX/Csw;

    invoke-direct {p1, p0}, LX/Csw;-><init>(LX/Csy;)V

    iget-object p2, p0, LX/Csy;->n:Ljava/util/concurrent/Executor;

    invoke-static {v3, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
