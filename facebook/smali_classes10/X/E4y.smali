.class public LX/E4y;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E4y;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E4y",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2077621
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2077622
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/E4y;->b:LX/0Zi;

    .line 2077623
    iput-object p1, p0, LX/E4y;->a:LX/0Ot;

    .line 2077624
    return-void
.end method

.method public static a(LX/0QB;)LX/E4y;
    .locals 4

    .prologue
    .line 2077678
    sget-object v0, LX/E4y;->c:LX/E4y;

    if-nez v0, :cond_1

    .line 2077679
    const-class v1, LX/E4y;

    monitor-enter v1

    .line 2077680
    :try_start_0
    sget-object v0, LX/E4y;->c:LX/E4y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2077681
    if-eqz v2, :cond_0

    .line 2077682
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2077683
    new-instance v3, LX/E4y;

    const/16 p0, 0x3119

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E4y;-><init>(LX/0Ot;)V

    .line 2077684
    move-object v0, v3

    .line 2077685
    sput-object v0, LX/E4y;->c:LX/E4y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077686
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2077687
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2077688
    :cond_1
    sget-object v0, LX/E4y;->c:LX/E4y;

    return-object v0

    .line 2077689
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2077690
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 7

    .prologue
    .line 2077674
    check-cast p2, LX/E4x;

    .line 2077675
    iget-object v0, p0, LX/E4y;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;

    iget-object v2, p2, LX/E4x;->h:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p2, LX/E4x;->i:Ljava/lang/String;

    iget-object v4, p2, LX/E4x;->j:LX/2km;

    iget-object v5, p2, LX/E4x;->k:Ljava/lang/String;

    iget-object v6, p2, LX/E4x;->l:Ljava/lang/String;

    move-object v1, p1

    .line 2077676
    invoke-static/range {v0 .. v6}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->b(Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    .line 2077677
    return-void
.end method

.method private b(Landroid/view/View;LX/1X1;)V
    .locals 7

    .prologue
    .line 2077670
    check-cast p2, LX/E4x;

    .line 2077671
    iget-object v0, p0, LX/E4y;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;

    iget-object v2, p2, LX/E4x;->m:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p2, LX/E4x;->i:Ljava/lang/String;

    iget-object v4, p2, LX/E4x;->j:LX/2km;

    iget-object v5, p2, LX/E4x;->k:Ljava/lang/String;

    iget-object v6, p2, LX/E4x;->l:Ljava/lang/String;

    move-object v1, p1

    .line 2077672
    invoke-static/range {v0 .. v6}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->b(Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;LX/2km;Ljava/lang/String;Ljava/lang/String;)V

    .line 2077673
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2077669
    const v0, 0xda8ed21

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2077641
    check-cast p2, LX/E4x;

    .line 2077642
    iget-object v0, p0, LX/E4y;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;

    iget-object v2, p2, LX/E4x;->a:Landroid/net/Uri;

    iget-object v3, p2, LX/E4x;->b:Ljava/lang/String;

    iget-object v4, p2, LX/E4x;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iget-object v5, p2, LX/E4x;->d:Ljava/lang/String;

    iget-object v6, p2, LX/E4x;->e:Landroid/net/Uri;

    iget-object v7, p2, LX/E4x;->f:LX/1X1;

    iget-object v8, p2, LX/E4x;->g:LX/4Ab;

    move-object v1, p1

    .line 2077643
    iget-object v9, v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->c:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/1Ad;

    .line 2077644
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    const/4 p0, 0x2

    invoke-interface {v10, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v10

    const/4 p0, 0x2

    invoke-interface {v10, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v10

    const/4 p0, 0x6

    const p1, 0x7f010717

    invoke-interface {v10, p0, p1}, LX/1Dh;->p(II)LX/1Dh;

    move-result-object v10

    invoke-static {v1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    invoke-static {v1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object p1

    const p2, 0x7f0a010a

    invoke-virtual {p1, p2}, LX/1nh;->i(I)LX/1nh;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object p0

    invoke-virtual {v9, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object p1

    sget-object p2, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p1, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object p1

    invoke-virtual {p1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object p0

    invoke-virtual {p0, v8}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-static {v4}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)I

    move-result p1

    invoke-interface {p0, p1}, LX/1Di;->q(I)LX/1Di;

    move-result-object p0

    invoke-static {v4}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)I

    move-result p1

    invoke-interface {p0, p1}, LX/1Di;->i(I)LX/1Di;

    move-result-object p0

    const/16 p1, 0x8

    const p2, 0x7f0b163a

    invoke-interface {p0, p1, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object p0

    const/4 p1, 0x2

    const p2, 0x7f0b163d

    invoke-interface {p0, p1, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object p0

    invoke-interface {v10, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    const/4 p1, 0x0

    invoke-interface {v10, p1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v10

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-interface {v10, p1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v10

    const/4 p1, 0x1

    invoke-interface {v10, p1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v10

    const/4 p1, 0x2

    const p2, 0x7f0b163a

    invoke-interface {v10, p1, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v10

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object p1

    const p2, 0x3f91eb85    # 1.14f

    invoke-virtual {p1, p2}, LX/1ne;->j(F)LX/1ne;

    move-result-object p1

    invoke-virtual {p1, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p1

    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, p2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object p1

    .line 2077645
    sget-object p2, LX/E4z;->a:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v0

    aget p2, p2, v0

    packed-switch p2, :pswitch_data_0

    .line 2077646
    :pswitch_0
    const p2, 0x7f0a010c

    :goto_0
    move p2, p2

    .line 2077647
    invoke-virtual {p1, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p1

    .line 2077648
    sget-object p2, LX/E4z;->a:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v0

    aget p2, p2, v0

    packed-switch p2, :pswitch_data_1

    .line 2077649
    sget-object p2, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v0, LX/0xr;->MEDIUM:LX/0xr;

    const/4 v2, 0x0

    invoke-static {v1, p2, v0, v2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object p2

    :goto_1
    move-object p2, p2

    .line 2077650
    invoke-virtual {p1, p2}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object p1

    .line 2077651
    sget-object p2, LX/E4z;->a:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v0

    aget p2, p2, v0

    packed-switch p2, :pswitch_data_2

    .line 2077652
    const p2, 0x7f0b0050

    :goto_2
    move p2, p2

    .line 2077653
    invoke-virtual {p1, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p1

    invoke-interface {v10, p1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object p1

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v10, 0x0

    :goto_3
    invoke-interface {p1, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v10

    invoke-interface {p0, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    .line 2077654
    if-eqz v7, :cond_2

    .line 2077655
    invoke-interface {v10, v7}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    .line 2077656
    :cond_0
    :goto_4
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const p0, 0x7f01072b

    invoke-interface {v9, p0}, LX/1Dh;->U(I)LX/1Dh;

    move-result-object v9

    .line 2077657
    const p0, 0xda8ed21

    const/4 p1, 0x0

    invoke-static {v1, p0, p1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2077658
    invoke-interface {v9, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9}, LX/1Di;->k()LX/1Dg;

    move-result-object v9

    move-object v0, v9

    .line 2077659
    return-object v0

    .line 2077660
    :cond_1
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    const p2, 0x3faa3d71    # 1.33f

    invoke-virtual {v10, p2}, LX/1ne;->j(F)LX/1ne;

    move-result-object v10

    const/4 p2, 0x3

    invoke-virtual {v10, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v10

    const p2, 0x7f0b004e

    invoke-virtual {v10, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v10, p2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v10

    const p2, 0x7f0a010e

    invoke-virtual {v10, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v10

    goto :goto_3

    .line 2077661
    :cond_2
    if-eqz v6, :cond_0

    .line 2077662
    invoke-static {v1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    invoke-virtual {v9, v6}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v9

    sget-object p1, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v9, p1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v9

    invoke-virtual {v9}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    const p0, 0x7f0b1632

    invoke-interface {v9, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v9

    const p0, 0x7f0b1632

    invoke-interface {v9, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v9

    const/16 p0, 0x8

    const p1, 0x7f0b163a

    invoke-interface {v9, p0, p1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v9

    .line 2077663
    const p0, 0xdc4303f

    const/4 p1, 0x0

    invoke-static {v1, p0, p1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2077664
    invoke-interface {v9, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v9

    invoke-interface {v10, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto/16 :goto_4

    .line 2077665
    :pswitch_1
    const p2, 0x7f0a010d

    goto/16 :goto_0

    .line 2077666
    :pswitch_2
    const p2, 0x7f0a00d2

    goto/16 :goto_0

    .line 2077667
    :pswitch_3
    sget-object p2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto/16 :goto_1

    .line 2077668
    :pswitch_4
    const p2, 0x7f0b004e

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x4
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2077633
    invoke-static {}, LX/1dS;->b()V

    .line 2077634
    iget v0, p1, LX/1dQ;->b:I

    .line 2077635
    sparse-switch v0, :sswitch_data_0

    .line 2077636
    :goto_0
    return-object v2

    .line 2077637
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2077638
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/E4y;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 2077639
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2077640
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/E4y;->b(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xda8ed21 -> :sswitch_0
        0xdc4303f -> :sswitch_1
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/E4w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/E4y",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2077625
    new-instance v1, LX/E4x;

    invoke-direct {v1, p0}, LX/E4x;-><init>(LX/E4y;)V

    .line 2077626
    iget-object v2, p0, LX/E4y;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/E4w;

    .line 2077627
    if-nez v2, :cond_0

    .line 2077628
    new-instance v2, LX/E4w;

    invoke-direct {v2, p0}, LX/E4w;-><init>(LX/E4y;)V

    .line 2077629
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/E4w;->a$redex0(LX/E4w;LX/1De;IILX/E4x;)V

    .line 2077630
    move-object v1, v2

    .line 2077631
    move-object v0, v1

    .line 2077632
    return-object v0
.end method
