.class public final LX/Erq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0tW;
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0tW;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Err;

.field private b:Lcom/facebook/api/feed/FetchFeedParams;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Err;Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 1

    .prologue
    .line 2173539
    iput-object p1, p0, LX/Erq;->a:LX/Err;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2173540
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Erq;->c:Ljava/util/List;

    .line 2173541
    iput-object p2, p0, LX/Erq;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 2173542
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0rl;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0rl",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2173515
    return-object p0
.end method

.method public final a()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "IgnoreExecutorServiceSubmitResult"
        }
    .end annotation

    .prologue
    .line 2173536
    iget-object v0, p0, LX/Erq;->a:LX/Err;

    iget-object v0, v0, LX/Err;->s:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173537
    :goto_0
    return-void

    .line 2173538
    :cond_0
    iget-object v0, p0, LX/Erq;->a:LX/Err;

    iget-object v0, v0, LX/Err;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$1;

    invoke-direct {v1, p0}, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$1;-><init>(LX/Erq;)V

    const v2, -0x73fa8939

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2173519
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2173520
    iget-object v0, p0, LX/Erq;->a:LX/Err;

    iget-object v0, v0, LX/Err;->s:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173521
    :goto_0
    return-void

    .line 2173522
    :cond_0
    iget-object v0, p0, LX/Erq;->a:LX/Err;

    iget-object v0, v0, LX/Err;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rm;

    iget-object v1, p0, LX/Erq;->b:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {v0, v1, p1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v0

    .line 2173523
    if-nez v0, :cond_1

    .line 2173524
    goto :goto_0

    .line 2173525
    :cond_1
    iget-object v1, p0, LX/Erq;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 2173526
    new-instance v2, Lcom/facebook/api/feed/FetchFeedResult;

    sget-object v5, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/Erq;->a:LX/Err;

    iget-object v3, v3, LX/Err;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object v3, v1

    move-object v4, v0

    invoke-direct/range {v2 .. v8}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    .line 2173527
    iget-object v3, p0, LX/Erq;->a:LX/Err;

    iget-object v3, v3, LX/Err;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0pm;

    invoke-virtual {v3, v2}, LX/0pm;->b(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v3

    .line 2173528
    iget-object v2, p0, LX/Erq;->a:LX/Err;

    iget-object v2, v2, LX/Err;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0pm;

    invoke-static {v3}, LX/0pn;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 2173529
    iget-object v2, p0, LX/Erq;->a:LX/Err;

    iget-object v2, v2, LX/Err;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/187;

    invoke-virtual {v2, v3}, LX/187;->b(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;

    .line 2173530
    move-object v0, v3

    .line 2173531
    invoke-virtual {v0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    .line 2173532
    iget-object v1, p0, LX/Erq;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2173533
    iget-object v1, p0, LX/Erq;->a:LX/Err;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2173534
    iget v2, v1, LX/Err;->q:I

    add-int/2addr v2, v0

    iput v2, v1, LX/Err;->q:I

    .line 2173535
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2173516
    iget-object v0, p0, LX/Erq;->a:LX/Err;

    iget-object v0, v0, LX/Err;->s:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173517
    :goto_0
    return-void

    .line 2173518
    :cond_0
    iget-object v0, p0, LX/Erq;->a:LX/Err;

    iget-object v0, v0, LX/Err;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$2;

    invoke-direct {v1, p0}, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$OfflineFeedFetcherSubscriber$2;-><init>(LX/Erq;)V

    const v2, 0xc67cc17

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method
