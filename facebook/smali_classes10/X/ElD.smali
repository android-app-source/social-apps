.class public final LX/ElD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ElE;


# direct methods
.method public constructor <init>(LX/ElE;)V
    .locals 0

    .prologue
    .line 2164116
    iput-object p1, p0, LX/ElD;->a:LX/ElE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2164117
    iget-object v0, p0, LX/ElD;->a:LX/ElE;

    .line 2164118
    instance-of v1, p1, Ljava/io/IOException;

    if-eqz v1, :cond_0

    .line 2164119
    check-cast p1, Ljava/io/IOException;

    .line 2164120
    :goto_0
    move-object v1, p1

    .line 2164121
    iput-object v1, v0, LX/ElE;->b:Ljava/io/IOException;

    .line 2164122
    iget-object v0, p0, LX/ElD;->a:LX/ElE;

    invoke-virtual {v0}, LX/ElE;->a()V

    .line 2164123
    return-void

    .line 2164124
    :cond_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 2164125
    invoke-virtual {v1, p1}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-object p1, v1

    .line 2164126
    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2164127
    iget-object v0, p0, LX/ElD;->a:LX/ElE;

    iget-object v0, v0, LX/ElE;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2164128
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "FbHttpResponseHandler expected to count down!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2164129
    :cond_0
    return-void
.end method
