.class public LX/ERU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/ERV;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2120952
    const-class v0, LX/ERU;

    sput-object v0, LX/ERU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2120955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120956
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 2120957
    check-cast p1, LX/ERV;

    .line 2120958
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2120959
    iget-object v1, p1, LX/ERV;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 2120960
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "last_sync_time"

    iget-object v3, p1, LX/ERV;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120961
    :cond_0
    iget-object v1, p1, LX/ERV;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 2120962
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "enabled"

    iget-object v3, p1, LX/ERV;->c:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120963
    :cond_1
    iget-object v1, p1, LX/ERV;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2120964
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "sync_mode"

    iget-object v3, p1, LX/ERV;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120965
    :cond_2
    iget-object v1, p1, LX/ERV;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2120966
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "sync_older_photos"

    iget-object v3, p1, LX/ERV;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120967
    :cond_3
    move-object v4, v0

    .line 2120968
    new-instance v0, LX/14N;

    const-string v1, "vaultDeviceUpdate"

    const-string v2, "POST"

    .line 2120969
    iget-wide v8, p1, LX/ERV;->a:J

    move-wide v6, v8

    .line 2120970
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2120953
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2120954
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
