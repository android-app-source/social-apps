.class public final LX/EOT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EOS;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;LX/CzL;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2114549
    iput-object p1, p0, LX/EOT;->c:Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;

    iput-object p2, p0, LX/EOT;->a:LX/CzL;

    iput-object p3, p0, LX/EOT;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(LX/CzL;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Landroid/view/View;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d2;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2114535
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2114536
    check-cast v0, LX/8d2;

    .line 2114537
    iget-object v1, p0, LX/EOT;->a:LX/CzL;

    .line 2114538
    iget-object v2, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v2

    .line 2114539
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fh_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v1

    .line 2114540
    new-instance v2, LX/EOR;

    invoke-direct {v2, p0}, LX/EOR;-><init>(LX/EOT;)V

    invoke-static {v1, v2}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2114541
    invoke-static {v1}, LX/9hF;->f(LX/0Px;)LX/9hE;

    move-result-object v1

    .line 2114542
    sget-object v2, LX/74S;->SEARCH_PHOTOS_GRID_MODULE:LX/74S;

    invoke-virtual {v1, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v1

    invoke-interface {v0}, LX/8d2;->dW_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    const/4 v1, 0x1

    .line 2114543
    iput-boolean v1, v0, LX/9hD;->C:Z

    .line 2114544
    move-object v0, v0

    .line 2114545
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 2114546
    iget-object v0, p0, LX/EOT;->c:Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    invoke-virtual {p5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v1, v3}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2114547
    iget-object v0, p0, LX/EOT;->b:LX/1Pn;

    check-cast v0, LX/Cxh;

    invoke-interface {v0, p1}, LX/Cxh;->c(LX/CzL;)V

    .line 2114548
    return-void
.end method
