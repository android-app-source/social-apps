.class public LX/EB0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/EB0;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNY;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNZ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNg;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNf;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNn;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1Ck;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BNY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BNZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BNg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BNf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BNn;",
            ">;",
            "LX/1Ck;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2086052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2086053
    iput-object p1, p0, LX/EB0;->a:LX/0Ot;

    .line 2086054
    iput-object p2, p0, LX/EB0;->b:LX/0Ot;

    .line 2086055
    iput-object p3, p0, LX/EB0;->c:LX/0Ot;

    .line 2086056
    iput-object p4, p0, LX/EB0;->d:LX/0Ot;

    .line 2086057
    iput-object p5, p0, LX/EB0;->e:LX/0Ot;

    .line 2086058
    iput-object p6, p0, LX/EB0;->f:LX/1Ck;

    .line 2086059
    return-void
.end method

.method public static a(LX/0QB;)LX/EB0;
    .locals 10

    .prologue
    .line 2086060
    sget-object v0, LX/EB0;->g:LX/EB0;

    if-nez v0, :cond_1

    .line 2086061
    const-class v1, LX/EB0;

    monitor-enter v1

    .line 2086062
    :try_start_0
    sget-object v0, LX/EB0;->g:LX/EB0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2086063
    if-eqz v2, :cond_0

    .line 2086064
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2086065
    new-instance v3, LX/EB0;

    const/16 v4, 0x31d7

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x31d8

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x31da

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x31d9

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x31db

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-direct/range {v3 .. v9}, LX/EB0;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1Ck;)V

    .line 2086066
    move-object v0, v3

    .line 2086067
    sput-object v0, LX/EB0;->g:LX/EB0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2086068
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2086069
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2086070
    :cond_1
    sget-object v0, LX/EB0;->g:LX/EB0;

    return-object v0

    .line 2086071
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2086072
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2086073
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EB0;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNZ;

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2086074
    new-instance p0, LX/BNi;

    invoke-direct {p0}, LX/BNi;-><init>()V

    move-object p0, p0

    .line 2086075
    const-string p1, "review_id"

    invoke-virtual {p0, p1, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2086076
    iget-object p1, v0, LX/BNZ;->a:LX/0tX;

    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    move-object v0, p0

    .line 2086077
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/EB0;LX/0am;Ljava/lang/String;ILX/EA4;LX/EA4;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Lcom/facebook/reviews/loader/UserReviewsListLoader$LoadSingleReviewCallback;",
            "Lcom/facebook/reviews/loader/UserReviewsListLoader$LoadPlacesToReviewsCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2086078
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, LX/EB0;->a(LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 2086079
    iget-object v2, p0, LX/EB0;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BNY;

    const/4 v3, 0x0

    invoke-virtual {v2, p2, p3, v3}, LX/BNY;->a(Ljava/lang/String;ILjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v2, v2

    .line 2086080
    aput-object v2, v0, v1

    invoke-static {v0}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2086081
    iget-object v1, p0, LX/EB0;->f:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "key_load_more_places_to_review"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/EAt;

    invoke-direct {v3, p0, p1, p4, p5}, LX/EAt;-><init>(LX/EB0;LX/0am;LX/EA4;LX/EA4;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2086082
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2086083
    iget-object v0, p0, LX/EB0;->f:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2086084
    return-void
.end method
