.class public LX/CwV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 1950757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950758
    iput-object p1, p0, LX/CwV;->a:Ljava/lang/String;

    .line 1950759
    iput-object p2, p0, LX/CwV;->b:Ljava/lang/String;

    .line 1950760
    iput-boolean p3, p0, LX/CwV;->d:Z

    .line 1950761
    iput-boolean p4, p0, LX/CwV;->c:Z

    .line 1950762
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1950763
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/CwV;

    if-nez v1, :cond_1

    .line 1950764
    :cond_0
    :goto_0
    return v0

    .line 1950765
    :cond_1
    check-cast p1, LX/CwV;

    .line 1950766
    iget-object v1, p0, LX/CwV;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CwV;->a:Ljava/lang/String;

    .line 1950767
    iget-object v2, p1, LX/CwV;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1950768
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1950769
    iget-object v0, p0, LX/CwV;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/CwV;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    goto :goto_0
.end method
