.class public LX/ERH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/ERH;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2120688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120689
    iput-object p1, p0, LX/ERH;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2120690
    return-void
.end method

.method public static a(LX/0QB;)LX/ERH;
    .locals 4

    .prologue
    .line 2120695
    sget-object v0, LX/ERH;->b:LX/ERH;

    if-nez v0, :cond_1

    .line 2120696
    const-class v1, LX/ERH;

    monitor-enter v1

    .line 2120697
    :try_start_0
    sget-object v0, LX/ERH;->b:LX/ERH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2120698
    if-eqz v2, :cond_0

    .line 2120699
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2120700
    new-instance p0, LX/ERH;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/ERH;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2120701
    move-object v0, p0

    .line 2120702
    sput-object v0, LX/ERH;->b:LX/ERH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2120703
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2120704
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2120705
    :cond_1
    sget-object v0, LX/ERH;->b:LX/ERH;

    return-object v0

    .line 2120706
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2120707
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 2120691
    iget-object v0, p0, LX/ERH;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 2120692
    sget-object v1, LX/2TR;->g:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 2120693
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2120694
    return-void
.end method
