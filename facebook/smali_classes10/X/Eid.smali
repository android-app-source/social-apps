.class public final LX/Eid;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6u6;


# instance fields
.field public final synthetic a:LX/Eie;


# direct methods
.method public constructor <init>(LX/Eie;)V
    .locals 0

    .prologue
    .line 2159976
    iput-object p1, p0, LX/Eid;->a:LX/Eie;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/7Tl;)V
    .locals 7

    .prologue
    .line 2159977
    iget-object v0, p0, LX/Eid;->a:LX/Eie;

    iget-object v0, v0, LX/Eie;->a:Lcom/facebook/confirmation/fragment/ConfPhoneFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v1, p0, LX/Eid;->a:LX/Eie;

    iget-object v1, v1, LX/Eie;->a:Lcom/facebook/confirmation/fragment/ConfPhoneFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159978
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v2

    .line 2159979
    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    iget-object v2, p0, LX/Eid;->a:LX/Eie;

    iget-object v2, v2, LX/Eie;->a:Lcom/facebook/confirmation/fragment/ConfPhoneFragment;

    invoke-virtual {v2}, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->n()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v2

    iget-object v3, p1, LX/7Tl;->a:Ljava/lang/String;

    .line 2159980
    iget-object v4, v0, LX/2U9;->a:LX/0Zb;

    sget-object v5, LX/Eiw;->CHANGE_CONTACTPOINT_COUNTRY_SELECTED:LX/Eiw;

    invoke-virtual {v5}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2159981
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2159982
    const-string v5, "confirmation"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159983
    const-string v5, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159984
    const-string v5, "new_contactpoint_type"

    invoke-virtual {v2}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159985
    const-string v5, "country_selected"

    invoke-virtual {v4, v5, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159986
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2159987
    :cond_0
    iget-object v0, p0, LX/Eid;->a:LX/Eie;

    iget-object v0, v0, LX/Eie;->a:Lcom/facebook/confirmation/fragment/ConfPhoneFragment;

    invoke-static {v0, p1}, Lcom/facebook/confirmation/fragment/ConfPhoneFragment;->a$redex0(Lcom/facebook/confirmation/fragment/ConfPhoneFragment;LX/7Tl;)V

    .line 2159988
    return-void
.end method
