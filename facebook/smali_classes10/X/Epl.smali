.class public LX/Epl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Epl;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2170565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170566
    return-void
.end method

.method public static a(LX/0QB;)LX/Epl;
    .locals 3

    .prologue
    .line 2170567
    sget-object v0, LX/Epl;->a:LX/Epl;

    if-nez v0, :cond_1

    .line 2170568
    const-class v1, LX/Epl;

    monitor-enter v1

    .line 2170569
    :try_start_0
    sget-object v0, LX/Epl;->a:LX/Epl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2170570
    if-eqz v2, :cond_0

    .line 2170571
    :try_start_1
    new-instance v0, LX/Epl;

    invoke-direct {v0}, LX/Epl;-><init>()V

    .line 2170572
    move-object v0, v0

    .line 2170573
    sput-object v0, LX/Epl;->a:LX/Epl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2170574
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2170575
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2170576
    :cond_1
    sget-object v0, LX/Epl;->a:LX/Epl;

    return-object v0

    .line 2170577
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2170578
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;)Z
    .locals 1

    .prologue
    .line 2170579
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->p()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardContextItemModel;->p()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
