.class public final LX/EII;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EBv;


# instance fields
.field public final synthetic a:LX/EIU;


# direct methods
.method public constructor <init>(LX/EIU;)V
    .locals 0

    .prologue
    .line 2101451
    iput-object p1, p0, LX/EII;->a:LX/EIU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2101452
    iget-object v0, p0, LX/EII;->a:LX/EIU;

    invoke-static {v0}, LX/EIU;->P(LX/EIU;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2101453
    iget-object v0, p0, LX/EII;->a:LX/EIU;

    sget-object v1, LX/ED5;->EMPTY:LX/ED5;

    invoke-static {v0, v1}, LX/EIU;->a$redex0(LX/EIU;LX/ED5;)V

    .line 2101454
    :cond_0
    :goto_0
    return-void

    .line 2101455
    :cond_1
    iget-object v0, p0, LX/EII;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->ah:LX/EBz;

    if-eqz v0, :cond_0

    .line 2101456
    iget-object v0, p0, LX/EII;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->ah:LX/EBz;

    invoke-virtual {v0}, LX/EBz;->e()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2101444
    iget-object v0, p0, LX/EII;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->s:LX/ECi;

    invoke-virtual {v0}, LX/ECi;->a()V

    .line 2101445
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2101446
    iget-object v0, p0, LX/EII;->a:LX/EIU;

    .line 2101447
    invoke-static {v0}, LX/EIU;->P(LX/EIU;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2101448
    sget-object p0, LX/ED5;->EMPTY:LX/ED5;

    invoke-static {v0, p0}, LX/EIU;->a$redex0(LX/EIU;LX/ED5;)V

    .line 2101449
    :goto_0
    return-void

    .line 2101450
    :cond_0
    sget-object p0, LX/ED5;->ROSTER:LX/ED5;

    invoke-static {v0, p0}, LX/EIU;->a$redex0(LX/EIU;LX/ED5;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2101431
    iget-object v0, p0, LX/EII;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->ah:LX/EBz;

    .line 2101432
    iget-object p0, v0, LX/EBz;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    iget-object p0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG:Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b()V

    .line 2101433
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2101434
    iget-object v0, p0, LX/EII;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->ah:LX/EBz;

    invoke-virtual {v0}, LX/EBz;->j()V

    .line 2101435
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2101436
    iget-object v0, p0, LX/EII;->a:LX/EIU;

    .line 2101437
    iget-object v1, v0, LX/EIU;->an:LX/EIT;

    sget-object p0, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    if-ne v1, p0, :cond_1

    .line 2101438
    sget-object v1, LX/EIT;->GRID_VIEW:LX/EIT;

    iput-object v1, v0, LX/EIU;->an:LX/EIT;

    .line 2101439
    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/EIU;->ao:Z

    .line 2101440
    iget-object v1, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->bj()LX/0Px;

    move-result-object v1

    invoke-static {v0}, LX/EIU;->getConferenceCall(LX/EIU;)LX/EFy;

    move-result-object p0

    invoke-static {v0, v1, p0}, LX/EIU;->a(LX/EIU;LX/0Px;LX/EFy;)V

    .line 2101441
    return-void

    .line 2101442
    :cond_1
    iget-object v1, v0, LX/EIU;->an:LX/EIT;

    sget-object p0, LX/EIT;->GRID_VIEW:LX/EIT;

    if-ne v1, p0, :cond_0

    .line 2101443
    sget-object v1, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    iput-object v1, v0, LX/EIU;->an:LX/EIT;

    goto :goto_0
.end method
