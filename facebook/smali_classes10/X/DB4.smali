.class public LX/DB4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0SI;

.field public c:Z

.field public d:Z

.field private e:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SI;LX/BiT;Landroid/content/Context;Ljava/lang/Boolean;LX/0SG;)V
    .locals 1
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1972147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1972148
    iput-object p3, p0, LX/DB4;->a:Landroid/content/Context;

    .line 1972149
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/DB4;->d:Z

    .line 1972150
    iput-object p1, p0, LX/DB4;->b:LX/0SI;

    .line 1972151
    const/4 v0, 0x1

    move v0, v0

    .line 1972152
    iput-boolean v0, p0, LX/DB4;->c:Z

    .line 1972153
    iput-object p5, p0, LX/DB4;->e:LX/0SG;

    .line 1972154
    return-void
.end method

.method public static b(LX/0QB;)LX/DB4;
    .locals 6

    .prologue
    .line 1972155
    new-instance v0, LX/DB4;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v1

    check-cast v1, LX/0SI;

    invoke-static {p0}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object v2

    check-cast v2, LX/BiT;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct/range {v0 .. v5}, LX/DB4;-><init>(LX/0SI;LX/BiT;Landroid/content/Context;Ljava/lang/Boolean;LX/0SG;)V

    .line 1972156
    return-object v0
.end method


# virtual methods
.method public final a(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1972157
    invoke-static {p1, p2}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v0

    .line 1972158
    invoke-static {v0}, LX/5O7;->a(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, LX/DB4;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1972159
    iget-object v1, p0, LX/DB4;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/5O7;->a(Ljava/util/Date;J)Ljava/lang/String;

    move-result-object v0

    .line 1972160
    iget-object v1, p0, LX/DB4;->a:Landroid/content/Context;

    const v2, 0x7f082f4c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1972161
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DB4;->a:Landroid/content/Context;

    const v1, 0x7f082f4d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/events/model/Event;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1972162
    iget-boolean v1, p1, Lcom/facebook/events/model/Event;->z:Z

    move v1, v1

    .line 1972163
    if-eqz v1, :cond_1

    .line 1972164
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1972165
    iget-wide v5, p1, Lcom/facebook/events/model/Event;->A:J

    move-wide v2, v5

    .line 1972166
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/DB4;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 1972167
    :cond_0
    :goto_0
    return-object v0

    .line 1972168
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->av()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1972169
    iget-object v1, p1, Lcom/facebook/events/model/Event;->v:Ljava/lang/String;

    move-object v1, v1

    .line 1972170
    iget-object v2, p0, LX/DB4;->b:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 1972171
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1972172
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_e

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 1972173
    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->HOST:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v1, v2, :cond_3

    .line 1972174
    :cond_2
    iget-object v0, p0, LX/DB4;->a:Landroid/content/Context;

    const v1, 0x7f082f3c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1972175
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->ap()Ljava/lang/String;

    move-result-object v1

    .line 1972176
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1972177
    iget-object v0, p1, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-object v0, v0

    .line 1972178
    iget-object v2, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v2, v2

    .line 1972179
    invoke-virtual {p0, v1, v0, v2}, LX/DB4;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventActionStyle;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1972180
    :cond_4
    iget v1, p1, Lcom/facebook/events/model/Event;->ae:I

    move v1, v1

    .line 1972181
    iget-object v2, p1, Lcom/facebook/events/model/Event;->ad:Ljava/lang/String;

    move-object v2, v2

    .line 1972182
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 1972183
    if-lez v1, :cond_10

    .line 1972184
    if-ne v1, v8, :cond_f

    .line 1972185
    iget-object v3, p0, LX/DB4;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082f47

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v2, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1972186
    :goto_2
    move-object v1, v3

    .line 1972187
    iget-boolean v2, p0, LX/DB4;->c:Z

    if-eqz v2, :cond_6

    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1972188
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    move-object v0, v1

    .line 1972189
    goto :goto_0

    .line 1972190
    :cond_5
    iget v1, p1, Lcom/facebook/events/model/Event;->ai:I

    move v1, v1

    .line 1972191
    iget-object v2, p1, Lcom/facebook/events/model/Event;->ah:Ljava/lang/String;

    move-object v2, v2

    .line 1972192
    const v3, 0x7f082f48

    const v4, 0x7f0f014e

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 1972193
    if-lez v1, :cond_13

    .line 1972194
    if-ne v1, v9, :cond_12

    .line 1972195
    iget-object v5, p0, LX/DB4;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-virtual {v5, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1972196
    :goto_3
    move-object v3, v5

    .line 1972197
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 1972198
    :goto_4
    move-object v1, v3

    .line 1972199
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    move-object v0, v1

    .line 1972200
    goto/16 :goto_0

    .line 1972201
    :cond_6
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    move-object v0, v1

    .line 1972202
    goto/16 :goto_0

    .line 1972203
    :cond_7
    iget v1, p1, Lcom/facebook/events/model/Event;->ag:I

    move v1, v1

    .line 1972204
    iget-object v2, p1, Lcom/facebook/events/model/Event;->af:Ljava/lang/String;

    move-object v2, v2

    .line 1972205
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 1972206
    if-lez v1, :cond_15

    .line 1972207
    if-ne v1, v8, :cond_14

    .line 1972208
    iget-object v3, p0, LX/DB4;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082f4a

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v2, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1972209
    :goto_5
    move-object v1, v3

    .line 1972210
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    move-object v0, v1

    .line 1972211
    goto/16 :goto_0

    .line 1972212
    :cond_8
    iget-boolean v1, p0, LX/DB4;->c:Z

    if-eqz v1, :cond_9

    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1972213
    iget-object v1, p1, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v1, v1

    .line 1972214
    if-eqz v1, :cond_a

    .line 1972215
    iget-object v1, p1, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v1, v1

    .line 1972216
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v1, v2, :cond_16

    .line 1972217
    iget-object v2, p0, LX/DB4;->a:Landroid/content/Context;

    const v3, 0x7f082f3e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1972218
    :goto_6
    move-object v1, v2

    .line 1972219
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    move-object v0, v1

    .line 1972220
    goto/16 :goto_0

    .line 1972221
    :cond_9
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->c()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1972222
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    .line 1972223
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v1, v2, :cond_18

    .line 1972224
    iget-object v2, p0, LX/DB4;->a:Landroid/content/Context;

    const v3, 0x7f082f3e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1972225
    :goto_7
    move-object v1, v2

    .line 1972226
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    move-object v0, v1

    .line 1972227
    goto/16 :goto_0

    .line 1972228
    :cond_a
    iget-object v1, p1, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    move-object v1, v1

    .line 1972229
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1972230
    iget-object v0, p1, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    move-object v0, v0

    .line 1972231
    iget-object v1, p0, LX/DB4;->a:Landroid/content/Context;

    const v2, 0x7f082f4b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1972232
    goto/16 :goto_0

    .line 1972233
    :cond_b
    iget-boolean v1, p1, Lcom/facebook/events/model/Event;->E:Z

    move v1, v1

    .line 1972234
    if-eqz v1, :cond_c

    .line 1972235
    iget-object v0, p0, LX/DB4;->a:Landroid/content/Context;

    const v1, 0x7f082f41

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1972236
    :cond_c
    iget-object v1, p1, Lcom/facebook/events/model/Event;->ao:Ljava/lang/String;

    move-object v1, v1

    .line 1972237
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 1972238
    iget-object v0, p0, LX/DB4;->a:Landroid/content/Context;

    const v1, 0x7f082f3b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 1972239
    iget-object v4, p1, Lcom/facebook/events/model/Event;->ao:Ljava/lang/String;

    move-object v4, v4

    .line 1972240
    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1972241
    :cond_d
    iget-object v1, p1, Lcom/facebook/events/model/Event;->am:Ljava/lang/String;

    move-object v1, v1

    .line 1972242
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1972243
    iget-object v0, p1, Lcom/facebook/events/model/Event;->am:Ljava/lang/String;

    move-object v0, v0

    .line 1972244
    goto/16 :goto_0

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1972245
    :cond_f
    add-int/lit8 v3, v1, -0x1

    .line 1972246
    iget-object v4, p0, LX/DB4;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f014d

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 1972247
    :cond_10
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 1972248
    :cond_12
    add-int/lit8 v5, v1, -0x1

    .line 1972249
    iget-object v6, p0, LX/DB4;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v6, v4, v5, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_3

    .line 1972250
    :cond_13
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 1972251
    :cond_14
    add-int/lit8 v3, v1, -0x1

    .line 1972252
    iget-object v4, p0, LX/DB4;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0150

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 1972253
    :cond_15
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 1972254
    :cond_16
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v1, v2, :cond_17

    .line 1972255
    iget-object v2, p0, LX/DB4;->a:Landroid/content/Context;

    const v3, 0x7f082f3f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 1972256
    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 1972257
    :cond_18
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v1, v2, :cond_19

    .line 1972258
    iget-object v2, p0, LX/DB4;->a:Landroid/content/Context;

    const v3, 0x7f082f3d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7

    .line 1972259
    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_7
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventActionStyle;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Ljava/lang/String;
    .locals 4
    .param p2    # Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1972260
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SEND:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    if-ne p2, v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne p3, v0, :cond_1

    .line 1972261
    iget-object v0, p0, LX/DB4;->a:Landroid/content/Context;

    const v1, 0x7f082f43

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1972262
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/DB4;->a:Landroid/content/Context;

    const v1, 0x7f082f42

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
