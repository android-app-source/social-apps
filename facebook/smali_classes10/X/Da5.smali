.class public LX/Da5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/Da6;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Da5;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2014542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Da5;
    .locals 3

    .prologue
    .line 2014495
    sget-object v0, LX/Da5;->a:LX/Da5;

    if-nez v0, :cond_1

    .line 2014496
    const-class v1, LX/Da5;

    monitor-enter v1

    .line 2014497
    :try_start_0
    sget-object v0, LX/Da5;->a:LX/Da5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2014498
    if-eqz v2, :cond_0

    .line 2014499
    :try_start_1
    new-instance v0, LX/Da5;

    invoke-direct {v0}, LX/Da5;-><init>()V

    .line 2014500
    move-object v0, v0

    .line 2014501
    sput-object v0, LX/Da5;->a:LX/Da5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2014502
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2014503
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2014504
    :cond_1
    sget-object v0, LX/Da5;->a:LX/Da5;

    return-object v0

    .line 2014505
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2014506
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2014509
    check-cast p1, LX/Da6;

    .line 2014510
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2014511
    const-string v0, "["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2014512
    const/4 v0, 0x0

    move v1, v0

    .line 2014513
    :goto_0
    iget-object v0, p1, LX/Da6;->b:LX/0Px;

    move-object v0, v0

    .line 2014514
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2014515
    iget-object v0, p1, LX/Da6;->b:LX/0Px;

    move-object v0, v0

    .line 2014516
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2014517
    iget-object v0, p1, LX/Da6;->b:LX/0Px;

    move-object v0, v0

    .line 2014518
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_0

    .line 2014519
    const-string v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2014520
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2014521
    :cond_1
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2014522
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2014523
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "name"

    .line 2014524
    iget-object v4, p1, LX/Da6;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2014525
    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2014526
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "recipients"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2014527
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "createGroupSideConversation"

    .line 2014528
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2014529
    move-object v1, v1

    .line 2014530
    const-string v2, "POST"

    .line 2014531
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2014532
    move-object v1, v1

    .line 2014533
    const-string v2, "me/group_threads"

    .line 2014534
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2014535
    move-object v1, v1

    .line 2014536
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2014537
    move-object v0, v1

    .line 2014538
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2014539
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2014540
    move-object v0, v0

    .line 2014541
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2014507
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2014508
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
