.class public final LX/Drn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:Ljava/lang/Runnable;

.field public final synthetic b:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2049675
    iput-object p1, p0, LX/Drn;->b:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iput-object p2, p0, LX/Drn;->a:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2049676
    iget-object v0, p0, LX/Drn;->b:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->d:LX/03V;

    sget-object v1, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failure syncing notifications: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2049677
    iget-object v0, p0, LX/Drn;->b:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->u:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2049678
    iget-object v0, p0, LX/Drn;->b:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->v:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/Drn;->a:Ljava/lang/Runnable;

    const v2, 0x49ca0523

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2049679
    :goto_0
    return-void

    .line 2049680
    :cond_0
    iget-object v0, p0, LX/Drn;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2049681
    iget-object v0, p0, LX/Drn;->b:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->u:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2049682
    iget-object v0, p0, LX/Drn;->b:Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;

    iget-object v0, v0, Lcom/facebook/notifications/tray/SystemTrayNotificationHelper;->v:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/Drn;->a:Ljava/lang/Runnable;

    const v2, 0x371af324

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2049683
    :goto_0
    return-void

    .line 2049684
    :cond_0
    iget-object v0, p0, LX/Drn;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
