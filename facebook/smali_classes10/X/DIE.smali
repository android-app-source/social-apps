.class public final LX/DIE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/107;


# instance fields
.field public final synthetic a:Lcom/facebook/goodfriends/audience/AudienceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V
    .locals 0

    .prologue
    .line 1983114
    iput-object p1, p0, LX/DIE;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 11

    .prologue
    .line 1983115
    iget-object v0, p0, LX/DIE;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-boolean v0, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DIE;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v0, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    .line 1983116
    iget v1, v0, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->o:I

    move v0, v1

    .line 1983117
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 1983118
    iget-object v0, p0, LX/DIE;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    const/4 v1, 0x0

    .line 1983119
    iput-boolean v1, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->p:Z

    .line 1983120
    iget-object v0, p0, LX/DIE;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/DIE;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v1, v1, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    .line 1983121
    iget v2, v1, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->o:I

    move v1, v2

    .line 1983122
    new-instance v2, LX/DID;

    invoke-direct {v2, p0}, LX/DID;-><init>(LX/DIE;)V

    const/4 p1, 0x0

    const/4 p0, 0x1

    const/4 v10, 0x0

    const/4 v9, -0x1

    const/4 v8, -0x2

    .line 1983123
    new-instance v3, LX/4me;

    invoke-direct {v3, v0}, LX/4me;-><init>(Landroid/content/Context;)V

    .line 1983124
    if-nez v1, :cond_1

    .line 1983125
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0823b5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4me;->setTitle(Ljava/lang/CharSequence;)V

    .line 1983126
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0823b6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1983127
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0823b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v9, v4, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1983128
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0823b8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v8, v4, p1}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1983129
    :goto_0
    invoke-virtual {v3}, LX/4me;->show()V

    .line 1983130
    :goto_1
    return-void

    .line 1983131
    :cond_0
    iget-object v0, p0, LX/DIE;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v0, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->h:Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;

    iget-object v1, p0, LX/DIE;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v1, v1, Lcom/facebook/goodfriends/audience/AudienceFragment;->g:LX/DIB;

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/audience/AudienceRecyclerAdapter;->a(LX/DIB;)V

    goto :goto_1

    .line 1983132
    :cond_1
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0116

    new-array v6, p0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4me;->setTitle(Ljava/lang/CharSequence;)V

    .line 1983133
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0823b7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1983134
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0115

    new-array v6, p0, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v9, v4, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1983135
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0823ba

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v8, v4, p1}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0
.end method
