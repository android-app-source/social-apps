.class public LX/D9h;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/0wT;

.field private static b:LX/0wT;


# instance fields
.field public final c:LX/3Nk;

.field public final d:LX/0wd;

.field public final e:LX/0wd;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/D9n;

.field public h:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1970411
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/D9h;->a:LX/0wT;

    .line 1970412
    const-wide v0, 0x406b800000000000L    # 220.0

    const-wide/high16 v2, 0x402e000000000000L    # 15.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/D9h;->b:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/3Nk;LX/0wW;)V
    .locals 7
    .param p1    # LX/3Nk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0wW;
        .annotation runtime Lcom/facebook/messaging/chatheads/annotations/ForChatHeads;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide v4, 0x3fd3333333333333L    # 0.3

    .line 1970413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970414
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/D9h;->f:Ljava/util/Set;

    .line 1970415
    iput-object p1, p0, LX/D9h;->c:LX/3Nk;

    .line 1970416
    new-instance v0, LX/D9g;

    invoke-direct {v0, p0}, LX/D9g;-><init>(LX/D9h;)V

    .line 1970417
    invoke-virtual {p2}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/D9h;->a:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    .line 1970418
    iput-wide v4, v1, LX/0wd;->k:D

    .line 1970419
    move-object v1, v1

    .line 1970420
    iput-wide v4, v1, LX/0wd;->l:D

    .line 1970421
    move-object v1, v1

    .line 1970422
    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/D9h;->d:LX/0wd;

    .line 1970423
    invoke-virtual {p2}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/D9h;->a:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    .line 1970424
    iput-wide v4, v1, LX/0wd;->k:D

    .line 1970425
    move-object v1, v1

    .line 1970426
    iput-wide v4, v1, LX/0wd;->l:D

    .line 1970427
    move-object v1, v1

    .line 1970428
    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/D9h;->e:LX/0wd;

    .line 1970429
    return-void
.end method

.method public static b(LX/D9h;)V
    .locals 2

    .prologue
    .line 1970430
    iget-object v0, p0, LX/D9h;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 1970431
    :cond_0
    return-void
.end method
