.class public final LX/D5k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BV4;


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V
    .locals 0

    .prologue
    .line 1963829
    iput-object p1, p0, LX/D5k;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1963830
    iget-object v0, p0, LX/D5k;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    if-nez v0, :cond_1

    .line 1963831
    :cond_0
    :goto_0
    return-void

    .line 1963832
    :cond_1
    const/4 v0, 0x0

    .line 1963833
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1963834
    iget-object v3, p0, LX/D5k;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v3, v3, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->d:LX/D6S;

    const/4 v7, 0x0

    .line 1963835
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    .line 1963836
    :cond_2
    :goto_2
    move v0, v7

    .line 1963837
    or-int/2addr v0, v1

    move v1, v0

    .line 1963838
    goto :goto_1

    .line 1963839
    :cond_3
    if-eqz v1, :cond_0

    .line 1963840
    iget-object v0, p0, LX/D5k;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ak:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    goto :goto_0

    .line 1963841
    :cond_4
    invoke-static {v0}, LX/3ha;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1963842
    const/4 v4, 0x0

    .line 1963843
    const/4 v5, 0x0

    move v6, v5

    :goto_3
    iget-object v5, v3, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v6, v5, :cond_d

    .line 1963844
    iget-object v5, v3, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963845
    iget-object v7, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v7

    .line 1963846
    check-cast v5, Lcom/facebook/graphql/model/FeedUnit;

    .line 1963847
    instance-of v7, v5, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    if-eqz v7, :cond_c

    .line 1963848
    check-cast v5, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    .line 1963849
    iget-object v6, v5, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v5, v6

    .line 1963850
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 1963851
    :goto_4
    move-object v5, v5

    .line 1963852
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 1963853
    :cond_5
    :goto_5
    move v7, v4

    .line 1963854
    goto :goto_2

    :cond_6
    move v6, v7

    .line 1963855
    :goto_6
    iget-object v4, v3, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v6, v4, :cond_2

    .line 1963856
    iget-object v4, v3, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963857
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1963858
    check-cast v5, Lcom/facebook/graphql/model/FeedUnit;

    .line 1963859
    instance-of v8, v5, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v8, :cond_8

    .line 1963860
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1963861
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_8

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1963862
    iget-object v7, v3, LX/D6S;->a:Ljava/util/List;

    invoke-virtual {v4, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-interface {v7, v6, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1963863
    iget-object v4, v3, LX/D6S;->b:Ljava/util/Set;

    invoke-static {v5}, LX/D6S;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1963864
    invoke-static {v0}, LX/D6S;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    .line 1963865
    if-eqz v4, :cond_7

    .line 1963866
    iget-object v6, v3, LX/D6S;->b:Ljava/util/Set;

    invoke-interface {v6, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1963867
    :cond_7
    const/4 v7, 0x1

    goto/16 :goto_2

    .line 1963868
    :cond_8
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_6

    :cond_9
    move v5, v4

    .line 1963869
    :goto_7
    iget-object v4, v3, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_b

    .line 1963870
    iget-object v4, v3, LX/D6S;->a:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963871
    iget-object v6, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v6

    .line 1963872
    check-cast v4, Lcom/facebook/graphql/model/FeedUnit;

    .line 1963873
    instance-of v6, v4, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    if-eqz v6, :cond_a

    .line 1963874
    check-cast v4, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    .line 1963875
    iget-object v6, v4, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v6, v6

    .line 1963876
    invoke-static {v6}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v6

    .line 1963877
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v7

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    .line 1963878
    iput-object v6, v7, LX/23u;->k:LX/0Px;

    .line 1963879
    move-object v6, v7

    .line 1963880
    invoke-virtual {v6}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    .line 1963881
    new-instance v7, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;

    iget-object v8, v4, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;->b:LX/D6N;

    invoke-direct {v7, v6, v8}, Lcom/facebook/video/channelfeed/MultiShareChannelStoryUnit;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/D6N;)V

    move-object v4, v7

    .line 1963882
    iget-object v6, v3, LX/D6S;->a:Ljava/util/List;

    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    invoke-interface {v6, v5, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1963883
    :cond_a
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_7

    .line 1963884
    :cond_b
    const/4 v4, 0x1

    goto/16 :goto_5

    .line 1963885
    :cond_c
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto/16 :goto_3

    .line 1963886
    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_4
.end method
