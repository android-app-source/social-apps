.class public LX/DF6;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DF4;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DF7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1977671
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DF6;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DF7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1977683
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1977684
    iput-object p1, p0, LX/DF6;->b:LX/0Ot;

    .line 1977685
    return-void
.end method

.method public static a(LX/0QB;)LX/DF6;
    .locals 4

    .prologue
    .line 1977686
    const-class v1, LX/DF6;

    monitor-enter v1

    .line 1977687
    :try_start_0
    sget-object v0, LX/DF6;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1977688
    sput-object v2, LX/DF6;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1977689
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977690
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1977691
    new-instance v3, LX/DF6;

    const/16 p0, 0x20db

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DF6;-><init>(LX/0Ot;)V

    .line 1977692
    move-object v0, v3

    .line 1977693
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1977694
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DF6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1977695
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1977696
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1977674
    iget-object v0, p0, LX/DF6;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DF7;

    .line 1977675
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const v1, 0x7f020a3d

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b0f68

    invoke-interface {v0, v1}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b0f67

    invoke-interface {v0, v1}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0a0048

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    const/16 p2, 0x37

    .line 1977676
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0f68

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0f69

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    const p0, 0x103005b

    invoke-static {p1, v3, p0}, LX/5Jt;->a(LX/1De;II)LX/5Jr;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const p0, 0x7f081869

    invoke-interface {v3, p0}, LX/1Di;->A(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    const/4 p0, 0x2

    invoke-interface {v3, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    move-object v2, v2

    .line 1977677
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 1977678
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v2

    const v3, 0x7f0a0464

    invoke-virtual {v2, v3}, LX/25Q;->i(I)LX/25Q;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x4

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    move-object v2, v2

    .line 1977679
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const/4 p2, 0x1

    .line 1977680
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0f68

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const p0, 0x7f081869

    invoke-virtual {v3, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    const p0, 0x7f0b0052

    invoke-virtual {v3, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const p0, 0x7f0a010c

    invoke-virtual {v3, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const p0, 0x7f081869

    invoke-interface {v3, p0}, LX/1Di;->A(I)LX/1Di;

    move-result-object v3

    const/4 p0, 0x2

    invoke-interface {v3, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    move-object v2, v2

    .line 1977681
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1977682
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1977672
    invoke-static {}, LX/1dS;->b()V

    .line 1977673
    const/4 v0, 0x0

    return-object v0
.end method
