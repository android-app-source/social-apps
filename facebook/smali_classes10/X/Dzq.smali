.class public final LX/Dzq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;",
        ">;",
        "LX/0am",
        "<",
        "Landroid/location/Location;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dzr;


# direct methods
.method public constructor <init>(LX/Dzr;)V
    .locals 0

    .prologue
    .line 2067479
    iput-object p1, p0, LX/Dzq;->a:LX/Dzr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2067480
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const v4, -0x2c0b8ec2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2067481
    if-eqz p1, :cond_0

    .line 2067482
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2067483
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 2067484
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 2067485
    :goto_2
    return-object v0

    .line 2067486
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2067487
    check-cast v0, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;

    invoke-virtual {v0}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2067488
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2067489
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2067490
    check-cast v0, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;

    invoke-virtual {v0}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2067491
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2067492
    :cond_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2067493
    check-cast v0, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;

    invoke-virtual {v0}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v1}, LX/15i;->g(II)I

    move-result v0

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2067494
    new-instance v4, Landroid/location/Location;

    const-string v5, ""

    invoke-direct {v4, v5}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2067495
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setLatitude(D)V

    .line 2067496
    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 2067497
    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_2

    .line 2067498
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
