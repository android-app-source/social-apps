.class public LX/Dr4;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/33W;

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Dr5;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field public final d:LX/0wM;

.field public final e:LX/Dq0;

.field public final f:Ljava/lang/String;

.field public final g:LX/33a;

.field public final h:LX/2nq;

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/BCR;Landroid/content/Context;LX/2nq;Ljava/lang/String;LX/Dq0;LX/33W;LX/0wM;LX/33a;)V
    .locals 1
    .param p1    # LX/BCR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/2nq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/Dq0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048873
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2048874
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Dr4;->i:Z

    .line 2048875
    iput-object p6, p0, LX/Dr4;->a:LX/33W;

    .line 2048876
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Dr4;->b:Ljava/util/ArrayList;

    .line 2048877
    iput-object p2, p0, LX/Dr4;->c:Landroid/content/Context;

    .line 2048878
    iput-object p3, p0, LX/Dr4;->h:LX/2nq;

    .line 2048879
    iput-object p4, p0, LX/Dr4;->f:Ljava/lang/String;

    .line 2048880
    iput-object p7, p0, LX/Dr4;->d:LX/0wM;

    .line 2048881
    iput-object p8, p0, LX/Dr4;->g:LX/33a;

    .line 2048882
    iput-object p5, p0, LX/Dr4;->e:LX/Dq0;

    .line 2048883
    invoke-interface {p1}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2048884
    :cond_0
    return-void

    .line 2048885
    :cond_1
    invoke-interface {p1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Dr4;->j:Ljava/lang/String;

    .line 2048886
    invoke-interface {p1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->id_()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->id_()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2048887
    :cond_2
    :goto_0
    const/4 v0, 0x0

    move p2, v0

    :goto_1
    invoke-interface {p1}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 2048888
    invoke-interface {p1}, LX/BCR;->c()LX/BCP;

    move-result-object v0

    invoke-interface {v0}, LX/BCP;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BCO;

    .line 2048889
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object p3

    if-eqz p3, :cond_5

    .line 2048890
    invoke-interface {v0}, LX/BCO;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object p3

    .line 2048891
    invoke-virtual {p3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object p4

    if-eqz p4, :cond_5

    .line 2048892
    new-instance p4, LX/Dr5;

    invoke-virtual {p3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->k()LX/4aa;

    move-result-object p5

    invoke-interface {p5}, LX/4aa;->a()Ljava/lang/String;

    move-result-object p5

    invoke-direct {p4, p5}, LX/Dr5;-><init>(Ljava/lang/String;)V

    .line 2048893
    invoke-virtual {p3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ic_()LX/174;

    move-result-object p5

    if-eqz p5, :cond_3

    .line 2048894
    invoke-virtual {p3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;->ic_()LX/174;

    move-result-object p3

    invoke-interface {p3}, LX/174;->a()Ljava/lang/String;

    move-result-object p3

    .line 2048895
    iput-object p3, p4, LX/Dr5;->c:Ljava/lang/String;

    .line 2048896
    :cond_3
    iput-object v0, p4, LX/Dr5;->b:LX/BCO;

    .line 2048897
    iget-object p3, p0, LX/Dr4;->j:Ljava/lang/String;

    if-eqz p3, :cond_4

    iget-object p3, p0, LX/Dr4;->j:Ljava/lang/String;

    invoke-interface {v0}, LX/BCO;->c()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p3, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_4

    .line 2048898
    const/4 p3, 0x1

    .line 2048899
    iput-boolean p3, p4, LX/Dr5;->a:Z

    .line 2048900
    invoke-interface {v0}, LX/BCO;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Dr4;->k:Ljava/lang/String;

    .line 2048901
    :cond_4
    iget-object v0, p0, LX/Dr4;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2048902
    :cond_5
    add-int/lit8 v0, p2, 0x1

    move p2, v0

    goto :goto_1

    .line 2048903
    :cond_6
    new-instance v0, LX/Dr5;

    invoke-interface {p1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->id_()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->a()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p2}, LX/Dr5;-><init>(Ljava/lang/String;)V

    .line 2048904
    invoke-interface {p1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->d()LX/174;

    move-result-object p2

    if-eqz p2, :cond_7

    .line 2048905
    invoke-interface {p1}, LX/BCR;->d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;->d()LX/174;

    move-result-object p2

    invoke-interface {p2}, LX/174;->a()Ljava/lang/String;

    move-result-object p2

    .line 2048906
    iput-object p2, v0, LX/Dr5;->c:Ljava/lang/String;

    .line 2048907
    :cond_7
    iget-object p2, p0, LX/Dr4;->b:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2048908
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Dr4;->i:Z

    goto/16 :goto_0
.end method

.method private e(I)LX/Dr5;
    .locals 1

    .prologue
    .line 2048922
    iget-object v0, p0, LX/Dr4;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dr5;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2048913
    iget-object v0, p0, LX/Dr4;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2048914
    if-nez p2, :cond_0

    .line 2048915
    const v1, 0x7f030c24

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2048916
    new-instance v0, LX/Dr3;

    invoke-direct {v0, v1}, LX/Dr3;-><init>(Landroid/view/View;)V

    .line 2048917
    :goto_0
    return-object v0

    .line 2048918
    :cond_0
    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    .line 2048919
    const v1, 0x7f030c23

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2048920
    new-instance v1, LX/Dr2;

    invoke-direct {v1, p0, v0}, LX/Dr2;-><init>(LX/Dr4;Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V

    move-object v0, v1

    goto :goto_0

    .line 2048921
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view type for binding view holder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2048923
    instance-of v0, p1, LX/Dr3;

    if-eqz v0, :cond_1

    .line 2048924
    check-cast p1, LX/Dr3;

    .line 2048925
    invoke-direct {p0, p2}, LX/Dr4;->e(I)LX/Dr5;

    move-result-object v0

    .line 2048926
    iget-object p0, p1, LX/Dr3;->l:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2048927
    iget-object p2, v0, LX/Dr5;->d:Ljava/lang/String;

    move-object p2, p2

    .line 2048928
    invoke-virtual {p0, p2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2048929
    iget-object p0, p1, LX/Dr3;->m:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2048930
    iget-object p2, v0, LX/Dr5;->c:Ljava/lang/String;

    move-object p2, p2

    .line 2048931
    invoke-virtual {p0, p2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2048932
    :cond_0
    :goto_0
    return-void

    .line 2048933
    :cond_1
    instance-of v0, p1, LX/Dr2;

    if-eqz v0, :cond_0

    .line 2048934
    check-cast p1, LX/Dr2;

    .line 2048935
    invoke-direct {p0, p2}, LX/Dr4;->e(I)LX/Dr5;

    move-result-object v0

    .line 2048936
    iget-object v1, p1, LX/Dr2;->m:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2048937
    iget-object v2, v0, LX/Dr5;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2048938
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2048939
    iget-object v1, p1, LX/Dr2;->m:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2048940
    iget-object v2, v0, LX/Dr5;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2048941
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2048942
    iget-boolean v1, v0, LX/Dr5;->a:Z

    move v1, v1

    .line 2048943
    if-eqz v1, :cond_2

    .line 2048944
    iget-object v1, p1, LX/Dr2;->m:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iget-object v2, p1, LX/Dr2;->l:LX/Dr4;

    iget-object v2, v2, LX/Dr4;->d:LX/0wM;

    const p0, 0x7f0207d6

    const p2, -0xa76f01

    invoke-virtual {v2, p0, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2048945
    :goto_1
    iget-object v1, p1, LX/Dr2;->m:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    new-instance v2, LX/Dr1;

    invoke-direct {v2, p1, v0}, LX/Dr1;-><init>(LX/Dr2;LX/Dr5;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2048946
    goto :goto_0

    .line 2048947
    :cond_2
    iget-object v1, p1, LX/Dr2;->m:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2048910
    iget-boolean v0, p0, LX/Dr4;->i:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 2048911
    const/4 v0, 0x0

    .line 2048912
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2048909
    iget-object v0, p0, LX/Dr4;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
