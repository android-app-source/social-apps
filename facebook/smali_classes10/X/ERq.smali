.class public LX/ERq;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:LX/11H;

.field private final e:LX/ERa;

.field private final f:LX/2TN;

.field private final g:LX/2TM;

.field private final h:LX/43C;

.field private final i:LX/2TL;

.field private final j:LX/2TK;

.field public final k:LX/ERr;

.field private final l:LX/ES8;

.field private final m:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2121661
    const-class v0, LX/ERq;

    .line 2121662
    sput-object v0, LX/ERq;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ERq;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/11H;LX/ERa;LX/2TN;LX/43C;LX/2TL;LX/2TK;LX/2TM;LX/ERr;LX/ES8;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2121629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121630
    iput-object p1, p0, LX/ERq;->c:Landroid/content/Context;

    .line 2121631
    iput-object p2, p0, LX/ERq;->d:LX/11H;

    .line 2121632
    iput-object p3, p0, LX/ERq;->e:LX/ERa;

    .line 2121633
    iput-object p4, p0, LX/ERq;->f:LX/2TN;

    .line 2121634
    iput-object p5, p0, LX/ERq;->h:LX/43C;

    .line 2121635
    iput-object p6, p0, LX/ERq;->i:LX/2TL;

    .line 2121636
    iput-object p7, p0, LX/ERq;->j:LX/2TK;

    .line 2121637
    iput-object p8, p0, LX/ERq;->g:LX/2TM;

    .line 2121638
    iput-object p9, p0, LX/ERq;->k:LX/ERr;

    .line 2121639
    iput-object p10, p0, LX/ERq;->l:LX/ES8;

    .line 2121640
    iput-object p11, p0, LX/ERq;->m:LX/03V;

    .line 2121641
    return-void
.end method

.method private static a(LX/ERq;Ljava/lang/String;Lcom/facebook/vault/provider/VaultImageProviderRow;Z)Ljava/io/File;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2121642
    if-eqz p3, :cond_0

    iget v1, p2, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    if-eq v1, v2, :cond_0

    .line 2121643
    const/16 v1, 0x800

    move v2, v1

    .line 2121644
    :goto_0
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2121645
    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-nez v1, :cond_2

    .line 2121646
    :goto_1
    return-object v0

    .line 2121647
    :cond_0
    if-nez p3, :cond_1

    iget v1, p2, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    if-eq v1, v2, :cond_1

    iget v1, p2, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    if-eqz v1, :cond_1

    .line 2121648
    const/16 v1, 0x3c0

    move v2, v1

    .line 2121649
    goto :goto_0

    .line 2121650
    :cond_1
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_1

    .line 2121651
    :cond_2
    :try_start_1
    const-string v1, "resized_"

    const-string v3, ""

    iget-object v4, p0, LX/ERq;->c:Landroid/content/Context;

    .line 2121652
    const-string v5, "vault_temp"

    const/4 p2, 0x0

    invoke-virtual {v4, v5, p2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    move-object v4, v5

    .line 2121653
    invoke-static {v1, v3, v4}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    .line 2121654
    new-instance v3, LX/43G;

    const/16 v4, 0x55

    invoke-direct {v3, v2, v2, v4}, LX/43G;-><init>(III)V

    .line 2121655
    iget-object v2, p0, LX/ERq;->h:LX/43C;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, p1, v4, v3}, LX/43C;->a(Ljava/lang/String;Ljava/lang/String;LX/43G;)LX/43G;
    :try_end_1
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 2121656
    goto :goto_1

    .line 2121657
    :catch_0
    move-exception v1

    .line 2121658
    iget-object v2, p0, LX/ERq;->m:LX/03V;

    const-string v3, "vault_image_upload_resize exception"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "resize of "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/bitmaps/ImageResizer$ImageResizingException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2121659
    :catch_1
    move-exception v1

    .line 2121660
    iget-object v2, p0, LX/ERq;->m:LX/03V;

    const-string v3, "vault_image_upload_resize exception"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "resize of "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method

.method private static a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 2121663
    iput v7, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->h:I

    .line 2121664
    invoke-virtual {p1}, Lcom/facebook/vault/provider/VaultImageProviderRow;->b()Landroid/content/ContentValues;

    move-result-object v0

    .line 2121665
    new-array v1, v5, [Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    aput-object v2, v1, v7

    .line 2121666
    iget-object v2, p0, LX/ERq;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, LX/DbE;->a:Landroid/net/Uri;

    const-string v4, "%s = ?"

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, LX/DbD;->a:LX/0U1;

    .line 2121667
    iget-object v8, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v8

    .line 2121668
    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2121669
    invoke-static {p0, p1}, LX/ERq;->b(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;)V

    .line 2121670
    return-void
.end method

.method private static a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;JZJJLjava/lang/String;)V
    .locals 9

    .prologue
    .line 2121607
    iget-object v0, p0, LX/ERq;->k:LX/ERr;

    const/16 v1, 0x64

    invoke-virtual {v0, p1, v1}, LX/ERr;->a(Lcom/facebook/vault/provider/VaultImageProviderRow;I)V

    .line 2121608
    iput-wide p2, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    .line 2121609
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->d:J

    .line 2121610
    iget v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    if-nez v0, :cond_0

    if-eqz p4, :cond_0

    const/4 v6, 0x1

    .line 2121611
    :goto_0
    if-eqz p4, :cond_1

    .line 2121612
    const/4 v0, 0x1

    iput v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    .line 2121613
    :goto_1
    const/4 v0, 0x0

    iput v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->h:I

    .line 2121614
    invoke-virtual {p1}, Lcom/facebook/vault/provider/VaultImageProviderRow;->b()Landroid/content/ContentValues;

    move-result-object v0

    .line 2121615
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 2121616
    iget-object v2, p0, LX/ERq;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, LX/DbE;->a:Landroid/net/Uri;

    const-string v4, "%s = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, LX/DbD;->a:LX/0U1;

    invoke-virtual {v8}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2121617
    iget-object v0, p0, LX/ERq;->g:LX/2TM;

    move-object v1, p1

    move-wide v2, p5

    move-wide/from16 v4, p7

    move-object/from16 v7, p9

    invoke-virtual/range {v0 .. v7}, LX/2TM;->a(Lcom/facebook/vault/provider/VaultImageProviderRow;JJZLjava/lang/String;)V

    .line 2121618
    invoke-static {p0, p1}, LX/ERq;->b(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;)V

    .line 2121619
    return-void

    .line 2121620
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 2121621
    :cond_1
    const/4 v0, 0x0

    iput v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    goto :goto_1
.end method

.method private static a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 9

    .prologue
    .line 2121622
    iget v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    .line 2121623
    iget v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 2121624
    iget-object v0, p0, LX/ERq;->g:LX/2TM;

    invoke-virtual {v0, p1, p5, p6}, LX/2TM;->b(Lcom/facebook/vault/provider/VaultImageProviderRow;J)V

    .line 2121625
    const/4 v0, 0x6

    iput v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    .line 2121626
    :cond_0
    iget-object v1, p0, LX/ERq;->g:LX/2TM;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, LX/2TM;->a(Lcom/facebook/vault/provider/VaultImageProviderRow;Ljava/lang/String;JJLjava/lang/String;)V

    .line 2121627
    invoke-static {p0, p1}, LX/ERq;->a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;)V

    .line 2121628
    return-void
.end method

.method private static a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;ZJ)V
    .locals 9

    .prologue
    .line 2121586
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2121587
    invoke-virtual {p1}, Lcom/facebook/vault/provider/VaultImageProviderRow;->b()Landroid/content/ContentValues;

    move-result-object v0

    .line 2121588
    iget-object v1, p0, LX/ERq;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, LX/DbE;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2121589
    iget-object v0, p0, LX/ERq;->g:LX/2TM;

    .line 2121590
    sget-object v1, LX/2TM;->e:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2121591
    const-string v1, "img_hash"

    iget-object v3, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2121592
    const-string v3, "resolution"

    if-eqz p2, :cond_1

    const-string v1, "high"

    :goto_0
    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2121593
    const-string v1, "file_size"

    invoke-virtual {v2, v1, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2121594
    invoke-static {v0, v2}, LX/2TM;->a(LX/2TM;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2121595
    iget-object v1, v0, LX/2TM;->y:LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2121596
    iget v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->e:I

    if-nez v0, :cond_0

    .line 2121597
    iget-object v0, p0, LX/ERq;->g:LX/2TM;

    .line 2121598
    sget-object v3, LX/2TM;->h:Ljava/lang/String;

    invoke-static {v0, v3}, LX/2TM;->e(LX/2TM;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2121599
    const-string v4, "img_hash"

    iget-object v5, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2121600
    const-string v4, "time_to_first_sync"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->c:J

    sub-long/2addr v5, v7

    invoke-virtual {v3, v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2121601
    const-string v4, "file_size"

    invoke-virtual {v3, v4, p3, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2121602
    invoke-static {v0, v3}, LX/2TM;->a(LX/2TM;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2121603
    iget-object v4, v0, LX/2TM;->y:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2121604
    :cond_0
    iget-object v0, p0, LX/ERq;->k:LX/ERr;

    invoke-virtual {v0, p1}, LX/ERr;->a(Lcom/facebook/vault/provider/VaultImageProviderRow;)V

    .line 2121605
    return-void

    .line 2121606
    :cond_1
    const-string v1, "low"

    goto :goto_0
.end method

.method private static a(Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 2121534
    instance-of v0, p0, LX/4cl;

    if-nez v0, :cond_0

    instance-of v0, p0, LX/4bK;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/net/UnknownHostException;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljavax/net/ssl/SSLException;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/net/SocketException;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/net/SocketTimeoutException;

    if-nez v0, :cond_0

    instance-of v0, p0, Lorg/apache/http/conn/ConnectTimeoutException;

    if-nez v0, :cond_0

    instance-of v0, p0, Lorg/apache/http/conn/HttpHostConnectException;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/ERq;
    .locals 12

    .prologue
    .line 2121581
    new-instance v0, LX/ERq;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v2

    check-cast v2, LX/11H;

    .line 2121582
    new-instance v4, LX/ERa;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {v4, v3}, LX/ERa;-><init>(LX/03V;)V

    .line 2121583
    move-object v3, v4

    .line 2121584
    check-cast v3, LX/ERa;

    invoke-static {p0}, LX/2TN;->c(LX/0QB;)LX/2TN;

    move-result-object v4

    check-cast v4, LX/2TN;

    invoke-static {p0}, LX/43D;->b(LX/0QB;)LX/43C;

    move-result-object v5

    check-cast v5, LX/43C;

    invoke-static {p0}, LX/2TL;->a(LX/0QB;)LX/2TL;

    move-result-object v6

    check-cast v6, LX/2TL;

    invoke-static {p0}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v7

    check-cast v7, LX/2TK;

    invoke-static {p0}, LX/2TM;->c(LX/0QB;)LX/2TM;

    move-result-object v8

    check-cast v8, LX/2TM;

    invoke-static {p0}, LX/ERr;->a(LX/0QB;)LX/ERr;

    move-result-object v9

    check-cast v9, LX/ERr;

    invoke-static {p0}, LX/ES8;->a(LX/0QB;)LX/ES8;

    move-result-object v10

    check-cast v10, LX/ES8;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-direct/range {v0 .. v11}, LX/ERq;-><init>(Landroid/content/Context;LX/11H;LX/ERa;LX/2TN;LX/43C;LX/2TL;LX/2TK;LX/2TM;LX/ERr;LX/ES8;LX/03V;)V

    .line 2121585
    return-object v0
.end method

.method private static b(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;)V
    .locals 1

    .prologue
    .line 2121579
    iget-object v0, p0, LX/ERq;->k:LX/ERr;

    invoke-virtual {v0, p1}, LX/ERr;->b(Lcom/facebook/vault/provider/VaultImageProviderRow;)V

    .line 2121580
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/vault/provider/VaultImageProviderRow;I)Z
    .locals 24

    .prologue
    .line 2121535
    move-object/from16 v0, p0

    iget-object v4, v0, LX/ERq;->j:LX/2TK;

    invoke-virtual {v4}, LX/2TK;->c()Z

    move-result v20

    .line 2121536
    move-object/from16 v0, p0

    iget-object v4, v0, LX/ERq;->i:LX/2TL;

    invoke-virtual {v4}, LX/2TL;->a()J

    move-result-wide v12

    .line 2121537
    move-object/from16 v0, p0

    iget-object v4, v0, LX/ERq;->j:LX/2TK;

    move/from16 v0, p2

    invoke-virtual {v4, v0}, LX/2TK;->a(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2121538
    move-object/from16 v0, p0

    iget-object v4, v0, LX/ERq;->l:LX/ES8;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/ES8;->b(Ljava/lang/String;)I

    .line 2121539
    invoke-static/range {p0 .. p1}, LX/ERq;->a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;)V

    .line 2121540
    const/4 v4, 0x0

    .line 2121541
    :goto_0
    return v4

    .line 2121542
    :cond_0
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    .line 2121543
    move-object/from16 v0, p0

    iget-object v4, v0, LX/ERq;->f:LX/2TN;

    invoke-virtual {v4, v10}, LX/2TN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 2121544
    if-nez v18, :cond_1

    .line 2121545
    move-object/from16 v0, p0

    iget-object v4, v0, LX/ERq;->k:LX/ERr;

    invoke-virtual {v4, v10}, LX/ERr;->a(Ljava/lang/String;)V

    .line 2121546
    const/4 v4, 0x0

    goto :goto_0

    .line 2121547
    :cond_1
    if-eqz v20, :cond_3

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v19, v4

    .line 2121548
    :goto_1
    if-eqz v19, :cond_2

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_4

    .line 2121549
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, LX/ERq;->k:LX/ERr;

    invoke-virtual {v4, v10}, LX/ERr;->a(Ljava/lang/String;)V

    .line 2121550
    const/4 v4, 0x0

    goto :goto_0

    .line 2121551
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p1

    move/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, LX/ERq;->a(LX/ERq;Ljava/lang/String;Lcom/facebook/vault/provider/VaultImageProviderRow;Z)Ljava/io/File;

    move-result-object v4

    move-object/from16 v19, v4

    goto :goto_1

    .line 2121552
    :cond_4
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 2121553
    new-instance v4, LX/ERp;

    move-object/from16 v5, p0

    move-object/from16 v8, p1

    invoke-direct/range {v4 .. v8}, LX/ERp;-><init>(LX/ERq;JLcom/facebook/vault/provider/VaultImageProviderRow;)V

    .line 2121554
    new-instance v8, LX/ERb;

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    move-object/from16 v9, v19

    move-object v14, v4

    invoke-direct/range {v8 .. v14}, LX/ERb;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JLX/4cr;)V

    .line 2121555
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-static {v0, v1, v2, v6, v7}, LX/ERq;->a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;ZJ)V

    .line 2121556
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    .line 2121557
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/ERq;->d:LX/11H;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/ERq;->e:LX/ERa;

    invoke-virtual {v4, v5, v8}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/ERc;

    .line 2121558
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v14, v8, v22

    .line 2121559
    invoke-virtual {v4}, LX/ERc;->a()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-lez v5, :cond_6

    .line 2121560
    invoke-virtual {v4}, LX/ERc;->a()J

    move-result-wide v11

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move/from16 v13, v20

    move-wide/from16 v16, v6

    invoke-static/range {v9 .. v18}, LX/ERq;->a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;JZJJLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2121561
    if-nez v20, :cond_5

    .line 2121562
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->delete()Z

    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 2121563
    :cond_6
    :try_start_1
    const-string v13, "upload result missing fbid"

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-wide/from16 v16, v6

    invoke-static/range {v11 .. v18}, LX/ERq;->a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;Ljava/lang/String;JJLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2121564
    if-nez v20, :cond_7

    .line 2121565
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->delete()Z

    :cond_7
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 2121566
    :catch_0
    move-exception v4

    .line 2121567
    :try_start_2
    sget-object v5, LX/ERq;->b:Ljava/lang/String;

    const-string v8, "syncPhoto"

    invoke-static {v5, v8, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2121568
    invoke-static {v4}, LX/ERq;->a(Ljava/lang/Exception;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2121569
    invoke-static/range {p0 .. p1}, LX/ERq;->a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2121570
    :goto_2
    if-nez v20, :cond_8

    .line 2121571
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->delete()Z

    :cond_8
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 2121572
    :cond_9
    :try_start_3
    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    .line 2121573
    if-nez v5, :cond_a

    .line 2121574
    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    const/16 v9, 0x64

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 2121575
    :cond_a
    move-object/from16 v0, p0

    iget-object v8, v0, LX/ERq;->m:LX/03V;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "vault_image_upload_api exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v9, ""

    invoke-virtual {v8, v5, v9, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2121576
    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v14, v4, v22

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-wide/from16 v16, v6

    invoke-static/range {v11 .. v18}, LX/ERq;->a(LX/ERq;Lcom/facebook/vault/provider/VaultImageProviderRow;Ljava/lang/String;JJLjava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 2121577
    :catchall_0
    move-exception v4

    if-nez v20, :cond_b

    .line 2121578
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->delete()Z

    :cond_b
    throw v4
.end method
