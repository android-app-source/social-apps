.class public LX/Erb;
.super LX/8tB;
.source ""

# interfaces
.implements LX/2ht;


# instance fields
.field private e:I

.field private final f:I

.field private g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/events/invite/EventInviteeToken;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2173151
    invoke-direct {p0, p1, p2}, LX/8tB;-><init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;)V

    .line 2173152
    const/4 v0, 0x0

    iput v0, p0, LX/Erb;->e:I

    .line 2173153
    const/4 v0, 0x0

    iput-object v0, p0, LX/Erb;->g:LX/0P1;

    .line 2173154
    iput-object p3, p0, LX/Erb;->h:Landroid/content/Context;

    .line 2173155
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0691

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/Erb;->f:I

    .line 2173156
    return-void
.end method


# virtual methods
.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2173162
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 2173163
    invoke-virtual {p0, v0, p2, p3}, LX/8tB;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2173164
    iget v1, p0, LX/Erb;->e:I

    if-nez v1, :cond_0

    .line 2173165
    const/4 p3, 0x0

    .line 2173166
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 p1, -0x1

    const/4 p2, -0x2

    invoke-direct {v1, p1, p2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2173167
    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->measure(II)V

    .line 2173168
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move v1, v1

    .line 2173169
    iput v1, p0, LX/Erb;->e:I

    .line 2173170
    :cond_0
    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2173161
    iget v0, p0, LX/Erb;->f:I

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 2173171
    iget v0, p0, LX/Erb;->e:I

    return v0
.end method

.method public final f(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2173158
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v1

    .line 2173159
    aget v1, v1, v0

    .line 2173160
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 2173157
    sget-object v0, LX/8vI;->HEADER:LX/8vI;

    invoke-virtual {v0}, LX/8vI;->ordinal()I

    move-result v0

    return v0
.end method
