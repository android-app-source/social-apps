.class public LX/EIh;
.super LX/EH2;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/EH3;

.field public c:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2102254
    invoke-direct {p0, p1}, LX/EH2;-><init>(Landroid/content/Context;)V

    .line 2102255
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2102256
    iput-object v0, p0, LX/EIh;->a:LX/0Ot;

    .line 2102257
    const-class v0, LX/EIh;

    invoke-static {v0, p0}, LX/EIh;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2102258
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2102259
    const v1, 0x7f03161e

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2102260
    const v0, 0x7f0d31b1

    invoke-virtual {p0, v0}, LX/EH2;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/EIh;->c:Landroid/widget/FrameLayout;

    .line 2102261
    iget-object v0, p0, LX/EIh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2102262
    iget-boolean v1, v0, LX/EDx;->aZ:Z

    move v0, v1

    .line 2102263
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EIh;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2102264
    :goto_0
    new-instance v1, LX/EH3;

    if-eqz v0, :cond_1

    sget-object v0, LX/EH1;->CONFERENCE_WITH_ADD_CALLEE:LX/EH1;

    :goto_1
    invoke-direct {v1, p1, v0}, LX/EH3;-><init>(Landroid/content/Context;LX/EH1;)V

    iput-object v1, p0, LX/EIh;->b:LX/EH3;

    .line 2102265
    iget-object v0, p0, LX/EIh;->c:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/EIh;->b:LX/EH3;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2102266
    return-void

    .line 2102267
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2102268
    :cond_1
    sget-object v0, LX/EH1;->CONFERENCE:LX/EH1;

    goto :goto_1
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/EIh;

    const/16 p0, 0x3257

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p1, LX/EIh;->a:LX/0Ot;

    return-void
.end method
