.class public LX/Eaz;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[B

.field private static final b:[B


# instance fields
.field public final c:I

.field public final d:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2142832
    new-array v0, v1, [B

    aput-byte v1, v0, v2

    sput-object v0, LX/Eaz;->a:[B

    .line 2142833
    new-array v0, v1, [B

    const/4 v1, 0x2

    aput-byte v1, v0, v2

    sput-object v0, LX/Eaz;->b:[B

    return-void
.end method

.method public constructor <init>(I[B)V
    .locals 0

    .prologue
    .line 2142828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142829
    iput p1, p0, LX/Eaz;->c:I

    .line 2142830
    iput-object p2, p0, LX/Eaz;->d:[B

    .line 2142831
    return-void
.end method

.method private static a([B[B)[B
    .locals 3

    .prologue
    .line 2142834
    :try_start_0
    const-string v0, "HmacSHA256"

    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v0

    .line 2142835
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "HmacSHA256"

    invoke-direct {v1, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 2142836
    invoke-virtual {v0, p0}, Ljavax/crypto/Mac;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 2142837
    :catch_0
    move-exception v0

    .line 2142838
    :goto_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 2142839
    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final b()LX/Eb0;
    .locals 4

    .prologue
    .line 2142827
    new-instance v0, LX/Eb0;

    iget v1, p0, LX/Eaz;->c:I

    sget-object v2, LX/Eaz;->a:[B

    iget-object v3, p0, LX/Eaz;->d:[B

    invoke-static {v2, v3}, LX/Eaz;->a([B[B)[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/Eb0;-><init>(I[B)V

    return-object v0
.end method

.method public final c()LX/Eaz;
    .locals 4

    .prologue
    .line 2142826
    new-instance v0, LX/Eaz;

    iget v1, p0, LX/Eaz;->c:I

    add-int/lit8 v1, v1, 0x1

    sget-object v2, LX/Eaz;->b:[B

    iget-object v3, p0, LX/Eaz;->d:[B

    invoke-static {v2, v3}, LX/Eaz;->a([B[B)[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/Eaz;-><init>(I[B)V

    return-object v0
.end method
