.class public LX/CxB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CxA;


# instance fields
.field private final a:LX/CzE;

.field private final b:LX/CxP;


# direct methods
.method public constructor <init>(LX/CzE;LX/CxP;)V
    .locals 0
    .param p1    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/CxP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951474
    iput-object p1, p0, LX/CxB;->a:LX/CzE;

    .line 1951475
    iput-object p2, p0, LX/CxB;->b:LX/CxP;

    .line 1951476
    return-void
.end method


# virtual methods
.method public final a(LX/CzL;LX/CzL;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/CzL",
            "<+TT;>;",
            "LX/CzL",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1951492
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 1951493
    invoke-static {p1, p2}, LX/CxD;->b(LX/CzL;LX/CzL;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    .line 1951494
    invoke-virtual {p0, v0, v1}, LX/CxB;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V

    .line 1951495
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 1951485
    iget-object v0, p0, LX/CxB;->a:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 1951486
    iget-object v2, v0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    move-object v0, v2

    .line 1951487
    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    .line 1951488
    invoke-static {v0, p1}, LX/CxD;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v2

    .line 1951489
    if-eqz v2, :cond_0

    .line 1951490
    invoke-virtual {p0, v0, v2}, LX/CxB;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V

    .line 1951491
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V
    .locals 3

    .prologue
    .line 1951496
    iget-object v0, p0, LX/CxB;->b:LX/CxP;

    invoke-interface {v0, p1}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v0

    .line 1951497
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1951498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Item not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1951499
    :cond_0
    iget-object v1, p0, LX/CxB;->a:LX/CzE;

    const/4 v2, 0x0

    invoke-static {p2, v2}, Lcom/facebook/search/results/model/SearchResultsBridge;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Ljava/lang/String;)Lcom/facebook/search/results/model/SearchResultsBridge;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/CzE;->b(ILcom/facebook/graphql/model/FeedUnit;)Z

    .line 1951500
    return-void
.end method

.method public final a(LX/CzL;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 1951484
    iget-object v0, p0, LX/CxB;->b:LX/CxP;

    invoke-interface {v0, p1}, LX/CxP;->b(LX/CzL;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    .line 1951478
    iget-object v0, p0, LX/CxB;->a:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 1951479
    iget-object p0, v0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    move-object v0, p0

    .line 1951480
    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    .line 1951481
    invoke-static {v0, p1}, LX/CxD;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951482
    const/4 v0, 0x1

    .line 1951483
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1951477
    const/4 v0, 0x0

    return-object v0
.end method
