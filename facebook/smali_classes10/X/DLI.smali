.class public final enum LX/DLI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DLI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DLI;

.field public static final enum DOWNLOADING_IS_IN_PROGRESS:LX/DLI;

.field public static final enum NORMAL:LX/DLI;

.field public static final enum UPLOADING_FAILED_SHOW_RETRY:LX/DLI;

.field public static final enum UPLOADING_IS_IN_PROGRESS:LX/DLI;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1988024
    new-instance v0, LX/DLI;

    const-string v1, "UPLOADING_FAILED_SHOW_RETRY"

    invoke-direct {v0, v1, v2}, LX/DLI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DLI;->UPLOADING_FAILED_SHOW_RETRY:LX/DLI;

    .line 1988025
    new-instance v0, LX/DLI;

    const-string v1, "UPLOADING_IS_IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/DLI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DLI;->UPLOADING_IS_IN_PROGRESS:LX/DLI;

    .line 1988026
    new-instance v0, LX/DLI;

    const-string v1, "DOWNLOADING_IS_IN_PROGRESS"

    invoke-direct {v0, v1, v4}, LX/DLI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DLI;->DOWNLOADING_IS_IN_PROGRESS:LX/DLI;

    .line 1988027
    new-instance v0, LX/DLI;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v5}, LX/DLI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DLI;->NORMAL:LX/DLI;

    .line 1988028
    const/4 v0, 0x4

    new-array v0, v0, [LX/DLI;

    sget-object v1, LX/DLI;->UPLOADING_FAILED_SHOW_RETRY:LX/DLI;

    aput-object v1, v0, v2

    sget-object v1, LX/DLI;->UPLOADING_IS_IN_PROGRESS:LX/DLI;

    aput-object v1, v0, v3

    sget-object v1, LX/DLI;->DOWNLOADING_IS_IN_PROGRESS:LX/DLI;

    aput-object v1, v0, v4

    sget-object v1, LX/DLI;->NORMAL:LX/DLI;

    aput-object v1, v0, v5

    sput-object v0, LX/DLI;->$VALUES:[LX/DLI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1988029
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DLI;
    .locals 1

    .prologue
    .line 1988030
    const-class v0, LX/DLI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DLI;

    return-object v0
.end method

.method public static values()[LX/DLI;
    .locals 1

    .prologue
    .line 1988031
    sget-object v0, LX/DLI;->$VALUES:[LX/DLI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DLI;

    return-object v0
.end method
