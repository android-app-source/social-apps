.class public final LX/EvN;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2181307
    const-class v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchFriendsGraphQLModels$FriendsCenterFetchFriendsQueryModel;

    const v0, -0x15b42c17

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FriendsCenterFetchFriendsQuery"

    const-string v6, "4529bdf1f58a2cb8338b9e64864b99b5"

    const-string v7, "me"

    const-string v8, "10155259866136729"

    const-string v9, "10155260460106729"

    .line 2181308
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2181309
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2181310
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2181311
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2181312
    sparse-switch v0, :sswitch_data_0

    .line 2181313
    :goto_0
    return-object p1

    .line 2181314
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2181315
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2181316
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2181317
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2181318
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2181319
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x295975c2 -> :sswitch_1
        0x1e8378b -> :sswitch_5
        0x21beac6a -> :sswitch_0
        0x475168b0 -> :sswitch_4
        0x6c6f579a -> :sswitch_3
        0x756ad05c -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2181303
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2181304
    :goto_1
    return v0

    .line 2181305
    :pswitch_0
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2181306
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x35
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
