.class public LX/DtJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/DtM;

.field public c:Lcom/facebook/payments/p2p/model/Sender;

.field public d:Lcom/facebook/payments/p2p/model/Receiver;

.field public e:Ljava/lang/String;

.field public f:LX/DtQ;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Lcom/facebook/payments/p2p/model/Amount;

.field public j:Lcom/facebook/payments/p2p/model/Amount;

.field public k:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

.field public l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

.field public m:Lcom/facebook/payments/p2p/model/CommerceOrder;

.field public n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2051844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)LX/DtJ;
    .locals 2

    .prologue
    .line 2051845
    new-instance v0, LX/DtJ;

    invoke-direct {v0}, LX/DtJ;-><init>()V

    .line 2051846
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2051847
    iput-object v1, v0, LX/DtJ;->a:Ljava/lang/String;

    .line 2051848
    move-object v0, v0

    .line 2051849
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->c:LX/DtM;

    move-object v1, v1

    .line 2051850
    iput-object v1, v0, LX/DtJ;->b:LX/DtM;

    .line 2051851
    move-object v0, v0

    .line 2051852
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    move-object v1, v1

    .line 2051853
    iput-object v1, v0, LX/DtJ;->c:Lcom/facebook/payments/p2p/model/Sender;

    .line 2051854
    move-object v0, v0

    .line 2051855
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    move-object v1, v1

    .line 2051856
    iput-object v1, v0, LX/DtJ;->d:Lcom/facebook/payments/p2p/model/Receiver;

    .line 2051857
    move-object v0, v0

    .line 2051858
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v1, v1

    .line 2051859
    iput-object v1, v0, LX/DtJ;->f:LX/DtQ;

    .line 2051860
    move-object v0, v0

    .line 2051861
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    move-object v1, v1

    .line 2051862
    iput-object v1, v0, LX/DtJ;->i:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051863
    move-object v0, v0

    .line 2051864
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    move-object v1, v1

    .line 2051865
    iput-object v1, v0, LX/DtJ;->j:Lcom/facebook/payments/p2p/model/Amount;

    .line 2051866
    move-object v0, v0

    .line 2051867
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2051868
    iput-object v1, v0, LX/DtJ;->e:Ljava/lang/String;

    .line 2051869
    move-object v0, v0

    .line 2051870
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    move-object v1, v1

    .line 2051871
    iput-object v1, v0, LX/DtJ;->g:Ljava/lang/String;

    .line 2051872
    move-object v0, v0

    .line 2051873
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    move-object v1, v1

    .line 2051874
    iput-object v1, v0, LX/DtJ;->h:Ljava/lang/String;

    .line 2051875
    move-object v0, v0

    .line 2051876
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-object v1, v1

    .line 2051877
    iput-object v1, v0, LX/DtJ;->k:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2051878
    move-object v0, v0

    .line 2051879
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-object v1, v1

    .line 2051880
    iput-object v1, v0, LX/DtJ;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2051881
    move-object v0, v0

    .line 2051882
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    move-object v1, v1

    .line 2051883
    iput-object v1, v0, LX/DtJ;->m:Lcom/facebook/payments/p2p/model/CommerceOrder;

    .line 2051884
    move-object v0, v0

    .line 2051885
    iget-object v1, p0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->o:Ljava/lang/String;

    move-object v1, v1

    .line 2051886
    iput-object v1, v0, LX/DtJ;->n:Ljava/lang/String;

    .line 2051887
    move-object v0, v0

    .line 2051888
    return-object v0
.end method


# virtual methods
.method public final o()Lcom/facebook/payments/p2p/model/PaymentTransaction;
    .locals 1

    .prologue
    .line 2051889
    new-instance v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    invoke-direct {v0, p0}, Lcom/facebook/payments/p2p/model/PaymentTransaction;-><init>(LX/DtJ;)V

    return-object v0
.end method
