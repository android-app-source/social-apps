.class public final enum LX/EnC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EnC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EnC;

.field public static final enum FAILED:LX/EnC;

.field public static final enum SUCCEEDED:LX/EnC;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2166712
    new-instance v0, LX/EnC;

    const-string v1, "SUCCEEDED"

    invoke-direct {v0, v1, v2}, LX/EnC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EnC;->SUCCEEDED:LX/EnC;

    .line 2166713
    new-instance v0, LX/EnC;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v3}, LX/EnC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EnC;->FAILED:LX/EnC;

    .line 2166714
    const/4 v0, 0x2

    new-array v0, v0, [LX/EnC;

    sget-object v1, LX/EnC;->SUCCEEDED:LX/EnC;

    aput-object v1, v0, v2

    sget-object v1, LX/EnC;->FAILED:LX/EnC;

    aput-object v1, v0, v3

    sput-object v0, LX/EnC;->$VALUES:[LX/EnC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2166711
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EnC;
    .locals 1

    .prologue
    .line 2166716
    const-class v0, LX/EnC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EnC;

    return-object v0
.end method

.method public static values()[LX/EnC;
    .locals 1

    .prologue
    .line 2166715
    sget-object v0, LX/EnC;->$VALUES:[LX/EnC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EnC;

    return-object v0
.end method
