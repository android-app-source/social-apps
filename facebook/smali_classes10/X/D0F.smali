.class public interface abstract LX/D0F;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final B:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final C:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final D:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final E:LX/CzP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzP",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final F:LX/CzP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzP",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final G:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final H:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final I:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final J:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final K:LX/CzP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzP",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final L:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final M:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final N:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final O:LX/CzP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzP",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final P:LX/CzP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzP",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final Q:LX/CzP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzP",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final R:LX/CzP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzP",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final S:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final T:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final U:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final V:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final W:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final X:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final Y:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final a:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final c:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final p:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final r:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final s:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final t:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final u:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation
.end field

.field public static final v:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final w:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final x:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final y:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final z:LX/CzO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1954950
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Cza;

    invoke-direct {v3}, LX/Cza;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->a:LX/CzO;

    .line 1954951
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v2}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v2

    new-instance v3, LX/Czl;

    invoke-direct {v3}, LX/Czl;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->b:LX/CzO;

    .line 1954952
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czw;

    invoke-direct {v3}, LX/Czw;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->c:LX/CzO;

    .line 1954953
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_PUBLIC_EMPTY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/D07;

    invoke-direct {v3}, LX/D07;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->d:LX/CzO;

    .line 1954954
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/D0A;

    invoke-direct {v3}, LX/D0A;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->e:LX/CzO;

    .line 1954955
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BLENDED_PHOTO_SOCIAL_EMPTY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/D0B;

    invoke-direct {v3}, LX/D0B;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->f:LX/CzO;

    .line 1954956
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/D0C;

    invoke-direct {v3}, LX/D0C;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->g:LX/CzO;

    .line 1954957
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MEDIA_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/D0D;

    invoke-direct {v3}, LX/D0D;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->h:LX/CzO;

    .line 1954958
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/D0E;

    invoke-direct {v3}, LX/D0E;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->i:LX/CzO;

    .line 1954959
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FLEXIBLE_NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/CzQ;

    invoke-direct {v3}, LX/CzQ;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->j:LX/CzO;

    .line 1954960
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v2}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v2

    new-instance v3, LX/CzR;

    invoke-direct {v3}, LX/CzR;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->k:LX/CzO;

    .line 1954961
    new-instance v7, LX/CzO;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_COMMON:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_PYMK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const/16 v6, 0x12

    new-array v6, v6, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_FROM:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v10

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_LIVED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v11

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v12

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_VISITED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v13

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FRIENDS_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v14

    const/4 v8, 0x5

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_FROM:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/4 v8, 0x6

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_LIVED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/4 v8, 0x7

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x8

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_VISITED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x9

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_PEOPLE_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xa

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_CITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xb

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_HOMETOWN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xc

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_SCHOOL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xd

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_PEOPLE_WORK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xe

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xf

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_QUERY_ENTITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x10

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x11

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v1}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v1

    new-instance v2, LX/CzS;

    invoke-direct {v2}, LX/CzS;-><init>()V

    invoke-direct {v7, v0, v1, v2}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v7, LX/D0F;->l:LX/CzO;

    .line 1954962
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_MIXED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/CzT;

    invoke-direct {v3}, LX/CzT;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->m:LX/CzO;

    .line 1954963
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WEATHER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/CzU;

    invoke-direct {v3}, LX/CzU;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->n:LX/CzO;

    .line 1954964
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIME:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/CzV;

    invoke-direct {v3}, LX/CzV;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->o:LX/CzO;

    .line 1954965
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ELECTIONS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/CzW;

    invoke-direct {v3}, LX/CzW;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->p:LX/CzO;

    .line 1954966
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->BIRTHDAY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/CzX;

    invoke-direct {v3}, LX/CzX;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->q:LX/CzO;

    .line 1954967
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_NON_PEOPLE_RELATED_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/CzY;

    invoke-direct {v3}, LX/CzY;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->r:LX/CzO;

    .line 1954968
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIMELINE_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/CzZ;

    invoke-direct {v3}, LX/CzZ;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->s:LX/CzO;

    .line 1954969
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SHORTCUT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czb;

    invoke-direct {v3}, LX/Czb;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->t:LX/CzO;

    .line 1954970
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TIMELINE_HEADER_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czc;

    invoke-direct {v3}, LX/Czc;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->u:LX/CzO;

    .line 1954971
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czd;

    invoke-direct {v3}, LX/Czd;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->v:LX/CzO;

    .line 1954972
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WIKIPEDIA_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Cze;

    invoke-direct {v3}, LX/Cze;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->w:LX/CzO;

    .line 1954973
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->CENTRAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czf;

    invoke-direct {v3}, LX/Czf;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->x:LX/CzO;

    .line 1954974
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NAVIGATIONAL_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v2}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v2

    new-instance v3, LX/Czg;

    invoke-direct {v3}, LX/Czg;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->y:LX/CzO;

    .line 1954975
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_FOR_SALE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czh;

    invoke-direct {v3}, LX/Czh;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->z:LX/CzO;

    .line 1954976
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMPTY_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czi;

    invoke-direct {v3}, LX/Czi;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->A:LX/CzO;

    .line 1954977
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMPTY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czj;

    invoke-direct {v3}, LX/Czj;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->B:LX/CzO;

    .line 1954978
    new-instance v7, LX/CzO;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_USER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    new-array v6, v10, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sget-object v1, LX/D0I;->a:LX/D0G;

    new-instance v2, LX/Czk;

    invoke-direct {v2}, LX/Czk;-><init>()V

    invoke-direct {v7, v0, v1, v2}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v7, LX/D0F;->C:LX/CzO;

    .line 1954979
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PIVOTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czm;

    invoke-direct {v3}, LX/Czm;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->D:LX/CzO;

    .line 1954980
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v2}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v2

    new-instance v3, LX/Czn;

    invoke-direct {v3}, LX/Czn;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->E:LX/CzP;

    .line 1954981
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PREFILLED_COMPOSER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czo;

    invoke-direct {v3}, LX/Czo;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->F:LX/CzP;

    .line 1954982
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_EYEWITNESSES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czp;

    invoke-direct {v3}, LX/Czp;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->G:LX/CzO;

    .line 1954983
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czq;

    invoke-direct {v3}, LX/Czq;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->H:LX/CzO;

    .line 1954984
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MY_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v2}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v2

    new-instance v3, LX/Czr;

    invoke-direct {v3}, LX/Czr;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->I:LX/CzO;

    .line 1954985
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->QUERY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v2}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v2

    new-instance v3, LX/Czs;

    invoke-direct {v3}, LX/Czs;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->J:LX/CzO;

    .line 1954986
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GLOBAL_SHARE_METADATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czt;

    invoke-direct {v3}, LX/Czt;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->K:LX/CzP;

    .line 1954987
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_SHARES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czu;

    invoke-direct {v3}, LX/Czu;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->L:LX/CzO;

    .line 1954988
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMON_PHRASES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czv;

    invoke-direct {v3}, LX/Czv;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->M:LX/CzO;

    .line 1954989
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMON_QUOTES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czx;

    invoke-direct {v3}, LX/Czx;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->N:LX/CzO;

    .line 1954990
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMOTIONAL_ANALYSIS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/Czy;

    invoke-direct {v3}, LX/Czy;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->O:LX/CzP;

    .line 1954991
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GLOBAL_SHARE_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v2}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v2

    new-instance v3, LX/Czz;

    invoke-direct {v3}, LX/Czz;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->P:LX/CzP;

    .line 1954992
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_TOPICS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v2}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v2

    new-instance v3, LX/D00;

    invoke-direct {v3}, LX/D00;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->Q:LX/CzP;

    .line 1954993
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_METADATA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v2}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v2

    new-instance v3, LX/D01;

    invoke-direct {v3}, LX/D01;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->R:LX/CzP;

    .line 1954994
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/D02;

    invoke-direct {v3}, LX/D02;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->S:LX/CzO;

    .line 1954995
    new-instance v0, LX/CzP;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v2}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v2

    new-instance v3, LX/D03;

    invoke-direct {v3}, LX/D03;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzP;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->T:LX/CzO;

    .line 1954996
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_SOCIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_KEY_VOICES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/D04;

    invoke-direct {v3}, LX/D04;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->U:LX/CzO;

    .line 1954997
    new-instance v0, LX/CzO;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SEE_MORE_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POST_SEE_MORE_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sget-object v2, LX/D0I;->a:LX/D0G;

    new-instance v3, LX/D05;

    invoke-direct {v3}, LX/D05;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v0, LX/D0F;->V:LX/CzO;

    .line 1954998
    new-instance v7, LX/CzO;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GROUP_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MY_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const/16 v6, 0x1d

    new-array v6, v6, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MAIN_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v10

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_MAIN_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v11

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v12

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v13

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOP_PUBLIC_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v14

    const/4 v8, 0x5

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PIVOT_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/4 v8, 0x6

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POST_SET:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/4 v8, 0x7

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x8

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x9

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_TOPIC_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xa

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CELEBRITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xb

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_CELEBRITIES_MENTION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xc

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_COMMENTARY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xd

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xe

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_EXPERIENTIAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xf

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_FEATURED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x10

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_GOVERNMENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x11

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_HOW_TO:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x12

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_LOCATION:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x13

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_MINUTIAE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x14

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RECENT_TOP:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x15

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RECIPES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x16

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_RELATED_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x17

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_REVIEWS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x18

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_REVIEWS_PEOPLE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x19

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_SET_VITAL_AUTHORS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x1a

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PRELOADED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x1b

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x1c

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_QUERY_ENTITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v1}, LX/D0I;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/D0G;

    move-result-object v1

    new-instance v2, LX/D06;

    invoke-direct {v2}, LX/D06;-><init>()V

    invoke-direct {v7, v0, v1, v2}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v7, LX/D0F;->W:LX/CzO;

    .line 1954999
    new-instance v7, LX/CzO;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_EVENTS_NEARBY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_FUTURE_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const/16 v6, 0xd

    new-array v6, v6, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_HOSTED_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v10

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_FRIENDS_UPCOMING_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v11

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_GROUPS_FRIENDS_JOINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v12

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_GROUPS_ONE_SHOULD_JOIN:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v13

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_BARS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v14

    const/4 v8, 0x5

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/4 v8, 0x6

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/4 v8, 0x7

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_LOCATION_LANDMARKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x8

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_MY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0x9

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->DISCOVERY_MY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xa

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xb

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_QUERY_ENTITY_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    const/16 v8, 0xc

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NONE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v9, v6, v8

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v8

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    new-array v6, v12, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v9, v6, v10

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    aput-object v9, v6, v11

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 1955000
    new-instance v1, LX/D0J;

    invoke-direct {v1, v0}, LX/D0J;-><init>(LX/0Rf;)V

    move-object v0, v1

    .line 1955001
    new-instance v1, LX/D08;

    invoke-direct {v1}, LX/D08;-><init>()V

    invoke-direct {v7, v8, v0, v1}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v7, LX/D0F;->X:LX/CzO;

    .line 1955002
    new-instance v7, LX/CzO;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_BY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_1:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_2:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_CV_3:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_LIKED_BY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_POPULAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    new-array v6, v14, [Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_PUBLIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v10

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_RECENT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v11

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR_PHOTO_SET_TAGGED_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v12

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    aput-object v8, v6, v13

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sget-object v1, LX/D0I;->a:LX/D0G;

    new-instance v2, LX/D09;

    invoke-direct {v2}, LX/D09;-><init>()V

    invoke-direct {v7, v0, v1, v2}, LX/CzO;-><init>(LX/0Rf;LX/D0G;LX/0RL;)V

    sput-object v7, LX/D0F;->Y:LX/CzO;

    return-void
.end method
