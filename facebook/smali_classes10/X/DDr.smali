.class public final LX/DDr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic e:Landroid/content/Context;

.field public final synthetic f:LX/34w;


# direct methods
.method public constructor <init>(LX/34w;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1976052
    iput-object p1, p0, LX/DDr;->f:LX/34w;

    iput-object p2, p0, LX/DDr;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DDr;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DDr;->c:Ljava/lang/String;

    iput-object p5, p0, LX/DDr;->d:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p6, p0, LX/DDr;->e:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 1976053
    iget-object v0, p0, LX/DDr;->a:Ljava/lang/String;

    iget-object v1, p0, LX/DDr;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1976054
    iget-object v1, p0, LX/DDr;->f:LX/34w;

    iget-object v2, p0, LX/DDr;->c:Ljava/lang/String;

    iget-object v3, p0, LX/DDr;->b:Ljava/lang/String;

    iget-object v4, p0, LX/DDr;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1, v2, v3, v4}, LX/34w;->a$redex0(LX/34w;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1976055
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1976056
    iget-object v0, p0, LX/DDr;->f:LX/34w;

    iget-object v0, v0, LX/34w;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DDr;->e:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1976057
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1976058
    :catch_0
    move-exception v0

    .line 1976059
    iget-object v1, p0, LX/DDr;->e:Landroid/content/Context;

    const v2, 0x7f08251c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1976060
    iget-object v2, p0, LX/DDr;->e:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1976061
    iget-object v1, p0, LX/DDr;->f:LX/34w;

    iget-object v1, v1, LX/34w;->d:LX/03V;

    sget-object v2, LX/34w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
