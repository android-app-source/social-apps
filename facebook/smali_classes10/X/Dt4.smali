.class public LX/Dt4;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/TimeZone;


# instance fields
.field private final b:Ljava/text/SimpleDateFormat;

.field private c:Ljava/text/DateFormatSymbols;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2051146
    const-string v0, "GMT-8"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, LX/Dt4;->a:Ljava/util/TimeZone;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2051140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2051141
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h:mma"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/Dt4;->b:Ljava/text/SimpleDateFormat;

    .line 2051142
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    iput-object v0, p0, LX/Dt4;->c:Ljava/text/DateFormatSymbols;

    .line 2051143
    sget-object v0, LX/Dt4;->a:Ljava/util/TimeZone;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 2051144
    iget-object v1, p0, LX/Dt4;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->setCalendar(Ljava/util/Calendar;)V

    .line 2051145
    return-void
.end method

.method private static a(LX/Dt4;LX/Dt3;ILandroid/content/res/Resources;)LX/Dsz;
    .locals 11

    .prologue
    .line 2051118
    invoke-virtual {p1, p2}, LX/Dt3;->a(I)Ljava/util/List;

    move-result-object v2

    .line 2051119
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    .line 2051120
    const/4 v0, 0x0

    .line 2051121
    const/4 v1, 0x0

    .line 2051122
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v0

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;

    .line 2051123
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->b()J

    move-result-wide v8

    invoke-static {p0, v8, v9}, LX/Dt4;->a(LX/Dt4;J)Ljava/lang/String;

    move-result-object v7

    .line 2051124
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;->a()J

    move-result-wide v8

    invoke-static {p0, v8, v9}, LX/Dt4;->a(LX/Dt4;J)Ljava/lang/String;

    move-result-object v8

    .line 2051125
    const v0, 0x7f0830f0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v7, v3, v4

    const/4 v4, 0x1

    aput-object v8, v3, v4

    invoke-virtual {p3, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 2051126
    if-nez v1, :cond_2

    .line 2051127
    invoke-virtual {v9, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 2051128
    const/4 v0, -0x1

    if-eq v3, v0, :cond_2

    .line 2051129
    invoke-virtual {v9, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 2051130
    const/4 v0, -0x1

    if-eq v4, v0, :cond_0

    .line 2051131
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v0

    .line 2051132
    if-le v3, v4, :cond_3

    .line 2051133
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    .line 2051134
    :goto_1
    add-int v7, v4, v0

    if-gt v7, v3, :cond_2

    .line 2051135
    add-int/2addr v0, v4

    invoke-virtual {v9, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2051136
    :goto_2
    aput-object v9, v5, v2

    .line 2051137
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    .line 2051138
    goto :goto_0

    .line 2051139
    :cond_1
    new-instance v0, LX/Dsz;

    invoke-direct {v0, v5, v1}, LX/Dsz;-><init>([Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move v10, v4

    move v4, v3

    move v3, v10

    goto :goto_1
.end method

.method private static a(LX/Dt4;IILandroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2051115
    if-eq p1, p2, :cond_0

    .line 2051116
    const v0, 0x7f0830ef

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Dt4;->c:Ljava/text/DateFormatSymbols;

    invoke-virtual {v3}, Ljava/text/DateFormatSymbols;->getShortWeekdays()[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, p1

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/Dt4;->c:Ljava/text/DateFormatSymbols;

    invoke-virtual {v3}, Ljava/text/DateFormatSymbols;->getShortWeekdays()[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, p2

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2051117
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Dt4;->c:Ljava/text/DateFormatSymbols;

    invoke-virtual {v0}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method private static a(LX/Dt4;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2051114
    iget-object v0, p0, LX/Dt4;->b:Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/util/Date;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$LocationModel;)Ljava/util/TimeZone;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2051088
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$LocationModel;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2051089
    :cond_0
    :goto_0
    return-object v0

    .line 2051090
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$LocationModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    .line 2051091
    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$LocationModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 2051092
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Dt3;Landroid/content/res/Resources;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Dt3;",
            "Landroid/content/res/Resources;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "LX/Dt0;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    .line 2051093
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2051094
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 2051095
    const/4 v3, 0x0

    move v5, v0

    move-object v6, v3

    move v8, v0

    move v4, v0

    .line 2051096
    :goto_0
    const/16 v0, 0x9

    if-ge v5, v0, :cond_3

    .line 2051097
    add-int/lit8 v0, v5, -0x1

    rem-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    move v3, v0

    .line 2051098
    invoke-static {p0, p1, v3, p2}, LX/Dt4;->a(LX/Dt4;LX/Dt3;ILandroid/content/res/Resources;)LX/Dsz;

    move-result-object v7

    .line 2051099
    if-eqz v6, :cond_0

    .line 2051100
    iget-object v0, v7, LX/Dsz;->b:LX/0Px;

    move-object v0, v0

    .line 2051101
    iget-object v11, v6, LX/Dsz;->b:LX/0Px;

    move-object v11, v11

    .line 2051102
    invoke-interface {v0, v11}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v3

    move v3, v4

    .line 2051103
    :goto_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v6, v7

    move v8, v0

    move v4, v3

    goto :goto_0

    .line 2051104
    :cond_1
    if-gt v4, v10, :cond_2

    if-lt v8, v10, :cond_2

    move v0, v1

    .line 2051105
    :goto_2
    invoke-static {p0, v4, v8, p2}, LX/Dt4;->a(LX/Dt4;IILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    .line 2051106
    new-instance v8, LX/Dt0;

    invoke-direct {v8, v4, v6, v0}, LX/Dt0;-><init>(Ljava/lang/String;LX/Dsz;Z)V

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v3

    .line 2051107
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2051108
    goto :goto_2

    .line 2051109
    :cond_3
    if-gt v4, v10, :cond_4

    if-lt v8, v10, :cond_4

    .line 2051110
    :goto_3
    invoke-static {p0, v4, v8, p2}, LX/Dt4;->a(LX/Dt4;IILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 2051111
    new-instance v2, LX/Dt0;

    invoke-direct {v2, v0, v6, v1}, LX/Dt0;-><init>(Ljava/lang/String;LX/Dsz;Z)V

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2051112
    return-object v9

    :cond_4
    move v1, v2

    .line 2051113
    goto :goto_3
.end method
