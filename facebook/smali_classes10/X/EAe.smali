.class public final LX/EAe;
.super LX/BNO;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/EAh;


# direct methods
.method public constructor <init>(LX/EAh;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2085803
    iput-object p1, p0, LX/EAe;->b:LX/EAh;

    iput-object p2, p0, LX/EAe;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/BNO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 9

    .prologue
    .line 2085804
    iget-object v0, p0, LX/EAe;->b:LX/EAh;

    iget-object v1, p0, LX/EAe;->a:Ljava/lang/String;

    .line 2085805
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5tj;

    .line 2085806
    iget-object v3, v0, LX/EAh;->c:LX/Ch5;

    invoke-static {v1, v2}, LX/ChH;->a(Ljava/lang/String;LX/5tj;)LX/ChF;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 2085807
    iget-object v3, v0, LX/EAh;->e:LX/EB0;

    new-instance v4, LX/EAf;

    invoke-direct {v4, v0, v1, v2}, LX/EAf;-><init>(LX/EAh;Ljava/lang/String;LX/5tj;)V

    .line 2085808
    iget-object v5, v3, LX/EB0;->f:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "key_load_updated_review"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v2, v3, LX/EB0;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BNg;

    .line 2085809
    new-instance v7, LX/BNj;

    invoke-direct {v7}, LX/BNj;-><init>()V

    move-object v7, v7

    .line 2085810
    const-string v8, "page_id"

    invoke-virtual {v7, v8, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v8

    const-string p0, "review_profile_pic_size"

    iget-object p1, v2, LX/BNg;->b:LX/1vn;

    invoke-virtual {p1}, LX/1vn;->b()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v8, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v8

    const-string p0, "review_cover_pic_size"

    iget-object p1, v2, LX/BNg;->b:LX/1vn;

    invoke-virtual {p1}, LX/1vn;->c()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v8, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2085811
    iget-object v8, v2, LX/BNg;->a:LX/0tX;

    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    move-object v2, v7

    .line 2085812
    new-instance v7, LX/EAy;

    invoke-direct {v7, v3, v4}, LX/EAy;-><init>(LX/EB0;LX/EAf;)V

    invoke-virtual {v5, v6, v2, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2085813
    iget-object v2, v0, LX/EAh;->e:LX/EB0;

    new-instance v3, LX/EAg;

    invoke-direct {v3, v0, v1}, LX/EAg;-><init>(LX/EAh;Ljava/lang/String;)V

    .line 2085814
    iget-object v5, v2, LX/EB0;->f:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "key_load_updated_place_to_review"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v4, v2, LX/EB0;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BNf;

    .line 2085815
    new-instance v7, LX/BNk;

    invoke-direct {v7}, LX/BNk;-><init>()V

    move-object v7, v7

    .line 2085816
    const-string v8, "page_id"

    invoke-virtual {v7, v8, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v8

    const-string p0, "review_profile_pic_size"

    iget-object v0, v4, LX/BNf;->b:LX/1vn;

    invoke-virtual {v0}, LX/1vn;->b()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, p0, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2085817
    iget-object v8, v4, LX/BNf;->a:LX/0tX;

    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    move-object v4, v7

    .line 2085818
    new-instance v7, LX/EAz;

    invoke-direct {v7, v2, v3}, LX/EAz;-><init>(LX/EB0;LX/EAg;)V

    invoke-virtual {v5, v6, v4, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2085819
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2085820
    return-void
.end method
