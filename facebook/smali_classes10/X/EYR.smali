.class public final LX/EYR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EYE;


# instance fields
.field private final a:I

.field public b:LX/EXj;

.field private final c:Ljava/lang/String;

.field private final d:LX/EYQ;

.field private final e:LX/EYS;

.field private f:LX/EYF;

.field private g:LX/EYF;


# direct methods
.method public constructor <init>(LX/EXj;LX/EYQ;LX/EYS;I)V
    .locals 2

    .prologue
    .line 2137374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2137375
    iput p4, p0, LX/EYR;->a:I

    .line 2137376
    iput-object p1, p0, LX/EYR;->b:LX/EXj;

    .line 2137377
    iput-object p2, p0, LX/EYR;->d:LX/EYQ;

    .line 2137378
    iput-object p3, p0, LX/EYR;->e:LX/EYS;

    .line 2137379
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, LX/EYS;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, LX/EXj;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EYR;->c:Ljava/lang/String;

    .line 2137380
    iget-object v0, p2, LX/EYQ;->h:LX/EYJ;

    invoke-virtual {v0, p0}, LX/EYJ;->a(LX/EYE;)V

    .line 2137381
    return-void
.end method

.method public static e(LX/EYR;)V
    .locals 4

    .prologue
    .line 2137365
    iget-object v0, p0, LX/EYR;->d:LX/EYQ;

    iget-object v0, v0, LX/EYQ;->h:LX/EYJ;

    iget-object v1, p0, LX/EYR;->b:LX/EXj;

    invoke-virtual {v1}, LX/EXj;->m()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/EYI;->TYPES_ONLY:LX/EYI;

    invoke-virtual {v0, v1, p0, v2}, LX/EYJ;->a(Ljava/lang/String;LX/EYE;LX/EYI;)LX/EYE;

    move-result-object v0

    .line 2137366
    instance-of v1, v0, LX/EYF;

    if-nez v1, :cond_0

    .line 2137367
    new-instance v0, LX/EYK;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/EYR;->b:LX/EXj;

    invoke-virtual {v2}, LX/EXj;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not a message type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137368
    :cond_0
    check-cast v0, LX/EYF;

    iput-object v0, p0, LX/EYR;->f:LX/EYF;

    .line 2137369
    iget-object v0, p0, LX/EYR;->d:LX/EYQ;

    iget-object v0, v0, LX/EYQ;->h:LX/EYJ;

    iget-object v1, p0, LX/EYR;->b:LX/EXj;

    invoke-virtual {v1}, LX/EXj;->o()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/EYI;->TYPES_ONLY:LX/EYI;

    invoke-virtual {v0, v1, p0, v2}, LX/EYJ;->a(Ljava/lang/String;LX/EYE;LX/EYI;)LX/EYE;

    move-result-object v0

    .line 2137370
    instance-of v1, v0, LX/EYF;

    if-nez v1, :cond_1

    .line 2137371
    new-instance v0, LX/EYK;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/EYR;->b:LX/EXj;

    invoke-virtual {v2}, LX/EXj;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not a message type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2137372
    :cond_1
    check-cast v0, LX/EYF;

    iput-object v0, p0, LX/EYR;->g:LX/EYF;

    .line 2137373
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137364
    iget-object v0, p0, LX/EYR;->b:LX/EXj;

    invoke-virtual {v0}, LX/EXj;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2137363
    iget-object v0, p0, LX/EYR;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/EYQ;
    .locals 1

    .prologue
    .line 2137362
    iget-object v0, p0, LX/EYR;->d:LX/EYQ;

    return-object v0
.end method

.method public final h()LX/EWY;
    .locals 1

    .prologue
    .line 2137361
    iget-object v0, p0, LX/EYR;->b:LX/EXj;

    return-object v0
.end method
