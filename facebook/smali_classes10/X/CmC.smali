.class public final LX/CmC;
.super LX/Cm7;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cm7",
        "<",
        "Lcom/facebook/richdocument/model/data/BylineBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentBylineProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentBylineText;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentBylineProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1933239
    const/16 v0, 0xe

    invoke-direct {p0, v0}, LX/Cm7;-><init>(I)V

    .line 1933240
    iput-object p1, p0, LX/CmC;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;

    .line 1933241
    iput-object p2, p0, LX/CmC;->b:Ljava/util/List;

    .line 1933242
    return-void
.end method


# virtual methods
.method public final synthetic b()LX/Clr;
    .locals 1

    .prologue
    .line 1933243
    invoke-virtual {p0}, LX/CmC;->c()LX/CmD;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/CmD;
    .locals 1

    .prologue
    .line 1933244
    new-instance v0, LX/CmD;

    invoke-direct {v0, p0}, LX/CmD;-><init>(LX/CmC;)V

    return-object v0
.end method
