.class public LX/DzI;
.super LX/Dz7;
.source ""


# direct methods
.method public constructor <init>(LX/0zw;Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p1    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066746
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, LX/Dz7;-><init>(LX/0zw;Ljava/lang/String;Landroid/content/res/Resources;)V

    .line 2066747
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2066743
    invoke-virtual {p0}, LX/DzI;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2066744
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2066745
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2066734
    invoke-virtual {p0}, LX/DzI;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2066735
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2066736
    const v1, 0x7f081757

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 2066737
    const v1, 0x7f0816db

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2066738
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 2066739
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowCloseButton(Z)V

    .line 2066740
    const v1, 0x7f081762

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 2066741
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2066742
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;LX/AhV;)Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 2066723
    invoke-virtual {p0}, LX/DzI;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2066724
    if-nez v0, :cond_0

    move v0, v1

    .line 2066725
    :goto_0
    return v0

    .line 2066726
    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    .line 2066727
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2066728
    :goto_1
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnSecondaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2066729
    const v2, 0x7f081762

    new-array v3, v1, [Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSecondaryButtonText(Ljava/lang/CharSequence;)V

    .line 2066730
    const v2, 0x7f081761

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p0, v2, v1}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 2066731
    iput-object p2, v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->m:LX/AhV;

    .line 2066732
    const/4 v0, 0x1

    goto :goto_0

    .line 2066733
    :cond_1
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setBackgroundColor(I)V

    goto :goto_1
.end method

.method public final b()Landroid/view/View;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2066748
    iget-object v0, p0, LX/Dz7;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2066714
    invoke-virtual {p0}, LX/DzI;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2066715
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2066716
    const v1, 0x7f081756

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 2066717
    const v1, 0x7f08175c

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2066718
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2066719
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2066720
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowCloseButton(Z)V

    .line 2066721
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 2066722
    return-void
.end method

.method public final c(Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2066704
    invoke-virtual {p0}, LX/DzI;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2066705
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2066706
    const v1, 0x7f081755

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 2066707
    const v1, 0x7f08175b

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2066708
    const v1, 0x7f081760

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 2066709
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2066710
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2066711
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowCloseButton(Z)V

    .line 2066712
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 2066713
    return-void
.end method

.method public final d(Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2066695
    invoke-virtual {p0}, LX/DzI;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2066696
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2066697
    const v1, 0x7f081758

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 2066698
    const v1, 0x7f08175d

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2066699
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2066700
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2066701
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowCloseButton(Z)V

    .line 2066702
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 2066703
    return-void
.end method

.method public final e(Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2066686
    invoke-virtual {p0}, LX/DzI;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2066687
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2066688
    const v1, 0x7f081759

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 2066689
    const v1, 0x7f08175e

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2066690
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2066691
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2066692
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowCloseButton(Z)V

    .line 2066693
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 2066694
    return-void
.end method

.method public final f(Landroid/view/View$OnClickListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2066676
    invoke-virtual {p0}, LX/DzI;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2066677
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2066678
    const v1, 0x7f08175a

    new-array v2, v4, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 2066679
    const v1, 0x7f08175f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, LX/Dz7;->b:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2066680
    const v1, 0x7f081763

    new-array v2, v4, [Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, LX/Dz7;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 2066681
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2066682
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2066683
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowCloseButton(Z)V

    .line 2066684
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 2066685
    return-void
.end method
