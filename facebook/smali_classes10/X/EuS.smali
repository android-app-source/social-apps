.class public LX/EuS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2179606
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2179607
    invoke-static {}, LX/Eve;->a()LX/Evd;

    move-result-object v0

    .line 2179608
    const-string v1, "location"

    sget-object v2, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    iget-object v2, v2, LX/2hC;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "max"

    const-string v3, "250"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after"

    .line 2179609
    iget-object v3, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2179610
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    .line 2179611
    iget v3, p1, LX/3DR;->e:I

    move v3, v3

    .line 2179612
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2179613
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179614
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179615
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v5

    .line 2179616
    new-instance v0, LX/5Mb;

    .line 2179617
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2179618
    check-cast v1, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchSuggestionsGraphQLModels$FriendsCenterFetchSuggestionsQueryModel$PeopleYouMayKnowModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v4

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/5Mb;-><init>(LX/0Px;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method
