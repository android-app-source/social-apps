.class public final LX/DY9;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

.field public final synthetic b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;LX/DML;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;)V
    .locals 0

    .prologue
    .line 2011226
    iput-object p1, p0, LX/DY9;->b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iput-object p3, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2011227
    check-cast p1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2011228
    iget-object v0, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2011229
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2011230
    :goto_0
    return-void

    .line 2011231
    :cond_1
    const v0, 0x7f0d14f5

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2011232
    iget-object v1, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2011233
    const v0, 0x7f0d14f6

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2011234
    iget-object v1, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->o()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_2

    .line 2011235
    iget-object v1, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->o()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v4, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2011236
    :cond_2
    const v0, 0x7f0d14f3

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2011237
    iget-object v1, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    .line 2011238
    iget-object v1, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2011239
    invoke-virtual {v4, v1, v3}, LX/15i;->g(II)I

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    if-eqz v1, :cond_6

    .line 2011240
    iget-object v1, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2011241
    invoke-virtual {v4, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v4, v1, v3}, LX/15i;->g(II)I

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_2
    if-eqz v1, :cond_8

    .line 2011242
    iget-object v1, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v4, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v4, v1, v3}, LX/15i;->g(II)I

    move-result v1

    .line 2011243
    invoke-virtual {v4, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    :goto_3
    if-eqz v2, :cond_9

    .line 2011244
    iget-object v1, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v2, v1, v3}, LX/15i;->g(II)I

    move-result v1

    .line 2011245
    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2011246
    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2011247
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2011248
    :goto_4
    iget-object v0, p0, LX/DY9;->b:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v1, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/DY9;->a:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 2011249
    new-instance v4, LX/DY1;

    invoke-direct {v4, v0, v1, v2}, LX/DY1;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 2011250
    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2011251
    invoke-virtual {p1, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    move v1, v3

    .line 2011252
    goto/16 :goto_1

    :cond_4
    move v1, v3

    goto/16 :goto_1

    :cond_5
    move v1, v3

    goto :goto_2

    :cond_6
    move v1, v3

    goto :goto_2

    :cond_7
    move v2, v3

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_3

    .line 2011253
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    const v1, 0x7f020c6c

    invoke-virtual {v0, v1}, LX/1af;->b(I)V

    goto :goto_4
.end method
