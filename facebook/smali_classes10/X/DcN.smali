.class public LX/DcN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2018288
    iput-object p1, p0, LX/DcN;->a:LX/0tX;

    .line 2018289
    iput-object p2, p0, LX/DcN;->b:LX/1Ck;

    .line 2018290
    return-void
.end method

.method public static a(LX/0QB;)LX/DcN;
    .locals 5

    .prologue
    .line 2018291
    const-class v1, LX/DcN;

    monitor-enter v1

    .line 2018292
    :try_start_0
    sget-object v0, LX/DcN;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2018293
    sput-object v2, LX/DcN;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2018294
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2018295
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2018296
    new-instance p0, LX/DcN;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {p0, v3, v4}, LX/DcN;-><init>(LX/0tX;LX/1Ck;)V

    .line 2018297
    move-object v0, p0

    .line 2018298
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2018299
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DcN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2018300
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2018301
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
