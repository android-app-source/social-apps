.class public LX/ELj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0wM;

.field private final b:LX/23P;


# direct methods
.method public constructor <init>(LX/0wM;LX/23P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2109482
    iput-object p1, p0, LX/ELj;->a:LX/0wM;

    .line 2109483
    iput-object p2, p0, LX/ELj;->b:LX/23P;

    .line 2109484
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;)I
    .locals 3

    .prologue
    .line 2109485
    sget-object v0, LX/ELi;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2109486
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unimplemented page call to action of type %s"

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2109487
    :pswitch_0
    const v0, 0x7f020957

    .line 2109488
    :goto_0
    return v0

    .line 2109489
    :pswitch_1
    const v0, 0x7f020742

    goto :goto_0

    .line 2109490
    :pswitch_2
    const v0, 0x7f020954

    goto :goto_0

    .line 2109491
    :pswitch_3
    const v0, 0x7f020a21

    goto :goto_0

    .line 2109492
    :pswitch_4
    const v0, 0x7f0207ad

    goto :goto_0

    .line 2109493
    :pswitch_5
    const v0, 0x7f020852

    goto :goto_0

    .line 2109494
    :pswitch_6
    const v0, 0x7f0209cb

    goto :goto_0

    .line 2109495
    :pswitch_7
    const v0, 0x7f0208f9

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2109496
    new-instance v0, LX/ELh;

    invoke-direct {v0, p1, p0}, LX/ELh;-><init>(LX/Cxd;Lcom/facebook/graphql/model/GraphQLNode;)V

    return-object v0
.end method

.method public static b(LX/0QB;)LX/ELj;
    .locals 3

    .prologue
    .line 2109497
    new-instance v2, LX/ELj;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v1

    check-cast v1, LX/23P;

    invoke-direct {v2, v0, v1}, LX/ELj;-><init>(LX/0wM;LX/23P;)V

    .line 2109498
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPageCallToAction;Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)LX/EMF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2109499
    if-nez p1, :cond_0

    new-instance v0, LX/EMF;

    invoke-direct {v0, v4, v4, v4}, LX/EMF;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/EMF;

    iget-object v1, p0, LX/ELj;->a:LX/0wM;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v2

    invoke-static {v2}, LX/ELj;->a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;)I

    move-result v2

    const v3, -0x6f6b64

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, LX/ELj;->b:LX/23P;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v4}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {p2, p3}, LX/ELj;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/EMF;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
