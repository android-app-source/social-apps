.class public final LX/DfK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2026872
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 2026873
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2026874
    :goto_0
    return v1

    .line 2026875
    :cond_0
    const-string v6, "number_of_items_to_show_on_see_more_tap"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2026876
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 2026877
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2026878
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2026879
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2026880
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2026881
    const-string v6, "messenger_inbox_unit_see_more_style"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2026882
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerInboxUnitSeeMoreStyle;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 2026883
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2026884
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2026885
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2026886
    if-eqz v0, :cond_4

    .line 2026887
    invoke-virtual {p1, v2, v3, v1}, LX/186;->a(III)V

    .line 2026888
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method
