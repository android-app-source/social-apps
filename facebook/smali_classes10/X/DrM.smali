.class public final LX/DrM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/notifications/protocol/NotifOptionRowsMutationModels$NotifOptionActionMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V
    .locals 0

    .prologue
    .line 2049172
    iput-object p1, p0, LX/DrM;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2049173
    iget-object v0, p0, LX/DrM;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    invoke-static {v0}, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->d(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V

    .line 2049174
    iget-object v0, p0, LX/DrM;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->c:LX/03V;

    sget-object v1, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->k:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fail to sync local settings"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2049175
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2049176
    iget-object v0, p0, LX/DrM;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    invoke-static {v0}, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->d(Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V

    .line 2049177
    iget-object v0, p0, LX/DrM;->a:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->d:LX/Dqp;

    invoke-virtual {v0}, LX/Dqp;->c()V

    .line 2049178
    return-void
.end method
