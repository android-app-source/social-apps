.class public final LX/CsD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CsG;


# direct methods
.method public constructor <init>(LX/CsG;)V
    .locals 0

    .prologue
    .line 1942306
    iput-object p1, p0, LX/CsD;->a:LX/CsG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, 0x1b362ab7

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1942307
    iget-object v1, p0, LX/CsD;->a:LX/CsG;

    .line 1942308
    iput-boolean v2, v1, LX/CsG;->r:Z

    .line 1942309
    iget-object v1, p0, LX/CsD;->a:LX/CsG;

    iget-boolean v1, v1, LX/CsG;->n:Z

    if-eqz v1, :cond_1

    .line 1942310
    iget-object v1, p0, LX/CsD;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->o:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1942311
    iget-object v1, p0, LX/CsD;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->f:Landroid/content/Context;

    iget-object v2, p0, LX/CsD;->a:LX/CsG;

    iget-object v2, v2, LX/CsG;->o:Ljava/lang/String;

    invoke-static {v1, v2}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1942312
    iget-object v1, p0, LX/CsD;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->c:LX/Ckw;

    const-string v2, "copied_link"

    invoke-virtual {v1, v2}, LX/Ckw;->a(Ljava/lang/String;)V

    .line 1942313
    :cond_0
    :goto_0
    iget-object v1, p0, LX/CsD;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->k:LX/Csm;

    invoke-virtual {v1}, LX/0ht;->l()V

    .line 1942314
    const v1, -0x3033598c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1942315
    :cond_1
    iget-object v1, p0, LX/CsD;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->f:Landroid/content/Context;

    iget-object v2, p0, LX/CsD;->a:LX/CsG;

    iget-object v2, v2, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1942316
    iget-object v1, p0, LX/CsD;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->c:LX/Ckw;

    const-string v2, "copied_text"

    invoke-virtual {v1, v2}, LX/Ckw;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
