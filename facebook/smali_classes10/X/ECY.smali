.class public LX/ECY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final v:Ljava/lang/Object;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/1MZ;

.field public final d:Landroid/telephony/TelephonyManager;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3E8;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/EG0;

.field public final h:LX/0SG;

.field private final i:LX/0Sh;

.field public final j:Ljava/util/concurrent/ScheduledExecutorService;

.field private final k:Ljava/util/concurrent/ExecutorService;

.field private final l:LX/0Uo;

.field private final m:LX/0pu;

.field public final n:LX/0Xl;

.field public final o:LX/2S3;

.field private final p:Ljava/util/Random;

.field private final q:LX/0ad;

.field public r:Landroid/telephony/PhoneStateListener;

.field public s:LX/0Yb;

.field public final t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/ECT;",
            ">;"
        }
    .end annotation
.end field

.field public final u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/ECT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2090165
    const-class v0, LX/ECY;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ECY;->a:Ljava/lang/String;

    .line 2090166
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ECY;->v:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1MZ;Landroid/telephony/TelephonyManager;LX/0SG;LX/0Or;LX/0Or;LX/EG0;LX/0Uo;LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ExecutorService;LX/0pu;LX/0ad;LX/2S3;Ljava/util/Random;LX/0Xl;)V
    .locals 2
    .param p10    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p11    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p15    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .param p16    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1MZ;",
            "Landroid/telephony/TelephonyManager;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/EDx;",
            ">;",
            "LX/0Or",
            "<",
            "LX/3E8;",
            ">;",
            "LX/EG0;",
            "LX/0Uo;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0pu;",
            "LX/0ad;",
            "LX/2S3;",
            "Ljava/util/Random;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2090145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2090146
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, LX/ECY;->t:Ljava/util/Map;

    .line 2090147
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, LX/ECY;->u:Ljava/util/Map;

    .line 2090148
    iput-object p1, p0, LX/ECY;->b:Landroid/content/Context;

    .line 2090149
    iput-object p2, p0, LX/ECY;->c:LX/1MZ;

    .line 2090150
    iput-object p3, p0, LX/ECY;->d:Landroid/telephony/TelephonyManager;

    .line 2090151
    iput-object p4, p0, LX/ECY;->h:LX/0SG;

    .line 2090152
    iput-object p5, p0, LX/ECY;->e:LX/0Or;

    .line 2090153
    iput-object p6, p0, LX/ECY;->f:LX/0Or;

    .line 2090154
    iput-object p7, p0, LX/ECY;->g:LX/EG0;

    .line 2090155
    iput-object p8, p0, LX/ECY;->l:LX/0Uo;

    .line 2090156
    iput-object p9, p0, LX/ECY;->i:LX/0Sh;

    .line 2090157
    iput-object p10, p0, LX/ECY;->j:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2090158
    iput-object p11, p0, LX/ECY;->k:Ljava/util/concurrent/ExecutorService;

    .line 2090159
    iput-object p12, p0, LX/ECY;->m:LX/0pu;

    .line 2090160
    move-object/from16 v0, p14

    iput-object v0, p0, LX/ECY;->o:LX/2S3;

    .line 2090161
    move-object/from16 v0, p15

    iput-object v0, p0, LX/ECY;->p:Ljava/util/Random;

    .line 2090162
    move-object/from16 v0, p16

    iput-object v0, p0, LX/ECY;->n:LX/0Xl;

    .line 2090163
    iput-object p13, p0, LX/ECY;->q:LX/0ad;

    .line 2090164
    return-void
.end method

.method public static a(LX/0QB;)LX/ECY;
    .locals 7

    .prologue
    .line 2090118
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2090119
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2090120
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2090121
    if-nez v1, :cond_0

    .line 2090122
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2090123
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2090124
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2090125
    sget-object v1, LX/ECY;->v:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2090126
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2090127
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2090128
    :cond_1
    if-nez v1, :cond_4

    .line 2090129
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2090130
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2090131
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/ECY;->b(LX/0QB;)LX/ECY;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2090132
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2090133
    if-nez v1, :cond_2

    .line 2090134
    sget-object v0, LX/ECY;->v:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECY;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2090135
    :goto_1
    if-eqz v0, :cond_3

    .line 2090136
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2090137
    :goto_3
    check-cast v0, LX/ECY;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2090138
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2090139
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2090140
    :catchall_1
    move-exception v0

    .line 2090141
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2090142
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2090143
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2090144
    :cond_2
    :try_start_8
    sget-object v0, LX/ECY;->v:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECY;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/ECY;
    .locals 18

    .prologue
    .line 2090116
    new-instance v1, LX/ECY;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v3

    check-cast v3, LX/1MZ;

    invoke-static/range {p0 .. p0}, LX/0e7;->a(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const/16 v6, 0x3257

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x10e4

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/EBQ;->a(LX/0QB;)LX/EG0;

    move-result-object v8

    check-cast v8, LX/EG0;

    invoke-static/range {p0 .. p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v9

    check-cast v9, LX/0Uo;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v10

    check-cast v10, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0pu;->a(LX/0QB;)LX/0pu;

    move-result-object v13

    check-cast v13, LX/0pu;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/2S3;->a(LX/0QB;)LX/2S3;

    move-result-object v15

    check-cast v15, LX/2S3;

    invoke-static/range {p0 .. p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v16

    check-cast v16, Ljava/util/Random;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v17

    check-cast v17, LX/0Xl;

    invoke-direct/range {v1 .. v17}, LX/ECY;-><init>(Landroid/content/Context;LX/1MZ;Landroid/telephony/TelephonyManager;LX/0SG;LX/0Or;LX/0Or;LX/EG0;LX/0Uo;LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ExecutorService;LX/0pu;LX/0ad;LX/2S3;Ljava/util/Random;LX/0Xl;)V

    .line 2090117
    return-object v1
.end method

.method public static c(LX/ECY;LX/ECT;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2089997
    iget-object v1, p1, LX/ECT;->g:Ljava/lang/String;

    move-object v1, v1

    .line 2089998
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2089999
    :goto_0
    return v0

    .line 2090000
    :cond_0
    iget-object v1, p0, LX/ECY;->g:LX/EG0;

    .line 2090001
    iget-wide v4, p1, LX/ECT;->b:J

    move-wide v2, v4

    .line 2090002
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    invoke-interface {v1}, LX/EG0;->a()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v1

    .line 2090003
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2090004
    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, LX/ECT;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2090005
    :cond_1
    iget-object v0, p0, LX/ECY;->g:LX/EG0;

    invoke-interface {v0}, LX/EG0;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2090006
    iput-object v0, p1, LX/ECT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2090007
    new-instance v1, LX/ECX;

    invoke-direct {v1, p0, p1}, LX/ECX;-><init>(LX/ECY;LX/ECT;)V

    iget-object v2, p0, LX/ECY;->j:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2090008
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/ECY;LX/ECT;)V
    .locals 6

    .prologue
    .line 2090103
    iget-object v0, p1, LX/ECT;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2090104
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090105
    invoke-static {p1}, LX/ECY;->e(LX/ECT;)V

    .line 2090106
    iget-object v0, p0, LX/ECY;->t:Ljava/util/Map;

    .line 2090107
    iget-wide v4, p1, LX/ECT;->b:J

    move-wide v2, v4

    .line 2090108
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090109
    iget-object v0, p0, LX/ECY;->o:LX/2S3;

    .line 2090110
    iget-wide v4, p1, LX/ECT;->b:J

    move-wide v2, v4

    .line 2090111
    invoke-virtual {v0, v2, v3}, LX/2S3;->a(J)V

    .line 2090112
    :goto_0
    return-void

    .line 2090113
    :cond_0
    invoke-static {p0}, LX/ECY;->k(LX/ECY;)V

    .line 2090114
    invoke-virtual {p1}, LX/ECT;->h()V

    .line 2090115
    invoke-static {p0, p1}, LX/ECY;->f(LX/ECY;LX/ECT;)V

    goto :goto_0
.end method

.method public static e(LX/ECT;)V
    .locals 2

    .prologue
    .line 2090096
    iget-object v0, p0, LX/ECT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v0

    .line 2090097
    if-eqz v0, :cond_0

    .line 2090098
    iget-object v0, p0, LX/ECT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v0

    .line 2090099
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2090100
    const/4 v0, 0x0

    .line 2090101
    iput-object v0, p0, LX/ECT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2090102
    :cond_0
    return-void
.end method

.method public static f(LX/ECY;LX/ECT;)V
    .locals 6

    .prologue
    .line 2090071
    iget-object v0, p0, LX/ECY;->t:Ljava/util/Map;

    .line 2090072
    iget-wide v4, p1, LX/ECT;->b:J

    move-wide v2, v4

    .line 2090073
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_1

    .line 2090074
    :cond_0
    :goto_0
    return-void

    .line 2090075
    :cond_1
    invoke-virtual {p1}, LX/ECT;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2090076
    iget-object v0, p0, LX/ECY;->t:Ljava/util/Map;

    .line 2090077
    iget-wide v4, p1, LX/ECT;->b:J

    move-wide v2, v4

    .line 2090078
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090079
    iget-object v0, p0, LX/ECY;->o:LX/2S3;

    .line 2090080
    iget-wide v4, p1, LX/ECT;->b:J

    move-wide v2, v4

    .line 2090081
    invoke-virtual {v0, v2, v3}, LX/2S3;->a(J)V

    .line 2090082
    invoke-static {p1}, LX/ECY;->e(LX/ECT;)V

    goto :goto_0

    .line 2090083
    :cond_2
    iget-object v0, p1, LX/ECT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v0

    .line 2090084
    if-nez v0, :cond_0

    .line 2090085
    invoke-virtual {p1}, LX/ECT;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2090086
    iget-object v0, p0, LX/ECY;->t:Ljava/util/Map;

    .line 2090087
    iget-wide v4, p1, LX/ECT;->b:J

    move-wide v2, v4

    .line 2090088
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090089
    iget-object v0, p0, LX/ECY;->o:LX/2S3;

    .line 2090090
    iget-wide v4, p1, LX/ECT;->b:J

    move-wide v2, v4

    .line 2090091
    invoke-virtual {v0, v2, v3}, LX/2S3;->a(J)V

    .line 2090092
    invoke-virtual {p1}, LX/ECT;->i()V

    .line 2090093
    invoke-static {p1}, LX/ECY;->e(LX/ECT;)V

    goto :goto_0

    .line 2090094
    :cond_3
    goto :goto_0
    .line 2090095
.end method

.method public static g$redex0(LX/ECY;)V
    .locals 2

    .prologue
    .line 2090068
    iget-object v0, p0, LX/ECY;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090069
    :goto_0
    return-void

    .line 2090070
    :cond_0
    iget-object v0, p0, LX/ECY;->i:LX/0Sh;

    new-instance v1, Lcom/facebook/rtc/campon/RtcCampOnManager$2;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/campon/RtcCampOnManager$2;-><init>(LX/ECY;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static k(LX/ECY;)V
    .locals 3

    .prologue
    .line 2090050
    iget-object v0, p0, LX/ECY;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2090051
    invoke-static {p0}, LX/ECY;->l(LX/ECY;)V

    .line 2090052
    :goto_0
    return-void

    .line 2090053
    :cond_0
    iget-object v0, p0, LX/ECY;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 2090054
    goto :goto_1

    .line 2090055
    :goto_2
    goto :goto_6

    .line 2090056
    :goto_3
    iget-object v0, p0, LX/ECY;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECT;

    .line 2090057
    invoke-virtual {v0}, LX/ECT;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2090058
    const/4 v0, 0x1

    .line 2090059
    :goto_4
    move v0, v0

    .line 2090060
    if-eqz v0, :cond_3

    .line 2090061
    iget-object v0, p0, LX/ECY;->d:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_6

    .line 2090062
    :cond_2
    :goto_5
    goto :goto_0

    .line 2090063
    :goto_6
    invoke-direct {p0}, LX/ECY;->m()V

    goto :goto_3

    .line 2090064
    :cond_3
    invoke-direct {p0}, LX/ECY;->q()V

    goto :goto_0

    :cond_4
    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    .line 2090065
    :cond_6
    iget-object v0, p0, LX/ECY;->r:Landroid/telephony/PhoneStateListener;

    if-nez v0, :cond_2

    .line 2090066
    new-instance v0, LX/ECV;

    invoke-direct {v0, p0}, LX/ECV;-><init>(LX/ECY;)V

    iput-object v0, p0, LX/ECY;->r:Landroid/telephony/PhoneStateListener;

    .line 2090067
    iget-object v0, p0, LX/ECY;->d:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, LX/ECY;->r:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    goto :goto_5
.end method

.method public static l(LX/ECY;)V
    .locals 0

    .prologue
    .line 2090047
    invoke-direct {p0}, LX/ECY;->q()V

    .line 2090048
    invoke-direct {p0}, LX/ECY;->m()V

    .line 2090049
    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 2090044
    iget-object v0, p0, LX/ECY;->s:LX/0Yb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ECY;->s:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2090045
    :cond_0
    :goto_0
    return-void

    .line 2090046
    :cond_1
    iget-object v0, p0, LX/ECY;->s:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    goto :goto_0
.end method

.method private q()V
    .locals 3

    .prologue
    .line 2090039
    iget-object v0, p0, LX/ECY;->d:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_1

    .line 2090040
    :cond_0
    :goto_0
    return-void

    .line 2090041
    :cond_1
    iget-object v0, p0, LX/ECY;->r:Landroid/telephony/PhoneStateListener;

    if-eqz v0, :cond_0

    .line 2090042
    iget-object v0, p0, LX/ECY;->d:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, LX/ECY;->r:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 2090043
    const/4 v0, 0x0

    iput-object v0, p0, LX/ECY;->r:Landroid/telephony/PhoneStateListener;

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2090038
    iget-object v0, p0, LX/ECY;->b:Landroid/content/Context;

    return-object v0
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;ZJJJLjava/lang/String;)Z
    .locals 16

    .prologue
    .line 2090030
    new-instance v3, LX/ECU;

    move-object/from16 v4, p0

    move-wide/from16 v5, p1

    move/from16 v7, p5

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    move-wide/from16 v12, p10

    move-object/from16 v14, p12

    invoke-direct/range {v3 .. v14}, LX/ECU;-><init>(LX/ECY;JZJJJLjava/lang/String;)V

    .line 2090031
    move-object/from16 v0, p0

    iget-object v2, v0, LX/ECY;->t:Ljava/util/Map;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090032
    invoke-static/range {p4 .. p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2090033
    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/ECY;->c(LX/ECY;LX/ECT;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2090034
    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/ECY;->d(LX/ECY;LX/ECT;)V

    .line 2090035
    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 2090036
    :cond_1
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v3, v0, v1}, LX/ECT;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2090037
    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/ECY;->d(LX/ECY;LX/ECT;)V

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;ZJLjava/lang/String;)Z
    .locals 13

    .prologue
    .line 2090021
    iget-object v0, p0, LX/ECY;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 2090022
    const-wide/32 v10, 0x5265c00

    move-object v0, p0

    move-wide v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-wide/from16 v8, p6

    move-object/from16 v12, p8

    invoke-virtual/range {v0 .. v12}, LX/ECY;->a(JLjava/lang/String;Ljava/lang/String;ZJJJLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2090023
    const/4 v0, 0x0

    .line 2090024
    :goto_0
    return v0

    .line 2090025
    :cond_0
    if-eqz p5, :cond_1

    const/4 v1, 0x4

    .line 2090026
    :goto_1
    iget-object v0, p0, LX/ECY;->o:LX/2S3;

    const-wide/32 v8, 0x5265c00

    move-wide v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v10, p8

    invoke-virtual/range {v0 .. v10}, LX/2S3;->a(IJLjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    .line 2090027
    iget-object v0, p0, LX/ECY;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3E8;

    invoke-virtual {v0, p1, p2}, LX/3E8;->a(J)V

    .line 2090028
    const/4 v0, 0x1

    goto :goto_0

    .line 2090029
    :cond_1
    const/4 v1, 0x3

    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2090019
    invoke-static {p0}, LX/ECY;->g$redex0(LX/ECY;)V

    .line 2090020
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 2090011
    iget-object v0, p0, LX/ECY;->t:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECT;

    .line 2090012
    if-eqz v0, :cond_0

    .line 2090013
    iget-object v1, p0, LX/ECY;->i:LX/0Sh;

    new-instance v2, Lcom/facebook/rtc/campon/RtcCampOnManager$3;

    invoke-direct {v2, p0, p1, p2, v0}, Lcom/facebook/rtc/campon/RtcCampOnManager$3;-><init>(LX/ECY;JLX/ECT;)V

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2090014
    :cond_0
    iget-object v0, p0, LX/ECY;->u:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECT;

    .line 2090015
    if-eqz v0, :cond_1

    .line 2090016
    invoke-virtual {v0}, LX/ECT;->j()V

    .line 2090017
    iget-object v0, p0, LX/ECY;->u:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090018
    :cond_1
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2090010
    iget-object v0, p0, LX/ECY;->d:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ECY;->d:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2090009
    iget-object v0, p0, LX/ECY;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    return v0
.end method
