.class public final LX/D9R;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V
    .locals 0

    .prologue
    .line 1970110
    iput-object p1, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;B)V
    .locals 0

    .prologue
    .line 1970109
    invoke-direct {p0, p1}, LX/D9R;-><init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1970103
    iget-object v0, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->e()V

    .line 1970104
    iget-object v0, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    .line 1970105
    iput-boolean v1, v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->g:Z

    .line 1970106
    iput-boolean v1, p0, LX/D9R;->b:Z

    .line 1970107
    iget-object v0, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v2, v3}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;D)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1970108
    return v1
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1970099
    iget-object v0, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-static {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->i(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)Z

    move-result v0

    if-eqz v0, :cond_0

    cmpg-float v0, p3, v1

    if-ltz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-static {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->i(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)Z

    move-result v0

    if-nez v0, :cond_2

    cmpl-float v0, p3, v1

    if-lez v0, :cond_2

    .line 1970100
    :cond_1
    iget-object v0, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1970101
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1970102
    :cond_2
    iget-object v0, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-virtual {v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v0, 0x1

    .line 1970084
    iget-object v1, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    .line 1970085
    iput-boolean v0, v1, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->g:Z

    .line 1970086
    iget-boolean v1, p0, LX/D9R;->b:Z

    if-nez v1, :cond_0

    .line 1970087
    iput-boolean v0, p0, LX/D9R;->b:Z

    .line 1970088
    iget-object v1, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    const-wide/16 v4, 0x0

    invoke-static {v1, v4, v5}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;D)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1970089
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 1970090
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    .line 1970091
    sub-float/2addr v3, v1

    .line 1970092
    iget-object v1, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-static {v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->getCurrentTextBubbleView(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->getWidth()I

    move-result v4

    .line 1970093
    iget-object v1, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-static {v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->i(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/high16 v1, -0x40800000    # -1.0f

    :goto_0
    mul-float/2addr v1, v3

    int-to-float v3, v4

    div-float/2addr v1, v3

    .line 1970094
    const/4 v3, 0x0

    cmpl-float v3, v1, v3

    if-ltz v3, :cond_2

    .line 1970095
    iget-object v3, p0, LX/D9R;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    sub-float v1, v2, v1

    float-to-double v4, v1

    invoke-static {v3, v4, v5}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;D)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1970096
    :goto_1
    return v0

    :cond_1
    move v1, v2

    .line 1970097
    goto :goto_0

    .line 1970098
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
