.class public final LX/ESY;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/photos/base/photos/VaultPhoto;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;)V
    .locals 1

    .prologue
    .line 2123113
    iput-object p1, p0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 2123114
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ESY;->b:Z

    .line 2123115
    return-void
.end method

.method public constructor <init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;Z)V
    .locals 0

    .prologue
    .line 2123116
    iput-object p1, p0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 2123117
    iput-boolean p2, p0, LX/ESY;->b:Z

    .line 2123118
    return-void
.end method

.method private varargs a()Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/photos/VaultPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2123119
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v11

    .line 2123120
    move-object/from16 v0, p0

    iget-object v2, v0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v2, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->a:LX/ERL;

    invoke-virtual {v2}, LX/ERL;->a()J

    move-result-wide v2

    .line 2123121
    move-object/from16 v0, p0

    iget-object v4, v0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v4, v4, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->i:LX/ES8;

    const/16 v5, 0x32

    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/ESY;->b:Z

    invoke-virtual {v4, v2, v3, v5, v6}, LX/ES8;->a(JIZ)Ljava/util/List;

    move-result-object v4

    .line 2123122
    move-object/from16 v0, p0

    iget-object v5, v0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v5, v5, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->j:LX/2TN;

    invoke-virtual {v5, v2, v3}, LX/2TN;->a(J)Ljava/util/Map;

    move-result-object v12

    .line 2123123
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v13

    .line 2123124
    move-object/from16 v0, p0

    iget-object v2, v0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-static {v2, v3}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->a(Lcom/facebook/vault/ui/VaultSyncScreenFragment;Ljava/util/Set;)Ljava/util/Set;

    .line 2123125
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2123126
    iget-object v3, v2, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-interface {v12, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v8, v3

    check-cast v8, Lcom/facebook/ipc/media/MediaItem;

    .line 2123127
    if-eqz v8, :cond_1

    new-instance v3, Ljava/io/File;

    invoke-virtual {v8}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2123128
    iget v3, v2, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 2123129
    new-instance v3, Lcom/facebook/photos/base/photos/VaultLocalPhoto;

    const-wide/16 v4, 0x0

    invoke-virtual {v8}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v8}, Lcom/facebook/ipc/media/MediaItem;->j()J

    move-result-wide v8

    invoke-static {}, LX/ESY;->b()I

    move-result v10

    int-to-long v0, v10

    move-wide/from16 v16, v0

    add-long v8, v8, v16

    iget-object v10, v2, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/photos/base/photos/VaultLocalPhoto;-><init>(JLjava/lang/String;IJLjava/lang/String;)V

    .line 2123130
    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2123131
    move-object/from16 v0, p0

    iget-object v3, v0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v3, v3, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->r:Ljava/util/Set;

    iget-object v2, v2, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2123132
    :cond_1
    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2123133
    :cond_2
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 2123134
    move-object/from16 v0, p0

    iget-object v2, v0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v2, v2, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->i:LX/ES8;

    invoke-virtual {v2, v13}, LX/ES8;->a(Ljava/util/List;)V

    .line 2123135
    :cond_3
    return-object v11
.end method

.method private static b()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2123136
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    .line 2123137
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    .line 2123138
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 2123139
    invoke-virtual {v2, v4}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v0

    .line 2123140
    :goto_0
    invoke-virtual {v3, v4}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v1

    .line 2123141
    :cond_0
    invoke-virtual {v3}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {v2}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int v0, v1, v0

    return v0

    :cond_1
    move v0, v1

    .line 2123142
    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2123143
    invoke-direct {p0}, LX/ESY;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2123144
    check-cast p1, Ljava/util/List;

    .line 2123145
    iget-object v0, p0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->w:LX/ES9;

    invoke-interface {v0, p1}, LX/ES9;->a(Ljava/util/List;)V

    .line 2123146
    iget-object v0, p0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->c()V

    .line 2123147
    iget-object v0, p0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->z:Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;

    invoke-virtual {v0}, Lcom/facebook/vault/ui/VaultSyncScreenPrivacyBar;->a()V

    .line 2123148
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2123149
    iget-object v0, p0, LX/ESY;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->b:LX/2TK;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, LX/2TK;->c(I)V

    .line 2123150
    :cond_0
    return-void
.end method
