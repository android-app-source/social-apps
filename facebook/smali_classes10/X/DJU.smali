.class public final LX/DJU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DJH;


# instance fields
.field public final synthetic a:LX/DJc;


# direct methods
.method public constructor <init>(LX/DJc;)V
    .locals 0

    .prologue
    .line 1985300
    iput-object p1, p0, LX/DJU;->a:LX/DJc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1985301
    iget-object v0, p0, LX/DJU;->a:LX/DJc;

    iget-object v0, v0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getShouldPostToMarketplace()Z

    move-result v4

    .line 1985302
    iget-object v3, p0, LX/DJU;->a:LX/DJc;

    if-eqz v4, :cond_0

    iget-object v0, p0, LX/DJU;->a:LX/DJc;

    iget-object v0, v0, LX/DJc;->m:LX/DIv;

    .line 1985303
    iget-wide v10, v0, LX/DIv;->l:J

    move-wide v0, v10

    .line 1985304
    :goto_0
    invoke-static {v3, v0, v1}, LX/DJc;->a$redex0(LX/DJc;J)V

    .line 1985305
    iget-object v0, p0, LX/DJU;->a:LX/DJc;

    invoke-static {v0}, LX/DJc;->aT(LX/DJc;)V

    .line 1985306
    iget-object v0, p0, LX/DJU;->a:LX/DJc;

    invoke-static {v0}, LX/DJc;->aU(LX/DJc;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJh;

    .line 1985307
    iget-object v0, v0, LX/DJh;->a:Ljava/lang/String;

    iget-object v5, p0, LX/DJU;->a:LX/DJc;

    iget-object v5, v5, LX/DJc;->k:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1985308
    const/4 v2, 0x1

    goto :goto_1

    .line 1985309
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 1985310
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1985311
    goto :goto_1

    .line 1985312
    :cond_2
    iget-object v0, p0, LX/DJU;->a:LX/DJc;

    .line 1985313
    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v0, v3

    .line 1985314
    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    invoke-static {v0}, LX/DJc;->b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Ljava/lang/String;

    move-result-object v3

    .line 1985315
    iget-object v0, p0, LX/DJU;->a:LX/DJc;

    .line 1985316
    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v5

    move-object v0, v5

    .line 1985317
    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-wide v6, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    iget-object v0, p0, LX/DJU;->a:LX/DJc;

    iget-object v0, v0, LX/DJc;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-nez v0, :cond_3

    iget-object v0, p0, LX/DJU;->a:LX/DJc;

    .line 1985318
    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v5

    move-object v0, v5

    .line 1985319
    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v5, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v0, v5, :cond_3

    .line 1985320
    iget-object v0, p0, LX/DJU;->a:LX/DJc;

    .line 1985321
    iget-object v3, v0, LX/AQ9;->b:Landroid/content/Context;

    move-object v0, v3

    .line 1985322
    const v3, 0x7f082f1f

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1985323
    :goto_2
    iget-object v3, p0, LX/DJU;->a:LX/DJc;

    iget-object v3, v3, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v3, v0, v4, v2, v1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Ljava/lang/String;ZZI)V

    .line 1985324
    return-void

    :cond_3
    move-object v0, v3

    goto :goto_2
.end method
