.class public final LX/DYs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2012618
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 2012619
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2012620
    :goto_0
    return v1

    .line 2012621
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_5

    .line 2012622
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2012623
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2012624
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 2012625
    const-string v12, "group_friend_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 2012626
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v10, v7

    move v7, v6

    goto :goto_1

    .line 2012627
    :cond_1
    const-string v12, "inviter"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2012628
    invoke-static {p0, p1}, LX/DYl;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2012629
    :cond_2
    const-string v12, "node"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2012630
    invoke-static {p0, p1}, LX/DYr;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2012631
    :cond_3
    const-string v12, "request_time"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2012632
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 2012633
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2012634
    :cond_5
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2012635
    if-eqz v7, :cond_6

    .line 2012636
    invoke-virtual {p1, v1, v10, v1}, LX/186;->a(III)V

    .line 2012637
    :cond_6
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 2012638
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 2012639
    if-eqz v0, :cond_7

    .line 2012640
    const/4 v1, 0x3

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2012641
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move-wide v2, v4

    move v8, v1

    move v9, v1

    move v10, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 2012642
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2012643
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 2012644
    if-eqz v0, :cond_0

    .line 2012645
    const-string v1, "group_friend_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012646
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2012647
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2012648
    if-eqz v0, :cond_1

    .line 2012649
    const-string v1, "inviter"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012650
    invoke-static {p0, v0, p2}, LX/DYl;->a(LX/15i;ILX/0nX;)V

    .line 2012651
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2012652
    if-eqz v0, :cond_2

    .line 2012653
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012654
    invoke-static {p0, v0, p2, p3}, LX/DYr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2012655
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2012656
    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    .line 2012657
    const-string v2, "request_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012658
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2012659
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2012660
    return-void
.end method
