.class public LX/Eqw;
.super LX/A8Z;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/A8Z",
        "<",
        "LX/Eqx;",
        ">;",
        "Lcom/facebook/widget/recyclerview/StickyHeaderItemDecorator$StickyHeaderAdapter",
        "<",
        "Ljava/lang/Integer;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/Eqx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2172280
    invoke-direct {p0, p1}, LX/A8Z;-><init>(LX/0Px;)V

    .line 2172281
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2172282
    invoke-virtual {p0, p1}, LX/A8Z;->e(I)LX/A8P;

    move-result-object v1

    .line 2172283
    iget-object v0, v1, LX/A8P;->b:LX/90h;

    check-cast v0, LX/Eqx;

    iget v1, v1, LX/A8P;->a:I

    const/4 p0, 0x0

    .line 2172284
    iget-boolean v2, v0, LX/Eqx;->b:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, LX/1OM;->getItemViewType(I)I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v2, p0

    .line 2172285
    :goto_0
    move-object v0, v2

    .line 2172286
    return-object v0

    .line 2172287
    :cond_1
    invoke-virtual {v0, v1}, LX/Eqx;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2172288
    invoke-virtual {v2}, LX/8QK;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 2172289
    invoke-virtual {v2}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    :cond_2
    move-object v2, p0

    .line 2172290
    goto :goto_0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2172291
    check-cast p2, Ljava/lang/Integer;

    .line 2172292
    if-nez p2, :cond_0

    .line 2172293
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2172294
    :goto_0
    return-void

    .line 2172295
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2172296
    const v0, 0x7f0d12a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
