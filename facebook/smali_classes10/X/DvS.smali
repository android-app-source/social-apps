.class public final LX/DvS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;",
        ">;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2058522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2058523
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2058524
    if-eqz p1, :cond_0

    .line 2058525
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058526
    if-nez v0, :cond_1

    .line 2058527
    :cond_0
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2058528
    :goto_0
    return-object v0

    .line 2058529
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058530
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;

    .line 2058531
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2058532
    :cond_2
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2058533
    :goto_1
    move-object v1, v1

    .line 2058534
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058535
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;

    .line 2058536
    new-instance v2, LX/17L;

    invoke-direct {v2}, LX/17L;-><init>()V

    .line 2058537
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    if-nez v3, :cond_6

    .line 2058538
    :cond_3
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 2058539
    :goto_2
    move-object v0, v2

    .line 2058540
    new-instance v2, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;

    invoke-direct {v2, v0, v1}, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;-><init>(Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0Px;)V

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2058541
    :cond_4
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2058542
    const/4 v1, 0x0

    move v2, v1

    :goto_3
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_5

    .line 2058543
    new-instance p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8IG;

    invoke-static {v1}, LX/8I6;->a(LX/8IG;)LX/8IH;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(LX/8IH;)V

    invoke-virtual {v3, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2058544
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2058545
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_1

    .line 2058546
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v3

    .line 2058547
    iput-object v3, v2, LX/17L;->f:Ljava/lang/String;

    .line 2058548
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2058549
    iput-object v3, v2, LX/17L;->c:Ljava/lang/String;

    .line 2058550
    const/4 v3, 0x0

    .line 2058551
    iput-boolean v3, v2, LX/17L;->d:Z

    .line 2058552
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v3

    .line 2058553
    iput-boolean v3, v2, LX/17L;->e:Z

    .line 2058554
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    goto/16 :goto_2
.end method
