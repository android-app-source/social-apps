.class public LX/ELK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2108473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ZLcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Landroid/view/View$OnClickListener;)LX/EL8;
    .locals 3

    .prologue
    .line 2108474
    if-eqz p0, :cond_1

    const/4 p2, 0x0

    const p0, -0xa76f01

    .line 2108475
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->HOST:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_2

    .line 2108476
    :cond_0
    new-instance v0, LX/EL8;

    const v1, 0x7f020857

    const v2, 0x7f082273

    invoke-direct {v0, v1, p0, v2, p2}, LX/EL8;-><init>(IIILandroid/view/View$OnClickListener;)V

    .line 2108477
    :goto_0
    move-object v0, v0

    .line 2108478
    :goto_1
    return-object v0

    :cond_1
    const/4 p1, 0x0

    const p0, -0xa76f01

    .line 2108479
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p2, v0, :cond_4

    .line 2108480
    new-instance v0, LX/EL8;

    const v1, 0x7f020857

    const v2, 0x7f082273

    invoke-direct {v0, v1, p0, v2, p1}, LX/EL8;-><init>(IIILandroid/view/View$OnClickListener;)V

    .line 2108481
    :goto_2
    move-object v0, v0

    .line 2108482
    goto :goto_1

    .line 2108483
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v0, :cond_3

    .line 2108484
    new-instance v0, LX/EL8;

    const v1, 0x7f020855

    const v2, 0x7f082272

    invoke-direct {v0, v1, p0, v2, p2}, LX/EL8;-><init>(IIILandroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2108485
    :cond_3
    new-instance v0, LX/EL8;

    const v1, 0x7f020855

    const v2, -0x6e685d

    const p0, 0x7f082271

    invoke-direct {v0, v1, v2, p0, p3}, LX/EL8;-><init>(IIILandroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2108486
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p2, v0, :cond_5

    .line 2108487
    new-instance v0, LX/EL8;

    const v1, 0x7f02085a

    const v2, 0x7f082270

    invoke-direct {v0, v1, p0, v2, p1}, LX/EL8;-><init>(IIILandroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2108488
    :cond_5
    new-instance v0, LX/EL8;

    const v1, 0x7f02085a

    const v2, -0x6e685d

    const p0, 0x7f08226f

    invoke-direct {v0, v1, v2, p0, p3}, LX/EL8;-><init>(IIILandroid/view/View$OnClickListener;)V

    goto :goto_2
.end method
