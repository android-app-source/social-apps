.class public final LX/Ekh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2163712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163713
    return-void
.end method

.method public static a(Landroid/util/JsonReader;I)I
    .locals 2

    .prologue
    .line 2163714
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v0

    .line 2163715
    sget-object v1, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v0, v1, :cond_0

    .line 2163716
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextNull()V

    .line 2163717
    :goto_0
    return p1

    :cond_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextInt()I

    move-result p1

    goto :goto_0
.end method

.method public static a(Landroid/util/JsonReader;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2163718
    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v0

    sget-object v1, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v0, v1, :cond_0

    .line 2163719
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextNull()V

    .line 2163720
    const/4 v0, 0x0

    .line 2163721
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
