.class public LX/Cum;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Cul;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/Cuk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1947148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1947149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Cum;->a:Ljava/util/List;

    .line 1947150
    return-void
.end method


# virtual methods
.method public final a(LX/Cuk;LX/Cuj;LX/Cuk;)V
    .locals 3

    .prologue
    .line 1947151
    iget-object v0, p0, LX/Cum;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1947152
    iget-object v0, p0, LX/Cum;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cul;

    .line 1947153
    iget-object v2, v0, LX/Cul;->a:LX/Cuk;

    move-object v2, v2

    .line 1947154
    invoke-virtual {v2, p1}, LX/Cuk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1947155
    iget-object v2, v0, LX/Cul;->b:LX/Cuj;

    move-object v0, v2

    .line 1947156
    invoke-virtual {v0, p2}, LX/Cuj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1947157
    iget-object v0, p0, LX/Cum;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1947158
    :cond_0
    iget-object v0, p0, LX/Cum;->a:Ljava/util/List;

    new-instance v1, LX/Cul;

    invoke-direct {v1, p1, p2, p3}, LX/Cul;-><init>(LX/Cuk;LX/Cuj;LX/Cuk;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1947159
    return-void

    .line 1947160
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method
