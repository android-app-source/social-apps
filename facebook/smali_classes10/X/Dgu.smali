.class public final LX/Dgu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dgr;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;)V
    .locals 0

    .prologue
    .line 2029967
    iput-object p1, p0, LX/Dgu;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2029965
    iget-object v0, p0, LX/Dgu;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->c:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    iget-object v1, p0, LX/Dgu;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    const v2, 0x7f082edd

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a(Ljava/lang/String;)V

    .line 2029966
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/location/sending/NearbyPlace;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2029960
    iget-object v0, p0, LX/Dgu;->a:Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;

    .line 2029961
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2029962
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->c:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    invoke-virtual {v1, p1}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a(LX/0Px;)V

    .line 2029963
    :goto_0
    return-void

    .line 2029964
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/NearbyPlacesSearchResultsFragment;->c:Lcom/facebook/messaging/location/sending/NearbyPlacesView;

    const p0, 0x7f082edd

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/messaging/location/sending/NearbyPlacesView;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
