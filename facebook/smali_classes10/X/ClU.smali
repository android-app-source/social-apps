.class public LX/ClU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/ClT;

.field public final b:Ljava/lang/String;

.field public final c:LX/ClS;

.field public final d:LX/ClQ;

.field public final e:LX/ClR;

.field private final f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

.field public final g:LX/8Z4;


# direct methods
.method public constructor <init>(LX/ClT;Ljava/lang/String;LX/8Z4;LX/ClS;LX/ClQ;LX/ClR;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V
    .locals 1

    .prologue
    .line 1932498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1932499
    iput-object p1, p0, LX/ClU;->a:LX/ClT;

    .line 1932500
    iput-object p2, p0, LX/ClU;->b:Ljava/lang/String;

    .line 1932501
    iput-object p3, p0, LX/ClU;->g:LX/8Z4;

    .line 1932502
    iput-object p4, p0, LX/ClU;->c:LX/ClS;

    .line 1932503
    iput-object p6, p0, LX/ClU;->e:LX/ClR;

    .line 1932504
    iput-object p7, p0, LX/ClU;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    .line 1932505
    invoke-static {p7, p1, p4}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;LX/ClT;LX/ClS;)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    .line 1932506
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 1932507
    :goto_0
    if-nez p5, :cond_0

    if-eqz v0, :cond_0

    move-object p5, v0

    :cond_0
    iput-object p5, p0, LX/ClU;->d:LX/ClQ;

    .line 1932508
    return-void

    .line 1932509
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->j()Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;

    move-result-object v0

    invoke-static {v0}, LX/ClQ;->from(Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;)LX/ClQ;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(LX/ClT;Ljava/lang/String;LX/ClS;LX/ClQ;LX/ClR;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V
    .locals 8

    .prologue
    .line 1932519
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, LX/ClU;-><init>(LX/ClT;Ljava/lang/String;LX/8Z4;LX/ClS;LX/ClQ;LX/ClR;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V

    .line 1932520
    return-void
.end method

.method public static a(LX/ClT;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)LX/ClU;
    .locals 8

    .prologue
    .line 1932510
    if-nez p1, :cond_0

    .line 1932511
    const/4 v0, 0x0

    .line 1932512
    :goto_0
    return-object v0

    .line 1932513
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1932514
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->d()Lcom/facebook/graphql/enums/GraphQLTextAnnotationPresentationStyle;

    move-result-object v0

    invoke-static {v0}, LX/ClS;->from(Lcom/facebook/graphql/enums/GraphQLTextAnnotationPresentationStyle;)LX/ClS;

    move-result-object v4

    .line 1932515
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->c()Lcom/facebook/graphql/enums/GraphQLTextAnnotationHorizontalPosition;

    move-result-object v0

    invoke-static {v0}, LX/ClQ;->from(Lcom/facebook/graphql/enums/GraphQLTextAnnotationHorizontalPosition;)LX/ClQ;

    move-result-object v5

    .line 1932516
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->e()Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;

    move-result-object v0

    invoke-static {v0}, LX/ClR;->from(Lcom/facebook/graphql/enums/GraphQLTextAnnotationVerticalPosition;)LX/ClR;

    move-result-object v6

    .line 1932517
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;->a()LX/8Z4;

    move-result-object v3

    .line 1932518
    new-instance v0, LX/ClU;

    move-object v1, p0

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/ClU;-><init>(LX/ClT;Ljava/lang/String;LX/8Z4;LX/ClS;LX/ClQ;LX/ClR;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V

    goto :goto_0
.end method
