.class public final LX/Euh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;",
        ">;",
        "Landroid/util/SparseArray",
        "<",
        "LX/Euu;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Eui;


# direct methods
.method public constructor <init>(LX/Eui;)V
    .locals 0

    .prologue
    .line 2179870
    iput-object p1, p0, LX/Euh;->a:LX/Eui;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2179841
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2179842
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 2179843
    new-instance v0, LX/Euu;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v5, v2, v3}, LX/Euu;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 2179844
    invoke-virtual {v1, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2179845
    if-eqz p1, :cond_0

    .line 2179846
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179847
    if-eqz v0, :cond_0

    .line 2179848
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179849
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2179850
    :goto_0
    return-object v0

    .line 2179851
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179852
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;

    move-result-object v2

    .line 2179853
    invoke-virtual {v2}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;->k()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$HometownModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2179854
    invoke-virtual {v2}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;->k()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$HometownModel;

    move-result-object v0

    .line 2179855
    new-instance v3, LX/Euu;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$HometownModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$HometownModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v6, v4, v0}, LX/Euu;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 2179856
    invoke-virtual {v1, v6, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2179857
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2179858
    invoke-virtual {v2}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;

    move-result-object v0

    .line 2179859
    new-instance v3, LX/Euu;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$CurrentCityModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v7, v4, v0}, LX/Euu;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 2179860
    invoke-virtual {v1, v7, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2179861
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;->j()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2179862
    invoke-virtual {v2}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;->j()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;

    move-result-object v0

    .line 2179863
    new-instance v3, LX/Euu;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$EducationExperiencesModel$NodesModel$SchoolModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v8, v4, v0}, LX/Euu;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 2179864
    invoke-virtual {v1, v8, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2179865
    :cond_4
    invoke-virtual {v2}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;->l()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel$NodesModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel$NodesModel$EmployerModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2179866
    invoke-virtual {v2}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel;->l()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel$NodesModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel$NodesModel$EmployerModel;

    move-result-object v0

    .line 2179867
    new-instance v2, LX/Euu;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel$NodesModel$EmployerModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKAttributesModels$FriendsCenterFetchContextualPYMKAttributesQueryModel$AccountUserModel$WorkExperiencesModel$NodesModel$EmployerModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v9, v3, v0}, LX/Euu;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 2179868
    invoke-virtual {v1, v9, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_5
    move-object v0, v1

    .line 2179869
    goto/16 :goto_0
.end method
