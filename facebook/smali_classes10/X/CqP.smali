.class public final LX/CqP;
.super LX/1PB;
.source ""


# instance fields
.field public final synthetic a:LX/CqR;


# direct methods
.method public constructor <init>(LX/CqR;)V
    .locals 0

    .prologue
    .line 1939667
    iput-object p1, p0, LX/CqP;->a:LX/CqR;

    invoke-direct {p0}, LX/1PB;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1939668
    iget-object v0, p0, LX/CqP;->a:LX/CqR;

    iget-object v0, v0, LX/CqR;->d:LX/0YU;

    invoke-virtual {v0, p2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqQ;

    .line 1939669
    if-eqz v0, :cond_0

    .line 1939670
    iget-object v2, v0, LX/CqQ;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v0, LX/CqQ;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1939671
    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 1939672
    :goto_1
    return-object v0

    .line 1939673
    :cond_1
    iget-object v2, v0, LX/CqQ;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1a1;

    .line 1939674
    iget-object v3, v0, LX/CqQ;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v3, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1939675
    if-nez v2, :cond_2

    iget-object v3, v0, LX/CqQ;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1939676
    iget-object v2, v0, LX/CqQ;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1a1;

    .line 1939677
    :cond_2
    move-object v0, v2

    .line 1939678
    if-nez v0, :cond_3

    move-object v0, v1

    .line 1939679
    goto :goto_1

    .line 1939680
    :cond_3
    iget-object v1, p0, LX/CqP;->a:LX/CqR;

    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    invoke-static {v1, v2}, LX/CqR;->n(LX/CqR;Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1939681
    iget-object v1, p0, LX/CqP;->a:LX/CqR;

    iget-object v1, v1, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    .line 1939682
    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v2

    .line 1939683
    invoke-virtual {v1, v0, p1}, LX/1OM;->b(LX/1a1;I)V

    .line 1939684
    :cond_4
    iget-object v1, p0, LX/CqP;->a:LX/CqR;

    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, LX/1OR;->f(Landroid/view/View;)V

    .line 1939685
    iget-object v1, p0, LX/CqP;->a:LX/CqR;

    iget-boolean v1, v1, LX/CqR;->t:Z

    if-nez v1, :cond_5

    .line 1939686
    iget-object v1, p0, LX/CqP;->a:LX/CqR;

    iget-object v1, v1, LX/CqR;->f:Landroid/support/v7/widget/RecyclerView;

    .line 1939687
    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v2

    .line 1939688
    invoke-virtual {v1, v0}, LX/1OM;->c(LX/1a1;)V

    .line 1939689
    :cond_5
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_0
.end method
