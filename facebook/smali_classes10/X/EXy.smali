.class public final LX/EXy;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EXx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EXy;",
        ">;",
        "LX/EXx;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EY2;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EY2;",
            "LX/EY1;",
            "LX/EY0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2135879
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2135880
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXy;->b:Ljava/util/List;

    .line 2135881
    invoke-direct {p0}, LX/EXy;->n()V

    .line 2135882
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2135883
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2135884
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXy;->b:Ljava/util/List;

    .line 2135885
    invoke-direct {p0}, LX/EXy;->n()V

    .line 2135886
    return-void
.end method

.method private A()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EY2;",
            "LX/EY1;",
            "LX/EY0;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2135887
    iget-object v1, p0, LX/EXy;->c:LX/EZ2;

    if-nez v1, :cond_0

    .line 2135888
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EXy;->b:Ljava/util/List;

    iget v3, p0, LX/EXy;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2135889
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2135890
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EXy;->c:LX/EZ2;

    .line 2135891
    const/4 v0, 0x0

    iput-object v0, p0, LX/EXy;->b:Ljava/util/List;

    .line 2135892
    :cond_0
    iget-object v0, p0, LX/EXy;->c:LX/EZ2;

    return-object v0

    .line 2135893
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/EXy;
    .locals 1

    .prologue
    .line 2135894
    instance-of v0, p1, LX/EY3;

    if-eqz v0, :cond_0

    .line 2135895
    check-cast p1, LX/EY3;

    invoke-virtual {p0, p1}, LX/EXy;->a(LX/EY3;)LX/EXy;

    move-result-object p0

    .line 2135896
    :goto_0
    return-object p0

    .line 2135897
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EXy;
    .locals 4

    .prologue
    .line 2135898
    const/4 v2, 0x0

    .line 2135899
    :try_start_0
    sget-object v0, LX/EY3;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EY3;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2135900
    if-eqz v0, :cond_0

    .line 2135901
    invoke-virtual {p0, v0}, LX/EXy;->a(LX/EY3;)LX/EXy;

    .line 2135902
    :cond_0
    return-object p0

    .line 2135903
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2135904
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2135905
    check-cast v0, LX/EY3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2135906
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2135907
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2135908
    invoke-virtual {p0, v1}, LX/EXy;->a(LX/EY3;)LX/EXy;

    :cond_1
    throw v0

    .line 2135909
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 2135910
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2135911
    invoke-direct {p0}, LX/EXy;->A()LX/EZ2;

    .line 2135912
    :cond_0
    return-void
.end method

.method public static u()LX/EXy;
    .locals 1

    .prologue
    .line 2135876
    new-instance v0, LX/EXy;

    invoke-direct {v0}, LX/EXy;-><init>()V

    return-object v0
.end method

.method private w()LX/EXy;
    .locals 2

    .prologue
    .line 2135917
    invoke-static {}, LX/EXy;->u()LX/EXy;

    move-result-object v0

    invoke-virtual {p0}, LX/EXy;->l()LX/EY3;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EXy;->a(LX/EY3;)LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/EY3;
    .locals 2

    .prologue
    .line 2135913
    invoke-virtual {p0}, LX/EXy;->l()LX/EY3;

    move-result-object v0

    .line 2135914
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2135915
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2135916
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2135941
    invoke-direct {p0, p1}, LX/EXy;->d(LX/EWY;)LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2135942
    invoke-direct {p0, p1, p2}, LX/EXy;->d(LX/EWd;LX/EYZ;)LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EY3;)LX/EXy;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2135919
    sget-object v1, LX/EY3;->c:LX/EY3;

    move-object v1, v1

    .line 2135920
    if-ne p1, v1, :cond_0

    .line 2135921
    :goto_0
    return-object p0

    .line 2135922
    :cond_0
    iget-object v1, p0, LX/EXy;->c:LX/EZ2;

    if-nez v1, :cond_4

    .line 2135923
    iget-object v0, p1, LX/EY3;->location_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2135924
    iget-object v0, p0, LX/EXy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2135925
    iget-object v0, p1, LX/EY3;->location_:Ljava/util/List;

    iput-object v0, p0, LX/EXy;->b:Ljava/util/List;

    .line 2135926
    iget v0, p0, LX/EXy;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, LX/EXy;->a:I

    .line 2135927
    :goto_1
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2135928
    :cond_1
    :goto_2
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    .line 2135929
    :cond_2
    iget v0, p0, LX/EXy;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    .line 2135930
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EXy;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EXy;->b:Ljava/util/List;

    .line 2135931
    iget v0, p0, LX/EXy;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EXy;->a:I

    .line 2135932
    :cond_3
    iget-object v0, p0, LX/EXy;->b:Ljava/util/List;

    iget-object v1, p1, LX/EY3;->location_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2135933
    :cond_4
    iget-object v1, p1, LX/EY3;->location_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2135934
    iget-object v1, p0, LX/EXy;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2135935
    iget-object v1, p0, LX/EXy;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2135936
    iput-object v0, p0, LX/EXy;->c:LX/EZ2;

    .line 2135937
    iget-object v1, p1, LX/EY3;->location_:Ljava/util/List;

    iput-object v1, p0, LX/EXy;->b:Ljava/util/List;

    .line 2135938
    iget v1, p0, LX/EXy;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/EXy;->a:I

    .line 2135939
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_5

    invoke-direct {p0}, LX/EXy;->A()LX/EZ2;

    move-result-object v0

    :cond_5
    iput-object v0, p0, LX/EXy;->c:LX/EZ2;

    goto :goto_2

    .line 2135940
    :cond_6
    iget-object v0, p0, LX/EXy;->c:LX/EZ2;

    iget-object v1, p1, LX/EY3;->location_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2135918
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2135877
    invoke-direct {p0, p1, p2}, LX/EXy;->d(LX/EWd;LX/EYZ;)LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2135878
    invoke-direct {p0}, LX/EXy;->w()LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2135857
    invoke-direct {p0, p1, p2}, LX/EXy;->d(LX/EWd;LX/EYZ;)LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2135851
    invoke-direct {p0}, LX/EXy;->w()LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2135852
    invoke-direct {p0, p1}, LX/EXy;->d(LX/EWY;)LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2135853
    invoke-direct {p0}, LX/EXy;->w()LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2135854
    sget-object v0, LX/EYC;->L:LX/EYn;

    const-class v1, LX/EY3;

    const-class v2, LX/EXy;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2135855
    sget-object v0, LX/EYC;->K:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2135856
    invoke-direct {p0}, LX/EXy;->w()LX/EXy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2135858
    invoke-virtual {p0}, LX/EXy;->l()LX/EY3;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2135859
    invoke-direct {p0}, LX/EXy;->y()LX/EY3;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2135860
    invoke-virtual {p0}, LX/EXy;->l()LX/EY3;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2135861
    invoke-direct {p0}, LX/EXy;->y()LX/EY3;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EY3;
    .locals 3

    .prologue
    .line 2135862
    new-instance v0, LX/EY3;

    invoke-direct {v0, p0}, LX/EY3;-><init>(LX/EWj;)V

    .line 2135863
    iget-object v1, p0, LX/EXy;->c:LX/EZ2;

    if-nez v1, :cond_1

    .line 2135864
    iget v1, p0, LX/EXy;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2135865
    iget-object v1, p0, LX/EXy;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXy;->b:Ljava/util/List;

    .line 2135866
    iget v1, p0, LX/EXy;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/EXy;->a:I

    .line 2135867
    :cond_0
    iget-object v1, p0, LX/EXy;->b:Ljava/util/List;

    .line 2135868
    iput-object v1, v0, LX/EY3;->location_:Ljava/util/List;

    .line 2135869
    :goto_0
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2135870
    return-object v0

    .line 2135871
    :cond_1
    iget-object v1, p0, LX/EXy;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2135872
    iput-object v1, v0, LX/EY3;->location_:Ljava/util/List;

    .line 2135873
    goto :goto_0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2135874
    sget-object v0, LX/EY3;->c:LX/EY3;

    move-object v0, v0

    .line 2135875
    return-object v0
.end method
