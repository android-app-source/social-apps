.class public LX/CyH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:LX/4FP;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1952588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952589
    iput-object p1, p0, LX/CyH;->a:Ljava/lang/String;

    .line 1952590
    iput-object p2, p0, LX/CyH;->b:Ljava/lang/String;

    .line 1952591
    new-instance v0, LX/4FP;

    invoke-direct {v0}, LX/4FP;-><init>()V

    invoke-virtual {v0, p1}, LX/4FP;->a(Ljava/lang/String;)LX/4FP;

    move-result-object v0

    const-string v1, "add"

    invoke-virtual {v0, v1}, LX/4FP;->c(Ljava/lang/String;)LX/4FP;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/4FP;->d(Ljava/lang/String;)LX/4FP;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4FP;->b(Ljava/lang/String;)LX/4FP;

    move-result-object v0

    iput-object v0, p0, LX/CyH;->c:LX/4FP;

    .line 1952592
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1952564
    iget-object v0, p0, LX/CyH;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/4FP;
    .locals 1

    .prologue
    .line 1952587
    iget-object v0, p0, LX/CyH;->c:LX/4FP;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1952572
    if-ne p0, p1, :cond_1

    .line 1952573
    :cond_0
    :goto_0
    return v0

    .line 1952574
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1952575
    goto :goto_0

    .line 1952576
    :cond_3
    check-cast p1, LX/CyH;

    .line 1952577
    iget-object v2, p0, LX/CyH;->c:LX/4FP;

    move-object v2, v2

    .line 1952578
    invoke-virtual {v2}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v2

    .line 1952579
    iget-object v3, p1, LX/CyH;->c:LX/4FP;

    move-object v3, v3

    .line 1952580
    invoke-virtual {v3}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1952581
    iget-object v2, p0, LX/CyH;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1952582
    iget-object v3, p1, LX/CyH;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1952583
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1952584
    iget-object v2, p0, LX/CyH;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1952585
    iget-object v3, p1, LX/CyH;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1952586
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1952565
    iget-object v0, p0, LX/CyH;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1952566
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1952567
    iget-object v1, p0, LX/CyH;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1952568
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 1952569
    iget-object v1, p0, LX/CyH;->c:LX/4FP;

    move-object v1, v1

    .line 1952570
    invoke-virtual {v1}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 1952571
    return v0
.end method
