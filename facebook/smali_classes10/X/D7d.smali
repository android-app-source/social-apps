.class public LX/D7d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/D7d;


# instance fields
.field public final a:Landroid/app/NotificationManager;

.field public final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/NotificationManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1967396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1967397
    iput-object p1, p0, LX/D7d;->b:Landroid/content/Context;

    .line 1967398
    iput-object p2, p0, LX/D7d;->a:Landroid/app/NotificationManager;

    .line 1967399
    return-void
.end method

.method public static a(LX/0QB;)LX/D7d;
    .locals 5

    .prologue
    .line 1967400
    sget-object v0, LX/D7d;->c:LX/D7d;

    if-nez v0, :cond_1

    .line 1967401
    const-class v1, LX/D7d;

    monitor-enter v1

    .line 1967402
    :try_start_0
    sget-object v0, LX/D7d;->c:LX/D7d;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1967403
    if-eqz v2, :cond_0

    .line 1967404
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1967405
    new-instance p0, LX/D7d;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    invoke-direct {p0, v3, v4}, LX/D7d;-><init>(Landroid/content/Context;Landroid/app/NotificationManager;)V

    .line 1967406
    move-object v0, p0

    .line 1967407
    sput-object v0, LX/D7d;->c:LX/D7d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1967408
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1967409
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1967410
    :cond_1
    sget-object v0, LX/D7d;->c:LX/D7d;

    return-object v0

    .line 1967411
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1967412
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
