.class public final LX/EKG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3ag;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2106415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/CvY;LX/CxV;Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/CxV;",
            ":",
            "LX/CxG;",
            ">(",
            "LX/CvY;",
            "TE;",
            "Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2106416
    check-cast p3, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    .line 2106417
    invoke-interface {p2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v8

    move-object v0, p2

    check-cast v0, LX/CxG;

    invoke-interface {v0, p3}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v9

    invoke-interface {p2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {p3}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p3}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v4

    check-cast p2, LX/CxG;

    invoke-interface {p2, p3}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/BaseFeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;IILcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {p1, v8, v9, p3, v0}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2106418
    return-void
.end method
