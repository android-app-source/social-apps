.class public final LX/DSz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupUnblockMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/DT7;


# direct methods
.method public constructor <init>(LX/DT7;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2000116
    iput-object p1, p0, LX/DSz;->d:LX/DT7;

    iput-object p2, p0, LX/DSz;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DSz;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DSz;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2000117
    iget-object v0, p0, LX/DSz;->d:LX/DT7;

    iget-object v0, v0, LX/DT7;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DSz;->d:LX/DT7;

    iget-object v2, v2, LX/DT7;->c:Landroid/content/res/Resources;

    const v3, 0x7f082fa3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2000118
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2000119
    const/4 v6, 0x0

    .line 2000120
    iget-object v0, p0, LX/DSz;->d:LX/DT7;

    iget-object v0, v0, LX/DT7;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DSz;->d:LX/DT7;

    iget-object v2, v2, LX/DT7;->c:Landroid/content/res/Resources;

    const v3, 0x7f082fa0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/DSz;->a:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2000121
    iget-object v0, p0, LX/DSz;->d:LX/DT7;

    iget-object v0, v0, LX/DT7;->f:LX/DTh;

    new-instance v1, LX/DTj;

    iget-object v2, p0, LX/DSz;->b:Ljava/lang/String;

    iget-object v3, p0, LX/DSz;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v6}, LX/DTj;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2000122
    return-void
.end method
