.class public interface abstract LX/Ctg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CrJ;
.implements LX/Cre;
.implements LX/CnP;
.implements LX/Cq9;
.implements LX/Ctf;
.implements LX/1OV;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "LX/Ct1;",
        ">",
        "Ljava/lang/Object;",
        "LX/CrJ;",
        "LX/Cre;",
        "LX/CnP;",
        "LX/Cq9;",
        "LX/Ctf;",
        "LX/1OV;"
    }
.end annotation


# virtual methods
.method public abstract a(Landroid/view/View;)Landroid/graphics/Rect;
.end method

.method public abstract a()Landroid/view/ViewGroup;
.end method

.method public abstract a(LX/Cqw;)V
.end method

.method public abstract a(Landroid/view/View;II)V
.end method

.method public abstract a(Landroid/view/View;Landroid/graphics/Rect;)V
.end method

.method public abstract b()V
.end method

.method public abstract b(LX/Cqw;)Z
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/richdocument/view/widget/media/MediaFrameBody",
            "<TV;>;"
        }
    .end annotation
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getCurrentLayout()LX/CrS;
.end method

.method public abstract getMediaView()LX/Ct1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method

.method public abstract getOverlayView()Landroid/view/View;
.end method

.method public abstract getTransitionStrategy()LX/Cqj;
.end method

.method public abstract setBody(Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/view/widget/media/MediaFrameBody",
            "<TV;>;)V"
        }
    .end annotation
.end method

.method public abstract setOverlayBackgroundColor(I)V
.end method

.method public abstract setTransitionStrategy(LX/Cqj;)V
.end method
