.class public final LX/D9O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;


# direct methods
.method public constructor <init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V
    .locals 0

    .prologue
    .line 1970062
    iput-object p1, p0, LX/D9O;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1970063
    iget-object v0, p0, LX/D9O;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    iget-object v0, v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->u:LX/D9T;

    if-eqz v0, :cond_0

    .line 1970064
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1970065
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 1970066
    iget-object v0, p0, LX/D9O;->a:Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    iget-object v0, v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->c:LX/03V;

    const-string v1, "ChatHeadTextBubbleView"

    const-string v2, "handleTimeout onFailure"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1970067
    :cond_0
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1970068
    invoke-direct {p0}, LX/D9O;->a()V

    return-void
.end method
