.class public final LX/DNN;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/44w",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedHomeStories;",
        "LX/0ta;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DNR;


# direct methods
.method public constructor <init>(LX/DNR;)V
    .locals 0

    .prologue
    .line 1991457
    iput-object p1, p0, LX/DNN;->a:LX/DNR;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1991458
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    const/4 v1, 0x0

    .line 1991459
    iput-boolean v1, v0, LX/DNR;->z:Z

    .line 1991460
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    invoke-static {v0}, LX/DNR;->p(LX/DNR;)V

    .line 1991461
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->u:LX/DNQ;

    invoke-interface {v0}, LX/DNQ;->b()V

    .line 1991462
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 1991463
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    invoke-static {v0, p1}, LX/DNR;->a$redex0(LX/DNR;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1991464
    :cond_0
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    iget-object v1, p0, LX/DNN;->a:LX/DNR;

    iget-object v1, v1, LX/DNR;->m:LX/DKg;

    invoke-static {v0, v1}, LX/DNR;->a$redex0(LX/DNR;LX/DKg;)V

    .line 1991465
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->b:LX/2lS;

    if-eqz v0, :cond_1

    .line 1991466
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->b:LX/2lS;

    .line 1991467
    sget-object v1, LX/2lT;->FRESH_STORIES:LX/2lT;

    invoke-static {v0, v1}, LX/2lS;->c(LX/2lS;LX/2lT;)V

    .line 1991468
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1991469
    check-cast p1, LX/44w;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1991470
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    invoke-static {v0}, LX/DNR;->q(LX/DNR;)V

    .line 1991471
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    .line 1991472
    iput-boolean v4, v0, LX/DNR;->B:Z

    .line 1991473
    iget-object v0, p1, LX/44w;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    .line 1991474
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1991475
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1991476
    iget-object v2, p0, LX/DNN;->a:LX/DNR;

    iget-object v2, v2, LX/DNR;->b:LX/2lS;

    if-eqz v2, :cond_1

    .line 1991477
    iget-object v2, p0, LX/DNN;->a:LX/DNR;

    iget-object v2, v2, LX/DNR;->b:LX/2lS;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v7, 0x1

    .line 1991478
    iget-boolean v6, v2, LX/2lS;->g:Z

    if-nez v6, :cond_0

    if-lez v3, :cond_7

    :cond_0
    move v6, v7

    :goto_0
    iput-boolean v6, v2, LX/2lS;->g:Z

    .line 1991479
    iput-boolean v7, v2, LX/2lS;->f:Z

    .line 1991480
    new-instance v6, LX/0P2;

    invoke-direct {v6}, LX/0P2;-><init>()V

    const-string v7, "stories_count"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v6

    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v6

    .line 1991481
    sget-object v7, LX/2lT;->FRESH_STORIES:LX/2lT;

    invoke-static {v2, v7, v6}, LX/2lS;->a(LX/2lS;LX/2lT;LX/0P1;)V

    .line 1991482
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p1, LX/44w;->b:Ljava/lang/Object;

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v2, v3, :cond_2

    .line 1991483
    iget-object v2, p0, LX/DNN;->a:LX/DNR;

    iget-object v2, v2, LX/DNR;->c:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->d()V

    .line 1991484
    :cond_2
    iget-object v2, p0, LX/DNN;->a:LX/DNR;

    invoke-virtual {v2}, LX/DNR;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1991485
    invoke-static {v0}, LX/DNR;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1991486
    :cond_3
    iget-object v2, p0, LX/DNN;->a:LX/DNR;

    invoke-static {v2, p1, v0}, LX/DNR;->a$redex0(LX/DNR;LX/44w;Ljava/util/List;)V

    .line 1991487
    iget-object v2, p0, LX/DNN;->a:LX/DNR;

    iget-object v2, v2, LX/DNR;->c:LX/0fz;

    sget-object v3, LX/0qw;->FULL:LX/0qw;

    invoke-virtual {v2, v0, v1, v3}, LX/0fz;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0qw;)V

    .line 1991488
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    if-nez v0, :cond_6

    .line 1991489
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    .line 1991490
    iput-boolean v4, v0, LX/DNR;->v:Z

    .line 1991491
    :cond_4
    :goto_1
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    iget-boolean v0, v0, LX/DNR;->z:Z

    if-eqz v0, :cond_5

    .line 1991492
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->u:LX/DNQ;

    invoke-interface {v0, v4}, LX/DNQ;->b(Z)V

    .line 1991493
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    .line 1991494
    iput-boolean v5, v0, LX/DNR;->z:Z

    .line 1991495
    :cond_5
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->u:LX/DNQ;

    invoke-interface {v0}, LX/DNQ;->a()V

    .line 1991496
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    invoke-static {v0}, LX/DNR;->p(LX/DNR;)V

    .line 1991497
    return-void

    .line 1991498
    :cond_6
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1991499
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    iget-object v0, v0, LX/DNR;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->r()I

    .line 1991500
    iget-object v0, p0, LX/DNN;->a:LX/DNR;

    .line 1991501
    iput-boolean v5, v0, LX/DNR;->v:Z

    .line 1991502
    goto :goto_1

    .line 1991503
    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_0
.end method
