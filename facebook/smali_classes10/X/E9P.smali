.class public LX/E9P;
.super LX/E9M;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/E9M",
        "<",
        "Landroid/util/Pair",
        "<",
        "LX/5tj;",
        "Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsInterfaces$FetchSingleReviewQuery$RepresentedProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/5tj;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsInterfaces$FetchSingleReviewQuery$RepresentedProfile;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/9Ea;

.field public d:LX/E9T;


# direct methods
.method public constructor <init>(LX/9Eb;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2084364
    invoke-direct {p0}, LX/E9M;-><init>()V

    .line 2084365
    const/4 v3, 0x0

    .line 2084366
    new-instance v2, LX/E9O;

    invoke-direct {v2, p0}, LX/E9O;-><init>(LX/E9P;)V

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v1 .. v7}, LX/9Eb;->a(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;)LX/9Ea;

    move-result-object v1

    move-object v0, v1

    .line 2084367
    iput-object v0, p0, LX/E9P;->c:LX/9Ea;

    .line 2084368
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/E9P;->a:LX/0am;

    .line 2084369
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/E9P;->b:LX/0am;

    .line 2084370
    return-void
.end method


# virtual methods
.method public final a(I)LX/E9W;
    .locals 1

    .prologue
    .line 2084359
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2084360
    iget-object v0, p0, LX/E9P;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2084361
    iget-object v0, p0, LX/E9P;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2084362
    sget-object v0, LX/E9W;->REVIEW_WITH_NO_ATTACHMENT:LX/E9W;

    return-object v0

    .line 2084363
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2084358
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/5tj;)V
    .locals 2
    .param p1    # LX/5tj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2084371
    iget-object v0, p0, LX/E9P;->c:LX/9Ea;

    invoke-static {p1}, LX/BNJ;->d(LX/5tj;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9Ea;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2084372
    if-nez p1, :cond_0

    .line 2084373
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/E9P;->a:LX/0am;

    .line 2084374
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/E9P;->b:LX/0am;

    .line 2084375
    :goto_0
    return-void

    .line 2084376
    :cond_0
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/E9P;->a:LX/0am;

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2084353
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2084354
    iget-object v0, p0, LX/E9P;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2084355
    iget-object v0, p0, LX/E9P;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2084356
    iget-object v0, p0, LX/E9P;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LX/E9P;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 2084357
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 2

    .prologue
    .line 2084348
    iget-object v0, p0, LX/E9P;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E9P;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/E9P;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LX/E9P;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2084349
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2084350
    goto :goto_0
.end method

.method public final i()LX/E9W;
    .locals 1

    .prologue
    .line 2084352
    sget-object v0, LX/E9W;->NO_HEADER:LX/E9W;

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 2084351
    iget-object v0, p0, LX/E9P;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
