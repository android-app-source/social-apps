.class public LX/ESr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/ETd;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/2nv;

.field public final c:LX/0Sh;

.field public final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0gM",
            "<",
            "Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$CreatorLiveStatusChangedSubModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7R6;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/videohome/creatorchannel/CreatorStatusUpdateListener;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2123696
    const-class v0, LX/ESr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ESr;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2nv;LX/0Ot;LX/0Sh;LX/03V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nv;",
            "LX/0Ot",
            "<",
            "LX/7R6;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2123677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2123678
    iput-object p3, p0, LX/ESr;->c:LX/0Sh;

    .line 2123679
    iput-object p4, p0, LX/ESr;->h:LX/03V;

    .line 2123680
    iput-object p1, p0, LX/ESr;->b:LX/2nv;

    .line 2123681
    iput-object p2, p0, LX/ESr;->e:LX/0Ot;

    .line 2123682
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/ESr;->d:Ljava/util/HashMap;

    .line 2123683
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/ESr;->g:Ljava/util/HashMap;

    .line 2123684
    return-void
.end method

.method public static a(LX/0QB;)LX/ESr;
    .locals 7

    .prologue
    .line 2123685
    const-class v1, LX/ESr;

    monitor-enter v1

    .line 2123686
    :try_start_0
    sget-object v0, LX/ESr;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2123687
    sput-object v2, LX/ESr;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2123688
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2123689
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2123690
    new-instance v6, LX/ESr;

    invoke-static {v0}, LX/2nv;->b(LX/0QB;)LX/2nv;

    move-result-object v3

    check-cast v3, LX/2nv;

    const/16 v4, 0x383e

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {v6, v3, p0, v4, v5}, LX/ESr;-><init>(LX/2nv;LX/0Ot;LX/0Sh;LX/03V;)V

    .line 2123691
    move-object v0, v6

    .line 2123692
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2123693
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ESr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2123694
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2123695
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2123670
    iget-object v0, p0, LX/ESr;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 2123671
    if-nez v0, :cond_0

    .line 2123672
    new-instance v0, Lcom/facebook/video/videohome/creatorchannel/VideoHomeCreatorChannelStatusManager$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/video/videohome/creatorchannel/VideoHomeCreatorChannelStatusManager$1;-><init>(LX/ESr;Ljava/lang/String;)V

    .line 2123673
    iget-object v1, p0, LX/ESr;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2123674
    iget-object v1, p0, LX/ESr;->c:LX/0Sh;

    invoke-virtual {v1, v0}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2123675
    :cond_0
    return-void
.end method

.method public static a(LX/ESz;)Z
    .locals 1

    .prologue
    .line 2123676
    sget-object v0, LX/ESz;->LIVE:LX/ESz;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/ESr;LX/ESz;Lcom/facebook/video/videohome/data/VideoHomeItem;)V
    .locals 3

    .prologue
    .line 2123664
    iget-object v0, p0, LX/ESr;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 2123665
    iget-object v0, p0, LX/ESr;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    .line 2123666
    if-eqz v0, :cond_0

    .line 2123667
    iget-object v1, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ay:LX/ETy;

    invoke-interface {v1, p2, p1}, LX/ETd;->a(Ljava/lang/Object;LX/ESz;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2123668
    sget-object v1, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->X:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment$13;

    invoke-direct {v2, v0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment$13;-><init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V

    const p0, 0x397cfb7a

    invoke-static {v1, v2, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2123669
    :cond_0
    return-void
.end method

.method public static f(Lcom/facebook/video/videohome/data/VideoHomeItem;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2123662
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2123663
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 6

    .prologue
    .line 2123623
    invoke-static {p1}, LX/ESx;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2123624
    const/4 v0, 0x0

    .line 2123625
    :goto_0
    return v0

    .line 2123626
    :cond_0
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2123627
    invoke-interface {v0}, LX/9uc;->cN()Ljava/lang/String;

    move-result-object v0

    .line 2123628
    invoke-static {p1}, LX/ESr;->f(Lcom/facebook/video/videohome/data/VideoHomeItem;)Ljava/lang/String;

    move-result-object v1

    .line 2123629
    invoke-static {v0}, LX/ESz;->fromString(Ljava/lang/String;)LX/ESz;

    move-result-object v0

    .line 2123630
    invoke-static {v0}, LX/ESr;->a(LX/ESz;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2123631
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2123632
    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2123633
    invoke-static {p1}, LX/ESx;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2123634
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2123635
    :cond_2
    invoke-direct {p0, v1}, LX/ESr;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 2123636
    :cond_3
    invoke-static {p1}, LX/ESr;->f(Lcom/facebook/video/videohome/data/VideoHomeItem;)Ljava/lang/String;

    move-result-object v1

    .line 2123637
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2123638
    invoke-interface {v0}, LX/9uc;->cN()Ljava/lang/String;

    move-result-object v0

    .line 2123639
    invoke-static {v0}, LX/ESz;->fromString(Ljava/lang/String;)LX/ESz;

    move-result-object v0

    .line 2123640
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/ESr;->a(LX/ESz;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2123641
    iget-object v0, p0, LX/ESr;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2123642
    iget-object v0, p0, LX/ESr;->b:LX/2nv;

    .line 2123643
    new-instance v2, LX/2rK;

    invoke-direct {v2, p0, p1}, LX/2rK;-><init>(LX/ESr;Lcom/facebook/video/videohome/data/VideoHomeItem;)V

    move-object v2, v2

    .line 2123644
    new-instance v3, LX/4Ds;

    invoke-direct {v3}, LX/4Ds;-><init>()V

    .line 2123645
    const-string v4, "creator_id"

    invoke-virtual {v3, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2123646
    move-object v3, v3

    .line 2123647
    new-instance v4, LX/7RB;

    invoke-direct {v4}, LX/7RB;-><init>()V

    move-object v4, v4

    .line 2123648
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2123649
    move-object v4, v4

    .line 2123650
    const/4 v3, 0x0

    .line 2123651
    :try_start_0
    iget-object v5, v0, LX/2nv;->b:LX/0gX;

    invoke-virtual {v5, v4, v2}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2123652
    :goto_2
    move-object v0, v3

    .line 2123653
    iget-object v2, p0, LX/ESr;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2123654
    :cond_4
    iget-object v0, p0, LX/ESr;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2123655
    iget-object v2, p0, LX/ESr;->c:LX/0Sh;

    iget-object v0, p0, LX/ESr;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v2, v0}, LX/0Sh;->c(Ljava/lang/Runnable;)V

    .line 2123656
    iget-object v0, p0, LX/ESr;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2123657
    :catch_0
    move-exception v4

    .line 2123658
    sget-object v5, LX/2nv;->a:Ljava/lang/String;

    const-string p1, "Video Home creator live status update  subscription failed. %s"

    invoke-static {v5, p1, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2123659
    iget-object v0, p0, LX/ESr;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2123660
    invoke-direct {p0, v0}, LX/ESr;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2123661
    :cond_0
    return-void
.end method
