.class public final LX/Ebw;
.super LX/EWj;
.source ""

# interfaces
.implements LX/Ebv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/Ebw;",
        ">;",
        "LX/Ebv;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:LX/Ecf;

.field public c:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/Ecf;",
            "LX/EcK;",
            "LX/EcJ;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ecf;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/Ecf;",
            "LX/EcK;",
            "LX/EcJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2144908
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2144909
    sget-object v0, LX/Ecf;->c:LX/Ecf;

    move-object v0, v0

    .line 2144910
    iput-object v0, p0, LX/Ebw;->b:LX/Ecf;

    .line 2144911
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ebw;->d:Ljava/util/List;

    .line 2144912
    invoke-direct {p0}, LX/Ebw;->n()V

    .line 2144913
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2144914
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2144915
    sget-object v0, LX/Ecf;->c:LX/Ecf;

    move-object v0, v0

    .line 2144916
    iput-object v0, p0, LX/Ebw;->b:LX/Ecf;

    .line 2144917
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ebw;->d:Ljava/util/List;

    .line 2144918
    invoke-direct {p0}, LX/Ebw;->n()V

    .line 2144919
    return-void
.end method

.method public static A(LX/Ebw;)V
    .locals 2

    .prologue
    .line 2144920
    iget v0, p0, LX/Ebw;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 2144921
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/Ebw;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/Ebw;->d:Ljava/util/List;

    .line 2144922
    iget v0, p0, LX/Ebw;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/Ebw;->a:I

    .line 2144923
    :cond_0
    return-void
.end method

.method private B()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/Ecf;",
            "LX/EcK;",
            "LX/EcJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2144924
    iget-object v0, p0, LX/Ebw;->e:LX/EZ2;

    if-nez v0, :cond_0

    .line 2144925
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/Ebw;->d:Ljava/util/List;

    iget v0, p0, LX/Ebw;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2144926
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2144927
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/Ebw;->e:LX/EZ2;

    .line 2144928
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ebw;->d:Ljava/util/List;

    .line 2144929
    :cond_0
    iget-object v0, p0, LX/Ebw;->e:LX/EZ2;

    return-object v0

    .line 2144930
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/Ebw;
    .locals 1

    .prologue
    .line 2144931
    instance-of v0, p1, LX/Ebx;

    if-eqz v0, :cond_0

    .line 2144932
    check-cast p1, LX/Ebx;

    invoke-virtual {p0, p1}, LX/Ebw;->a(LX/Ebx;)LX/Ebw;

    move-result-object p0

    .line 2144933
    :goto_0
    return-object p0

    .line 2144934
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/Ebw;
    .locals 4

    .prologue
    .line 2144935
    const/4 v2, 0x0

    .line 2144936
    :try_start_0
    sget-object v0, LX/Ebx;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ebx;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2144937
    if-eqz v0, :cond_0

    .line 2144938
    invoke-virtual {p0, v0}, LX/Ebw;->a(LX/Ebx;)LX/Ebw;

    .line 2144939
    :cond_0
    return-object p0

    .line 2144940
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2144941
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2144942
    check-cast v0, LX/Ebx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2144943
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2144944
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2144945
    invoke-virtual {p0, v1}, LX/Ebw;->a(LX/Ebx;)LX/Ebw;

    :cond_1
    throw v0

    .line 2144946
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private n()V
    .locals 4

    .prologue
    .line 2144947
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_1

    .line 2144948
    iget-object v0, p0, LX/Ebw;->c:LX/EZ7;

    if-nez v0, :cond_0

    .line 2144949
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/Ebw;->b:LX/Ecf;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2144950
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2144951
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/Ebw;->c:LX/EZ7;

    .line 2144952
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ebw;->b:LX/Ecf;

    .line 2144953
    :cond_0
    invoke-direct {p0}, LX/Ebw;->B()LX/EZ2;

    .line 2144954
    :cond_1
    return-void
.end method

.method public static u()LX/Ebw;
    .locals 1

    .prologue
    .line 2144955
    new-instance v0, LX/Ebw;

    invoke-direct {v0}, LX/Ebw;-><init>()V

    return-object v0
.end method

.method private w()LX/Ebw;
    .locals 2

    .prologue
    .line 2144956
    invoke-static {}, LX/Ebw;->u()LX/Ebw;

    move-result-object v0

    invoke-direct {p0}, LX/Ebw;->y()LX/Ebx;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ebw;->a(LX/Ebx;)LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/Ebx;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2144957
    new-instance v2, LX/Ebx;

    invoke-direct {v2, p0}, LX/Ebx;-><init>(LX/EWj;)V

    .line 2144958
    iget v3, p0, LX/Ebw;->a:I

    .line 2144959
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    move v1, v0

    .line 2144960
    :cond_0
    iget-object v0, p0, LX/Ebw;->c:LX/EZ7;

    if-nez v0, :cond_2

    .line 2144961
    iget-object v0, p0, LX/Ebw;->b:LX/Ecf;

    .line 2144962
    iput-object v0, v2, LX/Ebx;->currentSession_:LX/Ecf;

    .line 2144963
    :goto_0
    iget-object v0, p0, LX/Ebw;->e:LX/EZ2;

    if-nez v0, :cond_3

    .line 2144964
    iget v0, p0, LX/Ebw;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    .line 2144965
    iget-object v0, p0, LX/Ebw;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ebw;->d:Ljava/util/List;

    .line 2144966
    iget v0, p0, LX/Ebw;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, LX/Ebw;->a:I

    .line 2144967
    :cond_1
    iget-object v0, p0, LX/Ebw;->d:Ljava/util/List;

    .line 2144968
    iput-object v0, v2, LX/Ebx;->previousSessions_:Ljava/util/List;

    .line 2144969
    :goto_1
    iput v1, v2, LX/Ebx;->bitField0_:I

    .line 2144970
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2144971
    return-object v2

    .line 2144972
    :cond_2
    iget-object v0, p0, LX/Ebw;->c:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/Ecf;

    .line 2144973
    iput-object v0, v2, LX/Ebx;->currentSession_:LX/Ecf;

    .line 2144974
    goto :goto_0

    .line 2144975
    :cond_3
    iget-object v0, p0, LX/Ebw;->e:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v0

    .line 2144976
    iput-object v0, v2, LX/Ebx;->previousSessions_:Ljava/util/List;

    .line 2144977
    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2144978
    invoke-direct {p0, p1}, LX/Ebw;->d(LX/EWY;)LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2144979
    invoke-direct {p0, p1, p2}, LX/Ebw;->d(LX/EWd;LX/EYZ;)LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Ebx;)LX/Ebw;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2144980
    sget-object v1, LX/Ebx;->c:LX/Ebx;

    move-object v1, v1

    .line 2144981
    if-ne p1, v1, :cond_0

    .line 2144982
    :goto_0
    return-object p0

    .line 2144983
    :cond_0
    const/4 v1, 0x1

    .line 2144984
    iget v2, p1, LX/Ebx;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_7

    :goto_1
    move v1, v1

    .line 2144985
    if-eqz v1, :cond_1

    .line 2144986
    iget-object v1, p1, LX/Ebx;->currentSession_:LX/Ecf;

    move-object v1, v1

    .line 2144987
    iget-object v2, p0, LX/Ebw;->c:LX/EZ7;

    if-nez v2, :cond_9

    .line 2144988
    iget v2, p0, LX/Ebw;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    iget-object v2, p0, LX/Ebw;->b:LX/Ecf;

    .line 2144989
    sget-object v3, LX/Ecf;->c:LX/Ecf;

    move-object v3, v3

    .line 2144990
    if-eq v2, v3, :cond_8

    .line 2144991
    iget-object v2, p0, LX/Ebw;->b:LX/Ecf;

    invoke-static {v2}, LX/Ecf;->a(LX/Ecf;)LX/EcK;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/EcK;->a(LX/Ecf;)LX/EcK;

    move-result-object v2

    invoke-virtual {v2}, LX/EcK;->m()LX/Ecf;

    move-result-object v2

    iput-object v2, p0, LX/Ebw;->b:LX/Ecf;

    .line 2144992
    :goto_2
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2144993
    :goto_3
    iget v2, p0, LX/Ebw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/Ebw;->a:I

    .line 2144994
    :cond_1
    iget-object v1, p0, LX/Ebw;->e:LX/EZ2;

    if-nez v1, :cond_4

    .line 2144995
    iget-object v0, p1, LX/Ebx;->previousSessions_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2144996
    iget-object v0, p0, LX/Ebw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2144997
    iget-object v0, p1, LX/Ebx;->previousSessions_:Ljava/util/List;

    iput-object v0, p0, LX/Ebw;->d:Ljava/util/List;

    .line 2144998
    iget v0, p0, LX/Ebw;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, LX/Ebw;->a:I

    .line 2144999
    :goto_4
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145000
    :cond_2
    :goto_5
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    .line 2145001
    :cond_3
    invoke-static {p0}, LX/Ebw;->A(LX/Ebw;)V

    .line 2145002
    iget-object v0, p0, LX/Ebw;->d:Ljava/util/List;

    iget-object v1, p1, LX/Ebx;->previousSessions_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 2145003
    :cond_4
    iget-object v1, p1, LX/Ebx;->previousSessions_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2145004
    iget-object v1, p0, LX/Ebw;->e:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2145005
    iget-object v1, p0, LX/Ebw;->e:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2145006
    iput-object v0, p0, LX/Ebw;->e:LX/EZ2;

    .line 2145007
    iget-object v1, p1, LX/Ebx;->previousSessions_:Ljava/util/List;

    iput-object v1, p0, LX/Ebw;->d:Ljava/util/List;

    .line 2145008
    iget v1, p0, LX/Ebw;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, LX/Ebw;->a:I

    .line 2145009
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_5

    invoke-direct {p0}, LX/Ebw;->B()LX/EZ2;

    move-result-object v0

    :cond_5
    iput-object v0, p0, LX/Ebw;->e:LX/EZ2;

    goto :goto_5

    .line 2145010
    :cond_6
    iget-object v0, p0, LX/Ebw;->e:LX/EZ2;

    iget-object v1, p1, LX/Ebx;->previousSessions_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_5

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 2145011
    :cond_8
    iput-object v1, p0, LX/Ebw;->b:LX/Ecf;

    goto :goto_2

    .line 2145012
    :cond_9
    iget-object v2, p0, LX/Ebw;->c:LX/EZ7;

    invoke-virtual {v2, v1}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto :goto_3
.end method

.method public final a(LX/Ecf;)LX/Ebw;
    .locals 1

    .prologue
    .line 2145013
    iget-object v0, p0, LX/Ebw;->c:LX/EZ7;

    if-nez v0, :cond_1

    .line 2145014
    if-nez p1, :cond_0

    .line 2145015
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2145016
    :cond_0
    iput-object p1, p0, LX/Ebw;->b:LX/Ecf;

    .line 2145017
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145018
    :goto_0
    iget v0, p0, LX/Ebw;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ebw;->a:I

    .line 2145019
    return-object p0

    .line 2145020
    :cond_1
    iget-object v0, p0, LX/Ebw;->c:LX/EZ7;

    invoke-virtual {v0, p1}, LX/EZ7;->a(LX/EWp;)LX/EZ7;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2144907
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2144888
    invoke-direct {p0, p1, p2}, LX/Ebw;->d(LX/EWd;LX/EYZ;)LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2144906
    invoke-direct {p0}, LX/Ebw;->w()LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2144889
    invoke-direct {p0, p1, p2}, LX/Ebw;->d(LX/EWd;LX/EYZ;)LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2144890
    invoke-direct {p0}, LX/Ebw;->w()LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2144891
    invoke-direct {p0, p1}, LX/Ebw;->d(LX/EWY;)LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2144892
    invoke-direct {p0}, LX/Ebw;->w()LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2144893
    sget-object v0, LX/Eck;->n:LX/EYn;

    const-class v1, LX/Ebx;

    const-class v2, LX/Ebw;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2144894
    sget-object v0, LX/Eck;->m:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2144895
    invoke-direct {p0}, LX/Ebw;->w()LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2144896
    invoke-direct {p0}, LX/Ebw;->y()LX/Ebx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2144897
    invoke-virtual {p0}, LX/Ebw;->l()LX/Ebx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2144898
    invoke-direct {p0}, LX/Ebw;->y()LX/Ebx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2144899
    invoke-virtual {p0}, LX/Ebw;->l()LX/Ebx;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/Ebx;
    .locals 2

    .prologue
    .line 2144900
    invoke-direct {p0}, LX/Ebw;->y()LX/Ebx;

    move-result-object v0

    .line 2144901
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2144902
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2144903
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2144904
    sget-object v0, LX/Ebx;->c:LX/Ebx;

    move-object v0, v0

    .line 2144905
    return-object v0
.end method
