.class public final LX/DDn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1975997
    iput-object p1, p0, LX/DDn;->b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;

    iput-object p2, p0, LX/DDn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x30aba0c

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1975998
    iget-object v1, p0, LX/DDn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1975999
    if-nez v1, :cond_0

    .line 1976000
    const v1, -0x543b3589

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1976001
    :goto_0
    return-void

    .line 1976002
    :cond_0
    sget-object v2, LX/0ax;->ge:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1976003
    iget-object v2, p0, LX/DDn;->b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemSellerProfileComponentPartDefinition;->h:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1976004
    const v1, -0x779eb5f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
