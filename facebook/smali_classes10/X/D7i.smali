.class public LX/D7i;
.super LX/2my;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/17Y;


# direct methods
.method public constructor <init>(LX/17Y;LX/2n0;LX/2n3;LX/17V;LX/0Zb;LX/0hB;LX/2nF;LX/2nG;LX/2nH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1967460
    invoke-direct/range {p0 .. p9}, LX/2my;-><init>(LX/17Y;LX/2n0;LX/2n3;LX/17V;LX/0Zb;LX/0hB;LX/2nF;LX/2nG;LX/2nH;)V

    .line 1967461
    iput-object p1, p0, LX/D7i;->d:LX/17Y;

    .line 1967462
    return-void
.end method

.method public static b(LX/0QB;)LX/D7i;
    .locals 13

    .prologue
    .line 1967463
    const-class v1, LX/D7i;

    monitor-enter v1

    .line 1967464
    :try_start_0
    sget-object v0, LX/D7i;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1967465
    sput-object v2, LX/D7i;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1967466
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1967467
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1967468
    new-instance v3, LX/D7i;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-static {v0}, LX/2n0;->b(LX/0QB;)LX/2n0;

    move-result-object v5

    check-cast v5, LX/2n0;

    invoke-static {v0}, LX/2n3;->b(LX/0QB;)LX/2n3;

    move-result-object v6

    check-cast v6, LX/2n3;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v7

    check-cast v7, LX/17V;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v9

    check-cast v9, LX/0hB;

    invoke-static {v0}, LX/2nF;->a(LX/0QB;)LX/2nF;

    move-result-object v10

    check-cast v10, LX/2nF;

    invoke-static {v0}, LX/2nG;->a(LX/0QB;)LX/2nG;

    move-result-object v11

    check-cast v11, LX/2nG;

    invoke-static {v0}, LX/2nH;->b(LX/0QB;)LX/2nH;

    move-result-object v12

    check-cast v12, LX/2nH;

    invoke-direct/range {v3 .. v12}, LX/D7i;-><init>(LX/17Y;LX/2n0;LX/2n3;LX/17V;LX/0Zb;LX/0hB;LX/2nF;LX/2nG;LX/2nH;)V

    .line 1967469
    move-object v0, v3

    .line 1967470
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1967471
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D7i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1967472
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1967473
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1967474
    invoke-virtual {p0, p1, p2}, LX/D7i;->c(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/content/Intent;

    move-result-object v0

    .line 1967475
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/D8g;
    .locals 1

    .prologue
    .line 1967476
    sget-object v0, LX/D8g;->WATCH_AND_BROWSE_OFFER:LX/D8g;

    return-object v0
.end method

.method public final c(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1967477
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v1

    .line 1967478
    :goto_0
    return-object v0

    .line 1967479
    :cond_1
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1967480
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    const/4 v2, 0x0

    .line 1967481
    if-nez v0, :cond_5

    .line 1967482
    :cond_2
    :goto_1
    move-object v0, v2

    .line 1967483
    if-nez v0, :cond_3

    move-object v0, v1

    .line 1967484
    goto :goto_0

    .line 1967485
    :cond_3
    iget-object v1, p0, LX/D7i;->d:LX/17Y;

    invoke-interface {v1, p1, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1967486
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1967487
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->t(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1967488
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 1967489
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    .line 1967490
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1967491
    const-string v3, "offer_view_id"

    const-string p0, "offer_view_id"

    invoke-virtual {v2, p0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1967492
    const-string v3, "title"

    const-string p0, "title"

    invoke-virtual {v2, p0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1967493
    const-string v3, "offer_code"

    const-string p0, "offer_code"

    invoke-virtual {v2, p0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1967494
    const-string v3, "share_id"

    const-string p0, "share_id"

    invoke-virtual {v2, p0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1967495
    const-string v3, "claim_type"

    const-string p0, "claim_type"

    invoke-virtual {v2, p0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1967496
    move-object v0, v1

    .line 1967497
    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0

    .line 1967498
    :cond_5
    const v3, -0x1e53800c

    invoke-static {v0, v3}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 1967499
    if-eqz v3, :cond_2

    .line 1967500
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1967501
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->by()Lcom/facebook/graphql/model/GraphQLOffer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLOffer;->j()Ljava/lang/String;

    move-result-object v2

    .line 1967502
    :goto_2
    move-object v2, v2

    .line 1967503
    if-nez v2, :cond_2

    .line 1967504
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_2
.end method
