.class public LX/DgI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/DgH;

.field public final b:LX/6eE;

.field public final c:Lcom/facebook/android/maps/model/LatLng;

.field public final d:Lcom/facebook/messaging/location/sending/NearbyPlace;

.field public final e:Z

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/DgG;)V
    .locals 1

    .prologue
    .line 2029313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2029314
    iget-object v0, p1, LX/DgG;->a:LX/DgH;

    iput-object v0, p0, LX/DgI;->a:LX/DgH;

    .line 2029315
    iget-object v0, p1, LX/DgG;->b:LX/6eE;

    iput-object v0, p0, LX/DgI;->b:LX/6eE;

    .line 2029316
    iget-object v0, p1, LX/DgG;->c:Lcom/facebook/android/maps/model/LatLng;

    iput-object v0, p0, LX/DgI;->c:Lcom/facebook/android/maps/model/LatLng;

    .line 2029317
    iget-object v0, p1, LX/DgG;->d:Lcom/facebook/messaging/location/sending/NearbyPlace;

    iput-object v0, p0, LX/DgI;->d:Lcom/facebook/messaging/location/sending/NearbyPlace;

    .line 2029318
    iget-boolean v0, p1, LX/DgG;->e:Z

    iput-boolean v0, p0, LX/DgI;->e:Z

    .line 2029319
    iget-object v0, p1, LX/DgG;->f:Ljava/lang/String;

    iput-object v0, p0, LX/DgI;->f:Ljava/lang/String;

    .line 2029320
    return-void
.end method
