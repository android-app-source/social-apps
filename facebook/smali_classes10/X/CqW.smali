.class public LX/CqW;
.super LX/7yk;
.source ""


# instance fields
.field public f:LX/7yn;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1939927
    invoke-direct {p0, p1}, LX/7yk;-><init>(Landroid/content/Context;)V

    .line 1939928
    const/4 v0, 0x0

    iput-object v0, p0, LX/CqW;->f:LX/7yn;

    .line 1939929
    return-void
.end method

.method public static a(LX/7yn;)I
    .locals 2

    .prologue
    .line 1939930
    iget-object v0, p0, LX/7yn;->a:Landroid/view/View;

    move-object v0, v0

    .line 1939931
    sget v1, LX/CqS;->c:I

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    const/4 p0, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-static {v1, p0}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v0, v1

    .line 1939932
    return v0
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1939933
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1939934
    iput-object v1, p0, LX/CqW;->e:Landroid/view/ViewGroup;

    .line 1939935
    iput-object v1, p0, LX/CqW;->f:LX/7yn;

    .line 1939936
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;II)LX/7yn;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1939937
    if-nez p1, :cond_0

    .line 1939938
    :goto_0
    return-object v0

    .line 1939939
    :cond_0
    invoke-direct {p0}, LX/CqW;->c()V

    .line 1939940
    iput p2, p0, LX/CqW;->a:I

    .line 1939941
    iput p3, p0, LX/CqW;->b:I

    .line 1939942
    iget-object v1, p0, LX/7yk;->c:[I

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    .line 1939943
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/7yk;->a(Landroid/view/ViewGroup;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1939944
    invoke-direct {p0}, LX/CqW;->c()V

    goto :goto_0

    .line 1939945
    :cond_1
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1939946
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1939947
    if-ne v0, v2, :cond_4

    .line 1939948
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yn;

    .line 1939949
    :cond_2
    :goto_1
    iget-object v1, p0, LX/CqW;->f:LX/7yn;

    if-ne v0, v1, :cond_a

    .line 1939950
    :goto_2
    move-object v0, v4

    .line 1939951
    if-eqz v0, :cond_3

    .line 1939952
    iget-object v1, p0, LX/7yk;->e:Landroid/view/ViewGroup;

    .line 1939953
    iput-object v1, v0, LX/7yn;->b:Landroid/view/ViewGroup;

    .line 1939954
    :cond_3
    invoke-direct {p0}, LX/CqW;->c()V

    goto :goto_0

    .line 1939955
    :cond_4
    if-le v0, v2, :cond_d

    .line 1939956
    const v1, 0x7fffffff

    .line 1939957
    const/high16 v0, -0x80000000

    .line 1939958
    iget-object p1, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move p1, v1

    move v1, v0

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yn;

    .line 1939959
    invoke-static {v0}, LX/CqW;->a(LX/7yn;)I

    move-result p3

    .line 1939960
    invoke-static {v1, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1939961
    invoke-static {p1, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    move p1, v1

    move v1, v0

    .line 1939962
    goto :goto_3

    .line 1939963
    :cond_5
    if-ne p1, v1, :cond_6

    const/4 v1, -0x1

    :cond_6
    move v0, v1

    .line 1939964
    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    .line 1939965
    iget-object v1, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move p1, v1

    :goto_4
    if-ltz p1, :cond_8

    .line 1939966
    iget-object v1, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7yn;

    .line 1939967
    invoke-static {v1}, LX/CqW;->a(LX/7yn;)I

    move-result v1

    if-eq v1, v0, :cond_7

    .line 1939968
    iget-object v1, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1939969
    :cond_7
    add-int/lit8 v1, p1, -0x1

    move p1, v1

    goto :goto_4

    .line 1939970
    :cond_8
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_9

    .line 1939971
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yn;

    goto :goto_1

    .line 1939972
    :cond_9
    const v2, 0x7fffffff

    .line 1939973
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move-object v3, v4

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7yn;

    .line 1939974
    iget-object v1, p0, LX/CqW;->f:LX/7yn;

    if-eq v0, v1, :cond_2

    .line 1939975
    iget v1, p0, LX/7yk;->a:I

    iget p2, p0, LX/7yk;->b:I

    invoke-virtual {v0, v1, p2}, LX/7yn;->a(II)I

    move-result v1

    .line 1939976
    if-ge v1, v2, :cond_b

    move p3, v1

    move-object v1, v0

    move v0, p3

    :goto_6
    move v2, v0

    move-object v3, v1

    .line 1939977
    goto :goto_5

    :cond_a
    move-object v4, v0

    .line 1939978
    goto/16 :goto_2

    :cond_b
    move v0, v2

    move-object v1, v3

    goto :goto_6

    :cond_c
    move-object v0, v3

    goto/16 :goto_1

    :cond_d
    move-object v0, v4

    goto/16 :goto_1
.end method

.method public final a(Landroid/view/View;)Z
    .locals 7

    .prologue
    .line 1939979
    invoke-virtual {p0, p1}, LX/7yk;->b(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1939980
    iget v1, p0, LX/7yk;->a:I

    iget v2, p0, LX/7yk;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1939981
    new-instance v1, LX/7yn;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-direct {v1, p1, v2, v3}, LX/7yn;-><init>(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 1939982
    iget-object v0, p0, LX/CqW;->f:LX/7yn;

    if-nez v0, :cond_0

    .line 1939983
    iput-object v1, p0, LX/CqW;->f:LX/7yn;

    .line 1939984
    :cond_0
    iget-object v0, p0, LX/7yk;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1939985
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1939986
    :cond_2
    sget v1, LX/CqS;->a:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v2, v1

    .line 1939987
    sget v1, LX/CqS;->b:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v3}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v3, v1

    .line 1939988
    if-nez v2, :cond_3

    if-eqz v3, :cond_4

    .line 1939989
    :cond_3
    new-instance v1, Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v2

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v3

    iget v6, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v6

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v6

    invoke-direct {v1, v4, v5, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1939990
    :goto_1
    move-object v1, v1

    .line 1939991
    if-eqz v1, :cond_1

    iget v2, p0, LX/7yk;->a:I

    iget v3, p0, LX/7yk;->b:I

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1939992
    iget-object v2, p0, LX/7yk;->d:Ljava/util/List;

    new-instance v3, LX/7yn;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-direct {v3, p1, v4, v1}, LX/7yn;-><init>(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method
