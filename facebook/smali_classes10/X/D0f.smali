.class public LX/D0f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/D0m;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/D0i;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Vv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/D0m;LX/0Ot;LX/D0i;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/D0m;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/D0i;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Vv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1955751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1955752
    iput-object p1, p0, LX/D0f;->a:LX/D0m;

    .line 1955753
    iput-object p3, p0, LX/D0f;->c:LX/D0i;

    .line 1955754
    iput-object p2, p0, LX/D0f;->b:LX/0Ot;

    .line 1955755
    iput-object p4, p0, LX/D0f;->d:LX/0Ot;

    .line 1955756
    iput-object p5, p0, LX/D0f;->e:LX/0Ot;

    .line 1955757
    return-void
.end method

.method public static a(LX/0QB;)LX/D0f;
    .locals 9

    .prologue
    .line 1955760
    const-class v1, LX/D0f;

    monitor-enter v1

    .line 1955761
    :try_start_0
    sget-object v0, LX/D0f;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1955762
    sput-object v2, LX/D0f;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1955763
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1955764
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1955765
    new-instance v3, LX/D0f;

    invoke-static {v0}, LX/D0m;->a(LX/0QB;)LX/D0m;

    move-result-object v4

    check-cast v4, LX/D0m;

    const/16 v5, 0x455

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/D0i;->a(LX/0QB;)LX/D0i;

    move-result-object v6

    check-cast v6, LX/D0i;

    const/16 v7, 0xc49

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x11f1

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/D0f;-><init>(LX/D0m;LX/0Ot;LX/D0i;LX/0Ot;LX/0Ot;)V

    .line 1955766
    move-object v0, v3

    .line 1955767
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1955768
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D0f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1955769
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1955770
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/D0f;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1955758
    iget-object v0, p0, LX/D0f;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Vv;

    const-string v1, "dti_user_clicked_delete"

    invoke-virtual {v0, v1, p1}, LX/1Vv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1955759
    return-void
.end method
