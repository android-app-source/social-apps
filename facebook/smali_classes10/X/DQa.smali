.class public LX/DQa;
.super LX/DQZ;
.source ""


# instance fields
.field public final c:LX/0Zb;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field private e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0Zb;Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1994762
    invoke-direct {p0}, LX/DQZ;-><init>()V

    .line 1994763
    iput-object p1, p0, LX/DQa;->c:LX/0Zb;

    .line 1994764
    iput-object p3, p0, LX/DQa;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1994765
    iput-object p2, p0, LX/DQa;->e:Landroid/content/res/Resources;

    .line 1994766
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1994783
    iget-object v0, p0, LX/DQa;->e:Landroid/content/res/Resources;

    const v1, 0x7f0209c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1994782
    iget-object v0, p0, LX/DQa;->e:Landroid/content/res/Resources;

    const v1, 0x7f083068

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1994771
    const-string v0, "share_group"

    .line 1994772
    iget-object v1, p0, LX/DQa;->c:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1994773
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1994774
    const-string v2, "group_info"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1994775
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 1994776
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1994777
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1994778
    const-string v1, "android.intent.extra.TEXT"

    iget-object v2, p0, LX/DQZ;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1994779
    iget-object v1, p0, LX/DQZ;->b:Landroid/content/Context;

    const v2, 0x7f083069

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 1994780
    iget-object v1, p0, LX/DQa;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DQZ;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1994781
    return-void
.end method

.method public final d()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1994767
    iget-object v0, p0, LX/DQZ;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DQZ;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->l()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    move v0, v1

    .line 1994768
    :goto_0
    invoke-super {p0}, LX/DQZ;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 1994769
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1994770
    goto :goto_1
.end method
