.class public LX/Cw4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1949880
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949881
    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cw4;->a:Ljava/lang/String;

    .line 1949882
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v0, v0

    .line 1949883
    iput-object v0, p0, LX/Cw4;->b:Ljava/lang/String;

    .line 1949884
    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cw4;->c:Ljava/lang/String;

    .line 1949885
    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cw4;->d:Ljava/lang/String;

    .line 1949886
    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    iput v0, p0, LX/Cw4;->e:I

    .line 1949887
    iput-object v1, p0, LX/Cw4;->f:Ljava/lang/String;

    .line 1949888
    iput-object v1, p0, LX/Cw4;->g:Ljava/lang/String;

    .line 1949889
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v0, v0

    .line 1949890
    iput-object v0, p0, LX/Cw4;->h:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1949891
    iput-object v1, p0, LX/Cw4;->i:Ljava/lang/String;

    .line 1949892
    iput-object v1, p0, LX/Cw4;->j:Ljava/lang/String;

    .line 1949893
    return-void

    .line 1949894
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1949895
    const-string v0, "session_id"

    iget-object v1, p0, LX/Cw4;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "vertical"

    iget-object v2, p0, LX/Cw4;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "query"

    iget-object v2, p0, LX/Cw4;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "query_function"

    iget-object v2, p0, LX/Cw4;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "query_length"

    iget v2, p0, LX/Cw4;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "applied_filters"

    iget-object v2, p0, LX/Cw4;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "caller"

    iget-object v2, p0, LX/Cw4;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "source_module_role"

    iget-object v2, p0, LX/Cw4;->h:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "corrected_query_title"

    iget-object v2, p0, LX/Cw4;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "corrected_query_function"

    iget-object v2, p0, LX/Cw4;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949896
    return-object p1
.end method
