.class public LX/Dgm;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Dgl;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/location/sending/NearbyPlace;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2029834
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2029835
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2029836
    iput-object v0, p0, LX/Dgm;->a:LX/0Px;

    .line 2029837
    iput-object p1, p0, LX/Dgm;->b:Landroid/view/View$OnClickListener;

    .line 2029838
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2029840
    new-instance v0, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;-><init>(Landroid/content/Context;)V

    .line 2029841
    iget-object v1, p0, LX/Dgm;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2029842
    new-instance v1, LX/Dgl;

    invoke-direct {v1, v0}, LX/Dgl;-><init>(Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2029843
    check-cast p1, LX/Dgl;

    .line 2029844
    iget-object v0, p0, LX/Dgm;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/location/sending/NearbyPlace;

    .line 2029845
    iget-object v1, p1, LX/Dgl;->l:Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;

    .line 2029846
    iget-object p0, v1, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p1, v0, Lcom/facebook/messaging/location/sending/NearbyPlace;->c:Landroid/net/Uri;

    sget-object p2, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2029847
    iget-object p0, v1, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->c:Landroid/widget/TextView;

    iget-object p1, v0, Lcom/facebook/messaging/location/sending/NearbyPlace;->b:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2029848
    iget-object p0, v1, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->d:Landroid/widget/TextView;

    iget-object p1, v0, Lcom/facebook/messaging/location/sending/NearbyPlace;->f:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2029849
    iget-object p0, v1, Lcom/facebook/messaging/location/sending/NearbyPlaceListItemView;->e:Landroid/widget/TextView;

    iget-object p1, v0, Lcom/facebook/messaging/location/sending/NearbyPlace;->e:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2029850
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2029839
    iget-object v0, p0, LX/Dgm;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
