.class public final LX/EKe;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EKf;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionsModuleInterfaces$SearchResultsElectionsNode;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/CxP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/EKf;


# direct methods
.method public constructor <init>(LX/EKf;)V
    .locals 1

    .prologue
    .line 2107016
    iput-object p1, p0, LX/EKe;->c:LX/EKf;

    .line 2107017
    move-object v0, p1

    .line 2107018
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2107019
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2107020
    const-string v0, "SearchResultsElectionExtrasComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2107021
    if-ne p0, p1, :cond_1

    .line 2107022
    :cond_0
    :goto_0
    return v0

    .line 2107023
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2107024
    goto :goto_0

    .line 2107025
    :cond_3
    check-cast p1, LX/EKe;

    .line 2107026
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2107027
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2107028
    if-eq v2, v3, :cond_0

    .line 2107029
    iget-object v2, p0, LX/EKe;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EKe;->a:LX/CzL;

    iget-object v3, p1, LX/EKe;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2107030
    goto :goto_0

    .line 2107031
    :cond_5
    iget-object v2, p1, LX/EKe;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2107032
    :cond_6
    iget-object v2, p0, LX/EKe;->b:LX/CxP;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/EKe;->b:LX/CxP;

    iget-object v3, p1, LX/EKe;->b:LX/CxP;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2107033
    goto :goto_0

    .line 2107034
    :cond_7
    iget-object v2, p1, LX/EKe;->b:LX/CxP;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
