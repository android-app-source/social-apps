.class public LX/D0m;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/D0k;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D0o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1955969
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/D0m;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/D0o;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1955933
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1955934
    iput-object p1, p0, LX/D0m;->b:LX/0Ot;

    .line 1955935
    return-void
.end method

.method public static a(LX/0QB;)LX/D0m;
    .locals 4

    .prologue
    .line 1955958
    const-class v1, LX/D0m;

    monitor-enter v1

    .line 1955959
    :try_start_0
    sget-object v0, LX/D0m;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1955960
    sput-object v2, LX/D0m;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1955961
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1955962
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1955963
    new-instance v3, LX/D0m;

    const/16 p0, 0x356d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/D0m;-><init>(LX/0Ot;)V

    .line 1955964
    move-object v0, v3

    .line 1955965
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1955966
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D0m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1955967
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1955968
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1955946
    check-cast p2, LX/D0l;

    .line 1955947
    iget-object v0, p0, LX/D0m;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D0o;

    iget-object v2, p2, LX/D0l;->a:Ljava/lang/String;

    iget-object v3, p2, LX/D0l;->b:Ljava/lang/String;

    iget v4, p2, LX/D0l;->c:I

    iget v5, p2, LX/D0l;->d:I

    iget v6, p2, LX/D0l;->e:I

    move-object v1, p1

    .line 1955948
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x2

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/16 v8, 0x8

    const v9, 0x7f0b00d6

    invoke-interface {v7, v8, v9}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/1o5;->h(I)LX/1o5;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const/4 v9, 0x5

    const v10, 0x7f0b00d6

    invoke-interface {v8, v9, v10}, LX/1Di;->g(II)LX/1Di;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, LX/1Di;->b(I)LX/1Di;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p1

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p2

    if-nez v5, :cond_0

    invoke-virtual {v1, v4}, LX/1De;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    :goto_0
    invoke-virtual {p2, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const v8, 0x7f0b004e

    invoke-virtual {v7, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const v8, 0x7f0a00ab

    invoke-virtual {v7, v8}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v7, v8}, LX/1Di;->a(F)LX/1Di;

    move-result-object v7

    invoke-interface {p1, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 1955949
    return-object v0

    :cond_0
    invoke-virtual {v1}, LX/1De;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    move-object v7, v0

    move v9, v4

    move v10, v5

    move-object v11, v2

    move-object p0, v3

    .line 1955950
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1955951
    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1955952
    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1955953
    new-instance v3, LX/47x;

    invoke-direct {v3, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v3, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    .line 1955954
    new-instance v1, LX/D0n;

    invoke-direct {v1, v7, v8, p0}, LX/D0n;-><init>(LX/D0o;Landroid/content/Context;Ljava/lang/String;)V

    move-object v1, v1

    .line 1955955
    const/16 v3, 0x21

    invoke-virtual {v0, v11, v2, v1, v3}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    .line 1955956
    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    move-object v7, v0

    .line 1955957
    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1955944
    invoke-static {}, LX/1dS;->b()V

    .line 1955945
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/D0k;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1955936
    new-instance v1, LX/D0l;

    invoke-direct {v1, p0}, LX/D0l;-><init>(LX/D0m;)V

    .line 1955937
    sget-object v2, LX/D0m;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/D0k;

    .line 1955938
    if-nez v2, :cond_0

    .line 1955939
    new-instance v2, LX/D0k;

    invoke-direct {v2}, LX/D0k;-><init>()V

    .line 1955940
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/D0k;->a$redex0(LX/D0k;LX/1De;IILX/D0l;)V

    .line 1955941
    move-object v1, v2

    .line 1955942
    move-object v0, v1

    .line 1955943
    return-object v0
.end method
