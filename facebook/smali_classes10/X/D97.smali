.class public LX/D97;
.super Landroid/app/DatePickerDialog;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private c:LX/D96;

.field public d:Landroid/widget/DatePicker;

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/app/DatePickerDialog$OnDateSetListener;III)V
    .locals 10

    .prologue
    .line 1969767
    const/4 v4, 0x0

    const/4 v8, -0x1

    const/4 v9, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    invoke-direct/range {v0 .. v9}, LX/D97;-><init>(Landroid/content/Context;ILandroid/app/DatePickerDialog$OnDateSetListener;LX/D96;IIIII)V

    .line 1969768
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILandroid/app/DatePickerDialog$OnDateSetListener;LX/D96;IIIII)V
    .locals 9

    .prologue
    .line 1969746
    const/4 v5, 0x0

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v2 .. v8}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;ILandroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 1969747
    const/4 v2, -0x3

    iput v2, p0, LX/D97;->j:I

    .line 1969748
    const-class v2, LX/D97;

    invoke-static {v2, p0}, LX/D97;->a(Ljava/lang/Class;Landroid/app/Dialog;)V

    .line 1969749
    iput-object p3, p0, LX/D97;->b:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 1969750
    iput p5, p0, LX/D97;->e:I

    .line 1969751
    iput p6, p0, LX/D97;->f:I

    .line 1969752
    move/from16 v0, p7

    iput v0, p0, LX/D97;->g:I

    .line 1969753
    move/from16 v0, p8

    iput v0, p0, LX/D97;->h:I

    .line 1969754
    move/from16 v0, p9

    iput v0, p0, LX/D97;->i:I

    .line 1969755
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/D97;->setCancelable(Z)V

    .line 1969756
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/D97;->setCanceledOnTouchOutside(Z)V

    .line 1969757
    iput-object p4, p0, LX/D97;->c:LX/D96;

    .line 1969758
    iget-object v2, p0, LX/D97;->c:LX/D96;

    if-eqz v2, :cond_0

    .line 1969759
    const/4 v2, -0x1

    const v3, 0x7f082945

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/D90;

    invoke-direct {v4, p0}, LX/D90;-><init>(LX/D97;)V

    invoke-virtual {p0, v2, v3, v4}, LX/D97;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1969760
    const/4 v2, -0x2

    const v3, 0x7f082946

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/D91;

    invoke-direct {v4, p0}, LX/D91;-><init>(LX/D97;)V

    invoke-virtual {p0, v2, v3, v4}, LX/D97;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1969761
    :goto_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 1969762
    move/from16 v0, p7

    invoke-virtual {v3, p5, p6, v0}, Ljava/util/Calendar;->set(III)V

    .line 1969763
    iget-object v2, p0, LX/D97;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11S;

    sget-object v4, LX/1lB;->DATE_PICKER_STYLE:LX/1lB;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-interface {v2, v4, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/D97;->setTitle(Ljava/lang/CharSequence;)V

    .line 1969764
    return-void

    .line 1969765
    :cond_0
    const/4 v2, -0x1

    const v3, 0x7f080016

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/D92;

    invoke-direct {v4, p0}, LX/D92;-><init>(LX/D97;)V

    invoke-virtual {p0, v2, v3, v4}, LX/D97;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1969766
    const/4 v2, -0x2

    const v3, 0x7f080017

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/D93;

    invoke-direct {v4, p0}, LX/D93;-><init>(LX/D97;)V

    invoke-virtual {p0, v2, v3, v4}, LX/D97;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V
    .locals 7

    .prologue
    .line 1969744
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, LX/D97;-><init>(Landroid/content/Context;ILandroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 1969745
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/app/Dialog;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Dialog;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/D97;

    const/16 p0, 0x2e4

    invoke-static {v1, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    iput-object v1, p1, LX/D97;->a:LX/0Or;

    return-void
.end method

.method public static a$redex0(LX/D97;Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 1969726
    iput p2, p0, LX/D97;->j:I

    .line 1969727
    instance-of v0, p1, LX/D97;

    if-eqz v0, :cond_0

    .line 1969728
    check-cast p1, LX/D97;

    invoke-virtual {p1}, LX/D97;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1969729
    if-eqz v0, :cond_0

    .line 1969730
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 1969731
    :cond_0
    return-void
.end method


# virtual methods
.method public final onDateChanged(Landroid/widget/DatePicker;III)V
    .locals 6

    .prologue
    .line 1969732
    iput-object p1, p0, LX/D97;->d:Landroid/widget/DatePicker;

    .line 1969733
    iput p2, p0, LX/D97;->e:I

    .line 1969734
    iput p3, p0, LX/D97;->f:I

    .line 1969735
    iput p4, p0, LX/D97;->g:I

    .line 1969736
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1969737
    invoke-virtual {v1, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    .line 1969738
    iget-object v0, p0, LX/D97;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11S;

    sget-object v2, LX/1lB;->DATE_PICKER_STYLE:LX/1lB;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/D97;->setTitle(Ljava/lang/CharSequence;)V

    .line 1969739
    iget v0, p0, LX/D97;->j:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1969740
    iget-object v0, p0, LX/D97;->b:Landroid/app/DatePickerDialog$OnDateSetListener;

    if-eqz v0, :cond_0

    .line 1969741
    iget-object v0, p0, LX/D97;->b:Landroid/app/DatePickerDialog$OnDateSetListener;

    iget-object v1, p0, LX/D97;->d:Landroid/widget/DatePicker;

    iget v2, p0, LX/D97;->e:I

    iget v3, p0, LX/D97;->f:I

    iget v4, p0, LX/D97;->g:I

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/app/DatePickerDialog$OnDateSetListener;->onDateSet(Landroid/widget/DatePicker;III)V

    .line 1969742
    :cond_0
    const/4 v0, -0x3

    iput v0, p0, LX/D97;->j:I

    .line 1969743
    :cond_1
    return-void
.end method
