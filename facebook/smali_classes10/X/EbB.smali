.class public LX/EbB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Eb9;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:[B

.field public final d:LX/Eat;

.field private final e:[B


# direct methods
.method public constructor <init>(II[BLX/Eat;)V
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2143028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2143029
    new-array v0, v4, [B

    invoke-static {v1, v1}, LX/Eco;->a(II)B

    move-result v1

    aput-byte v1, v0, v3

    .line 2143030
    invoke-static {}, LX/EbL;->u()LX/EbL;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/EbL;->a(I)LX/EbL;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/EbL;->b(I)LX/EbL;

    move-result-object v1

    invoke-static {p3}, LX/EWc;->a([B)LX/EWc;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EbL;->a(LX/EWc;)LX/EbL;

    move-result-object v1

    invoke-virtual {p4}, LX/Eat;->a()[B

    move-result-object v2

    invoke-static {v2}, LX/EWc;->a([B)LX/EWc;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EbL;->b(LX/EWc;)LX/EbL;

    move-result-object v1

    invoke-virtual {v1}, LX/EbL;->l()LX/EbM;

    move-result-object v1

    invoke-virtual {v1}, LX/EWX;->jZ_()[B

    move-result-object v1

    .line 2143031
    iput p1, p0, LX/EbB;->a:I

    .line 2143032
    iput p2, p0, LX/EbB;->b:I

    .line 2143033
    iput-object p3, p0, LX/EbB;->c:[B

    .line 2143034
    iput-object p4, p0, LX/EbB;->d:LX/Eat;

    .line 2143035
    const/4 v2, 0x2

    new-array v2, v2, [[B

    aput-object v0, v2, v3

    aput-object v1, v2, v4

    invoke-static {v2}, LX/Eco;->a([[B)[B

    move-result-object v0

    iput-object v0, p0, LX/EbB;->e:[B

    .line 2143036
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 2143037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2143038
    const/4 v0, 0x1

    :try_start_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v0, v1}, LX/Eco;->a([BII)[[B

    move-result-object v0

    .line 2143039
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    .line 2143040
    const/4 v2, 0x1

    aget-object v0, v0, v2

    .line 2143041
    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v2

    if-ge v2, v3, :cond_0

    .line 2143042
    new-instance v0, LX/Eak;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Legacy message: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eak;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_1

    .line 2143043
    :catch_0
    move-exception v0

    .line 2143044
    :goto_0
    new-instance v1, LX/Eai;

    invoke-direct {v1, v0}, LX/Eai;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2143045
    :cond_0
    :try_start_1
    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v2

    if-le v2, v3, :cond_1

    .line 2143046
    new-instance v0, LX/Eai;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/Eco;->a(B)I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2143047
    :catch_1
    move-exception v0

    goto :goto_0

    .line 2143048
    :cond_1
    sget-object v1, LX/EbM;->a:LX/EWZ;

    invoke-virtual {v1, v0}, LX/EWZ;->a([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EbM;

    move-object v0, v1

    .line 2143049
    invoke-virtual {v0}, LX/EbM;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, LX/EbM;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, LX/EbM;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, LX/EbM;->q()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2143050
    :cond_2
    new-instance v0, LX/Eai;

    const-string v1, "Incomplete message."

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2143051
    :cond_3
    iput-object p1, p0, LX/EbB;->e:[B

    .line 2143052
    iget v1, v0, LX/EbM;->id_:I

    move v1, v1

    .line 2143053
    iput v1, p0, LX/EbB;->a:I

    .line 2143054
    iget v1, v0, LX/EbM;->iteration_:I

    move v1, v1

    .line 2143055
    iput v1, p0, LX/EbB;->b:I

    .line 2143056
    iget-object v1, v0, LX/EbM;->chainKey_:LX/EWc;

    move-object v1, v1

    .line 2143057
    invoke-virtual {v1}, LX/EWc;->d()[B

    move-result-object v1

    iput-object v1, p0, LX/EbB;->c:[B

    .line 2143058
    iget-object v1, v0, LX/EbM;->signingKey_:LX/EWc;

    move-object v0, v1

    .line 2143059
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v0

    iput-object v0, p0, LX/EbB;->d:LX/Eat;
    :try_end_1
    .catch LX/EYr; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/Eag; {:try_start_1 .. :try_end_1} :catch_1

    .line 2143060
    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 1

    .prologue
    .line 2143061
    iget-object v0, p0, LX/EbB;->e:[B

    return-object v0
.end method
