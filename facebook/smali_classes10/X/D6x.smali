.class public final LX/D6x;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;)V
    .locals 0

    .prologue
    .line 1966537
    iput-object p1, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1966538
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 1966539
    check-cast p1, LX/2ou;

    .line 1966540
    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    if-eqz v0, :cond_0

    .line 1966541
    sget-object v0, LX/D6w;->c:[I

    iget-object v1, p1, LX/2ou;->b:LX/2qV;

    invoke-virtual {v1}, LX/2qV;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1966542
    :cond_0
    :goto_0
    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_1

    .line 1966543
    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/3JC;

    iget-object v2, p1, LX/2ou;->b:LX/2qV;

    invoke-direct {v1, v2}, LX/3JC;-><init>(LX/2qV;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1966544
    :cond_1
    return-void

    .line 1966545
    :pswitch_0
    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1966546
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1966547
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    iget-object v0, v0, LX/D6v;->a:LX/2oN;

    sget-object v1, LX/2oN;->VIDEO_AD:LX/2oN;

    if-ne v0, v1, :cond_0

    .line 1966548
    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    iget-object v1, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1966549
    iget-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v1, v2

    .line 1966550
    invoke-virtual {v1}, LX/2pb;->h()I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, v0, LX/D6v;->c:J

    goto :goto_0

    .line 1966551
    :pswitch_1
    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2ou;->c:LX/04g;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    if-eq v0, v1, :cond_2

    iget-object v0, p1, LX/2ou;->c:LX/04g;

    sget-object v1, LX/04g;->BY_REPORTING_FLOW:LX/04g;

    if-ne v0, v1, :cond_0

    .line 1966552
    :cond_2
    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    invoke-virtual {v0}, LX/D6v;->j()LX/BSQ;

    move-result-object v0

    .line 1966553
    iget-object v1, p1, LX/2ou;->c:LX/04g;

    iput-object v1, v0, LX/BSQ;->e:LX/04g;

    .line 1966554
    iget-object v1, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->f:LX/3H4;

    sget-object v2, LX/BSX;->PAUSE_VIDEO_AD:LX/BSX;

    invoke-virtual {v1, v2, v0}, LX/3H4;->a(LX/BSX;LX/BSQ;)V

    goto :goto_0

    .line 1966555
    :pswitch_2
    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2ou;->c:LX/04g;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    if-ne v0, v1, :cond_0

    .line 1966556
    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    invoke-virtual {v0}, LX/D6v;->j()LX/BSQ;

    move-result-object v0

    .line 1966557
    iget-object v1, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v1, v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->f:LX/3H4;

    sget-object v2, LX/BSX;->PLAY_VIDEO_AD:LX/BSX;

    invoke-virtual {v1, v2, v0}, LX/3H4;->a(LX/BSX;LX/BSQ;)V

    goto/16 :goto_0

    .line 1966558
    :pswitch_3
    iget-object v0, p0, LX/D6x;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->w:LX/D6v;

    invoke-virtual {v0}, LX/D6v;->g()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
