.class public final LX/E2F;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E2G;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/E2G;


# direct methods
.method public constructor <init>(LX/E2G;)V
    .locals 1

    .prologue
    .line 2072524
    iput-object p1, p0, LX/E2F;->d:LX/E2G;

    .line 2072525
    move-object v0, p1

    .line 2072526
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2072527
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2072506
    const-string v0, "ReactionHorizontalListComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2072507
    if-ne p0, p1, :cond_1

    .line 2072508
    :cond_0
    :goto_0
    return v0

    .line 2072509
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2072510
    goto :goto_0

    .line 2072511
    :cond_3
    check-cast p1, LX/E2F;

    .line 2072512
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2072513
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2072514
    if-eq v2, v3, :cond_0

    .line 2072515
    iget-object v2, p0, LX/E2F;->a:LX/2km;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E2F;->a:LX/2km;

    iget-object v3, p1, LX/E2F;->a:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2072516
    goto :goto_0

    .line 2072517
    :cond_5
    iget-object v2, p1, LX/E2F;->a:LX/2km;

    if-nez v2, :cond_4

    .line 2072518
    :cond_6
    iget-object v2, p0, LX/E2F;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E2F;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/E2F;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2072519
    goto :goto_0

    .line 2072520
    :cond_8
    iget-object v2, p1, LX/E2F;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_7

    .line 2072521
    :cond_9
    iget-object v2, p0, LX/E2F;->c:LX/0Px;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/E2F;->c:LX/0Px;

    iget-object v3, p1, LX/E2F;->c:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2072522
    goto :goto_0

    .line 2072523
    :cond_a
    iget-object v2, p1, LX/E2F;->c:LX/0Px;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
