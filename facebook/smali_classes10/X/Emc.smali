.class public LX/Emc;
.super LX/AOu;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field private final a:I

.field private final b:I

.field public final c:LX/EoG;

.field public final d:LX/En7;

.field private final e:Landroid/view/LayoutInflater;

.field public final f:LX/Emx;

.field public final g:LX/03V;

.field public final h:LX/En0;

.field public final i:LX/0ja;

.field public final j:LX/Enl;

.field public final k:LX/Eo3;

.field private final l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Enn;",
            "LX/EoH;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Enn;",
            "LX/Emf;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/Ens;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Ens",
            "<",
            "LX/Enn;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/En9;


# direct methods
.method public constructor <init>(LX/Emj;LX/En7;LX/Enl;LX/EnL;LX/En0;Landroid/view/LayoutInflater;LX/0ja;Ljava/lang/Integer;Landroid/os/Bundle;LX/0Sh;LX/EoG;LX/EoI;LX/EnA;LX/Emx;LX/03V;)V
    .locals 8
    .param p1    # LX/Emj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/En7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Enl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/EnL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/En0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/view/LayoutInflater;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/0ja;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/os/Bundle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2166023
    move-object/from16 v0, p10

    invoke-direct {p0, v0}, LX/AOu;-><init>(LX/0Sh;)V

    .line 2166024
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, LX/Emc;->l:Ljava/util/HashMap;

    .line 2166025
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, LX/Emc;->m:Ljava/util/HashMap;

    .line 2166026
    invoke-virtual/range {p12 .. p12}, LX/EoI;->a()I

    move-result v1

    iput v1, p0, LX/Emc;->a:I

    .line 2166027
    invoke-virtual/range {p8 .. p8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, LX/Emc;->b:I

    .line 2166028
    iput-object p6, p0, LX/Emc;->e:Landroid/view/LayoutInflater;

    .line 2166029
    move-object/from16 v0, p11

    iput-object v0, p0, LX/Emc;->c:LX/EoG;

    .line 2166030
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Emc;->f:LX/Emx;

    .line 2166031
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Emc;->g:LX/03V;

    .line 2166032
    iput-object p5, p0, LX/Emc;->h:LX/En0;

    .line 2166033
    iput-object p7, p0, LX/Emc;->i:LX/0ja;

    .line 2166034
    invoke-static {}, LX/Ens;->a()LX/Ens;

    move-result-object v1

    iput-object v1, p0, LX/Emc;->n:LX/Ens;

    .line 2166035
    iput-object p2, p0, LX/Emc;->d:LX/En7;

    .line 2166036
    iput-object p3, p0, LX/Emc;->j:LX/Enl;

    .line 2166037
    invoke-virtual {p3}, LX/Enl;->a()LX/Eo3;

    move-result-object v1

    iput-object v1, p0, LX/Emc;->k:LX/Eo3;

    .line 2166038
    iget-object v2, p0, LX/Emc;->d:LX/En7;

    iget-object v3, p0, LX/Emc;->k:LX/Eo3;

    iget-object v1, p0, LX/Emc;->j:LX/Enl;

    invoke-virtual {v1}, LX/Enl;->d()LX/Enk;

    move-result-object v5

    iget-object v1, p0, LX/Emc;->j:LX/Enl;

    invoke-virtual {v1}, LX/Enl;->j()LX/Eny;

    move-result-object v6

    move-object/from16 v1, p9

    move-object v4, p4

    move-object v7, p1

    invoke-static/range {v1 .. v7}, LX/EnA;->a(Landroid/os/Bundle;LX/En7;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/Emj;)LX/En9;

    move-result-object v1

    iput-object v1, p0, LX/Emc;->o:LX/En9;

    .line 2166039
    return-void
.end method

.method private a(ILX/Enn;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2166021
    iget-object v0, p0, LX/Emc;->g:LX/03V;

    const-string v1, "entity_cards_adapter_item_not_recognized"

    const-string v2, "Key not recognized at position %d with key %s and value %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p3, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2166022
    return-void
.end method

.method public static g(LX/Emc;I)V
    .locals 3

    .prologue
    .line 2166015
    iget-object v0, p0, LX/Emc;->j:LX/Enl;

    invoke-virtual {v0}, LX/Enl;->l()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    .line 2166016
    if-gt p1, v0, :cond_0

    .line 2166017
    iget-object v1, p0, LX/Emc;->j:LX/Enl;

    sget-object v2, LX/Enq;->LEFT:LX/Enq;

    invoke-virtual {v1, v2}, LX/Enl;->a(LX/Enq;)V

    .line 2166018
    :cond_0
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v1

    sub-int v0, v1, v0

    if-lt p1, v0, :cond_1

    .line 2166019
    iget-object v0, p0, LX/Emc;->j:LX/Enl;

    sget-object v1, LX/Enq;->RIGHT:LX/Enq;

    invoke-virtual {v0, v1}, LX/Enl;->a(LX/Enq;)V

    .line 2166020
    :cond_1
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 2

    .prologue
    .line 2166012
    iget-object v0, p0, LX/Emc;->h:LX/En0;

    invoke-virtual {p0, p1}, LX/Emc;->e(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/En0;->a(ILjava/lang/Object;)V

    .line 2166013
    invoke-static {p0, p1}, LX/Emc;->g(LX/Emc;I)V

    .line 2166014
    return-void
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    const/4 v1, -0x2

    .line 2166003
    check-cast p1, LX/Enn;

    .line 2166004
    iget-object v0, p0, LX/Emc;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2166005
    iget-object v0, p0, LX/Emc;->n:LX/Ens;

    .line 2166006
    iget-object v2, v0, LX/Ens;->b:LX/0Px;

    invoke-virtual {v2, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    move v0, v2

    .line 2166007
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 2166008
    :cond_0
    iget-object v2, p0, LX/Emc;->n:LX/Ens;

    invoke-virtual {v2, p1}, LX/Ens;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v0, p1, v2}, LX/Emc;->a(ILX/Enn;Ljava/lang/Object;)V

    move v0, v1

    .line 2166009
    :cond_1
    :goto_0
    return v0

    .line 2166010
    :cond_2
    iget-object v0, p0, LX/Emc;->n:LX/Ens;

    invoke-virtual {v0, p1}, LX/Ens;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v1, p1, v0}, LX/Emc;->a(ILX/Enn;Ljava/lang/Object;)V

    move v0, v1

    .line 2166011
    goto :goto_0
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2166002
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2166040
    check-cast p2, LX/Enn;

    .line 2166041
    iget-object v0, p0, LX/Emc;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2166001
    iget-object v0, p0, LX/Emc;->n:LX/Ens;

    invoke-virtual {v0}, LX/Ens;->c()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2165985
    new-instance v0, LX/EoH;

    iget-object v1, p0, LX/Emc;->e:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EoH;-><init>(Landroid/content/Context;)V

    .line 2165986
    iget v1, p0, LX/Emc;->a:I

    iget v2, p0, LX/Emc;->b:I

    .line 2165987
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v4, 0x11

    invoke-direct {v3, v1, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 2165988
    iget-object v4, v0, LX/EoH;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v4, v3}, Lcom/facebook/widget/CustomFrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2165989
    iget-object v1, p0, LX/Emc;->n:LX/Ens;

    invoke-virtual {v1}, LX/Ens;->c()I

    move-result v1

    if-ge p2, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "getKey() for invalid index"

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2165990
    iget-object v1, p0, LX/Emc;->n:LX/Ens;

    invoke-virtual {v1, p2}, LX/Ens;->b(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Enn;

    move-object v1, v1

    .line 2165991
    invoke-virtual {p0, p2}, LX/Emc;->e(I)Ljava/lang/Object;

    move-result-object v2

    .line 2165992
    iget-object v3, p0, LX/Emc;->m:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2165993
    iget-object v3, p0, LX/Emc;->c:LX/EoG;

    iget-object v4, p0, LX/Emc;->d:LX/En7;

    iget-object v6, p0, LX/Emc;->k:LX/Eo3;

    iget-object v7, p0, LX/Emc;->o:LX/En9;

    iget-object v8, p0, LX/Emc;->i:LX/0ja;

    move-object v5, v1

    move-object v9, v2

    invoke-virtual/range {v3 .. v9}, LX/EoG;->a(LX/En7;Ljava/lang/Object;LX/Eo3;LX/En9;LX/0ja;Ljava/lang/Object;)LX/EoF;

    move-result-object v3

    .line 2165994
    iget-object v4, p0, LX/Emc;->m:Ljava/util/HashMap;

    invoke-virtual {v4, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2165995
    :goto_1
    move-object v2, v3

    .line 2165996
    invoke-virtual {v2, v0}, LX/Eme;->a(Ljava/lang/Object;)V

    .line 2165997
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2165998
    iget-object v2, p0, LX/Emc;->l:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2165999
    return-object v1

    .line 2166000
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-object v3, p0, LX/Emc;->m:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Emf;

    goto :goto_1
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2165984
    return-void
.end method

.method public final c(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 2165968
    check-cast p3, LX/Enn;

    .line 2165969
    iget-object v0, p0, LX/Emc;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to destroy unknown item at position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2165970
    iget-object v0, p0, LX/Emc;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EoH;

    .line 2165971
    iget-object v1, p0, LX/Emc;->m:Ljava/util/HashMap;

    invoke-virtual {v1, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2165972
    iget-object v1, p0, LX/Emc;->m:Ljava/util/HashMap;

    invoke-virtual {v1, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Emf;

    move-object v1, v1

    .line 2165973
    invoke-virtual {v1, v0}, LX/Eme;->b(Ljava/lang/Object;)V

    .line 2165974
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2165975
    return-void
.end method

.method public final e(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2165981
    iget-object v0, p0, LX/Emc;->n:LX/Ens;

    invoke-virtual {v0}, LX/Ens;->c()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "getModel() for invalid index"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2165982
    iget-object v0, p0, LX/Emc;->n:LX/Ens;

    invoke-virtual {v0, p1}, LX/Ens;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 2165983
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2165976
    iget-object v0, p0, LX/AOu;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2165977
    check-cast v0, LX/Enn;

    .line 2165978
    if-nez v0, :cond_0

    .line 2165979
    const/4 v0, 0x0

    .line 2165980
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/Emc;->j:LX/Enl;

    invoke-virtual {v1, v0}, LX/Enl;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
