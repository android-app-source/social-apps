.class public abstract LX/Chc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02k;
.implements LX/ChL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<REQUEST:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/02k;",
        "LX/ChL;",
        "Lcom/facebook/richdocument/RichDocumentFetchCallback;",
        "Lcom/facebook/richdocument/RichDocumentFetchResultParser",
        "<TRESU",
        "LT;",
        ">;",
        "Lcom/facebook/richdocument/logging/RichDocumentSequence;"
    }
.end annotation


# instance fields
.field public A:Z

.field public B:Lcom/facebook/richdocument/RichDocumentFragment;

.field private C:Landroid/content/Context;

.field public D:Landroid/view/View;

.field public E:Landroid/os/Bundle;

.field public F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public G:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

.field private H:LX/ChK;

.field public I:LX/FAd;

.field private J:LX/ChI;

.field public K:LX/1OR;

.field private L:LX/ClG;

.field private M:LX/Clo;

.field private N:Landroid/os/Handler;

.field private O:Ljava/lang/Thread;

.field private P:Z

.field public Q:I

.field public R:Z

.field private S:LX/CrX;

.field private T:LX/Crb;

.field private U:LX/CtI;

.field private V:LX/CtQ;

.field private W:LX/5Mq;

.field private X:LX/Chb;

.field private final Y:LX/1B1;

.field private final Z:LX/ChN;

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final aa:LX/ChP;

.field private final ab:LX/ChR;

.field private final ac:LX/ChT;

.field private final ad:LX/ChV;

.field private final ae:LX/ChX;

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cl7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Yg;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Yh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7ym;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11i;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Ciy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ClD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cl5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CsO;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ClK;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckv;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CqK;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cig;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/CjL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bM;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/CqV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1928215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1928216
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Chc;->R:Z

    .line 1928217
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, LX/Chc;->Y:LX/1B1;

    .line 1928218
    new-instance v0, LX/ChO;

    invoke-direct {v0, p0}, LX/ChO;-><init>(LX/Chc;)V

    iput-object v0, p0, LX/Chc;->Z:LX/ChN;

    .line 1928219
    new-instance v0, LX/ChQ;

    invoke-direct {v0, p0}, LX/ChQ;-><init>(LX/Chc;)V

    iput-object v0, p0, LX/Chc;->aa:LX/ChP;

    .line 1928220
    new-instance v0, LX/ChS;

    invoke-direct {v0, p0}, LX/ChS;-><init>(LX/Chc;)V

    iput-object v0, p0, LX/Chc;->ab:LX/ChR;

    .line 1928221
    new-instance v0, LX/ChU;

    invoke-direct {v0, p0}, LX/ChU;-><init>(LX/Chc;)V

    iput-object v0, p0, LX/Chc;->ac:LX/ChT;

    .line 1928222
    new-instance v0, LX/ChW;

    invoke-direct {v0, p0}, LX/ChW;-><init>(LX/Chc;)V

    iput-object v0, p0, LX/Chc;->ad:LX/ChV;

    .line 1928223
    new-instance v0, LX/ChY;

    invoke-direct {v0, p0}, LX/ChY;-><init>(LX/Chc;)V

    iput-object v0, p0, LX/Chc;->ae:LX/ChX;

    .line 1928224
    return-void
.end method

.method private static a(LX/Chc;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/Ciy;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/CjL;LX/0Ot;LX/0Ot;LX/Cju;LX/0Ot;LX/CqV;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Chc;",
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cl7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8Yg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8Yh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7ym;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/11i;",
            ">;",
            "LX/Ciy;",
            "LX/0Ot",
            "<",
            "LX/ClD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cl5;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CsO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ClK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ckv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ckq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CqK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cig;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;",
            "LX/CjL;",
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8bM;",
            ">;",
            "LX/Cju;",
            "LX/0Ot",
            "<",
            "LX/8bZ;",
            ">;",
            "LX/CqV;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1928225
    iput-object p1, p0, LX/Chc;->a:LX/0Ot;

    iput-object p2, p0, LX/Chc;->b:LX/0Ot;

    iput-object p3, p0, LX/Chc;->c:LX/0Ot;

    iput-object p4, p0, LX/Chc;->d:LX/0Ot;

    iput-object p5, p0, LX/Chc;->e:LX/0Ot;

    iput-object p6, p0, LX/Chc;->f:LX/0Ot;

    iput-object p7, p0, LX/Chc;->g:LX/0Ot;

    iput-object p8, p0, LX/Chc;->h:LX/0Ot;

    iput-object p9, p0, LX/Chc;->i:LX/0Ot;

    iput-object p10, p0, LX/Chc;->j:LX/Ciy;

    iput-object p11, p0, LX/Chc;->k:LX/0Ot;

    iput-object p12, p0, LX/Chc;->l:LX/0Ot;

    iput-object p13, p0, LX/Chc;->m:LX/0Ot;

    iput-object p14, p0, LX/Chc;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/Chc;->o:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/Chc;->p:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/Chc;->q:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/Chc;->r:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/Chc;->s:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/Chc;->t:LX/CjL;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/Chc;->u:LX/0Ot;

    move-object/from16 v0, p22

    iput-object v0, p0, LX/Chc;->v:LX/0Ot;

    move-object/from16 v0, p23

    iput-object v0, p0, LX/Chc;->w:LX/Cju;

    move-object/from16 v0, p24

    iput-object v0, p0, LX/Chc;->x:LX/0Ot;

    move-object/from16 v0, p25

    iput-object v0, p0, LX/Chc;->y:LX/CqV;

    return-void
.end method

.method public static a(LX/Chc;Z)V
    .locals 7

    .prologue
    .line 1928226
    invoke-virtual {p0}, LX/Chc;->F()V

    .line 1928227
    invoke-virtual {p0}, LX/Chc;->D()LX/CH4;

    move-result-object v1

    .line 1928228
    invoke-virtual {p0}, LX/Chc;->E()LX/CGs;

    move-result-object v2

    .line 1928229
    iget-object v0, p0, LX/Chc;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    iget-object v3, p0, LX/Chc;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-virtual {p0}, LX/Chc;->O()LX/0Pq;

    move-result-object v4

    invoke-interface {v3, v4}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v6

    move-object v3, p0

    move-object v4, p0

    move v5, p1

    invoke-virtual/range {v0 .. v6}, LX/Chi;->a(LX/CH4;LX/CGs;LX/Chc;LX/Chc;ZLX/11o;)V

    .line 1928230
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 28

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v27

    move-object/from16 v2, p0

    check-cast v2, LX/Chc;

    const/16 v3, 0x31e0

    move-object/from16 v0, v27

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x321b

    move-object/from16 v0, v27

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x31ec

    move-object/from16 v0, v27

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x31ed

    move-object/from16 v0, v27

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x31dc

    move-object/from16 v0, v27

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x259

    move-object/from16 v0, v27

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1c63

    move-object/from16 v0, v27

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xf9a

    move-object/from16 v0, v27

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x11e9

    move-object/from16 v0, v27

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {v27 .. v27}, LX/Ciy;->a(LX/0QB;)LX/Ciy;

    move-result-object v12

    check-cast v12, LX/Ciy;

    const/16 v13, 0x321c

    move-object/from16 v0, v27

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x321a

    move-object/from16 v0, v27

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x3246

    move-object/from16 v0, v27

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x321e

    move-object/from16 v0, v27

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x3215

    move-object/from16 v0, v27

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x3213

    move-object/from16 v0, v27

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x323c

    move-object/from16 v0, v27

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x31e1

    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x3216

    move-object/from16 v0, v27

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const-class v22, LX/CjL;

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v22

    check-cast v22, LX/CjL;

    const/16 v23, 0x2db

    move-object/from16 v0, v27

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x3232

    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    invoke-static/range {v27 .. v27}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v25

    check-cast v25, LX/Cju;

    const/16 v26, 0x3236

    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v26

    invoke-static/range {v27 .. v27}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object v27

    check-cast v27, LX/CqV;

    invoke-static/range {v2 .. v27}, LX/Chc;->a(LX/Chc;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/Ciy;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/CjL;LX/0Ot;LX/0Ot;LX/Cju;LX/0Ot;LX/CqV;)V

    return-void
.end method


# virtual methods
.method public A()LX/3x6;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1928231
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract D()LX/CH4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/richdocument/fetcher/RichDocumentFetcher",
            "<TREQUEST;TRESU",
            "LT;",
            ">;"
        }
    .end annotation
.end method

.method public abstract E()LX/CGs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/CGs",
            "<TREQUEST;>;"
        }
    .end annotation
.end method

.method public F()V
    .locals 0

    .prologue
    .line 1928232
    return-void
.end method

.method public G()V
    .locals 0

    .prologue
    .line 1928233
    return-void
.end method

.method public H()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1928234
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1928235
    return-object v0
.end method

.method public I()V
    .locals 0

    .prologue
    .line 1928236
    return-void
.end method

.method public final K()Landroid/app/Activity;
    .locals 2

    .prologue
    .line 1928237
    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method public N()V
    .locals 0

    .prologue
    .line 1927959
    invoke-virtual {p0}, LX/Chc;->t()V

    .line 1927960
    return-void
.end method

.method public abstract O()LX/0Pq;
.end method

.method public a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)LX/CtQ;
    .locals 1

    .prologue
    .line 1928238
    new-instance v0, LX/CtQ;

    invoke-direct {v0, p1}, LX/CtQ;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1928239
    invoke-virtual {p0, p1, p2, p3}, LX/Chc;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Chc;->D:Landroid/view/View;

    .line 1928240
    iget-object v0, p0, LX/Chc;->D:Landroid/view/View;

    return-object v0
.end method

.method public abstract a(Landroid/view/View;)Landroid/view/View;
.end method

.method public final a(LX/ChI;)V
    .locals 0

    .prologue
    .line 1928241
    iput-object p1, p0, LX/Chc;->J:LX/ChI;

    .line 1928242
    return-void
.end method

.method public final a(LX/ChK;)V
    .locals 0

    .prologue
    .line 1928243
    iput-object p1, p0, LX/Chc;->H:LX/ChK;

    .line 1928244
    return-void
.end method

.method public a(LX/ChZ;)V
    .locals 0

    .prologue
    .line 1928245
    return-void
.end method

.method public final a(LX/Clo;)V
    .locals 2

    .prologue
    .line 1928246
    iget-boolean v0, p0, LX/Chc;->P:Z

    if-eqz v0, :cond_0

    .line 1928247
    :goto_0
    return-void

    .line 1928248
    :cond_0
    if-nez p1, :cond_1

    .line 1928249
    invoke-virtual {p0}, LX/Chc;->v()V

    goto :goto_0

    .line 1928250
    :cond_1
    invoke-virtual {p0, p1}, LX/Chc;->c(LX/Clo;)LX/1OM;

    move-result-object v0

    .line 1928251
    iget-object v1, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1928252
    invoke-virtual {p0, p1}, LX/Chc;->b(LX/Clo;)V

    .line 1928253
    invoke-virtual {p0}, LX/Chc;->G()V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1928254
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, LX/Chc;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1928255
    invoke-virtual {p0}, LX/Chc;->K()Landroid/app/Activity;

    move-result-object v1

    .line 1928256
    if-eqz v1, :cond_0

    instance-of v0, v1, Lcom/facebook/richdocument/BaseRichDocumentActivity;

    if-nez v0, :cond_0

    .line 1928257
    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    iput v0, p0, LX/Chc;->Q:I

    .line 1928258
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Chc;->R:Z

    .line 1928259
    iget-object v0, p0, LX/Chc;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bZ;

    invoke-virtual {v0}, LX/8bZ;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    .line 1928260
    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1928261
    :cond_0
    return-void

    .line 1928262
    :cond_1
    const/4 v0, 0x7

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1928263
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1928264
    iput-object p1, p0, LX/Chc;->z:Landroid/view/View;

    .line 1928265
    invoke-virtual {p0}, LX/Chc;->I()V

    .line 1928266
    const/4 p1, 0x0

    invoke-static {p0, p1}, LX/Chc;->a(LX/Chc;Z)V

    .line 1928267
    return-void
.end method

.method public final a(Lcom/facebook/richdocument/RichDocumentFragment;)V
    .locals 0

    .prologue
    .line 1928268
    iput-object p1, p0, LX/Chc;->B:Lcom/facebook/richdocument/RichDocumentFragment;

    .line 1928269
    return-void
.end method

.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/195;)V
    .locals 1

    .prologue
    .line 1928270
    new-instance v0, LX/5Mq;

    invoke-direct {v0, p2}, LX/5Mq;-><init>(LX/195;)V

    iput-object v0, p0, LX/Chc;->W:LX/5Mq;

    .line 1928271
    iget-object v0, p0, LX/Chc;->W:LX/5Mq;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 1928272
    return-void
.end method

.method public abstract b(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)LX/1P1;
.end method

.method public abstract b(Ljava/lang/Object;)LX/Clo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRESU",
            "LT;",
            ")",
            "Lcom/facebook/richdocument/model/block/v2/RichDocumentBlocks;"
        }
    .end annotation
.end method

.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    .line 1928273
    iget-object v0, p0, LX/Chc;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11i;

    invoke-virtual {p0}, LX/Chc;->O()LX/0Pq;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 1928274
    if-eqz v1, :cond_0

    .line 1928275
    const-string v0, "rich_document_fragment_starts"

    const v2, -0xb74c81

    invoke-static {v1, v0, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1928276
    :cond_0
    if-eqz p2, :cond_2

    .line 1928277
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getRootView()Landroid/view/View;

    move-result-object v2

    .line 1928278
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1928279
    :cond_1
    instance-of v3, v0, LX/CqD;

    if-eqz v3, :cond_f

    .line 1928280
    iput-boolean v5, p0, LX/Chc;->A:Z

    .line 1928281
    :cond_2
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1928282
    new-instance v0, LX/Civ;

    invoke-direct {v0}, LX/Civ;-><init>()V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1928283
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-nez v0, :cond_10

    .line 1928284
    new-instance v0, LX/Ciw;

    invoke-direct {v0}, LX/Ciw;-><init>()V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1928285
    iget-object v0, p0, LX/Chc;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bM;

    .line 1928286
    iget-object v3, v0, LX/8bM;->a:Landroid/app/Activity;

    if-eqz v3, :cond_11

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 1928287
    if-nez v0, :cond_3

    .line 1928288
    iget-object v0, p0, LX/Chc;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bM;

    invoke-virtual {p0}, LX/Chc;->K()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/8bM;->a(Landroid/app/Activity;)V

    .line 1928289
    :cond_3
    :goto_2
    iget-object v0, p0, LX/Chc;->j:LX/Ciy;

    .line 1928290
    iget-object v3, v0, LX/Ciy;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1928291
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Cit;

    .line 1928292
    new-instance p1, LX/Cix;

    invoke-direct {p1, v0, v3}, LX/Cix;-><init>(LX/Ciy;LX/Cit;)V

    invoke-interface {v3, p1}, LX/Cit;->a(LX/Cix;)V

    .line 1928293
    invoke-interface {v3}, LX/Cit;->b()LX/ChM;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 1928294
    iget-object p1, v0, LX/Ciy;->a:LX/Chv;

    invoke-interface {v3}, LX/Cit;->b()LX/ChM;

    move-result-object p3

    invoke-virtual {p1, p3}, LX/0b4;->a(LX/0b2;)Z

    .line 1928295
    iget-object p1, v0, LX/Ciy;->b:Ljava/util/List;

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1928296
    :cond_5
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, LX/Chc;->O:Ljava/lang/Thread;

    .line 1928297
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/Chc;->N:Landroid/os/Handler;

    .line 1928298
    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1928299
    if-eqz v1, :cond_6

    .line 1928300
    const-string v2, "rich_document_layout_inflation"

    const v3, 0x1482b45c

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1928301
    :cond_6
    invoke-virtual {p0}, LX/Chc;->y()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1928302
    if-eqz v1, :cond_7

    .line 1928303
    const-string v0, "rich_document_layout_inflation"

    const v3, 0xd677909

    invoke-static {v1, v0, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1928304
    :cond_7
    invoke-virtual {p0}, LX/Chc;->z()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;

    .line 1928305
    if-eqz v0, :cond_8

    .line 1928306
    iget-object v3, v0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    move-object v0, v3

    .line 1928307
    new-instance v3, LX/CqW;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/CqW;-><init>(Landroid/content/Context;)V

    .line 1928308
    iput-object v3, v0, LX/7yl;->g:LX/7yk;

    .line 1928309
    :cond_8
    if-eqz v1, :cond_9

    .line 1928310
    const-string v0, "rich_document_view_initialization"

    const v3, -0x5a14b9cc

    invoke-static {v1, v0, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1928311
    :cond_9
    const v0, 0x7f0d04e7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 1928312
    iput-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1928313
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    instance-of v0, v0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;

    if-eqz v0, :cond_a

    .line 1928314
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;

    invoke-virtual {p0}, LX/Chc;->O()LX/0Pq;

    move-result-object v3

    .line 1928315
    iput-object v3, v0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->t:LX/0Pq;

    .line 1928316
    :cond_a
    const v0, 0x7f0d1678

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    iput-object v0, p0, LX/Chc;->G:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    .line 1928317
    iget-object v0, p0, LX/Chc;->G:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    if-eqz v0, :cond_b

    .line 1928318
    iget-object v0, p0, LX/Chc;->G:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-virtual {p0, v2}, LX/Chc;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v3

    .line 1928319
    iput-object v3, v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    .line 1928320
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 1928321
    if-eqz v0, :cond_b

    .line 1928322
    iget-object v0, p0, LX/Chc;->G:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    .line 1928323
    iget-object v3, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v3, v3

    .line 1928324
    const-string v4, "enableIncomingAnimation"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 1928325
    iput-boolean v3, v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->t:Z

    .line 1928326
    iget-object v0, p0, LX/Chc;->G:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    .line 1928327
    iget-object v3, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v3, v3

    .line 1928328
    const-string v4, "enableSwipeToDismiss"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 1928329
    iput-boolean v3, v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->u:Z

    .line 1928330
    :cond_b
    iget-object v0, p0, LX/Chc;->Y:LX/1B1;

    iget-object v3, p0, LX/Chc;->ab:LX/ChR;

    invoke-virtual {v0, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 1928331
    iget-object v0, p0, LX/Chc;->Y:LX/1B1;

    iget-object v3, p0, LX/Chc;->ac:LX/ChT;

    invoke-virtual {v0, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 1928332
    iget-object v0, p0, LX/Chc;->Y:LX/1B1;

    iget-object v3, p0, LX/Chc;->ad:LX/ChV;

    invoke-virtual {v0, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 1928333
    iget-object v0, p0, LX/Chc;->Y:LX/1B1;

    iget-object v3, p0, LX/Chc;->ae:LX/ChX;

    invoke-virtual {v0, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 1928334
    iget-object v0, p0, LX/Chc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cl7;

    iget-object v3, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1928335
    iput-object v3, v0, LX/Cl7;->b:Landroid/support/v7/widget/RecyclerView;

    .line 1928336
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {p0, v0}, LX/Chc;->b(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)LX/1P1;

    move-result-object v0

    iput-object v0, p0, LX/Chc;->K:LX/1OR;

    .line 1928337
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, LX/Chc;->K:LX/1OR;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1928338
    invoke-virtual {p0}, LX/Chc;->A()LX/3x6;

    move-result-object v0

    .line 1928339
    if-eqz v0, :cond_c

    .line 1928340
    iget-object v3, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1928341
    :cond_c
    if-eqz v1, :cond_d

    .line 1928342
    const-string v0, "rich_document_view_initialization"

    const v3, -0x62253010

    invoke-static {v1, v0, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1928343
    :cond_d
    iget-object v0, p0, LX/Chc;->Y:LX/1B1;

    new-instance v3, LX/Cht;

    iget-object v4, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v3, v4}, LX/Cht;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {v0, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 1928344
    iget-object v0, p0, LX/Chc;->Y:LX/1B1;

    new-instance v3, LX/Chs;

    iget-object v4, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v3, v4}, LX/Chs;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {v0, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 1928345
    new-instance v0, LX/CrX;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/CrX;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Chc;->S:LX/CrX;

    .line 1928346
    iget-object v0, p0, LX/Chc;->Y:LX/1B1;

    new-instance v3, LX/Chl;

    iget-object v4, p0, LX/Chc;->S:LX/CrX;

    invoke-direct {v3, v4}, LX/Chl;-><init>(LX/CrX;)V

    invoke-virtual {v0, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 1928347
    new-instance v0, LX/Crb;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/Crb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Chc;->T:LX/Crb;

    .line 1928348
    new-instance v0, LX/CtI;

    iget-object v3, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v4, p0, LX/Chc;->T:LX/Crb;

    invoke-direct {v0, v3, v4}, LX/CtI;-><init>(Landroid/support/v7/widget/RecyclerView;LX/Crb;)V

    iput-object v0, p0, LX/Chc;->U:LX/CtI;

    .line 1928349
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {p0, v0}, LX/Chc;->a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)LX/CtQ;

    move-result-object v0

    iput-object v0, p0, LX/Chc;->V:LX/CtQ;

    .line 1928350
    iget-object v0, p0, LX/Chc;->Y:LX/1B1;

    iget-object v3, p0, LX/Chc;->Z:LX/ChN;

    invoke-virtual {v0, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 1928351
    iget-object v0, p0, LX/Chc;->Y:LX/1B1;

    iget-object v3, p0, LX/Chc;->aa:LX/ChP;

    invoke-virtual {v0, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 1928352
    iget-object v3, p0, LX/Chc;->Y:LX/1B1;

    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b4;

    invoke-virtual {v3, v0}, LX/1B1;->a(LX/0b4;)V

    .line 1928353
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v3, LX/CiV;

    sget-object v4, LX/CiU;->ON_CREATE:LX/CiU;

    invoke-direct {v3, v4}, LX/CiV;-><init>(LX/CiU;)V

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1928354
    if-eqz v1, :cond_e

    .line 1928355
    const-string v0, "rich_document_fragment_starts"

    const v3, 0x5d1a2bb5

    invoke-static {v1, v0, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1928356
    :cond_e
    return-object v2

    .line 1928357
    :cond_f
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1928358
    if-eqz v0, :cond_2

    if-ne v0, v2, :cond_1

    goto/16 :goto_0

    .line 1928359
    :cond_10
    new-instance v0, LX/Ciu;

    invoke-direct {v0}, LX/Ciu;-><init>()V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_1
.end method

.method public b(LX/Clo;)V
    .locals 0

    .prologue
    .line 1927958
    return-void
.end method

.method public final b(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1927961
    iput-object p1, p0, LX/Chc;->C:Landroid/content/Context;

    .line 1927962
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1927963
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiV;

    sget-object v2, LX/CiU;->ON_SAVE_INSTANCE_STATE:LX/CiU;

    invoke-direct {v1, v2}, LX/CiV;-><init>(LX/CiU;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1927964
    return-void
.end method

.method public abstract b(Ljava/lang/Throwable;)V
.end method

.method public c(LX/Clo;)LX/1OM;
    .locals 7

    .prologue
    .line 1927965
    new-instance v0, LX/CoJ;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Chc;->t:LX/CjL;

    invoke-virtual {p0}, LX/Chc;->O()LX/0Pq;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/CjL;->a(LX/0Pq;)LX/CjK;

    move-result-object v3

    .line 1927966
    iget-object v2, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v2, v2

    .line 1927967
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v4

    check-cast v4, LX/1P1;

    .line 1927968
    iget-object v2, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v5, v2

    .line 1927969
    invoke-virtual {p0}, LX/Chc;->O()LX/0Pq;

    move-result-object v6

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, LX/CoJ;-><init>(Landroid/content/Context;LX/Clo;LX/CjK;LX/1P1;Landroid/support/v7/widget/RecyclerView;LX/0Pq;)V

    return-object v0
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1927970
    iput-object p1, p0, LX/Chc;->E:Landroid/os/Bundle;

    .line 1927971
    return-void
.end method

.method public c()Z
    .locals 5

    .prologue
    .line 1927972
    iget-object v0, p0, LX/Chc;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CsO;

    sget-object v1, LX/Crd;->BACK:LX/Crd;

    invoke-virtual {v0, v1}, LX/CsO;->a(LX/Crd;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1927973
    iget-object v0, p0, LX/Chc;->G:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    if-eqz v0, :cond_0

    .line 1927974
    iget-object v0, p0, LX/Chc;->G:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->e()V

    .line 1927975
    iget-object v0, p0, LX/Chc;->N:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/richdocument/RichDocumentDelegateImpl$7;

    invoke-direct {v1, p0}, Lcom/facebook/richdocument/RichDocumentDelegateImpl$7;-><init>(LX/Chc;)V

    const-wide/16 v2, 0x3e8

    const v4, -0x19051386

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1927976
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 1927977
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Chc;->P:Z

    .line 1927978
    iget-object v0, p0, LX/Chc;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11i;

    invoke-virtual {p0}, LX/Chc;->O()LX/0Pq;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1927979
    if-eqz v0, :cond_0

    .line 1927980
    const-string v1, "rich_document_fragment_resume"

    const v2, -0x114cad91

    invoke-static {v0, v1, v2}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1927981
    :cond_0
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-nez v0, :cond_1

    .line 1927982
    invoke-virtual {p0}, LX/Chc;->p()V

    .line 1927983
    :cond_1
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 1927984
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-nez v0, :cond_0

    .line 1927985
    invoke-virtual {p0}, LX/Chc;->r()V

    .line 1927986
    :cond_0
    return-void
.end method

.method public g()V
    .locals 11

    .prologue
    .line 1927987
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiV;

    sget-object v2, LX/CiU;->ON_DESTROY:LX/CiU;

    invoke-direct {v1, v2}, LX/CiV;-><init>(LX/CiU;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1927988
    iget-object v1, p0, LX/Chc;->Y:LX/1B1;

    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b4;

    invoke-virtual {v1, v0}, LX/1B1;->b(LX/0b4;)V

    .line 1927989
    iget-object v0, p0, LX/Chc;->S:LX/CrX;

    invoke-virtual {v0}, LX/CrX;->a()V

    .line 1927990
    iget-object v0, p0, LX/Chc;->U:LX/CtI;

    invoke-virtual {v0}, LX/CtI;->b()V

    .line 1927991
    iget-object v0, p0, LX/Chc;->T:LX/Crb;

    invoke-virtual {v0}, LX/Crb;->a()V

    .line 1927992
    iget-object v0, p0, LX/Chc;->V:LX/CtQ;

    if-eqz v0, :cond_0

    .line 1927993
    iget-object v0, p0, LX/Chc;->V:LX/CtQ;

    .line 1927994
    invoke-static {v0}, LX/CtQ;->g(LX/CtQ;)V

    .line 1927995
    iget-object v1, v0, LX/CtQ;->a:LX/Chv;

    iget-object v2, v0, LX/CtQ;->c:LX/Chw;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1927996
    iget-object v1, v0, LX/Csu;->a:Landroid/support/v7/widget/RecyclerView;

    move-object v1, v1

    .line 1927997
    check-cast v1, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, v0, LX/CtQ;->d:LX/1OX;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 1927998
    :cond_0
    iget-object v0, p0, LX/Chc;->L:LX/ClG;

    if-eqz v0, :cond_1

    .line 1927999
    iget-object v0, p0, LX/Chc;->L:LX/ClG;

    invoke-virtual {v0}, LX/ClG;->a()V

    .line 1928000
    :cond_1
    iget-object v0, p0, LX/Chc;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClK;

    iget-object v1, p0, LX/Chc;->s:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ckw;

    const/4 v7, 0x0

    .line 1928001
    iget-object v3, v0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 1928002
    if-nez v3, :cond_6

    .line 1928003
    :cond_2
    iget-object v0, p0, LX/Chc;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClK;

    iget-object v1, p0, LX/Chc;->s:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ckw;

    invoke-virtual {v0, v1}, LX/ClK;->b(LX/Ckw;)V

    .line 1928004
    iget-object v0, p0, LX/Chc;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckv;

    invoke-virtual {v0}, LX/Ckv;->a()V

    .line 1928005
    iget-object v0, p0, LX/Chc;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckq;

    .line 1928006
    iget-object v3, v0, LX/Ckq;->f:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1928007
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Ckp;

    .line 1928008
    if-eqz v3, :cond_3

    .line 1928009
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1928010
    const-string v8, "image_width"

    iget v9, v3, LX/Ckp;->c:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928011
    const-string v8, "image_height"

    iget v9, v3, LX/Ckp;->d:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928012
    const-string v8, "expands_on_click"

    iget-boolean v9, v3, LX/Ckp;->b:Z

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928013
    const-string v8, "user_clicked_to_expand"

    iget-boolean v9, v3, LX/Ckp;->g:Z

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928014
    const-string v8, "duration_in_viewport_before_intermediate_image"

    iget-wide v9, v3, LX/Ckp;->e:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928015
    const-string v8, "duration_in_viewport_before_final_image"

    iget-wide v9, v3, LX/Ckp;->f:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928016
    const-string v8, "fragment_name"

    iget-object v9, v3, LX/Ckp;->k:Ljava/lang/String;

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928017
    const-string v8, "supports_screen_resolution_images"

    iget-boolean v9, v3, LX/Ckp;->l:Z

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928018
    const-string v8, "supports_full_resolution_images"

    iget-boolean v9, v3, LX/Ckp;->m:Z

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928019
    move-object v3, v7

    .line 1928020
    const-string v5, "screen_width"

    iget v6, v0, LX/Ckq;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928021
    const-string v5, "screen_height"

    iget v6, v0, LX/Ckq;->b:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928022
    const-string v5, "connection_quality"

    iget-object v6, v0, LX/Ckq;->c:LX/0p3;

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928023
    iget-object v5, v0, LX/Ckq;->d:LX/Ckw;

    const-string v6, "android_native_article_image_perf"

    invoke-virtual {v5, v6, v3}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 1928024
    :cond_4
    iget-object v3, v0, LX/Ckq;->f:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 1928025
    invoke-virtual {p0}, LX/Chc;->N()V

    .line 1928026
    iget-object v0, p0, LX/Chc;->I:LX/FAd;

    if-eqz v0, :cond_5

    .line 1928027
    iget-object v0, p0, LX/Chc;->I:LX/FAd;

    invoke-virtual {v0}, LX/FAd;->a()V

    .line 1928028
    :cond_5
    return-void

    .line 1928029
    :cond_6
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1928030
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ClJ;

    .line 1928031
    iget-wide v5, v3, LX/ClJ;->h:J

    long-to-float v5, v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_7

    iget-wide v5, v3, LX/ClJ;->i:J

    long-to-float v5, v5

    cmpg-float v5, v5, v7

    if-gtz v5, :cond_7

    .line 1928032
    iget-object v5, v0, LX/ClK;->b:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v5

    iput-wide v5, v3, LX/ClJ;->i:J

    goto :goto_1
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1928033
    iget-object v0, p0, LX/Chc;->C:Landroid/content/Context;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1928034
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiC;

    invoke-direct {v1}, LX/CiC;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1928035
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 1928036
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiV;

    sget-object v2, LX/CiU;->ON_LOW_MEMORY:LX/CiU;

    invoke-direct {v1, v2}, LX/CiV;-><init>(LX/CiU;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1928037
    return-void
.end method

.method public j()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 1928038
    new-instance v0, LX/Cha;

    invoke-direct {v0, p0}, LX/Cha;-><init>(LX/Chc;)V

    return-object v0
.end method

.method public k()LX/ChZ;
    .locals 1

    .prologue
    .line 1928039
    sget-object v0, LX/ChZ;->NA:LX/ChZ;

    return-object v0
.end method

.method public m()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1928040
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-nez v0, :cond_3

    .line 1928041
    iget-object v0, p0, LX/Chc;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 1928042
    if-eqz v0, :cond_0

    .line 1928043
    const-string v4, "instant_articles"

    const-string v5, "RecyclerView null. isDetached = %s, isFocused = %s, isHidden = %s"

    const/4 v1, 0x3

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v1, p0, LX/Chc;->D:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v3

    iget-object v1, p0, LX/Chc;->D:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isFocused()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v2

    const/4 v1, 0x2

    iget-object v7, p0, LX/Chc;->D:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_2

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1928044
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v1, v3

    .line 1928045
    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    .line 1928046
    :cond_3
    iget-object v0, p0, LX/Chc;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ym;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1928047
    iput-boolean v3, v0, LX/7ym;->d:Z

    .line 1928048
    iget-object v1, v0, LX/7ym;->a:Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;

    if-eqz v1, :cond_4

    .line 1928049
    iget-object v6, v0, LX/7ym;->a:Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;

    if-nez v3, :cond_5

    move v1, v4

    :goto_3
    invoke-virtual {v6, v1}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->setWillNotDraw(Z)V

    .line 1928050
    :cond_4
    iget-object v1, v0, LX/7ym;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7yo;

    .line 1928051
    if-nez v3, :cond_6

    move v6, v4

    :goto_5
    invoke-interface {v1, v6}, LX/7yo;->setWillNotDraw(Z)V

    goto :goto_4

    :cond_5
    move v1, v5

    .line 1928052
    goto :goto_3

    :cond_6
    move v6, v5

    .line 1928053
    goto :goto_5

    .line 1928054
    :cond_7
    iget-object v0, p0, LX/Chc;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2fv;->m:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    .line 1928055
    iget-object v0, p0, LX/Chc;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/2fv;->n:LX/0Tn;

    invoke-interface {v0, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 1928056
    if-eqz v1, :cond_0

    .line 1928057
    new-instance v1, LX/ClG;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz v0, :cond_8

    sget-object v0, LX/ClF;->ONLY_DROPS:LX/ClF;

    :goto_6
    invoke-direct {v1, v2, v0}, LX/ClG;-><init>(Landroid/content/Context;LX/ClF;)V

    iput-object v1, p0, LX/Chc;->L:LX/ClG;

    .line 1928058
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/Chc;->L:LX/ClG;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    goto :goto_2

    .line 1928059
    :cond_8
    sget-object v0, LX/ClF;->ALL_FRAME_INTERVALS:LX/ClF;

    goto :goto_6
.end method

.method public mB_()V
    .locals 0

    .prologue
    .line 1928060
    return-void
.end method

.method public n()V
    .locals 6

    .prologue
    .line 1928061
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v2, LX/CiT;

    iget-object v1, p0, LX/Chc;->u:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, LX/CiT;-><init>(J)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1928062
    return-void
.end method

.method public o()V
    .locals 13

    .prologue
    .line 1928063
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 1928064
    const-string v1, "click_source_document_chaining_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1928065
    iget-object v0, p0, LX/Chc;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClD;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1928066
    iget-object v3, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v3, v3

    .line 1928067
    const/4 v8, -0x1

    .line 1928068
    iget-object v6, v0, LX/ClD;->o:LX/ClC;

    sget-object v7, LX/ClC;->IDLE:LX/ClC;

    if-ne v6, v7, :cond_1

    .line 1928069
    const-wide/16 v11, 0x0

    .line 1928070
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, LX/ClD;->h:Ljava/lang/String;

    .line 1928071
    iput-wide v11, v0, LX/ClD;->k:J

    .line 1928072
    iput-wide v11, v0, LX/ClD;->l:J

    .line 1928073
    iput-wide v11, v0, LX/ClD;->i:J

    .line 1928074
    iput-wide v11, v0, LX/ClD;->j:J

    .line 1928075
    const/4 v9, 0x0

    iput-object v9, v0, LX/ClD;->m:Landroid/content/Context;

    .line 1928076
    iput-wide v11, v0, LX/ClD;->n:J

    .line 1928077
    iget-object v9, v0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 1928078
    iget-object v9, v0, LX/ClD;->a:LX/Cig;

    new-instance v10, LX/Cir;

    invoke-direct {v10}, LX/Cir;-><init>()V

    invoke-virtual {v9, v10}, LX/0b4;->a(LX/0b7;)V

    .line 1928079
    iget-object v9, v0, LX/ClD;->d:LX/8bM;

    .line 1928080
    if-eqz v0, :cond_0

    .line 1928081
    iget-object v10, v9, LX/8bM;->c:Ljava/util/Set;

    invoke-interface {v10, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1928082
    :cond_0
    invoke-static {v0}, LX/ClD;->h(LX/ClD;)V

    .line 1928083
    new-instance v9, LX/Cl8;

    invoke-direct {v9, v0}, LX/Cl8;-><init>(LX/ClD;)V

    .line 1928084
    iget-object v10, v0, LX/ClD;->f:LX/0Xl;

    invoke-interface {v10}, LX/0Xl;->a()LX/0YX;

    move-result-object v10

    const-string v11, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-interface {v10, v11, v9}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v9

    invoke-interface {v9}, LX/0YX;->a()LX/0Yb;

    move-result-object v9

    iput-object v9, v0, LX/ClD;->p:LX/0Yb;

    .line 1928085
    iget-object v9, v0, LX/ClD;->p:LX/0Yb;

    invoke-virtual {v9}, LX/0Yb;->b()V

    .line 1928086
    new-instance v9, LX/Cl9;

    invoke-direct {v9, v0}, LX/Cl9;-><init>(LX/ClD;)V

    .line 1928087
    iget-object v10, v0, LX/ClD;->f:LX/0Xl;

    invoke-interface {v10}, LX/0Xl;->a()LX/0YX;

    move-result-object v10

    const-string v11, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-interface {v10, v11, v9}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v9

    invoke-interface {v9}, LX/0YX;->a()LX/0Yb;

    move-result-object v9

    iput-object v9, v0, LX/ClD;->q:LX/0Yb;

    .line 1928088
    iget-object v9, v0, LX/ClD;->q:LX/0Yb;

    invoke-virtual {v9}, LX/0Yb;->b()V

    .line 1928089
    invoke-static {v0}, LX/ClD;->i(LX/ClD;)V

    .line 1928090
    invoke-static {v0}, LX/ClD;->k(LX/ClD;)V

    .line 1928091
    iput-object v2, v0, LX/ClD;->m:Landroid/content/Context;

    .line 1928092
    :cond_1
    if-nez v3, :cond_3

    .line 1928093
    const/4 v6, 0x0

    invoke-static {v0, v2, v6, v8}, LX/ClD;->a(LX/ClD;Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    .line 1928094
    :goto_0
    move-object v0, v6

    .line 1928095
    if-nez v1, :cond_2

    .line 1928096
    iget-object v1, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v1, v1

    .line 1928097
    const-string v2, "click_source_document_chaining_id"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1928098
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 1928099
    const-string v1, "click_source_document_depth"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1928100
    :cond_2
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v2, LX/CiD;

    iget-object v1, p0, LX/Chc;->u:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, LX/CiD;-><init>(J)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1928101
    iget-object v0, p0, LX/Chc;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cig;

    new-instance v1, LX/Cip;

    invoke-direct {v1}, LX/Cip;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1928102
    return-void

    :cond_3
    const-string v6, "click_source_document_chaining_id"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "click_source_document_depth"

    invoke-virtual {v3, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v0, v2, v6, v7}, LX/ClD;->a(LX/ClD;Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public final p()V
    .locals 5

    .prologue
    .line 1928103
    iget-object v0, p0, LX/Chc;->X:LX/Chb;

    sget-object v1, LX/Chb;->ACTIVE:LX/Chb;

    if-ne v0, v1, :cond_1

    .line 1928104
    :cond_0
    :goto_0
    return-void

    .line 1928105
    :cond_1
    sget-object v0, LX/Chb;->ACTIVE:LX/Chb;

    iput-object v0, p0, LX/Chc;->X:LX/Chb;

    .line 1928106
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    if-eqz v0, :cond_2

    .line 1928107
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiV;

    sget-object v2, LX/CiU;->ON_RESUME:LX/CiU;

    invoke-direct {v1, v2}, LX/CiV;-><init>(LX/CiU;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1928108
    :cond_2
    iget-object v0, p0, LX/Chc;->l:LX/0Ot;

    if-eqz v0, :cond_4

    .line 1928109
    iget-object v0, p0, LX/Chc;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cl5;

    .line 1928110
    iget-object v3, v0, LX/Cl5;->s:LX/Cl4;

    sget-object v4, LX/Cl4;->IDLE:LX/Cl4;

    if-eq v3, v4, :cond_3

    iget-object v3, v0, LX/Cl5;->s:LX/Cl4;

    sget-object v4, LX/Cl4;->PAUSED:LX/Cl4;

    if-ne v3, v4, :cond_4

    .line 1928111
    :cond_3
    iget-object v3, v0, LX/Cl5;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    iput-wide v3, v0, LX/Cl5;->m:J

    .line 1928112
    sget-object v3, LX/Cl4;->TRACKING:LX/Cl4;

    iput-object v3, v0, LX/Cl5;->s:LX/Cl4;

    .line 1928113
    :cond_4
    iget-object v0, p0, LX/Chc;->M:LX/Clo;

    if-eqz v0, :cond_0

    .line 1928114
    iget-object v0, p0, LX/Chc;->M:LX/Clo;

    .line 1928115
    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v3

    .line 1928116
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_6

    .line 1928117
    invoke-virtual {v0, v2}, LX/Clo;->a(I)LX/Clr;

    move-result-object v1

    .line 1928118
    instance-of v4, v1, LX/Cmg;

    if-eqz v4, :cond_5

    .line 1928119
    check-cast v1, LX/Cmg;

    .line 1928120
    iget-object v4, v1, LX/Cmg;->l:LX/1M7;

    if-eqz v4, :cond_5

    .line 1928121
    iget-object v4, v1, LX/Cmg;->l:LX/1M7;

    const/4 p0, 0x1

    invoke-interface {v4, p0}, LX/1M7;->a(Z)V

    .line 1928122
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1928123
    :cond_6
    goto :goto_0
.end method

.method public q()V
    .locals 0

    .prologue
    .line 1928124
    return-void
.end method

.method public r()V
    .locals 5

    .prologue
    .line 1928125
    iget-object v0, p0, LX/Chc;->X:LX/Chb;

    sget-object v1, LX/Chb;->INACTIVE:LX/Chb;

    if-ne v0, v1, :cond_1

    .line 1928126
    :cond_0
    :goto_0
    return-void

    .line 1928127
    :cond_1
    sget-object v0, LX/Chb;->INACTIVE:LX/Chb;

    iput-object v0, p0, LX/Chc;->X:LX/Chb;

    .line 1928128
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    if-eqz v0, :cond_2

    .line 1928129
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiV;

    sget-object v2, LX/CiU;->ON_PAUSE:LX/CiU;

    invoke-direct {v1, v2}, LX/CiV;-><init>(LX/CiU;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1928130
    :cond_2
    iget-object v0, p0, LX/Chc;->K:LX/1OR;

    instance-of v0, v0, LX/CqR;

    if-eqz v0, :cond_3

    .line 1928131
    iget-object v0, p0, LX/Chc;->K:LX/1OR;

    check-cast v0, LX/CqR;

    .line 1928132
    iget-boolean v1, p0, LX/Chc;->A:Z

    if-nez v1, :cond_3

    .line 1928133
    iget-object v1, v0, LX/CqR;->h:LX/1Od;

    if-eqz v1, :cond_3

    .line 1928134
    iget-object v1, v0, LX/CqR;->h:LX/1Od;

    invoke-static {v0, v1}, LX/CqR;->d(LX/CqR;LX/1Od;)V

    .line 1928135
    :cond_3
    iget-object v0, p0, LX/Chc;->l:LX/0Ot;

    if-eqz v0, :cond_4

    .line 1928136
    iget-object v0, p0, LX/Chc;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cl5;

    invoke-virtual {v0}, LX/Cl5;->b()V

    .line 1928137
    :cond_4
    iget-object v0, p0, LX/Chc;->M:LX/Clo;

    if-eqz v0, :cond_0

    .line 1928138
    iget-object v0, p0, LX/Chc;->M:LX/Clo;

    .line 1928139
    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v3

    .line 1928140
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_6

    .line 1928141
    invoke-virtual {v0, v2}, LX/Clo;->a(I)LX/Clr;

    move-result-object v1

    .line 1928142
    instance-of v4, v1, LX/Cmg;

    if-eqz v4, :cond_5

    .line 1928143
    check-cast v1, LX/Cmg;

    .line 1928144
    iget-object v4, v1, LX/Cmg;->l:LX/1M7;

    if-eqz v4, :cond_5

    .line 1928145
    iget-object v4, v1, LX/Cmg;->l:LX/1M7;

    const/4 p0, 0x0

    invoke-interface {v4, p0}, LX/1M7;->a(Z)V

    .line 1928146
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1928147
    :cond_6
    goto :goto_0
.end method

.method public s()V
    .locals 12

    .prologue
    .line 1928148
    iget-object v0, p0, LX/Chc;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cig;

    new-instance v1, LX/Cio;

    invoke-direct {v1}, LX/Cio;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1928149
    iget-object v0, p0, LX/Chc;->l:LX/0Ot;

    if-eqz v0, :cond_0

    .line 1928150
    iget-object v0, p0, LX/Chc;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cl5;

    .line 1928151
    iget-object v2, v0, LX/Cl5;->s:LX/Cl4;

    sget-object v3, LX/Cl4;->IDLE:LX/Cl4;

    if-ne v2, v3, :cond_2

    .line 1928152
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Chc;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqK;

    .line 1928153
    iget-object v1, v0, LX/CqK;->g:LX/CqJ;

    if-eqz v1, :cond_1

    .line 1928154
    iget-object v1, v0, LX/CqK;->g:LX/CqJ;

    invoke-virtual {v1}, LX/1Rm;->b()V

    .line 1928155
    :cond_1
    iget-object v1, v0, LX/CqK;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 1928156
    const/4 v1, 0x0

    iput-object v1, v0, LX/CqK;->g:LX/CqJ;

    .line 1928157
    return-void

    .line 1928158
    :cond_2
    iget-object v2, v0, LX/Cl5;->s:LX/Cl4;

    sget-object v3, LX/Cl4;->TRACKING:LX/Cl4;

    if-ne v2, v3, :cond_3

    .line 1928159
    invoke-virtual {v0}, LX/Cl5;->b()V

    .line 1928160
    :cond_3
    invoke-static {v0}, LX/Cl5;->f(LX/Cl5;)V

    .line 1928161
    iget-object v2, v0, LX/Cl5;->a:LX/K25;

    if-eqz v2, :cond_4

    .line 1928162
    iget-object v2, v0, LX/Cl5;->a:LX/K25;

    .line 1928163
    iget-object v3, v2, LX/K25;->a:LX/1Ay;

    if-eqz v3, :cond_4

    .line 1928164
    iget-object v3, v2, LX/K25;->a:LX/1Ay;

    invoke-virtual {v3}, LX/1Ay;->a()V

    .line 1928165
    :cond_4
    iget-object v2, v0, LX/Cl5;->g:LX/ClO;

    .line 1928166
    iget-boolean v3, v2, LX/ClO;->b:Z

    move v2, v3

    .line 1928167
    if-eqz v2, :cond_5

    .line 1928168
    new-instance v2, LX/ClL;

    const-string v3, "client_long_click"

    invoke-direct {v2, v3}, LX/ClL;-><init>(Ljava/lang/String;)V

    const-string v3, "web_view_time"

    iget-wide v4, v0, LX/Cl5;->n:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    move-result-object v2

    invoke-virtual {v2}, LX/ClL;->a()LX/ClM;

    move-result-object v2

    .line 1928169
    iget-object v3, v0, LX/Cl5;->g:LX/ClO;

    invoke-virtual {v3, v2}, LX/ClO;->a(LX/ClM;)V

    .line 1928170
    :cond_5
    const-wide/16 v10, 0x0

    .line 1928171
    sget-object v8, LX/Cl4;->IDLE:LX/Cl4;

    iput-object v8, v0, LX/Cl5;->s:LX/Cl4;

    .line 1928172
    iput-wide v10, v0, LX/Cl5;->n:J

    .line 1928173
    iput-wide v10, v0, LX/Cl5;->m:J

    .line 1928174
    goto :goto_0
.end method

.method public t()V
    .locals 5

    .prologue
    .line 1928175
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v0, :cond_3

    .line 1928176
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1928177
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 1928178
    if-eqz v0, :cond_3

    .line 1928179
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1928180
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v3, v1

    .line 1928181
    const/4 v0, 0x0

    iget-object v1, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildCount()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    .line 1928182
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1928183
    iget-object v1, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v1

    .line 1928184
    if-eqz v1, :cond_0

    .line 1928185
    invoke-virtual {v3, v1}, LX/1OM;->d(LX/1a1;)V

    .line 1928186
    :cond_0
    instance-of v0, v1, LX/Cs4;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, LX/Cs4;

    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    instance-of v0, v0, LX/CoB;

    if-eqz v0, :cond_1

    .line 1928187
    check-cast v1, LX/Cs4;

    invoke-virtual {v1}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    check-cast v0, LX/CoB;

    .line 1928188
    invoke-virtual {v0}, LX/CoB;->e()V

    .line 1928189
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1928190
    :cond_2
    iget-object v0, p0, LX/Chc;->W:LX/5Mq;

    if-eqz v0, :cond_3

    .line 1928191
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/Chc;->W:LX/5Mq;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 1928192
    :cond_3
    return-void
.end method

.method public v()V
    .locals 4

    .prologue
    .line 1928193
    iget-boolean v0, p0, LX/Chc;->P:Z

    if-eqz v0, :cond_1

    .line 1928194
    :cond_0
    :goto_0
    return-void

    .line 1928195
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Chc;->P:Z

    .line 1928196
    iget-object v0, p0, LX/Chc;->H:LX/ChK;

    if-eqz v0, :cond_2

    .line 1928197
    iget-object v0, p0, LX/Chc;->H:LX/ChK;

    invoke-interface {v0}, LX/ChK;->a()V

    .line 1928198
    :cond_2
    iget-object v0, p0, LX/Chc;->J:LX/ChI;

    if-eqz v0, :cond_3

    .line 1928199
    iget-object v0, p0, LX/Chc;->J:LX/ChI;

    invoke-virtual {v0}, LX/ChI;->a()V

    .line 1928200
    :cond_3
    iget-object v0, p0, LX/Chc;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClD;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/ClD;->a(Landroid/content/Context;)I

    move-result v1

    .line 1928201
    iget-object v0, p0, LX/Chc;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClD;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/ClD;->c(Landroid/content/Context;)V

    .line 1928202
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 1928203
    iget-object v0, p0, LX/Chc;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bM;

    invoke-virtual {v0}, LX/8bM;->b()V

    .line 1928204
    iget-object v0, p0, LX/Chc;->y:LX/CqV;

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1928205
    iget-object v1, v0, LX/CqV;->e:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 1928206
    iput-boolean v2, v0, LX/CqV;->a:Z

    .line 1928207
    iput-boolean v2, v0, LX/CqV;->b:Z

    .line 1928208
    iput-object v3, v0, LX/CqV;->c:LX/CqU;

    .line 1928209
    iput-object v3, v0, LX/CqV;->d:Landroid/view/View;

    .line 1928210
    invoke-virtual {p0}, LX/Chc;->K()Landroid/app/Activity;

    move-result-object v0

    .line 1928211
    if-eqz v0, :cond_4

    iget-boolean v1, p0, LX/Chc;->R:Z

    if-eqz v1, :cond_4

    .line 1928212
    iget v1, p0, LX/Chc;->Q:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1928213
    :cond_4
    goto :goto_0
.end method

.method public abstract y()I
.end method

.method public z()I
    .locals 1

    .prologue
    .line 1928214
    const/4 v0, 0x0

    return v0
.end method
