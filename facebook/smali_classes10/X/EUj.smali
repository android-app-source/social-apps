.class public final LX/EUj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D6L;


# instance fields
.field public final synthetic a:LX/ETd;

.field public final synthetic b:LX/EUn;

.field public final synthetic c:Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

.field public final synthetic d:I

.field public final synthetic e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;LX/ETd;LX/EUn;Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;I)V
    .locals 0

    .prologue
    .line 2126507
    iput-object p1, p0, LX/EUj;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    iput-object p2, p0, LX/EUj;->a:LX/ETd;

    iput-object p3, p0, LX/EUj;->b:LX/EUn;

    iput-object p4, p0, LX/EUj;->c:Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    iput p5, p0, LX/EUj;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    .line 2126508
    iget-object v0, p0, LX/EUj;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    iget-object v0, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->e:LX/2xj;

    invoke-virtual {v0}, LX/2xj;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2126509
    iget-object v1, p0, LX/EUj;->a:LX/ETd;

    iget-object v0, p0, LX/EUj;->b:LX/EUn;

    iget-object v0, v0, LX/EUn;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-interface {v1, v0}, LX/ETd;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2126510
    iget-object v0, p0, LX/EUj;->a:LX/ETd;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2126511
    :cond_0
    iget-object v0, p0, LX/EUj;->e:Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;

    iget-object v7, v0, Lcom/facebook/video/videohome/partdefinitions/VideoHomeChannelFeedLauncherPartDefinition;->d:LX/3AW;

    sget-object v8, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v0, p0, LX/EUj;->b:LX/EUn;

    iget-object v9, v0, LX/EUn;->c:LX/0JD;

    new-instance v0, LX/7Qi;

    iget-object v1, p0, LX/EUj;->c:Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/EUj;->b:LX/EUn;

    iget-object v2, v2, LX/EUn;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126512
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2126513
    iget v3, p0, LX/EUj;->d:I

    iget-object v4, p0, LX/EUj;->b:LX/EUn;

    iget v4, v4, LX/EUn;->d:I

    iget-object v5, p0, LX/EUj;->b:LX/EUn;

    iget-object v5, v5, LX/EUn;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126514
    iget-object v6, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v6

    .line 2126515
    invoke-interface {v5}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/EUj;->b:LX/EUn;

    iget-object v6, v6, LX/EUn;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2126516
    iget-object p0, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, p0

    .line 2126517
    invoke-interface {v6}, LX/9uc;->cN()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/7Qi;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9, v0}, LX/3AW;->b(LX/04D;LX/0JD;LX/7Qf;)V

    .line 2126518
    :cond_1
    return-void
.end method
