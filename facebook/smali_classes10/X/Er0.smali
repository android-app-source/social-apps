.class public LX/Er0;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Eqz;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/EqG;

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field public d:I

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0kL;

.field public g:LX/ErW;


# direct methods
.method public constructor <init>(LX/ErW;LX/0kL;Ljava/util/Set;LX/EqG;)V
    .locals 1
    .param p3    # Ljava/util/Set;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/EqG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ErW;",
            "LX/0kL;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/events/invite/EventsExtendedInviteFriendSelectionChangedListener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172382
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2172383
    const v0, 0x7fffffff

    iput v0, p0, LX/Er0;->d:I

    .line 2172384
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2172385
    iput-object v0, p0, LX/Er0;->e:LX/0Px;

    .line 2172386
    iput-object p1, p0, LX/Er0;->g:LX/ErW;

    .line 2172387
    iput-object p2, p0, LX/Er0;->f:LX/0kL;

    .line 2172388
    iput-object p3, p0, LX/Er0;->b:Ljava/util/Set;

    .line 2172389
    iput-object p4, p0, LX/Er0;->a:LX/EqG;

    .line 2172390
    return-void
.end method

.method private d()I
    .locals 1

    .prologue
    .line 2172381
    iget-object v0, p0, LX/Er0;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public static g(LX/Er0;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2172378
    invoke-virtual {p0, p1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 2172379
    iget-object v0, p0, LX/Er0;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 2172380
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getItem call on non-invitee row is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2172370
    const/4 v2, 0x0

    .line 2172371
    packed-switch p2, :pswitch_data_0

    .line 2172372
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2172373
    :pswitch_0
    new-instance v1, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 2172374
    check-cast v0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->setAsHeaderItem(Z)V

    .line 2172375
    :goto_0
    new-instance v0, LX/Eqz;

    invoke-direct {v0, v1}, LX/Eqz;-><init>(Landroid/view/View;)V

    return-object v0

    .line 2172376
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2172377
    const v1, 0x7f03057a

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2172354
    check-cast p1, LX/Eqz;

    .line 2172355
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2172356
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2172357
    :pswitch_0
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    .line 2172358
    iget-object v1, p0, LX/Er0;->e:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2172359
    iget-object v2, p0, LX/Er0;->b:Ljava/util/Set;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a(LX/8QL;Z)V

    .line 2172360
    :goto_0
    return-void

    .line 2172361
    :pswitch_1
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    iget-boolean v0, p0, LX/Er0;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2172366
    iget-boolean v0, p0, LX/Er0;->c:Z

    if-eq v0, p1, :cond_0

    .line 2172367
    iput-boolean p1, p0, LX/Er0;->c:Z

    .line 2172368
    invoke-direct {p0}, LX/Er0;->d()I

    move-result v0

    invoke-virtual {p0, v0}, LX/1OM;->i_(I)V

    .line 2172369
    :cond_0
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2172363
    invoke-direct {p0}, LX/Er0;->d()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2172364
    const/4 v0, 0x1

    .line 2172365
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2172362
    iget-object v0, p0, LX/Er0;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
