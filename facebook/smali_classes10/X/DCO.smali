.class public LX/DCO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field private final b:LX/0bH;

.field private final c:LX/1NN;

.field private final d:LX/1YK;

.field public e:LX/0qq;

.field public f:LX/0g4;


# direct methods
.method public constructor <init>(LX/0SG;LX/0bH;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1973967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1973968
    iput-object p1, p0, LX/DCO;->a:LX/0SG;

    .line 1973969
    iput-object p2, p0, LX/DCO;->b:LX/0bH;

    .line 1973970
    new-instance v0, LX/DCN;

    invoke-direct {v0, p0}, LX/DCN;-><init>(LX/DCO;)V

    iput-object v0, p0, LX/DCO;->c:LX/1NN;

    .line 1973971
    new-instance v0, LX/DCM;

    invoke-direct {v0, p0}, LX/DCM;-><init>(LX/DCO;)V

    iput-object v0, p0, LX/DCO;->d:LX/1YK;

    .line 1973972
    return-void
.end method

.method public static a(LX/0QB;)LX/DCO;
    .locals 3

    .prologue
    .line 1973973
    new-instance v2, LX/DCO;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v1

    check-cast v1, LX/0bH;

    invoke-direct {v2, v0, v1}, LX/DCO;-><init>(LX/0SG;LX/0bH;)V

    .line 1973974
    move-object v0, v2

    .line 1973975
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1973976
    iget-object v0, p0, LX/DCO;->b:LX/0bH;

    iget-object v1, p0, LX/DCO;->c:LX/1NN;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1973977
    iget-object v0, p0, LX/DCO;->b:LX/0bH;

    iget-object v1, p0, LX/DCO;->d:LX/1YK;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1973978
    return-void
.end method

.method public final a(LX/0qq;LX/0g4;)V
    .locals 2

    .prologue
    .line 1973979
    iget-object v0, p0, LX/DCO;->b:LX/0bH;

    iget-object v1, p0, LX/DCO;->c:LX/1NN;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1973980
    iget-object v0, p0, LX/DCO;->b:LX/0bH;

    iget-object v1, p0, LX/DCO;->d:LX/1YK;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1973981
    iput-object p1, p0, LX/DCO;->e:LX/0qq;

    .line 1973982
    iput-object p2, p0, LX/DCO;->f:LX/0g4;

    .line 1973983
    return-void
.end method
