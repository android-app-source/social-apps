.class public abstract LX/EFS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Kh;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xf
.end annotation


# instance fields
.field public final a:Z

.field private final b:[I

.field private c:Landroid/view/Surface;

.field private d:Landroid/graphics/SurfaceTexture;

.field private e:LX/6Kl;

.field public f:Ljava/nio/ByteBuffer;

.field private final g:[F

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(IIZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2095884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2095885
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LX/EFS;->b:[I

    .line 2095886
    const/4 v0, 0x0

    iput-object v0, p0, LX/EFS;->f:Ljava/nio/ByteBuffer;

    .line 2095887
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/EFS;->g:[F

    .line 2095888
    iput v1, p0, LX/EFS;->h:I

    .line 2095889
    iput v1, p0, LX/EFS;->i:I

    .line 2095890
    iput p1, p0, LX/EFS;->h:I

    .line 2095891
    iput p2, p0, LX/EFS;->i:I

    .line 2095892
    iput-boolean p3, p0, LX/EFS;->a:Z

    .line 2095893
    return-void
.end method


# virtual methods
.method public a(LX/6Kl;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2095894
    const/4 v0, 0x1

    iget-object v1, p0, LX/EFS;->b:[I

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 2095895
    const-string v0, "glGenTextures"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 2095896
    iget-object v0, p0, LX/EFS;->g:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 2095897
    iget-object v0, p0, LX/EFS;->g:[F

    invoke-static {v0}, LX/61I;->a([F)V

    .line 2095898
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, LX/EFS;->b:[I

    aget v1, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, LX/EFS;->d:Landroid/graphics/SurfaceTexture;

    .line 2095899
    iget-object v0, p0, LX/EFS;->d:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p0}, LX/EFS;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/EFS;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 2095900
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, LX/EFS;->d:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/EFS;->c:Landroid/view/Surface;

    .line 2095901
    iput-object p1, p0, LX/EFS;->e:LX/6Kl;

    .line 2095902
    iget-object v0, p0, LX/EFS;->e:LX/6Kl;

    iget-object v1, p0, LX/EFS;->c:Landroid/view/Surface;

    invoke-virtual {v0, p0, v1}, LX/6Kl;->a(LX/6Kd;Landroid/view/Surface;)V

    .line 2095903
    return-void
.end method

.method public abstract a(Ljava/lang/Exception;)V
.end method

.method public abstract a(Ljava/nio/ByteBuffer;)V
.end method

.method public final a()[F
    .locals 1

    .prologue
    .line 2095883
    iget-object v0, p0, LX/EFS;->g:[F

    return-object v0
.end method

.method public final b()LX/61D;
    .locals 1

    .prologue
    .line 2095904
    sget-object v0, LX/61D;->BGRA:LX/61D;

    return-object v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2095881
    invoke-virtual {p0}, LX/EFS;->dE_()V

    .line 2095882
    return-void
.end method

.method public final dE_()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2095876
    iget-object v0, p0, LX/EFS;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 2095877
    iget-object v0, p0, LX/EFS;->d:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 2095878
    iput-object v1, p0, LX/EFS;->f:Ljava/nio/ByteBuffer;

    .line 2095879
    iput-object v1, p0, LX/EFS;->e:LX/6Kl;

    .line 2095880
    return-void
.end method

.method public final f()V
    .locals 11

    .prologue
    .line 2095864
    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, LX/EFS;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/EFS;->getHeight()I

    move-result v3

    .line 2095865
    mul-int v4, v2, v3

    mul-int/lit8 v4, v4, 0x4

    .line 2095866
    iget-object v5, p0, LX/EFS;->f:Ljava/nio/ByteBuffer;

    if-eqz v5, :cond_0

    iget-object v5, p0, LX/EFS;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v5

    if-ge v5, v4, :cond_1

    .line 2095867
    :cond_0
    iget-boolean v5, p0, LX/EFS;->a:Z

    if-eqz v5, :cond_2

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    :goto_0
    iput-object v4, p0, LX/EFS;->f:Ljava/nio/ByteBuffer;

    .line 2095868
    :cond_1
    iget-object v4, p0, LX/EFS;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 2095869
    const/16 v8, 0x1908

    const/16 v9, 0x1401

    iget-object v10, p0, LX/EFS;->f:Ljava/nio/ByteBuffer;

    move v4, v0

    move v5, v1

    move v6, v2

    move v7, v3

    invoke-static/range {v4 .. v10}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 2095870
    iget-object v4, p0, LX/EFS;->f:Ljava/nio/ByteBuffer;

    move-object v0, v4

    .line 2095871
    invoke-virtual {p0, v0}, LX/EFS;->a(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2095872
    :goto_1
    return-void

    .line 2095873
    :catch_0
    move-exception v0

    .line 2095874
    invoke-virtual {p0, v0}, LX/EFS;->a(Ljava/lang/Exception;)V

    goto :goto_1

    .line 2095875
    :cond_2
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    goto :goto_0
.end method

.method public final getHeight()I
    .locals 1

    .prologue
    .line 2095863
    iget v0, p0, LX/EFS;->i:I

    return v0
.end method

.method public final getWidth()I
    .locals 1

    .prologue
    .line 2095861
    iget v0, p0, LX/EFS;->h:I

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 2095862
    const/4 v0, 0x1

    return v0
.end method
