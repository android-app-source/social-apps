.class public LX/DmQ;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field public final c:LX/DkQ;

.field private final d:LX/DnP;

.field public final e:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final f:LX/0Uh;

.field public final g:Landroid/content/Context;

.field public final h:LX/DnT;

.field public i:LX/Did;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/DkQ;LX/DnT;LX/DnP;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Uh;Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/DkQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2039237
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2039238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/DmQ;->a:Ljava/util/List;

    .line 2039239
    iput-object p1, p0, LX/DmQ;->b:Landroid/content/Context;

    .line 2039240
    iput-object p3, p0, LX/DmQ;->h:LX/DnT;

    .line 2039241
    iput-object p2, p0, LX/DmQ;->c:LX/DkQ;

    .line 2039242
    iput-object p4, p0, LX/DmQ;->d:LX/DnP;

    .line 2039243
    iput-object p5, p0, LX/DmQ;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2039244
    iput-object p6, p0, LX/DmQ;->f:LX/0Uh;

    .line 2039245
    iput-object p7, p0, LX/DmQ;->g:Landroid/content/Context;

    .line 2039246
    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 2039203
    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    .line 2039204
    if-eqz p2, :cond_0

    .line 2039205
    iget-object v0, p0, LX/DmQ;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2039206
    invoke-static {p1, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2039207
    :goto_0
    return-void

    .line 2039208
    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/DmQ;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a07f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2039209
    invoke-static {p1, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/DmQ;Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)I
    .locals 3

    .prologue
    .line 2039247
    sget-object v0, LX/DmM;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2039248
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid booking status "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2039249
    :pswitch_0
    iget-object v0, p0, LX/DmQ;->b:Landroid/content/Context;

    const v1, 0x7f0a07f0

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    .line 2039250
    :goto_0
    return v0

    .line 2039251
    :pswitch_1
    iget-object v0, p0, LX/DmQ;->b:Landroid/content/Context;

    const v1, 0x7f0a07f1

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    goto :goto_0

    .line 2039252
    :pswitch_2
    iget-object v0, p0, LX/DmQ;->b:Landroid/content/Context;

    const v1, 0x7f0a07f2

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(LX/DmQ;Ljava/lang/String;Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;)Landroid/content/Intent;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2039210
    iget-object v1, p0, LX/DmQ;->d:LX/DnP;

    iget-object v2, p0, LX/DmQ;->b:Landroid/content/Context;

    const-string v3, "booking_request/%s/create_appointment"

    invoke-static {v3}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2039211
    new-instance v2, LX/Djn;

    invoke-direct {v2}, LX/Djn;-><init>()V

    .line 2039212
    iput-boolean v0, v2, LX/Djn;->a:Z

    .line 2039213
    iput-object p1, v2, LX/Djn;->b:Ljava/lang/String;

    .line 2039214
    iget-object v3, p0, LX/DmQ;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2039215
    iget-object p0, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, p0

    .line 2039216
    iput-object v3, v2, LX/Djn;->e:Ljava/lang/String;

    .line 2039217
    if-eqz p2, :cond_1

    .line 2039218
    invoke-virtual {p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;->l()Ljava/lang/String;

    move-result-object v3

    .line 2039219
    iput-object v3, v2, LX/Djn;->g:Ljava/lang/String;

    .line 2039220
    invoke-virtual {p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 2039221
    iput-object v3, v2, LX/Djn;->k:Ljava/lang/String;

    .line 2039222
    invoke-virtual {p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    .line 2039223
    :cond_0
    if-eqz v0, :cond_1

    .line 2039224
    invoke-virtual {p2}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel$ProductItemModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2039225
    iput-object v0, v2, LX/Djn;->l:Ljava/lang/String;

    .line 2039226
    :cond_1
    const-string v0, "extra_create_booking_appointment_model"

    invoke-virtual {v2}, LX/Djn;->a()Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2039227
    const-string v0, "referrer"

    const-string v2, "list"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2039228
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2039229
    sget-object v0, LX/DmP;->APPOINTMENT_REQUESTS_LIST_ITEM:LX/DmP;

    invoke-virtual {v0}, LX/DmP;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2039230
    iget-object v0, p0, LX/DmQ;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03010b

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2039231
    new-instance v0, LX/DmG;

    invoke-direct {v0, p0, v1}, LX/DmG;-><init>(LX/DmQ;Landroid/view/View;)V

    .line 2039232
    :goto_0
    return-object v0

    .line 2039233
    :cond_0
    sget-object v0, LX/DmP;->APPOINTMENTS_LIST_ITEM:LX/DmP;

    invoke-virtual {v0}, LX/DmP;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 2039234
    iget-object v0, p0, LX/DmQ;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03010d

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2039235
    new-instance v0, LX/DmJ;

    invoke-direct {v0, p0, v1}, LX/DmJ;-><init>(LX/DmQ;Landroid/view/View;)V

    goto :goto_0

    .line 2039236
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 10

    .prologue
    .line 2039183
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2039184
    sget-object v1, LX/DmP;->APPOINTMENT_REQUESTS_LIST_ITEM:LX/DmP;

    invoke-virtual {v1}, LX/DmP;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 2039185
    check-cast p1, LX/DmE;

    .line 2039186
    iget-object v0, p0, LX/DmQ;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;

    .line 2039187
    invoke-static {v0}, LX/DnR;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2039188
    const-string v0, ""

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, LX/DmE;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039189
    :goto_0
    return-void

    .line 2039190
    :cond_0
    invoke-virtual {p1, v0}, LX/DmE;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;)V

    goto :goto_0

    .line 2039191
    :cond_1
    sget-object v1, LX/DmP;->APPOINTMENTS_LIST_ITEM:LX/DmP;

    invoke-virtual {v1}, LX/DmP;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 2039192
    check-cast p1, LX/DmH;

    .line 2039193
    iget-object v0, p0, LX/DmQ;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;

    .line 2039194
    invoke-static {v0}, LX/DnR;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2039195
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2039196
    if-nez v1, :cond_3

    .line 2039197
    :cond_2
    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const-string v8, ""

    const/4 v9, 0x0

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, LX/DmH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2039198
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/DmQ;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 2039199
    :cond_3
    invoke-virtual {p1, v0}, LX/DmH;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;)V

    .line 2039200
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/DmQ;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 2039201
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid viewType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2039181
    sget-object v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENT_REQUESTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    iget-object v1, p0, LX/DmQ;->c:LX/DkQ;

    invoke-virtual {v1}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2039182
    if-eqz v0, :cond_0

    sget-object v0, LX/DmP;->APPOINTMENT_REQUESTS_LIST_ITEM:LX/DmP;

    invoke-virtual {v0}, LX/DmP;->toInt()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/DmP;->APPOINTMENTS_LIST_ITEM:LX/DmP;

    invoke-virtual {v0}, LX/DmP;->toInt()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2039202
    iget-object v0, p0, LX/DmQ;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
