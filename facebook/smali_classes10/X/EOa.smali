.class public LX/EOa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/EOj;


# direct methods
.method public constructor <init>(LX/EOj;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2114793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2114794
    iput-object p1, p0, LX/EOa;->a:LX/EOj;

    .line 2114795
    return-void
.end method

.method public static a(LX/1De;)LX/1Dh;
    .locals 2

    .prologue
    .line 2114807
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/EOa;
    .locals 4

    .prologue
    .line 2114796
    const-class v1, LX/EOa;

    monitor-enter v1

    .line 2114797
    :try_start_0
    sget-object v0, LX/EOa;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114798
    sput-object v2, LX/EOa;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114799
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114800
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114801
    new-instance p0, LX/EOa;

    invoke-static {v0}, LX/EOj;->a(LX/0QB;)LX/EOj;

    move-result-object v3

    check-cast v3, LX/EOj;

    invoke-direct {p0, v3}, LX/EOa;-><init>(LX/EOj;)V

    .line 2114802
    move-object v0, p0

    .line 2114803
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114804
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EOa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114805
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114806
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
