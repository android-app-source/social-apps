.class public final LX/DwW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dwa;


# direct methods
.method public constructor <init>(LX/Dwa;)V
    .locals 0

    .prologue
    .line 2060566
    iput-object p1, p0, LX/DwW;->a:LX/Dwa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2060541
    iget-object v0, p0, LX/DwW;->a:LX/Dwa;

    invoke-static {v0}, LX/Dwa;->s(LX/Dwa;)LX/Dv0;

    move-result-object v0

    .line 2060542
    iget-object v1, v0, LX/Dv0;->d:Ljava/lang/String;

    move-object v4, v1

    .line 2060543
    iget-object v0, p0, LX/DwW;->a:LX/Dwa;

    iget-object v0, v0, LX/Dwa;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DvH;

    iget-object v1, p0, LX/DwW;->a:LX/Dwa;

    iget-object v1, v1, LX/Dvb;->j:Ljava/lang/String;

    iget-object v2, p0, LX/DwW;->a:LX/Dwa;

    iget-object v2, v2, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    const/4 v3, 0x0

    iget-object v5, p0, LX/DwW;->a:LX/Dwa;

    .line 2060544
    invoke-virtual {v5}, LX/Dvb;->b()Z

    move-result v6

    move v5, v6

    .line 2060545
    if-eqz v5, :cond_2

    const/16 v5, 0x1e

    :goto_0
    iget-object v6, p0, LX/DwW;->a:LX/Dwa;

    .line 2060546
    invoke-virtual {v6}, LX/Dvb;->i()Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    move-result-object v7

    move-object v6, v7

    .line 2060547
    iget-object v7, p0, LX/DwW;->a:LX/Dwa;

    iget-object v7, v7, LX/Dwa;->w:LX/Dvf;

    .line 2060548
    new-instance v8, LX/8I7;

    invoke-direct {v8}, LX/8I7;-><init>()V

    move-object v9, v8

    .line 2060549
    const-string v8, "node_id"

    invoke-virtual {v9, v8, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2060550
    const-string v8, "count"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v8, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2060551
    const-string v10, "automatic_photo_captioning_enabled"

    iget-object v8, v0, LX/DvH;->g:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0sX;

    invoke-virtual {v8}, LX/0sX;->a()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v10, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2060552
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2060553
    const-string v8, "before"

    invoke-virtual {v9, v8, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2060554
    :cond_0
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 2060555
    const-string v8, "after"

    invoke-virtual {v9, v8, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2060556
    :cond_1
    iget-object v8, v0, LX/DvH;->f:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/DvI;

    invoke-virtual {v8, v9}, LX/DvI;->a(LX/0gW;)LX/0gW;

    .line 2060557
    iget-object v8, v0, LX/DvH;->h:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0ad;

    sget-short v10, LX/5lr;->d:S

    const/4 v11, 0x0

    invoke-interface {v8, v10, v11}, LX/0ad;->a(SZ)Z

    move-result v10

    .line 2060558
    iget-object v8, v0, LX/DvH;->h:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0ad;

    sget v11, LX/5lr;->e:I

    const v12, 0x15180

    invoke-interface {v8, v11, v12}, LX/0ad;->a(II)I

    move-result v11

    .line 2060559
    iget-object v8, v0, LX/DvH;->b:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v9}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v12

    if-eqz v10, :cond_3

    sget-object v9, LX/0zS;->a:LX/0zS;

    :goto_1
    invoke-virtual {v12, v9}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v9

    int-to-long v10, v11

    invoke-virtual {v9, v10, v11}, LX/0zO;->a(J)LX/0zO;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v10

    .line 2060560
    iget-object v8, v0, LX/DvH;->c:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0QK;

    iget-object v9, v0, LX/DvH;->a:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-static {v10, v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 2060561
    iget-object v8, v0, LX/DvH;->e:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/DvF;

    iget-object v9, v0, LX/DvH;->a:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-static {v10, v8, v6, v9}, LX/DvG;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/DvF;Ljava/lang/Object;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 2060562
    iget-object v8, v0, LX/DvH;->d:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/DvF;

    new-instance v11, LX/DvK;

    sget-object v9, LX/DvW;->ALBUM_MEDIA_SET:LX/DvW;

    invoke-direct {v11, v2, v9, v7}, LX/DvK;-><init>(Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;LX/Dvf;)V

    iget-object v9, v0, LX/DvH;->a:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-static {v10, v8, v11, v9}, LX/DvG;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/DvF;Ljava/lang/Object;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 2060563
    move-object v0, v8

    .line 2060564
    return-object v0

    :cond_2
    const/16 v5, 0xc

    goto/16 :goto_0

    .line 2060565
    :cond_3
    sget-object v9, LX/0zS;->c:LX/0zS;

    goto :goto_1
.end method
