.class public final LX/DQ7;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final synthetic b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

.field public final synthetic c:Lcom/facebook/groups/info/GroupInfoAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V
    .locals 0

    .prologue
    .line 1994054
    iput-object p1, p0, LX/DQ7;->c:Lcom/facebook/groups/info/GroupInfoAdapter;

    iput-object p3, p0, LX/DQ7;->a:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object p4, p0, LX/DQ7;->b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1994055
    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    .line 1994056
    iget-object v0, p0, LX/DQ7;->a:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/DQ7;->c:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v0, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v1, 0x7f08305a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1994057
    :goto_0
    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1994058
    iget-object v0, p0, LX/DQ7;->c:Lcom/facebook/groups/info/GroupInfoAdapter;

    sget-object v1, LX/DQO;->FOLLOW_GROUP:LX/DQO;

    iget-object v2, p0, LX/DQ7;->b:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v0, v1, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a$redex0(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1994059
    return-void

    .line 1994060
    :cond_0
    iget-object v0, p0, LX/DQ7;->c:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v0, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v1, 0x7f08305b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
