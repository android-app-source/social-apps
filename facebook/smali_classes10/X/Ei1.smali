.class public LX/Ei1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Ei1;


# instance fields
.field public a:Ljava/lang/String;

.field public final b:LX/0Zb;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2158972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2158973
    iput-object p1, p0, LX/Ei1;->b:LX/0Zb;

    .line 2158974
    const-string v0, "app_grid_chooser"

    iput-object v0, p0, LX/Ei1;->a:Ljava/lang/String;

    .line 2158975
    return-void
.end method

.method public static a(LX/0QB;)LX/Ei1;
    .locals 4

    .prologue
    .line 2158976
    sget-object v0, LX/Ei1;->d:LX/Ei1;

    if-nez v0, :cond_1

    .line 2158977
    const-class v1, LX/Ei1;

    monitor-enter v1

    .line 2158978
    :try_start_0
    sget-object v0, LX/Ei1;->d:LX/Ei1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2158979
    if-eqz v2, :cond_0

    .line 2158980
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2158981
    new-instance p0, LX/Ei1;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/Ei1;-><init>(LX/0Zb;)V

    .line 2158982
    move-object v0, p0

    .line 2158983
    sput-object v0, LX/Ei1;->d:LX/Ei1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2158984
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2158985
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2158986
    :cond_1
    sget-object v0, LX/Ei1;->d:LX/Ei1;

    return-object v0

    .line 2158987
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2158988
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2158989
    iget-object v0, p0, LX/Ei1;->c:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 2158990
    iget-object v0, p0, LX/Ei1;->b:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/Ei0;->CANCELLED:LX/Ei0;

    iget-object v2, v2, LX/Ei0;->eventName:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ei1;->a:Ljava/lang/String;

    .line 2158991
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2158992
    move-object v1, v1

    .line 2158993
    iget-object v2, p0, LX/Ei1;->c:Ljava/util/Map;

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2158994
    :goto_0
    return-void

    .line 2158995
    :cond_0
    iget-object v0, p0, LX/Ei1;->b:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/Ei0;->CANCELLED:LX/Ei0;

    iget-object v2, v2, LX/Ei0;->eventName:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ei1;->a:Ljava/lang/String;

    .line 2158996
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2158997
    move-object v1, v1

    .line 2158998
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
