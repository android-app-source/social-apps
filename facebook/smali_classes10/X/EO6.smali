.class public final LX/EO6;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EO7;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "LX/8d7;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/CxV;


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2113923
    const-string v0, "SearchResultsOpinionSearchQuerySeeMoreComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2113924
    if-ne p0, p1, :cond_1

    .line 2113925
    :cond_0
    :goto_0
    return v0

    .line 2113926
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2113927
    goto :goto_0

    .line 2113928
    :cond_3
    check-cast p1, LX/EO6;

    .line 2113929
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2113930
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2113931
    if-eq v2, v3, :cond_0

    .line 2113932
    iget-object v2, p0, LX/EO6;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EO6;->a:LX/CzL;

    iget-object v3, p1, LX/EO6;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2113933
    goto :goto_0

    .line 2113934
    :cond_5
    iget-object v2, p1, LX/EO6;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2113935
    :cond_6
    iget-object v2, p0, LX/EO6;->b:LX/CxV;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/EO6;->b:LX/CxV;

    iget-object v3, p1, LX/EO6;->b:LX/CxV;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2113936
    goto :goto_0

    .line 2113937
    :cond_7
    iget-object v2, p1, LX/EO6;->b:LX/CxV;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
