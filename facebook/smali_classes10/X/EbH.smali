.class public final LX/EbH;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EbG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EbH;",
        ">;",
        "LX/EbG;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:LX/EWc;

.field private f:LX/EWc;

.field private g:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2143238
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2143239
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbH;->e:LX/EWc;

    .line 2143240
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbH;->f:LX/EWc;

    .line 2143241
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbH;->g:LX/EWc;

    .line 2143242
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2143243
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2143244
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbH;->e:LX/EWc;

    .line 2143245
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbH;->f:LX/EWc;

    .line 2143246
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbH;->g:LX/EWc;

    .line 2143247
    return-void
.end method

.method private d(LX/EWY;)LX/EbH;
    .locals 1

    .prologue
    .line 2143248
    instance-of v0, p1, LX/EbI;

    if-eqz v0, :cond_0

    .line 2143249
    check-cast p1, LX/EbI;

    invoke-virtual {p0, p1}, LX/EbH;->a(LX/EbI;)LX/EbH;

    move-result-object p0

    .line 2143250
    :goto_0
    return-object p0

    .line 2143251
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EbH;
    .locals 4

    .prologue
    .line 2143252
    const/4 v2, 0x0

    .line 2143253
    :try_start_0
    sget-object v0, LX/EbI;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EbI;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2143254
    if-eqz v0, :cond_0

    .line 2143255
    invoke-virtual {p0, v0}, LX/EbH;->a(LX/EbI;)LX/EbH;

    .line 2143256
    :cond_0
    return-object p0

    .line 2143257
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2143258
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2143259
    check-cast v0, LX/EbI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2143260
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2143261
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2143262
    invoke-virtual {p0, v1}, LX/EbH;->a(LX/EbI;)LX/EbH;

    :cond_1
    throw v0

    .line 2143263
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static u()LX/EbH;
    .locals 1

    .prologue
    .line 2143264
    new-instance v0, LX/EbH;

    invoke-direct {v0}, LX/EbH;-><init>()V

    return-object v0
.end method

.method private w()LX/EbH;
    .locals 2

    .prologue
    .line 2143265
    invoke-static {}, LX/EbH;->u()LX/EbH;

    move-result-object v0

    invoke-direct {p0}, LX/EbH;->y()LX/EbI;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EbH;->a(LX/EbI;)LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method private y()LX/EbI;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2143266
    new-instance v2, LX/EbI;

    invoke-direct {v2, p0}, LX/EbI;-><init>(LX/EWj;)V

    .line 2143267
    iget v3, p0, LX/EbH;->a:I

    .line 2143268
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 2143269
    :goto_0
    iget v1, p0, LX/EbH;->b:I

    .line 2143270
    iput v1, v2, LX/EbI;->registrationId_:I

    .line 2143271
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2143272
    or-int/lit8 v0, v0, 0x2

    .line 2143273
    :cond_0
    iget v1, p0, LX/EbH;->c:I

    .line 2143274
    iput v1, v2, LX/EbI;->preKeyId_:I

    .line 2143275
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2143276
    or-int/lit8 v0, v0, 0x4

    .line 2143277
    :cond_1
    iget v1, p0, LX/EbH;->d:I

    .line 2143278
    iput v1, v2, LX/EbI;->signedPreKeyId_:I

    .line 2143279
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2143280
    or-int/lit8 v0, v0, 0x8

    .line 2143281
    :cond_2
    iget-object v1, p0, LX/EbH;->e:LX/EWc;

    .line 2143282
    iput-object v1, v2, LX/EbI;->baseKey_:LX/EWc;

    .line 2143283
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 2143284
    or-int/lit8 v0, v0, 0x10

    .line 2143285
    :cond_3
    iget-object v1, p0, LX/EbH;->f:LX/EWc;

    .line 2143286
    iput-object v1, v2, LX/EbI;->identityKey_:LX/EWc;

    .line 2143287
    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    .line 2143288
    or-int/lit8 v0, v0, 0x20

    .line 2143289
    :cond_4
    iget-object v1, p0, LX/EbH;->g:LX/EWc;

    .line 2143290
    iput-object v1, v2, LX/EbI;->message_:LX/EWc;

    .line 2143291
    iput v0, v2, LX/EbI;->bitField0_:I

    .line 2143292
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2143293
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2143331
    invoke-direct {p0, p1}, LX/EbH;->d(LX/EWY;)LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2143294
    invoke-direct {p0, p1, p2}, LX/EbH;->d(LX/EWd;LX/EYZ;)LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/EbH;
    .locals 1

    .prologue
    .line 2143295
    iget v0, p0, LX/EbH;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EbH;->a:I

    .line 2143296
    iput p1, p0, LX/EbH;->b:I

    .line 2143297
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143298
    return-object p0
.end method

.method public final a(LX/EWc;)LX/EbH;
    .locals 1

    .prologue
    .line 2143299
    if-nez p1, :cond_0

    .line 2143300
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2143301
    :cond_0
    iget v0, p0, LX/EbH;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EbH;->a:I

    .line 2143302
    iput-object p1, p0, LX/EbH;->e:LX/EWc;

    .line 2143303
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143304
    return-object p0
.end method

.method public final a(LX/EbI;)LX/EbH;
    .locals 2

    .prologue
    .line 2143305
    sget-object v0, LX/EbI;->c:LX/EbI;

    move-object v0, v0

    .line 2143306
    if-ne p1, v0, :cond_0

    .line 2143307
    :goto_0
    return-object p0

    .line 2143308
    :cond_0
    const/4 v0, 0x1

    .line 2143309
    iget v1, p1, LX/EbI;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_7

    :goto_1
    move v0, v0

    .line 2143310
    if-eqz v0, :cond_1

    .line 2143311
    iget v0, p1, LX/EbI;->registrationId_:I

    move v0, v0

    .line 2143312
    invoke-virtual {p0, v0}, LX/EbH;->a(I)LX/EbH;

    .line 2143313
    :cond_1
    invoke-virtual {p1}, LX/EbI;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2143314
    iget v0, p1, LX/EbI;->preKeyId_:I

    move v0, v0

    .line 2143315
    invoke-virtual {p0, v0}, LX/EbH;->b(I)LX/EbH;

    .line 2143316
    :cond_2
    invoke-virtual {p1}, LX/EbI;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2143317
    iget v0, p1, LX/EbI;->signedPreKeyId_:I

    move v0, v0

    .line 2143318
    invoke-virtual {p0, v0}, LX/EbH;->c(I)LX/EbH;

    .line 2143319
    :cond_3
    invoke-virtual {p1}, LX/EbI;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2143320
    iget-object v0, p1, LX/EbI;->baseKey_:LX/EWc;

    move-object v0, v0

    .line 2143321
    invoke-virtual {p0, v0}, LX/EbH;->a(LX/EWc;)LX/EbH;

    .line 2143322
    :cond_4
    invoke-virtual {p1}, LX/EbI;->w()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2143323
    iget-object v0, p1, LX/EbI;->identityKey_:LX/EWc;

    move-object v0, v0

    .line 2143324
    invoke-virtual {p0, v0}, LX/EbH;->b(LX/EWc;)LX/EbH;

    .line 2143325
    :cond_5
    invoke-virtual {p1}, LX/EbI;->y()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2143326
    iget-object v0, p1, LX/EbI;->message_:LX/EWc;

    move-object v0, v0

    .line 2143327
    invoke-virtual {p0, v0}, LX/EbH;->c(LX/EWc;)LX/EbH;

    .line 2143328
    :cond_6
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2143329
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2143330
    invoke-direct {p0, p1, p2}, LX/EbH;->d(LX/EWd;LX/EYZ;)LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2143233
    invoke-direct {p0}, LX/EbH;->w()LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)LX/EbH;
    .locals 1

    .prologue
    .line 2143234
    iget v0, p0, LX/EbH;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EbH;->a:I

    .line 2143235
    iput p1, p0, LX/EbH;->c:I

    .line 2143236
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143237
    return-object p0
.end method

.method public final b(LX/EWc;)LX/EbH;
    .locals 1

    .prologue
    .line 2143200
    if-nez p1, :cond_0

    .line 2143201
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2143202
    :cond_0
    iget v0, p0, LX/EbH;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/EbH;->a:I

    .line 2143203
    iput-object p1, p0, LX/EbH;->f:LX/EWc;

    .line 2143204
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143205
    return-object p0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2143232
    invoke-direct {p0, p1, p2}, LX/EbH;->d(LX/EWd;LX/EYZ;)LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2143206
    invoke-direct {p0}, LX/EbH;->w()LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2143207
    invoke-direct {p0, p1}, LX/EbH;->d(LX/EWY;)LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)LX/EbH;
    .locals 1

    .prologue
    .line 2143208
    iget v0, p0, LX/EbH;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EbH;->a:I

    .line 2143209
    iput p1, p0, LX/EbH;->d:I

    .line 2143210
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143211
    return-object p0
.end method

.method public final c(LX/EWc;)LX/EbH;
    .locals 1

    .prologue
    .line 2143212
    if-nez p1, :cond_0

    .line 2143213
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2143214
    :cond_0
    iget v0, p0, LX/EbH;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, LX/EbH;->a:I

    .line 2143215
    iput-object p1, p0, LX/EbH;->g:LX/EWc;

    .line 2143216
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2143217
    return-object p0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2143218
    invoke-direct {p0}, LX/EbH;->w()LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2143219
    sget-object v0, LX/EbV;->d:LX/EYn;

    const-class v1, LX/EbI;

    const-class v2, LX/EbH;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2143220
    sget-object v0, LX/EbV;->c:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2143221
    invoke-direct {p0}, LX/EbH;->w()LX/EbH;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2143222
    invoke-direct {p0}, LX/EbH;->y()LX/EbI;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2143223
    invoke-virtual {p0}, LX/EbH;->l()LX/EbI;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2143224
    invoke-direct {p0}, LX/EbH;->y()LX/EbI;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2143225
    invoke-virtual {p0}, LX/EbH;->l()LX/EbI;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EbI;
    .locals 2

    .prologue
    .line 2143226
    invoke-direct {p0}, LX/EbH;->y()LX/EbI;

    move-result-object v0

    .line 2143227
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2143228
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2143229
    :cond_0
    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2143230
    sget-object v0, LX/EbI;->c:LX/EbI;

    move-object v0, v0

    .line 2143231
    return-object v0
.end method
