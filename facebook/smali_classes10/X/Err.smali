.class public LX/Err;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DC7;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile x:LX/Err;


# instance fields
.field public b:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qz;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0rm;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pm;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/187;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pn;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Fn;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Ym;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oy;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/DC2;",
            ">;"
        }
    .end annotation
.end field

.field public final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public q:I

.field public r:Ljava/lang/Boolean;

.field public s:Ljava/lang/Boolean;

.field public t:Ljava/util/UUID;

.field public u:I

.field public v:I

.field public w:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2173672
    const-class v0, LX/Err;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Err;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2173640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2173641
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173642
    iput-object v0, p0, LX/Err;->b:LX/0Ot;

    .line 2173643
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173644
    iput-object v0, p0, LX/Err;->c:LX/0Ot;

    .line 2173645
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173646
    iput-object v0, p0, LX/Err;->d:LX/0Ot;

    .line 2173647
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173648
    iput-object v0, p0, LX/Err;->e:LX/0Ot;

    .line 2173649
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173650
    iput-object v0, p0, LX/Err;->f:LX/0Ot;

    .line 2173651
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173652
    iput-object v0, p0, LX/Err;->g:LX/0Ot;

    .line 2173653
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173654
    iput-object v0, p0, LX/Err;->h:LX/0Ot;

    .line 2173655
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173656
    iput-object v0, p0, LX/Err;->i:LX/0Ot;

    .line 2173657
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173658
    iput-object v0, p0, LX/Err;->j:LX/0Ot;

    .line 2173659
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173660
    iput-object v0, p0, LX/Err;->k:LX/0Ot;

    .line 2173661
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173662
    iput-object v0, p0, LX/Err;->l:LX/0Ot;

    .line 2173663
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173664
    iput-object v0, p0, LX/Err;->m:LX/0Ot;

    .line 2173665
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2173666
    iput-object v0, p0, LX/Err;->n:LX/0Ot;

    .line 2173667
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Err;->o:Ljava/util/List;

    .line 2173668
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Err;->p:Ljava/util/List;

    .line 2173669
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/Err;->r:Ljava/lang/Boolean;

    .line 2173670
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/Err;->s:Ljava/lang/Boolean;

    .line 2173671
    return-void
.end method

.method public static a(LX/0QB;)LX/Err;
    .locals 3

    .prologue
    .line 2173630
    sget-object v0, LX/Err;->x:LX/Err;

    if-nez v0, :cond_1

    .line 2173631
    const-class v1, LX/Err;

    monitor-enter v1

    .line 2173632
    :try_start_0
    sget-object v0, LX/Err;->x:LX/Err;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2173633
    if-eqz v2, :cond_0

    .line 2173634
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/Err;->b(LX/0QB;)LX/Err;

    move-result-object v0

    sput-object v0, LX/Err;->x:LX/Err;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173635
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2173636
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2173637
    :cond_1
    sget-object v0, LX/Err;->x:LX/Err;

    return-object v0

    .line 2173638
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2173639
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Err;Ljava/lang/String;LX/0rU;)V
    .locals 9

    .prologue
    .line 2173562
    iget-object v0, p0, LX/Err;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qz;

    iget-object v1, p0, LX/Err;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Ym;

    invoke-virtual {v1}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v1

    iget-object v2, p0, LX/Err;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Fn;

    invoke-virtual {v2}, LX/1Fn;->c()I

    move-result v2

    const/4 v4, 0x1

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v6, LX/0gf;->OFFLINE_FEED:LX/0gf;

    move-object v3, p1

    move-object v7, p2

    .line 2173563
    new-instance v8, LX/0rT;

    invoke-direct {v8}, LX/0rT;-><init>()V

    .line 2173564
    iput-object v5, v8, LX/0rT;->a:LX/0rS;

    .line 2173565
    move-object v8, v8

    .line 2173566
    iput-object v1, v8, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 2173567
    move-object v8, v8

    .line 2173568
    iput v2, v8, LX/0rT;->c:I

    .line 2173569
    move-object v8, v8

    .line 2173570
    invoke-virtual {v8, v6}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v8

    .line 2173571
    iput-boolean v4, v8, LX/0rT;->r:Z

    .line 2173572
    move-object v8, v8

    .line 2173573
    iput-object v7, v8, LX/0rT;->k:LX/0rU;

    .line 2173574
    move-object v8, v8

    .line 2173575
    const/4 p2, 0x0

    .line 2173576
    iput-boolean p2, v8, LX/0rT;->j:Z

    .line 2173577
    move-object v8, v8

    .line 2173578
    sget-object p2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2173579
    iput-object p2, v8, LX/0rT;->s:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2173580
    move-object v8, v8

    .line 2173581
    if-eqz v3, :cond_0

    .line 2173582
    iput-object v3, v8, LX/0rT;->f:Ljava/lang/String;

    .line 2173583
    :cond_0
    invoke-static {v0, v8}, LX/0qz;->a(LX/0qz;LX/0rT;)V

    .line 2173584
    invoke-virtual {v8}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v8

    move-object v1, v8

    .line 2173585
    iget-object v0, p0, LX/Err;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0rm;

    const-string v2, "offline_feed_new_data_fetch"

    new-instance v3, LX/Erq;

    invoke-direct {v3, p0, v1}, LX/Erq;-><init>(LX/Err;Lcom/facebook/api/feed/FetchFeedParams;)V

    invoke-virtual {v0, v1, v2, v3}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;LX/0tW;)LX/0v6;

    move-result-object v2

    .line 2173586
    iget-object v0, p0, LX/Err;->s:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2173587
    :goto_0
    return-void

    .line 2173588
    :cond_1
    iput-object p1, p0, LX/Err;->w:Ljava/lang/String;

    .line 2173589
    iget-object v0, p0, LX/Err;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    iget-object v1, p0, LX/Err;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, v2, v1}, LX/0tX;->b(LX/0v6;Ljava/util/concurrent/ExecutorService;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/Err;LX/0oG;LX/Erp;)V
    .locals 2

    .prologue
    .line 2173621
    const-string v0, "fetch_state"

    invoke-virtual {p1, v0, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2173622
    const-string v0, "num_stories_fetched"

    iget v1, p0, LX/Err;->q:I

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2173623
    const-string v0, "is_loading"

    iget-object v1, p0, LX/Err;->r:Ljava/lang/Boolean;

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2173624
    const-string v0, "is_interrupted"

    iget-object v1, p0, LX/Err;->s:Ljava/lang/Boolean;

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2173625
    const-string v0, "fetch_id"

    iget-object v1, p0, LX/Err;->t:Ljava/util/UUID;

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2173626
    const-string v0, "progress"

    iget v1, p0, LX/Err;->u:I

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2173627
    const-string v1, "num_feed_stories"

    iget-object v0, p0, LX/Err;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fn;

    invoke-virtual {v0}, LX/1Fn;->c()I

    move-result v0

    invoke-virtual {p1, v1, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2173628
    invoke-virtual {p1}, LX/0oG;->d()V

    .line 2173629
    return-void
.end method

.method public static a$redex0(LX/Err;LX/DC6;)V
    .locals 2

    .prologue
    .line 2173617
    iget-object v0, p0, LX/Err;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DC2;

    .line 2173618
    invoke-interface {v0, p1}, LX/DC2;->a(LX/DC6;)V

    goto :goto_0

    .line 2173619
    :cond_0
    invoke-static {p0}, LX/Err;->j(LX/Err;)V

    .line 2173620
    return-void
.end method

.method private static b(LX/0QB;)LX/Err;
    .locals 14

    .prologue
    .line 2173613
    new-instance v0, LX/Err;

    invoke-direct {v0}, LX/Err;-><init>()V

    .line 2173614
    const/16 v1, 0x140f

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x69c

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x671

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x6b5

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x5f2

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x670

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xafd

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2e3

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xbc

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xe8

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x69d

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x60a

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xe4

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    .line 2173615
    iput-object v1, v0, LX/Err;->b:LX/0Ot;

    iput-object v2, v0, LX/Err;->c:LX/0Ot;

    iput-object v3, v0, LX/Err;->d:LX/0Ot;

    iput-object v4, v0, LX/Err;->e:LX/0Ot;

    iput-object v5, v0, LX/Err;->f:LX/0Ot;

    iput-object v6, v0, LX/Err;->g:LX/0Ot;

    iput-object v7, v0, LX/Err;->h:LX/0Ot;

    iput-object v8, v0, LX/Err;->i:LX/0Ot;

    iput-object v9, v0, LX/Err;->j:LX/0Ot;

    iput-object v10, v0, LX/Err;->k:LX/0Ot;

    iput-object v11, v0, LX/Err;->l:LX/0Ot;

    iput-object v12, v0, LX/Err;->m:LX/0Ot;

    iput-object v13, v0, LX/Err;->n:LX/0Ot;

    .line 2173616
    return-object v0
.end method

.method public static b$redex0(LX/Err;I)V
    .locals 2

    .prologue
    .line 2173673
    iput p1, p0, LX/Err;->u:I

    .line 2173674
    iget-object v0, p0, LX/Err;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DC2;

    .line 2173675
    invoke-interface {v0, p1}, LX/DC2;->a(I)V

    goto :goto_0

    .line 2173676
    :cond_0
    return-void
.end method

.method public static c(LX/Err;Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2173606
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2173607
    if-nez v0, :cond_1

    iget v1, p0, LX/Err;->q:I

    iget-object v0, p0, LX/Err;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fn;

    invoke-virtual {v0}, LX/1Fn;->c()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 2173608
    :cond_1
    const/4 v0, 0x0

    .line 2173609
    :goto_1
    return v0

    .line 2173610
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    .line 2173611
    sget-object v1, LX/0rU;->TAIL:LX/0rU;

    invoke-static {p0, v0, v1}, LX/Err;->a(LX/Err;Ljava/lang/String;LX/0rU;)V

    .line 2173612
    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d$redex0(LX/Err;)V
    .locals 2

    .prologue
    .line 2173597
    invoke-static {p0}, LX/Err;->f(LX/Err;)LX/0oG;

    move-result-object v1

    .line 2173598
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173599
    iget-object v0, p0, LX/Err;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a(LX/0oG;)V

    .line 2173600
    sget-object v0, LX/Erp;->CANCEL:LX/Erp;

    invoke-static {p0, v1, v0}, LX/Err;->a$redex0(LX/Err;LX/0oG;LX/Erp;)V

    .line 2173601
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/Err;->t:Ljava/util/UUID;

    .line 2173602
    iget-object v0, p0, LX/Err;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DC2;

    .line 2173603
    invoke-interface {v0}, LX/DC2;->b()V

    goto :goto_0

    .line 2173604
    :cond_1
    invoke-static {p0}, LX/Err;->j(LX/Err;)V

    .line 2173605
    return-void
.end method

.method public static e(LX/Err;)I
    .locals 6

    .prologue
    .line 2173591
    iget-object v0, p0, LX/Err;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fn;

    invoke-virtual {v0}, LX/1Fn;->c()I

    move-result v1

    .line 2173592
    if-nez v1, :cond_0

    .line 2173593
    const/4 v0, 0x0

    .line 2173594
    :goto_0
    return v0

    .line 2173595
    :cond_0
    iget-object v0, p0, LX/Err;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fn;

    invoke-virtual {v0}, LX/1Fn;->e()D

    move-result-wide v2

    .line 2173596
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    iget v0, p0, LX/Err;->q:I

    int-to-double v4, v0

    mul-double/2addr v2, v4

    int-to-double v0, v1

    div-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method public static f(LX/Err;)LX/0oG;
    .locals 3

    .prologue
    .line 2173590
    iget-object v0, p0, LX/Err;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "fb4a_offline_feed_fetch"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    return-object v0
.end method

.method public static j(LX/Err;)V
    .locals 1

    .prologue
    .line 2173558
    iget-object v0, p0, LX/Err;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2173559
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/Err;->r:Ljava/lang/Boolean;

    .line 2173560
    iget-object v0, p0, LX/Err;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2173561
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 2173553
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/Err;->s:Ljava/lang/Boolean;

    .line 2173554
    iget-object v0, p0, LX/Err;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;

    invoke-virtual {v0}, Lcom/facebook/feed/offlinefeed/OfflineFeedDataFacade;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2173555
    invoke-static {p0}, LX/Err;->d$redex0(LX/Err;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2173556
    :cond_0
    monitor-exit p0

    return-void

    .line 2173557
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/DC2;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "IgnoreExecutorServiceSubmitResult"
        }
    .end annotation

    .prologue
    .line 2173543
    monitor-enter p0

    .line 2173544
    :try_start_0
    iget-object v0, p0, LX/Err;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2173545
    iget-object v0, p0, LX/Err;->r:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2173546
    :goto_0
    monitor-exit p0

    return-void

    .line 2173547
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/Err;->s:Ljava/lang/Boolean;

    .line 2173548
    iget-object v0, p0, LX/Err;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2173549
    const/4 v0, 0x0

    iput v0, p0, LX/Err;->v:I

    .line 2173550
    const/4 v0, 0x0

    iput-object v0, p0, LX/Err;->w:Ljava/lang/String;

    .line 2173551
    iget-object v0, p0, LX/Err;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$1;

    invoke-direct {v1, p0}, Lcom/facebook/feed/offlinefeed/OfflineFeedPrefetcherImpl$1;-><init>(LX/Err;)V

    const v2, -0x3bf892c9

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2173552
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
