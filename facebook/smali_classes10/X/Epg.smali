.class public LX/Epg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Epg;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2170426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170427
    return-void
.end method

.method public static a(LX/0QB;)LX/Epg;
    .locals 3

    .prologue
    .line 2170428
    sget-object v0, LX/Epg;->a:LX/Epg;

    if-nez v0, :cond_1

    .line 2170429
    const-class v1, LX/Epg;

    monitor-enter v1

    .line 2170430
    :try_start_0
    sget-object v0, LX/Epg;->a:LX/Epg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2170431
    if-eqz v2, :cond_0

    .line 2170432
    :try_start_1
    new-instance v0, LX/Epg;

    invoke-direct {v0}, LX/Epg;-><init>()V

    .line 2170433
    move-object v0, v0

    .line 2170434
    sput-object v0, LX/Epg;->a:LX/Epg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2170435
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2170436
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2170437
    :cond_1
    sget-object v0, LX/Epg;->a:LX/Epg;

    return-object v0

    .line 2170438
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2170439
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/Epg;Landroid/content/Context;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2170440
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 2170441
    new-instance v1, LX/0ju;

    invoke-direct {v1, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080019

    new-instance v3, LX/Epf;

    invoke-direct {v3, p0, v0}, LX/Epf;-><init>(LX/Epg;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080017

    new-instance v3, LX/Epe;

    invoke-direct {v3, p0, v0}, LX/Epe;-><init>(LX/Epg;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2170442
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2na;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2170443
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 2170444
    new-instance v1, LX/4mb;

    invoke-direct {v1, p1}, LX/4mb;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080f83

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Epd;

    invoke-direct {v3, p0, v0}, LX/Epd;-><init>(LX/Epg;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v1, v2, v3}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    move-result-object v1

    const v2, 0x7f080f81

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Epc;

    invoke-direct {v3, p0, v0}, LX/Epc;-><init>(LX/Epg;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v1, v2, v3}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    move-result-object v1

    const v2, 0x7f080017

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Epb;

    invoke-direct {v3, p0, v0}, LX/Epb;-><init>(LX/Epg;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v1, v2, v3}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    move-result-object v1

    invoke-virtual {v1}, LX/4mb;->show()Landroid/app/AlertDialog;

    .line 2170445
    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2170446
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080fa1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2170447
    invoke-static {p0, p1, v0}, LX/Epg;->c(LX/Epg;Landroid/content/Context;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
