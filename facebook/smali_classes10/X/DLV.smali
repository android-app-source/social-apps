.class public LX/DLV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DLU;


# instance fields
.field private a:LX/0hy;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0hy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1988324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1988325
    iput-object p1, p0, LX/DLV;->a:LX/0hy;

    .line 1988326
    return-void
.end method


# virtual methods
.method public final a()LX/DLU;
    .locals 0

    .prologue
    .line 1988311
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/DLU;
    .locals 0

    .prologue
    .line 1988312
    iput-object p1, p0, LX/DLV;->b:Ljava/lang/String;

    .line 1988313
    return-object p0
.end method

.method public final b()LX/DLU;
    .locals 0

    .prologue
    .line 1988314
    return-object p0
.end method

.method public final c()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1988315
    iget-object v0, p0, LX/DLV;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1988316
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    .line 1988317
    iget-object v1, p0, LX/DLV;->a:LX/0hy;

    iget-object v2, p0, LX/DLV;->b:Ljava/lang/String;

    .line 1988318
    iput-object v2, v0, LX/89k;->c:Ljava/lang/String;

    .line 1988319
    move-object v0, v0

    .line 1988320
    iget-object v2, p0, LX/DLV;->b:Ljava/lang/String;

    .line 1988321
    iput-object v2, v0, LX/89k;->b:Ljava/lang/String;

    .line 1988322
    move-object v0, v0

    .line 1988323
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
