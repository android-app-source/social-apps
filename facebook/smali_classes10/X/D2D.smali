.class public LX/D2D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/D2D;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0Zm;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0Zm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1958355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958356
    iput-object p1, p0, LX/D2D;->a:LX/0Zb;

    .line 1958357
    iput-object p2, p0, LX/D2D;->b:LX/0Zm;

    .line 1958358
    return-void
.end method

.method public static a(LX/0QB;)LX/D2D;
    .locals 5

    .prologue
    .line 1958342
    sget-object v0, LX/D2D;->c:LX/D2D;

    if-nez v0, :cond_1

    .line 1958343
    const-class v1, LX/D2D;

    monitor-enter v1

    .line 1958344
    :try_start_0
    sget-object v0, LX/D2D;->c:LX/D2D;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1958345
    if-eqz v2, :cond_0

    .line 1958346
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1958347
    new-instance p0, LX/D2D;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v4

    check-cast v4, LX/0Zm;

    invoke-direct {p0, v3, v4}, LX/D2D;-><init>(LX/0Zb;LX/0Zm;)V

    .line 1958348
    move-object v0, p0

    .line 1958349
    sput-object v0, LX/D2D;->c:LX/D2D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1958350
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1958351
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1958352
    :cond_1
    sget-object v0, LX/D2D;->c:LX/D2D;

    return-object v0

    .line 1958353
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1958354
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1958327
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "create_profile_video_android"

    .line 1958328
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1958329
    move-object v0, v0

    .line 1958330
    const-string v1, "session_id"

    invoke-virtual {v0, v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/D2D;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 1958337
    iget-object v0, p0, LX/D2D;->b:LX/0Zm;

    .line 1958338
    iget-object v1, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1958339
    invoke-virtual {v0, v1}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1958340
    :goto_0
    return-void

    .line 1958341
    :cond_0
    iget-object v0, p0, LX/D2D;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/D2C;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1958335
    invoke-virtual {p1}, LX/D2C;->getEventName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, LX/D2D;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/D2D;->a(LX/D2D;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1958336
    return-void
.end method

.method public final b(LX/D2C;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1958331
    const/4 v0, 0x0

    invoke-virtual {p1}, LX/D2C;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/D2D;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1958332
    const-string v1, "profile_video_cta_video_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1958333
    invoke-static {p0, v0}, LX/D2D;->a(LX/D2D;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1958334
    return-void
.end method
