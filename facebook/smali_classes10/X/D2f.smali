.class public final LX/D2f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/3rL",
        "<",
        "Lcom/facebook/photos/upload/operation/UploadOperation;",
        "LX/D2S;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/D2h;


# direct methods
.method public constructor <init>(LX/D2h;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1958980
    iput-object p1, p0, LX/D2f;->b:LX/D2h;

    iput-object p2, p0, LX/D2f;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1958997
    const-class v0, LX/D2h;

    const-string v1, "Failed to upload profile video"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1958998
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1958981
    check-cast p1, LX/3rL;

    .line 1958982
    if-nez p1, :cond_0

    .line 1958983
    :goto_0
    return-void

    .line 1958984
    :cond_0
    iget-object v0, p0, LX/D2f;->b:LX/D2h;

    iget-object v1, v0, LX/D2h;->c:LX/1EZ;

    iget-object v0, p1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v1, v0}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1958985
    iget-object v0, p0, LX/D2f;->b:LX/D2h;

    iget-object v0, v0, LX/D2h;->f:LX/D2c;

    iget-object v1, p0, LX/D2f;->a:Ljava/lang/String;

    .line 1958986
    new-instance v2, LX/D2b;

    invoke-static {v0}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v4

    check-cast v4, LX/0b3;

    invoke-static {v0}, LX/D2T;->a(LX/0QB;)LX/D2T;

    move-result-object v5

    check-cast v5, LX/D2T;

    invoke-static {v0}, LX/BQP;->a(LX/0QB;)LX/BQP;

    move-result-object v6

    check-cast v6, LX/BQP;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v7

    check-cast v7, Landroid/os/Handler;

    const-class v3, LX/D2X;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/D2X;

    move-object v3, v1

    invoke-direct/range {v2 .. v8}, LX/D2b;-><init>(Ljava/lang/String;LX/0b3;LX/D2T;LX/BQP;Landroid/os/Handler;LX/D2X;)V

    .line 1958987
    move-object v0, v2

    .line 1958988
    iget-object v2, v0, LX/D2b;->b:LX/0b3;

    iget-object v3, v0, LX/D2b;->h:LX/D2a;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 1958989
    iget-object v2, v0, LX/D2b;->b:LX/0b3;

    iget-object v3, v0, LX/D2b;->i:LX/D2Z;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 1958990
    iget-object v2, v0, LX/D2b;->e:Landroid/os/Handler;

    iget-object v3, v0, LX/D2b;->g:Ljava/lang/Runnable;

    const-wide/32 v4, 0x5265c00

    const v6, -0x3cd6b7b8

    invoke-static {v2, v3, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1958991
    iget-object v0, p0, LX/D2f;->b:LX/D2h;

    iget-object v1, v0, LX/D2h;->g:LX/D2T;

    iget-object v0, p1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, LX/D2S;

    .line 1958992
    invoke-static {v1}, LX/D2T;->f(LX/D2T;)V

    .line 1958993
    invoke-static {v1}, LX/D2T;->c(LX/D2T;)V

    .line 1958994
    iput-object v0, v1, LX/D2T;->g:LX/D2S;

    .line 1958995
    invoke-static {v1}, LX/D2T;->g(LX/D2T;)V

    .line 1958996
    iget-object v0, p0, LX/D2f;->b:LX/D2h;

    iget-object v0, v0, LX/D2h;->l:LX/BQP;

    invoke-virtual {v0}, LX/BQP;->e()V

    goto :goto_0
.end method
