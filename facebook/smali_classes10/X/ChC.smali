.class public abstract LX/ChC;
.super LX/Ch6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Ch6",
        "<",
        "LX/ChB;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1927809
    invoke-direct {p0}, LX/Ch6;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/ChB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1927808
    const-class v0, LX/ChB;

    return-object v0
.end method

.method public abstract a(Ljava/lang/String;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;)V
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1927803
    check-cast p1, LX/ChB;

    .line 1927804
    iget v0, p1, LX/ChB;->c:I

    packed-switch v0, :pswitch_data_0

    .line 1927805
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No event type matches: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, LX/ChB;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1927806
    :pswitch_0
    iget-object v0, p1, LX/ChB;->a:Ljava/lang/String;

    iget-object v1, p1, LX/ChB;->b:Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;

    invoke-virtual {p0, v0, v1}, LX/ChC;->a(Ljava/lang/String;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$PlaceToReviewModel;)V

    .line 1927807
    :pswitch_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
