.class public LX/E9x;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/17W;

.field public final b:LX/1nI;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reviews/handler/DeleteReviewHandler;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/E9e;

.field private final f:LX/BNP;

.field public final g:LX/79D;


# direct methods
.method public constructor <init>(LX/17W;LX/1nI;LX/0Ot;LX/0Or;LX/E9e;LX/BNP;LX/79D;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17W;",
            "LX/1nI;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reviews/handler/DeleteReviewHandler;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/E9e;",
            "LX/BNP;",
            "LX/79D;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2084989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2084990
    iput-object p3, p0, LX/E9x;->c:LX/0Ot;

    .line 2084991
    iput-object p1, p0, LX/E9x;->a:LX/17W;

    .line 2084992
    iput-object p2, p0, LX/E9x;->b:LX/1nI;

    .line 2084993
    iput-object p4, p0, LX/E9x;->d:LX/0Or;

    .line 2084994
    iput-object p5, p0, LX/E9x;->e:LX/E9e;

    .line 2084995
    iput-object p6, p0, LX/E9x;->f:LX/BNP;

    .line 2084996
    iput-object p7, p0, LX/E9x;->g:LX/79D;

    .line 2084997
    return-void
.end method

.method public static a(LX/E9x;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    .line 2084988
    new-instance v0, LX/E9r;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/E9r;-><init>(LX/E9x;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(LX/E9x;LX/5OM;Landroid/content/Context;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    .line 2084984
    invoke-virtual {p1}, LX/5OM;->c()LX/5OG;

    move-result-object v0

    const v1, 0x7f08001c

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v7

    .line 2084985
    invoke-static {p3}, LX/BNJ;->b(LX/5ti;)Ljava/lang/String;

    move-result-object v4

    .line 2084986
    new-instance v0, LX/E9v;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p4

    move-object v5, p2

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/E9v;-><init>(LX/E9x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2084987
    return-void
.end method

.method private static a(LX/E9x;LX/5OM;Landroid/content/Context;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    .line 2084981
    invoke-virtual {p1}, LX/5OM;->c()LX/5OG;

    move-result-object v0

    const v1, 0x7f0814e7

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v8

    .line 2084982
    new-instance v0, LX/E9u;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, LX/E9u;-><init>(LX/E9x;Landroid/content/Context;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2084983
    return-void
.end method

.method public static a(LX/E9x;Landroid/view/View;LX/5tj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2084979
    new-instance v0, LX/E9s;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/E9s;-><init>(LX/E9x;LX/5tj;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084980
    return-void
.end method

.method public static a$redex0(LX/E9x;Landroid/content/Context;LX/5tj;)V
    .locals 2

    .prologue
    .line 2084974
    invoke-static {p2}, LX/BNJ;->b(LX/5ti;)Ljava/lang/String;

    move-result-object v0

    .line 2084975
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2084976
    :goto_0
    return-void

    .line 2084977
    :cond_0
    sget-object v1, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2084978
    iget-object v1, p0, LX/E9x;->a:LX/17W;

    invoke-virtual {v1, p1, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static a$redex0(LX/E9x;Landroid/content/Context;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    .line 2084971
    move-object/from16 v0, p0

    iget-object v2, v0, LX/E9x;->g:LX/79D;

    invoke-static/range {p2 .. p2}, LX/BNJ;->b(LX/5ti;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/79C;->REVIEW_EDIT_MENU_OPTION:LX/79C;

    move-object/from16 v0, p6

    move-object/from16 v1, p3

    invoke-virtual {v2, v0, v1, v3, v4}, LX/79D;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/79C;)V

    .line 2084972
    move-object/from16 v0, p0

    iget-object v2, v0, LX/E9x;->f:LX/BNP;

    const/16 v3, 0x6df

    move-object/from16 v4, p1

    check-cast v4, Landroid/app/Activity;

    sget-object v5, LX/21D;->USER_REVIEWS_LIST:LX/21D;

    const-string v6, "review_row"

    const-string v8, "edit_menu"

    invoke-static/range {p2 .. p2}, LX/BNJ;->a(LX/55r;)I

    move-result v9

    invoke-static/range {p3 .. p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static/range {p2 .. p2}, LX/BNJ;->b(LX/55r;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p2 .. p2}, LX/BNJ;->c(LX/55r;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v7, p5

    move-object/from16 v12, p4

    invoke-virtual/range {v2 .. v15}, LX/BNP;->a(ILandroid/app/Activity;LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;)V

    .line 2084973
    return-void
.end method

.method public static b(LX/E9x;Landroid/view/View;LX/5tj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2084965
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2084966
    new-instance v6, LX/6WS;

    invoke-direct {v6, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2084967
    invoke-virtual {v6}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    const v2, 0x7f0814f6

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v7

    .line 2084968
    new-instance v0, LX/E9t;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/E9t;-><init>(LX/E9x;Landroid/view/View;LX/5tj;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2084969
    invoke-virtual {v6, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2084970
    return-void
.end method

.method public static b(LX/E9x;Landroid/view/View;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    .line 2084959
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2084960
    new-instance v1, LX/6WS;

    invoke-direct {v1, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    .line 2084961
    invoke-static/range {v0 .. v7}, LX/E9x;->a(LX/E9x;LX/5OM;Landroid/content/Context;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p6

    .line 2084962
    invoke-static/range {v0 .. v6}, LX/E9x;->a(LX/E9x;LX/5OM;Landroid/content/Context;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2084963
    invoke-virtual {v1, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2084964
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reviews/ui/ReviewFeedRowView;LX/5tj;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/4lK;LX/4lJ;)V
    .locals 9
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p8    # LX/4lK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2084946
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-virtual/range {v0 .. v8}, LX/E9x;->a(Lcom/facebook/reviews/ui/ReviewFeedRowView;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/4lK;LX/4lJ;)V

    .line 2084947
    invoke-static {p1, p3}, LX/E9e;->a(Lcom/facebook/reviews/ui/ReviewFeedRowView;Ljava/lang/CharSequence;)V

    .line 2084948
    return-void
.end method

.method public final a(Lcom/facebook/reviews/ui/ReviewFeedRowView;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/4lK;LX/4lJ;)V
    .locals 7
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p7    # LX/4lK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2084949
    iget-object v0, p0, LX/E9x;->e:LX/E9e;

    invoke-virtual {v0, p1, p2, p6}, LX/E9e;->a(Lcom/facebook/reviews/ui/ReviewFeedRowView;LX/5tj;Ljava/lang/String;)V

    .line 2084950
    new-instance v1, LX/E9p;

    invoke-direct {v1, p0, p6, p3, p2}, LX/E9p;-><init>(LX/E9x;Ljava/lang/String;Ljava/lang/String;LX/5tj;)V

    move-object v1, v1

    .line 2084951
    invoke-virtual {p1, v1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setProfilePicOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084952
    new-instance v1, LX/E9q;

    invoke-direct {v1, p0, p6, p3, p2}, LX/E9q;-><init>(LX/E9x;Ljava/lang/String;Ljava/lang/String;LX/5tj;)V

    move-object v1, v1

    .line 2084953
    invoke-virtual {p1, v1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setTitleOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 2084954
    invoke-static/range {v1 .. v6}, LX/E9x;->a(LX/E9x;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setSecondaryActionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084955
    invoke-virtual {p1, p8}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setReviewTextExpandedState(LX/4lJ;)V

    .line 2084956
    invoke-virtual {p1, p7}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setReviewTextOnExpandStateChangeListener(LX/4lK;)V

    .line 2084957
    invoke-static {p0, p1, p2, p3, p6}, LX/E9x;->a(LX/E9x;Landroid/view/View;LX/5tj;Ljava/lang/String;Ljava/lang/String;)V

    .line 2084958
    return-void
.end method
