.class public final LX/Dve;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2059097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2059098
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;

    invoke-direct {v0, p1}, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2059099
    new-array v0, p1, [Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;

    return-object v0
.end method
