.class public LX/Exz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hL;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z

.field public final c:I

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:F

.field public final h:Z

.field public final i:I

.field public final j:I


# direct methods
.method public constructor <init>(LX/0ad;Landroid/content/res/Resources;)V
    .locals 5
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 2185676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2185677
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2185678
    iput-object v0, p0, LX/Exz;->a:LX/0Ot;

    .line 2185679
    sget-short v0, LX/2hr;->l:S

    invoke-interface {p1, v0, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Exz;->b:Z

    .line 2185680
    sget v0, LX/2hr;->t:I

    invoke-interface {p1, v0, v4}, LX/0ad;->a(II)I

    move-result v1

    .line 2185681
    if-ne v1, v4, :cond_0

    const v0, 0x7f0b009b

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    iput v0, p0, LX/Exz;->c:I

    .line 2185682
    sget-short v0, LX/2hr;->p:S

    invoke-interface {p1, v0, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Exz;->d:Z

    .line 2185683
    sget-short v0, LX/2hr;->n:S

    invoke-interface {p1, v0, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Exz;->e:Z

    .line 2185684
    sget-short v0, LX/2hr;->q:S

    invoke-interface {p1, v0, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Exz;->f:Z

    .line 2185685
    sget v0, LX/2hr;->r:F

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {p1, v0, v2}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/Exz;->g:F

    .line 2185686
    sget-short v0, LX/2hr;->s:S

    invoke-interface {p1, v0, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Exz;->h:Z

    .line 2185687
    sget v0, LX/2hr;->m:I

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p2, v0}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    iput v0, p0, LX/Exz;->i:I

    .line 2185688
    iget-boolean v0, p0, LX/Exz;->h:Z

    if-eqz v0, :cond_1

    .line 2185689
    const v0, 0x7f0b0f86

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Exz;->j:I

    .line 2185690
    :goto_1
    return-void

    .line 2185691
    :cond_0
    int-to-float v0, v1

    invoke-static {p2, v0}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    goto :goto_0

    .line 2185692
    :cond_1
    iget-boolean v0, p0, LX/Exz;->f:Z

    if-eqz v0, :cond_2

    .line 2185693
    const v0, 0x7f0b0f85

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Exz;->j:I

    goto :goto_1

    .line 2185694
    :cond_2
    if-ltz v1, :cond_3

    .line 2185695
    iget v0, p0, LX/Exz;->c:I

    iput v0, p0, LX/Exz;->j:I

    goto :goto_1

    .line 2185696
    :cond_3
    iput v4, p0, LX/Exz;->j:I

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/Exz;
    .locals 5

    .prologue
    .line 2185697
    const-class v1, LX/Exz;

    monitor-enter v1

    .line 2185698
    :try_start_0
    sget-object v0, LX/Exz;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2185699
    sput-object v2, LX/Exz;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2185700
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2185701
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2185702
    new-instance p0, LX/Exz;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, LX/Exz;-><init>(LX/0ad;Landroid/content/res/Resources;)V

    .line 2185703
    const/16 v3, 0x298

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 2185704
    iput-object v3, p0, LX/Exz;->a:LX/0Ot;

    .line 2185705
    move-object v0, p0

    .line 2185706
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2185707
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Exz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2185708
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2185709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/Exz;Landroid/view/View;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2185710
    new-instance v0, Lcom/facebook/friending/common/list/FriendRequestCardView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestCardView;-><init>(Landroid/content/Context;)V

    .line 2185711
    iget v1, p0, LX/Exz;->g:F

    .line 2185712
    iget-object p0, v0, Lcom/facebook/friending/common/list/FriendRequestCardView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2185713
    return-object v0
.end method

.method public static f(LX/Exz;Landroid/view/View;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2185714
    new-instance v0, LX/EyU;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EyU;-><init>(Landroid/content/Context;)V

    .line 2185715
    iget v1, p0, LX/Exz;->c:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 2185716
    iget-boolean v1, p0, LX/Exz;->d:Z

    const v2, 0x7f0b0060

    const/4 p1, 0x0

    .line 2185717
    invoke-virtual {v0}, LX/EyU;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2185718
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 2185719
    if-eqz v1, :cond_0

    .line 2185720
    invoke-virtual {v0, p1, p1, p1, p1}, LX/EyU;->setPadding(IIII)V

    .line 2185721
    iget-object p0, v0, LX/EyU;->j:Landroid/view/View;

    invoke-virtual {p0, p1, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 2185722
    :goto_0
    return-object v0

    .line 2185723
    :cond_0
    invoke-virtual {v0, v3, v3, v3, v3}, LX/EyU;->setPadding(IIII)V

    .line 2185724
    iget-object v3, v0, LX/EyU;->j:Landroid/view/View;

    invoke-virtual {v3, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method
