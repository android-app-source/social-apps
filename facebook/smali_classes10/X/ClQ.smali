.class public final enum LX/ClQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ClQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ClQ;

.field public static final enum CENTER:LX/ClQ;

.field public static final enum LEFT:LX/ClQ;

.field public static final enum RIGHT:LX/ClQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1932433
    new-instance v0, LX/ClQ;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LX/ClQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClQ;->LEFT:LX/ClQ;

    .line 1932434
    new-instance v0, LX/ClQ;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v3}, LX/ClQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClQ;->CENTER:LX/ClQ;

    .line 1932435
    new-instance v0, LX/ClQ;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, LX/ClQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ClQ;->RIGHT:LX/ClQ;

    .line 1932436
    const/4 v0, 0x3

    new-array v0, v0, [LX/ClQ;

    sget-object v1, LX/ClQ;->LEFT:LX/ClQ;

    aput-object v1, v0, v2

    sget-object v1, LX/ClQ;->CENTER:LX/ClQ;

    aput-object v1, v0, v3

    sget-object v1, LX/ClQ;->RIGHT:LX/ClQ;

    aput-object v1, v0, v4

    sput-object v0, LX/ClQ;->$VALUES:[LX/ClQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1932437
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static from(Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;)LX/ClQ;
    .locals 2

    .prologue
    .line 1932438
    if-eqz p0, :cond_0

    .line 1932439
    sget-object v0, LX/ClP;->c:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1932440
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1932441
    :pswitch_0
    sget-object v0, LX/ClQ;->LEFT:LX/ClQ;

    goto :goto_0

    .line 1932442
    :pswitch_1
    sget-object v0, LX/ClQ;->CENTER:LX/ClQ;

    goto :goto_0

    .line 1932443
    :pswitch_2
    sget-object v0, LX/ClQ;->RIGHT:LX/ClQ;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static from(Lcom/facebook/graphql/enums/GraphQLTextAnnotationHorizontalPosition;)LX/ClQ;
    .locals 2

    .prologue
    .line 1932444
    if-eqz p0, :cond_0

    .line 1932445
    sget-object v0, LX/ClP;->b:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLTextAnnotationHorizontalPosition;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1932446
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1932447
    :pswitch_0
    sget-object v0, LX/ClQ;->LEFT:LX/ClQ;

    goto :goto_0

    .line 1932448
    :pswitch_1
    sget-object v0, LX/ClQ;->CENTER:LX/ClQ;

    goto :goto_0

    .line 1932449
    :pswitch_2
    sget-object v0, LX/ClQ;->RIGHT:LX/ClQ;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/ClQ;
    .locals 1

    .prologue
    .line 1932450
    const-class v0, LX/ClQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ClQ;

    return-object v0
.end method

.method public static values()[LX/ClQ;
    .locals 1

    .prologue
    .line 1932451
    sget-object v0, LX/ClQ;->$VALUES:[LX/ClQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ClQ;

    return-object v0
.end method
