.class public final LX/EY1;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EY0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EY1;",
        ">;",
        "LX/EY0;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2135960
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2135961
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY1;->b:Ljava/util/List;

    .line 2135962
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY1;->c:Ljava/util/List;

    .line 2135963
    const-string v0, ""

    iput-object v0, p0, LX/EY1;->d:Ljava/lang/Object;

    .line 2135964
    const-string v0, ""

    iput-object v0, p0, LX/EY1;->e:Ljava/lang/Object;

    .line 2135965
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2135966
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2135967
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY1;->b:Ljava/util/List;

    .line 2135968
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EY1;->c:Ljava/util/List;

    .line 2135969
    const-string v0, ""

    iput-object v0, p0, LX/EY1;->d:Ljava/lang/Object;

    .line 2135970
    const-string v0, ""

    iput-object v0, p0, LX/EY1;->e:Ljava/lang/Object;

    .line 2135971
    return-void
.end method

.method private d(LX/EWY;)LX/EY1;
    .locals 1

    .prologue
    .line 2135972
    instance-of v0, p1, LX/EY2;

    if-eqz v0, :cond_0

    .line 2135973
    check-cast p1, LX/EY2;

    invoke-virtual {p0, p1}, LX/EY1;->a(LX/EY2;)LX/EY1;

    move-result-object p0

    .line 2135974
    :goto_0
    return-object p0

    .line 2135975
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EY1;
    .locals 4

    .prologue
    .line 2135976
    const/4 v2, 0x0

    .line 2135977
    :try_start_0
    sget-object v0, LX/EY2;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EY2;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2135978
    if-eqz v0, :cond_0

    .line 2135979
    invoke-virtual {p0, v0}, LX/EY1;->a(LX/EY2;)LX/EY1;

    .line 2135980
    :cond_0
    return-object p0

    .line 2135981
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2135982
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2135983
    check-cast v0, LX/EY2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2135984
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2135985
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2135986
    invoke-virtual {p0, v1}, LX/EY1;->a(LX/EY2;)LX/EY1;

    :cond_1
    throw v0

    .line 2135987
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static m()LX/EY1;
    .locals 1

    .prologue
    .line 2135988
    new-instance v0, LX/EY1;

    invoke-direct {v0}, LX/EY1;-><init>()V

    return-object v0
.end method

.method private n()LX/EY1;
    .locals 2

    .prologue
    .line 2135989
    invoke-static {}, LX/EY1;->m()LX/EY1;

    move-result-object v0

    invoke-direct {p0}, LX/EY1;->x()LX/EY2;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EY1;->a(LX/EY2;)LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method private w()LX/EY2;
    .locals 2

    .prologue
    .line 2135990
    invoke-direct {p0}, LX/EY1;->x()LX/EY2;

    move-result-object v0

    .line 2135991
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2135992
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2135993
    :cond_0
    return-object v0
.end method

.method private x()LX/EY2;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2135994
    new-instance v2, LX/EY2;

    invoke-direct {v2, p0}, LX/EY2;-><init>(LX/EWj;)V

    .line 2135995
    iget v3, p0, LX/EY1;->a:I

    .line 2135996
    iget v4, p0, LX/EY1;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    .line 2135997
    iget-object v4, p0, LX/EY1;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, LX/EY1;->b:Ljava/util/List;

    .line 2135998
    iget v4, p0, LX/EY1;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, LX/EY1;->a:I

    .line 2135999
    :cond_0
    iget-object v4, p0, LX/EY1;->b:Ljava/util/List;

    .line 2136000
    iput-object v4, v2, LX/EY2;->path_:Ljava/util/List;

    .line 2136001
    iget v4, p0, LX/EY1;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 2136002
    iget-object v4, p0, LX/EY1;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, LX/EY1;->c:Ljava/util/List;

    .line 2136003
    iget v4, p0, LX/EY1;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, LX/EY1;->a:I

    .line 2136004
    :cond_1
    iget-object v4, p0, LX/EY1;->c:Ljava/util/List;

    .line 2136005
    iput-object v4, v2, LX/EY2;->span_:Ljava/util/List;

    .line 2136006
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    .line 2136007
    :goto_0
    iget-object v1, p0, LX/EY1;->d:Ljava/lang/Object;

    .line 2136008
    iput-object v1, v2, LX/EY2;->leadingComments_:Ljava/lang/Object;

    .line 2136009
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 2136010
    or-int/lit8 v0, v0, 0x2

    .line 2136011
    :cond_2
    iget-object v1, p0, LX/EY1;->e:Ljava/lang/Object;

    .line 2136012
    iput-object v1, v2, LX/EY2;->trailingComments_:Ljava/lang/Object;

    .line 2136013
    iput v0, v2, LX/EY2;->bitField0_:I

    .line 2136014
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2136015
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2136016
    invoke-direct {p0, p1}, LX/EY1;->d(LX/EWY;)LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2136017
    invoke-direct {p0, p1, p2}, LX/EY1;->d(LX/EWd;LX/EYZ;)LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EY2;)LX/EY1;
    .locals 2

    .prologue
    .line 2136018
    sget-object v0, LX/EY2;->c:LX/EY2;

    move-object v0, v0

    .line 2136019
    if-ne p1, v0, :cond_0

    .line 2136020
    :goto_0
    return-object p0

    .line 2136021
    :cond_0
    iget-object v0, p1, LX/EY2;->path_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2136022
    iget-object v0, p0, LX/EY1;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2136023
    iget-object v0, p1, LX/EY2;->path_:Ljava/util/List;

    iput-object v0, p0, LX/EY1;->b:Ljava/util/List;

    .line 2136024
    iget v0, p0, LX/EY1;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, LX/EY1;->a:I

    .line 2136025
    :goto_1
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136026
    :cond_1
    iget-object v0, p1, LX/EY2;->span_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2136027
    iget-object v0, p0, LX/EY1;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2136028
    iget-object v0, p1, LX/EY2;->span_:Ljava/util/List;

    iput-object v0, p0, LX/EY1;->c:Ljava/util/List;

    .line 2136029
    iget v0, p0, LX/EY1;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, LX/EY1;->a:I

    .line 2136030
    :goto_2
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136031
    :cond_2
    const/4 v0, 0x1

    .line 2136032
    iget v1, p1, LX/EY2;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_9

    :goto_3
    move v0, v0

    .line 2136033
    if-eqz v0, :cond_3

    .line 2136034
    iget v0, p0, LX/EY1;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EY1;->a:I

    .line 2136035
    iget-object v0, p1, LX/EY2;->leadingComments_:Ljava/lang/Object;

    iput-object v0, p0, LX/EY1;->d:Ljava/lang/Object;

    .line 2136036
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136037
    :cond_3
    iget v0, p1, LX/EY2;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2136038
    if-eqz v0, :cond_4

    .line 2136039
    iget v0, p0, LX/EY1;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EY1;->a:I

    .line 2136040
    iget-object v0, p1, LX/EY2;->trailingComments_:Ljava/lang/Object;

    iput-object v0, p0, LX/EY1;->e:Ljava/lang/Object;

    .line 2136041
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2136042
    :cond_4
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    .line 2136043
    :cond_5
    iget v0, p0, LX/EY1;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    .line 2136044
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EY1;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EY1;->b:Ljava/util/List;

    .line 2136045
    iget v0, p0, LX/EY1;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EY1;->a:I

    .line 2136046
    :cond_6
    iget-object v0, p0, LX/EY1;->b:Ljava/util/List;

    iget-object v1, p1, LX/EY2;->path_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2136047
    :cond_7
    iget v0, p0, LX/EY1;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    .line 2136048
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EY1;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EY1;->c:Ljava/util/List;

    .line 2136049
    iget v0, p0, LX/EY1;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EY1;->a:I

    .line 2136050
    :cond_8
    iget-object v0, p0, LX/EY1;->c:Ljava/util/List;

    iget-object v1, p1, LX/EY2;->span_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    :cond_a
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2135959
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2136051
    invoke-direct {p0, p1, p2}, LX/EY1;->d(LX/EWd;LX/EYZ;)LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2135945
    invoke-direct {p0}, LX/EY1;->n()LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2135947
    invoke-direct {p0, p1, p2}, LX/EY1;->d(LX/EWd;LX/EYZ;)LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2135946
    invoke-direct {p0}, LX/EY1;->n()LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2135948
    invoke-direct {p0, p1}, LX/EY1;->d(LX/EWY;)LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2135949
    invoke-direct {p0}, LX/EY1;->n()LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2135950
    sget-object v0, LX/EYC;->N:LX/EYn;

    const-class v1, LX/EY2;

    const-class v2, LX/EY1;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2135951
    sget-object v0, LX/EYC;->M:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2135952
    invoke-direct {p0}, LX/EY1;->n()LX/EY1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2135953
    invoke-direct {p0}, LX/EY1;->x()LX/EY2;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2135954
    invoke-direct {p0}, LX/EY1;->w()LX/EY2;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2135955
    invoke-direct {p0}, LX/EY1;->x()LX/EY2;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2135956
    invoke-direct {p0}, LX/EY1;->w()LX/EY2;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2135957
    sget-object v0, LX/EY2;->c:LX/EY2;

    move-object v0, v0

    .line 2135958
    return-object v0
.end method
