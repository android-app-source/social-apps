.class public final enum LX/Cqc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cqc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cqc;

.field public static final enum ANNOTATION_DEFAULT:LX/Cqc;

.field public static final enum ANNOTATION_OVERLAY:LX/Cqc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1940081
    new-instance v0, LX/Cqc;

    const-string v1, "ANNOTATION_DEFAULT"

    invoke-direct {v0, v1, v2}, LX/Cqc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    .line 1940082
    new-instance v0, LX/Cqc;

    const-string v1, "ANNOTATION_OVERLAY"

    invoke-direct {v0, v1, v3}, LX/Cqc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cqc;->ANNOTATION_OVERLAY:LX/Cqc;

    .line 1940083
    const/4 v0, 0x2

    new-array v0, v0, [LX/Cqc;

    sget-object v1, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    aput-object v1, v0, v2

    sget-object v1, LX/Cqc;->ANNOTATION_OVERLAY:LX/Cqc;

    aput-object v1, v0, v3

    sput-object v0, LX/Cqc;->$VALUES:[LX/Cqc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1940084
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cqc;
    .locals 1

    .prologue
    .line 1940085
    const-class v0, LX/Cqc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cqc;

    return-object v0
.end method

.method public static values()[LX/Cqc;
    .locals 1

    .prologue
    .line 1940086
    sget-object v0, LX/Cqc;->$VALUES:[LX/Cqc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cqc;

    return-object v0
.end method
