.class public final LX/ENv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/ENw;


# direct methods
.method public constructor <init>(LX/ENw;I)V
    .locals 0

    .prologue
    .line 2113685
    iput-object p1, p0, LX/ENv;->b:LX/ENw;

    iput p2, p0, LX/ENv;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 12

    .prologue
    .line 2113686
    iget-object v0, p0, LX/ENv;->b:LX/ENw;

    iget-object v0, v0, LX/ENw;->a:LX/1De;

    iget v1, p0, LX/ENv;->a:I

    .line 2113687
    iget-object v2, v0, LX/1De;->g:LX/1X1;

    move-object v2, v2

    .line 2113688
    if-nez v2, :cond_0

    .line 2113689
    :goto_0
    iget-object v0, p0, LX/ENv;->b:LX/ENw;

    iget-object v0, v0, LX/ENw;->b:LX/0Px;

    iget v1, p0, LX/ENv;->a:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2113690
    iget-object v0, p0, LX/ENv;->b:LX/ENw;

    iget-object v0, v0, LX/ENw;->e:LX/ENx;

    iget-object v0, v0, LX/ENx;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-object v1, p0, LX/ENv;->b:LX/ENw;

    iget-object v1, v1, LX/ENw;->c:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->CLICK:LX/8ch;

    iget-object v3, p0, LX/ENv;->b:LX/ENw;

    iget-object v3, v3, LX/ENw;->c:LX/1Ps;

    check-cast v3, LX/CxP;

    iget-object v4, p0, LX/ENv;->b:LX/ENw;

    iget-object v4, v4, LX/ENw;->d:LX/CzL;

    invoke-interface {v3, v4}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, LX/ENv;->b:LX/ENw;

    iget-object v4, v4, LX/ENw;->d:LX/CzL;

    iget-object v6, p0, LX/ENv;->b:LX/ENw;

    iget-object v6, v6, LX/ENw;->c:LX/1Ps;

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v7

    iget-object v6, p0, LX/ENv;->b:LX/ENw;

    iget-object v6, v6, LX/ENw;->c:LX/1Ps;

    check-cast v6, LX/CxP;

    iget-object v8, p0, LX/ENv;->b:LX/ENw;

    iget-object v8, v8, LX/ENw;->d:LX/CzL;

    invoke-interface {v6, v8}, LX/CxP;->b(LX/CzL;)I

    move-result v6

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aP()LX/8dH;

    move-result-object v8

    invoke-interface {v8}, LX/8dH;->j()LX/8dG;

    move-result-object v8

    invoke-interface {v8}, LX/8dG;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aP()LX/8dH;

    move-result-object v5

    invoke-interface {v5}, LX/8dH;->c()Ljava/lang/String;

    move-result-object v5

    iget v9, p0, LX/ENv;->a:I

    .line 2113691
    sget-object v10, LX/CvJ;->ITEM_TAPPED:LX/CvJ;

    invoke-static {v10, v7}, LX/CvY;->a(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "tapped_result_position"

    invoke-virtual {v10, v11, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "results_module_type"

    const-string p0, "subtopics"

    invoke-virtual {v10, v11, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "query"

    invoke-virtual {v10, v11, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "query_function"

    invoke-virtual {v10, v11, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "page_number"

    invoke-virtual {v10, v11, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    move-object v5, v10

    .line 2113692
    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2113693
    return-void

    .line 2113694
    :cond_0
    check-cast v2, LX/ENs;

    .line 2113695
    new-instance v3, LX/ENt;

    iget-object v4, v2, LX/ENs;->e:LX/ENu;

    invoke-direct {v3, v4, v1}, LX/ENt;-><init>(LX/ENu;I)V

    move-object v2, v3

    .line 2113696
    invoke-virtual {v0, v2}, LX/1De;->a(LX/48B;)V

    goto/16 :goto_0
.end method
