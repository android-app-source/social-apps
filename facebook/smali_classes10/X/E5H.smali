.class public LX/E5H;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2078168
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/E5H;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078169
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2078170
    iput-object p1, p0, LX/E5H;->b:LX/0Ot;

    .line 2078171
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2078172
    check-cast p2, LX/E5G;

    .line 2078173
    iget-object v0, p0, LX/E5H;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;

    iget-object v2, p2, LX/E5G;->a:Ljava/util/List;

    iget-object v3, p2, LX/E5G;->b:Ljava/lang/String;

    iget-object v4, p2, LX/E5G;->c:Ljava/lang/String;

    iget-object v5, p2, LX/E5G;->d:Landroid/net/Uri;

    iget v6, p2, LX/E5G;->e:I

    iget-object v7, p2, LX/E5G;->f:Landroid/net/Uri;

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/reaction/feed/unitcomponents/spec/body/ReactionPlacePhotoComponentSpec;->a(LX/1De;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;ILandroid/net/Uri;)LX/1Dg;

    move-result-object v0

    .line 2078174
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2078175
    invoke-static {}, LX/1dS;->b()V

    .line 2078176
    const/4 v0, 0x0

    return-object v0
.end method
