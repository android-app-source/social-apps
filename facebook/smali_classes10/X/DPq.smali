.class public final LX/DPq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DPr;


# direct methods
.method public constructor <init>(LX/DPr;)V
    .locals 0

    .prologue
    .line 1993905
    iput-object p1, p0, LX/DPq;->a:LX/DPr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5e4a0c09

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1993906
    iget-object v1, p0, LX/DPq;->a:LX/DPr;

    iget-object v1, v1, LX/DPr;->a:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v1, v1, Lcom/facebook/groups/info/GroupInfoAdapter;->k:LX/DPo;

    iget-object v2, p0, LX/DPq;->a:LX/DPr;

    iget-object v2, v2, LX/DPr;->a:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v2, v2, Lcom/facebook/groups/info/GroupInfoAdapter;->l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1993907
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v5

    if-nez v5, :cond_1

    .line 1993908
    :cond_0
    :goto_0
    const v1, 0x18c58745

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1993909
    :cond_1
    iget-object v5, v1, LX/DPo;->c:LX/DQW;

    .line 1993910
    invoke-static {v5}, LX/DQW;->a(LX/DQW;)Landroid/content/Intent;

    move-result-object v6

    .line 1993911
    const-string p0, "target_fragment"

    sget-object p1, LX/0cQ;->GROUPS_FOR_SALE_POSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {v6, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993912
    const-string p0, "group_feed_model"

    invoke-static {v6, p0, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1993913
    const-string p0, "group_feed_id"

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993914
    move-object v5, v6

    .line 1993915
    iget-object v6, v1, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v6, v5, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
