.class public LX/DTt;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/11S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/groups/multicompany/bridge/MultiCompanyGroupIconProvider;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/5QP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/groups/widget/remotepogview/RemotePogView;

.field public k:Lcom/facebook/widget/text/BetterTextView;

.field public l:Lcom/facebook/widget/text/BetterTextView;

.field public m:Landroid/view/View;

.field public n:LX/2qr;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:LX/DRg;

.field public r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/DRg;Z)V
    .locals 9

    .prologue
    .line 2000835
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2000836
    iput-object p2, p0, LX/DTt;->q:LX/DRg;

    .line 2000837
    iput-boolean p3, p0, LX/DTt;->r:Z

    .line 2000838
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    move-object v2, p0

    check-cast v2, LX/DTt;

    invoke-static {p3}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-static {p3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {p3}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v5

    check-cast v5, LX/11S;

    invoke-static {p3}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {p3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    const/16 v8, 0x13b2

    invoke-static {p3, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p3}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-static {p3}, LX/5QP;->a(LX/0QB;)LX/5QP;

    move-result-object p2

    check-cast p2, LX/5QP;

    const/16 v0, 0xb2d

    invoke-static {p3, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p3

    iput-object v3, v2, LX/DTt;->a:LX/0W9;

    iput-object v4, v2, LX/DTt;->b:Landroid/content/res/Resources;

    iput-object v5, v2, LX/DTt;->c:LX/11S;

    iput-object v6, v2, LX/DTt;->d:Ljava/lang/String;

    iput-object v7, v2, LX/DTt;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object v8, v2, LX/DTt;->f:LX/0Ot;

    iput-object p1, v2, LX/DTt;->g:Ljava/lang/Boolean;

    iput-object p2, v2, LX/DTt;->h:LX/5QP;

    iput-object p3, v2, LX/DTt;->i:LX/0Ot;

    .line 2000839
    const v0, 0x7f030aba

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2000840
    const v0, 0x7f0d1b6d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/DTt;->m:Landroid/view/View;

    .line 2000841
    const v0, 0x7f0d1b6b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;

    iput-object v0, p0, LX/DTt;->j:Lcom/facebook/groups/widget/remotepogview/RemotePogView;

    .line 2000842
    const v0, 0x7f0d1b6e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/DTt;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 2000843
    const v0, 0x7f0d1b6c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/DTt;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 2000844
    iget-boolean v0, p0, LX/DTt;->r:Z

    if-eqz v0, :cond_0

    .line 2000845
    new-instance v0, LX/DTq;

    invoke-direct {v0, p0}, LX/DTq;-><init>(LX/DTt;)V

    invoke-virtual {p0, v0}, LX/DTt;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2000846
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2000847
    iget-object v1, p0, LX/DTt;->n:LX/2qr;

    if-eqz v1, :cond_0

    .line 2000848
    iget-object v1, p0, LX/DTt;->n:LX/2qr;

    .line 2000849
    :goto_0
    move-object v0, v1

    .line 2000850
    invoke-virtual {p0, v0}, LX/DTt;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2000851
    return-void

    .line 2000852
    :cond_0
    new-instance v1, LX/2qr;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LX/2qr;-><init>(Landroid/view/View;I)V

    iput-object v1, p0, LX/DTt;->n:LX/2qr;

    .line 2000853
    iget-object v1, p0, LX/DTt;->n:LX/2qr;

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v3, v4}, LX/2qr;->setDuration(J)V

    .line 2000854
    iget-object v1, p0, LX/DTt;->n:LX/2qr;

    new-instance v2, LX/DTs;

    invoke-direct {v2, p0}, LX/DTs;-><init>(LX/DTt;)V

    invoke-virtual {v1, v2}, LX/2qr;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2000855
    iget-object v1, p0, LX/DTt;->n:LX/2qr;

    goto :goto_0
.end method

.method public final a(LX/DSf;ZLX/DSb;ZZZLjava/lang/String;)V
    .locals 7

    .prologue
    .line 2000856
    iget-object v0, p1, LX/DSf;->d:LX/DUV;

    move-object v1, v0

    .line 2000857
    invoke-interface {v1}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/DTt;->o:Ljava/lang/String;

    .line 2000858
    iput-object p7, p0, LX/DTt;->p:Ljava/lang/String;

    .line 2000859
    if-eqz p4, :cond_0

    if-nez p6, :cond_3

    iget-object v0, p0, LX/DTt;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/DSf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, LX/DSf;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2000860
    :cond_0
    iget-object v0, p0, LX/DTt;->m:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2000861
    iget-object v0, p0, LX/DTt;->m:Landroid/view/View;

    new-instance v2, LX/DTr;

    invoke-direct {v2, p0, p1}, LX/DTr;-><init>(LX/DTt;LX/DSf;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2000862
    :goto_0
    if-nez p5, :cond_1

    invoke-virtual {p0}, LX/DTt;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2000863
    invoke-virtual {p0}, LX/DTt;->clearAnimation()V

    .line 2000864
    :cond_1
    if-eqz p5, :cond_b

    .line 2000865
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/DTt;->setVisibility(I)V

    .line 2000866
    :goto_1
    if-nez p5, :cond_2

    .line 2000867
    invoke-interface {v1}, LX/DUV;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2000868
    invoke-interface {v1}, LX/DUV;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2000869
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/15i;->h(II)Z

    move-result v0

    .line 2000870
    :goto_2
    iget-object v2, p0, LX/DTt;->j:Lcom/facebook/groups/widget/remotepogview/RemotePogView;

    .line 2000871
    invoke-interface {v1}, LX/DUV;->k()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_c

    .line 2000872
    invoke-interface {v1}, LX/DUV;->k()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2000873
    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    :goto_3
    move-object v3, v3

    .line 2000874
    invoke-virtual {v2, v3, v0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a(Ljava/lang/String;Z)V

    .line 2000875
    iget-object v2, p0, LX/DTt;->k:Lcom/facebook/widget/text/BetterTextView;

    iget-object v0, p0, LX/DTt;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Cq;

    .line 2000876
    invoke-interface {v1}, LX/DUU;->kA_()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2000877
    const/4 v3, 0x0

    .line 2000878
    :goto_4
    move-object v0, v3

    .line 2000879
    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2000880
    iget-object v0, p1, LX/DSf;->e:LX/DS2;

    move-object v0, v0

    .line 2000881
    if-nez v0, :cond_5

    .line 2000882
    iget-object v0, p0, LX/DTt;->l:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2000883
    :goto_5
    const v0, 0x7f0d1b6f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2000884
    if-eqz p2, :cond_a

    .line 2000885
    sget-object v1, LX/DSb;->MODERATOR:LX/DSb;

    if-ne v1, p3, :cond_8

    .line 2000886
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2000887
    iget-object v1, p0, LX/DTt;->b:Landroid/content/res/Resources;

    const v2, 0x7f082f7c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2000888
    :cond_2
    :goto_6
    return-void

    .line 2000889
    :cond_3
    iget-object v0, p0, LX/DTt;->m:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 2000890
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 2000891
    :cond_5
    iget-object v0, p0, LX/DTt;->l:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2000892
    iget-object v0, p0, LX/DTt;->c:LX/11S;

    sget-object v1, LX/1lB;->FUZZY_RELATIVE_DATE_STYLE:LX/1lB;

    .line 2000893
    iget-object v2, p1, LX/DSf;->e:LX/DS2;

    move-object v2, v2

    .line 2000894
    invoke-virtual {v2}, LX/DS2;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/DTt;->a:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2000895
    iget-object v0, p0, LX/DTt;->o:Ljava/lang/String;

    .line 2000896
    iget-object v2, p1, LX/DSf;->e:LX/DS2;

    move-object v2, v2

    .line 2000897
    iget-object v3, v2, LX/DS2;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2000898
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2000899
    iget-object v0, p0, LX/DTt;->l:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p0, LX/DTt;->b:Landroid/content/res/Resources;

    const v3, 0x7f082f7d

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 2000900
    :cond_6
    iget-object v2, p0, LX/DTt;->l:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, LX/DTt;->b:Landroid/content/res/Resources;

    invoke-virtual {p1}, LX/DSf;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x7f082f7f

    :goto_7
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 2000901
    iget-object v6, p1, LX/DSf;->e:LX/DS2;

    move-object v6, v6

    .line 2000902
    iget-object p1, v6, LX/DS2;->a:Ljava/lang/String;

    move-object v6, p1

    .line 2000903
    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_7
    const v0, 0x7f082f7e

    goto :goto_7

    .line 2000904
    :cond_8
    sget-object v1, LX/DSb;->ADMIN:LX/DSb;

    if-ne v1, p3, :cond_9

    .line 2000905
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2000906
    iget-object v1, p0, LX/DTt;->b:Landroid/content/res/Resources;

    const v2, 0x7f082f7b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 2000907
    :cond_9
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 2000908
    :cond_a
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 2000909
    :cond_b
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DTt;->setVisibility(I)V

    goto/16 :goto_1

    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 2000910
    :cond_d
    iget-object v3, p0, LX/DTt;->g:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v1}, LX/DUU;->kz_()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v1}, LX/DUU;->d()Z

    move-result v3

    if-nez v3, :cond_e

    .line 2000911
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-interface {v1}, LX/DUU;->kA_()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2000912
    const/4 v4, 0x0

    move-object v4, v4

    .line 2000913
    invoke-static {v3, v4}, LX/47q;->a(Landroid/text/SpannableStringBuilder;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4

    .line 2000914
    :cond_e
    invoke-interface {v1}, LX/DUU;->kA_()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2000915
    const/16 v0, 0x8

    invoke-virtual {p0}, LX/DTt;->getVisibility()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2000916
    invoke-virtual {p0, v2, v2}, LX/DTt;->setMeasuredDimension(II)V

    .line 2000917
    :goto_0
    return-void

    .line 2000918
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    goto :goto_0
.end method
