.class public final LX/Dc6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;)V
    .locals 0

    .prologue
    .line 2017891
    iput-object p1, p0, LX/Dc6;->a:Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x53f4b24d

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017892
    iget-object v1, p0, LX/Dc6;->a:Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;

    .line 2017893
    iget-object v3, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2017894
    const-string v4, "com.facebook.katana.profile.id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2017895
    iget-object v4, v1, Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;->c:LX/DbM;

    .line 2017896
    iget-object v5, v4, LX/DbM;->a:LX/0Zb;

    const-string v6, "page_menu_management"

    const-string v7, "menu_management_save_link_menu_button_tap"

    invoke-static {v6, v7, v3}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2017897
    new-instance v4, LX/4Hp;

    invoke-direct {v4}, LX/4Hp;-><init>()V

    iget-object v5, v1, Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;->f:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2017898
    const-string v6, "link_menu_uri"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017899
    move-object v4, v4

    .line 2017900
    const-string v5, "page_id"

    invoke-virtual {v4, v5, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017901
    move-object v4, v4

    .line 2017902
    new-instance v5, LX/8Ah;

    invoke-direct {v5}, LX/8Ah;-><init>()V

    move-object v5, v5

    .line 2017903
    const-string v6, "input"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2017904
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2017905
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const p0, 0x7f0828fb

    invoke-virtual {v7, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 p0, 0x1

    const/4 p1, 0x0

    invoke-static {v5, v6, v7, p0, p1}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v5

    .line 2017906
    iget-object v6, v1, Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;->d:LX/1Ck;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string p0, "task_key_create_link_menu"

    invoke-direct {v7, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object p0, v1, Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;->a:LX/0tX;

    sget-object p1, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {p0, v4, p1}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance p0, LX/Dc7;

    invoke-direct {p0, v1, v5, v3}, LX/Dc7;-><init>(Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;LX/4BY;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v4, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2017907
    const v1, -0x675235fd

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
