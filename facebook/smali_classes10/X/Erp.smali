.class public final enum LX/Erp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Erp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Erp;

.field public static final enum CANCEL:LX/Erp;

.field public static final enum FAIL:LX/Erp;

.field public static final enum START:LX/Erp;

.field public static final enum SUCCESS:LX/Erp;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2173482
    new-instance v0, LX/Erp;

    const-string v1, "START"

    invoke-direct {v0, v1, v2}, LX/Erp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Erp;->START:LX/Erp;

    .line 2173483
    new-instance v0, LX/Erp;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, LX/Erp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Erp;->SUCCESS:LX/Erp;

    .line 2173484
    new-instance v0, LX/Erp;

    const-string v1, "CANCEL"

    invoke-direct {v0, v1, v4}, LX/Erp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Erp;->CANCEL:LX/Erp;

    .line 2173485
    new-instance v0, LX/Erp;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v5}, LX/Erp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Erp;->FAIL:LX/Erp;

    .line 2173486
    const/4 v0, 0x4

    new-array v0, v0, [LX/Erp;

    sget-object v1, LX/Erp;->START:LX/Erp;

    aput-object v1, v0, v2

    sget-object v1, LX/Erp;->SUCCESS:LX/Erp;

    aput-object v1, v0, v3

    sget-object v1, LX/Erp;->CANCEL:LX/Erp;

    aput-object v1, v0, v4

    sget-object v1, LX/Erp;->FAIL:LX/Erp;

    aput-object v1, v0, v5

    sput-object v0, LX/Erp;->$VALUES:[LX/Erp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2173487
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Erp;
    .locals 1

    .prologue
    .line 2173488
    const-class v0, LX/Erp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Erp;

    return-object v0
.end method

.method public static values()[LX/Erp;
    .locals 1

    .prologue
    .line 2173489
    sget-object v0, LX/Erp;->$VALUES:[LX/Erp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Erp;

    return-object v0
.end method
