.class public final LX/E8J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/widget/TextView;

.field public final synthetic b:LX/E8M;


# direct methods
.method public constructor <init>(LX/E8M;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 2082294
    iput-object p1, p0, LX/E8J;->b:LX/E8M;

    iput-object p2, p0, LX/E8J;->a:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x1cfb1b48

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2082295
    iget-object v2, p0, LX/E8J;->b:LX/E8M;

    iget-object v3, p0, LX/E8J;->b:LX/E8M;

    iget-boolean v3, v3, LX/E8M;->f:Z

    if-nez v3, :cond_1

    .line 2082296
    :goto_0
    iput-boolean v0, v2, LX/E8M;->f:Z

    .line 2082297
    iget-object v0, p0, LX/E8J;->b:LX/E8M;

    iget-object v2, p0, LX/E8J;->a:Landroid/widget/TextView;

    .line 2082298
    iget-boolean v3, v0, LX/E8M;->f:Z

    if-eqz v3, :cond_2

    const-string v5, "LOCAL"

    .line 2082299
    :goto_1
    iget-object v3, v0, LX/E8M;->i:LX/7va;

    iget-object v4, v0, LX/E8M;->g:Ljava/lang/String;

    sget-object v6, Lcom/facebook/events/common/ActionMechanism;->PLACE_TIPS:Lcom/facebook/events/common/ActionMechanism;

    const-string v7, "reaction_dialog"

    const-string v8, "unknown"

    const/4 p1, 0x0

    .line 2082300
    new-instance v9, LX/4Il;

    invoke-direct {v9}, LX/4Il;-><init>()V

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v10, p1, v11, p1}, LX/7va;->a(Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/4EL;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/4Il;->a(LX/4EL;)LX/4Il;

    move-result-object v9

    invoke-virtual {v9, v4}, LX/4Il;->a(Ljava/lang/String;)LX/4Il;

    move-result-object v9

    invoke-virtual {v9, v5}, LX/4Il;->b(Ljava/lang/String;)LX/4Il;

    move-result-object v9

    .line 2082301
    invoke-static {}, LX/7uR;->c()LX/7uB;

    move-result-object v10

    .line 2082302
    const-string v11, "input"

    invoke-virtual {v10, v11, v9}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2082303
    invoke-static {v10}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v9

    .line 2082304
    iget-object v10, v3, LX/7va;->a:LX/0tX;

    invoke-virtual {v10, v9}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    move-object v3, v9

    .line 2082305
    iget-object v4, v0, LX/E8M;->j:LX/1Ck;

    new-instance v5, LX/E8L;

    invoke-direct {v5, v0, v2}, LX/E8L;-><init>(LX/E8M;Landroid/widget/TextView;)V

    invoke-virtual {v4, v0, v3, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2082306
    iget-object v0, p0, LX/E8J;->b:LX/E8M;

    iget-object v2, p0, LX/E8J;->a:Landroid/widget/TextView;

    invoke-static {v0, v2}, LX/E8M;->b$redex0(LX/E8M;Landroid/widget/TextView;)V

    .line 2082307
    iget-object v0, p0, LX/E8J;->b:LX/E8M;

    iget-object v0, v0, LX/E8M;->e:LX/2ja;

    if-eqz v0, :cond_0

    .line 2082308
    iget-object v0, p0, LX/E8J;->b:LX/E8M;

    iget-object v0, v0, LX/E8M;->e:LX/2ja;

    iget-object v2, p0, LX/E8J;->b:LX/E8M;

    iget-object v2, v2, LX/E8M;->a:Lcom/facebook/reaction/common/ReactionCardNode;

    invoke-virtual {v2}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E8J;->b:LX/E8M;

    iget-object v3, v3, LX/E8M;->a:Lcom/facebook/reaction/common/ReactionCardNode;

    invoke-virtual {v3}, Lcom/facebook/reaction/common/ReactionCardNode;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/Cfl;

    iget-object v5, p0, LX/E8J;->b:LX/E8M;

    iget-object v5, v5, LX/E8M;->g:Ljava/lang/String;

    sget-object v6, LX/Cfc;->SUBSCRIBE_PAGE_EVENTS_TAP:LX/Cfc;

    invoke-direct {v4, v5, v6}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;)V

    invoke-virtual {v0, v2, v3, v4}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2082309
    :cond_0
    const v0, -0x2d4d253e

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2082310
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2082311
    :cond_2
    const-string v5, "NONE"

    goto/16 :goto_1
.end method
