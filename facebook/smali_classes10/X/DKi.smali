.class public final LX/DKi;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TResultType;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DKo;


# direct methods
.method public constructor <init>(LX/DKo;)V
    .locals 0

    .prologue
    .line 1987449
    iput-object p1, p0, LX/DKi;->a:LX/DKo;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1987450
    iget-object v1, p0, LX/DKi;->a:LX/DKo;

    monitor-enter v1

    .line 1987451
    :try_start_0
    iget-object v0, p0, LX/DKi;->a:LX/DKo;

    iget-object v2, p0, LX/DKi;->a:LX/DKo;

    iget-object v2, v2, LX/DKo;->a:LX/DKg;

    invoke-static {v0, v2}, LX/DKo;->a$redex0(LX/DKo;LX/DKg;)V

    .line 1987452
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 1987453
    iget-object v0, p0, LX/DKi;->a:LX/DKo;

    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    invoke-static {v0, p1}, LX/DKo;->a$redex0(LX/DKo;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1987454
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1987455
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1987456
    iget-object v1, p0, LX/DKi;->a:LX/DKo;

    monitor-enter v1

    .line 1987457
    :try_start_0
    iget-object v0, p0, LX/DKi;->a:LX/DKo;

    invoke-static {v0}, LX/DKo;->d$redex0(LX/DKo;)V

    .line 1987458
    iget-object v0, p0, LX/DKi;->a:LX/DKo;

    .line 1987459
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1987460
    iput-object v2, v0, LX/DKo;->f:LX/0Px;

    .line 1987461
    iget-object v0, p0, LX/DKi;->a:LX/DKo;

    invoke-static {v0, p1}, LX/DKo;->a$redex0(LX/DKo;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1987462
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
