.class public LX/Djl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2033611
    iput-object p1, p0, LX/Djl;->a:Ljava/lang/Integer;

    .line 2033612
    iput-object p2, p0, LX/Djl;->b:Ljava/lang/Integer;

    .line 2033613
    return-void
.end method

.method public static a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)LX/Djl;
    .locals 13

    .prologue
    const/16 v12, 0xc

    const/16 v11, 0xb

    const/4 v10, 0x2

    const/4 v9, 0x5

    const/4 v7, 0x1

    .line 2033614
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->p:Ljava/util/Calendar;

    if-nez v0, :cond_1

    .line 2033615
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Date or Time"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2033616
    :cond_1
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 2033617
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 2033618
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    invoke-virtual {v2, v10}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    invoke-virtual {v3, v9}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->p:Ljava/util/Calendar;

    invoke-virtual {v4, v11}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v5, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->p:Ljava/util/Calendar;

    invoke-virtual {v5, v12}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 2033619
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v8, v0

    .line 2033620
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->q:Ljava/util/Calendar;

    if-nez v0, :cond_2

    .line 2033621
    new-instance v0, LX/Djl;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/Djl;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2033622
    :goto_0
    return-object v0

    .line 2033623
    :cond_2
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2033624
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->q:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->p:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_4

    move v6, v7

    .line 2033625
    :goto_1
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 2033626
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 2033627
    iget-object v1, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    invoke-virtual {v2, v10}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->o:Ljava/util/Calendar;

    invoke-virtual {v3, v9}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->q:Ljava/util/Calendar;

    invoke-virtual {v4, v11}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v5, p0, Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;->q:Ljava/util/Calendar;

    invoke-virtual {v5, v12}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 2033628
    if-eqz v6, :cond_3

    .line 2033629
    invoke-virtual {v0, v9, v7}, Ljava/util/Calendar;->add(II)V

    .line 2033630
    :cond_3
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v1, v0

    .line 2033631
    new-instance v0, LX/Djl;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/Djl;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_0

    .line 2033632
    :cond_4
    const/4 v0, 0x0

    move v6, v0

    goto :goto_1
.end method
