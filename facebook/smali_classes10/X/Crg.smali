.class public LX/Crg;
.super LX/BA0;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Crg;


# instance fields
.field private final b:LX/0TD;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0hB;LX/BA2;LX/0TD;Ljava/util/concurrent/Executor;LX/0Uh;)V
    .locals 0
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1941289
    invoke-direct {p0, p1, p2, p4}, LX/BA0;-><init>(LX/0hB;LX/BA2;Ljava/util/concurrent/Executor;)V

    .line 1941290
    iput-object p3, p0, LX/Crg;->b:LX/0TD;

    .line 1941291
    iput-object p5, p0, LX/Crg;->c:LX/0Uh;

    .line 1941292
    return-void
.end method

.method public static b(LX/0QB;)LX/Crg;
    .locals 9

    .prologue
    .line 1941293
    sget-object v0, LX/Crg;->d:LX/Crg;

    if-nez v0, :cond_1

    .line 1941294
    const-class v1, LX/Crg;

    monitor-enter v1

    .line 1941295
    :try_start_0
    sget-object v0, LX/Crg;->d:LX/Crg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1941296
    if-eqz v2, :cond_0

    .line 1941297
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1941298
    new-instance v3, LX/Crg;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {v0}, LX/BA2;->a(LX/0QB;)LX/BA2;

    move-result-object v5

    check-cast v5, LX/BA2;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {v0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v3 .. v8}, LX/Crg;-><init>(LX/0hB;LX/BA2;LX/0TD;Ljava/util/concurrent/Executor;LX/0Uh;)V

    .line 1941299
    move-object v0, v3

    .line 1941300
    sput-object v0, LX/Crg;->d:LX/Crg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1941301
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1941302
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1941303
    :cond_1
    sget-object v0, LX/Crg;->d:LX/Crg;

    return-object v0

    .line 1941304
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1941305
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;IFFFF)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IFFFF)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1941306
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    .line 1941307
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, LX/Crg;->c:LX/0Uh;

    const/16 v1, 0xac

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941308
    iget-object v0, p0, LX/BA0;->a:LX/BA2;

    invoke-virtual {v0, p1}, LX/BA2;->b(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    new-instance v0, LX/Crf;

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, LX/Crf;-><init>(LX/Crg;IFFFF)V

    iget-object v1, p0, LX/Crg;->b:LX/0TD;

    invoke-static {v7, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1941309
    :goto_0
    return-object v0

    :cond_0
    invoke-super/range {p0 .. p6}, LX/BA0;->a(Ljava/lang/String;IFFFF)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
