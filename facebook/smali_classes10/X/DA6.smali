.class public LX/DA6;
.super LX/DA2;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field public final d:Landroid/content/ContentResolver;

.field private final e:LX/DA5;

.field public final f:LX/1Ml;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1970823
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    const-string v1, "address"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    const-string v1, "charset"

    aput-object v1, v0, v6

    sput-object v0, LX/DA6;->a:[Ljava/lang/String;

    .line 1970824
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "m_id"

    aput-object v1, v0, v4

    const-string v1, "ct_cls"

    aput-object v1, v0, v5

    const-string v1, "ct_t"

    aput-object v1, v0, v6

    const-string v1, "date"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "date_sent"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "d_tm"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "d_rpt"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "msg_box"

    aput-object v2, v0, v1

    sput-object v0, LX/DA6;->b:[Ljava/lang/String;

    .line 1970825
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "m_id"

    aput-object v1, v0, v3

    const-string v1, "ct_cls"

    aput-object v1, v0, v4

    const-string v1, "ct_t"

    aput-object v1, v0, v5

    const-string v1, "date"

    aput-object v1, v0, v6

    const-string v1, "date_sent"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "d_tm"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "d_rpt"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "msg_box"

    aput-object v2, v0, v1

    sput-object v0, LX/DA6;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/DA5;LX/1Ml;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1970818
    invoke-direct {p0}, LX/DA2;-><init>()V

    .line 1970819
    iput-object p1, p0, LX/DA6;->d:Landroid/content/ContentResolver;

    .line 1970820
    iput-object p2, p0, LX/DA6;->e:LX/DA5;

    .line 1970821
    iput-object p3, p0, LX/DA6;->f:LX/1Ml;

    .line 1970822
    return-void
.end method


# virtual methods
.method public final a()LX/DA1;
    .locals 1

    .prologue
    .line 1970817
    sget-object v0, LX/DA1;->MMS_LOG:LX/DA1;

    return-object v0
.end method

.method public final a(Landroid/database/Cursor;)Lcom/facebook/contactlogs/data/ContactLogMetadata;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1970792
    new-instance v7, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 1970793
    iget-object v0, p0, LX/DA6;->f:LX/1Ml;

    const-string v1, "android.permission.READ_SMS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1970794
    new-instance v0, Lcom/facebook/contactlogs/data/ContactLogMetadata;

    invoke-virtual {p0}, LX/DA6;->a()LX/DA1;

    move-result-object v1

    invoke-direct {v0, v7, v1}, Lcom/facebook/contactlogs/data/ContactLogMetadata;-><init>(LX/0m9;LX/DA1;)V

    .line 1970795
    :goto_0
    return-object v0

    .line 1970796
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/DA6;->d:Landroid/content/ContentResolver;

    const-string v1, "content://mms/%s/addr"

    const-string v2, "_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/DA6;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 1970797
    :try_start_1
    new-instance v0, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v2}, LX/162;-><init>(LX/0mC;)V

    .line 1970798
    if-eqz v1, :cond_2

    .line 1970799
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1970800
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 1970801
    invoke-static {v2, v1}, LX/DA2;->a(LX/0m9;Landroid/database/Cursor;)V

    .line 1970802
    invoke-virtual {v0, v2}, LX/162;->a(LX/0lF;)LX/162;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1970803
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 1970804
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 1970805
    :cond_2
    :try_start_2
    const-string v2, "addresses"

    invoke-virtual {v7, v2, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1970806
    if-eqz v1, :cond_3

    .line 1970807
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1970808
    :cond_3
    invoke-static {v7, p1}, LX/DA2;->a(LX/0m9;Landroid/database/Cursor;)V

    .line 1970809
    new-instance v0, Lcom/facebook/contactlogs/data/ContactLogMetadata;

    invoke-virtual {p0}, LX/DA6;->a()LX/DA1;

    move-result-object v1

    invoke-direct {v0, v7, v1}, Lcom/facebook/contactlogs/data/ContactLogMetadata;-><init>(LX/0m9;LX/DA1;)V

    goto :goto_0

    .line 1970810
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_2
.end method

.method public final b()LX/DA4;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1970811
    sget-object v0, LX/DA6;->b:[Ljava/lang/String;

    const/4 v4, 0x0

    .line 1970812
    iget-object v1, p0, LX/DA6;->f:LX/1Ml;

    const-string v2, "android.permission.READ_SMS"

    invoke-virtual {v1, v2}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1970813
    :cond_0
    :goto_0
    move-object v0, v4

    .line 1970814
    return-object v0

    .line 1970815
    :cond_1
    iget-object v1, p0, LX/DA6;->d:Landroid/content/ContentResolver;

    sget-object v2, LX/2UG;->a:Landroid/net/Uri;

    const-string v6, "_id"

    move-object v3, v0

    move-object v5, v4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1970816
    if-eqz v1, :cond_0

    invoke-static {v1, p0}, LX/DA5;->a(Landroid/database/Cursor;LX/DA2;)LX/DA4;

    move-result-object v4

    goto :goto_0
.end method
