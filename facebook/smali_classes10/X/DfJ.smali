.class public final LX/DfJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2026790
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2026791
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2026792
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2026793
    invoke-static {p0, p1}, LX/DfJ;->b(LX/15w;LX/186;)I

    move-result v1

    .line 2026794
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2026795
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2026796
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 2026797
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2026798
    :goto_0
    return v1

    .line 2026799
    :cond_0
    const-string v11, "messenger_inbox_item_clicks_remaining"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2026800
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v2

    .line 2026801
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_7

    .line 2026802
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2026803
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2026804
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2026805
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2026806
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2026807
    :cond_2
    const-string v11, "messenger_inbox_item_attachment"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2026808
    invoke-static {p0, p1}, LX/DfI;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2026809
    :cond_3
    const-string v11, "messenger_inbox_item_hides_remaining"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2026810
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 2026811
    :cond_4
    const-string v11, "messenger_inbox_item_logging_data"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2026812
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2026813
    :cond_5
    const-string v11, "messenger_inbox_item_title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 2026814
    const/4 v10, 0x0

    .line 2026815
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v11, :cond_e

    .line 2026816
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2026817
    :goto_2
    move v4, v10

    .line 2026818
    goto :goto_1

    .line 2026819
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2026820
    :cond_7
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2026821
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 2026822
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 2026823
    if-eqz v3, :cond_8

    .line 2026824
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7, v1}, LX/186;->a(III)V

    .line 2026825
    :cond_8
    if-eqz v0, :cond_9

    .line 2026826
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6, v1}, LX/186;->a(III)V

    .line 2026827
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2026828
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2026829
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1

    .line 2026830
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2026831
    :cond_c
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_d

    .line 2026832
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2026833
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2026834
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_c

    if-eqz v11, :cond_c

    .line 2026835
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 2026836
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 2026837
    :cond_d
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2026838
    invoke-virtual {p1, v10, v4}, LX/186;->b(II)V

    .line 2026839
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto :goto_2

    :cond_e
    move v4, v10

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2026840
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2026841
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026842
    if-eqz v0, :cond_0

    .line 2026843
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026844
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026845
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026846
    if-eqz v0, :cond_1

    .line 2026847
    const-string v1, "messenger_inbox_item_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026848
    invoke-static {p0, v0, p2, p3}, LX/DfI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2026849
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2026850
    if-eqz v0, :cond_2

    .line 2026851
    const-string v1, "messenger_inbox_item_clicks_remaining"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026852
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2026853
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2026854
    if-eqz v0, :cond_3

    .line 2026855
    const-string v1, "messenger_inbox_item_hides_remaining"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026856
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2026857
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2026858
    if-eqz v0, :cond_4

    .line 2026859
    const-string v1, "messenger_inbox_item_logging_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026860
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026861
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2026862
    if-eqz v0, :cond_6

    .line 2026863
    const-string v1, "messenger_inbox_item_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026864
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2026865
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2026866
    if-eqz v1, :cond_5

    .line 2026867
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2026868
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2026869
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2026870
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2026871
    return-void
.end method
