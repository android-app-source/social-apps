.class public LX/E37;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3Tp;

.field private final b:LX/1DS;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition",
            "<",
            "LX/3U6;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/E2N;


# direct methods
.method public constructor <init>(LX/3Tp;LX/1DS;LX/0Ot;LX/E2N;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3Tp;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionVerticalListUnitComponentGroupPartDefinition;",
            ">;",
            "LX/E2N;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2073635
    iput-object p1, p0, LX/E37;->a:LX/3Tp;

    .line 2073636
    iput-object p2, p0, LX/E37;->b:LX/1DS;

    .line 2073637
    iput-object p3, p0, LX/E37;->c:LX/0Ot;

    .line 2073638
    iput-object p4, p0, LX/E37;->d:LX/E2N;

    .line 2073639
    return-void
.end method

.method public static b(LX/0QB;)LX/E37;
    .locals 5

    .prologue
    .line 2073640
    new-instance v3, LX/E37;

    const-class v0, LX/3Tp;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3Tp;

    invoke-static {p0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v1

    check-cast v1, LX/1DS;

    const/16 v2, 0x3109

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const-class v2, LX/E2N;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/E2N;

    invoke-direct {v3, v0, v1, v4, v2}, LX/E37;-><init>(LX/3Tp;LX/1DS;LX/0Ot;LX/E2N;)V

    .line 2073641
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/1PT;LX/1SX;LX/E39;Ljava/lang/Runnable;LX/0o8;LX/2jY;LX/1PY;)LX/1Qq;
    .locals 12

    .prologue
    .line 2073642
    iget-object v7, p0, LX/E37;->d:LX/E2N;

    new-instance v1, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    const-string v2, "unknown"

    const-string v3, "unknown"

    const-string v4, "unknown"

    const-string v5, "unknown"

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v2, p0, LX/E37;->a:LX/3Tp;

    move-object/from16 v0, p6

    invoke-virtual {v2, p1, v0}, LX/3Tp;->a(Landroid/content/Context;LX/0o8;)LX/3Tv;

    move-result-object v8

    invoke-interface/range {p6 .. p6}, LX/0o8;->m()LX/2ja;

    move-result-object v9

    move-object v2, v7

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p5

    move-object v7, v1

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-virtual/range {v2 .. v11}, LX/E2N;->a(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/3Tv;LX/2ja;LX/2jY;LX/1PY;)LX/3U6;

    move-result-object v1

    .line 2073643
    iget-object v2, p0, LX/E37;->b:LX/1DS;

    iget-object v3, p0, LX/E37;->c:LX/0Ot;

    move-object/from16 v0, p4

    invoke-virtual {v2, v3, v0}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1Ql;->a(LX/1PW;)LX/1Ql;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v1

    return-object v1
.end method
