.class public LX/Co4;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/RelatedArticleFullMediaSocialBlockView;",
        "Lcom/facebook/richdocument/model/data/RelatedArticleBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;)V
    .locals 1

    .prologue
    .line 1934843
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934844
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Co4;

    invoke-static {v0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v0

    check-cast v0, LX/Ck0;

    iput-object v0, p0, LX/Co4;->d:LX/Ck0;

    .line 1934845
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 13

    .prologue
    .line 1934846
    check-cast p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;

    .line 1934847
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934848
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    .line 1934849
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1934850
    iget-object v1, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1934851
    iget-object v2, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1934852
    iget-object v3, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->h:Ljava/lang/String;

    move-object v3, v3

    .line 1934853
    iget-object v4, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->f:Ljava/lang/String;

    move-object v4, v4

    .line 1934854
    iget-object v5, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->i:Ljava/lang/String;

    move-object v5, v5

    .line 1934855
    iget-boolean v6, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->k:Z

    move v6, v6

    .line 1934856
    iget-object v7, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->m:Ljava/lang/String;

    move-object v7, v7

    .line 1934857
    iget-object v8, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v8, v8

    .line 1934858
    iget-object v9, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->p:Ljava/lang/String;

    move-object v9, v9

    .line 1934859
    iget-object v10, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->j:Ljava/lang/String;

    move-object v10, v10

    .line 1934860
    iget-object v11, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->r:Ljava/util/List;

    move-object v11, v11

    .line 1934861
    iget-object v12, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->s:Ljava/lang/String;

    move-object v12, v12

    .line 1934862
    invoke-virtual/range {v0 .. v12}, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 1934863
    iget-object v0, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->n:LX/Cm2;

    move-object v1, v0

    .line 1934864
    sget-object v0, LX/Cm2;->BOTTOM:LX/Cm2;

    if-ne v1, v0, :cond_2

    .line 1934865
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934866
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    const-string v2, "footer_related_article"

    .line 1934867
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->z:Ljava/lang/String;

    .line 1934868
    :cond_0
    :goto_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934869
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    invoke-interface {v0}, LX/CnG;->c()Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, LX/Co4;->d:LX/Ck0;

    invoke-static {v0, v1, v2}, LX/Crt;->a(Landroid/view/View;LX/Cm2;LX/Ck0;)V

    .line 1934870
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934871
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0622

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1934872
    iput v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->w:I

    .line 1934873
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934874
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v2

    .line 1934875
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->A:Ljava/lang/String;

    .line 1934876
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934877
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v2

    .line 1934878
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->C:Ljava/lang/String;

    .line 1934879
    iget v0, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->l:I

    move v2, v0

    .line 1934880
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934881
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    .line 1934882
    iput v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->x:I

    .line 1934883
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934884
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    .line 1934885
    iget v3, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->q:I

    move v3, v3

    .line 1934886
    iput v3, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->y:I

    .line 1934887
    sget-object v0, LX/Cm2;->BOTTOM:LX/Cm2;

    if-ne v1, v0, :cond_1

    const/4 v0, 0x1

    if-ne v2, v0, :cond_1

    .line 1934888
    iget-object v0, p0, LX/CnT;->a:LX/Chv;

    new-instance v1, LX/CiE;

    invoke-direct {v1}, LX/CiE;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1934889
    :cond_1
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934890
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->lw_()LX/Cml;

    move-result-object v1

    invoke-interface {v0, v1}, LX/CnG;->a(LX/Cml;)V

    .line 1934891
    return-void

    .line 1934892
    :cond_2
    sget-object v0, LX/Cm2;->INLINE:LX/Cm2;

    if-ne v1, v0, :cond_0

    .line 1934893
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934894
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    const-string v2, "inline_related_article"

    .line 1934895
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->z:Ljava/lang/String;

    .line 1934896
    goto :goto_0
.end method
