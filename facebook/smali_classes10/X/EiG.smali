.class public final enum LX/EiG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EiG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EiG;

.field public static final enum CODE_SUCCESS:LX/EiG;

.field public static final enum EMAIL_ACQUIRED:LX/EiG;

.field public static final enum EMAIL_SWITCH_TO_PHONE:LX/EiG;

.field public static final enum PHONE_ACQUIRED:LX/EiG;

.field public static final enum PHONE_SWITCH_TO_EMAIL:LX/EiG;

.field public static final enum UNKNOWN_ERROR:LX/EiG;

.field public static final enum UPDATE_EMAIL:LX/EiG;

.field public static final enum UPDATE_PHONE:LX/EiG;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2159224
    new-instance v0, LX/EiG;

    const-string v1, "CODE_SUCCESS"

    invoke-direct {v0, v1, v3}, LX/EiG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiG;->CODE_SUCCESS:LX/EiG;

    .line 2159225
    new-instance v0, LX/EiG;

    const-string v1, "EMAIL_ACQUIRED"

    invoke-direct {v0, v1, v4}, LX/EiG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiG;->EMAIL_ACQUIRED:LX/EiG;

    .line 2159226
    new-instance v0, LX/EiG;

    const-string v1, "PHONE_ACQUIRED"

    invoke-direct {v0, v1, v5}, LX/EiG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiG;->PHONE_ACQUIRED:LX/EiG;

    .line 2159227
    new-instance v0, LX/EiG;

    const-string v1, "UPDATE_PHONE"

    invoke-direct {v0, v1, v6}, LX/EiG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiG;->UPDATE_PHONE:LX/EiG;

    .line 2159228
    new-instance v0, LX/EiG;

    const-string v1, "UPDATE_EMAIL"

    invoke-direct {v0, v1, v7}, LX/EiG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiG;->UPDATE_EMAIL:LX/EiG;

    .line 2159229
    new-instance v0, LX/EiG;

    const-string v1, "EMAIL_SWITCH_TO_PHONE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EiG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiG;->EMAIL_SWITCH_TO_PHONE:LX/EiG;

    .line 2159230
    new-instance v0, LX/EiG;

    const-string v1, "PHONE_SWITCH_TO_EMAIL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/EiG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiG;->PHONE_SWITCH_TO_EMAIL:LX/EiG;

    .line 2159231
    new-instance v0, LX/EiG;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/EiG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiG;->UNKNOWN_ERROR:LX/EiG;

    .line 2159232
    const/16 v0, 0x8

    new-array v0, v0, [LX/EiG;

    sget-object v1, LX/EiG;->CODE_SUCCESS:LX/EiG;

    aput-object v1, v0, v3

    sget-object v1, LX/EiG;->EMAIL_ACQUIRED:LX/EiG;

    aput-object v1, v0, v4

    sget-object v1, LX/EiG;->PHONE_ACQUIRED:LX/EiG;

    aput-object v1, v0, v5

    sget-object v1, LX/EiG;->UPDATE_PHONE:LX/EiG;

    aput-object v1, v0, v6

    sget-object v1, LX/EiG;->UPDATE_EMAIL:LX/EiG;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EiG;->EMAIL_SWITCH_TO_PHONE:LX/EiG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EiG;->PHONE_SWITCH_TO_EMAIL:LX/EiG;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EiG;->UNKNOWN_ERROR:LX/EiG;

    aput-object v2, v0, v1

    sput-object v0, LX/EiG;->$VALUES:[LX/EiG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2159233
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EiG;
    .locals 1

    .prologue
    .line 2159234
    const-class v0, LX/EiG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EiG;

    return-object v0
.end method

.method public static valueOfKey(Ljava/lang/String;)LX/EiG;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2159235
    if-eqz p0, :cond_0

    const-string v1, "com.facebook.confirmation."

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2159236
    :cond_0
    :goto_0
    return-object v0

    .line 2159237
    :cond_1
    const/16 v1, 0x1a

    :try_start_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/EiG;->valueOf(Ljava/lang/String;)LX/EiG;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2159238
    :catch_0
    goto :goto_0
.end method

.method public static values()[LX/EiG;
    .locals 1

    .prologue
    .line 2159239
    sget-object v0, LX/EiG;->$VALUES:[LX/EiG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EiG;

    return-object v0
.end method


# virtual methods
.method public final getKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2159240
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "com.facebook.confirmation."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/EiG;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
