.class public final LX/DQS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DMP;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/info/GroupInfoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoFragment;)V
    .locals 0

    .prologue
    .line 1994355
    iput-object p1, p0, LX/DQS;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)V
    .locals 2

    .prologue
    .line 1994356
    invoke-static {p1, p2}, LX/7vF;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)LX/7vF;

    move-result-object v0

    .line 1994357
    iget-object v1, p0, LX/DQS;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    invoke-static {v1, v0, p3}, Lcom/facebook/groups/info/GroupInfoFragment;->a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;LX/7vF;Z)V

    .line 1994358
    iget-object v1, p0, LX/DQS;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v1, v1, Lcom/facebook/groups/info/GroupInfoFragment;->g:LX/DMZ;

    invoke-virtual {v1, v0, p2}, LX/DMZ;->a(LX/7vF;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    .line 1994359
    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Z)V
    .locals 2

    .prologue
    .line 1994360
    invoke-static {p1, p2}, LX/7vF;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/7vF;

    move-result-object v0

    .line 1994361
    iget-object v1, p0, LX/DQS;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    invoke-static {v1, v0, p3}, Lcom/facebook/groups/info/GroupInfoFragment;->a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;LX/7vF;Z)V

    .line 1994362
    iget-object v1, p0, LX/DQS;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v1, v1, Lcom/facebook/groups/info/GroupInfoFragment;->g:LX/DMZ;

    invoke-virtual {v1, v0, p2}, LX/DMZ;->a(LX/7vF;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    .line 1994363
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1994364
    iget-object v0, p0, LX/DQS;->a:Lcom/facebook/groups/info/GroupInfoFragment;

    iget-object v0, v0, Lcom/facebook/groups/info/GroupInfoFragment;->s:Lcom/facebook/groups/info/GroupInfoAdapter;

    const v1, 0x2da0f48f

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1994365
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 1

    .prologue
    .line 1994366
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/DQS;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)V

    .line 1994367
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 1

    .prologue
    .line 1994368
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/DQS;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Z)V

    .line 1994369
    return-void
.end method

.method public final b(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 1

    .prologue
    .line 1994370
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/DQS;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)V

    .line 1994371
    return-void
.end method

.method public final b(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 1

    .prologue
    .line 1994372
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/DQS;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Z)V

    .line 1994373
    return-void
.end method
