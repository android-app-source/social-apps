.class public LX/CrT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CrS;


# instance fields
.field private final a:LX/Cqv;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "LX/CrR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Cqv;)V
    .locals 1

    .prologue
    .line 1941010
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/CrT;-><init>(LX/Cqv;LX/CrS;)V

    .line 1941011
    return-void
.end method

.method private constructor <init>(LX/Cqv;LX/CrS;)V
    .locals 4

    .prologue
    .line 1941001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941002
    iput-object p1, p0, LX/CrT;->a:LX/Cqv;

    .line 1941003
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/CrT;->b:Ljava/util/Map;

    .line 1941004
    if-eqz p2, :cond_0

    .line 1941005
    invoke-interface {p2}, LX/CrS;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1941006
    iget-object v2, p0, LX/CrT;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CrR;

    .line 1941007
    new-instance p1, LX/CrR;

    invoke-direct {p1, v0}, LX/CrR;-><init>(LX/CrR;)V

    move-object v0, p1

    .line 1941008
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1941009
    :cond_0
    return-void
.end method

.method private constructor <init>(LX/CrS;)V
    .locals 1

    .prologue
    .line 1940999
    invoke-interface {p1}, LX/CrS;->a()LX/Cqv;

    move-result-object v0

    invoke-direct {p0, v0, p1}, LX/CrT;-><init>(LX/Cqv;LX/CrS;)V

    .line 1941000
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V::",
            "LX/CqY;",
            ">(",
            "Landroid/view/View;",
            "LX/CrQ;",
            "Ljava/lang/Class",
            "<TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 1940995
    invoke-virtual {p0, p1}, LX/CrT;->a(Landroid/view/View;)LX/CrR;

    move-result-object v0

    .line 1940996
    if-eqz v0, :cond_0

    .line 1940997
    iget-object p0, v0, LX/CrR;->a:Ljava/util/Map;

    invoke-interface {p0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CqY;

    move-object v0, p0

    .line 1940998
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()LX/Cqv;
    .locals 1

    .prologue
    .line 1940994
    iget-object v0, p0, LX/CrT;->a:LX/Cqv;

    return-object v0
.end method

.method public final a(Landroid/view/View;)LX/CrR;
    .locals 1

    .prologue
    .line 1940993
    iget-object v0, p0, LX/CrT;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CrR;

    return-object v0
.end method

.method public final a(LX/CrS;F)LX/CrS;
    .locals 8

    .prologue
    .line 1941012
    iget-object v0, p0, LX/CrT;->a:LX/Cqv;

    invoke-interface {p1}, LX/CrS;->a()LX/Cqv;

    move-result-object v1

    invoke-interface {v0, v1, p2}, LX/Cqv;->a(LX/Cqv;F)LX/Cqv;

    move-result-object v0

    .line 1941013
    new-instance v2, LX/CrT;

    invoke-direct {v2, v0}, LX/CrT;-><init>(LX/Cqv;)V

    .line 1941014
    iget-object v0, p0, LX/CrT;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1941015
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1941016
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CrR;

    .line 1941017
    invoke-interface {p1, v1}, LX/CrS;->b(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1941018
    invoke-interface {p1, v1}, LX/CrS;->a(Landroid/view/View;)LX/CrR;

    move-result-object v4

    .line 1941019
    new-instance v6, LX/CrR;

    invoke-direct {v6}, LX/CrR;-><init>()V

    .line 1941020
    iget-object v5, v0, LX/CrR;->a:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CqY;

    .line 1941021
    invoke-interface {v5}, LX/CqY;->a()LX/CrQ;

    move-result-object p0

    .line 1941022
    iget-object v0, v4, LX/CrR;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    move p0, v0

    .line 1941023
    if-eqz p0, :cond_1

    .line 1941024
    invoke-interface {v5}, LX/CqY;->a()LX/CrQ;

    move-result-object p0

    .line 1941025
    iget-object v0, v4, LX/CrR;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqY;

    move-object p0, v0

    .line 1941026
    invoke-interface {v5, p0, p2}, LX/CqY;->a(LX/CqY;F)LX/CqY;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/CrR;->a(LX/CqY;)V

    goto :goto_1

    .line 1941027
    :cond_1
    invoke-interface {v5}, LX/CqY;->c()LX/CqY;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/CrR;->a(LX/CqY;)V

    goto :goto_1

    .line 1941028
    :cond_2
    move-object v0, v6

    .line 1941029
    invoke-interface {v2, v1, v0}, LX/CrS;->a(Landroid/view/View;LX/CrR;)V

    goto :goto_0

    .line 1941030
    :cond_3
    return-object v2
.end method

.method public final a(Landroid/view/View;LX/CrR;)V
    .locals 1

    .prologue
    .line 1940991
    iget-object v0, p0, LX/CrT;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1940992
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "LX/CrR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1940990
    iget-object v0, p0, LX/CrT;->b:Ljava/util/Map;

    return-object v0
.end method

.method public final b(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1940989
    iget-object v0, p0, LX/CrT;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()LX/CrS;
    .locals 1

    .prologue
    .line 1940988
    new-instance v0, LX/CrT;

    invoke-direct {v0, p0}, LX/CrT;-><init>(LX/CrS;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1940983
    if-ne p1, p0, :cond_1

    .line 1940984
    :cond_0
    :goto_0
    return v0

    .line 1940985
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1940986
    :cond_3
    check-cast p1, LX/CrT;

    .line 1940987
    iget-object v2, p0, LX/CrT;->a:LX/Cqv;

    iget-object v3, p1, LX/CrT;->a:LX/Cqv;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/CrT;->b:Ljava/util/Map;

    iget-object v3, p1, LX/CrT;->b:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1940969
    iget-object v0, p0, LX/CrT;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/CrT;->a:LX/Cqv;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1940970
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v0, 0x40

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1940971
    const-string v0, "{\n  "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/CrT;->a:LX/Cqv;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1940972
    iget-object v0, p0, LX/CrT;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1940973
    const-string v1, "  "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1940974
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1940975
    const-string v1, ": "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1940976
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CrR;

    invoke-virtual {v0}, LX/CrR;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1940977
    const-string v0, ",\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1940978
    :cond_0
    const-string v0, ",\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 1940979
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 1940980
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1940981
    :cond_1
    const-string v0, "}"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1940982
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
