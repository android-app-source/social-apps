.class public final LX/EI5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/EI8;

.field public final b:Z

.field public final c:Z

.field public final d:I

.field public final e:LX/EGE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/EI8;ZZILX/EGE;)V
    .locals 0

    .prologue
    .line 2100554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2100555
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2100556
    iput-object p1, p0, LX/EI5;->a:LX/EI8;

    .line 2100557
    iput-boolean p2, p0, LX/EI5;->b:Z

    .line 2100558
    iput-boolean p3, p0, LX/EI5;->c:Z

    .line 2100559
    iput p4, p0, LX/EI5;->d:I

    .line 2100560
    iput-object p5, p0, LX/EI5;->e:LX/EGE;

    .line 2100561
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2100562
    if-ne p0, p1, :cond_1

    .line 2100563
    :cond_0
    :goto_0
    return v0

    .line 2100564
    :cond_1
    instance-of v2, p1, LX/EI5;

    if-eqz v2, :cond_3

    .line 2100565
    check-cast p1, LX/EI5;

    .line 2100566
    iget-object v2, p0, LX/EI5;->a:LX/EI8;

    iget-object v3, p1, LX/EI5;->a:LX/EI8;

    invoke-virtual {v2, v3}, LX/EI8;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, LX/EI5;->b:Z

    iget-boolean v3, p1, LX/EI5;->b:Z

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, LX/EI5;->c:Z

    iget-boolean v3, p1, LX/EI5;->c:Z

    if-ne v2, v3, :cond_2

    iget v2, p0, LX/EI5;->d:I

    iget v3, p1, LX/EI5;->d:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, LX/EI5;->e:LX/EGE;

    iget-object v3, p1, LX/EI5;->e:LX/EGE;

    .line 2100567
    if-nez v2, :cond_5

    .line 2100568
    if-nez v3, :cond_4

    const/4 p0, 0x1

    .line 2100569
    :goto_1
    move v2, p0

    .line 2100570
    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2100571
    goto :goto_0

    .line 2100572
    :cond_4
    const/4 p0, 0x0

    goto :goto_1

    .line 2100573
    :cond_5
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2100574
    iget-object v0, p0, LX/EI5;->a:LX/EI8;

    invoke-virtual {v0}, LX/EI8;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 2100575
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, LX/EI5;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2100576
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, LX/EI5;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2100577
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/EI5;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2100578
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, LX/EI5;->e:LX/EGE;

    if-nez v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 2100579
    return v0

    .line 2100580
    :cond_0
    iget-object v0, p0, LX/EI5;->e:LX/EGE;

    invoke-virtual {v0}, LX/EGE;->hashCode()I

    move-result v0

    goto :goto_0
.end method
