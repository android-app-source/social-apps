.class public LX/Dyy;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:LX/96B;

.field public b:Landroid/location/Location;

.field public c:Ljava/util/Locale;

.field public d:Ljava/lang/String;

.field public e:LX/9jN;

.field public f:Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

.field public g:LX/Dyv;

.field public h:LX/Dyx;

.field public i:LX/Dz3;

.field public j:LX/Dz1;

.field public k:LX/Dz0;

.field public l:I

.field public final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Dyu;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/9jL;",
            "LX/Dyu;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public p:Z


# direct methods
.method public constructor <init>(Ljava/util/Locale;LX/96B;Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;LX/Dyx;LX/Dyv;LX/Dz3;LX/Dz1;LX/Dz0;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066114
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2066115
    const-string v0, ""

    iput-object v0, p0, LX/Dyy;->d:Ljava/lang/String;

    .line 2066116
    new-instance v0, LX/9jN;

    invoke-direct {v0}, LX/9jN;-><init>()V

    iput-object v0, p0, LX/Dyy;->e:LX/9jN;

    .line 2066117
    const/4 v0, 0x0

    iput v0, p0, LX/Dyy;->l:I

    .line 2066118
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Dyy;->m:Ljava/util/List;

    .line 2066119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Dyy;->n:Ljava/util/HashMap;

    .line 2066120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    .line 2066121
    iput-object p1, p0, LX/Dyy;->c:Ljava/util/Locale;

    .line 2066122
    iput-object p2, p0, LX/Dyy;->a:LX/96B;

    .line 2066123
    iput-object p3, p0, LX/Dyy;->f:Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    .line 2066124
    iput-object p4, p0, LX/Dyy;->h:LX/Dyx;

    .line 2066125
    iput-object p5, p0, LX/Dyy;->g:LX/Dyv;

    .line 2066126
    iput-object p6, p0, LX/Dyy;->i:LX/Dz3;

    .line 2066127
    iput-object p7, p0, LX/Dyy;->j:LX/Dz1;

    .line 2066128
    iput-object p8, p0, LX/Dyy;->k:LX/Dz0;

    .line 2066129
    iget-object v0, p0, LX/Dyy;->m:Ljava/util/List;

    iget-object v1, p0, LX/Dyy;->j:LX/Dz1;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2066130
    iget-object v0, p0, LX/Dyy;->m:Ljava/util/List;

    iget-object v1, p0, LX/Dyy;->g:LX/Dyv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2066131
    iget-object v0, p0, LX/Dyy;->m:Ljava/util/List;

    iget-object v1, p0, LX/Dyy;->f:Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2066132
    iget-object v0, p0, LX/Dyy;->m:Ljava/util/List;

    iget-object v1, p0, LX/Dyy;->h:LX/Dyx;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2066133
    iget-object v0, p0, LX/Dyy;->m:Ljava/util/List;

    iget-object v1, p0, LX/Dyy;->i:LX/Dz3;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2066134
    iget-object v0, p0, LX/Dyy;->m:Ljava/util/List;

    iget-object v1, p0, LX/Dyy;->k:LX/Dz0;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2066135
    const/4 v0, 0x0

    .line 2066136
    iget-object v1, p0, LX/Dyy;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dyu;

    .line 2066137
    invoke-virtual {v0}, LX/Dyu;->c()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2066138
    invoke-virtual {v0}, LX/Dyu;->b()LX/9jL;

    move-result-object p2

    invoke-static {p0, p2, v0}, LX/Dyy;->a(LX/Dyy;LX/9jL;LX/Dyu;)V

    .line 2066139
    iget-object p2, p0, LX/Dyy;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, LX/Dyu;->b()LX/9jL;

    move-result-object p3

    invoke-virtual {p2, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2066140
    add-int/lit8 v1, v1, 0x1

    .line 2066141
    :cond_0
    invoke-virtual {v0}, LX/Dyu;->a()LX/9jL;

    move-result-object p2

    invoke-static {p0, p2, v0}, LX/Dyy;->a(LX/Dyy;LX/9jL;LX/Dyu;)V

    .line 2066142
    iget-object p2, p0, LX/Dyy;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, LX/Dyu;->a()LX/9jL;

    move-result-object p3

    invoke-virtual {p2, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2066143
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2066144
    goto :goto_0

    .line 2066145
    :cond_1
    iput v1, p0, LX/Dyy;->l:I

    .line 2066146
    return-void
.end method

.method public static a(LX/0QB;)LX/Dyy;
    .locals 1

    .prologue
    .line 2066199
    invoke-static {p0}, LX/Dyy;->b(LX/0QB;)LX/Dyy;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/Dyy;LX/9jL;LX/Dyu;)V
    .locals 3

    .prologue
    .line 2066194
    sget-object v0, LX/9jL;->Undefined:LX/9jL;

    if-ne p1, v0, :cond_0

    .line 2066195
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not define the correct type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2066196
    :cond_0
    iget-object v0, p0, LX/Dyy;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2066197
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " declared a type already used: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2066198
    :cond_1
    return-void
.end method

.method private static b(LX/0QB;)LX/Dyy;
    .locals 15

    .prologue
    .line 2066177
    new-instance v0, LX/Dyy;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {p0}, LX/96B;->a(LX/0QB;)LX/96B;

    move-result-object v2

    check-cast v2, LX/96B;

    invoke-static {p0}, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->b(LX/0QB;)Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    move-result-object v3

    check-cast v3, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    .line 2066178
    new-instance v7, LX/Dyx;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    const-class v5, Landroid/content/Context;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {p0}, LX/96B;->a(LX/0QB;)LX/96B;

    move-result-object v6

    check-cast v6, LX/96B;

    invoke-direct {v7, v4, v5, v6}, LX/Dyx;-><init>(Landroid/view/LayoutInflater;Landroid/content/Context;LX/96B;)V

    .line 2066179
    move-object v4, v7

    .line 2066180
    check-cast v4, LX/Dyx;

    .line 2066181
    new-instance v6, LX/Dyv;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    invoke-direct {v6, v5}, LX/Dyv;-><init>(Landroid/view/LayoutInflater;)V

    .line 2066182
    move-object v5, v6

    .line 2066183
    check-cast v5, LX/Dyv;

    .line 2066184
    new-instance v7, LX/Dz3;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    invoke-direct {v7, v6}, LX/Dz3;-><init>(Landroid/view/LayoutInflater;)V

    .line 2066185
    move-object v6, v7

    .line 2066186
    check-cast v6, LX/Dz3;

    .line 2066187
    new-instance v9, LX/Dz1;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    const-class v11, Landroid/content/Context;

    invoke-interface {p0, v11}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-static {p0}, LX/DzM;->a(LX/0QB;)LX/DzM;

    move-result-object v12

    check-cast v12, LX/DzM;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v13

    check-cast v13, Ljava/util/Locale;

    invoke-static {p0}, LX/9j5;->a(LX/0QB;)LX/9j5;

    move-result-object v14

    check-cast v14, LX/9j5;

    invoke-direct/range {v9 .. v14}, LX/Dz1;-><init>(LX/0ad;Landroid/content/Context;LX/DzM;Ljava/util/Locale;LX/9j5;)V

    .line 2066188
    move-object v7, v9

    .line 2066189
    check-cast v7, LX/Dz1;

    .line 2066190
    new-instance v9, LX/Dz0;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v8

    check-cast v8, Landroid/view/LayoutInflater;

    invoke-direct {v9, v8}, LX/Dz0;-><init>(Landroid/view/LayoutInflater;)V

    .line 2066191
    move-object v8, v9

    .line 2066192
    check-cast v8, LX/Dz0;

    invoke-direct/range {v0 .. v8}, LX/Dyy;-><init>(Ljava/util/Locale;LX/96B;Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;LX/Dyx;LX/Dyv;LX/Dz3;LX/Dz1;LX/Dz0;)V

    .line 2066193
    return-object v0
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2066172
    iget-object v0, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    iget-object v0, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2066173
    iget-object v1, p0, LX/Dyy;->n:Ljava/util/HashMap;

    iget-object v5, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dyu;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v0}, LX/Dyu;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 2066174
    :goto_1
    return v0

    .line 2066175
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2066176
    :cond_1
    invoke-super {p0}, Landroid/widget/BaseAdapter;->areAllItemsEnabled()Z

    move-result v0

    goto :goto_1
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2066171
    iget-object v0, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2066170
    iget-object v0, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2066169
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2066166
    iget-object v0, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 2066167
    const/4 v0, -0x1

    .line 2066168
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/9jL;

    invoke-virtual {v0}, LX/9jL;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2066164
    iget-object v1, p0, LX/Dyy;->n:Ljava/util/HashMap;

    iget-object v0, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dyu;

    .line 2066165
    iget-object v1, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v0, p2, p3, v1}, LX/Dyu;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2066163
    iget v0, p0, LX/Dyy;->l:I

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 3

    .prologue
    .line 2066161
    iget-object v0, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2066162
    iget-object v1, p0, LX/Dyy;->n:Ljava/util/HashMap;

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dyu;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v0}, LX/Dyu;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 5

    .prologue
    .line 2066147
    iget-object v0, p0, LX/Dyy;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dyu;

    .line 2066148
    invoke-virtual {v0}, LX/Dyu;->d()V

    goto :goto_0

    .line 2066149
    :cond_0
    iget-object v0, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2066150
    iget-object v0, p0, LX/Dyy;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dyu;

    .line 2066151
    iget-object v2, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, LX/Dyu;->a(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 2066152
    :cond_1
    iget-object v0, p0, LX/Dyy;->e:LX/9jN;

    .line 2066153
    iget-object v1, v0, LX/9jN;->b:Ljava/util/List;

    move-object v0, v1

    .line 2066154
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2066155
    iget-object v1, p0, LX/Dyy;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dyu;

    .line 2066156
    iget-object v4, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v4}, LX/Dyu;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_2

    .line 2066157
    :cond_3
    iget-object v0, p0, LX/Dyy;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dyu;

    .line 2066158
    iget-object v2, p0, LX/Dyy;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, LX/Dyu;->b(Ljava/util/ArrayList;)V

    goto :goto_3

    .line 2066159
    :cond_4
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 2066160
    return-void
.end method
