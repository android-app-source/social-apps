.class public LX/DFk;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DFi;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DFl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1978692
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/DFk;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DFl;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978725
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1978726
    iput-object p1, p0, LX/DFk;->b:LX/0Ot;

    .line 1978727
    return-void
.end method

.method public static a(LX/0QB;)LX/DFk;
    .locals 4

    .prologue
    .line 1978714
    const-class v1, LX/DFk;

    monitor-enter v1

    .line 1978715
    :try_start_0
    sget-object v0, LX/DFk;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978716
    sput-object v2, LX/DFk;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978717
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978718
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978719
    new-instance v3, LX/DFk;

    const/16 p0, 0x20ed

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DFk;-><init>(LX/0Ot;)V

    .line 1978720
    move-object v0, v3

    .line 1978721
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978722
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978723
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1978713
    const v0, -0x10540935

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1978702
    iget-object v0, p0, LX/DFk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DFl;

    .line 1978703
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const v1, 0x7f020a3d

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b0f68

    invoke-interface {v0, v1}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b0f67

    invoke-interface {v0, v1}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    .line 1978704
    const/4 v2, 0x0

    const v3, 0x103005b

    invoke-static {p1, v2, v3}, LX/1o2;->a(LX/1De;II)LX/1o5;

    move-result-object v2

    const v3, 0x7f020af2

    invoke-virtual {v2, v3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0f69

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f081867

    invoke-interface {v2, v3}, LX/1Di;->A(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    move-object v2, v2

    .line 1978705
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 1978706
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v2

    const v3, 0x7f0a0464

    invoke-virtual {v2, v3}, LX/25Q;->i(I)LX/25Q;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x4

    invoke-interface {v2, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    move-object v2, v2

    .line 1978707
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const/4 p2, 0x1

    .line 1978708
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    .line 1978709
    const v3, -0x10540935

    const/4 p0, 0x0

    invoke-static {p1, v3, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1978710
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const p0, 0x7f081868

    invoke-virtual {v3, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    const p0, 0x7f0b0052

    invoke-virtual {v3, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const p0, 0x7f0a010c

    invoke-virtual {v3, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const p0, 0x7f081867

    invoke-interface {v3, p0}, LX/1Di;->A(I)LX/1Di;

    move-result-object v3

    const/4 p0, 0x2

    invoke-interface {v3, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    move-object v2, v2

    .line 1978711
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1978712
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1978693
    invoke-static {}, LX/1dS;->b()V

    .line 1978694
    iget v0, p1, LX/1dQ;->b:I

    .line 1978695
    packed-switch v0, :pswitch_data_0

    .line 1978696
    :goto_0
    return-object v2

    .line 1978697
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1978698
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 1978699
    iget-object p1, p0, LX/DFk;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DFl;

    .line 1978700
    iget-object p0, p1, LX/DFl;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/pymk/rows/PymkSeeAllPartDefinition;->a(Landroid/view/View;Lcom/facebook/intent/feed/IFeedIntentBuilder;)V

    .line 1978701
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x10540935
        :pswitch_0
    .end packed-switch
.end method
