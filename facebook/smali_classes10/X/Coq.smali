.class public LX/Coq;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CoX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Cnd;",
        ">;",
        "LX/CoX;"
    }
.end annotation


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1936120
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1936121
    const-class v0, LX/Coq;

    invoke-static {v0, p0}, LX/Coq;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936122
    const v0, 0x7f0d2a3b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Coq;->b:Landroid/view/View;

    .line 1936123
    new-instance v0, LX/Cmz;

    new-instance v1, LX/CnB;

    iget-object v2, p0, LX/Coq;->a:LX/Cju;

    invoke-direct {v1, v2}, LX/CnB;-><init>(LX/Cju;)V

    new-instance v2, LX/Cop;

    invoke-direct {v2}, LX/Cop;-><init>()V

    invoke-direct {v0, v1, v2, v4, v4}, LX/Cmz;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;)V

    .line 1936124
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1936125
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Coq;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object p0

    check-cast p0, LX/Cju;

    iput-object p0, p1, LX/Coq;->a:LX/Cju;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1936126
    iget-object v0, p0, LX/Coq;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1936127
    iget-object v0, p0, LX/Coq;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1936128
    iget-object v0, p0, LX/Coq;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1936129
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1936130
    iget-object v0, p0, LX/Coq;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1936131
    iget-object v0, p0, LX/Coq;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1936132
    iget-object v0, p0, LX/Coq;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1936133
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 1936134
    iget-object v0, p0, LX/Coq;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1936135
    iget-object v0, p0, LX/Coq;->b:Landroid/view/View;

    invoke-static {v0, p1}, LX/8ba;->a(Landroid/view/View;I)V

    .line 1936136
    :cond_0
    return-void
.end method
