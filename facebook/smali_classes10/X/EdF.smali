.class public final LX/EdF;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public final synthetic a:LX/EdG;


# direct methods
.method public constructor <init>(LX/EdG;)V
    .locals 0

    .prologue
    .line 2148921
    iput-object p1, p0, LX/EdF;->a:LX/EdG;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x5d69ae5d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2148906
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2148907
    const/16 v1, 0x27

    const v2, -0x60fb711b

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 2148908
    :goto_0
    return-void

    .line 2148909
    :cond_0
    const/4 v2, -0x1

    .line 2148910
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v1, v4, :cond_2

    .line 2148911
    const-string v1, "networkType"

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2148912
    :goto_1
    move v1, v1

    .line 2148913
    if-eq v1, v3, :cond_1

    .line 2148914
    const v1, 0x5250f67c

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 2148915
    :cond_1
    iget-object v1, p0, LX/EdF;->a:LX/EdG;

    invoke-static {v1, p1, p2}, LX/EdG;->a(LX/EdG;Landroid/content/Context;Landroid/content/Intent;)V

    .line 2148916
    const v1, 0x7c417ffa

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 2148917
    :cond_2
    const-string v1, "networkInfo"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 2148918
    if-eqz v1, :cond_3

    .line 2148919
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    goto :goto_1

    :cond_3
    move v1, v2

    .line 2148920
    goto :goto_1
.end method
