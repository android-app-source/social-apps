.class public final enum LX/CvJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CvJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CvJ;

.field public static final enum CLICK:LX/CvJ;

.field public static final enum COMMERCE_CAROUSEL_INTERACTION:LX/CvJ;

.field public static final enum ERROR:LX/CvJ;

.field public static final enum INLINE_ACTION:LX/CvJ;

.field public static final enum INLINE_ACTION_MESSAGE:LX/CvJ;

.field public static final enum INLINE_EVENT_JOIN:LX/CvJ;

.field public static final enum INLINE_FRIEND_REQUEST:LX/CvJ;

.field public static final enum INLINE_FRIEND_REQUEST_CANCEL:LX/CvJ;

.field public static final enum INLINE_FRIEND_REQUEST_CONFIRM:LX/CvJ;

.field public static final enum INLINE_GROUP_JOIN:LX/CvJ;

.field public static final enum INLINE_PAGE_LIKE:LX/CvJ;

.field public static final enum ITEM_IN_MODULE_TAPPED:LX/CvJ;

.field public static final enum ITEM_TAPPED:LX/CvJ;

.field public static final enum MODULE_IMPRESSION:LX/CvJ;

.field public static final enum PLACE_MAP_INTERACTION:LX/CvJ;

.field public static final enum PLACE_SAVE_INTERACTION:LX/CvJ;

.field public static final enum PLACE_UNSAVE_INTERACTION:LX/CvJ;

.field public static final enum PROFILE_SNAPSHOT_INTERACTION:LX/CvJ;

.field public static final enum PULL_TO_REFRESH:LX/CvJ;

.field public static final enum RESULTS_FILTER:LX/CvJ;

.field public static final enum RESULTS_LOADED:LX/CvJ;

.field public static final enum RESULTS_PAGED:LX/CvJ;

.field public static final enum SEE_MORE_ON_MODULE_TAPPED:LX/CvJ;

.field public static final enum SEE_MORE_POSTS_TAPPED:LX/CvJ;

.field public static final enum VIEW_PORT_VIEWS:LX/CvJ;

.field public static final enum WEATHER_FORECAST_INTERACTION:LX/CvJ;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1948032
    new-instance v0, LX/CvJ;

    const-string v1, "RESULTS_LOADED"

    const-string v2, "search"

    invoke-direct {v0, v1, v4, v2}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->RESULTS_LOADED:LX/CvJ;

    .line 1948033
    new-instance v0, LX/CvJ;

    const-string v1, "RESULTS_PAGED"

    const-string v2, "graph_search_results_paged"

    invoke-direct {v0, v1, v5, v2}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->RESULTS_PAGED:LX/CvJ;

    .line 1948034
    new-instance v0, LX/CvJ;

    const-string v1, "ERROR"

    const-string v2, "graph_search_results_error"

    invoke-direct {v0, v1, v6, v2}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->ERROR:LX/CvJ;

    .line 1948035
    new-instance v0, LX/CvJ;

    const-string v1, "RESULTS_FILTER"

    const-string v2, "graph_search_results_filter"

    invoke-direct {v0, v1, v7, v2}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->RESULTS_FILTER:LX/CvJ;

    .line 1948036
    new-instance v0, LX/CvJ;

    const-string v1, "MODULE_IMPRESSION"

    const-string v2, "module_impression"

    invoke-direct {v0, v1, v8, v2}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->MODULE_IMPRESSION:LX/CvJ;

    .line 1948037
    new-instance v0, LX/CvJ;

    const-string v1, "INLINE_ACTION"

    const/4 v2, 0x5

    const-string v3, "inline_action"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->INLINE_ACTION:LX/CvJ;

    .line 1948038
    new-instance v0, LX/CvJ;

    const-string v1, "ITEM_TAPPED"

    const/4 v2, 0x6

    const-string v3, "graph_search_results_item_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->ITEM_TAPPED:LX/CvJ;

    .line 1948039
    new-instance v0, LX/CvJ;

    const-string v1, "PULL_TO_REFRESH"

    const/4 v2, 0x7

    const-string v3, "graph_search_results_pull_to_refresh"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->PULL_TO_REFRESH:LX/CvJ;

    .line 1948040
    new-instance v0, LX/CvJ;

    const-string v1, "ITEM_IN_MODULE_TAPPED"

    const/16 v2, 0x8

    const-string v3, "graph_search_results_item_in_module_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->ITEM_IN_MODULE_TAPPED:LX/CvJ;

    .line 1948041
    new-instance v0, LX/CvJ;

    const-string v1, "SEE_MORE_ON_MODULE_TAPPED"

    const/16 v2, 0x9

    const-string v3, "graph_search_results_see_more_on_module_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->SEE_MORE_ON_MODULE_TAPPED:LX/CvJ;

    .line 1948042
    new-instance v0, LX/CvJ;

    const-string v1, "SEE_MORE_POSTS_TAPPED"

    const/16 v2, 0xa

    const-string v3, "graph_search_results_see_more_posts_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->SEE_MORE_POSTS_TAPPED:LX/CvJ;

    .line 1948043
    new-instance v0, LX/CvJ;

    const-string v1, "CLICK"

    const/16 v2, 0xb

    const-string v3, "click"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->CLICK:LX/CvJ;

    .line 1948044
    new-instance v0, LX/CvJ;

    const-string v1, "VIEW_PORT_VIEWS"

    const/16 v2, 0xc

    const-string v3, "view_port_views_on_search_results"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->VIEW_PORT_VIEWS:LX/CvJ;

    .line 1948045
    new-instance v0, LX/CvJ;

    const-string v1, "COMMERCE_CAROUSEL_INTERACTION"

    const/16 v2, 0xd

    const-string v3, "commerce_carousel_interaction"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->COMMERCE_CAROUSEL_INTERACTION:LX/CvJ;

    .line 1948046
    new-instance v0, LX/CvJ;

    const-string v1, "PLACE_SAVE_INTERACTION"

    const/16 v2, 0xe

    const-string v3, "inline_action_save_click"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->PLACE_SAVE_INTERACTION:LX/CvJ;

    .line 1948047
    new-instance v0, LX/CvJ;

    const-string v1, "PLACE_UNSAVE_INTERACTION"

    const/16 v2, 0xf

    const-string v3, "inline_action_unsave_click"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->PLACE_UNSAVE_INTERACTION:LX/CvJ;

    .line 1948048
    new-instance v0, LX/CvJ;

    const-string v1, "PLACE_MAP_INTERACTION"

    const/16 v2, 0x10

    const-string v3, "graph_search_results_map_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->PLACE_MAP_INTERACTION:LX/CvJ;

    .line 1948049
    new-instance v0, LX/CvJ;

    const-string v1, "WEATHER_FORECAST_INTERACTION"

    const/16 v2, 0x11

    const-string v3, "weather_hourly_forecast_interaction"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->WEATHER_FORECAST_INTERACTION:LX/CvJ;

    .line 1948050
    new-instance v0, LX/CvJ;

    const-string v1, "PROFILE_SNAPSHOT_INTERACTION"

    const/16 v2, 0x12

    const-string v3, "profile_snapshot_interaction"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->PROFILE_SNAPSHOT_INTERACTION:LX/CvJ;

    .line 1948051
    new-instance v0, LX/CvJ;

    const-string v1, "INLINE_FRIEND_REQUEST"

    const/16 v2, 0x13

    const-string v3, "inline_friend_request"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->INLINE_FRIEND_REQUEST:LX/CvJ;

    .line 1948052
    new-instance v0, LX/CvJ;

    const-string v1, "INLINE_FRIEND_REQUEST_CANCEL"

    const/16 v2, 0x14

    const-string v3, "inline_cancel_friend_request"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->INLINE_FRIEND_REQUEST_CANCEL:LX/CvJ;

    .line 1948053
    new-instance v0, LX/CvJ;

    const-string v1, "INLINE_FRIEND_REQUEST_CONFIRM"

    const/16 v2, 0x15

    const-string v3, "inline_confirm_friend_request"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->INLINE_FRIEND_REQUEST_CONFIRM:LX/CvJ;

    .line 1948054
    new-instance v0, LX/CvJ;

    const-string v1, "INLINE_ACTION_MESSAGE"

    const/16 v2, 0x16

    const-string v3, "inline_action_message"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->INLINE_ACTION_MESSAGE:LX/CvJ;

    .line 1948055
    new-instance v0, LX/CvJ;

    const-string v1, "INLINE_PAGE_LIKE"

    const/16 v2, 0x17

    const-string v3, "inline_page_like_request"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->INLINE_PAGE_LIKE:LX/CvJ;

    .line 1948056
    new-instance v0, LX/CvJ;

    const-string v1, "INLINE_GROUP_JOIN"

    const/16 v2, 0x18

    const-string v3, "inline_action_join_group_click"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->INLINE_GROUP_JOIN:LX/CvJ;

    .line 1948057
    new-instance v0, LX/CvJ;

    const-string v1, "INLINE_EVENT_JOIN"

    const/16 v2, 0x19

    const-string v3, "inline_action_join_event_click"

    invoke-direct {v0, v1, v2, v3}, LX/CvJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/CvJ;->INLINE_EVENT_JOIN:LX/CvJ;

    .line 1948058
    const/16 v0, 0x1a

    new-array v0, v0, [LX/CvJ;

    sget-object v1, LX/CvJ;->RESULTS_LOADED:LX/CvJ;

    aput-object v1, v0, v4

    sget-object v1, LX/CvJ;->RESULTS_PAGED:LX/CvJ;

    aput-object v1, v0, v5

    sget-object v1, LX/CvJ;->ERROR:LX/CvJ;

    aput-object v1, v0, v6

    sget-object v1, LX/CvJ;->RESULTS_FILTER:LX/CvJ;

    aput-object v1, v0, v7

    sget-object v1, LX/CvJ;->MODULE_IMPRESSION:LX/CvJ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/CvJ;->INLINE_ACTION:LX/CvJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CvJ;->ITEM_TAPPED:LX/CvJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CvJ;->PULL_TO_REFRESH:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/CvJ;->ITEM_IN_MODULE_TAPPED:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/CvJ;->SEE_MORE_ON_MODULE_TAPPED:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/CvJ;->SEE_MORE_POSTS_TAPPED:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/CvJ;->CLICK:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/CvJ;->VIEW_PORT_VIEWS:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/CvJ;->COMMERCE_CAROUSEL_INTERACTION:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/CvJ;->PLACE_SAVE_INTERACTION:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/CvJ;->PLACE_UNSAVE_INTERACTION:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/CvJ;->PLACE_MAP_INTERACTION:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/CvJ;->WEATHER_FORECAST_INTERACTION:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/CvJ;->PROFILE_SNAPSHOT_INTERACTION:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/CvJ;->INLINE_FRIEND_REQUEST:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/CvJ;->INLINE_FRIEND_REQUEST_CANCEL:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/CvJ;->INLINE_FRIEND_REQUEST_CONFIRM:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/CvJ;->INLINE_ACTION_MESSAGE:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/CvJ;->INLINE_PAGE_LIKE:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/CvJ;->INLINE_GROUP_JOIN:LX/CvJ;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/CvJ;->INLINE_EVENT_JOIN:LX/CvJ;

    aput-object v2, v0, v1

    sput-object v0, LX/CvJ;->$VALUES:[LX/CvJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1948029
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1948030
    iput-object p3, p0, LX/CvJ;->name:Ljava/lang/String;

    .line 1948031
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CvJ;
    .locals 1

    .prologue
    .line 1948028
    const-class v0, LX/CvJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CvJ;

    return-object v0
.end method

.method public static values()[LX/CvJ;
    .locals 1

    .prologue
    .line 1948027
    sget-object v0, LX/CvJ;->$VALUES:[LX/CvJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CvJ;

    return-object v0
.end method
