.class public LX/CnZ;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/AuthorsBlockView;",
        "Lcom/facebook/richdocument/model/data/AuthorBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalPageLiker;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalFriending;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field public i:Z

.field private j:Z

.field public k:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;)V
    .locals 4

    .prologue
    .line 1933924
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1933925
    const-string v0, "Page"

    iput-object v0, p0, LX/CnZ;->g:Ljava/lang/String;

    .line 1933926
    const-string v0, "User"

    iput-object v0, p0, LX/CnZ;->h:Ljava/lang/String;

    .line 1933927
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, LX/CnZ;

    invoke-static {v3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    const/16 p1, 0x3225

    invoke-static {v3, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    const/16 v0, 0x3223

    invoke-static {v3, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    iput-object v2, p0, LX/CnZ;->d:LX/1Ck;

    iput-object p1, p0, LX/CnZ;->e:LX/0Ot;

    iput-object v3, p0, LX/CnZ;->f:LX/0Ot;

    .line 1933928
    return-void
.end method

.method public static b(LX/CnZ;)V
    .locals 3

    .prologue
    .line 1933929
    iget-boolean v0, p0, LX/CnZ;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/CnZ;->i:Z

    .line 1933930
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933931
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    iget-boolean v1, p0, LX/CnZ;->j:Z

    iget-boolean v2, p0, LX/CnZ;->i:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->a(ZZ)V

    .line 1933932
    return-void

    .line 1933933
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/CnZ;)V
    .locals 2

    .prologue
    .line 1933934
    sget-object v0, LX/CnY;->a:[I

    iget-object v1, p0, LX/CnZ;->k:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1933935
    :goto_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933936
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    iget-object v1, p0, LX/CnZ;->k:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 1933937
    return-void

    .line 1933938
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, LX/CnZ;->k:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0

    .line 1933939
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, LX/CnZ;->k:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 7

    .prologue
    .line 1933940
    check-cast p1, LX/Cm6;

    const/4 v1, 0x0

    .line 1933941
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933942
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1933943
    iget-object v0, p1, LX/Cm6;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;

    move-result-object v0

    move-object v2, v0

    .line 1933944
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933945
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    .line 1933946
    iget-object v3, p1, LX/Cm6;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel;

    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel;->c()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 1933947
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 1933948
    iget-object v5, p1, LX/Cm6;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel;

    invoke-virtual {v5}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel;->b()LX/8Z4;

    move-result-object v5

    move-object v5, v5

    .line 1933949
    sget-object v6, LX/Clb;->CREDITS:LX/Clb;

    .line 1933950
    new-instance p1, LX/Cle;

    invoke-direct {p1, v4}, LX/Cle;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v5}, LX/Cle;->a(LX/8Z4;)LX/Cle;

    move-result-object p1

    invoke-virtual {p1, v6}, LX/Cle;->a(LX/Clb;)LX/Cle;

    move-result-object p1

    invoke-virtual {p1}, LX/Cle;->a()LX/Clf;

    move-result-object p1

    move-object v4, p1

    .line 1933951
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->eb_()LX/1Fb;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->eb_()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 1933952
    :cond_0
    iget-object v5, v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->d:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1933953
    iget-object v5, v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1933954
    iget-object v6, v5, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v5, v6

    .line 1933955
    invoke-virtual {v5, v4}, LX/CtG;->setText(LX/Clf;)V

    .line 1933956
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1933957
    iget-object v5, v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1933958
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->ej_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->ej_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1933959
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->ej_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Page"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1933960
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/CnZ;->j:Z

    .line 1933961
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->c()Z

    move-result v0

    iput-boolean v0, p0, LX/CnZ;->i:Z

    .line 1933962
    iget-object v0, p0, LX/CnZ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1933963
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933964
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    iget-boolean v1, p0, LX/CnZ;->j:Z

    iget-boolean v3, p0, LX/CnZ;->i:Z

    invoke-virtual {v0, v1, v3}, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->a(ZZ)V

    .line 1933965
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933966
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    new-instance v1, LX/CnV;

    invoke-direct {v1, p0, v2}, LX/CnV;-><init>(LX/CnZ;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;)V

    .line 1933967
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->h:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v2, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1933968
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->h:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    .line 1933969
    :cond_1
    :goto_1
    return-void

    .line 1933970
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->ej_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "User"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CnZ;->f:LX/0Ot;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CnZ;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1933971
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;->j()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p0, LX/CnZ;->k:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1933972
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933973
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    iget-object v1, p0, LX/CnZ;->k:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 1933974
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1933975
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    new-instance v1, LX/CnX;

    invoke-direct {v1, p0, v2}, LX/CnX;-><init>(LX/CnZ;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel$ProfileModel;)V

    .line 1933976
    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->i:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v2, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1933977
    goto :goto_1

    .line 1933978
    :cond_3
    iget-object v5, v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    sget-object p1, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_0
.end method
