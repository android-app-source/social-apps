.class public LX/DT7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/0tX;

.field public final e:LX/0kL;

.field public final f:LX/DTh;

.field private final g:LX/DOK;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0kL;LX/DTh;LX/DOK;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2000180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2000181
    iput-object p1, p0, LX/DT7;->c:Landroid/content/res/Resources;

    .line 2000182
    iput-object p2, p0, LX/DT7;->a:Ljava/lang/String;

    .line 2000183
    iput-object p3, p0, LX/DT7;->b:Ljava/util/concurrent/ExecutorService;

    .line 2000184
    iput-object p4, p0, LX/DT7;->d:LX/0tX;

    .line 2000185
    iput-object p5, p0, LX/DT7;->e:LX/0kL;

    .line 2000186
    iput-object p6, p0, LX/DT7;->f:LX/DTh;

    .line 2000187
    iput-object p7, p0, LX/DT7;->g:LX/DOK;

    .line 2000188
    return-void
.end method

.method public static a$redex0(LX/DT7;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)V
    .locals 2

    .prologue
    .line 2000160
    iget-object v0, p0, LX/DT7;->f:LX/DTh;

    new-instance v1, LX/DTi;

    invoke-direct {v1, p1, p2, p3}, LX/DTi;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2000161
    iget-object v0, p0, LX/DT7;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2000162
    iget-object v0, p0, LX/DT7;->g:LX/DOK;

    invoke-virtual {v0, p1, p3}, LX/DOK;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)V

    .line 2000163
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/DT7;
    .locals 8

    .prologue
    .line 2000178
    new-instance v0, LX/DT7;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {p0}, LX/DTh;->a(LX/0QB;)LX/DTh;

    move-result-object v6

    check-cast v6, LX/DTh;

    invoke-static {p0}, LX/DOK;->a(LX/0QB;)LX/DOK;

    move-result-object v7

    check-cast v7, LX/DOK;

    invoke-direct/range {v0 .. v7}, LX/DT7;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0kL;LX/DTh;LX/DOK;)V

    .line 2000179
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2000170
    new-instance v0, LX/4FZ;

    invoke-direct {v0}, LX/4FZ;-><init>()V

    iget-object v1, p0, LX/DT7;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4FZ;->a(Ljava/lang/String;)LX/4FZ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4FZ;->b(Ljava/lang/String;)LX/4FZ;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4FZ;->c(Ljava/lang/String;)LX/4FZ;

    move-result-object v0

    const-string v1, "moderator"

    .line 2000171
    const-string v2, "admin_type"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000172
    move-object v0, v0

    .line 2000173
    invoke-static {}, LX/DV9;->a()LX/DV3;

    move-result-object v1

    .line 2000174
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2000175
    iget-object v0, p0, LX/DT7;->d:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2000176
    new-instance v1, LX/DT0;

    invoke-direct {v1, p0, p1, p2}, LX/DT0;-><init>(LX/DT7;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, LX/DT7;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2000177
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2000164
    new-instance v0, LX/4FZ;

    invoke-direct {v0}, LX/4FZ;-><init>()V

    iget-object v1, p0, LX/DT7;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4FZ;->a(Ljava/lang/String;)LX/4FZ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4FZ;->b(Ljava/lang/String;)LX/4FZ;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4FZ;->c(Ljava/lang/String;)LX/4FZ;

    move-result-object v0

    .line 2000165
    invoke-static {}, LX/DV9;->a()LX/DV3;

    move-result-object v1

    .line 2000166
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2000167
    iget-object v0, p0, LX/DT7;->d:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2000168
    new-instance v1, LX/DT3;

    invoke-direct {v1, p0, p1, p2}, LX/DT3;-><init>(LX/DT7;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, LX/DT7;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2000169
    return-void
.end method
