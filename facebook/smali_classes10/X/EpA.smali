.class public LX/EpA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/EpA;


# instance fields
.field public final a:LX/EoI;

.field public final b:Landroid/content/res/Resources;

.field private final c:LX/0hB;


# direct methods
.method public constructor <init>(LX/EoI;Landroid/content/res/Resources;LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2170050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170051
    iput-object p1, p0, LX/EpA;->a:LX/EoI;

    .line 2170052
    iput-object p2, p0, LX/EpA;->b:Landroid/content/res/Resources;

    .line 2170053
    iput-object p3, p0, LX/EpA;->c:LX/0hB;

    .line 2170054
    return-void
.end method

.method public static a(LX/0QB;)LX/EpA;
    .locals 6

    .prologue
    .line 2170055
    sget-object v0, LX/EpA;->d:LX/EpA;

    if-nez v0, :cond_1

    .line 2170056
    const-class v1, LX/EpA;

    monitor-enter v1

    .line 2170057
    :try_start_0
    sget-object v0, LX/EpA;->d:LX/EpA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2170058
    if-eqz v2, :cond_0

    .line 2170059
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2170060
    new-instance p0, LX/EpA;

    invoke-static {v0}, LX/EoI;->a(LX/0QB;)LX/EoI;

    move-result-object v3

    check-cast v3, LX/EoI;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v5

    check-cast v5, LX/0hB;

    invoke-direct {p0, v3, v4, v5}, LX/EpA;-><init>(LX/EoI;Landroid/content/res/Resources;LX/0hB;)V

    .line 2170061
    move-object v0, p0

    .line 2170062
    sput-object v0, LX/EpA;->d:LX/EpA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2170063
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2170064
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2170065
    :cond_1
    sget-object v0, LX/EpA;->d:LX/EpA;

    return-object v0

    .line 2170066
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2170067
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static g(LX/EpA;)I
    .locals 2

    .prologue
    .line 2170068
    iget-object v0, p0, LX/EpA;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b2225

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public static h(LX/EpA;)I
    .locals 2

    .prologue
    .line 2170049
    iget-object v0, p0, LX/EpA;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b2227

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2170042
    iget-object v0, p0, LX/EpA;->a:LX/EoI;

    invoke-virtual {v0}, LX/EoI;->a()I

    move-result v0

    move v0, v0

    .line 2170043
    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2170048
    iget-object v0, p0, LX/EpA;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b2216

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 2170044
    iget-object v0, p0, LX/EpA;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b2211

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, LX/EpA;->a()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {p0}, LX/EpA;->g(LX/EpA;)I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {p0}, LX/EpA;->h(LX/EpA;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2170045
    iget-object v1, p0, LX/EpA;->c:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->g()I

    move-result v1

    sub-int v0, v1, v0

    iget-object v1, p0, LX/EpA;->b:Landroid/content/res/Resources;

    const v2, 0x7f0b2227

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    div-int/2addr v0, v1

    .line 2170046
    const/4 v1, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2170047
    return v0
.end method
