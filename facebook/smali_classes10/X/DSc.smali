.class public final enum LX/DSc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DSc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DSc;

.field public static final enum BLOCKED:LX/DSc;

.field public static final enum NOT_BLOCKED:LX/DSc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1999743
    new-instance v0, LX/DSc;

    const-string v1, "BLOCKED"

    invoke-direct {v0, v1, v2}, LX/DSc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSc;->BLOCKED:LX/DSc;

    .line 1999744
    new-instance v0, LX/DSc;

    const-string v1, "NOT_BLOCKED"

    invoke-direct {v0, v1, v3}, LX/DSc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DSc;->NOT_BLOCKED:LX/DSc;

    .line 1999745
    const/4 v0, 0x2

    new-array v0, v0, [LX/DSc;

    sget-object v1, LX/DSc;->BLOCKED:LX/DSc;

    aput-object v1, v0, v2

    sget-object v1, LX/DSc;->NOT_BLOCKED:LX/DSc;

    aput-object v1, v0, v3

    sput-object v0, LX/DSc;->$VALUES:[LX/DSc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1999746
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DSc;
    .locals 1

    .prologue
    .line 1999747
    const-class v0, LX/DSc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DSc;

    return-object v0
.end method

.method public static values()[LX/DSc;
    .locals 1

    .prologue
    .line 1999748
    sget-object v0, LX/DSc;->$VALUES:[LX/DSc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DSc;

    return-object v0
.end method
