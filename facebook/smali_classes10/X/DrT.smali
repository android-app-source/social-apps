.class public final LX/DrT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

.field public final synthetic b:LX/1Pr;

.field public final synthetic c:LX/Drf;

.field public final synthetic d:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;Lcom/facebook/notifications/settings/data/NotifOptionNode;LX/1Pr;LX/Drf;)V
    .locals 0

    .prologue
    .line 2049476
    iput-object p1, p0, LX/DrT;->d:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;

    iput-object p2, p0, LX/DrT;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    iput-object p3, p0, LX/DrT;->b:LX/1Pr;

    iput-object p4, p0, LX/DrT;->c:LX/Drf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    const v0, 0x58105a01

    invoke-static {v7, v8, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2049477
    check-cast p1, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2049478
    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->isChecked()Z

    move-result v2

    .line 2049479
    iget-object v0, p0, LX/DrT;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 2049480
    iget-object v3, v0, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v0, v3

    .line 2049481
    invoke-interface {v0}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v3

    .line 2049482
    iget-object v0, p0, LX/DrT;->d:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;

    iget-object v4, v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsSelectorPartDefinition;->b:LX/33W;

    iget-object v0, p0, LX/DrT;->b:LX/1Pr;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v5, p0, LX/DrT;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 2049483
    iget-object v6, v5, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v5, v6

    .line 2049484
    sget-object v6, LX/8D0;->MULTI_SELECTOR:LX/8D0;

    invoke-virtual {v4, v0, v5, v6}, LX/33W;->a(Landroid/content/Context;LX/BCO;LX/8D0;)V

    .line 2049485
    iget-object v0, p0, LX/DrT;->c:LX/Drf;

    invoke-virtual {v0, v3, v2}, LX/Drf;->a(Ljava/lang/String;Z)V

    .line 2049486
    iget-object v0, p0, LX/DrT;->b:LX/1Pr;

    check-cast v0, LX/1Pq;

    new-array v2, v8, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    iget-object v4, p0, LX/DrT;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2049487
    const v0, 0x7788a181

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
