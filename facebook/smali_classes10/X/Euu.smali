.class public LX/Euu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I
    .annotation build Lcom/facebook/friending/center/model/FriendsCenterContextMenuItemModel$ContextType;
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
        .annotation build Lcom/facebook/friending/center/model/FriendsCenterContextMenuItemModel$ContextType;
        .end annotation
    .end param

    .prologue
    .line 2180063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2180064
    iput p1, p0, LX/Euu;->a:I

    .line 2180065
    iput-object p2, p0, LX/Euu;->b:Ljava/lang/String;

    .line 2180066
    iput-object p3, p0, LX/Euu;->c:Ljava/lang/String;

    .line 2180067
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2180068
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2180069
    iget v1, p0, LX/Euu;->a:I

    packed-switch v1, :pswitch_data_0

    .line 2180070
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not a valid ContextType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2180071
    :pswitch_0
    const v1, 0x7f0818af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2180072
    :goto_0
    return-object v0

    .line 2180073
    :pswitch_1
    const v1, 0x7f0818b0

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/Euu;->b:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2180074
    :pswitch_2
    const v1, 0x7f0818b1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/Euu;->b:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2180075
    :pswitch_3
    const v1, 0x7f0818b2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/Euu;->b:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2180076
    :pswitch_4
    const v1, 0x7f0818b3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/Euu;->b:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 2180077
    iget v0, p0, LX/Euu;->a:I

    packed-switch v0, :pswitch_data_0

    .line 2180078
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not a valid ContextType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2180079
    :pswitch_0
    const v0, 0x7f0208a6

    .line 2180080
    :goto_0
    return v0

    .line 2180081
    :pswitch_1
    const v0, 0x7f0208d4

    goto :goto_0

    .line 2180082
    :pswitch_2
    const v0, 0x7f020965

    goto :goto_0

    .line 2180083
    :pswitch_3
    const v0, 0x7f0209ba

    goto :goto_0

    .line 2180084
    :pswitch_4
    const v0, 0x7f02078c

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
