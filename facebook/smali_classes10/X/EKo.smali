.class public LX/EKo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/EKM;


# direct methods
.method public constructor <init>(LX/EKM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2107236
    iput-object p1, p0, LX/EKo;->a:LX/EKM;

    .line 2107237
    return-void
.end method

.method public static a(LX/0QB;)LX/EKo;
    .locals 4

    .prologue
    .line 2107238
    const-class v1, LX/EKo;

    monitor-enter v1

    .line 2107239
    :try_start_0
    sget-object v0, LX/EKo;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107240
    sput-object v2, LX/EKo;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107241
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107242
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107243
    new-instance p0, LX/EKo;

    invoke-static {v0}, LX/EKM;->a(LX/0QB;)LX/EKM;

    move-result-object v3

    check-cast v3, LX/EKM;

    invoke-direct {p0, v3}, LX/EKo;-><init>(LX/EKM;)V

    .line 2107244
    move-object v0, p0

    .line 2107245
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107246
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107247
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107248
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/EKo;LX/1De;LX/0Px;Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;)LX/1Dg;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsCandidateInfo;",
            ">;",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsElectionRace;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x2

    const/4 v2, 0x0

    .line 2107249
    invoke-virtual {p3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2107250
    :goto_0
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08229b

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v2

    invoke-virtual {p3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->e()D

    move-result-wide v6

    double-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v9

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 2107251
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08229c

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->e()D

    move-result-wide v6

    double-to-int v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2107252
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v3, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b172a

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v4, 0x7f0b0052

    invoke-virtual {v0, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v4, 0x7f0a00ab

    invoke-virtual {v0, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v0

    const v1, 0x3f99999a    # 1.2f

    invoke-interface {v0, v1}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2107253
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2107254
    invoke-virtual {p3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_1

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    .line 2107255
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2107256
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2107257
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2107258
    :cond_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_4

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;

    .line 2107259
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateInfoModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2107260
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    .line 2107261
    if-eqz v0, :cond_3

    .line 2107262
    iget-object v6, p0, LX/EKo;->a:LX/EKM;

    const/4 v7, 0x0

    .line 2107263
    new-instance v8, LX/EKL;

    invoke-direct {v8, v6}, LX/EKL;-><init>(LX/EKM;)V

    .line 2107264
    sget-object v10, LX/EKM;->a:LX/0Zi;

    invoke-virtual {v10}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/EKK;

    .line 2107265
    if-nez v10, :cond_2

    .line 2107266
    new-instance v10, LX/EKK;

    invoke-direct {v10}, LX/EKK;-><init>()V

    .line 2107267
    :cond_2
    invoke-static {v10, p1, v7, v7, v8}, LX/EKK;->a$redex0(LX/EKK;LX/1De;IILX/EKL;)V

    .line 2107268
    move-object v8, v10

    .line 2107269
    move-object v7, v8

    .line 2107270
    move-object v6, v7

    .line 2107271
    iget-object v7, v6, LX/EKK;->a:LX/EKL;

    iput-object v0, v7, LX/EKL;->a:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsCandidateModel;

    .line 2107272
    iget-object v7, v6, LX/EKK;->d:Ljava/util/BitSet;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 2107273
    move-object v0, v6

    .line 2107274
    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2107275
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2107276
    :cond_4
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f08229d

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionRaceModel;->b()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a00ab

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2107277
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
