.class public LX/EjB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0aG;


# direct methods
.method public constructor <init>(LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2160939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160940
    iput-object p1, p0, LX/EjB;->a:LX/0aG;

    .line 2160941
    return-void
.end method

.method public static b(LX/0QB;)LX/EjB;
    .locals 2

    .prologue
    .line 2160947
    new-instance v1, LX/EjB;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-direct {v1, v0}, LX/EjB;-><init>(LX/0aG;)V

    .line 2160948
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2160942
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2160943
    new-instance v1, Lcom/facebook/interstitial/api/FetchInterstitialsParams;

    const-string v2, "1907"

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/api/FetchInterstitialsParams;-><init>(LX/0Px;)V

    .line 2160944
    const-string v2, "fetchAndUpdateInterstitialsParams"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2160945
    iget-object v1, p0, LX/EjB;->a:LX/0aG;

    const-string v2, "interstitials_fetch_and_update"

    const v3, -0x3b1e6803

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2160946
    return-void
.end method
