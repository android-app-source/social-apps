.class public final LX/Exq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5P5;


# instance fields
.field public final synthetic a:LX/83X;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic d:Z

.field public final synthetic e:Z

.field public final synthetic f:LX/Exs;


# direct methods
.method public constructor <init>(LX/Exs;LX/83X;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;ZZ)V
    .locals 0

    .prologue
    .line 2185353
    iput-object p1, p0, LX/Exq;->f:LX/Exs;

    iput-object p2, p0, LX/Exq;->a:LX/83X;

    iput-object p3, p0, LX/Exq;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p4, p0, LX/Exq;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-boolean p5, p0, LX/Exq;->d:Z

    iput-boolean p6, p0, LX/Exq;->e:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2185338
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2185339
    iget-object v0, p0, LX/Exq;->a:LX/83X;

    iget-object v1, p0, LX/Exq;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-interface {v0, v1}, LX/2np;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185340
    iget-object v0, p0, LX/Exq;->a:LX/83X;

    iget-object v1, p0, LX/Exq;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-interface {v0, v1}, LX/83X;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2185341
    iget-object v0, p0, LX/Exq;->f:LX/Exs;

    iget-object v1, p0, LX/Exq;->a:LX/83X;

    invoke-interface {v1}, LX/2lr;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/Exs;->b(LX/Exs;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2185342
    iget-boolean v0, p0, LX/Exq;->d:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/Exq;->e:Z

    if-eqz v0, :cond_1

    .line 2185343
    iget-object v0, p0, LX/Exq;->f:LX/Exs;

    iget-object v0, v0, LX/Exs;->r:LX/Ew6;

    if-eqz v0, :cond_0

    .line 2185344
    iget-object v0, p0, LX/Exq;->f:LX/Exs;

    iget-object v0, v0, LX/Exs;->r:LX/Ew6;

    iget-object v1, p0, LX/Exq;->a:LX/83X;

    invoke-interface {v1}, LX/2lr;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/Ew6;->a(J)V

    .line 2185345
    :cond_0
    :goto_0
    return-void

    .line 2185346
    :cond_1
    iget-object v0, p0, LX/Exq;->f:LX/Exs;

    iget-object v1, p0, LX/Exq;->a:LX/83X;

    invoke-interface {v1}, LX/2lr;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/Exs;->a(LX/Exs;J)LX/EyP;

    move-result-object v0

    .line 2185347
    instance-of v1, v0, Lcom/facebook/friending/common/list/FriendListItemView;

    if-eqz v1, :cond_0

    .line 2185348
    check-cast v0, Lcom/facebook/friending/common/list/FriendListItemView;

    .line 2185349
    iget-object v1, p0, LX/Exq;->f:LX/Exs;

    iget-object v2, p0, LX/Exq;->a:LX/83X;

    invoke-static {v1, v0, v2}, LX/Exs;->c(LX/Exs;LX/EyP;LX/83X;)V

    .line 2185350
    iget-object v1, p0, LX/Exq;->f:LX/Exs;

    iget-object v2, p0, LX/Exq;->a:LX/83X;

    invoke-virtual {v1, v0, v2}, LX/Exs;->b(LX/EyQ;LX/83X;)V

    .line 2185351
    iget-object v1, p0, LX/Exq;->f:LX/Exs;

    iget-object v2, p0, LX/Exq;->a:LX/83X;

    invoke-virtual {v1, v0, v2}, LX/Exs;->c(LX/EyQ;LX/83X;)V

    .line 2185352
    iget-object v1, p0, LX/Exq;->f:LX/Exs;

    iget-object v2, p0, LX/Exq;->a:LX/83X;

    iget-boolean v3, p0, LX/Exq;->e:Z

    invoke-static {v1, v0, v2, v3}, LX/Exs;->d(LX/Exs;LX/EyQ;LX/83X;Z)V

    goto :goto_0
.end method
