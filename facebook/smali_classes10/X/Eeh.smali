.class public final enum LX/Eeh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Eeh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Eeh;

.field public static final enum BSDIFF:LX/Eeh;

.field public static final enum ZIPDIFF:LX/Eeh;


# instance fields
.field private name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2153059
    new-instance v0, LX/Eeh;

    const-string v1, "BSDIFF"

    const-string v2, "BSDIFF"

    invoke-direct {v0, v1, v3, v2}, LX/Eeh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eeh;->BSDIFF:LX/Eeh;

    .line 2153060
    new-instance v0, LX/Eeh;

    const-string v1, "ZIPDIFF"

    const-string v2, "ZIPDIFF"

    invoke-direct {v0, v1, v4, v2}, LX/Eeh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eeh;->ZIPDIFF:LX/Eeh;

    .line 2153061
    const/4 v0, 0x2

    new-array v0, v0, [LX/Eeh;

    sget-object v1, LX/Eeh;->BSDIFF:LX/Eeh;

    aput-object v1, v0, v3

    sget-object v1, LX/Eeh;->ZIPDIFF:LX/Eeh;

    aput-object v1, v0, v4

    sput-object v0, LX/Eeh;->$VALUES:[LX/Eeh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2153062
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2153063
    iput-object p3, p0, LX/Eeh;->name:Ljava/lang/String;

    .line 2153064
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Eeh;
    .locals 1

    .prologue
    .line 2153065
    const-class v0, LX/Eeh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Eeh;

    return-object v0
.end method

.method public static values()[LX/Eeh;
    .locals 1

    .prologue
    .line 2153066
    sget-object v0, LX/Eeh;->$VALUES:[LX/Eeh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Eeh;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2153067
    iget-object v0, p0, LX/Eeh;->name:Ljava/lang/String;

    return-object v0
.end method
