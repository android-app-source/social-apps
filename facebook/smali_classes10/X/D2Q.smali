.class public LX/D2Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0TD;

.field private final b:LX/0TD;

.field public c:LX/D1q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/D1q",
            "<",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/D1q",
            "<",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>(LX/0TD;LX/0TD;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/timeline/profilevideo/service/ForProfileVideoWorker;
        .end annotation
    .end param
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1958641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958642
    iput-object p1, p0, LX/D2Q;->a:LX/0TD;

    .line 1958643
    iput-object p2, p0, LX/D2Q;->b:LX/0TD;

    .line 1958644
    iput-boolean v0, p0, LX/D2Q;->e:Z

    .line 1958645
    iput-boolean v0, p0, LX/D2Q;->f:Z

    .line 1958646
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/D2Q;->d:Ljava/util/List;

    .line 1958647
    return-void
.end method

.method public static b(LX/D2Q;)V
    .locals 3

    .prologue
    .line 1958648
    iget-object v0, p0, LX/D2Q;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1958649
    iget-object v0, p0, LX/D2Q;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D1q;

    .line 1958650
    iget-object v1, p0, LX/D2Q;->a:LX/0TD;

    .line 1958651
    iget-object v2, v0, LX/D1q;->a:Ljava/util/concurrent/Callable;

    move-object v2, v2

    .line 1958652
    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1958653
    iget-object v2, v0, LX/D1q;->b:LX/0TF;

    move-object v0, v2

    .line 1958654
    iget-object v2, p0, LX/D2Q;->b:LX/0TD;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1958655
    :goto_0
    return-void

    .line 1958656
    :cond_0
    invoke-static {p0}, LX/D2Q;->c(LX/D2Q;)V

    goto :goto_0
.end method

.method public static c(LX/D2Q;)V
    .locals 3

    .prologue
    .line 1958657
    iget-object v0, p0, LX/D2Q;->c:LX/D1q;

    if-eqz v0, :cond_0

    .line 1958658
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D2Q;->f:Z

    .line 1958659
    iget-object v0, p0, LX/D2Q;->a:LX/0TD;

    iget-object v1, p0, LX/D2Q;->c:LX/D1q;

    .line 1958660
    iget-object v2, v1, LX/D1q;->a:Ljava/util/concurrent/Callable;

    move-object v1, v2

    .line 1958661
    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1958662
    iget-object v1, p0, LX/D2Q;->c:LX/D1q;

    .line 1958663
    iget-object v2, v1, LX/D1q;->b:LX/0TF;

    move-object v1, v2

    .line 1958664
    iget-object v2, p0, LX/D2Q;->b:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1958665
    const/4 v0, 0x0

    iput-object v0, p0, LX/D2Q;->c:LX/D1q;

    .line 1958666
    :goto_0
    return-void

    .line 1958667
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D2Q;->e:Z

    goto :goto_0
.end method
