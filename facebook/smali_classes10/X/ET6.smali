.class public final LX/ET6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ETB;

.field private final b:Lcom/facebook/video/videohome/data/VideoHomeItem;

.field private final c:LX/ETO;


# direct methods
.method public constructor <init>(LX/ETB;Lcom/facebook/video/videohome/data/VideoHomeItem;LX/ETO;)V
    .locals 0

    .prologue
    .line 2123934
    iput-object p1, p0, LX/ET6;->a:LX/ETB;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2123935
    iput-object p2, p0, LX/ET6;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2123936
    iput-object p3, p0, LX/ET6;->c:LX/ETO;

    .line 2123937
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2123932
    iget-object v0, p0, LX/ET6;->c:LX/ETO;

    invoke-interface {v0}, LX/ETO;->a()V

    .line 2123933
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2123926
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2123927
    invoke-static {p1}, LX/ETB;->b(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    .line 2123928
    iget-object v1, p0, LX/ET6;->c:LX/ETO;

    invoke-interface {v1}, LX/ETO;->a()V

    .line 2123929
    if-nez v0, :cond_0

    .line 2123930
    :goto_0
    return-void

    .line 2123931
    :cond_0
    iget-object v1, p0, LX/ET6;->a:LX/ETB;

    iget-object v2, p0, LX/ET6;->b:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v1, v2, v0}, LX/ETB;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;)V

    goto :goto_0
.end method
