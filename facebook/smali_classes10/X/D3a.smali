.class public LX/D3a;
.super Landroid/preference/DialogPreference;
.source ""


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:LX/11R;

.field public d:I

.field private e:LX/1Bf;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/view/LayoutInflater;LX/11R;LX/1Bf;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1960137
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1960138
    iput-object p2, p0, LX/D3a;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1960139
    iput-object p3, p0, LX/D3a;->b:Landroid/view/LayoutInflater;

    .line 1960140
    iput-object p4, p0, LX/D3a;->c:LX/11R;

    .line 1960141
    iput-object p5, p0, LX/D3a;->e:LX/1Bf;

    .line 1960142
    sget-object v0, LX/1C0;->f:LX/0Tn;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/D3a;->setKey(Ljava/lang/String;)V

    .line 1960143
    invoke-direct {p0}, LX/D3a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/D3a;->setSummary(Ljava/lang/CharSequence;)V

    .line 1960144
    return-void
.end method

.method public static a(LX/0QB;)LX/D3a;
    .locals 7

    .prologue
    .line 1960134
    new-instance v1, LX/D3a;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v5

    check-cast v5, LX/11R;

    invoke-static {p0}, LX/1Bf;->a(LX/0QB;)LX/1Bf;

    move-result-object v6

    check-cast v6, LX/1Bf;

    invoke-direct/range {v1 .. v6}, LX/D3a;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/view/LayoutInflater;LX/11R;LX/1Bf;)V

    .line 1960135
    move-object v0, v1

    .line 1960136
    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 5

    .prologue
    const-wide/16 v2, -0x1

    .line 1960130
    iget-object v0, p0, LX/D3a;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1C0;->f:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 1960131
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1960132
    const-string v0, ""

    .line 1960133
    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Last Cleared on "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/D3a;->c:LX/11R;

    sget-object v4, LX/1lB;->EXACT_TIME_DATE_STYLE:LX/1lB;

    invoke-virtual {v3, v4, v0, v1}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final onBindView(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1960125
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindView(Landroid/view/View;)V

    .line 1960126
    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1960127
    iget v1, p0, LX/D3a;->d:I

    if-eqz v1, :cond_0

    .line 1960128
    invoke-virtual {p0}, LX/D3a;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, LX/D3a;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1960129
    :cond_0
    return-void
.end method

.method public final onDialogClosed(Z)V
    .locals 4

    .prologue
    .line 1960114
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 1960115
    if-nez p1, :cond_0

    .line 1960116
    :goto_0
    return-void

    .line 1960117
    :cond_0
    invoke-virtual {p0}, LX/D3a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/048;->a(Landroid/content/Context;)V

    .line 1960118
    iget-object v0, p0, LX/D3a;->e:LX/1Bf;

    invoke-virtual {p0}, LX/D3a;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Bf;->a(Landroid/content/Context;)V

    .line 1960119
    iget-object v0, p0, LX/D3a;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 1960120
    sget-object v1, LX/1C0;->f:LX/0Tn;

    .line 1960121
    sget-object v2, LX/0SF;->a:LX/0SF;

    move-object v2, v2

    .line 1960122
    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 1960123
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1960124
    invoke-direct {p0}, LX/D3a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/D3a;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 1960107
    invoke-virtual {p1, v4}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 1960108
    const v0, 0x7f081cfd

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1960109
    iget-object v0, p0, LX/D3a;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0301dd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1960110
    const v0, 0x7f0d0799

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1960111
    invoke-virtual {p0}, LX/D3a;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081cfe

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, LX/D3a;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0800c7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1960112
    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1960113
    return-void
.end method
