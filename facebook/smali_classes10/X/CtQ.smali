.class public LX/CtQ;
.super LX/Csu;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Coc;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/Chw;

.field public final d:LX/1OX;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;FF)V
    .locals 2

    .prologue
    .line 1944743
    invoke-direct {p0, p1, p2, p3}, LX/Csu;-><init>(Landroid/support/v7/widget/RecyclerView;FF)V

    .line 1944744
    const-class v0, LX/CtQ;

    invoke-static {v0, p0}, LX/CtQ;->a(Ljava/lang/Class;LX/02k;)V

    .line 1944745
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    .line 1944746
    new-instance v0, LX/CtO;

    invoke-direct {v0, p0}, LX/CtO;-><init>(LX/CtQ;)V

    iput-object v0, p0, LX/CtQ;->c:LX/Chw;

    .line 1944747
    iget-object v0, p0, LX/CtQ;->a:LX/Chv;

    iget-object v1, p0, LX/CtQ;->c:LX/Chw;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1944748
    new-instance v0, LX/CtP;

    invoke-direct {v0, p0}, LX/CtP;-><init>(LX/CtQ;)V

    iput-object v0, p0, LX/CtQ;->d:LX/1OX;

    .line 1944749
    iget-object v0, p0, LX/CtQ;->d:LX/1OX;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 1944750
    return-void
.end method

.method public constructor <init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V
    .locals 2

    .prologue
    .line 1944751
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v0, v1}, LX/CtQ;-><init>(Landroid/support/v7/widget/RecyclerView;FF)V

    .line 1944752
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CtQ;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object p0

    check-cast p0, LX/Chv;

    iput-object p0, p1, LX/CtQ;->a:LX/Chv;

    return-void
.end method

.method public static declared-synchronized g(LX/CtQ;)V
    .locals 1

    .prologue
    .line 1944753
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1944754
    invoke-virtual {p0}, LX/CtQ;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1944755
    monitor-exit p0

    return-void

    .line 1944756
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/Coc;)V
    .locals 1

    .prologue
    .line 1944757
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1944758
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1944759
    invoke-virtual {p0}, LX/CtQ;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1944760
    :cond_0
    monitor-exit p0

    return-void

    .line 1944761
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/Coc;)V
    .locals 1

    .prologue
    .line 1944762
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1944763
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1944764
    :cond_0
    invoke-virtual {p0}, LX/CtQ;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1944765
    monitor-exit p0

    return-void

    .line 1944766
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 4

    .prologue
    .line 1944767
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 1944768
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1944769
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/Csu;->a:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 1944770
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 1944771
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v1

    .line 1944772
    invoke-virtual {v0}, LX/1OR;->v()I

    move-result v0

    add-int v2, v1, v0

    move v0, v1

    .line 1944773
    :goto_1
    if-gt v0, v2, :cond_3

    .line 1944774
    iget-object v1, p0, LX/Csu;->a:Landroid/support/v7/widget/RecyclerView;

    move-object v1, v1

    .line 1944775
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->c(I)LX/1a1;

    move-result-object v1

    .line 1944776
    instance-of v3, v1, LX/Cs4;

    if-eqz v3, :cond_4

    .line 1944777
    check-cast v1, LX/Cs4;

    .line 1944778
    invoke-virtual {v1}, LX/Cs4;->w()LX/CnT;

    move-result-object v1

    .line 1944779
    iget-object v3, v1, LX/CnT;->d:LX/CnG;

    move-object v1, v3

    .line 1944780
    instance-of v3, v1, LX/CpT;

    if-eqz v3, :cond_4

    .line 1944781
    check-cast v1, LX/CpT;

    .line 1944782
    :goto_2
    move-object v1, v1

    .line 1944783
    if-eqz v1, :cond_2

    .line 1944784
    invoke-virtual {v1}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v3

    .line 1944785
    if-eqz v3, :cond_5

    .line 1944786
    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v3

    .line 1944787
    :goto_3
    move v1, v3

    .line 1944788
    if-eqz v1, :cond_2

    .line 1944789
    const/4 v0, 0x1

    .line 1944790
    :goto_4
    move v0, v0

    .line 1944791
    if-nez v0, :cond_0

    .line 1944792
    invoke-virtual {p0}, LX/CtQ;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1944793
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1944794
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1944795
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    :cond_5
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public declared-synchronized e()V
    .locals 4

    .prologue
    .line 1944796
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1944797
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Coc;

    .line 1944798
    invoke-interface {v0}, LX/Coc;->p()Landroid/view/View;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1944799
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1944800
    :cond_0
    :try_start_1
    invoke-virtual {p0, v1}, LX/Csu;->a(Ljava/util/Collection;)Landroid/view/View;

    move-result-object v1

    .line 1944801
    if-eqz v1, :cond_2

    .line 1944802
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Coc;

    .line 1944803
    invoke-interface {v0}, LX/Coc;->p()Landroid/view/View;

    move-result-object v3

    if-ne v1, v3, :cond_1

    invoke-interface {v0}, LX/Coc;->q()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1944804
    invoke-interface {v0}, LX/Coc;->r()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1944805
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1944806
    iget-object v0, p0, LX/Csu;->a:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 1944807
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
