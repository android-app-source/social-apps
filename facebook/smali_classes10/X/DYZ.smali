.class public LX/DYZ;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/DYa;",
        "LX/DYY;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DYZ;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2011664
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 2011665
    return-void
.end method

.method public static a(LX/0QB;)LX/DYZ;
    .locals 3

    .prologue
    .line 2011666
    sget-object v0, LX/DYZ;->a:LX/DYZ;

    if-nez v0, :cond_1

    .line 2011667
    const-class v1, LX/DYZ;

    monitor-enter v1

    .line 2011668
    :try_start_0
    sget-object v0, LX/DYZ;->a:LX/DYZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2011669
    if-eqz v2, :cond_0

    .line 2011670
    :try_start_1
    new-instance v0, LX/DYZ;

    invoke-direct {v0}, LX/DYZ;-><init>()V

    .line 2011671
    move-object v0, v0

    .line 2011672
    sput-object v0, LX/DYZ;->a:LX/DYZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2011673
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2011674
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2011675
    :cond_1
    sget-object v0, LX/DYZ;->a:LX/DYZ;

    return-object v0

    .line 2011676
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2011677
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
