.class public final LX/Djt;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dju;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/Djv;


# direct methods
.method public constructor <init>(LX/Djv;LX/Dju;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2033827
    iput-object p1, p0, LX/Djt;->c:LX/Djv;

    iput-object p2, p0, LX/Djt;->a:LX/Dju;

    iput-object p3, p0, LX/Djt;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2033828
    iget-object v0, p0, LX/Djt;->c:LX/Djv;

    iget-object v0, v0, LX/Djv;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08003c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2033829
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2033830
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2033831
    iget-object v0, p0, LX/Djt;->a:LX/Dju;

    if-eqz v0, :cond_1

    .line 2033832
    const/4 v0, 0x0

    .line 2033833
    if-eqz p1, :cond_0

    .line 2033834
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2033835
    if-eqz v1, :cond_0

    .line 2033836
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2033837
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentAdminNotesMutationModels$AppointmentAdminNotesMutationFragmentModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;

    move-result-object v0

    .line 2033838
    :cond_0
    iget-object v1, p0, LX/Djt;->a:LX/Dju;

    invoke-interface {v1, v0}, LX/Dju;->a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;)V

    .line 2033839
    :cond_1
    iget-object v1, p0, LX/Djt;->b:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_2
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2033840
    :goto_1
    return-void

    .line 2033841
    :sswitch_0
    const-string v2, "ADD"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "UPDATE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "DELETE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    .line 2033842
    :pswitch_0
    iget-object v0, p0, LX/Djt;->c:LX/Djv;

    iget-object v0, v0, LX/Djv;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082bea

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1

    .line 2033843
    :pswitch_1
    iget-object v0, p0, LX/Djt;->c:LX/Djv;

    iget-object v0, v0, LX/Djv;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082beb

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1

    .line 2033844
    :pswitch_2
    iget-object v0, p0, LX/Djt;->c:LX/Djv;

    iget-object v0, v0, LX/Djv;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082bec

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x6a6cd337 -> :sswitch_1
        0xfc81 -> :sswitch_0
        0x77f979ab -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
