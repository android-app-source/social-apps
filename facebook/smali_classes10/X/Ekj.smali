.class public LX/Ekj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:LX/Ekz;

.field public final c:LX/Eko;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/Ekl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/Ekk;

.field public final f:LX/FXM;

.field private volatile g:Z

.field private volatile h:LX/0nB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private volatile i:LX/El6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Eki;)V
    .locals 2

    .prologue
    .line 2163756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163757
    iget-object v0, p1, LX/Eki;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2163758
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must set friendlyName"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163759
    :cond_0
    iget-object v0, p1, LX/Eki;->b:Ljava/lang/String;

    iput-object v0, p0, LX/Ekj;->a:Ljava/lang/String;

    .line 2163760
    iget-object v0, p1, LX/Eki;->a:LX/Ekz;

    iput-object v0, p0, LX/Ekj;->b:LX/Ekz;

    .line 2163761
    iget-object v0, p1, LX/Eki;->c:LX/Eko;

    iput-object v0, p0, LX/Ekj;->c:LX/Eko;

    .line 2163762
    iget-object v0, p1, LX/Eki;->d:LX/Ekl;

    iput-object v0, p0, LX/Ekj;->d:LX/Ekl;

    .line 2163763
    iget-object v0, p1, LX/Eki;->e:LX/FXM;

    if-nez v0, :cond_1

    .line 2163764
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must set callback"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163765
    :cond_1
    iget-object v0, p1, LX/Eki;->e:LX/FXM;

    iput-object v0, p0, LX/Ekj;->f:LX/FXM;

    .line 2163766
    const-string v0, "release"

    .line 2163767
    const/4 v1, 0x0

    invoke-static {v1, v0}, LX/0nB;->a(LX/0nB;Ljava/lang/String;)LX/0nB;

    move-result-object v1

    move-object v0, v1

    .line 2163768
    iput-object v0, p0, LX/Ekj;->h:LX/0nB;

    .line 2163769
    new-instance v0, LX/Ekk;

    invoke-direct {v0}, LX/Ekk;-><init>()V

    iput-object v0, p0, LX/Ekj;->e:LX/Ekk;

    .line 2163770
    return-void
.end method


# virtual methods
.method public final d()LX/Ekz;
    .locals 2

    .prologue
    .line 2163752
    invoke-virtual {p0}, LX/Ekj;->j()V

    .line 2163753
    iget-object v0, p0, LX/Ekj;->i:LX/El6;

    if-eqz v0, :cond_0

    .line 2163754
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must be called before AppRequest.buildRequest"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163755
    :cond_0
    iget-object v0, p0, LX/Ekj;->b:LX/Ekz;

    return-object v0
.end method

.method public final f()LX/El6;
    .locals 2

    .prologue
    .line 2163748
    invoke-virtual {p0}, LX/Ekj;->j()V

    .line 2163749
    iget-object v0, p0, LX/Ekj;->i:LX/El6;

    if-nez v0, :cond_0

    .line 2163750
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must call AppRequest.buildRequest"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163751
    :cond_0
    iget-object v0, p0, LX/Ekj;->i:LX/El6;

    return-object v0
.end method

.method public final finalize()V
    .locals 1

    .prologue
    .line 2163725
    :try_start_0
    iget-object v0, p0, LX/Ekj;->h:LX/0nB;

    invoke-static {v0}, LX/0nB;->b(LX/0nB;)V

    .line 2163726
    iget-boolean v0, p0, LX/Ekj;->g:Z

    if-nez v0, :cond_0

    .line 2163727
    invoke-virtual {p0}, LX/Ekj;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2163728
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 2163729
    return-void

    .line 2163730
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 2163743
    invoke-virtual {p0}, LX/Ekj;->j()V

    .line 2163744
    iget-object v0, p0, LX/Ekj;->i:LX/El6;

    if-eqz v0, :cond_0

    .line 2163745
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Request has already been built."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163746
    :cond_0
    iget-object v0, p0, LX/Ekj;->b:LX/Ekz;

    invoke-interface {v0}, LX/Ekz;->a()LX/El6;

    move-result-object v0

    iput-object v0, p0, LX/Ekj;->i:LX/El6;

    .line 2163747
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 2163740
    iget-boolean v0, p0, LX/Ekj;->g:Z

    if-eqz v0, :cond_0

    .line 2163741
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Request has already been executed and may not be re-used"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163742
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2163731
    iget-boolean v0, p0, LX/Ekj;->g:Z

    if-eqz v0, :cond_0

    .line 2163732
    :goto_0
    return-void

    .line 2163733
    :cond_0
    iget-object v0, p0, LX/Ekj;->i:LX/El6;

    if-eqz v0, :cond_1

    .line 2163734
    iget-object v0, p0, LX/Ekj;->i:LX/El6;

    .line 2163735
    iget-object v1, v0, LX/El6;->b:LX/El1;

    move-object v0, v1

    .line 2163736
    instance-of v1, v0, LX/Eks;

    if-eqz v1, :cond_1

    .line 2163737
    check-cast v0, LX/Eks;

    invoke-interface {v0}, LX/Eks;->c()V

    .line 2163738
    :cond_1
    iget-object v0, p0, LX/Ekj;->h:LX/0nB;

    invoke-static {v0}, LX/0nB;->a(LX/0nB;)V

    .line 2163739
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ekj;->g:Z

    goto :goto_0
.end method
