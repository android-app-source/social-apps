.class public final LX/EKS;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EKT;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionsModuleInterfaces$SearchResultsElectionsNode;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/Cwx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/EKT;


# direct methods
.method public constructor <init>(LX/EKT;)V
    .locals 1

    .prologue
    .line 2106708
    iput-object p1, p0, LX/EKS;->c:LX/EKT;

    .line 2106709
    move-object v0, p1

    .line 2106710
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2106711
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2106712
    const-string v0, "SearchResultsElectionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2106713
    if-ne p0, p1, :cond_1

    .line 2106714
    :cond_0
    :goto_0
    return v0

    .line 2106715
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2106716
    goto :goto_0

    .line 2106717
    :cond_3
    check-cast p1, LX/EKS;

    .line 2106718
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2106719
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2106720
    if-eq v2, v3, :cond_0

    .line 2106721
    iget-object v2, p0, LX/EKS;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/EKS;->a:LX/CzL;

    iget-object v3, p1, LX/EKS;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2106722
    goto :goto_0

    .line 2106723
    :cond_5
    iget-object v2, p1, LX/EKS;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2106724
    :cond_6
    iget-object v2, p0, LX/EKS;->b:LX/Cwx;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/EKS;->b:LX/Cwx;

    iget-object v3, p1, LX/EKS;->b:LX/Cwx;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2106725
    goto :goto_0

    .line 2106726
    :cond_7
    iget-object v2, p1, LX/EKS;->b:LX/Cwx;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
