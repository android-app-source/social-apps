.class public LX/DLX;
.super LX/3AP;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V
    .locals 10
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1988330
    const-string v3, "fileordoc"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, LX/3AP;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    .line 1988331
    return-void
.end method

.method public static b(LX/0QB;)LX/DLX;
    .locals 9

    .prologue
    .line 1988332
    new-instance v0, LX/DLX;

    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v2

    check-cast v2, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v3

    check-cast v3, LX/1Gm;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/1Gk;->a(LX/0QB;)LX/1Gk;

    move-result-object v5

    check-cast v5, LX/1Gk;

    invoke-static {p0}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v6

    check-cast v6, LX/1Gl;

    invoke-static {p0}, LX/1Gn;->a(LX/0QB;)LX/1Gn;

    move-result-object v7

    check-cast v7, LX/1Go;

    invoke-static {p0}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v8

    check-cast v8, LX/13t;

    invoke-direct/range {v0 .. v8}, LX/DLX;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    .line 1988333
    return-object v0
.end method
