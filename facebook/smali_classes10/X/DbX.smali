.class public LX/DbX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/DbU;

.field public final b:LX/DbZ;

.field public final c:LX/DbM;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/17Y;

.field public f:Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;


# direct methods
.method public constructor <init>(LX/DbU;LX/DbZ;LX/DbM;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2017158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2017159
    iput-object p1, p0, LX/DbX;->a:LX/DbU;

    .line 2017160
    iput-object p2, p0, LX/DbX;->b:LX/DbZ;

    .line 2017161
    iput-object p3, p0, LX/DbX;->c:LX/DbM;

    .line 2017162
    iput-object p4, p0, LX/DbX;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2017163
    iput-object p5, p0, LX/DbX;->e:LX/17Y;

    .line 2017164
    return-void
.end method

.method public static b(LX/0QB;)LX/DbX;
    .locals 6

    .prologue
    .line 2017165
    new-instance v0, LX/DbX;

    const-class v1, LX/DbU;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/DbU;

    invoke-static {p0}, LX/DbZ;->a(LX/0QB;)LX/DbZ;

    move-result-object v2

    check-cast v2, LX/DbZ;

    invoke-static {p0}, LX/DbM;->b(LX/0QB;)LX/DbM;

    move-result-object v3

    check-cast v3, LX/DbM;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-direct/range {v0 .. v5}, LX/DbX;-><init>(LX/DbU;LX/DbZ;LX/DbM;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 2017166
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2017167
    iput-object p1, p0, LX/DbX;->k:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    .line 2017168
    iput-object p2, p0, LX/DbX;->j:Ljava/lang/String;

    .line 2017169
    iput-object p3, p0, LX/DbX;->g:Ljava/lang/String;

    .line 2017170
    iget-object v0, p0, LX/DbX;->a:LX/DbU;

    iget-object v1, p0, LX/DbX;->g:Ljava/lang/String;

    iget-object v2, p0, LX/DbX;->j:Ljava/lang/String;

    .line 2017171
    new-instance p2, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

    invoke-static {v0}, LX/DbM;->b(LX/0QB;)LX/DbM;

    move-result-object v3

    check-cast v3, LX/DbM;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object p1

    check-cast p1, LX/23R;

    invoke-direct {p2, v3, p1, v1, v2}, Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;-><init>(LX/DbM;LX/23R;Ljava/lang/String;Ljava/lang/String;)V

    .line 2017172
    move-object v0, p2

    .line 2017173
    iput-object v0, p0, LX/DbX;->f:Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

    .line 2017174
    iget-object v0, p0, LX/DbX;->k:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->setVisibility(I)V

    .line 2017175
    iget-object v0, p0, LX/DbX;->k:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    iget-object v1, p0, LX/DbX;->f:Lcom/facebook/localcontent/menus/FoodPhotosHScrollAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->setRecyclerAdapter(LX/1OM;)V

    .line 2017176
    iget-object v0, p0, LX/DbX;->k:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    new-instance v1, LX/DbV;

    invoke-direct {v1, p0}, LX/DbV;-><init>(LX/DbX;)V

    invoke-virtual {v0, v1}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->setRecyclerOnScrollListener(LX/1OX;)V

    .line 2017177
    iget-object v0, p0, LX/DbX;->k:Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;

    new-instance v1, LX/DbW;

    invoke-direct {v1, p0}, LX/DbW;-><init>(LX/DbX;)V

    invoke-virtual {v0, v1}, Lcom/facebook/localcontent/menus/FoodPhotosHscrollView;->setSeeMoreOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2017178
    iget-object v0, p0, LX/DbX;->b:LX/DbZ;

    iget-object v1, p0, LX/DbX;->j:Ljava/lang/String;

    iget-object v2, p0, LX/DbX;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p0}, LX/DbZ;->a(Ljava/lang/String;Ljava/lang/String;LX/DbX;)V

    .line 2017179
    return-void
.end method
