.class public final enum LX/Cqt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cqt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cqt;

.field public static final enum LANDSCAPE_LEFT:LX/Cqt;

.field public static final enum LANDSCAPE_RIGHT:LX/Cqt;

.field public static final enum PORTRAIT:LX/Cqt;


# instance fields
.field public mDegree:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1940502
    new-instance v0, LX/Cqt;

    const-string v1, "PORTRAIT"

    invoke-direct {v0, v1, v3, v3}, LX/Cqt;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Cqt;->PORTRAIT:LX/Cqt;

    .line 1940503
    new-instance v0, LX/Cqt;

    const-string v1, "LANDSCAPE_LEFT"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v4, v2}, LX/Cqt;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    .line 1940504
    new-instance v0, LX/Cqt;

    const-string v1, "LANDSCAPE_RIGHT"

    const/16 v2, -0x5a

    invoke-direct {v0, v1, v5, v2}, LX/Cqt;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    .line 1940505
    const/4 v0, 0x3

    new-array v0, v0, [LX/Cqt;

    sget-object v1, LX/Cqt;->PORTRAIT:LX/Cqt;

    aput-object v1, v0, v3

    sget-object v1, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    aput-object v1, v0, v4

    sget-object v1, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    aput-object v1, v0, v5

    sput-object v0, LX/Cqt;->$VALUES:[LX/Cqt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1940498
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1940499
    iput p3, p0, LX/Cqt;->mDegree:I

    .line 1940500
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cqt;
    .locals 1

    .prologue
    .line 1940497
    const-class v0, LX/Cqt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cqt;

    return-object v0
.end method

.method public static values()[LX/Cqt;
    .locals 1

    .prologue
    .line 1940501
    sget-object v0, LX/Cqt;->$VALUES:[LX/Cqt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cqt;

    return-object v0
.end method


# virtual methods
.method public final getDegree()I
    .locals 1

    .prologue
    .line 1940496
    iget v0, p0, LX/Cqt;->mDegree:I

    return v0
.end method

.method public final isLandscape()Z
    .locals 1

    .prologue
    .line 1940495
    sget-object v0, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
